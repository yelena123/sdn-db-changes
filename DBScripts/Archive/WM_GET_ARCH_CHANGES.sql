-- Script has to run as a part of Archive installation after all the archive tables created. 
Create table ARCH_AUTO_INCR 
(ACTION VARCHAR2(10),
arch_table_name VARCHAR2(30),
column_name VARCHAR2(30),
data_type varchar2(106),
data_length NUMBER,
data_precision NUMBER,
data_scale NUMBER
);

INSERT INTO    ARCH_AUTO_INCR ( ACTION,arch_table_name,column_name,data_type,data_length,data_precision,data_scale)
SELECT    'ADD' ACTION,utc.table_name,column_name,data_type,char_length,data_precision,data_scale
FROM all_tab_columns@WMSADMIN_DBLINK utc  WHERE (table_name, column_name) IN
                 (   SELECT distinct atc.table_name, atc.column_name
                    FROM all_tab_columns@WMSADMIN_DBLINK  atc  ,user_tab_columns  utc
                    where  atc.table_name = utc.table_name 
                  MINUS
                    SELECT distinct table_name , column_name
                    FROM user_tab_columns ) ;

INSERT INTO    ARCH_AUTO_INCR (ACTION,arch_table_name,column_name)
SELECT  distinct  'DROP' ACTION,table_name,column_name
FROM  (   SELECT distinct utc.table_name, utc.column_name
                    FROM user_tab_columns  utc where utc.column_name not in ('ARCHIVE_DTTM','RUN_ID') and utc.table_name not in 
					('ARCH_AUTO_INCR','DB_BUILD_HISTORY','ARCHIVE_TABLE_MAPPING') 
        MINUS
                    SELECT distinct atc.table_name , atc.column_name
                    FROM all_tab_columns@WMSADMIN_DBLINK  atc) ;
          
          
INSERT INTO    ARCH_AUTO_INCR (ACTION,arch_table_name,column_name,data_type,data_length)
SELECT    'MODIFY',table_name,column_name,data_type,char_length
  FROM (  SELECT distinct b.table_name, a.column_name,a.data_type, a.char_length
            FROM all_tab_columns@WMSADMIN_DBLINK a, user_tab_columns b
           WHERE     a.table_name =  b.table_name 
                 AND a.column_name = b.column_name
                 AND a.data_type = b.data_type
                 AND a.data_type = 'VARCHAR2'
                 AND a.char_length > b.char_length
            ORDER BY 1);
    
INSERT INTO    ARCH_AUTO_INCR (ACTION,arch_table_name,column_name,data_type,data_length,data_precision,data_scale)
SELECT    'MODIFY',table_name,column_name,data_type,null,data_precision,data_scale
  FROM (  SELECT distinct b.table_name, a.column_name,a.data_type, a.data_precision,a.data_scale
            FROM all_tab_columns@WMSADMIN_DBLINK a, user_tab_columns b
           WHERE     a.table_name =  b.table_name 
                 AND a.column_name = b.column_name
                 AND a.data_type = b.data_type
                 AND a.data_type = 'NUMBER'
                --AND a.data_scale = 0
                 AND ( (a.data_precision > b.data_precision and a.data_scale > b.data_scale)
                    OR (a.data_precision > b.data_precision and a.data_scale = b.data_scale))
                 ORDER BY 1);

          
/*INSERT INTO    ARCH_AUTO_INCR (ACTION,arch_table_name,column_name,data_type,data_length)          
SELECT  'MODIFY',table_name,column_name,data_type,char_length
  FROM (  SELECT distinct b.table_name, a.column_name,a.data_type, a.char_length
            FROM all_tab_columns@WMSADMIN_DBLINK a, user_tab_columns b
           WHERE     a.table_name = b.table_name 
                 AND a.column_name = b.column_name
                 AND a.data_type = b.data_type
                 AND a.data_type = 'VARCHAR2'
                 AND a.char_length < b.char_length        
        ORDER BY 1);
          

INSERT INTO    ARCH_AUTO_INCR (ACTION,arch_table_name,column_name,data_type,data_length,data_precision,data_scale)
SELECT    'MODIFY',table_name,column_name,data_type,null,data_precision,data_scale
  FROM (  SELECT distinct b.table_name, a.column_name,a.data_type, a.data_precision,a.data_scale
            FROM all_tab_columns@WMSADMIN_DBLINK a, user_tab_columns b
           WHERE     a.table_name = b.table_name 
                 AND a.column_name = b.column_name
                 AND a.data_type = b.data_type
                 AND a.data_type = 'NUMBER'
                 --AND a.data_scale = 0  
                 AND ( (a.data_precision < b.data_precision and a.data_scale < b.data_scale)
                    OR (a.data_precision < b.data_precision and a.data_scale = b.data_scale))
        ORDER BY 1); */ -- DB-6623
        
		
-- Add Columns

DECLARE
   msg1         VARCHAR2 (100);
   msg2         VARCHAR2 (100);
   msg3         VARCHAR2 (100);
   msg4         VARCHAR2 (100);
   x            NUMBER := 0;
   Ptablename   VARCHAR (30) := 'abc';
   Ntablename   VARCHAR (30) := NULL;
   v_query      VARCHAR2 (3000) := NULL;
BEGIN
   FOR Cursor_arch IN (SELECT arch_table_name,
                              column_name,
                              data_type,
                              data_length,
                              data_precision,
                              data_scale
                         FROM arch_auto_incr
                        WHERE action = 'ADD')
   LOOP
      Ntablename := Cursor_arch.arch_table_name;

      IF (Ptablename <> 'abc' AND Ntablename = Ptablename)
      THEN
         msg3 := ',';
         v_query := v_query || msg3;
      END IF;

      IF (Ntablename <> Ptablename)
      THEN
         IF (x = 1)
         THEN
            msg4 := ');';

            v_query := v_query || msg4;
            DBMS_OUTPUT.PUT_LINE (v_query);
             x := 0;
         END IF;

         v_query := NULL;

         msg1 := 'Alter table ' || Cursor_arch.arch_table_name || ' add  (';

         x := 1;
      END IF;

      SELECT Cursor_arch.column_name || ' ' || Cursor_arch.data_type
             || DECODE (Cursor_arch.data_type,
                        'NUMBER', '(',
                        'VARCHAR2', '(')
             || DECODE (Cursor_arch.data_type,
                        'NUMBER', Cursor_arch.data_precision,
                        'VARCHAR2', Cursor_arch.data_length)
             || DECODE (Cursor_arch.data_type,
                        'NUMBER', ',' || Cursor_arch.data_scale)
             || DECODE (Cursor_arch.data_type,
                        'NUMBER', ')',
                        'VARCHAR2', ')')
        INTO msg2
        FROM DUAL;

      IF LENGTH (v_query) > 0
      THEN
         v_query := v_query || msg2;
      ELSE
         v_query := msg1 || msg2;
      END IF;

      Ptablename := Cursor_arch.arch_table_name;
   END LOOP;

   IF (x = 1)
   THEN
      msg4 := ');';
      v_query := v_query || msg4;
      DBMS_OUTPUT.PUT_LINE (v_query);
       x := 0;
   END IF;
END;
/

--- Drop columns
DECLARE
   msg1         VARCHAR2 (100);
   val_ck       NUMBER := 0;
   msg2         VARCHAR2 (100);
   msg3         VARCHAR2 (100);
   msg4         VARCHAR2 (100);
   cnt          NUMBER := 0;
   x            NUMBER := 0;
   Ptablename   VARCHAR (30) := 'abc';
   Ntablename   VARCHAR (30) := NULL;
   v_query      VARCHAR2 (3000) := NULL;
BEGIN
   FOR Cursor_arch
      IN (SELECT b.arch_table_name, b.column_name
            FROM (  SELECT arch_table_name, 'DROP' ACTION
                      FROM arch_auto_incr
                     WHERE action = 'DROP'
                  GROUP BY arch_table_name
                    HAVING COUNT (column_name) > 1
                  ORDER BY arch_table_name) a,
                 arch_auto_incr b
           WHERE a.arch_table_name = b.arch_table_name
                 AND A.ACTION = B.ACTION)
   LOOP
      Ntablename := Cursor_arch.arch_table_name;

      IF (Ptablename <> 'abc' AND Ntablename = Ptablename)
      THEN
         msg3 := ',';
         v_query := v_query || msg3;
      END IF;

      IF (Ntablename <> Ptablename)
      THEN
         IF (x = 1)
         THEN
            msg4 := ');';

            v_query := v_query || msg4;

            val_ck := INSTR (v_query, ',', 1);

            IF val_ck = 0
            THEN
               v_query :=
                     SUBSTR (v_query, 1, (INSTR (v_query, '(', 1)) - 1)
                  || 'column '
                  || SUBSTR (v_query, (INSTR (v_query, '(', 1)));
            END IF;

            val_ck := 0;
            DBMS_OUTPUT.PUT_LINE (v_query);

            x := 0;
         END IF;

         v_query := NULL;
         msg1 := 'Alter table ' || Cursor_arch.arch_table_name || ' drop (';
         cnt := 0;


         x := 1;
      END IF;

      msg2 := Cursor_arch.column_name;

      cnt := cnt + 1;

      Ptablename := Cursor_arch.arch_table_name;

      --dbms_output.put_line('cnt '||cnt);

      IF (Ptablename <> Ntablename)
      THEN
         cnt := 0;
      END IF;

      IF (cnt = 10 AND Ptablename = Ntablename)
      THEN
         val_ck := INSTR (v_query, ',', 1);

         IF val_ck = 0
         THEN
            v_query :=
                  SUBSTR (v_query, 1, (INSTR (v_query, '(', 1)) - 1)
               || 'column '
               || SUBSTR (v_query, (INSTR (v_query, '(', 1)));
         END IF;

         val_ck := 0;
         v_query :=
            SUBSTR (v_query,
                    1,
                    (INSTR (v_query,
                            ',',
                            -1,
                            1)
                     - 1))
            || ');';
         DBMS_OUTPUT.PUT_LINE (v_query);
         msg1 := 'Alter table ' || Cursor_arch.arch_table_name || ' drop (';
         v_query := NULL;
         cnt := 0;
      END IF;

      IF LENGTH (v_query) > 0
      THEN
         v_query := v_query || msg2;
      ELSE
         v_query := msg1 || msg2;
      END IF;
   END LOOP;

   IF (x = 1)
   THEN
      val_ck := INSTR (v_query, ',', 1);

      IF val_ck = 0
      THEN
         v_query :=
               SUBSTR (v_query, 1, (INSTR (v_query, '(', 1)) - 1)
            || 'column '
            || SUBSTR (v_query, (INSTR (v_query, '(', 1)));
      END IF;

      msg4 := ');';
      v_query := v_query || msg4;
      DBMS_OUTPUT.PUT_LINE (v_query);
       x := 0;
   END IF;
END;
/

DECLARE
   msg1         VARCHAR2 (100);
   msg2         VARCHAR2 (100);
   msg3         VARCHAR2 (100);
   msg4         VARCHAR2 (100);
   x            NUMBER := 0;
   Ptablename   VARCHAR (30) := 'abc';
   Ntablename   VARCHAR (30) := NULL;
   v_query      VARCHAR2 (3000) := NULL;
BEGIN
   FOR Cursor_arch
      IN (SELECT b.arch_table_name, b.column_name
            FROM (  SELECT arch_table_name, 'DROP' ACTION
                      FROM arch_auto_incr
                     WHERE action = 'DROP'
                  GROUP BY arch_table_name
                    HAVING COUNT (column_name) = 1
                  ORDER BY arch_table_name) a,
                 arch_auto_incr b
           WHERE a.arch_table_name = b.arch_table_name
                 AND A.ACTION = B.ACTION)
   LOOP
      Ntablename := Cursor_arch.arch_table_name;

      IF (Ptablename <> 'abc' AND Ntablename = Ptablename)
      THEN
         msg3 := ',';
         v_query := v_query || msg3;
      END IF;

      IF (Ntablename <> Ptablename)
      THEN
         IF (x = 1)
         THEN
            msg4 := ';';

            v_query := v_query || msg4;
            DBMS_OUTPUT.PUT_LINE (v_query);

            x := 0;
         END IF;

         v_query := NULL;

         msg1 :=
            'Alter table ' || Cursor_arch.arch_table_name || ' drop column ';

         x := 1;
      END IF;

      msg2 := Cursor_arch.column_name;

      IF LENGTH (v_query) > 0
      THEN
         v_query := v_query || msg2;
      ELSE
         v_query := msg1 || msg2;
      END IF;

    Ptablename := Cursor_arch.arch_table_name;
      
   END LOOP;

   IF (x = 1)
   THEN
      msg4 := ';';
      v_query := v_query || msg4;
      DBMS_OUTPUT.PUT_LINE (v_query);
      x := 0;
   END IF;
END;
/

-- Modify Varchar and Number

DECLARE
   msg1         VARCHAR2 (100);
   msg2         VARCHAR2 (100);
   msg3         VARCHAR2 (100);
   msg4         VARCHAR2 (100);
   x            NUMBER := 0;
   Ptablename   VARCHAR (30) := 'abc';
   Ntablename   VARCHAR (30) := NULL;
   v_query      VARCHAR2 (3000) := NULL;
BEGIN
   FOR Cursor_arch IN (SELECT arch_table_name,
                              column_name,
                              data_type,
                              data_length,
                              data_precision,
                              data_scale
                         FROM ARCH_AUTO_INCR
                        WHERE action = 'MODIFY')
   LOOP
      Ntablename := Cursor_arch.arch_table_name;

      IF (Ptablename <> 'abc' AND Ntablename = Ptablename)
      THEN
         msg3 := ',';
         v_query := v_query || msg3;
      END IF;

      IF (Ntablename <> Ptablename)
      THEN
         IF (x = 1)
         THEN
            msg4 := ');';

            v_query := v_query || msg4;
            DBMS_OUTPUT.PUT_LINE (v_query);

            x := 0;
         END IF;

         v_query := NULL;

         msg1 := 'Alter table ' || Cursor_arch.arch_table_name || ' modify (';

         x := 1;
      END IF;

      SELECT Cursor_arch.column_name || ' ' || Cursor_arch.data_type
             || DECODE (Cursor_arch.data_type,
                        'NUMBER', '(',
                        'VARCHAR2', '(')
             || DECODE (Cursor_arch.data_type,
                        'NUMBER', Cursor_arch.data_precision,
                        'VARCHAR2', Cursor_arch.data_length)
             || DECODE (Cursor_arch.data_type,
                        'NUMBER', ',' || Cursor_arch.data_scale)
             || DECODE (Cursor_arch.data_type,
                        'NUMBER', ')',
                        'VARCHAR2', ')')
        INTO msg2
        FROM DUAL;

      IF LENGTH (v_query) > 0
      THEN
         v_query := v_query || msg2;
      ELSE
         v_query := msg1 || msg2;
      END IF;

      Ptablename := Cursor_arch.arch_table_name;
   END LOOP;

   IF (x = 1)
   THEN
      msg4 := ');';
      v_query := v_query || msg4;
      DBMS_OUTPUT.PUT_LINE (v_query);
       x := 0;
   END IF;
END;
/


drop table ARCH_AUTO_INCR;


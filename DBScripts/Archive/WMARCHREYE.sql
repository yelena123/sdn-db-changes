-- DBTicket WM-106357
-- Fix for SF 4074730

@DBScripts/Archive/PurgeAndArchive/WM01_DDL_ForArchDB.sql;
@DBScripts/Archive/PurgeAndArchive/WM27_DDL_ForArchDB.sql;

commit;

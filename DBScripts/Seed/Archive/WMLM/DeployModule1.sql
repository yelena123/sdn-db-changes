-- &1 WM schema name
-- &2 WM schema password
-- &3 WM Schema Oracle SID

--SPOOL DeployToArchiveSchema.log

SET ECHO ON;

@@WMLMArchTablesIndexes.sql

--SPOOL OFF;
exit;

set scan off echo on;

CREATE OR REPLACE FORCE VIEW CLAIMS_VIEW
(
   CLAIM_ID,
   TC_COMPANY_ID,
   TC_CLAIM_ID,
   CARRIER_CLAIM_ID,
   CLAIM_CLASSIFICATION,
   CLAIM_CATEGORY,
   CLAIM_TYPE,
   CLAIM_STATUS,
   CLAIM_DATE,
   TC_SHIPMENT_ID,
   FREIGHT_INVOICE_ID,
   MERCHANDISE_INVOICE_ID,
   CARRIER_ID,
   MOT_ID,
   O_NAME,
   O_FACILITY_ALIAS_ID,
   O_FACILITY_ID,
   O_ADDRESS,
   O_CITY,
   O_STATE_PROV,
   O_POSTAL_CODE,
   O_COUNTY,
   O_COUNTRY,
   D_NAME,
   D_FACILITY_ALIAS_ID,
   D_FACILITY_ID,
   D_ADDRESS,
   D_CITY,
   D_STATE_PROV,
   D_POSTAL_CODE,
   D_COUNTY,
   D_COUNTRY,
   CURRENCY_CODE,
   SHIP_DATE,
   RECD_DATE,
   MERCHANDISE_AMOUNT,
   PROP_FREIGHT_AMOUNT,
   SALVAGE_AMOUNT,
   TOTAL_CLAIM_AMOUNT,
   TOTAL_PAID_AMOUNT,
   SETTLEMENT_AMOUNT,
   ORIGINAL_CLAIM_AMOUNT,
   SALVAGE_PERCENTAGE,
   SHIPMENT_QTY,
   SHIPMENT_WEIGHT,
   SEAL_1,
   SEAL_2,
   TRACKING_NO,
   WEIGHT_UOM,
   QTY_UOM,
   ROLE,
   ACKNOWLEDGE_DATE,
   SUPPLIER_NAME,
   CLAIMS_FACILITY_ID,
   CLAIMS_DIVISION_ID,
   EQUIPMENT_ID,
   BILLING_ACCOUNT_NO,
   SERVICE_LEVEL_ID,
   CREATED_DTTM,
   CREATED_SOURCE,
   LAST_UPDATED_DTTM,
   LAST_UPDATED_SOURCE,
   RECOUP_AMOUNT,
   TEMPERATURE_RECORDED,
   PICKUP_NO,
   NEW_PO_NO,
   HANDLING_FEE,
   PURCHASE_ORDER,
   IS_APPROVED_BY_MANAGER,
   CLAIM_FILED_DATE,
   CLAIM_DUE_DATE,
   TP_COMPANY_ID,
   AP_STATUS_FLAG,
   CLAIM_ACTION_CODE,
   CLAIM_ACTION_TYPE,
   HAS_ALERTS,
   HAS_WARNINGS,
   HAS_ERRORS,
   BALANCE_DUE,
   DAYS_ASSIGNED,
   STATUS_CHANGE_DATE,
   TRANS_CLAIM_AMOUNT,
   CREATED_SOURCE_TYPE,
   LAST_UPDATED_SOURCE_TYPE,
   REFUND_AMOUNT,
   BILLING_METHOD,
   INV_ID,
   SUPPLIER_NUMBER,
   BILL_OF_LADING_NUMBER,
   PARENT_CLAIM_ID,
   PARENT_TC_CLAIM_ID,
   SUPPLIER_BOL_NUMBER
)
AS
   SELECT claim_id,
          tc_company_id,
          tc_claim_id,
          carrier_claim_id,
          claim_classification,
          claim_category,
          claim_type,
          claim_status,
          claim_date,
          tc_shipment_id,
          freight_invoice_id,
          merchandise_invoice_id,
          carrier_id,
          mot_id,
          o_name,
          o_facility_alias_id,
          o_facility_id,
          o_address,
          o_city,
          o_state_prov,
          o_postal_code,
          o_county,
          o_country,
          d_name,
          d_facility_alias_id,
          d_facility_id,
          d_address,
          d_city,
          d_state_prov,
          d_postal_code,
          d_county,
          d_country,
          currency_code,
          ship_date,
          recd_date,
          merchandise_amount,
          prop_freight_amount,
          salvage_amount,
          CASE
             WHEN claim_classification = 2
             THEN
                TRUNC (
                   (COALESCE (trans_claim_amount, 0)
                    + COALESCE (handling_fee, 0)),
                   2)
             ELSE
                TRUNC (
                     COALESCE (merchandise_amount, 0)
                   + COALESCE (prop_freight_amount, 0)
                   + COALESCE (recoup_amount, 0)
                   - COALESCE (
                        salvage_amount,
                        (COALESCE (merchandise_amount, 0)
                         * (COALESCE (salvage_percentage, 0) / 100)))
                   + .005,
                   2)
          END
             total_claim_amount,
          total_paid_amount,
          settlement_amount,
          original_claim_amount,
          salvage_percentage,
          shipment_qty,
          shipment_weight,
          seal_1,
          seal_2,
          tracking_no,
          weight_uom,
          qty_uom,
          ROLE,
          acknowledge_date,
          supplier_name,
          claims_facility_id,
          claims_division_id,
          equipment_id,
          billing_account_no,
          service_level_id,
          created_dttm,
          created_source,
          last_updated_dttm,
          last_updated_source,
          recoup_amount,
          temperature_recorded,
          pickup_no,
          new_po_no,
          handling_fee,
          purchase_order,
          is_approved_by_manager,
          claim_filed_date,
          claim_due_date,
          tp_company_id,
          ap_status_flag,
          claim_action_code,
          claim_action_type,
          has_alerts,
          has_warnings,
          has_errors,
          CASE
             WHEN claim_classification = 2
             THEN
                CASE
                   WHEN claim_status = 90
                   THEN
                      0
                   ELSE
                      CASE
                         WHEN (  COALESCE (trans_claim_amount, 0)
                               + COALESCE (handling_fee, 0)
                               - COALESCE (total_paid_amount, 0)) < 0
                         THEN
                            0
                         ELSE
                            TRUNC (
                                 COALESCE (trans_claim_amount, 0)
                               + COALESCE (handling_fee, 0)
                               - COALESCE (total_paid_amount, 0),
                               2)
                      END
                END
             ELSE
                CASE
                   WHEN claim_status = 90
                   THEN
                      0
                   ELSE
                      CASE
                         WHEN (  COALESCE (merchandise_amount, 0)
                               + COALESCE (prop_freight_amount, 0)
                               + COALESCE (recoup_amount, 0)
                               - TRUNC (
                                    COALESCE (
                                       salvage_amount,
                                       TRUNC (
                                          COALESCE (merchandise_amount, 0)
                                          * (COALESCE (salvage_percentage, 0)
                                             / 100)),
                                       2),
                                    2)
                               - COALESCE (total_paid_amount, 0)
                               - COALESCE (settlement_amount, 0)) < 0
                         THEN
                            0
                         ELSE
                            (TRUNC (
                                  COALESCE (merchandise_amount, 0)
                                + COALESCE (prop_freight_amount, 0)
                                + COALESCE (recoup_amount, 0)
                                - COALESCE (
                                     salvage_amount,
                                     (COALESCE (merchandise_amount, 0)
                                      * (COALESCE (salvage_percentage, 0)
                                         / 100)))
                                + .005,
                                2)
                             - COALESCE (total_paid_amount, 0)
                             - COALESCE (settlement_amount, 0))
                      END
                END
          END
             balance_due,
          CASE
             WHEN (   claim_status = 70
                   OR claim_status = 80
                   OR claim_status = 90)
             THEN
                (TRUNC (last_updated_dttm) - TRUNC (claim_filed_date))
             ELSE
                (TRUNC (SYSDATE) - TRUNC (claim_filed_date))
          END
             days_assigned,
          status_change_date,
          trans_claim_amount,
          created_source_type,
          last_updated_source_type,
          CASE
             WHEN claim_classification = 2
             THEN
                CASE
                   WHEN (  COALESCE (trans_claim_amount, 0)
                         + COALESCE (handling_fee, 0)
                         - COALESCE (total_paid_amount, 0)) < 0
                   THEN
                      ABS (
                           COALESCE (trans_claim_amount, 0)
                         + COALESCE (handling_fee, 0)
                         - COALESCE (total_paid_amount, 0))
                   ELSE
                      NULL
                END
             ELSE
                CASE
                   WHEN (  COALESCE (merchandise_amount, 0)
                         + COALESCE (prop_freight_amount, 0)
                         + COALESCE (recoup_amount, 0)
                         - TRUNC (
                              COALESCE (
                                 salvage_amount,
                                 TRUNC (
                                    COALESCE (merchandise_amount, 0)
                                    * (COALESCE (salvage_percentage, 0) / 100)),
                                 2),
                              2)
                         - COALESCE (total_paid_amount, 0)
                         - COALESCE (settlement_amount, 0)) < 0
                   THEN
                      TRUNC (
                         ABS (
                              COALESCE (merchandise_amount, 0)
                            + COALESCE (prop_freight_amount, 0)
                            + COALESCE (recoup_amount, 0)
                            - TRUNC (
                                 COALESCE (
                                    salvage_amount,
                                    TRUNC (
                                       COALESCE (merchandise_amount, 0)
                                       * (COALESCE (salvage_percentage, 0)
                                          / 100)),
                                    2),
                                 2)
                            - COALESCE (total_paid_amount, 0)
                            - COALESCE (settlement_amount, 0)),
                         2)
                   ELSE
                      NULL
                END
          END
             refund_amount  -- formual same as balance_due, but with ABS func.
                          ,
          billing_method,
          inv_id,
          supplier_number,
          bill_of_lading_number,
          parent_claim_id,
          parent_tc_claim_id,
          supplier_bol_number
     FROM claims;


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW ILM_LOCATION_VIEW
(
   TYPE,
   LOCATION_ID,
   SEQUENCE_NO,
   CURRENT_QUANTITY,
   DEFINED_SVG,
   ZONE_ID,
   ZONE_NAME,
   STATUS,
   TC_COMPANY_ID,
   PARENT_LOC
)
AS
   SELECT 'YZN',
          LN.location_id,
          LN.sequence_no,
          current_quantity,
          LN.is_location_defined_in_svg,
          NULL,
          yard_zone_name,
          NULL,
          LN.tc_company_id,
          NULL
     FROM yard_zone yz, location LN
    WHERE     LN.location_objid_pk1 = TO_CHAR (yz.yard_id)
          AND LN.location_objid_pk2 = TO_CHAR (yz.yard_zone_id)
          AND LN.ilm_object_type = 28
   UNION ALL
   SELECT 'DDR',
          LN.location_id,
          LN.sequence_no,
          current_quantity,
          LN.is_location_defined_in_svg,
          NULL,
          dock_door_name,
          dock_door_status,
          LN.tc_company_id,
          dock_id
     FROM dock_door dd, location LN
    WHERE     LN.location_objid_pk1 = TO_CHAR (dd.facility_id)
          AND LN.location_objid_pk2 = dd.dock_id
          AND LN.location_objid_pk3 = TO_CHAR (dd.dock_door_id)
          AND LN.tc_company_id = dd.tc_company_id
          AND LN.ilm_object_type = 8
   UNION ALL
   SELECT 'YZS',
          LN.location_id,
          LN.sequence_no,
          current_quantity,
          LN.is_location_defined_in_svg,
          yard_zone_slot_id,
          yard_zone_slot_name,
          yard_zone_slot_status,
          LN.tc_company_id,
          y.yard_zone_name
     FROM yard_zone_slot yz, location LN, yard_zone y
    WHERE     LN.location_objid_pk1 = TO_CHAR (yz.yard_id)
          AND LN.location_objid_pk2 = TO_CHAR (yz.yard_zone_id)
          AND LN.location_objid_pk3 = TO_CHAR (yz.yard_zone_slot_id)
          AND LN.ilm_object_type = 32
          AND y.yard_zone_id = yz.yard_zone_id
   UNION ALL
   SELECT 'DCK',
          LN.location_id,
          LN.sequence_no,
          current_quantity,
          LN.is_location_defined_in_svg,
          NULL,
          dock_id,
          NULL,
          LN.tc_company_id,
          NULL
     FROM dock dck, location LN
    WHERE     LN.location_objid_pk1 = TO_CHAR (dck.facility_id)
          AND LN.location_objid_pk2 = dck.dock_id
          AND LN.ilm_object_type = 4;


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW ORDERS_ORDER_MOVEMENT_VIEW
(
   ORDER_ID,
   SHIPMENT_ID,
   ORDER_SPLIT_ID
)
AS
   SELECT DISTINCT
          ORDERS.ORDER_ID,
          STOP_ACTION_ORDER.SHIPMENT_ID,
          STOP_ACTION_ORDER.ORDER_SPLIT_ID
     FROM    ORDERS
          LEFT OUTER JOIN
             STOP_ACTION_ORDER
          ON STOP_ACTION_ORDER.ORDER_ID = ORDERS.ORDER_ID;


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW ORDER_LINE_ITEM_SIZE_VIEW
(
   ORDER_ID,
   LINE_ITEM_ID,
   RECEIVED_SIZE_VALUE,
   SIZE_VALUE,
   SIZE_UOM_ID,
   TC_COMPANY_ID,
   MASTER_ORDER_ID,
   MO_LINE_ITEM_ID
)
AS
   SELECT order_id,
          line_item_id,
          RECEIVED_SIZE_VALUE,
          size_value,
          size_uom_id,
          tc_company_id,
          MASTER_ORDER_ID,
          MO_LINE_ITEM_ID
     FROM ORDER_LINE_ITEM UNPIVOT ((received_size_value,
                                    size_value,
                                    size_uom_id)
                          FOR x
                          IN  ((RECEIVED_WEIGHT,
                                PLANNED_WEIGHT,
                                WEIGHT_UOM_ID_BASE),
                              (RECEIVED_VOLUME,
                               PLANNED_VOLUME,
                               VOLUME_UOM_ID_BASE),
                              (RECEIVED_QTY, ORDER_QTY, QTY_UOM_ID_BASE),
                              (RECEIVED_SIZE1, SIZE1_VALUE, SIZE1_UOM_ID),
                              (RECEIVED_SIZE2, SIZE2_VALUE, SIZE2_UOM_ID)))
    WHERE is_cancelled = '0'
   UNION
   SELECT OLIS.ORDER_ID,
          OLIS.LINE_ITEM_ID,
          RECEIVED_SIZE_VALUE,
          SIZE_VALUE,
          SIZE_UOM_ID,
          OLIS.TC_COMPANY_ID,
          MASTER_ORDER_ID,
          MO_LINE_ITEM_ID
     FROM ORDER_LINE_ITEM_SIZE OLIS, ORDER_LINE_ITEM OLI
    WHERE     OLIS.ORDER_ID = OLI.ORDER_ID
          AND OLIS.TC_COMPANY_ID = OLI.TC_COMPANY_ID
          AND OLIS.LINE_ITEM_ID = OLI.LINE_ITEM_ID
          AND OLI.IS_CANCELLED = '0';


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW ORDER_LI_SIZES_VIEW
(
   ORDER_ID,
   LINE_ITEM_ID,
   RECEIVED_SIZE_VALUE,
   SIZE_VALUE,
   SIZE_UOM_ID,
   TC_COMPANY_ID,
   MASTER_ORDER_ID,
   MO_LINE_ITEM_ID
)
AS
   SELECT DISTINCT order_id,
                   line_item_id,
                   RECEIVED_SIZE_VALUE,
                   size_value,
                   size_uom_id,
                   tc_company_id,
                   MASTER_ORDER_ID,
                   MO_LINE_ITEM_ID
     FROM ORDER_LINE_ITEM UNPIVOT ((received_size_value,
                                    size_value,
                                    size_uom_id)
                          FOR x
                          IN  ((RECEIVED_WEIGHT,
                                PLANNED_WEIGHT,
                                WEIGHT_UOM_ID_BASE),
                              (RECEIVED_VOLUME,
                               PLANNED_VOLUME,
                               VOLUME_UOM_ID_BASE),
                              (RECEIVED_QTY, ORDER_QTY, QTY_UOM_ID_BASE),
                              (RECEIVED_SIZE1, SIZE1_VALUE, SIZE1_UOM_ID),
                              (RECEIVED_SIZE2, SIZE2_VALUE, SIZE2_UOM_ID)))
    WHERE is_cancelled = '0';


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW RATE_LANE_DTL_RATE_VIEW_1DTLRW
(
   TC_COMPANY_ID,
   LANE_ID,
   RATING_LANE_DTL_SEQ,
   RLD_RATE_SEQ,
   MINIMUM_SIZE,
   MAXIMUM_SIZE,
   SIZE_UOM_ID,
   RATE_CALC_METHOD,
   RATE_UOM,
   RATE,
   MINIMUM_RATE,
   CURRENCY_CODE,
   TARIFF_ID,
   MIN_DISTANCE,
   MAX_DISTANCE,
   DISTANCE_UOM,
   SUPPORTS_MSTL,
   HAS_BH,
   HAS_RT,
   MIN_COMMODITY,
   MAX_COMMODITY,
   PARCEL_TIER,
   EXCESS_WT_RATE,
   COMMODITY_CODE_ID,
   PAYEE_CARRIER_ID,
   MAX_RANGE_COMMODITY_CODE_ID
)
AS
   SELECT TC_COMPANY_ID,
          LANE_ID,
          RATING_LANE_DTL_SEQ,
          RLD_RATE_SEQ,
          MINIMUM_SIZE,
          MAXIMUM_SIZE,
          SIZE_UOM_ID,
          RATE_CALC_METHOD,
          RATE_UOM,
          RATE,
          MINIMUM_RATE,
          CURRENCY_CODE,
          TARIFF_ID,
          MIN_DISTANCE,
          MAX_DISTANCE,
          DISTANCE_UOM,
          SUPPORTS_MSTL,
          HAS_BH,
          HAS_RT,
          MIN_COMMODITY,
          MAX_COMMODITY,
          PARCEL_TIER,
          EXCESS_WT_RATE,
          COMMODITY_CODE_ID,
          PAYEE_CARRIER_ID,
          MAX_RANGE_COMMODITY_CODE_ID
     FROM RATING_LANE_DTL_RATE rldr
    WHERE rldr.RLD_RATE_SEQ =
             (SELECT MIN (rldr_1.RLD_RATE_SEQ)
                FROM RATING_LANE_DTL_RATE rldr_1
               WHERE rldr.TC_COMPANY_ID = rldr_1.TC_COMPANY_ID
                     AND rldr.LANE_ID = rldr_1.LANE_ID
                     AND rldr.RATING_LANE_DTL_SEQ =
                            rldr_1.RATING_LANE_DTL_SEQ);


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW RATING_LANE
(
   TC_COMPANY_ID,
   LANE_ID,
   LANE_HIERARCHY,
   LANE_STATUS,
   O_LOC_TYPE,
   O_FACILITY_ID,
   O_FACILITY_ALIAS_ID,
   O_CITY,
   O_STATE_PROV,
   O_COUNTY,
   O_POSTAL_CODE,
   O_COUNTRY_CODE,
   O_ZONE_ID,
   O_ZONE_NAME,
   D_LOC_TYPE,
   D_FACILITY_ID,
   D_FACILITY_ALIAS_ID,
   D_CITY,
   D_STATE_PROV,
   D_COUNTY,
   D_POSTAL_CODE,
   D_COUNTRY_CODE,
   D_ZONE_ID,
   D_ZONE_NAME,
   CREATED_SOURCE_TYPE,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   O_SEARCH_LOCATION,
   D_SEARCH_LOCATION,
   LANE_UNIQUE_ID,
   LANE_NAME,
   BILLING_METHOD,
   INCOTERM_ID,
   CUSTOMER_ID
)
AS
   SELECT tc_company_id,
          lane_id,
          lane_hierarchy,
          lane_status,
          o_loc_type,
          o_facility_id,
          o_facility_alias_id,
          o_city,
          o_state_prov,
          o_county,
          o_postal_code,
          o_country_code,
          o_zone_id,
          '',
          d_loc_type,
          d_facility_id,
          d_facility_alias_id,
          d_city,
          d_state_prov,
          d_county,
          d_postal_code,
          d_country_code,
          d_zone_id,
          '',
          created_source_type,
          created_source,
          created_dttm,
          last_updated_source_type,
          last_updated_source,
          last_updated_dttm,
          o_search_location,
          d_search_location,
          lane_unique_id,
          lane_name,
          billing_method,
          incoterm_id,
          customer_id
     FROM comb_lane
    WHERE is_rating = 1;


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW RATING_LANE_DTL
(
   TC_COMPANY_ID,
   LANE_ID,
   RATING_LANE_DTL_SEQ,
   CARRIER_ID,
   MOT_ID,
   EQUIPMENT_ID,
   SERVICE_LEVEL_ID,
   PROTECTION_LEVEL_ID,
   LAST_UPDATED_SOURCE_TYPE,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   RATING_LANE_DTL_STATUS,
   EFFECTIVE_DT,
   EXPIRATION_DT,
   IS_BUDGETED,
   SCNDR_CARRIER_ID,
   O_SHIP_VIA,
   D_SHIP_VIA,
   CONTRACT_NUMBER,
   CUSTOM_TEXT1,
   CUSTOM_TEXT2,
   CUSTOM_TEXT3,
   CUSTOM_TEXT4,
   CUSTOM_TEXT5,
   PACKAGE_ID,
   PACKAGE_NAME,
   SAILING_SCHEDULE_NAME,
   VOYAGE
)
AS
   SELECT TC_COMPANY_ID,
          LANE_ID,
          LANE_DTL_SEQ RATING_LANE_DTL_SEQ,
          CARRIER_ID,
          MOT_ID,
          EQUIPMENT_ID,
          SERVICE_LEVEL_ID,
          PROTECTION_LEVEL_ID,
          LAST_UPDATED_SOURCE_TYPE,
          LAST_UPDATED_SOURCE,
          LAST_UPDATED_DTTM,
          LANE_DTL_STATUS RATING_LANE_DTL_STATUS,
          EFFECTIVE_DT,
          EXPIRATION_DT,
          IS_BUDGETED,
          SCNDR_CARRIER_ID,
          O_SHIP_VIA,
          D_SHIP_VIA,
          CONTRACT_NUMBER,
          CUSTOM_TEXT1,
          CUSTOM_TEXT2,
          CUSTOM_TEXT3,
          CUSTOM_TEXT4,
          CUSTOM_TEXT5,
          PACKAGE_ID,
          PACKAGE_NAME,
          SAILING_SCHEDULE_NAME,
          VOYAGE
     FROM COMB_LANE_DTL
    WHERE IS_RATING = 1;


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW RG_LANE
(
   TC_COMPANY_ID,
   LANE_ID,
   LANE_HIERARCHY,
   LANE_STATUS,
   O_LOC_TYPE,
   O_FACILITY_ID,
   O_FACILITY_ALIAS_ID,
   O_CITY,
   O_STATE_PROV,
   O_COUNTY,
   O_POSTAL_CODE,
   O_COUNTRY_CODE,
   O_ZONE_ID,
   O_ZONE_NAME,
   D_LOC_TYPE,
   D_FACILITY_ID,
   D_FACILITY_ALIAS_ID,
   D_CITY,
   D_STATE_PROV,
   D_COUNTY,
   D_POSTAL_CODE,
   D_COUNTRY_CODE,
   D_ZONE_ID,
   D_ZONE_NAME,
   RG_QUALIFIER,
   FREQUENCY,
   CREATED_SOURCE_TYPE,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   O_SEARCH_LOCATION,
   D_SEARCH_LOCATION,
   LANE_NAME,
   CUSTOMER_ID,
   BILLING_METHOD,
   INCOTERM_ID,
   ROUTE_TO,
   ROUTE_TYPE_1,
   ROUTE_TYPE_2,
   NO_RATING,
   USE_FASTEST,
   USE_PREFERENCE_BONUS
)
AS
   SELECT TC_COMPANY_ID,
          LANE_ID,
          LANE_HIERARCHY,
          LANE_STATUS,
          O_LOC_TYPE,
          O_FACILITY_ID,
          O_FACILITY_ALIAS_ID,
          O_CITY,
          O_STATE_PROV,
          O_COUNTY,
          O_POSTAL_CODE,
          O_COUNTRY_CODE,
          O_ZONE_ID,
          '',
          D_LOC_TYPE,
          D_FACILITY_ID,
          D_FACILITY_ALIAS_ID,
          D_CITY,
          D_STATE_PROV,
          D_COUNTY,
          D_POSTAL_CODE,
          D_COUNTRY_CODE,
          D_ZONE_ID,
          '',
          RG_QUALIFIER,
          FREQUENCY,
          CREATED_SOURCE_TYPE,
          CREATED_SOURCE,
          CREATED_DTTM,
          LAST_UPDATED_SOURCE_TYPE,
          LAST_UPDATED_SOURCE,
          LAST_UPDATED_DTTM,
          O_SEARCH_LOCATION,
          D_SEARCH_LOCATION,
          LANE_NAME,
          CUSTOMER_ID,
          BILLING_METHOD,
          INCOTERM_ID,
          ROUTE_TO,
          ROUTE_TYPE_1,
          ROUTE_TYPE_2,
          NO_RATING,
          USE_FASTEST,
          USE_PREFERENCE_BONUS
     FROM COMB_LANE
    WHERE IS_ROUTING = 1;


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW RG_LANE_DTL
(
   TC_COMPANY_ID,
   LANE_ID,
   RG_LANE_DTL_SEQ,
   CARRIER_ID,
   MOT_ID,
   EQUIPMENT_ID,
   SERVICE_LEVEL_ID,
   PROTECTION_LEVEL_ID,
   TIER_ID,
   RANK,
   REP_TP_FLAG,
   WEEKLY_CAPACITY,
   DAILY_CAPACITY,
   CAPACITY_SUN,
   CAPACITY_MON,
   CAPACITY_TUE,
   CAPACITY_WED,
   CAPACITY_THU,
   CAPACITY_FRI,
   CAPACITY_SAT,
   WEEKLY_COMMITMENT,
   DAILY_COMMITMENT,
   COMMITMENT_SUN,
   COMMITMENT_MON,
   COMMITMENT_TUE,
   COMMITMENT_WED,
   COMMITMENT_THU,
   COMMITMENT_FRI,
   COMMITMENT_SAT,
   WEEKLY_COMMIT_PCT,
   DAILY_COMMIT_PCT,
   COMMIT_PCT_SUN,
   COMMIT_PCT_MON,
   COMMIT_PCT_TUE,
   COMMIT_PCT_WED,
   COMMIT_PCT_THU,
   COMMIT_PCT_FRI,
   COMMIT_PCT_SAT,
   MONTHLY_CAPACITY,
   YEARLY_CAPACITY,
   MONTHLY_COMMITMENT,
   YEARLY_COMMITMENT,
   MONTHLY_COMMIT_PCT,
   YEARLY_COMMIT_PCT,
   LAST_UPDATED_SOURCE_TYPE,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   IS_PREFERRED,
   LANE_DTL_STATUS,
   EFFECTIVE_DT,
   EXPIRATION_DT,
   SCNDR_CARRIER_ID,
   TT_TOLERANCE_FACTOR,
   PACKAGE_ID,
   PACKAGE_NAME,
   SIZE_UOM_ID,
   SP_LPN_TYPE,
   SP_MIN_LPN_COUNT,
   SP_MAX_LPN_COUNT,
   SP_MIN_WEIGHT,
   SP_MAX_WEIGHT,
   SP_MIN_VOLUME,
   SP_MAX_VOLUME,
   SP_MIN_LINEAR_FEET,
   SP_MAX_LINEAR_FEET,
   SP_MIN_MONETARY_VALUE,
   SP_MAX_MONETARY_VALUE,
   SP_CURRENCY_CODE,
   CUBING_INDICATOR,
   NO_RATING,
   IS_FASTEST,
   IS_USE_PREFERENCE_BONUS,
   PREFERENCE_BONUS_VALUE,
   OVERRIDE_CODE,
   RFP_PACKAGE_ID,
   RFP_PACKAGE_REFERENCE,
   HAS_SHIPPING_PARAM,
   RG_SHIPPING_PARAM_ID,
   VOYAGE,
   REJECT_FURTHER_SHIPMENTS,
   CARRIER_REJECT_PERIOD
)
AS
   SELECT TC_COMPANY_ID,
          LANE_ID,
          LANE_DTL_SEQ RG_LANE_DTL_SEQ,
          CARRIER_ID,
          MOT_ID,
          EQUIPMENT_ID,
          SERVICE_LEVEL_ID,
          PROTECTION_LEVEL_ID,
          TIER_ID,
          RANK,
          REP_TP_FLAG,
          WEEKLY_CAPACITY,
          DAILY_CAPACITY,
          CAPACITY_SUN,
          CAPACITY_MON,
          CAPACITY_TUE,
          CAPACITY_WED,
          CAPACITY_THU,
          CAPACITY_FRI,
          CAPACITY_SAT,
          WEEKLY_COMMITMENT,
          DAILY_COMMITMENT,
          COMMITMENT_SUN,
          COMMITMENT_MON,
          COMMITMENT_TUE,
          COMMITMENT_WED,
          COMMITMENT_THU,
          COMMITMENT_FRI,
          COMMITMENT_SAT,
          WEEKLY_COMMIT_PCT,
          DAILY_COMMIT_PCT,
          COMMIT_PCT_SUN,
          COMMIT_PCT_MON,
          COMMIT_PCT_TUE,
          COMMIT_PCT_WED,
          COMMIT_PCT_THU,
          COMMIT_PCT_FRI,
          COMMIT_PCT_SAT,
          MONTLY_CAPACITY MONTHLY_CAPACITY,
          YEARLY_CAPACITY,
          MONTLY_COMMITMENT MONTHLY_COMMITMENT,
          YEARLY_COMMITMENT,
          MONTLY_COMMIT_PCT MONTHLY_COMMIT_PCT,
          YEARLY_COMMIT_PCT,
          LAST_UPDATED_SOURCE_TYPE,
          LAST_UPDATED_SOURCE,
          LAST_UPDATED_DTTM,
          IS_PREFERRED,
          LANE_DTL_STATUS,
          EFFECTIVE_DT,
          EXPIRATION_DT,
          SCNDR_CARRIER_ID,
          TT_TOLERANCE_FACTOR,
          PACKAGE_ID,
          PACKAGE_NAME,
          SIZE_UOM_ID,
          SP_LPN_TYPE,
          SP_MIN_LPN_COUNT,
          SP_MAX_LPN_COUNT,
          SP_MIN_WEIGHT,
          SP_MAX_WEIGHT,
          SP_MIN_VOLUME,
          SP_MAX_VOLUME,
          SP_MIN_LINEAR_FEET,
          SP_MAX_LINEAR_FEET,
          SP_MIN_MONETARY_VALUE,
          SP_MAX_MONETARY_VALUE,
          SP_CURRENCY_CODE,
          CUBING_INDICATOR,
          NO_RATING,
          IS_FASTEST,
          IS_USE_PREFERENCE_BONUS,
          PREFERENCE_BONUS_VALUE,
          OVERRIDE_CODE,
          RFP_PACKAGE_ID,
          rfP_PACKAGE_REFERENCE,
          HAS_SHIPPING_PARAM,
          RG_SHIPPING_PARAM_ID,
          VOYAGE,
          REJECT_FURTHER_SHIPMENTS,
          CARRIER_REJECT_PERIOD
     FROM COMB_LANE_DTL
    WHERE IS_ROUTING = 1;


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW RG_LANE_DTL_VIEW
(
   TC_COMPANY_ID,
   LANE_ID,
   RG_LANE_DTL_SEQ,
   CARRIER_CODE,
   CARRIER_ID,
   CARRIER_CODE_STATUS,
   MOT,
   MOT_ID,
   EQUIPMENT_CODE,
   EQUIPMENT_ID,
   SERVICE_LEVEL,
   SERVICE_LEVEL_ID,
   PROTECTION_LEVEL,
   PROTECTION_LEVEL_ID,
   TIER_ID,
   RANK,
   REP_TP_FLAG,
   IS_PREFERRED,
   TP_COMPANY_ID,
   WEEKLY_CAPACITY,
   DAILY_CAPACITY,
   CAPACITY_SUN,
   CAPACITY_MON,
   CAPACITY_TUE,
   CAPACITY_WED,
   CAPACITY_THU,
   CAPACITY_FRI,
   CAPACITY_SAT,
   WEEKLY_COMMITMENT,
   DAILY_COMMITMENT,
   COMMITMENT_SUN,
   COMMITMENT_MON,
   COMMITMENT_TUE,
   COMMITMENT_WED,
   COMMITMENT_THU,
   COMMITMENT_FRI,
   COMMITMENT_SAT,
   WEEKLY_COMMIT_PCT,
   DAILY_COMMIT_PCT,
   COMMIT_PCT_SUN,
   COMMIT_PCT_MON,
   COMMIT_PCT_TUE,
   COMMIT_PCT_WED,
   COMMIT_PCT_THU,
   COMMIT_PCT_FRI,
   COMMIT_PCT_SAT,
   EFFECTIVE_DT,
   EXPIRATION_DT,
   EFF_DT,
   EXP_DT,
   LAST_UPDATED_SOURCE_TYPE,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   HAS_ERRORS,
   SCNDR_CARRIER_CODE,
   SCNDR_CARRIER_ID,
   LANE_DTL_STATUS,
   TT_TOLERANCE_FACTOR,
   MONTHLY_CAPACITY,
   YEARLY_CAPACITY,
   MONTHLY_COMMITMENT,
   YEARLY_COMMITMENT,
   MONTHLY_COMMIT_PCT,
   YEARLY_COMMIT_PCT,
   CUSTOM_TEXT1,
   CUSTOM_TEXT2,
   CUSTOM_TEXT3,
   CUSTOM_TEXT4,
   CUSTOM_TEXT5,
   PACKAGE_ID,
   PACKAGE_NAME,
   SIZE_UOM_ID,
   RG_SHIPPING_PARAM_ID,
   SP_LPN_TYPE,
   SP_MIN_LPN_COUNT,
   SP_MAX_LPN_COUNT,
   SP_MIN_WEIGHT,
   SP_MAX_WEIGHT,
   SP_MIN_VOLUME,
   SP_MAX_VOLUME,
   SP_MIN_LINEAR_FEET,
   SP_MAX_LINEAR_FEET,
   SP_MIN_MONETARY_VALUE,
   SP_MAX_MONETARY_VALUE,
   SP_CURRENCY_CODE,
   CUBING_INDICATOR,
   PREFERENCE_BONUS_VALUE,
   OVERRIDE_CODE,
   RFP_PACKAGE_ID,
   RFP_PACKAGE_REFERENCE,
   HAS_SHIPPING_PARAM,
   VOYAGE,
   REJECT_FURTHER_SHIPMENTS,
   CARRIER_REJECT_PERIOD
)
AS
   SELECT rld.TC_COMPANY_ID TC_COMPANY_ID,
          rld.LANE_ID LANE_ID,
          rld.LANE_DTL_SEQ RG_LANE_DTL_SEQ,
          '' CARRIER_CODE,
          rld.CARRIER_ID CARRIER_ID,
          cc.CARRIER_CODE_STATUS CARRIER_CODE_STATUS,
          '' MOT,
          rld.MOT_ID MOT_ID,
          '' EQUIPMENT_CODE,
          rld.EQUIPMENT_ID EQUIPMENT_ID,
          '' SERVICE_LEVEL,
          rld.SERVICE_LEVEL_ID SERVICE_LEVEL_ID,
          '' PROTECTION_LEVEL,
          rld.PROTECTION_LEVEL_ID PROTECTION_LEVEL_ID,
          NVL (rld.TIER_ID, 'ALL') TIER_ID,
          rld.RANK RANK,
          rld.REP_TP_FLAG REP_TP_FLAG,
          rld.IS_PREFERRED IS_PREFERRED,
          cc.TP_COMPANY_ID TP_COMPANY_ID,
          rld.WEEKLY_CAPACITY WEEKLY_CAPACITY,
          rld.DAILY_CAPACITY DAILY_CAPACITY,
          rld.CAPACITY_SUN CAPACITY_SUN,
          rld.CAPACITY_MON CAPACITY_MON,
          rld.CAPACITY_TUE CAPACITY_TUE,
          rld.CAPACITY_WED CAPACITY_WED,
          rld.CAPACITY_THU CAPACITY_THU,
          rld.CAPACITY_FRI CAPACITY_FRI,
          rld.CAPACITY_SAT CAPACITY_SAT,
          rld.WEEKLY_COMMITMENT WEEKLY_COMMITMENT,
          rld.DAILY_COMMITMENT DAILY_COMMITMENT,
          rld.COMMITMENT_SUN COMMITMENT_SUN,
          rld.COMMITMENT_MON COMMITMENT_MON,
          rld.COMMITMENT_TUE COMMITMENT_TUE,
          rld.COMMITMENT_WED COMMITMENT_WED,
          rld.COMMITMENT_THU COMMITMENT_THU,
          rld.COMMITMENT_FRI COMMITMENT_FRI,
          rld.COMMITMENT_SAT COMMITMENT_SAT,
          rld.WEEKLY_COMMIT_PCT WEEKLY_COMMIT_PCT,
          rld.DAILY_COMMIT_PCT DAILY_COMMIT_PCT,
          rld.COMMIT_PCT_SUN COMMIT_PCT_SUN,
          rld.COMMIT_PCT_MON COMMIT_PCT_MON,
          rld.COMMIT_PCT_TUE COMMIT_PCT_TUE,
          rld.COMMIT_PCT_WED COMMIT_PCT_WED,
          rld.COMMIT_PCT_THU COMMIT_PCT_THU,
          rld.COMMIT_PCT_FRI COMMIT_PCT_FRI,
          rld.COMMIT_PCT_SAT COMMIT_PCT_SAT,
          rld.EFFECTIVE_DT EFFECTIVE_DT,
          rld.EXPIRATION_DT EXPIRATION_DT,
          COALESCE (
             rld.EFFECTIVE_DT,
             TO_DATE ('1950-01-01-00.00.00.000000', 'yyyy-mm-dd hh24:mi:ss'))
             EFF_DT,
          COALESCE (
             rld.EXPIRATION_DT,
             TO_DATE ('2049-12-31-00.00.00.000000', 'yyyy-mm-dd hh24:mi:ss'))
             EXP_DT,
          rld.LAST_UPDATED_SOURCE_TYPE LAST_UPDATED_SOURCE_TYPE,
          rld.LAST_UPDATED_SOURCE LAST_UPDATED_SOURCE,
          rld.LAST_UPDATED_DTTM LAST_UPDATED_DTTM,
          DECODE (rld.LANE_DTL_STATUS, 4, 1, 0) HAS_ERRORS,
          '' SCNDR_CARRIER_CODE,
          rld.SCNDR_CARRIER_ID SCNDR_CARRIER_ID,
          rld.LANE_DTL_STATUS LANE_DTL_STATUS,
          rld.TT_TOLERANCE_FACTOR TT_TOLERANCE_FACTOR,
          rld.MONTLY_CAPACITY MONTHLY_CAPACITY,
          rld.YEARLY_CAPACITY YEARLY_CAPACITY,
          rld.MONTLY_COMMITMENT MONTHLY_COMMITMENT,
          rld.YEARLY_COMMITMENT YEARLY_COMMITMENT,
          rld.MONTLY_COMMIT_PCT MONTHLY_COMMIT_PCT,
          rld.YEARLY_COMMIT_PCT YEARLY_COMMIT_PCT,
          rld.CUSTOM_TEXT1 CUSTOM_TEXT1,
          rld.CUSTOM_TEXT2 CUSTOM_TEXT2,
          rld.CUSTOM_TEXT3 CUSTOM_TEXT3,
          rld.CUSTOM_TEXT4 CUSTOM_TEXT4,
          rld.CUSTOM_TEXT5 CUSTOM_TEXT5,
          rld.PACKAGE_ID PACKAGE_ID,
          rld.PACKAGE_NAME PACKAGE_NAME,
          rld.SIZE_UOM_ID SIZE_UOM_ID,
          rld.RG_SHIPPING_PARAM_ID RG_SHIPPING_PARAM_ID,
          rld.SP_LPN_TYPE SP_LPN_TYPE,      --  Gopalakrishnan 3/25/2008 Start
          rld.SP_MIN_LPN_COUNT SP_MIN_LPN_COUNT,
          rld.SP_MAX_LPN_COUNT SP_MAX_LPN_COUNT,
          rld.SP_MIN_WEIGHT SP_MIN_WEIGHT,
          rld.SP_MAX_WEIGHT SP_MAX_WEIGHT,
          rld.SP_MIN_VOLUME SP_MIN_VOLUME,
          rld.SP_MAX_VOLUME SP_MAX_VOLUME,
          rld.SP_MIN_LINEAR_FEET SP_MIN_LINEAR_FEET,
          rld.SP_MAX_LINEAR_FEET SP_MAX_LINEAR_FEET,
          rld.SP_MIN_MONETARY_VALUE SP_MIN_MONETARY_VALUE,
          rld.SP_MAX_MONETARY_VALUE SP_MAX_MONETARY_VALUE,
          rld.SP_CURRENCY_CODE SP_CURRENCY_CODE, --  Gopalakrishnan 3/25/2008 End
          rld.CUBING_INDICATOR CUBING_INDICATOR,
          rld.PREFERENCE_BONUS_VALUE PREFERENCE_BONUS_VALUE,
          rld.OVERRIDE_CODE OVERRIDE_CODE,
          rld.RFP_PACKAGE_ID RFP_PACKAGE_ID,
          rld.RFP_PACKAGE_REFERENCE RFP_PACKAGE_REFERENCE,
          rld.has_shipping_param HAS_SHIPPING_PARAM,
          rld.voyage VOYAGE,
          rld.REJECT_FURTHER_SHIPMENTS REJECT_FURTHER_SHIPMENTS,
          rld.CARRIER_REJECT_PERIOD CARRIER_REJECT_PERIOD
     FROM comb_lane_dtl rld, carrier_code cc
    WHERE     rld.carrier_id = cc.carrier_id
          AND rld.LANE_DTL_STATUS <> 2
          AND rld.IS_ROUTING = 1
   UNION ALL
   SELECT irld.TC_COMPANY_ID TC_COMPANY_ID,
          irl.RG_LANE_ID LANE_ID,
          irld.RG_LANE_DTL_SEQ RG_LANE_DTL_SEQ,
          irld.CARRIER_CODE CARRIER_CODE,
          CAST (NULL AS INT) CARRIER_ID,
          icc.CARRIER_CODE_STATUS CARRIER_CODE_STATUS,
          NVL (irld.MOT, 'ALL') MOT,
          CAST (NULL AS INT) MOT_ID,
          NVL (irld.EQUIPMENT_CODE, 'ALL') EQUIPMENT_CODE,
          CAST (NULL AS INT) EQUIPMENT_ID,
          NVL (irld.SERVICE_LEVEL, 'ALL') SERVICE_LEVEL,
          CAST (NULL AS INT) SERVICE_LEVEL_ID,
          NVL (irld.PROTECTION_LEVEL, 'ALL') PROTECTION_LEVEL,
          CAST (NULL AS INT) PROTECTION_LEVEL_ID,
          NVL (irld.TIER_ID, 'ALL') TIER_ID,
          irld.RANK RANK,
          irld.REP_TP_FLAG REP_TP_FLAG,
          irld.IS_PREFERRED IS_PREFERRED,
          icc.TP_COMPANY_ID TP_COMPANY_ID,
          irld.WEEKLY_CAPACITY WEEKLY_CAPACITY,
          irld.DAILY_CAPACITY DAILY_CAPACITY,
          irld.CAPACITY_SUN CAPACITY_SUN,
          irld.CAPACITY_MON CAPACITY_MON,
          irld.CAPACITY_TUE CAPACITY_TUE,
          irld.CAPACITY_WED CAPACITY_WED,
          irld.CAPACITY_THU CAPACITY_THU,
          irld.CAPACITY_FRI CAPACITY_FRI,
          irld.CAPACITY_SAT CAPACITY_SAT,
          irld.WEEKLY_COMMITMENT WEEKLY_COMMITMENT,
          irld.DAILY_COMMITMENT DAILY_COMMITMENT,
          irld.COMMITMENT_SUN COMMITMENT_SUN,
          irld.COMMITMENT_MON COMMITMENT_MON,
          irld.COMMITMENT_TUE COMMITMENT_TUE,
          irld.COMMITMENT_WED COMMITMENT_WED,
          irld.COMMITMENT_THU COMMITMENT_THU,
          irld.COMMITMENT_FRI COMMITMENT_FRI,
          irld.COMMITMENT_SAT COMMITMENT_SAT,
          irld.WEEKLY_COMMIT_PCT WEEKLY_COMMIT_PCT,
          irld.DAILY_COMMIT_PCT DAILY_COMMIT_PCT,
          irld.COMMIT_PCT_SUN COMMIT_PCT_SUN,
          irld.COMMIT_PCT_MON COMMIT_PCT_MON,
          irld.COMMIT_PCT_TUE COMMIT_PCT_TUE,
          irld.COMMIT_PCT_WED COMMIT_PCT_WED,
          irld.COMMIT_PCT_THU COMMIT_PCT_THU,
          irld.COMMIT_PCT_FRI COMMIT_PCT_FRI,
          irld.COMMIT_PCT_SAT COMMIT_PCT_SAT,
          irld.EFFECTIVE_DT EFFECTIVE_DT,
          irld.EXPIRATION_DT EXPIRATION_DT,
          COALESCE (
             irld.EXPIRATION_DT,
             TO_DATE ('1950-01-01-00.00.00.000000', 'yyyy-mm-dd hh24:mi:ss'))
             EFF_DT,
          COALESCE (
             irld.EXPIRATION_DT,
             TO_DATE ('2049-12-31-00.00.00.000000', 'yyyy-mm-dd hh24:mi:ss'))
             EXP_DT,
          irld.LAST_UPDATED_SOURCE_TYPE LAST_UPDATED_SOURCE_TYPE,
          irld.LAST_UPDATED_SOURCE LAST_UPDATED_SOURCE,
          irld.LAST_UPDATED_DTTM LAST_UPDATED_DTTM,
          DECODE (irld.LANE_DTL_STATUS, 4, 1, 0) HAS_ERRORS,
          irld.SCNDR_CARRIER_CODE SCNDR_CARRIER_CODE,
          CAST (NULL AS INT) SCNDR_CARRIER_ID,
          irld.LANE_DTL_STATUS LANE_DTL_STATUS,
          irld.TT_TOLERANCE_FACTOR TT_TOLERANCE_FACTOR,
          CAST (NULL AS INT) MONTHLY_CAPACITY,
          CAST (NULL AS INT) YEARLY_CAPACITY,
          CAST (NULL AS DECIMAL) MONTHLY_COMMITMENT,
          CAST (NULL AS DECIMAL) YEARLY_COMMITMENT,
          CAST (NULL AS DECIMAL) MONTHLY_COMMIT_PCT,
          CAST (NULL AS DECIMAL) YEARLY_COMMIT_PCT,
          '' CUSTOM_TEXT1,
          '' CUSTOM_TEXT2,
          '' CUSTOM_TEXT3,
          '' CUSTOM_TEXT4,
          '' CUSTOM_TEXT5,
          irld.PACKAGE_ID PACKAGE_ID,
          irld.PACKAGE_NAME PACKAGE_NAME,
          CAST (NULL AS INT) SIZE_UOM_ID,
          CAST (NULL AS INT) RG_SHIPPING_PARAM_ID,
          irld.SP_LPN_TYPE SP_LPN_TYPE,     --  Gopalakrishnan 3/25/2008 Start
          irld.SP_MIN_LPN_COUNT SP_MIN_LPN_COUNT,
          irld.SP_MAX_LPN_COUNT SP_MAX_LPN_COUNT,
          irld.SP_MIN_WEIGHT SP_MIN_WEIGHT,
          irld.SP_MAX_WEIGHT SP_MAX_WEIGHT,
          irld.SP_MIN_VOLUME SP_MIN_VOLUME,
          irld.SP_MAX_VOLUME SP_MAX_VOLUME,
          irld.SP_MIN_LINEAR_FEET SP_MIN_LINEAR_FEET,
          irld.SP_MAX_LINEAR_FEET SP_MAX_LINEAR_FEET,
          irld.SP_MIN_MONETARY_VALUE SP_MIN_MONETARY_VALUE,
          irld.SP_MAX_MONETARY_VALUE SP_MAX_MONETARY_VALUE,
          irld.SP_CURRENCY_CODE SP_CURRENCY_CODE, --  Gopalakrishnan 3/25/2008 End
          CAST (NULL AS INT) CUBING_INDICATOR,
          CAST (NULL AS INT) OVERRIDE_CODE,
          CAST (NULL AS INT) PREFERENCE_BONUS_VALUE,
          CAST (NULL AS INT) RFP_PACKAGE_ID,
          '' RFP_PACKAGE_REFERENCE,
          0 HAS_SHIPPING_PARAM,
          irld.VOYAGE VOYAGE,
          0 REJECT_FURTHER_SHIPMENTS,
          0 CARRIER_REJECT_PERIOD
     FROM import_rg_lane_dtl irld
          LEFT OUTER JOIN carrier_code icc
             ON (irld.tc_company_id = icc.tc_company_id
                 AND irld.carrier_code = icc.carrier_code)
          JOIN import_rg_lane irl
             ON irld.lane_id = irl.lane_id;


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW RG_VIEW
(
   TC_COMPANY_ID,
   LANE_ID,
   LANE_HIERARCHY,
   LANE_STATUS,
   O_LOC_TYPE,
   O_FACILITY_ID,
   O_FACILITY_ALIAS_ID,
   O_CITY,
   O_STATE_PROV,
   O_COUNTY,
   O_POSTAL_CODE,
   O_COUNTRY_CODE,
   O_ZONE_ID,
   O_ZONE_NAME,
   D_LOC_TYPE,
   D_FACILITY_ID,
   D_FACILITY_ALIAS_ID,
   D_CITY,
   D_STATE_PROV,
   D_COUNTY,
   D_POSTAL_CODE,
   D_COUNTRY_CODE,
   D_ZONE_ID,
   D_ZONE_NAME,
   RG_QUALIFIER,
   FREQUENCY,
   HAS_ERRORS,
   DTL_HAS_ERRORS,
   CREATED_SOURCE_TYPE,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   LANE_NAME,
   CUSTOMER_ID,
   BILLING_METHOD,
   INCOTERM_ID,
   ROUTE_TO,
   ROUTE_TYPE_1,
   ROUTE_TYPE_2,
   NO_RATING,
   USE_FASTEST,
   USE_PREFERENCE_BONUS
)
AS
   SELECT l.tc_company_id,
          l.lane_id,
          l.lane_hierarchy,
          l.lane_status,
          l.o_loc_type,
          l.o_facility_id,
          l.o_facility_alias_id,
          UPPER (l.o_city),
          l.o_state_prov,
          UPPER (l.o_county),
          l.o_postal_code,
          l.o_country_code,
          l.o_zone_id,
          '',
          l.d_loc_type,
          l.d_facility_id,
          l.d_facility_alias_id,
          UPPER (l.d_city),
          l.d_state_prov,
          UPPER (l.d_county),
          l.d_postal_code,
          l.d_country_code,
          l.d_zone_id,
          '',
          COALESCE (l.rg_qualifier, 'ALL'),
          l.frequency,
          DECODE (l.lane_status, 4, 1, 0) has_errors,
          COALESCE (
             (SELECT SIGN (MAX (DECODE (ID.lane_dtl_status, 4, 1, 0)))
                FROM import_rg_lane ir, import_rg_lane_dtl ID
               WHERE (    l.lane_id = ir.rg_lane_id
                      AND l.tc_company_id = ir.tc_company_id
                      AND ir.lane_id = ID.lane_id
                      AND ir.tc_company_id = ID.tc_company_id)),
             0)
             dtl_has_errors,
          l.created_source_type,
          l.created_source,
          l.created_dttm,
          l.last_updated_source_type,
          l.last_updated_source,
          l.last_updated_dttm,
          l.lane_name,
          l.customer_id,
          l.billing_method,
          l.incoterm_id,
          l.route_to,
          l.route_type_1,
          l.route_type_2,
          l.no_rating,
          l.use_fastest,
          l.use_preference_bonus
     FROM rg_lane l
   UNION ALL
   SELECT il.tc_company_id,
          il.rg_lane_id,
          il.lane_hierarchy,
          il.lane_status,
          il.o_loc_type,
          il.o_facility_id,
          il.o_facility_alias_id,
          UPPER (il.o_city),
          il.o_state_prov,
          UPPER (il.o_county),
          il.o_postal_code,
          il.o_country_code,
          il.o_zone_id,
          il.o_zone_name,
          il.d_loc_type,
          il.d_facility_id,
          il.d_facility_alias_id,
          UPPER (il.d_city),
          il.d_state_prov,
          UPPER (il.d_county),
          il.d_postal_code,
          il.d_country_code,
          il.d_zone_id,
          il.d_zone_name,
          COALESCE (il.rg_qualifier, 'ALL'),
          il.frequency,
          DECODE (il.lane_status, 4, 1, 0) has_errors,
          COALESCE (
             (SELECT MAX (DECODE (d.lane_dtl_status, 4, 1, 0))
                FROM import_rg_lane_dtl d
               WHERE d.lane_id = il.lane_id
                     AND d.tc_company_id = il.tc_company_id),
             0)
             dtl_has_errors,
          il.created_source_type,
          il.created_source,
          il.created_dttm,
          il.last_updated_source_type,
          il.last_updated_source,
          il.last_updated_dttm,
          il.lane_name,
          il.customer_id,
          il.billing_method,
          il.incoterm_id,
          il.route_to,
          il.route_type_1,
          il.route_type_2,
          il.no_rating,
          il.use_fastest,
          il.use_preference_bonus
     FROM import_rg_lane il
    WHERE il.lane_status = 4
          AND NOT EXISTS
                     (SELECT 1
                        FROM rg_lane r
                       WHERE il.tc_company_id = r.tc_company_id
                             AND il.rg_lane_id = r.lane_id);


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW RATE_LANE_DTL_VIEW
(
   TC_COMPANY_ID,
   LANE_ID,
   RATING_LANE_DTL_SEQ,
   CARRIER_CODE,
   CARRIER_ID,
   CARRIER_CODE_STATUS,
   IS_BUDGETED,
   MOT,
   MOT_ID,
   EQUIPMENT_CODE,
   EQUIPMENT_ID,
   SERVICE_LEVEL,
   SERVICE_LEVEL_ID,
   PROTECTION_LEVEL,
   PROTECTION_LEVEL_ID,
   EFFECTIVE_DT,
   EXPIRATION_DT,
   EFF_DT,
   EXP_DT,
   HAS_ERRORS,
   LAST_UPDATED_SOURCE_TYPE,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   SCNDR_CARRIER_CODE,
   SCNDR_CARRIER_ID,
   RATING_LANE_DTL_STATUS,
   O_SHIP_VIA,
   D_SHIP_VIA,
   CONTRACT_NUMBER,
   CUSTOM_TEXT1,
   CUSTOM_TEXT2,
   CUSTOM_TEXT3,
   CUSTOM_TEXT4,
   CUSTOM_TEXT5,
   PACKAGE_ID,
   PACKAGE_NAME,
   VOYAGE
)
AS
   SELECT rld.TC_COMPANY_ID,
          rld.LANE_ID,
          rld.RATING_LANE_DTL_SEQ,
          NVL (cc.CARRIER_CODE, 'ALL'),                             --Was NULL
          rld.CARRIER_ID,
          cc.CARRIER_CODE_STATUS,                                   --Was NULL
          rld.IS_BUDGETED,
          NVL (M.MOT, 'ALL'),                                       --Was NULL
          rld.MOT_ID,
          NVL (eq.EQUIPMENT_CODE, 'ALL'),                           --Was NULL
          rld.EQUIPMENT_ID,
          NVL (sl.SERVICE_LEVEL, 'ALL'),                            --Was NULL
          rld.SERVICE_LEVEL_ID,
          NVL (pl.PROTECTION_LEVEL, 'ALL'),                         --Was NULL
          rld.PROTECTION_LEVEL_ID,
          rld.EFFECTIVE_DT,
          rld.EXPIRATION_DT,
          COALESCE (rld.EFFECTIVE_DT, TO_DATE ('01-JAN-1950', 'dd-mon-yyyy')),
          COALESCE (rld.EXPIRATION_DT, TO_DATE ('31-DEC-49')),
          CASE rld.RATING_LANE_DTL_STATUS WHEN 4 THEN 1 ELSE 0 END
             AS HAS_ERRORS,
          rld.LAST_UPDATED_SOURCE_TYPE,
          rld.LAST_UPDATED_SOURCE,
          rld.LAST_UPDATED_DTTM,
          NULL,
          rld.SCNDR_CARRIER_ID,
          rld.RATING_LANE_DTL_STATUS,
          rld.O_SHIP_VIA,
          rld.D_SHIP_VIA,
          rld.CONTRACT_NUMBER,
          rld.CUSTOM_TEXT1,
          rld.CUSTOM_TEXT2,
          rld.CUSTOM_TEXT3,
          rld.CUSTOM_TEXT4,
          rld.CUSTOM_TEXT5,
          rld.package_id,
          rld.package_name,
          rld.VOYAGE
     FROM RATING_LANE_DTL rld
          LEFT OUTER JOIN carrier_code cc
             ON (rld.carrier_id = cc.carrier_id)
          LEFT OUTER JOIN equipment eq
             ON (rld.EQUIPMENT_ID = eq.EQUIPMENT_ID)
          LEFT OUTER JOIN mot M
             ON (rld.mot_id = M.mot_id)
          LEFT OUTER JOIN service_level sl
             ON (rld.SERVICE_LEVEL_ID = sl.SERVICE_LEVEL_ID)
          LEFT OUTER JOIN protection_level pl
             ON (rld.PROTECTION_LEVEL_ID = pl.PROTECTION_LEVEL_ID)
    WHERE cc.CARRIER_ID = rld.CARRIER_ID AND rld.RATING_LANE_DTL_STATUS <> 2
   UNION ALL
   SELECT irld.TC_COMPANY_ID,
          rl.LANE_ID,
          irld.RATING_LANE_DTL_SEQ,
          NVL (irld.CARRIER_CODE, 'ALL'),
          icc.CARRIER_ID,                                           --Was NULL
          icc.CARRIER_CODE_STATUS,
          irld.IS_BUDGETED,
          COALESCE (irld.MOT, 'ALL'),
          im.MOT_ID,
          --Was NULL                                                                                                        -- MOT_ID is not present in IMPORT_RATING_LANE, so we are using NULL
          COALESCE (irld.EQUIPMENT_CODE, 'ALL'),
          ieq.EQUIPMENT_ID,
          --Was NULL                                                                                                       -- EQUIPMENT_ID is not present in IMPORT_RATING_LANE, so we are using NULL
          COALESCE (irld.SERVICE_LEVEL, 'ALL'),
          isl.SERVICE_LEVEL_ID,
          --Was NULL                                                                                                      -- SERVICE_LEVEL_ID is not present in IMPORT_RATING_LANE, so we are using NULL
          COALESCE (irld.PROTECTION_LEVEL, 'ALL'),
          ipl.PROTECTION_LEVEL_ID,
          --Was NULL                                                                                                -- PROTECTION_LEVEL_ID is not present in IMPORT_RATING_LANE, so we are using NULL
          irld.EFFECTIVE_DT,
          irld.EXPIRATION_DT,
          COALESCE (irld.EFFECTIVE_DT,
                    TO_DATE ('01-JAN-1950', 'dd-mon-yyyy')),
          COALESCE (irld.EXPIRATION_DT, TO_DATE ('31-DEC-49')),
          CASE irld.RATING_LANE_DTL_STATUS WHEN 4 THEN 1 ELSE 0 END
             AS HAS_ERRORS,
          irld.LAST_UPDATED_SOURCE_TYPE,
          irld.LAST_UPDATED_SOURCE,
          irld.LAST_UPDATED_DTTM,
          irld.SCNDR_CARRIER_CODE,
          NULL,
          irld.RATING_LANE_DTL_STATUS,
          irld.O_SHIP_VIA,
          irld.D_SHIP_VIA,
          irld.CONTRACT_NUMBER,
          irld.CUSTOM_TEXT1,
          irld.CUSTOM_TEXT2,
          irld.CUSTOM_TEXT3,
          irld.CUSTOM_TEXT4,
          irld.CUSTOM_TEXT5,
          irld.package_id,
          irld.package_name,
          irld.voyage
     FROM IMPORT_RATING_LANE irl
          JOIN IMPORT_RATING_LANE_DTL irld
             ON irl.LANE_ID = irld.LANE_ID
                AND irl.TC_COMPANY_ID = irld.TC_COMPANY_ID
          JOIN RATING_LANE rl
             ON rl.TC_COMPANY_ID = irl.TC_COMPANY_ID
                AND rl.LANE_ID = irl.RATING_LANE_ID
          LEFT OUTER JOIN carrier_code icc
             ON (irld.carrier_id = icc.carrier_id)
          LEFT OUTER JOIN equipment ieq
             ON (irld.EQUIPMENT_ID = ieq.EQUIPMENT_ID)
          LEFT OUTER JOIN mot im
             ON (irld.mot_id = im.mot_id)
          LEFT OUTER JOIN service_level isl
             ON (irld.SERVICE_LEVEL_ID = isl.SERVICE_LEVEL_ID)
          LEFT OUTER JOIN protection_level ipl
             ON (irld.PROTECTION_LEVEL_ID = ipl.PROTECTION_LEVEL_ID)
   UNION ALL
   SELECT irld1.TC_COMPANY_ID,
          irl1.RATING_LANE_ID,
          irld1.RATING_LANE_DTL_SEQ,
          irld1.CARRIER_CODE,
          irld1.CARRIER_ID,                                         --Was NULL
          NULL,                                                     --Was NULL
          irld1.IS_BUDGETED,
          COALESCE (irld1.MOT, 'ALL'),
          im1.MOT_ID,                                               --Was NULL
          COALESCE (irld1.EQUIPMENT_CODE, 'ALL'),
          ieq1.EQUIPMENT_ID,                                        --Was NULL
          COALESCE (irld1.SERVICE_LEVEL, 'ALL'),
          isl1.SERVICE_LEVEL_ID,                                    --Was NULL
          COALESCE (irld1.PROTECTION_LEVEL, 'ALL'),
          ipl1.PROTECTION_LEVEL_ID,                                 --Was NULL
          irld1.EFFECTIVE_DT,
          irld1.EXPIRATION_DT,
          COALESCE (irld1.EFFECTIVE_DT,
                    TO_DATE ('01-JAN-1950', 'dd-mon-yyyy')),
          COALESCE (irld1.EXPIRATION_DT, TO_DATE ('31-DEC-49')),
          CASE irld1.RATING_LANE_DTL_STATUS WHEN 4 THEN 1 ELSE 0 END
             AS HAS_ERRORS,
          irld1.LAST_UPDATED_SOURCE_TYPE,
          irld1.LAST_UPDATED_SOURCE,
          irld1.LAST_UPDATED_DTTM,
          irld1.SCNDR_CARRIER_CODE,
          NULL,
          irld1.RATING_LANE_DTL_STATUS,
          irld1.O_SHIP_VIA,
          irld1.D_SHIP_VIA,
          irld1.CONTRACT_NUMBER,
          irld1.CUSTOM_TEXT1,
          irld1.CUSTOM_TEXT2,
          irld1.CUSTOM_TEXT3,
          irld1.CUSTOM_TEXT4,
          irld1.CUSTOM_TEXT5,
          irld1.package_id,
          irld1.package_name,
          irld1.voyage
     FROM IMPORT_RATING_LANE irl1
          JOIN IMPORT_RATING_LANE_DTL irld1
             ON irl1.LANE_ID = irld1.LANE_ID
                AND irl1.TC_COMPANY_ID = irld1.TC_COMPANY_ID
          LEFT OUTER JOIN carrier_code icc1
             ON (irld1.carrier_id = icc1.carrier_id)
          LEFT OUTER JOIN equipment ieq1
             ON (irld1.EQUIPMENT_ID = ieq1.EQUIPMENT_ID)
          LEFT OUTER JOIN mot im1
             ON (irld1.mot_id = im1.mot_id)
          LEFT OUTER JOIN service_level isl1
             ON (irld1.SERVICE_LEVEL_ID = isl1.SERVICE_LEVEL_ID)
          LEFT OUTER JOIN protection_level ipl1
             ON (irld1.PROTECTION_LEVEL_ID = ipl1.PROTECTION_LEVEL_ID)
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM RATING_LANE R
                   WHERE irl1.TC_COMPANY_ID = R.TC_COMPANY_ID
                         AND irl1.RATING_LANE_ID = R.LANE_ID)
          AND irl1.LANE_STATUS = 4;


/* Formatted on 1/14/2014 11:58:22 AM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW RATE_LANE_VIEW
(
   TC_COMPANY_ID,
   LANE_ID,
   LANE_HIERARCHY,
   LANE_STATUS,
   O_LOC_TYPE,
   O_FACILITY_ID,
   O_FACILITY_ALIAS_ID,
   O_CITY,
   O_STATE_PROV,
   O_COUNTY,
   O_POSTAL_CODE,
   O_COUNTRY_CODE,
   O_ZONE_ID,
   O_ZONE_NAME,
   D_LOC_TYPE,
   D_FACILITY_ID,
   D_FACILITY_ALIAS_ID,
   D_CITY,
   D_STATE_PROV,
   D_COUNTY,
   D_POSTAL_CODE,
   D_COUNTRY_CODE,
   D_ZONE_ID,
   D_ZONE_NAME,
   HAS_ERRORS,
   DTL_HAS_ERRORS,
   CREATED_SOURCE_TYPE,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   LANE_NAME,
   BILLING_METHOD,
   INCOTERM_ID,
   CUSTOMER_ID
)
AS
   SELECT l.tc_company_id,
          l.lane_id,
          l.lane_hierarchy,
          l.lane_status,
          l.o_loc_type,
          l.o_facility_id,
          l.o_facility_alias_id,
          UPPER (l.o_city),
          l.o_state_prov,
          UPPER (l.o_county),
          l.o_postal_code,
          l.o_country_code,
          l.o_zone_id,
          '',
          l.d_loc_type,
          l.d_facility_id,
          l.d_facility_alias_id,
          UPPER (l.d_city),
          l.d_state_prov,
          UPPER (l.d_county),
          l.d_postal_code,
          l.d_country_code,
          l.d_zone_id,
          '',
          DECODE (l.lane_status, 4, 1, 0) has_errors,
          COALESCE (
             (SELECT SIGN (MAX (DECODE (id.rating_lane_dtl_status, 4, 1, 0)))
                FROM import_rating_lane ir, import_rating_lane_dtl id
               WHERE (    l.lane_id = ir.rating_lane_id
                      AND l.tc_company_id = ir.tc_company_id
                      AND ir.lane_id = id.lane_id
                      AND ir.tc_company_id = id.tc_company_id)),
             0)
             dtl_has_errors,
          l.created_source_type,
          l.created_source,
          l.created_dttm,
          l.last_updated_source_type,
          l.last_updated_source,
          l.last_updated_dttm,
          l.lane_name,
          l.billing_method,
          l.incoterm_id,
          l.customer_id
     FROM rating_lane l
   UNION ALL
   SELECT i.tc_company_id,
          i.rating_lane_id,
          i.lane_hierarchy,
          i.lane_status,
          i.o_loc_type,
          i.o_facility_id,
          i.o_facility_alias_id,
          UPPER (i.o_city),
          i.o_state_prov,
          UPPER (i.o_county),
          i.o_postal_code,
          i.o_country_code,
          I.O_ZONE_ID,
          I.O_ZONE_NAME,
          i.d_loc_type,
          i.d_facility_id,
          i.d_facility_alias_id,
          UPPER (i.d_city),
          i.d_state_prov,
          UPPER (i.d_county),
          i.d_postal_code,
          i.d_country_code,
          I.D_ZONE_ID,
          I.D_ZONE_NAME,
          DECODE (i.lane_status, 4, 1, 0) has_errors,
          COALESCE (
             (SELECT MAX (DECODE (d.rating_lane_dtl_status, 4, 1, 0))
                FROM import_rating_lane_dtl d
               WHERE d.lane_id = i.lane_id
                     AND d.tc_company_id = i.tc_company_id),
             0)
             dtl_has_errors,
          i.created_source_type,
          i.created_source,
          i.created_dttm,
          i.last_updated_source_type,
          i.last_updated_source,
          i.last_updated_dttm,
          '',
          CAST (NULL AS INT),
          CAST (NULL AS INT),
          CAST (NULL AS INT)
     FROM import_rating_lane i
    WHERE i.lane_status = 4
          AND NOT EXISTS
                     (SELECT 1
                        FROM rating_lane r
                       WHERE i.tc_company_id = r.tc_company_id
                             AND i.rating_lane_id = r.lane_id);
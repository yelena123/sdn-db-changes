set scan off echo on;

CREATE OR REPLACE FUNCTION DATEDIFF
(
 p_startdate in date,
 p_enddate in date
)
return number
as
begin
 return p_startdate - p_enddate;
end DATEDIFF;
/


CREATE OR REPLACE FUNCTION "FN_APPT_CURR_EQUIP"
(
	p_appt_id	in	ILM_APPOINTMENTS.APPOINTMENT_ID%type
)
return EQUIPMENT_INSTANCE.EQUIPMENT_INSTANCE_ID%type
is
	l_trailer	EQUIPMENT_INSTANCE.EQUIPMENT_INSTANCE_ID%type;
begin
	begin
		select	EQUIPMENT_ID1
		into		l_trailer
		from		ILM_YARD_ACTIVITY
		where		APPOINTMENT_ID	= p_appt_id
		and		ACTIVITY_TYPE	= 50
		and		rownum		= 1
		;
	exception
		when	NO_DATA_FOUND	then
			l_trailer := NULL;
	end;

	if (l_trailer is NULL) then
		begin
	   		select	ilmeq.EQUIPMENT_INSTANCE_ID
			into		l_trailer
			from		ILM_APPT_EQUIPMENTS	ilmeq,
					EQUIPMENT_INSTANCE	ei,
					EQUIPMENT			e
			where		ilmeq.APPOINTMENT_ID		= p_appt_id
			and		ilmeq.EQUIPMENT_INSTANCE_ID	= ei.EQUIPMENT_INSTANCE_ID
			and		ei.EQUIPMENT_ID		= e.EQUIPMENT_ID
			and		e.EQUIPMENT_TYPE			= 16
			;
		exception
			when	NO_DATA_FOUND	then
				 l_trailer := NULL;
		end;
	end if;

	return	l_trailer;
end FN_APPT_CURR_EQUIP;
/


CREATE OR REPLACE FUNCTION fn_get_shipval (p_ship_id IN NUMBER)
   RETURN VARCHAR2
AS
   l_out   VARCHAR2 (9999);
   l_cnt   NUMBER;
BEGIN
   l_cnt := 0;
   l_out := '';

   FOR ship_cur IN (SELECT (   TO_CHAR (arch_stop.stop_seq)
                            || '_'
                            || TO_CHAR (arch_stop.facility_id)
                            || '_'
                            || TO_CHAR (arch_stop.stop_tz)
                            || '_'
                            || TO_CHAR (arch_stop.appt_dttm,
                                        'YYYY-MM-DD HH24:MI:SS'
                                       )
                           ) AS res
                      FROM arch_shipment, arch_stop_action, arch_stop
                     WHERE arch_stop_action.action_type IN ('DA', 'DL')
                       AND arch_stop.stop_seq = arch_stop_action.stop_seq
                       AND arch_stop_action.shipment_id =
                                                     arch_shipment.shipment_id
                       AND arch_stop.shipment_id = arch_shipment.shipment_id
                       AND arch_shipment.shipment_id = p_ship_id)
   LOOP
      l_cnt := l_cnt + 1;

      IF l_cnt = 1
      THEN
         l_out := ship_cur.res;
      ELSE
         l_out := l_out || ',' || ship_cur.res;
      END IF;
   END LOOP;

   RETURN l_out;
END;
/


CREATE OR REPLACE FUNCTION GETDATE
return date
as
begin
 return SYSDATE;
end GETDATE;
/


CREATE OR REPLACE FUNCTION SUB_DAYS (
 hv_date In  date,
 hv_days IN integer
)
return date
is
 begin
 RETURN hv_date - hv_days;
end;
/
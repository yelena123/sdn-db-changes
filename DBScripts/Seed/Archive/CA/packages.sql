set scan off echo on;

CREATE OR REPLACE PACKAGE "LANE_LOCATION_PKG"
as
    -- if this value changes all tables containing
 -- these strings will need to be refreshed
    cvDelimiter  constant varchar2(1) := '~';

    function fnGetLocationString (
  aLocType      IN VARCHAR2
  ,aFacilityId  IN rating_lane.o_facility_id%TYPE
  ,aCity        IN rating_lane.o_city%TYPE
  ,aCounty      IN rating_lane.o_county%TYPE
  ,aStateProv   IN rating_lane.o_state_prov%TYPE
  ,aPostalCode  IN rating_lane.o_postal_code%TYPE
  ,aCountryCode IN rating_lane.o_country_code%TYPE
  ,aZoneId      IN rating_lane.o_zone_name%TYPE
 )
 return varchar2
    ;

    function fnGetFacilityCSVFromAlias3PL (
 aCompanyId    IN VARCHAR2
 ,aList        IN VARCHAR2
) return varchar2;

    function fnGetLocationCSVFromCSV (
  aPrefix       IN VARCHAR2
  ,aList        IN VARCHAR2
  ,aSuffix      IN VARCHAR2 default null
 )
 return varchar2
 ;

    function fnGetFacilityCSVFromAliasCSV (
  aCompanyId    IN number
  ,aList        IN VARCHAR2
 )
 return varchar2
 ;

    function fnGetImportLocationString (
  vBusinessUnit  IN import_rating_lane.BUSINESS_UNIT%TYPE,
  vOFacilityAliasId  IN import_rating_lane.O_FACILITY_ALIAS_ID%TYPE,
  vOCity    IN import_rating_lane.O_CITY%TYPE,
  vOStateProv   IN import_rating_lane.O_STATE_PROV%TYPE,
  vOCounty   IN import_rating_lane.O_COUNTY%TYPE,
  vOPostal   IN import_rating_lane.O_POSTAL_CODE%TYPE,
  vOCountry   IN import_rating_lane.O_COUNTRY_CODE%TYPE,
  vOZone    IN import_rating_lane.o_zone_name%TYPE,
  vDFacilityAliasId  IN import_rating_lane.D_FACILITY_ALIAS_ID%TYPE,
  vDCity    IN import_rating_lane.D_CITY%TYPE,
  vDStateProv   IN import_rating_lane.D_STATE_PROV%TYPE,
  vDCounty   IN import_rating_lane.D_COUNTY%TYPE,
  vDPostal   IN import_rating_lane.D_POSTAL_CODE%TYPE,
  vDCountry   IN import_rating_lane.D_COUNTRY_CODE%TYPE,
  vDZone    IN import_rating_lane.d_zone_name%TYPE
 )
 return varchar2
 ;

end
;
/


CREATE OR REPLACE PACKAGE "RATING_ROUTING_FILTER_PKG"
AS
---------------------------------------------------------------------------------------
--
--   DATE               AUTHOR         DESCRIPTION
--  ----------         ---------      ---------------------------------------------------
--  07/15/2005         RS             modified by Priti Khare (CR#3354)
--  11/11/2005 Ganesh  modifed CODE to ID's
--  01/24/2006 Rajeev  CR# 17923
--  06/28/2006 Ganesh  CR# 28058 added parameter sailingSchdName in get_rate_data_from_filter_csv procedure
--  8th April 2009 gakumar MACR00262049
---------------------------------------------------------------------------------------


 TYPE rate_filter_curtype IS REF CURSOR;
  gv_companyHierarchy      VARCHAR2(12500);

 PROCEDURE GET_RATE_DATA_FROM_FILTER_CSV
 (
      aQueryType                   IN VARCHAR2,  -- RATING or ROUTING OR SAILING
            coid                         IN rating_lane.tc_company_id%TYPE,
            ofacid                       IN VARCHAR2,
            ocity                        IN rating_lane.o_city%TYPE,
            ocnty                        IN rating_lane.o_county%TYPE,
            ost                          IN rating_lane.o_state_prov%TYPE,
            ozip                         IN rating_lane.o_postal_code%TYPE,
            ocountry                     IN rating_lane.o_country_code%TYPE,
            ozone                        IN rating_lane.o_zone_id%TYPE,
            dfacid                       IN VARCHAR2,
            dcity                        IN rating_lane.d_city%TYPE,
            dcnty                        IN rating_lane.d_county%TYPE,
            dst                          IN rating_lane.d_state_prov%TYPE,
            dzip                         IN rating_lane.d_postal_code%TYPE,
            dcountry                     IN rating_lane.d_country_code%TYPE,
            dzone                        IN rating_lane.d_zone_id%TYPE,
            idt                          IN DATE,
            ccode                        IN rating_lane_dtl.carrier_id%TYPE,
            mot                          IN rating_lane_dtl.mot_id%TYPE,
            equip                        IN rating_lane_dtl.equipment_id%TYPE,
            sl                           IN rating_lane_dtl.service_level_id%TYPE,
            pl                           IN rating_lane_dtl.protection_level_id%TYPE,
         originVia                    IN rating_lane_dtl.o_ship_via%TYPE DEFAULT NULL,
         destinationVia               IN rating_lane_dtl.d_ship_via%TYPE DEFAULT NULL,
         contractNumber               IN rating_lane_dtl.contract_number%TYPE DEFAULT NULL,
         pkgName                     IN rating_lane_dtl.PACKAGE_NAME%TYPE DEFAULT NULL,
         sailingSchdName             IN rating_lane_dtl.SAILING_SCHEDULE_NAME%TYPE DEFAULT NULL,
         search_type                  IN VARCHAR2 DEFAULT NULL,
         sort_field                   IN VARCHAR2,
         sort_direction               IN VARCHAR2,
          error_indicator              IN NUMBER,
         start_row                    IN NUMBER,
         end_row                      IN NUMBER,
         row_count                    OUT NUMBER,
            rate_filter_refcur           OUT rate_filter_curtype,
          aRgQualifier                 IN RG_LANE.RG_QUALIFIER%TYPE DEFAULT NULL,
            companyIdList                IN VARCHAR2,
             COMPANYHIERARCHY   IN VARCHAR2,
             CUSTOMER                     IN VARCHAR2,
            LANE_NAME                    IN VARCHAR2,
            INCOTERM_ID                  IN NUMBER,
            BILLING_ID                   IN NUMBER,
            ROUTE_TO                     IN VARCHAR2,
            ROUTE_TYPE_1                 IN VARCHAR2,
            ROUTE_TYPE_2                 IN VARCHAR2,
            SHIP_PARAM_WHERE_CLAUSE      IN VARCHAR2
   ) ;


 FUNCTION fnFormatFilterLocWhereClause
        (
  aSearchType IN VARCHAR2,
        loc_prefix IN VARCHAR2,
        coid IN rating_lane.tc_company_id%TYPE,
        facid IN VARCHAR2,
        city IN rating_lane.o_city%TYPE,
        cnty IN rating_lane.o_county%TYPE,
        st IN rating_lane.o_state_prov%TYPE,
        zip IN rating_lane.o_postal_code%TYPE,
        COUNTRY IN rating_lane.o_country_code%TYPE,
        ZONE IN rating_lane.o_zone_id%TYPE
        )
    RETURN VARCHAR2;

 FUNCTION fnGetZoneListCsv
     (
     coid IN rg_lane.tc_company_id%TYPE,
     facid IN VARCHAR2,
     city IN rg_lane.o_city%TYPE,
     st IN rg_lane.o_state_prov%TYPE,
     zip IN rg_lane.o_postal_code%TYPE,
     COUNTRY IN rg_lane.o_country_code%TYPE
     )
 RETURN VARCHAR2
 ;

 FUNCTION fnFormatImportLocFilter
        (
  aSearchType IN VARCHAR2,
        loc_prefix IN VARCHAR2,
        coid IN rating_lane.tc_company_id%TYPE,
        facid IN VARCHAR2,
        city IN rating_lane.o_city%TYPE,
        cnty IN rating_lane.o_county%TYPE,
        st IN rating_lane.o_state_prov%TYPE,
        zip IN rating_lane.o_postal_code%TYPE,
        COUNTRY IN rating_lane.o_country_code%TYPE,
        ZONE IN rating_lane.o_zone_id%TYPE
        )
    RETURN VARCHAR2
 ;

 FUNCTION fnCreateCsvSubstr
 (
      aString    VARCHAR2,
   aLength    NUMBER,
   aAllLanes  BOOLEAN
 )
 RETURN VARCHAR2
    ;


END ;
/


CREATE OR REPLACE PACKAGE "RG_SUB_VALIDATION_PKG"
AS
	  FUNCTION VALIDATE_BUSINESS_UNIT
	  (
	   	  vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
	   	  vBusinessUnitId	  IN business_unit.BUSINESS_UNIT%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_FACILITY
	  (
	   	  vTCCompanyId		  IN company.COMPANY_ID%TYPE,
	   	  vFacilityId		  IN facility.FACILITY_ID%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_FACILITY_ALIAS
	  (
	      vTCCompanyId        IN company.COMPANY_ID%TYPE,
	   	  vFacilityAliasId	   IN facility_alias.FACILITY_ALIAS_ID%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_ZONE
	  (
	      vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
	   	  vZone			  IN zone.ZONE_ID%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_STATE_PROV
	  (
	      vStateProv			  IN state_prov.STATE_PROV%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_POSTAL_CODE
	  (
	      vPostalCode			  IN postal_code.POSTAL_CODE%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_COUNTRY
	  (
	      vCountry			  IN country.COUNTRY_CODE%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_CARRIER_CODE
	  (
	      vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
	      vCarrierCode		  IN carrier_code.CARRIER_CODE%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_MODE
	  (
	      vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
	      vMode			  IN mot.MOT%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_SERVICE_LEVEL
	  (
	      vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
	   	  vServiceLevel	  IN service_level.SERVICE_LEVEL%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_EQUIPMENT
	  (
	      vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
	   	  vEquipment		  IN equipment.EQUIPMENT_CODE%TYPE
	  )
	  RETURN NUMBER;
	  FUNCTION VALIDATE_PROTECTION_LEVEL
	  (
	      vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
	   	  vProtectionLevel	  IN protection_level.PROTECTION_LEVEL%TYPE
	  )
	  RETURN NUMBER;

        FUNCTION VALIDATE_SIZE_UOM
    (
       vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
       vSizeUom			  IN SIZE_UOM.SIZE_UOM%TYPE
    )
    RETURN NUMBER;

	FUNCTION VALIDATE_CUSTOMER
	(
		vTCCompanyId	IN company.COMPANY_ID%TYPE,
		vCustomerId		IN rg_lane.CUSTOMER_ID%TYPE
	)
	RETURN NUMBER;

	FUNCTION VALIDATE_CUSTOMER_CODE
	(
		vTCCompanyId	IN company.COMPANY_ID%TYPE,
		vCustomerCode	IN import_rg_lane.CUSTOMER_CODE%TYPE
	)
	RETURN NUMBER;

	FUNCTION VALIDATE_INCOTERM
	(
		vTCCompanyId	IN company.COMPANY_ID%TYPE,
		vIncotermId		IN rg_lane.INCOTERM_ID%TYPE
	)
	RETURN NUMBER;

	FUNCTION VALIDATE_INCOTERM_NAME
	(
		vTCCompanyId	IN company.COMPANY_ID%TYPE,
		vIncotermName	IN import_rg_lane.INCOTERM_NAME%TYPE
	)
	RETURN NUMBER;

END RG_SUB_VALIDATION_PKG;
/
CREATE OR REPLACE PACKAGE BODY "LANE_LOCATION_PKG"
as
/*************************************
*
*  Returns the concatenated location string
*  of the location type then the relevent
*  data for the particular location type
*
*************************************/
function fnGetLocationString (
 aLocType      IN VARCHAR2
 ,aFacilityId  IN rating_lane.o_facility_id%TYPE
 ,aCity        IN rating_lane.o_city%TYPE
 ,aCounty      IN rating_lane.o_county%TYPE
 ,aStateProv   IN rating_lane.o_state_prov%TYPE
 ,aPostalCode  IN rating_lane.o_postal_code%TYPE
 ,aCountryCode IN rating_lane.o_country_code%TYPE
 ,aZoneId      IN rating_lane.o_zone_name%TYPE
)
return varchar2
is

    vLocationString  varchar2(50);

begin

    SELECT
        DECODE(aLocType
          ,'CO',aLocType || cvDelimiter || aCountryCode
          ,'FA',aLocType || cvDelimiter || TO_CHAR(aFacilityId)
    ,'CS',aLocType || cvDelimiter || aCity || cvDelimiter || aStateProv || cvDelimiter || aCountryCode
    ,'ST',aLocType || cvDelimiter || aStateProv || cvDelimiter || aCountryCode
    ,'P2',aLocType || cvDelimiter || SUBSTR(aPostalCode,1,2) || cvDelimiter || aCountryCode
    ,'P3',aLocType || cvDelimiter || SUBSTR(aPostalCode,1,3) || cvDelimiter || aCountryCode
    ,'P4',aLocType || cvDelimiter || SUBSTR(aPostalCode,1,4) || cvDelimiter || aCountryCode
    ,'P5',aLocType || cvDelimiter || SUBSTR(aPostalCode,1,5) || cvDelimiter || aCountryCode
    ,'P6',aLocType || cvDelimiter || SUBSTR(aPostalCode,1,6) || cvDelimiter || aCountryCode
    ,'ZN',aLocType || cvDelimiter || aZoneId
    ,NULL)
    INTO vLocationString
 FROM DUAL
 ;

 RETURN vLocationString;

end
;

/*************************************
*
*  Returns a CSV String with Location Type inserted in
*  front of each value in the input string
*
*  Output can be used in "in" queries against
*  search locations in rating_lane and rg_lane
*
*  For example:
*      aLocType = 'FA'     aList = '''test'',''test2'''
*      returns '''FA~test'',''FA~test2'''
*************************************/

function fnGetLocationCSVFromCSV (
 aPrefix       IN VARCHAR2
 ,aList        IN VARCHAR2
 ,aSuffix      IN VARCHAR2 default null
)
return varchar2
IS
    vList      varchar2(1000) := null;
 vPrefix    VARCHAR2(30) := aPrefix || cvDelimiter;
 vSuffix    VARCHAR2(30) := null;

 -- These can be changed if simular data is in the string
 cvValStart constant varchar2(5) := '<<<<';
 cvValEnd   constant varchar2(5) := '>>>>';
BEGIN

    if aList is not null then
  if aSuffix is not null then
        vSuffix := cvDelimiter || aSuffix;
  end if;

     vList := cvValStart || ltrim(rtrim(alist,''''),'''') || cvValEnd;
     vList := replace(vlist,''''',''''',cvValStart || cvValEnd);
     vList := replace(vlist,''',''',cvValEnd || cvValStart);
     vList := replace(vList,cvValStart || cvValEnd,''''',''''');
     vList := replace(vList,cvValStart,'''' || vPrefix);
     vList := rtrim(replace(vList,cvValEnd,vSuffix || ''','),',');
    end if;

 RETURN vList;
END
;

/*************************************
*
*  Returns a Search location string of facility id's
*  For the input company and facility alias csv list
*
*************************************/

function fnGetFacilityCSVFromAlias3PL (
 aCompanyId    IN VARCHAR2
 ,aList        IN VARCHAR2
)
return varchar2
is
    vSQL             varchar2(2000);
 vFacilityString  varchar2(2000) := null;

   TYPE tRefCur IS REF CURSOR;
    cFacilityId    tRefCur;
 nFacilityId    FACILITY_ALIAS.FACILITY_ID%TYPE;

 nCount         number := 0;

begin
           vSQL := 'SELECT FACILITY_ID FROM FACILITY_ALIAS ' ||
    ' WHERE TC_COMPANY_ID in ( '||aCompanyId||' ) and facility_alias_id in (' || aList  || ')';

--    '(select * from THE (SELECT CAST(IN_LIST(:FacilityAliasList) AS IN_LIST_TAB) FROM DUAL))';

--    open cFacilityId for vSql using aCompanyId, replace(aList,'''','');
 open cFacilityId for vSql ;
 loop
        FETCH cFacilityId into nFacilityId;
        EXIT WHEN cFacilityId%NOTFOUND;

  nCount := nCount + 1;
  vFacilityString := vFacilityString || ',''FA' || cvDelimiter || to_char(nFacilityId) || '''';
 end loop
 ;

 if nCount = 0 then
    vFacilityString := '''DUMMY''';
 end if;

 vFacilityString := LTRIM(vFacilityString,',');

 RETURN vFacilityString
 ;

null;

end
;

/****************************************
****************************************/
function fnGetFacilityCSVFromAliasCSV (
 aCompanyId    IN number
 ,aList        IN VARCHAR2
)
return varchar2
is
begin
 return fnGetFacilityCSVFromAlias3PL(aCompanyId,aList) ;

end
;

/****************************************
****************************************/
function fnGetImportLocationString (
 vBusinessUnit  IN import_rating_lane.BUSINESS_UNIT%TYPE,
 vOFacilityAliasId  IN import_rating_lane.O_FACILITY_ALIAS_ID%TYPE,
 vOCity    IN import_rating_lane.O_CITY%TYPE,
 vOStateProv   IN import_rating_lane.O_STATE_PROV%TYPE,
 vOCounty   IN import_rating_lane.O_COUNTY%TYPE,
 vOPostal   IN import_rating_lane.O_POSTAL_CODE%TYPE,
 vOCountry   IN import_rating_lane.O_COUNTRY_CODE%TYPE,
 vOZone    IN import_rating_lane.o_zone_name%TYPE,
 vDFacilityAliasId  IN import_rating_lane.D_FACILITY_ALIAS_ID%TYPE,
 vDCity    IN import_rating_lane.D_CITY%TYPE,
 vDStateProv   IN import_rating_lane.D_STATE_PROV%TYPE,
 vDCounty   IN import_rating_lane.D_COUNTY%TYPE,
 vDPostal   IN import_rating_lane.D_POSTAL_CODE%TYPE,
 vDCountry   IN import_rating_lane.D_COUNTRY_CODE%TYPE,
 vDZone    IN import_rating_lane.d_zone_name%TYPE
)
return varchar2
is
    vLocationString  varchar2(500);
begin

    vLocationString :=
  trim(vBusinessUnit) || cvDelimiter ||
  trim(vOFacilityAliasId) || cvDelimiter ||
  trim(vOCity) || cvDelimiter ||
  trim(vOStateProv) || cvDelimiter ||
  trim(vOCounty) || cvDelimiter ||
  trim(vOPostal) || cvDelimiter ||
  trim(vOCountry) || cvDelimiter ||
  trim(vOZone) || cvDelimiter ||
  trim(vDFacilityAliasId) || cvDelimiter ||
  trim(vDCity) || cvDelimiter ||
  trim(vDStateProv) || cvDelimiter ||
  trim(vDCounty) || cvDelimiter ||
  trim(vDPostal) || cvDelimiter ||
  trim(vDCountry) || cvDelimiter ||
  trim(vDZone) || cvDelimiter;

 return vLocationString;

end
;


end
;
/


CREATE OR REPLACE PACKAGE BODY "RATING_ROUTING_FILTER_PKG"
AS
--	***********************
--	Change Tracker
--	Date:		By:	Desc:
--	***********************
--	01/13/2004	MB		modified:	fnGetZoneListCsv ZONES cursor definition
--	08/18/2004	MB		modified:	get_rate_data_from_filter_csv vSubQueryFilter extended	(SP03 - Roman)
--  05/27/2005  Bo Wang  modified   TT 49915  filtering routing Lane based on Facility Ids
--      07/15/2005      RS              modified by Priti Khare (CR#3354)
--	11/11/2005	Ganesh	modified CODE columns to ID's
--	01/24/2006	Rajeev		CR# 17923
--	06/06/2006	Ganesh		CR# 24382
--	06/06/2006	Ganesh		CR# 26026
--	06/28/2006	Ganesh		CR# 28058
--	07/14/2006	Prashant	CR# 29682
--	07/25/2006	Abhishek	CR# 30441
--	08/07/2006      Abhishek        CR# 31220
--  	8th April 2009 gakumar MACR00262049
-- 	23rd April 2009 : gakumar MACR00275591 Fix
-- 	26th May 2009 : gakumar MACR00291954 Fix
--	***********************
	PROCEDURE get_rate_data_from_filter_csv
            (
    		    aQueryType                   IN VARCHAR2,  -- RATING or ROUTING OR SAILING
            coid                         IN rating_lane.tc_company_id%TYPE,
            ofacid                       IN VARCHAR2,
            ocity                        IN rating_lane.o_city%TYPE,
            ocnty                        IN rating_lane.o_county%TYPE,
            ost                          IN rating_lane.o_state_prov%TYPE,
            ozip                         IN rating_lane.o_postal_code%TYPE,
            ocountry                     IN rating_lane.o_country_code%TYPE,
            ozone                        IN rating_lane.o_zone_id%TYPE,
            dfacid                       IN VARCHAR2,
            dcity                        IN rating_lane.d_city%TYPE,
            dcnty                        IN rating_lane.d_county%TYPE,
            dst                          IN rating_lane.d_state_prov%TYPE,
            dzip                         IN rating_lane.d_postal_code%TYPE,
            dcountry                     IN rating_lane.d_country_code%TYPE,
            dzone                        IN rating_lane.d_zone_id%TYPE,
            idt                          IN DATE,
            ccode                        IN rating_lane_dtl.carrier_id%TYPE,
            mot                          IN rating_lane_dtl.mot_id%TYPE,
            equip                        IN rating_lane_dtl.equipment_id%TYPE,
            sl                           IN rating_lane_dtl.service_level_id%TYPE,
            pl                           IN rating_lane_dtl.protection_level_id%TYPE,
		    originVia                    IN rating_lane_dtl.o_ship_via%TYPE DEFAULT NULL,
		    destinationVia               IN rating_lane_dtl.d_ship_via%TYPE DEFAULT NULL,
			contractNumber               IN rating_lane_dtl.contract_number%TYPE DEFAULT NULL,
		    pkgName		                 IN rating_lane_dtl.PACKAGE_NAME%TYPE DEFAULT NULL,
			sailingSchdName		         IN rating_lane_dtl.SAILING_SCHEDULE_NAME%TYPE DEFAULT NULL,
			search_type                  IN VARCHAR2 DEFAULT NULL,
			sort_field                   IN VARCHAR2,
		 	sort_direction               IN VARCHAR2,
		    error_indicator              IN NUMBER,
			start_row                    IN NUMBER,
			end_row                      IN NUMBER,
			row_count                    OUT NUMBER,
            rate_filter_refcur           OUT rate_filter_curtype,
    		aRgQualifier                 IN RG_LANE.RG_QUALIFIER%TYPE DEFAULT NULL,
            companyIdList                IN VARCHAR2,
            COMPANYHIERARCHY		         IN VARCHAR2,
            CUSTOMER                     IN VARCHAR2,
            LANE_NAME                    IN VARCHAR2,
            INCOTERM_ID                  IN NUMBER,
            BILLING_ID                   IN NUMBER,
            ROUTE_TO                     IN VARCHAR2,
            ROUTE_TYPE_1                 IN VARCHAR2,
            ROUTE_TYPE_2                 IN VARCHAR2,
            SHIP_PARAM_WHERE_CLAUSE      IN VARCHAR2
			)
    IS

        sql_statement VARCHAR2(12500);
        sql_statement1 VARCHAR2(12500);
        sql_statement2 VARCHAR2(12500);
        sql_statement3 VARCHAR2(12500);
        sql_statement4 VARCHAR2(12500);
        cQueryFilterCust VARCHAR2(200);
        cQueryFilterBill VARCHAR2(200);
        cQueryFilterInco VARCHAR2(200);
        cQueryFilterRto VARCHAR2(200);
        cQueryFilterRt1 VARCHAR2(200);
        cQueryFilterRt2 VARCHAR2(200);
        cQueryFilterLaneName VARCHAR2(200);
            cExistClauseStartValid      CONSTANT VARCHAR2(400) :=
			        ' AND ( EXISTS ( SELECT 1 FROM <<<TABLE_NAME>>>_dtl srld ' ||
 	                'WHERE srld.tc_company_id = rl.tc_company_id AND srld.lane_id = rl.lane_id ';
            cInClauseStartValid          VARCHAR2(400) ;
            --:=
			       -- 'and ((rl.TC_COMPANY_ID, rl.lane_id) in ' ||
					   -- '(SELECT srld.tc_company_id, srld.lane_id ' ||
						 --  'FROM <<<TABLE_NAME>>>_dtl srld ' ||
 	            --          'WHERE srld.tc_company_id = <<<tc_company_id>>>';
            cExistClauseStartImport     CONSTANT VARCHAR2(500) :=
			        ' or  EXISTS ( SELECT 1 FROM IMPORT_<<<TABLE_NAME>>>_DTL srld, IMPORT_<<<TABLE_NAME>>> irl ' ||
                                       'WHERE rl.TC_COMPANY_ID = irl.TC_COMPANY_ID ' ||
                                       'AND rl.LANE_ID = irl.<<<TABLE_NAME>>>_ID ' ||
                                       'AND srld.TC_COMPANY_ID = irl.TC_COMPANY_ID ' ||
                                       'AND srld.LANE_ID = irl.LANE_ID ' ||
                                       'AND irl.LANE_STATUS = 4' ;
            cInClauseStartImport        VARCHAR2(500);

            cExistClauseStartErrImport  CONSTANT VARCHAR2(200) :=
			        ' AND EXISTS ( SELECT 1 FROM IMPORT_<<<TABLE_NAME>>>_dtl srld ' ||
					               'WHERE srld.tc_company_id = rl.tc_company_id AND srld.lane_id = rl.lane_id ';
            cInClauseStartErrImport  CONSTANT VARCHAR2(200) :=
			        'and (rl.TC_COMPANY_ID, rl.lane_id) in ' ||
			                    '( SELECT srld.TC_COMPANY_ID, srld.lane_id ' ||
								    'FROM IMPORT_<<<TABLE_NAME>>>_dtl srld ' ||
					               'WHERE srld.tc_company_id = <<<tc_company_id>>>';
			-- the aliases to the rating_lane_dtl or import_rating_lane_dtl must be the same
			-- 'srld'
            cSubQueryFilterCarrier      CONSTANT VARCHAR2(85) :=
	    			              ' and srld.carrier_id = '||ccode;

            cSubQueryFilterMot          CONSTANT VARCHAR2(80) :=
			                      ' and srld.mot_id = '|| mot;
            cSubQueryFilterEquipment    CONSTANT VARCHAR2(85) :=
			                      ' and srld.equipment_id = '||equip;
            cSubQueryFilterSl           CONSTANT VARCHAR2(85) :=
			                      ' and srld.SERVICE_LEVEL_id = '||sl;
            cSubQueryFilterPl           CONSTANT VARCHAR2(105) :=
			                      ' and srld.PROTECTION_LEVEL_id = '|| pl;
			      cSubQueryFilterOVia         CONSTANT VARCHAR2(85) :=
			                      ' and srld.O_SHIP_VIA = ''' || originVia ||'''';
			      cSubQueryFilterDVia         CONSTANT VARCHAR2(85) :=
			                      ' and srld.D_SHIP_VIA = '''|| destinationVia ||'''';
			      cSubQueryFilterCn           CONSTANT VARCHAR2(85) :=
			                      ' and srld.CONTRACT_NUMBER = ''|| contractNumber ||''';
            cSubQueryFilterDate         CONSTANT VARCHAR2(150) :=
                 ' AND TO_DATE(''' || TO_CHAR(idt, 'DD-MON-YYYY') || ''',''DD-MON-YYYY'') >= effective_dt ' ||
                 ' AND TO_DATE(''' || TO_CHAR(idt, 'DD-MON-YYYY') || ''',''DD-MON-YYYY'') <= expiration_dt ';


			cSubQueryFilterPkg           CONSTANT VARCHAR2(100) :=
			                      ' and srld.package_name = ''|| pkgName ||''';
			cSubQueryFilterSailingSch           CONSTANT VARCHAR2(100) :=
			                      ' and srld.sailing_schedule_name = ''|| sailingSchdName ||''';

            vSubQuery                   VARCHAR2(2000) := NULL;
            vSubQueryFilter             VARCHAR2(3000) := NULL;
			      vTableName                  VARCHAR2(30);
            origin_where_clause         VARCHAR2(5000);
            destination_where_clause    VARCHAR2(5000);
           -- sql_statement		     	VARCHAR2(12500);
            buid_where_clause           VARCHAR2(200) := NULL;
			rgQualifierWhereClause      VARCHAR2(200) := NULL;
            date_where_clause			VARCHAR2(200);
            error_where_clause			VARCHAR2(200) := NULL;
            order_by_clause				VARCHAR2(200);
			vSQLFromWhere               VARCHAR2(11000);
			vCarrierSql                 VARCHAR2(200);
			vBaseTable                  VARCHAR2(15);
			vImportDtlStatusColumn      VARCHAR2(30);
			bSelectiveLaneLevel         BOOLEAN := FALSE;
            vLaneIdColumn               VARCHAR2(30);
            vRgQualifierColumn          VARCHAR2(30) := NULL;
			vSqlErrm                    VARCHAR2(255);
			vStatusClause               VARCHAR2(255) := NULL;
			-- bSelectiveLaneLevel
			-- this set to true if the lane level information is filtered by business_unit, rg_qualifier,
			-- or one of the search locations
			-- The subqueies into the detail level will then be build based on the following logic
			-- if Selective -- subquery will be an exists clause
			-- if Not Selective -- subquery will be an in clause based on the tc_company_id and lane_id and
			---     the results of the subquery
     BEGIN
		/*insert into debug_filter_parameters values
		(aQueryType,coid,buid,ofacid,ocity,ocnty,ost,ozip,ocountry,ozone
		,dfacid,dcity,dcnty,dst,dzip,dcountry,dzone,idt
		,ccode,mot,equip,sl,pl,search_type,sort_field,sort_direction
		,error_indicator,start_row,end_row,aRgQualifier
		);
		commit;
		*/
		-- KDM:  Removed references to the company with contexts to get parses by company
		--dbms_session.set_context(cgPackageContext, 'tc_company_id',coid);
		--*********************************************************
		-- Lane Level filtering clauses
		--*********************************************************

    -- setting related company list for CR#16938
    		--dbms_session.set_context(cgPackageContext, 'relatedCompanyIdList',companyIdList);
    		--dbms_session.set_context(cgPackageContext, 'companyHierarchy',COMPANYHIERARCHY);
            gv_companyHierarchy := COMPANYHIERARCHY;


   cInClauseStartValid := 'and ((rl.TC_COMPANY_ID, rl.lane_id) in ' ||
					   '(SELECT srld.tc_company_id, srld.lane_id ' ||
						  'FROM <<<TABLE_NAME>>>_dtl srld ' ||
 	                     'WHERE srld.tc_company_id = <<<tc_company_id>>>' ;

  cInClauseStartImport :=
			        'or (rl.TC_COMPANY_ID, rl.lane_id) in (' ||
			                      'SELECT srld.tc_company_id, irl.<<<TABLE_NAME>>>_ID ' ||
					                'FROM IMPORT_<<<TABLE_NAME>>>_DTL srld, IMPORT_<<<TABLE_NAME>>> irl ' ||
                                     'WHERE srld.TC_COMPANY_ID = <<<tc_company_id>>> ' ||
                                     'AND srld.TC_COMPANY_ID = irl.TC_COMPANY_ID ' ||
                                     'AND srld.LANE_ID = irl.LANE_ID ' ||
                                     'AND irl.LANE_STATUS = 4' ;

   cQueryFilterLaneName := 'rl.lane_name = '''||lane_name||'''';
   cQueryFilterCust := 'rl.customer_id =(Select customer_id from customer where customer_name ='''||customer||''' and tc_company_id ='||to_char(coid)||') ';
   cQueryFilterBill := 'rl.billing_method = '||TO_CHAR(billing_id);
   cQueryFilterInco := 'rl.incoterm_id = '||TO_CHAR(incoterm_id);
   cQueryFilterRto :=  'rl.route_to = ''' ||route_to||'''';
   cQueryFilterRt1 := 'rl.route_type_1 = ''' ||route_type_1||'''';
   cQueryFilterRt2 := 'rl.route_type_2 =''' ||route_type_2||'''';


     /*   IF ( buid IS NOT NULL ) THEN
            buid_where_clause := ' AND rl.business_unit IN (' || fnFormatContextInClause(buid, 'BU') || ')';
			bSelectiveLaneLevel := TRUE;
        END IF;*/
        IF error_indicator = 0 OR error_indicator = 2 THEN
			origin_where_clause := fnFormatFilterLocWhereClause(search_type, 'o', coid, ofacid, ocity, ocnty, ost, ozip, ocountry, ozone);
			destination_where_clause := fnFormatFilterLocWhereClause(search_type,'d', coid, dfacid, dcity, dcnty, dst, dzip, dcountry, dzone);
        ELSE
			origin_where_clause := fnFormatImportLocFilter(search_type, 'o', coid, ofacid, ocity, ocnty, ost, ozip, ocountry, ozone);
			destination_where_clause := fnFormatImportLocFilter(search_type,'d', coid, dfacid, dcity, dcnty, dst, dzip, dcountry, dzone);
        END IF;

    IF aQueryType = 'SAILING' THEN
		    vBaseTable := 'SAILING_LANE';
			vImportDtlStatusColumn := 'IMPORT_SAILING_DTL_STATUS';
			vLaneIdColumn := 'rl.sailing_lane_id';
   ELSIF aQueryType = 'RATING' THEN
		    vBaseTable := 'RATING_LANE';
			vImportDtlStatusColumn := 'RATING_LANE_DTL_STATUS';
			vLaneIdColumn := 'rl.rating_lane_id';
		ELSE
		    vBaseTable := 'RG_LANE';
			vImportDtlStatusColumn := 'LANE_DTL_STATUS';
			vLaneIdColumn := 'rl.rg_lane_id';
			vRgQualifierColumn := ',rl.rg_qualifier';
			IF aRgQualifier IS NOT NULL THEN
	            rgQualifierWhereClause := ' AND rl.rg_qualifier IN (''' || aRgQualifier || ''')';
    			bSelectiveLaneLevel := TRUE;
			END IF;
        END IF
		;
		--*********************************************************
		-- Lane Detail Level filtering clauses
		--*********************************************************
		-- Add the where clauses when filtering data is provided
		IF (ccode IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterCarrier;
		END IF;

    IF (aQueryType = 'ROUTING' AND SHIP_PARAM_WHERE_CLAUSE IS NOT NULL) THEN
    	vSubQueryFilter := vSubQueryFilter || SHIP_PARAM_WHERE_CLAUSE ;
   END IF;

    		IF (mot IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterMot;
		END IF;
		IF (equip IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterEquipment;
		END IF;
		IF (sl IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterSl;
		END IF;
		IF (pl IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterPl;
		END IF;
		IF (originVia IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterOVia;
		END IF;
		IF (destinationVia IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterDVia;
		END IF;
		IF (contractNumber IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterCn;
		END IF;
		IF (pkgName IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterPkg;
		END IF;
		IF (sailingSchdName IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterSailingSch;
		END IF;
		IF (idt IS NOT NULL) THEN
		    vSubQueryFilter := vSubQueryFilter || cSubQueryFilterDate;
		END IF;
  	    IF ( error_indicator = 0 OR error_indicator = 2 ) THEN
		    --dbms_output.put_line('--- ei 0 or 2 ');
	        vTableName := vBaseTable;
			vLaneIdColumn := 'rl.lane_id';
			IF( error_indicator = 0 ) THEN
				vStatusClause := ' and rl.lane_status = 0 ';
			ELSE
				vStatusClause := ' and rl.lane_status = 1 ';
			END IF;
			IF vSubQueryFilter IS NOT NULL THEN
			    IF bSelectiveLaneLevel THEN
				    --dbms_output.put_line('bSelectiveLaneLevel');
		            vSubQuery := REPLACE(cExistClauseStartValid || vSubQueryFilter || ')' ||
					                CASE error_indicator
									    WHEN 0 THEN cExistClauseStartImport || vSubQueryFilter || '))'
										ELSE ')'
									END
									,'<<<TABLE_NAME>>>',vBaseTable);
				ELSE
				    --dbms_output.put_line('not bSelectiveLaneLevel');
		            vSubQuery := REPLACE(cInClauseStartValid || vSubQueryFilter || ')' ||
					                CASE error_indicator
		                                WHEN 0 THEN cInClauseStartImport || vSubQueryFilter || '))'
										ELSE ')'
									END
									,'<<<TABLE_NAME>>>',vBaseTable);
				END IF;
			END IF;
  	    ELSE
		    --dbms_output.put_line('--- ei 1 ');
	        vTableName := 'IMPORT_' || vBaseTable;
			-- vLaneIdColumn is set in the query type check above
			IF vSubQueryFilter IS NOT NULL THEN
			    IF bSelectiveLaneLevel THEN
		            vSubQuery := REPLACE(cExistClauseStartErrImport || vSubQueryFilter || ')'
     								,'<<<TABLE_NAME>>>',vBaseTable);
				ELSE
		            vSubQuery := REPLACE(cInClauseStartErrImport || vSubQueryFilter || ')'
     								,'<<<TABLE_NAME>>>',vBaseTable);
				END IF;
			END IF;
	   	    --:1:2error_where_clause := ' AND ( has_errors <> 0 or dtl_has_errors <> 0 ) ';
		END IF;
		--*********************************************************
		-- Prepare the SQL Statement with the Lane level
		-- filtering clauses
		--*********************************************************
		vSQLFromWhere :=
            ' FROM ' || vTableName || ' rl' ||
			  ' WHERE rl.tc_company_id in ( ' || companyIdList  || ' ) ' || vStatusClause
--3PL change for CR 29473
--            ' WHERE rl.tc_company_id = ' || TO_CHAR(coid)  || vStatusClause
			;
	   IF (origin_where_clause IS NOT NULL) THEN
	   	   vSQLFromWhere := vSQLFromWhere || ' AND ' || origin_where_clause;
		   bSelectiveLaneLevel := TRUE;
	   END IF;
	   IF (destination_where_clause IS NOT NULL) THEN
	   	   vSQLFromWhere := vSQLFromWhere || ' AND ' || destination_where_clause;
		   bSelectiveLaneLevel := TRUE;
	   END IF;
     --Adding new where clauses

 IF(lane_name IS NOT NULL)THEN
   vSQLFromWhere := vsqlfromwhere || ' AND ' || cQueryFilterLaneName;
	 bselectivelanelevel := TRUE;
END IF;

IF(customer IS NOT NULL)THEN
  vSQLFromWhere := vsqlfromwhere || ' AND ' || cQueryFilterCust;
	bselectivelanelevel := TRUE;
END IF;

IF(billing_id >= 0)THEN
  vSQLFromWhere := vsqlfromwhere || ' AND ' || cQueryFilterBill;
  bselectivelanelevel := TRUE;
END IF;

IF(incoterm_id >= 0)THEN
  vSQLFromWhere := vsqlfromwhere || ' AND ' || cQueryFilterInco ;
	bselectivelanelevel := TRUE;
END IF;

IF(route_to IS NOT NULL)THEN
   vSQLFromWhere := vsqlfromwhere || ' AND ' || cQueryFilterRto ;
   bselectivelanelevel := TRUE;
END IF;

IF(route_type_1 IS NOT NULL)THEN
  vSQLFromWhere := vsqlfromwhere || ' AND ' || cQueryFilterRt1;
	bselectivelanelevel := TRUE;
END IF;

IF(route_type_2 IS NOT NULL)THEN
  vSQLFromWhere := vsqlfromwhere || ' AND ' || cQueryFilterRt2;
  bselectivelanelevel := TRUE;
END IF;

IF aQueryType = 'SAILING' AND
   origin_where_clause IS NULL AND
   destination_where_clause IS NULL THEN
  -- MACR00275591
  IF error_indicator = 1 THEN
    vSQLFromWhere := vSQLFromWhere || ' AND LANE_STATUS=4 ';
  END IF;

END IF;
     --end adding new where clauses


	   vSQLFromWhere := vSQLFromWhere || buid_where_clause || rgQualifierWhereClause || vSubQuery;
   	   --IF (error_where_clause IS NOT NULL) THEN
	   --	  vSQLFromWhere := vSQLFromWhere || error_where_clause;
	   --END IF;
		-- Sorting of result set
		IF (sort_field = 'rl.lane_id') OR (sort_field = 'lane_id') THEN
		    -- do not add hierarchy/business_unit to the sort of laneId sorts
	   		order_by_clause := ' order by ' || 'rl.tc_company_id ' || sort_direction || ', ' ||
			                    sort_field || ' ' || sort_direction;
		ELSE
	   		order_by_clause := ' order by rl.tc_company_id ' || sort_direction || ', ' ||
			                    sort_field || ' ' || sort_direction ||
			                   ', rl.lane_hierarchy ' || sort_direction
			                   --', rl.business_unit ' || sort_direction
							   --|| ', rl.lane_id ' || sort_direction
							   ;
	    END IF;
		vSQLFromWhere := REPLACE(vSQLFromWhere,'<<<tc_company_id>>>',TO_CHAR(coid));
    sql_statement1 :=', route_to, route_type_1, route_type_2';
	   sql_statement3 := --'/* rate_route_filter.get_rate_data_from_filter_csv page detail */ ' ||
	        'SELECT ' || vLaneIdColumn || ' lane_id' ||
			',rl.tc_company_id, lane_hierarchy,' ||
            ' o_facility_id, o_facility_alias_id, o_city, o_state_prov, rl.o_county' ||
			' ,o_postal_code, o_country_code, o_zone_id,o_zone_name, ' ||
            ' d_facility_id, d_facility_alias_id, d_city, d_state_prov, rl.d_county';

			sql_statement2 :=' ,d_postal_code, d_country_code, d_zone_id,d_zone_name, ' ||
			TO_CHAR(error_indicator) ||' has_errors' ||
			' ,NVL((SELECT SIGN(MAX(DECODE(ID.' || vImportDtlStatusColumn || ',4,1,0)))' ||
		    '         FROM IMPORT_' || vBaseTable || ' IR, IMPORT_' || vBaseTable || '_DTL ID' ||
		    '        WHERE (rl.LANE_ID = IR.' || vBaseTable || '_ID' ||
		    '          AND rl.TC_COMPANY_ID = IR.TC_COMPANY_ID' ||
			'          AND IR.LANE_ID = ID.LANE_ID' ||
			'          AND IR.TC_COMPANY_ID = ID.TC_COMPANY_ID)),0) DTL_HAS_ERRORS' || vRgQualifierColumn ||
	        ' FROM ' || vTableName || ' rl' ||
		    ' WHERE (rl.tc_company_id, rl.lane_id) in ' ||
 			  '(select coid,id FROM (' ||
		       ' SELECT ROWNUM rn, tc_company_id coid, lane_id id FROM (' ||
		       ' SELECT rl.tc_company_id, rl.lane_id ' ||
			    vSQLFromWhere ||
				order_by_clause ||
				') WHERE ROWNUM <= :ern' ||
				') WHERE RN >= :srn)' ||
			order_by_clause;
      sql_statement4 := ', lane_name, customer_id, billing_method, incoterm_id ';

      IF aQueryType = 'ROUTING' THEN
        sql_statement := sql_statement3 ||sql_statement4||sql_statement1 || sql_statement2 ;
    END IF;
  IF aQueryType = 'RATING' THEN
  sql_statement := sql_statement3 ||sql_statement4 || sql_statement2 ;
 END IF;
 IF aQueryType = 'SAILING' THEN
  sql_statement := sql_statement3 || sql_statement2 ;
END IF;

            --INSERT INTO debug_sql VALUES ('rate_route_filter.get_rate_data_from_filter_csv detail'||':'||sys_context(cgPackageContext ,'equipment_code'),SYSDATE,sql_statement, vSqlErrm);
            --COMMIT;
	   BEGIN
	       EXECUTE IMMEDIATE '/* rate_route_filter.get_rate_data_from_filter_csv count */ SELECT COUNT(*) ' || vSQLFromWhere
		   INTO row_count
		   ;
	       OPEN rate_filter_refcur FOR sql_statement
		   USING end_row,start_row
		   ;


	   EXCEPTION
	   WHEN OTHERS THEN
	       vSqlErrm := SQLERRM;
	     --  INSERT INTO debug_sql VALUES ('rate_route_filter.get_rate_data_from_filter_csv count',SYSDATE,'SELECT COUNT(*) ' || vSQLFromWhere,vSqlErrm);
	      -- INSERT INTO debug_sql VALUES ('rate_route_filter.get_rate_data_from_filter_csv detail',SYSDATE,sql_statement, vSqlErrm);
		   --COMMIT;
           RAISE;
	   END;
    END get_rate_data_from_filter_csv;








	FUNCTION fnFormatFilterLocWhereClause
        (
		aSearchType IN VARCHAR2,
        loc_prefix IN VARCHAR2,
        coid IN rating_lane.tc_company_id%TYPE,
        facid IN VARCHAR2,
        city IN rating_lane.o_city%TYPE,
        cnty IN rating_lane.o_county%TYPE,
        st IN rating_lane.o_state_prov%TYPE,
        zip IN rating_lane.o_postal_code%TYPE,
        country IN rating_lane.o_country_code%TYPE,
        ZONE IN rating_lane.o_zone_id%TYPE
        )
    RETURN VARCHAR2
    IS
        formatted_where_clause  VARCHAR2(5000);
        vCountyClause           VARCHAR2(200) := NULL;
		    vSearchString           VARCHAR2(2000) := NULL;
        vcompanyIdList           VARCHAR2(2000) := NULL;
		    bAllLanes               BOOLEAN:= FALSE;
    BEGIN
		/* Horea 11/02/02 - if one of the fields is null the boolean value is null */
		IF( (aSearchType IS NOT NULL AND aSearchType = 'All Lanes' ) AND ( ZONE IS NULL ) ) THEN
			bAllLanes := TRUE;
		END IF;
        IF (facid IS NOT NULL) THEN

       		IF (trim(gv_companyHierarchy) IS NULL) THEN
		        vcompanyIdList:= to_char(coid);
		ELSE
		        vcompanyIdList:= gv_companyHierarchy; --sys_context( cgPackageContext ,'companyHierarchy') ;
        	END IF;

			vSearchString := Lane_Location_Pkg.fnGetFacilityCSVFromAlias3PL(vcompanyIdList,facid);
			IF NOT bAllLanes THEN
			    GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (city IS NOT NULL) THEN
			vSearchString := RTRIM(vSearchString || ',' ||
			        Lane_Location_Pkg.fnGetLocationCSVFromCSV('CS',city,st||'~'||country),',');
    		IF (cnty IS NOT NULL) THEN
             	vCountyClause :=
	                ' and ( rl.' || loc_prefix || '_county IN (' || cnty || ') OR rl.' || loc_prefix || '_county IS NULL )';
		       --  fnFormatContextInClause(REPLACE(cnty,'''',''), loc_prefix || 'CNTY') || ') OR rl.' || loc_prefix || '_county IS NULL )';
			END IF;
			IF ( NOT bAllLanes ) THEN
			   GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (st IS NOT NULL) THEN
			vSearchString := RTRIM(vSearchString || ',' ||
			        Lane_Location_Pkg.fnGetLocationCSVFromCSV('ST',st,country),',');
			IF NOT bAllLanes THEN
			    GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (zip IS NOT NULL) THEN
			vSearchString := REPLACE(
                   RTRIM(vSearchString || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P2', fnCreateCsvSubstr( zip, 2, bAllLanes) , country) || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P3', fnCreateCsvSubstr( zip, 3, bAllLanes) , country) || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P4', fnCreateCsvSubstr( zip, 4, bAllLanes) , country) || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P5', fnCreateCsvSubstr( zip, 5, bAllLanes) , country) || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('P6', fnCreateCsvSubstr( zip, 6, bAllLanes) , country),',')
				   ,',''''','') ;
			IF NOT bAllLanes THEN
			    GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (country IS NOT NULL) THEN
			vSearchString :=  RTRIM(vSearchString || ',' ||
			        Lane_Location_Pkg.fnGetLocationCSVFromCSV('CO',country),',');
			IF NOT bAllLanes THEN
			    GOTO CreateWhereClause;
			END IF;
        END IF;
        IF (ZONE IS NOT NULL) THEN
			vSearchString := RTRIM(vSearchString || ',' ||
			        Lane_Location_Pkg.fnGetLocationCSVFromCSV('ZN',ZONE),',');
		ELSE
            vSearchString := RTRIM(vSearchString || ',' ||
			       Lane_Location_Pkg.fnGetLocationCSVFromCSV('ZN'
				          , fnGetZoneListCsv( coid, facid, city, st, zip, country )
						  ),',') ;
        END IF;
<<CreateWhereClause>>
	   vSearchString := LTRIM(vSearchString,',');
	   IF vSearchString IS NOT NULL THEN
            formatted_where_clause := loc_prefix || '_SEARCH_LOCATION IN (' || vSearchString || ')';
	   END IF
	   ;
       IF vCountyClause IS NOT NULL THEN
	       formatted_where_clause := formatted_where_clause || vCountyClause;
	   END IF
	   ;
       RETURN formatted_where_clause;
    END fnFormatFilterLocWhereClause;
/************************************
*************************************/
FUNCTION fnGetZoneListCsv
    (
    coid IN rg_lane.tc_company_id%TYPE,
    facid IN VARCHAR2,
    city IN rg_lane.o_city%TYPE,
    st IN rg_lane.o_state_prov%TYPE,
    zip IN rg_lane.o_postal_code%TYPE,
    country IN rg_lane.o_country_code%TYPE
    )
RETURN VARCHAR2
IS
  	TYPE ZONE_CURSOR IS REF CURSOR;
    zones ZONE_CURSOR;
    vzone          ZONE.zone_id%TYPE;
    zone_list      VARCHAR2(1000);
	sql_str        VARCHAR2(4000);
	where_clause   VARCHAR2(4000) := NULL;
	date_str       VARCHAR(30);
    vcompanyIdList VARCHAR2(2000) := NULL;
BEGIN
	vcompanyIdList:= gv_companyHierarchy; --sys_context( cgPackageContext ,'companyHierarchy',4000) ;
	--CR 29473 for 3PL
	IF (vcompanyIdList IS NOT NULL)
	THEN
		sql_str :=  'select distinct z1.zone_id ' ||
            	'from zone_attribute z1 ' ||
                'where (z1.tc_company_id in (' || vcompanyIdList || ' ) ) ';
	ELSE
		sql_str :=  'select distinct z1.zone_id ' ||
            	'from zone_attribute z1 ' ||
                'where (z1.tc_company_id = ' || coid || ') ';
	END IF;

	IF( facid IS NOT NULL )
	THEN
		IF (vcompanyIdList IS NOT NULL)
		THEN
	       where_clause := where_clause || '( z1.attribute_type = ''FC'' and z1.attribute_value IN ( select to_char(facility_id) from facility_alias where facility_alias_id in ( '|| facid || ') and tc_company_id in ( ' || vcompanyIdList || ' ) ) ) ';
		ELSE
		   where_clause := where_clause || '( z1.attribute_type = ''FC'' and z1.attribute_value IN ( select to_char(facility_id) from facility_alias where facility_alias_id in ( '|| facid || ') and tc_company_id = ' || coid || ') ) ';
		END IF;
	END IF;
	IF( country IS NOT NULL )
	THEN

  	IF( where_clause IS NOT NULL and facid is not null )
		THEN
			where_clause := where_clause || ' or ' ;
		END IF;
		where_clause := where_clause || '( z1.attribute_type = ''CT'' and z1.attribute_value = ''' || country || ''' ) ';

  	IF( st IS NOT NULL )
		THEN
			where_clause := where_clause || ' or ( z1.attribute_type = ''ST'' and z1.attribute_value = ''' || st || ''' and z1.country_code = ''' || country || ''') ' ;
		END IF;

    IF(city IS NOT NULL AND st IS NOT NULL) THEN
    where_clause := where_clause || ' or ( z1.attribute_type = ''CI'' and z1.attribute_value = ' || city || ' and z1.attribute_value2 = ''' || st || ''' and z1.country_code = ''' || country || ''')' ;
    END IF;

		IF( zip IS NOT NULL )
		THEN
			where_clause := where_clause ||
                         'or (z1.attribute_type = ''Z2'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(zip,2,true) || ' ) and z1.country_code = ''' || country || ''') ' ||
                         'or (z1.attribute_type = ''Z3'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(zip,3,true) || ' ) and z1.country_code = ''' || country || ''') ' ||
                         'or (z1.attribute_type = ''Z4'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(zip,4,true) || ' ) and z1.country_code = ''' || country || ''') ' ;
       IF length(TRIM(zip)) > 6 THEN
       where_clause := where_clause ||
                         'or (z1.attribute_type = ''Z5'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(zip,5,true) || ' ) and z1.country_code = ''' || country || ''') ' ;
       END IF;

       IF length(TRIM(zip)) > 6 THEN
       where_clause := where_clause ||
                         'or (z1.attribute_type = ''Z6'' and z1.attribute_value in ( ' || fnCreateCsvSubstr(zip,6,true) || ' ) and z1.country_code = ''' || country || ''') ' ;
       END IF;

       where_clause := where_clause ||
					'or	(		z1.ATTRIBUTE_TYPE = ''PR'' ' ||
						'AND	 ' || zip || '  between z1.attribute_value and z1.attribute_value2 and	z1.COUNTRY_CODE = ''' || country || ''' )';

        END IF;
	END IF;
	IF where_clause IS NULL  OR WHERE_CLAUSE =''
	THEN
		RETURN NULL;
	END IF;
	sql_str := sql_str || ' and ( ' || where_clause || ' ) ';


    OPEN zones FOR sql_str;
    LOOP
        FETCH zones INTO vzone;
        EXIT WHEN zones%NOTFOUND;
        zone_list := zone_list || '''' || vzone || ''',';
    END LOOP;
    CLOSE zones;
	zone_list := RTRIM(zone_list,',');
    RETURN zone_list;
END;
/*************************************
*************************************/
FUNCTION fnFormatImportLocFilter
        (
		     aSearchType IN VARCHAR2,
         loc_prefix IN VARCHAR2,
         coid IN rating_lane.tc_company_id%TYPE,
         facid IN VARCHAR2,
         city IN rating_lane.o_city%TYPE,
         cnty IN rating_lane.o_county%TYPE,
         st IN rating_lane.o_state_prov%TYPE,
         zip IN rating_lane.o_postal_code%TYPE,
         country IN rating_lane.o_country_code%TYPE,
         ZONE IN rating_lane.o_zone_id%TYPE
        )
RETURN VARCHAR2
IS
    formatted_where_clause  VARCHAR2(4000);
    zones                   VARCHAR2(2000);
	bAllLanes               BOOLEAN;
	nZipPrecision           NUMBER;
	vPCInList               VARCHAR2(500);
	vZipFilter              VARCHAR2(2000)  := NULL;
BEGIN
   	bAllLanes := (NVL(aSearchType,'exact') = 'All Lanes' AND ZONE IS NULL);
    zones := fnGetZoneListCSV( coid, facid, city, st, zip, country );
    formatted_where_clause := NULL;
    IF (facid IS NOT NULL) THEN
        formatted_where_clause := formatted_where_clause ||
            '(nvl(' || loc_prefix || '_loc_type,''FA'') = ''FA''' ||
                ' and ' || loc_prefix || '_facility_alias_id in (' || facid || '))';
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
    END IF;
    IF (city IS NOT NULL) THEN
        IF (formatted_where_clause IS NOT NULL) THEN
            formatted_where_clause := formatted_where_clause || ' or ';
        END IF;
        formatted_where_clause := formatted_where_clause ||
            '(nvl('  || loc_prefix || '_loc_type,''CS'') = ''CS''' ||
                ' and ' || loc_prefix || '_city = ' || city ||
                ' and ' || loc_prefix || '_state_prov = ''' || st || '''' ||
        		' and ' || loc_prefix || '_country_code = ''' || country || '''';
		IF (cnty IS NOT NULL) THEN
           	formatted_where_clause := formatted_where_clause || ' and (' || loc_prefix || '_county = ' || cnty || ' OR ' || loc_prefix || '_county IS NULL )';
		END IF;
		formatted_where_clause := formatted_where_clause || ')';
		IF NOT bAllLanes THEN
		    GOTO endFunction ;
		END IF;
    END IF;
    IF (st IS NOT NULL) THEN
        IF (formatted_where_clause IS NOT NULL) THEN
            formatted_where_clause := formatted_where_clause || ' or ';
        END IF;
        formatted_where_clause := formatted_where_clause ||
            '(nvl(' || loc_prefix || '_loc_type,''ST'') = ''ST''' ||
                ' and ' || loc_prefix || '_state_prov = ''' || st || '''' ||
                ' and ' || loc_prefix || '_country_code = ''' || country || ''')';
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
    END IF;
    IF (zip IS NOT NULL) THEN
        nZipPrecision := 1;
		WHILE nZipPrecision < 6
		LOOP
		    nZipPrecision := nZipPrecision + 1;
			vPCInList := fnCreateCsvSubstr(zip,nZipPrecision,bAllLanes);
			IF LENGTH(vPCInList) > 2 THEN
			    vZipFilter := vZipFilter ||
	                ' or (nvl(' || loc_prefix || '_loc_type,''P' || TO_CHAR(nZipPrecision) || ''')' ||
					' = ''P' || TO_CHAR(nZipPrecision) || '''' ||
	                ' and ' || loc_prefix || '_postal_code in (' || vPCInList || ')' ||
	                ' and ' || loc_prefix || '_country_code = ''' || country || ''')' ;
			END IF
			;
		END LOOP
		;
        IF (formatted_where_clause IS NOT NULL) THEN
            formatted_where_clause := formatted_where_clause || ' or ';
        END IF;
		IF vZipFilter IS NOT NULL THEN
		    formatted_where_clause := formatted_where_clause || LTRIM(vZipFilter,' or ');
		END IF
		;
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
    END IF;
    IF (country IS NOT NULL) THEN
        IF (formatted_where_clause IS NOT NULL) THEN
            formatted_where_clause := formatted_where_clause || ' or ';
        END IF;
        formatted_where_clause := formatted_where_clause ||
            '(nvl(' || loc_prefix || '_loc_type,''CO'') = ''CO''' ||
                ' and ' || loc_prefix || '_country_code = ''' || country || ''')';
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
    END IF;
	IF (ZONE IS NOT NULL) THEN
	     IF (formatted_where_clause IS NOT NULL) THEN
	             formatted_where_clause := formatted_where_clause || ' or ';
	     END IF;
	     formatted_where_clause := formatted_where_clause ||
	         '(nvl(' || loc_prefix || '_loc_type,''ZN'') = ''ZN''' ||
	             ' and ' || loc_prefix || '_zone_id in ( ''' || ZONE || '''))';
		IF NOT bAllLanes THEN
		    GOTO endFunction;
		END IF;
	END IF;
	IF (zones IS NOT NULL) THEN
	     IF (formatted_where_clause IS NOT NULL) THEN
	             formatted_where_clause := formatted_where_clause || ' or ';
	     END IF;
	     formatted_where_clause := formatted_where_clause ||
	         '(nvl(' || loc_prefix || '_loc_type,''ZN'') = ''ZN''' ||
	             ' and ' || loc_prefix || '_zone_id in ( ' || zones || '))';
	END IF;
<<endFunction>>
    IF formatted_where_clause IS NOT NULL THEN
	   formatted_where_clause := 'lane_status = 4 and (' || formatted_where_clause ||')';
	END IF
	;
    RETURN formatted_where_clause;
END ;
/*************************************
*************************************/

/********************************************
********************************************/
FUNCTION fnCreateCsvSubstr
(
     aString    VARCHAR2,
	 aLength    NUMBER,
	 aAllLanes  BOOLEAN
)
RETURN VARCHAR2
IS
  vWorkString    VARCHAR2(500) := REPLACE( aString, '''' ) || ',';
  vResultStr     VARCHAR2(500) := NULL;
  vValue         VARCHAR2(20);
  i              NUMBER := 0;
BEGIN
	WHILE LENGTH(vWorkString) > 1 AND i < 10
	LOOP
	    i := i +1;
	    vValue := SUBSTR(vWorkString,1,INSTR(vWorkString,',') - 1);
		vWorkString := LTRIM(LTRIM(vWorkString,vValue),',');
	    IF aAllLanes THEN
    	    vValue := SUBSTR(vValue, 1, aLength);
	    END IF
		;
	    IF LENGTH(vValue) = aLength THEN
    	    vResultStr := vResultStr || ',''' || vValue || '''';
		END IF
		;
	END LOOP
	;
	RETURN LTRIM(NVL(vResultStr,''''''),',');
END;
END ;
/


CREATE OR REPLACE PACKAGE BODY "RG_SUB_VALIDATION_PKG"
AS
FUNCTION VALIDATE_BUSINESS_UNIT
    (
       vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
       vBusinessUnitId	  IN business_unit.BUSINESS_UNIT%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vBusinessUnitCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(BUSINESS_UNIT) INTO vBusinessUnitCount
       FROM BUSINESS_UNIT
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND BUSINESS_UNIT = vBusinessUnitId;
       IF (vBusinessUnitCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_BUSINESS_UNIT;
    FUNCTION VALIDATE_FACILITY
    (
       vTCCompanyId		  IN company.COMPANY_ID%TYPE,
       vFacilityId		  IN facility.FACILITY_ID%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vFacilityCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(FACILITY_ID) INTO vFacilityCount
       FROM FACILITY
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND FACILITY_ID = vFacilityId;
       IF (vFacilityCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_FACILITY;
    FUNCTION VALIDATE_FACILITY_ALIAS
    (
       vTCCompanyId        IN company.COMPANY_ID%TYPE,
       vFacilityAliasId	   IN facility_alias.FACILITY_ALIAS_ID%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail  		    NUMBER;
       vFacilityAliasCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(FACILITY_ALIAS_ID) INTO vFacilityAliasCount
       FROM FACILITY_ALIAS
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND FACILITY_ALIAS_ID = vFacilityAliasId;
       IF (vFacilityAliasCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_FACILITY_ALIAS;
    FUNCTION VALIDATE_ZONE
    (
       vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
       vZone			  IN zone.ZONE_ID%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vZoneCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(ZONE_ID) INTO vZoneCount
       FROM ZONE
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND ZONE_ID = vZone;
       IF (vZoneCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_ZONE;
    FUNCTION VALIDATE_STATE_PROV
    (
       vStateProv			  IN state_prov.STATE_PROV%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vStateCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(STATE_PROV) INTO vStateCount
       FROM STATE_PROV
       WHERE STATE_PROV = vStateProv;
       IF (vStateCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_STATE_PROV;
    FUNCTION VALIDATE_POSTAL_CODE
    (
       vPostalCode			  IN postal_code.POSTAL_CODE%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vPostalCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(POSTAL_CODE) INTO vPostalCount
       FROM POSTAL_CODE
       WHERE POSTAL_CODE = vPostalCode;
       IF (vPostalCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_POSTAL_CODE;
    FUNCTION VALIDATE_COUNTRY
    (
       vCountry			  IN country.COUNTRY_CODE%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vCountryCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(COUNTRY_CODE) INTO vCountryCount
       FROM COUNTRY
       WHERE COUNTRY_CODE = vCountry;
       IF (vCountryCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_COUNTRY;
    FUNCTION VALIDATE_CARRIER_CODE
    (
       vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
       vCarrierCode		  IN carrier_code.CARRIER_CODE%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(CARRIER_CODE) INTO vCount
       FROM CARRIER_CODE
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND CARRIER_CODE = vCarrierCode;
       IF (vCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_CARRIER_CODE;
    FUNCTION VALIDATE_MODE
    (
       vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
       vMode			  IN mot.MOT%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(MOT) INTO vCount
       FROM MOT
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND MOT = vMode AND MARK_FOR_DELETION = 0;
       IF (vCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_MODE;
    FUNCTION VALIDATE_SERVICE_LEVEL
    (
       vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
       vServiceLevel	  IN service_level.SERVICE_LEVEL%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(SERVICE_LEVEL) INTO vCount
       FROM SERVICE_LEVEL
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND SERVICE_LEVEL = vServiceLevel;
       IF (vCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_SERVICE_LEVEL;
    FUNCTION VALIDATE_EQUIPMENT
    (
       vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
       vEquipment		  IN equipment.EQUIPMENT_CODE%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(EQUIPMENT_CODE) INTO vCount
       FROM EQUIPMENT
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND EQUIPMENT_CODE = vEquipment;
       IF (vCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_EQUIPMENT;
    FUNCTION VALIDATE_PROTECTION_LEVEL
    (
       vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
       vProtectionLevel	  IN protection_level.PROTECTION_LEVEL%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(PROTECTION_LEVEL) INTO vCount
       FROM PROTECTION_LEVEL
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND PROTECTION_LEVEL = vProtectionLevel;
       IF (vCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_PROTECTION_LEVEL;

    FUNCTION VALIDATE_SIZE_UOM
    (
       vTCCompanyId 	  IN company.COMPANY_ID%TYPE,
       vSizeUom			  IN SIZE_UOM.SIZE_UOM%TYPE
    )
    RETURN NUMBER
    IS
       vPassFail   NUMBER;
       vCount  NUMBER;
    BEGIN
       vPassFail := 0;
       SELECT count(SIZE_UOM) INTO vCount
       FROM SIZE_UOM
       WHERE TC_COMPANY_ID = vTCCompanyId
          AND SIZE_UOM = vSizeUom ;
       IF (vCount < 1) THEN
       	  vPassFail := 1;
       END IF;
       RETURN vPassFail;
    END VALIDATE_SIZE_UOM;

	FUNCTION VALIDATE_CUSTOMER
	(
		vTCCompanyId	IN company.COMPANY_ID%TYPE,
		vCustomerId		IN rg_lane.CUSTOMER_ID%TYPE
	)
	RETURN NUMBER
	IS
		vPassFail NUMBER;
		vCustomerCount NUMBER;
	BEGIN
		vPassFail := 0;
    	SELECT count(CUSTOMER_ID) INTO vCustomerCount
		FROM CUSTOMER
    	WHERE TC_COMPANY_ID = vTCCompanyId AND CUSTOMER_ID = vCustomerId;
		IF (vCustomerCount < 1) THEN
			vPassFail := 1;
		END IF;
		RETURN vPassFail;
	END VALIDATE_CUSTOMER;

	FUNCTION VALIDATE_CUSTOMER_CODE
	(
		vTCCompanyId	IN company.COMPANY_ID%TYPE,
		vCustomerCode	IN import_rg_lane.CUSTOMER_CODE%TYPE
	)
	RETURN NUMBER
	IS
		vPassFail NUMBER;
		vCustomerCount NUMBER;
	BEGIN
		vPassFail := 0;
    	SELECT count(CUSTOMER_ID) INTO vCustomerCount
		FROM CUSTOMER
    	WHERE TC_COMPANY_ID = vTCCompanyId AND CUSTOMER_CODE = vCustomerCode;
		IF (vCustomerCount < 1) THEN
			vPassFail := 1;
		END IF;
		RETURN vPassFail;
	END VALIDATE_CUSTOMER_CODE;

	FUNCTION VALIDATE_INCOTERM
	(
		vTCCompanyId	IN company.COMPANY_ID%TYPE,
		vIncotermId		IN rg_lane.INCOTERM_ID%TYPE
	)
	RETURN NUMBER
	IS
		vPassFail NUMBER;
		vIncotermCount NUMBER;
	BEGIN
		vPassFail := 0;
    	SELECT count(INCOTERM_ID) INTO vIncotermCount
		FROM INCOTERM
    	WHERE   INCOTERM_ID = vIncotermId;
		IF (vIncotermCount < 1) THEN
			vPassFail := 1;
		END IF;
		RETURN vPassFail;
	END VALIDATE_INCOTERM;

	FUNCTION VALIDATE_INCOTERM_NAME
	(
		vTCCompanyId	IN company.COMPANY_ID%TYPE,
		vIncotermName	IN import_rg_lane.INCOTERM_NAME%TYPE
	)
	RETURN NUMBER
	IS
		vPassFail NUMBER;
		vIncotermCount NUMBER;
	BEGIN
		vPassFail := 0;
    	SELECT count(INCOTERM_ID) INTO vIncotermCount
		FROM INCOTERM
    	WHERE   INCOTERM_NAME = vIncotermName;
		IF (vIncotermCount < 1) THEN
			vPassFail := 1;
		END IF;
		RETURN vPassFail;
	END VALIDATE_INCOTERM_NAME;

END RG_SUB_VALIDATION_PKG;
/
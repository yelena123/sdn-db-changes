set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_INTERCEPTOR

INSERT INTO CL_INTERCEPTOR ( VERSION_ID,INTERCEPTOR_ID,INTERCEPTOR_TYPE,INTERCEPTOR_NAME,INTERCEPTOR_DATA,BUCKET_NAME_IN,BUCKET_NAME_OUT,FIELD_MASK_IN,FIELD_MASK_OUT,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 1,1,3,'VocollectInterceptor',null,'$REQUEST','$RESPONSE',null,null,SYSDATE,
systimestamp);

Commit;





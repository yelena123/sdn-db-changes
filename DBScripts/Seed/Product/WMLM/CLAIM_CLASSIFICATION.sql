set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CLAIM_CLASSIFICATION

INSERT INTO CLAIM_CLASSIFICATION ( CLAIM_CLASSIFICATION,DESCRIPTION) 
VALUES  ( 1,'CARGO');

INSERT INTO CLAIM_CLASSIFICATION ( CLAIM_CLASSIFICATION,DESCRIPTION) 
VALUES  ( 2,'TRANSPORTATION');

Commit;





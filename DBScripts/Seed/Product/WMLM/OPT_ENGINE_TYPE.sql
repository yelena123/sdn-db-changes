set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for OPT_ENGINE_TYPE

INSERT INTO OPT_ENGINE_TYPE ( OPT_ENGINE_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CONSOLIDATION','Consolidation Engine',SYSTIMESTAMP,systimestamp);

Commit;





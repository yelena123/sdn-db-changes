set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for NOTE_TYPE

INSERT INTO NOTE_TYPE ( NOTE_TYPE,DESCRIPTION,FOR_ORDERS,FOR_SHIPMENTS) 
VALUES  ( 'STANDARD','Standard Note',1,1);

INSERT INTO NOTE_TYPE ( NOTE_TYPE,DESCRIPTION,FOR_ORDERS,FOR_SHIPMENTS) 
VALUES  ( 'RATEINFO','Rate Information',0,1);

INSERT INTO NOTE_TYPE ( NOTE_TYPE,DESCRIPTION,FOR_ORDERS,FOR_SHIPMENTS) 
VALUES  ( 'DECLINE','Decline Info',1,0);

Commit;





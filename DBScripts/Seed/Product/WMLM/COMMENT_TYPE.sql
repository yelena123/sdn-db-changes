set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for COMMENT_TYPE

INSERT INTO COMMENT_TYPE ( CODE,DESCRIPTION) 
VALUES  ( 0,'All');

INSERT INTO COMMENT_TYPE ( CODE,DESCRIPTION) 
VALUES  ( 1,'Internal Only');

INSERT INTO COMMENT_TYPE ( CODE,DESCRIPTION) 
VALUES  ( 2,'Business Partner');

INSERT INTO COMMENT_TYPE ( CODE,DESCRIPTION) 
VALUES  ( 3,'Carrier');

Commit;





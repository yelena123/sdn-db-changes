SET DEFINE OFF;
SET ECHO ON;

CREATE OR REPLACE FORCE VIEW "E_EVNT_UOM_QTY" ("ELS_TRAN_ID", "TRAN_NBR", "WHSE", "MSRMNT_ID", "QTY", "USER_ID") AS
select distinct els_tran_id,
               tran_nbr,
               whse,
               msrmnt_id,
               uom_qty as qty,
               user_id
 from e_evnt_smry_elm_dtl;

CREATE OR REPLACE FORCE VIEW "HDR_DTL" ("ELS_TRAN_ID", "ADJ_AMT_SUM") AS
select e_evnt_smry_hdr.els_tran_id,
        coalesce (sum (e_evnt_adj_smry_dtl.adj_amt), 0) adj_amt_sum
   from    e_evnt_smry_hdr
        left outer join
           e_evnt_adj_smry_dtl
        on e_evnt_smry_hdr.els_tran_id = e_evnt_adj_smry_dtl.els_tran_id
 group by e_evnt_smry_hdr.els_tran_id;

CREATE OR REPLACE FORCE VIEW "ILM_APPOINTMENT_VIEW" ("APPOINTMENT_ID", "COMPANY_ID", "APPOINTMENTNUMBER", "APPOINTMENTDATETIME", "CARRIER", "CONTROL_ID", "CHECKIN_DATE", "APPT_TYPE", "STATUS", "DOCKTYPE", "DOORTYPE", "COMMENTS", "PURCHASEORDER", "TRAILERID", "TRAILERNUMBER", "TRACTORID", "TRACTORNUMBER", "BILLOFLADING", "SHIPMENTID", "SHIPMENTNUMBER", "SHIPMENT_TYPE", "ORDERNUMBER", "TC_ORDER_ID", "STOP_SEQ", "FACILITY_ID", "BUSINESS_PARTNER_ID", "IS_HAZMAT", "SEAL_NUMBER", "DRIVER_ID", "DRIVER_NAME", "APPT_PRIORITY", "LOADING_TYPE", "TYPE_DESCRIPTION", "STATUS_DESCRIPTION", "PRO_NUMBER") AS
select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      orders.purchase_order_number purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      orders.bill_of_lading_number billoflading,
      -1 shipmentid,
      '' shipmentnumber,
      '' shipment_type,
      orders.order_id ordernumber,
      orders.tc_order_id tc_order_id,
      -1 stop_seq,
      ilm_appointments.facility_id,
      orders.business_partner_id,
      orders.is_hazmat,
      '' seal_number,
      ilm_appointments.driver_id,
      driver.first_name || ' ' || driver.surname,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      cast (null as char) pro_number
 from ilm_appointments
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      join ilm_appointment_objects appt_obj
         on (appt_obj.appointment_id = ilm_appointments.appointment_id
             and appt_obj.company_id = ilm_appointments.company_id
             and appt_obj.appt_obj_type = 40)
      join orders
         on (orders.order_id = appt_obj.appt_obj_id)
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
 union
 select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      shipment.purchase_order purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      stop.bill_of_lading_number billoflading,
      shipment.shipment_id shipmentid,
      shipment.tc_shipment_id shipmentnumber,
      shipment.shipment_type,
      orders.order_id ordernumber,
      orders.tc_order_id tc_order_id,
      stop.stop_seq,
      ilm_appointments.facility_id,
      shipment.business_partner_id,
      shipment.is_hazmat,
      shipment.seal_number,
      ilm_appointments.driver_id,
      driver.first_name || ' ' || driver.surname,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      shipment.pro_number
 from ilm_appointments
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      join ilm_appointment_objects appt_obj
         on (appt_obj.appointment_id = ilm_appointments.appointment_id
             and appt_obj.company_id = ilm_appointments.company_id)
      join shipment
         on (shipment.shipment_id = appt_obj.appt_obj_id
             and appt_obj.appt_obj_type = 30)
      join stop
         on (stop.stop_seq = appt_obj.stop_seq
             and stop.shipment_id = shipment.shipment_id)
      join orders
         on (orders.shipment_id = shipment.shipment_id)
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
 union
 select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      shipment.purchase_order purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      stop.bill_of_lading_number billoflading,
      shipment.shipment_id shipmentid,
      shipment.tc_shipment_id shipmentnumber,
      shipment.shipment_type,
      orders.order_id ordernumber,
      orders.tc_order_id tc_order_id,
      stop.stop_seq,
      ilm_appointments.facility_id,
      shipment.business_partner_id,
      shipment.is_hazmat,
      shipment.seal_number,
      ilm_appointments.driver_id,
      driver.surname || ' ' || driver.first_name,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      shipment.pro_number
 from ilm_appointments
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      join ilm_appointment_objects appt_obj
         on (appt_obj.appointment_id = ilm_appointments.appointment_id
             and appt_obj.company_id = ilm_appointments.company_id)
      join shipment
         on (shipment.shipment_id = appt_obj.appt_obj_id
             and appt_obj.appt_obj_type = 30)
      join stop
         on (stop.stop_seq = appt_obj.stop_seq
             and stop.shipment_id = shipment.shipment_id)
      join order_movement om
         on (om.shipment_id = shipment.shipment_id)
      join orders
         on (orders.order_id = om.order_id)
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
 union
 select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      shipment.purchase_order purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      stop.bill_of_lading_number billoflading,
      shipment.shipment_id shipmentid,
      shipment.tc_shipment_id shipmentnumber,
      shipment.shipment_type,
      0 order_id,
      cast (null as char) tc_order_id,
      stop.stop_seq,
      ilm_appointments.facility_id,
      shipment.business_partner_id,
      shipment.is_hazmat,
      shipment.seal_number,
      ilm_appointments.driver_id,
      driver.surname || ' ' || driver.first_name,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      shipment.pro_number
 from ilm_appointments
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      join ilm_appointment_objects appt_obj
         on (appt_obj.appointment_id = ilm_appointments.appointment_id
             and appt_obj.company_id = ilm_appointments.company_id)
      join shipment
         on (shipment.shipment_id = appt_obj.appt_obj_id
             and appt_obj.appt_obj_type = 30)
      join stop
         on (stop.stop_seq = appt_obj.stop_seq
             and stop.shipment_id = shipment.shipment_id)
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
 union
 select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      cast (null as char) purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      cast (null as char) billoflading,
      cast (null as integer) shipmentid,
      cast (null as char) shipmentnumber,
      cast (null as char) shipmenttype,
      0,
      cast (null as char) tc_order_id,
      cast (null as smallint) stopseq,
      ilm_appointments.facility_id,
      cast (null as char) business_partner_id,
      cast (null as smallint) is_hazmat,
      cast (null as char) seal_number,
      ilm_appointments.driver_id,
      driver.first_name || ' ' || driver.surname,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      cast (null as char) pro_number
 from ilm_appointments
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
where appointment_id not in
         (select appointment_id from ilm_appointment_objects);

CREATE OR REPLACE FORCE VIEW "ILM_APPT_EQUIPMENTS_VIEW" ("APPOINTMENT_ID", "COMPANY_ID", "EQUIPMENT_INSTANCE_ID", "EQUIPMENT_TYPE") AS
select appointment_id,
      company_id,
      equipment_instance_id,
      getequiptype (equipment_instance_id)
 from ilm_appt_equipments;

CREATE OR REPLACE FORCE VIEW "ILM_APPT_TRAILER" ("APPOINTMENT_ID", "COMPANY_ID", "CARRIERID", "TRAILERNUMBER", "TRAILERID") AS
select distinct ilm_appointments.appointment_id,
               ilm_appointments.company_id,
               equipment_instance.carrier_id carrierid,
               equip_inst_ref_carrier trailernumber,
               ilm_appt_equipments.equipment_instance_id trailerid
 from equipment_instance,
      ilm_appt_equipments,
      ilm_appointments,
      equipment
where ilm_appt_equipments.appointment_id =
         ilm_appointments.appointment_id
      and ilm_appt_equipments.company_id = ilm_appointments.company_id
      and equipment_instance.equipment_instance_id =
             ilm_appt_equipments.equipment_instance_id
      and equipment_instance.equipment_id = equipment.equipment_id
      and equipment.equipment_type = 16
      and not exists
                 (select 1
                    from ilm_yard_activity iya
                   where iya.appointment_id =
                            ilm_appointments.appointment_id
                         and activity_type = 50)
 union
 select ilm_appointments.appointment_id,
      ilm_appointments.company_id,
      equipment_instance.carrier_id carrierid,
      equip_inst_ref_carrier trailernumber,
      equipment_instance.equipment_instance_id trailerid
 from ilm_yard_activity iya, ilm_appointments, equipment_instance
where     iya.appointment_id = ilm_appointments.appointment_id
      and iya.equipment_id1 = equipment_instance.equipment_instance_id
      and ilm_appointments.company_id = tc_company_id
      and activity_dttm =
             (select max (activity_dttm)
                from ilm_yard_activity a
               where a.appointment_id = iya.appointment_id
                     and a.activity_type = 50)
      and activity_type = 50;

CREATE OR REPLACE FORCE VIEW "ILM_BILL_SEALNUMBER_VIEW" ("APPOINTMENT_ID", "COMPANY_ID", "BILL_OF_LADING", "SEAL_NUMBER", "BUSINESS_PARTNER_ID") AS
select distinct ilm_appointments.appointment_id appointment_id,
               ilm_appointments.company_id company_id,
               orders.bill_of_lading_number bill_of_lading,
               '' seal_number,
               orders.business_partner_id business_partner_id
 from ilm_appointments, ilm_appointment_objects appt_obj, orders
where     appt_obj.appointment_id = ilm_appointments.appointment_id
      and appt_obj.company_id = ilm_appointments.company_id
      and appt_obj.appt_obj_type = 40
      and orders.order_id = appt_obj.appt_obj_id
 union
 select distinct ilm_appointments.appointment_id appointment_id,
               ilm_appointments.company_id company_id,
               stop.bill_of_lading_number bill_of_lading,
               shipment.seal_number seal_number,
               shipment.business_partner_id
 from ilm_appointments,
      ilm_appointment_objects appt_obj,
      shipment,
      stop
where     appt_obj.appointment_id = ilm_appointments.appointment_id
      and appt_obj.company_id = ilm_appointments.company_id
      and appt_obj.appt_obj_type = 30
      and stop.stop_seq = appt_obj.stop_seq
      and stop.shipment_id = shipment.shipment_id
      and shipment.shipment_id = appt_obj.appt_obj_id;

CREATE OR REPLACE FORCE VIEW "ILM_LOCATION_VIEW" ("TYPE", "LOCATION_ID", "SEQUENCE_NO", "CURRENT_QUANTITY", "DEFINED_SVG", "ZONE_ID", "ZONE_NAME", "STATUS", "TC_COMPANY_ID", "PARENT_LOC") AS
select /*+ ALL_ROWS */ 'YZN',
      ln.location_id,
      ln.sequence_no,
      current_quantity,
      ln.is_location_defined_in_svg,
      null,
      yard_zone_name,
      null,
      ln.tc_company_id,
      null
 from yard_zone yz, location ln
where     ln.location_objid_pk1 = to_char (yz.yard_id)
      and ln.location_objid_pk2 = to_char (yz.yard_zone_id)
      and ln.ilm_object_type = 28
 union all
 select /*+ ALL_ROWS */ 'DDR',
      ln.location_id,
      ln.sequence_no,
      current_quantity,
      ln.is_location_defined_in_svg,
      null,
      dock_door_name,
      dock_door_status,
      ln.tc_company_id,
      dock_id
 from dock_door dd, location ln
where     ln.location_objid_pk1 = to_char (dd.facility_id)
      and ln.location_objid_pk2 = dd.dock_id
      and ln.location_objid_pk3 = to_char (dd.dock_door_id)
      and ln.tc_company_id = dd.tc_company_id
      and ln.ilm_object_type = 8
 union all
 select /*+ ALL_ROWS */ 'YZS',
      ln.location_id,
      ln.sequence_no,
      current_quantity,
      ln.is_location_defined_in_svg,
      yard_zone_slot_id,
      yard_zone_slot_name,
      yard_zone_slot_status,
      ln.tc_company_id,
      y.yard_zone_name
 from yard_zone_slot yz, location ln, yard_zone y
where     ln.location_objid_pk1 = to_char (yz.yard_id)
      and ln.location_objid_pk2 = to_char (yz.yard_zone_id)
      and ln.location_objid_pk3 = to_char (yz.yard_zone_slot_id)
      and ln.ilm_object_type = 32
      and y.yard_zone_id = yz.yard_zone_id
 union all
 select /*+ ALL_ROWS */ 'DCK',
      ln.location_id,
      ln.sequence_no,
      current_quantity,
      ln.is_location_defined_in_svg,
      null,
      dock_id,
      null,
      ln.tc_company_id,
      null
 from dock dck, location ln
where     ln.location_objid_pk1 = to_char (dck.facility_id)
      and ln.location_objid_pk2 = dck.dock_id
      and ln.ilm_object_type = 4;

CREATE OR REPLACE FORCE VIEW "ILM_TASKS_DEST_LOCATION_VIEW" ("LOCATION_ID", "TASK_DETAILS") AS
select getval (tsk.task_details, 'DEST_LOCATION') as location_id,
      tsk.task_details
 from ilm_tasks tsk
where tsk.task_type = 40 and tsk.status in (20, 30, 40, 50)
 with read only;

CREATE OR REPLACE FORCE VIEW "ILM_TASKS_TRAILER_VIEW" ("TRAILER", "TASK_STATUS") AS
select getval (tsk.task_details, 'TRAILER') as trailer,
      description as task_status
 from ilm_tasks tsk, ilm_task_status tss
where     tsk.status >= 10
      and tsk.status < 60
      and tsk.task_details like '%TRAILER=%'
      and tsk.status = tss.task_status
 with read only;

CREATE OR REPLACE FORCE VIEW "ILM_TRAILER_VIEW" ("LOCKED", "TRAILER_ID", "TRAILER_NAME", "TRAILER_LOC", "EQUIPMENT_STATUS", "CARRIER_CODE", "APPOINTMENT_ID", "TC_APPOINTMENT_ID", "APPT_TYPE", "APPT_STATUS", "TRAILER_STATUS", "FACILITY_ID", "FACILITY_ALIAS", "LOCATION", "PURCHASE_ORDER_ID", "PURCHASE_ORDER", "VENDOR_ID", "SHIPMENT_ID", "SHIPMENT", "BILL_OF_LADING", "ASN_ID", "ASN", "LOAD_CONFIG", "YARD", "PARENT_LOCATION", "LOCATION_ID", "CARRIER_CODE_NAME", "IS_HAZMAT", "LICENSE_TAG_NUMBER", "LICENSE_STATE_PROV", "EQUIPMENT_ID", "CONDITION_TYPE", "EQUIP_DOOR_TYPE", "SEAL_NUMBER", "BEEPER_NUMBER", "APPT_PRIORITY", "ASSIGNED_LOCATION", "CHECKIN_DTTM", "FOB_INDICATOR", "COMPANY_ID", "CURRENT_LOCN", "ASSIGNED_LOCN") AS
SELECT EQUIPMENT_INSTANCE.IS_LOCKED,
 EQUIPMENT_INSTANCE.EQUIPMENT_INSTANCE_ID,
 EQUIPMENT_INSTANCE.EQUIP_INST_REF_CARRIER,
 EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID,
 EQUIPMENT_INSTANCE.EQUIP_INST_STATUS,
 CARRIER_CODE.CARRIER_CODE,
 IL.APPOINTMENT_ID,
 IL.TC_APPOINTMENT_ID,
 IL.APPT_TYPE,
 IL.APPT_STATUS,
 ILM_STATUS.DESCRIPTION,
 IL.FACILITY_ID,
 FACILITY_ALIAS.FACILITY_ALIAS_ID,
 FN_GET_LOC_CODE(EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID) LOC,
 (SELECT MAX(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 40
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) PO_ID,
 (SELECT (case
 when count(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID) > 1 then
 'Pos'
 else
 max(PURCHASE_ORDERS.TC_PURCHASE_ORDERS_ID)
 end)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, PURCHASE_ORDERS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = PURCHASE_ORDERS.PURCHASE_ORDERS_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 40
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) PO,
 (SELECT MAX(PURCHASE_ORDERS.BUSINESS_PARTNER_ID)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, PURCHASE_ORDERS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = PURCHASE_ORDERS.PURCHASE_ORDERS_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 40
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) BP,
 (SELECT MAX(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 30
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) SHIPMENT_ID,
 (SELECT (case
 when count(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID) > 1 then
 'Shipments'
 else
 max(SHIPMENT.tc_shipment_id)
 end)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, SHIPMENT
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = SHIPMENT.SHIPMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 30
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) SHIPMENT,
 (SELECT MAX(SHIPMENT.BILL_OF_LADING_NUMBER)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, SHIPMENT
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = SHIPMENT.SHIPMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 30
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) BILLOFLADING,
 (SELECT MAX(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 10
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) ASN_ID,
 (SELECT (case
 when count(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID) > 1 then
 'ASNs'
 else
 max(ASN.TC_ASN_ID)
 end)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, ASN
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = ASN.ASN_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 10
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) ASN,
 (SELECT MAX(ILM_APPT_LOAD_CONFIG.LOAD_CONFIG_ID)
 FROM ILM_APPT_LOAD_CONFIG, ILM_APPOINTMENTS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPT_LOAD_CONFIG.APPOINTMENT_ID
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) LOADCONFIG,
 (SELECT YARD.YARD_NAME
 FROM YARD, YARD_ZONE_SLOT
 WHERE YARD_ZONE_SLOT.YARD_ID = YARD.YARD_ID
 AND YARD_ZONE_SLOT.YARD_ZONE_SLOT_ID=LOCATION.LOCATION_OBJID_PK3
 and LOCATION.ILM_OBJECT_TYPE= 32) YARD,
 (SELECT PARENT_LOC
 FROM ILM_LOCATION_VIEW
 WHERE LOCATION_ID = EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID) PARENT_LOC,
 LOCATION.LOCATION_ID,
 CARRIER_CODE.CARRIER_CODE_NAME,
 (SELECT MAX(SHIPMENT.IS_HAZMAT)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, SHIPMENT
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = SHIPMENT.SHIPMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 30
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) IS_HAZMAT,
 EQUIPMENT_INSTANCE.LICENSE_TAG_NUMBER,
 EQUIPMENT_INSTANCE.LICENSE_STATE_PROV,
 EQUIPMENT_INSTANCE.EQUIPMENT_ID,
 EQUIPMENT_INSTANCE.CONDITION_TYPE,
 EQUIPMENT_INSTANCE.EQUIP_DOOR_TYPE,
 EQUIPMENT_INSTANCE.SEAL_NUMBER,
 IL.BEEPER_NUMBER,
 IL.APPT_PRIORITY,
 FN_GET_LOC_CODE(EQUIPMENT_INSTANCE.ASSIGNED_LOCATION_ID) ASSIGNED_LOCATION,
 IL.CHECKIN_DTTM,
 Equipment_Instance.Fob_Indicator,
 Il.Company_Id,
 (SELECT ZONE_NAME
 From Ilm_Location_View
 WHERE LOCATION_ID = EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID) current_locn,
 (SELECT ZONE_NAME
 FROM ILM_LOCATION_VIEW
 Where Location_Id = Equipment_Instance.Assigned_Location_Id) assigned_Locn
FROM EQUIPMENT_INSTANCE,
 ILM_APPOINTMENTS IL,
 CARRIER_CODE,
 ILM_STATUS,
 FACILITY_ALIAS,
 LOCATION
 WHERE EQUIPMENT_INSTANCE.CURRENT_APPOINTMENT_ID = IL.APPOINTMENT_ID
 AND EQUIPMENT_INSTANCE.EQUIP_INST_STATUS = ILM_STATUS.ILM_STATUS
 AND IL.APPT_STATUS IN (5, 6, 7, 8, 11, 12)
 AND EQUIPMENT_INSTANCE.CARRIER_ID = CARRIER_CODE.CARRIER_ID
 AND FACILITY_ALIAS.FACILITY_ID = IL.FACILITY_ID
 AND FACILITY_ALIAS.IS_PRIMARY = 1
 AND LOCATION.LOCATION_ID = EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID;

CREATE OR REPLACE FORCE VIEW "IMPORT_COMB_LANE" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "RG_QUALIFIER", "FREQUENCY", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_NAME", "CUSTOMER_ID", "BILLING_METHOD", "INCOTERM_ID", "ROUTE_TO", "ROUTE_TYPE_1", "ROUTE_TYPE_2", "NO_RATING", "USE_FASTEST", "USE_PREFERENCE_BONUS", "IS_ROUTING", "IS_RATING", "IS_SAILING") AS
SELECT il.tc_company_id,
il.lane_id,
il.lane_hierarchy,
il.lane_status,
il.o_loc_type,
il.o_facility_id,
il.o_facility_alias_id,
UPPER (il.o_city),
il.o_state_prov,
UPPER (il.o_county),
il.o_postal_code,
il.o_country_code,
il.o_zone_id,
il.o_zone_name,
il.d_loc_type,
il.d_facility_id,
il.d_facility_alias_id,
UPPER (il.d_city),
il.d_state_prov,
UPPER (il.d_county),
il.d_postal_code,
il.d_country_code,
il.d_zone_id,
il.d_zone_name,
il.rg_qualifier,
il.frequency,
DECODE (il.lane_status, 4, 1, 0) has_errors,
COALESCE (
(SELECT MAX (DECODE (d.lane_dtl_status, 4, 1, 0))
FROM import_rg_lane_dtl d
WHERE d.lane_id     = il.lane_id
AND d.tc_company_id = il.tc_company_id
), 0) dtl_has_errors,
il.created_source_type,
il.created_source,
il.created_dttm,
il.last_updated_source_type,
il.last_updated_source,
il.last_updated_dttm,
il.lane_name,
il.customer_id,
il.billing_method,
il.incoterm_id,
il.route_to,
il.route_type_1,
il.route_type_2,
il.no_rating,
il.use_fastest,
il.use_preference_bonus,
1,
0,
0
FROM import_rg_lane il
/*AND NOT EXISTS
(SELECT 1
FROM comb_lane cl
WHERE il.tc_company_id = cl.tc_company_id
AND il.lane_id      = cl.lane_id
AND cl.is_routing      = 1
)*/
UNION ALL
SELECT I.TC_COMPANY_ID,
I.LANE_ID,
I.LANE_HIERARCHY,
I.LANE_STATUS,
I.O_LOC_TYPE,
I.O_FACILITY_ID,
I.O_FACILITY_ALIAS_ID,
UPPER (I.O_CITY),
I.O_STATE_PROV,
UPPER (I.O_COUNTY),
I.O_POSTAL_CODE,
I.O_COUNTRY_CODE,
I.O_ZONE_ID,
I.O_ZONE_NAME,
I.D_LOC_TYPE,
I.D_FACILITY_ID,
I.D_FACILITY_ALIAS_ID,
UPPER (I.D_CITY),
I.D_STATE_PROV,
UPPER (I.D_COUNTY),
I.D_POSTAL_CODE,
I.D_COUNTRY_CODE,
I.D_ZONE_ID,
I.D_ZONE_NAME,
'',
'',
DECODE (CAST (I.LANE_STATUS AS INT), 4, 1, 0) HAS_ERRORS,
COALESCE (
(SELECT MAX (DECODE (CAST (D.LANE_DTL_STATUS AS INT), 4, 1, 0))
FROM IMPORT_SAILING_LANE_DTL D
WHERE D.LANE_ID     = I.LANE_ID
AND D.TC_COMPANY_ID = I.TC_COMPANY_ID
), 0) DTL_HAS_ERRORS,
I.CREATED_SOURCE_TYPE,
I.CREATED_SOURCE,
I.CREATED_DTTM,
I.LAST_UPDATED_SOURCE_TYPE,
I.LAST_UPDATED_SOURCE,
I.LAST_UPDATED_DTTM,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
0, --routing
0, --rating
1  --sailing
FROM import_SAILING_LANE I
/*AND NOT EXISTS
(SELECT 1
FROM COMB_LANE CL
WHERE I.TC_COMPANY_ID = CL.TC_COMPANY_ID
AND I.LANE_ID = CL.LANE_ID
AND CL.is_SAILING     = 1
)*/
UNION ALL
SELECT i.tc_company_id,
i.lane_id,
i.lane_hierarchy,
i.lane_status,
i.o_loc_type,
i.o_facility_id,
i.o_facility_alias_id,
UPPER (i.o_city),
i.o_state_prov,
UPPER (i.o_county),
i.o_postal_code,
i.o_country_code,
I.O_ZONE_ID,
I.O_ZONE_NAME,
i.d_loc_type,
i.d_facility_id,
i.d_facility_alias_id,
UPPER (i.d_city),
i.d_state_prov,
UPPER (i.d_county),
i.d_postal_code,
i.d_country_code,
I.D_ZONE_ID,
I.D_ZONE_NAME,
'',
'',
DECODE (i.lane_status, 4, 1, 0) has_errors,
COALESCE (
(SELECT MAX (DECODE (d.rating_lane_dtl_status, 4, 1, 0))
FROM import_rating_lane_dtl d
WHERE d.lane_id     = i.lane_id
AND d.tc_company_id = i.tc_company_id
), 0) dtl_has_errors,
i.created_source_type,
i.created_source,
i.created_dttm,
i.last_updated_source_type,
i.last_updated_source,
i.last_updated_dttm,
'',
CAST (NULL AS INT),
CAST (NULL AS INT),
CAST (NULL AS INT),
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
0, --routing
1, --rating
0  --sailing
FROM import_rating_lane i
  /*AND NOT EXISTS
(SELECT 1
FROM COMB_LANE CL
WHERE i.tc_company_id = CL.tc_company_id
AND i.lane_id  = CL.lane_id
AND CL.IS_RATING      = 1
);*/
;

CREATE OR REPLACE FORCE VIEW "IMPORT_COMB_LANE_DTL" ("TC_COMPANY_ID", "LANE_ID", "LANE_DTL_STATUS", "CARRIER_ID", "MOT_ID", "EQUIPMENT_ID", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL_ID", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "EFFECTIVE_DT", "EXPIRATION_DT", "PACKAGE_NAME", "SAILING_SCHEDULE_NAME", "SP_MIN_LPN_COUNT", "SP_MAX_LPN_COUNT", "SP_MIN_WEIGHT", "SP_MAX_WEIGHT", "SP_MIN_VOLUME", "SP_MAX_VOLUME", "SP_MIN_LINEAR_FEET", "SP_MAX_LINEAR_FEET", "SP_MIN_MONETARY_VALUE", "SP_MAX_MONETARY_VALUE", "OVERRIDE_CODE", "CUBING_INDICATOR") AS
select  rating_dtl.tc_company_id, rating_dtl.lane_id,rating_dtl.import_rating_dtl_status, rating_dtl.carrier_id, rating_dtl.mot_id, rating_dtl.equipment_id, rating_dtl.service_level_id, rating_dtl.protection_level_id, rating_dtl.o_ship_via, rating_dtl.d_ship_via,rating_dtl.contract_number, rating_dtl.effective_dt,
rating_dtl.expiration_dt, rating_dtl.package_name, null as sailing_schedule_name, null as SP_MIN_LPN_COUNT, null as  SP_MAX_LPN_COUNT, null as  SP_MIN_WEIGHT, null as  SP_MAX_WEIGHT, null as  SP_MIN_VOLUME, null as  SP_MAX_VOLUME, null as SP_MIN_LINEAR_FEET, null as  SP_MAX_LINEAR_FEET, null as
SP_MIN_MONETARY_VALUE, null as SP_MAX_MONETARY_VALUE, null as  OVERRIDE_CODE, null as  CUBING_INDICATOR from import_rating_lane_dtl  rating_dtl
UNION ALL
select  rg_dtl.tc_company_id, rg_dtl.lane_id,rg_dtl.import_rg_dtl_status, rg_dtl.carrier_id, rg_dtl.mot_id, rg_dtl.equipment_id, rg_dtl.service_level_id, rg_dtl.protection_level_id, null as o_ship_via, null as d_ship_via, null as contract_number, rg_dtl.effective_dt,
rg_dtl.expiration_dt, rg_dtl.package_name, null as sailing_schedule_name, SP_MIN_LPN_COUNT, SP_MAX_LPN_COUNT, SP_MIN_WEIGHT, SP_MAX_WEIGHT, SP_MIN_VOLUME, SP_MAX_VOLUME,SP_MIN_LINEAR_FEET, SP_MAX_LINEAR_FEET,
SP_MIN_MONETARY_VALUE,SP_MAX_MONETARY_VALUE, OVERRIDE_CODE, CUBING_INDICATOR from import_rg_lane_dtl rg_dtl
UNION ALL
select  sailing_dtl.tc_company_id, sailing_dtl.lane_id, sailing_dtl.lane_dtl_status , sailing_dtl.carrier_id, sailing_dtl.mot_id,  null as equipment_id, sailing_dtl.service_level_id,  null as protection_level_id, null as o_ship_via, null as d_ship_via, null as contract_number, sailing_dtl.effective_dt,sailing_dtl.expiration_dt, null as package_name,sailing_dtl.sailing_schedule_name, null as SP_MIN_LPN_COUNT, null as  SP_MAX_LPN_COUNT, null as  SP_MIN_WEIGHT, null as  SP_MAX_WEIGHT, null as  SP_MIN_VOLUME, null as  SP_MAX_VOLUME, null as SP_MIN_LINEAR_FEET, null as  SP_MAX_LINEAR_FEET, null as
SP_MIN_MONETARY_VALUE, null as SP_MAX_MONETARY_VALUE, null as  OVERRIDE_CODE, null as  CUBING_INDICATOR from import_sailing_lane_dtl sailing_dtl;

CREATE OR REPLACE FORCE VIEW "ITEM_MASTER" ("ITEM_ID", "SEASON", "SEASON_YR", "STYLE", "STYLE_SFX", "COLOR", "COLOR_SFX", "SEC_DIM", "QUAL", "SIZE_DESC", "SKU_BRCD", "SKU_DESC", "DSP_SKU", "COLOR_DESC", "STAB_CODE", "ITEM_ORIENTATION", "PROTN_FACTOR", "CAVITY_LEN", "CAVITY_WD", "CAVITY_HT", "INCREMENTAL_LEN", "INCREMENTAL_WD", "INCREMENTAL_HT", "STACKABLE_ITEM", "MAX_NEST_NUMBER", "UNIT_HT", "UNIT_LEN", "UNIT_WIDTH", "UNIT_WT", "UNIT_VOL", "COMMODITY_LEVEL_DESC", "SKU_IMAGE_FILENAME", "STAT_CODE", "CATCH_WT", "CD_MASTER_ID", "ITEM_MASTER_ID", "CREATE_DATE_TIME", "MOD_DATE_TIME", "WM_VERSION_ID", "FTZ_HDR_ID", "UPC_PRE_DIGIT", "UPC_VENDOR_CODE", "UPC_SRL_PROD_NBR", "UPC_POST_DIGIT", "SKU_ID", "NMFC_CODE", "SIZE_RANGE_CODE", "SIZE_REL_POSN_IN_TABLE", "VOLTY_CODE", "PKG_TYPE", "PROD_SUB_GRP", "PROD_TYPE", "PROD_LINE", "SALE_GRP", "COORD_1", "COORD_2", "CARTON_TYPE", "UNIT_PRICE", "RETAIL_PRICE", "OPER_CODE", "MAX_CASE_QTY", "CUBE_MULT_QTY", "NEST_VOL", "NEST_CNT", "UNITS_PER_PICK_ACTIVE", "HNDL_ATTR_ACTIVE", "UNITS_PER_PICK_CASE_PICK", "HNDL_ATTR_CASE_PICK", "UNITS_PER_PICK_RESV", "HNDL_ATTR_RESV", "PROD_LIFE_IN_DAY", "MAX_RECV_TO_XPIRE_DAYS", "AVG_DLY_DMND", "WT_TOL_PCNT", "CONS_PRTY_DATE_CODE", "CONS_PRTY_DATE_WINDOW", "CONS_PRTY_DATE_WINDOW_INCR", "ACTVTN_DATE", "ALLOW_RCPT_OLDER_SKU", "CRITCL_DIM_1", "CRITCL_DIM_2", "CRITCL_DIM_3", "MFG_DATE_REQD", "XPIRE_DATE_REQD", "SHIP_BY_DATE_REQD", "SKU_ATTR_REQD", "BATCH_REQD", "PROD_STAT_REQD", "CNTRY_OF_ORGN_REQD", "VENDOR_ITEM_NBR", "PICK_WT_TOL_TYPE", "PICK_WT_TOL_AMNT", "MHE_WT_TOL_TYPE", "MHE_WT_TOL_AMNT", "LOAD_ATTR", "TEMP_ZONE", "TRLR_TEMP_ZONE", "PKT_CONSOL_ATTR", "BUYER_DISP_CODE", "CRUSH_CODE", "CONVEY_FLAG", "STORE_DEPT", "MERCH_TYPE", "MERCH_GROUP", "SPL_INSTR_CODE_1", "SPL_INSTR_CODE_2", "SPL_INSTR_CODE_3", "SPL_INSTR_CODE_4", "SPL_INSTR_CODE_5", "SPL_INSTR_CODE_6", "SPL_INSTR_CODE_7", "SPL_INSTR_CODE_8", "SPL_INSTR_CODE_9", "SPL_INSTR_CODE_10", "SPL_INSTR_1", "SPL_INSTR_2", 
"PROMPT_FOR_VENDOR_ITEM_NBR", "PROMPT_PACK_QTY", "ECCN_NBR", "EXP_LICN_NBR", "EXP_LICN_XP_DATE", "EXP_LICN_SYMBOL", "ORGN_CERT_CODE", "ITAR_EXEMPT_NBR", "FRT_CLASS", "DFLT_BATCH_STAT", "DFLT_INCUB_LOCK", "BASE_INCUB_FLAG", "INCUB_DAYS", "INCUB_HOURS", "SRL_NBR_BRCD_TYPE", "MINOR_SRL_NBR_REQ", "DUP_SRL_NBR_FLAG", "MAX_RCPT_QTY", "FTZ_FLAG", "HTS_NBR", "CC_UNIT_TOLER_VALUE", "CC_WGT_TOLER_VALUE", "CC_DLR_TOLER_VALUE", "CC_PCNT_TOLER_VALUE", "VOCOLLECT_BASE_WT", "VOCOLLECT_BASE_QTY", "VOCOLLECT_BASE_ITEM", "PICK_WT_TOL_AMNT_ERROR", "PRICE_TKT_TYPE", "MONETARY_VALUE", "MV_CURRENCY_CODE", "MV_SIZE_UOM", "CODE_DATE_PROMPT_METHOD_FLAG", "MIN_RECV_TO_XPIRE_DAYS", "MIN_PCNT_FOR_LPN_SPLIT", "MIN_LPN_QTY_FOR_SPLIT", "PROD_CATGRY", "PRICE_TIX_AVAIL_FLAG", "USER_ID", "UN_NBR", "HAZMAT_ID", "PROD_GROUP", "COMMODITY_CODE", "DSP_QTY_UOM", "DB_QTY_UOM", "STD_CASE_QTY", "STD_CASE_WT", "STD_CASE_VOL", "STD_CASE_LEN", "STD_CASE_WIDTH", "STD_CASE_HT", "STD_PACK_QTY", "STD_PACK_WT", "STD_PACK_VOL", "STD_PACK_LEN", "STD_PACK_WIDTH", "STD_PACK_HT", "STD_SUB_PACK_QTY", "STD_SUB_PACK_WT", "STD_SUB_PACK_VOL", "STD_SUB_PACK_LEN", "STD_SUB_PACK_WIDTH", "STD_SUB_PACK_HT", "STD_TIER_QTY", "STD_BUNDL_QTY", "STD_PLT_QTY", "MARKS_NBRS", "NET_COST_FLAG", "PRODUCER_FLAG", "PREF_CRIT_FLAG", "SRL_NBR_REQD") AS
(SELECT ITEM_CBO.ITEM_ID,
       ITEM_CBO.ITEM_SEASON AS SEASON,
       ITEM_CBO.ITEM_SEASON_YEAR AS SEASON_YR,
       ITEM_CBO.ITEM_STYLE AS STYLE,
       ITEM_CBO.ITEM_STYLE_SFX AS STYLE_SFX,
       ITEM_CBO.ITEM_COLOR AS COLOR,
       ITEM_CBO.ITEM_COLOR_SFX AS COLOR_SFX,
       ITEM_CBO.ITEM_SECOND_DIM AS SEC_DIM,
       ITEM_CBO.ITEM_QUALITY AS QUAL,
       ITEM_CBO.ITEM_SIZE_DESC AS SIZE_DESC,
       ITEM_CBO.ITEM_BAR_CODE AS SKU_BRCD,
       ITEM_CBO.DESCRIPTION AS SKU_DESC,
       ITEM_CBO.ITEM_NAME AS DSP_SKU,
       ITEM_CBO.COLOR_DESC AS COLOR_DESC,
       ITEM_CBO.STAB_CODE AS STAB_CODE,
       '0' AS ITEM_ORIENTATION,
       ITEM_CBO.PROTN_FACTOR AS PROTN_FACTOR,
       CAST (0 AS DECIMAL (4, 2)) AS CAVITY_LEN,
       CAST (0 AS DECIMAL (4, 2)) AS CAVITY_WD,
       CAST (0 AS DECIMAL (4, 2)) AS CAVITY_HT,
       CAST (0 AS DECIMAL (4, 2)) AS INCREMENTAL_LEN,
       CAST (0 AS DECIMAL (4, 2)) AS INCREMENTAL_WD,
       CAST (0 AS DECIMAL (4, 2)) AS INCREMENTAL_HT,
       'N' AS STACKABLE_ITEM,
       0 AS MAX_NEST_NUMBER,
       CAST (COALESCE (ITEM_CBO.UNIT_HEIGHT, 0) AS DECIMAL (16, 4))
          AS UNIT_HT,
       CAST (COALESCE (ITEM_CBO.UNIT_LENGTH, 0) AS DECIMAL (16, 4))
          AS UNIT_LEN,
       CAST (COALESCE (ITEM_CBO.UNIT_WIDTH, 0) AS DECIMAL (16, 4))
          AS UNIT_WIDTH,
       CAST (COALESCE (ITEM_CBO.UNIT_WEIGHT, 0) AS DECIMAL (16, 4))
          AS UNIT_WT,
       CAST (COALESCE (ITEM_CBO.UNIT_VOLUME, 0) AS DECIMAL (16, 4))
          AS UNIT_VOL,
       ITEM_CBO.COMMODITY_LEVEL_DESC AS COMMODITY_LEVEL_DESC,
       ITEM_CBO.ITEM_IMAGE_FILENAME AS SKU_IMAGE_FILENAME,
       CAST (COALESCE (ITEM_CBO.STATUS_CODE, 0) AS SMALLINT) AS STAT_CODE,
       CAST (ITEM_CBO.CATCH_WEIGHT_ITEM AS VARCHAR (4)) AS CATCH_WT,
       ITEM_CBO.COMPANY_ID AS CD_MASTER_ID,
       ITEM_CBO.ITEM_ID AS ITEM_MASTER_ID,
       ITEM_CBO.AUDIT_CREATED_DTTM AS CREATE_DATE_TIME,
       ITEM_CBO.AUDIT_LAST_UPDATED_DTTM AS MOD_DATE_TIME,
       1 AS WM_VERSION_ID,
       1 AS FTZ_HDR_ID,
       ITEM_CBO.ITEM_UPC_GTIN AS UPC_PRE_DIGIT,
       '' AS UPC_VENDOR_CODE,
       '' AS UPC_SRL_PROD_NBR,
       '' AS UPC_POST_DIGIT,
       ITEM_CBO.ITEM_NAME AS SKU_ID,
       ITEM_WMS.NMFC_CODE AS NMFC_CODE,
       ITEM_WMS.SIZE_RANGE_CODE AS SIZE_RANGE_CODE,
       ITEM_WMS.SIZE_REL_POSN_IN_TABLE AS SIZE_REL_POSN_IN_TABLE,
       ITEM_WMS.VOLTY_CODE AS VOLTY_CODE,
       ITEM_WMS.PKG_TYPE AS PKG_TYPE,
       ITEM_WMS.PROD_SUB_GRP AS PROD_SUB_GRP,
       ITEM_CBO.PROD_TYPE AS PROD_TYPE,
       ITEM_WMS.PROD_LINE AS PROD_LINE,
       ITEM_WMS.SALE_GRP AS SALE_GRP,
       CAST (COALESCE (ITEM_WMS.COORD_1, 0) AS INTEGER) AS COORD_1,
       CAST (COALESCE (ITEM_WMS.COORD_2, 0) AS INTEGER) AS COORD_2,
       ITEM_WMS.CARTON_TYPE AS CARTON_TYPE,
       CAST (COALESCE (ITEM_WMS.UNIT_PRICE, 0) AS DECIMAL (9, 2))
          AS UNIT_PRICE,
       CAST (COALESCE (ITEM_WMS.RETAIL_PRICE, 0) AS DECIMAL (9, 2))
          AS RETAIL_PRICE,
       ITEM_WMS.OPER_CODE AS OPER_CODE,
       CAST (COALESCE (ITEM_WMS.MAX_CASE_QTY, 0) AS DECIMAL (9, 2))
          AS MAX_CASE_QTY,
       CAST (COALESCE (ITEM_WMS.CUBE_MULT_QTY, 0) AS DECIMAL (9, 2))
          AS CUBE_MULT_QTY,
       CAST (ITEM_WMS.NEST_VOL AS DECIMAL (13, 4)) AS NEST_VOL,
       CAST (ITEM_WMS.NEST_CNT AS INTEGER) AS NEST_CNT,
       CAST (
          COALESCE (ITEM_WMS.UNITS_PER_PICK_ACTIVE, 0) AS DECIMAL (9, 2))
          AS UNITS_PER_PICK_ACTIVE,
       ITEM_WMS.HNDL_ATTR_ACTIVE AS HNDL_ATTR_ACTIVE,
       CAST (
          COALESCE (ITEM_WMS.UNITS_PER_PICK_CASE_PICK, 0) AS DECIMAL (9, 2))
          AS UNITS_PER_PICK_CASE_PICK,
       ITEM_WMS.HNDL_ATTR_CASE_PICK AS HNDL_ATTR_CASE_PICK,
       CAST (
          COALESCE (ITEM_WMS.UNITS_PER_PICK_RESV, 0) AS DECIMAL (9, 2))
          AS UNITS_PER_PICK_RESV,
       ITEM_WMS.HNDL_ATTR_RESV AS HNDL_ATTR_RESV,
       CAST (COALESCE (ITEM_WMS.PROD_LIFE_IN_DAY, 0) AS INTEGER)
          AS PROD_LIFE_IN_DAY,
       CAST (COALESCE (ITEM_WMS.MAX_RECV_TO_XPIRE_DAYS, 0) AS INTEGER)
          AS MAX_RECV_TO_XPIRE_DAYS,
       CAST (COALESCE (ITEM_WMS.AVG_DLY_DMND, 0) AS DECIMAL (13, 5))
          AS AVG_DLY_DMND,
       CAST (COALESCE (ITEM_WMS.WT_TOL_PCNT, 0) AS DECIMAL (5, 2))
          AS WT_TOL_PCNT,
       ITEM_WMS.CONS_PRTY_DATE_CODE AS CONS_PRTY_DATE_CODE,
       ITEM_WMS.CONS_PRTY_DATE_WINDOW AS CONS_PRTY_DATE_WINDOW,
       CAST (
          COALESCE (ITEM_WMS.CONS_PRTY_DATE_WINDOW_INCR, 0) AS SMALLINT)
          AS CONS_PRTY_DATE_WINDOW_INCR,
       ITEM_WMS.ACTVTN_DATE AS ACTVTN_DATE,
       ITEM_WMS.ALLOW_RCPT_OLDER_ITEM AS ALLOW_RCPT_OLDER_SKU,
       CAST (COALESCE (ITEM_WMS.CRITCL_DIM_1, 0) AS DECIMAL (7, 2))
          AS CRITCL_DIM_1,
       CAST (COALESCE (ITEM_WMS.CRITCL_DIM_2, 0) AS DECIMAL (7, 2))
          AS CRITCL_DIM_2,
       CAST (COALESCE (ITEM_WMS.CRITCL_DIM_3, 0) AS DECIMAL (7, 2))
          AS CRITCL_DIM_3,
       ITEM_WMS.MFG_DATE_REQD AS MFG_DATE_REQD,
       ITEM_WMS.XPIRE_DATE_REQD AS XPIRE_DATE_REQD,
       ITEM_WMS.SHIP_BY_DATE_REQD AS SHIP_BY_DATE_REQD,
       ITEM_WMS.ITEM_ATTR_REQD AS SKU_ATTR_REQD,
       ITEM_WMS.BATCH_REQD AS BATCH_REQD,
       ITEM_WMS.PROD_STAT_REQD AS PROD_STAT_REQD,
       ITEM_WMS.CNTRY_OF_ORGN_REQD AS CNTRY_OF_ORGN_REQD,
       ITEM_WMS.VENDOR_ITEM_NBR AS VENDOR_ITEM_NBR,
       ITEM_WMS.PICK_WT_TOL_TYPE AS PICK_WT_TOL_TYPE,
       CAST (COALESCE (ITEM_WMS.PICK_WT_TOL_AMNT, 0) AS SMALLINT)
          AS PICK_WT_TOL_AMNT,
       ITEM_WMS.MHE_WT_TOL_TYPE AS MHE_WT_TOL_TYPE,
       CAST (COALESCE (ITEM_WMS.MHE_WT_TOL_AMNT, 0) AS SMALLINT)
          AS MHE_WT_TOL_AMNT,
       ITEM_WMS.LOAD_ATTR AS LOAD_ATTR,
       ITEM_WMS.TEMP_ZONE AS TEMP_ZONE,
       ITEM_WMS.TRLR_TEMP_ZONE AS TRLR_TEMP_ZONE,
       ITEM_WMS.PKT_CONSOL_ATTR AS PKT_CONSOL_ATTR,
       ITEM_WMS.BUYER_DISP_CODE AS BUYER_DISP_CODE,
       ITEM_WMS.CRUSH_CODE AS CRUSH_CODE,
       ITEM_WMS.CONVEY_FLAG AS CONVEY_FLAG,
       ITEM_WMS.STORE_DEPT AS STORE_DEPT,
       ITEM_WMS.MERCH_TYPE AS MERCH_TYPE,
       ITEM_WMS.MERCH_GROUP AS MERCH_GROUP,
       ITEM_WMS.SPL_INSTR_CODE_1 AS SPL_INSTR_CODE_1,
       ITEM_WMS.SPL_INSTR_CODE_2 AS SPL_INSTR_CODE_2,
       ITEM_WMS.SPL_INSTR_CODE_3 AS SPL_INSTR_CODE_3,
       ITEM_WMS.SPL_INSTR_CODE_4 AS SPL_INSTR_CODE_4,
       ITEM_WMS.SPL_INSTR_CODE_5 AS SPL_INSTR_CODE_5,
       ITEM_WMS.SPL_INSTR_CODE_6 AS SPL_INSTR_CODE_6,
       ITEM_WMS.SPL_INSTR_CODE_7 AS SPL_INSTR_CODE_7,
       ITEM_WMS.SPL_INSTR_CODE_8 AS SPL_INSTR_CODE_8,
       ITEM_WMS.SPL_INSTR_CODE_9 AS SPL_INSTR_CODE_9,
       ITEM_WMS.SPL_INSTR_CODE_10 AS SPL_INSTR_CODE_10,
       ITEM_WMS.SPL_INSTR_1 AS SPL_INSTR_1,
       ITEM_WMS.SPL_INSTR_2 AS SPL_INSTR_2,
       ITEM_WMS.PROMPT_FOR_VENDOR_ITEM_NBR AS PROMPT_FOR_VENDOR_ITEM_NBR,
       ITEM_WMS.PROMPT_PACK_QTY AS PROMPT_PACK_QTY,
       ITEM_WMS.ECCN_NBR AS ECCN_NBR,
       ITEM_WMS.EXP_LICN_NBR AS EXP_LICN_NBR,
       ITEM_WMS.EXP_LICN_XP_DATE AS EXP_LICN_XP_DATE,
       ITEM_WMS.EXP_LICN_SYMBOL AS EXP_LICN_SYMBOL,
       ITEM_WMS.ORGN_CERT_CODE AS ORGN_CERT_CODE,
       ITEM_WMS.ITAR_EXEMPT_NBR AS ITAR_EXEMPT_NBR,
       ITEM_WMS.FRT_CLASS AS FRT_CLASS,
       CAST (COALESCE (ITEM_WMS.DFLT_BATCH_STAT, '0') AS VARCHAR (3))
          AS DFLT_BATCH_STAT,
       ITEM_WMS.DFLT_INCUB_LOCK AS DFLT_INCUB_LOCK,
       ITEM_WMS.BASE_INCUB_FLAG AS BASE_INCUB_FLAG,
       CAST (COALESCE (ITEM_WMS.INCUB_DAYS, 0) AS SMALLINT) AS INCUB_DAYS,
       CAST (COALESCE (ITEM_WMS.INCUB_HOURS, 0) AS DECIMAL (4, 2))
          AS INCUB_HOURS,
       ITEM_WMS.SRL_NBR_BRCD_TYPE AS SRL_NBR_BRCD_TYPE,
       CAST (COALESCE (ITEM_WMS.MINOR_SRL_NBR_REQ, 0) AS SMALLINT)
          AS MINOR_SRL_NBR_REQ,
       CAST (COALESCE (ITEM_WMS.DUP_SRL_NBR_FLAG, 0) AS SMALLINT)
          AS DUP_SRL_NBR_FLAG,
       CAST (COALESCE (ITEM_WMS.MAX_RCPT_QTY, 0) AS DECIMAL (9, 2))
          AS MAX_RCPT_QTY,
       CAST (NULL AS VARCHAR (1)) AS FTZ_FLAG,
       CAST (NULL AS VARCHAR (12)) AS HTS_NBR,
       CAST (COALESCE (ITEM_WMS.CC_UNIT_TOLER_VALUE, 0) AS INTEGER)
          AS CC_UNIT_TOLER_VALUE,
       CAST (COALESCE (ITEM_WMS.CC_WGT_TOLER_VALUE, 0) AS INTEGER)
          AS CC_WGT_TOLER_VALUE,
       CAST (COALESCE (ITEM_WMS.CC_DLR_TOLER_VALUE, 0) AS INTEGER)
          AS CC_DLR_TOLER_VALUE,
       CAST (COALESCE (ITEM_WMS.CC_PCNT_TOLER_VALUE, 0) AS INTEGER)
          AS CC_PCNT_TOLER_VALUE,
       CAST (COALESCE (ITEM_WMS.VOCOLLECT_BASE_WT, 0) AS DECIMAL (13, 4))
          AS VOCOLLECT_BASE_WT,
       CAST (COALESCE (ITEM_WMS.VOCOLLECT_BASE_QTY, 0) AS DECIMAL (9, 2))
          AS VOCOLLECT_BASE_QTY,
       ITEM_WMS.VOCOLLECT_BASE_ITEM AS VOCOLLECT_BASE_ITEM,
       CAST (COALESCE (ITEM_WMS.PICK_WT_TOL_AMNT_ERROR, 0) AS SMALLINT)
          AS PICK_WT_TOL_AMNT_ERROR,
       ITEM_WMS.PRICE_TKT_TYPE AS PRICE_TKT_TYPE,
       CAST (COALESCE (ITEM_WMS.MONETARY_VALUE, 0) AS DECIMAL (13, 2))
          AS MONETARY_VALUE,
       ITEM_WMS.MV_CURRENCY_CODE AS MV_CURRENCY_CODE,
       ITEM_WMS.MV_SIZE_UOM AS MV_SIZE_UOM,
       ITEM_WMS.CODE_DATE_PROMPT_METHOD_FLAG
          AS CODE_DATE_PROMPT_METHOD_FLAG,
       CAST (COALESCE (ITEM_WMS.MIN_RECV_TO_XPIRE_DAYS, 0) AS INTEGER)
          AS MIN_RECV_TO_XPIRE_DAYS,
       CAST (COALESCE (ITEM_WMS.MIN_PCNT_FOR_LPN_SPLIT, 0) AS SMALLINT)
          AS MIN_PCNT_FOR_LPN_SPLIT,
       CAST (
          COALESCE (ITEM_WMS.MIN_LPN_QTY_FOR_SPLIT, 0) AS DECIMAL (13, 5))
          AS MIN_LPN_QTY_FOR_SPLIT,
       ITEM_WMS.PROD_CATGRY AS PROD_CATGRY,
       ITEM_WMS.PRICE_TIX_AVAIL AS PRICE_TIX_AVAIL_FLAG,
       ITEM_CBO.Audit_Last_Updated_Source AS USER_ID,
       UN_NUMBER.UN_NUMBER_VALUE AS UN_NBR,
       --ITEM_CBO.HAZMAT_CODE AS HAZMAT_CODE,
       '' AS HAZMAT_ID,
       (SELECT PRODUCT_CLASS.PRODUCT_CLASS
          FROM PRODUCT_CLASS
         WHERE ITEM_CBO.PRODUCT_CLASS_ID = PRODUCT_CLASS.PRODUCT_CLASS_ID
               AND ITEM_CBO.COMPANY_ID = PRODUCT_CLASS.TC_COMPANY_ID)
          AS PROD_GROUP,
       (SELECT COMMODITY_CODE.DESCRIPTION_SHORT
          FROM COMMODITY_CODE
         WHERE ITEM_CBO.COMMODITY_CODE_ID =
                  COMMODITY_CODE.COMMODITY_CODE_ID
               AND ITEM_CBO.COMPANY_ID = COMMODITY_CODE.TC_COMPANY_ID)
          AS COMMODITY_CODE,
       (SELECT STANDARD_UOM.ABBREVIATION
          FROM STANDARD_UOM, SIZE_UOM
         WHERE     ITEM_CBO.DISPLAY_QTY_UOM_ID = SIZE_UOM.SIZE_UOM_ID
               AND ITEM_CBO.DISPLAY_QTY_UOM_ID = SIZE_UOM.SIZE_UOM_ID
               AND SIZE_UOM.STANDARD_UOM = STANDARD_UOM.STANDARD_UOM)
          AS DSP_QTY_UOM,
       (SELECT STANDARD_UOM.ABBREVIATION
          FROM STANDARD_UOM, SIZE_UOM
         WHERE     ITEM_CBO.DISPLAY_QTY_UOM_ID = SIZE_UOM.SIZE_UOM_ID
               AND ITEM_CBO.BASE_STORAGE_UOM_ID = SIZE_UOM.SIZE_UOM_ID
               AND SIZE_UOM.STANDARD_UOM = STANDARD_UOM.STANDARD_UOM)
          AS DB_QTY_UOM,
       CAST (COALESCE (CASE.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_CASE_QTY,
       CAST (COALESCE (CASE.STD_CASE_WT, 0) AS DECIMAL (16, 4))
          AS STD_CASE_WT,
       CAST (COALESCE (CASE.STD_CASE_VOL, 0) AS DECIMAL (16, 4))
          AS STD_CASE_VOL,
       CAST (COALESCE (CASE.STD_CASE_LEN, 0) AS DECIMAL (16, 4))
          AS STD_CASE_LEN,
       CAST (COALESCE (CASE.STD_CASE_WIDTH, 0) AS DECIMAL (16, 4))
          AS STD_CASE_WIDTH,
       CAST (COALESCE (CASE.STD_CASE_HT, 0) AS DECIMAL (16, 4))
          AS STD_CASE_HT,
       CAST (COALESCE (PACK.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_PACK_QTY,
       CAST (COALESCE (PACK.STD_PACK_WT, 0) AS DECIMAL (16, 4))
          AS STD_PACK_WT,
       CAST (COALESCE (PACK.STD_PACK_VOL, 0) AS DECIMAL (16, 4))
          AS STD_PACK_VOL,
       CAST (COALESCE (PACK.STD_PACK_LEN, 0) AS DECIMAL (16, 4))
          AS STD_PACK_LEN,
       CAST (COALESCE (PACK.STD_PACK_WIDTH, 0) AS DECIMAL (16, 4))
          AS STD_PACK_WIDTH,
       CAST (COALESCE (PACK.STD_PACK_HT, 0) AS DECIMAL (16, 4))
          AS STD_PACK_HT,
       CAST (COALESCE (SUB_PACK.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_QTY,
       CAST (COALESCE (SUB_PACK.STD_SUB_PACK_WT, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_WT,
       CAST (COALESCE (SUB_PACK.STD_SUB_PACK_VOL, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_VOL,
       CAST (COALESCE (SUB_PACK.STD_SUB_PACK_LEN, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_LEN,
       CAST (
          COALESCE (SUB_PACK.STD_SUB_PACK_WIDTH, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_WIDTH,
       CAST (COALESCE (SUB_PACK.STD_SUB_PACK_HT, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_HT,
       CAST (COALESCE (TIER.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_TIER_QTY,
       CAST (COALESCE (BUNDL.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_BUNDL_QTY,
       CAST (COALESCE (PLT.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_PLT_QTY,
       'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN' AS MARKS_NBRS,
       'N' AS NET_COST_FLAG,
       'N' AS PRODUCER_FLAG,
       'N' AS PREF_CRIT_FLAG,
       CAST (COALESCE (ITEM_WMS.SRL_NBR_REQD, 0) AS SMALLINT)
          AS SRL_NBR_REQD
  FROM ITEM_CBO
       LEFT OUTER JOIN ITEM_WMS
          ON ITEM_CBO.ITEM_ID = ITEM_WMS.ITEM_ID
       LEFT OUTER JOIN UN_NUMBER
          ON ITEM_CBO.UN_NUMBER_ID = UN_NUMBER.UN_NUMBER_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID,
                               ITEM_PACKAGE_CBO.WEIGHT AS STD_CASE_WT,
                               ITEM_PACKAGE_CBO.VOLUME AS STD_CASE_VOL,
                               ITEM_PACKAGE_CBO.LENGTH AS STD_CASE_LEN,
                               ITEM_PACKAGE_CBO.WIDTH AS STD_CASE_WIDTH,
                               ITEM_PACKAGE_CBO.HEIGHT AS STD_CASE_HT
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'C'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) CASE
          ON CASE.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID,
                               ITEM_PACKAGE_CBO.WEIGHT AS STD_PACK_WT,
                               ITEM_PACKAGE_CBO.VOLUME AS STD_PACK_VOL,
                               ITEM_PACKAGE_CBO.LENGTH AS STD_PACK_LEN,
                               ITEM_PACKAGE_CBO.WIDTH AS STD_PACK_WIDTH,
                               ITEM_PACKAGE_CBO.HEIGHT AS STD_PACK_HT
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'K'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) PACK
          ON PACK.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID,
                               ITEM_PACKAGE_CBO.WEIGHT AS STD_SUB_PACK_WT,
                               ITEM_PACKAGE_CBO.VOLUME
                                  AS STD_SUB_PACK_VOL,
                               ITEM_PACKAGE_CBO.LENGTH
                                  AS STD_SUB_PACK_LEN,
                               ITEM_PACKAGE_CBO.WIDTH
                                  AS STD_SUB_PACK_WIDTH,
                               ITEM_PACKAGE_CBO.HEIGHT AS STD_SUB_PACK_HT
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'S'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) SUB_PACK
          ON SUB_PACK.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'T'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) TIER
          ON TIER.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'B'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) BUNDL
          ON BUNDL.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'P'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) PLT
          ON PLT.ITEM_ID = ITEM_CBO.ITEM_ID);

CREATE OR REPLACE FORCE VIEW "ITEM_QUANTITY" ("UNIT_WT", "UNIT_VOL", "SKU_ID", "ITEM_ID", "CD_MASTER_ID", "STD_BUNDL_QTY", "STD_CASE_QTY", "STD_PLT_QTY", "STD_PACK_QTY", "STD_SUB_PACK_QTY", "STD_TIER_QTY", "STD_BUNDL_WT", "STD_CASE_WT", "STD_PLT_WT", "STD_PACK_WT", "STD_SUB_PACK_WT", "STD_TIER_WT", "STD_BUNDL_VOL", "STD_CASE_VOL", "STD_PLT_VOL", "STD_PACK_VOL", "STD_SUB_PACK_VOL", "STD_TIER_VOL", "STD_BUNDL_LEN", "STD_CASE_LEN", "STD_PLT_LEN", "STD_PACK_LEN", "STD_SUB_PACK_LEN", "STD_TIER_LEN", "STD_BUNDL_WIDTH", "STD_CASE_WIDTH", "STD_PLT_WIDTH", "STD_PACK_WIDTH", "STD_SUB_PACK_WIDTH", "STD_TIER_WIDTH", "STD_BUNDL_HT", "STD_CASE_HT", "STD_PLT_HT", "STD_PACK_HT", "STD_SUB_PACK_HT", "STD_TIER_HT", "DB_QTY_UOM", "DSP_QTY_UOM") AS
(select cast (nvl (item_cbo.unit_weight, 0) as number (16, 4)) as unit_wt,
       cast (nvl (item_cbo.unit_volume, 0) as number (16, 4)) as unit_vol,
       item_cbo.item_name as sku_id,
       item_cbo.item_id as item_id,
       item_cbo.company_id as cd_master_id,
       cast (nvl (bundl.quantity, 1) as number (16, 4)) as std_bundl_qty,
       cast (nvl (case.quantity, 1) as number (16, 4)) as std_case_qty,
       cast (nvl (plt.quantity, 1) as number (16, 4)) as std_plt_qty,
       cast (nvl (pack.quantity, 1) as number (16, 4)) as std_pack_qty,
       cast (nvl (sub_pack.quantity, 1) as number (16, 4))
          as std_sub_pack_qty,
       cast (nvl (tier.quantity, 1) as number (16, 4)) as std_tier_qty,
       cast (nvl (bundl.weight, 0) as number (16, 4)) as std_bundl_wt,
       cast (nvl (case.weight, 0) as number (16, 4)) as std_case_wt,
       cast (nvl (plt.weight, 0) as number (16, 4)) as std_plt_wt,
       cast (nvl (pack.weight, 0) as number (16, 4)) as std_pack_wt,
       cast (nvl (sub_pack.weight, 0) as number (16, 4))
          as std_sub_pack_wt,
       cast (nvl (tier.weight, 0) as number (16, 4)) as std_tier_wt,
       cast (nvl (bundl.volume, 0) as number (16, 4)) as std_bundl_vol,
       cast (nvl (case.volume, 0) as number (16, 4)) as std_case_vol,
       cast (nvl (plt.volume, 0) as number (16, 4)) as std_plt_vol,
       cast (nvl (pack.volume, 0) as number (16, 4)) as std_pack_vol,
       cast (nvl (sub_pack.volume, 0) as number (16, 4))
          as std_sub_pack_vol,
       cast (nvl (tier.volume, 0) as number (16, 4)) as std_tier_vol,
       cast (nvl (bundl.length, 0) as number (16, 4)) as std_bundl_len,
       cast (nvl (case.length, 0) as number (16, 4)) as std_case_len,
       cast (nvl (plt.length, 0) as number (16, 4)) as std_plt_len,
       cast (nvl (pack.length, 0) as number (16, 4)) as std_pack_len,
       cast (nvl (sub_pack.length, 0) as number (16, 4))
          as std_sub_pack_len,
       cast (nvl (tier.length, 0) as number (16, 4)) as std_tier_len,
       cast (nvl (bundl.width, 0) as number (16, 4)) as std_bundl_width,
       cast (nvl (case.width, 0) as number (16, 4)) as std_case_width,
       cast (nvl (plt.width, 0) as number (16, 4)) as std_plt_width,
       cast (nvl (pack.width, 0) as number (16, 4)) as std_pack_width,
       cast (nvl (sub_pack.width, 0) as number (16, 4))
          as std_sub_pack_width,
       cast (nvl (tier.width, 0) as number (16, 4)) as std_tier_width,
       cast (nvl (bundl.height, 0) as number (16, 4)) as std_bundl_ht,
       cast (nvl (case.height, 0) as number (16, 4)) as std_case_ht,
       cast (nvl (plt.height, 0) as number (16, 4)) as std_plt_ht,
       cast (nvl (pack.height, 0) as number (16, 4)) as std_pack_ht,
       cast (nvl (sub_pack.height, 0) as number (16, 4))
          as std_sub_pack_ht,
       cast (nvl (tier.height, 0) as number (16, 4)) as std_tier_ht,
       (select standard_uom.abbreviation
          from standard_uom, size_uom
         where     item_cbo.display_qty_uom_id = size_uom.size_uom_id
               and item_cbo.base_storage_uom_id = size_uom.size_uom_id
               and size_uom.standard_uom = standard_uom.standard_uom)
          as db_qty_uom,
       (select standard_uom.abbreviation
          from standard_uom, size_uom
         where     item_cbo.display_qty_uom_id = size_uom.size_uom_id
               and item_cbo.display_qty_uom_id = size_uom.size_uom_id
               and size_uom.standard_uom = standard_uom.standard_uom)
          as dsp_qty_uom
  from item_cbo
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'C'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) case
          on case.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'K'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) pack
          on pack.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'S'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) sub_pack
          on sub_pack.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'B'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) bundl
          on bundl.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'P'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) plt
          on plt.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               -item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'T'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) tier
          on tier.item_id = item_cbo.item_id);

CREATE OR REPLACE FORCE VIEW "LANE_DETAIL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_DTL_SEQ", "IS_RATING", "IS_ROUTING", "IS_SAILING", "CARRIER_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "RANK", "EQUIPMENT_ID", "TIER_ID", "REP_TP_FLAG", "RLD_RATE_SEQ", "RATE", "CURRENCY_CODE", "RATE_CALC_METHOD", "RATE_UOM", "MINIMUM_RATE", "IS_BUDGETED", "IS_PREFERRED", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "COMMODITY_CODE_ID", "FIXED_TRANSIT_STANDARD_UOM", "FIXED_TRANSIT_VALUE", "SAILING_FREQUENCY_TYPE", "SAILING_SCHEDULE_NAME", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5", "TT_TOLERANCE_FACTOR", "PACKAGE_ID", "PACKAGE_NAME", "SCNDR_CARRIER_ID", "MOT_ID", "PROTECTION_LEVEL_ID", "SERVICE_LEVEL_ID", "SIZE_UOM_ID", "VOYAGE", "HAS_SHIPPING_PARAM", "CUBING_INDICATOR", "OVERRIDE_CODE", "CARRIER_REJECT_PERIOD", "PREFERENCE_BONUS_VALUE", "REJECT_FURTHER_SHIPMENTS", "LANE_DTL_STATUS") AS
SELECT comb_lane_dtl.TC_COMPANY_ID,
comb_lane_dtl.lane_id,
comb_lane_dtl.lane_dtl_seq,
comb_lane_dtl.is_rating,
comb_lane_dtl.is_routing,
comb_lane_dtl.is_sailing,
comb_lane_dtl.carrier_id,
comb_lane_dtl.effective_dt,
comb_lane_dtl.expiration_dt,
comb_lane_dtl.RANK,
comb_lane_dtl.EQUIPMENT_ID,
comb_lane_dtl.tier_id,
comb_lane_dtl.rep_tp_flag,
rlr.rld_rate_seq,
rlr.rate,
rlr.currency_code,
rlr.rate_calc_method,
rlr.rate_uom,
rlr.minimum_rate,
comb_lane_dtl.IS_BUDGETED,
comb_lane_dtl.IS_PREFERRED,
comb_lane_dtl.LAST_UPDATED_SOURCE_TYPE,
comb_lane_dtl.LAST_UPDATED_SOURCE,
comb_lane_dtl.LAST_UPDATED_DTTM,
comb_lane_dtl.COMMODITY_CODE_ID,
comb_lane_dtl.FIXED_TRANSIT_STANDARD_UOM,
comb_lane_dtl.FIXED_TRANSIT_VALUE,
comb_lane_dtl.SAILING_FREQUENCY_TYPE,
comb_lane_dtl.SAILING_SCHEDULE_NAME,
comb_lane_dtl.O_SHIP_VIA,
comb_lane_dtl.D_SHIP_VIA,
comb_lane_dtl.CONTRACT_NUMBER,
comb_lane_dtl.CUSTOM_TEXT1,
comb_lane_dtl.CUSTOM_TEXT2,
comb_lane_dtl.CUSTOM_TEXT3,
comb_lane_dtl.CUSTOM_TEXT4,
comb_lane_dtl.CUSTOM_TEXT5,
comb_lane_dtl.TT_TOLERANCE_FACTOR,
comb_lane_dtl.PACKAGE_ID,
comb_lane_dtl.PACKAGE_NAME,
comb_lane_dtl.SCNDR_CARRIER_ID,
comb_lane_dtl.MOT_ID,
comb_lane_dtl.PROTECTION_LEVEL_ID,
comb_lane_dtl.SERVICE_LEVEL_ID,
comb_lane_dtl.SIZE_UOM_ID,
comb_lane_dtl.VOYAGE,
comb_lane_dtl.HAS_SHIPPING_PARAM,
comb_lane_dtl.cubing_indicator,
comb_lane_dtl.override_code,
comb_lane_dtl.carrier_reject_period,
comb_lane_dtl.preference_bonus_value,
comb_lane_dtl.reject_further_shipments,
comb_lane_dtl.LANE_DTL_STATUS
FROM comb_lane_dtl
LEFT JOIN
(SELECT *
FROM rating_lane_dtl_rate inner_rlr
WHERE inner_rlr.rld_rate_seq =
  (SELECT MIN (NVL (rld_rate_seq, -1))
  FROM rating_lane_dtl_rate
  WHERE lane_id           = inner_rlr.lane_id
  AND rating_lane_dtl_seq = inner_rlr.rating_lane_dtl_seq
  AND tc_company_id       = inner_rlr.tc_company_id
  )
) rlr
ON comb_lane_dtl.lane_dtl_seq = rlr.rating_lane_dtl_seq
AND comb_lane_dtl.LANE_ID     = rlr.LANE_ID
UNION ALL
SELECT im_rating_dtl.TC_COMPANY_ID,
IRL.RATING_LANE_ID,
im_rating_dtl.RATING_LANE_DTL_SEQ,
1 AS IS_RATING,
0 AS IS_ROUTING,
0 AS IS_SAILING,
im_rating_dtl.carrier_id,
im_rating_dtl.effective_dt,
im_rating_dtl.expiration_dt,
NULL AS RANK,
im_rating_dtl.equipment_id,
NULL AS TIER_ID,
NULL AS rep_tp_flag,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
im_rating_dtl.IS_BUDGETED,
NULL AS IS_PREFERRED,
im_rating_dtl.LAST_UPDATED_SOURCE_TYPE,
im_rating_dtl.LAST_UPDATED_SOURCE,
im_rating_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
NULL AS FIXED_TRANSIT_STANDARD_UOM,
NULL AS FIXED_TRANSIT_VALUE,
NULL AS SAILING_FREQUENCY_TYPE,
NULL AS SAILING_SCHEDULE_NAME,
im_rating_dtl.O_SHIP_VIA,
im_rating_dtl.D_SHIP_VIA,
im_rating_dtl.CONTRACT_NUMBER,
im_rating_dtl.CUSTOM_TEXT1,
im_rating_dtl.CUSTOM_TEXT2,
im_rating_dtl.CUSTOM_TEXT3,
im_rating_dtl.CUSTOM_TEXT4,
im_rating_dtl.CUSTOM_TEXT5,
NULL AS TT_TOLERANCE_FACTOR,
im_rating_dtl.PACKAGE_ID,
im_rating_dtl.PACKAGE_NAME,
im_rating_dtl.SCNDR_CARRIER_ID,
im_rating_dtl.MOT_ID,
im_rating_dtl.PROTECTION_LEVEL_ID,
im_rating_dtl.SERVICE_LEVEL_ID,
NULL AS SIZE_UOM_ID,
im_rating_dtl.VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
NULL AS CUBING_INDICATOR,
NULL AS OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
NULL AS PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
im_rating_dtl.RATING_LANE_DTL_STATUS
FROM IMPORT_RATING_LANE irl
JOIN IMPORT_RATING_LANE_DTL im_rating_dtl
ON irl.LANE_ID = im_rating_dtl.LANE_ID
JOIN COMB_LANE Cl
ON Cl.LANE_ID = irl.RATING_LANE_ID
UNION ALL
SELECT im_rating_dtl.TC_COMPANY_ID,
IRL.RATING_LANE_ID,
im_rating_dtl.RATING_LANE_DTL_SEQ,
1 AS IS_RATING,
0 AS IS_ROUTING,
0 AS IS_SAILING,
im_rating_dtl.carrier_id,
im_rating_dtl.effective_dt,
im_rating_dtl.expiration_dt,
NULL AS RANK,
im_rating_dtl.equipment_id,
NULL AS TIER_ID,
NULL AS rep_tp_flag,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
im_rating_dtl.IS_BUDGETED,
NULL AS IS_PREFERRED,
im_rating_dtl.LAST_UPDATED_SOURCE_TYPE,
im_rating_dtl.LAST_UPDATED_SOURCE,
im_rating_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
NULL AS FIXED_TRANSIT_STANDARD_UOM,
NULL AS FIXED_TRANSIT_VALUE,
NULL AS SAILING_FREQUENCY_TYPE,
NULL AS SAILING_SCHEDULE_NAME,
im_rating_dtl.O_SHIP_VIA,
im_rating_dtl.D_SHIP_VIA,
im_rating_dtl.CONTRACT_NUMBER,
im_rating_dtl.CUSTOM_TEXT1,
im_rating_dtl.CUSTOM_TEXT2,
im_rating_dtl.CUSTOM_TEXT3,
im_rating_dtl.CUSTOM_TEXT4,
im_rating_dtl.CUSTOM_TEXT5,
NULL AS TT_TOLERANCE_FACTOR,
im_rating_dtl.PACKAGE_ID,
im_rating_dtl.PACKAGE_NAME,
im_rating_dtl.SCNDR_CARRIER_ID,
im_rating_dtl.MOT_ID,
im_rating_dtl.PROTECTION_LEVEL_ID,
im_rating_dtl.SERVICE_LEVEL_ID,
NULL AS SIZE_UOM_ID,
im_rating_dtl.VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
NULL AS CUBING_INDICATOR,
NULL AS OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
NULL AS PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
im_rating_dtl.RATING_LANE_DTL_STATUS
FROM IMPORT_RATING_LANE irl
JOIN IMPORT_RATING_LANE_DTL im_rating_dtl
ON irl.LANE_ID        = im_rating_dtl.LANE_ID
WHERE IRL.LANE_STATUS = 4
UNION ALL
SELECT import_rg_dtl.TC_COMPANY_ID,
irgl.RG_LANE_ID,
import_rg_dtl.RG_LANE_DTL_SEQ,
0 AS IS_RATING,
1 AS IS_ROUTING,
0 AS IS_SAILING,
import_rg_dtl.CARRIER_ID,
import_rg_dtl.EFFECTIVE_DT,
import_rg_dtl.EXPIRATION_DT,
import_rg_dtl.RANK,
import_rg_dtl.EQUIPMENT_ID,
import_rg_dtl.TIER_ID,
import_rg_dtl.REP_TP_FLAG,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
NULL AS IS_BUDGETED,
import_rg_dtl.IS_PREFERRED,
import_rg_dtl.LAST_UPDATED_SOURCE_TYPE,
import_rg_dtl.LAST_UPDATED_SOURCE,
import_rg_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
NULL AS FIXED_TRANSIT_STANDARD_UOM,
NULL AS FIXED_TRANSIT_VALUE,
NULL AS SAILING_FREQUENCY_TYPE,
NULL AS SAILING_SCHEDULE_NAME,
NULL AS O_SHIP_VIA,
NULL AS D_SHIP_VIA,
NULL AS CONTRACT_NUMBER,
NULL AS CUSTOM_TEXT1,
NULL AS CUSTOM_TEXT2,
NULL AS CUSTOM_TEXT3,
NULL AS CUSTOM_TEXT4,
NULL AS CUSTOM_TEXT5,
import_rg_dtl.TT_TOLERANCE_FACTOR,
NULL AS PACKAGE_ID,
NULL AS PACKAGE_NAME,
import_rg_dtl.SCNDR_CARRIER_ID,
import_rg_dtl.MOT_ID,
import_rg_dtl.PROTECTION_LEVEL_ID,
import_rg_dtl.SERVICE_LEVEL_ID,
import_rg_dtl.SIZE_UOM_ID,
import_rg_dtl.VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
import_rg_dtl.CUBING_INDICATOR,
import_rg_dtl.OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
import_rg_dtl.PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
import_rg_dtl.LANE_DTL_STATUS
FROM import_rg_lane irgl
JOIN import_rg_lane_dtl import_rg_dtl
ON irgl.LANE_ID = import_rg_dtl.LANE_ID
JOIN COMB_LANE Cl
ON Cl.LANE_ID = irgl.RG_LANE_ID
UNION ALL
SELECT import_rg_dtl.TC_COMPANY_ID,
irgl.RG_LANE_ID,
import_rg_dtl.RG_LANE_DTL_SEQ,
0 AS IS_RATING,
1 AS IS_ROUTING,
0 AS IS_SAILING,
import_rg_dtl.CARRIER_ID,
import_rg_dtl.EFFECTIVE_DT,
import_rg_dtl.EXPIRATION_DT,
import_rg_dtl.RANK,
import_rg_dtl.EQUIPMENT_ID,
import_rg_dtl.TIER_ID,
import_rg_dtl.REP_TP_FLAG,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
NULL AS IS_BUDGETED,
import_rg_dtl.IS_PREFERRED,
import_rg_dtl.LAST_UPDATED_SOURCE_TYPE,
import_rg_dtl.LAST_UPDATED_SOURCE,
import_rg_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
NULL AS FIXED_TRANSIT_STANDARD_UOM,
NULL AS FIXED_TRANSIT_VALUE,
NULL AS SAILING_FREQUENCY_TYPE,
NULL AS SAILING_SCHEDULE_NAME,
NULL AS O_SHIP_VIA,
NULL AS D_SHIP_VIA,
NULL AS CONTRACT_NUMBER,
NULL AS CUSTOM_TEXT1,
NULL AS CUSTOM_TEXT2,
NULL AS CUSTOM_TEXT3,
NULL AS CUSTOM_TEXT4,
NULL AS CUSTOM_TEXT5,
import_rg_dtl.TT_TOLERANCE_FACTOR,
NULL AS PACKAGE_ID,
NULL AS PACKAGE_NAME,
import_rg_dtl.SCNDR_CARRIER_ID,
import_rg_dtl.MOT_ID,
import_rg_dtl.PROTECTION_LEVEL_ID,
import_rg_dtl.SERVICE_LEVEL_ID,
import_rg_dtl.SIZE_UOM_ID,
import_rg_dtl.VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
import_rg_dtl.CUBING_INDICATOR,
import_rg_dtl.OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
import_rg_dtl.PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
import_rg_dtl.LANE_DTL_STATUS
FROM import_rg_lane irgl
JOIN import_rg_lane_dtl import_rg_dtl
ON irgl.LANE_ID        = import_rg_dtl.LANE_ID
WHERE irgl.LANE_STATUS = 4
UNION ALL
SELECT im_sailing_dtl.TC_COMPANY_ID,
isl.SAILING_LANE_ID,
im_sailing_dtl.SAILING_LANE_DTL_SEQ,
0 AS IS_RATING,
0 AS IS_ROUTING,
1 AS IS_SAILING,
im_sailing_dtl.CARRIER_ID,
im_sailing_dtl.EFFECTIVE_DT,
im_sailing_dtl.EXPIRATION_DT,
NULL AS RANK,
NULL AS EQUIPMENT_ID,
NULL AS TIER_ID,
NULL AS REP_TP_FLAG,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
NULL AS IS_BUDGETED,
NULL AS IS_PREFERRED,
im_sailing_dtl.LAST_UPDATED_SOURCE_TYPE,
im_sailing_dtl.LAST_UPDATED_SOURCE,
im_sailing_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
im_sailing_dtl.FIXED_TRANSIT_STANDARD_UOM,
im_sailing_dtl.FIXED_TRANSIT_VALUE,
im_sailing_dtl.SAILING_FREQUENCY_TYPE,
im_sailing_dtl.SAILING_SCHEDULE_NAME,
NULL AS O_SHIP_VIA,
NULL AS D_SHIP_VIA,
NULL AS CONTRACT_NUMBER,
NULL AS CUSTOM_TEXT1,
NULL AS CUSTOM_TEXT2,
NULL AS CUSTOM_TEXT3,
NULL AS CUSTOM_TEXT4,
NULL AS CUSTOM_TEXT5,
NULL AS TT_TOLERANCE_FACTOR,
NULL AS PACKAGE_ID,
NULL AS PACKAGE_NAME,
im_sailing_dtl.SCNDR_CARRIER_ID,
im_sailing_dtl.MOT_ID,
NULL AS PROTECTION_LEVEL_ID,
im_sailing_dtl.SERVICE_LEVEL_ID,
NULL AS SIZE_UOM_ID,
NULL AS VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
NULL AS CUBING_INDICATOR,
NULL AS OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
NULL AS PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
im_sailing_dtl.LANE_DTL_STATUS
FROM import_sailing_lane isl
JOIN import_sailing_lane_dtl im_sailing_dtl
ON isl.LANE_ID = im_sailing_dtl.LANE_ID
JOIN COMB_LANE Cl
ON Cl.LANE_ID = isl.SAILING_LANE_ID
UNION ALL
SELECT im_sailing_dtl.TC_COMPANY_ID,
isl.SAILING_LANE_ID,
im_sailing_dtl.SAILING_LANE_DTL_SEQ,
0 AS IS_RATING,
0 AS IS_ROUTING,
1 AS IS_SAILING,
im_sailing_dtl.CARRIER_ID,
im_sailing_dtl.EFFECTIVE_DT,
im_sailing_dtl.EXPIRATION_DT,
NULL AS RANK,
NULL AS EQUIPMENT_ID,
NULL AS TIER_ID,
NULL AS REP_TP_FLAG,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
NULL AS IS_BUDGETED,
NULL AS IS_PREFERRED,
im_sailing_dtl.LAST_UPDATED_SOURCE_TYPE,
im_sailing_dtl.LAST_UPDATED_SOURCE,
im_sailing_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
im_sailing_dtl.FIXED_TRANSIT_STANDARD_UOM,
im_sailing_dtl.FIXED_TRANSIT_VALUE,
im_sailing_dtl.SAILING_FREQUENCY_TYPE,
im_sailing_dtl.SAILING_SCHEDULE_NAME,
NULL AS O_SHIP_VIA,
NULL AS D_SHIP_VIA,
NULL AS CONTRACT_NUMBER,
NULL AS CUSTOM_TEXT1,
NULL AS CUSTOM_TEXT2,
NULL AS CUSTOM_TEXT3,
NULL AS CUSTOM_TEXT4,
NULL AS CUSTOM_TEXT5,
NULL AS TT_TOLERANCE_FACTOR,
NULL AS PACKAGE_ID,
NULL AS PACKAGE_NAME,
im_sailing_dtl.SCNDR_CARRIER_ID,
im_sailing_dtl.MOT_ID,
NULL AS PROTECTION_LEVEL_ID,
im_sailing_dtl.SERVICE_LEVEL_ID,
NULL AS SIZE_UOM_ID,
NULL AS VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
NULL AS CUBING_INDICATOR,
NULL AS OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
NULL AS PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
im_sailing_dtl.LANE_DTL_STATUS
FROM import_sailing_lane isl
JOIN import_sailing_lane_dtl im_sailing_dtl
ON isl.LANE_ID        = im_sailing_dtl.LANE_ID
WHERE isl.LANE_STATUS = 4;

CREATE OR REPLACE FORCE VIEW "LANE_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "RG_QUALIFIER", "FREQUENCY", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_NAME", "CUSTOMER_ID", "BILLING_METHOD", "INCOTERM_ID", "ROUTE_TO", "ROUTE_TYPE_1", "ROUTE_TYPE_2", "NO_RATING", "USE_FASTEST", "USE_PREFERENCE_BONUS", "IS_ROUTING", "IS_RATING", "IS_SAILING") AS
SELECT l.tc_company_id,
l.lane_id,
l.lane_hierarchy,
l.lane_status,
l.o_loc_type,
l.o_facility_id,
l.o_facility_alias_id,
UPPER (l.o_city),
l.o_state_prov,
UPPER (l.o_county),
l.o_postal_code,
l.o_country_code,
l.o_zone_id,
'',
l.d_loc_type,
l.d_facility_id,
l.d_facility_alias_id,
UPPER (l.d_city),
l.d_state_prov,
UPPER (l.d_county),
l.d_postal_code,
l.d_country_code,
l.d_zone_id,
'',
l.rg_qualifier,
l.frequency,
DECODE (l.lane_status, 4, 1, 0) has_errors,
COALESCE (
CASE
  WHEN IS_ROUTING = 1
  THEN
    (SELECT SIGN (MAX (DECODE (ID.lane_dtl_status, 4, 1, 0)))
    FROM import_rg_lane ir,
      import_rg_lane_dtl ID
    WHERE ( l.lane_id    = ir.rg_lane_id
    AND l.tc_company_id  = ir.tc_company_id
    AND ir.lane_id       = ID.lane_id
    AND ir.tc_company_id = ID.tc_company_id)
    )
  WHEN IS_RATING = 1
  THEN
    (SELECT SIGN ( MAX ( DECODE (id.rating_lane_dtl_status, 4, 1, 0)))
    FROM import_rating_lane ir,
      import_rating_lane_dtl id
    WHERE ( l.lane_id    = ir.rating_lane_id
    AND l.tc_company_id  = ir.tc_company_id
    AND ir.lane_id       = id.lane_id
    AND ir.tc_company_id = id.tc_company_id)
    )
  WHEN IS_SAILING = 1
  THEN
    (SELECT SIGN ( MAX ( DECODE (CAST (SID.LANE_DTL_STATUS AS INT), 4, 1, 0)))
    FROM IMPORT_SAILING_LANE IR,
      IMPORT_SAILING_LANE_DTL SID
    WHERE ( L.LANE_ID    = IR.SAILING_LANE_ID
    AND L.TC_COMPANY_ID  = IR.TC_COMPANY_ID
    AND IR.LANE_ID       = SID.LANE_ID
    AND IR.TC_COMPANY_ID = SID.TC_COMPANY_ID)
    )
END, 0) dtl_has_errors,
l.created_source_type,
l.created_source,
l.created_dttm,
l.last_updated_source_type,
l.last_updated_source,
l.last_updated_dttm,
l.lane_name,
l.customer_id,
l.billing_method,
l.incoterm_id,
l.route_to,
l.route_type_1,
l.route_type_2,
l.no_rating,
l.use_fastest,
l.use_preference_bonus,
IS_ROUTING,
is_rating,
IS_SAILING
FROM comb_lane l
WHERE (l.IS_ROUTING = 1
OR l.is_rating      = 1
OR l.IS_SAILING     = 1)
UNION ALL
SELECT il.tc_company_id,
il.rg_lane_id,
il.lane_hierarchy,
il.lane_status,
il.o_loc_type,
il.o_facility_id,
il.o_facility_alias_id,
UPPER (il.o_city),
il.o_state_prov,
UPPER (il.o_county),
il.o_postal_code,
il.o_country_code,
il.o_zone_id,
il.o_zone_name,
il.d_loc_type,
il.d_facility_id,
il.d_facility_alias_id,
UPPER (il.d_city),
il.d_state_prov,
UPPER (il.d_county),
il.d_postal_code,
il.d_country_code,
il.d_zone_id,
il.d_zone_name,
il.rg_qualifier,
il.frequency,
DECODE (il.lane_status, 4, 1, 0) has_errors,
COALESCE (
(SELECT MAX (DECODE (d.lane_dtl_status, 4, 1, 0))
FROM import_rg_lane_dtl d
WHERE d.lane_id     = il.lane_id
AND d.tc_company_id = il.tc_company_id
), 0) dtl_has_errors,
il.created_source_type,
il.created_source,
il.created_dttm,
il.last_updated_source_type,
il.last_updated_source,
il.last_updated_dttm,
il.lane_name,
il.customer_id,
il.billing_method,
il.incoterm_id,
il.route_to,
il.route_type_1,
il.route_type_2,
il.no_rating,
il.use_fastest,
il.use_preference_bonus,
1,
0,
0
FROM import_rg_lane il
WHERE il.lane_status = 4
AND NOT EXISTS
(SELECT 1
FROM comb_lane cl
WHERE il.tc_company_id = cl.tc_company_id
AND il.rg_lane_id      = cl.lane_id
AND cl.is_routing      = 1
)
UNION ALL
SELECT I.TC_COMPANY_ID,
I.SAILING_LANE_ID,
I.LANE_HIERARCHY,
I.LANE_STATUS,
I.O_LOC_TYPE,
I.O_FACILITY_ID,
I.O_FACILITY_ALIAS_ID,
UPPER (I.O_CITY),
I.O_STATE_PROV,
UPPER (I.O_COUNTY),
I.O_POSTAL_CODE,
I.O_COUNTRY_CODE,
I.O_ZONE_ID,
I.O_ZONE_NAME,
I.D_LOC_TYPE,
I.D_FACILITY_ID,
I.D_FACILITY_ALIAS_ID,
UPPER (I.D_CITY),
I.D_STATE_PROV,
UPPER (I.D_COUNTY),
I.D_POSTAL_CODE,
I.D_COUNTRY_CODE,
I.D_ZONE_ID,
I.D_ZONE_NAME,
'',
'',
DECODE (CAST (I.LANE_STATUS AS INT), 4, 1, 0) HAS_ERRORS,
COALESCE (
(SELECT MAX (DECODE (CAST (D.LANE_DTL_STATUS AS INT), 4, 1, 0))
FROM IMPORT_SAILING_LANE_DTL D
WHERE D.LANE_ID     = I.LANE_ID
AND D.TC_COMPANY_ID = I.TC_COMPANY_ID
), 0) DTL_HAS_ERRORS,
I.CREATED_SOURCE_TYPE,
I.CREATED_SOURCE,
I.CREATED_DTTM,
I.LAST_UPDATED_SOURCE_TYPE,
I.LAST_UPDATED_SOURCE,
I.LAST_UPDATED_DTTM,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
0, --routing
0, --rating
1  --sailing
FROM IMPORT_SAILING_LANE I
WHERE I.LANE_STATUS = 4
AND NOT EXISTS
(SELECT 1
FROM COMB_LANE CL
WHERE I.TC_COMPANY_ID = CL.TC_COMPANY_ID
AND I.SAILING_LANE_ID = CL.LANE_ID
AND CL.is_SAILING     = 1
)
UNION ALL
SELECT i.tc_company_id,
i.rating_lane_id,
i.lane_hierarchy,
i.lane_status,
i.o_loc_type,
i.o_facility_id,
i.o_facility_alias_id,
UPPER (i.o_city),
i.o_state_prov,
UPPER (i.o_county),
i.o_postal_code,
i.o_country_code,
I.O_ZONE_ID,
I.O_ZONE_NAME,
i.d_loc_type,
i.d_facility_id,
i.d_facility_alias_id,
UPPER (i.d_city),
i.d_state_prov,
UPPER (i.d_county),
i.d_postal_code,
i.d_country_code,
I.D_ZONE_ID,
I.D_ZONE_NAME,
'',
'',
DECODE (i.lane_status, 4, 1, 0) has_errors,
COALESCE (
(SELECT MAX (DECODE (d.rating_lane_dtl_status, 4, 1, 0))
FROM import_rating_lane_dtl d
WHERE d.lane_id     = i.lane_id
AND d.tc_company_id = i.tc_company_id
), 0) dtl_has_errors,
i.created_source_type,
i.created_source,
i.created_dttm,
i.last_updated_source_type,
i.last_updated_source,
i.last_updated_dttm,
'',
CAST (NULL AS INT),
CAST (NULL AS INT),
CAST (NULL AS INT),
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
0, --routing
1, --rating
0  --sailing
FROM import_rating_lane i
WHERE i.lane_status = 4
AND NOT EXISTS
(SELECT 1
FROM COMB_LANE CL
WHERE i.tc_company_id = CL.tc_company_id
AND i.rating_lane_id  = CL.lane_id
AND CL.IS_RATING      = 1
);

CREATE OR REPLACE FORCE VIEW "LATEST_EMP_DTL" ("ROW_NBR", "EMP_ID", "EFF_DATE_TIME", "EMP_STAT_ID", "PAY_RATE", "PAY_SCALE_ID", "SPVSR_EMP_ID", "DEPT_ID", "SHIFT_ID", "ROLE_ID", "USER_DEF_FIELD_1", "USER_DEF_FIELD_2", "CMNT", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "WHSE", "JOB_FUNC_ID", "STARTUP_TIME", "CLEANUP_TIME", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2", "DFLT_PERF_GOAL", "IS_SUPER") AS
SELECT ROWNUM as ROW_NBR ,"EMP_ID",
  "EFF_DATE_TIME",
  "EMP_STAT_ID",
  "PAY_RATE",
  "PAY_SCALE_ID",
  "SPVSR_EMP_ID",
  "DEPT_ID",
  "SHIFT_ID",
  "ROLE_ID",
  "USER_DEF_FIELD_1",
  "USER_DEF_FIELD_2",
  "CMNT",
  "CREATE_DATE_TIME",
  "MOD_DATE_TIME",
  "USER_ID",
  "WHSE",
  "JOB_FUNC_ID",
  "STARTUP_TIME",
  "CLEANUP_TIME",
  "MISC_TXT_1",
  "MISC_TXT_2",
  "MISC_NUM_1",
  "MISC_NUM_2",
  "DFLT_PERF_GOAL",
  "IS_SUPER"
FROM E_EMP_DTL E1
WHERE (EFF_DATE_TIME =
  (SELECT MAX(EFF_DATE_TIME)
  FROM E_EMP_DTL E2
  WHERE EFF_DATE_TIME <= SYSDATE
  AND E1.EMP_ID        = E2.EMP_ID
  AND E1.WHSE          = E2.WHSE
  GROUP BY EMP_ID
  ));

CREATE OR REPLACE FORCE VIEW "LOCATION_VIEW" ("PARENT_ID", "STATUS", "LOCN_CLASS", "MAX_CAPACITY", "LOCN_ID", "IS_GUARD_HOUSE", "CURRENT_CAPACITY", "ID", "FACILITY_ID", "YARD_ID", "LOCATION_NAME", "PARENT_LOCATION_NAME", "LOCN_BRCD", "DSP_LOCN", "CHECK_DIGIT", "TC_COMPANY_ID", "YARD_NAME") AS
(select parent_id,
         status,
         locn_class,
         max_capacity,
         locn_id,
         is_guard_house,
         current_capacity,
         id,
         facility_id,
         yard_id,
         location_name,
         parent_location_name,
         locn_brcd,
         dsp_locn,
         check_digit,
         tc_company_id,
         yard_name
    from dock_door_view
  union all
  select parent_id,
         status,
         locn_class,
         max_capacity,
         locn_id,
         is_guard_house,
         current_capacity,
         id,
         facility_id,
         yard_id,
         location_name,
         parent_location_name,
         locn_brcd,
         dsp_locn,
         check_digit,
         tc_company_id,
         yard_name
    from yard_slot_view);

CREATE OR REPLACE FORCE VIEW "LPN_DWNLOAD" ("INB_TC_LPN_ID", "INB_LPN_ID", "INB_C_FACILITY_ID", "INB_INBOUND_OUTBOUND_INDICATOR", "INB_CURR_SUB_LOCN_ID", "OBN_TC_LPN_ID", "OBN_LPN_ID", "OBN_INBOUND_OUTBOUND_INDICATOR", "ASN_TC_ASN_ID", "ASN_ASN_ID", "ADT_ASN_ID", "ADT_STD_PACK_QTY", "ADT_SKU_ID", "ADT_DISPOSITION_TYPE", "ADT_PRE_RECEIPT_STATUS", "ADT_SHIPPED_QTY", "ASN_TC_SHIPMENT_ID", "ITEM_NAME", "ITEM_DESCRIPTION", "DTL_QUANTITY", "PACK_QTY", "IW_STORE_DEPT", "ORDERS_D_FACILITY_ALIAS_ID", "ORDERS_ORDER_CONSOL_LOCN_ID", "ASN_DEST_FACILITY_ALIAS_ID", "CONSOL_LOCN_PKT_CONSOL_ATTR", "ORDERS_PRIORITY", "ASN_ORIGIN_FACILITY_ALIAS_ID", "ASN_BUSINESS_PARTNER_ID", "ASN_ASN_TYPE", "ASN_ASN_STATUS", "ASN_ORIGIN_FACILITY_ID", "ASN_DESTINATION_FACILITY_ID", "ASN_APPOINTMENT_ID", "ASN_TOTAL_SHIPPED_QTY", "ASN_TOTAL_RECEIVED_QTY", "ASN_ASSIGNED_CARRIER_CODE", "ASN_BILL_OF_LADING_NUMBER", "ASN_PRO_NUMBER", "ASN_TOTAL_VOLUME", "ASN_INBOUND_REGION_ID", "ASN_OUTBOUND_REGION_ID", "ASN_REGION_ID", "ASN_ASN_LEVEL", "ASN_HAS_IMPORT_ERROR", "ASN_HAS_SOFT_CHECK_ERROR", "ASN_HAS_ALERTS", "ASN_SYSTEM_ALLOCATED", "ASN_ASN_PRIORITY", "ASN_IS_CANCELLED", "ASN_IS_CLOSED", "ASN_MANIF_NBR", "ASN_WORK_ORD_NBR", "ASN_SHIPPED_LPN_COUNT", "ASN_RECEIVED_LPN_COUNT", "ASN_INITIATE_FLAG", "ASN_SHIPMENT_ID", "ASN_ALLOCATION_COMPLETED", "ASN_ASN_ORGN_TYPE", "ASN_MHE_SENT", "ASN_FLOW_THR_ALLOC_METHOD", "ASN_TRAILER_NUMBER", "ASN_DESTINATION_TYPE", "ASN_ORIGINAL_ASN_ID", "ASN_PRE_RECEIPT_STATUS", "ASN_PRE_ALLOC_FIT_PERCENTAGE", "ADT_ASN_DETAIL_ID", "ADT_TC_PURCHASE_ORDERS_ID", "ADT_PURCHASE_ORDERS_ID", "ADT_BUSINESS_PARTNER_ID", "ADT_SKU_ATTR_1", "ADT_SKU_ATTR_2", "ADT_SKU_ATTR_3", "ADT_SKU_ATTR_4", "ADT_SKU_ATTR_5", "ADT_STD_CASE_QTY", "ADT_RECEIVED_QTY", "ADT_STD_SUB_PACK_QTY", "ADT_LPN_PER_TIER", "ADT_IS_CANCELLED", "ADT_IS_CLOSED", "ADT_INVN_TYPE", "ADT_PROD_STAT", "ADT_BATCH_NBR", "ADT_CNTRY_OF_ORGN", "ADT_SHIPPED_LPN_COUNT", "ADT_RECEIVED_LPN_COUNT", "ADT_UNITS_ASSIGNED_TO_LPN", 
"ADT_TC_COMPANY_ID", "ADT_TC_PO_LINE_ID", "ADT_TC_ORDER_ID", "ADT_ORDER_ID", "ADT_TC_ORDER_LINE_ID", "ADT_ORDER_LINE_ITEM_ID", "INB_BUSINESS_PARTNER_ID", "OBN_BUSINESS_PARTNER_ID", "INB_TC_COMPANY_ID", "INB_PARENT_LPN_ID", "INB_TC_PARENT_LPN_ID", "INB_LPN_TYPE", "OBN_TC_COMPANY_ID", "OBN_PARENT_LPN_ID", "OBN_TC_PARENT_LPN_ID", "OBN_LPN_TYPE", "INB_LPN_FACILITY_STATUS", "INB_TC_REFERENCE_LPN_ID", "INB_TC_ORDER_ID", "INB_ORIG_TC_SHIPMENT_ID", "OBN_LPN_FACILITY_STATUS", "OBN_TC_REFERENCE_LPN_ID", "OBN_TC_ORDER_ID", "OBN_ORIG_TC_SHIPMENT_ID", "INB_TC_PURCHASE_ORDERS_ID", "INB_PURCHASE_ORDERS_ID", "INB_TC_SHIPMENT_ID", "INB_SHIPMENT_ID", "INB_TC_ASN_ID", "INB_ASN_ID", "OBN_TC_PURCHASE_ORDERS_ID", "OBN_PURCHASE_ORDERS_ID", "OBN_TC_SHIPMENT_ID", "OBN_SHIPMENT_ID", "OBN_TC_ASN_ID", "OBN_ASN_ID", "INB_ESTIMATED_VOLUME", "INB_WEIGHT", "INB_ACTUAL_VOLUME", "INB_SPLIT_LPN_ID", "INB_C_FACILITY_ALIAS_ID", "INB_LPN_SIZE_TYPE_ID", "INB_PICK_SUB_LOCN_ID", "INB_PREV_SUB_LOCN_ID", "INB_DEST_SUB_LOCN_ID", "INB_WORK_ORD_NBR", "INB_INTERNAL_ORDER_ID", "INB_PACK_WAVE_NBR", "INB_WAVE_NBR", "INB_WAVE_SEQ_NBR", "INB_WAVE_STAT_CODE", "INB_DIRECTED_QTY", "INB_TRANS_INVN_TYPE", "INB_CONS_PRTY_DTTM", "INB_PUTAWAY_TYPE", "INB_SINGLE_LINE_LPN", "INB_ITEM_ID", "INB_D_FACILITY_ID", "INB_D_FACILITY_ALIAS_ID", "INB_PLANNED_TC_ASN_ID", "INB_ESTIMATED_WEIGHT", "INB_ORDER_ID", "INB_DISPOSITION_TYPE") AS
(SELECT DISTINCT
       INB.tc_lpn_id AS INB_TC_LPN_ID,
       INB.lpn_id AS INB_LPN_ID,
       INB.c_facility_id AS INB_C_FACILITY_ID,
       INB.inbound_outbound_indicator AS INB_INBOUND_OUTBOUND_INDICATOR,
       INB.curr_sub_locn_id AS INB_CURR_SUB_LOCN_ID,
       OBN.tc_lpn_id AS OBN_TC_LPN_ID,
       OBN.lpn_id AS OBN_LPN_ID,
       OBN.inbound_outbound_indicator AS OBN_INBOUND_OUTBOUND_INDICATOR,
       asn.tc_asn_id AS ASN_TC_ASN_ID,
       asn.asn_id AS ASN_ASN_ID,
       ADT.asn_id AS ADT_ASN_ID,
       ADT.std_pack_qty AS ADT_STD_PACK_QTY,
       ADT.sku_id AS ADT_SKU_ID,
       ADT.disposition_type AS ADT_DISPOSITION_TYPE,
       ADT.pre_receipt_status AS ADT_PRE_RECEIPT_STATUS,
       ADT.shipped_qty AS ADT_SHIPPED_QTY,
       asn.tc_shipment_id AS ASN_TC_SHIPMENT_ID,
       IC.item_name AS ITEM_NAME,
       ic.description AS ITEM_DESCRIPTION,
       ld.size_value AS DTL_QUANTITY,
       ld.std_pack_qty AS PACK_QTY,
       IW.store_dept AS IW_STORE_DEPT,
       orders.d_facility_alias_id AS ORDERS_d_facility_alias_id,
       orders.order_consol_locn_id AS orders_ORDER_CONSOL_LOCN_ID,
       asn.destination_facility_alias_id AS ASN_DEST_FACILITY_ALIAS_ID,
       pkt_consol_locn.pkt_consol_attr AS CONSOL_LOCN_PKT_CONSOL_ATTR,
       orders.priority AS ORDERS_PRIORITY,
       asn.origin_facility_alias_id AS ASN_ORIGIN_FACILITY_ALIAS_ID,
       asn.business_partner_id AS ASN_BUSINESS_PARTNER_ID,
       asn.asn_type AS ASN_ASN_TYPE,
       asn.asn_status AS ASN_ASN_STATUS,
       asn.origin_facility_id AS ASN_ORIGIN_FACILITY_ID,
       asn.destination_facility_id AS ASN_DESTINATION_FACILITY_ID,
       asn.appointment_id AS ASN_APPOINTMENT_ID,
       asn.total_shipped_qty AS ASN_TOTAL_SHIPPED_QTY,
       asn.total_received_qty AS ASN_TOTAL_RECEIVED_QTY,
       asn.assigned_carrier_code AS ASN_ASSIGNED_CARRIER_CODE,
       asn.bill_of_lading_number AS ASN_BILL_OF_LADING_NUMBER,
       asn.pro_number AS ASN_PRO_NUMBER,
       asn.total_volume AS ASN_TOTAL_VOLUME,
       asn.inbound_region_id AS ASN_INBOUND_REGION_ID,
       asn.outbound_region_id AS ASN_OUTBOUND_REGION_ID,
       asn.region_id AS ASN_REGION_ID,
       asn.asn_level AS ASN_ASN_LEVEL,
       asn.has_import_error AS ASN_HAS_IMPORT_ERROR,
       asn.has_soft_check_error AS ASN_HAS_SOFT_CHECK_ERROR,
       asn.has_alerts AS ASN_HAS_ALERTS,
       asn.system_allocated AS ASN_SYSTEM_ALLOCATED,
       asn.asn_priority AS ASN_ASN_PRIORITY,
       asn.is_cancelled AS ASN_IS_CANCELLED,
       asn.is_closed AS ASN_IS_CLOSED,
       asn.manif_nbr AS ASN_MANIF_NBR,
       asn.work_ord_nbr AS ASN_WORK_ORD_NBR,
       asn.shipped_lpn_count AS ASN_SHIPPED_LPN_COUNT,
       asn.received_lpn_count AS ASN_RECEIVED_LPN_COUNT,
       asn.initiate_flag AS ASN_INITIATE_FLAG,
       asn.shipment_id AS ASN_SHIPMENT_ID,
       asn.allocation_completed AS ASN_ALLOCATION_COMPLETED,
       asn.asn_orgn_type AS ASN_ASN_ORGN_TYPE,
       asn.mhe_sent AS ASN_MHE_SENT,
       asn.flow_through_allocation_method AS ASN_FLOW_THR_ALLOC_METHOD,
       asn.trailer_number AS ASN_TRAILER_NUMBER,
       asn.destination_type AS ASN_DESTINATION_TYPE,
       asn.original_asn_id AS ASN_ORIGINAL_ASN_ID,
       asn.pre_receipt_status AS ASN_PRE_RECEIPT_STATUS,
       asn.pre_allocation_fit_percentage AS ASN_PRE_ALLOC_FIT_PERCENTAGE,
       ADT.asn_detail_id AS ADT_ASN_DETAIL_ID,
       ADT.tc_purchase_orders_id AS ADT_TC_PURCHASE_ORDERS_ID,
       ADT.purchase_orders_id AS ADT_PURCHASE_ORDERS_ID,
       ADT.business_partner_id AS ADT_BUSINESS_PARTNER_ID,
       ADT.sku_attr_1 AS ADT_SKU_ATTR_1,
       ADT.sku_attr_2 AS ADT_SKU_ATTR_2,
       ADT.sku_attr_3 AS ADT_SKU_ATTR_3,
       ADT.sku_attr_4 AS ADT_SKU_ATTR_4,
       ADT.sku_attr_5 AS ADT_SKU_ATTR_5,
       ADT.std_case_qty AS ADT_STD_CASE_QTY,
       ADT.received_qty AS ADT_RECEIVED_QTY,
       ADT.std_sub_pack_qty AS ADT_STD_SUB_PACK_QTY,
       ADT.lpn_per_tier AS ADT_LPN_PER_TIER,
       ADT.is_cancelled AS ADT_IS_CANCELLED,
       ADT.is_closed AS ADT_IS_CLOSED,
       ADT.invn_type AS ADT_INVN_TYPE,
       ADT.prod_stat AS ADT_PROD_STAT,
       ADT.batch_nbr AS ADT_BATCH_NBR,
       ADT.cntry_of_orgn AS ADT_CNTRY_OF_ORGN,
       ADT.shipped_lpn_count AS ADT_SHIPPED_LPN_COUNT,
       ADT.received_lpn_count AS ADT_RECEIVED_LPN_COUNT,
       ADT.units_assigned_to_lpn AS ADT_UNITS_ASSIGNED_TO_LPN,
       ADT.tc_company_id AS ADT_TC_COMPANY_ID,
       ADT.tc_po_line_id AS ADT_TC_PO_LINE_ID,
       ADT.tc_order_id AS ADT_TC_ORDER_ID,
       ADT.order_id AS ADT_ORDER_ID,
       ADT.tc_order_line_id AS ADT_TC_ORDER_LINE_ID,
       ADT.order_line_item_id AS ADT_ORDER_LINE_ITEM_ID,
       INB.business_partner_id AS INB_BUSINESS_PARTNER_ID,
       OBN.business_partner_id AS OBN_BUSINESS_PARTNER_ID,
       INB.tc_company_id AS INB_TC_COMPANY_ID,
       INB.parent_lpn_id AS INB_PARENT_LPN_ID,
       INB.tc_parent_lpn_id AS INB_TC_PARENT_LPN_ID,
       INB.lpn_type AS INB_LPN_TYPE,
       OBN.tc_company_id AS OBN_TC_COMPANY_ID,
       OBN.parent_lpn_id AS OBN_PARENT_LPN_ID,
       OBN.tc_parent_lpn_id AS OBN_TC_PARENT_LPN_ID,
       OBN.lpn_type AS OBN_LPN_TYPE,
       INB.lpn_facility_status AS INB_LPN_FACILITY_STATUS,
       INB.tc_reference_lpn_id AS INB_TC_REFERENCE_LPN_ID,
       INB.tc_order_id AS INB_TC_ORDER_ID,
       INB.original_tc_shipment_id AS INB_ORIG_TC_SHIPMENT_ID,
       OBN.lpn_facility_status AS OBN_LPN_FACILITY_STATUS,
       OBN.tc_reference_lpn_id AS OBN_TC_REFERENCE_LPN_ID,
       OBN.tc_order_id AS OBN_TC_ORDER_ID,
       OBN.original_tc_shipment_id AS OBN_ORIG_TC_SHIPMENT_ID,
       INB.tc_purchase_orders_id AS INB_TC_PURCHASE_ORDERS_ID,
       INB.purchase_orders_id AS INB_PURCHASE_ORDERS_ID,
       INB.tc_shipment_id AS INB_TC_SHIPMENT_ID,
       INB.shipment_id AS INB_SHIPMENT_ID,
       INB.tc_asn_id AS INB_TC_ASN_ID,
       INB.asn_id AS INB_ASN_ID,
       OBN.tc_purchase_orders_id AS OBN_TC_PURCHASE_ORDERS_ID,
       OBN.purchase_orders_id AS OBN_PURCHASE_ORDERS_ID,
       OBN.tc_shipment_id AS OBN_TC_SHIPMENT_ID,
       OBN.shipment_id AS OBN_SHIPMENT_ID,
       OBN.tc_asn_id AS OBN_TC_ASN_ID,
       OBN.asn_id AS OBN_ASN_ID,
       INB.estimated_volume AS INB_ESTIMATED_VOLUME,
       INB.weight AS INB_WEIGHT,
       INB.actual_volume AS INB_ACTUAL_VOLUME,
       INB.split_lpn_id AS INB_SPLIT_LPN_ID,
       INB.c_facility_alias_id AS INB_C_FACILITY_ALIAS_ID,
       INB.lpn_size_type_id AS INB_LPN_SIZE_TYPE_ID,
       INB.pick_sub_locn_id AS INB_PICK_SUB_LOCN_ID,
       INB.prev_sub_locn_id AS INB_PREV_SUB_LOCN_ID,
       INB.dest_sub_locn_id AS INB_DEST_SUB_LOCN_ID,
       INB.work_ord_nbr AS INB_WORK_ORD_NBR,
       INB.internal_order_id AS INB_INTERNAL_ORDER_ID,
       INB.pack_wave_nbr AS INB_PACK_WAVE_NBR,
       INB.wave_nbr AS INB_WAVE_NBR,
       INB.wave_seq_nbr AS INB_WAVE_SEQ_NBR,
       INB.wave_stat_code AS INB_WAVE_STAT_CODE,
       INB.directed_qty AS INB_DIRECTED_QTY,
       INB.transitional_inventory_type AS INB_TRANS_INVN_TYPE,
       INB.consumption_priority_dttm AS INB_CONS_PRTY_DTTM,
       INB.putaway_type AS INB_PUTAWAY_TYPE,
       INB.single_line_lpn AS INB_SINGLE_LINE_LPN,
       INB.item_id AS INB_ITEM_ID,
       INB.d_facility_id AS INB_D_FACILITY_ID,
       INB.d_facility_alias_id AS INB_D_FACILITY_ALIAS_ID,
       INB.planned_tc_asn_id AS INB_PLANNED_TC_ASN_ID,
       INB.estimated_weight AS INB_ESTIMATED_WEIGHT,
       INB.order_id AS INB_ORDER_ID,
       INB.DISPOSITION_TYPE AS INB_DISPOSITION_TYPE
  FROM lpn INB,
       lpn OBN,
       asn ASN,
       asn_detail ADT,
       item_cbo IC,
       lpn_detail LD,
       item_wms IW,
       orders ORDERS,
       pkt_consol_locn PKT_CONSOL_LOCN
 WHERE     INB.tc_lpn_id = OBN.tc_reference_lpn_id
       AND asn.tc_asn_id = INB.tc_asn_id
       AND asn.asn_id = ADT.asn_id
       AND asn.HAS_IMPORT_ERROR = 0
       AND adt.IS_CANCELLED = 0
       AND adt.IS_CLOSED = 0
       AND obn.inbound_outbound_indicator = 'O'
       AND OBN.lpn_id = LD.lpn_id
       AND LD.item_id = IC.item_id
       AND ADT.sku_id = LD.item_id
       AND ADT.sku_id = IC.item_id
       AND IW.item_id = IC.item_id
       AND LD.item_id = IW.item_id
       AND ADT.sku_id = IW.item_id
       AND OBN.order_id = orders.order_id(+)
       AND LD.item_id = ADT.sku_id
       AND OBN.dest_sub_locn_id = pkt_consol_locn.locn_id(+)
       AND inb.inbound_outbound_indicator = 'I'
UNION
SELECT DISTINCT
       INB.tc_lpn_id AS INB_TC_LPN_ID,
       INB.lpn_id AS INB_LPN_ID,
       INB.c_facility_id AS INB_C_FACILITY_ID,
       INB.inbound_outbound_indicator AS INB_INBOUND_OUTBOUND_INDICATOR,
       INB.curr_sub_locn_id AS INB_CURR_SUB_LOCN_ID,
       '' AS OBN_TC_LPN_ID,
       0 AS OBN_LPN_ID,
       '' AS OBN_INBOUND_OUTBOUND_INDICATOR,
       asn.tc_asn_id AS ASN_TC_ASN_ID,
       asn.asn_id AS ASN_ASN_ID,
       ADT.asn_id AS ADT_ASN_ID,
       ADT.std_pack_qty AS ADT_STD_PACK_QTY,
       ADT.sku_id AS ADT_SKU_ID,
       ADT.disposition_type AS ADT_DISPOSITION_TYPE,
       ADT.pre_receipt_status AS ADT_PRE_RECEIPT_STATUS,
       ADT.shipped_qty AS ADT_SHIPPED_QTY,
       asn.tc_shipment_id AS ASN_TC_SHIPMENT_ID,
       IC.item_name AS ITEM_NAME,
       ic.description AS ITEM_DESCRIPTION,
       ld.size_value AS DTL_QUANTITY,
       ld.std_pack_qty AS PACK_QTY,
       IW.store_dept AS IW_STORE_DEPT,
       '' AS ORDERS_d_facility_alias_id,
       '' AS orders_ORDER_CONSOL_LOCN_ID,
       asn.destination_facility_alias_id AS ASN_DEST_FACILITY_ALIAS_ID,
       '' AS CONSOL_LOCN_PKT_CONSOL_ATTR,
       0 AS ORDERS_PRIORITY,
       asn.origin_facility_alias_id AS ASN_ORIGIN_FACILITY_ALIAS_ID,
       asn.business_partner_id AS ASN_BUSINESS_PARTNER_ID,
       asn.asn_type AS ASN_ASN_TYPE,
       asn.asn_status AS ASN_ASN_STATUS,
       asn.origin_facility_id AS ASN_ORIGIN_FACILITY_ID,
       asn.destination_facility_id AS ASN_DESTINATION_FACILITY_ID,
       asn.appointment_id AS ASN_APPOINTMENT_ID,
       asn.total_shipped_qty AS ASN_TOTAL_SHIPPED_QTY,
       asn.total_received_qty AS ASN_TOTAL_RECEIVED_QTY,
       asn.assigned_carrier_code AS ASN_ASSIGNED_CARRIER_CODE,
       asn.bill_of_lading_number AS ASN_BILL_OF_LADING_NUMBER,
       asn.pro_number AS ASN_PRO_NUMBER,
       asn.total_volume AS ASN_TOTAL_VOLUME,
       asn.inbound_region_id AS ASN_INBOUND_REGION_ID,
       asn.outbound_region_id AS ASN_OUTBOUND_REGION_ID,
       asn.region_id AS ASN_REGION_ID,
       asn.asn_level AS ASN_ASN_LEVEL,
       asn.has_import_error AS ASN_HAS_IMPORT_ERROR,
       asn.has_soft_check_error AS ASN_HAS_SOFT_CHECK_ERROR,
       asn.has_alerts AS ASN_HAS_ALERTS,
       asn.system_allocated AS ASN_SYSTEM_ALLOCATED,
       asn.asn_priority AS ASN_ASN_PRIORITY,
       asn.is_cancelled AS ASN_IS_CANCELLED,
       asn.is_closed AS ASN_IS_CLOSED,
       asn.manif_nbr AS ASN_MANIF_NBR,
       asn.work_ord_nbr AS ASN_WORK_ORD_NBR,
       asn.shipped_lpn_count AS ASN_SHIPPED_LPN_COUNT,
       asn.received_lpn_count AS ASN_RECEIVED_LPN_COUNT,
       asn.initiate_flag AS ASN_INITIATE_FLAG,
       asn.shipment_id AS ASN_SHIPMENT_ID,
       asn.allocation_completed AS ASN_ALLOCATION_COMPLETED,
       asn.asn_orgn_type AS ASN_ASN_ORGN_TYPE,
       asn.mhe_sent AS ASN_MHE_SENT,
       asn.flow_through_allocation_method AS ASN_FLOW_THR_ALLOC_METHOD,
       asn.trailer_number AS ASN_TRAILER_NUMBER,
       asn.destination_type AS ASN_DESTINATION_TYPE,
       asn.original_asn_id AS ASN_ORIGINAL_ASN_ID,
       asn.pre_receipt_status AS ASN_PRE_RECEIPT_STATUS,
       asn.pre_allocation_fit_percentage AS ASN_PRE_ALLOC_FIT_PERCENTAGE,
       ADT.asn_detail_id AS ADT_ASN_DETAIL_ID,
       ADT.tc_purchase_orders_id AS ADT_TC_PURCHASE_ORDERS_ID,
       ADT.purchase_orders_id AS ADT_PURCHASE_ORDERS_ID,
       ADT.business_partner_id AS ADT_BUSINESS_PARTNER_ID,
       ADT.sku_attr_1 AS ADT_SKU_ATTR_1,
       ADT.sku_attr_2 AS ADT_SKU_ATTR_2,
       ADT.sku_attr_3 AS ADT_SKU_ATTR_3,
       ADT.sku_attr_4 AS ADT_SKU_ATTR_4,
       ADT.sku_attr_5 AS ADT_SKU_ATTR_5,
       ADT.std_case_qty AS ADT_STD_CASE_QTY,
       ADT.received_qty AS ADT_RECEIVED_QTY,
       ADT.std_sub_pack_qty AS ADT_STD_SUB_PACK_QTY,
       ADT.lpn_per_tier AS ADT_LPN_PER_TIER,
       ADT.is_cancelled AS ADT_IS_CANCELLED,
       ADT.is_closed AS ADT_IS_CLOSED,
       ADT.invn_type AS ADT_INVN_TYPE,
       ADT.prod_stat AS ADT_PROD_STAT,
       ADT.batch_nbr AS ADT_BATCH_NBR,
       ADT.cntry_of_orgn AS ADT_CNTRY_OF_ORGN,
       ADT.shipped_lpn_count AS ADT_SHIPPED_LPN_COUNT,
       ADT.received_lpn_count AS ADT_RECEIVED_LPN_COUNT,
       ADT.units_assigned_to_lpn AS ADT_UNITS_ASSIGNED_TO_LPN,
       ADT.tc_company_id AS ADT_TC_COMPANY_ID,
       ADT.tc_po_line_id AS ADT_TC_PO_LINE_ID,
       ADT.tc_order_id AS ADT_TC_ORDER_ID,
       ADT.order_id AS ADT_ORDER_ID,
       ADT.tc_order_line_id AS ADT_TC_ORDER_LINE_ID,
       ADT.order_line_item_id AS ADT_ORDER_LINE_ITEM_ID,
       INB.business_partner_id AS INB_BUSINESS_PARTNER_ID,
       '' AS OBN_BUSINESS_PARTNER_ID,
       INB.tc_company_id AS INB_TC_COMPANY_ID,
       INB.parent_lpn_id AS INB_PARENT_LPN_ID,
       INB.tc_parent_lpn_id AS INB_TC_PARENT_LPN_ID,
       INB.lpn_type AS INB_LPN_TYPE,
       0 AS OBN_TC_COMPANY_ID,
       0 AS OBN_PARENT_LPN_ID,
       '' AS OBN_TC_PARENT_LPN_ID,
       0 AS OBN_LPN_TYPE,
       INB.lpn_facility_status AS INB_LPN_FACILITY_STATUS,
       INB.tc_reference_lpn_id AS INB_TC_REFERENCE_LPN_ID,
       INB.tc_order_id AS INB_TC_ORDER_ID,
       INB.original_tc_shipment_id AS INB_ORIG_TC_SHIPMENT_ID,
       -1 AS OBN_LPN_FACILITY_STATUS,
       '' AS OBN_TC_REFERENCE_LPN_ID,
       '' AS OBN_TC_ORDER_ID,
       '' AS OBN_ORIG_TC_SHIPMENT_ID,
       INB.tc_purchase_orders_id AS INB_TC_PURCHASE_ORDERS_ID,
       INB.purchase_orders_id AS INB_PURCHASE_ORDERS_ID,
       INB.tc_shipment_id AS INB_TC_SHIPMENT_ID,
       INB.shipment_id AS INB_SHIPMENT_ID,
       INB.tc_asn_id AS INB_TC_ASN_ID,
       INB.asn_id AS INB_ASN_ID,
       '' AS OBN_TC_PURCHASE_ORDERS_ID,
       0 AS OBN_PURCHASE_ORDERS_ID,
       '' AS OBN_TC_SHIPMENT_ID,
       0 AS OBN_SHIPMENT_ID,
       '' AS OBN_TC_ASN_ID,
       0 AS OBN_ASN_ID,
       INB.estimated_volume AS INB_ESTIMATED_VOLUME,
       INB.weight AS INB_WEIGHT,
       INB.actual_volume AS INB_ACTUAL_VOLUME,
       INB.split_lpn_id AS INB_SPLIT_LPN_ID,
       INB.c_facility_alias_id AS INB_C_FACILITY_ALIAS_ID,
       INB.lpn_size_type_id AS INB_LPN_SIZE_TYPE_ID,
       INB.pick_sub_locn_id AS INB_PICK_SUB_LOCN_ID,
       INB.prev_sub_locn_id AS INB_PREV_SUB_LOCN_ID,
       INB.dest_sub_locn_id AS INB_DEST_SUB_LOCN_ID,
       INB.work_ord_nbr AS INB_WORK_ORD_NBR,
       INB.internal_order_id AS INB_INTERNAL_ORDER_ID,
       INB.pack_wave_nbr AS INB_PACK_WAVE_NBR,
       INB.wave_nbr AS INB_WAVE_NBR,
       INB.wave_seq_nbr AS INB_WAVE_SEQ_NBR,
       INB.wave_stat_code AS INB_WAVE_STAT_CODE,
       INB.directed_qty AS INB_DIRECTED_QTY,
       INB.transitional_inventory_type AS INB_TRANS_INVN_TYPE,
       INB.consumption_priority_dttm AS INB_CONS_PRTY_DTTM,
       INB.putaway_type AS INB_PUTAWAY_TYPE,
       INB.single_line_lpn AS INB_SINGLE_LINE_LPN,
       INB.item_id AS INB_ITEM_ID,
       INB.d_facility_id AS INB_D_FACILITY_ID,
       INB.d_facility_alias_id AS INB_D_FACILITY_ALIAS_ID,
       INB.planned_tc_asn_id AS INB_PLANNED_TC_ASN_ID,
       INB.estimated_weight AS INB_ESTIMATED_WEIGHT,
       INB.order_id AS INB_ORDER_ID,
       INB.DISPOSITION_TYPE AS INB_DISPOSITION_TYPE
  FROM lpn INB,
       asn ASN,
       asn_detail ADT,
       item_cbo IC,
       lpn_detail LD,
       item_wms IW
 WHERE     asn.tc_asn_id = INB.tc_asn_id
       AND asn.asn_id = ADT.asn_id
       AND asn.HAS_IMPORT_ERROR = 0
       AND ADT.IS_CANCELLED = 0
       AND ADT.IS_CLOSED = 0
       AND INB.tc_lpn_id NOT IN
              (SELECT DISTINCT INB.tc_lpn_id
                 FROM lpn INB,
                      lpn OBN,
                      asn ASN,
                      asn_detail ADT,
                      item_cbo IC,
                      lpn_detail LD,
                      item_wms IW
                WHERE     INB.tc_lpn_id = OBN.tc_reference_lpn_id
                      AND asn.tc_asn_id = INB.tc_asn_id
                      AND asn.asn_id = ADT.asn_id
                      AND asn.HAS_IMPORT_ERROR = 0
                      AND adt.IS_CANCELLED = 0
                      AND adt.IS_CLOSED = 0
                      AND obn.inbound_outbound_indicator = 'O'
                      AND OBN.lpn_id = LD.lpn_id
                      AND LD.item_id = IC.item_id
                      AND ADT.sku_id = LD.item_id
                      AND ADT.sku_id = IC.item_id
                      AND IW.item_id = IC.item_id
                      AND LD.item_id = IW.item_id
                      AND ADT.sku_id = IW.item_id
                      AND LD.item_id = ADT.sku_id)
       AND INB.inbound_outbound_indicator = 'I'
       AND INB.lpn_id = LD.lpn_id
       AND LD.item_id = IC.item_id
       AND ADT.sku_id = LD.item_id
       AND ADT.sku_id = IC.item_id
       AND IW.item_id = IC.item_id
       AND LD.item_id = IW.item_id
       AND ADT.sku_id = IW.item_id
       AND LD.item_id = ADT.sku_id);

CREATE OR REPLACE FORCE VIEW "MANIFEST_EXCEPTIONS" ("TC_COMPANY_ID", "MANIFEST_ID", "LPN_ID", "TC_LPN_ID", "TC_PARENT_LPN_ID", "TC_SHIPMENT_ID", "TC_ORDER_ID", "LPN_FACILITY_STATUS") AS
(SELECT L.TC_COMPANY_ID L1,
       ML.MANIFEST_ID L2,
       L.LPN_ID L3,
       L.TC_LPN_ID L4,
       L.TC_PARENT_LPN_ID L5,
       L.TC_SHIPMENT_ID L6,
       L.TC_ORDER_ID || '-' || L.ORDER_ID L7,
       TO_CHAR (L.LPN_FACILITY_STATUS) L8
  FROM LPN L, MANIFESTED_LPN ML
 WHERE     L.SHIPMENT_ID = ML.SHIPMENT_ID
       AND ML.MANIFEST_ID IN (SELECT manifest_id
                                FROM manifest_hdr
                               WHERE manifest_status_id = 10)
       AND L.LPN_FACILITY_STATUS < 40
UNION
SELECT RS.COID,
       RS.mid L0,
       RS.LINEITEMID L1,
       RS.TCORDERID L2,
       RS.TCORDERID L3,
       TO_CHAR (RS.ORDERID) L4,
       TO_CHAR (RS.QTYVARIANCE) L5,
       RS.QTYUOM L6
  FROM (  SELECT ORD.TC_COMPANY_ID COID,
                 MLPN.MANIFEST_ID mid,
                 ORD.ORDER_ID ORDERID,
                 ORD.TC_ORDER_ID TCORDERID,
                 OLI.LINE_ITEM_ID AS LINEITEMID,
                 (COALESCE (OLI.ORDER_QTY, 0)
                  - COALESCE (OLI.USER_CANCELED_QTY, 0)
                  - SUM (
                       CASE
                          WHEN L.LPN_FACILITY_STATUS < 20
                          THEN
                             COALESCE (LD.INITIAL_QTY, 0)
                          ELSE
                             COALESCE (LD.SIZE_VALUE, 0)
                       END))
                    QTYVARIANCE,
                 MIN (SIZE_UOM.SIZE_UOM) AS QTYUOM
            FROM MANIFESTED_LPN MLPN
                 INNER JOIN ORDERS ORD
                    ON ORD.ORDER_ID = MLPN.ORDER_ID
                       AND ORD.IS_CANCELLED = 0
                       AND MLPN.MANIFEST_ID IN
                              (SELECT manifest_id
                                 FROM manifest_hdr
                                WHERE manifest_status_id = 10)
                 INNER JOIN ORDER_LINE_ITEM OLI
                    ON OLI.ORDER_ID = ORD.ORDER_ID AND OLI.IS_CANCELLED = 0
                 INNER JOIN LPN L
                    ON     L.ORDER_ID = ORD.ORDER_ID
                       AND L.LPN_FACILITY_STATUS < 99
                       AND L.LPN_TYPE = 1
                       AND L.INBOUND_OUTBOUND_INDICATOR = 'O'
                 LEFT JOIN LPN_DETAIL LD
                    ON LD.LPN_ID = L.LPN_ID
                       AND LD.DISTRIBUTION_ORDER_DTL_ID = OLI.LINE_ITEM_ID
                 LEFT JOIN SIZE_UOM
                    ON SIZE_UOM.SIZE_UOM_ID = OLI.QTY_UOM_ID_BASE
        GROUP BY ORD.TC_COMPANY_ID,
                 MLPN.MANIFEST_ID,
                 ORD.ORDER_ID,
                 ORD.TC_ORDER_ID,
                 OLI.LINE_ITEM_ID,
                 OLI.ORDER_QTY,
                 OLI.USER_CANCELED_QTY) RS
 WHERE RS.QTYVARIANCE > 0);

CREATE OR REPLACE FORCE VIEW "ACCESSORIAL_PARAM_SET_VIEW" ("ACCESSORIAL_PARAM_SET_ID", "TC_COMPANY_ID", "CARRIER_CODE", "CARRIER_ID", "USE_FAK_COMMODITY", "COMMODITY_CLASS", "EQUIPMENT_CODE", "EQUIPMENT_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "CM_RATE_TYPE", "CM_RATE_CURRENCY_CODE", "STOP_OFF_CURRENCY_CODE", "DISTANCE_UOM", "BUDGETED_APPROVED", "SIZE_UOM", "SIZE_UOM_ID", "IS_GLOBAL", "HAS_ERRORS", "DTL_HAS_ERRORS") AS
select aps.accessorial_param_set_id,
      aps.tc_company_id,
      cast (null as varchar (10)),
      aps.carrier_id,
      aps.use_fak_commodity,
      aps.commodity_class,
      cast (null as varchar (8)),
      aps.equipment_id,
      aps.effective_dt,
      aps.expiration_dt,
      aps.cm_rate_type,
      aps.cm_rate_currency_code,
      aps.stop_off_currency_code,
      aps.distance_uom,
      aps.budgeted_approved,
      cast (null as varchar (8)),
      aps.size_uom_id,
      aps.is_global,
      0 has_errors,
      coalesce (
         (select sum (decode (icrd.status, 4, 1, 0))
            from import_accessorial_param_set iaps,
                 import_cm_rate_discount icrd
           where aps.accessorial_param_set_id =
                    iaps.accessorial_param_set_id
                 and aps.tc_company_id = iaps.tc_company_id
                 and iaps.param_id = icrd.param_id),
         0)
      + coalesce (
           (select sum (decode (isoc.status, 4, 1, 0))
              from import_accessorial_param_set iaps,
                   import_stop_off_charge isoc
             where aps.accessorial_param_set_id =
                      iaps.accessorial_param_set_id
                   and aps.tc_company_id = iaps.tc_company_id
                   and iaps.param_id = isoc.param_id),
           0)
      + coalesce (
           (select sum (decode (iar.status, 4, 1, 0))
              from import_accessorial_param_set iaps,
                   import_accessorial_rate iar
             where aps.accessorial_param_set_id =
                      iaps.accessorial_param_set_id
                   and aps.tc_company_id = iaps.tc_company_id
                   and iaps.param_id = iar.param_id
                   and iaps.tc_company_id = iar.tc_company_id),
           0)
         dtl_has_errors
 from accessorial_param_set aps
 union all
 select iaps.param_id,
      iaps.tc_company_id,
      iaps.carrier_code,
      cast (null as int),
      iaps.use_fak_commodity,
      iaps.commodity_class,
      iaps.equipment_code,
      iaps.equipment_id,
      iaps.effective_dt,
      iaps.expiration_dt,
      iaps.cm_rate_type,
      iaps.cm_rate_currency_code,
      iaps.stop_off_currency_code,
      iaps.distance_uom,
      iaps.budgeted_approved,
      iaps.size_uom,
      cast (null as int),
      iaps.is_global,
      decode (iaps.status, 4, 1, 0) has_errors,
      coalesce ( (select sum (decode (icrd.status, 4, 1, 0))
                    from import_cm_rate_discount icrd
                   where iaps.param_id = icrd.param_id),
                0)
      + coalesce ( (select sum (decode (isoc.status, 4, 1, 0))
                      from import_stop_off_charge isoc
                     where iaps.param_id = isoc.param_id),
                  0)
      + coalesce (
           (select sum (decode (iar.status, 4, 1, 0))
              from import_accessorial_rate iar
             where iaps.param_id = iar.param_id
                   and iaps.tc_company_id = iar.tc_company_id),
           0)
         dtl_has_errors
 from import_accessorial_param_set iaps
where iaps.status = 4
      and not exists
             (select 1
                from accessorial_param_set aps
               where aps.accessorial_param_set_id = iaps.param_id);

CREATE OR REPLACE FORCE VIEW "ACCESSORIAL_RATE_DATA_VIEW" ("LANE_ACCESSORIAL_ID", "TC_COMPANY_ID", "ACCESSORIAL_ID", "ACCESSORIAL_CODE", "LANE_ID", "RATING_LANE_DTL_SEQ", "RATE", "MINIMUM_RATE", "CURRENCY_CODE", "IS_AUTO_APPROVE", "ACCESSORIAL_TYPE", "DATA_SOURCE", "DISTANCE_UOM", "IS_SHIPMENT_ACCESSORIAL", "PAYEE_CARRIER_ID", "MINIMUM_SIZE", "MAXIMUM_SIZE", "IS_TAX_ACCESSORIAL", "IS_ADD_TO_SPOT_CHARGE", "IS_APPLY_ONCE_PER_SHIP", "IS_NON_MACHINEABLE", "IS_SHIP_VIA", "IS_REQUESTED", "IS_RESIDENTIAL_DELIVERY") AS
select la.lane_accessorial_id,
      la.tc_company_id,
      la.accessorial_id,
      ac.accessorial_code,
      la.lane_id,
      la.rating_lane_dtl_seq,
      la.rate,
      la.minimum_rate,
      la.currency_code,
      la.is_auto_approve,
      ac.accessorial_type,
      ac.data_source,
      ac.distance_uom,
      la.is_shipment_accessorial,
      la.payee_carrier_id,
      minimum_size,
      maximum_size,
      ac.is_tax_accessorial,
      ac.is_add_to_spot_charge,
      ac.is_apply_once_per_ship,
      ac.is_non_machineable,
      ac.is_ship_via,
      ac.is_requested,
      ac.is_residential_delivery
 from lane_accessorial la, accessorial_code ac
where la.tc_company_id = ac.tc_company_id
      and la.accessorial_id = ac.accessorial_id;

CREATE OR REPLACE FORCE VIEW "ACCESSORIAL_RATE_VIEW" ("ACCESSORIAL_PARAM_SET_ID", "ACCESSORIAL_RATE_ID", "TC_COMPANY_ID", "MOT", "MOT_ID", "EQUIPMENT_CODE", "EQUIPMENT_ID", "SERVICE_LEVEL", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL", "PROTECTION_LEVEL_ID", "ACCESSORIAL_CODE", "RATE", "MINIMUM_RATE", "CURRENCY_CODE", "IS_AUTO_APPROVE", "EFFECTIVE_DT", "EXPIRATION_DT", "HAS_ERRORS", "PAYEE_CARRIER_CODE", "PAYEE_CARRIER_ID", "MINIMUM_SIZE", "MAXIMUM_SIZE", "SIZE_UOM", "SIZE_UOM_ID", "ZONE_ID", "IS_SHIPMENT_ACCESSORIAL", "ACCESSORIAL_ID", "INCOTERM_ID", "LONGEST_DIMENSION", "SECOND_LONGEST_DIMENSION", "LENGTH_PLUS_GRITH", "WEIGHT", "BASE_AMOUNT", "CHARGE_PER_X_FIELD1", "CHARGE_PER_X_FIELD2", "BASE_CHARGE", "RANGE_MAX_AMOUNT", "RANGE_MIN_AMOUNT", "MIN_RATE_TYPE", "MAXIMUM_RATE", "AMOUNT", "CUSTOMER_RATE", "CUSTOMER_MIN_CHARGE", "CALCULATED_RATE", "PACKAGE_TYPE_ID", "OPTION_GROUP_ID", "OPTION_GROUP_DESC", "MIN_LONGEST_DIMENSION", "MAX_LONGEST_DIMENSION", "MIN_SECOND_LONGEST_DIMENSION", "MAX_SECOND_LONGEST_DIMENSION", "MIN_THIRD_LONGEST_DIMENSION", "MAX_THIRD_LONGEST_DIMENSION", "MIN_WEIGHT", "MAX_WEIGHT", "MIN_LENGTH_PLUS_GRITH", "MAX_LENGTH_PLUS_GRITH", "BILLING_METHOD", "IS_ZONE_ORIGIN", "IS_ZONE_DEST", "IS_ZONE_STOPOFF", "INCREMENT_VAL", "ORM_TYPE", "ORM_OPERATOR", "ORM_OFSET", "COMMODITY_CODE_ID", "MAX_RANGE_COMMODITY_CODE_ID") AS
SELECT ACCESSORIAL_PARAM_SET_ID,
      ACCESSORIAL_RATE_ID,
      TC_COMPANY_ID,
      CAST (NULL AS VARCHAR (8)),
      MOT_ID,
      CAST (NULL AS VARCHAR (8)),
      EQUIPMENT_ID,
      CAST (NULL AS VARCHAR (8)),
      SERVICE_LEVEL_ID,
      CAST (NULL AS VARCHAR (8)),
      PROTECTION_LEVEL_ID,
      CAST (NULL AS VARCHAR (8)),
      RATE,
      MINIMUM_RATE,
      CURRENCY_CODE,
      IS_AUTO_APPROVE,
      EFFECTIVE_DT,
      EXPIRATION_DT,
      0,
      CAST (NULL AS VARCHAR (8)),
      PAYEE_CARRIER_ID,
      MINIMUM_SIZE,
      MAXIMUM_SIZE,
      CAST (NULL AS VARCHAR (8)),
      SIZE_UOM_ID,
      ZONE_ID,
      IS_SHIPMENT_ACCESSORIAL,
      ACCESSORIAL_ID,
      INCOTERM_ID,
      LONGEST_DIMENSION,
      SECOND_LONGEST_DIMENSION,
      LENGTH_PLUS_GRITH,
      WEIGHT,
      BASE_AMOUNT,
      CHARGE_PER_X_FIELD1,
      CHARGE_PER_X_FIELD2,
      BASE_CHARGE,                    -- Enhancement# MACR00094092 Changes
      RANGE_MAX_AMOUNT,
      RANGE_MIN_AMOUNT,
      MIN_RATE_TYPE,
      MAXIMUM_RATE,
      AMOUNT,
      CUSTOMER_RATE,
      CUSTOMER_MIN_CHARGE,
      CALCULATED_RATE,
      PACKAGE_TYPE_ID,
      OPTION_GROUP_ID,
      OPTION_GROUP_DESC,
      MIN_LONGEST_DIMENSION,
      MAX_LONGEST_DIMENSION,
      MIN_SECOND_LONGEST_DIMENSION,
      MAX_SECOND_LONGEST_DIMENSION,
      MIN_THIRD_LONGEST_DIMENSION,
      MAX_THIRD_LONGEST_DIMENSION,
      MIN_WEIGHT,
      MAX_WEIGHT,
      MIN_LENGTH_PLUS_GRITH,
      MAX_LENGTH_PLUS_GRITH,
      BILLING_METHOD,
      IS_ZONE_ORIGIN,
      IS_ZONE_DEST,
      IS_ZONE_STOPOFF,
      INCREMENT_VAL,                           -- Enhancement# MACR00075690
	  ORM_TYPE,
	   ORM_OPERATOR,
	  ORM_OFSET,
	  commodity_code_id,
	  max_range_commodity_code_id
 FROM ACCESSORIAL_RATE
 UNION ALL
 SELECT IAPS.PARAM_ID,
      IAR.ACCESSORIAL_RATE_ID,
      IAR.TC_COMPANY_ID,
      IAR.MOT,
      CAST (NULL AS INT),
      IAR.EQUIPMENT_CODE,
      CAST (NULL AS INT),
      IAR.SERVICE_LEVEL,
      CAST (NULL AS INT),
      IAR.PROTECTION_LEVEL,
      CAST (NULL AS INT),
      IAR.ACCESSORIAL_CODE,
      IAR.RATE,
      IAR.MINIMUM_RATE,
      IAR.CURRENCY_CODE,
      IAR.IS_AUTO_APPROVE,
      IAR.EFFECTIVE_DT,
      IAR.EXPIRATION_DT,
      DECODE (IAR.STATUS, 4, 1, 0) HAS_ERRORS,
      IAR.PAYEE_CARRIER_CODE,
      CAST (NULL AS INT),
      IAR.MINIMUM_SIZE,
      IAR.MAXIMUM_SIZE,
      IAR.SIZE_UOM,
      CAST (NULL AS INT),
      IAR.ZONE_ID,
      IAR.IS_SHIPMENT_ACCESSORIAL,
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      IAR.LONGEST_DIMENSION,
      IAR.SECOND_LONGEST_DIMENSION,
      IAR.LENGTH_PLUS_GRITH,
      IAR.WEIGHT,
      IAR.BASE_AMOUNT,
      IAR.CHARGE_PER_X_FIELD1,
      IAR.CHARGE_PER_X_FIELD2,
      CAST (NULL AS DECIMAL),
      IAR.HIGHER_RANGE,
      IAR.LOWER_RANGE,
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS VARCHAR (8)),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS INT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS DECIMAL)  ,                -- Enhancement# MACR00075690
	  CAST (NULL AS VARCHAR (1)),
	  CAST (NULL AS VARCHAR (1)),
	   CAST (NULL AS VARCHAR (9)),
	   CAST (NULL AS INT),
	   CAST (NULL AS INT)
 FROM IMPORT_ACCESSORIAL_RATE IAR, IMPORT_ACCESSORIAL_PARAM_SET IAPS
WHERE IAR.PARAM_ID = IAPS.PARAM_ID AND IAPS.STATUS = 1
 UNION ALL
 SELECT IAPS.PARAM_ID,
      IAR.ACCESSORIAL_RATE_ID,
      IAR.TC_COMPANY_ID,
      IAR.MOT,
      CAST (NULL AS INT),
      IAR.EQUIPMENT_CODE,
      CAST (NULL AS INT),
      IAR.SERVICE_LEVEL,
      CAST (NULL AS INT),
      IAR.PROTECTION_LEVEL,
      CAST (NULL AS INT),
      IAR.ACCESSORIAL_CODE,
      IAR.RATE,
      IAR.MINIMUM_RATE,
      IAR.CURRENCY_CODE,
      IAR.IS_AUTO_APPROVE,
      IAR.EFFECTIVE_DT,
      IAR.EXPIRATION_DT,
      DECODE (IAR.STATUS, 4, 1, 0) HAS_ERRORS,
      IAR.PAYEE_CARRIER_CODE,
      CAST (NULL AS INT),
      IAR.MINIMUM_SIZE,
      IAR.MAXIMUM_SIZE,
      IAR.SIZE_UOM,
      CAST (NULL AS INT),
      IAR.ZONE_ID,
      IAR.IS_SHIPMENT_ACCESSORIAL,
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      IAR.LONGEST_DIMENSION,
      IAR.SECOND_LONGEST_DIMENSION,
      IAR.LENGTH_PLUS_GRITH,
      IAR.WEIGHT,
      IAR.BASE_AMOUNT,
      IAR.CHARGE_PER_X_FIELD1,
      IAR.CHARGE_PER_X_FIELD2,
      CAST (NULL AS DECIMAL),
      IAR.HIGHER_RANGE,
      IAR.LOWER_RANGE,
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS VARCHAR (8)),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS INT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS DECIMAL)   ,               -- Enhancement# MACR00075690
	  CAST (NULL AS VARCHAR (1)),
	   CAST (NULL AS VARCHAR (1)),
	 CAST (NULL AS VARCHAR (9)),
	 CAST (NULL AS INT),
	   CAST (NULL AS INT)
 FROM IMPORT_ACCESSORIAL_RATE IAR, IMPORT_ACCESSORIAL_PARAM_SET IAPS
WHERE IAR.PARAM_ID = IAPS.PARAM_ID AND IAPS.STATUS = 4;

CREATE OR REPLACE FORCE VIEW "ALLOC_INVN_DTL_VIEW" ("ALLOC_INVN_DTL_ID", "CASE_NBR", "PLT_ID", "INVN_NEED_TYPE", "WHSE", "TASK_GENRTN_REF_CODE", "TASK_GENRTN_REF_NBR", "STAT_CODE", "TASK_BATCH", "PKT_CTRL_NBR") AS
select a.alloc_invn_dtl_id,
      cast (null as varchar (20)) as case_nbr,
      a.cntr_nbr as plt_id,
      a.invn_need_type,
      a.whse,
      a.task_genrtn_ref_code,
      a.task_genrtn_ref_nbr,
      a.stat_code,
      a.task_batch,
      a.pkt_ctrl_nbr
 from alloc_invn_dtl a
where a.alloc_uom = 'P'
      and exists
             (select 1
                from lpn c
               where a.cntr_nbr = c.tc_parent_lpn_id
                     and c.inbound_outbound_indicator = 'I')
 union all
 select a.alloc_invn_dtl_id,
      a.cntr_nbr as case_nbr,
      cast (null as varchar (20)) as plt_id,
      a.invn_need_type,
      a.whse,
      a.task_genrtn_ref_code,
      a.task_genrtn_ref_nbr,
      a.stat_code,
      a.task_batch,
      a.pkt_ctrl_nbr
 from alloc_invn_dtl a
where (a.alloc_uom > 'P' or a.alloc_uom < 'P')
      and exists
             (select 1
                from lpn c
               where a.cntr_nbr = c.tc_lpn_id
                     and c.inbound_outbound_indicator = 'I');

CREATE OR REPLACE FORCE VIEW "ASN_DETAIL_VIEW" ("ASN_DETAIL_ID", "ASN_ID", "TC_PURCHASE_ORDERS_ID", "PURCHASE_ORDERS_ID", "SKU_ID", "SKU_NAME", "SKU_ATTR_1", "SKU_ATTR_2", "SKU_ATTR_3", "SKU_ATTR_4", "SKU_ATTR_5", "BUSINESS_PARTNER_ID", "PACKAGE_TYPE_ID", "PACKAGE_TYPE_DESC", "PACKAGE_TYPE_INSTANCE", "EPC_TRACKING_RFID_VALUE", "ORDER_TYPE_DESC", "GTIN", "SHIPPED_QTY", "STD_PACK_QTY", "STD_CASE_QTY", "ASN_DETAIL_STATUS", "RECEIVED_QTY", "STD_SUB_PACK_QTY", "LPN_PER_TIER", "TIER_PER_PALLET", "MFG_PLNT", "MFG_DATE", "SHIP_BY_DATE", "EXPIRE_DATE", "INCUBATION_DATE", "EPC_REQ_ON_ALL_CASES", "REGION_ID", "IS_ASSOCIATED_TO_OUTBOUND", "IS_CANCELLED", "IS_CLOSED", "INVN_TYPE", "PROD_STAT", "BATCH_NBR", "CNTRY_OF_ORGN", "SHIPPED_LPN_COUNT", "RECEIVED_LPN_COUNT", "UNITS_ASSIGNED_TO_LPN", "ACTUAL_WEIGHT_RECEIVED", "PROC_IMMD_NEEDS", "QUALITY_CHECK_HOLD_UPON_RCPT", "REFERENCE_ORDER_NBR", "ACTUAL_WEIGHT", "ACTUAL_WEIGHT_PACK_COUNT", "NBR_OF_PACK_FOR_CATCH_WT", "PUTWY_TYPE", "RETAIL_PRICE", "PRICE_TIX_AVAIL", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "HIBERNATE_VERSION", "TC_COMPANY_ID", "TC_PO_LINE_ID") AS
SELECT asn_detail_id,
asn_id,
tc_purchase_orders_id,
purchase_orders_id,
sku_id,
sku_name,
sku_attr_1,
sku_attr_2,
sku_attr_3,
sku_attr_4,
sku_attr_5, --po_size_value,
business_partner_id,
package_type_id,
package_type_desc,
package_type_instance,
epc_tracking_rfid_value,
order_type_desc,
gtin,
shipped_qty, --shipped_qty_uom,
std_pack_qty,
std_case_qty,
asn_detail_status,
received_qty,
--received_qty_uom, po_size_uom_id, shipped_qty_uom_id, received_qty_uom_id,
std_sub_pack_qty,
lpn_per_tier,
tier_per_pallet,
mfg_plnt,
mfg_date,
ship_by_date,
expire_date,
incubation_date,
epc_req_on_all_cases,
--catch_weight, catch_weight_uom_id, total_weight, total_weight_uom_id,
region_id,
is_associated_to_outbound,
is_cancelled,
is_closed,
invn_type,
prod_stat,
batch_nbr,
cntry_of_orgn,
shipped_lpn_count,
received_lpn_count,
units_assigned_to_lpn,
actual_weight_received,
proc_immd_needs,
quality_check_hold_upon_rcpt,
reference_order_nbr,
actual_weight,
actual_weight_pack_count,
nbr_of_pack_for_catch_wt,
putwy_type,
 --   item_dimensions_checked_flag,
retail_price,
price_tix_avail,
created_source_type,
created_source,
created_dttm,
last_updated_source_type,
last_updated_source,
last_updated_dttm,
hibernate_version,
tc_company_id,
tc_po_line_id
FROM asn_detail
WHERE asn_id IN
(SELECT asn_id FROM asn_view
);

CREATE OR REPLACE FORCE VIEW "ASN_DWNLOAD" ("INB_TC_LPN_ID", "INB_LPN_ID", "INB_C_FACILITY_ID", "INB_INBOUND_OUTBOUND_INDICATOR", "INB_CURR_SUB_LOCN_ID", "OBN_TC_LPN_ID", "OBN_LPN_ID", "OBN_INBOUND_OUTBOUND_INDICATOR", "ASN_TC_ASN_ID", "ASN_ASN_ID", "ADT_ASN_ID", "ADT_STD_PACK_QTY", "ADT_SKU_ID", "ADT_DISPOSITION_TYPE", "ADT_PRE_RECEIPT_STATUS", "ADT_SHIPPED_QTY", "ASN_TC_SHIPMENT_ID", "ITEM_NAME", "ITEM_DESCRIPTION", "DTL_QUANTITY", "PACK_QTY", "IW_STORE_DEPT", "ORDERS_D_FACILITY_ALIAS_ID", "ORDERS_ORDER_CONSOL_LOCN_ID", "ASN_DEST_FACILITY_ALIAS_ID", "CONSOL_LOCN_PKT_CONSOL_ATTR", "ORDERS_PRIORITY", "ASN_ORIGIN_FACILITY_ALIAS_ID", "ASN_BUSINESS_PARTNER_ID", "ASN_ASN_TYPE", "ASN_ASN_STATUS", "ASN_ORIGIN_FACILITY_ID", "ASN_DESTINATION_FACILITY_ID", "ASN_APPOINTMENT_ID", "ASN_TOTAL_SHIPPED_QTY", "ASN_TOTAL_RECEIVED_QTY", "ASN_ASSIGNED_CARRIER_CODE", "ASN_BILL_OF_LADING_NUMBER", "ASN_PRO_NUMBER", "ASN_TOTAL_VOLUME", "ASN_INBOUND_REGION_ID", "ASN_OUTBOUND_REGION_ID", "ASN_REGION_ID", "ASN_ASN_LEVEL", "ASN_HAS_IMPORT_ERROR", "ASN_HAS_SOFT_CHECK_ERROR", "ASN_HAS_ALERTS", "ASN_SYSTEM_ALLOCATED", "ASN_ASN_PRIORITY", "ASN_IS_CANCELLED", "ASN_IS_CLOSED", "ASN_MANIF_NBR", "ASN_WORK_ORD_NBR", "ASN_SHIPPED_LPN_COUNT", "ASN_RECEIVED_LPN_COUNT", "ASN_INITIATE_FLAG", "ASN_SHIPMENT_ID", "ASN_ALLOCATION_COMPLETED", "ASN_ASN_ORGN_TYPE", "ASN_MHE_SENT", "ASN_FLOW_THR_ALLOC_METHOD", "ASN_TRAILER_NUMBER", "ASN_DESTINATION_TYPE", "ASN_ORIGINAL_ASN_ID", "ASN_PRE_RECEIPT_STATUS", "ASN_PRE_ALLOC_FIT_PERCENTAGE", "ADT_ASN_DETAIL_ID", "ADT_TC_PURCHASE_ORDERS_ID", "ADT_PURCHASE_ORDERS_ID", "ADT_BUSINESS_PARTNER_ID", "ADT_SKU_ATTR_1", "ADT_SKU_ATTR_2", "ADT_SKU_ATTR_3", "ADT_SKU_ATTR_4", "ADT_SKU_ATTR_5", "ADT_STD_CASE_QTY", "ADT_RECEIVED_QTY", "ADT_STD_SUB_PACK_QTY", "ADT_LPN_PER_TIER", "ADT_IS_CANCELLED", "ADT_IS_CLOSED", "ADT_INVN_TYPE", "ADT_PROD_STAT", "ADT_BATCH_NBR", "ADT_CNTRY_OF_ORGN", "ADT_SHIPPED_LPN_COUNT", "ADT_RECEIVED_LPN_COUNT", "ADT_UNITS_ASSIGNED_TO_LPN", 
"ADT_TC_COMPANY_ID", "ADT_TC_PO_LINE_ID", "ADT_TC_ORDER_ID", "ADT_ORDER_ID", "ADT_TC_ORDER_LINE_ID", "ADT_ORDER_LINE_ITEM_ID", "INB_BUSINESS_PARTNER_ID", "OBN_BUSINESS_PARTNER_ID", "INB_TC_COMPANY_ID", "INB_PARENT_LPN_ID", "INB_TC_PARENT_LPN_ID", "INB_LPN_TYPE", "OBN_TC_COMPANY_ID", "OBN_PARENT_LPN_ID", "OBN_TC_PARENT_LPN_ID", "OBN_LPN_TYPE", "INB_LPN_FACILITY_STATUS", "INB_TC_REFERENCE_LPN_ID", "INB_TC_ORDER_ID", "INB_ORIG_TC_SHIPMENT_ID", "OBN_LPN_FACILITY_STATUS", "OBN_TC_REFERENCE_LPN_ID", "OBN_TC_ORDER_ID", "OBN_ORIG_TC_SHIPMENT_ID", "INB_TC_PURCHASE_ORDERS_ID", "INB_PURCHASE_ORDERS_ID", "INB_TC_SHIPMENT_ID", "INB_SHIPMENT_ID", "INB_TC_ASN_ID", "INB_ASN_ID", "OBN_TC_PURCHASE_ORDERS_ID", "OBN_PURCHASE_ORDERS_ID", "OBN_TC_SHIPMENT_ID", "OBN_SHIPMENT_ID", "OBN_TC_ASN_ID", "OBN_ASN_ID", "INB_ESTIMATED_VOLUME", "INB_WEIGHT", "INB_ACTUAL_VOLUME", "INB_SPLIT_LPN_ID", "INB_C_FACILITY_ALIAS_ID", "INB_LPN_SIZE_TYPE_ID", "INB_PICK_SUB_LOCN_ID", "INB_PREV_SUB_LOCN_ID", "INB_DEST_SUB_LOCN_ID", "INB_WORK_ORD_NBR", "INB_INTERNAL_ORDER_ID", "INB_PACK_WAVE_NBR", "INB_WAVE_NBR", "INB_WAVE_SEQ_NBR", "INB_WAVE_STAT_CODE", "INB_DIRECTED_QTY", "INB_TRANS_INVN_TYPE", "INB_CONS_PRTY_DTTM", "INB_PUTAWAY_TYPE", "INB_SINGLE_LINE_LPN", "INB_ITEM_ID", "INB_D_FACILITY_ID", "INB_D_FACILITY_ALIAS_ID", "INB_PLANNED_TC_ASN_ID", "INB_ESTIMATED_WEIGHT", "INB_ORDER_ID") AS
(SELECT DISTINCT
       INB.tc_lpn_id AS INB_TC_LPN_ID,
       INB.lpn_id AS INB_LPN_ID,
       INB.c_facility_id AS INB_C_FACILITY_ID,
       INB.inbound_outbound_indicator AS INB_INBOUND_OUTBOUND_INDICATOR,
       INB.curr_sub_locn_id AS INB_CURR_SUB_LOCN_ID,
       OBN.tc_lpn_id AS OBN_TC_LPN_ID,
       OBN.lpn_id AS OBN_LPN_ID,
       OBN.inbound_outbound_indicator AS OBN_INBOUND_OUTBOUND_INDICATOR,
       asn.tc_asn_id AS ASN_TC_ASN_ID,
       asn.asn_id AS ASN_ASN_ID,
       ADT.asn_id AS ADT_ASN_ID,
       ADT.std_pack_qty AS ADT_STD_PACK_QTY,
       ADT.sku_id AS ADT_SKU_ID,
       ADT.disposition_type AS ADT_DISPOSITION_TYPE,
       ADT.pre_receipt_status AS ADT_PRE_RECEIPT_STATUS,
       ADT.shipped_qty AS ADT_SHIPPED_QTY,
       asn.tc_shipment_id AS ASN_TC_SHIPMENT_ID,
       IC.item_name AS ITEM_NAME,
       ic.description AS ITEM_DESCRIPTION,
       ld.size_value AS DTL_QUANTITY,
       ld.std_pack_qty AS PACK_QTY,
       IW.store_dept AS IW_STORE_DEPT,
       orders.d_facility_alias_id AS ORDERS_d_facility_alias_id,
       orders.order_consol_locn_id AS orders_ORDER_CONSOL_LOCN_ID,
       asn.destination_facility_alias_id AS ASN_DEST_FACILITY_ALIAS_ID,
       pkt_consol_locn.pkt_consol_attr AS CONSOL_LOCN_PKT_CONSOL_ATTR,
       orders.priority AS ORDERS_PRIORITY,
       asn.origin_facility_alias_id AS ASN_ORIGIN_FACILITY_ALIAS_ID,
       asn.business_partner_id AS ASN_BUSINESS_PARTNER_ID,
       asn.asn_type AS ASN_ASN_TYPE,
       asn.asn_status AS ASN_ASN_STATUS,
       asn.origin_facility_id AS ASN_ORIGIN_FACILITY_ID,
       asn.destination_facility_id AS ASN_DESTINATION_FACILITY_ID,
       asn.appointment_id AS ASN_APPOINTMENT_ID,
       asn.total_shipped_qty AS ASN_TOTAL_SHIPPED_QTY,
       asn.total_received_qty AS ASN_TOTAL_RECEIVED_QTY,
       asn.assigned_carrier_code AS ASN_ASSIGNED_CARRIER_CODE,
       asn.bill_of_lading_number AS ASN_BILL_OF_LADING_NUMBER,
       asn.pro_number AS ASN_PRO_NUMBER,
       asn.total_volume AS ASN_TOTAL_VOLUME,
       asn.inbound_region_id AS ASN_INBOUND_REGION_ID,
       asn.outbound_region_id AS ASN_OUTBOUND_REGION_ID,
       asn.region_id AS ASN_REGION_ID,
       asn.asn_level AS ASN_ASN_LEVEL,
       asn.has_import_error AS ASN_HAS_IMPORT_ERROR,
       asn.has_soft_check_error AS ASN_HAS_SOFT_CHECK_ERROR,
       asn.has_alerts AS ASN_HAS_ALERTS,
       asn.system_allocated AS ASN_SYSTEM_ALLOCATED,
       asn.asn_priority AS ASN_ASN_PRIORITY,
       asn.is_cancelled AS ASN_IS_CANCELLED,
       asn.is_closed AS ASN_IS_CLOSED,
       asn.manif_nbr AS ASN_MANIF_NBR,
       asn.work_ord_nbr AS ASN_WORK_ORD_NBR,
       asn.shipped_lpn_count AS ASN_SHIPPED_LPN_COUNT,
       asn.received_lpn_count AS ASN_RECEIVED_LPN_COUNT,
       asn.initiate_flag AS ASN_INITIATE_FLAG,
       asn.shipment_id AS ASN_SHIPMENT_ID,
       asn.allocation_completed AS ASN_ALLOCATION_COMPLETED,
       asn.asn_orgn_type AS ASN_ASN_ORGN_TYPE,
       asn.mhe_sent AS ASN_MHE_SENT,
       asn.flow_through_allocation_method AS ASN_FLOW_THR_ALLOC_METHOD,
       asn.trailer_number AS ASN_TRAILER_NUMBER,
       asn.destination_type AS ASN_DESTINATION_TYPE,
       asn.original_asn_id AS ASN_ORIGINAL_ASN_ID,
       asn.pre_receipt_status AS ASN_PRE_RECEIPT_STATUS,
       asn.pre_allocation_fit_percentage AS ASN_PRE_ALLOC_FIT_PERCENTAGE,
       ADT.asn_detail_id AS ADT_ASN_DETAIL_ID,
       ADT.tc_purchase_orders_id AS ADT_TC_PURCHASE_ORDERS_ID,
       ADT.purchase_orders_id AS ADT_PURCHASE_ORDERS_ID,
       ADT.business_partner_id AS ADT_BUSINESS_PARTNER_ID,
       ADT.sku_attr_1 AS ADT_SKU_ATTR_1,
       ADT.sku_attr_2 AS ADT_SKU_ATTR_2,
       ADT.sku_attr_3 AS ADT_SKU_ATTR_3,
       ADT.sku_attr_4 AS ADT_SKU_ATTR_4,
       ADT.sku_attr_5 AS ADT_SKU_ATTR_5,
       ADT.std_case_qty AS ADT_STD_CASE_QTY,
       ADT.received_qty AS ADT_RECEIVED_QTY,
       ADT.std_sub_pack_qty AS ADT_STD_SUB_PACK_QTY,
       ADT.lpn_per_tier AS ADT_LPN_PER_TIER,
       ADT.is_cancelled AS ADT_IS_CANCELLED,
       ADT.is_closed AS ADT_IS_CLOSED,
       ADT.invn_type AS ADT_INVN_TYPE,
       ADT.prod_stat AS ADT_PROD_STAT,
       ADT.batch_nbr AS ADT_BATCH_NBR,
       ADT.cntry_of_orgn AS ADT_CNTRY_OF_ORGN,
       ADT.shipped_lpn_count AS ADT_SHIPPED_LPN_COUNT,
       ADT.received_lpn_count AS ADT_RECEIVED_LPN_COUNT,
       ADT.units_assigned_to_lpn AS ADT_UNITS_ASSIGNED_TO_LPN,
       ADT.tc_company_id AS ADT_TC_COMPANY_ID,
       ADT.tc_po_line_id AS ADT_TC_PO_LINE_ID,
       ADT.tc_order_id AS ADT_TC_ORDER_ID,
       ADT.order_id AS ADT_ORDER_ID,
       ADT.tc_order_line_id AS ADT_TC_ORDER_LINE_ID,
       ADT.order_line_item_id AS ADT_ORDER_LINE_ITEM_ID,
       INB.business_partner_id AS INB_BUSINESS_PARTNER_ID,
       OBN.business_partner_id AS OBN_BUSINESS_PARTNER_ID,
       INB.tc_company_id AS INB_TC_COMPANY_ID,
       INB.parent_lpn_id AS INB_PARENT_LPN_ID,
       INB.tc_parent_lpn_id AS INB_TC_PARENT_LPN_ID,
       INB.lpn_type AS INB_LPN_TYPE,
       OBN.tc_company_id AS OBN_TC_COMPANY_ID,
       OBN.parent_lpn_id AS OBN_PARENT_LPN_ID,
       OBN.tc_parent_lpn_id AS OBN_TC_PARENT_LPN_ID,
       OBN.lpn_type AS OBN_LPN_TYPE,
       INB.lpn_facility_status AS INB_LPN_FACILITY_STATUS,
       INB.tc_reference_lpn_id AS INB_TC_REFERENCE_LPN_ID,
       INB.tc_order_id AS INB_TC_ORDER_ID,
       INB.original_tc_shipment_id AS INB_ORIG_TC_SHIPMENT_ID,
       OBN.lpn_facility_status AS OBN_LPN_FACILITY_STATUS,
       OBN.tc_reference_lpn_id AS OBN_TC_REFERENCE_LPN_ID,
       OBN.tc_order_id AS OBN_TC_ORDER_ID,
       OBN.original_tc_shipment_id AS OBN_ORIG_TC_SHIPMENT_ID,
       INB.tc_purchase_orders_id AS INB_TC_PURCHASE_ORDERS_ID,
       INB.purchase_orders_id AS INB_PURCHASE_ORDERS_ID,
       INB.tc_shipment_id AS INB_TC_SHIPMENT_ID,
       INB.shipment_id AS INB_SHIPMENT_ID,
       INB.tc_asn_id AS INB_TC_ASN_ID,
       INB.asn_id AS INB_ASN_ID,
       OBN.tc_purchase_orders_id AS OBN_TC_PURCHASE_ORDERS_ID,
       OBN.purchase_orders_id AS OBN_PURCHASE_ORDERS_ID,
       OBN.tc_shipment_id AS OBN_TC_SHIPMENT_ID,
       OBN.shipment_id AS OBN_SHIPMENT_ID,
       OBN.tc_asn_id AS OBN_TC_ASN_ID,
       OBN.asn_id AS OBN_ASN_ID,
       INB.estimated_volume AS INB_ESTIMATED_VOLUME,
       INB.weight AS INB_WEIGHT,
       INB.actual_volume AS INB_ACTUAL_VOLUME,
       INB.split_lpn_id AS INB_SPLIT_LPN_ID,
       INB.c_facility_alias_id AS INB_C_FACILITY_ALIAS_ID,
       INB.lpn_size_type_id AS INB_LPN_SIZE_TYPE_ID,
       INB.pick_sub_locn_id AS INB_PICK_SUB_LOCN_ID,
       INB.prev_sub_locn_id AS INB_PREV_SUB_LOCN_ID,
       INB.dest_sub_locn_id AS INB_DEST_SUB_LOCN_ID,
       INB.work_ord_nbr AS INB_WORK_ORD_NBR,
       INB.internal_order_id AS INB_INTERNAL_ORDER_ID,
       INB.pack_wave_nbr AS INB_PACK_WAVE_NBR,
       INB.wave_nbr AS INB_WAVE_NBR,
       INB.wave_seq_nbr AS INB_WAVE_SEQ_NBR,
       INB.wave_stat_code AS INB_WAVE_STAT_CODE,
       INB.directed_qty AS INB_DIRECTED_QTY,
       INB.transitional_inventory_type AS INB_TRANS_INVN_TYPE,
       INB.consumption_priority_dttm AS INB_CONS_PRTY_DTTM,
       INB.putaway_type AS INB_PUTAWAY_TYPE,
       INB.single_line_lpn AS INB_SINGLE_LINE_LPN,
       INB.item_id AS INB_ITEM_ID,
       INB.d_facility_id AS INB_D_FACILITY_ID,
       INB.d_facility_alias_id AS INB_D_FACILITY_ALIAS_ID,
       INB.planned_tc_asn_id AS INB_PLANNED_TC_ASN_ID,
       INB.estimated_weight AS INB_ESTIMATED_WEIGHT,
       INB.order_id AS INB_ORDER_ID
  FROM lpn INB,
       lpn OBN,
       asn ASN,
       asn_detail ADT,
       item_cbo IC,
       lpn_detail LD,
       item_wms IW,
       orders ORDERS,
       pkt_consol_locn PKT_CONSOL_LOCN
 WHERE     INB.tc_lpn_id = OBN.tc_reference_lpn_id
       AND asn.tc_asn_id = INB.tc_asn_id
       AND asn.asn_id = ADT.asn_id
       AND asn.HAS_IMPORT_ERROR = 0
       AND adt.pre_receipt_status IN (30, 40, 75, 80, 90)
       AND adt.IS_CANCELLED = 0
       AND adt.IS_CLOSED = 0
       AND obn.inbound_outbound_indicator = 'O'
       AND OBN.lpn_id = LD.lpn_id
       AND LD.item_id = IC.item_id
       AND ADT.sku_id = LD.item_id
       AND ADT.sku_id = IC.item_id
       AND IW.item_id = IC.item_id
       AND LD.item_id = IW.item_id
       AND ADT.sku_id = IW.item_id
       AND OBN.order_id = orders.order_id(+)
       AND LD.item_id = ADT.sku_id
       AND OBN.dest_sub_locn_id = pkt_consol_locn.locn_id(+)
       AND inb.inbound_outbound_indicator = 'I'
UNION
SELECT DISTINCT
       INB.tc_lpn_id AS INB_TC_LPN_ID,
       INB.lpn_id AS INB_LPN_ID,
       INB.c_facility_id AS INB_C_FACILITY_ID,
       INB.inbound_outbound_indicator AS INB_INBOUND_OUTBOUND_INDICATOR,
       INB.curr_sub_locn_id AS INB_CURR_SUB_LOCN_ID,
       '' AS OBN_TC_LPN_ID,
       0 AS OBN_LPN_ID,
       '' AS OBN_INBOUND_OUTBOUND_INDICATOR,
       asn.tc_asn_id AS ASN_TC_ASN_ID,
       asn.asn_id AS ASN_ASN_ID,
       ADT.asn_id AS ADT_ASN_ID,
       ADT.std_pack_qty AS ADT_STD_PACK_QTY,
       ADT.sku_id AS ADT_SKU_ID,
       ADT.disposition_type AS ADT_DISPOSITION_TYPE,
       ADT.pre_receipt_status AS ADT_PRE_RECEIPT_STATUS,
       ADT.shipped_qty AS ADT_SHIPPED_QTY,
       asn.tc_shipment_id AS ASN_TC_SHIPMENT_ID,
       IC.item_name AS ITEM_NAME,
       ic.description AS ITEM_DESCRIPTION,
       ld.size_value AS DTL_QUANTITY,
       ld.std_pack_qty AS PACK_QTY,
       IW.store_dept AS IW_STORE_DEPT,
       '' AS ORDERS_d_facility_alias_id,
       '' AS orders_ORDER_CONSOL_LOCN_ID,
       asn.destination_facility_alias_id AS ASN_DEST_FACILITY_ALIAS_ID,
       '' AS CONSOL_LOCN_PKT_CONSOL_ATTR,
       0 AS ORDERS_PRIORITY,
       asn.origin_facility_alias_id AS ASN_ORIGIN_FACILITY_ALIAS_ID,
       asn.business_partner_id AS ASN_BUSINESS_PARTNER_ID,
       asn.asn_type AS ASN_ASN_TYPE,
       asn.asn_status AS ASN_ASN_STATUS,
       asn.origin_facility_id AS ASN_ORIGIN_FACILITY_ID,
       asn.destination_facility_id AS ASN_DESTINATION_FACILITY_ID,
       asn.appointment_id AS ASN_APPOINTMENT_ID,
       asn.total_shipped_qty AS ASN_TOTAL_SHIPPED_QTY,
       asn.total_received_qty AS ASN_TOTAL_RECEIVED_QTY,
       asn.assigned_carrier_code AS ASN_ASSIGNED_CARRIER_CODE,
       asn.bill_of_lading_number AS ASN_BILL_OF_LADING_NUMBER,
       asn.pro_number AS ASN_PRO_NUMBER,
       asn.total_volume AS ASN_TOTAL_VOLUME,
       asn.inbound_region_id AS ASN_INBOUND_REGION_ID,
       asn.outbound_region_id AS ASN_OUTBOUND_REGION_ID,
       asn.region_id AS ASN_REGION_ID,
       asn.asn_level AS ASN_ASN_LEVEL,
       asn.has_import_error AS ASN_HAS_IMPORT_ERROR,
       asn.has_soft_check_error AS ASN_HAS_SOFT_CHECK_ERROR,
       asn.has_alerts AS ASN_HAS_ALERTS,
       asn.system_allocated AS ASN_SYSTEM_ALLOCATED,
       asn.asn_priority AS ASN_ASN_PRIORITY,
       asn.is_cancelled AS ASN_IS_CANCELLED,
       asn.is_closed AS ASN_IS_CLOSED,
       asn.manif_nbr AS ASN_MANIF_NBR,
       asn.work_ord_nbr AS ASN_WORK_ORD_NBR,
       asn.shipped_lpn_count AS ASN_SHIPPED_LPN_COUNT,
       asn.received_lpn_count AS ASN_RECEIVED_LPN_COUNT,
       asn.initiate_flag AS ASN_INITIATE_FLAG,
       asn.shipment_id AS ASN_SHIPMENT_ID,
       asn.allocation_completed AS ASN_ALLOCATION_COMPLETED,
       asn.asn_orgn_type AS ASN_ASN_ORGN_TYPE,
       asn.mhe_sent AS ASN_MHE_SENT,
       asn.flow_through_allocation_method AS ASN_FLOW_THR_ALLOC_METHOD,
       asn.trailer_number AS ASN_TRAILER_NUMBER,
       asn.destination_type AS ASN_DESTINATION_TYPE,
       asn.original_asn_id AS ASN_ORIGINAL_ASN_ID,
       asn.pre_receipt_status AS ASN_PRE_RECEIPT_STATUS,
       asn.pre_allocation_fit_percentage AS ASN_PRE_ALLOC_FIT_PERCENTAGE,
       ADT.asn_detail_id AS ADT_ASN_DETAIL_ID,
       ADT.tc_purchase_orders_id AS ADT_TC_PURCHASE_ORDERS_ID,
       ADT.purchase_orders_id AS ADT_PURCHASE_ORDERS_ID,
       ADT.business_partner_id AS ADT_BUSINESS_PARTNER_ID,
       ADT.sku_attr_1 AS ADT_SKU_ATTR_1,
       ADT.sku_attr_2 AS ADT_SKU_ATTR_2,
       ADT.sku_attr_3 AS ADT_SKU_ATTR_3,
       ADT.sku_attr_4 AS ADT_SKU_ATTR_4,
       ADT.sku_attr_5 AS ADT_SKU_ATTR_5,
       ADT.std_case_qty AS ADT_STD_CASE_QTY,
       ADT.received_qty AS ADT_RECEIVED_QTY,
       ADT.std_sub_pack_qty AS ADT_STD_SUB_PACK_QTY,
       ADT.lpn_per_tier AS ADT_LPN_PER_TIER,
       ADT.is_cancelled AS ADT_IS_CANCELLED,
       ADT.is_closed AS ADT_IS_CLOSED,
       ADT.invn_type AS ADT_INVN_TYPE,
       ADT.prod_stat AS ADT_PROD_STAT,
       ADT.batch_nbr AS ADT_BATCH_NBR,
       ADT.cntry_of_orgn AS ADT_CNTRY_OF_ORGN,
       ADT.shipped_lpn_count AS ADT_SHIPPED_LPN_COUNT,
       ADT.received_lpn_count AS ADT_RECEIVED_LPN_COUNT,
       ADT.units_assigned_to_lpn AS ADT_UNITS_ASSIGNED_TO_LPN,
       ADT.tc_company_id AS ADT_TC_COMPANY_ID,
       ADT.tc_po_line_id AS ADT_TC_PO_LINE_ID,
       ADT.tc_order_id AS ADT_TC_ORDER_ID,
       ADT.order_id AS ADT_ORDER_ID,
       ADT.tc_order_line_id AS ADT_TC_ORDER_LINE_ID,
       ADT.order_line_item_id AS ADT_ORDER_LINE_ITEM_ID,
       INB.business_partner_id AS INB_BUSINESS_PARTNER_ID,
       '' AS OBN_BUSINESS_PARTNER_ID,
       INB.tc_company_id AS INB_TC_COMPANY_ID,
       INB.parent_lpn_id AS INB_PARENT_LPN_ID,
       INB.tc_parent_lpn_id AS INB_TC_PARENT_LPN_ID,
       INB.lpn_type AS INB_LPN_TYPE,
       0 AS OBN_TC_COMPANY_ID,
       0 AS OBN_PARENT_LPN_ID,
       '' AS OBN_TC_PARENT_LPN_ID,
       0 AS OBN_LPN_TYPE,
       INB.lpn_facility_status AS INB_LPN_FACILITY_STATUS,
       INB.tc_reference_lpn_id AS INB_TC_REFERENCE_LPN_ID,
       INB.tc_order_id AS INB_TC_ORDER_ID,
       INB.original_tc_shipment_id AS INB_ORIG_TC_SHIPMENT_ID,
       -1 AS OBN_LPN_FACILITY_STATUS,
       '' AS OBN_TC_REFERENCE_LPN_ID,
       '' AS OBN_TC_ORDER_ID,
       '' AS OBN_ORIG_TC_SHIPMENT_ID,
       INB.tc_purchase_orders_id AS INB_TC_PURCHASE_ORDERS_ID,
       INB.purchase_orders_id AS INB_PURCHASE_ORDERS_ID,
       INB.tc_shipment_id AS INB_TC_SHIPMENT_ID,
       INB.shipment_id AS INB_SHIPMENT_ID,
       INB.tc_asn_id AS INB_TC_ASN_ID,
       INB.asn_id AS INB_ASN_ID,
       '' AS OBN_TC_PURCHASE_ORDERS_ID,
       0 AS OBN_PURCHASE_ORDERS_ID,
       '' AS OBN_TC_SHIPMENT_ID,
       0 AS OBN_SHIPMENT_ID,
       '' AS OBN_TC_ASN_ID,
       0 AS OBN_ASN_ID,
       INB.estimated_volume AS INB_ESTIMATED_VOLUME,
       INB.weight AS INB_WEIGHT,
       INB.actual_volume AS INB_ACTUAL_VOLUME,
       INB.split_lpn_id AS INB_SPLIT_LPN_ID,
       INB.c_facility_alias_id AS INB_C_FACILITY_ALIAS_ID,
       INB.lpn_size_type_id AS INB_LPN_SIZE_TYPE_ID,
       INB.pick_sub_locn_id AS INB_PICK_SUB_LOCN_ID,
       INB.prev_sub_locn_id AS INB_PREV_SUB_LOCN_ID,
       INB.dest_sub_locn_id AS INB_DEST_SUB_LOCN_ID,
       INB.work_ord_nbr AS INB_WORK_ORD_NBR,
       INB.internal_order_id AS INB_INTERNAL_ORDER_ID,
       INB.pack_wave_nbr AS INB_PACK_WAVE_NBR,
       INB.wave_nbr AS INB_WAVE_NBR,
       INB.wave_seq_nbr AS INB_WAVE_SEQ_NBR,
       INB.wave_stat_code AS INB_WAVE_STAT_CODE,
       INB.directed_qty AS INB_DIRECTED_QTY,
       INB.transitional_inventory_type AS INB_TRANS_INVN_TYPE,
       INB.consumption_priority_dttm AS INB_CONS_PRTY_DTTM,
       INB.putaway_type AS INB_PUTAWAY_TYPE,
       INB.single_line_lpn AS INB_SINGLE_LINE_LPN,
       INB.item_id AS INB_ITEM_ID,
       INB.d_facility_id AS INB_D_FACILITY_ID,
       INB.d_facility_alias_id AS INB_D_FACILITY_ALIAS_ID,
       INB.planned_tc_asn_id AS INB_PLANNED_TC_ASN_ID,
       INB.estimated_weight AS INB_ESTIMATED_WEIGHT,
       INB.order_id AS INB_ORDER_ID
  FROM lpn INB,
       asn ASN,
       asn_detail ADT,
       item_cbo IC,
       lpn_detail LD,
       item_wms IW
 WHERE     asn.tc_asn_id = INB.tc_asn_id
       AND asn.asn_id = ADT.asn_id
       AND asn.HAS_IMPORT_ERROR = 0
       AND ADT.IS_CANCELLED = 0
       AND ADT.IS_CLOSED = 0
       AND INB.tc_lpn_id NOT IN
              (SELECT DISTINCT INB.tc_lpn_id
                 FROM lpn INB,
                      lpn OBN,
                      asn ASN,
                      asn_detail ADT,
                      item_cbo IC,
                      lpn_detail LD,
                      item_wms IW
                WHERE     INB.tc_lpn_id = OBN.tc_reference_lpn_id
                      AND asn.tc_asn_id = INB.tc_asn_id
                      AND asn.asn_id = ADT.asn_id
                      AND asn.HAS_IMPORT_ERROR = 0
                      AND adt.pre_receipt_status IN (30, 40, 75, 80, 90)
                      AND adt.IS_CANCELLED = 0
                      AND adt.IS_CLOSED = 0
                      AND obn.inbound_outbound_indicator = 'O'
                      AND OBN.lpn_id = LD.lpn_id
                      AND LD.item_id = IC.item_id
                      AND ADT.sku_id = LD.item_id
                      AND ADT.sku_id = IC.item_id
                      AND IW.item_id = IC.item_id
                      AND LD.item_id = IW.item_id
                      AND ADT.sku_id = IW.item_id
                      AND LD.item_id = ADT.sku_id)
       AND adt.pre_receipt_status IN (30, 40, 75, 80, 90)
       AND INB.inbound_outbound_indicator = 'I'
       AND INB.lpn_id = LD.lpn_id
       AND LD.item_id = IC.item_id
       AND ADT.sku_id = LD.item_id
       AND ADT.sku_id = IC.item_id
       AND IW.item_id = IC.item_id
       AND LD.item_id = IW.item_id
       AND ADT.sku_id = IW.item_id
       AND LD.item_id = ADT.sku_id
UNION
	SELECT DISTINCT
        '' AS INB_TC_LPN_ID,
        0  AS INB_LPN_ID,
        0  AS INB_C_FACILITY_ID,
        '' AS INB_INBOUND_OUTBOUND_INDICATOR,
        '' AS INB_CURR_SUB_LOCN_ID,
        '' AS OBN_TC_LPN_ID,
        0  AS OBN_LPN_ID,
        '' AS OBN_INBOUND_OUTBOUND_INDICATOR,
        asn.tc_asn_id AS ASN_TC_ASN_ID,
        asn.asn_id AS ASN_ASN_ID,
        ADT.asn_id AS ADT_ASN_ID,
        ADT.std_pack_qty AS ADT_STD_PACK_QTY,
        ADT.sku_id AS ADT_SKU_ID,
        ADT.disposition_type AS ADT_DISPOSITION_TYPE,
        ADT.pre_receipt_status AS ADT_PRE_RECEIPT_STATUS,
        ADT.shipped_qty AS ADT_SHIPPED_QTY,
        asn.tc_shipment_id AS ASN_TC_SHIPMENT_ID,
        IC.item_name AS ITEM_NAME,
        ic.description AS ITEM_DESCRIPTION,
        adt.shipped_qty AS DTL_QUANTITY,
        adt.std_pack_qty AS PACK_QTY,
        IW.store_dept AS IW_STORE_DEPT,
        '' AS ORDERS_d_facility_alias_id,
        '' AS orders_ORDER_CONSOL_LOCN_ID,
        asn.destination_facility_alias_id AS ASN_DEST_FACILITY_ALIAS_ID,
        '' AS CONSOL_LOCN_PKT_CONSOL_ATTR,
        0 AS ORDERS_PRIORITY,
        asn.origin_facility_alias_id AS ASN_ORIGIN_FACILITY_ALIAS_ID,
        asn.business_partner_id AS ASN_BUSINESS_PARTNER_ID,
        asn.asn_type AS ASN_ASN_TYPE,
        asn.asn_status AS ASN_ASN_STATUS,
        asn.origin_facility_id AS ASN_ORIGIN_FACILITY_ID,
        asn.destination_facility_id AS ASN_DESTINATION_FACILITY_ID,
        asn.appointment_id AS ASN_APPOINTMENT_ID,
        asn.total_shipped_qty AS ASN_TOTAL_SHIPPED_QTY,
        asn.total_received_qty AS ASN_TOTAL_RECEIVED_QTY,
        asn.assigned_carrier_code AS ASN_ASSIGNED_CARRIER_CODE,
        asn.bill_of_lading_number AS ASN_BILL_OF_LADING_NUMBER,
        asn.pro_number AS ASN_PRO_NUMBER,
        asn.total_volume AS ASN_TOTAL_VOLUME,
        asn.inbound_region_id AS ASN_INBOUND_REGION_ID,
        asn.outbound_region_id AS ASN_OUTBOUND_REGION_ID,
        asn.region_id AS ASN_REGION_ID,
        asn.asn_level AS ASN_ASN_LEVEL,
        asn.has_import_error AS ASN_HAS_IMPORT_ERROR,
        asn.has_soft_check_error AS ASN_HAS_SOFT_CHECK_ERROR,
        asn.has_alerts AS ASN_HAS_ALERTS,
        asn.system_allocated AS ASN_SYSTEM_ALLOCATED,
        asn.asn_priority AS ASN_ASN_PRIORITY,
        asn.is_cancelled AS ASN_IS_CANCELLED,
        asn.is_closed AS ASN_IS_CLOSED,
        asn.manif_nbr AS ASN_MANIF_NBR,
        asn.work_ord_nbr AS ASN_WORK_ORD_NBR,
        asn.shipped_lpn_count AS ASN_SHIPPED_LPN_COUNT,
        asn.received_lpn_count AS ASN_RECEIVED_LPN_COUNT,
        asn.initiate_flag AS ASN_INITIATE_FLAG,
        asn.shipment_id AS ASN_SHIPMENT_ID,
        asn.allocation_completed AS ASN_ALLOCATION_COMPLETED,
        asn.asn_orgn_type AS ASN_ASN_ORGN_TYPE,
        asn.mhe_sent AS ASN_MHE_SENT,
        asn.flow_through_allocation_method AS ASN_FLOW_THR_ALLOC_METHOD,
        asn.trailer_number AS ASN_TRAILER_NUMBER,
        asn.destination_type AS ASN_DESTINATION_TYPE,
        asn.original_asn_id AS ASN_ORIGINAL_ASN_ID,
        asn.pre_receipt_status AS ASN_PRE_RECEIPT_STATUS,
        asn.pre_allocation_fit_percentage AS ASN_PRE_ALLOC_FIT_PERCENTAGE,
        ADT.asn_detail_id AS ADT_ASN_DETAIL_ID,
        ADT.tc_purchase_orders_id AS ADT_TC_PURCHASE_ORDERS_ID,
        ADT.purchase_orders_id AS ADT_PURCHASE_ORDERS_ID,
        ADT.business_partner_id AS ADT_BUSINESS_PARTNER_ID,
        ADT.sku_attr_1 AS ADT_SKU_ATTR_1,
        ADT.sku_attr_2 AS ADT_SKU_ATTR_2,
        ADT.sku_attr_3 AS ADT_SKU_ATTR_3,
        ADT.sku_attr_4 AS ADT_SKU_ATTR_4,
        ADT.sku_attr_5 AS ADT_SKU_ATTR_5,
        ADT.std_case_qty AS ADT_STD_CASE_QTY,
        ADT.received_qty AS ADT_RECEIVED_QTY,
        ADT.std_sub_pack_qty AS ADT_STD_SUB_PACK_QTY,
        ADT.lpn_per_tier AS ADT_LPN_PER_TIER,
        ADT.is_cancelled AS ADT_IS_CANCELLED,
        ADT.is_closed AS ADT_IS_CLOSED,
        ADT.invn_type AS ADT_INVN_TYPE,
        ADT.prod_stat AS ADT_PROD_STAT,
        ADT.batch_nbr AS ADT_BATCH_NBR,
        ADT.cntry_of_orgn AS ADT_CNTRY_OF_ORGN,
        ADT.shipped_lpn_count AS ADT_SHIPPED_LPN_COUNT,
        ADT.received_lpn_count AS ADT_RECEIVED_LPN_COUNT,
        ADT.units_assigned_to_lpn AS ADT_UNITS_ASSIGNED_TO_LPN,
        ADT.tc_company_id AS ADT_TC_COMPANY_ID,
        ADT.tc_po_line_id AS ADT_TC_PO_LINE_ID,
        ADT.tc_order_id AS ADT_TC_ORDER_ID,
        ADT.order_id AS ADT_ORDER_ID,
        ADT.tc_order_line_id AS ADT_TC_ORDER_LINE_ID,
        ADT.order_line_item_id AS ADT_ORDER_LINE_ITEM_ID,
        '' AS INB_BUSINESS_PARTNER_ID,
        '' AS OBN_BUSINESS_PARTNER_ID,
        0  AS INB_TC_COMPANY_ID,
        0  AS INB_PARENT_LPN_ID,
        '' AS INB_TC_PARENT_LPN_ID,
        0  AS INB_LPN_TYPE,
        0  AS OBN_TC_COMPANY_ID,
        0  AS OBN_PARENT_LPN_ID,
        '' AS OBN_TC_PARENT_LPN_ID,
        0  AS OBN_LPN_TYPE,
        0  AS INB_LPN_FACILITY_STATUS,
        '' AS INB_TC_REFERENCE_LPN_ID,
        '' AS INB_TC_ORDER_ID,
        '' AS INB_ORIG_TC_SHIPMENT_ID,
        -1 AS OBN_LPN_FACILITY_STATUS,
        '' AS OBN_TC_REFERENCE_LPN_ID,
        '' AS OBN_TC_ORDER_ID,
        '' AS OBN_ORIG_TC_SHIPMENT_ID,
        '' AS INB_TC_PURCHASE_ORDERS_ID,
        0  AS INB_PURCHASE_ORDERS_ID,
        '' AS INB_TC_SHIPMENT_ID,
        0  AS INB_SHIPMENT_ID,
        '' AS INB_TC_ASN_ID,
        0  AS INB_ASN_ID,
        '' AS OBN_TC_PURCHASE_ORDERS_ID,
        0  AS OBN_PURCHASE_ORDERS_ID,
        '' AS OBN_TC_SHIPMENT_ID,
        0  AS OBN_SHIPMENT_ID,
        '' AS OBN_TC_ASN_ID,
        0  AS OBN_ASN_ID,
        0  AS INB_ESTIMATED_VOLUME,
        0  AS INB_WEIGHT,
        0  AS INB_ACTUAL_VOLUME,
        0  AS INB_SPLIT_LPN_ID,
        '' AS INB_C_FACILITY_ALIAS_ID,
        0  AS INB_LPN_SIZE_TYPE_ID,
        '' AS INB_PICK_SUB_LOCN_ID,
        '' AS INB_PREV_SUB_LOCN_ID,
        '' AS INB_DEST_SUB_LOCN_ID,
        '' AS INB_WORK_ORD_NBR,
        '' AS INB_INTERNAL_ORDER_ID,
        '' AS INB_PACK_WAVE_NBR,
        '' AS INB_WAVE_NBR,
        0  AS INB_WAVE_SEQ_NBR,
        0  AS INB_WAVE_STAT_CODE,
        0  AS INB_DIRECTED_QTY,
        0  AS INB_TRANS_INVN_TYPE,
        SYSDATE AS INB_CONS_PRTY_DTTM,
        '' AS INB_PUTAWAY_TYPE,
        '' AS INB_SINGLE_LINE_LPN,
        0  AS INB_ITEM_ID,
        0  AS INB_D_FACILITY_ID,
        '' AS INB_D_FACILITY_ALIAS_ID,
        '' AS INB_PLANNED_TC_ASN_ID,
        0  AS INB_ESTIMATED_WEIGHT,
        0  AS INB_ORDER_ID
   FROM
        asn ASN,
        asn_detail ADT,
        item_cbo IC,
        item_wms IW
  WHERE
        asn.asn_id = ADT.asn_id
        AND asn.HAS_IMPORT_ERROR = 0
        AND ADT.sku_id = IC.item_id
        AND ADT.pre_receipt_status NOT IN (0, 30, 40, 75, 80, 90)
        AND ADT.IS_CANCELLED = 0
        AND ADT.IS_CLOSED = 0
        AND IC.item_id = IW.item_id
        AND ADT.sku_id = IW.item_id);

CREATE OR REPLACE FORCE VIEW "ASN_VIEW" ("ASN_ID", "TC_COMPANY_ID", "TC_ASN_ID", "TC_ASN_ID_U", "ASN_TYPE", "ASN_STATUS", "TRACTOR_NUMBER", "DELIVERY_STOP_SEQ", "ORIGIN_FACILITY_ALIAS_ID", "ORIGIN_FACILITY_ID", "PICKUP_END_DTTM", "ACTUAL_DEPARTURE_DTTM", "DESTINATION_FACILITY_ALIAS_ID", "DESTINATION_FACILITY_ID", "DELIVERY_START_DTTM", "DELIVERY_END_DTTM", "ACTUAL_ARRIVAL_DTTM", "RECEIPT_DTTM", "INBOUND_REGION_NAME", "OUTBOUND_REGION_NAME", "REF_FIELD_1", "REF_FIELD_2", "REF_FIELD_3", "TOTAL_WEIGHT", "APPOINTMENT_ID", "APPOINTMENT_DTTM", "APPOINTMENT_DURATION", "PALLET_FOOTPRINT", "LAST_TRANSMITTED_DTTM", "TOTAL_SHIPPED_QTY", "TOTAL_RECEIVED_QTY", "ASSIGNED_CARRIER_CODE", "BILL_OF_LADING_NUMBER", "PRO_NUMBER", "TRAILER_NUMBER", "EQUIPMENT_CODE", "EQUIPMENT_CODE_ID", "ASSIGNED_CARRIER_CODE_ID", "TOTAL_VOLUME", "FIRM_APPT_IND", "TRACTOR_CARRIER_CODE_ID", "TRACTOR_CARRIER_CODE", "BUYER_CODE", "REP_NAME", "CONTACT_ADDRESS_1", "CONTACT_ADDRESS_2", "CONTACT_ADDRESS_3", "CONTACT_CITY", "CONTACT_STATE_PROV", "CONTACT_ZIP", "CONTACT_NUMBER", "EXT_CREATED_DTTM", "INBOUND_REGION_ID", "OUTBOUND_REGION_ID", "REGION_ID", "ASN_LEVEL", "RECEIPT_VARIANCE", "HAS_IMPORT_ERROR", "HAS_SOFT_CHECK_ERROR", "HAS_ALERTS", "SYSTEM_ALLOCATED", "IS_COGI_GENERATED", "ASN_PRIORITY", "SCHEDULE_APPT", "IS_ASSOCIATED_TO_OUTBOUND", "IS_CANCELLED", "IS_CLOSED", "BUSINESS_PARTNER_ID", "MANIF_NBR", "MANIF_TYPE", "WORK_ORD_NBR", "CUT_NBR", "MFG_PLNT", "IS_WHSE_TRANSFER", "QUALITY_CHECK_HOLD_UPON_RCPT", "QUALITY_AUDIT_PERCENT", "SHIPPED_LPN_COUNT", "RECEIVED_LPN_COUNT", "ACTUAL_SHIPPED_DTTM", "LAST_RECEIPT_DTTM", "VERIFIED_DTTM", "LABEL_PRINT_REQD", "INITIATE_FLAG", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "HIBERNATE_VERSION", "SHIPMENT_ID", "TC_SHIPMENT_ID", "VERIFICATION_ATTEMPTED", "FLOW_THRU_ALLOC_IN_PROG", "ALLOCATION_COMPLETED", "ASN_ORGN_TYPE") AS
select asn_id,
      tc_company_id,
      tc_asn_id,
      tc_asn_id_u,
      asn_type,
      asn_status,
      tractor_number,
      delivery_stop_seq,
      origin_facility_alias_id,
      origin_facility_id,
      pickup_end_dttm,
      actual_departure_dttm,
      destination_facility_alias_id,
      destination_facility_id,
      delivery_start_dttm,
      delivery_end_dttm,
      actual_arrival_dttm,
      receipt_dttm,
      inbound_region_name,
      outbound_region_name,
      ref_field_1,
      ref_field_2,
      ref_field_3,
      total_weight,
      --total_weight_uom,
      appointment_id,
      appointment_dttm,
      appointment_duration,
      pallet_footprint,
      last_transmitted_dttm,
      total_shipped_qty,
      total_received_qty,
      --total_shipped_qty_uom, total_received_qty_uom, total_weight_uom_id, total_shipped_qty_uom_id, total_received_qty_uom_id,
      assigned_carrier_code,
      bill_of_lading_number,
      pro_number,
      trailer_number,
      equipment_code,
      equipment_code_id,
      assigned_carrier_code_id,
      total_volume,                                 --total_volume_uom_id,
      firm_appt_ind,
      tractor_carrier_code_id,
      tractor_carrier_code,
      buyer_code,
      rep_name,
      contact_address_1,
      contact_address_2,
      contact_address_3,
      contact_city,
      contact_state_prov,
      contact_zip,
      contact_number,
      ext_created_dttm,
      inbound_region_id,
      outbound_region_id,
      region_id,
      asn_level,
      receipt_variance,
      has_import_error,
      has_soft_check_error,
      has_alerts,
      system_allocated,
      is_cogi_generated,
      asn_priority,
      schedule_appt,
      is_associated_to_outbound,
      is_cancelled,
      is_closed,
      business_partner_id,
      manif_nbr,
      manif_type,
      work_ord_nbr,
      cut_nbr,
      mfg_plnt,
      is_whse_transfer,
      quality_check_hold_upon_rcpt,
      quality_audit_percent,
      shipped_lpn_count,
      received_lpn_count,
      actual_shipped_dttm,
      last_receipt_dttm,
      verified_dttm,
      label_print_reqd,
      initiate_flag,
      created_source_type,
      created_source,
      created_dttm,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      hibernate_version,
      shipment_id,
      tc_shipment_id,
      verification_attempted,
      flow_thru_alloc_in_prog,
      allocation_completed,
      asn_orgn_type
 from asn;

CREATE OR REPLACE FORCE VIEW "BASE_INC" ("EMP_ID", "LOGIN_USER_ID", "LAST_NAME", "FIRST_NAME", "PAY_DATE", "WHSE_DATE", "PAY_HOURS", "TOTAL_HOURLY_PAY", "ADJ_TOT_HRLY_PAY", "INC_PAY", "TOTAL_INC_PAY", "ADJ_TOTAL_INC_PAY", "INC_CODE", "INC_CODE_ID", "STATUS", "JOB_FUNC_ID", "JOB_FUNCTION__NAME", "SHIFT_ID", "SHIFT_CODE", "DEPT_ID", "DEPT_CODE", "SPVSR_EMP_ID", "SR_LOGIN_USER_ID", "PAY_TYPE", "PAY_TYPE_ID", "LABOR_TYPE_CODE", "LABOR_TYPE_ID", "BASE_INC_PAY", "PAY_ID", "LAST_MOD_BY", "MOD_LOGIN_USER_ID", "ADJ_APRV_USER_ID", "ADJ_REASON_CODE", "ADJ_COMMENT", "WHSE", "TIME_IND_ID") AS
select e.emp_id,
      e.login_user_id,
      e.last_name,
      e.first_name,
      e.pay_date,
      e.whse_date,
      e.pay_hours,
      e.total_hourly_pay,
      e.adj_tot_hrly_pay,
      e.inc_pay,
      e.total_inc_pay,
      e.adj_total_inc_pay,
      e.inc_code,
      e.inc_code_id,
      e.status,
      e.job_func_id,
      e.job_function__name,
      e.shift_id,
      e.shift_code,
      e.dept_id,
      e.dept_code,
      e.spvsr_emp_id,
      e.sr_login_user_id,
      e.pay_type,
      e.pay_type_id,
      e.labor_type_code,
      e.labor_type_id,
      e.base_inc_pay,
      e.pay_id,
      e.last_mod_by,
      e.mod_login_user_id,
      e.adj_aprv_user_id,
      e.adj_reason_code,
      e.adj_comment,
      e.whse,
      e.time_ind_id
 from (select view_ucl_user.emp_id as emp_id,
              view_ucl_user.login_user_id as login_user_id,
              view_ucl_user.last_name as last_name,
              view_ucl_user.first_name as first_name,
              e_emp_pay.pay_date as pay_date,
              e_emp_pay.whse_date as whse_date,
              e_emp_pay.pay_hours as pay_hours,
              e_emp_pay.total_hourly_pay as total_hourly_pay,
              (case
                  when e_emp_pay.status = 40 then e_emp_pay.roll_neg_pay
                  else e_emp_pay.adj_total_hourly_pay
               end)
                 as adj_tot_hrly_pay,
              null as inc_pay,
              null as total_inc_pay,
              null as adj_total_inc_pay,
              null as inc_code,
              null as inc_code_id,
              e_emp_pay.status as status,
              e_job_function.job_func_id as job_func_id,
              e_job_function.name as job_function__name,
              e_shift.shift_id as shift_id,
              e_shift.shift_code as shift_code,
              e_dept.dept_id as dept_id,
              e_dept.dept_code as dept_code,
              e_emp_pay.spvsr_emp_id as spvsr_emp_id,
              (select ucl_user1.login_user_id
                 from view_ucl_user ucl_user1
                where ucl_user1.emp_id = e_emp_pay.spvsr_emp_id)
                 as sr_login_user_id,
              e_pay_type.pay_type as pay_type,
              e_pay_type.pay_type_id as pay_type_id,
              null as labor_type_code,
              null as labor_type_id,
              'BASEPAY' as base_inc_pay,
              e_emp_pay.emp_pay_id as pay_id,
              e_emp_pay.last_mod_by as last_mod_by,
              e_emp_pay.mod_login_user_id as mod_login_user_id,
              e_emp_pay.adj_aprv_user_id as adj_aprv_user_id,
              e_emp_pay.adj_reason_code as adj_reason_code,
              e_emp_pay.adj_comment as adj_comment,
              e_emp_pay.whse as whse,
              e_emp_pay.time_ind_id
         from (   (   (   (   (   view_ucl_user
                               inner join
                                  e_emp_pay
                               on view_ucl_user.emp_id = e_emp_pay.emp_id)
                           left outer join
                              e_pay_type
                           on e_pay_type.pay_type_id =
                                 e_emp_pay.pay_type_id)
                       left outer join
                          e_job_function
                       on e_emp_pay.job_func_id =
                             e_job_function.job_func_id)
                   left outer join
                      e_shift
                   on e_emp_pay.shift_id = e_shift.shift_id)
               left outer join
                  e_dept
               on e_emp_pay.dept_id = e_dept.dept_id)
        where e_emp_pay.status <> '99'
       union
       select view_ucl_user.emp_id as emp_id,
              view_ucl_user.login_user_id as login_user_id,
              view_ucl_user.last_name as last_name,
              view_ucl_user.first_name as first_name,
              e_emp_inc_pay.pay_date as pay_date,
              e_emp_inc_pay.whse_date as whse_date,
              e_emp_inc_pay.pay_hours as pay_hours,
              null as total_hourly_pay,
              null as adj_tot_hrly_pay,
              e_emp_inc_pay.inc_pay as inc_pay,
              e_emp_inc_pay.total_inc_pay as total_inc_pay,
              (case
                  when e_emp_inc_pay.status = 40
                  then
                     e_emp_inc_pay.roll_neg_inc_pay
                  else
                     e_emp_inc_pay.adj_total_inc_pay
               end)
                 as adj_total_inc_pay,
              e_inc_code.inc_code as inc_code,
              e_inc_code.inc_code_id as inc_code_id,
              e_emp_inc_pay.status as status,
              e_job_function.job_func_id as job_func_id,
              e_job_function.name as job_function__name,
              e_shift.shift_id as shift_id,
              e_shift.shift_code as shift_code,
              e_dept.dept_id as dept_id,
              e_dept.dept_code as dept_code,
              e_emp_inc_pay.spvsr_emp_id as spvsr_emp_id,
              (select ucl_user1.login_user_id
                 from view_ucl_user ucl_user1
                where ucl_user1.emp_id = e_emp_inc_pay.spvsr_emp_id)
                 as sr_login_user_id,
              null as pay_type,
              null as pay_type_id,
              e_labor_type_code.description as labor_type_code,
              e_labor_type_code.labor_type_id as labor_type_id,
              'INCENTIVE' as base_inc_pay,
              e_emp_inc_pay.emp_inc_pay_id as pay_id,
              e_emp_inc_pay.last_mod_by as last_mod_by,
              e_emp_inc_pay.mod_login_user_id as mod_login_user_id,
              e_emp_inc_pay.adj_aprv_user_id as adj_aprv_user_id,
              e_emp_inc_pay.adj_reason_code as adj_reason_code,
              e_emp_inc_pay.adj_comment as adj_comment,
              e_emp_inc_pay.whse as whse,
              e_emp_inc_pay.time_ind_id
         from (   (   (   (   (   (   view_ucl_user
                                   inner join
                                      e_emp_inc_pay
                                   on view_ucl_user.emp_id =
                                         e_emp_inc_pay.emp_id)
                               left outer join
                                  e_inc_code
                               on e_inc_code.inc_code_id =
                                     e_emp_inc_pay.inc_code_id)
                           left outer join
                              e_job_function
                           on e_emp_inc_pay.job_func_id =
                                 e_job_function.job_func_id)
                       left outer join
                          e_shift
                       on e_emp_inc_pay.shift_id = e_shift.shift_id)
                   left outer join
                      e_dept
                   on e_emp_inc_pay.dept_id = e_dept.dept_id)
               left outer join
                  e_labor_type_code
               on e_emp_inc_pay.labor_type_id =
                     e_labor_type_code.labor_type_id)
        where e_emp_inc_pay.status <> '99') e;

CREATE OR REPLACE FORCE VIEW "BASE_INCC" ("ROW_NBR", "EMP_ID", "LOGIN_USER_ID", "LAST_NAME", "FIRST_NAME", "PAY_DATE", "WHSE_DATE", "PAY_HOURS", "TOTAL_HOURLY_PAY", "ADJ_TOT_HRLY_PAY", "INC_PAY", "TOTAL_INC_PAY", "ADJ_TOTAL_INC_PAY", "INC_CODE", "INC_CODE_ID", "STATUS", "JOB_FUNC_ID", "JOB_FUNCTION__NAME", "SHIFT_ID", "SHIFT_CODE", "DEPT_ID", "DEPT_CODE", "SPVSR_EMP_ID", "SR_LOGIN_USER_ID", "PAY_TYPE", "PAY_TYPE_ID", "LABOR_TYPE_CODE", "LABOR_TYPE_ID", "BASE_INC_PAY", "PAY_ID", "LAST_MOD_BY", "MOD_LOGIN_USER_ID", "ADJ_APRV_USER_ID", "ADJ_REASON_CODE", "ADJ_COMMENT", "WHSE", "TIME_IND_ID") AS
select rownum as row_nbr,
      e.emp_id,
      e.login_user_id,
      e.last_name,
      e.first_name,
      e.pay_date,
      e.whse_date,
      e.pay_hours,
      e.total_hourly_pay,
      e.adj_tot_hrly_pay,
      e.inc_pay,
      e.total_inc_pay,
      e.adj_total_inc_pay,
      e.inc_code,
      e.inc_code_id,
      e.status,
      e.job_func_id,
      e.job_function__name,
      e.shift_id,
      e.shift_code,
      e.dept_id,
      e.dept_code,
      e.spvsr_emp_id,
      e.sr_login_user_id,
      e.pay_type,
      e.pay_type_id,
      e.labor_type_code,
      e.labor_type_id,
      e.base_inc_pay,
      e.pay_id,
      e.last_mod_by,
      e.mod_login_user_id,
      e.adj_aprv_user_id,
      e.adj_reason_code,
      e.adj_comment,
      e.whse,
      e.time_ind_id
 from (select view_ucl_user.emp_id as emp_id,
              view_ucl_user.login_user_id as login_user_id,
              view_ucl_user.last_name as last_name,
              view_ucl_user.first_name as first_name,
              e_emp_pay.pay_date as pay_date,
              e_emp_pay.whse_date as whse_date,
              e_emp_pay.pay_hours as pay_hours,
              e_emp_pay.total_hourly_pay as total_hourly_pay,
              (case
                  when e_emp_pay.status = 40 then e_emp_pay.roll_neg_pay
                  else e_emp_pay.adj_total_hourly_pay
               end)
                 as adj_tot_hrly_pay,
              null as inc_pay,
              null as total_inc_pay,
              null as adj_total_inc_pay,
              null as inc_code,
              null as inc_code_id,
              e_emp_pay.status as status,
              e_job_function.job_func_id as job_func_id,
              e_job_function.name as job_function__name,
              e_shift.shift_id as shift_id,
              e_shift.shift_code as shift_code,
              e_dept.dept_id as dept_id,
              e_dept.dept_code as dept_code,
              e_emp_pay.spvsr_emp_id as spvsr_emp_id,
              (select ucl_user1.login_user_id
                 from view_ucl_user ucl_user1
                where ucl_user1.emp_id = e_emp_pay.spvsr_emp_id)
                 as sr_login_user_id,
              e_pay_type.pay_type as pay_type,
              e_pay_type.pay_type_id as pay_type_id,
              null as labor_type_code,
              null as labor_type_id,
              'BASEPAY' as base_inc_pay,
              e_emp_pay.emp_pay_id as pay_id,
              e_emp_pay.last_mod_by as last_mod_by,
              e_emp_pay.mod_login_user_id as mod_login_user_id,
              e_emp_pay.adj_aprv_user_id as adj_aprv_user_id,
              e_emp_pay.adj_reason_code as adj_reason_code,
              e_emp_pay.adj_comment as adj_comment,
              e_emp_pay.whse as whse,
              e_emp_pay.time_ind_id
         from (   (   (   (   (   view_ucl_user
                               inner join
                                  e_emp_pay
                               on view_ucl_user.emp_id = e_emp_pay.emp_id)
                           left outer join
                              e_pay_type
                           on e_pay_type.pay_type_id =
                                 e_emp_pay.pay_type_id)
                       left outer join
                          e_job_function
                       on e_emp_pay.job_func_id =
                             e_job_function.job_func_id)
                   left outer join
                      e_shift
                   on e_emp_pay.shift_id = e_shift.shift_id)
               left outer join
                  e_dept
               on e_emp_pay.dept_id = e_dept.dept_id)
        where e_emp_pay.status <> '99'
       union
       select view_ucl_user.emp_id as emp_id,
              view_ucl_user.login_user_id as login_user_id,
              view_ucl_user.last_name as last_name,
              view_ucl_user.first_name as first_name,
              e_emp_inc_pay.pay_date as pay_date,
              e_emp_inc_pay.whse_date as whse_date,
              e_emp_inc_pay.pay_hours as pay_hours,
              null as total_hourly_pay,
              null as adj_tot_hrly_pay,
              e_emp_inc_pay.inc_pay as inc_pay,
              e_emp_inc_pay.total_inc_pay as total_inc_pay,
              (case
                  when e_emp_inc_pay.status = 40
                  then
                     e_emp_inc_pay.roll_neg_inc_pay
                  else
                     e_emp_inc_pay.adj_total_inc_pay
               end)
                 as adj_total_inc_pay,
              e_inc_code.inc_code as inc_code,
              e_inc_code.inc_code_id as inc_code_id,
              e_emp_inc_pay.status as status,
              e_job_function.job_func_id as job_func_id,
              e_job_function.name as job_function__name,
              e_shift.shift_id as shift_id,
              e_shift.shift_code as shift_code,
              e_dept.dept_id as dept_id,
              e_dept.dept_code as dept_code,
              e_emp_inc_pay.spvsr_emp_id as spvsr_emp_id,
              (select ucl_user1.login_user_id
                 from view_ucl_user ucl_user1
                where ucl_user1.emp_id = e_emp_inc_pay.spvsr_emp_id)
                 as sr_login_user_id,
              null as pay_type,
              null as pay_type_id,
              e_labor_type_code.description as labor_type_code,
              e_labor_type_code.labor_type_id as labor_type_id,
              'INCENTIVE' as base_inc_pay,
              e_emp_inc_pay.emp_inc_pay_id as pay_id,
              e_emp_inc_pay.last_mod_by as last_mod_by,
              e_emp_inc_pay.mod_login_user_id as mod_login_user_id,
              e_emp_inc_pay.adj_aprv_user_id as adj_aprv_user_id,
              e_emp_inc_pay.adj_reason_code as adj_reason_code,
              e_emp_inc_pay.adj_comment as adj_comment,
              e_emp_inc_pay.whse as whse,
              e_emp_inc_pay.time_ind_id
         from (   (   (   (   (   (   view_ucl_user
                                   inner join
                                      e_emp_inc_pay
                                   on view_ucl_user.emp_id =
                                         e_emp_inc_pay.emp_id)
                               left outer join
                                  e_inc_code
                               on e_inc_code.inc_code_id =
                                     e_emp_inc_pay.inc_code_id)
                           left outer join
                              e_job_function
                           on e_emp_inc_pay.job_func_id =
                                 e_job_function.job_func_id)
                       left outer join
                          e_shift
                       on e_emp_inc_pay.shift_id = e_shift.shift_id)
                   left outer join
                      e_dept
                   on e_emp_inc_pay.dept_id = e_dept.dept_id)
               left outer join
                  e_labor_type_code
               on e_emp_inc_pay.labor_type_id =
                     e_labor_type_code.labor_type_id)
        where e_emp_inc_pay.status <> '99') e;

CREATE OR REPLACE FORCE VIEW "BASE_INC_REPORT" ("EMP_ID", "LOGIN_USER_ID", "LAST_NAME", "FIRST_NAME", "PAY_DATE", "WHSE_DATE", "PAY_HOURS", "TOTAL_HOURLY_PAY", "ADJ_TOT_HRLY_PAY", "INC_PAY", "TOTAL_INC_PAY", "ADJ_TOTAL_INC_PAY", "INC_CODE", "INC_CODE_ID", "STATUS", "JOB_FUNC_ID", "JOB_FUNCTION__NAME", "SHIFT_ID", "SHIFT_CODE", "DEPT_ID", "DEPT_CODE", "SPVSR_EMP_ID", "SR_LOGIN_USER_ID", "PAY_TYPE", "PAY_TYPE_ID", "LABOR_TYPE_CODE", "LABOR_TYPE_ID", "BASE_INC_PAY", "PAY_ID", "LAST_MOD_BY", "MOD_LOGIN_USER_ID", "ADJ_APRV_USER_ID", "ADJ_REASON_CODE", "ADJ_COMMENT", "WHSE", "TIME_IND_ID", "BONUS_TYPE", "PAY_RATE_ID") AS
select e.emp_id,
      e.login_user_id,
      e.last_name,
      e.first_name,
      e.pay_date,
      e.whse_date,
      e.pay_hours,
      e.total_hourly_pay,
      e.adj_tot_hrly_pay,
      e.inc_pay,
      e.total_inc_pay,
      e.adj_total_inc_pay,
      e.inc_code,
      e.inc_code_id,
      e.status,
      e.job_func_id,
      e.job_function__name,
      e.shift_id,
      e.shift_code,
      e.dept_id,
      e.dept_code,
      e.spvsr_emp_id,
      e.sr_login_user_id,
      e.pay_type,
      e.pay_type_id,
      e.labor_type_code,
      e.labor_type_id,
      e.base_inc_pay,
      e.pay_id,
      e.last_mod_by,
      e.mod_login_user_id,
      e.adj_aprv_user_id,
      e.adj_reason_code,
      e.adj_comment,
      e.whse,
      e.time_ind_id,
      e.bonus_type,
      e.pay_rate_id
 from (select view_ucl_user.emp_id as emp_id,
              view_ucl_user.login_user_id as login_user_id,
              view_ucl_user.last_name as last_name,
              view_ucl_user.first_name as first_name,
              e_emp_pay.pay_date as pay_date,
              e_emp_pay.whse_date as whse_date,
              e_emp_pay.pay_hours as pay_hours,
              e_emp_pay.total_hourly_pay as total_hourly_pay,
              (case
                  when e_emp_pay.status = 40 then e_emp_pay.roll_neg_pay
                  else e_emp_pay.adj_total_hourly_pay
               end)
                 as adj_tot_hrly_pay,
              null as inc_pay,
              null as total_inc_pay,
              null as adj_total_inc_pay,
              null as inc_code,
              null as inc_code_id,
              e_emp_pay.status as status,
              e_job_function.job_func_id as job_func_id,
              e_job_function.name as job_function__name,
              e_shift.shift_id as shift_id,
              e_shift.shift_code as shift_code,
              e_dept.dept_id as dept_id,
              e_dept.dept_code as dept_code,
              e_emp_pay.spvsr_emp_id as spvsr_emp_id,
              (select ucl_user1.login_user_id
                 from view_ucl_user ucl_user1
                where ucl_user1.emp_id = e_emp_pay.spvsr_emp_id)
                 as sr_login_user_id,
              e_pay_type.pay_type as pay_type,
              e_pay_type.pay_type_id as pay_type_id,
              null as labor_type_code,
              null as labor_type_id,
              'BASEPAY' as base_inc_pay,
              e_emp_pay.emp_pay_id as pay_id,
              e_emp_pay.last_mod_by as last_mod_by,
              e_emp_pay.mod_login_user_id as mod_login_user_id,
              e_emp_pay.adj_aprv_user_id as adj_aprv_user_id,
              e_emp_pay.adj_reason_code as adj_reason_code,
              e_emp_pay.adj_comment as adj_comment,
              e_emp_pay.whse as whse,
              e_emp_pay.time_ind_id,
              '' as bonus_type,
              e_emp_pay.pay_rate_id pay_rate_id
         from (   (   (   (   (   view_ucl_user
                               inner join
                                  e_emp_pay
                               on view_ucl_user.emp_id = e_emp_pay.emp_id)
                           left outer join
                              e_pay_type
                           on e_pay_type.pay_type_id =
                                 e_emp_pay.pay_type_id)
                       left outer join
                          e_job_function
                       on e_emp_pay.job_func_id =
                             e_job_function.job_func_id)
                   left outer join
                      e_shift
                   on e_emp_pay.shift_id = e_shift.shift_id)
               left outer join
                  e_dept
               on e_emp_pay.dept_id = e_dept.dept_id)
        where e_emp_pay.status <> '99'
       union
       select view_ucl_user.emp_id as emp_id,
              view_ucl_user.login_user_id as login_user_id,
              view_ucl_user.last_name as last_name,
              view_ucl_user.first_name as first_name,
              e_emp_inc_pay.pay_date as pay_date,
              e_emp_inc_pay.whse_date as whse_date,
              e_emp_inc_pay.pay_hours as pay_hours,
              null as total_hourly_pay,
              null as adj_tot_hrly_pay,
              e_emp_inc_pay.inc_pay as inc_pay,
              e_emp_inc_pay.total_inc_pay as total_inc_pay,
              (case
                  when e_emp_inc_pay.status = 40
                  then
                     e_emp_inc_pay.roll_neg_inc_pay
                  else
                     e_emp_inc_pay.adj_total_inc_pay
               end)
                 as adj_total_inc_pay,
              e_inc_code.inc_code as inc_code,
              e_inc_code.inc_code_id as inc_code_id,
              e_emp_inc_pay.status as status,
              e_job_function.job_func_id as job_func_id,
              e_job_function.name as job_function__name,
              e_shift.shift_id as shift_id,
              e_shift.shift_code as shift_code,
              e_dept.dept_id as dept_id,
              e_dept.dept_code as dept_code,
              e_emp_inc_pay.spvsr_emp_id as spvsr_emp_id,
              (select ucl_user1.login_user_id
                 from view_ucl_user ucl_user1
                where ucl_user1.emp_id = e_emp_inc_pay.spvsr_emp_id)
                 as sr_login_user_id,
              null as pay_type,
              null as pay_type_id,
              e_labor_type_code.description as labor_type_code,
              e_labor_type_code.labor_type_id as labor_type_id,
              'INCENTIVE' as base_inc_pay,
              e_emp_inc_pay.emp_inc_pay_id as pay_id,
              e_emp_inc_pay.last_mod_by as last_mod_by,
              e_emp_inc_pay.mod_login_user_id as mod_login_user_id,
              e_emp_inc_pay.adj_aprv_user_id as adj_aprv_user_id,
              e_emp_inc_pay.adj_reason_code as adj_reason_code,
              e_emp_inc_pay.adj_comment as adj_comment,
              e_emp_inc_pay.whse as whse,
              e_emp_inc_pay.time_ind_id,
              e_inc_code.bonus_type as bonus_type,
              null as pay_rate_id
         from (   (   (   (   (   (   view_ucl_user
                                   inner join
                                      e_emp_inc_pay
                                   on view_ucl_user.emp_id =
                                         e_emp_inc_pay.emp_id)
                               left outer join
                                  e_inc_code
                               on e_inc_code.inc_code_id =
                                     e_emp_inc_pay.inc_code_id)
                           left outer join
                              e_job_function
                           on e_emp_inc_pay.job_func_id =
                                 e_job_function.job_func_id)
                       left outer join
                          e_shift
                       on e_emp_inc_pay.shift_id = e_shift.shift_id)
                   left outer join
                      e_dept
                   on e_emp_inc_pay.dept_id = e_dept.dept_id)
               left outer join
                  e_labor_type_code
               on e_emp_inc_pay.labor_type_id =
                     e_labor_type_code.labor_type_id)
        where e_emp_inc_pay.status <> '99') e;

CREATE OR REPLACE FORCE VIEW "CLS_TIMEZONE" ("CLS_TIMEZONE_ID", "WM_VERSION_ID", "TZ_ID", "POPULARITY", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "USES_DST", "RAW_OFFSET") AS
select time_zone_id,
      cast (null as varchar (1)),
      time_zone_name,
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      gmt_offset
 from time_zone;

CREATE OR REPLACE FORCE VIEW "CM_RATE_DISCOUNT_VIEW" ("ACCESSORIAL_PARAM_SET_ID", "CM_RATE_DISCOUNT_ID", "START_NUMBER", "END_NUMBER", "VALUE", "CURRENCY_CODE", "DISTANCE_UOM", "HAS_ERRORS") AS
select cmrd.accessorial_param_set_id,
      cmrd.cm_rate_discount_id,
      cmrd.start_number,
      cmrd.end_number,
      cmrd.value,
      aps.cm_rate_currency_code,
      aps.distance_uom,
      0
 from cm_rate_discount cmrd, accessorial_param_set aps
where cmrd.accessorial_param_set_id = aps.accessorial_param_set_id
 union all
 select iaps.accessorial_param_set_id,
      icmrd.cm_rate_discount_id,
      icmrd.start_number,
      icmrd.end_number,
      icmrd.value,
      aps.cm_rate_currency_code,
      aps.distance_uom,
      case icmrd.status when 4 then 1 else 0 end as has_errors
 from import_cm_rate_discount icmrd,
      import_accessorial_param_set iaps,
      accessorial_param_set aps
where     icmrd.param_id = iaps.param_id
      and iaps.accessorial_param_set_id = aps.accessorial_param_set_id
      and iaps.status = 1
 union all
 select icmrd.param_id,
      icmrd.cm_rate_discount_id,
      icmrd.start_number,
      icmrd.end_number,
      icmrd.value,
      iaps.cm_rate_currency_code,
      iaps.distance_uom,
      case icmrd.status when 4 then 1 else 0 end as has_errors
 from import_cm_rate_discount icmrd, import_accessorial_param_set iaps
where icmrd.param_id = iaps.param_id and iaps.status = 4;

CREATE OR REPLACE FORCE VIEW "CNTRY" ("CNTRY", "CNTRY_DESC", "ISO_CNTRY_CODE", "STATE_REQD_FLAG", "CURR_CODE", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select country_code,
      country_name,
      country_code,
      has_state_prov,
      dflt_currency_code,
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1))
 from country;

CREATE OR REPLACE FORCE VIEW "COMPANY_HIERARCHY" ("TC_COMPANY_ID", "COMPANY_LEVEL", "DESCRIPTION") AS
(    select distinct
           company_id,
           ('LEVEL' || to_char (level - 1)),
           ('LEVEL' || to_char (level - 1))
      from company
connect by prior parent_company_id = company_id);

CREATE OR REPLACE FORCE VIEW "COUNT_OF_ACTIVITY_JOBFUNC" ("JOB_FUNC_ID", "COUNT_OF_ACTIVITY") AS
select job_func_id, count (distinct act_id) as count_of_activity
   from e_act
  where act_id < 900000
 group by job_func_id;

CREATE OR REPLACE FORCE VIEW "DOCK_DOOR_VIEW" ("PARENT_ID", "STATUS", "LOCN_CLASS", "MAX_CAPACITY", "LOCN_ID", "IS_GUARD_HOUSE", "CURRENT_CAPACITY", "ID", "FACILITY_ID", "YARD_ID", "LOCATION_NAME", "PARENT_LOCATION_NAME", "LOCN_BRCD", "DSP_LOCN", "CHECK_DIGIT", "TC_COMPANY_ID", "YARD_NAME") AS
(SELECT DD.DOCK_ID AS PARENT_ID,
         DD.DOCK_DOOR_STATUS AS STATUS,
         LH.LOCN_CLASS AS LOCN_CLASS,
         1 AS MAX_CAPACITY,
         LH.LOCN_ID AS LOCN_ID,
         0 AS IS_GUARD_HOUSE,
         1 AS CURRENT_CAPACITY,
         DD.DOCK_DOOR_ID AS id,
         DD.FACILITY_ID AS FACILITY_ID,
         0 AS YARD_ID,
         DD.DOCK_DOOR_NAME AS LOCATION_NAME,
         DO.DOCK_NAME AS PARENT_LOCATION_NAME,
         LH.LOCN_BRCD AS LOCN_BRCD,
         LH.DSP_LOCN AS DSP_LOCN,
         LH.CHECK_DIGIT AS CHECK_DIGIT,
         DD.TC_COMPANY_ID AS TC_COMPANY_ID,
         '' AS YARD_NAME
    FROM DOCK_DOOR DD, LOCN_HDR LH, DOCK DO
   WHERE     DD.DOCK_ID = DO.DOCK_ID
         AND DD.FACILITY_ID = DO.FACILITY_ID
         AND DD.DOCK_DOOR_LOCN_ID = LH.LOCN_ID
         AND LH.LOCN_CLASS = 'Q');

CREATE OR REPLACE FORCE VIEW "DOCK_LOCATION_VIEW" ("TYPE", "LOCATION_ID", "SEQUENCE_NO", "CURRENT_QUANTITY", "DEFINED_SVG", "ZONE_ID", "ZONE_NAME", "STATUS", "TC_COMPANY_ID", "PARENT_LOC") AS
SELECT 'YZN',
      LN.location_id,
      LN.sequence_no,
      current_quantity,
      LN.is_location_defined_in_svg,
      CAST (NULL AS NUMBER),
      yard_zone_name,
      CAST (NULL AS NUMBER),
      LN.tc_company_id,
      CAST (NULL AS CHAR (20))
 FROM yard_zone yz, LOCATION LN
WHERE     LN.location_objid_pk1 = TO_CHAR (yz.yard_id)
      AND LN.location_objid_pk2 = TO_CHAR (yz.yard_zone_id)
      AND LN.ilm_object_type = 28
 UNION ALL
 SELECT 'DDR',
      LN.location_id,
      LN.sequence_no,
      current_quantity,
      LN.is_location_defined_in_svg,
      CAST (NULL AS NUMBER),
      dock_door_name,
      dock_door_status,
      LN.tc_company_id,
      dock_id
 FROM dock_door dd, LOCATION LN
WHERE     LN.location_objid_pk1 = TO_CHAR (dd.facility_id)
      AND LN.location_objid_pk2 = dd.dock_id
      AND LN.location_objid_pk3 = TO_CHAR (dd.dock_door_id)
      AND LN.tc_company_id = dd.tc_company_id
      AND LN.ilm_object_type = 8
 UNION ALL
 SELECT 'YZS',
      LN.location_id,
      LN.sequence_no,
      current_quantity,
      LN.is_location_defined_in_svg,
      yard_zone_slot_id,
      yard_zone_slot_name,
      yard_zone_slot_status,
      LN.tc_company_id,
      y.yard_zone_name
 FROM yard_zone_slot yz, LOCATION LN, yard_zone y
WHERE     LN.location_objid_pk1 = TO_CHAR (yz.yard_id)
      AND LN.location_objid_pk2 = TO_CHAR (yz.yard_zone_id)
      AND LN.location_objid_pk3 = TO_CHAR (yz.yard_zone_slot_id)
      AND LN.ilm_object_type = 32
      AND y.yard_zone_id = yz.yard_zone_id
 UNION ALL
 SELECT 'DCK',
      LN.location_id,
      LN.sequence_no,
      current_quantity,
      LN.is_location_defined_in_svg,
      CAST (NULL AS NUMBER),
      dock_id,
      CAST (NULL AS NUMBER),
      LN.tc_company_id,
      CAST (NULL AS CHAR (20))
 FROM dock dck, LOCATION LN
WHERE     LN.location_objid_pk1 = TO_CHAR (dck.facility_id)
      AND LN.location_objid_pk2 = TO_CHAR (dck.dock_id)
      AND LN.ilm_object_type = 4;

CREATE OR REPLACE FORCE VIEW "DOT_DRIVING_RULES" ("DRIVING_RULE_CFG_ID", "TC_COMPANY_ID", "SERVICE_LEVEL_ID", "LONG_DRIVING_TIME_RULE", "SHORT_DRIVING_TIME_RULE", "LONG_RESTING_TIME_RULE", "SHORT_RESTING_TIME_RULE", "WAITING_TIME_RULE", "DUTY_TIME_RULE", "TOTAL_DUTY_TIME_RULE", "SHORT_DUTY_TIME_RULE", "SHORT_DUTY_RESTING_TIME_RULE", "DISTANCE_RULE", "DISTANCE_UOM", "COUNTRY_CODE", "MARK_FOR_DELETION") AS
SELECT DSP_DR_SET.DSP_DR_CONFIGURATION_ID DRIVING_RULE_CFG_ID,
      DSP_DR_SET.TC_COMPANY_ID,
      DSP_DR_SET.SERVICE_LEVEL_ID SERVICE_LEVEL_ID,
      (SELECT TO_NUMBER (RULE_VALUE_1, 99.99) FROM
DSP_DR WHERE  DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND DSP_DR.DSP_DR_TYPE_ID
= 2 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID AND MARK_FOR_DELETION
= 0) LONG_DRIVING_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99) FROM
 DSP_DR WHERE  DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND DSP_DR.DSP_DR_TYPE_ID
 = 0 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID AND MARK_FOR_DELETION
 = 0) SHORT_DRIVING_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99) FROM
 DSP_DR WHERE  DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND DSP_DR.DSP_DR_TYPE_ID
 = 16 AND MARK_FOR_DELETION = 0) LONG_RESTING_TIME_RULE,
 (SELECT TO_NUMBER
 (RULE_VALUE_1, 99.99) FROM   DSP_DR WHERE  DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID
 AND DSP_DR.DSP_DR_TYPE_ID = 14 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID
 AND MARK_FOR_DELETION = 0) SHORT_RESTING_TIME_RULE,
 0.0 WAITING_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99)
 FROM DSP_DR WHERE DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND
 DSP_DR.DSP_DR_TYPE_ID = 8 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID
 AND MARK_FOR_DELETION = 0) DUTY_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99)
 FROM DSP_DR WHERE DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND
 DSP_DR.DSP_DR_TYPE_ID = 38 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID
 AND MARK_FOR_DELETION = 0) TOTAL_DUTY_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99)
 FROM DSP_DR WHERE DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND
 DSP_DR.DSP_DR_TYPE_ID = 6 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID
 AND MARK_FOR_DELETION = 0) SHORT_DUTY_TIME_RULE,
 (SELECT TO_NUMBER (PARAMETER_VALUE,99.99)
 FROM DSP_DR_PARAMETER_SET WHERE DSP_DR_PARAMETER_SET.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND
 DSP_DR_PARAMETER_SET.DSP_DR_PARAMETER_ID = 4 AND MARK_FOR_DELETION = 0)
 SHORT_DUTY_RESTING_TIME_RULE,
 0.0 DISTANCE_RULE,
 'MI' DISTANCE_UOM,
 COUNTRY_CODE,
 0 MARK_FOR_DELETION
 FROM DSP_DR_SET WHERE MARK_FOR_DELETION = 0;

CREATE OR REPLACE FORCE VIEW "DUMMY_CRITERIA_TEMP_TABLE" ("HDR_MSG_ID", "DTL_MSG_ID", "TRAN_NBR", "TRAN_SEQ_NBR", "WHSE") AS
select coalesce (dc.labor_msg_id, hc.labor_msg_id) hdr_msg_id,
        coalesce (dc.labor_msg_dtl_id, hc.labor_msg_dtl_id) dtl_msg_id,
        coalesce (dc.tran_nbr, hc.tran_nbr) tran_nbr,
        coalesce (dc.tran_seq_nbr, hc.tran_seq_nbr) tran_seq_nbr,
        coalesce (dc.whse, hc.whse) whse
   from    view_dtl_crit dc
        full outer join
           view_hdr_crit hc
        on     dc.labor_msg_id = hc.labor_msg_id
           and dc.labor_msg_dtl_id = hc.labor_msg_dtl_id
           and dc.tran_nbr = hc.tran_nbr
           and dc.crit_type = hc.crit_type
 group by coalesce (dc.labor_msg_id, hc.labor_msg_id),
        coalesce (dc.labor_msg_dtl_id, hc.labor_msg_dtl_id),
        coalesce (dc.tran_nbr, hc.tran_nbr),
        coalesce (dc.tran_seq_nbr, hc.tran_seq_nbr),
        coalesce (dc.whse, hc.whse);

CREATE OR REPLACE FORCE VIEW "DUMMY_LABOR_TRAN_CRIT_TABLE" ("TRAN_NBR", "TRAN_SEQ_NBR", "LABOR_TRAN_ID", "LABOR_TRAN_DTL_ID", "WHSE") AS
select coalesce (dc.tran_nbr, hc.tran_nbr) tran_nbr,
        coalesce (dc.tran_seq_nbr, hc.tran_seq_nbr) tran_seq_nbr,
        coalesce (dc.labor_tran_id, hc.labor_tran_id) labor_tran_id,
        coalesce (dc.labor_tran_dtl_id, hc.labor_tran_dtl_id)
           labor_tran_dtl_id,
        coalesce (dc.whse, hc.whse) whse
   from    view_labor_tran_dtl_crit dc
        full outer join
           view_labor_tran_crit hc
        on     dc.labor_tran_id = hc.labor_tran_id
           and dc.labor_tran_dtl_id = hc.labor_tran_dtl_id
           and dc.tran_nbr = hc.tran_nbr
           and dc.crit_type = hc.crit_type
  where dc.crit_type in ('ABCD', 'TEST3', 'VINCRI')
        or hc.crit_type in ('ABCD', 'TEST3', 'VINCRI')
 group by coalesce (dc.labor_tran_id, hc.labor_tran_id),
        coalesce (dc.labor_tran_dtl_id, hc.labor_tran_dtl_id),
        coalesce (dc.tran_nbr, hc.tran_nbr),
        coalesce (dc.tran_seq_nbr, hc.tran_seq_nbr),
        coalesce (dc.whse, hc.whse);

CREATE OR REPLACE FORCE VIEW "MV_DYNAMIC_PICK_LOCN" ("WHSE", "LH_LOCN_ID", "LOCN_ID", "MAX_NBR_OF_SKU", "REPL_FLAG", "MAX_WT", "MAX_VOL", "LOCN_CLASS", "LOCN_PICK_SEQ", "PICK_LOCN_ASSIGN_ZONE", "SKU_DEDCTN_TYPE", "PICK_LOCN_HDR_ID", "LOCN_DYN_ASSGN_SEQ") AS
(SELECT whse,
       lh.locn_id lh_locn_id,
       plh.locn_id,
       max_nbr_of_sku,
       repl_flag,
       max_wt,
       max_vol,
       locn_class,
       locn_pick_seq,
       pick_locn_assign_zone,
       sku_dedctn_type,
       plh.PICK_LOCN_HDR_ID,
       lh.LOCN_DYN_ASSGN_SEQ
  FROM locn_hdr lh, pick_locn_hdr plh
 WHERE     plh.locn_id = lh.locn_id
       AND sku_dedctn_type = 'T'
       AND (lh.locn_class = 'A' OR lh.locn_class = 'C')
       AND (slot_unusable IS NULL OR slot_unusable != 'Y'));

CREATE OR REPLACE FORCE VIEW "NAVIGATION_VIEW" ("PARENT_TITLE", "NAVIGATION_ID", "PARENT_ID", "LEVEL", "NAV_TITLE", "URL", "TYPE", "SIDE_NAV", "UDEF_SEQ") AS
select p.title parent_title, t."NAVIGATION_ID",t."PARENT_ID",t."LEVEL",t."NAV_TITLE",t."URL",t."TYPE",t."SIDE_NAV",t."UDEF_SEQ"
from navigation p right outer join (
              select navigation_id, parent_id, level, title nav_title, url,type,side_nav,udef_seq
              from navigation
              start with parent_id is null
              connect by prior navigation_id = parent_id
              ) t
on (p.navigation_id = t.parent_id);

CREATE OR REPLACE FORCE VIEW "OPS_CODE_JOB_FUNC_NAME" ("OPSCODEID", "JOB_FUNCTIONS") AS
select ops_code_id as opscodeid,
      getjobfunctionforopscode (ops_code_id) as job_functions
 from e_ops_code;

CREATE OR REPLACE FORCE VIEW "ORDERS_ORDER_MOVEMENT_VIEW" ("ORDER_ID", "SHIPMENT_ID", "ORDER_SPLIT_ID") AS
select distinct
      orders.order_id,
      stop_action_order.shipment_id,
      stop_action_order.order_split_id
 from    orders
      left outer join
         stop_action_order
      on stop_action_order.order_id = orders.order_id;

CREATE OR REPLACE FORCE VIEW "ORDERS_SHIPMENT_RELATION_VIEW" ("ORDER_ID", "TC_ORDER_ID", "DO_STATUS", "SHIPMENT_ID") AS
SELECT L.ORDER_ID,
  L.TC_ORDER_ID,
  O.DO_STATUS,
  L.SHIPMENT_ID
FROM LPN L,
  ORDERS O
WHERE L.ORDER_ID   = O.ORDER_ID
AND L.SHIPMENT_ID > -1
UNION ALL
SELECT SAO.ORDER_ID,
  O.TC_ORDER_ID,
  O.DO_STATUS,
  SAO.SHIPMENT_ID
FROM STOP_ACTION_ORDER SAO,
  ORDERS O
WHERE O.ORDER_ID = SAO.ORDER_ID AND O.DO_STATUS < 150
UNION ALL
SELECT O.ORDER_ID,
  O.TC_ORDER_ID,
  O.DO_STATUS,
  O.SHIPMENT_ID
FROM ORDERS O
WHERE O.SHIPMENT_ID > -1 AND O.DO_STATUS < 150;

CREATE OR REPLACE FORCE VIEW "ORDERS_VIEW" ("ORDER_ID", "TC_ORDER_ID", "TC_ORDER_ID_U", "TC_COMPANY_ID", "CREATION_TYPE", "BUSINESS_PARTNER_ID", "PARENT_ORDER_ID", "EXT_PURCHASE_ORDER", "CONS_RUN_ID", "O_FACILITY_ALIAS_ID", "O_FACILITY_ID", "O_DOCK_ID", "O_ADDRESS_1", "O_ADDRESS_2", "O_ADDRESS_3", "O_CITY", "O_STATE_PROV", "O_POSTAL_CODE", "O_COUNTY", "O_COUNTRY_CODE", "D_FACILITY_ALIAS_ID", "D_FACILITY_ID", "D_DOCK_ID", "D_ADDRESS_1", "D_ADDRESS_2", "D_ADDRESS_3", "D_CITY", "D_STATE_PROV", "D_POSTAL_CODE", "D_COUNTY", "D_COUNTRY_CODE", "BILL_TO_NAME", "BILL_FACILITY_ALIAS_ID", "BILL_FACILITY_ID", "BILL_TO_ADDRESS_1", "BILL_TO_ADDRESS_2", "BILL_TO_ADDRESS_3", "BILL_TO_CITY", "BILL_TO_STATE_PROV", "BILL_TO_COUNTY", "BILL_TO_POSTAL_CODE", "BILL_TO_COUNTRY_CODE", "BILL_TO_PHONE_NUMBER", "BILL_TO_FAX_NUMBER", "BILL_TO_EMAIL", "PLAN_O_FACILITY_ID", "PLAN_D_FACILITY_ID", "PLAN_O_FACILITY_ALIAS_ID", "PLAN_D_FACILITY_ALIAS_ID", "PLAN_LEG_D_ALIAS_ID", "PLAN_LEG_O_ALIAS_ID", "INCOTERM_FACILITY_ID", "INCOTERM_FACILITY_ALIAS_ID", "INCOTERM_ID", "INCOTERM_LOC_AVA_DTTM", "INCOTERM_LOC_AVA_TIME_ZONE_ID", "PICKUP_TZ", "DELIVERY_TZ", "PICKUP_START_DTTM", "PICKUP_END_DTTM", "DELIVERY_START_DTTM", "DELIVERY_END_DTTM", "ORDER_DATE_DTTM", "ORDER_RECON_DTTM", "INBOUND_REGION_ID", "OUTBOUND_REGION_ID", "DSG_SERVICE_LEVEL_ID", "DSG_CARRIER_ID", "DSG_EQUIPMENT_ID", "DSG_TRACTOR_EQUIPMENT_ID", "DSG_MOT_ID", "BASELINE_MOT_ID", "BASELINE_SERVICE_LEVEL_ID", "PRODUCT_CLASS_ID", "PROTECTION_LEVEL_ID", "MERCHANDIZING_DEPARTMENT_ID", "WAVE_ID", "WAVE_OPTION_ID", "PATH_ID", "PATH_SET_ID", "DRIVER_TYPE_ID", "UN_NUMBER_ID", "BLOCK_AUTO_CREATE", "BLOCK_AUTO_CONSOLIDATE", "HAS_ALERTS", "HAS_EM_NOTIFY_FLAG", "HAS_IMPORT_ERROR", "HAS_NOTES", "HAS_SOFT_CHECK_ERRORS", "HAS_SPLIT", "IS_BOOKING_REQUIRED", "IS_CANCELLED", "IS_HAZMAT", "IS_IMPORTED", "IS_PARTIALLY_PLANNED", "IS_PERISHABLE", "IS_SUSPENDED", "NORMALIZED_BASELINE_COST", "BASELINE_COST_CURRENCY_CODE", "ORIG_BUDG_COST", 
"BUDG_COST", "ACTUAL_COST", "BASELINE_COST", "TRANS_RESP_CODE", "BILLING_METHOD", "DROPOFF_PICKUP", "MOVEMENT_OPTION", "PRIORITY", "MOVE_TYPE", "SCHED_DOW", "EQUIPMENT_TYPE", "DELIVERY_REQ", "MV_CURRENCY_CODE", "MONETARY_VALUE", "COMPARTMENT_NO", "PACKAGING", "EST_PALLET_BRIDGED", "ORDER_LOADING_SEQ", "REF_FIELD_1", "REF_FIELD_2", "REF_FIELD_3", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "HIBERNATE_VERSION", "ACTUAL_COST_CURRENCY_CODE", "BUDG_COST_CURRENCY_CODE", "SHIPMENT_ID", "BILL_TO_TITLE", "ORDER_STATUS", "ADDR_CODE", "ADDR_VALID", "ADVT_DATE", "BILL_ACCT_NBR", "CHUTE_ID", "COD_FUNDS", "CUST_BROKER_ACCT_NBR", "DC_CTR_NBR", "DO_STATUS", "DO_TYPE", "DOCS_ONLY_SHPMT", "DUTY_AND_TAX", "DUTY_TAX_ACCT_NBR", "DUTY_TAX_PAYMENT_TYPE", "EST_LPN", "EST_LPN_BRIDGED", "EST_PALLET", "FREIGHT_FORWARDER_ACCT_NBR", "FTSR_NBR", "GLOBAL_LOCN_NBR", "IMPORTER_DEFN", "IS_BACK_ORDERED", "LANG_ID", "LINE_HAUL_SHIP_VIA", "LPN_CUBING_INDIC", "MAJOR_MINOR_ORDER", "MAJOR_ORDER_CTRL_NBR", "MHE_FLAG", "MHE_ORD_STATE", "O_PHONE_NUMBER", "ORDER_CONSOL_LOCN_ID", "ORDER_PRINT_DTTM", "PACK_WAVE_NBR", "PALLET_CUBING_INDIC", "PARTIAL_LPN_OPTION", "PARTIES_RELATED", "PNH_FLAG", "PRE_PACK_FLAG", "PRE_STICKER_CODE", "PRIMARY_MAXI_ADDR_NBR", "RTE_SWC_NBR", "RTE_TO", "RTE_TYPE_1", "RTE_TYPE_2", "RTE_WAVE_NBR", "SHIP_GROUP_SEQUENCE", "SHPNG_CHRG", "STAGE_INDIC", "STORE_NBR", "TOTAL_NBR_OF_LPN", "TOTAL_NBR_OF_PLT", "WM_ORDER_STATUS", "D_NAME", "O_CONTACT", "SECONDARY_MAXI_ADDR_NBR", "D_CONTACT", "AES_ITN", "BATCH_ID", "COD_RETURN_COMPANY_NAME", "ORIGIN_SHIP_THRU_FACILITY_ID", "ORIGIN_SHIP_THRU_FAC_ALIAS_ID", "DEST_SHIP_THRU_FACILITY_ID", "DEST_SHIP_THRU_FAC_ALIAS_ID", "ORIGINAL_ASSIGNED_SHIP_VIA", "ACTUAL_SHIPPED_DTTM", "DISTRIBUTION_SHIP_VIA", "DSG_SHIP_VIA", "BILL_OF_LADING_NUMBER", "BOL_BREAK_ATTR", "D_EMAIL", "D_FAX_NUMBER", "D_PHONE_NUMBER", "DYNAMIC_ROUTING_REQD", "INTL_GOODS_DESC", 
"MAJOR_ORDER_GRP_ATTR", "ORDER_CONSOL_PROFILE", "PACK_SLIP_PRT_CNT", "PROD_SCHED_REF_NUMBER", "MUST_RELEASE_BY_DTTM", "IS_ORIGINAL_ORDER", "CANCEL_DTTM", "ASSIGNED_STATIC_ROUTE_ID", "DSG_STATIC_ROUTE_ID", "O_FACILITY_NAME", "O_DOCK_DOOR_ID", "O_FAX_NUMBER", "O_EMAIL", "D_FACILITY_NAME", "D_DOCK_DOOR_ID", "PURCHASE_ORDER_NUMBER", "CUSTOMER_ID", "REPL_WAVE_NBR", "CUBING_STATUS", "SCHED_PICKUP_DTTM", "SCHED_DELIVERY_DTTM", "ZONE_SKIP_HUB_LOCATION_ID", "BILL_TO_FACILITY_NAME", "FREIGHT_CLASS", "NON_MACHINEABLE", "ASSIGNED_MOT_ID", "ASSIGNED_CARRIER_ID", "ASSIGNED_SERVICE_LEVEL_ID", "ASSIGNED_EQUIPMENT_ID", "DYNAMIC_REQUEST_SENT", "COMMODITY_CODE_ID", "TC_SHIPMENT_ID", "IN_TRANSIT_ALLOCATION", "RELEASE_DESTINATION", "TEMPLATE_ID", "COD_AMOUNT", "BILL_TO_CONTACT_NAME", "SHIP_GROUP_ID", "PURCHASE_ORDER_ID", "DSG_HUB_LOCATION_ID", "OVERRIDE_BILLING_METHOD", "PARENT_TYPE", "ACCT_RCVBL_CODE", "ADVT_CODE", "IS_CUSTOMER_PICKUP", "IS_DIRECT_ALLOWED", "ORDER_TYPE", "RTE_ATTR", "LAST_RUN_ID") AS
SELECT order_id, tc_order_id, tc_order_id_u, tc_company_id, creation_type,
      business_partner_id, parent_order_id, ext_purchase_order,
      --master_order_status,
       cons_run_id, o_facility_alias_id,
      o_facility_id, o_dock_id, o_address_1, o_address_2, o_address_3,
      o_city, o_state_prov, o_postal_code, o_county, o_country_code,
      d_facility_alias_id, d_facility_id, d_dock_id, d_address_1,
      d_address_2, d_address_3, d_city, d_state_prov, d_postal_code,
      d_county, d_country_code, bill_to_name, bill_facility_alias_id,
      bill_facility_id, bill_to_address_1, bill_to_address_2,
      bill_to_address_3, bill_to_city, bill_to_state_prov, bill_to_county,
      bill_to_postal_code, bill_to_country_code, bill_to_phone_number,
      bill_to_fax_number, bill_to_email, plan_o_facility_id,
      plan_d_facility_id, plan_o_facility_alias_id,
      plan_d_facility_alias_id, plan_leg_d_alias_id, plan_leg_o_alias_id,
      incoterm_facility_id, incoterm_facility_alias_id, incoterm_id,
      incoterm_loc_ava_dttm, incoterm_loc_ava_time_zone_id, pickup_tz,
      delivery_tz, pickup_start_dttm, pickup_end_dttm,
      delivery_start_dttm, delivery_end_dttm, order_date_dttm,
      order_recon_dttm,
      --pickup_handling_time,
       --delivery_handling_time,
      --region_id,
      inbound_region_id, outbound_region_id,
      --carrier_id,
      dsg_service_level_id, dsg_carrier_id, dsg_equipment_id,
      dsg_tractor_equipment_id, dsg_mot_id, baseline_mot_id,
      baseline_service_level_id, product_class_id, protection_level_id,
      merchandizing_department_id, wave_id, wave_option_id, path_id,
      path_set_id, driver_type_id, --event_ind_typeid,
      un_number_id,
      block_auto_create, block_auto_consolidate, has_alerts,
      has_em_notify_flag, has_import_error, has_notes,
      has_soft_check_errors, has_split, is_booking_required, is_cancelled,
      --is_from_master_order,
      is_hazmat, is_imported,
      is_partially_planned, is_perishable,
      --is_saturday_delivery,
      --is_saturday_pickup,
      is_suspended,
      --update_sent,
      normalized_baseline_cost, baseline_cost_currency_code,
      orig_budg_cost, budg_cost, actual_cost, baseline_cost,
      --freight_allowance,
      trans_resp_code, billing_method, dropoff_pickup,
      movement_option, --priority_type,
      priority,
      --pro_number,
      move_type,
      sched_dow, equipment_type, delivery_req,
      --buyer_code,
      mv_currency_code, monetary_value, compartment_no, --contnt_label_type,
      --osd,
      packaging, --est_outbd_lpn_per_plt_bridged,
      est_pallet_bridged,
      --est_wt_bridged,
      --nbr_of_contnt_label, --nbr_of_label, --nbr_of_pakng_slips,
      order_loading_seq, --order_weight,
      ref_field_1, ref_field_2,
      ref_field_3, created_source_type, created_source, created_dttm,
      last_updated_source_type, last_updated_source, last_updated_dttm,
      hibernate_version, --dsg_carrier_code,
       actual_cost_currency_code,
      budg_cost_currency_code, shipment_id, bill_to_title, order_status,
      addr_code, addr_valid, advt_date, --auto_invc_stat_code,
      --backordered_quantity,
      bill_acct_nbr, --bol_type, --chute_assign_type,
      chute_id, cod_funds,
      cust_broker_acct_nbr, dc_ctr_nbr, do_status, do_type,
      docs_only_shpmt, duty_and_tax, duty_tax_acct_nbr,
      duty_tax_payment_type, est_lpn, est_lpn_bridged, est_pallet,
      --est_vol,                                          --est_vol_bridged,
              --est_wt,
              freight_forwarder_acct_nbr, ftsr_nbr,
      global_locn_nbr, importer_defn, --internal_order_id,
      is_back_ordered,
      --item_invn_verf,
       lang_id, line_haul_ship_via,
      --lpn_asn_reqd,
      lpn_cubing_indic, --lpn_label_ship_from_fac_id,
      --lpn_label_type,
      major_minor_order, major_order_ctrl_nbr, --mark_for,
                                                        --max_lpn_units, max_lpn_weight,
                                                        --merch_class,
      --merch_code, --merch_div,
      mhe_flag, mhe_ord_state, o_phone_number,
      order_consol_locn_id, order_print_dttm, --pack_and_hold_ctrl_nbr,
      --pack_slip_type,
      pack_wave_nbr, pallet_cubing_indic, --palletize_order,
      --partial_lpn_min_weight,
      partial_lpn_option, parties_related, --planned_carrier_id,
      --planned_mot_id, --planned_service_level_id,
      pnh_flag, pre_pack_flag,
      pre_sticker_code, primary_maxi_addr_nbr,
      --print_canadian_cust_invc_flag, --print_coo, --print_dock_rcpt_flag,
      --print_inv, --print_nafta_coo_flag, --print_ocean_bol_flag,
      --print_pkg_list_flag, --print_sed, --print_shpr_ltr_of_instr_flag,
      --ref_consol_nbr, --rte_consol_nbr,             --rte_guide_nbr, rte_id,
                                     --rte_stop_seq,
                                     rte_swc_nbr, rte_to,
      rte_type_1, rte_type_2, rte_wave_nbr,
      --sale_group,
      ship_group_sequence, shpng_chrg,                 --single_unit_flag,
                                      stage_indic, store_nbr,
      total_nbr_of_lpn, total_nbr_of_plt,            --total_nbr_of_units,
                                         --wave_seq_nbr,
                                         --wave_stat_code,
      wm_order_status, d_name, o_contact, secondary_maxi_addr_nbr,
      --tariff_class,
      --custom_tag,
      d_contact, aes_itn, batch_id,
      cod_return_company_name,
      --cod_return_dept,
      --cod_return_tracking_nbr,
      --cust_chrg_carrier_code,
      --cust_chrg_origin_facility_id,
      --cust_company_duns_nbr,
      --dc_pickup_tz,
      --sender_rs_ein_vat_nbr,
      --total_messages,
       origin_ship_thru_facility_id,
      origin_ship_thru_fac_alias_id, dest_ship_thru_facility_id,
      dest_ship_thru_fac_alias_id, original_assigned_ship_via,
      actual_shipped_dttm, distribution_ship_via,
      --cust_chrg_service_level,
      dsg_ship_via,
      --sent_to_wms,
      --sent_to_wms_dttm,
      --order_type_desc,
      --accessorial_option_grp,
      bill_of_lading_number, bol_break_attr,
      d_email, d_fax_number, d_phone_number, dynamic_routing_reqd,
      --incoterm_name,
       intl_goods_desc,
      --lpn_epc_type,
       major_order_grp_attr,
      --message_number,
       order_consol_profile,
      --order_group_code,
      pack_slip_prt_cnt, --pallet_content_label_type,
      --pallet_epc_type,
      --routing_pending,
      prod_sched_ref_number, must_release_by_dttm,
      is_original_order, cancel_dttm,
      --baseline_res_det_type,
      --baseline_ship_via,
      assigned_static_route_id, dsg_static_route_id,
      o_facility_name, o_dock_door_id, o_fax_number, o_email,
      d_facility_name, d_dock_door_id, purchase_order_number, customer_id,
      --rte_load_seq,
       repl_wave_nbr, --repl_flag,
       cubing_status,
      sched_pickup_dttm, sched_delivery_dttm, zone_skip_hub_location_id,
      bill_to_facility_name, freight_class,
      --manifest_id,
       non_machineable,
      --org_parent_order_id,
       assigned_mot_id, assigned_carrier_id,
      assigned_service_level_id, assigned_equipment_id,
      dynamic_request_sent, commodity_code_id, tc_shipment_id,
      in_transit_allocation, release_destination, template_id, cod_amount,
      bill_to_contact_name, ship_group_id, purchase_order_id,
      dsg_hub_location_id, override_billing_method, parent_type,
      --rtn_route_lvl,
      --acct_rcvbl_acct_nbr,
      acct_rcvbl_code, advt_code,
      is_customer_pickup, is_direct_allowed,
      order_type,
      --org_ship_via_facility_alias_id,
      --return_address_1,
      --return_address_2,
      rte_attr, last_run_id
 FROM orders;

CREATE OR REPLACE FORCE VIEW "ORDER_LINE_ITEM_VIEW" ("ORDER_ID", "LINE_ITEM_ID", "PARENT_LINE_ITEM_ID", "TC_COMPANY_ID", "COMMODITY_CLASS", "REF_FIELD1", "REF_FIELD2", "REF_FIELD3", "EXT_SYS_LINE_ITEM_ID", "MASTER_ORDER_ID", "MO_LINE_ITEM_ID", "IS_HAZMAT", "IS_STACKABLE", "HAS_ERRORS", "ORIG_BUDG_COST", "BUDG_COST_CURRENCY_CODE", "BUDG_COST", "ACTUAL_COST_CURRENCY_CODE", "ACTUAL_COST", "TOTAL_MONETARY_VALUE", "UNIT_MONETARY_VALUE", "UNIT_TAX_AMOUNT", "RTS_ID", "RTS_LINE_ITEM_ID", "STD_PACK_QTY", "PRODUCT_CLASS_ID", "PROTECTION_LEVEL_ID", "PACKAGE_TYPE_ID", "COMMODITY_CODE_ID", "MV_SIZE_UOM_ID", "MV_CURRENCY_CODE", "PRIORITY", "MERCHANDIZING_DEPARTMENT_ID", "UN_NUMBER_ID", "PICKUP_REFERENCE_NUMBER", "DELIVERY_REFERENCE_NUMBER", "CNTRY_OF_ORGN", "PROD_STAT", "STACK_RANK", "STACK_LENGTH_VALUE", "STACK_LENGTH_STANDARD_UOM", "STACK_WIDTH_VALUE", "STACK_WIDTH_STANDARD_UOM", "STACK_HEIGHT_VALUE", "STACK_HEIGHT_STANDARD_UOM", "STACK_DIAMETER_VALUE", "STACK_DIAMETER_STANDARD_UOM", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "HIBERNATE_VERSION", "CRITCL_DIM_1", "CRITCL_DIM_2", "CRITCL_DIM_3", "CUBE_MULTIPLE_QTY", "DO_DTL_STATUS", "INTERNAL_ORDER_SEQ_NBR", "IS_EMERGENCY", "LPN_SIZE", "LPN_TYPE", "MANUFACTURING_DTTM", "ORDER_CONSOL_ATTR", "ORDER_LINE_ID", "PACK_RATE", "PLANNED_SHIP_DATE", "PPACK_QTY", "SKU_BREAK_ATTR", "STD_BUNDLE_QTY", "STD_LPN_QTY", "STD_LPN_VOL", "STD_LPN_WT", "UNIT_COST", "UNIT_PRICE_AMOUNT", "UNIT_VOL", "UNIT_WT", "USER_CANCELED_QTY", "WAVE_PROC_TYPE", "DELIVERY_END_DTTM", "DELIVERY_START_DTTM", "EVENT_CODE", "REASON_CODE", "EXP_INFO_CODE", "PARTL_FILL", "LINE_TYPE", "REPL_PROC_TYPE", "ORDER_QTY", "ORIG_ORDER_QTY", "RTL_TO_BE_DISTROED_QTY", "PRICE", "RETAIL_PRICE", "UNITS_PAKD", "MINOR_ORDER_NBR", "ITEM_ID", "TC_ORDER_LINE_ID", "ACTUAL_SHIPPED_DTTM", "PICKUP_END_DTTM", "PURCHASE_ORDER_LINE_NUMBER", "PICKUP_START_DTTM", "REFERENCE_LINE_ITEM_ID", 
"REFERENCE_ORDER_ID", "REPL_WAVE_RUN", "STD_PALLET_QTY", "STD_SUB_PACK_QTY", "STORE_DEPT", "ALLOC_TYPE", "ASSORT_NBR", "BATCH_NBR", "BATCH_REQUIREMENT_TYPE", "CHUTE_ASSIGN_TYPE", "CUSTOM_TAG", "CUSTOMER_ITEM", "INTERNAL_ORDER_ID", "INVN_TYPE", "ITEM_ATTR_1", "ITEM_ATTR_2", "ITEM_ATTR_3", "ITEM_ATTR_4", "ITEM_ATTR_5", "LPN_BRK_ATTRIB", "MERCH_GRP", "MERCH_TYPE", "PACK_ZONE", "PALLET_TYPE", "PICK_LOCN_ASSIGN_TYPE", "PICK_LOCN_ID", "PPACK_GRP_CODE", "PRICE_TKT_TYPE", "SERIAL_NUMBER_REQUIRED_FLAG", "SINGLE_UNIT_FLAG", "SKU_GTIN", "SKU_SUB_CODE_ID", "SKU_SUB_CODE_VALUE", "VAS_PROCESS_TYPE", "SUBSTITUTED_PARENT_LINE_ID", "IS_CANCELLED", "SHIPPED_QTY", "ITEM_NAME", "ALLOC_LINE_ID", "ALLOCATION_SOURCE_ID", "ALLOCATION_SOURCE_LINE_ID", "ALLOCATION_SOURCE", "SHELF_DAYS") AS
select order_id,
      line_item_id,
      parent_line_item_id,
      tc_company_id,
      commodity_class,
      ref_field1,
      ref_field2,
      ref_field3,
      ext_sys_line_item_id,
      master_order_id,
      mo_line_item_id,
      is_hazmat,          --is_bonded, --is_deleted, --is_variable_weight,
      is_stackable,
      has_errors,               --is_associated_to_outbound, --sub_sku_id,
      orig_budg_cost,
      budg_cost_currency_code,
      budg_cost,
      actual_cost_currency_code,
      actual_cost,
      total_monetary_value,
      unit_monetary_value,
      unit_tax_amount,
      rts_id,
      rts_line_item_id,
      std_pack_qty,
      product_class_id,
      protection_level_id,
      package_type_id,
      commodity_code_id,                                         --uom_id,
      mv_size_uom_id,                                    --mo_size_uom_id,
      mv_currency_code,
      priority,
      merchandizing_department_id,
      un_number_id,                             --epc_tracking_rfid_value,
      pickup_reference_number,
      delivery_reference_number, --asn_id,                   --assigned_qty,
      cntry_of_orgn,                                       --receipt_dttm,
      prod_stat,
      stack_rank,
      stack_length_value,
      stack_length_standard_uom,
      stack_width_value,
      stack_width_standard_uom,
      stack_height_value,
      stack_height_standard_uom,
      stack_diameter_value,
      stack_diameter_standard_uom,
      created_source_type,
      created_source,
      created_dttm,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      hibernate_version,
      critcl_dim_1,
      critcl_dim_2,
      critcl_dim_3,
      cube_multiple_qty,
      do_dtl_status,
      internal_order_seq_nbr,
      is_emergency,
      lpn_size,
      lpn_type,
      manufacturing_dttm,
      order_consol_attr,
      order_line_id,
      pack_rate,
      planned_ship_date,
      ppack_qty,                                    --release_update_dttm,
      sku_break_attr,
      std_bundle_qty,
      std_lpn_qty,
      std_lpn_vol,
      std_lpn_wt,
      unit_cost,
      unit_price_amount,
      unit_vol,
      unit_wt,
      user_canceled_qty,
      wave_proc_type,                      --purchase_orders_line_item_id,
      delivery_end_dttm,
      delivery_start_dttm,
      event_code,
      reason_code,
      exp_info_code,
      partl_fill,
      line_type,
      repl_proc_type,                                   --sensor_tag_type,
      order_qty,
      orig_order_qty,                                      --back_ord_qty,
      rtl_to_be_distroed_qty,
      price,
      retail_price,
      units_pakd,                              --units_pikd, verf_as_pakd,
      minor_order_nbr,
      item_id,
      tc_order_line_id,
      actual_shipped_dttm,
      pickup_end_dttm,
      purchase_order_line_number,
      pickup_start_dttm,
      reference_line_item_id,
      reference_order_id,
      repl_wave_run,
      std_pallet_qty,
      std_sub_pack_qty,
      store_dept,
      alloc_type,
      assort_nbr,
      batch_nbr,
      batch_requirement_type,
      chute_assign_type,
      custom_tag,
      customer_item,
      internal_order_id,
      invn_type,
      item_attr_1,
      item_attr_2,
      item_attr_3,
      item_attr_4,
      item_attr_5,
      lpn_brk_attrib,                                      --lpn_epc_type,
      merch_grp,
      merch_type,
      pack_zone,
      pallet_type,
      pick_locn_assign_type,
      pick_locn_id,
      ppack_grp_code,
      price_tkt_type,
      serial_number_required_flag,
      single_unit_flag,
      sku_gtin,
      sku_sub_code_id,
      sku_sub_code_value,
      vas_process_type,
      --work_type,
      substituted_parent_line_id,
      is_cancelled,
      shipped_qty,
      item_name,
      alloc_line_id,
      allocation_source_id,
      allocation_source_line_id,
      allocation_source,
      shelf_days
 from order_line_item
where order_id in (select order_id from orders_view);

CREATE OR REPLACE FORCE VIEW "ORDER_SIZE" ("ORDER_ID", "TC_COMPANY_ID", "SIZE_UOM_ID", "SIZE_VALUE") AS
select order_id,
        tc_company_id,
        size_uom_id,
        sum (size_value) as size_value
   from order_line_item_size
 group by order_id, tc_company_id, size_uom_id;

CREATE OR REPLACE FORCE VIEW "ORDER_SIZE_VIEW" ("ORDER_ID", "SIZE_VALUE", "SIZE_UOM_ID", "SUMMARY_VIEW_FLAG") AS
select olis.order_id,
        sum (olis.size_value),
        olis.size_uom_id,
        sud.summary_view_flag
   from order_line_item_size olis,
        size_uom_display sud,
        order_line_item oli
  where     olis.size_uom_id = sud.size_uom_id
        and olis.line_item_id = oli.line_item_id
        and olis.tc_company_id = sud.tc_company_id
        and oli.is_cancelled = '0'
 group by olis.order_id, olis.size_uom_id, sud.summary_view_flag;

CREATE OR REPLACE FORCE VIEW "PARTNER_SHIPPER_VIEW" ("PARTNER_COMPANY_ID", "PARTNER_COMPANY_NAME", "SHIPPER_COMPANY_ID", "SHIPPER_COMPANY_NAME") AS
select distinct bp.bp_company_id as partner_company_id,
               co1.company_name as partner_company_name,
               bp.tc_company_id as shipper_company_id,
               co2.company_name as shipper_company_name
 from business_partner bp, company co1, company co2
where     bp.mark_for_deletion = 0
      and bp.bp_company_id = co1.company_id
      and bp.tc_company_id = co2.company_id;

CREATE OR REPLACE FORCE VIEW "PIX_ACTIVITY" ("TRAN_TYPE", "TRAN_CODE", "ACTN_CODE", "ACTIVITY", "TRANSACTION_TYPE") AS
(select distinct
         tran_type,
         tran_code,
         actn_code,
         (case
             when tran_type = '100' and tran_code = '01'
             then
                'WMInHouseRcpt'
             when tran_type = '100' and tran_code = '02'
             then
                'WMOutSourceRcpt'
             when tran_type = '100' and tran_code = '03'
             then
                'WMReturnRcpt'
             when tran_type = '100' and tran_code = '04'
             then
                'WMXdockrcpt'
             when tran_type = '300' and tran_code = '01'
             then
                'WMMaintiLPN'
             when tran_type = '300' and tran_code = '02'
             then
                'WMMaintoLPN'
             when tran_type = '300' and tran_code = '04'
             then
                'WMMaintActive'
             when tran_type = '300' and tran_code = '05'
             then
                'WMMaintTrans'
             when tran_type = '300' and tran_code = '09'
             then
                'WMMaintiLPNPick'
             when tran_type = '606' and tran_code = '01'
             then
                'WMTransUnalloc'
             when     tran_type = '606'
                  and tran_code = '02'
                  and actn_code <> '02'
             then
                'WMiLPNUnalloc'
             when     tran_type = '606'
                  and tran_code = '02'
                  and actn_code = '02'
             then
                'WMTransUnalloc'
             when     tran_type = '606'
                  and tran_code in ('03', '04')
                  and actn_code = '*'
             then
                'WMRcptUnalloc'
             when     tran_type = '606'
                  and tran_code = '04'
                  and actn_code <> '*'
             then
                'WMActiveUnalloc'
             when tran_type = '606' and tran_code = '05'
             then
                'WMWOUnalloc'
             when tran_type = '606' and tran_code = '09'
             then
                'WMiLPNPkUnalloc'
             when tran_type = '608' and tran_code = '12'
             then
                'WMLockCodeChg'
          end)
            activity,
         (case
             when tran_type = '100'
             then
                'Receipt'
             when     tran_type = '300'
                  and tran_code = '01'
                  and actn_code in ('08', '10', '21', '30')
             then
                'Receipt'
             when     tran_type = '300'
                  and tran_code in ('04', '09')
                  and actn_code in ('21', '30')
             then
                'Receipt'
             when     tran_type = '606'
                  and tran_code = '02'
                  and actn_code in ('08', '10', '21', '30')
             then
                'Receipt'
             when     tran_type = '606'
                  and tran_code in ('03', '04')
                  and actn_code = '*'
             then
                'Receipt'
             when     tran_type = '606'
                  and tran_code in ('04', '09')
                  and actn_code in ('30')
             then
                'Receipt'
             when tran_type = '300' and tran_code = '01'
                  and actn_code in
                         ('01',
                          '02',
                          '07',
                          '09',
                          '11',
                          '13',
                          '14',
                          '15',
                          '17',
                          '18')
             then
                'Adjustment'
             when tran_type = '300' and tran_code = '02'
             then
                'Adjustment'
             when     tran_type = '300'
                  and tran_code in ('04', '09')
                  and (actn_code in ('*', '14'))
             then
                'Adjustment'
             when tran_type = '300' and tran_code = '05'
             then
                'Adjustment'
             when tran_type = '606' and tran_code = '01'
             then
                'Adjustment'
             when tran_type = '606' and tran_code = '02'
                  and actn_code in
                         ('01',
                          '07',
                          '09',
                          '11',
                          '13',
                          '14',
                          '15',
                          '17',
                          '18')
             then
                'Adjustment'
             when     tran_type = '606'
                  and tran_code in ('04', '09')
                  and actn_code in ('03', '14')
             then
                'Adjustment'
             when tran_type = '606' and tran_code = '05'
             then
                'Adjustment'
             when     tran_type = '606'
                  and tran_code = '02'
                  and actn_code in ('02')
             then
                'Adjustment'
             when     tran_type = '606'
                  and tran_code = '02'
                  and actn_code in ('05', '06', '19', '20')
             then
                'Movement'
             when     tran_type = '606'
                  and tran_code in ('04', '09')
                  and actn_code in ('01', '02')
             then
                'Movement'
             when tran_type = '608' and tran_code = '12'
             then
                'Movement'
          end)
            transaction_type
    from pix_tran_code
   where tran_type in ('100', '300', '606', '608'));

CREATE OR REPLACE FORCE VIEW "PKT_DTL_SUMMARY" ("PKT_CTRL_NBR", "SUM_UNITS_PAKD") AS
select pkt_dtl.pkt_ctrl_nbr, sum (units_pakd)
   from pkt_dtl
 group by pkt_ctrl_nbr;

CREATE OR REPLACE FORCE VIEW "RATE_LANE_DTL_RATE_VIEW_1DTLRW" ("TC_COMPANY_ID", "LANE_ID", "RATING_LANE_DTL_SEQ", "RLD_RATE_SEQ", "MINIMUM_SIZE", "MAXIMUM_SIZE", "SIZE_UOM_ID", "RATE_CALC_METHOD", "RATE_UOM", "RATE", "MINIMUM_RATE", "CURRENCY_CODE", "TARIFF_ID", "MIN_DISTANCE", "MAX_DISTANCE", "DISTANCE_UOM", "SUPPORTS_MSTL", "HAS_BH", "HAS_RT", "MIN_COMMODITY", "MAX_COMMODITY", "PARCEL_TIER", "EXCESS_WT_RATE", "COMMODITY_CODE_ID", "PAYEE_CARRIER_ID", "MAX_RANGE_COMMODITY_CODE_ID") AS
select tc_company_id,
      lane_id,
      rating_lane_dtl_seq,
      rld_rate_seq,
      minimum_size,
      maximum_size,
      size_uom_id,
      rate_calc_method,
      rate_uom,
      rate,
      minimum_rate,
      currency_code,
      tariff_id,
      min_distance,
      max_distance,
      distance_uom,
      supports_mstl,
      has_bh,
      has_rt,
      min_commodity,
      max_commodity,
      parcel_tier,
      excess_wt_rate,
      commodity_code_id,
      payee_carrier_id,
      max_range_commodity_code_id
 from rating_lane_dtl_rate rldr
where rldr.rld_rate_seq =
         (select min (rldr_1.rld_rate_seq)
            from rating_lane_dtl_rate rldr_1
           where rldr.tc_company_id = rldr_1.tc_company_id
                 and rldr.lane_id = rldr_1.lane_id
                 and rldr.rating_lane_dtl_seq =
                        rldr_1.rating_lane_dtl_seq);

CREATE OR REPLACE FORCE VIEW "RATE_LANE_DTL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "RATING_LANE_DTL_SEQ", "CARRIER_CODE", "CARRIER_ID", "CARRIER_CODE_STATUS", "IS_BUDGETED", "MOT", "MOT_ID", "EQUIPMENT_CODE", "EQUIPMENT_ID", "SERVICE_LEVEL", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL", "PROTECTION_LEVEL_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "EFF_DT", "EXP_DT", "HAS_ERRORS", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "SCNDR_CARRIER_CODE", "SCNDR_CARRIER_ID", "RATING_LANE_DTL_STATUS", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5", "PACKAGE_ID", "PACKAGE_NAME", "VOYAGE") AS
select rld.tc_company_id,
      rld.lane_id,
      rld.rating_lane_dtl_seq,
      nvl (cc.carrier_code, 'ALL'),                             --Was NULL
      rld.carrier_id,
      cc.carrier_code_status,                                   --Was NULL
      rld.is_budgeted,
      nvl (m.mot, 'ALL'),                                       --Was NULL
      rld.mot_id,
      nvl (eq.equipment_code, 'ALL'),                           --Was NULL
      rld.equipment_id,
      nvl (sl.service_level, 'ALL'),                            --Was NULL
      rld.service_level_id,
      nvl (pl.protection_level, 'ALL'),                         --Was NULL
      rld.protection_level_id,
      rld.effective_dt,
      rld.expiration_dt,
      coalesce (rld.effective_dt, to_date ('01-JAN-1950', 'dd-mon-yyyy')),
      coalesce (rld.expiration_dt, to_date ('31-DEC-49')),
      case rld.rating_lane_dtl_status when 4 then 1 else 0 end
         as has_errors,
      rld.last_updated_source_type,
      rld.last_updated_source,
      rld.last_updated_dttm,
      null,
      rld.scndr_carrier_id,
      rld.rating_lane_dtl_status,
      rld.o_ship_via,
      rld.d_ship_via,
      rld.contract_number,
      rld.custom_text1,
      rld.custom_text2,
      rld.custom_text3,
      rld.custom_text4,
      rld.custom_text5,
      rld.package_id,
      rld.package_name,
      rld.voyage
 from rating_lane_dtl rld
      left outer join carrier_code cc
         on (rld.carrier_id = cc.carrier_id)
      left outer join equipment eq
         on (rld.equipment_id = eq.equipment_id)
      left outer join mot m
         on (rld.mot_id = m.mot_id)
      left outer join service_level sl
         on (rld.service_level_id = sl.service_level_id)
      left outer join protection_level pl
         on (rld.protection_level_id = pl.protection_level_id)
where cc.carrier_id = rld.carrier_id and rld.rating_lane_dtl_status <> 2
 union all
 select irld.tc_company_id,
      rl.lane_id,
      irld.rating_lane_dtl_seq,
      nvl (irld.carrier_code, 'ALL'),
      icc.carrier_id,                                           --Was NULL
      icc.carrier_code_status,
      irld.is_budgeted,
      coalesce (irld.mot, 'ALL'),
      im.mot_id,
      --Was NULL                                                                                                        -- MOT_ID is not present in IMPORT_RATING_LANE, so we are using NULL
      coalesce (irld.equipment_code, 'ALL'),
      ieq.equipment_id,
      --Was NULL                                                                                                       -- EQUIPMENT_ID is not present in IMPORT_RATING_LANE, so we are using NULL
      coalesce (irld.service_level, 'ALL'),
      isl.service_level_id,
      --Was NULL                                                                                                      -- SERVICE_LEVEL_ID is not present in IMPORT_RATING_LANE, so we are using NULL
      coalesce (irld.protection_level, 'ALL'),
      ipl.protection_level_id,
      --Was NULL                                                                                                -- PROTECTION_LEVEL_ID is not present in IMPORT_RATING_LANE, so we are using NULL
      irld.effective_dt,
      irld.expiration_dt,
      coalesce (irld.effective_dt,
                to_date ('01-JAN-1950', 'dd-mon-yyyy')),
      coalesce (irld.expiration_dt, to_date ('31-DEC-49')),
      case irld.rating_lane_dtl_status when 4 then 1 else 0 end
         as has_errors,
      irld.last_updated_source_type,
      irld.last_updated_source,
      irld.last_updated_dttm,
      irld.scndr_carrier_code,
      null,
      irld.rating_lane_dtl_status,
      irld.o_ship_via,
      irld.d_ship_via,
      irld.contract_number,
      irld.custom_text1,
      irld.custom_text2,
      irld.custom_text3,
      irld.custom_text4,
      irld.custom_text5,
      irld.package_id,
      irld.package_name,
      irld.voyage
 from import_rating_lane irl
      join import_rating_lane_dtl irld
         on irl.lane_id = irld.lane_id
            and irl.tc_company_id = irld.tc_company_id
      join rating_lane rl
         on rl.tc_company_id = irl.tc_company_id
            and rl.lane_id = irl.rating_lane_id
      left outer join carrier_code icc
         on (irld.carrier_id = icc.carrier_id)
      left outer join equipment ieq
         on (irld.equipment_id = ieq.equipment_id)
      left outer join mot im
         on (irld.mot_id = im.mot_id)
      left outer join service_level isl
         on (irld.service_level_id = isl.service_level_id)
      left outer join protection_level ipl
         on (irld.protection_level_id = ipl.protection_level_id)
 union all
 select irld1.tc_company_id,
      irl1.rating_lane_id,
      irld1.rating_lane_dtl_seq,
      irld1.carrier_code,
      irld1.carrier_id,                                         --Was NULL
      null,                                                     --Was NULL
      irld1.is_budgeted,
      coalesce (irld1.mot, 'ALL'),
      im1.mot_id,                                               --Was NULL
      coalesce (irld1.equipment_code, 'ALL'),
      ieq1.equipment_id,                                        --Was NULL
      coalesce (irld1.service_level, 'ALL'),
      isl1.service_level_id,                                    --Was NULL
      coalesce (irld1.protection_level, 'ALL'),
      ipl1.protection_level_id,                                 --Was NULL
      irld1.effective_dt,
      irld1.expiration_dt,
      coalesce (irld1.effective_dt,
                to_date ('01-JAN-1950', 'dd-mon-yyyy')),
      coalesce (irld1.expiration_dt, to_date ('31-DEC-49')),
      case irld1.rating_lane_dtl_status when 4 then 1 else 0 end
         as has_errors,
      irld1.last_updated_source_type,
      irld1.last_updated_source,
      irld1.last_updated_dttm,
      irld1.scndr_carrier_code,
      null,
      irld1.rating_lane_dtl_status,
      irld1.o_ship_via,
      irld1.d_ship_via,
      irld1.contract_number,
      irld1.custom_text1,
      irld1.custom_text2,
      irld1.custom_text3,
      irld1.custom_text4,
      irld1.custom_text5,
      irld1.package_id,
      irld1.package_name,
      irld1.voyage
 from import_rating_lane irl1
      join import_rating_lane_dtl irld1
         on irl1.lane_id = irld1.lane_id
            and irl1.tc_company_id = irld1.tc_company_id
      left outer join carrier_code icc1
         on (irld1.carrier_id = icc1.carrier_id)
      left outer join equipment ieq1
         on (irld1.equipment_id = ieq1.equipment_id)
      left outer join mot im1
         on (irld1.mot_id = im1.mot_id)
      left outer join service_level isl1
         on (irld1.service_level_id = isl1.service_level_id)
      left outer join protection_level ipl1
         on (irld1.protection_level_id = ipl1.protection_level_id)
where not exists
             (select 1
                from rating_lane r
               where irl1.tc_company_id = r.tc_company_id
                     and irl1.rating_lane_id = r.lane_id)
      and irl1.lane_status = 4;

CREATE OR REPLACE FORCE VIEW "RATE_LANE_VALID_DTL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "RATING_LANE_DTL_SEQ", "CARRIER_ID", "CARRIER_CODE_STATUS", "MOT_ID", "EQUIPMENT_ID", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "EFF_DT", "EXP_DT", "HAS_ERRORS", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5") AS
select rld.tc_company_id,
      rld.lane_id,
      rld.rating_lane_dtl_seq,
      rld.carrier_id,
      cc.carrier_code_status,
      nvl (rld.mot_id, -1),
      nvl (rld.equipment_id, -1),
      nvl (rld.service_level_id, -1),
      nvl (rld.protection_level_id, -1),
      rld.effective_dt,
      rld.expiration_dt,
      nvl (rld.effective_dt, to_date ('01-JAN-1950', 'DD-MON-YYYY')),
      nvl (rld.expiration_dt, to_date ('31-DEC-2049', 'DD-MON-YYYY')),
      decode (rld.rating_lane_dtl_status, 4, 1, 0) has_errors,
      rld.last_updated_source_type,
      rld.last_updated_source,
      rld.last_updated_dttm,
      rld.o_ship_via,
      rld.d_ship_via,
      rld.contract_number,
      rld.custom_text1,
      rld.custom_text2,
      rld.custom_text3,
      rld.custom_text4,
      rld.custom_text5
 from rating_lane_dtl rld, carrier_code cc
where     rld.tc_company_id = cc.tc_company_id
      and rld.carrier_id = cc.carrier_id
      and rld.rating_lane_dtl_status = 0;

CREATE OR REPLACE FORCE VIEW "RATE_LANE_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_NAME", "BILLING_METHOD", "INCOTERM_ID", "CUSTOMER_ID") AS
select l.tc_company_id,
      l.lane_id,
      l.lane_hierarchy,
      l.lane_status,
      l.o_loc_type,
      l.o_facility_id,
      l.o_facility_alias_id,
      upper (l.o_city),
      l.o_state_prov,
      upper (l.o_county),
      l.o_postal_code,
      l.o_country_code,
      l.o_zone_id,
      '',
      l.d_loc_type,
      l.d_facility_id,
      l.d_facility_alias_id,
      upper (l.d_city),
      l.d_state_prov,
      upper (l.d_county),
      l.d_postal_code,
      l.d_country_code,
      l.d_zone_id,
      '',
      decode (l.lane_status, 4, 1, 0) has_errors,
      coalesce (
         (select sign (max (decode (id.rating_lane_dtl_status, 4, 1, 0)))
            from import_rating_lane ir, import_rating_lane_dtl id
           where (    l.lane_id = ir.rating_lane_id
                  and l.tc_company_id = ir.tc_company_id
                  and ir.lane_id = id.lane_id
                  and ir.tc_company_id = id.tc_company_id)),
         0)
         dtl_has_errors,
      l.created_source_type,
      l.created_source,
      l.created_dttm,
      l.last_updated_source_type,
      l.last_updated_source,
      l.last_updated_dttm,
      l.lane_name,
      l.billing_method,
      l.incoterm_id,
      l.customer_id
 from rating_lane l
 union all
 select i.tc_company_id,
      i.rating_lane_id,
      i.lane_hierarchy,
      i.lane_status,
      i.o_loc_type,
      i.o_facility_id,
      i.o_facility_alias_id,
      upper (i.o_city),
      i.o_state_prov,
      upper (i.o_county),
      i.o_postal_code,
      i.o_country_code,
      i.o_zone_id,
      i.o_zone_name,
      i.d_loc_type,
      i.d_facility_id,
      i.d_facility_alias_id,
      upper (i.d_city),
      i.d_state_prov,
      upper (i.d_county),
      i.d_postal_code,
      i.d_country_code,
      i.d_zone_id,
      i.d_zone_name,
      decode (i.lane_status, 4, 1, 0) has_errors,
      coalesce (
         (select max (decode (d.rating_lane_dtl_status, 4, 1, 0))
            from import_rating_lane_dtl d
           where d.lane_id = i.lane_id
                 and d.tc_company_id = i.tc_company_id),
         0)
         dtl_has_errors,
      i.created_source_type,
      i.created_source,
      i.created_dttm,
      i.last_updated_source_type,
      i.last_updated_source,
      i.last_updated_dttm,
      '',
      cast (null as int),
      cast (null as int),
      cast (null as int)
 from import_rating_lane i
where i.lane_status = 4
      and not exists
                 (select 1
                    from rating_lane r
                   where i.tc_company_id = r.tc_company_id
                         and i.rating_lane_id = r.lane_id);

CREATE OR REPLACE FORCE VIEW "RATING_LANE" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "O_SEARCH_LOCATION", "D_SEARCH_LOCATION", "LANE_UNIQUE_ID", "LANE_NAME", "BILLING_METHOD", "INCOTERM_ID", "CUSTOMER_ID") AS
select tc_company_id,
      lane_id,
      lane_hierarchy,
      lane_status,
      o_loc_type,
      o_facility_id,
      o_facility_alias_id,
      o_city,
      o_state_prov,
      o_county,
      o_postal_code,
      o_country_code,
      o_zone_id,
      '',
      d_loc_type,
      d_facility_id,
      d_facility_alias_id,
      d_city,
      d_state_prov,
      d_county,
      d_postal_code,
      d_country_code,
      d_zone_id,
      '',
      created_source_type,
      created_source,
      created_dttm,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      o_search_location,
      d_search_location,
      lane_unique_id,
      lane_name,
      billing_method,
      incoterm_id,
      customer_id
 from comb_lane
where is_rating = 1;

CREATE OR REPLACE FORCE VIEW "RATING_LANE_DTL" ("TC_COMPANY_ID", "LANE_ID", "RATING_LANE_DTL_SEQ", "CARRIER_ID", "MOT_ID", "EQUIPMENT_ID", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL_ID", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "RATING_LANE_DTL_STATUS", "EFFECTIVE_DT", "EXPIRATION_DT", "IS_BUDGETED", "SCNDR_CARRIER_ID", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5", "PACKAGE_ID", "PACKAGE_NAME", "SAILING_SCHEDULE_NAME", "VOYAGE") AS
select tc_company_id,
      lane_id,
      lane_dtl_seq rating_lane_dtl_seq,
      carrier_id,
      mot_id,
      equipment_id,
      service_level_id,
      protection_level_id,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      lane_dtl_status rating_lane_dtl_status,
      effective_dt,
      expiration_dt,
      is_budgeted,
      scndr_carrier_id,
      o_ship_via,
      d_ship_via,
      contract_number,
      custom_text1,
      custom_text2,
      custom_text3,
      custom_text4,
      custom_text5,
      package_id,
      package_name,
      sailing_schedule_name,
      voyage
 from comb_lane_dtl
where is_rating = 1;

CREATE OR REPLACE FORCE VIEW "REPROCESS_ERROR" ("EMP_ID", "LOGIN_USER_ID", "WHSE", "REPROCESS_STATUS", "CLOCK_IN_DATE", "CLOCK_OUT_DATE", "SEQ_NUM", "NXT_CLOCK_IN_DATE", "NEX_CLOCK_OUT_DATE", "PRE_CLOCK_IN_DATE", "PRE_CLOCK_OUT_DATE", "CLKIN_GRACE", "CLKOUT_GRACE", "EMP_PERF_SMRY_ID") AS
select c.emp_id,
      c.login_user_id,
      c.whse,
      c.reprocess_status,
      c.clock_in_date,
      c.clock_out_date,
      c.seq_num,
      fn_get_next_clock_in_date (c.whse,
                                 c.login_user_id,
                                 c.clock_in_date)
         nxt_clock_in_date,
      fn_get_next_clock_out_date (c.whse,
                                  c.login_user_id,
                                  c.clock_in_date)
         nex_clock_out_date,
      fn_get_previous_clock_in_date (c.whse,
                                     c.login_user_id,
                                     c.clock_in_date)
         pre_clock_in_date,
      fn_get_previous_clock_out_date (c.whse,
                                      c.login_user_id,
                                      c.clock_in_date)
         pre_clock_out_date,
      (select clockin_grace_period
         from whse_labor_parameters wd1
        where wd1.facility_id = (select facility_id
                                   from facility
                                  where whse = c.whse))
         clkin_grace,
      (select clockout_grace_period
         from whse_labor_parameters wd2
        where wd2.facility_id = (select facility_id
                                   from facility
                                  where whse = c.whse))
         clkout_grace,
      c.emp_perf_smry_id
 from e_emp_perf_smry c
where c.reprocess_status >= 900;

CREATE OR REPLACE FORCE VIEW "REPROCESS_ERROR_LOG_VIEW" ("USER_LAST_NAME", "USER_FIRST_NAME", "LOGIN_USER_ID", "ERROR_MSG", "CLOCK_IN_DATE", "CLOCK_OUT_DATE", "WHSE", "NXT_CLOCK_IN_DATE", "NEX_CLOCK_OUT_DATE", "REPROCESS_STATUS", "CLKIN_GRACE", "CLKOUT_GRACE", "PRE_CLOCK_IN_DATE", "PRE_CLOCK_OUT_DATE", "SCHED_START_DATE", "ACTUAL_END_DATE", "NAME", "TRAN_NBR") AS
select ucl_user.user_last_name,
      ucl_user.user_first_name,
      reprocess_error.login_user_id,
      e_error_log.error_msg,
      reprocess_error.clock_in_date,
      reprocess_error.clock_out_date,
      reprocess_error.whse,
      reprocess_error.nxt_clock_in_date,
      reprocess_error.nex_clock_out_date,
      reprocess_error.reprocess_status,
      reprocess_error.clkin_grace,
      reprocess_error.clkout_grace,
      reprocess_error.pre_clock_in_date,
      reprocess_error.pre_clock_out_date,
      hdr.sched_start_date,
      hdr.actual_end_date,
      lbr.name,
      hdr.tran_nbr
 from (   reprocess_error reprocess_error
       inner join
          ucl_user ucl_user
       on (reprocess_error.emp_id = ucl_user.ucl_user_id)
          and (reprocess_error.login_user_id = ucl_user.user_name))
      inner join e_error_log e_error_log
         on reprocess_error.seq_num = e_error_log.error_log_id
      left outer join (      e_evnt_smry_hdr hdr
                          inner join
                             e_act eact
                          on eact.act_id = hdr.act_id
                       inner join
                          labor_activity lbr
                       on lbr.labor_activity_id = eact.labor_activity_id)
         on     hdr.login_user_id = reprocess_error.login_user_id
            and hdr.whse = reprocess_error.whse
            and reprocess_error.emp_perf_smry_id = hdr.emp_perf_smry_id
            and ( (hdr.sched_start_date >= reprocess_error.clock_in_date
                   and hdr.actual_end_date >
                          reprocess_error.clock_out_date
                   and reprocess_error.reprocess_status = 901)
                 or (hdr.sched_start_date < reprocess_error.clock_in_date
                     and reprocess_error.reprocess_status > 901));

CREATE OR REPLACE FORCE VIEW "RG_LANE" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "RG_QUALIFIER", "FREQUENCY", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "O_SEARCH_LOCATION", "D_SEARCH_LOCATION", "LANE_NAME", "CUSTOMER_ID", "BILLING_METHOD", "INCOTERM_ID", "ROUTE_TO", "ROUTE_TYPE_1", "ROUTE_TYPE_2", "NO_RATING", "USE_FASTEST", "USE_PREFERENCE_BONUS") AS
select tc_company_id,
      lane_id,
      lane_hierarchy,
      lane_status,
      o_loc_type,
      o_facility_id,
      o_facility_alias_id,
      o_city,
      o_state_prov,
      o_county,
      o_postal_code,
      o_country_code,
      o_zone_id,
      '',
      d_loc_type,
      d_facility_id,
      d_facility_alias_id,
      d_city,
      d_state_prov,
      d_county,
      d_postal_code,
      d_country_code,
      d_zone_id,
      '',
      rg_qualifier,
      frequency,
      created_source_type,
      created_source,
      created_dttm,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      o_search_location,
      d_search_location,
      lane_name,
      customer_id,
      billing_method,
      incoterm_id,
      route_to,
      route_type_1,
      route_type_2,
      no_rating,
      use_fastest,
      use_preference_bonus
 from comb_lane
where is_routing = 1;

CREATE OR REPLACE FORCE VIEW "RG_LANE_DTL" ("TC_COMPANY_ID", "LANE_ID", "RG_LANE_DTL_SEQ", "CARRIER_ID", "MOT_ID", "EQUIPMENT_ID", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL_ID", "TIER_ID", "RANK", "REP_TP_FLAG", "WEEKLY_CAPACITY", "DAILY_CAPACITY", "CAPACITY_SUN", "CAPACITY_MON", "CAPACITY_TUE", "CAPACITY_WED", "CAPACITY_THU", "CAPACITY_FRI", "CAPACITY_SAT", "WEEKLY_COMMITMENT", "DAILY_COMMITMENT", "COMMITMENT_SUN", "COMMITMENT_MON", "COMMITMENT_TUE", "COMMITMENT_WED", "COMMITMENT_THU", "COMMITMENT_FRI", "COMMITMENT_SAT", "WEEKLY_COMMIT_PCT", "DAILY_COMMIT_PCT", "COMMIT_PCT_SUN", "COMMIT_PCT_MON", "COMMIT_PCT_TUE", "COMMIT_PCT_WED", "COMMIT_PCT_THU", "COMMIT_PCT_FRI", "COMMIT_PCT_SAT", "MONTHLY_CAPACITY", "YEARLY_CAPACITY", "MONTHLY_COMMITMENT", "YEARLY_COMMITMENT", "MONTHLY_COMMIT_PCT", "YEARLY_COMMIT_PCT", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "IS_PREFERRED", "LANE_DTL_STATUS", "EFFECTIVE_DT", "EXPIRATION_DT", "SCNDR_CARRIER_ID", "TT_TOLERANCE_FACTOR", "PACKAGE_ID", "PACKAGE_NAME", "SIZE_UOM_ID", "SP_LPN_TYPE", "SP_MIN_LPN_COUNT", "SP_MAX_LPN_COUNT", "SP_MIN_WEIGHT", "SP_MAX_WEIGHT", "SP_MIN_VOLUME", "SP_MAX_VOLUME", "SP_MIN_LINEAR_FEET", "SP_MAX_LINEAR_FEET", "SP_MIN_MONETARY_VALUE", "SP_MAX_MONETARY_VALUE", "SP_CURRENCY_CODE", "CUBING_INDICATOR", "NO_RATING", "IS_FASTEST", "IS_USE_PREFERENCE_BONUS", "PREFERENCE_BONUS_VALUE", "OVERRIDE_CODE", "RFP_PACKAGE_ID", "RFP_PACKAGE_REFERENCE", "HAS_SHIPPING_PARAM", "RG_SHIPPING_PARAM_ID", "VOYAGE", "REJECT_FURTHER_SHIPMENTS", "CARRIER_REJECT_PERIOD") AS
SELECT TC_COMPANY_ID,
      LANE_ID,
      LANE_DTL_SEQ RG_LANE_DTL_SEQ,
      CARRIER_ID,
      MOT_ID,
      EQUIPMENT_ID,
      SERVICE_LEVEL_ID,
      PROTECTION_LEVEL_ID,
      TIER_ID,
      RANK,
      REP_TP_FLAG,
      WEEKLY_CAPACITY,
      DAILY_CAPACITY,
      CAPACITY_SUN,
      CAPACITY_MON,
      CAPACITY_TUE,
      CAPACITY_WED,
      CAPACITY_THU,
      CAPACITY_FRI,
      CAPACITY_SAT,
      WEEKLY_COMMITMENT,
      DAILY_COMMITMENT,
      COMMITMENT_SUN,
      COMMITMENT_MON,
      COMMITMENT_TUE,
      COMMITMENT_WED,
      COMMITMENT_THU,
      COMMITMENT_FRI,
      COMMITMENT_SAT,
      WEEKLY_COMMIT_PCT,
      DAILY_COMMIT_PCT,
      COMMIT_PCT_SUN,
      COMMIT_PCT_MON,
      COMMIT_PCT_TUE,
      COMMIT_PCT_WED,
      COMMIT_PCT_THU,
      COMMIT_PCT_FRI,
      COMMIT_PCT_SAT,
      MONTLY_CAPACITY MONTHLY_CAPACITY,
      YEARLY_CAPACITY,
      MONTLY_COMMITMENT MONTHLY_COMMITMENT,
      YEARLY_COMMITMENT,
      MONTLY_COMMIT_PCT MONTHLY_COMMIT_PCT,
      YEARLY_COMMIT_PCT,
      LAST_UPDATED_SOURCE_TYPE,
      LAST_UPDATED_SOURCE,
      LAST_UPDATED_DTTM,
      IS_PREFERRED,
      LANE_DTL_STATUS,
      EFFECTIVE_DT,
      EXPIRATION_DT,
      SCNDR_CARRIER_ID,
      TT_TOLERANCE_FACTOR,
      PACKAGE_ID,
      PACKAGE_NAME,
      SIZE_UOM_ID,
      SP_LPN_TYPE,
      SP_MIN_LPN_COUNT,
      SP_MAX_LPN_COUNT,
      SP_MIN_WEIGHT,
      SP_MAX_WEIGHT,
      SP_MIN_VOLUME,
      SP_MAX_VOLUME,
      SP_MIN_LINEAR_FEET,
      SP_MAX_LINEAR_FEET,
      SP_MIN_MONETARY_VALUE,
      SP_MAX_MONETARY_VALUE,
      SP_CURRENCY_CODE,
      CUBING_INDICATOR,
      NO_RATING,
      IS_FASTEST,
      IS_USE_PREFERENCE_BONUS,
      PREFERENCE_BONUS_VALUE,
      OVERRIDE_CODE,
      RFP_PACKAGE_ID,
      rfP_PACKAGE_REFERENCE,
      HAS_SHIPPING_PARAM,
      RG_SHIPPING_PARAM_ID,
		  VOYAGE,
	  REJECT_FURTHER_SHIPMENTS,
	  CARRIER_REJECT_PERIOD
 FROM COMB_LANE_DTL
WHERE IS_ROUTING = 1;

CREATE OR REPLACE FORCE VIEW "RG_LANE_DTL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "RG_LANE_DTL_SEQ", "CARRIER_CODE", "CARRIER_ID", "CARRIER_CODE_STATUS", "MOT", "MOT_ID", "EQUIPMENT_CODE", "EQUIPMENT_ID", "SERVICE_LEVEL", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL", "PROTECTION_LEVEL_ID", "TIER_ID", "RANK", "REP_TP_FLAG", "IS_PREFERRED", "TP_COMPANY_ID", "WEEKLY_CAPACITY", "DAILY_CAPACITY", "CAPACITY_SUN", "CAPACITY_MON", "CAPACITY_TUE", "CAPACITY_WED", "CAPACITY_THU", "CAPACITY_FRI", "CAPACITY_SAT", "WEEKLY_COMMITMENT", "DAILY_COMMITMENT", "COMMITMENT_SUN", "COMMITMENT_MON", "COMMITMENT_TUE", "COMMITMENT_WED", "COMMITMENT_THU", "COMMITMENT_FRI", "COMMITMENT_SAT", "WEEKLY_COMMIT_PCT", "DAILY_COMMIT_PCT", "COMMIT_PCT_SUN", "COMMIT_PCT_MON", "COMMIT_PCT_TUE", "COMMIT_PCT_WED", "COMMIT_PCT_THU", "COMMIT_PCT_FRI", "COMMIT_PCT_SAT", "EFFECTIVE_DT", "EXPIRATION_DT", "EFF_DT", "EXP_DT", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "HAS_ERRORS", "SCNDR_CARRIER_CODE", "SCNDR_CARRIER_ID", "LANE_DTL_STATUS", "TT_TOLERANCE_FACTOR", "MONTHLY_CAPACITY", "YEARLY_CAPACITY", "MONTHLY_COMMITMENT", "YEARLY_COMMITMENT", "MONTHLY_COMMIT_PCT", "YEARLY_COMMIT_PCT", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5", "PACKAGE_ID", "PACKAGE_NAME", "SIZE_UOM_ID", "RG_SHIPPING_PARAM_ID", "SP_LPN_TYPE", "SP_MIN_LPN_COUNT", "SP_MAX_LPN_COUNT", "SP_MIN_WEIGHT", "SP_MAX_WEIGHT", "SP_MIN_VOLUME", "SP_MAX_VOLUME", "SP_MIN_LINEAR_FEET", "SP_MAX_LINEAR_FEET", "SP_MIN_MONETARY_VALUE", "SP_MAX_MONETARY_VALUE", "SP_CURRENCY_CODE", "CUBING_INDICATOR", "PREFERENCE_BONUS_VALUE", "OVERRIDE_CODE", "RFP_PACKAGE_ID", "RFP_PACKAGE_REFERENCE", "HAS_SHIPPING_PARAM", "VOYAGE", "REJECT_FURTHER_SHIPMENTS", "CARRIER_REJECT_PERIOD") AS
SELECT rld.TC_COMPANY_ID TC_COMPANY_ID,
      rld.LANE_ID LANE_ID,
      rld.LANE_DTL_SEQ RG_LANE_DTL_SEQ,
      '' CARRIER_CODE,
      rld.CARRIER_ID CARRIER_ID,
      cc.CARRIER_CODE_STATUS CARRIER_CODE_STATUS,
      '' MOT,
      rld.MOT_ID MOT_ID,
      '' EQUIPMENT_CODE,
      rld.EQUIPMENT_ID EQUIPMENT_ID,
      '' SERVICE_LEVEL,
      rld.SERVICE_LEVEL_ID SERVICE_LEVEL_ID,
      '' PROTECTION_LEVEL,
      rld.PROTECTION_LEVEL_ID PROTECTION_LEVEL_ID,
      NVL (rld.TIER_ID, 'ALL') TIER_ID,
      rld.RANK RANK,
      rld.REP_TP_FLAG REP_TP_FLAG,
      rld.IS_PREFERRED IS_PREFERRED,
      cc.TP_COMPANY_ID TP_COMPANY_ID,
      rld.WEEKLY_CAPACITY WEEKLY_CAPACITY,
      rld.DAILY_CAPACITY DAILY_CAPACITY,
      rld.CAPACITY_SUN CAPACITY_SUN,
      rld.CAPACITY_MON CAPACITY_MON,
      rld.CAPACITY_TUE CAPACITY_TUE,
      rld.CAPACITY_WED CAPACITY_WED,
      rld.CAPACITY_THU CAPACITY_THU,
      rld.CAPACITY_FRI CAPACITY_FRI,
      rld.CAPACITY_SAT CAPACITY_SAT,
      rld.WEEKLY_COMMITMENT WEEKLY_COMMITMENT,
      rld.DAILY_COMMITMENT DAILY_COMMITMENT,
      rld.COMMITMENT_SUN COMMITMENT_SUN,
      rld.COMMITMENT_MON COMMITMENT_MON,
      rld.COMMITMENT_TUE COMMITMENT_TUE,
      rld.COMMITMENT_WED COMMITMENT_WED,
      rld.COMMITMENT_THU COMMITMENT_THU,
      rld.COMMITMENT_FRI COMMITMENT_FRI,
      rld.COMMITMENT_SAT COMMITMENT_SAT,
      rld.WEEKLY_COMMIT_PCT WEEKLY_COMMIT_PCT,
      rld.DAILY_COMMIT_PCT DAILY_COMMIT_PCT,
      rld.COMMIT_PCT_SUN COMMIT_PCT_SUN,
      rld.COMMIT_PCT_MON COMMIT_PCT_MON,
      rld.COMMIT_PCT_TUE COMMIT_PCT_TUE,
      rld.COMMIT_PCT_WED COMMIT_PCT_WED,
      rld.COMMIT_PCT_THU COMMIT_PCT_THU,
      rld.COMMIT_PCT_FRI COMMIT_PCT_FRI,
      rld.COMMIT_PCT_SAT COMMIT_PCT_SAT,
      rld.EFFECTIVE_DT EFFECTIVE_DT,
      rld.EXPIRATION_DT EXPIRATION_DT,
      COALESCE (
         rld.EFFECTIVE_DT,
         TO_DATE ('1950-01-01-00.00.00.000000', 'yyyy-mm-dd hh24:mi:ss'))
         EFF_DT,
      COALESCE (
         rld.EXPIRATION_DT,
         TO_DATE ('2049-12-31-00.00.00.000000', 'yyyy-mm-dd hh24:mi:ss'))
         EXP_DT,
      rld.LAST_UPDATED_SOURCE_TYPE LAST_UPDATED_SOURCE_TYPE,
      rld.LAST_UPDATED_SOURCE LAST_UPDATED_SOURCE,
      rld.LAST_UPDATED_DTTM LAST_UPDATED_DTTM,
      DECODE (rld.LANE_DTL_STATUS, 4, 1, 0) HAS_ERRORS,
      '' SCNDR_CARRIER_CODE,
      rld.SCNDR_CARRIER_ID SCNDR_CARRIER_ID,
      rld.LANE_DTL_STATUS LANE_DTL_STATUS,
      rld.TT_TOLERANCE_FACTOR TT_TOLERANCE_FACTOR,
      rld.MONTLY_CAPACITY MONTHLY_CAPACITY,
      rld.YEARLY_CAPACITY YEARLY_CAPACITY,
      rld.MONTLY_COMMITMENT MONTHLY_COMMITMENT,
      rld.YEARLY_COMMITMENT YEARLY_COMMITMENT,
      rld.MONTLY_COMMIT_PCT MONTHLY_COMMIT_PCT,
      rld.YEARLY_COMMIT_PCT YEARLY_COMMIT_PCT,
      rld.CUSTOM_TEXT1 CUSTOM_TEXT1,
      rld.CUSTOM_TEXT2 CUSTOM_TEXT2,
      rld.CUSTOM_TEXT3 CUSTOM_TEXT3,
      rld.CUSTOM_TEXT4 CUSTOM_TEXT4,
      rld.CUSTOM_TEXT5 CUSTOM_TEXT5,
      rld.PACKAGE_ID PACKAGE_ID,
      rld.PACKAGE_NAME PACKAGE_NAME,
      rld.SIZE_UOM_ID SIZE_UOM_ID,
      rld.RG_SHIPPING_PARAM_ID RG_SHIPPING_PARAM_ID,
      rld.SP_LPN_TYPE SP_LPN_TYPE,      --  Gopalakrishnan 3/25/2008 Start
      rld.SP_MIN_LPN_COUNT SP_MIN_LPN_COUNT,
      rld.SP_MAX_LPN_COUNT SP_MAX_LPN_COUNT,
      rld.SP_MIN_WEIGHT SP_MIN_WEIGHT,
      rld.SP_MAX_WEIGHT SP_MAX_WEIGHT,
      rld.SP_MIN_VOLUME SP_MIN_VOLUME,
      rld.SP_MAX_VOLUME SP_MAX_VOLUME,
      rld.SP_MIN_LINEAR_FEET SP_MIN_LINEAR_FEET,
      rld.SP_MAX_LINEAR_FEET SP_MAX_LINEAR_FEET,
      rld.SP_MIN_MONETARY_VALUE SP_MIN_MONETARY_VALUE,
      rld.SP_MAX_MONETARY_VALUE SP_MAX_MONETARY_VALUE,
      rld.SP_CURRENCY_CODE SP_CURRENCY_CODE, --  Gopalakrishnan 3/25/2008 End
      rld.CUBING_INDICATOR CUBING_INDICATOR,
      rld.PREFERENCE_BONUS_VALUE PREFERENCE_BONUS_VALUE,
      rld.OVERRIDE_CODE OVERRIDE_CODE,
      rld.RFP_PACKAGE_ID RFP_PACKAGE_ID,
      rld.RFP_PACKAGE_REFERENCE RFP_PACKAGE_REFERENCE,
      rld.has_shipping_param HAS_SHIPPING_PARAM,
		  rld.voyage VOYAGE,
      rld.REJECT_FURTHER_SHIPMENTS REJECT_FURTHER_SHIPMENTS,
      rld.CARRIER_REJECT_PERIOD CARRIER_REJECT_PERIOD
 FROM comb_lane_dtl rld, carrier_code cc
WHERE     rld.carrier_id = cc.carrier_id
      AND rld.LANE_DTL_STATUS <> 2
      AND rld.IS_ROUTING = 1
 UNION ALL
 SELECT irld.TC_COMPANY_ID TC_COMPANY_ID,
      irl.RG_LANE_ID LANE_ID,
      irld.RG_LANE_DTL_SEQ RG_LANE_DTL_SEQ,
      irld.CARRIER_CODE CARRIER_CODE,
      CAST (NULL AS INT) CARRIER_ID,
      icc.CARRIER_CODE_STATUS CARRIER_CODE_STATUS,
      NVL (irld.MOT, 'ALL') MOT,
      CAST (NULL AS INT) MOT_ID,
      NVL (irld.EQUIPMENT_CODE, 'ALL') EQUIPMENT_CODE,
      CAST (NULL AS INT) EQUIPMENT_ID,
      NVL (irld.SERVICE_LEVEL, 'ALL') SERVICE_LEVEL,
      CAST (NULL AS INT) SERVICE_LEVEL_ID,
      NVL (irld.PROTECTION_LEVEL, 'ALL') PROTECTION_LEVEL,
      CAST (NULL AS INT) PROTECTION_LEVEL_ID,
      NVL (irld.TIER_ID, 'ALL') TIER_ID,
      irld.RANK RANK,
      irld.REP_TP_FLAG REP_TP_FLAG,
      irld.IS_PREFERRED IS_PREFERRED,
      icc.TP_COMPANY_ID TP_COMPANY_ID,
      irld.WEEKLY_CAPACITY WEEKLY_CAPACITY,
      irld.DAILY_CAPACITY DAILY_CAPACITY,
      irld.CAPACITY_SUN CAPACITY_SUN,
      irld.CAPACITY_MON CAPACITY_MON,
      irld.CAPACITY_TUE CAPACITY_TUE,
      irld.CAPACITY_WED CAPACITY_WED,
      irld.CAPACITY_THU CAPACITY_THU,
      irld.CAPACITY_FRI CAPACITY_FRI,
      irld.CAPACITY_SAT CAPACITY_SAT,
      irld.WEEKLY_COMMITMENT WEEKLY_COMMITMENT,
      irld.DAILY_COMMITMENT DAILY_COMMITMENT,
      irld.COMMITMENT_SUN COMMITMENT_SUN,
      irld.COMMITMENT_MON COMMITMENT_MON,
      irld.COMMITMENT_TUE COMMITMENT_TUE,
      irld.COMMITMENT_WED COMMITMENT_WED,
      irld.COMMITMENT_THU COMMITMENT_THU,
      irld.COMMITMENT_FRI COMMITMENT_FRI,
      irld.COMMITMENT_SAT COMMITMENT_SAT,
      irld.WEEKLY_COMMIT_PCT WEEKLY_COMMIT_PCT,
      irld.DAILY_COMMIT_PCT DAILY_COMMIT_PCT,
      irld.COMMIT_PCT_SUN COMMIT_PCT_SUN,
      irld.COMMIT_PCT_MON COMMIT_PCT_MON,
      irld.COMMIT_PCT_TUE COMMIT_PCT_TUE,
      irld.COMMIT_PCT_WED COMMIT_PCT_WED,
      irld.COMMIT_PCT_THU COMMIT_PCT_THU,
      irld.COMMIT_PCT_FRI COMMIT_PCT_FRI,
      irld.COMMIT_PCT_SAT COMMIT_PCT_SAT,
      irld.EFFECTIVE_DT EFFECTIVE_DT,
      irld.EXPIRATION_DT EXPIRATION_DT,
      COALESCE (
         irld.EXPIRATION_DT,
         TO_DATE ('1950-01-01-00.00.00.000000', 'yyyy-mm-dd hh24:mi:ss'))
         EFF_DT,
      COALESCE (
         irld.EXPIRATION_DT,
         TO_DATE ('2049-12-31-00.00.00.000000', 'yyyy-mm-dd hh24:mi:ss'))
         EXP_DT,
      irld.LAST_UPDATED_SOURCE_TYPE LAST_UPDATED_SOURCE_TYPE,
      irld.LAST_UPDATED_SOURCE LAST_UPDATED_SOURCE,
      irld.LAST_UPDATED_DTTM LAST_UPDATED_DTTM,
      DECODE (irld.LANE_DTL_STATUS, 4, 1, 0) HAS_ERRORS,
      irld.SCNDR_CARRIER_CODE SCNDR_CARRIER_CODE,
      CAST (NULL AS INT) SCNDR_CARRIER_ID,
      irld.LANE_DTL_STATUS LANE_DTL_STATUS,
      irld.TT_TOLERANCE_FACTOR TT_TOLERANCE_FACTOR,
      CAST (NULL AS INT) MONTHLY_CAPACITY,
      CAST (NULL AS INT) YEARLY_CAPACITY,
      CAST (NULL AS DECIMAL) MONTHLY_COMMITMENT,
      CAST (NULL AS DECIMAL) YEARLY_COMMITMENT,
      CAST (NULL AS DECIMAL) MONTHLY_COMMIT_PCT,
      CAST (NULL AS DECIMAL) YEARLY_COMMIT_PCT,
      '' CUSTOM_TEXT1,
      '' CUSTOM_TEXT2,
      '' CUSTOM_TEXT3,
      '' CUSTOM_TEXT4,
      '' CUSTOM_TEXT5,
      irld.PACKAGE_ID PACKAGE_ID,
      irld.PACKAGE_NAME PACKAGE_NAME,
      CAST (NULL AS INT) SIZE_UOM_ID,
      CAST (NULL AS INT) RG_SHIPPING_PARAM_ID,
      irld.SP_LPN_TYPE SP_LPN_TYPE,     --  Gopalakrishnan 3/25/2008 Start
      irld.SP_MIN_LPN_COUNT SP_MIN_LPN_COUNT,
      irld.SP_MAX_LPN_COUNT SP_MAX_LPN_COUNT,
      irld.SP_MIN_WEIGHT SP_MIN_WEIGHT,
      irld.SP_MAX_WEIGHT SP_MAX_WEIGHT,
      irld.SP_MIN_VOLUME SP_MIN_VOLUME,
      irld.SP_MAX_VOLUME SP_MAX_VOLUME,
      irld.SP_MIN_LINEAR_FEET SP_MIN_LINEAR_FEET,
      irld.SP_MAX_LINEAR_FEET SP_MAX_LINEAR_FEET,
      irld.SP_MIN_MONETARY_VALUE SP_MIN_MONETARY_VALUE,
      irld.SP_MAX_MONETARY_VALUE SP_MAX_MONETARY_VALUE,
      irld.SP_CURRENCY_CODE SP_CURRENCY_CODE, --  Gopalakrishnan 3/25/2008 End
      CAST (NULL AS INT) CUBING_INDICATOR,
      CAST (NULL AS INT) OVERRIDE_CODE,
      CAST (NULL AS INT) PREFERENCE_BONUS_VALUE,
      CAST (NULL AS INT) RFP_PACKAGE_ID,
      '' RFP_PACKAGE_REFERENCE,
      0 HAS_SHIPPING_PARAM,
		  irld.VOYAGE VOYAGE,
      0 REJECT_FURTHER_SHIPMENTS,
      0 CARRIER_REJECT_PERIOD
 FROM import_rg_lane_dtl irld
      LEFT OUTER JOIN carrier_code icc
         ON (irld.tc_company_id = icc.tc_company_id
             AND irld.carrier_code = icc.carrier_code)
      JOIN import_rg_lane irl
         ON irld.lane_id = irl.lane_id;

CREATE OR REPLACE FORCE VIEW "RG_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "RG_QUALIFIER", "FREQUENCY", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_NAME", "CUSTOMER_ID", "BILLING_METHOD", "INCOTERM_ID", "ROUTE_TO", "ROUTE_TYPE_1", "ROUTE_TYPE_2", "NO_RATING", "USE_FASTEST", "USE_PREFERENCE_BONUS") AS
select l.tc_company_id,
      l.lane_id,
      l.lane_hierarchy,
      l.lane_status,
      l.o_loc_type,
      l.o_facility_id,
      l.o_facility_alias_id,
      upper (l.o_city),
      l.o_state_prov,
      upper (l.o_county),
      l.o_postal_code,
      l.o_country_code,
      l.o_zone_id,
      '',
      l.d_loc_type,
      l.d_facility_id,
      l.d_facility_alias_id,
      upper (l.d_city),
      l.d_state_prov,
      upper (l.d_county),
      l.d_postal_code,
      l.d_country_code,
      l.d_zone_id,
      '',
      coalesce (l.rg_qualifier, 'ALL'),
      l.frequency,
      decode (l.lane_status, 4, 1, 0) has_errors,
      coalesce (
         (select sign (max (decode (id.lane_dtl_status, 4, 1, 0)))
            from import_rg_lane ir, import_rg_lane_dtl id
           where (    l.lane_id = ir.rg_lane_id
                  and l.tc_company_id = ir.tc_company_id
                  and ir.lane_id = id.lane_id
                  and ir.tc_company_id = id.tc_company_id)),
         0)
         dtl_has_errors,
      l.created_source_type,
      l.created_source,
      l.created_dttm,
      l.last_updated_source_type,
      l.last_updated_source,
      l.last_updated_dttm,
      l.lane_name,
      l.customer_id,
      l.billing_method,
      l.incoterm_id,
      l.route_to,
      l.route_type_1,
      l.route_type_2,
      l.no_rating,
      l.use_fastest,
      l.use_preference_bonus
 from rg_lane l
 union all
 select il.tc_company_id,
      il.rg_lane_id,
      il.lane_hierarchy,
      il.lane_status,
      il.o_loc_type,
      il.o_facility_id,
      il.o_facility_alias_id,
      upper (il.o_city),
      il.o_state_prov,
      upper (il.o_county),
      il.o_postal_code,
      il.o_country_code,
      il.o_zone_id,
      il.o_zone_name,
      il.d_loc_type,
      il.d_facility_id,
      il.d_facility_alias_id,
      upper (il.d_city),
      il.d_state_prov,
      upper (il.d_county),
      il.d_postal_code,
      il.d_country_code,
      il.d_zone_id,
      il.d_zone_name,
      coalesce (il.rg_qualifier, 'ALL'),
      il.frequency,
      decode (il.lane_status, 4, 1, 0) has_errors,
      coalesce (
         (select max (decode (d.lane_dtl_status, 4, 1, 0))
            from import_rg_lane_dtl d
           where d.lane_id = il.lane_id
                 and d.tc_company_id = il.tc_company_id),
         0)
         dtl_has_errors,
      il.created_source_type,
      il.created_source,
      il.created_dttm,
      il.last_updated_source_type,
      il.last_updated_source,
      il.last_updated_dttm,
      il.lane_name,
      il.customer_id,
      il.billing_method,
      il.incoterm_id,
      il.route_to,
      il.route_type_1,
      il.route_type_2,
      il.no_rating,
      il.use_fastest,
      il.use_preference_bonus
 from import_rg_lane il
where il.lane_status = 4
      and not exists
                 (select 1
                    from rg_lane r
                   where il.tc_company_id = r.tc_company_id
                         and il.rg_lane_id = r.lane_id);

CREATE OR REPLACE FORCE VIEW "SAILING_LANE" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "O_SEARCH_LOCATION", "D_SEARCH_LOCATION") AS
select tc_company_id,
      lane_id,
      lane_hierarchy,
      lane_status,
      o_loc_type,
      o_facility_id,
      o_facility_alias_id,
      o_city,
      o_state_prov,
      o_county,
      o_postal_code,
      o_country_code,
      o_zone_id,
      '',
      d_loc_type,
      d_facility_id,
      d_facility_alias_id,
      d_city,
      d_state_prov,
      d_county,
      d_postal_code,
      d_country_code,
      d_zone_id,
      '',
      created_source_type,
      created_source,
      created_dttm,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      o_search_location,
      d_search_location
 from comb_lane
where is_sailing = 1;

CREATE OR REPLACE FORCE VIEW "SAILING_LANE_DTL" ("TC_COMPANY_ID", "LANE_ID", "SAILING_LANE_DTL_SEQ", "CARRIER_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "MOT_ID", "SERVICE_LEVEL_ID", "SCNDR_CARRIER_ID", "FIXED_TRANSIT_VALUE", "FIXED_TRANSIT_STANDARD_UOM", "SAILING_SCHEDULE_NAME", "SAILING_FREQUENCY_TYPE", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_DTL_STATUS") AS
select tc_company_id,
      lane_id,
      lane_dtl_seq as sailing_lane_dtl_seq,
      carrier_id,
      effective_dt,
      expiration_dt,
      mot_id,
      service_level_id,
      scndr_carrier_id,
      fixed_transit_value,
      fixed_transit_standard_uom,
      sailing_schedule_name,
      sailing_frequency_type,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      lane_dtl_status
 from comb_lane_dtl
where is_sailing = 1;

CREATE OR REPLACE FORCE VIEW "SAILING_LANE_DTL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "SAILING_LANE_DTL_SEQ", "CARRIER_CODE", "CARRIER_ID", "CARRIER_CODE_STATUS", "MOT", "MOT_ID", "SERVICE_LEVEL", "SERVICE_LEVEL_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "EFF_DT", "EXP_DT", "HAS_ERRORS", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "SCNDR_CARRIER_CODE", "SCNDR_CARRIER_ID", "LANE_DTL_STATUS", "FIXED_TRANSIT_VALUE", "FIXED_TRANSIT_STANDARD_UOM", "SAILING_SCHEDULE_NAME", "SAILING_FREQUENCY_TYPE") AS
select sld.tc_company_id,
      sld.lane_id,
      sld.sailing_lane_dtl_seq,
      coalesce (cc.carrier_code, 'ALL'),
      sld.carrier_id,
      cc.carrier_code_status,
      coalesce (m.mot, 'ALL'),
      sld.mot_id,
      coalesce (sl.service_level, 'ALL'),
      sld.service_level_id,
      sld.effective_dt,
      sld.expiration_dt,
      coalesce (sld.effective_dt,
                to_date ('1950-01-01-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      coalesce (sld.expiration_dt,
                to_date ('2049-12-31-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      case sld.lane_dtl_status when 4 then 1 else 0 end as has_errors,
      sld.last_updated_source_type,
      sld.last_updated_source,
      sld.last_updated_dttm,
      cast (null as varchar (10)),
      sld.scndr_carrier_id,
      sld.lane_dtl_status,
      fixed_transit_value,
      fixed_transit_standard_uom,
      sailing_schedule_name,
      sailing_frequency_type
 from sailing_lane_dtl sld
      left outer join carrier_code cc
         on (sld.carrier_id = cc.carrier_id)
      left outer join mot m
         on (sld.mot_id = m.mot_id)
      left outer join service_level sl
         on (sld.service_level_id = sl.service_level_id)
where cc.carrier_id = sld.carrier_id and sld.lane_dtl_status <> 2
 union all
 select isld.tc_company_id,
      rl.lane_id,
      isld.sailing_lane_dtl_seq,
      coalesce (isld.carrier_code, 'ALL'),
      icc.carrier_id,
      icc.carrier_code_status,
      coalesce (isld.mot, 'ALL'),
      im.mot_id,
      coalesce (isld.service_level, 'ALL'),
      isl.service_level_id,
      isld.effective_dt,
      isld.expiration_dt,
      coalesce (isld.effective_dt,
                to_date ('1950-01-01-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      coalesce (isld.expiration_dt,
                to_date ('2049-12-31-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      case isld.lane_dtl_status when 4 then 1 else 0 end as has_errors,
      isld.last_updated_source_type,
      isld.last_updated_source,
      isld.last_updated_dttm,
      isld.scndr_carrier_code,
      cast (null as integer),
      isld.lane_dtl_status,
      fixed_transit_value,
      fixed_transit_standard_uom,
      sailing_schedule_name,
      sailing_frequency_type
 from import_sailing_lane irl
      join import_sailing_lane_dtl isld
         on irl.lane_id = isld.lane_id
            and irl.tc_company_id = isld.tc_company_id
      join sailing_lane rl
         on rl.tc_company_id = irl.tc_company_id
            and rl.lane_id = irl.sailing_lane_id
      left outer join carrier_code icc
         on (isld.carrier_id = icc.carrier_id)
      left outer join mot im
         on (isld.mot_id = im.mot_id)
      left outer join service_level isl
         on (isld.service_level_id = isl.service_level_id)
 union all
 select isld1.tc_company_id,
      irl1.sailing_lane_id,
      isld1.sailing_lane_dtl_seq,
      isld1.carrier_code,
      cast (null as integer),
      cast (null as smallint),
      coalesce (isld1.mot, 'ALL'),
      im1.mot_id,
      coalesce (isld1.service_level, 'ALL'),
      isl1.service_level_id,
      isld1.effective_dt,
      isld1.expiration_dt,
      coalesce (isld1.effective_dt,
                to_date ('1950-01-01-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      coalesce (isld1.expiration_dt,
                to_date ('2049-12-31-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      case isld1.lane_dtl_status when 4 then 1 else 0 end as has_errors,
      isld1.last_updated_source_type,
      isld1.last_updated_source,
      isld1.last_updated_dttm,
      isld1.scndr_carrier_code,
      cast (null as integer),
      isld1.lane_dtl_status,
      fixed_transit_value,
      fixed_transit_standard_uom,
      sailing_schedule_name,
      sailing_frequency_type
 from import_sailing_lane irl1
      join import_sailing_lane_dtl isld1
         on irl1.lane_id = isld1.lane_id
            and irl1.tc_company_id = isld1.tc_company_id
      left outer join carrier_code icc1
         on (isld1.tc_company_id = icc1.tc_company_id
             and isld1.carrier_id = icc1.carrier_id)
      left outer join mot im1
         on (isld1.mot_id = im1.mot_id)
      left outer join service_level isl1
         on (isld1.service_level_id = isl1.service_level_id)
where not exists
             (select 1
                from rating_lane r
               where irl1.tc_company_id = r.tc_company_id
                     and irl1.sailing_lane_id = r.lane_id)
      and irl1.lane_status = 4;

CREATE OR REPLACE FORCE VIEW "SAILING_LANE_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM") AS
select l.tc_company_id,
      l.lane_id,
      l.lane_hierarchy,
      l.lane_status,
      l.o_loc_type,
      l.o_facility_id,
      l.o_facility_alias_id,
      upper (l.o_city),
      l.o_state_prov,
      upper (l.o_county),
      l.o_postal_code,
      l.o_country_code,
      l.o_zone_id,
      '',
      l.d_loc_type,
      l.d_facility_id,
      l.d_facility_alias_id,
      upper (l.d_city),
      l.d_state_prov,
      upper (l.d_county),
      l.d_postal_code,
      l.d_country_code,
      l.d_zone_id,
      '',
      decode (cast (l.lane_status as int), 4, 1, 0) has_errors,
      coalesce (
         (select sign (
                    max (
                       decode (cast (sid.lane_dtl_status as int),
                               4, 1,
                               0)))
            from import_sailing_lane ir, import_sailing_lane_dtl sid
           where (    l.lane_id = ir.sailing_lane_id
                  and l.tc_company_id = ir.tc_company_id
                  and ir.lane_id = sid.lane_id
                  and ir.tc_company_id = sid.tc_company_id)),
         0)
         dtl_has_errors,
      l.created_source_type,
      l.created_source,
      l.created_dttm,
      l.last_updated_source_type,
      l.last_updated_source,
      l.last_updated_dttm
 from sailing_lane l
 union all
 select i.tc_company_id,
      i.sailing_lane_id,
      i.lane_hierarchy,
      i.lane_status,
      i.o_loc_type,
      i.o_facility_id,
      i.o_facility_alias_id,
      upper (i.o_city),
      i.o_state_prov,
      upper (i.o_county),
      i.o_postal_code,
      i.o_country_code,
      i.o_zone_id,
      i.o_zone_name,
      i.d_loc_type,
      i.d_facility_id,
      i.d_facility_alias_id,
      upper (i.d_city),
      i.d_state_prov,
      upper (i.d_county),
      i.d_postal_code,
      i.d_country_code,
      i.d_zone_id,
      i.d_zone_name,
      decode (cast (i.lane_status as int), 4, 1, 0) has_errors,
      coalesce (
         (select max (decode (cast (d.lane_dtl_status as int), 4, 1, 0))
            from import_sailing_lane_dtl d
           where d.lane_id = i.lane_id
                 and d.tc_company_id = i.tc_company_id),
         0)
         dtl_has_errors,
      i.created_source_type,
      i.created_source,
      i.created_dttm,
      i.last_updated_source_type,
      i.last_updated_source,
      i.last_updated_dttm
 from import_sailing_lane i
where i.lane_status = 4
      and not exists
                 (select 1
                    from sailing_lane r
                   where i.tc_company_id = r.tc_company_id
                         and i.sailing_lane_id = r.lane_id);

CREATE OR REPLACE FORCE VIEW "SCHED_ASSIGN_VIEW_SH" ("SCHEDULE_RSLT_HDR_ID", "SCHEDULE_DATE", "SHIFT_CODE", "SPVSR_LASTNAME", "SPVSR_FIRSTNAME", "LAST_NAME", "FIRST_NAME", "NAME", "START_TIME", "END_TIME", "SPVSR_LOGIN_USER_ID", "SCHEDULE_USER_ID", "SHIFT_ID", "JOB_FUNC_ID", "SCHED_DAY_ID") AS
select sa.schedule_rslt_hdr_id,
        sa.schedule_date,
        sh.shift_code,
        emp.spvsr_lastname,
        emp.spvsr_firstname,
        emp.last_name,
        emp.first_name,
        jf.name,
        sa.start_time,
        sa.end_time,
        emp.spvsr_login_user_id,
        sa.schedule_user_id,
        sa.shift_id,
        sa.job_func_id,
        datepart ('DW', sa.schedule_date, sh.whse) sched_day_id
   from e_schedule_asgmnt sa
        inner join e_shift sh
           on sh.shift_id = sa.shift_id
        inner join e_job_function jf
           on sa.job_func_id = jf.job_func_id and jf.whse = sh.whse
        inner join view_emp_latest_info emp
           on sa.schedule_user_id = emp.emp_id and emp.whse = jf.whse
 order by schedule_date,
        shift_code,
        spvsr_lastname,
        spvsr_firstname,
        last_name,
        first_name,
        start_time;

CREATE OR REPLACE FORCE VIEW "SCHED_ASSIGN_VIEW_SP" ("SCHEDULE_RSLT_HDR_ID", "SCHEDULE_DATE", "SPVSR_LASTNAME", "SPVSR_FIRSTNAME", "SHIFT_CODE", "LAST_NAME", "FIRST_NAME", "NAME", "START_TIME", "END_TIME", "SPVSR_LOGIN_USER_ID", "SCHEDULE_USER_ID", "SHIFT_ID", "JOB_FUNC_ID", "SCHED_DAY_ID") AS
select sa.schedule_rslt_hdr_id,
        sa.schedule_date,
        emp.spvsr_lastname,
        emp.spvsr_firstname,
        sh.shift_code,
        emp.last_name,
        emp.first_name,
        jf.name,
        sa.start_time,
        sa.end_time,
        emp.spvsr_login_user_id,
        sa.schedule_user_id,
        sa.shift_id,
        sa.job_func_id,
        datepart ('DW', sa.schedule_date, sh.whse) sched_day_id
   from e_schedule_asgmnt sa
        inner join e_shift sh
           on sh.shift_id = sa.shift_id
        inner join e_job_function jf
           on sa.job_func_id = jf.job_func_id and jf.whse = sh.whse
        inner join view_emp_latest_info emp
           on sa.schedule_user_id = emp.emp_id and emp.whse = jf.whse
 order by schedule_date,
        spvsr_lastname,
        spvsr_firstname,
        shift_code,
        last_name,
        first_name,
        start_time;

CREATE OR REPLACE FORCE VIEW "SCH_DAY_SP_SH_EMP_PG_SH" ("SCHEDULE_DATE", "SHIFT_CODE", "SPVSR_LASTNAME", "SPVSR_FIRSTNAME", "SPVSR_LOGIN_USER_ID", "SCHEDULE_USER_ID", "LAST_NAME", "FIRST_NAME", "SCHEDULE_RSLT_HDR_ID", "WHSE", "PAGE") AS
select schedule_date,
        shift_code,
        spvsr_lastname,
        spvsr_firstname,
        spvsr_login_user_id,
        schedule_user_id,
        last_name,
        first_name,
        schedule_rslt_hdr_id,
        whse,
        ceil (
           row_number ()
           over (
              partition by whse,
                           schedule_rslt_hdr_id,
                           schedule_date,
                           shift_code,
                           spvsr_lastname,
                           spvsr_firstname
              order by
                 whse,
                 schedule_rslt_hdr_id,
                 schedule_date,
                 shift_code,
                 spvsr_lastname,
                 spvsr_firstname)
           / 20)
           page
   from (  select distinct sa.schedule_date,
                           sa.shift_code,
                           sa.spvsr_lastname,
                           sa.spvsr_firstname,
                           sa.spvsr_login_user_id,
                           sa.last_name,
                           sa.first_name,
                           sa.schedule_user_id,
                           srh.schedule_rslt_hdr_id,
                           srh.whse
             from    sched_assign_view_sh sa
                  inner join
                     e_schedule_rslt_hdr srh
                  on srh.schedule_rslt_hdr_id = sa.schedule_rslt_hdr_id
         order by sa.schedule_date,
                  sa.shift_code,
                  sa.spvsr_lastname,
                  sa.spvsr_firstname,
                  sa.last_name,
                  sa.first_name) outerv
 order by whse,
        schedule_rslt_hdr_id,
        schedule_date,
        shift_code,
        spvsr_lastname,
        spvsr_firstname,
        page,
        last_name,
        first_name;

CREATE OR REPLACE FORCE VIEW "SCH_DAY_SP_SH_EMP_PG_SP" ("SCHEDULE_DATE", "SPVSR_LASTNAME", "SPVSR_FIRSTNAME", "SPVSR_LOGIN_USER_ID", "SHIFT_CODE", "SCHEDULE_USER_ID", "LAST_NAME", "FIRST_NAME", "SCHEDULE_RSLT_HDR_ID", "WHSE", "PAGE") AS
select schedule_date,
        spvsr_lastname,
        spvsr_firstname,
        spvsr_login_user_id,
        shift_code,
        schedule_user_id,
        last_name,
        first_name,
        schedule_rslt_hdr_id,
        whse,
        ceil (
           row_number ()
           over (
              partition by whse,
                           schedule_rslt_hdr_id,
                           schedule_date,
                           spvsr_lastname,
                           spvsr_firstname,
                           shift_code
              order by
                 whse,
                 schedule_rslt_hdr_id,
                 schedule_date,
                 spvsr_lastname,
                 spvsr_firstname,
                 shift_code)
           / 20)
           page
   from (  select distinct sa.schedule_date,
                           sa.spvsr_lastname,
                           sa.spvsr_firstname,
                           sa.spvsr_login_user_id,
                           sa.shift_code,
                           sa.last_name,
                           sa.first_name,
                           sa.schedule_user_id,
                           srh.schedule_rslt_hdr_id,
                           srh.whse
             from    sched_assign_view_sp sa
                  inner join
                     e_schedule_rslt_hdr srh
                  on srh.schedule_rslt_hdr_id = sa.schedule_rslt_hdr_id
         order by sa.schedule_date,
                  sa.spvsr_lastname,
                  sa.spvsr_firstname,
                  sa.shift_code,
                  sa.last_name,
                  sa.first_name) outerv
 order by whse,
        schedule_rslt_hdr_id,
        schedule_date,
        spvsr_lastname,
        spvsr_firstname,
        shift_code,
        page,
        last_name,
        first_name;

CREATE OR REPLACE FORCE VIEW "SHIPMENT_SIZE_VIEW" ("SHIPMENT_ID", "SIZE_VALUE", "TC_COMPANY_ID", "SIZE_UOM_ID", "SUMMARY_VIEW_FLAG") AS
select sc.shipment_id,
        sum (sc.size_value) size_value,
        sc.tc_company_id,
        sc.size_uom_id,
        sud.summary_view_flag
   from shipment_commodity sc, size_uom_display sud
  where sc.tc_company_id = sud.tc_company_id
        and sc.size_uom_id = sud.size_uom_id
 group by sc.tc_company_id,
        sc.shipment_id,
        sc.size_uom_id,
        sud.summary_view_flag;

CREATE OR REPLACE FORCE VIEW "SHIPMENT_TEMPLATE_SIZE_VIEW" ("SHIPMENT_ID", "SIZE_VALUE", "SIZE_UOM_ID", "SUMMARY_VIEW_FLAG") AS
select sc.shipment_id,
        sum (sc.size_value),
        sc.size_uom_id,
        sud.summary_view_flag
   from shipment_commodity_template sc, size_uom_display sud
  where sc.tc_company_id = sud.tc_company_id
        and sc.size_uom_id = sud.size_uom_id
 group by sc.shipment_id, sc.size_uom_id, sud.summary_view_flag;

CREATE OR REPLACE FORCE VIEW "SHIPMENT_WAYPOINT" ("SHIPMENT_ID", "TC_COMPANY_ID", "O_FACILITY_NUMBER", "O_FACILITY_ID", "O_ADDRESS", "O_CITY", "O_STATE_PROV", "O_POSTAL_CODE", "O_COUNTY", "O_COUNTRY_CODE", "D_FACILITY_NUMBER", "D_FACILITY_ID", "D_ADDRESS", "D_CITY", "D_STATE_PROV", "D_POSTAL_CODE", "D_COUNTY", "D_COUNTRY_CODE", "CONS_RUN_ID", "PICKUP_START_DTTM", "SHIPMENT_STATUS", "PRODUCT_CLASS_ID", "PROTECTION_LEVEL_ID", "INBOUND_REGION_ID", "OUTBOUND_REGION_ID", "FACILITY_ALIAS_ID") AS
select distinct sh.shipment_id,
               sh.tc_company_id,
               sh.o_facility_number,
               sh.o_facility_id,
               sh.o_address,
               sh.o_city,
               sh.o_state_prov,
               sh.o_postal_code,
               sh.o_county,
               sh.o_country_code,
               sh.d_facility_number,
               sh.d_facility_id,
               sh.d_address,
               sh.d_city,
               sh.d_state_prov,
               sh.d_postal_code,
               sh.d_county,
               sh.d_country_code,
               sh.cons_run_id,
               sh.pickup_start_dttm,
               sh.shipment_status,
               sh.product_class_id,
               sh.protection_level_id,
               sh.inbound_region_id,
               sh.outbound_region_id,
               pw.facility_alias_id
 from shipment sh, order_movement om, path_waypoint pw
where     sh.shipment_id = om.shipment_id
      and om.path_set_id = pw.path_set_id
      and om.path_id = pw.path_id;

CREATE OR REPLACE FORCE VIEW "SHIP_INFO_PO_VIEW" ("PONUMBER", "SHIPMENT_ID") AS
(Select Oi.Purchase_Order_Number, oomv.shipment_id From Order_Line_Item Oi,Orders_Order_Movement_View Oomv
Where Oi.Order_Id = Oomv.Order_Id And Oi.Purchase_Order_Number Is Not Null And Oi.Master_Order_Id Is Null
And Oi.Is_Cancelled = 0) Union
(Select Orders.Purchase_Order_Number, oomv.shipment_id From Orders Orders,
Orders_Order_Movement_View Oomv Where Orders.Order_Id = Oomv.Order_Id And Orders.Is_Cancelled = 0 And
ORDERS.PURCHASE_ORDER_NUMBER IS NOT NULL ) UNION
(Select Purchase_Order,Shipment_Id From Shipment Where Shipment.Purchase_Order Is Not Null);

CREATE OR REPLACE FORCE VIEW "SHPMT_ALLOCATION_SET_VW" ("ORDER_ID", "SHIPMENT_ID", "TC_SHIPMENT_ID", "LINE_ITEM_ID", "TOTAL_LPN_QTY") AS
select o.order_id, l.shipment_id, l.tc_shipment_id, prnt.line_item_id,
coalesce(sum(ld.size_value + decode(ld.lpn_detail_status, 90, 0, ld.initial_qty)), 0)
from orders o
join order_line_item oli on oli.order_id = o.order_id
and oli.do_dtl_status <= 190
join order_line_item prnt on prnt.order_id = o.order_id
and prnt.line_item_id = coalesce(oli.substituted_parent_line_id, oli.line_item_id)
join lpn l on l.order_id = o.order_id and l.lpn_facility_status < 99
and l.inbound_outbound_indicator = 'O' and l.tc_shipment_id is not null
join lpn_detail ld on ld.lpn_id = l.lpn_id
and ld.distribution_order_dtl_id = oli.line_item_id
where o.has_split = 1
group by o.order_id, l.shipment_id, l.tc_shipment_id, prnt.line_item_id,
ld.lpn_detail_status
order by o.order_id, l.tc_shipment_id;

CREATE OR REPLACE FORCE VIEW "SKU" ("SKU_ID", "TC_COMPANY_ID", "SKU", "COMMODITY_CLASS", "DESCRIPTION", "MARK_FOR_DELETION", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "SKU_VALUE", "CURRENCY_CODE", "COMMODITY_CODE_ID", "UN_NUMBER_ID", "REBATE_VALUE", "PRODUCT_CLASS_ID", "PROTECTION_LEVEL_ID", "SIZE_UOM_ID", "INVENTORY_SIZE_UOM_ID", "BASE_STORAGE_UOM_ID", "DATABASE_QTY_UOM_ID", "DISPLAY_QTY_UOM_ID") AS
select item_cbo.item_id,
      item_cbo.company_id,
      item_cbo.item_name,
      item_cbo.commodity_class_id,
      item_cbo.description,
      item_cbo.mark_for_deletion,
      item_cbo.audit_created_source_type,
      item_cbo.audit_created_source,
      item_cbo.audit_created_dttm,
      item_cbo.audit_last_updated_source_type,
      item_cbo.audit_last_updated_source,
      item_cbo.audit_last_updated_dttm,
      item_tms.item_value,
      item_tms.currency_code,
      item_cbo.commodity_code_id,
      item_cbo.un_number_id,
      item_tms.rebate_value,
      item_cbo.product_class_id,
      item_cbo.protection_level_id,
      item_tms.uom_id,
      item_tms.uom_id,
      item_cbo.base_storage_uom_id,
      item_cbo.database_qty_uom_id,
      item_cbo.display_qty_uom_id
 from    item_cbo item_cbo
      left outer join
         item_tms item_tms
      on (item_cbo.item_id = item_tms.item_id);

CREATE OR REPLACE FORCE VIEW "SKU_ATTRIBUTE" ("SKU_ID", "SKU_SEASON", "SKU_SEASON_YEAR", "SKU_STYLE", "SKU_STYLE_SFX", "SKU_COLOR", "SKU_COLOR_SFX", "SKU_SECOND_DIM", "SKU_QUALITY", "SKU_SIZE_DESC", "SKU_BAR_CODE", "SKU_DESC", "SKU_DESC_SHORT", "SKU_UPC_GTIN", "IS_SEASONAL", "SEASON_CODE", "IS_BONDED", "UNIT_TAX_AMOUNT", "VARIABLE_WEIGHT", "SKU_QUALITY_CODE", "SKU_SIZE_RANGE_CODE") AS
select item_cbo.item_id,
      item_cbo.item_season,
      item_cbo.item_season_year,
      item_cbo.item_style,
      item_cbo.item_style_sfx,
      item_cbo.item_color,
      item_cbo.item_color_sfx,
      item_cbo.item_second_dim,
      item_cbo.item_quality,
      item_cbo.item_size_desc,
      item_cbo.item_bar_code,
      item_cbo.item_name,
      item_cbo.item_desc_short,
      item_cbo.item_upc_gtin,
      item_tms.is_seasonal,
      item_tms.season_code,
      item_tms.is_bonded,
      item_tms.unit_tax_amount,
      item_cbo.variable_weight,
      item_cbo.item_quality_code,
      item_wms.size_range_code
 from item_cbo item_cbo
      left outer join item_tms item_tms
         on (item_cbo.item_id = item_tms.item_id)
      left outer join item_wms item_wms
         on (item_cbo.item_id = item_wms.item_id)
where    (trim (item_bar_code) is not null)
      or (trim (item_color) is not null)
      or (trim (item_color_sfx) is not null)
      or (trim (item_desc_short) is not null)
      or (trim (item_quality) is not null)
      or item_quality_code is not null
      or (trim (item_season) is not null)
      or (trim (item_season_year) is not null)
      or (trim (item_second_dim) is not null)
      or (trim (item_size_desc) is not null)
      or (trim (item_style) is not null)
      or (trim (item_style_sfx) is not null)
      or (trim (item_upc_gtin) is not null)
      or (trim (item_wms.size_range_code) is not null)
      or (item_tms.is_seasonal != 0 and item_tms.is_seasonal is not null)
      or trim (item_tms.season_code) is not null
      or (item_tms.is_seasonal != 0 and item_tms.is_seasonal is not null)
      or (item_tms.unit_tax_amount != 0
          and item_tms.unit_tax_amount is not null)
      or (variable_weight is not null and variable_weight != 0);

CREATE OR REPLACE FORCE VIEW "SKU_ATTRIBUTE_HIDDEN" ("SKU_ID", "BATCH_REQD", "PROD_STAT_REQD", "CNTRY_OF_ORGN_REQD", "CATCH_WT", "PROD_GROUP", "PROD_SUB_GRP", "PROD_TYPE", "PROD_LINE", "SALE_GRP", "OPER_CODE", "STORE_DEPT", "ACTVTN_DATE", "COORD_1", "COORD_2", "PROD_LIFE_IN_DAY", "VOLTY_CODE", "MERCH_TYPE", "MERCH_GROUP", "CRUSH_CODE", "CONVEY_FLAG", "PKT_CONSOL_ATTR", "SIZE_RANGE_CODE", "SIZE_REL_POSN_IN_TABLE", "VENDOR_ITEM_NBR", "UNIT_PRICE", "RETAIL_PRICE", "PRICE_TKT_TYPE", "AVG_DLY_DMND", "MFG_DATE_REQD", "SHIP_BY_DATE_REQD", "XPIRE_DATE_REQD", "MAX_RECV_TO_XPIRE_DAYS", "ALLOW_RCPT_OLDER_SKU", "BUYER_DISP_CODE", "PROMPT_FOR_VENDOR_ITEM_NBR", "MAX_RCPT_QTY", "SRL_NBR_BRCD_TYPE", "DUP_SRL_NBR_FLAG", "MINOR_SRL_NBR_REQ", "INCUB_DAYS", "INCUB_HOURS", "DFLT_INCUB_LOCK", "BASE_INCUB_FLAG", "CONS_PRTY_DATE_WINDOW_INCR", "CONS_PRTY_DATE_CODE", "CONS_PRTY_DATE_WINDOW", "CRITCL_DIM_1", "CRITCL_DIM_2", "CRITCL_DIM_3", "NEST_VOL", "NEST_CNT", "CUBE_MULT_QTY", "PKG_TYPE", "CARTON_TYPE", "UNITS_PER_PICK_ACTIVE", "HNDL_ATTR_ACTIVE", "HNDL_ATTR_RESV", "PICK_WT_TOL_TYPE", "PICK_WT_TOL_AMNT", "MHE_WT_TOL_TYPE", "MHE_WT_TOL_AMNT", "WT_TOL_PCNT", "ECCN_NBR", "EXP_LICN_NBR", "EXP_LICN_XP_DATE", "EXP_LICN_SYMBOL", "ORGN_CERT_CODE", "ITAR_EXEMPT_NBR", "COMMODITY_LEVEL_DESC", "NMFC_CODE", "FRT_CLASS", "FTZ_FLAG", "HTS_NBR", "LOAD_ATTR", "TEMP_ZONE", "TRLR_TEMP_ZONE", "DSP_QTY_UOM", "DB_QTY_UOM", "MAX_CASE_QTY", "SKU_ATTR_REQD", "SPL_INSTR_CODE_1", "SPL_INSTR_CODE_2", "SPL_INSTR_CODE_3", "SPL_INSTR_CODE_4", "SPL_INSTR_CODE_5", "SPL_INSTR_CODE_6", "SPL_INSTR_CODE_7", "SPL_INSTR_CODE_8", "SPL_INSTR_CODE_9", "SPL_INSTR_CODE_10", "SPL_INSTR_1", "SPL_INSTR_2", "PROMPT_PACK_QTY", "STAT_CODE", "UNIT_HT", "UNIT_LEN", "UNIT_WIDTH", "UNIT_WT", "UNIT_VOL") AS
select item_cbo.item_id,
      item_wms.batch_reqd,
      item_wms.prod_stat_reqd,
      item_wms.cntry_of_orgn_reqd,
      item_cbo.catch_weight_item,
      item_wms.prod_catgry,
      item_wms.prod_sub_grp,
      item_cbo.prod_type,
      item_wms.prod_line,
      item_wms.sale_grp,
      item_wms.oper_code,
      item_wms.store_dept,
      item_wms.actvtn_date,
      item_wms.coord_1,
      item_wms.coord_2,
      item_wms.prod_life_in_day,
      item_wms.volty_code,
      item_wms.merch_type,
      item_wms.merch_group,
      item_wms.crush_code,
      item_wms.convey_flag,
      --ITEM_CBO.HAZMAT_CODE,
      item_wms.pkt_consol_attr,
      item_wms.size_range_code,
      item_wms.size_rel_posn_in_table,
      item_wms.vendor_item_nbr,
      item_wms.unit_price,
      item_wms.retail_price,
      item_wms.price_tkt_type,
      item_wms.avg_dly_dmnd,
      item_wms.mfg_date_reqd,
      item_wms.ship_by_date_reqd,
      item_wms.xpire_date_reqd,
      item_wms.max_recv_to_xpire_days,
      item_wms.allow_rcpt_older_item,
      item_wms.buyer_disp_code,
      item_wms.prompt_for_vendor_item_nbr,
      item_wms.max_rcpt_qty,
      item_wms.srl_nbr_brcd_type,
      item_wms.dup_srl_nbr_flag,
      item_wms.minor_srl_nbr_req,
      item_wms.incub_days,
      item_wms.incub_hours,
      item_wms.dflt_incub_lock,
      item_wms.base_incub_flag,
      item_wms.cons_prty_date_window_incr,
      item_wms.cons_prty_date_code,
      item_wms.cons_prty_date_window,
      item_wms.critcl_dim_1,
      item_wms.critcl_dim_2,
      item_wms.critcl_dim_3,
      item_wms.nest_vol,
      item_wms.nest_cnt,
      item_wms.cube_mult_qty,
      item_wms.pkg_type,
      item_wms.carton_type,
      item_wms.units_per_pick_active,
      item_wms.hndl_attr_active,
      item_wms.hndl_attr_resv,
      item_wms.pick_wt_tol_type,
      item_wms.pick_wt_tol_amnt,
      item_wms.mhe_wt_tol_type,
      item_wms.mhe_wt_tol_amnt,
      item_wms.wt_tol_pcnt,
      item_wms.eccn_nbr,
      item_wms.exp_licn_nbr,
      item_wms.exp_licn_xp_date,
      item_wms.exp_licn_symbol,
      item_wms.orgn_cert_code,
      item_wms.itar_exempt_nbr,
      item_cbo.commodity_level_desc,
      item_wms.nmfc_code,
      item_wms.frt_class,
      cast (null as varchar (1)) as ftz_flag,
      cast (null as varchar (12)) as hts_nbr,
      item_wms.load_attr,
      item_wms.temp_zone,
      item_wms.trlr_temp_zone,
      item_cbo.display_qty_uom_id,
      item_cbo.database_qty_uom_id,
      item_wms.max_case_qty,
      item_wms.item_attr_reqd,
      item_wms.spl_instr_code_1,
      item_wms.spl_instr_code_2,
      item_wms.spl_instr_code_3,
      item_wms.spl_instr_code_4,
      item_wms.spl_instr_code_5,
      item_wms.spl_instr_code_6,
      item_wms.spl_instr_code_7,
      item_wms.spl_instr_code_8,
      item_wms.spl_instr_code_9,
      item_wms.spl_instr_code_10,
      item_wms.spl_instr_1,
      item_wms.spl_instr_2,
      item_wms.prompt_pack_qty,
      item_cbo.status_code,
      item_cbo.unit_height,
      item_cbo.unit_length,
      item_cbo.unit_width,
      item_cbo.unit_weight,
      item_cbo.unit_volume
 from    item_cbo item_cbo
      left outer join
         item_wms item_wms
      on item_cbo.item_id = item_wms.item_id;

CREATE OR REPLACE FORCE VIEW "SKU_BUDGETED_COST" ("SKU_BC_ID", "SKU_ID", "BUDGETED_COST", "BC_CURRENCY_CODE", "BC_UOM", "BC_ORIGIN_STATE_PROV", "BC_ORIGIN_COUNTRY_CODE", "BC_DESTINATION_STATE_PROV", "BC_DESTINATION_COUNTRY_CODE", "COMMENTS", "BC_UOM_ID") AS
select item_budgeted_cost_tms.item_budgeted_cost_id,
      item_budgeted_cost_tms.item_id,
      item_budgeted_cost_tms.cost,
      item_budgeted_cost_tms.currency_code,
      item_budgeted_cost_tms.uom,
      item_budgeted_cost_tms.origin_state_prov,
      item_budgeted_cost_tms.origin_country_code,
      item_budgeted_cost_tms.destination_state_prov,
      item_budgeted_cost_tms.destination_country_code,
      item_budgeted_cost_tms.comments,
      item_budgeted_cost_tms.uom_id
 from item_budgeted_cost_tms item_budgeted_cost_tms;

CREATE OR REPLACE FORCE VIEW "SKU_MONETARY_VALUE" ("SKU_ID", "MONETARY_VALUE_ID", "MONETARY_VALUE", "CURRENCY_CODE", "TC_COMPANY_ID", "SIZE_UOM", "EFFECTIVE_DT", "EXPIRATION_DT", "SIZE_UOM_ID") AS
select item_cost_cbo.item_id,
      item_cost_cbo.cost_type_id,
      item_cost_cbo.unit_cost,
      item_cost_cbo.currency_code,
      item_cbo.company_id,
      size_uom.size_uom,
      item_cost_cbo.effective_dt,
      item_cost_cbo.expiration_dt,
      item_cost_cbo.uom_id
 from item_cost_cbo item_cost_cbo
      join item_cbo item_cbo
         on (item_cost_cbo.item_id = item_cbo.item_id)
      left outer join size_uom size_uom
         on (item_cost_cbo.uom_id = size_uom.size_uom_id);

CREATE OR REPLACE FORCE VIEW "SKU_SIZE" ("SKU_ID", "SIZE_UOM", "TC_COMPANY_ID", "SIZE_VALUE", "SIZE_UOM_ID") AS
select item_size_tms.item_id,
      size_uom.size_uom,
      item_cbo.company_id,
      item_size_tms.uom_value,
      item_size_tms.uom_id
 from item_size_tms item_size_tms
      join item_cbo item_cbo
         on (item_size_tms.item_id = item_cbo.item_id)
      left outer join size_uom size_uom
         on (item_size_tms.uom_id = size_uom.size_uom_id);

CREATE OR REPLACE FORCE VIEW "SKU_SIZE_UOM_PACKAGE_TYPE_MAP" ("SKU_ID", "TC_COMPANY_ID", "SIZE_UOM", "PACKAGE_TYPE_ID", "SIZE_UOM_VALUE", "SIZE_UOM_ID") AS
select item_package_cbo.item_id,
      item_cbo.company_id,
      size_uom.size_uom,
      cast (null as number (8)) as package_type_id,
      item_package_cbo.quantity,
      item_package_cbo.package_uom_id
 from item_package_cbo item_package_cbo
      join item_cbo item_cbo
         on (item_package_cbo.item_id = item_cbo.item_id)
      left outer join size_uom size_uom
         on (item_package_cbo.package_uom_id = size_uom.size_uom_id);

CREATE OR REPLACE FORCE VIEW "STATE_CODE" ("STATE", "CNTRY", "STATE_NAME", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select state_prov,
      country_code,
      state_prov_name,
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1))
 from state_prov;

CREATE OR REPLACE FORCE VIEW "STATIC_ORDER_PLANNING_SET_VW" ("ORDER_ID", "SHIPMENT_ID", "LINE_HAUL_SHIP_VIA", "DISTRIBUTION_SHIP_VIA", "SHIPMENT_CLOSED_INDICATOR", "LPN_ASSIGNMENT_STOPPED", "ORDER_SPLIT_ID", "LINE_ITEM_ID", "ORDER_SPLIT_LINE_ITEM_ID", "TOTAL_SPLIT_QTY") AS
select o.order_id, os.shipment_id, os.line_haul_ship_via,
os.distribution_ship_via, s.shipment_closed_indicator,
s.lpn_assignment_stopped, os.order_split_id, osli.line_item_id,
osli.order_split_line_item_id, osli.order_qty
from orders o
join order_split os on os.order_id = o.order_id and os.is_cancelled = 0
join order_split_line_item osli on os.order_split_id = osli.order_split_id
and osli.order_qty > 0
left join shipment s on s.shipment_id = os.shipment_id
and s.shipment_closed_indicator != 1 and s.shipment_status < 80;

CREATE OR REPLACE FORCE VIEW "STOP_OFF_CHARGE_VIEW" ("ACCESSORIAL_PARAM_SET_ID", "STOP_OFF_CHARGE_ID", "INIT_STOP_OFF", "FINAL_STOP_OFF", "STOP_CHARGE", "CURRENCY_CODE", "HAS_ERRORS") AS
select aps.accessorial_param_set_id,
      soc.stop_off_charge_id,
      soc.start_number,
      soc.end_number,
      soc.value,
      aps.stop_off_currency_code,
      0
 from accessorial_param_set aps, stop_off_charge soc
where aps.accessorial_param_set_id = soc.accessorial_param_set_id
 union all
 select iaps.accessorial_param_set_id,
      isoc.stop_off_charge_id,
      isoc.start_number,
      isoc.end_number,
      isoc.value,
      iaps.stop_off_currency_code,
      case isoc.status when 4 then 1 else 0 end as has_errors
 from import_stop_off_charge isoc, import_accessorial_param_set iaps
where isoc.param_id = iaps.param_id and iaps.status = 1
 union all
 select iaps.param_id,
      isoc.stop_off_charge_id,
      isoc.start_number,
      isoc.end_number,
      isoc.value,
      iaps.stop_off_currency_code,
      case isoc.status when 4 then 1 else 0 end as has_errors
 from import_stop_off_charge isoc, import_accessorial_param_set iaps
where isoc.param_id = iaps.param_id and iaps.status = 4;

CREATE OR REPLACE FORCE VIEW "STORE_MASTER" ("STORE_NBR", "NAME", "ADDRESS_1", "ADDRESS_2", "ADDRESS_3", "CITY", "STATE_PROV", "POSTAL_CODE", "COUNTY", "COUNTRY_CODE", "STAT_CODE", "OPEN_DATE", "CLOSE_DATE", "HOLD_DATE", "GRP", "CHAIN", "ZONE", "TERRITORY", "REGION", "DISTRICT", "SHIP_MON", "SHIP_TUE", "SHIP_WED", "SHIP_THU", "SHIP_FRI", "SHIP_SAT", "SHIP_SU", "ACCEPT_IRREG", "WAVE_LABEL_TYPE", "PKG_SLIP_TYPE", "PRINT_CODE", "CARTON_CNT_TYPE", "STORE_TYPE", "SHIP_VIA", "RTE_NBR", "RTE_ATTR", "RTE_TO", "RTE_TYPE_1", "RTE_TYPE_2", "RTE_ZIP", "SPL_INSTR_CODE_1", "SPL_INSTR_CODE_2", "SPL_INSTR_CODE_3", "SPL_INSTR_CODE_4", "SPL_INSTR_CODE_5", "SPL_INSTR_CODE_6", "SPL_INSTR_CODE_7", "SPL_INSTR_CODE_8", "SPL_INSTR_CODE_9", "SPL_INSTR_CODE_10", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "ASSIGN_MERCH_TYPE", "ASSIGN_MERCH_GROUP", "ASSIGN_STORE_DEPT", "CARTON_LABEL_TYPE", "CARTON_CUBNG_INDIC", "MAX_CTN", "MAX_PLT", "BUSN_UNIT_CODE", "USE_INBD_LPN_AS_OUTBD_LPN", "DFLT_CD_MASTER_ID", "STORE_MASTER_ID", "WM_VERSION_ID", "RLS_HOLD_DATE") AS
(select fa.facility_alias_id as store_nbr,
       fa.facility_name as name,
       facility.address_1,
       facility.address_2,
       facility.address_3,
       facility.city,
       facility.state_prov,
       facility.postal_code,
       facility.county,
       facility.country_code,
       facility.stat_code,
       open_date,
       close_date,
       hold_date,
       grp,
       chain,
       zone,
       territory,
       region,
       district,
       ship_mon,
       ship_tue,
       ship_wed,
       ship_thu,
       ship_fri,
       ship_sat,
       ship_su,
       accept_irreg,
       wave_label_type,
       pkg_slip_type,
       print_code,
       carton_cnt_type,
       store_type,
       ship_via,
       rte_nbr,
       rte_attr,
       rte_to,
       rte_type_1,
       rte_type_2,
       rte_zip,
       spl_instr_code_1,
       spl_instr_code_2,
       spl_instr_code_3,
       spl_instr_code_4,
       spl_instr_code_5,
       spl_instr_code_6,
       spl_instr_code_7,
       spl_instr_code_8,
       spl_instr_code_9,
       spl_instr_code_10,
       facility.created_dttm as create_date_time,
       facility.last_updated_dttm as mod_date_time,
       facility.last_updated_source as user_id,
       assign_merch_type,
       assign_merch_group,
       assign_store_dept,
       carton_label_type,
       carton_cubng_indic,
       max_ctn,
       max_plt,
       busn_unit_code,
       use_inbd_lpn_as_outbd_lpn,
       facility.tc_company_id as dflt_cd_master_id,
       facility.facility_id as store_master_id,
       facility.hibernate_version as wm_version_id,
       rls_hold_date
  from    facility
       inner join
          facility_alias fa
       on     facility.facility_id = fa.facility_id
          and fa.is_primary = 1
          and facility.facility_type_bits in (64, 65));

CREATE OR REPLACE FORCE VIEW "TBLLABORPLANEXPORTCONTROL" ("PLANNUMBER", "INTERFACECONNECTIONID", "FILEID", "FILEKEY", "ERRORSEQUENCENBR") AS
select lpec.plan_nbr plannumber,
      lpec.interface_conn_id interfaceconnectionid,
      lpec.file_id fileid,
      lpec.file_key filekey,
      lpec.error_seq_nbr errorsequencenbr
 from labor_plan_export_control lpec;

CREATE OR REPLACE FORCE VIEW "TBLLABORPLANEXPORTHEADER" ("PLANNUMBER", "INTERFACECONNECTIONID", "LABORPLANID", "STATUSCODE", "PLANDESCRIPTION", "WAREHOUSE", "TOTALASN", "TOTALPICKTICKETS", "TOTALDISTROS", "DATECREATED", "USERID") AS
select lpeh.plan_nbr plannumber,
      lpeh.interface_conn_id interfaceconnectionid,
      lpeh.labor_plan_id laborplanid,
      lpeh.status_code statuscode,
      lpeh.plan_description plandescription,
      lpeh.whse warehouse,
      lpeh.total_asn totalasn,
      lpeh.total_picktickets totalpicktickets,
      lpeh.total_distros totaldistros,
      lpeh.date_created datecreated,
      lpeh.user_id userid
 from labor_plan_export_hdr lpeh;

CREATE OR REPLACE FORCE VIEW "TBLLABORPLANEXPORTSUMMARY" ("PLANNUMBER", "INTERFACECONNECTIONID", "FUNCTIONALAREAID", "TOTALHOURS", "TOTALWORKERS") AS
select lpes.plan_nbr plannumber,
      lpes.interface_conn_id interfaceconnectionid,
      lpes.functional_area functionalareaid,
      lpes.total_hours totalhours,
      lpes.total_workers totalworkers
 from labor_plan_export_summary lpes;

CREATE OR REPLACE FORCE VIEW "USER_MASTER" ("LOGIN_USER_ID", "MENU_ID", "USER_NAME", "EMPLYE_ID", "PSWD", "RESTR_TASK_GRP_TO_DFLT", "RESTR_MENU_MODE_TO_DFLT", "DFLT_RF_MENU_MODE", "LANG_ID", "DATE_MASK", "LAST_TASK", "LAST_LOCN", "LAST_WORK_GRP", "LAST_WORK_AREA", "DFLT_TASK_INT", "ALLOW_TASK_INT_CHG", "NBR_OF_TASK_TO_DSP", "TASK_DSP_MODE", "DB_USER_ID", "DB_PSWD", "DB_CONNECT_STRING", "PRTR_REQSTR", "IDLE_TIME_BEF_SHTDWN", "NBR_DAYS_TO_EXPIRE_PSWD", "NBR_GRACE_LOGINS", "NBR_GRACE_LOGINS_USED", "DATE_PSWD_LAST_CHGD", "RF_MENU_ID", "PAGE_SIZE", "VOCOLLECT_WORK_TYPE", "CURR_TASK_GRP", "CURR_VOCOLLECT_PTS_CASE", "CURR_VOCOLLECT_REASON_CODE", "TASK_GRP_JUMP_FLAG", "AUTO_3PL_LOGIN_FLAG", "SECURITY_CONTEXT_ID", "SEC_USER_NAME", "CLS_TIMEZONE_ID", "VOCOLLECT_PUTAWAY_FLAG", "VOCOLLECT_REPLEN_FLAG", "VOCOLLECT_PACKING_FLAG", "DAL_CONNECTION_STRING", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select cast (user_profile.login_user_id as varchar2 (15)) as login_user_id,
      cast (user_profile.menu_id as number (9)) as menu_id,
      cast (user_profile.login_user_id as varchar2 (20)) as user_name,
      cast (user_profile.emplye_id as varchar2 (10)) as emplye_id,
      cast ('' as varchar2 (40)) as pswd,
      cast (user_profile.restr_task_grp_to_dflt as varchar2 (1))
         as restr_task_grp_to_dflt,
      cast (user_profile.restr_menu_mode_to_dflt as varchar2 (1))
         as restr_menu_mode_to_dflt,
      cast (user_profile.dflt_rf_menu_mode as varchar2 (1))
         as dflt_rf_menu_mode,
      cast (user_profile.lang_id as varchar2 (3)) as lang_id,
      cast (user_profile.date_mask as varchar2 (11)) as date_mask,
      cast (user_profile.last_task as varchar2 (10)) as last_task,
      cast (user_profile.last_locn as varchar2 (10)) as last_locn,
      cast (user_profile.last_work_grp as varchar2 (4)) as last_work_grp,
      cast (user_profile.last_work_area as varchar2 (4))
         as last_work_area,
      cast (user_profile.dflt_task_int as number (4)) as dflt_task_int,
      cast (user_profile.allow_task_int_chg as varchar2 (1))
         as allow_task_int_chg,
      cast (user_profile.nbr_of_task_to_dsp as number (4))
         as nbr_of_task_to_dsp,
      cast (user_profile.task_dsp_mode as varchar2 (1)) as task_dsp_mode,
      cast (user_profile.db_user_id as varchar2 (30)) as db_user_id,
      cast (user_profile.db_pswd as varchar2 (90)) as db_pswd,
      cast (user_profile.db_connect_string as varchar2 (30))
         as db_connect_string,
      cast (user_profile.prtr_reqstr as varchar2 (20)) as prtr_reqstr,
      cast (user_profile.idle_time_bef_shtdwn as number (9))
         as idle_time_bef_shtdwn,
      cast (0 as number (4)) as nbr_days_to_expire_pswd,
      cast (0 as number (4)) as nbr_grace_logins,
      cast (0 as number (4)) as nbr_grace_logins_used,
      cast (null as date) as date_pswd_last_chgd,
      cast (user_profile.rf_menu_id as number (9)) as rf_menu_id,
      cast (user_profile.page_size as number (9)) as page_size,
      cast (user_profile.vocollect_work_type as number (4))
         as vocollect_work_type,
      cast (user_profile.curr_task_grp as varchar2 (3)) as curr_task_grp,
      cast (user_profile.curr_vocollect_pts_case as varchar2 (20))
         as curr_vocollect_pts_case,
      cast (user_profile.curr_vocollect_reason_code as varchar2 (2))
         as curr_vocollect_reason_code,
      cast (user_profile.task_grp_jump_flag as varchar2 (1))
         as task_grp_jump_flag,
      cast (user_profile.auto_3pl_login_flag as varchar2 (1))
         as auto_3pl_login_flag,
      cast (user_profile.security_context_id as number (9))
         as security_context_id,
      cast (user_profile.sec_user_name as varchar2 (128))
         as sec_user_name,
      cast (user_profile.cls_timezone_id as number (9))
         as cls_timezone_id,
      cast (user_profile.vocollect_putaway_flag as number (4))
         as vocollect_putaway_flag,
      cast (user_profile.vocollect_replen_flag as number (4))
         as vocollect_replen_flag,
      cast (user_profile.vocollect_packing_flag as number (4))
         as vocollect_packing_flag,
      cast (user_profile.dal_connection_string as varchar2 (255))
         as dal_connection_string,
      cast (user_profile.create_date_time as date) as create_date_time,
      cast (user_profile.mod_date_time as date) as mod_date_time,
      cast (user_profile.user_id as varchar2 (15)) as user_id
 from user_profile;

CREATE OR REPLACE FORCE VIEW "VAL_JOB_DATA_VW" ("JOB_NAME", "VAL_JOB_DTL_ID", "SQL_ID", "BIND_VAL_LIST_CSV", "RESULT_LIST_CSV") AS
select vjh.job_name, vjd.val_job_dtl_id, vjd.sql_id,
  wm_get_bind_values_for_job_dtl(vjd.val_job_dtl_id),
  wm_get_exp_results_for_job_dtl(vjd.val_job_dtl_id)
from val_job_hdr vjh
join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id;

CREATE OR REPLACE FORCE VIEW "VAL_JOB_RESULT_SUMM_VW" ("FLOW_NAME", "PGM_ID", "INTRNL_JOB_NAME", "CURR_NUM_SQLS", "CURR_DISABLED", "VAL_TXN_ID", "JOB_TXN_ID", "PASSED", "FAILED", "FIRST_SQL_END_TIME", "LAST_SQL_END_TIME") AS
with curr_job_data as
(
  select vjh.job_name, vjh.val_job_hdr_id, count(*) curr_num_sqls,
      sum(case when vjd.is_locked = 1 or vs.is_locked = 1 then 1
      else 0 end) curr_disabled
  from val_job_hdr vjh
  join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
  join val_sql vs on vs.sql_id = vjd.sql_id
  group by vjh.job_name, vjh.val_job_hdr_id
)
select coalesce(vrh.app_hook, vjs.app_hook) flow_name, coalesce(vrh.pgm_id, vjs.pgm_id) pgm_id,
  cjd.job_name intrnl_job_name, min(cjd.curr_num_sqls) curr_num_sqls,
  min(cjd.curr_disabled) curr_disabled, vrh.parent_txn_id val_txn_id, vrh.txn_id job_txn_id,
  sum(vrh.last_run_passed) passed,
  sum(decode(vrh.last_run_passed, 0, 1, decode(vrh.parent_txn_id, null, null, 0))) failed,
  min(vrh.create_date_time) first_sql_end_time, max(vrh.create_date_time) last_sql_end_time
from val_job_seq vjs
join curr_job_data cjd on cjd.job_name = vjs.job_name
join val_job_dtl vjd on vjd.val_job_hdr_id = cjd.val_job_hdr_id
left join val_result_hist vrh on vrh.val_job_dtl_id = vjd.val_job_dtl_id
  and vrh.app_hook = vjs.app_hook and vrh.pgm_id = vjs.pgm_id
group by coalesce(vrh.app_hook, vjs.app_hook), coalesce(vrh.pgm_id, vjs.pgm_id), cjd.job_name,
  vrh.txn_id, vrh.parent_txn_id;

CREATE OR REPLACE FORCE VIEW "VAL_JOB_RESULT_VW" ("VAL_TXN_ID", "JOB_TXN_ID", "CREATE_DATE_TIME", "JOB_NAME", "PASSED", "MSG", "SQL_IDENT", "SQL_TEXT", "PGM_ID", "BIND_SUBS_SQL_TEXT", "FLOW_NAME") AS
select vrh.parent_txn_id val_txn_id, vrh.txn_id job_txn_id, vrh.create_date_time, vjh.job_name,
  vrh.last_run_passed passed, vrh.msg, vs.ident sql_ident, vs.sql_text, vrh.pgm_id,
  wm_val_get_bind_subs_sql(vjd.val_job_dtl_id, vrh.txn_id, vs.sql_text) bind_subs_sql_text,
  vrh.app_hook flow_name
from val_result_hist vrh
join val_job_dtl vjd on vjd.val_job_dtl_id = vrh.val_job_dtl_id
join val_job_hdr vjh on vjh.val_job_hdr_id = vjd.val_job_hdr_id
join val_sql vs on vs.sql_id = vjd.sql_id;

CREATE OR REPLACE FORCE VIEW "VAL_JOB_SQL_VW" ("IDENT", "SQL_ID", "JOB_NAME", "DISABLED") AS
select vs.ident, vs.sql_id, vjh.job_name,
  case when vjd.is_locked = 1 or vs.is_locked = 1 then 1 else 0 end disabled
from val_sql vs
join val_job_dtl vjd on vjd.sql_id = vs.sql_id
join val_job_hdr vjh on vjh.val_job_hdr_id = vjd.val_job_hdr_id;

CREATE OR REPLACE FORCE VIEW "VAL_SQL_DATA_VW" ("IDENT", "SQL_ID", "SQL_TEXT", "RAW_TEXT", "BIND_VAR_LIST_CSV", "COL_LIST_CSV") AS
select vs.ident, vs.sql_id, vs.sql_text, vs.raw_text, vs.bind_var_list bind_var_list_csv,
  wm_val_get_col_list_for_sql(vs.sql_id) col_list_csv
from val_sql vs;

CREATE OR REPLACE FORCE VIEW "VAL_SQL_TAG_SUMM_VW" ("SQL_ID", "SQL_IDENT", "TYPE_LIST", "TAG_LIST", "TYPE_TAG_LIST") AS
select vs.sql_id, vs.ident sql_ident,
  listagg(decode(vst.type, '1', 'select', '2', 'from', '3', 'via', '4', 'module', '5', 'vertical', '6', 'desc', vst.type), ' || ')
      within group(order by vst.type) type_list,
  listagg(vst.tag, ' || ') within group(order by vst.type) tag_list,
  listagg(decode(vst.type, '1', 'select', '2', 'from', '3', 'via', '4', 'module', '5', 'vertical', '6', 'desc', vst.type) || ' '
      || vst.tag, ' || ') within group(order by vst.type) type_tag_list
from val_sql_tags vst
join val_sql vs on vs.sql_id = vst.sql_id
group by vs.sql_id, vs.ident;

CREATE OR REPLACE FORCE VIEW "VHCL_PARAMETERS" ("ROW_NBR", "VHCL_TYPE_ID", "VHCL_TYPE", "DESCRIPTION", "WHSE", "WHSE_VHCL_TYPE_ID", "MAX_FWD_VELC", "FWD_ACEL_TIME", "FWD_ACEL_DIST", "MAX_BWD_VELC", "BWD_ACEL_TIME", "BWD_ACEL_DIST", "DCEL_TIME", "DCEL_DIST", "ELV_SPD", "MAX_WALK_DIST", "WALK_VELC", "MAX_WR_DIST", "WR_VELC", "LVL_RETN_FLAG", "UNLD_MAX_FWD_VELC", "UNLD_FWD_ACEL_TIME", "UNLD_FWD_ACEL_DIST", "UNLD_FWD_DCEL_TIME", "UNLD_FWD_DCEL_DIST", "LD_BWD_DCEL_TIME", "LD_BWD_DCEL_DIST", "UNLD_MAX_BWD_VELC", "UNLD_BWD_ACEL_TIME", "UNLD_BWD_ACEL_DIST", "UNLD_BWD_DCEL_TIME", "UNLD_BWD_DCEL_DIST", "UNLD_WR_VELC", "UNLD_MAX_WR_DIST", "UNLD_ELV_SPD", "UNLD_LWRG_SPD", "LWRG_SPD") AS
SELECT ROWNUM AS row_nbr,
   res.VHCL_TYPE_ID,
   res.VHCL_TYPE,
   res.DESCRIPTION,
   res.WHSE,
   res.WHSE_VHCL_TYPE_ID,
   res.MAX_FWD_VELC,
   res.FWD_ACEL_TIME,
   res.FWD_ACEL_DIST,
   res.MAX_BWD_VELC,
   res.BWD_ACEL_TIME,
   res.BWD_ACEL_DIST,
   res.DCEL_TIME,
   res.DCEL_DIST,
   res.ELV_SPD,
   res.MAX_WALK_DIST,
   res.WALK_VELC,
   res.MAX_WR_DIST,
   res.WR_VELC,
   res.LVL_RETN_FLAG,
   res.UNLD_MAX_FWD_VELC,
   res.UNLD_FWD_ACEL_TIME,
   res.UNLD_FWD_ACEL_DIST,
   res.UNLD_FWD_DCEL_TIME,
   res.UNLD_FWD_DCEL_DIST,
   res.LD_BWD_DCEL_TIME,
   res.LD_BWD_DCEL_DIST,
   res.UNLD_MAX_BWD_VELC,
   res.UNLD_BWD_ACEL_TIME,
   res.UNLD_BWD_ACEL_DIST,
   res.UNLD_BWD_DCEL_TIME,
   res.UNLD_BWD_DCEL_DIST,
   res.UNLD_WR_VELC,
   res.UNLD_MAX_WR_DIST,
   res.UNLD_ELV_SPD,
   res.UNLD_LWRG_SPD,
   res.LWRG_SPD
FROM ((SELECT v1.VHCL_TYPE_ID AS VHCL_TYPE_ID,
            v1.VHCL_TYPE AS VHCL_TYPE,
            v1.DESCRIPTION AS DESCRIPTION,
            '*' AS WHSE,
            NULL AS WHSE_VHCL_TYPE_ID,
            v1.MAX_FWD_VELC,
            v1.FWD_ACEL_TIME,
            v1.FWD_ACEL_DIST,
            v1.MAX_BWD_VELC,
            v1.BWD_ACEL_TIME,
            v1.BWD_ACEL_DIST,
            v1.DCEL_TIME,
            v1.DCEL_DIST,
            v1.ELV_SPD,
            v1.MAX_WALK_DIST,
            v1.WALK_VELC,
            v1.MAX_WR_DIST,
            v1.WR_VELC,
            v1.LVL_RETN_FLAG,
            v1.UNLD_MAX_FWD_VELC,
            v1.UNLD_FWD_ACEL_TIME,
            v1.UNLD_FWD_ACEL_DIST,
            v1.UNLD_FWD_DCEL_TIME,
            v1.UNLD_FWD_DCEL_DIST,
            v1.LD_BWD_DCEL_TIME,
            v1.LD_BWD_DCEL_DIST,
            v1.UNLD_MAX_BWD_VELC,
            v1.UNLD_BWD_ACEL_TIME,
            v1.UNLD_BWD_ACEL_DIST,
            v1.UNLD_BWD_DCEL_TIME,
            v1.UNLD_BWD_DCEL_DIST,
            v1.UNLD_WR_VELC,
            v1.UNLD_MAX_WR_DIST,
            v1.UNLD_ELV_SPD,
            v1.UNLD_LWRG_SPD,
            v1.LWRG_SPD
       FROM E_VHCL v1) UNION ALL
    (SELECT vw.VHCL_TYPE_ID AS VHCL_TYPE_ID,
            (SELECT v.vhcl_type
               FROM e_vhcl v
              WHERE v.vhcl_type_id = vw.vhcl_type_id) AS VHCL_TYPE,
            vw.DESCRIPTION AS DESCRIPTION,
            vw.WHSE AS WHSE,
            vw.WHSE_VHCL_TYPE_ID AS WHSE_VHCL_TYPE_ID,
            vw.MAX_FWD_VELC,
            vw.FWD_ACEL_TIME,
            vw.FWD_ACEL_DIST,
            vw.MAX_BWD_VELC,
            vw.BWD_ACEL_TIME,
            vw.BWD_ACEL_DIST,
            vw.DCEL_TIME,
            vw.DCEL_DIST,
            vw.ELV_SPD,
            vw.MAX_WALK_DIST,
            vw.WALK_VELC,
            vw.MAX_WR_DIST,
            vw.WR_VELC,
            vw.LVL_RETN_FLAG,
            vw.UNLD_MAX_FWD_VELC,
            vw.UNLD_FWD_ACEL_TIME,
            vw.UNLD_FWD_ACEL_DIST,
            vw.UNLD_FWD_DCEL_TIME,
            vw.UNLD_FWD_DCEL_DIST,
            vw.LD_BWD_DCEL_TIME,
            vw.LD_BWD_DCEL_DIST,
            vw.UNLD_MAX_BWD_VELC,
            vw.UNLD_BWD_ACEL_TIME,
            vw.UNLD_BWD_ACEL_DIST,
            vw.UNLD_BWD_DCEL_TIME,
            vw.UNLD_BWD_DCEL_DIST,
            vw.UNLD_WR_VELC,
            vw.UNLD_MAX_WR_DIST,
            vw.UNLD_ELV_SPD,
            vw.UNLD_LWRG_SPD,
            vw.LWRG_SPD
       FROM E_VHCL_WHSE vw)) res;

CREATE OR REPLACE FORCE VIEW "VIEW_ACC_UCL" ("REGION_ID", "EMP_ID", "LOGIN_USER_ID", "LAST_NAME", "FIRST_NAME") AS
select acc.geo_region_id as region_id,
      ucl.ucl_user_id as emp_id,
      ucl.user_name as login_user_id,
      ucl.user_last_name as last_name,
      ucl.user_first_name as first_name
 from    access_control acc
      join
         ucl_user ucl
      on acc.ucl_user_id = ucl.ucl_user_id;

CREATE OR REPLACE FORCE VIEW "VIEW_ADDASN" ("PLAN_ID", "WHSE", "SHIPMENT_NBR", "PO_NBR", "TO_LOCATION", "RECEIVED_FROM", "TRANSACTION_SEQ", "WORKFLOW_NAME", "EXISTING_SEQUENCE_STATUS", "PLAN_TRANSACTION_ID") AS
select decode ( (nvl (epd.plan_id, 'abc')), 'abc', '---', epd.plan_id)
         as plan_id,
      eah.to_location as whse,
      eah.shipment_nbr,
      decode ( (nvl (epd.plan_id, '0')), '0', null, eah.po_nbr) as po_nbr,
      decode ( (nvl (epd.plan_id, '0')), '0', null, eah.to_location)
         as to_location,
      decode ( (nvl (epd.plan_id, '0')), '0', null, eah.received_from)
         as received_from,
      decode ( (nvl (epd.plan_id, '0')), '0', null, epd.transaction_seq)
         as transaction_seq,
      decode ( (nvl (epd.plan_id, '0')), '0', null, epwh.workflow_name)
         as workflow_name,
      decode (
         (nvl (epd.plan_id, '0')),
         '0', 'INSERT',
         decode ( (nvl (epth.plan_transaction_id, 0)),
                 0, 'INSERT',
                 'UPDATE'))
         as existing_sequence_status,
      decode ( (nvl (epd.plan_id, '0')),
              '0', null,
              epth.plan_transaction_id)
         as plan_transaction_id
 from e_asn_hdr eah,
      e_plan_transaction_hdr epth,
      e_plan_dtl epd,
      e_plan_workflow_hdr epwh
where (epth.transaction_value = eah.shipment_nbr(+))
      and (epd.plan_id = epth.plan_id(+)
           and epd.transaction_seq = epth.transaction_seq(+))
      and (epd.plan_workflow_id = epwh.plan_workflow_id)
 union
 select '---',
      to_location,
      shipment_nbr,
      null,
      null,
      null,
      null,
      null,
      'INSERT',
      null
 from e_asn_hdr;

CREATE OR REPLACE FORCE VIEW "VIEW_ADD_PICKTICKET" ("PLAN_ID", "WHSE", "PICKTICKET_CONTROL_NUMBER", "TRANSACTION_SEQ", "PLAN_WORKFLOW_ID", "SHIPTO", "SOLDTO", "STORE_NBR", "EXISTING_SEQUENCE_STATUS") AS
select decode ( (nvl (epd.plan_id, 'abc')), 'abc', '---', epd.plan_id)
         as plan_id,
      eph.warehouse as whse,
      eph.pickticket_control_number,
      decode ( (nvl (epd.plan_id, '0')), '0', null, epd.transaction_seq)
         as transaction_seq,
      decode ( (nvl (epd.plan_id, '0')), '0', null, epd.plan_workflow_id)
         as plan_workflow_id,
      decode ( (nvl (epd.plan_id, '0')), '0', null, eph.shipto) as shipto,
      decode ( (nvl (epd.plan_id, '0')), '0', null, eph.soldto) as soldto,
      decode ( (nvl (epd.plan_id, '0')), '0', null, eph.store_nbr)
         as store_nbr,
      decode ( (nvl (epd.plan_id, '0')),
              '0', 'No',
              decode ( (nvl (epd.transaction_seq, 0)), 0, 'No', 'YES'))
         as existing_sequence_status
 from e_pkt_hdr eph, e_plan_transaction_hdr epth, e_plan_dtl epd
where     epth.transaction_value = eph.pickticket_control_number(+)
      and epd.plan_id = epth.plan_id(+)
      and epd.transaction_seq = epth.transaction_seq(+)
 union
 select '---',
      warehouse,
      pickticket_control_number,
      null,
      null,
      null,
      null,
      null,
      'No'
 from e_pkt_hdr;

CREATE OR REPLACE FORCE VIEW "VIEW_CLK_IN_OUT" ("ROW_NBR", "EMP_ID", "LOGIN_USER_ID", "LAST_NAME", "FIRST_NAME", "IS_ACTIVE", "EMP_STAT_ID", "EMP_STAT_CODE", "EFF_DATE_TIME", "SPVSR_EMP_ID", "DFLT_PERF_GOAL", "WHSE", "PAY_SCALE_ID", "DEPT_ID", "SHIFT_ID", "JOB_FUNC_ID", "PAY_RATE", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "STARTUP_TIME", "CLEANUP_TIME", "USER_DEF_FIELD_1", "USER_DEF_FIELD_2", "IS_SUPER", "COMPANY_ID", "DEPT_CODE", "SHIFT_CODE", "SPVSR_LOGIN_USER_ID", "CLOCK_IN_DATE", "CLOCK_OUT_DATE", "REPROCESS_STATUS") AS
SELECT DISTINCT emp."ROW_NBR",emp."EMP_ID",emp."LOGIN_USER_ID",emp."LAST_NAME",emp."FIRST_NAME",emp."IS_ACTIVE",emp."EMP_STAT_ID",emp."EMP_STAT_CODE",emp."EFF_DATE_TIME",emp."SPVSR_EMP_ID",emp."DFLT_PERF_GOAL",emp."WHSE",emp."PAY_SCALE_ID",emp."DEPT_ID",emp."SHIFT_ID",emp."JOB_FUNC_ID",emp."PAY_RATE",emp."CREATE_DATE_TIME",emp."MOD_DATE_TIME",emp."USER_ID",emp."STARTUP_TIME",emp."CLEANUP_TIME",emp."USER_DEF_FIELD_1",emp."USER_DEF_FIELD_2",emp."IS_SUPER",emp."COMPANY_ID",emp."DEPT_CODE",emp."SHIFT_CODE",emp."SPVSR_LOGIN_USER_ID",
                 a.clock_in_date,
                 a.clock_out_date,
                 a.reprocess_status
   FROM    VIEW_EMP_ALL emp
        LEFT OUTER JOIN
           (SELECT *
              FROM e_emp_perf_smry psmry
             WHERE psmry.clock_out_date IS NULL) a
        ON emp.emp_id = a.emp_id AND emp.whse = a.whse;

CREATE OR REPLACE FORCE VIEW "VIEW_COUNT_ACT_ELM" ("ACT_ID", "COUNT_ELEMENT") AS
select act_id, count (distinct elm_id) as count_element
   from e_act_elm
 group by act_id;

CREATE OR REPLACE FORCE VIEW "VIEW_COUNT_ERRMSG_INTERM_QA" ("INTERM_CNTL_NBR", "INTERM_NXT_UP_NBR", "INTERM_ERRMSG_COUNT") AS
select a.interm_cntl_nbr as interm_cntl_nbr,
      a.interm_nxt_up_nbr as interm_nxt_up_nbr,
      (select count (*)
         from e_interm_qa b
        where b.error_msg = a.error_msg)
         as interm_errmsg_count
 from e_interm_qa a;

CREATE OR REPLACE FORCE VIEW "VIEW_COUNT_EVNT_TEMPL" ("TEMPL_ID", "COUNT_OF_ACT", "COUNT_OF_UOM", "COUNT_OF_CRIT") AS
select e_evnt_templ_hdr.templ_id,
      (select distinct count (*)
         from e_evnt_templ_act
        where e_evnt_templ_act.templ_id = e_evnt_templ_hdr.templ_id)
         as count_of_act,
      (select distinct count (*)
         from e_evnt_templ_uom
        where e_evnt_templ_uom.templ_id = e_evnt_templ_hdr.templ_id)
         as count_of_uom,
      (select count (distinct e_evnt_templ_crit.crit_type)
         from e_evnt_templ_crit
        where e_evnt_templ_crit.templ_id = e_evnt_templ_hdr.templ_id)
         as count_of_crit
 from e_evnt_templ_hdr;

CREATE OR REPLACE FORCE VIEW "VIEW_COUNT_OPS_CODE" ("OPS_CODE_ID", "OPS_COUNT") AS
select a.ops_code_id,
      (select count (1)
         from e_ops_code_spvsr b
        where b.ops_code_id = a.ops_code_id)
         as ops_count
 from e_ops_code a;

CREATE OR REPLACE FORCE VIEW "VIEW_COUNT_QAREASON_JF" ("QA_REASON_ID", "QA_REASON_JF_COUNT") AS
select a.qa_reason_id,
      (select count (1)
         from e_job_func_qa_reason b
        where b.qa_reason_id = a.qa_reason_id)
         as qa_reason_jf_count
 from e_qa_reason a;

CREATE OR REPLACE FORCE VIEW "VIEW_COUNT_RULE_TEMPLATE_ID" ("RULE_TEMPLATE_ID", "COUNT_RULE") AS
select rhdr.rule_template_id rule_template_id,
      (select count (1)
         from e_plan_template_dtl dtl
        where dtl.rule_template_id = rhdr.rule_template_id)
         count_rule
 from e_rule_template_hdr rhdr;

CREATE OR REPLACE FORCE VIEW "VIEW_COUNT_UOM_ELM" ("MSRMNT_ID", "UOM_ELM_COUNT") AS
select a.msrmnt_id,
      (select count (1)
         from e_elm b
        where b.msrmnt_id = a.msrmnt_id)
         as uom_elm_count
 from e_msrmnt a;

CREATE OR REPLACE FORCE VIEW "VIEW_COUNT_WKFLOW_TEMPLATE" ("WORKFLOW_TEMPLATE_ID", "COUNT_WORKFLOW") AS
select wth.workflow_template_id as workflow_template_id,
      (select count (1)
         from e_plan_template_dtl pdt
        where pdt.workflow_template_id = wth.workflow_template_id)
         as count_workflow
 from e_workflow_template_hdr wth;

CREATE OR REPLACE FORCE VIEW "VIEW_CRIT_MSG_LOG" ("WHSE", "TRAN_NBR", "CRIT_TYPE", "CRIT_VAL", "CRIT_SEQ_NBR", "MSG_TYPE", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2") AS
select whse,
      tran_nbr,
      crit_type,
      crit_val,
      crit_seq_nbr,
      '016' as msg_type,
      misc_txt_1,
      misc_txt_2,
      misc_num_1,
      misc_num_2
 from view_e_hdr_crit_msg_log
 union
 select whse,
      tran_nbr,
      crit_type,
      crit_val,
      crit_seq_nbr,
      '015' as msg_type,
      misc_txt_1,
      misc_txt_2,
      misc_num_1,
      misc_num_2
 from view_e_dtl_crit_msg_log;

CREATE OR REPLACE FORCE VIEW "VIEW_CRIT_TRAN" ("WHSE", "TRAN_NBR", "CRIT_TYPE", "CRIT_VAL", "CRIT_SEQ_NBR", "MSG_TYPE", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2") AS
select whse,
      tran_nbr,
      crit_type,
      crit_val,
      crit_seq_nbr,
      '016' as msg_type,
      misc_txt_1,
      misc_txt_2,
      misc_num_1,
      misc_num_2
 from labor_tran_crit
 union
 select whse,
      tran_nbr,
      crit_type,
      crit_val,
      crit_seq_nbr,
      '016' as msg_type,
      misc_txt_1,
      misc_txt_2,
      misc_num_1,
      misc_num_2
 from labor_tran_dtl_crit;

CREATE OR REPLACE FORCE VIEW "VIEW_DTL_CRIT" ("LABOR_MSG_ID", "LABOR_MSG_DTL_ID", "TRAN_NBR", "TRAN_SEQ_NBR", "CRIT_TYPE", "CRIT_VAL", "WHSE") AS
select d.labor_msg_id,
      d.labor_msg_dtl_id,
      d.tran_nbr,
      d.tran_seq_nbr,
      dc.crit_type,
      dc.crit_val,
      dc.whse
 from    labor_msg_dtl d
      inner join
         labor_msg_dtl_crit dc
      on d.labor_msg_dtl_id = dc.labor_msg_dtl_id
         and d.tran_nbr = dc.tran_nbr;

CREATE OR REPLACE FORCE VIEW "VIEW_ELEMENT_CRITERIA" ("ELM_ID", "COUNT_ELEMENT_CRITERIA") AS
select elm_id, count (distinct crit_id) as count_element_criteria
   from e_elm_crit
 group by elm_id;

CREATE OR REPLACE FORCE VIEW "VIEW_ELEMENT_FACTOR" ("ELM_ID", "COUNT_ELEMENT_FACTOR") AS
select elm_id, count (distinct fct_id) as count_element_factor
   from e_elm_fct
 group by elm_id;

CREATE OR REPLACE FORCE VIEW "VIEW_ELM_CRIT_TABLE" ("ELM_ID", "CRIT_VAL_ID", "CRIT_ID", "TIME_ALLOW", "STATUS") AS
select elm_id,
      crit_val_id,
      crit_id,
      time_allow,
      1 as status
 from e_elm_crit
 union
 select elm_id as elm_id,
      e_crit_val.crit_val_id as crit_val_id,
      e_crit_val.crit_id as crit_id,
      0 as time_allow,
      0 as status
 from e_elm, e_crit_val, labor_criteria
where e_crit_val.crit_val_id not in
         (select crit_val_id
            from e_elm_crit
           where e_elm_crit.elm_id = e_elm.elm_id
                 and e_elm_crit.crit_id = labor_criteria.crit_id)
      and e_crit_val.crit_id = labor_criteria.crit_id;

CREATE OR REPLACE FORCE VIEW "VIEW_ELM_CRIT_VAL" ("ELM_ID", "CRIT_ID", "COUNT_CRITERIA_VAL", "ELEMENT_CRITERIA_VALUES") AS
select elm_id,
        crit_id,
        count (distinct crit_val_id) as count_criteria_val,
        fn_elm_crit_val_str (elm_id, crit_id) as element_criteria_values
   from e_elm_crit
 group by elm_id, crit_id;

CREATE OR REPLACE FORCE VIEW "VIEW_ELM_FCT_CRIT" ("ELM_ID", "FCT_ID", "COUNT_ELEMENT_FACTOR_CRITERIA", "ELEMENT_CRITERIA_VALUES") AS
select elm_id,
        fct_id,
        count (distinct crit_id) as count_element_factor_criteria,
        fn_elm_fct_crit_str (elm_id, fct_id) as element_criteria_values
   from e_elm_fct_crit
 group by elm_id, fct_id;

CREATE OR REPLACE FORCE VIEW "VIEW_ELM_FCT_CRIT_VAL" ("ELM_ID", "FCT_ID", "CRIT_ID", "COUNT_CRITERIA_VAL", "CRIT_VALUE") AS
select elm_id,
        fct_id,
        crit_id,
        count (distinct crit_val_id) as count_criteria_val,
        fn_elm_fct_crit_val_str (elm_id, fct_id, crit_id) as crit_value
   from e_elm_fct_crit
 group by elm_id, fct_id, crit_id;

CREATE OR REPLACE FORCE VIEW "VIEW_ELM_FCT_TABLE" ("ELM_ID", "FCT_ID", "TIME_ALLOW", "ADJ_PCNT", "STATUS", "MINADJSEL") AS
select elm_id,
      fct_id,
      time_allow,
      adj_pcnt,
      1 as status,
      get_min_adj (time_allow, adj_pcnt) as minadjsel
 from e_elm_fct
 union
 select elm_id as elm_id,
      e_fct.fct_id as fct_id,
      0 as time_allow,
      0 as adj_pcnt,
      0 as status,
      'M' as minadjsel
 from e_elm, e_fct
where (elm_id, fct_id) not in (select elm_id, fct_id from e_elm_fct);

CREATE OR REPLACE FORCE VIEW "VIEW_EMAN_ACT_LIST" ("TEMPL_ID", "ACT_NAME") AS
select e_evnt_templ_hdr.templ_id as templ_id,
      labor_activity.name as act_name
 from e_evnt_templ_hdr,
      e_evnt_templ_act,
      e_act,
      labor_activity
where     e_evnt_templ_act.templ_id(+) = e_evnt_templ_hdr.templ_id
      and e_act.act_id(+) = e_evnt_templ_act.act_id
      and labor_activity.labor_activity_id = e_act.labor_activity_id;

CREATE OR REPLACE FORCE VIEW "VIEW_EMAN_CRIT_LIST" ("TEMPL_ID", "CRIT_TYPE") AS
select e_evnt_templ_hdr.templ_id as templ_id,
      e_evnt_templ_crit.crit_type as crit_type
 from    e_evnt_templ_hdr
      left outer join
         e_evnt_templ_crit
      on e_evnt_templ_crit.templ_id = e_evnt_templ_hdr.templ_id;

CREATE OR REPLACE FORCE VIEW "VIEW_EMP" ("ROW_NBR", "EMP_ID", "LOGIN_USER_ID", "LAST_NAME", "FIRST_NAME", "IS_ACTIVE", "TAX_ID_NBR", "EMP_STAT_ID", "EMP_STAT_CODE", "EFF_DATE_TIME", "SPVSR_EMP_ID", "DFLT_PERF_GOAL", "WHSE", "PAY_SCALE_ID", "DEPT_ID", "SHIFT_ID", "JOB_FUNC_ID", "PAY_RATE", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "STARTUP_TIME", "CLEANUP_TIME", "USER_DEF_FIELD_1", "USER_DEF_FIELD_2", "IS_SUPER", "COMPANY_ID") AS
SELECT ROWNUM        AS ROW_NBR,
  U.UCL_USER_ID      AS EMP_ID,
  U.USER_NAME        AS LOGIN_USER_ID,
  U.USER_LAST_NAME   AS LAST_NAME,
  U.USER_FIRST_NAME  AS FIRST_NAME,
  U.IS_ACTIVE        AS IS_ACTIVE,
  U.TAX_ID_NBR       AS TAX_ID_NBR,
  CODE.EMP_STAT_ID   AS EMP_STAT_ID,
  CODE.EMP_STAT_CODE AS EMP_STAT_CODE,
  D.EFF_DATE_TIME    AS EFF_DATE_TIME,
  D.SPVSR_EMP_ID     AS SPVSR_EMP_ID,
  D.DFLT_PERF_GOAL   AS DFLT_PERF_GOAL,
  U.WHSE             AS WHSE,
  D.PAY_SCALE_ID     AS PAY_SCALE_ID,
  D.DEPT_ID          AS DEPT_ID,
  D.SHIFT_ID         AS SHIFT_ID,
  D.JOB_FUNC_ID      AS JOB_FUNC_ID,
  D.PAY_RATE         AS PAY_RATE,
  D.CREATE_DATE_TIME AS CREATE_DATE_TIME,
  D.MOD_DATE_TIME    AS MOD_DATE_TIME,
  D.USER_ID          AS USER_ID,
  D.STARTUP_TIME     AS STARTUP_TIME,
  D.CLEANUP_TIME     AS CLEANUP_TIME,
  D.USER_DEF_FIELD_1 AS USER_DEF_FIELD_1,
  D.USER_DEF_FIELD_2 AS USER_DEF_FIELD_2,
  D.IS_SUPER         AS IS_SUPER,
  U.COMPANY_ID       AS COMPANY_ID
FROM
  (SELECT *
  FROM UCL_USER
  CROSS JOIN FACILITY where WHSE is not null
  ) U
LEFT OUTER JOIN E_EMP_DTL D
ON U.UCL_USER_ID = D.EMP_ID
AND U.WHSE       = D.WHSE
LEFT OUTER JOIN E_EMP_STAT_CODE CODE
ON D.EMP_STAT_ID    = CODE.EMP_STAT_ID
WHERE USER_TYPE_ID != 12
AND (U.IS_ACTIVE    = 1
OR D.EMP_STAT_ID    = 1)
AND (EFF_DATE_TIME IS NULL
OR (EFF_DATE_TIME   =
  (SELECT MAX (EFF_DATE_TIME)
  FROM E_EMP_DTL E
  WHERE E.EMP_ID = U.UCL_USER_ID
  AND E.WHSE     = U.WHSE
  )));

CREATE OR REPLACE FORCE VIEW "VIEW_EMP_ALL" ("ROW_NBR", "EMP_ID", "LOGIN_USER_ID", "LAST_NAME", "FIRST_NAME", "IS_ACTIVE", "EMP_STAT_ID", "EMP_STAT_CODE", "EFF_DATE_TIME", "SPVSR_EMP_ID", "DFLT_PERF_GOAL", "WHSE", "PAY_SCALE_ID", "DEPT_ID", "SHIFT_ID", "JOB_FUNC_ID", "PAY_RATE", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "STARTUP_TIME", "CLEANUP_TIME", "USER_DEF_FIELD_1", "USER_DEF_FIELD_2", "IS_SUPER", "COMPANY_ID", "DEPT_CODE", "SHIFT_CODE", "SPVSR_LOGIN_USER_ID") AS
select rownum as row_nbr,
      u.ucl_user_id as emp_id,
      u.user_name as login_user_id,
      u.user_last_name as last_name,
      u.user_first_name as first_name,
      u.is_active as is_active,
      code.emp_stat_id as emp_stat_id,
      code.emp_stat_code as emp_stat_code,
      d.eff_date_time as eff_date_time,
      d.spvsr_emp_id as spvsr_emp_id,
      d.dflt_perf_goal as dflt_perf_goal,
      u.whse as whse,
      d.pay_scale_id as pay_scale_id,
      d.dept_id as dept_id,
      d.shift_id as shift_id,
      d.job_func_id as job_func_id,
      d.pay_rate as pay_rate,
      d.create_date_time as create_date_time,
      d.mod_date_time as mod_date_time,
      d.user_id as user_id,
      d.startup_time as startup_time,
      d.cleanup_time as cleanup_time,
      d.user_def_field_1 as user_def_field_1,
      d.user_def_field_2 as user_def_field_2,
      d.is_super as is_super,
      u.company_id as company_id,
      dept.dept_code as dept_code,
      shft.shift_code as shift_code,
      (select user_name
         from ucl_user
        where ucl_user_id = d.spvsr_emp_id)
         as spvsr_login_user_id
 from (select *
         from ucl_user cross join facility) u
      left outer join (      e_emp_dtl d
                          inner join
                             e_dept dept
                          on (d.dept_id = dept.dept_id
                              and d.whse = dept.whse)
                       inner join
                          e_shift shft
                       on d.shift_id = shft.shift_id
                          and d.whse = shft.whse)
         on u.ucl_user_id = d.emp_id and u.whse = d.whse
      left outer join e_emp_stat_code code
         on d.emp_stat_id = code.emp_stat_id
where user_type_id != 12
      and (eff_date_time =
              (select max (eff_date_time)
                 from e_emp_dtl e
                where     e.emp_id = u.ucl_user_id
                      and e.whse = u.whse
                      and e.eff_date_time <= sysdate));

CREATE OR REPLACE FORCE VIEW "VIEW_EMP_LATEST_INFO" ("EMP_ID", "LOGIN_USER_ID", "TAX_ID_NBR", "LAST_NAME", "FIRST_NAME", "CREATED_SOURCE", "DFLT_PERF_GOAL", "WHSE", "EMP_STAT_CODE", "JOB_FUNC_NAME", "DEPT_CODE", "SHIFT_CODE", "SPVSR_LOGIN_USER_ID", "SPVSR_LASTNAME", "SPVSR_FIRSTNAME") AS
select e1.emp_id,
      e1.login_user_id,
      e1.tax_id_nbr,
      e1.last_name,
      e1.first_name,
      e1.created_source,
      latest_emp_dtl.dflt_perf_goal,
      latest_emp_dtl.whse,
      e_emp_stat_code.emp_stat_code,
      e_job_function.name job_func_name,
      e_dept.dept_code,
      e_shift.shift_code,
      e2.login_user_id spvsr_login_user_id,
      e2.last_name as spvsr_lastname,
      e2.first_name as spvsr_firstname
 from view_ucl_user e1,
      latest_emp_dtl,
      e_emp_stat_code,
      e_job_function,
      e_dept,
      e_shift,
      view_ucl_user e2
where     e1.emp_id = latest_emp_dtl.emp_id
      and latest_emp_dtl.emp_stat_id = e_emp_stat_code.emp_stat_id
      and latest_emp_dtl.job_func_id = e_job_function.job_func_id
      and latest_emp_dtl.dept_id = e_dept.dept_id
      and latest_emp_dtl.shift_id = e_shift.shift_id
      and latest_emp_dtl.spvsr_emp_id = e2.emp_id;

CREATE OR REPLACE FORCE VIEW "VIEW_EMP_PAY" ("JOB_ID", "EXTRACTION_DATE_TIME", "EMP_PAY_ID", "EMP_ID", "JOB_FUNC_ID", "SHIFT_ID", "WHSE_DATE", "PAY_DATE", "PAY_HOURS", "PAY_RATE_ID", "TOTAL_HOURLY_PAY", "SPVSR_EMP_ID", "DEPT_ID", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "WHSE", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2", "PAY_TYPE_ID", "LAST_MOD_BY", "ADJ_APRV_USER_ID", "ADJ_REASON_CODE", "ADJ_COMMENT", "MOD_LOGIN_USER_ID") AS
select job_id,
      extraction_date_time,
      emp_pay_id,
      emp_id,
      job_func_id,
      shift_id,
      whse_date,
      pay_date,
      coalesce (pay_hours, 0) pay_hours,
      pay_rate_id,
      coalesce (
         (case
             when adj_total_hourly_pay is not null
             then
                adj_total_hourly_pay
             else
                total_hourly_pay
          end),
         0)
         total_hourly_pay,
      spvsr_emp_id,
      dept_id,
      create_date_time,
      mod_date_time,
      user_id,
      whse,
      misc_txt_1,
      misc_txt_2,
      misc_num_1,
      misc_num_2,
      pay_type_id,
      last_mod_by,
      adj_aprv_user_id,
      adj_reason_code,
      adj_comment,
      mod_login_user_id
 from e_interm_emp_pay;

CREATE OR REPLACE FORCE VIEW "VIEW_EMP_PERF_SMRY" ("EMP_ID", "SHIFT_ID", "SPVSR_ID", "CLOCK_IN_DATE", "CLOCK_OUT_DATE", "WHSE", "EMPWHSEBEGINTIME") AS
select emp_id,
      shift_id,
      spvsr_id,
      clock_in_date,
      clock_out_date,
      whse,
      getwhsedatetime (clock_in_date, whse) as empwhsebegintime
 from e_emp_perf_smry;

CREATE OR REPLACE FORCE VIEW "VIEW_ENGINE_TYPE" ("MISC_FLAGS", "CODE_DESC") AS
select misc_flags, code_desc
 from sys_code
where rec_type = 'L' and code_type = '087';

CREATE OR REPLACE FORCE VIEW "VIEW_EVNT_SMRY_HDR" ("ELS_TRAN_ID", "WHSE", "TRAN_NBR", "CO", "DIV", "SCHED_START_DATE", "REF_CODE", "REF_NBR", "LOGIN_USER_ID", "DEPT_CODE", "VHCL_TYPE", "SHIFT_CODE", "USER_DEF_FIELD_1", "USER_DEF_FIELD_2", "STD_TIME_ALLOW", "EMP_PERF_ALLOW", "PERF_ADJ_AMT", "SCHED_ADJ_AMT", "SCHED_UNITS_QTY", "ACTL_TIME", "ERROR_CNT", "EVNT_STAT_CODE", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "TMU_RECALC_COUNT", "MISC", "LABOR_TYPE_ID", "SOURCE", "EVENT_TYPE", "TOTAL_WEIGHT", "TEAM_STD_SMRY_ID", "TEAM_CODE", "ORIG_LOGIN_USER_ID", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2", "PROC_ZONE_ID", "LAST_MOD_BY", "EVNT_CTGRY_1", "EVNT_CTGRY_2", "EVNT_CTGRY_3", "EVNT_CTGRY_4", "EVNT_CTGRY_5", "SYS_UPDATED_FLAG", "APRV_SPVSR", "APRV_SPVSR_DATE_TIME", "VERSION_ID", "TEAM_CHG_ID", "ACTUAL_END_DATE", "SPVSR_LOGIN_USER_ID", "PAID_BRK_OVERLAP", "UNPAID_BRK_OVERLAP", "EMP_PERF_SMRY_ID", "HDR_MSG_ID", "ACT_ID", "JOB_FUNCTION_NAME", "CLOCK_IN_DATE", "CLOCK_OUT_DATE") AS
SELECT HDR.ELS_TRAN_ID AS ELS_TRAN_ID,
        HDR.WHSE AS WHSE,
        HDR.TRAN_NBR AS TRAN_NBR,
        HDR.CO AS CO,
        HDR.DIV AS DIV,
        HDR.SCHED_START_DATE AS SCHED_START_DATE,
        HDR.REF_CODE AS REF_CODE,
        HDR.REF_NBR AS REF_NBR,
        HDR.LOGIN_USER_ID AS LOGIN_USER_ID,
        HDR.DEPT_CODE AS DEPT_CODE,
        HDR.VHCL_TYPE AS VHCL_TYPE,
        HDR.SHIFT_CODE AS SHIFT_CODE,
        HDR.USER_DEF_FIELD_1 AS USER_DEF_FIELD_1,
        HDR.USER_DEF_FIELD_2 AS USER_DEF_FIELD_2,
        HDR.STD_TIME_ALLOW AS STD_TIME_ALLOW,
        HDR.EMP_PERF_ALLOW AS EMP_PERF_ALLOW,
        HDR.PERF_ADJ_AMT AS PERF_ADJ_AMT,
        HDR.SCHED_ADJ_AMT AS SCHED_ADJ_AMT,
        HDR.SCHED_UNITS_QTY AS SCHED_UNITS_QTY,
        HDR.ACTL_TIME AS ACTL_TIME,
        HDR.ERROR_CNT AS ERROR_CNT,
        HDR.EVNT_STAT_CODE AS EVNT_STAT_CODE,
        HDR.CREATE_DATE_TIME AS CREATE_DATE_TIME,
        HDR.MOD_DATE_TIME AS MOD_DATE_TIME,
        HDR.USER_ID AS USER_ID,
        HDR.TMU_RECALC_COUNT AS TMU_RECALC_COUNT,
        HDR.MISC AS MISC,
        HDR.LABOR_TYPE_ID AS LABOR_TYPE_ID,
        HDR.SOURCE AS SOURCE,
        HDR.EVENT_TYPE AS EVENT_TYPE,
        HDR.TOTAL_WEIGHT AS TOTAL_WEIGHT,
        HDR.TEAM_STD_SMRY_ID AS TEAM_STD_SMRY_ID,
        HDR.TEAM_CODE AS TEAM_CODE,
        HDR.ORIG_LOGIN_USER_ID AS ORIG_LOGIN_USER_ID,
        HDR.MISC_TXT_1 AS MISC_TXT_1,
        HDR.MISC_TXT_2 AS MISC_TXT_2,
        HDR.MISC_NUM_1 AS MISC_NUM_1,
        HDR.MISC_NUM_2 AS MISC_NUM_2,
        HDR.PROC_ZONE_ID AS PROC_ZONE_ID,
        HDR.LAST_MOD_BY AS LAST_MOD_BY,
        HDR.EVNT_CTGRY_1 AS EVNT_CTGRY_1,
        HDR.EVNT_CTGRY_2 AS EVNT_CTGRY_2,
        HDR.EVNT_CTGRY_3 AS EVNT_CTGRY_3,
        HDR.EVNT_CTGRY_4 AS EVNT_CTGRY_4,
        HDR.EVNT_CTGRY_5 AS EVNT_CTGRY_5,
        HDR.SYS_UPDATED_FLAG AS SYS_UPDATED_FLAG,
        HDR.APRV_SPVSR AS APRV_SPVSR,
        HDR.APRV_SPVSR_DATE_TIME AS APRV_SPVSR_DATE_TIME,
        HDR.VERSION_ID AS VERSION_ID,
        HDR.TEAM_CHG_ID AS TEAM_CHG_ID,
        HDR.ACTUAL_END_DATE AS ACTUAL_END_DATE,
        HDR.SPVSR_LOGIN_USER_ID AS SPVSR_LOGIN_USER_ID,
        HDR.PAID_BRK_OVERLAP AS PAID_BRK_OVERLAP,
        HDR.UNPAID_BRK_OVERLAP AS UNPAID_BRK_OVERLAP,
        HDR.EMP_PERF_SMRY_ID AS EMP_PERF_SMRY_ID,
        HDR.HDR_MSG_ID AS HDR_MSG_ID,
        HDR.ACT_ID AS ACT_ID,
        COALESCE (smry.job_function_name, jfunc.NAME) AS job_function_name,
        smry.clock_in_date AS CLOCK_IN_DATE,
        smry.clock_out_date AS CLOCK_OUT_DATE
   FROM FACILITY FAC
        INNER JOIN WHSE_LABOR_PARAMETERS PARM
           ON FAC.FACILITY_ID = PARM.FACILITY_ID
        INNER JOIN E_EVNT_SMRY_HDR HDR
           ON FAC.WHSE = HDR.WHSE
        INNER JOIN E_ACT ACT
           ON HDR.ACT_ID = ACT.ACT_ID
        INNER JOIN E_JOB_FUNCTION JFUNC
           ON JFUNC.JOB_FUNC_ID = ACT.JOB_FUNC_ID
        LEFT OUTER JOIN E_CONSOL_PERF_SMRY SMRY
           ON (    HDR.EMP_PERF_SMRY_ID = SMRY.EMP_PERF_SMRY_ID
               AND HDR.WHSE = SMRY.WHSE
               AND SMRY.CLOCK_IN_STATUS <> 99
               AND jfunc.NAME = 'DEFAULT JOB FUNCTION'
               AND ( (    HDR.SCHED_START_DATE >= SMRY.START_DATE_TIME
                      AND HDR.SCHED_START_DATE < SMRY.END_DATE_TIME
                      AND HDR.SCHED_START_DATE <> HDR.ACTUAL_END_DATE)
                    OR (    HDR.SCHED_START_DATE = HDR.ACTUAL_END_DATE
                        AND HDR.SCHED_START_DATE > SMRY.START_DATE_TIME
                        AND HDR.SCHED_START_DATE <= SMRY.END_DATE_TIME
                        AND JFUNC.NAME = 'DEFAULT JOB FUNCTION'
                        AND PARM.Prepend_Job_Func = 'N')
                    OR (    HDR.SCHED_START_DATE = HDR.ACTUAL_END_DATE
                        AND HDR.SCHED_START_DATE >= SMRY.START_DATE_TIME
                        AND HDR.SCHED_START_DATE < SMRY.END_DATE_TIME
                        AND JFUNC.NAME = 'DEFAULT JOB FUNCTION'
                        AND PARM.Prepend_Job_Func = 'Y')
                    OR (    HDR.SCHED_START_DATE = HDR.ACTUAL_END_DATE
                        AND HDR.SCHED_START_DATE >= SMRY.START_DATE_TIME
                        AND HDR.SCHED_START_DATE < SMRY.END_DATE_TIME
                        AND JFUNC.NAME = SMRY.JOB_FUNCTION_NAME)));

CREATE OR REPLACE FORCE VIEW "VIEW_E_ASN_DTL" ("SHIPMENT_NBR", "SEQUENCE_NUMBER", "ITEM", "CASES_SHIPPED", "CASES_RECEIVED", "UNITS_SHIPPED", "UNITS_RECEIVED", "ASN_TYPE", "VENDOR_ASN_NBR", "PO_NBR", "PO_LINE", "COMPANY", "DIVISION", "SKU_ID", "SEASON", "SEASON_YEAR", "STYLE", "STYLE_SFX", "COLOR", "COLOR_SFX", "SEC_DIMENSION", "QUALITY", "SIZE_RNGE_CODE", "SIZE_REL_POSN_IN_TABLE", "SIZE_DESC", "INVENTORY_TYPE", "PRODUCT_STATUS", "SKU_BATCH_NBR", "COUNTRY_OF_ORIGIN", "SKU_ATTRIBUTE_1", "SKU_ATTRIBUTE_2", "SKU_ATTRIBUTE_3", "SKU_ATTRIBUTE_4", "SKU_ATTRIBUTE_5", "INNER_PACK_QTY", "CASES_VERIFIED", "UNITS_VERIFIED", "PRE_RECEIVED_QUANTITY", "STD_CASE_QUANTITY", "VENDOR_TIE", "VENDOR_HIGH", "PRICE", "TICKET_TYPE", "PROCESS_IMMD_NEEDS", "MANUFACTURING_PLANT", "MANUFACTURING_DATE", "EXPIRATION_DATE", "QC_HOLD_UPON_RECEIPT", "CASE_QUALITY_AUDITED_FLAG", "PURCHASING_UNIT_OF_MEASURE", "SHIPMENT_PRIORITY", "PATH_NUMBER", "ASSIGNMENT_NUMBER", "WEIGHT", "CONDITION_CODE", "STATUS_CODE", "REASON_CODE", "CONVERT_TO_PRE_OWN_SKU", "MANUFACTURE_TIME", "INCUBATION_CMPL_DATE", "INCUBATION_CMPL_TIME", "REFERENCE_1", "REFERENCE_2", "REFERENCE_3", "SPECIAL_INS_CODE_1", "SPECIAL_INS_CODE_2", "SPECIAL_INS_CODE_3", "SPECIAL_INS_CODE_4", "MISC_INS_20_BYTE_1", "MISC_INS_20_BYTE_2", "MISC_INS_20_BYTE_3", "MISC_INS_20_BYTE_4", "MISC_NUMBER_1", "MISC_NUMBER_2", "MISC_NUMBER_3", "MISC_NUMBER_4", "RECORD_EXPANSION_FIELD", "CUST_RCD_EXPANSION_FIELD", "DATE_CREATED", "TIME_CREATED", "DATE_LAST_MODIFIED", "TIME_LAST_MODIFIED", "USER_ID", "VENDOR_ID", "PRE_PACK_GROUP_CODE", "ASSORT_NUMBER", "STANDARD_SUB_PACK_QUANTITY", "LPN_PER_TIER", "TIER_PER_PALLET", "UNITS_ASSIGNED_TO_LPN", "SHIP_BY_DATE", "CUT_NUMBER", "TOTAL_CATCH_WEIGHT", "TOTAL_SKU_UNLOAD_TIME", "NBR_PACK_FOR_CATCH_WEIGHT", "PUTAWAY_TYPE", "SKU_VERIFICATION", "CI_NBR", "CI_SEQ_NBR", "ITEM_DIM_CHECKED_FLAG") AS
select shipment_nbr,
      sequence_number,
         nvl (season, '')
      || '_'
      || nvl (season_year, '')
      || '_'
      || nvl (style, '')
      || '_'
      || nvl (style_sfx, '')
      || '_'
      || nvl (color, '')
      || '_'
      || nvl (color_sfx, '')
      || '_'
      || nvl (sec_dimension, '')
      || '_'
      || nvl (quality, '')
      || '_'
      || nvl (size_rnge_code, '')
      || '_'
      || nvl (size_rel_posn_in_table, '')
      || '_'
      || nvl (size_desc, '')
         item,
      cases_shipped,
      cases_received,
      units_shipped,
      units_received,
      asn_type,
      vendor_asn_nbr,
      po_nbr,
      po_line,
      company,
      division,
      sku_id,
      season,
      season_year,
      style,
      style_sfx,
      color,
      color_sfx,
      sec_dimension,
      quality,
      size_rnge_code,
      size_rel_posn_in_table,
      size_desc,
      inventory_type,
      product_status,
      sku_batch_nbr,
      country_of_origin,
      sku_attribute_1,
      sku_attribute_2,
      sku_attribute_3,
      sku_attribute_4,
      sku_attribute_5,
      inner_pack_qty,
      cases_verified,
      units_verified,
      pre_received_quantity,
      std_case_quantity,
      vendor_tie,
      vendor_high,
      price,
      ticket_type,
      process_immd_needs,
      manufacturing_plant,
      manufacturing_date,
      expiration_date,
      qc_hold_upon_receipt,
      case_quality_audited_flag,
      purchasing_unit_of_measure,
      shipment_priority,
      path_number,
      assignment_number,
      weight,
      condition_code,
      status_code,
      reason_code,
      convert_to_pre_own_sku,
      manufacture_time,
      incubation_cmpl_date,
      incubation_cmpl_time,
      reference_1,
      reference_2,
      reference_3,
      special_ins_code_1,
      special_ins_code_2,
      special_ins_code_3,
      special_ins_code_4,
      misc_ins_20_byte_1,
      misc_ins_20_byte_2,
      misc_ins_20_byte_3,
      misc_ins_20_byte_4,
      misc_number_1,
      misc_number_2,
      misc_number_3,
      misc_number_4,
      record_expansion_field,
      cust_rcd_expansion_field,
      date_created,
      time_created,
      date_last_modified,
      time_last_modified,
      user_id,
      vendor_id,
      pre_pack_group_code,
      assort_number,
      standard_sub_pack_quantity,
      lpn_per_tier,
      tier_per_pallet,
      units_assigned_to_lpn,
      ship_by_date,
      cut_number,
      total_catch_weight,
      total_sku_unload_time,
      nbr_pack_for_catch_weight,
      putaway_type,
      sku_verification,
      ci_nbr,
      ci_seq_nbr,
      item_dim_checked_flag
 from e_asn_dtl;

CREATE OR REPLACE FORCE VIEW "VIEW_E_DTL_CRIT_MSG_LOG" ("DTL_CRIT_MSG_ID", "DTL_MSG_ID", "WHSE", "TRAN_NBR", "CRIT_SEQ_NBR", "CRIT_TYPE", "CRIT_VAL", "MSG_STAT_CODE", "CREATE_DATE_TIME", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2", "MOD_DATE_TIME", "USER_ID", "VERSION_ID") AS
select labor_msg_dtl_crit_id as dtl_crit_msg_id,
      labor_msg_dtl_id as dtl_msg_id,
      whse as whse,
      tran_nbr as tran_nbr,
      crit_seq_nbr as crit_seq_nbr,
      crit_type as crit_type,
      crit_val as crit_val,
      msg_stat_code as msg_stat_code,
      created_dttm as create_date_time,
      misc_txt_1 as misc_txt_1,
      misc_txt_2 as misc_txt_2,
      misc_num_1 as misc_num_1,
      misc_num_2 as misc_num_2,
      last_updated_dttm as mod_date_time,
      created_source as user_id,
      hibernate_version as version_id
 from labor_msg_dtl_crit;

CREATE OR REPLACE FORCE VIEW "VIEW_E_DTL_MSG_LOG" ("DTL_MSG_ID", "HDR_MSG_ID", "TRAN_NBR", "TRAN_SEQ_NBR", "SKU_BRCD", "ITEM_NAME", "SKU_SEQ_NBR", "SKU_HNDL_ATTR", "QTY_PER_GRAB", "DSP_LOCN", "LOCN_CLASS", "QTY", "CARTON_ID", "CASE_ID", "PALLET_ID", "LOCN_SLOT_TYPE", "LOCN_X_COORD", "LOCN_Y_COORD", "LOCN_Z_COORD", "LOCN_TRAV_AISLE", "LOCN_TRAV_ZONE", "SKU_BOX_QTY", "SKU_SUB_PACK_QTY", "SKU_PACK_QTY", "SKU_CASE_QTY", "SKU_TIER_QTY", "SKU_PALLET_QTY", "CO", "DIV", "MISC", "WEIGHT", "VOLUME", "CRIT_DIM1", "CRIT_DIM2", "CRIT_DIM3", "START_DATE_TIME", "HANDLING_UOM", "SEASON", "SEASON_YR", "STYLE", "STYLE_SFX", "COLOR", "COLOR_SFX", "SEC_DIM", "QUAL", "SIZE_RNGE_CODE", "SIZE_REL_POSN_IN_TABLE", "MSG_STAT_CODE", "MISC_NUM_1", "MISC_NUM_2", "MISC_2", "LOADED", "PUTAWAY_ZONE", "PICK_DETERMINATION_ZONE", "WORK_GROUP", "WORK_AREA", "PULL_ZONE", "ASSIGNMENT_ZONE", "CREATE_DATE_TIME", "MOD_DATE_TIME", "VERSION_ID", "USER_ID") AS
select labor_msg_dtl_id as dtl_msg_id,
      labor_msg_id as hdr_msg_id,
      tran_nbr as tran_nbr,
      tran_seq_nbr as tran_seq_nbr,
      item_bar_code as sku_brcd,
      item_name as item_name,
      seq_nbr as sku_seq_nbr,
      hndl_attr as sku_hndl_attr,
      qty_per_grab as qty_per_grab,
      dsp_locn as dsp_locn,
      locn_class as locn_class,
      qty as qty,
      tc_olpn_id as carton_id,
      tc_ilpn_id as case_id,
      pallet_id as pallet_id,
      locn_slot_type as locn_slot_type,
      locn_x_coord as locn_x_coord,
      locn_y_coord as locn_y_coord,
      locn_z_coord as locn_z_coord,
      locn_trav_aisle as locn_trav_aisle,
      locn_trav_zone as locn_trav_zone,
      box_qty as sku_box_qty,
      innerpack_qty as sku_sub_pack_qty,
      pack_qty as sku_pack_qty,
      case_qty as sku_case_qty,
      tier_qty as sku_tier_qty,
      pallet_qty as sku_pallet_qty,
      co as co,
      div as div,
      misc as misc,
      weight as weight,
      volume as volume,
      crit_dim1 as crit_dim1,
      crit_dim2 as crit_dim2,
      crit_dim3 as crit_dim3,
      start_date_time as start_date_time,
      handling_uom as handling_uom,
      season as season,
      season_yr as season_yr,
      style as style,
      style_sfx as style_sfx,
      color as color,
      color_sfx as color_sfx,
      sec_dim as sec_dim,
      qual as qual,
      size_rnge_code as size_rnge_code,
      size_rel_posn_in_table as size_rel_posn_in_table,
      msg_stat_code as msg_stat_code,
      misc_num_1 as misc_num_1,
      misc_num_2 as misc_num_2,
      misc_2 as misc_2,
      loaded as loaded,
      putaway_zone as putaway_zone,
      pick_determination_zone as pick_determination_zone,
      work_group as work_group,
      work_area as work_area,
      pull_zone as pull_zone,
      assignment_zone as assignment_zone,
      created_dttm as create_date_time,
      last_updated_dttm as mod_date_time,
      hibernate_version as version_id,
      created_source as user_id
 from labor_msg_dtl;

CREATE OR REPLACE FORCE VIEW "VIEW_E_HDR_CRIT_MSG_LOG" ("HDR_CRIT_MSG_ID", "HDR_MSG_ID", "WHSE", "TRAN_NBR", "CRIT_SEQ_NBR", "CRIT_TYPE", "CRIT_VAL", "MSG_STAT_CODE", "CREATE_DATE_TIME", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2", "MOD_DATE_TIME", "VERSION_ID", "USER_ID") AS
select labor_msg_crit_id as hdr_crit_msg_id,
      labor_msg_id as hdr_msg_id,
      whse as whse,
      tran_nbr as tran_nbr,
      crit_seq_nbr as crit_seq_nbr,
      crit_type as crit_type,
      crit_val as crit_val,
      msg_stat_code as msg_stat_code,
      created_dttm as create_date_time,
      misc_txt_1 as misc_txt_1,
      misc_txt_2 as misc_txt_2,
      misc_num_1 as misc_num_1,
      misc_num_2 as misc_num_2,
      last_updated_dttm as mod_date_time,
      hibernate_version as version_id,
      created_source as user_id
 from labor_msg_crit;

CREATE OR REPLACE FORCE VIEW "VIEW_E_HDR_MSG_LOG" ("HDR_MSG_ID", "TRAN_NBR", "MSG_STAT_CODE", "STATUS", "LOGIN_USER_ID", "SCHED_START_TIME", "SCHED_START_DATE", "WHSE", "VHCL_TYPE", "REF_CODE", "REF_NBR", "DIV", "DEPT", "SHIFT", "USER_DEF_VAL_1", "USER_DEF_VAL_2", "MISC", "MOD_USER_ID", "RESEND_TRAN", "REQ_SAM_REPLY", "PRIORITY", "ENGINE_CODE", "TEAM_STD_GRP", "MISC_2", "ACTUAL_END_TIME", "ACTUAL_END_DATE", "PLAN_ID", "WAVE_NBR", "TRANS_TYPE", "TRANS_VALUE", "ENGINE_GROUP", "COMPLETED_WORK", "WHSE_DATE", "JOB_FUNCTION", "LEVEL_1", "LEVEL_2", "LEVEL_3", "LEVEL_4", "LEVEL_5", "CARTON_NBR", "TASK_NBR", "CASE_NBR", "ORIG_COMPLETED_WORK", "TRAN_DATA_ELS_TRAN_ID", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "LAST_MOD_BY", "EVNT_CTGRY_1", "EVNT_CTGRY_2", "EVNT_CTGRY_3", "EVNT_CTGRY_4", "EVNT_CTGRY_5", "VERSION_ID", "ACT_NAME", "CO", "MONITOR_SMRY_ID", "ORIG_ACT_NAME", "HAS_MONITOR_MSG", "SOURCE", "TOTAL_QTY") AS
SELECT labor_msg_id AS hdr_msg_id,
        tran_nbr AS tran_nbr,
        msg_stat_code AS msg_stat_code,
        status AS status,
        login_user_id AS login_user_id,
        sched_start_time AS sched_start_time,
        sched_start_date AS sched_start_date,
        whse AS whse,
        vhcl_type AS vhcl_type,
        ref_code AS ref_code,
        ref_nbr AS ref_nbr,
        div AS div,
        dept AS dept,
        shift AS shift,
        user_def_val_1 AS user_def_val_1,
        user_def_val_2 AS user_def_val_2,
        misc AS misc,
        mod_user_id AS mod_user_id,
        resend_tran AS resend_tran,
        req_sam_reply AS req_sam_reply,
        priority AS priority,
        engine_code AS engine_code,
        team_std_grp AS team_std_grp,
        misc_2 AS misc_2,
        actual_end_time AS actual_end_time,
        actual_end_date AS actual_end_date,
        plan_id AS plan_id,
        wave_nbr AS wave_nbr,
        trans_type AS trans_type,
        trans_value AS trans_value,
        engine_group AS engine_group,
        completed_work AS completed_work,
        whse_date AS whse_date,
        job_function AS job_function,
        level_1 AS level1,
        level_2 AS level2,
        level_3 AS level3,
        level_4 AS level4,
        level_5 AS level5,
        carton_nbr AS carton_nbr,
        task_nbr AS task_nbr,
        case_nbr AS case_nbr,
        orig_completed_work AS orig_completed_work,
        tran_data_els_tran_id AS tran_data_els_tran_id,
        misc_txt_1 AS misc_txt_1,
        misc_txt_2 AS misc_txt_2,
        misc_num_1 AS misc_num_1,
        misc_num_2 AS misc_num_2,
        created_dttm AS create_date_time,
        last_updated_dttm AS mod_date_time,
        created_source AS user_id,
        last_updated_source AS last_mod_by,
        evnt_ctgry_1 AS evnt_ctgry_1,
        evnt_ctgry_2 AS evnt_ctgry_2,
        evnt_ctgry_3 AS evnt_ctgry_3,
        evnt_ctgry_4 AS evnt_ctgry_4,
        evnt_ctgry_5 AS evnt_ctgry_5,
        hibernate_version AS version_id,
        act_name AS act_name,
        tc_company_id AS co,
        monitor_smry_id AS monitor_smry_id,
        orig_act_name AS orig_act_name,
        HAS_MONITOR_MSG AS HAS_MONITOR_MSG,
        SOURCE AS SOURCE,
        TOTAL_QTY AS TOTAL_QTY
   FROM labor_msg;

CREATE OR REPLACE FORCE VIEW "VIEW_E_MONITOR_SMRY" ("ROW_NBR", "RELEASED_WORK", "COMPLETED_WORK", "REMAINING_WORK", "ADJUSTED_WORK", "WHSE", "PLAN_ID", "WAVE_NBR", "WHSE_DATE", "MONITOR_ZONE", "JOB_FUNCTION", "EVNT_CTGRY_1", "EVNT_CTGRY_2", "EVNT_CTGRY_3", "EVNT_CTGRY_4", "EVNT_CTGRY_5", "LEVEL_1", "LEVEL_2", "LEVEL_3", "LEVEL_4", "LEVEL_5") AS
select row_number () over (order by whse) as row_nbr,
        sum (released_work) released_work,
        sum (completed_work) completed_work,
        sum (remaining_work) remaining_work,
        sum (adjusted_work) adjusted_work,
        whse,
        plan_id,
        wave_nbr,
        whse_date,
        monitor_zone,
        job_function,
        evnt_ctgry_1,
        evnt_ctgry_2,
        evnt_ctgry_3,
        evnt_ctgry_4,
        evnt_ctgry_5,
        level_1,
        level_2,
        level_3,
        level_4,
        level_5
   from (select released_work,
                0 as completed_work,
                remaining_work,
                adjusted_work,
                whse,
                plan_id,
                wave_nbr,
                whse_date,
                monitor_zone,
                job_function,
                evnt_ctgry_1,
                evnt_ctgry_2,
                evnt_ctgry_3,
                evnt_ctgry_4,
                evnt_ctgry_5,
                level_1,
                level_2,
                level_3,
                level_4,
                level_5
           from e_zone_tran_data
         union all
         select 0 as released_work,
                completed_work,
                0 as remaining_work,
                0 as adjusted_work,
                whse,
                plan_id,
                wave_nbr,
                whse_date,
                monitor_zone,
                job_function,
                evnt_ctgry_1,
                evnt_ctgry_2,
                evnt_ctgry_3,
                evnt_ctgry_4,
                evnt_ctgry_5,
                level_1,
                level_2,
                level_3,
                level_4,
                level_5
           from e_zone_msg_log
          where hdr_msg_id in
                   (select labor_msg_id
                      from labor_msg
                     where msg_stat_code < 90
                           and tran_data_els_tran_id is not null))
 group by whse,
        plan_id,
        wave_nbr,
        whse_date,
        monitor_zone,
        job_function,
        evnt_ctgry_1,
        evnt_ctgry_2,
        evnt_ctgry_3,
        evnt_ctgry_4,
        evnt_ctgry_5,
        level_1,
        level_2,
        level_3,
        level_4,
        level_5;

CREATE OR REPLACE FORCE VIEW "VIEW_E_PROC_LOCK_SCHED" ("PROC_ID", "SCHED_COUNT") AS
select proc_id, count (*) as sched_count
   from e_proc_lock_sched
 group by proc_id;

CREATE OR REPLACE FORCE VIEW "VIEW_E_QA_SMRY" ("QA_TOTAL_QTY", "QA_TOTAL_FAIL_QTY", "WHSE", "LOGIN_USER_ID", "QA_TOTALS_DATE", "NAME", "TEAM_ID", "TEAM_CODE") AS
select e_qa_smry.qa_total_qty,
      e_qa_smry.qa_total_fail_qty,
      e_qa_smry.whse,
      view_ucl_user.login_user_id,
      e_qa_smry.qa_totals_date,
      e_job_function.name,
      e_qa_smry.team_id,
      e_team_std.team_code
 from    (   (   e_qa_smry
              inner join
                 e_job_function
              on e_qa_smry.job_func_id = e_job_function.job_func_id)
          left outer join
             e_team_std
          on e_qa_smry.team_id = e_team_std.team_id)
      left outer join
         view_ucl_user
      on e_qa_smry.emp_id = view_ucl_user.emp_id;

CREATE OR REPLACE FORCE VIEW "VIEW_E_VHCL_WHSE" ("VHCL_TYPE_ID", "VHCL_TYPE", "DESCRIPTION", "WHSE", "WHSE_VHCL_TYPE_ID") AS
select vhcl_type_id,
      vhcl_type,
      description,
      '*' whse,
      0 whse_vhcl_type_id
 from e_vhcl
 union
 select e_vhcl.vhcl_type_id,
      e_vhcl.vhcl_type,
      e_vhcl_whse.description,
      whse,
      whse_vhcl_type_id
 from    e_vhcl_whse
      inner join
         e_vhcl
      on e_vhcl_whse.vhcl_type_id = e_vhcl.vhcl_type_id;

CREATE OR REPLACE FORCE VIEW "VIEW_FACILITY" ("FACILITY_ID", "TC_COMPANY_ID", "BUSINESS_PARTNER_ID", "FACILITY_NAME", "DESCRIPTION", "ADDRESS_1", "ADDRESS_2", "ADDRESS_3", "ADDRESS_KEY_1", "CITY", "STATE_PROV", "POSTAL_CODE", "COUNTY", "COUNTRY_CODE", "LONGITUDE", "LATITUDE", "EIN_NBR", "IATA_CODE", "GLOBAL_LOCN_NBR", "TAX_ID", "DUNS_NBR", "INBOUNDS_RS_AREA_ID", "OUTBOUND_RS_AREA_ID", "INBOUND_REGION_ID", "OUTBOUND_REGION_ID", "FACILITY_TZ", "DROP_INDICATOR", "HOOK_INDICATOR", "HANDLER", "IS_SHIP_APPT_REQD", "IS_RCV_APPT_REQD", "TRACK_ONTIME_INDICATOR", "ONTIME_PERF_METHOD", "LOAD_FACTOR_SIZE_VALUE", "MARK_FOR_DELETION", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "ALLOW_OVERLAPPING_APPS", "EARLY_TOLERANCE_POINT", "LATE_TOLERANCE_POINT", "EARLY_TOLERANCE_WINDOW", "LATE_TOLERANCE_WINDOW", "FACILITY_TYPE_BITS", "PP_HANDLING_TIME", "STOP_CONSTRAINT_BITS", "IS_DOCK_SCHED_FAC", "ASN_COMMUNICATION_METHOD", "PICK_TIME", "DISPATCH_INITIATION_TIME", "PAPERWORK_TIME", "VEHICLE_CHECK_TIME", "VISIT_GAP", "MIN_HANDLING_TIME", "IS_CREDIT_AVAILABLE", "BUSINESS_GROUP_ID_1", "BUSINESS_GROUP_ID_2", "IS_OPERATIONAL", "IS_MAINTENANCE_FACILITY", "DRIVER_CHECK_IN_TIME", "DISPATCH_DTTM", "DSP_DR_CONFIGURATION_ID", "DROP_HOOK_TIME", "LOADING_END_TIME", "DRIVER_DEBRIEF_TIME", "ACCOUNT_CODE_NUMBER", "LOAD_FACTOR_MOT_ID", "LOAD_UNLOAD_MOT_ID", "MIN_TRAIL_FILL", "TANDEM_DROP_HOOK_TIME", "STORE_TYPE", "DRIVEIN_TIME", "CHECKIN_TIME", "DRIVEOUT_TIME", "CHECKOUT_TIME", "CUTOFF_DTTM", "MAX_TRAILER_YARD_TIME", "HAND_OVER_TIME", "OVER_BOOK_PERCENTAGE", "RECOMMENDATION_CRITERIA", "NBR_OF_SLOTS_TO_SHOW", "RESTRICT_FLEET_ASMT_TIME", "GROUP_ID", "IS_VTBP", "AUTO_CREATE_SHIPMENT", "LOGO_IMAGE_PATH", "GEN_LAST_SHIPMENT_LEG", "DEF_DRIVER_TYPE_ID", "DEF_TRACTOR_ID", "DEF_EQUIPMENT_ID", "DRIVER_AVAIL_HRS", "WHSE", "OPEN_DATE", "CLOSE_DATE", "HOLD_DATE", "GRP", "CHAIN", "ZONE", "TERRITORY", "REGION", "DISTRICT",
"SHIP_MON", "SHIP_TUE", "SHIP_WED", "SHIP_THU", "SHIP_FRI", "SHIP_SAT", "SHIP_SU", "ACCEPT_IRREG", "WAVE_LABEL_TYPE", "PKG_SLIP_TYPE", "PRINT_CODE", "CARTON_CNT_TYPE", "SHIP_VIA", "RTE_NBR", "RTE_ATTR", "RTE_TO", "RTE_TYPE_1", "RTE_TYPE_2", "RTE_ZIP", "SPL_INSTR_CODE_1", "SPL_INSTR_CODE_2", "SPL_INSTR_CODE_3", "SPL_INSTR_CODE_4", "SPL_INSTR_CODE_5", "SPL_INSTR_CODE_6", "SPL_INSTR_CODE_7", "SPL_INSTR_CODE_8", "SPL_INSTR_CODE_9", "SPL_INSTR_CODE_10", "ASSIGN_MERCH_TYPE", "ASSIGN_MERCH_GROUP", "ASSIGN_STORE_DEPT", "CARTON_LABEL_TYPE", "CARTON_CUBNG_INDIC", "MAX_CTN", "MAX_PLT", "BUSN_UNIT_CODE", "USE_INBD_LPN_AS_OUTBD_LPN", "PRINT_COO", "PRINT_INV", "PRINT_SED", "PRINT_CANADIAN_CUST_INVC_FLAG", "PRINT_DOCK_RCPT_FLAG", "PRINT_NAFTA_COO_FLAG", "PRINT_OCEAN_BOL_FLAG", "PRINT_PKG_LIST_FLAG", "PRINT_SHPR_LTR_OF_INSTR_FLAG", "AUDIT_TRANSACTION", "AUDIT_PARTY_ID", "CAPTURE_OTHER_MA", "HIBERNATE_VERSION", "STORE_TYPE_GROUPING", "LOAD_RATE", "UNLOAD_RATE", "HANDLING_RATE", "LOAD_UNLOAD_SIZE_UOM_ID", "LOAD_FACTOR_SIZE_UOM_ID", "PARCEL_LENGTH_RATIO", "PARCEL_WIDTH_RATIO", "PARCEL_HEIGHT_RATIO", "METER_NUMBER", "DIRECT_DELIVERY_ALLOWED", "TRACK_EQUIP_ID_FLAG", "STAT_CODE", "FACILITY_ALIAS_ID") AS
SELECT f1.facility_id,
      f1.tc_company_id,
      f1.business_partner_id,
      f2.facility_name,
      f1.description,
      f1.address_1,
      f1.address_2,
      f1.address_3,
      f1.address_key_1,
      f1.city,
      f1.state_prov,
      f1.postal_code,
      f1.county,
      f1.country_code,
      f1.longitude,
      f1.latitude,
      f1.ein_nbr,
      f1.iata_code,
      f1.global_locn_nbr,
      f1.tax_id,
      f1.duns_nbr,
      f1.inbounds_rs_area_id,
      f1.outbound_rs_area_id,
      f1.inbound_region_id,
      f1.outbound_region_id,
      f1.facility_tz,
      f1.drop_indicator,
      f1.hook_indicator,
      f1.handler,
      f1.is_ship_appt_reqd,
      f1.is_rcv_appt_reqd,
      f1.track_ontime_indicator,
      f1.ontime_perf_method,
      f1.load_factor_size_value,
      f1.mark_for_deletion,
      f1.created_source_type,
      f1.created_source,
      f1.created_dttm,
      f1.last_updated_source_type,
      f1.last_updated_source,
      f1.last_updated_dttm,
      f1.allow_overlapping_apps,
      f1.early_tolerance_point,
      f1.late_tolerance_point,
      f1.early_tolerance_window,
      f1.late_tolerance_window,
      --f1.must_be_first_stop,
      f1.facility_type_bits,
      f1.pp_handling_time,
      f1.stop_constraint_bits,
      f1.is_dock_sched_fac,
      f1.asn_communication_method,
      f1.pick_time,
      f1.dispatch_initiation_time,
      f1.paperwork_time,
      f1.vehicle_check_time,
      f1.visit_gap,
      f1.min_handling_time,
      f1.is_credit_available,
      f1.business_group_id_1,
      f1.business_group_id_2,
      f1.is_operational,
      f1.is_maintenance_facility,
      f1.driver_check_in_time,
      f1.dispatch_dttm,
      f1.dsp_dr_configuration_id,
      f1.drop_hook_time,
      f1.loading_end_time,
      f1.driver_debrief_time,
      f1.account_code_number,
      f1.load_factor_mot_id,
      f1.load_unload_mot_id,
      f1.min_trail_fill,
      f1.tandem_drop_hook_time,
      f1.store_type,
      f1.drivein_time,
      f1.checkin_time,
      f1.driveout_time,
      f1.checkout_time,
      f1.cutoff_dttm,
      f1.max_trailer_yard_time,
      f1.hand_over_time, --f1.dflt_ib_carrier_id,f1.dflt_ib_mot_id, f1.dflt_ib_service_level_id,
      f1.over_book_percentage,
      f1.recommendation_criteria,
      f1.nbr_of_slots_to_show,
      f1.restrict_fleet_asmt_time,
      f1.GROUP_ID,
      f1.is_vtbp,
      f1.auto_create_shipment,
      f1.logo_image_path,
      f1.gen_last_shipment_leg,
      f1.def_driver_type_id,
      f1.def_tractor_id,
      f1.def_equipment_id,
      f1.driver_avail_hrs,
      f1.whse,
      --f1.STORE_NBR,
      f1.open_date,
      f1.close_date,
      f1.hold_date,
      f1.grp,
      f1.chain,
      f1.ZONE,
      f1.territory,
      f1.region,
      f1.district,
      f1.ship_mon,
      f1.ship_tue,
      f1.ship_wed,
      f1.ship_thu,
      f1.ship_fri,
      f1.ship_sat,
      f1.ship_su,
      f1.accept_irreg,
      f1.wave_label_type,
      f1.pkg_slip_type,
      f1.print_code,
      f1.carton_cnt_type,
      f1.ship_via,
      f1.rte_nbr,
      f1.rte_attr,
      f1.rte_to,
      f1.rte_type_1,
      f1.rte_type_2,
      f1.rte_zip,
      f1.spl_instr_code_1,
      f1.spl_instr_code_2,
      f1.spl_instr_code_3,
      f1.spl_instr_code_4,
      f1.spl_instr_code_5,
      f1.spl_instr_code_6,
      f1.spl_instr_code_7,
      f1.spl_instr_code_8,
      f1.spl_instr_code_9,
      f1.spl_instr_code_10,
      f1.assign_merch_type,
      f1.assign_merch_group,
      f1.assign_store_dept,
      f1.carton_label_type,
      f1.carton_cubng_indic,
      f1.max_ctn,
      f1.max_plt,
      f1.busn_unit_code,
      f1.use_inbd_lpn_as_outbd_lpn,
      f1.print_coo,
      f1.print_inv,
      f1.print_sed,
      f1.print_canadian_cust_invc_flag,
      f1.print_dock_rcpt_flag,
      f1.print_nafta_coo_flag,
      f1.print_ocean_bol_flag,
      f1.print_pkg_list_flag,
      f1.print_shpr_ltr_of_instr_flag,                --f1.whse_region_id,
      f1.audit_transaction,
      f1.audit_party_id,
      f1.capture_other_ma,
      f1.hibernate_version,
      f1.store_type_grouping,
      f1.load_rate,
      f1.unload_rate,
      f1.handling_rate,
      f1.load_unload_size_uom_id,
      f1.load_factor_size_uom_id,
      f1.parcel_length_ratio,
      f1.parcel_width_ratio,
      f1.parcel_height_ratio,
      f1.meter_number,
      f1.direct_delivery_allowed,
      f1.track_equip_id_flag,
      f1.stat_code,
      f2.facility_alias_id
 FROM facility f1, facility_alias f2
WHERE f1.facility_id = f2.facility_id AND f2.is_primary = 1;

CREATE OR REPLACE FORCE VIEW "VIEW_FACTOR_CRITERIA" ("FCT_ID", "COUNT_FACTOR_CRITERIA") AS
select fct_id, count (distinct crit_id) as count_factor_criteria
   from e_fct_crit
 group by fct_id;

CREATE OR REPLACE FORCE VIEW "VIEW_FCT_CRIT_ALL" ("FCT_ID", "CRIT_VAL_ID", "CRIT_ID", "TIME_ALLOW", "ADJ_PCNT", "STATUS", "MINADJSEL") AS
select fct_id,
      crit_val_id,
      crit_id,
      time_allow,
      adj_pcnt,
      1 as status,
      get_min_adj (time_allow, adj_pcnt) as minadjsel
 from e_fct_crit
 union
 select e_fct.fct_id as fct_id,
      e_crit_val.crit_val_id as crit_val_id,
      e_crit_val.crit_id as crit_id,
      0 as time_allow,
      0 as adj_pcnt,
      0 as status,
      'M' as minadjsel
 from e_fct, e_crit_val, labor_criteria
where crit_val_id not in
         (select crit_val_id
            from e_fct_crit
           where e_fct_crit.fct_id = e_fct.fct_id
                 and e_fct_crit.crit_id = labor_criteria.crit_id)
      and e_crit_val.crit_id = labor_criteria.crit_id;

CREATE OR REPLACE FORCE VIEW "VIEW_FCT_CRIT_VAL" ("FCT_ID", "CRIT_ID", "COUNT_CRITERIA_VAL", "FACTOR_CRITERIA_VALUES") AS
select fct_id,
        crit_id,
        count (distinct crit_val_id) as count_criteria_val,
        fn_fct_crit_val_str (fct_id, crit_id) as factor_criteria_values
   from e_fct_crit
 group by fct_id, crit_id;

CREATE OR REPLACE FORCE VIEW "VIEW_HDR_CRIT" ("LABOR_MSG_ID", "LABOR_MSG_DTL_ID", "TRAN_NBR", "TRAN_SEQ_NBR", "CRIT_TYPE", "CRIT_VAL", "WHSE") AS
select d.labor_msg_id,
      d.labor_msg_dtl_id,
      d.tran_nbr,
      d.tran_seq_nbr,
      hc.crit_type,
      hc.crit_val,
      hc.whse
 from    labor_msg_dtl d
      inner join
         labor_msg_crit hc
      on d.labor_msg_id = hc.labor_msg_id and d.tran_nbr = hc.tran_nbr;

CREATE OR REPLACE FORCE VIEW "VIEW_HIST_ACT_ELM_UOM_TIME" ("HIST_SEQ_NBR", "JOB_ID", "ACT_ID", "ELM_ID", "ELEMENT_NAME", "MSRMNT_ID", "MSRMNT_NAME", "TIME_ALLOW", "ACT_NAME", "WHSE", "CREATE_DATE_TIME", "SEQ_NBR") AS
select aeh.hist_seq_nbr,
      aeh.job_id,
      aeh.act_id,
      aeh.elm_id,
      elm.name as element_name,
      elm.msrmnt_id,
      msrmnt.name as msrmnt_name,
      aeh.time_allow,
      act.act_name,
      act.whse,
      aeh.create_date_time,
      aeh.seq_nbr
 from e_act_elm_hist aeh
      inner join e_elm_hist elm
         on     aeh.elm_id = elm.elm_id
            and aeh.hist_seq_nbr = elm.hist_seq_nbr
            and aeh.job_id = elm.job_id
      inner join e_msrmnt msrmnt
         on elm.msrmnt_id = msrmnt.msrmnt_id
      inner join e_act_hist act
         on     aeh.act_id = act.act_id
            and aeh.hist_seq_nbr = act.hist_seq_nbr
            and aeh.job_id = act.job_id;

CREATE OR REPLACE FORCE VIEW "VIEW_INC_PAY" ("JOB_ID", "EXTRACTION_DATE_TIME", "EMP_INC_PAY_ID", "INC_CODE_ID", "EMP_ID", "JOB_FUNC_ID", "LABOR_TYPE_ID", "SHIFT_ID", "SPVSR_EMP_ID", "DEPT_ID", "WHSE_DATE", "PAY_DATE", "PAY_HOURS", "INC_PAY", "TOTAL_INC_PAY", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "WHSE", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2", "LAST_MOD_BY", "ADJ_APRV_USER_ID", "ADJ_REASON_CODE", "ADJ_COMMENT", "MOD_LOGIN_USER_ID") AS
select job_id,
      extraction_date_time,
      emp_inc_pay_id,
      inc_code_id,
      emp_id,
      job_func_id,
      labor_type_id,
      shift_id,
      spvsr_emp_id,
      dept_id,
      whse_date,
      pay_date,
      coalesce (pay_hours, 0) pay_hours,
      inc_pay,
      coalesce (
         (case
             when adj_total_inc_pay is not null then adj_total_inc_pay
             else total_inc_pay
          end),
         0)
         total_inc_pay,
      create_date_time,
      mod_date_time,
      user_id,
      whse,
      misc_txt_1,
      misc_txt_2,
      misc_num_1,
      misc_num_2,
      last_mod_by,
      adj_aprv_user_id,
      adj_reason_code,
      adj_comment,
      mod_login_user_id
 from e_interm_emp_inc_pay;

CREATE OR REPLACE FORCE VIEW "VIEW_JF_LEVEL_LIST" ("LEVELNAME", "NAME", "WHSE", "CODE_ID") AS
select "LEVELNAME",
      "NAME",
      "WHSE",
      "CODE_ID"
 from (select 'Job Function' as "LEVELNAME",
              name,
              whse,
              null as "CODE_ID"
         from e_job_function
       union
       select 'Level1' as "LEVELNAME",
              code_desc,
              whse,
              code_id as "CODE_ID"
         from whse_sys_code
        where code_type = '111'
       union
       select 'Level2' as "LEVELNAME",
              code_desc,
              whse,
              code_id as "CODE_ID"
         from whse_sys_code
        where code_type = '112'
       union
       select 'Level3' as "LEVELNAME",
              code_desc,
              whse,
              code_id as "CODE_ID"
         from whse_sys_code
        where code_type = '113'
       union
       select 'Level4' as "LEVELNAME",
              code_desc,
              whse,
              code_id as "CODE_ID"
         from whse_sys_code
        where code_type = '114'
       union
       select 'Level5' as "LEVELNAME",
              code_desc,
              whse,
              code_id as "CODE_ID"
         from whse_sys_code
        where code_type = '115') t;

CREATE OR REPLACE FORCE VIEW "VIEW_JOB_FUNCTION" ("JOB_FUNC_ID", "ACT_TYPE", "COUNT_OF_ACTIVITY") AS
select job_func_id,
        lact.act_type,
        count (distinct act_id) as count_of_activity
   from    e_act
        inner join
           labor_activity lact
        on e_act.labor_activity_id = lact.labor_activity_id
 group by job_func_id, act_type
 having job_func_id in (select job_func_id from e_job_function)
        and act_type < '20';

CREATE OR REPLACE FORCE VIEW "VIEW_JOB_FUNCTION_TRAIN" ("JOB_FUNC_ID", "TRAIN_PERIOD", "RETRAIN_PERIOD", "OS_ONLY", "RETRAIN_REQ_DURATION", "WHSE", "PERF_GOAL_IND", "TRAINING_REQD") AS
select job_func_id,
      train_period,
      retrain_period,
      os_only,
      retrain_req_duration,
      whse,
      perf_goal_ind,
      training_reqd
 from e_job_function;

CREATE OR REPLACE FORCE VIEW "VIEW_LABOR" ("ROW_NBR", "LABOR_ACTIVITY_ID", "NAME", "DESCRIPTION", "ACT_TYPE", "AIL_ACT", "PROMPT_LOCN", "DISPL_EPP", "INCLD_TRVL", "CRIT_RULE_TYPE", "ACT_ID", "JOB_FUNC_ID", "WHSE", "LABOR_TYPE_ID", "JF_NAME", "LABOR_TYPE", "MONITOR_UOM", "OVERRIDE_PROC_ZONE_ID", "PROC_ZONE_LOCN_IND", "LM_MAN_EVNT_REQ_APRV", "LM_KIOSK_REQ_APRV", "WM_REQ_APRV", "VHCL_TYPE_ID", "MOD_DATE_TIME", "USER_ID", "CONFIGURED", "ACT_ELM_COUNT", "ACT_ELM_PFD_COUNT") AS
select rownum as row_nbr,
      l.labor_activity_id,
      l.name,
      l.description,
      l.act_type,
      l.ail_act,
      l.prompt_locn,
      l.displ_epp,
      l.incld_trvl,
      l.crit_rule_type,
      b.act_id,
      b.job_func_id,
      b.whse,
      b.labor_type_id,
      b.jf_name,
      b.labor_type,
      b.monitor_uom,
      b.override_proc_zone_id,
      b.proc_zone_locn_ind,
      b.lm_man_evnt_req_aprv,
      b.lm_kiosk_req_aprv,
      b.wm_req_aprv,
      b.vhcl_type_id,
      b.mod_date_time,
      b.user_id,
      decode (b.whse, null, 'No', 'Yes') as configured,
      b.act_elm_count,
      b.act_elm_pfd_count
 from    labor_activity l
      left outer join
         (select a.act_id,
                 a.job_func_id,
                 a.whse,
                 a.labor_type_id,
                 a.labor_activity_id,
                 jf.name as jf_name,
                 lc.description as labor_type,
                 a.monitor_uom,
                 a.override_proc_zone_id,
                 a.proc_zone_locn_ind,
                 a.lm_man_evnt_req_aprv,
                 a.lm_kiosk_req_aprv,
                 a.wm_req_aprv,
                 a.vhcl_type_id,
                 a.mod_date_time,
                 a.user_id,
                 (select coalesce (count (*), 0)
                    from e_act_elm ae
                   where ae.act_id = a.act_id)
                    as act_elm_count,
                 (select coalesce (count (*), 0)
                    from e_act_elm_pfd aep
                   where aep.act_id = a.act_id)
                    as act_elm_pfd_count
            from e_act a, e_job_function jf, e_labor_type_code lc
           where a.job_func_id = jf.job_func_id
                 and a.labor_type_id = lc.labor_type_id) b
      on l.labor_activity_id = b.labor_activity_id;

CREATE OR REPLACE FORCE VIEW "VIEW_LABOR_ABC" ("THRUPUT_ID", "MSRMNT_ID", "MSRMNT_CODE", "QTY", "TOTAL_COST", "NAME", "WHSE", "PERF_SMRY_TRAN_ID") AS
select tsmry.thruput_id,
      mnt.msrmnt_id,
      mnt.msrmnt_code,
      tdtl.qty,
      round ( ( (tsmry.total_time / 60) * cp.labor_cost_rate), 2)
         as total_cost,
      mnt.name,
      tsmry.whse,
      tsmry.perf_smry_tran_id
 from e_msrmnt mnt
      inner join (   e_thruput_smry tsmry
                  inner join
                     e_thruput_smry_dtl tdtl
                  on (tsmry.thruput_id = tdtl.thruput_id)
                     and (tsmry.whse = tdtl.whse))
         on mnt.msrmnt_id = tdtl.msrmnt_id
      inner join e_consol_perf_smry cp
         on tsmry.perf_smry_tran_id = cp.perf_smry_tran_id;

CREATE OR REPLACE FORCE VIEW "VIEW_LABOR_CRIT_MSG_LOG" ("LABOR_MSG_ID", "LABOR_MSG_DTL_ID", "TRAN_NBR", "WHSE", "CRIT_SEQ_NBR", "CRIT_TYPE", "CRIT_VAL") AS
select labor_msg_id,
      0 as labor_msg_dtl_id,
      tran_nbr,
      whse,
      crit_seq_nbr,
      crit_type,
      crit_val
 from labor_msg_crit
 union all
 select (select labor_msg_id
         from labor_msg_dtl dtl
        where dtl.labor_msg_dtl_id = dtlcrit.labor_msg_dtl_id)
         as labor_msg_id,
      labor_msg_dtl_id,
      tran_nbr,
      whse,
      crit_seq_nbr,
      crit_type,
      crit_val
 from labor_msg_dtl_crit dtlcrit;

CREATE OR REPLACE FORCE VIEW "VIEW_LABOR_CRIT_TRAN" ("LABOR_TRAN_ID", "LABOR_TRAN_DTL_ID", "TRAN_NBR", "WHSE", "CRIT_SEQ_NBR", "CRIT_TYPE", "CRIT_VAL") AS
select labor_tran_id,
      0 as labor_tran_dtl_id,
      tran_nbr,
      whse,
      crit_seq_nbr,
      crit_type,
      crit_val
 from labor_tran_crit
 union all
 select (select labor_tran_id
         from labor_tran_dtl dtl
        where dtl.labor_tran_dtl_id = dtlcrit.labor_tran_dtl_id)
         as labor_tran_id,
      labor_tran_dtl_id,
      tran_nbr,
      whse,
      crit_seq_nbr,
      crit_type,
      crit_val
 from labor_tran_dtl_crit dtlcrit;

CREATE OR REPLACE FORCE VIEW "VIEW_LABOR_TRAN" ("LABOR_TRAN_ID", "STATUS", "WHSE", "TRAN_NBR", "ACT_NAME", "LOGIN_USER_ID", "SCHED_START_TIME", "SCHED_START_DATE", "VHCL_TYPE", "REF_CODE", "REF_NBR", "CO", "DIV", "SHIFT", "USER_DEF_VAL_1", "USER_DEF_VAL_2", "MOD_USER_ID", "RESEND_TRAN", "REQ_SAM_REPLY", "PRIORITY", "ENGINE_CODE", "TEAM_STD_GRP", "MISC", "MISC_2", "USER_ID", "CREATE_DATE_TIME", "LAST_MOD_BY", "MOD_DATE_TIME", "ACTUAL_END_TIME", "ACTUAL_END_DATE", "PLAN_ID", "WAVE_NBR", "TRANS_TYPE", "TRANS_VALUE", "ENGINE_GROUP", "COMPLETED_WORK", "WHSE_DATE", "JOB_FUNCTION", "LEVEL_1", "LEVEL_2", "LEVEL_3", "LEVEL_4", "LEVEL_5", "CARTON_NBR", "TASK_NBR", "CASE_NBR", "MISC_TXT_1", "MISC_TXT_2", "MISC_NUM_1", "MISC_NUM_2", "EVNT_CTGRY_1", "EVNT_CTGRY_2", "EVNT_CTGRY_3", "EVNT_CTGRY_4", "EVNT_CTGRY_5", "VERSION_ID", "DEPT", "MSG_STAT_CODE") AS
select labor_tran_id,
      status,
      whse,
      tran_nbr,
      act_name,
      login_user_id,
      sched_start_time,
      sched_start_date,
      vhcl_type,
      ref_code,
      ref_nbr,
      tc_company_id,
      div,
      shift,
      user_def_val_1,
      user_def_val_2,
      mod_user_id,
      resend_tran,
      req_sam_reply,
      priority,
      engine_code,
      team_std_grp,
      misc,
      misc_2,
      created_source,
      created_dttm,
      last_updated_source,
      last_updated_dttm,
      actual_end_time,
      actual_end_date,
      plan_id,
      wave_nbr,
      trans_type,
      trans_value,
      engine_group,
      completed_work,
      whse_date,
      job_function,
      level_1,
      level_2,
      level_3,
      level_4,
      level_5,
      carton_nbr,
      task_nbr,
      case_nbr,
      misc_txt_1,
      misc_txt_2,
      misc_num_1,
      misc_num_2,
      evnt_ctgry_1,
      evnt_ctgry_2,
      evnt_ctgry_3,
      evnt_ctgry_4,
      evnt_ctgry_5,
      hibernate_version,
      dept,
      msg_stat_code
 from labor_tran;

CREATE OR REPLACE FORCE VIEW "VIEW_LABOR_TRAN_CRIT" ("LABOR_TRAN_ID", "LABOR_TRAN_DTL_ID", "TRAN_NBR", "TRAN_SEQ_NBR", "CRIT_TYPE", "CRIT_VAL", "WHSE") AS
select d.labor_tran_id,
      d.labor_tran_dtl_id,
      d.tran_nbr,
      d.tran_seq_nbr,
      hc.crit_type,
      hc.crit_val,
      hc.whse
 from    labor_tran_dtl d
      inner join
         labor_tran_crit hc
      on d.labor_tran_id = hc.labor_tran_id and d.tran_nbr = hc.tran_nbr;

CREATE OR REPLACE FORCE VIEW "VIEW_LABOR_TRAN_DTL" ("LABOR_TRAN_DTL_ID", "LABOR_TRAN_ID", "TRAN_NBR", "TRAN_SEQ_NBR", "SKU_SEQ_NBR", "SKU_HNDL_ATTR", "QTY_PER_GRAB", "LOCN_CLASS", "QTY", "CARTON_ID", "CASE_ID", "PALLET_ID", "LOCN_SLOT_TYPE", "LOCN_X_COORD", "LOCN_Y_COORD", "LOCN_Z_COORD", "LOCN_TRAV_AISLE", "LOCN_TRAV_ZONE", "SKU_BOX_QTY", "SKU_SUB_PACK_QTY", "SKU_PACK_QTY", "SKU_CASE_QTY", "SKU_TIER_QTY", "SKU_PALLET_QTY", "MISC", "CO", "DIV", "DSP_LOCN", "SKU_BRCD", "WEIGHT", "VOLUME", "CRIT_DIM1", "CRIT_DIM2", "CRIT_DIM3", "START_DATE_TIME", "HANDLING_UOM", "SEASON", "SEASON_YR", "STYLE", "STYLE_SFX", "COLOR", "COLOR_SFX", "SEC_DIM", "QUAL", "SIZE_RNGE_CODE", "SIZE_REL_POSN_IN_TABLE", "MISC_NUM_1", "MISC_NUM_2", "MISC_2", "LOADED", "PUTAWAY_ZONE", "PICK_DETERMINATION_ZONE", "WORK_GROUP", "WORK_AREA", "PULL_ZONE", "ASSIGNMENT_ZONE", "USER_ID", "CREATE_DATE_TIME", "MOD_DATE_TIME", "VERSION_ID", "ITEM_NAME") AS
SELECT LABOR_TRAN_DTL_ID,
        LABOR_TRAN_ID,
        TRAN_NBR,
        TRAN_SEQ_NBR,
        SEQ_NBR,
        HNDL_ATTR,
        QTY_PER_GRAB,
        LOCN_CLASS,
        QTY,
        TC_OLPN_ID,
        TC_ILPN_ID,
        PALLET_ID,
        LOCN_SLOT_TYPE,
        LOCN_X_COORD,
        LOCN_Y_COORD,
        LOCN_Z_COORD,
        LOCN_TRAV_AISLE,
        LOCN_TRAV_ZONE,
        BOX_QTY,
        INNERPACK_QTY,
        PACK_QTY,
        CASE_QTY,
        TIER_QTY,
        PALLET_QTY,
        MISC,
        TC_COMPANY_ID,
        DIV,
        DSP_LOCN,
        ITEM_BAR_CODE,
        WEIGHT,
        VOLUME,
        CRIT_DIM1,
        CRIT_DIM2,
        CRIT_DIM3,
        START_DATE_TIME,
        HANDLING_UOM,
        SEASON,
        SEASON_YR,
        STYLE,
        STYLE_SFX,
        COLOR,
        COLOR_SFX,
        SEC_DIM,
        QUAL,
        SIZE_RNGE_CODE,
        SIZE_REL_POSN_IN_TABLE,
        MISC_NUM_1,
        MISC_NUM_2,
        MISC_2,
        LOADED,
        PUTAWAY_ZONE,
        PICK_DETERMINATION_ZONE,
        WORK_GROUP,
        WORK_AREA,
        PULL_ZONE,
        CREATED_SOURCE_TYPE,
        CREATED_SOURCE,
        CREATED_DTTM,
        LAST_UPDATED_DTTM,
        HIBERNATE_VERSION,
        ITEM_NAME
   FROM LABOR_TRAN_DTL;

CREATE OR REPLACE FORCE VIEW "VIEW_LABOR_TRAN_DTL_CRIT" ("LABOR_TRAN_ID", "LABOR_TRAN_DTL_ID", "TRAN_NBR", "TRAN_SEQ_NBR", "CRIT_TYPE", "CRIT_VAL", "WHSE") AS
select d.labor_tran_id,
      d.labor_tran_dtl_id,
      d.tran_nbr,
      d.tran_seq_nbr,
      dc.crit_type,
      dc.crit_val,
      dc.whse
 from    labor_tran_dtl d
      inner join
         labor_tran_dtl_crit dc
      on d.labor_tran_dtl_id = dc.labor_tran_dtl_id
         and d.tran_nbr = dc.tran_nbr;

CREATE OR REPLACE FORCE VIEW "VIEW_LOCATION" ("LOCN_HDR_ID", "WHSE", "DSP_LOCN", "LOCN_CLASS", "TRAVEL_AISLE", "TRAVEL_ZONE", "X_COORD", "Y_COORD", "Z_COORD", "SLOT_TYPE", "AREA", "ZONE", "AISLE", "BAY", "LVL", "POSN", "LOCN_PICK_SEQ", "PUTWY_ZONE", "PICK_DETRM_ZONE", "WORK_GRP", "WORK_AREA", "PULL_ZONE", "ASSIGN_ZONE", "MOD_DATE_TIME", "CREATE_DATE_TIME", "USER_ID", "WM_VERSION_ID") AS
select l."LOCN_HDR_ID",
      l."WHSE",
      l."DSP_LOCN",
      l."LOCN_CLASS",
      l."TRAVEL_AISLE",
      l."TRAVEL_ZONE",
      l."X_COORD",
      l."Y_COORD",
      l."Z_COORD",
      l."SLOT_TYPE",
      l."AREA",
      l."ZONE",
      l."AISLE",
      l."BAY",
      l."LVL",
      l."POSN",
      l."LOCN_PICK_SEQ",
      l."PUTWY_ZONE",
      l."PICK_DETRM_ZONE",
      l."WORK_GRP",
      l."WORK_AREA",
      l."PULL_ZONE",
      null as "ASSIGN_ZONE",
      l."MOD_DATE_TIME",
      l."CREATE_DATE_TIME",
      l."USER_ID",
      l."WM_VERSION_ID"
 from locn_hdr l
where dsp_locn is not null and locn_class is not null
 union all
 select l."LOCN_HDR_ID",
      l."WHSE",
      l."DSP_LOCN",
      '9' as "LOCN_CLASS",
      p."REPL_TRAVEL_AISLE" as "TRAVEL_AISLE",
      p."REPL_TRAVEL_ZONE" as "TRAVEL_ZONE",
      p."REPL_X_COORD" as "X_COORD",
      p."REPL_Y_COORD" as "Y_COORD",
      p."REPL_Z_COORD" as "Z_COORD",
      l."SLOT_TYPE",
      l."AREA",
      l."ZONE",
      l."AISLE",
      l."BAY",
      l."LVL",
      l."POSN",
      l."LOCN_PICK_SEQ",
      l."PUTWY_ZONE",
      l."PICK_DETRM_ZONE",
      l."WORK_GRP",
      l."WORK_AREA",
      l."PULL_ZONE",
      p."PICK_LOCN_ASSIGN_ZONE" as "ASSIGN_ZONE",
      l."MOD_DATE_TIME",
      l."CREATE_DATE_TIME",
      l."USER_ID",
      l."WM_VERSION_ID"
 from locn_hdr l, pick_locn_hdr p
where     l.locn_hdr_id = p.locn_hdr_id
      and dsp_locn is not null
      and locn_class is not null
 order by dsp_locn asc;

CREATE OR REPLACE FORCE VIEW "VIEW_LP_LABOR_EST_PKT_ASN_DIST" ("ROW_NBR", "PLAN_ID", "NAME", "WHSE", "TRANSACTION_TYPE", "PLAN_WORKFLOW_ID", "PLAN_RULE_ID", "TRANSACTION_VALUE", "STATUS", "MISC_1", "LABOR_EST") AS
select rownum as row_nbr,
      tmp.plan_id,
      tmp.name,
      tmp.whse,
      tmp.transaction_type,
      tmp.plan_workflow_id,
      tmp.plan_rule_id,
      tmp.transaction_value,
      tmp.status,
      tmp.misc_1,
      tmp.labor_est
 from (  select eph.plan_id,
                eph.name,
                eph.whse,
                epd.transaction_type,
                epd.plan_workflow_id,
                epd.plan_rule_id,
                epth.transaction_value,
                epth.status,
                epth.misc_1,
                round (nvl (sum (epta.actual_labor_est / 60), 0), 2)
                   as labor_est
           from e_plan_hdr eph,
                e_plan_dtl epd,
                e_plan_transaction_hdr epth,
                e_plan_transaction_act epta
          where eph.plan_id = epd.plan_id
                and (epd.plan_id = epth.plan_id
                     and epd.transaction_seq = epth.transaction_seq)
                and (    epth.plan_id = epta.plan_id(+)
                     and epth.transaction_seq = epta.transaction_seq(+)
                     and epth.transaction_value = epta.transaction_value(+))
       group by eph.plan_id,
                eph.name,
                eph.whse,
                epd.transaction_type,
                epd.plan_workflow_id,
                epd.plan_rule_id,
                epth.transaction_value,
                epth.status,
                epth.misc_1) tmp;

CREATE OR REPLACE FORCE VIEW "VIEW_LP_LEVELS" ("PLAN_ID", "PLAN_GROUP", "ZONE", "PRORATED_TIME", "FULL_TIME_EMP", "TEMP_EMP", "FULL_TIME_EMP_HRS", "TEMP_EMP_HRS", "WHSE") AS
select plan_id,
        plan_group,
        zone,
        sum (prorated_time) as "Prorated_Time",
        full_time_emp,
        temp_emp,
        full_time_emp_hrs,
        temp_emp_hrs,
        whse
   from (select epta.plan_id,
                case
                   when wfp.plan_group = 'JOBFUNCTION'
                   then
                      epta.plan_group
                   when wfp.plan_group = 'LEVEL_1'
                   then
                      nvl (ejf.level_1, 'IGNORE_RECORD')
                   when wfp.plan_group = 'LEVEL_2'
                   then
                      nvl (ejf.level_2, 'IGNORE_RECORD')
                   when wfp.plan_group = 'LEVEL_3'
                   then
                      nvl (ejf.level_3, 'IGNORE_RECORD')
                   when wfp.plan_group = 'LEVEL_4'
                   then
                      nvl (ejf.level_4, 'IGNORE_RECORD')
                   when wfp.plan_group = 'LEVEL_5'
                   then
                      nvl (ejf.level_5, 'IGNORE_RECORD')
                end
                   plan_group,
                eptaz.zone,
                case
                   when eptaz.zone is null then epta.actual_labor_est
                   else eptaz.prorated_time
                end
                   prorated_time,
                nvl (eresults.full_time_emp, 0) as full_time_emp,
                nvl (eresults.temp_emp, 0) as temp_emp,
                nvl (eresults.full_time_emp_hrs, 0) as full_time_emp_hrs,
                nvl (eresults.temp_emp_hrs, 0) as temp_emp_hrs,
                epta.whse
           from e_plan_transaction_act epta
                left outer join e_plan_transaction_act_zone eptaz
                   on (    (epta.plan_id = eptaz.plan_id)
                       and (epta.plan_group = eptaz.plan_group)
                       and (epta.whse = eptaz.whse))
                left outer join e_plan_results eresults
                   on eptaz.plan_group = eresults.plan_group
                join facility f
                   on f.whse = epta.whse
                join whse_labor_parameters wfp
                   on (wfp.facility_id = f.facility_id)
                join e_job_function ejf
                   on (epta.plan_group = ejf.name and epta.whse = ejf.whse)) source
  where source.plan_group <> 'IGNORE_RECORD'
 group by plan_id,
        plan_group,
        zone,
        full_time_emp,
        temp_emp,
        full_time_emp_hrs,
        temp_emp_hrs,
        whse;

CREATE OR REPLACE FORCE VIEW "VIEW_LRF_EVENTS" ("EVENT_ID", "LRF_EVENT_ID", "LRF_EVENT_NAME", "GROUP_ID", "EVENT_START_DTTM", "EVENT_EXP_DATE", "OM_CATEGORY", "EVENT_TIMESTAMP", "EVENT_FREQ_IN_DAYS", "EVENT_FREQ_PER_DAY", "EXECUTED_DTTM", "IS_EXECUTED", "EVENT_OBJECTS", "EVENT_TYPE", "EVENT_CNT", "EVENT_FREQ_IN_DAY_OF_MONTH", "FREQ_TYPE", "FREQ_VAL_1", "FREQ_VAL_2", "SCHEDULED_DTTM", "SCHEDULED_TIME", "CREATED_DTTM", "MODIFIED_DTTM", "CREATED_BY", "MODIFIED_BY", "PREVIOUS_SCHEDULED_DTTM", "EXCLUDE_WEEKEND") AS
(select om.event_id,
       lrf_event_id,
       lrf_event_name,
       group_id,
       event_start_dttm,
       event_exp_date,
       om_category,
       event_timestamp,
       event_freq_in_days,
       event_freq_per_day,
       executed_dttm,
       is_executed,
       event_objects,
       event_type,
       event_cnt,
       event_freq_in_day_of_month,
       freq_type,
       freq_val_1,
       freq_val_2,
       scheduled_dttm,
       scheduled_time,
       lrf.created_dttm,
       modified_dttm,
       created_by,
       modified_by,
       previous_scheduled_dttm,
       exclude_weekend
  from lrf_event lrf, lrf_sched_event_report om
 where lrf.event_id = om.event_id);

CREATE OR REPLACE FORCE VIEW "VIEW_MILESTONE_TEAM_EMP" ("ROW_NBR", "MILESTONE_ID", "MILESTONE_CODE", "M_DESCRIPTION", "MILESTONE_DATE_TIME", "EMP_ID", "LOGIN_USER_ID", "TEAM_ID", "TEAM_CODE", "WHSE") AS
select rownum as row_nbr,
      e.milestone_id,
      e.milestone_code,
      e.m_description,
      e.milestone_date_time,
      e.emp_id,
      e.login_user_id,
      e.team_id,
      e.team_code,
      e.whse
 from (select e_milestone.milestone_id as milestone_id,
              e_milestone.milestone_code as milestone_code,
              e_milestone.description as m_description,
              milestone_date_time as milestone_date_time,
              ucl_user.ucl_user_id as emp_id,
              ucl_user.user_name as login_user_id,
              null as team_id,
              null as team_code,
              e_milestone_emp.whse as whse
         from (   (   ucl_user
                   inner join
                      e_milestone_emp
                   on ucl_user.ucl_user_id = e_milestone_emp.emp_id)
               left outer join
                  e_milestone
               on e_milestone.milestone_id = e_milestone_emp.milestone_id)
       union
       select e_milestone.milestone_id as milestone_id,
              e_milestone.milestone_code as milestone_code,
              e_milestone.description as m_description,
              milestone_date_time as milestone_date_time,
              null as emp_id,
              null as login_user_id,
              e_team_std.team_id as team_id,
              e_team_std.team_code as team_code,
              e_milestone_team.whse as whse
         from (   (   e_team_std
                   inner join
                      e_milestone_team
                   on e_team_std.team_id = e_milestone_team.team_id)
               left outer join
                  e_milestone
               on e_milestone.milestone_id =
                     e_milestone_team.milestone_id)) e;

CREATE OR REPLACE FORCE VIEW "VIEW_MLS_ACTIVITY" ("MLS_ACT_ELM_ID", "ACT_ID", "ELM_ID", "ACT_NAME", "ACT_DESCRIPTION", "ELM_SEQ_NBR", "ELM_NAME", "ELM_DESCRIPTION", "MSRMNT_NAME", "MSRMNT_ID", "ELM_TIME_ALLOW", "MSRMNT_THRUPUT", "LABOR_TYPE_CODE", "LABOR_TYPE_ID", "ACT_ELM_TIME_ALLOW") AS
select actelm.mls_act_elm_id,
      act.mls_act_id,
      elm.mls_elm_id,
      act.mls_act_name,
      act.description,
      actelm.seq_nbr,
      elm.name,
      elm.description,
      msrmnt.name,
      msrmnt.msrmnt_id,
      elm.time_allow,
      actelm.thruput_msrmnt,
      lbcode.description,
      lbcode.labor_type_id,
      actelm.time_allow
 from mls_act act,
      mls_elm elm,
      mls_act_elm actelm,
      e_msrmnt msrmnt,
      e_labor_type_code lbcode
where     actelm.mls_act_id = act.mls_act_id
      and actelm.mls_elm_id = elm.mls_elm_id
      and elm.msrmnt_id = msrmnt.msrmnt_id
      and act.labor_type_id = lbcode.labor_type_id;

CREATE OR REPLACE FORCE VIEW "VIEW_MODIFIED_ACTIVITIES" ("ACT_NAME", "SIM_ACT_NAME", "JOB_FUNCTION_NAME", "ACT_ACTION", "WHSE", "SIM_WHSE", "TOTAL_SAM", "SIM_TOTAL_SAM", "PROCESS_TIME_SAVINGS", "TOTAL_TRAVEL_TIME", "SIM_TOTAL_TRAVEL_TIME", "TRAVEL_TIME_SAVINGS", "LABOR_COST_RATE", "SIM_LABOR_COST_RATE", "LABOR_COST_SAVINGS", "EP", "SIM_EP", "EP_SAVINGS", "NO_OF_EMP", "SIM_NO_OF_EMP", "UOM_QTY", "SIM_UOM_QTY", "PROCESSTIMESAVINGS", "TRAVELTIMESAVINGS") AS
SELECT COALESCE (VIEW_PROD_SIM.ACT_NAME, VIEW_PROD_SIM.SIM_ACT_NAME)
         ACT_NAME,
      COALESCE (VIEW_PROD_SIM.ACT_NAME, VIEW_PROD_SIM.SIM_ACT_NAME)
         SIM_ACT_NAME,
      COALESCE (VIEW_PROD_SIM.SIM_JOB_FUNCTION_NAME,
                VIEW_PROD_SIM.JOB_FUNCTION_NAME)
         JOB_FUNCTION_NAME,
      CASE
         WHEN (VIEW_PROD_SIM.ACT_NAME IS NULL
               AND VIEW_PROD_SIM.SIM_ACT_NAME IS NOT NULL)
         THEN
            'NEW'
         WHEN (VIEW_PROD_SIM.ACT_NAME IS NOT NULL
               AND VIEW_PROD_SIM.SIM_ACT_NAME IS NULL)
         THEN
            'DELETED'
         WHEN (VIEW_PROD_SIM.STATUS >= 20)
         THEN
            'EDITED'
         ELSE
            'NOT EDITED'
      END
         ACT_ACTION,
      VIEW_PROD_SIM.WHSE,
      VIEW_PROD_SIM.SIM_WHSE,
      ROUND (NVL (VIEW_PROD_SIM.TOTAL_SAM, 0) / 60.0, 2) TOTAL_SAM,
      ROUND (NVL (VIEW_PROD_SIM.SIM_TOTAL_SAM, 0) / 60.0, 2)
         SIM_TOTAL_SAM,
      ROUND (NVL (VIEW_PROD_SIM.TOTAL_SAM, 0) / 60.0, 2)
      - ROUND (NVL (VIEW_PROD_SIM.SIM_TOTAL_SAM, 0) / 60.0, 2)
         PROCESS_TIME_SAVINGS,
      ROUND (NVL (VIEW_PROD_SIM.TOTAL_TRAVEL_TIME, 0) / 60.0, 2)
         TOTAL_TRAVEL_TIME,
      ROUND (NVL (VIEW_PROD_SIM.SIM_TOTAL_TRAVEL_TIME, 0) / 60.0, 2)
         SIM_TOTAL_TRAVEL_TIME,
      ROUND (NVL (VIEW_PROD_SIM.TOTAL_TRAVEL_TIME, 0) / 60.0, 2)
      - ROUND (NVL (VIEW_PROD_SIM.SIM_TOTAL_TRAVEL_TIME, 0) / 60.0, 2)
         TRAVEL_TIME_SAVINGS,
      ROUND (NVL (VIEW_PROD_SIM.LABOR_COST_RATE, 0), 2) LABOR_COST_RATE,
      ROUND (NVL (VIEW_PROD_SIM.SIM_LABOR_COST_RATE, 0), 2)
         SIM_LABOR_COST_RATE,
      ROUND (NVL (VIEW_PROD_SIM.LABOR_COST_RATE, 0), 2)
      - ROUND (NVL (VIEW_PROD_SIM.SIM_LABOR_COST_RATE, 0), 2)
         LABOR_COST_SAVINGS,
      ROUND (NVL (VIEW_PROD_SIM.EP, 0), 2) EP,
      ROUND (NVL (VIEW_PROD_SIM.SIM_EP, 0), 2) SIM_EP,
      ROUND (NVL (VIEW_PROD_SIM.EP, 0), 2)
      - ROUND (NVL (VIEW_PROD_SIM.SIM_EP, 0), 2)
         EP_SAVINGS,
      NVL (VIEW_PROD_SIM.NO_OF_EMP, 0) NO_OF_EMP,
      NVL (VIEW_PROD_SIM.SIM_NO_OF_EMP, 0) SIM_NO_OF_EMP,
      ROUND (
         NVL (
            (SELECT SUM (UOM_QTY)
               FROM    E_SIMULATION_ELM_SMRY ELMSMRY
                    INNER JOIN
                       E_ELM ELM
                    ON ELMSMRY.ELM_ID = ELM.ELM_ID
                       AND ELM.unq_seed_id <> 2
              WHERE VIEW_PROD_SIM.SIMULATION_SMRY_ID =
                       ELMSMRY.SIMULATION_SMRY_ID),
            0),
         2)
         UOM_QTY,
      ROUND (
         NVL (
            (SELECT SUM (SIM_UOM_QTY)
               FROM    E_SIMULATION_ELM_SMRY ELMSMRY
                    INNER JOIN
                       E_ELM ELM
                    ON ELMSMRY.SIM_ELM_ID = ELM.ELM_ID
                       AND ELM.unq_seed_id <> 2
              WHERE VIEW_PROD_SIM.SIMULATION_SMRY_ID =
                       ELMSMRY.SIMULATION_SMRY_ID),
            0),
         2)
         SIM_UOM_QTY,
      (CASE
          WHEN (ROUND ( (VIEW_PROD_SIM.TOTAL_SAM / 60.0), 2)) = 0
          THEN
             0.00
             - CAST (
                  (ROUND ( (VIEW_PROD_SIM.SIM_TOTAL_SAM / 6000.0), 2)) AS DECIMAL (9, 2))
          ELSE
             CAST (
                ROUND (
                   ( (CAST (
                         (ROUND ( (VIEW_PROD_SIM.TOTAL_SAM / 60.0), 2)) AS DECIMAL (9, 2))
                      - CAST (
                           (ROUND ( (VIEW_PROD_SIM.SIM_TOTAL_SAM / 60.0),
                                   2)) AS DECIMAL (9, 2)))
                    / CAST (
                         (ROUND ( (VIEW_PROD_SIM.TOTAL_SAM / 60.0), 2)) AS DECIMAL (9, 2))),
                   2) AS DECIMAL (9, 2))
       END)
         PROCESSTIMESAVINGS,
      (CASE
          WHEN (ROUND ( (VIEW_PROD_SIM.TOTAL_TRAVEL_TIME / 60.0), 2)) = 0
          THEN
             0.00
             - CAST (
                  (ROUND (
                      (VIEW_PROD_SIM.SIM_TOTAL_TRAVEL_TIME / 6000.0),
                      2)) AS DECIMAL (9, 2))
          ELSE
             CAST (
                ROUND (
                   ( (CAST (
                         (ROUND (
                             (VIEW_PROD_SIM.TOTAL_TRAVEL_TIME / 60.0),
                             2)) AS DECIMAL (9, 2))
                      - CAST (
                           (ROUND (
                               (VIEW_PROD_SIM.SIM_TOTAL_TRAVEL_TIME
                                / 60.0),
                               2)) AS DECIMAL (9, 2)))
                    / CAST (
                         (ROUND (
                             (VIEW_PROD_SIM.TOTAL_TRAVEL_TIME / 60.0),
                             2)) AS DECIMAL (9, 2))),
                   4) AS DECIMAL (9, 2))
       END)
         TRAVELTIMESAVINGS
 FROM E_SIMULATION_SMRY VIEW_PROD_SIM
WHERE VIEW_PROD_SIM.STATUS > 20;

CREATE OR REPLACE FORCE VIEW "VIEW_MOTS_FOR_PARCEL" ("TC_COMPANY_ID", "MOT_ID", "MOT", "DESCRIPTION") AS
select tc_company_id,
      mot_id,
      mot,
      description
 from mot
where standard_mot = 28;

CREATE OR REPLACE FORCE VIEW "VIEW_NEW_MOD_DEL_ELM" ("ELEMENT_NAME", "ACTION", "WHSE", "ACT_NAME", "SIMULATION_ID") AS
SELECT simulation.element_name,
CASE
  WHEN (production.element_name IS NULL
  AND simulation.element_name   IS NOT NULL)
  THEN 'Add'
  WHEN (production.element_name IS NOT NULL
  AND simulation.element_name   IS NULL)
  THEN 'Deleted'
  WHEN (production.msrmnt_name <> simulation.msrmnt_name
  OR production.time_allow     != simulation.time_allow)
  THEN 'Edit'
  ELSE 'No Change'
END ACTION,
simulation.whse,
simulation.act_name,
simulation.simulation_id
FROM VIEW_PR_WH_ACT_ELM_UOM_TIME production
FULL OUTER JOIN VIEW_SIM_WH_ACT_ELM_UOM_TIME simulation
ON production.simulation_id = simulation.simulation_id
AND production.act_name     = simulation.act_name
AND production.element_name = simulation.element_name;

CREATE OR REPLACE FORCE VIEW "VIEW_OBS_QUESTIONS" ("QUES_MASTER_ID", "QUES_TXT", "QUES_DISP_MODE", "MARK_FOR_DELETION", "COMPANY_ID") AS
SELECT QUES_MASTER_ID,
  QUES_TXT,
  QUES_DISP_MODE,
  MARK_FOR_DELETION,
  COMPANY_ID
FROM QUESTION_MASTER;

CREATE OR REPLACE FORCE VIEW "VIEW_OBS_QUES_ACT" ("ROW_NBR", "QUES_MASTER_ID", "QUESTION", "ACTIVITY", "INT_TYPE", "WHSE") AS
SELECT ROWNUM AS row_nbr,
      mgmtques.Ques_master_id,
      quesmaster.ques_txt AS Question,
      mgmtques.act_name AS Activity,
      it.int_type AS INT_TYPE,
      mgmtques.whse
 FROM e_obs_mgmt_question mgmtques
      INNER JOIN question_master quesmaster
         ON mgmtques.Ques_master_id = quesmaster.Ques_master_id
      LEFT OUTER JOIN E_OBS_INT it
         ON it.INT_TYPE_ID = mgmtques.int_TYPE_ID;

CREATE OR REPLACE FORCE VIEW "VIEW_PAYROLL_AUDIT_REPORT" ("EMP_ID", "JOB_FUNC_ID", "SHIFT_ID", "PAY_DATE", "PAY_HOURS", "DEPT_ID", "WHSE", "TOTAL_HOURLY_PAY", "ADJ_TOTAL_HOURLY_PAY", "INC_PAY", "TOTAL_INC_PAY", "ADJ_TOTAL_INC_PAY", "LABOR_TYPE_ID", "PAY_TYPE_ID", "SPVSR_EMP_ID", "ISDEL", "SPVSR_LOGIN_USER_ID") AS
select emp_id,
      job_func_id,
      shift_id,
      pay_date,
      pay_hours,
      dept_id,
      whse,
      total_hourly_pay,
      (case
          when status = 40 then roll_neg_pay
          else adj_total_hourly_pay
       end)
         adj_total_hourly_pay,
      0 as inc_pay,
      0 as total_inc_pay,
      0 as adj_total_inc_pay,
      cast (null as decimal (27, 0)) as labor_type_id,
      pay_type_id,
      spvsr_emp_id,
      (case when status = 99 then 'Y' else null end) isdel,
      (select ucl_user.login_user_id
         from view_ucl_user ucl_user
        where ucl_user.emp_id = e_emp_pay.spvsr_emp_id)
         spvsr_login_user_id
 from e_emp_pay
where last_mod_by = '1'
 union all
 select emp_id,
      job_func_id,
      shift_id,
      pay_date,
      pay_hours,
      dept_id,
      whse,
      0 as total_hourly_pay,
      0 as adj_total_hourly_pay,
      inc_pay,
      total_inc_pay,
      (case
          when status = 40 then roll_neg_inc_pay
          else adj_total_inc_pay
       end)
         adj_total_inc_pay,
      labor_type_id,
      cast (null as decimal (27, 0)) as pay_type_id,
      spvsr_emp_id,
      (case when status = 99 then 'Y' else null end) isdel,
      (select ucl_user.login_user_id
         from view_ucl_user ucl_user
        where ucl_user.emp_id = e_emp_inc_pay.spvsr_emp_id)
         spvsr_login_user_id
 from e_emp_inc_pay
where last_mod_by = '1';

CREATE OR REPLACE FORCE VIEW "VIEW_PAYROLL_EXPORT_REPORT" ("PAY_TYPE", "JOB_ID", "EXTRACTION_DATE_TIME", "EMP_INC_PAY_ID", "INC_CODE_ID", "EMP_PAY_ID", "EMP_ID", "JOB_FUNC_ID", "LABOR_TYPE_ID", "SHIFT_ID", "SPVSR_EMP_ID", "DEPT_ID", "WHSE_DATE", "PAY_DATE", "PAY_HOURS", "INC_PAY", "TOTAL_INC_PAY", "PAY_RATE_ID", "TOTAL_HOURLY_PAY", "WHSE") AS
select 'INTERM_EMP_PAY' as pay_type,
      job_id,
      extraction_date_time,
      0 as emp_inc_pay_id,                                   --DUMMYCOLUMN
      0 as inc_code_id,                                      --DUMMYCOLUMN
      emp_pay_id,
      emp_id,
      job_func_id,
      0 as labor_type_id,
      shift_id,
      spvsr_emp_id,
      dept_id,
      whse_date,
      pay_date,
      pay_hours,
      0 as inc_pay,                                         --DUMMY COLUMN
      0 as total_inc_pay,                                   --DUMMY COLUMN
      pay_rate_id,
      total_hourly_pay,
      whse
 from e_interm_emp_pay
 union all
 select 'INTERM_EMP_INC_PAY' as pay_type,
      job_id,
      extraction_date_time,
      emp_inc_pay_id,
      inc_code_id,
      0 as emp_pay_id,
      emp_id,
      job_func_id,
      labor_type_id,
      shift_id,
      spvsr_emp_id,
      dept_id,
      whse_date,
      pay_date,
      pay_hours,
      inc_pay,
      total_inc_pay,
      0 as pay_rate_id,
      0 as total_hourly_pay,
      whse
 from e_interm_emp_inc_pay;

CREATE OR REPLACE FORCE VIEW "VIEW_PAYROLL_EXPT_RPT_BY_DATE" ("EXTRACTION_DATE_TIME", "EMP_ID", "JOB_FUNC_ID", "SHIFT_ID", "SPVSR_EMP_ID", "DEPT_ID", "PAY_DATE", "BASE_PAY_HOURS", "INC_PAY_HOURS", "INC_PAY", "TOTAL_INC_PAY", "TOTAL_HOURLY_PAY", "WHSE", "PAY_RATE_ID", "JOB_ID") AS
select a.extraction_date_time,
        a.emp_id,
        a.job_func_id,
        a.shift_id,
        a.spvsr_emp_id,
        a.dept_id,
        a.pay_date,
        sum (a.basepayhours) base_pay_hours,
        sum (a.incpayhours) inc_pay_hours,
        sum (a.inc_pay) inc_pay,                            --DUMMY COLUMN
        sum (a.total_inc_pay) total_inc_pay,                --DUMMY COLUMN
        sum (a.total_hourly_pay) total_hourly_pay,
        a.whse,
        a.pay_rate_id,
        a.job_id
   from (select 'INTERM_EMP_PAY' as pay_type,
                job_id,
                extraction_date_time,
                0 as emp_inc_pay_id,                         --DUMMYCOLUMN
                0 as inc_code_id,                            --DUMMYCOLUMN
                emp_pay_id,
                emp_id,
                job_func_id,
                0 as labor_type_id,
                shift_id,
                spvsr_emp_id,
                dept_id,
                whse_date,
                pay_date,
                pay_hours as basepayhours,
                0 as incpayhours,
                0 as inc_pay,                               --DUMMY COLUMN
                0 as total_inc_pay,                         --DUMMY COLUMN
                pay_rate_id,
                total_hourly_pay,
                whse
           from e_interm_emp_pay base
         union all
         select 'INTERM_EMP_INC_PAY' as pay_type,
                job_id,
                extraction_date_time,
                emp_inc_pay_id,
                inc_code_id,
                0 as emp_pay_id,
                emp_id,
                job_func_id,
                labor_type_id,
                shift_id,
                spvsr_emp_id,
                dept_id,
                whse_date,
                pay_date,
                0 as basepayhours,
                pay_hours as incpayhours,
                inc_pay,
                total_inc_pay,
                0 as pay_rate_id,
                0 as total_hourly_pay,
                whse
           from e_interm_emp_inc_pay inc) a
 group by a.whse,
        a.extraction_date_time,
        a.emp_id,
        a.pay_date,
        a.job_func_id,
        a.dept_id,
        a.shift_id,
        a.spvsr_emp_id,
        a.pay_rate_id,
        a.job_id;

CREATE OR REPLACE FORCE VIEW "VIEW_PAYROLL_REPORT" ("PAY_TYPE", "EMP_ID", "JOB_FUNC_ID", "SHIFT_ID", "PAY_DATE", "BASEPAYHRS", "DEPT_ID", "WHSE_DATE", "WHSE", "PAY_RATE_ID", "TOTAL_HOURLY_PAY", "INC_PAY", "TOTAL_INC_PAY", "INCPAYHRS", "STATUS", "LABOR_TYPE_ID") AS
select 'EMP_PAY' as pay_type,
      emp_id,
      job_func_id,
      shift_id,
      pay_date,
      pay_hours as basepayhrs,
      dept_id,
      whse_date,
      whse,
      pay_rate_id,
      (case
          when (status = 40)
          then
             roll_neg_pay
          else
             case
                when (adj_total_hourly_pay is not null)
                then
                   adj_total_hourly_pay
                else
                   total_hourly_pay
             end
       end)
         total_hourly_pay,
      0 as inc_pay,                                          --DUMMYCOLUMN
      0 as total_inc_pay,                                    --DUMMYCOLUMN
      0 as incpayhrs,                                        --DUMMYCOLUMN
      status,
      0 as labor_type_id
 from e_emp_pay
 union all
 select 'INC_PAY' as pay_type,
      emp_id,
      job_func_id,
      shift_id,
      pay_date,
      0 as basepayhrs,
      dept_id,
      whse_date,
      whse,
      0 as pay_rate_id,                                      --DUMMYCOLUMN
      0 as total_hourly_pay,                                 --DUMMYCOLUMN
      inc_pay,
      (case
          when (status = 40)
          then
             roll_neg_inc_pay
          else
             case
                when (adj_total_inc_pay is not null)
                then
                   adj_total_inc_pay
                else
                   total_inc_pay
             end
       end)
         total_inc_pay,
      pay_hours as incpayhrs,
      status,
      labor_type_id
 from e_emp_inc_pay;

CREATE OR REPLACE FORCE VIEW "VIEW_PAYROLL_REPORT_SCI" ("PAY_TYPE", "EMP_ID", "JOB_FUNC_ID", "SHIFT_ID", "PAY_DATE", "PAY_HOURS", "DEPT_ID", "WHSE", "PAY_RATE_ID", "TOTAL_HOURLY_PAY", "ADJ_TOTAL_HOURLY_PAY", "INC_PAY", "TOTAL_INC_PAY", "ADJ_TOTAL_INC_PAY", "LABOR_TYPE_ID", "PAY_TYPE_ID", "SPVSR_EMP_ID", "ISDEL", "SPVSR_LOGIN_USER_ID", "LAST_MOD_BY", "STATUS") AS
SELECT 'EMP_PAY' AS PAY_TYPE,
EMP_ID,
JOB_FUNC_ID,
SHIFT_ID,
PAY_DATE,
PAY_HOURS,
DEPT_ID,
WHSE,
PAY_RATE_ID,
TOTAL_HOURLY_PAY,
(
CASE
  WHEN STATUS = 40
  THEN ROLL_NEG_PAY
  ELSE ADJ_TOTAL_HOURLY_PAY
END) ADJ_TOTAL_HOURLY_PAY,
0                           AS INC_PAY,
0                           AS TOTAL_INC_PAY,
0                           AS ADJ_TOTAL_INC_PAY,
CAST(NULL AS DECIMAL(27,0)) AS LABOR_TYPE_ID,
PAY_TYPE_ID,
SPVSR_EMP_ID,
(
CASE
  WHEN STATUS = 99
  THEN 'Y'
  ELSE NULL
END) ISDEL,
(SELECT UCL_USER.LOGIN_USER_ID
FROM VIEW_UCL_USER UCL_USER
WHERE UCL_USER.EMP_ID = E_EMP_PAY.SPVSR_EMP_ID
) SPVSR_LOGIN_USER_ID,
LAST_MOD_BY,
STATUS
FROM E_EMP_PAY
--WHERE LAST_MOD_BY = '1'
UNION ALL
SELECT 'INC_PAY' AS PAY_TYPE,
EMP_ID,
JOB_FUNC_ID,
SHIFT_ID,
PAY_DATE,
PAY_HOURS,
DEPT_ID,
WHSE,
0 AS PAY_RATE_ID, --DUMMYCOLUMN
0 AS TOTAL_HOURLY_PAY,
0 AS ADJ_TOTAL_HOURLY_PAY,
INC_PAY,
TOTAL_INC_PAY,
(
CASE
  WHEN STATUS=40
  THEN ROLL_NEG_INC_PAY
  ELSE ADJ_TOTAL_INC_PAY
END) ADJ_TOTAL_INC_PAY,
LABOR_TYPE_ID,
CAST(NULL AS DECIMAL(27,0)) AS PAY_TYPE_ID,
SPVSR_EMP_ID,
(
CASE
  WHEN STATUS = 99
  THEN 'Y'
  ELSE NULL
END) ISDEL,
(SELECT UCL_USER.LOGIN_USER_ID
FROM VIEW_UCL_USER UCL_USER
WHERE UCL_USER.EMP_ID = E_EMP_INC_PAY.SPVSR_EMP_ID
) SPVSR_LOGIN_USER_ID,
LAST_MOD_BY,
STATUS
FROM E_EMP_INC_PAY;

CREATE OR REPLACE FORCE VIEW "VIEW_PAY_CALC" ("ID", "VALUE") AS
( (select '0' as id, 'Pay Rate Condition' as value from dual)
union
(select '1' as id, 'Pay Rate Statement' as value from dual)
union
(select '2' as id, 'Incentive Pay Condition' as value from dual)
union
(select '3' as id, 'Incentive Pay Statement' as value from dual));

CREATE OR REPLACE FORCE VIEW "VIEW_PFD_CODE" ("ELM_ID", "COUNT_OF_PFD_CODE") AS
select elm_id, count (pfd_id) as count_of_pfd_code
   from e_elm_pfd
 group by elm_id
 having elm_id in (select elm_id from e_elm);

CREATE OR REPLACE FORCE VIEW "VIEW_PFD_DATAGRID" ("ELM_ID", "PFD_ID", "STATUS", "WHSE") AS
select distinct elm_id,
               e_elm_pfd.pfd_id,
               1 as status,
               e_elm_pfd.whse
 from e_elm_pfd
 union
 select elm_id as elm_id,
      e_pfd_hdr.pfd_id as pfd_id,
      0 as status,
      e_pfd_hdr.whse as whse
 from e_elm, e_pfd_hdr
where (elm_id, pfd_id) not in (select elm_id, pfd_id from e_elm_pfd);

CREATE OR REPLACE FORCE VIEW "VIEW_PFD_TIME" ("TRAN_NBR", "WHSE", "PFD_SAM") AS
select tran_nbr, whse, fn_pfd_time (tran_nbr, whse) as pfd_sam
   from e_evnt_smry_elm_dtl
 group by tran_nbr, whse;

CREATE OR REPLACE FORCE VIEW "VIEW_PR_WH_ACT_ELM_UOM_TIME" ("SIMULATION_ID", "START_DATE", "END_DATE", "ACT_ID", "ELM_ID", "ELEMENT_NAME", "MSRMNT_ID", "MSRMNT_NAME", "TIME_ALLOW", "ACT_NAME", "WHSE", "CREATE_DATE_TIME") AS
SELECT sim.simulation_id,
sim.start_date,
sim.end_date,
aes.act_id,
aes.elm_id,
COALESCE (elm.orig_name, elm.name) AS element_name,
elm.msrmnt_id,
COALESCE (msrmnt.orig_name, msrmnt.name) AS msrmnt_name,
aes.time_allow,
lact.name AS act_name,
act.whse,
aes.create_date_time
FROM labor_activity lact
INNER JOIN e_act act
ON lact.labor_activity_id = act.labor_activity_id
INNER JOIN e_simulation_whse sim
ON COALESCE(sim.app_sbx_whse,sim.src_whse) = act.whse
INNER JOIN e_act_elm aes
ON aes.act_id = act.act_id
INNER JOIN e_elm elm
ON aes.elm_id = elm.elm_id
INNER JOIN e_msrmnt msrmnt
ON elm.msrmnt_id = msrmnt.msrmnt_id;

CREATE OR REPLACE FORCE VIEW "VIEW_QAREASON_EMP_JF_RPT" ("QA_QTY", "QA_FAIL_QTY", "QA_REASON_DESCRIPTION", "WHSE", "LOGIN_USER_ID", "QA_DATE_TIME", "NAME", "CODE_ID", "TEAM_ID", "QA_REF_VALUE", "TEAM_CODE", "QA_ID") AS
select e_qa.qa_qty,
      e_qa_dtl.qa_fail_qty,
      e_qa_reason.qa_reason_description,
      e_qa.whse,
      view_ucl_user.login_user_id,
      e_qa.qa_date_time,
      e_job_function.name,
      e_qa.code_id,
      e_qa.team_id,
      e_qa.qa_ref_value,
      e_team_std.team_code,
      e_qa.qa_id
 from    (   (   (   (   e_qa_reason
                      inner join
                         e_qa_dtl
                      on e_qa_reason.qa_reason_id = e_qa_dtl.qa_reason_id)
                  right outer join
                     e_qa
                  on e_qa_dtl.qa_id = e_qa.qa_id)
              inner join
                 e_job_function
              on e_qa.job_func_id = e_job_function.job_func_id)
          left outer join
             e_team_std
          on e_qa.team_id = e_team_std.team_id)
      left outer join
         view_ucl_user
      on e_qa.emp_id = view_ucl_user.emp_id;

CREATE OR REPLACE FORCE VIEW "VIEW_QAREASON_TEAM_RPT" ("QA_QTY", "QA_FAIL_QTY", "QA_REASON_DESCRIPTION", "WHSE", "QA_DATE_TIME", "NAME", "CODE_ID", "TEAM_CODE", "QA_REF_VALUE") AS
select e_qa.qa_qty,
      e_qa_dtl.qa_fail_qty,
      e_qa_reason.qa_reason_description,
      e_qa.whse,
      e_qa.qa_date_time,
      e_job_function.name,
      e_qa.code_id,
      e_team_std.team_code,
      e_qa.qa_ref_value
 from    (   (   (   e_qa_reason
                  inner join
                     e_qa_dtl
                  on e_qa_reason.qa_reason_id = e_qa_dtl.qa_reason_id)
              right outer join
                 e_qa
              on e_qa_dtl.qa_id = e_qa.qa_id)
          inner join
             e_job_function
          on e_qa.job_func_id = e_job_function.job_func_id)
      left outer join
         e_team_std
      on e_qa.team_id = e_team_std.team_id;

CREATE OR REPLACE FORCE VIEW "VIEW_REFLECT_STD_SOURCE_NAME" ("REFLECT_STD_DTL_ID", "SOURCE_GROUP_TYPE", "MOD_USER_ID") AS
select reflect_std_dtl_id,
        source_group_type,
        fn_ref_std_smry (reflect_std_dtl_id, source_group_type) mod_user_id
   from e_reflect_std_source_smry
 group by reflect_std_dtl_id, source_group_type;

CREATE OR REPLACE FORCE VIEW "VIEW_RULE_NBR_RULE_CALC" ("MSRMNT_ID", "RULE_NBR", "CALC_TYPE") AS
select a.msrmnt_id as msrmnt_id,
      a.rule_nbr as rule_nbr,
      (select b.calc_type
         from e_msrmnt_rule_calc b
        where     b.rule_nbr = a.rule_nbr
              and a.msrmnt_id = b.msrmnt_id
              and rownum = 1)
         as calc_type
 from e_msrmnt_rule a;

CREATE OR REPLACE FORCE VIEW "VIEW_SHIFT_DTL_ACTIVITY" ("ACT_ID", "ACTIVITY_NAME") AS
select distinct e_act.act_id as act_id, l_act.name as activity_name
 from e_act, labor_activity l_act, e_shift_dtl
where e_act.labor_activity_id = l_act.labor_activity_id
      and e_act.labor_type_id in
             (select labor_type_id
                from e_labor_type_code
               where labor_type_code in (200, 400, 600));

CREATE OR REPLACE FORCE VIEW "VIEW_SIM_WH_ACT_ELM_UOM_TIME" ("SIMULATION_ID", "START_DATE", "END_DATE", "ACT_ID", "ELM_ID", "ELEMENT_NAME", "MSRMNT_ID", "MSRMNT_NAME", "TIME_ALLOW", "ACT_NAME", "WHSE", "CREATE_DATE_TIME") AS
SELECT sim.simulation_id,
      sim.start_date,
      sim.end_date,
      aes.act_id,
      aes.elm_id,
      COALESCE (elm.orig_name, elm.name) AS element_name,
      elm.msrmnt_id,
      COALESCE (msrmnt.orig_name, msrmnt.name) AS msrmnt_name,
      aes.time_allow,
      lact.name AS act_name,
      act.whse,
      aes.create_date_time
 FROM labor_activity lact
      INNER JOIN e_act act
         ON lact.labor_activity_id = act.labor_activity_id
      INNER JOIN e_simulation_whse sim
         ON sim.app_sim_whse = act.whse
      INNER JOIN e_act_elm aes
         ON aes.act_id = act.act_id
      INNER JOIN e_elm elm
         ON aes.elm_id = elm.elm_id
      INNER JOIN e_msrmnt msrmnt
         ON elm.msrmnt_id = msrmnt.msrmnt_id;

CREATE OR REPLACE FORCE VIEW "VIEW_TEAM_EMP" ("TEAM_EMP_ID", "EMP_ID", "TEAM_ID", "ACT_ID", "BEGIN_DATE_TIME", "END_DATE_TIME", "WHSE", "TEAMWHSEBEGINTIME") AS
select team_emp_id,
      emp_id,
      team_id,
      act_id,
      begin_date_time,
      end_date_time,
      whse,
      getwhsedatetime (begin_date_time, whse) as teamwhsebegintime
 from e_team_emp;

CREATE OR REPLACE FORCE VIEW "VIEW_TEAM_EMP_ASSIGNED" ("WHSE", "EMP_ID", "BEGIN_TIME", "EMPASSIGNED") AS
select distinct
      whse,
      emp_id,
      begin_time,
      fn_team_std_empassigned (whse, emp_id, begin_time) as empassigned
 from e_team_emp_smry;

CREATE OR REPLACE FORCE VIEW "VIEW_TEAM_EMP_SMRY" ("TEAM_EMP_SMRY_ID", "EMP_ID", "WHSE", "TEAM_ID", "TEAM_CODE", "ACT_ID", "BEGIN_TIME", "END_TIME", "SAM", "PAM", "TEAMEMPTIME", "OSDL", "OSIL", "NSDL", "SIL", "UDIL", "UIL", "ADJ_OSDL", "ADJ_OSIL", "ADJ_UDIL", "ADJ_NSDL", "PAID_BRK", "UNPAID_BRK", "TEAM_TIME", "WHSEBEGINTIME") AS
select tes.team_emp_smry_id,
      tes.emp_id,
      tes.whse,
      tes.team_id,
      tes.team_code,
      tes.act_id,
      tes.begin_time,
      tes.end_time,
      sam,
      pam,
      lmdatediff (
         'MI',
         to_date (to_char (tes.begin_time, 'YYYY-MON-DD HH24.MI.SS'),
                  'YYYY-MON-DD HH24.MI.SS'),
         to_date (to_char (tes.end_time, 'YYYY-MON-DD HH24.MI.SS'),
                  'YYYY-MON-DD HH24.MI.SS'),
         tes.whse)
         as teamemptime,
      osdl,
      osil,
      nsdl,
      sil,
      udil,
      uil,
      adj_osdl,
      adj_osil,
      adj_udil,
      adj_nsdl,
      paid_brk,
      unpaid_brk,
      tes.team_time as team_time,
      getwhsedatetime (tes.begin_time, tes.whse) as whsebegintime
 from e_team_emp_smry tes;

CREATE OR REPLACE FORCE VIEW "VIEW_TEAM_QA_QTY" ("JOB_FUNC_ID", "EMP_ID", "TEAM_ID", "QA_TOTALS_DATE", "QA_TOTAL_QTY", "QA_TOTAL_FAIL_QTY", "WHSE") AS
select job_func_id,
      emp_id,
      team_id,
      qa_totals_date,
      qa_total_qty,
      qa_total_fail_qty,
      whse
 from e_qa_smry
where emp_id = -1;

CREATE OR REPLACE FORCE VIEW "VIEW_TEAM_STD_BY_SPVSR" ("TOTALS_DATE", "SPVSR_LOGIN_USER_ID", "TEAM_CODE", "WHSE", "TOTAL_SAM", "TOTAL_PAM", "PERF_ADJ_AMT", "TOTAL_ACTUAL_TIME", "ADJ_AMT", "OSDL", "OSIL", "UDIL", "NSDL", "SIL", "TOTAL_PAID_BRK", "TOTAL_UNPAID_BRK") AS
select distinct a.totals_date,
               d.spvsr_login_user_id,
               a.team_code,
               a.whse,
               a.total_sam,
               a.total_pam,
               a.perf_adj_amt,
               a.total_actual_time,
               a.adj_amt,
               a.osdl,
               a.osil,
               a.udil,
               a.nsdl,
               a.sil,
               a.total_paid_brk,
               a.total_unpaid_brk
 from e_team_std_smry a
      inner join e_team_emp b
         on a.team_id = b.team_id
            and getwhsedatetime (a.totals_date, a.whse) =
                   getwhsedatetime (b.begin_date_time, b.whse)
            and a.whse = b.whse
      inner join view_ucl_user c
         on b.emp_id = c.emp_id
      inner join e_consol_perf_smry d
         on     c.login_user_id = d.login_user_id
            and d.whse_date = a.totals_date
            and d.whse = a.whse;

CREATE OR REPLACE FORCE VIEW "VIEW_THRUPUT_DTL" ("THRUPUT_ID", "MSRMNT_ID", "QTY", "NAME", "MSRMNT_CODE") AS
select tdtl.thruput_id,
      tdtl.msrmnt_id,
      tdtl.qty,
      mnt.name,
      mnt.msrmnt_code
 from    e_thruput_smry_dtl tdtl
      inner join
         e_msrmnt mnt
      on mnt.msrmnt_id = tdtl.msrmnt_id;

CREATE OR REPLACE FORCE VIEW "VIEW_THRUPUT_TOTAL_TIME" ("PERF_SMRY_TRAN_ID", "THRUPUT_ID", "MSRMNT_ID", "SUM_TOTAL_TIME", "TOTAL_TIME") AS
select s.perf_smry_tran_id,
        s.thruput_id,
        l.msrmnt_id,
        sum (s.total_time) / count (s.thruput_id) as sum_total_time,
        sum (s.total_time) as total_time
   from e_thruput_smry s, e_thruput_smry_dtl l
  where s.thruput_id = l.thruput_id
 group by s.thruput_id, s.perf_smry_tran_id, l.msrmnt_id;

CREATE OR REPLACE FORCE VIEW "VIEW_TOTAL_QA_FAIL_QTY" ("QA_ID", "TOTAL_QA_FAIL_QTY") AS
select a.qa_id as qa_id,
      (select nvl (sum (b.qa_fail_qty), 0)
         from e_qa_dtl b
        where b.qa_id = a.qa_id)
         as total_qa_fail_qty
 from e_qa a;

CREATE OR REPLACE FORCE VIEW "VIEW_UCL_USER" ("EMP_ID", "LOGIN_USER_ID", "PSWD", "FIRST_NAME", "LAST_NAME", "COMPANY_ID", "USER_PASSWORD", "IS_ACTIVE", "CREATED_SOURCE_TYPE_ID", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE_ID", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "USER_TYPE_ID", "LOCALE_ID", "LOCATION_ID", "USER_PREFIX", "USER_TITLE", "TEL_NBR", "FAX", "ADDR_LINE_1", "ADDR_LINE_2", "CITY", "STATE_PROV_CODE", "PSTL_CODE", "COUNTRY", "EMAIL", "USER_EMAIL_2", "COMM_METHOD_ID_DURING_BH_1", "COMM_METHOD_ID_DURING_BH_2", "USER_MIDDLE_NAME", "COMM_METHOD_ID_AFTER_BH_1", "COMM_METHOD_ID_AFTER_BH_2", "COMMON_NAME", "LAST_PASSWORD_CHANGE_DTTM", "LOGGED_IN", "LAST_LOGIN_DTTM", "DEFAULT_BUSINESS_UNIT_ID", "DEFAULT_WHSE_REGION_ID", "CHANNEL_ID", "HIBERNATE_VERSION", "NUMBER_OF_INVALID_LOGINS", "TAX_ID_NBR", "EMP_START_DATE", "BRITH_DATE", "GENDER_ID") AS
select ucl_user_id as emp_id,
      user_name as login_user_id,
      user_password as pswd,
      user_first_name as first_name,
      user_last_name as last_name,
      company_id as company_id,
      user_password as user_password,
      is_active as is_active,
      created_source_type_id as created_source_type_id,
      created_source as created_source,
      created_dttm as created_dttm,
      last_updated_source_type_id as last_updated_source_type_id,
      last_updated_source as last_updated_source,
      last_updated_dttm as last_updated_dttm,
      user_type_id as user_type_id,
      locale_id as locale_id,
      location_id as location_id,
      user_prefix as user_prefix,
      user_title as user_title,
      telephone_number as tel_nbr,
      fax_number as fax,
      address_1 as addr_line_1,
      address_2 as addr_line_2,
      city as city,
      state_prov_code as state_prov_code,
      postal_code as pstl_code,
      country_code as country,
      user_email_1 as email,
      user_email_2 as user_email_2,
      comm_method_id_during_bh_1 as comm_method_id_during_bh_1,
      comm_method_id_during_bh_2 as comm_method_id_during_bh_2,
      user_middle_name as user_middle_name,
      comm_method_id_after_bh_1 as comm_method_id_after_bh_1,
      comm_method_id_after_bh_2 as comm_method_id_after_bh_2,
      common_name as common_name,
      last_password_change_dttm as last_password_change_dttm,
      logged_in as logged_in,
      last_login_dttm as last_login_dttm,
      default_business_unit_id as default_business_unit_id,
      default_whse_region_id as default_whse_region_id,
      channel_id as channel_id,
      hibernate_version as hibernate_version,
      number_of_invalid_logins as number_of_invalid_logins,
      tax_id_nbr as tax_id_nbr,
      emp_start_date as emp_start_date,
      birth_date as brith_date,
      gender_id as gender_id
 from ucl_user;

CREATE OR REPLACE FORCE VIEW "VIEW_UOM_NAME" ("MSRMNT_ID", "MSRMNT_NAME", "SYS_UOM") AS
select msrmnt_id as msrmnt_id, name as msrmnt_name, sys_created as sys_uom
 from e_msrmnt;

CREATE OR REPLACE FORCE VIEW "VIEW_VEHICLE" ("SIMULATION_ID", "SIMULATION_SMRY_ID", "PROD_WHSE", "SIM_WHSE", "PROD_VHCL_TYPE", "SIM_VHCL_TYPE", "PRODTIME", "SIMTIME", "ACT_NAME", "VHCL_ACTION") AS
SELECT vhclSmry.Simulation_Id,
      vhclSmry.simulation_smry_id,
      prod.whse AS prod_whse,
      sim.whse AS sim_whse,
      vhclSmry.vhcl_type prod_vhcl_type,
      vhclSmry.sim_vhcl_type sim_vhcl_type,
      vhclSmry.travel_time prodtime,
      vhclSmry.sim_travel_time simtime,
      COALESCE (simsmry.act_name, simsmry.sim_act_name) act_name,
      CASE
         WHEN (   prod.MAX_FWD_VELC != sim.MAX_FWD_VELC
               OR prod.FWD_ACEL_TIME != sim.FWD_ACEL_TIME
               OR prod.FWD_ACEL_DIST != sim.FWD_ACEL_DIST
               OR prod.MAX_BWD_VELC != sim.MAX_BWD_VELC
               OR prod.BWD_ACEL_TIME != sim.BWD_ACEL_TIME
               OR prod.BWD_ACEL_DIST != sim.BWD_ACEL_DIST
               OR prod.DCEL_TIME != sim.DCEL_TIME
               OR prod.DCEL_DIST != sim.DCEL_DIST
               OR prod.ELV_SPD != sim.ELV_SPD
               OR prod.MAX_WALK_DIST != sim.MAX_WALK_DIST
               OR prod.WALK_VELC != sim.WALK_VELC
               OR prod.MAX_WR_DIST != sim.MAX_WR_DIST
               OR prod.WR_VELC != sim.WR_VELC
               OR prod.LVL_RETN_FLAG != sim.LVL_RETN_FLAG
               OR prod.UNLD_MAX_FWD_VELC != sim.UNLD_MAX_FWD_VELC
               OR prod.UNLD_FWD_ACEL_TIME != sim.UNLD_FWD_ACEL_TIME
               OR prod.UNLD_FWD_ACEL_DIST != sim.UNLD_FWD_ACEL_DIST
               OR prod.UNLD_FWD_DCEL_TIME != sim.UNLD_FWD_DCEL_TIME
               OR prod.UNLD_FWD_DCEL_DIST != sim.UNLD_FWD_DCEL_DIST
               OR prod.LD_BWD_DCEL_TIME != sim.LD_BWD_DCEL_TIME
               OR prod.LD_BWD_DCEL_DIST != sim.LD_BWD_DCEL_DIST
               OR prod.UNLD_MAX_BWD_VELC != sim.UNLD_MAX_BWD_VELC
               OR prod.UNLD_BWD_ACEL_TIME != sim.UNLD_BWD_ACEL_TIME
               OR prod.UNLD_BWD_ACEL_DIST != sim.UNLD_BWD_ACEL_DIST
               OR prod.UNLD_BWD_DCEL_TIME != sim.UNLD_BWD_DCEL_TIME
               OR prod.UNLD_BWD_DCEL_DIST != sim.UNLD_BWD_DCEL_DIST
               OR prod.UNLD_WR_VELC != sim.UNLD_WR_VELC
               OR prod.UNLD_MAX_WR_DIST != sim.UNLD_MAX_WR_DIST
               OR prod.UNLD_ELV_SPD != sim.UNLD_ELV_SPD
               OR prod.UNLD_LWRG_SPD != sim.UNLD_LWRG_SPD
               OR prod.LWRG_SPD != sim.LWRG_SPD)
         THEN
            'Edited'
         ELSE
            'NOT MODIFIED'
      END
         vhcl_action
 FROM e_simulation_vhcl_smry vhclSmry
      INNER JOIN e_simulation_smry simsmry
         ON simsmry.simulation_id = vhclSmry.simulation_id
            AND simsmry.simulation_smry_id = vhclSmry.simulation_smry_id
      LEFT OUTER JOIN vhcl_parameters prod
         ON vhclSmry.Vhcl_Type = prod.vhcl_type
            AND prod.whse =
                   DECODE (prod.whse,
                           simsmry.whse, simsmry.whse,
                           '*', '*')
      LEFT OUTER JOIN vhcl_parameters sim
         ON vhclSmry.Vhcl_Type = sim.vhcl_type
            AND sim.whse = simsmry.sim_whse
WHERE simsmry.status >= 20;

CREATE OR REPLACE FORCE VIEW "VIEW_VHCL" ("SIMULATION_ID", "SIMULATION_SMRY_ID", "PROD_WHSE", "SIM_WHSE", "PROD_VHCL_TYPE", "SIM_VHCL_TYPE", "PRODTIME", "SIMTIME", "ACT_NAME", "VHCL_ACTION") AS
SELECT vhclSmry.Simulation_Id,
      vhclSmry.simulation_smry_id,
      prod.whse AS prod_whse,
      sim.whse AS sim_whse,
      vhclSmry.vhcl_type prod_vhcl_type,
      vhclSmry.sim_vhcl_type sim_vhcl_type,
      vhclSmry.travel_time prodtime,
      vhclSmry.sim_travel_time simtime,
      COALESCE (simsmry.act_name, simsmry.sim_act_name) act_name,
      CASE
         WHEN (   prod.MAX_FWD_VELC != sim.MAX_FWD_VELC
               OR prod.FWD_ACEL_TIME != sim.FWD_ACEL_TIME
               OR prod.FWD_ACEL_DIST != sim.FWD_ACEL_DIST
               OR prod.MAX_BWD_VELC != sim.MAX_BWD_VELC
               OR prod.BWD_ACEL_TIME != sim.BWD_ACEL_TIME
               OR prod.BWD_ACEL_DIST != sim.BWD_ACEL_DIST
               OR prod.DCEL_TIME != sim.DCEL_TIME
               OR prod.DCEL_DIST != sim.DCEL_DIST
               OR prod.ELV_SPD != sim.ELV_SPD
               OR prod.MAX_WALK_DIST != sim.MAX_WALK_DIST
               OR prod.WALK_VELC != sim.WALK_VELC
               OR prod.MAX_WR_DIST != sim.MAX_WR_DIST
               OR prod.WR_VELC != sim.WR_VELC
               OR prod.LVL_RETN_FLAG != sim.LVL_RETN_FLAG
               OR prod.UNLD_MAX_FWD_VELC != sim.UNLD_MAX_FWD_VELC
               OR prod.UNLD_FWD_ACEL_TIME != sim.UNLD_FWD_ACEL_TIME
               OR prod.UNLD_FWD_ACEL_DIST != sim.UNLD_FWD_ACEL_DIST
               OR prod.UNLD_FWD_DCEL_TIME != sim.UNLD_FWD_DCEL_TIME
               OR prod.UNLD_FWD_DCEL_DIST != sim.UNLD_FWD_DCEL_DIST
               OR prod.LD_BWD_DCEL_TIME != sim.LD_BWD_DCEL_TIME
               OR prod.LD_BWD_DCEL_DIST != sim.LD_BWD_DCEL_DIST
               OR prod.UNLD_MAX_BWD_VELC != sim.UNLD_MAX_BWD_VELC
               OR prod.UNLD_BWD_ACEL_TIME != sim.UNLD_BWD_ACEL_TIME
               OR prod.UNLD_BWD_ACEL_DIST != sim.UNLD_BWD_ACEL_DIST
               OR prod.UNLD_BWD_DCEL_TIME != sim.UNLD_BWD_DCEL_TIME
               OR prod.UNLD_BWD_DCEL_DIST != sim.UNLD_BWD_DCEL_DIST
               OR prod.UNLD_WR_VELC != sim.UNLD_WR_VELC
               OR prod.UNLD_MAX_WR_DIST != sim.UNLD_MAX_WR_DIST
               OR prod.UNLD_ELV_SPD != sim.UNLD_ELV_SPD
               OR prod.UNLD_LWRG_SPD != sim.UNLD_LWRG_SPD
               OR prod.LWRG_SPD != sim.LWRG_SPD)
         THEN
            'Edited'
         ELSE
            'NOT MODIFIED'
      END
         vhcl_action
 FROM e_simulation_vhcl_smry vhclSmry
      INNER JOIN e_simulation_smry simsmry
         ON simsmry.simulation_id = vhclSmry.simulation_id
            AND simsmry.simulation_smry_id = vhclSmry.simulation_smry_id
      LEFT OUTER JOIN vhcl_parameters prod
         ON vhclSmry.Vhcl_Type = prod.vhcl_type
            AND prod.whse =
                   DECODE (prod.whse,
                           simsmry.whse, simsmry.whse,
                           '*', '*')
      LEFT OUTER JOIN vhcl_parameters sim
         ON vhclSmry.Vhcl_Type = sim.vhcl_type
            AND sim.whse = simsmry.sim_whse
WHERE simsmry.status > 20;

CREATE OR REPLACE FORCE VIEW "VIEW_VHCLWHSE" ("ROW_NBR", "VHCL_TYPE_ID", "VHCL_TYPE", "DESCRIPTION", "WHSE", "WHSE_VHCL_TYPE_ID") AS
select rownum as row_nbr,
      res.vhcl_type_id,
      res.vhcl_type,
      res.description,
      res.whse,
      res.whse_vhcl_type_id
 from ( (select v1.vhcl_type_id as vhcl_type_id,
                v1.vhcl_type as vhcl_type,
                v1.description as description,
                '*' as whse,
                null as whse_vhcl_type_id
           from e_vhcl v1)
       union all
       (select vw.vhcl_type_id as vhcl_type_id,
               (select v.vhcl_type
                  from e_vhcl v
                 where v.vhcl_type_id = vw.vhcl_type_id)
                  as vhcl_type,
               vw.description as description,
               vw.whse as whse,
               vw.whse_vhcl_type_id as whse_vhcl_type_id
          from e_vhcl_whse vw)) res;

CREATE OR REPLACE FORCE VIEW "VIEW_W_WHSE_SYS_CODE" ("REC_TYPE", "CODE_TYPE", "WHSE", "CODE_ID", "CODE_DESC", "SHORT_DESC", "MISC_FLAGS", "NUMBER_OF_UNITS", "ACTIVITY_NAME", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select rec_type,
      code_type,
      whse,
      code_id,
      code_desc,
      short_desc,
      misc_flags,
      (case
          when (length (
                   trim (
                      translate (trim (substr (misc_flags, 1, 21)),
                                 ' +-.0123456789',
                                 ' '))))
                  is null
          then
             to_number (trim (substr (misc_flags, 1, 21)))
          else
             1
       end)
         number_of_units,
      trim (substr (misc_flags, 22, 71)) activity_name,
      create_date_time,
      mod_date_time,
      user_id
 from whse_sys_code;

CREATE OR REPLACE FORCE VIEW "VLD_ACC_PARM_SET_V" ("ACCESSORIAL_PARAM_SET_ID", "TC_COMPANY_ID", "CARRIER_ID", "USE_FAK_COMMODITY", "COMMODITY_CLASS", "EQUIPMENT_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "CM_RATE_TYPE", "CM_RATE_CURRENCY_CODE", "STOP_OFF_CURRENCY_CODE", "DISTANCE_UOM", "BUDGETED_APPROVED", "SIZE_UOM_ID", "IS_GLOBAL", "IS_VALID", "DTL_HAS_ERRORS") AS
select aps.accessorial_param_set_id,
      aps.tc_company_id,
      aps.carrier_id,
      aps.use_fak_commodity,
      aps.commodity_class,
      aps.equipment_id,
      aps.effective_dt,
      aps.expiration_dt,
      aps.cm_rate_type,
      aps.cm_rate_currency_code,
      aps.stop_off_currency_code,
      aps.distance_uom,
      aps.budgeted_approved,
      aps.size_uom_id,
      aps.is_global,
      1 is_valid,
      nvl (
         (select sum (decode (icrd.status, 4, 1, 0))
            from import_accessorial_param_set iaps,
                 import_cm_rate_discount icrd
           where aps.accessorial_param_set_id =
                    iaps.accessorial_param_set_id
                 and aps.tc_company_id = iaps.tc_company_id
                 and iaps.param_id = icrd.param_id),
         0)
      + nvl (
           (select sum (decode (isoc.status, 4, 1, 0))
              from import_accessorial_param_set iaps,
                   import_stop_off_charge isoc
             where aps.accessorial_param_set_id =
                      iaps.accessorial_param_set_id
                   and aps.tc_company_id = iaps.tc_company_id
                   and iaps.param_id = isoc.param_id),
           0)
      + nvl (
           (select sum (decode (iar.status, 4, 1, 0))
              from import_accessorial_param_set iaps,
                   import_accessorial_rate iar
             where aps.accessorial_param_set_id =
                      iaps.accessorial_param_set_id
                   and aps.tc_company_id = iaps.tc_company_id
                   and iaps.param_id = iar.param_id
                   and iaps.tc_company_id = iar.tc_company_id),
           0)
         dtl_has_errors
 from accessorial_param_set aps;

CREATE OR REPLACE FORCE VIEW "VW_CD_MASTER" ("TC_COMPANY_ID", "COMPANY_CODE", "AUTO_CREATE_BATCH_FLAG", "BATCH_CTRL_FLAG", "BATCH_ROLE_ID", "CASE_LOCK_CODE_EXP_REC", "CASE_LOCK_CODE_HELD", "COLOR_MASK", "COLOR_OFFSET", "COLOR_SEPTR", "COLOR_SFX_MASK", "COLOR_SFX_OFFSET", "COLOR_SFX_SEPTR", "DFLT_BATCH_STAT_CODE", "LOCK_CODE_INVALID", "PICK_LOCK_CODE_EXP_REC", "PICK_LOCK_CODE_HELD", "PROC_WHSE_XFER", "QUAL_MASK", "QUAL_OFFSET", "QUAL_SEPTR", "RECV_BATCH", "SEASON_MASK", "SEASON_OFFSET", "SEASON_SEPTR", "SEASON_YR_MASK", "SEASON_YR_OFFSET", "SEASON_YR_SEPTR", "SEC_DIM_MASK", "SEC_DIM_OFFSET", "SEC_DIM_SEPTR", "SIZE_DESC_MASK", "SIZE_DESC_OFFSET", "SIZE_DESC_SEPTR", "SKU_MASK", "SKU_OFFSET_MASK", "STYLE_MASK", "STYLE_OFFSET", "STYLE_SEPTR", "STYLE_SFX_MASK", "STYLE_SFX_OFFSET", "STYLE_SFX_SEPTR", "UCC_EAN_CO_PFX", "DSP_ITEM_DESC_FLAG", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select cast (cd_master_id as integer) as tc_company_id,
      cast (company_code as varchar (10)) as company_code,
      cast (auto_create_batch_flag as varchar (1))
         as auto_create_batch_flag,
      cast (batch_ctrl_flag as varchar (1)) as batch_ctrl_flag,
      cast (batch_role_id as varchar (15)) as batch_role_id,
      cast (case_lock_code_exp_rec as varchar (2))
         as case_lock_code_exp_rec,
      cast (case_lock_code_held as varchar (2)) as case_lock_code_held,
      cast (color_mask as varchar (4)) as color_mask,
      cast (color_offset as smallint) as color_offset,
      cast (color_septr as varchar (1)) as color_septr,
      cast (color_sfx_mask as varchar (2)) as color_sfx_mask,
      cast (color_sfx_offset as smallint) as color_sfx_offset,
      cast (color_sfx_septr as varchar (1)) as color_sfx_septr,
      cast (dflt_batch_stat_code as smallint) as dflt_batch_stat_code,
      cast (lock_code_invalid as varchar (2)) as lock_code_invalid,
      cast (pick_lock_code_exp_rec as varchar (2))
         as pick_lock_code_exp_rec,
      cast (pick_lock_code_held as varchar (2)) as pick_lock_code_held,
      cast (proc_whse_xfer as varchar (1)) as proc_whse_xfer,
      cast (qual_mask as varchar (1)) as qual_mask,
      cast (qual_offset as smallint) as qual_offset,
      cast (qual_septr as varchar (1)) as qual_septr,
      cast (recv_batch as varchar (1)) as recv_batch,
      cast (season_mask as varchar (2)) as season_mask,
      cast (season_offset as smallint) as season_offset,
      cast (season_septr as varchar (1)) as season_septr,
      cast (season_yr_mask as varchar (2)) as season_yr_mask,
      cast (season_yr_offset as smallint) as season_yr_offset,
      cast (season_yr_septr as varchar (1)) as season_yr_septr,
      cast (sec_dim_mask as varchar (3)) as sec_dim_mask,
      cast (sec_dim_offset as smallint) as sec_dim_offset,
      cast (sec_dim_septr as varchar (1)) as sec_dim_septr,
      cast (size_desc_mask as varchar (15)) as size_desc_mask,
      cast (size_desc_offset as smallint) as size_desc_offset,
      cast (size_desc_septr as varchar (1)) as size_desc_septr,
      cast (sku_mask as varchar (30)) as sku_mask,
      cast (sku_offset_mask as varchar (25)) as sku_offset_mask,
      cast (style_mask as varchar (8)) as style_mask,
      cast (style_offset as smallint) as style_offset,
      cast (style_septr as varchar (1)) as style_septr,
      cast (style_sfx_mask as varchar (8)) as style_sfx_mask,
      cast (style_sfx_offset as smallint) as style_sfx_offset,
      cast (style_sfx_septr as varchar (1)) as style_sfx_septr,
      cast (ucc_ean_co_pfx as varchar (40)) as ucc_ean_co_pfx,
      cast (dsp_item_desc_flag as varchar (4)) as dsp_item_desc_flag,
      cast (create_date_time as timestamp) as create_date_time,
      cast (mod_date_time as timestamp) as mod_date_time,
      cast (user_id as varchar (15)) as user_id
 from (  select c.company_code,
                cdm.cd_master_id,
                max (
                   case
                      when cdmd.column_name = 'AUTO_CREATE_BATCH_FLAG'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as auto_create_batch_flag,
                max (
                   case
                      when cdmd.column_name = 'BATCH_CTRL_FLAG'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as batch_ctrl_flag,
                max (
                   case
                      when cdmd.column_name = 'BATCH_ROLE_ID'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as batch_role_id,
                max (
                   case
                      when cdmd.column_name = 'CASE_LOCK_CODE_EXP_REC'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as case_lock_code_exp_rec,
                max (
                   case
                      when cdmd.column_name = 'CASE_LOCK_CODE_HELD'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as case_lock_code_held,
                max (
                   case
                      when cdmd.column_name = 'COLOR_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as color_mask,
                max (
                   case
                      when cdmd.column_name = 'COLOR_OFFSET'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as color_offset,
                max (
                   case
                      when cdmd.column_name = 'COLOR_SEPTR'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as color_septr,
                max (
                   case
                      when cdmd.column_name = 'COLOR_SFX_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as color_sfx_mask,
                max (
                   case
                      when cdmd.column_name = 'COLOR_SFX_OFFSET'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as color_sfx_offset,
                max (
                   case
                      when cdmd.column_name = 'COLOR_SFX_SEPTR'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as color_sfx_septr,
                max (
                   case
                      when cdmd.column_name = 'DFLT_BATCH_STAT_CODE'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as dflt_batch_stat_code,
                max (
                   case
                      when cdmd.column_name = 'LOCK_CODE_INVALID'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as lock_code_invalid,
                max (
                   case
                      when cdmd.column_name = 'PICK_LOCK_CODE_EXP_REC'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as pick_lock_code_exp_rec,
                max (
                   case
                      when cdmd.column_name = 'PICK_LOCK_CODE_HELD'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as pick_lock_code_held,
                max (
                   case
                      when cdmd.column_name = 'PROC_WHSE_XFER'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as proc_whse_xfer,
                max (
                   case
                      when cdmd.column_name = 'QUAL_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as qual_mask,
                max (
                   case
                      when cdmd.column_name = 'QUAL_OFFSET'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as qual_offset,
                max (
                   case
                      when cdmd.column_name = 'QUAL_SEPTR'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as qual_septr,
                max (
                   case
                      when cdmd.column_name = 'RECV_BATCH'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as recv_batch,
                max (
                   case
                      when cdmd.column_name = 'SEASON_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as season_mask,
                max (
                   case
                      when cdmd.column_name = 'SEASON_OFFSET'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as season_offset,
                max (
                   case
                      when cdmd.column_name = 'SEASON_SEPTR'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as season_septr,
                max (
                   case
                      when cdmd.column_name = 'SEASON_YR_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as season_yr_mask,
                max (
                   case
                      when cdmd.column_name = 'SEASON_YR_OFFSET'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as season_yr_offset,
                max (
                   case
                      when cdmd.column_name = 'SEASON_YR_SEPTR'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as season_yr_septr,
                max (
                   case
                      when cdmd.column_name = 'SEC_DIM_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as sec_dim_mask,
                max (
                   case
                      when cdmd.column_name = 'SEC_DIM_OFFSET'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as sec_dim_offset,
                max (
                   case
                      when cdmd.column_name = 'SEC_DIM_SEPTR'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as sec_dim_septr,
                max (
                   case
                      when cdmd.column_name = 'SIZE_DESC_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as size_desc_mask,
                max (
                   case
                      when cdmd.column_name = 'SIZE_DESC_OFFSET'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as size_desc_offset,
                max (
                   case
                      when cdmd.column_name = 'SIZE_DESC_SEPTR'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as size_desc_septr,
                max (
                   case
                      when cdmd.column_name = 'SKU_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as sku_mask,
                max (
                   case
                      when cdmd.column_name = 'SKU_OFFSET_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as sku_offset_mask,
                max (
                   case
                      when cdmd.column_name = 'STYLE_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as style_mask,
                max (
                   case
                      when cdmd.column_name = 'STYLE_OFFSET'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as style_offset,
                max (
                   case
                      when cdmd.column_name = 'STYLE_SEPTR'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as style_septr,
                max (
                   case
                      when cdmd.column_name = 'STYLE_SFX_MASK'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as style_sfx_mask,
                max (
                   case
                      when cdmd.column_name = 'STYLE_SFX_OFFSET'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as style_sfx_offset,
                max (
                   case
                      when cdmd.column_name = 'STYLE_SFX_SEPTR'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as style_sfx_septr,
                max (
                   case
                      when cdmd.column_name = 'UCC_EAN_CO_PFX'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as ucc_ean_co_pfx,
                max (
                   case
                      when cdmd.column_name = 'DSP_ITEM_DESC_FLAG'
                      then
                         cdmd.column_value
                      else
                         cast (null as varchar (250))
                   end)
                   as dsp_item_desc_flag,
                cdm.create_date_time,
                cdm.mod_date_time,
                cdm.user_id
           from cd_master cdm
                inner join cd_master_dtl cdmd
                   on cdm.cd_master_id = cdmd.cd_master_id
                inner join company c
                   on c.company_id = cdm.cd_master_id
       group by c.company_code,
                cdm.cd_master_id,
                cdm.create_date_time,
                cdm.mod_date_time,
                cdm.user_id) temptable;

CREATE OR REPLACE FORCE VIEW "VW_DB_BUILD_MAX_VERSION" ("VERSION_LABEL", "LAYER", "MAJOR", "MINOR", "REV1", "REV2", "REV3", "START_DTTM") AS
select version_label               ,regexp_substr(version_label,'[^_]+',1,1) layer               ,to_number(regexp_substr(fullver,'[^.]+',1,1)) major               ,to_number(regexp_substr(fullver,'[^.]+',1,2)) minor               ,to_number(regexp_replace(regexp_substr(fullver,'[^.]+',1,3),'[A-Za-z]*','')*1) rev1               ,to_number(coalesce(regexp_substr(fullver,'[^.]+',1,4),'0')) rev2               ,to_number(coalesce(regexp_substr(fullver,'[^.]+',1,5),'0')) rev3               ,start_dttm           from (select version_label                       ,regexp_substr(version_label,'[^_]+',1,2) fullver                       ,start_dttm                 from db_build_history                 where version_label like 'SCPP%'                 or version_label like 'MDA%'                 or version_label like 'CBO%'                 or version_label like 'LMARCH%'                 or (version_label like 'WM%'))            order by 2 desc,3 desc,4 desc,5 desc NULLS LAST,6 desc NULLS LAST,7 desc nulls last, 1 desc;

CREATE OR REPLACE FORCE VIEW "VW_LPN_TRACKING_LOCNS" ("LOCN_ID", "PICK_LOCN_DTL_ID", "ITEM_ID", "ACTL_INVN_QTY", "ACTL_INVN_CASES") AS
select
    lh.locn_id, pld.pick_locn_dtl_id, pld.item_id,
    sum(coalesce(on_hand_qty, 0)) actl_invn_qty,
    sum(case when lpn_id is not null and on_hand_qty > 0 then 1 else 0 end) actl_invn_cases
from pick_locn_dtl pld
join wm_inventory wi on wi.location_dtl_id = pld.pick_locn_dtl_id
join locn_hdr lh on pld.locn_id = lh.locn_id
where
    lh.locn_class in ('A', 'C')
    and exists
        (
        select 1
        from sys_code
        where rec_type = 'B'
            and code_type = '331'
            and code_id = lh.pick_detrm_zone
            and substr(misc_flags, 1, 1) = '2'
        union
        select 1
        from whse_sys_code
        where rec_type = 'B' and code_type = '331' and whse = lh.whse
            and code_id = lh.pick_detrm_zone
            and substr(misc_flags, 1, 1) = '2')
group by lh.locn_id, pld.pick_locn_dtl_id, pld.item_id;

CREATE OR REPLACE FORCE VIEW "VW_PIX_TRAN_CODE" ("PIX_TRAN_CODE_ID", "TRAN_TYPE", "TRAN_CODE", "ACTN_CODE", "PIX_DESC", "REF_CODE_ID_1", "REF_CODE_ID_DESC_1", "REF_CODE_ID_2", "REF_CODE_ID_DESC_2", "REF_CODE_ID_3", "REF_CODE_ID_DESC_3", "REF_CODE_ID_4", "REF_CODE_ID_DESC_4", "REF_CODE_ID_5", "REF_CODE_ID_DESC_5", "REF_CODE_ID_6", "REF_CODE_ID_DESC_6", "REF_CODE_ID_7", "REF_CODE_ID_DESC_7", "REF_CODE_ID_8", "REF_CODE_ID_DESC_8", "REF_CODE_ID_9", "REF_CODE_ID_DESC_9", "REF_CODE_ID_10", "REF_CODE_ID_DESC_10", "PIX_XML_GROUPING_ATTR", "PIX_CREATE_STAT_CODE", "COMPANY_CODE", "TRAN_TYPE_DESC", "TRAN_CODE_DESC", "PIX_CREATE_STATUS_DESC", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select pix_tran_code.pix_tran_code_id,
      pix_tran_code.tran_type,
      pix_tran_code.tran_code,
      pix_tran_code.actn_code,
      pix_tran_code.pix_desc,
      iv.ref_code_id_1,
      iv.ref_code_id_desc_1,
      iv.ref_code_id_2,
      iv.ref_code_id_desc_2,
      iv.ref_code_id_3,
      iv.ref_code_id_desc_3,
      iv.ref_code_id_4,
      iv.ref_code_id_desc_4,
      iv.ref_code_id_5,
      iv.ref_code_id_desc_5,
      iv.ref_code_id_6,
      iv.ref_code_id_desc_6,
      iv.ref_code_id_7,
      iv.ref_code_id_desc_7,
      iv.ref_code_id_8,
      iv.ref_code_id_desc_8,
      iv.ref_code_id_9,
      iv.ref_code_id_desc_9,
      iv.ref_code_id_10,
      iv.ref_code_id_desc_10,
      pix_tran_code.pix_xml_grouping_attr,
      pix_tran_code.pix_create_stat_code,
      company.company_code,
      get_sys_code_desc ('S', '053', tran_type) tran_type_desc,
      get_sys_code_desc ('S', '054', tran_type || tran_code)
         as tran_code_desc,
      get_sys_code_desc ('S',
                         '731',
                         rtrim (cast (pix_create_stat_code as char (2))))
         as pix_create_status_desc,
      pix_tran_code.create_date_time,
      pix_tran_code.mod_date_time,
      pix_tran_code.user_id
 from pix_tran_code
      left outer join company
         on company.company_id = pix_tran_code.cd_master_id
      left outer join (  select pix_tran_code_dtl.pix_tran_code_id,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              1
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_1,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              2
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_2,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              3
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_3,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              4
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_4,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              5
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_5,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              6
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_6,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              7
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_7,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              8
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_8,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              9
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_9,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              10
                                      then
                                         pix_tran_code_dtl.ref_code_id
                                   end)
                                   as ref_code_id_10,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              1
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_1,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              2
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_2,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              3
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_3,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              4
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_4,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              5
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_5,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              6
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_6,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              7
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_7,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              8
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_8,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              9
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_9,
                                min (
                                   case
                                      when pix_tran_code_dtl.pix_tran_code_seq =
                                              10
                                      then
                                         pix_ref_code_master.ref_code_id_desc
                                   end)
                                   as ref_code_id_desc_10
                           from    pix_tran_code_dtl
                                inner join
                                   pix_ref_code_master
                                on pix_ref_code_master.ref_code_id =
                                      pix_tran_code_dtl.ref_code_id
                       group by pix_tran_code_dtl.pix_tran_code_id) iv
         on pix_tran_code.pix_tran_code_id = iv.pix_tran_code_id;

CREATE OR REPLACE FORCE VIEW "VW_REPLENISHABLE_LOCN" ("LOCN_ID", "ITEM_ID", "WHSE", "LOCN_CLASS", "LOCN_SEQ_NBR", "MAX_INVN_QTY", "ON_HAND_QTY", "WM_ALLOCATED_QTY", "TO_BE_FILLED_QTY", "LTST_PICK_ASSIGN_DATE_TIME", "PICK_DETRM_ZONE") AS
SELECT MAX (Pld.Locn_Id),
        MAX (pld.item_id),
        MAX (lh.whse),
        MAX (lh.locn_class),
        MAX (locn_seq_nbr),
        MAX (Max_Invn_Qty),
        SUM (on_hand_qty),
        SUM (wm_allocated_qty),
        SUM (to_be_filled_qty),
        MAX (ltst_pick_assign_date_time),
        MAX (pick_detrm_zone)
   FROM pick_locn_dtl pld
        JOIN wm_inventory wmInv
           ON wmInv.location_dtl_id = pld.pick_locn_dtl_id
        JOIN locn_hdr lh
           ON lh.locn_id = pld.locn_id
  WHERE     trig_repl_for_sku = 'Y'
        AND ltst_sku_assign = 'Y'
        AND Pikng_Lock_Code IS NULL
 GROUP BY Pick_Locn_Dtl_Id
 HAVING (  SUM (On_Hand_Qty)
         + SUM (To_Be_Filled_Qty)
         - SUM (Wm_Allocated_Qty)) <= 0
        AND (  SUM (on_hand_qty)
             + SUM (to_be_filled_qty)
             - SUM (wm_allocated_qty)) < MAX (max_invn_qty)
 UNION
 SELECT pld.locn_id,
      pld.item_id,
      lh.whse,
      lh.locn_class,
      locn_seq_nbr,
      max_invn_qty,
      0 AS on_hand_qty,
      0 AS wm_allocated_qty,
      0 AS to_be_filled_qty,
      ltst_pick_assign_date_time,
      pick_detrm_zone
 FROM pick_locn_dtl pld JOIN locn_hdr lh ON lh.locn_id = pld.locn_id
WHERE     trig_repl_for_sku = 'Y'
      AND ltst_sku_assign = 'Y'
      AND pikng_lock_code IS NULL
      AND NOT EXISTS
             (SELECT 1
                FROM WM_INVENTORY wmInv
               WHERE wmInv.location_dtl_id = pld.pick_locn_dtl_id);

CREATE OR REPLACE FORCE VIEW "VW_WHSE_MASTER" ("WHSE", "WHSE_NAME", "WHSE_ADDR_ID", "ACTV_REPL_ORGN", "ADJ_ALG_FLAG", "AES_FILE_UPLOAD_PATH", "AES_PSWD", "ALLOW_CLOSE_LOAD_HELD_BATCH", "ALLOW_LOAD_HELD_BATCH", "ALLOW_MANIF_HELD_BATCH", "ALLOW_PAKNG_W_OUT_CASE_NBR", "ALLOW_RCV_W_OUT_CASE_ASN", "ALLOW_RCV_W_OUT_CASE_NBR", "ALLOW_RCV_W_OUT_PO_NBR", "ALLOW_RCV_W_OUT_SHPMT", "ALLOW_REPL_TASK_UPDT_PRTY", "ALLOW_RLS_QUAL_HELD_ON_RCPT", "ALLOW_SKU_NOT_ON_PO", "ALLOW_SKU_NOT_ON_SHPMT", "ALLOW_SRL_CREATE", "ALLOW_WEIGH_HELD_BATCH", "ALLOW_WORK_ORD_QTY_CHG", "ALLOW_WORK_ORD_SKU_ADDN", "ASN_CASE_LVL_VERF_PIX", "ASN_SKU_LVL_VERF_PIX", "ASSIGN_AND_PRT_BOL", "ASSIGN_AND_PRT_MANIF", "ASSIGN_EPC_IN_ACTIVE_FLAG", "ASSIGN_PRO_NBR", "AUTO_MANIF_AT_PNH", "BASE_INCUB_FLAG", "BATCH_NBR_EQ_VENDOR_BATCH_NBR", "CALC_CASE_SIZE_TYPE", "CALC_REPL_CTRL", "CALC_TO_BE_LOCD", "CARTON_BREAK_ON_AISLE_CHG", "CARTON_BREAK_ON_AREA_CHG", "CARTON_BREAK_ON_ZONE_CHG", "CARTON_TYPE", "CARTON_WT_TOL", "CASE_ALLOC_STRK", "CASE_NBR_DSP_USAGE", "CASE_WGHT_TOL", "CC_TOLER_UOM", "CC_VAR_TOLER_VALUE", "CHKIN_WINDOW", "CMBIN_REPL_PRTY", "CNTY", "CO_ID", "CO_ID_NAME", "CPCTY_EST_FLAG", "CREATE_ASN_FLAG", "CREATE_LOAD", "CREATE_MAJOR_PKT", "CREATE_SHPMT", "CUBISCAN_IGNORE_COLOR", "CUBISCAN_IGNORE_COLOR_SFX", "CUBISCAN_IGNORE_QUAL", "CUBISCAN_IGNORE_SEC_DIM", "CUBISCAN_IGNORE_SIZE_DESC", "CUBISCAN_IGNORE_STYLE_SFX", "CUBISCAN_OVERWRITE", "DATE_MASK", "DEFAULT_PRICE_TIX_LOCK_CODE", "DFLT_ALLOC_CUTOFF", "DFLT_ALLOC_TYPE", "DFLT_BOL_TYPE", "DFLT_CAPCTY_UOM", "DFLT_CASE_SIZE_TYPE", "DFLT_CONS_DATE", "DFLT_CONS_PRTY_MXD", "DFLT_CONS_PRTY_PARTL", "DFLT_CONS_PRTY_SINGL_SKU_FULL", "DFLT_CONS_SEQ_MXD", "DFLT_CONS_SEQ_PARTL", "DFLT_CONS_SEQ_SINGL_SKU_FULL", "DFLT_DB_QTY_UOM", "DFLT_DB_VOL_UOM", "DFLT_DB_WT_UOM", "DFLT_DSP_QTY_UOM", "DFLT_DSP_VOL_UOM", "DFLT_DSP_WT_UOM", "DFLT_INCUB_LOCK", "DFLT_MANIF_TYPE", "DFLT_PALLET_HT", "DFLT_PIKNG_LOCN_CODE", "DFLT_PLT_TYPE", "DFLT_PUTWY_TYPE", "DFLT_RECALL_CARTON_LOCK", 
"DFLT_SKU_SUB", "DFLT_STOP_SEQ_ORD", "DFLT_UNIT_VALUE", "DFLT_UNIT_VOL", "DFLT_UNIT_WT", "DFLT_UPC_PRE_DIGIT", "DFLT_UPC_VENDOR_CODE", "DUP_SRL_NBR_FLAG", "DWP_INTERFACE_ID", "EAN_ENABLED_FLAG", "EAN_MAX_LEN", "EAN_NBR_OF_SCANS", "EAN_PREFIX", "EAN_SEPARATOR", "ELS_INSTL", "ELS_TASK_ACTVTY_CODE", "ELS_TASK_INTERFACE", "EPC_MATCH_LOCK_CODE", "EPC_REQD_ON_ALL_CARTONS_FLAG", "EPC_REQD_ON_ALL_CASES_FLAG", "ERP_HOST_TYPE", "ERROR_RCPT_PCNT_PO_SKU", "ERROR_RCPT_PCNT_SHMT_PO_SKU", "EST_NBR_CARTONS", "FEDEX_SERVER_IP", "FEDEX_SERVER_PORT", "FIFO_MAINT_FLAG", "FTZ_TRK_FLAG", "GENRT_CARTON_ASN", "GEN_EAN_CNTR_NBR_FLAG", "GEN_ITEM_BRCD", "GET_EPC_W_WAVE_FLAG", "HAZMAT_CNTCT_LINE1", "HAZMAT_CNTCT_LINE2", "HNDL_ATTR_UOM_ACT", "HNDL_ATTR_UOM_CP", "HOT_TASK_PRTY", "I2O5_CASE_QTY_REF", "I2O5_PACK_QTY_REF", "I2O5_PLT_QTY_REF", "I2O5_TIER_QTY_REF", "INCUB_CTRL_FLAG", "INFOLINK_INSTL", "INVC_MODE", "INVN_UPD_ON_CANCEL_PKT", "INVN_UPD_ON_PAKNG_CHG", "INVN_UPD_ON_RESET_WAVE_PKT", "LAST_SLOT_DOWNLOAD_DATE", "LIMIT_USER_IN_TRVL_AISLE_FLAG", "LM_MONITOR_INSTL_FLAG", "LOAD_PLAN", "LOCN_CHK_DIGIT_FLAG", "LOG_FEDEX_TRAN_FLAG", "LOG_LANG_ID", "LOOK_UP_XREF", "LOST_IN_CYCLE_CNT_LOCK_CODE", "LOST_IN_WHSE_LOCK_CODE", "LOST_ON_PLT_LOCK_CODE", "LOST_ON_PUTWY_LOCK_CODE", "LTL_TL_RATE", "MANIF_INCMPL_ORD", "MULT_SKU_CASE_LOCK_CODE", "MXD_SKU_PUTWY_TYPE", "NBR_OF_DYN_ACTV_PICK_PER_SKU", "NBR_OF_DYN_CASE_PICK_PER_SKU", "NBR_OF_WAVE_BEFORE_LETUP", "NBR_OF_WAVE_BEFORE_REUSE", "OB_QA_LVL", "ORGN_ZIP", "OVRD_RCPT_PCNT_PO_SKU", "OVRD_RCPT_PCNT_SHMT_PO_SKU", "OVRPK_BOXES", "OVRPK_CTRL", "OVRPK_PARCL_CARTON", "OVRPK_PCNT", "PENDING_CYCLE_CNT_LOCK_CODE", "PHYS_INVN_CNT_ADJ_RSN_CODE", "PICK_LOCN_ASSIGN_SEQ_FLAG", "PKT_STAT_CHG_PIX", "PLT_TYPE", "PO_SKU_LVL_VERF_PIX", "PPICK_REPL_CTRL", "PRICE_TKT_TYPE", "PRT_CONTENT_LBL_ON_CTN_MODIFY", "PRT_PACK_SLIP_ON_CTN_MODIFY", "PRT_PKT_W_WAVE", "PURGE_APPT_STAT_CODE", "PURGE_ASN_STAT_CODE", "PURGE_CASE_BEYOND_STAT_CODE", "PUTWY_LOCN_LOCK_CODE", 
"QUAL_HELD_LOCK_CODE", "REPL_ACTV_W_WAVE", "REPL_CASE_PICK_W_WAVE", "REPL_TASK_UPDT_PRTY", "REPORT_TRANS", "RLS_HELD_RPLN_TASKS", "RTE_WITH_WAVE", "SAVE_RTE_WAVE_UNDO_INFO", "SKU_CNSTR", "SKU_LEVEL", "SLOTINFO_CRIT_NBR", "SLOTINFO_DOWNLOAD_FILE_PATH", "SLOTINFO_ONE_USER_PER_GRP", "SLOTINFO_TEMP_LOCN_ID", "SLOTINFO_UPLOAD_FILE_PATH", "SLOT_IT_USED", "SMARTINFO_INSTL", "SORT_ID_CODES", "SRL_NBR_LIST_MAX", "SRL_PIX_FLAG", "SRL_TRK_FLAG", "SUPPRESS_CTN_LBL", "SYS_TIME_ZONE", "TASK_EXCEPTION_FLAG", "TASK_PRIORITIZATION", "TI_UPD_ON_CANCEL_PKT", "TI_UPD_ON_PAKNG_CHG", "TI_UPD_ON_RESET_WAVE_PKT", "TRACK_OB_RETN_LEVEL", "TRIG_INVC_PKT_STS", "TRK_ACTIVE_MULT_PACK_QTY", "TRK_ACTV_BY_BATCH_NBR", "TRK_ACTV_BY_CNTRY_OF_ORGN", "TRK_ACTV_BY_ID_CODES", "TRK_ACTV_BY_PROD_STAT", "TRK_CASE_PICK_BY_BATCH_NBR", "TRK_CASE_PICK_BY_CNTRY_OF_ORGN", "TRK_CASE_PICK_BY_ID_CODE", "TRK_CASE_PICK_BY_PROD_STAT", "TRK_MULT_PACK_QTY", "TRK_PROD_FLAG", "UPD_ALLOC_ON_PICK_WAVE", "USE_APPT_DURTN_FOR_DOCK_DOOR", "USE_INBD_LPN_AS_OUTBD_LPN", "USE_LOCK_CODE_PRTY_PIX", "USE_LOCN_ID_AS_BRCD", "VENDOR_PERFRM_PIX", "VERF_CARTON_CONTNT", "VICS_BOL_FLAG", "VOCOLLECT_PACK_CNTR_TRACK_FLAG", "VOCOLLECT_PACK_LUT_FLAG", "VOCOLLECT_PACK_WORK_ID_DESC", "VOCOLLECT_PTS_LUT_FLAG", "VOCOLLECT_PTS_WORK_ID_DESC", "WARN_RCPT_PCNT_PO_SKU", "WARN_RCPT_PCNT_SHMT_PO_SKU", "WAVE_SUSPND_OPTN", "WHEN_TO_PRT_CTN_CONTENT_LBL", "WHEN_TO_PRT_CTN_SHIP_LBL", "WHEN_TO_PRT_PAKNG_SLIP", "WHEN_TO_PRT_PLT_LBL", "WHSE_TIME_ZONE", "WORKINFO_LOG", "WORKINFO_SERVER_FACTORY_NAME", "WORKINFO_TRAVEL_ACTVTY", "XCESS_WAVE_NEED_PROC_TYPE", "XFER_MAX_UOM", "YARD_JOCK_TASK", "ZONE_PAYMENT_STAT", "ZONE_SKIP", "ALLOW_MIXED_CD_IN_RESV_FLAG", "CLS_TIMEZONE_ID", "UPS_ADF_ACTV", "UPS_ADF_FILE_UPLOAD_PATH", "UPS_EMT_URL", "UPS_EMT_REMOTE_PATH", "UPS_EMT_USER", "UPS_EMT_PSWD", "UPS_EMT_FILE_UPLOAD_PATH", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "ACTV_LOCN_THRESHOLD_PCNT", "CASE_PICK_LOCN_THRESHOLD_PCNT", "AUTO_GEN_CC_TASK", 
"QUAL_AUD_FAILED_LOCK_CODE", "MAX_CC_RECNT", "DELV_CONFIRMATION_ENABLED", "DISTRO_SEQ_METHOD", "FLOWTHRU_ALLOC_METHOD") AS
select cast (whse as varchar2 (3)) as whse,
      cast (whse_name as varchar2 (30)) as whse_name,
      cast (whse_addr_id as number (9)) as whse_addr_id,
      cast (actv_repl_orgn as varchar2 (1)) as actv_repl_orgn,
      cast (adj_alg_flag as varchar2 (1)) as adj_alg_flag,
      cast (aes_file_upload_path as varchar2 (250))
         as aes_file_upload_path,
      cast (aes_pswd as varchar2 (6)) as aes_pswd,
      cast (allow_close_load_held_batch as varchar2 (1))
         as allow_close_load_held_batch,
      cast (allow_load_held_batch as varchar2 (1))
         as allow_load_held_batch,
      cast (allow_manif_held_batch as varchar2 (1))
         as allow_manif_held_batch,
      cast (allow_pakng_w_out_case_nbr as varchar2 (1))
         as allow_pakng_w_out_case_nbr,
      cast (allow_rcv_w_out_case_asn as varchar2 (1))
         as allow_rcv_w_out_case_asn,
      cast (allow_rcv_w_out_case_nbr as varchar2 (1))
         as allow_rcv_w_out_case_nbr,
      cast (allow_rcv_w_out_po_nbr as varchar2 (1))
         as allow_rcv_w_out_po_nbr,
      cast (allow_rcv_w_out_shpmt as varchar2 (1))
         as allow_rcv_w_out_shpmt,
      cast (allow_repl_task_updt_prty as varchar2 (1))
         as allow_repl_task_updt_prty,
      cast (allow_rls_qual_held_on_rcpt as varchar2 (1))
         as allow_rls_qual_held_on_rcpt,
      cast (allow_sku_not_on_po as varchar2 (1)) as allow_sku_not_on_po,
      cast (allow_sku_not_on_shpmt as varchar2 (1))
         as allow_sku_not_on_shpmt,
      cast (allow_srl_create as number (1)) as allow_srl_create,
      cast (allow_weigh_held_batch as varchar2 (1))
         as allow_weigh_held_batch,
      cast (allow_work_ord_qty_chg as varchar2 (1))
         as allow_work_ord_qty_chg,
      cast (allow_work_ord_sku_addn as varchar2 (1))
         as allow_work_ord_sku_addn,
      cast (asn_case_lvl_verf_pix as varchar2 (1))
         as asn_case_lvl_verf_pix,
      cast (asn_sku_lvl_verf_pix as varchar2 (1)) as asn_sku_lvl_verf_pix,
      cast (assign_and_prt_bol as varchar2 (1)) as assign_and_prt_bol,
      cast (assign_and_prt_manif as varchar2 (1)) as assign_and_prt_manif,
      cast (assign_epc_in_active_flag as varchar2 (1))
         as assign_epc_in_active_flag,
      cast (assign_pro_nbr as varchar2 (1)) as assign_pro_nbr,
      cast (auto_manif_at_pnh as varchar2 (1)) as auto_manif_at_pnh,
      cast (base_incub_flag as varchar2 (2)) as base_incub_flag,
      cast (batch_nbr_eq_vendor_batch_nbr as varchar2 (1))
         as batch_nbr_eq_vendor_batch_nbr,
      cast (calc_case_size_type as varchar2 (1)) as calc_case_size_type,
      cast (calc_repl_ctrl as varchar2 (1)) as calc_repl_ctrl,
      cast (calc_to_be_locd as varchar2 (1)) as calc_to_be_locd,
      cast (carton_break_on_aisle_chg as varchar2 (1))
         as carton_break_on_aisle_chg,
      cast (carton_break_on_area_chg as varchar2 (1))
         as carton_break_on_area_chg,
      cast (carton_break_on_zone_chg as varchar2 (1))
         as carton_break_on_zone_chg,
      cast (carton_type as varchar2 (3)) as carton_type,
      cast (carton_wt_tol as number (5, 2)) as carton_wt_tol,
      cast (case_alloc_strk as number (3)) as case_alloc_strk,
      cast (case_nbr_dsp_usage as varchar2 (1)) as case_nbr_dsp_usage,
      cast (case_wght_tol as number (5, 2)) as case_wght_tol,
      cast (cc_toler_uom as varchar2 (1)) as cc_toler_uom,
      cast (cc_var_toler_value as number (10)) as cc_var_toler_value,
      cast (chkin_window as varchar2 (5)) as chkin_window,
      cast (cmbin_repl_prty as varchar2 (1)) as cmbin_repl_prty,
      cast (cnty as varchar2 (40)) as cnty,
      cast (co_id as number (8)) as co_id,
      cast (co_id_name as varchar2 (30)) as co_id_name,
      cast (cpcty_est_flag as number (1)) as cpcty_est_flag,
      cast (create_asn_flag as varchar2 (1)) as create_asn_flag,
      cast (create_load as varchar2 (1)) as create_load,
      cast (create_major_pkt as varchar2 (1)) as create_major_pkt,
      cast (create_shpmt as varchar2 (1)) as create_shpmt,
      cast (cubiscan_ignore_color as number (1)) as cubiscan_ignore_color,
      cast (cubiscan_ignore_color_sfx as number (1))
         as cubiscan_ignore_color_sfx,
      cast (cubiscan_ignore_qual as number (1)) as cubiscan_ignore_qual,
      cast (cubiscan_ignore_sec_dim as number (1))
         as cubiscan_ignore_sec_dim,
      cast (cubiscan_ignore_size_desc as number (1))
         as cubiscan_ignore_size_desc,
      cast (cubiscan_ignore_style_sfx as number (1))
         as cubiscan_ignore_style_sfx,
      cast (cubiscan_overwrite as number (1)) as cubiscan_overwrite,
      cast (date_mask as varchar2 (11)) as date_mask,
      cast (default_price_tix_lock_code as varchar2 (2))
         as default_price_tix_lock_code,
      cast (dflt_alloc_cutoff as number (3)) as dflt_alloc_cutoff,
      cast (dflt_alloc_type as varchar2 (3)) as dflt_alloc_type,
      cast (dflt_bol_type as varchar2 (1)) as dflt_bol_type,
      cast (dflt_capcty_uom as varchar2 (1)) as dflt_capcty_uom,
      cast (dflt_case_size_type as varchar2 (3)) as dflt_case_size_type,
      cast (dflt_cons_date as date) as dflt_cons_date,
      cast (dflt_cons_prty_mxd as varchar2 (3)) as dflt_cons_prty_mxd,
      cast (dflt_cons_prty_partl as varchar2 (3)) as dflt_cons_prty_partl,
      cast (dflt_cons_prty_singl_sku_full as varchar2 (3))
         as dflt_cons_prty_singl_sku_full,
      cast (dflt_cons_seq_mxd as varchar2 (3)) as dflt_cons_seq_mxd,
      cast (dflt_cons_seq_partl as varchar2 (3)) as dflt_cons_seq_partl,
      cast (dflt_cons_seq_singl_sku_full as varchar2 (3))
         as dflt_cons_seq_singl_sku_full,
      cast (dflt_db_qty_uom as varchar2 (2)) as dflt_db_qty_uom,
      cast (dflt_db_vol_uom as varchar2 (2)) as dflt_db_vol_uom,
      cast (dflt_db_wt_uom as varchar2 (2)) as dflt_db_wt_uom,
      cast (dflt_dsp_qty_uom as varchar2 (2)) as dflt_dsp_qty_uom,
      cast (dflt_dsp_vol_uom as varchar2 (2)) as dflt_dsp_vol_uom,
      cast (dflt_dsp_wt_uom as varchar2 (2)) as dflt_dsp_wt_uom,
      cast (dflt_incub_lock as varchar2 (2)) as dflt_incub_lock,
      cast (dflt_manif_type as varchar2 (4)) as dflt_manif_type,
      cast (dflt_pallet_ht as number (7, 2)) as dflt_pallet_ht,
      cast (dflt_pikng_locn_code as varchar2 (2)) as dflt_pikng_locn_code,
      cast (dflt_plt_type as varchar2 (3)) as dflt_plt_type,
      cast (dflt_putwy_type as varchar2 (3)) as dflt_putwy_type,
      cast (dflt_recall_carton_lock as varchar2 (2))
         as dflt_recall_carton_lock,
      cast (dflt_sku_sub as varchar2 (1)) as dflt_sku_sub,
      cast (dflt_stop_seq_ord as varchar2 (1)) as dflt_stop_seq_ord,
      cast (dflt_unit_value as number (9, 2)) as dflt_unit_value,
      cast (dflt_unit_vol as number (13, 4)) as dflt_unit_vol,
      cast (dflt_unit_wt as number (13, 4)) as dflt_unit_wt,
      cast (dflt_upc_pre_digit as varchar2 (1)) as dflt_upc_pre_digit,
      cast (dflt_upc_vendor_code as varchar2 (5)) as dflt_upc_vendor_code,
      cast (dup_srl_nbr_flag as number (1)) as dup_srl_nbr_flag,
      cast (dwp_interface_id as number (10)) as dwp_interface_id,
      cast (ean_enabled_flag as number (1)) as ean_enabled_flag,
      cast (ean_max_len as number (2)) as ean_max_len,
      cast (ean_nbr_of_scans as number (1)) as ean_nbr_of_scans,
      cast (ean_prefix as varchar2 (3)) as ean_prefix,
      cast (ean_separator as varchar2 (3)) as ean_separator,
      cast (els_instl as varchar2 (1)) as els_instl,
      cast (els_task_actvty_code as varchar2 (15))
         as els_task_actvty_code,
      cast (els_task_interface as varchar2 (1)) as els_task_interface,
      cast (epc_match_lock_code as varchar2 (2)) as epc_match_lock_code,
      cast (epc_reqd_on_all_cartons_flag as varchar2 (1))
         as epc_reqd_on_all_cartons_flag,
      cast (epc_reqd_on_all_cases_flag as varchar2 (1))
         as epc_reqd_on_all_cases_flag,
      cast (erp_host_type as varchar2 (1)) as erp_host_type,
      cast (error_rcpt_pcnt_po_sku as number (5, 2))
         as error_rcpt_pcnt_po_sku,
      cast (error_rcpt_pcnt_shmt_po_sku as number (5, 2))
         as error_rcpt_pcnt_shmt_po_sku,
      cast (est_nbr_cartons as number (1)) as est_nbr_cartons,
      cast (fedex_server_ip as varchar2 (15)) as fedex_server_ip,
      cast (fedex_server_port as varchar2 (10)) as fedex_server_port,
      cast (fifo_maint_flag as number (1)) as fifo_maint_flag,
      cast (ftz_trk_flag as varchar2 (1)) as ftz_trk_flag,
      cast (genrt_carton_asn as varchar2 (1)) as genrt_carton_asn,
      cast (gen_ean_cntr_nbr_flag as varchar2 (1))
         as gen_ean_cntr_nbr_flag,
      cast (gen_item_brcd as varchar2 (20)) as gen_item_brcd,
      cast (get_epc_w_wave_flag as varchar2 (1)) as get_epc_w_wave_flag,
      cast (hazmat_cntct_line1 as varchar2 (100)) as hazmat_cntct_line1,
      cast (hazmat_cntct_line2 as varchar2 (100)) as hazmat_cntct_line2,
      cast (hndl_attr_uom_act as varchar2 (2)) as hndl_attr_uom_act,
      cast (hndl_attr_uom_cp as varchar2 (2)) as hndl_attr_uom_cp,
      cast (hot_task_prty as number (5)) as hot_task_prty,
      cast (i2o5_case_qty_ref as varchar2 (1)) as i2o5_case_qty_ref,
      cast (i2o5_pack_qty_ref as varchar2 (1)) as i2o5_pack_qty_ref,
      cast (i2o5_plt_qty_ref as varchar2 (1)) as i2o5_plt_qty_ref,
      cast (i2o5_tier_qty_ref as varchar2 (1)) as i2o5_tier_qty_ref,
      cast (incub_ctrl_flag as varchar2 (1)) as incub_ctrl_flag,
      cast (infolink_instl as varchar2 (15)) as infolink_instl,
      cast (invc_mode as number (1)) as invc_mode,
      cast (invn_upd_on_cancel_pkt as varchar2 (2))
         as invn_upd_on_cancel_pkt,
      cast (invn_upd_on_pakng_chg as varchar2 (2))
         as invn_upd_on_pakng_chg,
      cast (invn_upd_on_reset_wave_pkt as varchar2 (2))
         as invn_upd_on_reset_wave_pkt,
      cast (last_slot_download_date as date) as last_slot_download_date,
      cast (limit_user_in_trvl_aisle_flag as varchar2 (1))
         as limit_user_in_trvl_aisle_flag,
      cast (lm_monitor_instl_flag as varchar2 (1))
         as lm_monitor_instl_flag,
      cast (load_plan as varchar2 (1)) as load_plan,
      cast (locn_chk_digit_flag as number (1)) as locn_chk_digit_flag,
      cast (log_fedex_tran_flag as varchar2 (1)) as log_fedex_tran_flag,
      cast (log_lang_id as varchar2 (3)) as log_lang_id,
      cast (look_up_xref as varchar2 (1)) as look_up_xref,
      cast (lost_in_cycle_cnt_lock_code as varchar2 (2))
         as lost_in_cycle_cnt_lock_code,
      cast (lost_in_whse_lock_code as varchar2 (2))
         as lost_in_whse_lock_code,
      cast (lost_on_plt_lock_code as varchar2 (2))
         as lost_on_plt_lock_code,
      cast (lost_on_putwy_lock_code as varchar2 (2))
         as lost_on_putwy_lock_code,
      cast (ltl_tl_rate as varchar2 (1)) as ltl_tl_rate,
      cast (manif_incmpl_ord as varchar2 (1)) as manif_incmpl_ord,
      cast (mult_sku_case_lock_code as varchar2 (2))
         as mult_sku_case_lock_code,
      cast (mxd_sku_putwy_type as varchar2 (3)) as mxd_sku_putwy_type,
      cast (nbr_of_dyn_actv_pick_per_sku as number (3))
         as nbr_of_dyn_actv_pick_per_sku,
      cast (nbr_of_dyn_case_pick_per_sku as number (3))
         as nbr_of_dyn_case_pick_per_sku,
      cast (nbr_of_wave_before_letup as number (3))
         as nbr_of_wave_before_letup,
      cast (nbr_of_wave_before_reuse as number (3))
         as nbr_of_wave_before_reuse,
      cast (ob_qa_lvl as number (1)) as ob_qa_lvl,
      cast (orgn_zip as varchar2 (11)) as orgn_zip,
      cast (ovrd_rcpt_pcnt_po_sku as number (5, 2))
         as ovrd_rcpt_pcnt_po_sku,
      cast (ovrd_rcpt_pcnt_shmt_po_sku as number (5, 2))
         as ovrd_rcpt_pcnt_shmt_po_sku,
      cast (ovrpk_boxes as number (3)) as ovrpk_boxes,
      cast (ovrpk_ctrl as varchar2 (1)) as ovrpk_ctrl,
      cast (ovrpk_parcl_carton as varchar2 (1)) as ovrpk_parcl_carton,
      cast (ovrpk_pcnt as number (5, 2)) as ovrpk_pcnt,
      cast (pending_cycle_cnt_lock_code as varchar2 (2))
         as pending_cycle_cnt_lock_code,
      cast (phys_invn_cnt_adj_rsn_code as varchar2 (2))
         as phys_invn_cnt_adj_rsn_code,
      cast (pick_locn_assign_seq_flag as varchar2 (1))
         as pick_locn_assign_seq_flag,
      cast (pkt_stat_chg_pix as varchar2 (1)) as pkt_stat_chg_pix,
      cast (plt_type as varchar2 (3)) as plt_type,
      cast (po_sku_lvl_verf_pix as varchar2 (1)) as po_sku_lvl_verf_pix,
      cast (ppick_repl_ctrl as varchar2 (1)) as ppick_repl_ctrl,
      cast (price_tkt_type as varchar2 (3)) as price_tkt_type,
      cast (prt_content_lbl_on_ctn_modify as varchar2 (1))
         as prt_content_lbl_on_ctn_modify,
      cast (prt_pack_slip_on_ctn_modify as varchar2 (1))
         as prt_pack_slip_on_ctn_modify,
      cast (prt_pkt_w_wave as varchar2 (1)) as prt_pkt_w_wave,
      cast (purge_appt_stat_code as number (2)) as purge_appt_stat_code,
      cast (purge_asn_stat_code as number (2)) as purge_asn_stat_code,
      cast (purge_case_beyond_stat_code as number (2))
         as purge_case_beyond_stat_code,
      cast (putwy_locn_lock_code as varchar2 (2)) as putwy_locn_lock_code,
      cast (qual_held_lock_code as varchar2 (2)) as qual_held_lock_code,
      cast (repl_actv_w_wave as varchar2 (1)) as repl_actv_w_wave,
      cast (repl_case_pick_w_wave as varchar2 (1))
         as repl_case_pick_w_wave,
      cast (repl_task_updt_prty as number (2)) as repl_task_updt_prty,
      cast (report_trans as varchar2 (1)) as report_trans,
      cast (rls_held_rpln_tasks as varchar2 (1)) as rls_held_rpln_tasks,
      cast (rte_with_wave as varchar2 (1)) as rte_with_wave,
      cast (save_rte_wave_undo_info as varchar2 (1))
         as save_rte_wave_undo_info,
      cast (sku_cnstr as varchar2 (1)) as sku_cnstr,
      cast (sku_level as number (1)) as sku_level,
      cast (slotinfo_crit_nbr as varchar2 (10)) as slotinfo_crit_nbr,
      cast (slotinfo_download_file_path as varchar2 (100))
         as slotinfo_download_file_path,
      cast (slotinfo_one_user_per_grp as varchar2 (1))
         as slotinfo_one_user_per_grp,
      cast (slotinfo_temp_locn_id as varchar2 (10))
         as slotinfo_temp_locn_id,
      cast (slotinfo_upload_file_path as varchar2 (100))
         as slotinfo_upload_file_path,
      cast (slot_it_used as varchar2 (1)) as slot_it_used,
      cast (smartinfo_instl as varchar2 (15)) as smartinfo_instl,
      cast (sort_id_codes as varchar2 (1)) as sort_id_codes,
      cast (srl_nbr_list_max as number (5)) as srl_nbr_list_max,
      cast (srl_pix_flag as number (1)) as srl_pix_flag,
      cast (srl_trk_flag as number (1)) as srl_trk_flag,
      cast (suppress_ctn_lbl as varchar2 (1)) as suppress_ctn_lbl,
      cast (sys_time_zone as varchar2 (3)) as sys_time_zone,
      cast (task_exception_flag as number (1)) as task_exception_flag,
      cast (task_prioritization as varchar2 (1)) as task_prioritization,
      cast (ti_upd_on_cancel_pkt as number (3)) as ti_upd_on_cancel_pkt,
      cast (ti_upd_on_pakng_chg as number (3)) as ti_upd_on_pakng_chg,
      cast (ti_upd_on_reset_wave_pkt as number (3))
         as ti_upd_on_reset_wave_pkt,
      cast (track_ob_retn_level as varchar2 (1)) as track_ob_retn_level,
      cast (trig_invc_pkt_sts as number (2)) as trig_invc_pkt_sts,
      cast (trk_active_mult_pack_qty as varchar2 (1))
         as trk_active_mult_pack_qty,
      cast (trk_actv_by_batch_nbr as varchar2 (1))
         as trk_actv_by_batch_nbr,
      cast (trk_actv_by_cntry_of_orgn as varchar2 (1))
         as trk_actv_by_cntry_of_orgn,
      cast (trk_actv_by_id_codes as varchar2 (1)) as trk_actv_by_id_codes,
      cast (trk_actv_by_prod_stat as varchar2 (1))
         as trk_actv_by_prod_stat,
      cast (trk_case_pick_by_batch_nbr as varchar2 (1))
         as trk_case_pick_by_batch_nbr,
      cast (trk_case_pick_by_cntry_of_orgn as varchar2 (1))
         as trk_case_pick_by_cntry_of_orgn,
      cast (trk_case_pick_by_id_code as varchar2 (1))
         as trk_case_pick_by_id_code,
      cast (trk_case_pick_by_prod_stat as varchar2 (1))
         as trk_case_pick_by_prod_stat,
      cast (trk_mult_pack_qty as varchar2 (1)) as trk_mult_pack_qty,
      cast (trk_prod_flag as varchar2 (1)) as trk_prod_flag,
      cast (upd_alloc_on_pick_wave as varchar2 (1))
         as upd_alloc_on_pick_wave,
      cast (use_appt_durtn_for_dock_door as varchar2 (1))
         as use_appt_durtn_for_dock_door,
      cast (use_inbd_lpn_as_outbd_lpn as varchar2 (1))
         as use_inbd_lpn_as_outbd_lpn,
      cast (use_lock_code_prty_pix as varchar2 (1))
         as use_lock_code_prty_pix,
      cast (use_locn_id_as_brcd as number (1)) as use_locn_id_as_brcd,
      cast (vendor_perfrm_pix as varchar2 (1)) as vendor_perfrm_pix,
      cast (verf_carton_contnt as varchar2 (1)) as verf_carton_contnt,
      cast (vics_bol_flag as varchar2 (1)) as vics_bol_flag,
      cast (vocollect_pack_cntr_track_flag as varchar2 (1))
         as vocollect_pack_cntr_track_flag,
      cast (vocollect_pack_lut_flag as varchar2 (1))
         as vocollect_pack_lut_flag,
      cast (vocollect_pack_work_id_desc as varchar2 (30))
         as vocollect_pack_work_id_desc,
      cast (vocollect_pts_lut_flag as varchar2 (1))
         as vocollect_pts_lut_flag,
      cast (vocollect_pts_work_id_desc as varchar2 (30))
         as vocollect_pts_work_id_desc,
      cast (warn_rcpt_pcnt_po_sku as number (5, 2))
         as warn_rcpt_pcnt_po_sku,
      cast (warn_rcpt_pcnt_shmt_po_sku as number (5, 2))
         as warn_rcpt_pcnt_shmt_po_sku,
      cast (wave_suspnd_optn as varchar2 (1)) as wave_suspnd_optn,
      cast (when_to_prt_ctn_content_lbl as varchar2 (1))
         as when_to_prt_ctn_content_lbl,
      cast (when_to_prt_ctn_ship_lbl as varchar2 (1))
         as when_to_prt_ctn_ship_lbl,
      cast (when_to_prt_pakng_slip as varchar2 (1))
         as when_to_prt_pakng_slip,
      cast (when_to_prt_plt_lbl as number (1)) as when_to_prt_plt_lbl,
      cast (whse_time_zone as varchar2 (3)) as whse_time_zone,
      cast (workinfo_log as varchar2 (1)) as workinfo_log,
      cast (workinfo_server_factory_name as varchar2 (100))
         as workinfo_server_factory_name,
      cast (workinfo_travel_actvty as varchar2 (15))
         as workinfo_travel_actvty,
      cast (xcess_wave_need_proc_type as number (1))
         as xcess_wave_need_proc_type,
      cast (xfer_max_uom as varchar2 (1)) as xfer_max_uom,
      cast (yard_jock_task as varchar2 (1)) as yard_jock_task,
      cast (zone_payment_stat as varchar2 (2)) as zone_payment_stat,
      cast (zone_skip as varchar2 (1)) as zone_skip,
      cast (allow_mixed_cd_in_resv_flag as char (1))
         as allow_mixed_cd_in_resv_flag,
      cast (cls_timezone_id as number (9)) as cls_timezone_id,
      cast (ups_adf_actv as char (1)) as ups_adf_actv,
      cast (ups_adf_file_upload_path as varchar (50))
         as ups_adf_file_upload_path,
      cast (ups_emt_url as varchar (50)) as ups_emt_url,
      cast (ups_emt_remote_path as varchar (50)) as ups_emt_remote_path,
      cast (ups_emt_user as varchar (40)) as ups_emt_user,
      cast (ups_emt_pswd as varchar (40)) as ups_emt_pswd,
      cast (ups_emt_file_upload_path as varchar (50))
         as ups_emt_file_upload_path,
      cast (create_date_time as date) as create_date_time,
      cast (mod_date_time as date) as mod_date_time,
      cast (user_id as varchar2 (15)) as user_id,
      cast (actv_locn_threshold_pcnt as number (3))
         as actv_locn_threshold_pcnt,
      cast (case_pick_locn_threshold_pcnt as number (3))
         as case_pick_locn_threshold_pcnt,
      cast (auto_gen_cc_task as varchar2 (1)) as auto_gen_cc_task,
      cast (qual_aud_failed_lock_code as varchar2 (2))
         as qual_aud_failed_lock_code,
      cast (max_cc_recnt as number (2)) as max_cc_recnt,
      cast (delv_confirmation_enabled as varchar2 (1))
         as delv_confirmation_enabled,
      cast (distro_seq_method as varchar2 (1)) as distro_seq_method,
      cast (flowthru_alloc_method as varchar2 (1))
         as flowthru_alloc_method
 from (  select wm.whse,
                wm.whse_name,
                wm.whse_addr_id,
                wm.cls_timezone_id,
                max (
                   case
                      when wmd.column_name = 'ACTV_REPL_ORGN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   actv_repl_orgn,
                max (
                   case
                      when wmd.column_name = 'ADJ_ALG_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   adj_alg_flag,
                max (
                   case
                      when wmd.column_name = 'AES_FILE_UPLOAD_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   aes_file_upload_path,
                max (
                   case
                      when wmd.column_name = 'AES_PSWD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   aes_pswd,
                max (
                   case
                      when wmd.column_name = 'ALLOW_CLOSE_LOAD_HELD_BATCH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_close_load_held_batch,
                max (
                   case
                      when wmd.column_name = 'ALLOW_LOAD_HELD_BATCH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_load_held_batch,
                max (
                   case
                      when wmd.column_name = 'ALLOW_MANIF_HELD_BATCH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_manif_held_batch,
                max (
                   case
                      when wmd.column_name = 'ALLOW_PAKNG_W_OUT_CASE_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_pakng_w_out_case_nbr,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RCV_W_OUT_CASE_ASN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rcv_w_out_case_asn,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RCV_W_OUT_CASE_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rcv_w_out_case_nbr,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RCV_W_OUT_PO_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rcv_w_out_po_nbr,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RCV_W_OUT_SHPMT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rcv_w_out_shpmt,
                max (
                   case
                      when wmd.column_name = 'ALLOW_REPL_TASK_UPDT_PRTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_repl_task_updt_prty,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RLS_QUAL_HELD_ON_RCPT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rls_qual_held_on_rcpt,
                max (
                   case
                      when wmd.column_name = 'ALLOW_SKU_NOT_ON_PO'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_sku_not_on_po,
                max (
                   case
                      when wmd.column_name = 'ALLOW_SKU_NOT_ON_SHPMT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_sku_not_on_shpmt,
                max (
                   case
                      when wmd.column_name = 'ALLOW_SRL_CREATE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_srl_create,
                max (
                   case
                      when wmd.column_name = 'ALLOW_WEIGH_HELD_BATCH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_weigh_held_batch,
                max (
                   case
                      when wmd.column_name = 'ALLOW_WORK_ORD_QTY_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_work_ord_qty_chg,
                max (
                   case
                      when wmd.column_name = 'ALLOW_WORK_ORD_SKU_ADDN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_work_ord_sku_addn,
                max (
                   case
                      when wmd.column_name = 'ASN_CASE_LVL_VERF_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   asn_case_lvl_verf_pix,
                max (
                   case
                      when wmd.column_name = 'ASN_SKU_LVL_VERF_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   asn_sku_lvl_verf_pix,
                max (
                   case
                      when wmd.column_name = 'ASSIGN_AND_PRT_BOL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   assign_and_prt_bol,
                max (
                   case
                      when wmd.column_name = 'ASSIGN_AND_PRT_MANIF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   assign_and_prt_manif,
                max (
                   case
                      when wmd.column_name = 'ASSIGN_EPC_IN_ACTIVE_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   assign_epc_in_active_flag,
                max (
                   case
                      when wmd.column_name = 'ASSIGN_PRO_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   assign_pro_nbr,
                max (
                   case
                      when wmd.column_name = 'AUTO_MANIF_AT_PNH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   auto_manif_at_pnh,
                max (
                   case
                      when wmd.column_name = 'BASE_INCUB_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   base_incub_flag,
                max (
                   case
                      when wmd.column_name =
                              'BATCH_NBR_EQ_VENDOR_BATCH_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   batch_nbr_eq_vendor_batch_nbr,
                max (
                   case
                      when wmd.column_name = 'CALC_CASE_SIZE_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   calc_case_size_type,
                max (
                   case
                      when wmd.column_name = 'CALC_REPL_CTRL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   calc_repl_ctrl,
                max (
                   case
                      when wmd.column_name = 'CALC_TO_BE_LOCD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   calc_to_be_locd,
                max (
                   case
                      when wmd.column_name = 'CARTON_BREAK_ON_AISLE_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_break_on_aisle_chg,
                max (
                   case
                      when wmd.column_name = 'CARTON_BREAK_ON_AREA_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_break_on_area_chg,
                max (
                   case
                      when wmd.column_name = 'CARTON_BREAK_ON_ZONE_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_break_on_zone_chg,
                max (
                   case
                      when wmd.column_name = 'CARTON_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_type,
                max (
                   case
                      when wmd.column_name = 'CARTON_WT_TOL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_wt_tol,
                max (
                   case
                      when wmd.column_name = 'CASE_ALLOC_STRK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   case_alloc_strk,
                max (
                   case
                      when wmd.column_name = 'CASE_NBR_DSP_USAGE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   case_nbr_dsp_usage,
                max (
                   case
                      when wmd.column_name = 'CASE_WGHT_TOL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   case_wght_tol,
                max (
                   case
                      when wmd.column_name = 'CC_TOLER_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cc_toler_uom,
                max (
                   case
                      when wmd.column_name = 'CC_VAR_TOLER_VALUE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cc_var_toler_value,
                max (
                   case
                      when wmd.column_name = 'CHKIN_WINDOW'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   chkin_window,
                max (
                   case
                      when wmd.column_name = 'CMBIN_REPL_PRTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cmbin_repl_prty,
                max (
                   case
                      when wmd.column_name = 'CNTY' then wmd.column_value
                      else null
                   end)
                   cnty,
                max (
                   case
                      when wmd.column_name = 'CO_ID' then wmd.column_value
                      else null
                   end)
                   co_id,
                max (
                   case
                      when wmd.column_name = 'CO_ID_NAME'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   co_id_name,
                max (
                   case
                      when wmd.column_name = 'CPCTY_EST_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cpcty_est_flag,
                max (
                   case
                      when wmd.column_name = 'CREATE_ASN_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   create_asn_flag,
                max (
                   case
                      when wmd.column_name = 'CREATE_LOAD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   create_load,
                max (
                   case
                      when wmd.column_name = 'CREATE_MAJOR_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   create_major_pkt,
                max (
                   case
                      when wmd.column_name = 'CREATE_SHPMT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   create_shpmt,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_COLOR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_color,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_COLOR_SFX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_color_sfx,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_QUAL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_qual,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_SEC_DIM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_sec_dim,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_SIZE_DESC'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_size_desc,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_STYLE_SFX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_style_sfx,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_OVERWRITE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_overwrite,
                max (
                   case
                      when wmd.column_name = 'DATE_MASK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   date_mask,
                max (
                   case
                      when wmd.column_name = 'DEFAULT_PRICE_TIX_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   default_price_tix_lock_code,
                max (
                   case
                      when wmd.column_name = 'DFLT_ALLOC_CUTOFF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_alloc_cutoff,
                max (
                   case
                      when wmd.column_name = 'DFLT_ALLOC_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_alloc_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_BOL_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_bol_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_CAPCTY_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_capcty_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_CASE_SIZE_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_case_size_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_DATE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_date,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_PRTY_MXD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_prty_mxd,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_PRTY_PARTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_prty_partl,
                max (
                   case
                      when wmd.column_name =
                              'DFLT_CONS_PRTY_SINGL_SKU_FULL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_prty_singl_sku_full,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_SEQ_MXD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_seq_mxd,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_SEQ_PARTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_seq_partl,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_SEQ_SINGL_SKU_FULL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_seq_singl_sku_full,
                max (
                   case
                      when wmd.column_name = 'DFLT_DB_QTY_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_db_qty_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DB_VOL_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_db_vol_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DB_WT_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_db_wt_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DSP_QTY_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_dsp_qty_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DSP_VOL_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_dsp_vol_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DSP_WT_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_dsp_wt_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_INCUB_LOCK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_incub_lock,
                max (
                   case
                      when wmd.column_name = 'DFLT_MANIF_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_manif_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_PALLET_HT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_pallet_ht,
                max (
                   case
                      when wmd.column_name = 'DFLT_PIKNG_LOCN_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_pikng_locn_code,
                max (
                   case
                      when wmd.column_name = 'DFLT_PLT_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_plt_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_PUTWY_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_putwy_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_RECALL_CARTON_LOCK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_recall_carton_lock,
                max (
                   case
                      when wmd.column_name = 'DFLT_SKU_SUB'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_sku_sub,
                max (
                   case
                      when wmd.column_name = 'DFLT_STOP_SEQ_ORD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_stop_seq_ord,
                max (
                   case
                      when wmd.column_name = 'DFLT_UNIT_VALUE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_unit_value,
                max (
                   case
                      when wmd.column_name = 'DFLT_UNIT_VOL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_unit_vol,
                max (
                   case
                      when wmd.column_name = 'DFLT_UNIT_WT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_unit_wt,
                max (
                   case
                      when wmd.column_name = 'DFLT_UPC_PRE_DIGIT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_upc_pre_digit,
                max (
                   case
                      when wmd.column_name = 'DFLT_UPC_VENDOR_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_upc_vendor_code,
                max (
                   case
                      when wmd.column_name = 'DUP_SRL_NBR_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dup_srl_nbr_flag,
                max (
                   case
                      when wmd.column_name = 'DWP_INTERFACE_ID'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dwp_interface_id,
                max (
                   case
                      when wmd.column_name = 'EAN_ENABLED_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_enabled_flag,
                max (
                   case
                      when wmd.column_name = 'EAN_MAX_LEN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_max_len,
                max (
                   case
                      when wmd.column_name = 'EAN_NBR_OF_SCANS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_nbr_of_scans,
                max (
                   case
                      when wmd.column_name = 'EAN_PREFIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_prefix,
                max (
                   case
                      when wmd.column_name = 'EAN_SEPARATOR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_separator,
                max (
                   case
                      when wmd.column_name = 'ELS_INSTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   els_instl,
                max (
                   case
                      when wmd.column_name = 'ELS_TASK_ACTVTY_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   els_task_actvty_code,
                max (
                   case
                      when wmd.column_name = 'ELS_TASK_INTERFACE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   els_task_interface,
                max (
                   case
                      when wmd.column_name = 'EPC_MATCH_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   epc_match_lock_code,
                max (
                   case
                      when wmd.column_name = 'EPC_REQD_ON_ALL_CARTONS_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   epc_reqd_on_all_cartons_flag,
                max (
                   case
                      when wmd.column_name = 'EPC_REQD_ON_ALL_CASES_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   epc_reqd_on_all_cases_flag,
                max (
                   case
                      when wmd.column_name = 'ERP_HOST_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   erp_host_type,
                max (
                   case
                      when wmd.column_name = 'ERROR_RCPT_PCNT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   error_rcpt_pcnt_po_sku,
                max (
                   case
                      when wmd.column_name = 'ERROR_RCPT_PCNT_SHMT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   error_rcpt_pcnt_shmt_po_sku,
                max (
                   case
                      when wmd.column_name = 'EST_NBR_CARTONS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   est_nbr_cartons,
                max (
                   case
                      when wmd.column_name = 'FEDEX_SERVER_IP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   fedex_server_ip,
                max (
                   case
                      when wmd.column_name = 'FEDEX_SERVER_PORT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   fedex_server_port,
                max (
                   case
                      when wmd.column_name = 'FIFO_MAINT_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   fifo_maint_flag,
                max (
                   case
                      when wmd.column_name = 'FTZ_TRK_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ftz_trk_flag,
                max (
                   case
                      when wmd.column_name = 'GENRT_CARTON_ASN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   genrt_carton_asn,
                max (
                   case
                      when wmd.column_name = 'GEN_EAN_CNTR_NBR_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   gen_ean_cntr_nbr_flag,
                max (
                   case
                      when wmd.column_name = 'GEN_ITEM_BRCD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   gen_item_brcd,
                max (
                   case
                      when wmd.column_name = 'GET_EPC_W_WAVE_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   get_epc_w_wave_flag,
                max (
                   case
                      when wmd.column_name = 'HAZMAT_CNTCT_LINE1'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hazmat_cntct_line1,
                max (
                   case
                      when wmd.column_name = 'HAZMAT_CNTCT_LINE2'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hazmat_cntct_line2,
                max (
                   case
                      when wmd.column_name = 'HNDL_ATTR_UOM_ACT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hndl_attr_uom_act,
                max (
                   case
                      when wmd.column_name = 'HNDL_ATTR_UOM_CP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hndl_attr_uom_cp,
                max (
                   case
                      when wmd.column_name = 'HOT_TASK_PRTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hot_task_prty,
                max (
                   case
                      when wmd.column_name = 'I2O5_CASE_QTY_REF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   i2o5_case_qty_ref,
                max (
                   case
                      when wmd.column_name = 'I2O5_PACK_QTY_REF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   i2o5_pack_qty_ref,
                max (
                   case
                      when wmd.column_name = 'I2O5_PLT_QTY_REF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   i2o5_plt_qty_ref,
                max (
                   case
                      when wmd.column_name = 'I2O5_TIER_QTY_REF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   i2o5_tier_qty_ref,
                max (
                   case
                      when wmd.column_name = 'INCUB_CTRL_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   incub_ctrl_flag,
                max (
                   case
                      when wmd.column_name = 'INFOLINK_INSTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   infolink_instl,
                max (
                   case
                      when wmd.column_name = 'INVC_MODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   invc_mode,
                max (
                   case
                      when wmd.column_name = 'INVN_UPD_ON_CANCEL_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   invn_upd_on_cancel_pkt,
                max (
                   case
                      when wmd.column_name = 'INVN_UPD_ON_PAKNG_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   invn_upd_on_pakng_chg,
                max (
                   case
                      when wmd.column_name = 'INVN_UPD_ON_RESET_WAVE_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   invn_upd_on_reset_wave_pkt,
                max (
                   case
                      when wmd.column_name = 'LAST_SLOT_DOWNLOAD_DATE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   last_slot_download_date,
                max (
                   case
                      when wmd.column_name =
                              'LIMIT_USER_IN_TRVL_AISLE_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   limit_user_in_trvl_aisle_flag,
                max (
                   case
                      when wmd.column_name = 'LM_MONITOR_INSTL_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lm_monitor_instl_flag,
                max (
                   case
                      when wmd.column_name = 'LOAD_PLAN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   load_plan,
                max (
                   case
                      when wmd.column_name = 'LOCN_CHK_DIGIT_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   locn_chk_digit_flag,
                max (
                   case
                      when wmd.column_name = 'LOG_FEDEX_TRAN_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   log_fedex_tran_flag,
                max (
                   case
                      when wmd.column_name = 'LOG_LANG_ID'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   log_lang_id,
                max (
                   case
                      when wmd.column_name = 'LOOK_UP_XREF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   look_up_xref,
                max (
                   case
                      when wmd.column_name = 'LOST_IN_CYCLE_CNT_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lost_in_cycle_cnt_lock_code,
                max (
                   case
                      when wmd.column_name = 'LOST_IN_WHSE_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lost_in_whse_lock_code,
                max (
                   case
                      when wmd.column_name = 'LOST_ON_PLT_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lost_on_plt_lock_code,
                max (
                   case
                      when wmd.column_name = 'LOST_ON_PUTWY_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lost_on_putwy_lock_code,
                max (
                   case
                      when wmd.column_name = 'LTL_TL_RATE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ltl_tl_rate,
                max (
                   case
                      when wmd.column_name = 'MANIF_INCMPL_ORD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   manif_incmpl_ord,
                max (
                   case
                      when wmd.column_name = 'MULT_SKU_CASE_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   mult_sku_case_lock_code,
                max (
                   case
                      when wmd.column_name = 'MXD_SKU_PUTWY_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   mxd_sku_putwy_type,
                max (
                   case
                      when wmd.column_name = 'NBR_OF_DYN_ACTV_PICK_PER_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   nbr_of_dyn_actv_pick_per_sku,
                max (
                   case
                      when wmd.column_name = 'NBR_OF_DYN_CASE_PICK_PER_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   nbr_of_dyn_case_pick_per_sku,
                max (
                   case
                      when wmd.column_name = 'NBR_OF_WAVE_BEFORE_LETUP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   nbr_of_wave_before_letup,
                max (
                   case
                      when wmd.column_name = 'NBR_OF_WAVE_BEFORE_REUSE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   nbr_of_wave_before_reuse,
                max (
                   case
                      when wmd.column_name = 'OB_QA_LVL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ob_qa_lvl,
                max (
                   case
                      when wmd.column_name = 'ORGN_ZIP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   orgn_zip,
                max (
                   case
                      when wmd.column_name = 'OVRD_RCPT_PCNT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrd_rcpt_pcnt_po_sku,
                max (
                   case
                      when wmd.column_name = 'OVRD_RCPT_PCNT_SHMT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrd_rcpt_pcnt_shmt_po_sku,
                max (
                   case
                      when wmd.column_name = 'OVRPK_BOXES'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrpk_boxes,
                max (
                   case
                      when wmd.column_name = 'OVRPK_CTRL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrpk_ctrl,
                max (
                   case
                      when wmd.column_name = 'OVRPK_PARCL_CARTON'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrpk_parcl_carton,
                max (
                   case
                      when wmd.column_name = 'OVRPK_PCNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrpk_pcnt,
                max (
                   case
                      when wmd.column_name = 'PENDING_CYCLE_CNT_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   pending_cycle_cnt_lock_code,
                max (
                   case
                      when wmd.column_name = 'PHYS_INVN_CNT_ADJ_RSN_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   phys_invn_cnt_adj_rsn_code,
                max (
                   case
                      when wmd.column_name = 'PICK_LOCN_ASSIGN_SEQ_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   pick_locn_assign_seq_flag,
                max (
                   case
                      when wmd.column_name = 'PKT_STAT_CHG_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   pkt_stat_chg_pix,
                max (
                   case
                      when wmd.column_name = 'PLT_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   plt_type,
                max (
                   case
                      when wmd.column_name = 'PO_SKU_LVL_VERF_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   po_sku_lvl_verf_pix,
                max (
                   case
                      when wmd.column_name = 'PPICK_REPL_CTRL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ppick_repl_ctrl,
                max (
                   case
                      when wmd.column_name = 'PRICE_TKT_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   price_tkt_type,
                max (
                   case
                      when wmd.column_name =
                              'PRT_CONTENT_LBL_ON_CTN_MODIFY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   prt_content_lbl_on_ctn_modify,
                max (
                   case
                      when wmd.column_name = 'PRT_PACK_SLIP_ON_CTN_MODIFY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   prt_pack_slip_on_ctn_modify,
                max (
                   case
                      when wmd.column_name = 'PRT_PKT_W_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   prt_pkt_w_wave,
                max (
                   case
                      when wmd.column_name = 'PURGE_APPT_STAT_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   purge_appt_stat_code,
                max (
                   case
                      when wmd.column_name = 'PURGE_ASN_STAT_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   purge_asn_stat_code,
                max (
                   case
                      when wmd.column_name = 'PURGE_CASE_BEYOND_STAT_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   purge_case_beyond_stat_code,
                max (
                   case
                      when wmd.column_name = 'PUTWY_LOCN_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   putwy_locn_lock_code,
                max (
                   case
                      when wmd.column_name = 'QUAL_HELD_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   qual_held_lock_code,
                max (
                   case
                      when wmd.column_name = 'REPL_ACTV_W_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   repl_actv_w_wave,
                max (
                   case
                      when wmd.column_name = 'REPL_CASE_PICK_W_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   repl_case_pick_w_wave,
                max (
                   case
                      when wmd.column_name = 'REPL_TASK_UPDT_PRTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   repl_task_updt_prty,
                max (
                   case
                      when wmd.column_name = 'REPORT_TRANS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   report_trans,
                max (
                   case
                      when wmd.column_name = 'RLS_HELD_RPLN_TASKS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   rls_held_rpln_tasks,
                max (
                   case
                      when wmd.column_name = 'RTE_WITH_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   rte_with_wave,
                max (
                   case
                      when wmd.column_name = 'SAVE_RTE_WAVE_UNDO_INFO'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   save_rte_wave_undo_info,
                max (
                   case
                      when wmd.column_name = 'SKU_CNSTR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   sku_cnstr,
                max (
                   case
                      when wmd.column_name = 'SKU_LEVEL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   sku_level,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_CRIT_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_crit_nbr,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_DOWNLOAD_FILE_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_download_file_path,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_ONE_USER_PER_GRP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_one_user_per_grp,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_TEMP_LOCN_ID'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_temp_locn_id,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_UPLOAD_FILE_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_upload_file_path,
                max (
                   case
                      when wmd.column_name = 'SLOT_IT_USED'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slot_it_used,
                max (
                   case
                      when wmd.column_name = 'SMARTINFO_INSTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   smartinfo_instl,
                max (
                   case
                      when wmd.column_name = 'SORT_ID_CODES'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   sort_id_codes,
                max (
                   case
                      when wmd.column_name = 'SRL_NBR_LIST_MAX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   srl_nbr_list_max,
                max (
                   case
                      when wmd.column_name = 'SRL_PIX_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   srl_pix_flag,
                max (
                   case
                      when wmd.column_name = 'SRL_TRK_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   srl_trk_flag,
                max (
                   case
                      when wmd.column_name = 'SUPPRESS_CTN_LBL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   suppress_ctn_lbl,
                max (
                   case
                      when wmd.column_name = 'SYS_TIME_ZONE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   sys_time_zone,
                max (
                   case
                      when wmd.column_name = 'TASK_EXCEPTION_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   task_exception_flag,
                max (
                   case
                      when wmd.column_name = 'TASK_PRIORITIZATION'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   task_prioritization,
                max (
                   case
                      when wmd.column_name = 'TI_UPD_ON_CANCEL_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ti_upd_on_cancel_pkt,
                max (
                   case
                      when wmd.column_name = 'TI_UPD_ON_PAKNG_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ti_upd_on_pakng_chg,
                max (
                   case
                      when wmd.column_name = 'TI_UPD_ON_RESET_WAVE_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ti_upd_on_reset_wave_pkt,
                max (
                   case
                      when wmd.column_name = 'TRACK_OB_RETN_LEVEL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   track_ob_retn_level,
                max (
                   case
                      when wmd.column_name = 'TRIG_INVC_PKT_STS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trig_invc_pkt_sts,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTIVE_MULT_PACK_QTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_active_mult_pack_qty,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTV_BY_BATCH_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_actv_by_batch_nbr,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTV_BY_CNTRY_OF_ORGN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_actv_by_cntry_of_orgn,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTV_BY_ID_CODES'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_actv_by_id_codes,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTV_BY_PROD_STAT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_actv_by_prod_stat,
                max (
                   case
                      when wmd.column_name = 'TRK_CASE_PICK_BY_BATCH_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_case_pick_by_batch_nbr,
                max (
                   case
                      when wmd.column_name =
                              'TRK_CASE_PICK_BY_CNTRY_OF_ORGN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_case_pick_by_cntry_of_orgn,
                max (
                   case
                      when wmd.column_name = 'TRK_CASE_PICK_BY_ID_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_case_pick_by_id_code,
                max (
                   case
                      when wmd.column_name = 'TRK_CASE_PICK_BY_PROD_STAT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_case_pick_by_prod_stat,
                max (
                   case
                      when wmd.column_name = 'TRK_MULT_PACK_QTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_mult_pack_qty,
                max (
                   case
                      when wmd.column_name = 'TRK_PROD_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_prod_flag,
                max (
                   case
                      when wmd.column_name = 'UPD_ALLOC_ON_PICK_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   upd_alloc_on_pick_wave,
                max (
                   case
                      when wmd.column_name = 'USE_APPT_DURTN_FOR_DOCK_DOOR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   use_appt_durtn_for_dock_door,
                max (
                   case
                      when wmd.column_name = 'USE_INBD_LPN_AS_OUTBD_LPN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   use_inbd_lpn_as_outbd_lpn,
                max (
                   case
                      when wmd.column_name = 'USE_LOCK_CODE_PRTY_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   use_lock_code_prty_pix,
                max (
                   case
                      when wmd.column_name = 'USE_LOCN_ID_AS_BRCD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   use_locn_id_as_brcd,
                max (
                   case
                      when wmd.column_name = 'VENDOR_PERFRM_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vendor_perfrm_pix,
                max (
                   case
                      when wmd.column_name = 'VERF_CARTON_CONTNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   verf_carton_contnt,
                max (
                   case
                      when wmd.column_name = 'VICS_BOL_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vics_bol_flag,
                max (
                   case
                      when wmd.column_name =
                              'VOCOLLECT_PACK_CNTR_TRACK_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pack_cntr_track_flag,
                max (
                   case
                      when wmd.column_name = 'VOCOLLECT_PACK_LUT_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pack_lut_flag,
                max (
                   case
                      when wmd.column_name = 'VOCOLLECT_PACK_WORK_ID_DESC'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pack_work_id_desc,
                max (
                   case
                      when wmd.column_name = 'VOCOLLECT_PTS_LUT_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pts_lut_flag,
                max (
                   case
                      when wmd.column_name = 'VOCOLLECT_PTS_WORK_ID_DESC'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pts_work_id_desc,
                max (
                   case
                      when wmd.column_name = 'WARN_RCPT_PCNT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   warn_rcpt_pcnt_po_sku,
                max (
                   case
                      when wmd.column_name = 'WARN_RCPT_PCNT_SHMT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   warn_rcpt_pcnt_shmt_po_sku,
                max (
                   case
                      when wmd.column_name = 'WAVE_SUSPND_OPTN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   wave_suspnd_optn,
                max (
                   case
                      when wmd.column_name = 'WHEN_TO_PRT_CTN_CONTENT_LBL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   when_to_prt_ctn_content_lbl,
                max (
                   case
                      when wmd.column_name = 'WHEN_TO_PRT_CTN_SHIP_LBL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   when_to_prt_ctn_ship_lbl,
                max (
                   case
                      when wmd.column_name = 'WHEN_TO_PRT_PAKNG_SLIP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   when_to_prt_pakng_slip,
                max (
                   case
                      when wmd.column_name = 'WHEN_TO_PRT_PLT_LBL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   when_to_prt_plt_lbl,
                max (
                   case
                      when wmd.column_name = 'WHSE_TIME_ZONE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   whse_time_zone,
                max (
                   case
                      when wmd.column_name = 'WORKINFO_LOG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   workinfo_log,
                max (
                   case
                      when wmd.column_name = 'WORKINFO_SERVER_FACTORY_NAME'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   workinfo_server_factory_name,
                max (
                   case
                      when wmd.column_name = 'WORKINFO_TRAVEL_ACTVTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   workinfo_travel_actvty,
                max (
                   case
                      when wmd.column_name = 'XCESS_WAVE_NEED_PROC_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   xcess_wave_need_proc_type,
                max (
                   case
                      when wmd.column_name = 'XFER_MAX_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   xfer_max_uom,
                max (
                   case
                      when wmd.column_name = 'YARD_JOCK_TASK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   yard_jock_task,
                max (
                   case
                      when wmd.column_name = 'ZONE_PAYMENT_STAT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   zone_payment_stat,
                max (
                   case
                      when wmd.column_name = 'ZONE_SKIP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   zone_skip,
                max (
                   case
                      when wmd.column_name = 'ALLOW_MIXED_CD_IN_RESV_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_mixed_cd_in_resv_flag,
                max (
                   case
                      when wmd.column_name = 'UPS_ADF_ACTV'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_adf_actv,
                max (
                   case
                      when wmd.column_name = 'UPS_ADF_FILE_UPLOAD_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_adf_file_upload_path,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_URL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_url,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_REMOTE_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_remote_path,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_USER'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_user,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_PSWD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_pswd,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_FILE_UPLOAD_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_file_upload_path,
                wm.create_date_time,
                wm.mod_date_time,
                wm.user_id,
                max (
                   case
                      when wmd.column_name = 'ACTV_LOCN_THRESHOLD_PCNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   actv_locn_threshold_pcnt,
                max (
                   case
                      when wmd.column_name =
                              'CASE_PICK_LOCN_THRESHOLD_PCNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   case_pick_locn_threshold_pcnt,
                max (
                   case
                      when wmd.column_name = 'AUTO_GEN_CC_TASK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   auto_gen_cc_task,
                max (
                   case
                      when wmd.column_name = 'QUAL_AUD_FAILED_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   qual_aud_failed_lock_code,
                max (
                   case
                      when wmd.column_name = 'MAX_CC_RECNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   max_cc_recnt,
                max (
                   case
                      when wmd.column_name = 'DELV_CONFIRMATION_ENABLED'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   delv_confirmation_enabled,
                max (
                   case
                      when wmd.column_name = 'DISTRO_SEQ_METHOD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   distro_seq_method,
                max (
                   case
                      when wmd.column_name = 'FLOWTHRU_ALLOC_METHOD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   flowthru_alloc_method
           from    whse_master wm
                inner join
                   whse_master_dtl wmd
                on wmd.whse_master_id = wm.whse_master_id
       group by wm.whse,
                wm.whse_name,
                wm.whse_addr_id,
                wm.cls_timezone_id,
                wm.create_date_time,
                wm.mod_date_time,
                wm.user_id);

CREATE OR REPLACE FORCE VIEW "V_ELM_FCT_CRIT" ("ELM_ID", "FCT_ID", "CRIT_VAL_ID", "CRIT_ID", "TIME_ALLOW", "ADJ_PCNT", "STATUS", "MINADJSEL", "CRIT_VAL") AS
select crit.elm_id,
      crit.fct_id,
      crit.crit_val_id,
      crit.crit_id,
      crit.time_allow,
      crit.adj_pcnt,
      1 as status,
      get_min_adj (time_allow, adj_pcnt) as minadjsel,
      val.crit_val
 from e_elm_fct_crit crit, e_crit_val val
where crit.crit_val_id = val.crit_val_id and crit.crit_id = val.crit_id
 union
 select elm.elm_id as elm_id,
      crit.fct_id as fct_id,
      crit.crit_val_id as crit_val_id,
      crit.crit_id as crit_id,
      0 as time_allow,
      0 as adj_pcnt,
      0 as status,
      'M' as minadjsel,
      val.crit_val
 from e_elm elm, e_fct_crit crit, e_crit_val val
where crit.crit_id = val.crit_id and crit.crit_val_id = val.crit_val_id
      and val.crit_val_id not in
             (select crit_val_id
                from e_elm_fct_crit fct
               where     fct.fct_id = crit.fct_id
                     and fct.crit_id = crit.crit_id
                     and crit.crit_val_id = val.crit_val_id
                     and fct.elm_id = elm.elm_id);

CREATE OR REPLACE FORCE VIEW "WHSE_GEN_PARM" ("WHSE", "TC_COMPANY_ID", "ADDR_1", "ADDR_2", "STATE", "CITY", "ZIP", "CNTRY", "TEL_NBR", "PKT_ADDR_1", "PKT_ADDR_2", "PKT_CITY", "PKT_STATE", "PKT_ZIP", "PKT_CNTRY", "PKT_TEL_NBR", "LABEL_ADDR_1", "LABEL_ADDR_2", "LABEL_CITY", "LABEL_STATE", "LABEL_ZIP", "LABEL_CNTRY", "LABEL_TEL_NBR", "GET_EPC_W_WAVE_FLAG", "ASSIGN_EPC_IN_ACTIVE_FLAG", "EPC_REQD_ON_ALL_CASES_FLAG", "EPC_REQD_ON_ALL_CARTONS_FLAG", "EPC_MATCH_LOCK_CODE", "AES_PSWD", "AES_FILE_UPLOAD_PATH", "LM_MONITOR_INSTL_FLAG", "DSP_ITEM_DESC_FLAG", "LIMIT_USER_IN_TRVL_AISLE_FLAG", "LOOK_UP_XREF", "I2O5_PACK_QTY_REF", "I2O5_CASE_QTY_REF", "I2O5_TIER_QTY_REF", "I2O5_PLT_QTY_REF", "TRK_PROD_FLAG", "ELS_INSTL", "SMARTINFO_INSTL", "INFOLINK_INSTL", "USE_LOCK_CODE_PRTY_PIX", "TRK_MULT_PACK_QTY", "TRK_ACTIVE_MULT_PACK_QTY", "ELS_TASK_ACTVTY_CODE", "ELS_TASK_INTERFACE", "EAN_ENABLED_FLAG", "EAN_PREFIX", "EAN_SEPARATOR", "EAN_MAX_LEN", "EAN_NBR_OF_SCANS", "LOCN_CHK_DIGIT_FLAG", "USE_LOCN_ID_AS_BRCD", "WORKINFO_SERVER_FACTORY_NAME", "FTZ_TRK_FLAG", "ADJ_ALG_FLAG", "ZONE_PAYMENT_STAT", "SKU_LEVEL", "DWP_INTERFACE_ID", "CNTY", "REPORT_TRANS", "CO_ID_NAME", "VOCOLLECT_PTS_WORK_ID_DESC", "VOCOLLECT_PTS_LUT_FLAG", "VOCOLLECT_PACK_WORK_ID_DESC", "VOCOLLECT_PACK_LUT_FLAG", "VOCOLLECT_PACK_CNTR_TRACK_FLAG", "UPS_EMT_FILE_UPLOAD_PATH", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select fcl.whse as whse,
        cd.company_id as tc_company_id,
        max (wp.addr_1) as addr_1,
        max (wp.addr_2) as addr_2,
        max (wp.state) as state,
        max (wp.city) as city,
        max (wp.zip) as zip,
        max (wp.cntry) as cntry,
        max (wp.tel_nbr) as tel_nbr,
        max (wp.pkt_addr_1) as pkt_addr_1,
        max (wp.pkt_addr_2) as pkt_addr_2,
        max (wp.pkt_city) as pkt_city,
        max (wp.pkt_state) as pkt_state,
        max (wp.pkt_zip) as pkt_zip,
        max (wp.pkt_cntry) as pkt_cntry,
        max (wp.pkt_tel_nbr) as pkt_tel_nbr,
        max (wp.label_addr_1) as label_addr_1,
        max (wp.label_addr_2) as label_addr_2,
        max (wp.label_city) as label_city,
        max (wp.label_state) as label_state,
        max (wp.label_zip) as label_zip,
        max (wp.label_cntry) as label_cntry,
        max (wp.label_tel_nbr) as label_tel_nbr,
        max (wp.get_epc_w_wave_flag) as get_epc_w_wave_flag,
        max (wp.assign_epc_in_active_flag) as assign_epc_in_active_flag,
        max (wp.epc_reqd_on_all_cases_flag) as epc_reqd_on_all_cases_flag,
        max (wp.epc_reqd_on_all_cartons_flag)
           as epc_reqd_on_all_cartons_flag,
        max (wp.epc_match_lock_code) as epc_match_lock_code,
        max (wp.aes_pswd) as aes_pswd,
        max (wp.aes_file_upload_path) as aes_file_upload_path,
        max (wp.lm_monitor_instl_flag) as lm_monitor_instl_flag,
        max (wp.dsp_item_desc_flag) as dsp_item_desc_flag,
        max (wp.limit_user_in_trvl_aisle_flag)
           as limit_user_in_trvl_aisle_flag,
        max (wp.look_up_xref) as look_up_xref,
        max (wp.i2o5_pack_qty_ref) as i2o5_pack_qty_ref,
        max (wp.i2o5_case_qty_ref) as i2o5_case_qty_ref,
        max (wp.i2o5_tier_qty_ref) as i2o5_tier_qty_ref,
        max (wp.i2o5_plt_qty_ref) as i2o5_plt_qty_ref,
        max (wp.trk_prod_flag) as trk_prod_flag,
        max (wp.els_instl) as els_instl,
        max (wp.smartinfo_instl) as smartinfo_instl,
        max (wp.infolink_instl) as infolink_instl,
        max (wp.use_lock_code_prty_pix) as use_lock_code_prty_pix,
        max (wp.trk_mult_pack_qty) as trk_mult_pack_qty,
        max (wp.trk_active_mult_pack_qty) as trk_active_mult_pack_qty,
        max (wp.els_task_actvty_code) as els_task_actvty_code,
        max (wp.els_task_interface) as els_task_interface,
        max (coalesce (wp.ean_enabled_flag, 0)) as ean_enabled_flag,
        max (wp.ean_prefix) as ean_prefix,
        max (wp.ean_separator) as ean_separator,
        max (coalesce (wp.ean_max_len, 0)) as ean_max_len,
        max (coalesce (wp.ean_nbr_of_scans, 0)) as ean_nbr_of_scans,
        max (coalesce (wp.locn_chk_digit_flag, 0)) as locn_chk_digit_flag,
        max (coalesce (wp.use_locn_id_as_brcd, 0)) as use_locn_id_as_brcd,
        max (wp.workinfo_server_factory_name)
           as workinfo_server_factory_name,
        max (wp.ftz_trk_flag) as ftz_trk_flag,
        max (wp.adj_alg_flag) as adj_alg_flag,
        max (wp.zone_payment_stat) as zone_payment_stat,
        max (coalesce (wp.sku_level, 0)) as sku_level,
        max (cast (coalesce (wp.dwp_interface_id, -1) as integer))
           as dwp_interface_id,
        max (wp.cnty) as cnty,
        max (wp.report_trans) as report_trans,
        '' as co_id_name,
        max (wp.vocollect_pts_work_id_desc) as vocollect_pts_work_id_desc,
        max (wp.vocollect_pts_lut_flag) as vocollect_pts_lut_flag,
        max (wp.vocollect_pack_work_id_desc) as vocollect_pack_work_id_desc,
        max (wp.vocollect_pack_lut_flag) as vocollect_pack_lut_flag,
        max (wp.vocollect_pack_cntr_track_flag)
           as vocollect_pack_cntr_track_flag,
        max (cast (null as varchar (50))) as ups_emt_file_upload_path,
        max (fcl.created_dttm) as create_date_time,
        max (fcl.last_updated_dttm) as mod_date_time,
        max (fcl.created_source) as user_id
   from company cd
        inner join access_control
           on access_control.business_unit_id = cd.company_id
        inner join facility fcl
           on ( (fcl.inbound_region_id = access_control.geo_region_id
                 or fcl.outbound_region_id = access_control.geo_region_id)
               and fcl.whse is not null)
        inner join whse_parameters wp
           on wp.whse_master_id = fcl.facility_id
 group by fcl.whse, cd.company_id;

CREATE OR REPLACE FORCE VIEW "WHSE_INBD_PARM" ("WHSE", "TC_COMPANY_ID", "HNDL_ATTR_UOM_ACT", "HNDL_ATTR_UOM_CP", "SRL_TRK_FLAG", "SRL_PIX_FLAG", "ALLOW_SRL_CREATE", "DUP_SRL_NBR_FLAG", "SRL_NBR_LIST_MAX", "CUBISCAN_IGNORE_STYLE_SFX", "CUBISCAN_IGNORE_COLOR", "CUBISCAN_IGNORE_COLOR_SFX", "CUBISCAN_IGNORE_SEC_DIM", "CUBISCAN_IGNORE_QUAL", "CUBISCAN_IGNORE_SIZE_DESC", "CUBISCAN_OVERWRITE", "TASK_EXCEPTION_FLAG", "FIFO_MAINT_FLAG", "GEN_EAN_CNTR_NBR_FLAG", "LAST_SLOT_DOWNLOAD_DATE", "NBR_OF_DYN_ACTV_PICK_PER_SKU", "NBR_OF_DYN_CASE_PICK_PER_SKU", "DEFAULT_PRICE_TIX_LOCK_CODE", "INVN_UPD_ON_PAKNG_CHG", "INVN_UPD_ON_CANCEL_PKT", "TI_UPD_ON_RESET_WAVE_PKT", "TI_UPD_ON_PAKNG_CHG", "TI_UPD_ON_CANCEL_PKT", "UPD_ALLOC_ON_PICK_WAVE", "TRK_ACTV_BY_ID_CODES", "TRK_ACTV_BY_BATCH_NBR", "TRK_ACTV_BY_PROD_STAT", "TRK_ACTV_BY_CNTRY_OF_ORGN", "HOT_TASK_PRTY", "DFLT_PUTWY_TYPE", "PUTWY_LOCN_LOCK_CODE", "DFLT_CAPCTY_UOM", "MXD_SKU_PUTWY_TYPE", "ALLOW_WORK_ORD_QTY_CHG", "ALLOW_WORK_ORD_SKU_ADDN", "ALLOW_PAKNG_W_OUT_CASE_NBR", "TRK_CASE_PICK_BY_ID_CODE", "TRK_CASE_PICK_BY_PROD_STAT", "TRK_CASE_PICK_BY_BATCH_NBR", "TRK_CASE_PICK_BY_CNTRY_OF_ORGN", "WARN_RCPT_PCNT_PO_SKU", "WARN_RCPT_PCNT_SHMT_PO_SKU", "ERROR_RCPT_PCNT_PO_SKU", "ERROR_RCPT_PCNT_SHMT_PO_SKU", "OVRD_RCPT_PCNT_PO_SKU", "OVRD_RCPT_PCNT_SHMT_PO_SKU", "NBR_OF_WAVE_BEFORE_LETUP", "NBR_OF_WAVE_BEFORE_REUSE", "XCESS_WAVE_NEED_PROC_TYPE", "PICK_LOCN_ASSIGN_SEQ_FLAG", "CALC_TO_BE_LOCD", "XFER_MAX_UOM", "ALLOW_SKU_NOT_ON_SHPMT", "ALLOW_SKU_NOT_ON_PO", "ASN_SKU_LVL_VERF_PIX", "ASN_CASE_LVL_VERF_PIX", "PO_SKU_LVL_VERF_PIX", "SKU_CNSTR", "SLOT_IT_USED", "TASK_PRIORITIZATION", "DFLT_PIKNG_LOCN_CODE", "RLS_HELD_RPLN_TASKS", "DFLT_PALLET_HT", "CALC_CASE_SIZE_TYPE", "PURGE_ASN_STAT_CODE", "SLOTINFO_DOWNLOAD_FILE_PATH", "SLOTINFO_UPLOAD_FILE_PATH", "SLOTINFO_ONE_USER_PER_GRP", "SLOTINFO_TEMP_LOCN_ID", "SLOTINFO_CRIT_NBR", "BATCH_NBR_EQ_VENDOR_BATCH_NBR", "ALLOW_WEIGH_HELD_BATCH", "ALLOW_MANIF_HELD_BATCH", 
"ALLOW_LOAD_HELD_BATCH", "ALLOW_CLOSE_LOAD_HELD_BATCH", "INCUB_CTRL_FLAG", "DFLT_INCUB_LOCK", "BASE_INCUB_FLAG", "CASE_NBR_DSP_USAGE", "SORT_ID_CODES", "CASE_WGHT_TOL", "DFLT_CONS_PRTY_SINGL_SKU_FULL", "DFLT_CONS_PRTY_PARTL", "DFLT_CONS_PRTY_MXD", "DFLT_CONS_DATE", "DFLT_CONS_SEQ_SINGL_SKU_FULL", "DFLT_CONS_SEQ_PARTL", "DFLT_CONS_SEQ_MXD", "DFLT_CASE_SIZE_TYPE", "LOST_IN_WHSE_LOCK_CODE", "QUAL_HELD_LOCK_CODE", "LOST_IN_CYCLE_CNT_LOCK_CODE", "LOST_ON_PLT_LOCK_CODE", "LOST_ON_PUTWY_LOCK_CODE", "MULT_SKU_CASE_LOCK_CODE", "PENDING_CYCLE_CNT_LOCK_CODE", "CASE_ALLOC_STRK", "VENDOR_PERFRM_PIX", "ALLOW_RCV_W_OUT_CASE_ASN", "ALLOW_RCV_W_OUT_SHPMT", "ALLOW_RCV_W_OUT_PO_NBR", "ALLOW_RLS_QUAL_HELD_ON_RCPT", "ALLOW_RCV_W_OUT_CASE_NBR", "CALC_REPL_CTRL", "CMBIN_REPL_PRTY", "REPL_ACTV_W_WAVE", "REPL_CASE_PICK_W_WAVE", "PURGE_CASE_BEYOND_STAT_CODE", "PPICK_REPL_CTRL", "PHYS_INVN_CNT_ADJ_RSN_CODE", "INVN_UPD_ON_RESET_WAVE_PKT", "ALLOW_MIXED_CD_IN_RESV_FLAG", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select fcl.whse as whse,
        cd.company_id as tc_company_id,
        max (hndl_attr_uom_act) as hndl_attr_uom_act,
        max (hndl_attr_uom_cp) as hndl_attr_uom_cp,
        max (coalesce (srl_trk_flag, 0)) as srl_trk_flag,
        max (coalesce (srl_pix_flag, 0)) as srl_pix_flag,
        max (coalesce (allow_srl_create, 0)) as allow_srl_create,
        max (coalesce (dup_srl_nbr_flag, 0)) as dup_srl_nbr_flag,
        max (coalesce (srl_nbr_list_max, 0)) as srl_nbr_list_max,
        max (coalesce (cubiscan_ignore_style_sfx, 0))
           as cubiscan_ignore_style_sfx,
        max (coalesce (cubiscan_ignore_color, 0)) as cubiscan_ignore_color,
        max (coalesce (cubiscan_ignore_color_sfx, 0))
           as cubiscan_ignore_color_sfx,
        max (coalesce (cubiscan_ignore_sec_dim, 0))
           as cubiscan_ignore_sec_dim,
        max (coalesce (cubiscan_ignore_qual, 0)) as cubiscan_ignore_qual,
        max (coalesce (cubiscan_ignore_size_desc, 0))
           as cubiscan_ignore_size_desc,
        max (coalesce (cubiscan_overwrite, 0)) as cubiscan_overwrite,
        max (coalesce (task_exception_flag, 0)) as task_exception_flag,
        max (coalesce (fifo_maint_flag, 0)) as fifo_maint_flag,
        max (gen_ean_cntr_nbr_flag) as gen_ean_cntr_nbr_flag,
        max (last_slot_download_date) as last_slot_download_date,
        max (nbr_of_dyn_actv_pick_per_sku) as nbr_of_dyn_actv_pick_per_sku,
        max (nbr_of_dyn_case_pick_per_sku) as nbr_of_dyn_case_pick_per_sku,
        max (default_price_tix_lock_code) as default_price_tix_lock_code,
        max (invn_upd_on_pakng_chg) as invn_upd_on_pakng_chg,
        max (invn_upd_on_cancel_pkt) as invn_upd_on_cancel_pkt,
        max (coalesce (ti_upd_on_reset_wave_pkt, 0))
           as ti_upd_on_reset_wave_pkt,
        max (coalesce (ti_upd_on_pakng_chg, 0)) as ti_upd_on_pakng_chg,
        max (coalesce (ti_upd_on_cancel_pkt, 0)) as ti_upd_on_cancel_pkt,
        max (upd_alloc_on_pick_wave) as upd_alloc_on_pick_wave,
        max (trk_actv_by_id_codes) as trk_actv_by_id_codes,
        max (trk_actv_by_batch_nbr) as trk_actv_by_batch_nbr,
        max (trk_actv_by_prod_stat) as trk_actv_by_prod_stat,
        max (trk_actv_by_cntry_of_orgn) as trk_actv_by_cntry_of_orgn,
        max (coalesce (hot_task_prty, 0)) as hot_task_prty,
        max (dflt_putwy_type) as dflt_putwy_type,
        max (putwy_locn_lock_code) as putwy_locn_lock_code,
        max (dflt_capcty_uom) as dflt_capcty_uom,
        max (mxd_sku_putwy_type) as mxd_sku_putwy_type,
        max (allow_work_ord_qty_chg) as allow_work_ord_qty_chg,
        max (allow_work_ord_sku_addn) as allow_work_ord_sku_addn,
        max (allow_pakng_w_out_case_nbr) as allow_pakng_w_out_case_nbr,
        max (trk_case_pick_by_id_code) as trk_case_pick_by_id_code,
        max (trk_case_pick_by_prod_stat) as trk_case_pick_by_prod_stat,
        max (trk_case_pick_by_batch_nbr) as trk_case_pick_by_batch_nbr,
        max (trk_case_pick_by_cntry_of_orgn)
           as trk_case_pick_by_cntry_of_orgn,
        max (coalesce (warn_rcpt_pcnt_po_sku, 0)) as warn_rcpt_pcnt_po_sku,
        max (coalesce (warn_rcpt_pcnt_shmt_po_sku, 0))
           as warn_rcpt_pcnt_shmt_po_sku,
        max (coalesce (error_rcpt_pcnt_po_sku, 0))
           as error_rcpt_pcnt_po_sku,
        max (coalesce (error_rcpt_pcnt_shmt_po_sku, 0))
           as error_rcpt_pcnt_shmt_po_sku,
        max (coalesce (ovrd_rcpt_pcnt_po_sku, 0)) as ovrd_rcpt_pcnt_po_sku,
        max (coalesce (ovrd_rcpt_pcnt_shmt_po_sku, 0))
           as ovrd_rcpt_pcnt_shmt_po_sku,
        max (coalesce (nbr_of_wave_before_letup, 0))
           as nbr_of_wave_before_letup,
        max (coalesce (nbr_of_wave_before_reuse, 0))
           as nbr_of_wave_before_reuse,
        max (coalesce (xcess_wave_need_proc_type, 0))
           as xcess_wave_need_proc_type,
        max (pick_locn_assign_seq_flag) as pick_locn_assign_seq_flag,
        max (calc_to_be_locd) as calc_to_be_locd,
        max (xfer_max_uom) as xfer_max_uom,
        max (allow_sku_not_on_shpmt) as allow_sku_not_on_shpmt,
        max (allow_sku_not_on_po) as allow_sku_not_on_po,
        max (asn_sku_lvl_verf_pix) as asn_sku_lvl_verf_pix,
        max (asn_case_lvl_verf_pix) as asn_case_lvl_verf_pix,
        max (po_sku_lvl_verf_pix) as po_sku_lvl_verf_pix,
        max (sku_cnstr) as sku_cnstr,
        max (slot_it_used) as slot_it_used,
        max (task_prioritization) as task_prioritization,
        max (dflt_pikng_locn_code) as dflt_pikng_locn_code,
        max (rls_held_rpln_tasks) as rls_held_rpln_tasks,
        max (coalesce (dflt_pallet_ht, 0)) as dflt_pallet_ht,
        max (calc_case_size_type) as calc_case_size_type,
        max (coalesce (purge_asn_stat_code, 0)) as purge_asn_stat_code,
        max (slotinfo_download_file_path) as slotinfo_download_file_path,
        max (slotinfo_upload_file_path) as slotinfo_upload_file_path,
        max (slotinfo_one_user_per_grp) as slotinfo_one_user_per_grp,
        max (slotinfo_temp_locn_id) as slotinfo_temp_locn_id,
        max (slotinfo_crit_nbr) as slotinfo_crit_nbr,
        max (batch_nbr_eq_vendor_batch_nbr)
           as batch_nbr_eq_vendor_batch_nbr,
        max (allow_weigh_held_batch) as allow_weigh_held_batch,
        max (allow_manif_held_batch) as allow_manif_held_batch,
        max (allow_load_held_batch) as allow_load_held_batch,
        max (allow_close_load_held_batch) as allow_close_load_held_batch,
        max (incub_ctrl_flag) as incub_ctrl_flag,
        max (dflt_incub_lock) as dflt_incub_lock,
        max (base_incub_flag) as base_incub_flag,
        max (case_nbr_dsp_usage) as case_nbr_dsp_usage,
        max (sort_id_codes) as sort_id_codes,
        max (coalesce (case_wght_tol, 0)) as case_wght_tol,
        max (dflt_cons_prty_singl_sku_full)
           as dflt_cons_prty_singl_sku_full,
        max (dflt_cons_prty_partl) as dflt_cons_prty_partl,
        max (dflt_cons_prty_mxd) as dflt_cons_prty_mxd,
        max (dflt_cons_date) as dflt_cons_date,
        max (dflt_cons_seq_singl_sku_full) as dflt_cons_seq_singl_sku_full,
        max (dflt_cons_seq_partl) as dflt_cons_seq_partl,
        max (dflt_cons_seq_mxd) as dflt_cons_seq_mxd,
        max (dflt_case_size_type) as dflt_case_size_type,
        max (lost_in_whse_lock_code) as lost_in_whse_lock_code,
        max (qual_held_lock_code) as qual_held_lock_code,
        max (lost_in_cycle_cnt_lock_code) as lost_in_cycle_cnt_lock_code,
        max (lost_on_plt_lock_code) as lost_on_plt_lock_code,
        max (lost_on_putwy_lock_code) as lost_on_putwy_lock_code,
        max (mult_sku_case_lock_code) as mult_sku_case_lock_code,
        max (pending_cycle_cnt_lock_code) as pending_cycle_cnt_lock_code,
        max (coalesce (case_alloc_strk, 0)) as case_alloc_strk,
        max (vendor_perfrm_pix) as vendor_perfrm_pix,
        max (allow_rcv_w_out_case_asn) as allow_rcv_w_out_case_asn,
        max (allow_rcv_w_out_shpmt) as allow_rcv_w_out_shpmt,
        max (allow_rcv_w_out_po_nbr) as allow_rcv_w_out_po_nbr,
        max (allow_rls_qual_held_on_rcpt) as allow_rls_qual_held_on_rcpt,
        max (allow_rcv_w_out_case_nbr) as allow_rcv_w_out_case_nbr,
        max (calc_repl_ctrl) as calc_repl_ctrl,
        max (cmbin_repl_prty) as cmbin_repl_prty,
        max (repl_actv_w_wave) as repl_actv_w_wave,
        max (repl_case_pick_w_wave) as repl_case_pick_w_wave,
        max (coalesce (purge_case_beyond_stat_code, 0))
           as purge_case_beyond_stat_code,
        max (ppick_repl_ctrl) as ppick_repl_ctrl,
        max (phys_invn_cnt_adj_rsn_code) as phys_invn_cnt_adj_rsn_code,
        max (invn_upd_on_reset_wave_pkt) as invn_upd_on_reset_wave_pkt,
        max (allow_mixed_cd_in_resv_flag) as allow_mixed_cd_in_resv_flag,
        max (fcl.created_dttm) as create_date_time,
        max (fcl.last_updated_dttm) as mod_date_time,
        max (fcl.created_source) as user_id
   from company cd
        join access_control
           on access_control.business_unit_id = cd.company_id
        join facility fcl
           on ( (fcl.inbound_region_id = access_control.geo_region_id
                 or fcl.outbound_region_id = access_control.geo_region_id)
               and fcl.whse is not null)
        join whse_parameters wp
           on wp.whse_master_id = fcl.facility_id
 group by fcl.whse, cd.company_id;

CREATE OR REPLACE FORCE VIEW "WHSE_OUTBD_PARM" ("WHSE", "TC_COMPANY_ID", "DFLT_PLT_TYPE", "WHEN_TO_PRT_PLT_LBL", "SUPPRESS_CTN_LBL", "ACTV_REPL_ORGN", "DFLT_RECALL_CARTON_LOCK", "OB_QA_LVL", "VICS_BOL_FLAG", "DFLT_STOP_SEQ_ORD", "TRACK_OB_RETN_LEVEL", "USE_INBD_LPN_AS_OUTBD_LPN", "HAZMAT_CNTCT_LINE1", "HAZMAT_CNTCT_LINE2", "PRICE_TKT_TYPE", "CREATE_ASN_FLAG", "WAVE_SUSPND_OPTN", "PRT_CONTENT_LBL_ON_CTN_MODIFY", "PRT_PACK_SLIP_ON_CTN_MODIFY", "VERF_CARTON_CONTNT", "PRT_PKT_W_WAVE", "WHEN_TO_PRT_CTN_CONTENT_LBL", "WHEN_TO_PRT_PAKNG_SLIP", "WHEN_TO_PRT_CTN_SHIP_LBL", "ASSIGN_AND_PRT_MANIF", "ASSIGN_AND_PRT_BOL", "OVRPK_CTRL", "OVRPK_PCNT", "OVRPK_BOXES", "GEN_ITEM_BRCD", "DFLT_UNIT_WT", "DFLT_UNIT_VOL", "DFLT_UPC_PRE_DIGIT", "DFLT_UPC_VENDOR_CODE", "CARTON_BREAK_ON_AREA_CHG", "CARTON_BREAK_ON_ZONE_CHG", "CARTON_BREAK_ON_AISLE_CHG", "OVRPK_PARCL_CARTON", "CARTON_TYPE", "PLT_TYPE", "GENRT_CARTON_ASN", "MANIF_INCMPL_ORD", "TRIG_INVC_PKT_STS", "AUTO_MANIF_AT_PNH", "PKT_STAT_CHG_PIX", "RTE_WITH_WAVE", "ASSIGN_PRO_NBR", "CARTON_WT_TOL", "CREATE_MAJOR_PKT", "REPL_TASK_UPDT_PRTY", "INVC_MODE", "ALLOW_REPL_TASK_UPDT_PRTY", "CREATE_SHPMT", "CREATE_LOAD", "DFLT_BOL_TYPE", "DFLT_ALLOC_TYPE", "DFLT_ALLOC_CUTOFF", "DFLT_SKU_SUB", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select fcl.whse as whse,
        cd.company_id as tc_company_id,
        max (wp.dflt_plt_type) as dflt_plt_type,
        max (coalesce (wp.when_to_prt_plt_lbl, 0)) as when_to_prt_plt_lbl,
        max (wp.suppress_ctn_lbl) as suppress_ctn_lbl,
        max (wp.actv_repl_orgn) as actv_repl_orgn,
        max (wp.dflt_recall_carton_lock) as dflt_recall_carton_lock,
        max (coalesce (wp.ob_qa_lvl, 0)) as ob_qa_lvl,
        max (wp.vics_bol_flag) as vics_bol_flag,
        max (wp.dflt_stop_seq_ord) as dflt_stop_seq_ord,
        max (wp.track_ob_retn_level) as track_ob_retn_level,
        max (wp.use_inbd_lpn_as_outbd_lpn) as use_inbd_lpn_as_outbd_lpn,
        max (wp.hazmat_cntct_line1) as hazmat_cntct_line1,
        max (wp.hazmat_cntct_line2) as hazmat_cntct_line2,
        max (wp.price_tkt_type) as price_tkt_type,
        max (wp.create_asn_flag) as create_asn_flag,
        max (wp.wave_suspnd_optn) as wave_suspnd_optn,
        max (wp.prt_content_lbl_on_ctn_modify)
           as prt_content_lbl_on_ctn_modify,
        max (wp.prt_pack_slip_on_ctn_modify) as prt_pack_slip_on_ctn_modify,
        max (wp.verf_carton_contnt) as verf_carton_contnt,
        max (wp.prt_pkt_w_wave) as prt_pkt_w_wave,
        max (wp.when_to_prt_ctn_content_lbl) as when_to_prt_ctn_content_lbl,
        max (wp.when_to_prt_pakng_slip) as when_to_prt_pakng_slip,
        max (wp.when_to_prt_ctn_ship_lbl) as when_to_prt_ctn_ship_lbl,
        max (wp.assign_and_prt_manif) as assign_and_prt_manif,
        max (wp.assign_and_prt_bol) as assign_and_prt_bol,
        max (wp.ovrpk_ctrl) as ovrpk_ctrl,
        max (coalesce (wp.ovrpk_pcnt, 0)) as ovrpk_pcnt,
        max (coalesce (wp.ovrpk_boxes, 0)) as ovrpk_boxes,
        max (wp.gen_item_brcd) as gen_item_brcd,
        max (coalesce (wp.dflt_unit_wt, 0)) as dflt_unit_wt,
        max (coalesce (wp.dflt_unit_vol, 0)) as dflt_unit_vol,
        max (wp.dflt_upc_pre_digit) as dflt_upc_pre_digit,
        max (wp.dflt_upc_vendor_code) as dflt_upc_vendor_code,
        max (wp.carton_break_on_area_chg) as carton_break_on_area_chg,
        max (wp.carton_break_on_zone_chg) as carton_break_on_zone_chg,
        max (wp.carton_break_on_aisle_chg) as carton_break_on_aisle_chg,
        max (wp.ovrpk_parcl_carton) as ovrpk_parcl_carton,
        max (wp.carton_type) as carton_type,
        max (wp.plt_type) as plt_type,
        max (wp.genrt_carton_asn) as genrt_carton_asn,
        max (wp.manif_incmpl_ord) as manif_incmpl_ord,
        max (coalesce (wp.trig_invc_pkt_sts, 0)) as trig_invc_pkt_sts,
        max (wp.auto_manif_at_pnh) as auto_manif_at_pnh,
        max (wp.pkt_stat_chg_pix) as pkt_stat_chg_pix,
        max (wp.rte_with_wave) as rte_with_wave,
        max (wp.assign_pro_nbr) as assign_pro_nbr,
        max (coalesce (wp.carton_wt_tol, 0)) as carton_wt_tol,
        max (wp.create_major_pkt) as create_major_pkt,
        max (coalesce (wp.repl_task_updt_prty, 0)) as repl_task_updt_prty,
        max (coalesce (wp.invc_mode, 1)) as invc_mode,
        max (wp.allow_repl_task_updt_prty) as allow_repl_task_updt_prty,
        max (wp.create_shpmt) as create_shpmt,
        max (wp.create_load) as create_load,
        max (wp.dflt_bol_type) as dflt_bol_type,
        max (wp.dflt_alloc_type) as dflt_alloc_type,
        max (coalesce (wp.dflt_alloc_cutoff, 0)) as dflt_alloc_cutoff,
        max (wp.dflt_sku_sub) as dflt_sku_sub,
        max (fcl.created_dttm) as create_date_time,
        max (fcl.last_updated_dttm) as mod_date_time,
        max (fcl.created_source) as user_id
   from company cd
        join access_control
           on access_control.business_unit_id = cd.company_id
        join facility fcl
           on ( (fcl.inbound_region_id = access_control.geo_region_id
                 or fcl.outbound_region_id = access_control.geo_region_id)
               and fcl.whse is not null)
        join whse_parameters wp
           on wp.whse_master_id = fcl.facility_id
 group by fcl.whse, cd.company_id;

CREATE OR REPLACE FORCE VIEW "X_ACCESSORIAL_PARAM_SET" ("ACCESSORIAL_PARAM_SET_ID", "TC_COMPANY_ID", "COMMODITY_CLASS", "EFFECTIVE_DT", "EXPIRATION_DT", "CM_RATE_TYPE", "CM_RATE_CURRENCY_CODE", "STOP_OFF_CURRENCY_CODE", "DISTANCE_UOM", "IS_GLOBAL", "EQUIPMENT_ID", "SIZE_UOM_ID", "CARRIER_ID", "BUDGETED_APPROVED", "USE_FAK_COMMODITY", "IS_VALID", "PARAM_ID") AS
select	ACCESSORIAL_PARAM_SET_ID, coalesce(TC_COMPANY_ID,0), COMMODITY_CLASS, EFFECTIVE_DT, EXPIRATION_DT, CM_RATE_TYPE, CM_RATE_CURRENCY_CODE, STOP_OFF_CURRENCY_CODE, DISTANCE_UOM,
		IS_GLOBAL, coalesce(EQUIPMENT_ID,0), SIZE_UOM_ID, coalesce(CARRIER_ID,0), BUDGETED_APPROVED, USE_FAK_COMMODITY, 1 AS IS_VALID, 0 AS PARAM_ID
from	ACCESSORIAL_PARAM_SET
union
select	ACCESSORIAL_PARAM_SET_ID, coalesce(TC_COMPANY_ID,0), COMMODITY_CLASS, EFFECTIVE_DT, EXPIRATION_DT, CM_RATE_TYPE, CM_RATE_CURRENCY_CODE, STOP_OFF_CURRENCY_CODE, DISTANCE_UOM,
		IS_GLOBAL, coalesce(EQUIPMENT_ID,0), SIZE_UOM_ID, coalesce(CARRIER_ID,0), BUDGETED_APPROVED, USE_FAK_COMMODITY, 0 AS IS_VALID, PARAM_ID
from	IMPORT_ACCESSORIAL_PARAM_SET;

CREATE OR REPLACE FORCE VIEW "YARD_SLOT_VIEW" ("PARENT_ID", "STATUS", "LOCN_CLASS", "MAX_CAPACITY", "LOCN_ID", "IS_GUARD_HOUSE", "CURRENT_CAPACITY", "ID", "FACILITY_ID", "YARD_ID", "LOCATION_NAME", "PARENT_LOCATION_NAME", "LOCN_BRCD", "DSP_LOCN", "CHECK_DIGIT", "TC_COMPANY_ID", "YARD_NAME") AS
(SELECT TO_CHAR (YZS.YARD_ZONE_ID) AS PARENT_ID,
         YZS.YARD_ZONE_SLOT_STATUS AS STATUS,
         LH.LOCN_CLASS AS LOCN_CLASS,
         YZS.MAX_CAPACITY AS MAX_CAPACITY,
         LH.LOCN_ID AS LOCN_ID,
         YZS.IS_GUARD_HOUSE AS IS_GUARD_HOUSE,
         YZS.USED_CAPACITY AS CURRENT_CAPACITY,
         YZS.YARD_ZONE_SLOT_ID AS id,
         FY.FACILITY_ID AS FACILITY_ID,
         YZS.YARD_ID AS YARD_ID,
         YZS.YARD_ZONE_SLOT_NAME AS LOCATION_NAME,
         YZ.YARD_ZONE_NAME AS PARENT_LOCATION_NAME,
         LH.LOCN_BRCD AS LOCN_BRCD,
         LH.DSP_LOCN AS DSP_LOCN,
         LH.CHECK_DIGIT AS CHECK_DIGIT,
         YD.TC_COMPANY_ID AS TC_COMPANY_ID,
         YD.YARD_NAME AS YARD_NAME
    FROM YARD_ZONE_SLOT YZS,
         LOCN_HDR LH,
         YARD_ZONE YZ,
         FACILITY_YARD FY,
         YARD YD
   WHERE     YZS.LOCN_ID = LH.LOCN_ID
         AND YZS.YARD_ZONE_ID = YZ.YARD_ZONE_ID
         AND YZS.YARD_ID = YZ.YARD_ID
         AND FY.YARD_ID = YZS.YARD_ID
         AND YD.YARD_ID = YZS.YARD_ID
         AND YZ.YARD_ID = YD.YARD_ID
         AND LH.LOCN_CLASS = 'Y');

CREATE OR REPLACE FORCE VIEW "ZONE_VIEW" ("TC_COMPANY_ID", "ZONE_ID", "ATTRIBUTE_TYPE", "ATTRIBUTE_VALUE", "COUNTRY_CODE", "ZONE_ATTRIB_SEQ") AS
select zone_attribute.tc_company_id,
      zone_attribute.zone_id,
      zone_attribute.attribute_type,
      ltrim (rtrim (zone_attribute.attribute_value)),
      zone_attribute.country_code,
      zone_attribute.zone_attrib_seq
 from zone_attribute;

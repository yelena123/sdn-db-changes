set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_CONNECT_MODE

INSERT INTO CL_CONNECT_MODE ( CONNECT_MODE_ID,CONNECT_MODE_NAME) 
VALUES  ( 0,'Listen');

INSERT INTO CL_CONNECT_MODE ( CONNECT_MODE_ID,CONNECT_MODE_NAME) 
VALUES  ( 1,'Connect');

Commit;





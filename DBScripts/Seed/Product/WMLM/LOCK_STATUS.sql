set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LOCK_STATUS

INSERT INTO LOCK_STATUS ( LOCK_STATUS,DESCRIPTION) 
VALUES  ( 0,'No');

INSERT INTO LOCK_STATUS ( LOCK_STATUS,DESCRIPTION) 
VALUES  ( 1,'Partial');

INSERT INTO LOCK_STATUS ( LOCK_STATUS,DESCRIPTION) 
VALUES  ( 2,'Yes');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SUBSTITUTION_TYPE

INSERT INTO SUBSTITUTION_TYPE ( SUBSTITUTION_TYPE,DESCRIPTION) 
VALUES  ( 4,'Immediate');

INSERT INTO SUBSTITUTION_TYPE ( SUBSTITUTION_TYPE,DESCRIPTION) 
VALUES  ( 16,'When Out');

Commit;





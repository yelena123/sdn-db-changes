set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LOAD_TYPE

INSERT INTO LOAD_TYPE ( LOAD_TYPE,DESCRIPTION) 
VALUES  ( 'STD','Standard');

INSERT INTO LOAD_TYPE ( LOAD_TYPE,DESCRIPTION) 
VALUES  ( 'OPP','Orign pool point');

INSERT INTO LOAD_TYPE ( LOAD_TYPE,DESCRIPTION) 
VALUES  ( 'DPP','Destination pool point');

Commit;





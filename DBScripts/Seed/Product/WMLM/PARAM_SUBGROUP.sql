set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for PARAM_SUBGROUP

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_TEPE','Threads','Multi Threading',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'RLM','RetOrd','Return Order',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_SIZE','ITM_SIZE','Item Sizes',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_SIZE','PLN_SIZE','Planning Sizes',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_SIZE','ADD_MSR','Addition Measures',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_WM','SHIPPING','Ship Confirm Parameters',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'ILM2','YARD','Yard',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'ILM','YARD','Yard',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFOB','SHIP','Shipment Characteristics',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFOB','PRE_H','Preprocess',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFOB','IS_H','Initial Solution',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFOB','RG_H','Route Generation',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFOB','APP_H','Application',5,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFOB','OPT_H','Optional Functionality',6,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFOB','CPLEX_H','Cplex',7,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFOB','DEBUG_H','Debug',8,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFIB','SHIP','Shipment Characteristics',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFIB','PRE_H','Preprocess',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFIB','IS_H','Initial Solution',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFIB','RG_H','Route Generation',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFIB','APP_H','Application',5,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFIB','OPT_H','Optional Functionality',6,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFIB','CPLEX_H','Cplex',7,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_OFIB','DEBUG_H','Debug',8,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_CON4','SHIP','Shipment Characteristics',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_CON4','APPL_H','Application',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_CON4','RG_H','Route Generation',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_CON4','OPTM_H','Optimization',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_CON4','DEBUG_H','Debugging',5,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_CON4','CPLEX_H','Cplex',6,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_AGGR','DEBUG','Debugging',5,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_SPLT','APP','Application',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_SPLT','MODE_SL','Mode Service Level',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','ID_GEN','ID Generation',10,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','MISC','Miscellaneous',-1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VALID8','ALERT','Alerts',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VALID8','FLEX','Alerts, Warnings, or Errors',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VALID8','WARNING','Warnings',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VALID8','ERROR','Errors',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_AGGR','APP','Existing Shipments',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_AGGR','MODE_SL','Mode Service Level',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_AGEX','SHIP_MOD','Existing Shipments',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_AGGR','ZS_MODE','Pooling: LineHaul Shipments',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_AGEX','APP','Existing Shipments',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','PO_ACCPT','PO Acceptance',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'VV','VVALERT','Vendor Visibility Alert',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','MISC','Miscellaneous',5,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','SAOR','Store Orders',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','LPN','LPNs',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','EDI','EDI',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_PFLT','SHIP','Shipment Characteristics',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_PFLT','RG_H','Route Generation',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_PFLT','OPTM_H','Optimization',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_PFLT','DEBUG_H','Debugging',5,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','ASN','Advance Shipment Notice',6,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','COGI','Cost of Goods Invoice',7,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','ST_ORD','Store Orders',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','FULF_ORD','Fulfillment Orders',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','SKU_SUBS','Substitution',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','SOURCING','Sourcing',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','ALLOC','Allocation',5,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','RELEASE','Release',6,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','MISC','Miscellaneous',7,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_FLEET','MISC','Miscellaneous',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','OPTIMIZE','Optimization Parameters',8,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','DCPRIOR','Prioritize by DC - Configuration',9,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOMREAPP','OPTIMIZE','Optimization Parameters',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOMREAPP','DCPRIOR','Prioritize by DC - Configuration',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','PRIORTZ','Order Prioritization',7,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DOM','SCHED','Delivery Scheduling',10,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','AGGR_CP','Aggregated RTS Parameters',8,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','PAR','Parcel',9,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_VNDR','FED','FedEx',10,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_CON4','DT_H','Distance Time Parameter set Parameters',6,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CON_PFLT','DT_H','Distance Time Parameter Set Parameters',6,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_TEPE','SROUTE','Static Route',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_TEPE','CLSHIP','Close Shipment',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_WM','E-SIGN','E-Signature Parameter',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_WM','WAVE','Wave Parameters',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','GEN','General',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','ASN','ASN',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','DO','Distribution Order',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','PO','Purchase Order',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','RTS','RTS',5,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','FULF_ORD','Sales Order',6,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','SHPMNT','Shipment',7,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','INTFC','Interface',8,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','DOCRPTS','Documents and Reports',9,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_GEN','TRNSPTN','Transportation',11,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_WM','INVN','Inventory Parameters',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_WM','AGG_CRIT','DO Aggregation Criteria',4,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'EPI','EPI','EPI',1,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_TEPE','MNFT','Manifesting',2,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_TEPE','EPI','External Parcel Integration (EPI)',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COM_WM','DOM_FTP','DOM System - File Upload',3,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO PARAM_SUBGROUP ( PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DM','Mobile','DM Mobile Parameters',4,SYSTIMESTAMP,SYSTIMESTAMP);

Commit;





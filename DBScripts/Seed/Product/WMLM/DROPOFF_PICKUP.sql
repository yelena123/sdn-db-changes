set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DROPOFF_PICKUP

INSERT INTO DROPOFF_PICKUP ( DROPOFF_PICKUP,DESCRIPTION) 
VALUES  ( 'DROP','Drop-box letter center or carrier location');

INSERT INTO DROPOFF_PICKUP ( DROPOFF_PICKUP,DESCRIPTION) 
VALUES  ( 'DAILY','Daily pickup service');

INSERT INTO DROPOFF_PICKUP ( DROPOFF_PICKUP,DESCRIPTION) 
VALUES  ( 'CALL','On call air pickup');

INSERT INTO DROPOFF_PICKUP ( DROPOFF_PICKUP,DESCRIPTION) 
VALUES  ( 'CUST','Customer counter');

INSERT INTO DROPOFF_PICKUP ( DROPOFF_PICKUP,DESCRIPTION) 
VALUES  ( 'ONCE','One time pickup');

Commit;





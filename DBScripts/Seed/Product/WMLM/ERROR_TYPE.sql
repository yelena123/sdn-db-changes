set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ERROR_TYPE

INSERT INTO ERROR_TYPE ( ERROR_TYPE,DESCRIPTION) 
VALUES  ( 4,'Error');

INSERT INTO ERROR_TYPE ( ERROR_TYPE,DESCRIPTION) 
VALUES  ( 8,'Warning');

INSERT INTO ERROR_TYPE ( ERROR_TYPE,DESCRIPTION) 
VALUES  ( 12,'Alert');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for APPT_UPLOAD_TYPE

INSERT INTO APPT_UPLOAD_TYPE ( APPT_UPLOAD_TYPE,DESCRIPTION) 
VALUES  ( 1,'Daily Capacity');

INSERT INTO APPT_UPLOAD_TYPE ( APPT_UPLOAD_TYPE,DESCRIPTION) 
VALUES  ( 2,'Total Capacity');

INSERT INTO APPT_UPLOAD_TYPE ( APPT_UPLOAD_TYPE,DESCRIPTION) 
VALUES  ( 3,'Priority Setup');

INSERT INTO APPT_UPLOAD_TYPE ( APPT_UPLOAD_TYPE,DESCRIPTION) 
VALUES  ( 4,'Infeasibility Setup');

INSERT INTO APPT_UPLOAD_TYPE ( APPT_UPLOAD_TYPE,DESCRIPTION) 
VALUES  ( 5,'Appointment Priority Setup');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for HIERARCHY_DEFINITIONS

INSERT INTO HIERARCHY_DEFINITIONS ( HIERARCHY_DEFINITION_ID,NAME,DESCRIPTION,HIERARCHY_TYPE,IS_ACTIVE,TC_COMPANY_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DOMAIN_ATTR_1,DOMAIN_ATTR_2) 
VALUES  ( 1,'Task Hierarchy','Seed Task Hierarchy','TASK',1,1,1,'Manhattan Associates',SYSDATE,1,
'Manhattan Associates',SYSDATE,'TASK_TYPE',null);

Commit;





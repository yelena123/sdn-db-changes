set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DO_STATUS

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 112,'Special Processing Pending');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 100,'Draft');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 105,'Pre-Released');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 110,'Released');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 115,'Selected for Preview Wave');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 120,'Partially DC Allocated');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 130,'DC Allocated');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 140,'In Packing');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 150,'Packed');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 160,'Weighed');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 170,'Manifested');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 180,'Loaded');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 190,'Shipped');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 200,'Cancelled');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 185,'Partially Shipped');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 165,'Staged');

INSERT INTO DO_STATUS ( ORDER_STATUS,DESCRIPTION) 
VALUES  ( 195,'Delivered');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TRANS_RESP_CODE

INSERT INTO TRANS_RESP_CODE ( TRANS_RESP_CODE,DESCRIPTION) 
VALUES  ( 'SHP','Shipper');

INSERT INTO TRANS_RESP_CODE ( TRANS_RESP_CODE,DESCRIPTION) 
VALUES  ( 'VEN','Business Partner');

INSERT INTO TRANS_RESP_CODE ( TRANS_RESP_CODE,DESCRIPTION) 
VALUES  ( 'CPU','Customer Pickup');

Commit;





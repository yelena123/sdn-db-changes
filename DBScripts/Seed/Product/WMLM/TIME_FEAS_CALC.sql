set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TIME_FEAS_CALC

INSERT INTO TIME_FEAS_CALC ( TIME_FEAS_CALC,DESCRIPTION) 
VALUES  ( 'B','Backwards End');

INSERT INTO TIME_FEAS_CALC ( TIME_FEAS_CALC,DESCRIPTION) 
VALUES  ( 'F','Forward Start');

INSERT INTO TIME_FEAS_CALC ( TIME_FEAS_CALC,DESCRIPTION) 
VALUES  ( 'N','No Method');

INSERT INTO TIME_FEAS_CALC ( TIME_FEAS_CALC,DESCRIPTION) 
VALUES  ( 'C','Backwards Start');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DISTRIBUTION_LIST

INSERT INTO DISTRIBUTION_LIST ( DL_ID,TC_COMPANY_ID,DESCRIPTION,IS_PREFERRED_LIST,TC_CONTACT_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 25,-1,'All Company Carriers',1,0,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP);

INSERT INTO DISTRIBUTION_LIST ( DL_ID,TC_COMPANY_ID,DESCRIPTION,IS_PREFERRED_LIST,TC_CONTACT_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 26,-1,'All Lane Carriers',2,0,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP);

INSERT INTO DISTRIBUTION_LIST ( DL_ID,TC_COMPANY_ID,DESCRIPTION,IS_PREFERRED_LIST,TC_CONTACT_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 27,0,'All Company Carriers',1,0,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP);

INSERT INTO DISTRIBUTION_LIST ( DL_ID,TC_COMPANY_ID,DESCRIPTION,IS_PREFERRED_LIST,TC_CONTACT_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 28,0,'All Lane Carriers',2,0,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP);

INSERT INTO DISTRIBUTION_LIST ( DL_ID,TC_COMPANY_ID,DESCRIPTION,IS_PREFERRED_LIST,TC_CONTACT_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 1,-2,'All Company Carriers',1,0,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP);

INSERT INTO DISTRIBUTION_LIST ( DL_ID,TC_COMPANY_ID,DESCRIPTION,IS_PREFERRED_LIST,TC_CONTACT_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 2,-2,'All Lane Carriers',2,0,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP);

INSERT INTO DISTRIBUTION_LIST ( DL_ID,TC_COMPANY_ID,DESCRIPTION,IS_PREFERRED_LIST,TC_CONTACT_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 3,1,'All Company Carriers',1,0,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP);

INSERT INTO DISTRIBUTION_LIST ( DL_ID,TC_COMPANY_ID,DESCRIPTION,IS_PREFERRED_LIST,TC_CONTACT_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 4,1,'All Lane Carriers',2,0,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP);

Commit;





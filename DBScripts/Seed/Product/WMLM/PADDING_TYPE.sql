set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for PADDING_TYPE

INSERT INTO PADDING_TYPE ( PADDING_TYPE,DESCRIPTION,PADDING_PATTERN) 
VALUES  ( 4,'None',null);

INSERT INTO PADDING_TYPE ( PADDING_TYPE,DESCRIPTION,PADDING_PATTERN) 
VALUES  ( 8,'Add Leading Zeroes','0');

INSERT INTO PADDING_TYPE ( PADDING_TYPE,DESCRIPTION,PADDING_PATTERN) 
VALUES  ( 12,'Add Trailing Zeroes','0');

Commit;





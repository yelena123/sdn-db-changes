set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for WM_QUEUE_CLASS

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_MHE_OUTBOUND_LOW_PRIORITY_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_MHE_OUTBOUND_HIGH_PRIORITY_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_INBOUND_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_LPN_DISPOSITION_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_LPNDISPOSITION_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_RECEIVING_OUTBOUND_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_WAVE_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'OM_SCHD_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_BOOKING_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_MHE_INBOUND_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_MHE_OUTBOUND_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_MHE_INBOUND_HIGH_PRIORITY_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_MHE_INBOUND_LOW_PRIORITY_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

INSERT INTO WM_QUEUE_CLASS ( QUEUE_NAME,QUEUE_CLASS) 
VALUES  ( 'jms.queue.WM_OUTBOUND_QUEUE','com.manh.wm.services.queue.WMQueueInfo');

Commit;





set echo on

-- create the product tables, pkeys, and synonyms
@@WMLM_Tables_PKs.sql
@@WMLM_Tables_Column_Comments.sql
@@WMLM_Synonyms.sql

-- load WMLM static data
@@WMLM_StaticData.sql

exit

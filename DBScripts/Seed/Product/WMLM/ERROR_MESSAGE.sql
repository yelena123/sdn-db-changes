set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ERROR_MESSAGE

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5001,'Process Date is a required field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5002,'Invalid date format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5003,'Incoming Message Exceeds Max. Length');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5004,'Outgoing Message Exceeds Max. Length');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5005,'Incoming Message Id Should be numeric');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5006,'Outgoing Message Id Should be numeric');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500278,'Invalid Merge Facility {0} on line {1}');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500279,'Invalid Order Fulfillment Option {0} on Line {1}');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1630256,'Invalid Destination Action {0} for DO {1}');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 6100180,'LPN {0} status is not valid');

Commit;





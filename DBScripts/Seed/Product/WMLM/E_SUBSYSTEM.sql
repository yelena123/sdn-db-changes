set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for E_SUBSYSTEM

INSERT INTO E_SUBSYSTEM ( SUBSYSTEM_ID,SUBSYSTEM_NAME,DESCRIPTION,XML_FILE_NAME,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,
MISC_NUM_2,VERSION_ID) 
VALUES  ( 1,'locn','Location grabber','locn.xml',sysdate,sysdate,'LMADMIN',null,null,null,
null,1);

INSERT INTO E_SUBSYSTEM ( SUBSYSTEM_ID,SUBSYSTEM_NAME,DESCRIPTION,XML_FILE_NAME,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,
MISC_NUM_2,VERSION_ID) 
VALUES  ( 2,'itm','Item grabber','itm.xml',sysdate,sysdate,'LMADMIN',null,null,null,
null,1);

INSERT INTO E_SUBSYSTEM ( SUBSYSTEM_ID,SUBSYSTEM_NAME,DESCRIPTION,XML_FILE_NAME,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,
MISC_NUM_2,VERSION_ID) 
VALUES  ( 3,'hta','Handling Attribute grabber','hta.xml',sysdate,sysdate,'LMADMIN',null,null,null,
null,1);

INSERT INTO E_SUBSYSTEM ( SUBSYSTEM_ID,SUBSYSTEM_NAME,DESCRIPTION,XML_FILE_NAME,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,
MISC_NUM_2,VERSION_ID) 
VALUES  ( 21,'asn','ASN grabber','asn.xml',sysdate,sysdate,'lmadmin','1','1',1,
1,1);

INSERT INTO E_SUBSYSTEM ( SUBSYSTEM_ID,SUBSYSTEM_NAME,DESCRIPTION,XML_FILE_NAME,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,
MISC_NUM_2,VERSION_ID) 
VALUES  ( 22,'distro','Distro grabber','distro.xml',sysdate,sysdate,'lmadmin','1','1',1,
1,1);

INSERT INTO E_SUBSYSTEM ( SUBSYSTEM_ID,SUBSYSTEM_NAME,DESCRIPTION,XML_FILE_NAME,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,
MISC_NUM_2,VERSION_ID) 
VALUES  ( 23,'pkt','Pickticket grabber','pkt.xml',sysdate,sysdate,'lmadmin','1','1',1,
1,1);

Commit;





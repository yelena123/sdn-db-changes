set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SOA_MESSAGE_FRAGMENT

INSERT INTO SOA_MESSAGE_FRAGMENT ( ID,NAME,BUCKETNAME,XPATH,DESCRIPTION,OPTIONS,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 0,'<None>',null,null,null,16384,SYSTIMESTAMP,systimestamp);

INSERT INTO SOA_MESSAGE_FRAGMENT ( ID,NAME,BUCKETNAME,XPATH,DESCRIPTION,OPTIONS,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 17054001,'WM.MHE.Parser.Input','$REQUEST','message/data',null,4096,SYSTIMESTAMP,systimestamp);

INSERT INTO SOA_MESSAGE_FRAGMENT ( ID,NAME,BUCKETNAME,XPATH,DESCRIPTION,OPTIONS,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 17054002,'WM.MHE.Parser.Output','SOA.Variables',' ',null,0,SYSTIMESTAMP,systimestamp);

INSERT INTO SOA_MESSAGE_FRAGMENT ( ID,NAME,BUCKETNAME,XPATH,DESCRIPTION,OPTIONS,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 18546689,'CPS.HTML','SOA.Listener.HTTP','response/body',null,4096,SYSTIMESTAMP,systimestamp);

INSERT INTO SOA_MESSAGE_FRAGMENT ( ID,NAME,BUCKETNAME,XPATH,DESCRIPTION,OPTIONS,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 18546690,'CPS.GetMessages','SOA.Listener.HTTP','text/xml',null,0,SYSTIMESTAMP,systimestamp);

INSERT INTO SOA_MESSAGE_FRAGMENT ( ID,NAME,BUCKETNAME,XPATH,DESCRIPTION,OPTIONS,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 19202048,'<SOAMessage>',null,null,null,0,SYSTIMESTAMP,systimestamp);

INSERT INTO SOA_MESSAGE_FRAGMENT ( ID,NAME,BUCKETNAME,XPATH,DESCRIPTION,OPTIONS,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 19202049,'SOA.Listener.HTTP/request/body','SOA.Listener.HTTP','request/body',null,16384,SYSTIMESTAMP,systimestamp);

INSERT INTO SOA_MESSAGE_FRAGMENT ( ID,NAME,BUCKETNAME,XPATH,DESCRIPTION,OPTIONS,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 19202050,'SOA.Listener.HTTP/response/body','SOA.Listener.HTTP','response/body',null,16384,SYSTIMESTAMP,systimestamp);

Commit;





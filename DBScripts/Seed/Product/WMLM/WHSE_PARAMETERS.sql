set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for WHSE_PARAMETERS

INSERT INTO WHSE_PARAMETERS ( WHSE_MASTER_ID,ADDR_1,ADDR_2,CITY,STATE,ZIP,CNTRY,TEL_NBR,PKT_ADDR_1,PKT_ADDR_2,
PKT_CITY,PKT_STATE,PKT_ZIP,PKT_CNTRY,PKT_TEL_NBR,LABEL_ADDR_1,LABEL_ADDR_2,LABEL_CITY,LABEL_STATE,LABEL_ZIP,
LABEL_CNTRY,LABEL_TEL_NBR,GET_EPC_W_WAVE_FLAG,ASSIGN_EPC_IN_ACTIVE_FLAG,EPC_REQD_ON_ALL_CASES_FLAG,EPC_REQD_ON_ALL_CARTONS_FLAG,EPC_MATCH_LOCK_CODE,AES_PSWD,AES_FILE_UPLOAD_PATH,LM_MONITOR_INSTL_FLAG,
DSP_ITEM_DESC_FLAG,LIMIT_USER_IN_TRVL_AISLE_FLAG,LOOK_UP_XREF,I2O5_PACK_QTY_REF,I2O5_CASE_QTY_REF,I2O5_TIER_QTY_REF,I2O5_PLT_QTY_REF,TRK_PROD_FLAG,ELS_INSTL,SMARTINFO_INSTL,
INFOLINK_INSTL,USE_LOCK_CODE_PRTY_PIX,TRK_MULT_PACK_QTY,TRK_ACTIVE_MULT_PACK_QTY,ELS_TASK_ACTVTY_CODE,ELS_TASK_INTERFACE,ELS_LOG,EAN_ENABLED_FLAG,EAN_PREFIX,EAN_SEPARATOR,
EAN_MAX_LEN,EAN_NBR_OF_SCANS,LOCN_CHK_DIGIT_FLAG,USE_LOCN_ID_AS_BRCD,WORKINFO_SERVER_FACTORY_NAME,FTZ_TRK_FLAG,ADJ_ALG_FLAG,ZONE_PAYMENT_STAT,SKU_LEVEL,DWP_INTERFACE_ID,
CNTY,REPORT_TRANS,VOCOLLECT_PTS_WORK_ID_DESC,VOCOLLECT_PTS_LUT_FLAG,VOCOLLECT_PACK_WORK_ID_DESC,VOCOLLECT_PACK_LUT_FLAG,VOCOLLECT_PACK_CNTR_TRACK_FLAG,HNDL_ATTR_UOM_ACT,HNDL_ATTR_UOM_CP,SRL_TRK_FLAG,
SRL_PIX_FLAG,ALLOW_SRL_CREATE,DUP_SRL_NBR_FLAG,SRL_NBR_LIST_MAX,CUBISCAN_IGNORE_STYLE_SFX,CUBISCAN_IGNORE_COLOR,CUBISCAN_IGNORE_COLOR_SFX,CUBISCAN_IGNORE_SEC_DIM,CUBISCAN_IGNORE_QUAL,CUBISCAN_IGNORE_SIZE_DESC,
CUBISCAN_OVERWRITE,TASK_EXCEPTION_FLAG,FIFO_MAINT_FLAG,GEN_EAN_CNTR_NBR_FLAG,LAST_SLOT_DOWNLOAD_DATE,NBR_OF_DYN_ACTV_PICK_PER_SKU,NBR_OF_DYN_CASE_PICK_PER_SKU,DEFAULT_PRICE_TIX_LOCK_CODE,INVN_UPD_ON_PAKNG_CHG,INVN_UPD_ON_CANCEL_PKT,
TI_UPD_ON_RESET_WAVE_PKT,TI_UPD_ON_PAKNG_CHG,TI_UPD_ON_CANCEL_PKT,UPD_ALLOC_ON_PICK_WAVE,TRK_ACTV_BY_ID_CODES,TRK_ACTV_BY_BATCH_NBR,TRK_ACTV_BY_PROD_STAT,TRK_ACTV_BY_CNTRY_OF_ORGN,HOT_TASK_PRTY,DFLT_PUTWY_TYPE,
PUTWY_LOCN_LOCK_CODE,DFLT_CAPCTY_UOM,MXD_SKU_PUTWY_TYPE,ALLOW_WORK_ORD_QTY_CHG,ALLOW_WORK_ORD_SKU_ADDN,ALLOW_PAKNG_W_OUT_CASE_NBR,TRK_CASE_PICK_BY_ID_CODE,TRK_CASE_PICK_BY_PROD_STAT,TRK_CASE_PICK_BY_BATCH_NBR,TRK_CASE_PICK_BY_CNTRY_OF_ORGN,
WARN_RCPT_PCNT_PO_SKU,WARN_RCPT_PCNT_SHMT_PO_SKU,ERROR_RCPT_PCNT_PO_SKU,ERROR_RCPT_PCNT_SHMT_PO_SKU,OVRD_RCPT_PCNT_PO_SKU,OVRD_RCPT_PCNT_SHMT_PO_SKU,NBR_OF_WAVE_BEFORE_LETUP,NBR_OF_WAVE_BEFORE_REUSE,XCESS_WAVE_NEED_PROC_TYPE,PICK_LOCN_ASSIGN_SEQ_FLAG,
CALC_TO_BE_LOCD,XFER_MAX_UOM,ALLOW_SKU_NOT_ON_SHPMT,ALLOW_SKU_NOT_ON_PO,ASN_SKU_LVL_VERF_PIX,ASN_CASE_LVL_VERF_PIX,PO_SKU_LVL_VERF_PIX,SKU_CNSTR,SLOT_IT_USED,TASK_PRIORITIZATION,
DFLT_PIKNG_LOCN_CODE,RLS_HELD_RPLN_TASKS,DFLT_PALLET_HT,CALC_CASE_SIZE_TYPE,PURGE_ASN_STAT_CODE,SLOTINFO_DOWNLOAD_FILE_PATH,SLOTINFO_UPLOAD_FILE_PATH,SLOTINFO_ONE_USER_PER_GRP,SLOTINFO_TEMP_LOCN_ID,SLOTINFO_CRIT_NBR,
BATCH_NBR_EQ_VENDOR_BATCH_NBR,ALLOW_WEIGH_HELD_BATCH,ALLOW_MANIF_HELD_BATCH,ALLOW_LOAD_HELD_BATCH,ALLOW_CLOSE_LOAD_HELD_BATCH,INCUB_CTRL_FLAG,DFLT_INCUB_LOCK,BASE_INCUB_FLAG,CASE_NBR_DSP_USAGE,SORT_ID_CODES,
CASE_WGHT_TOL,DFLT_CONS_PRTY_SINGL_SKU_FULL,DFLT_CONS_PRTY_PARTL,DFLT_CONS_PRTY_MXD,DFLT_CONS_DATE,DFLT_CONS_SEQ_SINGL_SKU_FULL,DFLT_CONS_SEQ_PARTL,DFLT_CONS_SEQ_MXD,DFLT_CASE_SIZE_TYPE,LOST_IN_WHSE_LOCK_CODE,
QUAL_HELD_LOCK_CODE,LOST_IN_CYCLE_CNT_LOCK_CODE,LOST_ON_PLT_LOCK_CODE,LOST_ON_PUTWY_LOCK_CODE,MULT_SKU_CASE_LOCK_CODE,PENDING_CYCLE_CNT_LOCK_CODE,CASE_ALLOC_STRK,VENDOR_PERFRM_PIX,ALLOW_RCV_W_OUT_CASE_ASN,ALLOW_RCV_W_OUT_SHPMT,
ALLOW_RCV_W_OUT_PO_NBR,ALLOW_RLS_QUAL_HELD_ON_RCPT,ALLOW_RCV_W_OUT_CASE_NBR,CALC_REPL_CTRL,CMBIN_REPL_PRTY,REPL_ACTV_W_WAVE,REPL_CASE_PICK_W_WAVE,PURGE_CASE_BEYOND_STAT_CODE,PPICK_REPL_CTRL,PHYS_INVN_CNT_ADJ_RSN_CODE,
INVN_UPD_ON_RESET_WAVE_PKT,ALLOW_MIXED_CD_IN_RESV_FLAG,DFLT_PLT_TYPE,WHEN_TO_PRT_PLT_LBL,SUPPRESS_CTN_LBL,ACTV_REPL_ORGN,DFLT_RECALL_CARTON_LOCK,OB_QA_LVL,VICS_BOL_FLAG,DFLT_STOP_SEQ_ORD,
TRACK_OB_RETN_LEVEL,USE_INBD_LPN_AS_OUTBD_LPN,HAZMAT_CNTCT_LINE1,HAZMAT_CNTCT_LINE2,PRICE_TKT_TYPE,CREATE_ASN_FLAG,WAVE_SUSPND_OPTN,PRT_CONTENT_LBL_ON_CTN_MODIFY,PRT_PACK_SLIP_ON_CTN_MODIFY,VERF_CARTON_CONTNT,
PRT_PKT_W_WAVE,WHEN_TO_PRT_CTN_CONTENT_LBL,WHEN_TO_PRT_PAKNG_SLIP,WHEN_TO_PRT_CTN_SHIP_LBL,ASSIGN_AND_PRT_MANIF,ASSIGN_AND_PRT_BOL,OVRPK_CTRL,OVRPK_PCNT,OVRPK_BOXES,GEN_ITEM_BRCD,
DFLT_UNIT_WT,DFLT_UNIT_VOL,DFLT_UPC_PRE_DIGIT,DFLT_UPC_VENDOR_CODE,CARTON_BREAK_ON_AREA_CHG,CARTON_BREAK_ON_ZONE_CHG,CARTON_BREAK_ON_AISLE_CHG,OVRPK_PARCL_CARTON,CARTON_TYPE,PLT_TYPE,
GENRT_CARTON_ASN,MANIF_INCMPL_ORD,TRIG_INVC_PKT_STS,AUTO_MANIF_AT_PNH,PKT_STAT_CHG_PIX,RTE_WITH_WAVE,ASSIGN_PRO_NBR,CARTON_WT_TOL,CREATE_MAJOR_PKT,REPL_TASK_UPDT_PRTY,
INVC_MODE,ALLOW_REPL_TASK_UPDT_PRTY,CREATE_SHPMT,CREATE_LOAD,DFLT_BOL_TYPE,DFLT_ALLOC_TYPE,DFLT_ALLOC_CUTOFF,DFLT_SKU_SUB,AUDIT_CREATED_SOURCE,AUDIT_CREATED_SOURCE_TYPE,
AUDIT_CREATED_DTTM,AUDIT_LAST_UPDATED_SOURCE,AUDIT_LAST_UPDATED_SOURCE_TYPE,AUDIT_LAST_UPDATED_DTTM,AUDIT_TRANSACTION,AUDIT_PARTY_ID,MARK_FOR_DELETION,DFLT_UNIT_VALUE,GEN_SHIP_CONF,MAX_CC_RECNT,
ACTV_LOCATION_THRESHOLD_PCNT,CASE_PICK_LOCN_THRESHOLD_PCNT,AUTO_GEN_CC_TASK,QUAL_AUD_FAILED_LOCK_CODE,ELS_ONE_WAY_DEFERRED,LABEL_ADDR_3,ORDER_CONTACT,LABEL_CONTACT,PLAN_PARCEL_IN_WM,RETAIL_SHIPMENT_PLANNING,
ELS_TRAVEL_ACTVTY,OUTBD_AUDIT_LEVEL,CC_UNIT_TOLER_VALUE,CC_WGT_TOLER_VALUE,CC_DLR_TOLER_VALUE,CC_PCNT_TOLER_VALUE,ERP_HOST_TYPE,CO_ID,CO_ID_NAME,PERFORM_PRE_RECEIPT_ALLOC,
PRE_RECEIPT_TOLERANCE_PCT,ALLOW_SHIPPABLE_INNERS,REVIVE_ORDER_LINE,EXECUTE_CLOSE_ORDER_LINE,AGGR_ORD_LIFECYCLE_STATUS,CREATED_DTTM,LAST_UPDATED_DTTM,OLPN_BREAK_ON_LOCN_CLASS,CPCTY_EST_FLAG,EST_NBR_OLPN,
SPLIT_ASN_DTL_BY_DO,LM_PUSH_INTERVAL) 
VALUES  ( 1,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,
null,null,'N','N','N','N',null,null,null,'N',
null,'N','Y',null,null,null,null,'Y','0','0',
'0','N','N','N','?',null,null,0,null,null,
null,null,0,0,'WiServerFactory','1',null,null,7,null,
null,'N','iLPN Number','N','Task Number','N','N','0','2',0,
1,1,0,9999,0,0,0,0,0,0,
0,0,0,'N',null,0,0,'?','A','T',
12,12,12,'Y','N','N','N','N',0,null,
null,'C',null,'N','N','N','N','N','N','N',
10,10,10,10,10,10,0,0,0,'1',
'N','N','N','N','2','2',null,'1','N','2',
null,'N',0,'0',90,'/WMS/MANH2010R1/QA/data/','/WMS/MANH2010R1/QA/data/','N','000009648',null,
'N',null,'N','N','N','N','IH','1','R','N',
0,null,null,null,null,null,null,null,null,'?',
'?','?','?','?','?','?',0,'3','N','1',
'N','N','N','1',null,'R','N',95,'2',null,
'A','N',null,2,'N','0',null,0,'N',null,
null,'N',null,null,null,null,null,'N','N','N',
'N','1','1','3',null,null,'0',0,0,null,
0,0,null,null,'N','N','N','N',null,'ATP',
'Y','N',90,'0','N','0','N',0,'0',10,
0,null,'Y','Y','1',null,0,null,null,2,
SYSTIMESTAMP,'SEED',1,SYSTIMESTAMP,null,45,0,null,null,null,
null,null,'N',null,'N',null,null,null,null,0,
null,null,null,null,null,null,null,null,null,'0',
null,'0',null,null,'150',SYSDATE,systimestamp,0,0,0,
0,null);

Commit;





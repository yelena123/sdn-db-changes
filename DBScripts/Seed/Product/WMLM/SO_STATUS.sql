set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SO_STATUS

INSERT INTO SO_STATUS ( SO_STATUS,DESCRIPTION) 
VALUES  ( 40,'Complete');

INSERT INTO SO_STATUS ( SO_STATUS,DESCRIPTION) 
VALUES  ( 10,'Open');

INSERT INTO SO_STATUS ( SO_STATUS,DESCRIPTION) 
VALUES  ( 20,'In Progress');

INSERT INTO SO_STATUS ( SO_STATUS,DESCRIPTION) 
VALUES  ( 30,'Closed');

Commit;





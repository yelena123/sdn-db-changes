set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_PROTOCOL

INSERT INTO CL_PROTOCOL ( PROTOCOL_ID,PROTOCOL_NAME,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'MHE',SYSDATE,systimestamp);

INSERT INTO CL_PROTOCOL ( PROTOCOL_ID,PROTOCOL_NAME,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'Vocollect',SYSDATE,systimestamp);

Commit;





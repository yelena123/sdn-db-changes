SET DEFINE OFF;
SET ECHO ON;
BEGIN
dbms_scheduler.create_job('LRF_SCHED_EVENT_RPT_PURGE_JOB',
job_type=>'PLSQL_BLOCK', job_action=>
'begin purge_lrf_sched_event_report (p_commit_frequency => 10000,p_purge_days => 7 ); end;'
, number_of_arguments=>0,
start_date=>systimestamp, repeat_interval=>
'FREQ=DAILY; BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN; BYHOUR=03; BYMINUTE=30;'
, end_date=>NULL,
job_class=>'DEFAULT_JOB_CLASS', enabled=>FALSE, auto_drop=>TRUE,comments=>
'LRF_SCHED_EVENT_RPT_PURGE_JOB job'
);
dbms_scheduler.enable('LRF_SCHED_EVENT_RPT_PURGE_JOB');
COMMIT;
END;
/
BEGIN
dbms_scheduler.create_job('CL_MESSAGE_PURGE_JOB',
job_type=>'PLSQL_BLOCK', job_action=>
'begin purge_cl_message (p_commit_frequency => 10000,p_purge_days => 7 ); end;'
, number_of_arguments=>0,
start_date=>systimestamp, repeat_interval=>
'FREQ=DAILY; BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN; BYHOUR=02; BYMINUTE=30;'
, end_date=>NULL,
job_class=>'DEFAULT_JOB_CLASS', enabled=>FALSE, auto_drop=>TRUE,comments=>
'CL_MESSAGE_PURGE_JOB job'
);
dbms_scheduler.enable('CL_MESSAGE_PURGE_JOB');
COMMIT;
END;
/
BEGIN
dbms_scheduler.create_job('EVENT_MESSAGE_PURGE_JOB',
job_type=>'PLSQL_BLOCK', job_action=>
'begin purge_event_message (p_commit_frequency => 10000,p_purge_days => 3 ); end;'
, number_of_arguments=>0,
start_date=>systimestamp, repeat_interval=>
'FREQ=DAILY; BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN; BYHOUR=02;'
, end_date=>NULL,
job_class=>'DEFAULT_JOB_CLASS', enabled=>FALSE, auto_drop=>TRUE,comments=>
'purge_event_message job'
);
dbms_scheduler.enable('EVENT_MESSAGE_PURGE_JOB');
COMMIT;
END;
/
BEGIN
dbms_scheduler.create_job('OM_SCHED_EVENT_PURGE_JOB',
job_type=>'PLSQL_BLOCK', job_action=>
'begin purge_om_sched_event (p_commit_frequency => 10000,p_purge_days => 7 ); end;'
, number_of_arguments=>0,
start_date=>systimestamp, repeat_interval=>
'FREQ=DAILY; BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN; BYHOUR=03;'
, end_date=>NULL,
job_class=>'DEFAULT_JOB_CLASS', enabled=>FALSE, auto_drop=>TRUE,comments=>
'OM_SCHED_EVENT_PURGE_JOB job'
);
dbms_scheduler.enable('OM_SCHED_EVENT_PURGE_JOB');
COMMIT;
END;
/

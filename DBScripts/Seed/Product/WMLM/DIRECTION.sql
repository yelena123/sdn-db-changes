set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DIRECTION

INSERT INTO DIRECTION ( DIRECTION,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'I','Inbound',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO DIRECTION ( DIRECTION,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'O','Outbound',SYSTIMESTAMP,SYSTIMESTAMP);

Commit;





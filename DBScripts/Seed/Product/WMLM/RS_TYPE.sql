set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RS_TYPE

INSERT INTO RS_TYPE ( RS_TYPE,DESCRIPTION,FOR_MANUAL_USE) 
VALUES  ( 'Core','Core',0);

INSERT INTO RS_TYPE ( RS_TYPE,DESCRIPTION,FOR_MANUAL_USE) 
VALUES  ( 'MF','Capacity Finder - Dynamic',1);

INSERT INTO RS_TYPE ( RS_TYPE,DESCRIPTION,FOR_MANUAL_USE) 
VALUES  ( 'Sel','Selector',0);

INSERT INTO RS_TYPE ( RS_TYPE,DESCRIPTION,FOR_MANUAL_USE) 
VALUES  ( 'CF','Capacity Finder - Contract',1);

INSERT INTO RS_TYPE ( RS_TYPE,DESCRIPTION,FOR_MANUAL_USE) 
VALUES  ( 'GRS','Global Resource Selector',0);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for REPORT_DEF

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'CarrUtlCmt','CarrierUtilizationAndCommitment','Report provides the measurement of commitments by carrier, and how the shipper is meeting those demands.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'RepCList','CarrierList','To generate a report to find the carriers added for the day, week, etc.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'RepDlySchd','DailyScheduleByOriginAndCarrier','Report lists the daily delivery schedule based on the loading facilities for a specified date or date range.','OracleDB','MR');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for E_OBS_INT

INSERT INTO E_OBS_INT ( INT_TYPE_ID,INT_TYPE,DESCRIPTION,WHSE,SYS_CREATED,CREATE_DATE_TIME,MOD_DATE_TIME,VERSION_ID) 
VALUES  ( 1,'Performance','Performance','01','Y',sysdate,sysdate,0);

INSERT INTO E_OBS_INT ( INT_TYPE_ID,INT_TYPE,DESCRIPTION,WHSE,SYS_CREATED,CREATE_DATE_TIME,MOD_DATE_TIME,VERSION_ID) 
VALUES  ( 2,'Quality','Quality','01','Y',sysdate,sysdate,0);

INSERT INTO E_OBS_INT ( INT_TYPE_ID,INT_TYPE,DESCRIPTION,WHSE,SYS_CREATED,CREATE_DATE_TIME,MOD_DATE_TIME,VERSION_ID) 
VALUES  ( 3,'Attendance','Attendance','01','Y',sysdate,sysdate,0);

Commit;





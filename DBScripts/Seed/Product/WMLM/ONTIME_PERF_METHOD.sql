set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ONTIME_PERF_METHOD

INSERT INTO ONTIME_PERF_METHOD ( ONTIME_PERF_METHOD,DESCRIPTION) 
VALUES  ( 'POINT','Point');

INSERT INTO ONTIME_PERF_METHOD ( ONTIME_PERF_METHOD,DESCRIPTION) 
VALUES  ( 'WINDOW','Window');

Commit;





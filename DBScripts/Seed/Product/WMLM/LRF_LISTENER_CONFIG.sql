set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LRF_LISTENER_CONFIG

INSERT INTO LRF_LISTENER_CONFIG ( QUEUE_NAME,TRANSACTION_TIMEOUT_SEC,INITIAL_CONSUMER_COUNT,MAX_CONSUMER_COUNT,DESCRIPTION) 
VALUES  ( 'jms.queue.LRF_REPORT_QUEUE',600,5,15,null);

Commit;





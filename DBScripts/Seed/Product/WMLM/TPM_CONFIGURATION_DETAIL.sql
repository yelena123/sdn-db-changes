set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TPM_CONFIGURATION_DETAIL

INSERT INTO TPM_CONFIGURATION_DETAIL ( RULE,OBJECT,PARAMETER,PARAMETER_VALUE,ACTION_TYPE) 
VALUES  ( 1,'ASN','INVENTORY_STATUS','AVAILABLE','DELETE');

INSERT INTO TPM_CONFIGURATION_DETAIL ( RULE,OBJECT,PARAMETER,PARAMETER_VALUE,ACTION_TYPE) 
VALUES  ( 1,'ASN','INVENTORY_TYPE','CLOSED','EXCLUDE');

INSERT INTO TPM_CONFIGURATION_DETAIL ( RULE,OBJECT,PARAMETER,PARAMETER_VALUE,ACTION_TYPE) 
VALUES  ( 1,'ASN','IS_CANCELLED','TRUE','EXCLUDE');

INSERT INTO TPM_CONFIGURATION_DETAIL ( RULE,OBJECT,PARAMETER,PARAMETER_VALUE,ACTION_TYPE) 
VALUES  ( 1,'ASN','ALLOCATED_QTY',null,'IGNORE');

INSERT INTO TPM_CONFIGURATION_DETAIL ( RULE,OBJECT,PARAMETER,PARAMETER_VALUE,ACTION_TYPE) 
VALUES  ( 2,'GENERIC','ALLOCATED_QTY',null,'IGNORE');

Commit;





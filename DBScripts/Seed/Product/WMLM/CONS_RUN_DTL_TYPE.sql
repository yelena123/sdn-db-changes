set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CONS_RUN_DTL_TYPE

INSERT INTO CONS_RUN_DTL_TYPE ( CONS_RUN_DTL_TYPE,DESCRIPTION) 
VALUES  ( 'E','Error');

INSERT INTO CONS_RUN_DTL_TYPE ( CONS_RUN_DTL_TYPE,DESCRIPTION) 
VALUES  ( 'I','Information');

INSERT INTO CONS_RUN_DTL_TYPE ( CONS_RUN_DTL_TYPE,DESCRIPTION) 
VALUES  ( 'W','Warning');

Commit;





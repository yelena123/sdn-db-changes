set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ASN_TYPE

INSERT INTO ASN_TYPE ( ASN_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 10,'Ready to Ship',SYSDATE,systimestamp);

INSERT INTO ASN_TYPE ( ASN_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 20,'Shipment',SYSDATE,systimestamp);

INSERT INTO ASN_TYPE ( ASN_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 30,'System Generated',SYSDATE,systimestamp);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for E_OBS_EMAIL_TEMPL

INSERT INTO E_OBS_EMAIL_TEMPL ( WHSE,CONTENT,SPVSR,EMP,JOB_FUNC,EP,THRESHOLD_EP,PERF_TIME_PERIOD,PERF_DATE,CREATE_DATE_TIME,
MODIFIED_DATE_TIME,USER_ID,VERSION_ID) 
VALUES  ( '01','The following employees have performed below threshold:',null,null,'Y','Y','Y','Y','Y',sysdate,
sysdate,'LMADMIN',1);

Commit;





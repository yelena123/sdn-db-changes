set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LRF_SERVER

INSERT INTO LRF_SERVER ( APP_INST_SHORT_NAME,HEART_BEAT,SERVER_STATUS,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
VALUES  ( 'wm','2014-08-27 14:55:11',1,'setup',systimestamp);

Commit;





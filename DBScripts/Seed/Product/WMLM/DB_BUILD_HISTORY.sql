set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DB_BUILD_HISTORY

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CBO_20130607.2013.18.05.01','2014-08-27 14:54:39','2014-08-27 14:54:39','CBO DB Incremental Build - 26 for 2013','CBO_20130607.2013.18.05.01');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CBO_20130704.2013.18.06','2014-08-27 14:57:15','2014-08-27 14:57:15','CBO DB Incremental Build - 26 for 2013','CBO_20130704.2013.18.06');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CBO_20130725.2013.18.07','2014-08-27 14:57:16','2014-08-27 14:57:16','CBO DB Incremental Build - 27 for 2013','CBO_20130725.2013.18.07');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CBO_20130808.2013.18.08','2014-08-27 14:57:17','2014-08-27 14:57:18','CBO DB Incremental Build - 28 for 2013','CBO_20130808.2013.18.08');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CBO_20131215.2014.00.00','2014-08-27 14:58:50','2014-08-27 14:58:50','DD INCREMENTAL BUILD FOR 2014','CBO_20131215.2014.00.00');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CM_20130704.2013.18.06','2014-08-27 14:57:15','2014-08-27 14:57:15','CM DB Incremental Build for 2013','CM_20130704.2013.18.06');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CM_20130725.2013.18.07','2014-08-27 14:57:16','2014-08-27 14:57:16','CM DB Incremental Build for 2013','CM_20130725.2013.18.07');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CM_20130808.2013.18.08','2014-08-27 14:57:18','2014-08-27 14:57:18','CM DB Incremental Build for 2013','CM_20130808.2013.18.08');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CM_PKG_20130704.2013.18.06','2014-08-27 14:57:15','2014-08-27 14:57:15','CMPKG DB Incremental Build for 2013','CM_PKG_20130704.2013.18.06');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CM_PKG_20130725.2013.18.07','2014-08-27 14:57:16','2014-08-27 14:57:16','CMPKG DB Incremental Build for 2013','CM_PKG_20130725.2013.18.07');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'CM_PKG_20130808.2013.18.08','2014-08-27 14:57:18','2014-08-27 14:57:18','CMPKG DB Incremental Build for 2013','CM_PKG_20130808.2013.18.08');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'SCPP_20130607.2013.18.05','2014-08-27 14:54:39','2014-08-27 14:54:39','SCPP DB Incremental Build - 25 for 2013','20130607.2013.18.05');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'SCPP_20130704.2013.18.06','2014-08-27 14:57:15','2014-08-27 14:57:15','SCPP DB Incremental Build - 26 for 2013','20130704.2013.18.06');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'SCPP_20130725.2013.18.07','2014-08-27 14:57:15','2014-08-27 14:57:16','SCPP DB Incremental Build - 27 for 2013','20130725.2013.18.07');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'SCPP_20130808.2013.18.08','2014-08-27 14:57:16','2014-08-27 14:57:17','SCPP DB Incremental Build - 28 for 2013','20130808.2013.18.08');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'TE_2013.20130704.18.06','2014-08-27 14:57:15','2014-08-27 14:57:15',null,'TE.2013.18.06.0709');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'TE_2013.20130704.18.06.01','2014-08-27 14:57:15','2014-08-27 14:57:15',null,'TE.2013.18.06.04.0725');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'TE_2013.20130725.18.07','2014-08-27 14:57:16','2014-08-27 14:57:16',null,'TE.2013.18.07.0729');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'TE_2013.20130725.18.07.01','2014-08-27 14:57:16','2014-08-27 14:57:16',null,'TE.2013.18.07.00.0730');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'TE_2013.20130725.18.07.02','2014-08-27 14:57:18','2014-08-27 14:57:18',null,'TE.2013.18.07.02.0805');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'TE_2013.20130808.18.08','2014-08-27 14:57:18','2014-08-27 14:57:18',null,'TE.2013.18.08.00.0812');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'TE_2013.20130808.18.08.03','2014-08-27 14:57:18','2014-08-27 14:57:18',null,'TE.2013.18.08.00.0906');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'TE_2013.20130808.18.08.10','2014-08-27 14:57:18','2014-08-27 14:57:18',null,'TE.2013.18.08. 10. 1008');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130607.18.05.07','2014-08-27 14:54:39','2014-08-27 14:54:39',null,'WM.2013.18.05.07.0617');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130704.18.06','2014-08-27 14:57:19','2014-08-27 14:57:19',null,'WM.2013.18.06.0711');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130704.18.06.01','2014-08-27 14:57:29','2014-08-27 14:57:29',null,'WM.2013.18.06.0718');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130704.18.06.02','2014-08-27 14:57:30','2014-08-27 14:57:30',null,'WM.2013.18.06.01.0723');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130704.18.06.03','2014-08-27 14:57:31','2014-08-27 14:57:31',null,'WM.2013.18.06.03.0725');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130704.18.06.04','2014-08-27 14:57:31','2014-08-27 14:57:31',null,'WM.2013.18.06.04.0725');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130725.18.07','2014-08-27 14:57:31','2014-08-27 14:57:31',null,'WM.2013.18.07.00.0730');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130725.18.07.01','2014-08-27 14:57:33','2014-08-27 14:57:33',null,'WM.2013.18.07.01.0801');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130725.18.07.02','2014-08-27 14:57:34','2014-08-27 14:57:34',null,'WM.2013.18.07.02.0805');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130725.18.07.03','2014-08-27 14:57:37','2014-08-27 14:57:37',null,'WM.2013.18.07.03.0809');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08','2014-08-27 14:57:37','2014-08-27 14:57:37',null,'WM.2013.18.08.00.0812');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.01','2014-08-27 14:57:39','2014-08-27 14:57:39',null,'WM.2013.18.08.01.0813');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.02','2014-08-27 14:57:39','2014-08-27 14:57:39',null,'WM.2013.18.08.01.0813');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.03','2014-08-27 14:57:39','2014-08-27 14:57:39',null,'WM.2013.18.08.00.0906');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.04','2014-08-27 14:57:53','2014-08-27 14:57:53',null,'WM.2013.18.08.01.0917');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.05','2014-08-27 14:57:55','2014-08-27 14:57:55',null,'WM.2013.18.08.05.0918');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.06','2014-08-27 14:57:55','2014-08-27 14:57:55',null,'WM.2013.18.08.06.0924');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.07','2014-08-27 14:57:57','2014-08-27 14:57:57',null,'WM.2013.18.08.07.0926');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.08','2014-08-27 14:57:57','2014-08-27 14:57:57',null,'WM.2013.18.08.08.1001');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.09','2014-08-27 14:57:58','2014-08-27 14:57:58',null,'WM.2013.18.08.09.1004');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.10','2014-08-27 14:57:58','2014-08-27 14:57:58',null,'WM.2013.18.08.10.1008');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.11','2014-08-27 14:58:00','2014-08-27 14:58:00',null,'WM.2013.18.08.11.1010');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'WM_2013.20130808.18.08.12','2014-08-27 14:58:01','2014-08-27 14:58:01',null,'WM.2013.18.08.12.1014');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'YMS_2013.20130704.18.06','2014-08-27 14:57:15','2014-08-27 14:57:15',null,'YMS.2013.20130709.18.06.0');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'YMS_2013.20130725.18.07.02','2014-08-27 14:57:18','2014-08-27 14:57:18',null,'YMS.2013.18.07.02.0805');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'YMS_2013.20130808.18.08.03','2014-08-27 14:57:18','2014-08-27 14:57:18',null,'YMS.2013.18.08.00.0906');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'YMS_2013.20130808.18.08.04','2014-08-27 14:57:18','2014-08-27 14:57:18',null,'YMS.2013.18.08.01.0917');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'YMS_2013.20130808.18.08.05','2014-08-27 14:57:19','2014-08-27 14:57:19',null,'YMS.2013.18.08.05.0918');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'YMS_2013.20130808.18.08.08','2014-08-27 14:57:19','2014-08-27 14:57:19',null,'YMS.2013.18.08.08.1001');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'YMS_2013.20130808.18.08.09','2014-08-27 14:57:19','2014-08-27 14:57:19',null,'YMS.2013.18.08.09.1004');

INSERT INTO DB_BUILD_HISTORY ( VERSION_LABEL,START_DTTM,END_DTTM,VERSION_RANGE,APP_BLD_NO) 
VALUES  ( 'YMS_2013.20130808.18.08.11','2014-08-27 14:57:19','2014-08-27 14:57:19',null,'YMS.2013.18.08.11.1010');

Commit;





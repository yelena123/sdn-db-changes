set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for OAUTH_CLIENT_DETAILS

INSERT INTO OAUTH_CLIENT_DETAILS ( CLIENT_ID,RESOURCE_IDS,CLIENT_SECRET,SCOPE,AUTHORIZED_GRANT_TYPES,WEB_SERVER_REDIRECT_URI,AUTHORITIES,ACCESS_TOKEN_VALIDITY,REFRESH_TOKEN_VALIDITY,ADDITIONAL_INFORMATION,
AUTOAPPROVE) 
VALUES  ( 'sp-module-ci',null,'B9DCC4D0-388F-4E7E-9758-CD3EFA665E02',null,'password,authorization_code,refresh_token,implicit',null,'ROLE_CLIENT, ROLE_TRUSTED_CLIENT',null,null,null,
null);

Commit;





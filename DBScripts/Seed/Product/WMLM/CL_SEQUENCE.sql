set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_SEQUENCE

INSERT INTO CL_SEQUENCE ( SEQUENCE_NAME,SEQUENCE_LAST,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CL_MESSAGE.MSG_ID',1,SYSDATE,systimestamp);

Commit;





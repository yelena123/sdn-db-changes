set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for REACT_HANDLER

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 3,'Changed','PUR_ORDER','com.manh.appointment.ui.handlers.POChangeHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 4,'Changed','SHIPMENT','com.manh.appointment.ui.handlers.ShipmentChangeHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 1,'Changed','ASN','com.manh.appointment.ui.handlers.ASNChangeHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 2,'Created','FREEZE','com.manh.wmos.services.inventorymgmt.handler.FreezeReactHandler');

Commit;





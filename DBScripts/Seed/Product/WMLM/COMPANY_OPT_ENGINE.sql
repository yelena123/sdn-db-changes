set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for COMPANY_OPT_ENGINE

INSERT INTO COMPANY_OPT_ENGINE ( COMPANY_ID,OPT_ENGINE_ID,OPT_ENGINE_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'TEPE','CONSOLIDATION',SYSTIMESTAMP,systimestamp);

Commit;





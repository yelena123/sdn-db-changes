set echo on;

begin
execute immediate 'ALTER TABLE E_PURGE_DTL DROP CONSTRAINT E_PURGE_DTL_UK';
exception 
when others
then
   NULL;
end;
/

begin
execute immediate 'DROP INDEX E_PURGE_DTL_UK';
exception 
when others 
then
   NULL;
end;
/

begin
execute immediate 'ALTER TABLE E_PURGE_DTL ADD (PURGE_SQL CLOB)';
exception 
when others 
then
   NULL;
end;
/

begin
execute immediate 'ALTER TABLE E_PURGE_DTL  ADD ARCHIVE_FLAG  CHAR(1) DEFAULT ''N'' NOT NULL';
exception 
when others 
then
   NULL;
end;
/


comment on column E_PURGE_DTL.PURGE_SQL is 'Used to store dynamic SQL statements';
comment on column E_PURGE_DTL.ARCHIVE_FLAG is 'Archive Flag Y or N';

begin
execute immediate 'ALTER TABLE PURGE_HIST ADD (MESSAGE  VARCHAR2(250))';
exception 
when others 
then
   NULL;
end;
/
 
COMMENT ON COLUMN PURGE_HIST.MESSAGE IS 'Informational column, typically used to communicate archive/purge job errors.';

call quiet_drop ('TABLE', 'GTMP_ARCH_ID_LIST');
CREATE GLOBAL TEMPORARY TABLE GTMP_ARCH_ID_LIST
(
  COL_NAME  VARCHAR2(30),
  I_ID      NUMBER(10),
  VC_ID     VARCHAR2(255)
)
ON COMMIT PRESERVE ROWS
NOCACHE;

CREATE INDEX GIDX_TAIL_COL_NAME ON GTMP_ARCH_ID_LIST(COL_NAME);
CREATE INDEX GIDX_TAIL_I_ID ON GTMP_ARCH_ID_LIST(I_ID);
CREATE INDEX GIDX_TAIL_VC_ID ON GTMP_ARCH_ID_LIST(VC_ID);

call quiet_drop ('TABLE', 'GTMP_PURGE_ORDERS');
CREATE TABLE GTMP_PURGE_ORDERS
(
  COL_NAME  VARCHAR2(30),
  I_ID      NUMBER(10),
  VC_ID     VARCHAR2(255)
) TABLESPACE SLMDATA;

CREATE INDEX GIDX_TPO_COL_NAME ON GTMP_PURGE_ORDERS (COL_NAME) TABLESPACE SLMINDX;
CREATE INDEX GIDX_TPO_I_ID ON GTMP_PURGE_ORDERS (I_ID) TABLESPACE SLMINDX;
CREATE INDEX GIDX_TPO_VC_ID ON GTMP_PURGE_ORDERS (VC_ID) TABLESPACE SLMINDX;

call quiet_drop ('TABLE', 'TMP_WM_ID_1');
CREATE GLOBAL TEMPORARY TABLE TMP_WM_ID_1
(
  ROW_NBR  INTEGER,
  I_ID     INTEGER,
  V_ID     VARCHAR2(50)
)
ON COMMIT PRESERVE ROWS
NOCACHE;

CREATE INDEX GTMP_IDX_WM_ID1_I_ID ON TMP_WM_ID_1(I_ID);

call quiet_drop ('TABLE', 'TMP_WM_ID_2');
CREATE GLOBAL TEMPORARY TABLE TMP_WM_ID_2
(
  ROW_NBR  INTEGER,
  I_ID     INTEGER,
  V_ID     VARCHAR2(50)
)
ON COMMIT PRESERVE ROWS
NOCACHE;

call quiet_drop ('TABLE', 'TMP_WM_ID_3');
CREATE GLOBAL TEMPORARY TABLE TMP_WM_ID_3
(
  ROW_NBR         INTEGER,
  LPN_ID          NUMBER,
  INVC_BATCH_NBR  NUMBER
)
ON COMMIT PRESERVE ROWS
NOCACHE;

call quiet_drop ('TABLE', 'TMP_WM_ID_4');
CREATE GLOBAL TEMPORARY TABLE TMP_WM_ID_4
(
  I_ID1  INTEGER,
  I_ID2  INTEGER
)
ON COMMIT PRESERVE ROWS
NOCACHE;

call quiet_drop ('TABLE', 'TMP_WM_ARCH_ASN_2');
CREATE GLOBAL TEMPORARY TABLE TMP_WM_ARCH_ASN_2
(
  I_ID  INTEGER,
  V_ID  VARCHAR2(50)
)
ON COMMIT PRESERVE ROWS
NOCACHE;

call quiet_drop ('TABLE', 'TMP_WM_ARCH_LPN_1');
CREATE GLOBAL TEMPORARY TABLE TMP_WM_ARCH_LPN_1
(
  I_ID          INTEGER,
  V_ID          VARCHAR2(50 ),
  IO_INDICATOR  VARCHAR2(1 )
)
ON COMMIT PRESERVE ROWS
NOCACHE;

CREATE INDEX GTMP_IDX_WM_ARCH_LPN_I_ID ON TMP_WM_ARCH_LPN_1(I_ID);

call quiet_drop ('TABLE', 'TMP_WM_ARCH_LPN_2');
CREATE GLOBAL TEMPORARY TABLE TMP_WM_ARCH_LPN_2
(
  I_DTL_ID  INTEGER,
  I_HDR_ID  VARCHAR2(50 )
)
ON COMMIT PRESERVE ROWS
NOCACHE;

call quiet_drop ('TABLE', 'GTMP_ARCH_SHIP_IDS');
CREATE GLOBAL TEMPORARY TABLE GTMP_ARCH_SHIP_IDS
(
  SHIPMENT_ID  NUMBER(10)                       NOT NULL
)
ON COMMIT PRESERVE ROWS
NOCACHE;

CREATE INDEX GIDX_GASI_SHIPMENT_ID ON GTMP_ARCH_SHIP_IDS
(SHIPMENT_ID);

call quiet_drop ('TABLE', 'GTMP_ARCH_ORDER_IDS');
CREATE GLOBAL TEMPORARY TABLE GTMP_ARCH_ORDER_IDS
(
  ORDER_ID     NUMBER(10)                       NOT NULL,
  TC_ORDER_ID  VARCHAR2(50 )                NOT NULL,
  SHIPMENT_ID  NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;

call quiet_drop ('TABLE', 'GTMP_ARCH_LPN_IDS');
CREATE GLOBAL TEMPORARY TABLE GTMP_ARCH_LPN_IDS
(
  LPN_ID       NUMBER(10)                       NOT NULL,
  ORDER_ID     NUMBER(10),
  SHIPMENT_ID  NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


call quiet_drop ('TABLE', 'ARCHIVE_LONG_TABLE_MAPPING');
CREATE TABLE ARCHIVE_LONG_TABLE_MAPPING
(
  ARCHIVE_TABLE_NAME  VARCHAR2(30 )         NOT NULL,
  TABLE_NAME          VARCHAR2(30 )         NOT NULL
) TABLESPACE SLMDATA;

COMMENT ON TABLE ARCHIVE_LONG_TABLE_MAPPING IS 'Archive/Purge Mappings for Abbreviated Archive Table Names.';
COMMENT ON COLUMN ARCHIVE_LONG_TABLE_MAPPING.ARCHIVE_TABLE_NAME IS 'Archive schema table with abbreviated name.';
COMMENT ON COLUMN ARCHIVE_LONG_TABLE_MAPPING.TABLE_NAME IS 'Product schema table name of length 26 or greater requiring abbreviation in the archive schema.';


DECLARE
   v_cnt   NUMBER (1) := 0;
BEGIN
   SELECT COUNT (*)
     INTO v_cnt
     FROM USER_TABLES
    WHERE TABLE_NAME IN ('PURGE_EXCEPTIONS');

   IF (v_cnt = 0)
   THEN
      EXECUTE IMMEDIATE
         'CREATE TABLE PURGE_EXCEPTIONS
        (
          CREATE_DTTM       TIMESTAMP(6),
          PURGE_CODE        VARCHAR2(3 ),
          COMPANY_ID        NUMBER(9),
          WHSE_MASTER_ID    NUMBER(10),
          ERROR_MSG         VARCHAR2(250 ),
          SOURCE_PROCEDURE  VARCHAR2(30 ),
          TAB_COL_NAME      VARCHAR2(61 ),
          ID_VALUE          NUMBER(20)
        ) TABLESPACE SLMDATA';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/
  
COMMENT ON TABLE PURGE_EXCEPTIONS IS 'Archive/Purge IDs That Failed During the Purge Process';
COMMENT ON COLUMN PURGE_EXCEPTIONS.PURGE_CODE IS 'The purge code corresponding to the failed id value.';
COMMENT ON COLUMN PURGE_EXCEPTIONS.COMPANY_ID IS 'The company id used in the archive/purge selection process for the purge code.';
COMMENT ON COLUMN PURGE_EXCEPTIONS.WHSE_MASTER_ID IS 'The warehouse id used in the archive/purge selection process for the purge code.';
COMMENT ON COLUMN PURGE_EXCEPTIONS.ERROR_MSG IS 'The error message returned during the purge process.';
COMMENT ON COLUMN PURGE_EXCEPTIONS.SOURCE_PROCEDURE IS 'The procedure within the archive/purge package that generated the error.';
COMMENT ON COLUMN PURGE_EXCEPTIONS.TAB_COL_NAME IS 'The table and column purge_exceptions.name that failed in the source procedure.';
COMMENT ON COLUMN PURGE_EXCEPTIONS.ID_VALUE IS 'The value of the id that failed during the purge process.';


begin
execute immediate 'CREATE INDEX E_AUD_LOG_IDX1_ARCH ON E_AUD_LOG (WHSE, CREATE_DATE_TIME) TABLESPACE LM_IDX_TBS';
exception 
when others 
then
   NULL;
end;
/
 
begin
execute immediate 'CREATE INDEX E_EMP_REFLECT_STD_IDX1_ARCH ON E_EMP_REFLECT_STD(REFLECT_STD_EMP_END_DATE_TIME) TABLESPACE LM_IDX_TBS';
exception 
when others 
then
   NULL;
end;
/

begin
execute immediate 'CREATE INDEX E_WMS_EVNT_MSG_IDX1_ARCH ON E_WMS_EVNT_MSG(WHSE, MOD_DATE_TIME) TABLESPACE LM_IDX_TBS';
exception 
when others 
then
   NULL;
end;
/

begin
execute immediate 'CREATE INDEX LABOR_MSG_IDX1_ARCH ON LABOR_MSG(LAST_UPDATED_DTTM) TABLESPACE LM_IDX_TBS';
exception 
when others 
then
   NULL;
end;
/

begin
execute immediate 'CREATE INDEX IDX_VOCOLLECT_ACTIVE_CARTON ON VOCOLLECT_ACTIVE_CARTON(ACTIVE_CARTON) TABLESPACE LLMINDX';
exception 
when others 
then
   NULL;
end;
/


BEGIN
EXECUTE IMMEDIATE
'alter table PURGE_CONFIG add MISC_PARM_1 VARCHAR2(50) ';
EXCEPTION
WHEN OTHERS
THEN
NULL;
END;
/

BEGIN
EXECUTE IMMEDIATE
'alter table PURGE_CONFIG add MISC_PARM_2 VARCHAR2(50)';
EXCEPTION
WHEN OTHERS
THEN
NULL;
END;
/

BEGIN
EXECUTE IMMEDIATE
'alter table PURGE_CONFIG add MISC_PARM_3 VARCHAR2(50)';
EXCEPTION
WHEN OTHERS
THEN
NULL;
END;
/ 


COMMENT ON COLUMN PURGE_CONFIG.MISC_PARM_1 IS 'Miscellaneous parameter column for specifying additional purge criteria on pre-defied purge types.';
COMMENT ON COLUMN PURGE_CONFIG.MISC_PARM_2 IS 'Miscellaneous parameter column for specifying additional purge criteria on pre-defied purge types.';
COMMENT ON COLUMN PURGE_CONFIG.MISC_PARM_3 IS 'Miscellaneous parameter column for specifying additional purge criteria on pre-defied purge types.';

begin
execute immediate 'CREATE INDEX ALOCINVNDTL_CRTNNBR_IDX ON ALLOC_INVN_DTL(CARTON_NBR) TABLESPACE LLMINDX';
exception 
when others 
then
   NULL;
end;
/

begin
execute immediate 'CREATE INDEX E_EVNT_SMRY_HDR_IDX1_ARCH ON E_EVNT_SMRY_HDR(WHSE, MOD_DATE_TIME) TABLESPACE LM_IDX_TBS';
exception 
when others 
then
   NULL;
end;
/


commit;

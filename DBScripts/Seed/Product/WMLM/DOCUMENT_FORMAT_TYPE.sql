set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DOCUMENT_FORMAT_TYPE

INSERT INTO DOCUMENT_FORMAT_TYPE ( DOCUMENT_FORMAT_TYPE,DESCRIPTION) 
VALUES  ( 4,'EDI');

INSERT INTO DOCUMENT_FORMAT_TYPE ( DOCUMENT_FORMAT_TYPE,DESCRIPTION) 
VALUES  ( 8,'PDF');

Commit;





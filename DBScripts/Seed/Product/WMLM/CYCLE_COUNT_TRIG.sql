set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CYCLE_COUNT_TRIG

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1001,'01',5668,'Cycle Count Case Reserve','Y','N',systimestamp,systimestamp,'SEED','0',
22,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2000,'01',5668,'Cycle Count Active','Y','N',systimestamp,systimestamp,'SEED','0',
23,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2010,'01',5668,'Cycle Count Active - Pick to Threshold','Y','Y',systimestamp,systimestamp,'SEED','0',
24,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 3000,'01',5668,'Cycle Count Case Pick','Y','N',systimestamp,systimestamp,'SEED','0',
25,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 3010,'01',5668,'Cycle Count Case Pick- Pick to Threshold','Y','Y',systimestamp,systimestamp,'SEED','0',
26,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5011,'01',5668,'Alternate for Reserve (Pull)','N','N',systimestamp,systimestamp,'SEED','0',
27,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5012,'01',5668,'Alternate for Case-Pick (Pull)','N','N',systimestamp,systimestamp,'SEED','0',
28,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5013,'01',5668,'Alternate for Active (Pull)','N','N',systimestamp,systimestamp,'SEED','0',
29,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5021,'01',5668,'Substitute for Reserve (Pull)','N','N',systimestamp,systimestamp,'SEED','0',
30,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5022,'01',5668,'Substitute for Case-Pick (Pull)','N','N',systimestamp,systimestamp,'SEED','0',
31,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5023,'01',5668,'Substitute for Active (Pull)','N','N',systimestamp,systimestamp,'SEED','0',
32,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5031,'01',5668,'Short Pick in Reserve (Pull)','N','N',systimestamp,systimestamp,'SEED','0',
33,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5032,'01',5668,'Short Pick in Case-Pick (Pull)','Y','Y',systimestamp,systimestamp,'SEED','0',
34,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5033,'01',5668,'Short Pick in Active (Pull)','Y','Y',systimestamp,systimestamp,'SEED','0',
35,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 6011,'01',5668,'Alternate for Reserve (Put)','Y','Y',systimestamp,systimestamp,'SEED','0',
36,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 6012,'01',5668,'Alternate for Case-Pick (Put)','Y','Y',systimestamp,systimestamp,'SEED','0',
37,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 6013,'01',5668,'Alternate for Active (Put)','Y','Y',systimestamp,systimestamp,'SEED','0',
38,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 6041,'01',5668,'Skip for Reserve (Put)','N','N',systimestamp,systimestamp,'SEED','0',
39,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 6042,'01',5668,'Skip for Case-Pick (Put)','Y','Y',systimestamp,systimestamp,'SEED','0',
40,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 6043,'01',5668,'Skip for Active (Put)','N','N',systimestamp,systimestamp,'SEED','0',
41,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 6051,'01',5668,'Substitute for Reserve (Put)','Y','Y',systimestamp,systimestamp,'SEED','0',
42,1,SYSTIMESTAMP,systimestamp);

INSERT INTO CYCLE_COUNT_TRIG ( TRIG_CODE,WHSE,TASK_PARM_ID,TRIG_DESC,CREATE_TASK,PROMPT_USER,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,PIX_RSN,
CYCLE_COUNT_TRIG_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 7001,'01',5668,'Move Serial Number','Y','Y',systimestamp,systimestamp,'SEED','0',
43,1,SYSTIMESTAMP,systimestamp);

Commit;





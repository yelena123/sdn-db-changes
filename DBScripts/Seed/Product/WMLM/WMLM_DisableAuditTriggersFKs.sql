spool WMLM_DisableAuditTriggersFKs.log
set echo on

exec quiet_drop('TRIGGER', 'ASN_A_U_TR_1');
exec quiet_drop('TRIGGER', 'ASN_DETAIL_A_D');
exec quiet_drop('TRIGGER', 'ASN_DETAIL_A_I');
exec quiet_drop('TRIGGER', 'ASN_DETAIL_A_U');
exec quiet_drop('TRIGGER', 'ASN_NOTE_A_I_TR_1');
exec quiet_drop('TRIGGER', 'ASN_NOTE_A_U_TR_1');
exec quiet_drop('TRIGGER', 'ASN_SEAL_A_IU');
exec quiet_drop('TRIGGER', 'LPN_A_U_TR_1');
exec quiet_drop('TRIGGER', 'LPN_DETAIL_A_U');
exec quiet_drop('TRIGGER', 'ORDERMVMNT_A_D_TR1');
exec quiet_drop('TRIGGER', 'ORDERMVMNT_A_I_TR1');
exec quiet_drop('TRIGGER', 'ORDERS_AD_TR_1');
exec quiet_drop('TRIGGER', 'ORDERS_AI_TR_1');
exec quiet_drop('TRIGGER', 'ORDERS_AU_2');
exec quiet_drop('TRIGGER', 'ORDER_LI_A_ID_TR_1');
exec quiet_drop('TRIGGER', 'ORDER_LI_A_U_TR_1');
exec quiet_drop('TRIGGER', 'PO_AU_1');
exec quiet_drop('TRIGGER', 'PO_LINE_ITEM_AI_1');
exec quiet_drop('TRIGGER', 'PO_LINE_ITEM_AU_1');
exec quiet_drop('TRIGGER', 'RTS_A_U_TR_1');
exec quiet_drop('TRIGGER', 'RTS_LI_SIZE_A_I_TR');
exec quiet_drop('TRIGGER', 'RTS_LI_SIZE_A_U_TR');
exec quiet_drop('TRIGGER', 'RTS_LN_ITM_AI_TR_1');
exec quiet_drop('TRIGGER', 'RTS_LN_ITM_AU_TR_1');
exec quiet_drop('TRIGGER', 'SHIPMENT_A_U_TR_1');
--exec quiet_drop('TRIGGER', 'ILM_APPT_TRG'); -- Commented as part of DB-4411
--exec quiet_drop('TRIGGER', 'ILM_TASK_TRG'); -- Commented as part of DB-4410
exec quiet_drop('TRIGGER', 'ILM_TRAILR_AI_TRG2');
exec quiet_drop('TRIGGER', 'ILM_TRAILR_AU_TRG2');
exec quiet_drop('TRIGGER', 'ACCESSORIAL_RATE_A_IU_TR_1');
exec quiet_drop('TRIGGER', 'CM_RT_DSC_A_U_TR_1');
exec quiet_drop('TRIGGER', 'CURRENCY_CONVERSION_A_U_TR_1');
exec quiet_drop('TRIGGER', 'ILM_TRAILER_TRG3');
exec quiet_drop('TRIGGER', 'LANE_ACCESSORIAL_A_IU_TR_1');

ALTER TABLE LPN_EVENT DISABLE CONSTRAINT LPN_EVENT_LPN_FK;
ALTER TABLE LPN_EVENT DISABLE CONSTRAINT LPN_EVENT_LPN_FK2;
ALTER TABLE ORDER_EVENT DISABLE CONSTRAINT TO_ORDER_EVT_FK;
ALTER TABLE PURCHASE_ORDERS_EVENT DISABLE CONSTRAINT PURCHASE_EVENT_FK1;
ALTER TABLE SHIPMENT_EVENT DISABLE CONSTRAINT SHIPMENT_EVENT_FK;

--ALTER TABLE ILM_APPOINTMENT_EVENTS DISABLE CONSTRAINT aevnts_appt_fk;  -- Commented as part of DB-4411
--ALTER TABLE ILM_APPOINTMENT_EVENTS DISABLE CONSTRAINT aevnts_srctyp_fk;   
ALTER TABLE ilm_dock_door_events DISABLE CONSTRAINT adckdrevnt_srctyp_fk;
--ALTER TABLE ILM_TASK_EVENTS DISABLE CONSTRAINT tskevnts_tsk_fk; -- Commented as part of DB-4410
--ALTER TABLE ILM_TASK_EVENTS DISABLE CONSTRAINT tskevnts_scrtyp_fk;
ALTER TABLE ILM_TRAILER_EVENTS DISABLE CONSTRAINT trlrevnt_srctype_fk;

spool off
exit;


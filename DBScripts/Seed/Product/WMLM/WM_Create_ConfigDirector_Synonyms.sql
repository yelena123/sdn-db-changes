-- This script creates synonyms pointing to MSF schema tables which
-- also exist in the WM product schema as a requirement of the 
-- Config Directory tool.
--
-- &1 = MSF Schema Name
set echo on define on
spool WM_ORA_Create_ConfigDirector_Synonyms.log

create or replace synonym MSF_BINDING_DETAILS for &1..BINDING_DETAILS;
create or replace synonym MSF_BINDING_TYPE for &1..BINDING_TYPE;
create or replace synonym MSF_CA_DATA_TYPE for &1..CA_DATA_TYPE;
create or replace synonym MSF_CA_ENTITY_APPL for &1..CA_ENTITY_APPL;
create or replace synonym MSF_CA_ENTITY_DEF for &1..CA_ENTITY_DEF;
create or replace synonym MSF_CA_ENTITY_DEF_KEY for &1..CA_ENTITY_DEF_KEY;
create or replace synonym MSF_CA_PROP_APPLICABLE for &1..CA_PROP_APPLICABLE;
create or replace synonym MSF_CA_PROPERTY_DEF for &1..CA_PROPERTY_DEF;
create or replace synonym MSF_CONTACT_FILTER_MAP for &1..CONTACT_FILTER_MAP;
create or replace synonym MSF_CUSTOM_BINDING_DETAILS for &1..CUSTOM_BINDING_DETAILS;
create or replace synonym MSF_DATA_TYPE for &1..DATA_TYPE;
create or replace synonym MSF_EVENT_HANDLER for &1..EVENT_HANDLER;
create or replace synonym MSF_FILTER for &1..FILTER;
create or replace synonym MSF_FILTER_DETAIL for &1..FILTER_DETAIL;
create or replace synonym MSF_FILTER_GROUP_DEFINITION for &1..FILTER_GROUP_DEFINITION;
create or replace synonym MSF_FILTER_GROUPBY_DETAIL for &1..FILTER_GROUPBY_DETAIL;
create or replace synonym MSF_FILTER_ORDERBY_DETAIL for &1..FILTER_ORDERBY_DETAIL;
create or replace synonym MSF_HANDLER for &1..HANDLER;
create or replace synonym MSF_LEMA_RULE_CONF_RES_ATTR for &1..LEMA_RULE_CONF_RES_ATTR;
create or replace synonym MSF_LEMA_RULE_CONF_RES_ATTR_V for &1..LEMA_RULE_CONF_RES_ATTR_VAL;
create or replace synonym MSF_LEMA_RULE_TEMPLATE for &1..LEMA_RULE_TEMPLATE;
create or replace synonym MSF_LEMA_WORKFLOW for &1..LEMA_WORKFLOW;
create or replace synonym MSF_LRF_PRT_QUEUE for &1..LRF_PRT_QUEUE;
create or replace synonym MSF_LRF_PRT_QUEUE_DEST for &1..LRF_PRT_QUEUE_DEST;
create or replace synonym MSF_LRF_PRT_QUEUE_SERVICE for &1..LRF_PRT_QUEUE_SERVICE;
create or replace synonym MSF_LRF_PRT_REQUESTOR for &1..LRF_PRT_REQUESTOR;
create or replace synonym MSF_LRF_REPORT for &1..LRF_REPORT;
create or replace synonym MSF_LRF_REPORT_PARAM_VALUE for &1..LRF_REPORT_PARAM_VALUE;
create or replace synonym MSF_LRF_REPORT_PARAM_VALUE_DTL for &1..LRF_REPORT_PARAM_VALUE_DTL;
create or replace synonym MSF_LRF_SCHED_EVENT_REPORT for &1..LRF_SCHED_EVENT_REPORT;
create or replace synonym MSF_NAVIGATION for &1..NAVIGATION;
create or replace synonym MSF_NAVIGATION_APP_MOD_PERM for &1..NAVIGATION_APP_MOD_PERM;
create or replace synonym MSF_NAVIGATION_PARAMETER for &1..NAVIGATION_PARAMETER;
create or replace synonym MSF_SCRIPTS for &1..SCRIPTS;
create or replace synonym MSF_USER_APP_INSTANCE for &1..USER_APP_INSTANCE;
create or replace synonym MSF_USER_NAVIGATION for &1..USER_NAVIGATION;
create or replace synonym MSF_WSDL_BINDINGS for &1..WSDL_BINDINGS;
create or replace synonym MSF_WSDL_IMPORTS for &1..WSDL_IMPORTS;
create or replace synonym MSF_WSDL_MESSAGES for &1..WSDL_MESSAGES;
create or replace synonym MSF_WSDL_MSG_PARTS for &1..WSDL_MSG_PARTS;
create or replace synonym MSF_WSDL_PARTNER_LINK_TYPE for &1..WSDL_PARTNER_LINK_TYPE;
create or replace synonym MSF_WSDL_PARTNERLINK_TYPE_ROLE for &1..WSDL_PARTNERLINK_TYPE_ROLE;
create or replace synonym MSF_WSDL_PORT_TYPES for &1..WSDL_PORT_TYPES;
create or replace synonym MSF_WSDL_PT_OPERATIONS for &1..WSDL_PT_OPERATIONS;
create or replace synonym MSF_WSDL_PT_OPERATIONS_ARGS for &1..WSDL_PT_OPERATIONS_ARGS;
create or replace synonym MSF_WSDL_TYPES for &1..WSDL_TYPES;
create or replace synonym MSF_WSDL_TYPES_SCHEMA_IMPTS for &1..WSDL_TYPES_SCHEMA_IMPTS;

-- create dashboard synonyms
-- tables
create or replace synonym MSF_DASHBOARD for &1..DASHBOARD;
create or replace synonym MSF_DSHBRD_COGNOS_PTLT_DTL for &1..DSHBRD_COGNOS_PTLT_DTL;
create or replace synonym MSF_DSHBRD_COMP_PTLT_DPNDCY for &1..DSHBRD_COMP_PTLT_DPNDCY;
create or replace synonym MSF_DSHBRD_COMP_PTLT_DTL for &1..DSHBRD_COMP_PTLT_DTL;
create or replace synonym MSF_DSHBRD_COMP_PTLT for &1..DSHBRD_COMP_PTLT;
create or replace synonym MSF_DSHBRD_COMP_PTLT_PARAM_DEF for &1..DSHBRD_COMP_PTLT_PARAM_DEF;
create or replace synonym MSF_DSHBRD_LAYOUT_DETAILS for &1..DSHBRD_LAYOUT_DETAILS;
create or replace synonym MSF_DSHBRD_LAYOUT for &1..DSHBRD_LAYOUT;
create or replace synonym MSF_DSHBRD_OWNER_TYPE for &1..DSHBRD_OWNER_TYPE;
create or replace synonym MSF_DSHBRD_PORTLET_PARAM_DEF for &1..DSHBRD_PORTLET_PARAM_DEF;
create or replace synonym MSF_DSHBRD_PORTLET_PARAMS for &1..DSHBRD_PORTLET_PARAMS;
create or replace synonym MSF_DSHBRD_PORTLET_PERMISSION for &1..DSHBRD_PORTLET_PERMISSION;
create or replace synonym MSF_DSHBRD_PORTLET_TAGS for &1..DSHBRD_PORTLET_TAGS;
create or replace synonym MSF_DSHBRD_PORTLET_TYPE for &1..DSHBRD_PORTLET_TYPE;
create or replace synonym MSF_DSHBRD_PTLT_HEIGHT_TYPE for &1..DSHBRD_PTLT_HEIGHT_TYPE;
create or replace synonym MSF_DSHBRD_PTLT_WIDTH_TYPE for &1..DSHBRD_PTLT_WIDTH_TYPE;
create or replace synonym MSF_DSHBRD_UIB_PTLT_DTL for &1..DSHBRD_UIB_PTLT_DTL;
create or replace synonym MSF_DSHBRD_USER_DATA for &1..DSHBRD_USER_DATA;
create or replace synonym MSF_DSHBRD_USER_TAB for &1..DSHBRD_USER_TAB;
create or replace synonym MSF_DSHBRD_USER_DATA_STATUS for &1..DSHBRD_USER_DATA_STATUS;
create or replace synonym MSF_DSHBRD_USER_DATA_TYPE for &1..DSHBRD_USER_DATA_TYPE;
-- create or replace synonym MSF_UIB_DB_TYPE for &1..UIB_DB_TYPE;
create or replace synonym MSF_UIB_QUERY for &1..UIB_QUERY;
create or replace synonym MSF_UIB_QUERY_CACHE_TIMEOUT for &1..UIB_QUERY_CACHE_TIMEOUT;
create or replace synonym MSF_UIB_QUERY_DETAILS for &1..UIB_QUERY_DETAILS;
create or replace synonym MSF_UIB_QUERY_TYPE for &1..UIB_QUERY_TYPE;
create or replace synonym MSF_UIB_SMRY_VIEW_CELL for &1..UIB_SMRY_VIEW_CELL;
create or replace synonym MSF_UIB_SMRY_VIEW_CELL_RTG for &1..UIB_SMRY_VIEW_CELL_RTG;
create or replace synonym MSF_UIB_SMRY_VIEW_CELL_RTG_XR for &1..UIB_SMRY_VIEW_CELL_RTG_XREF;
create or replace synonym MSF_UIB_SMRY_VIEW_DTL for &1..UIB_SMRY_VIEW_DTL;
create or replace synonym MSF_UIB_SMRY_VIEW_ROW_CELL_XR for &1..UIB_SMRY_VIEW_ROW_CELL_XREF;
create or replace synonym MSF_UIB_SMRY_VIEW_ROW for &1..UIB_SMRY_VIEW_ROW;
create or replace synonym MSF_UIB_SMRY_VIEW_ROW_XREF for &1..UIB_SMRY_VIEW_ROW_XREF;
create or replace synonym MSF_UIB_SP_DETAILS for &1..UIB_SP_DETAILS;
create or replace synonym MSF_UIB_SQL_FLAVOR for &1..UIB_SQL_FLAVOR;
create or replace synonym MSF_UIB_VIEW_PROPERTY_DEF for &1..UIB_VIEW_PROPERTY_DEF;
create or replace synonym MSF_UIB_VIEW_PROPERTY_SETS for &1..UIB_VIEW_PROPERTY_SETS;
create or replace synonym MSF_UIB_VIEW_PROP_SET_DTL for &1..UIB_VIEW_PROP_SET_DTL;
create or replace synonym MSF_UIB_VIEW_TYPE for &1..UIB_VIEW_TYPE;

-- sequences
create or replace synonym MSF_SEQ_DASHBOARD_ID for &1..SEQ_DASHBOARD_ID;
create or replace synonym MSF_SEQ_DSHBRD_COGNOS_PTLT_DTL for &1..SEQ_DSHBRD_COGNOS_PTLT_DTL_ID;
create or replace synonym MSF_SEQ_DSHBRD_LAYOUT_DTL_ID for &1..SEQ_DSHBRD_LAYOUT_DTL_ID;
create or replace synonym MSF_SEQ_DSHBRD_LAYOUT_ID for &1..SEQ_DSHBRD_LAYOUT_ID;
create or replace synonym MSF_SEQ_DSHBRD_PORTLET_ID for &1..SEQ_DSHBRD_PORTLET_ID;
create or replace synonym MSF_SEQ_DSHBRD_PORTLET_PARAMS for &1..SEQ_DSHBRD_PORTLET_PARAMS_ID;
create or replace synonym MSF_SEQ_DSHBRD_PORTLET_PERM_ID for &1..SEQ_DSHBRD_PORTLET_PERM_ID;
create or replace synonym MSF_SEQ_DSHBRD_PORTLET_TAGS_ID for &1..SEQ_DSHBRD_PORTLET_TAGS_ID;
create or replace synonym MSF_SEQ_DSHBRD_PTLT_PARAM_DEF for &1..SEQ_DSHBRD_PTLT_PARAM_DEF_ID;
create or replace synonym MSF_SEQ_DSHBRD_UIB_PTLT_DTL_ID for &1..SEQ_DSHBRD_UIB_PTLT_DTL_ID;
create or replace synonym MSF_SEQ_DSHBRD_USER_DATA_ID for &1..SEQ_DSHBRD_USER_DATA_ID;
create or replace synonym MSF_SEQ_DSHBRD_USER_TAB_ID for &1..SEQ_DSHBRD_USER_TAB_ID;
create or replace synonym MSF_SEQ_UIB_QUERY_DETAILS_ID for &1..SEQ_UIB_QUERY_DETAILS_ID;
create or replace synonym MSF_SEQ_UIB_QUERY_ID for &1..SEQ_UIB_QUERY_ID;
create or replace synonym MSF_SEQ_UIB_SMRY_VIEW_CELL_ID for &1..SEQ_UIB_SMRY_VIEW_CELL_ID;
create or replace synonym MSF_SEQ_UIB_SMRY_VIEW_CELL_RTG for &1..SEQ_UIB_SMRY_VIEW_CELL_RTG_ID;
create or replace synonym MSF_SEQ_UIB_SMRY_VIEW_DTL_ID for &1..SEQ_UIB_SMRY_VIEW_DTL_ID;
create or replace synonym MSF_SEQ_UIB_SMRY_VIEW_ROW_ID for &1..SEQ_UIB_SMRY_VIEW_ROW_ID;
create or replace synonym MSF_SEQ_UIB_SMRY_VIEW_ROW_XREF for &1..SEQ_UIB_SMRY_VIEW_ROW_XREF_ID;
create or replace synonym MSF_SEQ_UIB_SMRY_VW_CELLRTG_XR for &1..SEQ_UIB_SMRY_VW_CELLRTG_XRF_ID;
create or replace synonym MSF_SEQ_UIB_SMRY_VW_ROWCELL_XR for &1..SEQ_UIB_SMRY_VW_ROWCELL_XRF_ID;
create or replace synonym MSF_SEQ_UIB_SP_DETAILS_ID for &1..SEQ_UIB_SP_DETAILS_ID;
create or replace synonym MSF_SEQ_UIB_VIEW_PROP_SET_DTL for &1..SEQ_UIB_VIEW_PROP_SET_DTL_ID;
create or replace synonym MSF_SEQ_UIB_VIEW_PROP_SET_ID for &1..SEQ_UIB_VIEW_PROP_SET_ID;

spool off
quit

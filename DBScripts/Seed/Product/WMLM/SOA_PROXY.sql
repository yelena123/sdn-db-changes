set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SOA_PROXY

INSERT INTO SOA_PROXY ( ID,PROXYTYPE,NAME,OPTIONS,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 0,0,'<None>',0,'No proxy. This row exists to satisfy referential integrity constraints when no proxy is desired.',SYSTIMESTAMP,systimestamp);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for APPT_UPLOAD_STATUS

INSERT INTO APPT_UPLOAD_STATUS ( APPT_UPLOAD_STATUS,DESCRIPTION) 
VALUES  ( 1,'Waiting To Be Processed');

INSERT INTO APPT_UPLOAD_STATUS ( APPT_UPLOAD_STATUS,DESCRIPTION) 
VALUES  ( 2,'In Progress');

INSERT INTO APPT_UPLOAD_STATUS ( APPT_UPLOAD_STATUS,DESCRIPTION) 
VALUES  ( 3,'Progress Successful');

INSERT INTO APPT_UPLOAD_STATUS ( APPT_UPLOAD_STATUS,DESCRIPTION) 
VALUES  ( 4,'Processed With Error');

Commit;





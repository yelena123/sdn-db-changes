set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for OPT_PARAM_LIST

INSERT INTO OPT_PARAM_LIST ( TC_COMPANY_ID,OPT_PARAM_LIST_ID,DESCRIPTION,OPT_ENGINE_ID,IS_DEFAULT,INIT_PARAM_LIST_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,1,'Routing Parameters','TEPE',0,null,SYSTIMESTAMP,systimestamp);

Commit;





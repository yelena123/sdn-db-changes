-- Grant privs on MSF schema tables to the WM product schema as a
-- requirement of the Config Director tool.
--
-- &1 = WM Schema Name
set echo on define on
spool WM_ORA_Grants_for_ConfigDirector_Synonyms.log

grant insert,update,delete,select on BINDING_DETAILS to &1;
grant insert,update,delete,select on BINDING_TYPE to &1;
grant insert,update,delete,select on CA_DATA_TYPE to &1;
grant insert,update,delete,select on CA_ENTITY_APPL to &1;
grant insert,update,delete,select on CA_ENTITY_DEF to &1;
grant insert,update,delete,select on CA_ENTITY_DEF_KEY to &1;
grant insert,update,delete,select on CA_PROP_APPLICABLE to &1;
grant insert,update,delete,select on CA_PROPERTY_DEF to &1;
grant insert,update,delete,select on CONTACT_FILTER_MAP to &1;
grant insert,update,delete,select on CUSTOM_BINDING_DETAILS to &1;
grant insert,update,delete,select on DATA_TYPE to &1;
grant insert,update,delete,select on EVENT_HANDLER to &1;
grant insert,update,delete,select on FILTER to &1;
grant insert,update,delete,select on FILTER_DETAIL to &1;
grant insert,update,delete,select on FILTER_GROUP_DEFINITION to &1;
grant insert,update,delete,select on FILTER_GROUPBY_DETAIL to &1;
grant insert,update,delete,select on FILTER_ORDERBY_DETAIL to &1;
grant insert,update,delete,select on HANDLER to &1;
grant insert,update,delete,select on LEMA_RULE_CONF_RES_ATTR to &1;
grant insert,update,delete,select on LEMA_RULE_CONF_RES_ATTR_VAL to &1;
grant insert,update,delete,select on LEMA_RULE_TEMPLATE to &1;
grant insert,update,delete,select on LEMA_WORKFLOW to &1;
grant insert,update,delete,select on LRF_PRT_QUEUE to &1;
grant insert,update,delete,select on LRF_PRT_QUEUE_DEST to &1;
grant insert,update,delete,select on LRF_PRT_QUEUE_SERVICE to &1;
grant insert,update,delete,select on LRF_PRT_REQUESTOR to &1;
grant insert,update,delete,select on LRF_REPORT to &1;
grant insert,update,delete,select on LRF_REPORT_PARAM_VALUE to &1;
grant insert,update,delete,select on LRF_REPORT_PARAM_VALUE_DTL to &1;
grant insert,update,delete,select on LRF_SCHED_EVENT_REPORT to &1;
grant insert,update,delete,select on NAVIGATION to &1;
grant insert,update,delete,select on NAVIGATION_APP_MOD_PERM to &1;
grant insert,update,delete,select on NAVIGATION_PARAMETER to &1;
grant insert,update,delete,select on SCRIPTS to &1;
grant insert,update,delete,select on USER_APP_INSTANCE to &1;
grant insert,update,delete,select on USER_NAVIGATION to &1;
grant insert,update,delete,select on WSDL_BINDINGS to &1;
grant insert,update,delete,select on WSDL_IMPORTS to &1;
grant insert,update,delete,select on WSDL_MESSAGES to &1;
grant insert,update,delete,select on WSDL_MSG_PARTS to &1;
grant insert,update,delete,select on WSDL_PARTNER_LINK_TYPE to &1;
grant insert,update,delete,select on WSDL_PARTNERLINK_TYPE_ROLE to &1;
grant insert,update,delete,select on WSDL_PORT_TYPES to &1;
grant insert,update,delete,select on WSDL_PT_OPERATIONS to &1;
grant insert,update,delete,select on WSDL_PT_OPERATIONS_ARGS to &1;
grant insert,update,delete,select on WSDL_TYPES to &1;
grant insert,update,delete,select on WSDL_TYPES_SCHEMA_IMPTS to &1;

-- grants on dashboard tables
grant insert,update,delete,select on DASHBOARD to &1;
grant insert,update,delete,select on DSHBRD_COGNOS_PTLT_DTL to &1;
grant insert,update,delete,select on DSHBRD_COMP_PTLT_DPNDCY to &1;
grant insert,update,delete,select on DSHBRD_COMP_PTLT_DTL to &1;
grant insert,update,delete,select on DSHBRD_COMP_PTLT to &1;
grant insert,update,delete,select on DSHBRD_COMP_PTLT_PARAM_DEF to &1;
grant insert,update,delete,select on DSHBRD_LAYOUT_DETAILS to &1;
grant insert,update,delete,select on DSHBRD_LAYOUT to &1;
grant insert,update,delete,select on DSHBRD_OWNER_TYPE to &1;
grant insert,update,delete,select on DSHBRD_PORTLET_PARAM_DEF to &1;
grant insert,update,delete,select on DSHBRD_PORTLET_PARAMS to &1;
grant insert,update,delete,select on DSHBRD_PORTLET_PERMISSION to &1;
grant insert,update,delete,select on DSHBRD_PORTLET_TAGS to &1;
grant insert,update,delete,select on DSHBRD_PORTLET_TYPE to &1;
grant insert,update,delete,select on DSHBRD_PTLT_HEIGHT_TYPE to &1;
grant insert,update,delete,select on DSHBRD_PTLT_WIDTH_TYPE to &1;
grant insert,update,delete,select on DSHBRD_UIB_PTLT_DTL to &1;
grant insert,update,delete,select on DSHBRD_USER_DATA to &1;
grant insert,update,delete,select on DSHBRD_USER_TAB to &1;
grant insert,update,delete,select on DSHBRD_USER_DATA_STATUS to &1;
grant insert,update,delete,select on DSHBRD_USER_DATA_TYPE to &1;
-- grant insert,update,delete,select on UIB_DB_TYPE to &1;
grant insert,update,delete,select on UIB_QUERY to &1;
grant insert,update,delete,select on UIB_QUERY_CACHE_TIMEOUT to &1;
grant insert,update,delete,select on UIB_QUERY_DETAILS to &1;
grant insert,update,delete,select on UIB_QUERY_TYPE to &1;
grant insert,update,delete,select on UIB_SMRY_VIEW_CELL to &1;
grant insert,update,delete,select on UIB_SMRY_VIEW_CELL_RTG to &1;
grant insert,update,delete,select on UIB_SMRY_VIEW_CELL_RTG_XREF to &1;
grant insert,update,delete,select on UIB_SMRY_VIEW_DTL to &1;
grant insert,update,delete,select on UIB_SMRY_VIEW_ROW_CELL_XREF to &1;
grant insert,update,delete,select on UIB_SMRY_VIEW_ROW to &1;
grant insert,update,delete,select on UIB_SMRY_VIEW_ROW_XREF to &1;
grant insert,update,delete,select on UIB_SP_DETAILS to &1;
grant insert,update,delete,select on UIB_SQL_FLAVOR to &1;
grant insert,update,delete,select on UIB_VIEW_PROPERTY_DEF to &1;
grant insert,update,delete,select on UIB_VIEW_PROPERTY_SETS to &1;
grant insert,update,delete,select on UIB_VIEW_PROP_SET_DTL to &1;
grant insert,update,delete,select on UIB_VIEW_TYPE to &1;

-- grants on dashboard sequences
grant all privileges on SEQ_DASHBOARD_ID to &1;
grant all privileges on SEQ_DSHBRD_COGNOS_PTLT_DTL_ID to &1;
grant all privileges on SEQ_DSHBRD_LAYOUT_DTL_ID to &1;
grant all privileges on SEQ_DSHBRD_LAYOUT_ID to &1;
grant all privileges on SEQ_DSHBRD_PORTLET_ID to &1;
grant all privileges on SEQ_DSHBRD_PORTLET_PARAMS_ID to &1;
grant all privileges on SEQ_DSHBRD_PORTLET_PERM_ID to &1;
grant all privileges on SEQ_DSHBRD_PORTLET_TAGS_ID to &1;
grant all privileges on SEQ_DSHBRD_PTLT_PARAM_DEF_ID to &1;
grant all privileges on SEQ_DSHBRD_UIB_PTLT_DTL_ID to &1;
grant all privileges on SEQ_DSHBRD_USER_DATA_ID to &1;
grant all privileges on SEQ_DSHBRD_USER_TAB_ID to &1;
grant all privileges on SEQ_UIB_QUERY_DETAILS_ID to &1;
grant all privileges on SEQ_UIB_QUERY_ID to &1;
grant all privileges on SEQ_UIB_SMRY_VIEW_CELL_ID to &1;
grant all privileges on SEQ_UIB_SMRY_VIEW_CELL_RTG_ID to &1;
grant all privileges on SEQ_UIB_SMRY_VIEW_DTL_ID to &1;
grant all privileges on SEQ_UIB_SMRY_VIEW_ROW_ID to &1;
grant all privileges on SEQ_UIB_SMRY_VIEW_ROW_XREF_ID to &1;
grant all privileges on SEQ_UIB_SMRY_VW_CELLRTG_XRF_ID to &1;
grant all privileges on SEQ_UIB_SMRY_VW_ROWCELL_XRF_ID to &1;
grant all privileges on SEQ_UIB_SP_DETAILS_ID to &1;
grant all privileges on SEQ_UIB_VIEW_PROP_SET_DTL_ID to &1;
grant all privileges on SEQ_UIB_VIEW_PROP_SET_ID to &1;

spool off
quit

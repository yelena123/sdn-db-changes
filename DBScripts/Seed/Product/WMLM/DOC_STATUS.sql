set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DOC_STATUS

INSERT INTO DOC_STATUS ( DOC_STATUS,DESCRIPTION) 
VALUES  ( 10,'New');

INSERT INTO DOC_STATUS ( DOC_STATUS,DESCRIPTION) 
VALUES  ( 20,'Replaced');

INSERT INTO DOC_STATUS ( DOC_STATUS,DESCRIPTION) 
VALUES  ( 30,'Updated');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for VOCOLLECT_PARM

INSERT INTO VOCOLLECT_PARM ( WHSE,VOCOLLECT_INSTL,WELCOME_PROMPT,CNTR_TRKG_FLAG,VOCOLLECT_WORK_TYPE,VOCOLLECT_BASE_WT,VOCOLLECT_BASE_QTY,VOCOLLECT_DLVRY_LOCN,BASE_PICK_AISLE_FLAG,DELIMITER,
CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,DROP_CARTON_TEXT,DROP_ALL_TEXT,VOCOLLECT_PARM_ID,WM_VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '01','2004r1',null,'Y',3,0,0,'1','N',',',
systimestamp,systimestamp,'goya',null,null,2,3,SYSTIMESTAMP,systimestamp);

Commit;





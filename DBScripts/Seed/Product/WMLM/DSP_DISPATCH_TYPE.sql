set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DSP_DISPATCH_TYPE

INSERT INTO DSP_DISPATCH_TYPE ( DSP_DISPATCH_TYPE,DESCRIPTION) 
VALUES  ( 4,'Checked In');

INSERT INTO DSP_DISPATCH_TYPE ( DSP_DISPATCH_TYPE,DESCRIPTION) 
VALUES  ( 8,'Checked Out');

INSERT INTO DSP_DISPATCH_TYPE ( DSP_DISPATCH_TYPE,DESCRIPTION) 
VALUES  ( 12,'Debriefed');

INSERT INTO DSP_DISPATCH_TYPE ( DSP_DISPATCH_TYPE,DESCRIPTION) 
VALUES  ( 16,'Dispatched');

INSERT INTO DSP_DISPATCH_TYPE ( DSP_DISPATCH_TYPE,DESCRIPTION) 
VALUES  ( 20,'Assigned');

INSERT INTO DSP_DISPATCH_TYPE ( DSP_DISPATCH_TYPE,DESCRIPTION) 
VALUES  ( 24,'UnAssigned');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ISSUE_STATUS

INSERT INTO ISSUE_STATUS ( STATUS_CODE,STATUS_DESCRIPTION) 
VALUES  ( 10,'Open');

INSERT INTO ISSUE_STATUS ( STATUS_CODE,STATUS_DESCRIPTION) 
VALUES  ( 20,'Closed');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for E_EVNT_STAT_CODE

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 1,'10','CREATED',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 2,'30','UPDATED',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 3,'40','VERIFIED',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 4,'90','SCHEDULED DELETE',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 5,'95','CANCELED',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 6,'80','SCHEDULED ADJUSTMENT',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 7,'5','PENDING APPROVAL - CREATED',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 8,'25','PENDING APPROVAL - UPDATED',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 9,'70','REJECTED - CREATED',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 10,'75','REJECTED - UPDATED',SYSDATE,SYSDATE,'WORKINFO',null,null,null,null,
1);

INSERT INTO E_EVNT_STAT_CODE ( EVNT_STAT_ID,EVNT_STAT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,MISC_NUM_2,
VERSION_ID) 
VALUES  ( 11,'99','DISABLED',SYSDATE,SYSDATE,'WORKINFO',null,null,0,0,
1);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TPM_CONFIGURATION

INSERT INTO TPM_CONFIGURATION ( MODULE,PROCESS,RULE,ASSOCIATION_TYPE) 
VALUES  ( 'GIV','SyncExclusion',1,'AND');

INSERT INTO TPM_CONFIGURATION ( MODULE,PROCESS,RULE,ASSOCIATION_TYPE) 
VALUES  ( 'GIV','SyncExclusion',2,null);

INSERT INTO TPM_CONFIGURATION ( MODULE,PROCESS,RULE,ASSOCIATION_TYPE) 
VALUES  ( 'TPM','Manually Accept-Reject ASN',3,'AND');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_CONTROL_FILE_FORMAT

INSERT INTO CL_CONTROL_FILE_FORMAT ( CONTROL_FILE_FORMAT_ID,CONTROL_FILE_FORMAT_NAME) 
VALUES  ( 1,'FileName');

INSERT INTO CL_CONTROL_FILE_FORMAT ( CONTROL_FILE_FORMAT_ID,CONTROL_FILE_FORMAT_NAME) 
VALUES  ( 2,'FileInfo');

INSERT INTO CL_CONTROL_FILE_FORMAT ( CONTROL_FILE_FORMAT_ID,CONTROL_FILE_FORMAT_NAME) 
VALUES  ( 3,'Message');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DYNAMIC_CONTROL_FIELDS

INSERT INTO DYNAMIC_CONTROL_FIELDS ( FIELD_ID,FIELD_NAME) 
VALUES  ( 1,'Season');

INSERT INTO DYNAMIC_CONTROL_FIELDS ( FIELD_ID,FIELD_NAME) 
VALUES  ( 2,'Season Year');

INSERT INTO DYNAMIC_CONTROL_FIELDS ( FIELD_ID,FIELD_NAME) 
VALUES  ( 3,'Style');

INSERT INTO DYNAMIC_CONTROL_FIELDS ( FIELD_ID,FIELD_NAME) 
VALUES  ( 4,'Style Suffix');

INSERT INTO DYNAMIC_CONTROL_FIELDS ( FIELD_ID,FIELD_NAME) 
VALUES  ( 5,'Color');

INSERT INTO DYNAMIC_CONTROL_FIELDS ( FIELD_ID,FIELD_NAME) 
VALUES  ( 6,'Color Suffix');

INSERT INTO DYNAMIC_CONTROL_FIELDS ( FIELD_ID,FIELD_NAME) 
VALUES  ( 7,'Size Desc');

INSERT INTO DYNAMIC_CONTROL_FIELDS ( FIELD_ID,FIELD_NAME) 
VALUES  ( 8,'Item Quality');

INSERT INTO DYNAMIC_CONTROL_FIELDS ( FIELD_ID,FIELD_NAME) 
VALUES  ( 9,'Item Second Dimension');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for MANH_VERSION

INSERT INTO MANH_VERSION ( MANH_VERSION) 
VALUES  ( 'WMLM2010FP3.4');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for FREQUENCY_TYPE

INSERT INTO FREQUENCY_TYPE ( FREQUENCY_TYPE,DESCRIPTION) 
VALUES  ( 4,'DATE');

INSERT INTO FREQUENCY_TYPE ( FREQUENCY_TYPE,DESCRIPTION) 
VALUES  ( 8,'WEEKLY');

Commit;





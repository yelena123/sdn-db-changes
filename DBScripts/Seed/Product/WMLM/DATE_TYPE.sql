set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DATE_TYPE

INSERT INTO DATE_TYPE ( DATE_TYPE,DESCRIPTION) 
VALUES  ( 'PS','Pickup Start');

INSERT INTO DATE_TYPE ( DATE_TYPE,DESCRIPTION) 
VALUES  ( 'PE','Pickup End');

INSERT INTO DATE_TYPE ( DATE_TYPE,DESCRIPTION) 
VALUES  ( 'DS','Delivery Start');

INSERT INTO DATE_TYPE ( DATE_TYPE,DESCRIPTION) 
VALUES  ( 'DE','Delivery End');

INSERT INTO DATE_TYPE ( DATE_TYPE,DESCRIPTION) 
VALUES  ( 'AT','Appointment Time');

INSERT INTO DATE_TYPE ( DATE_TYPE,DESCRIPTION) 
VALUES  ( 'DT','Est. Dispatch Time');

Commit;





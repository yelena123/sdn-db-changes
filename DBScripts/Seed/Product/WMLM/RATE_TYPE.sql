set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RATE_TYPE

INSERT INTO RATE_TYPE ( RATE_TYPE,DESCRIPTION) 
VALUES  ( 'M','Per Mile');

INSERT INTO RATE_TYPE ( RATE_TYPE,DESCRIPTION) 
VALUES  ( 'P','Percentage');

INSERT INTO RATE_TYPE ( RATE_TYPE,DESCRIPTION) 
VALUES  ( 'C','Contract Rate');

INSERT INTO RATE_TYPE ( RATE_TYPE,DESCRIPTION) 
VALUES  ( 'K','Per Kilometer');

INSERT INTO RATE_TYPE ( RATE_TYPE,DESCRIPTION) 
VALUES  ( 'S','Spot Rate');

INSERT INTO RATE_TYPE ( RATE_TYPE,DESCRIPTION) 
VALUES  ( 'F','Flat Charge');

Commit;





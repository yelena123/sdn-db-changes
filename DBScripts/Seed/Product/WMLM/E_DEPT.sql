set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for E_DEPT

INSERT INTO E_DEPT ( DEPT_ID,DEPT_CODE,DESCRIPTION,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WHSE,MISC_TXT_1,MISC_TXT_2,MISC_NUM_1,
MISC_NUM_2,PERF_GOAL,VERSION_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'MANH','Default Team',SYSDATE,SYSDATE,'WORKINFO','01',null,null,null,
null,100,1,SYSTIMESTAMP,systimestamp);

Commit;





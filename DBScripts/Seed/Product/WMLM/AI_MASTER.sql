set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for AI_MASTER

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '329','Depth, thickness, height or 3rd dimensio','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,1,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '390','Amount payable - single monetary area','N',15,'2',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',2,2,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '327','Depth, thickness, height or 3rd dimensio','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,3,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '352','Area, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,4,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '351','Area, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,5,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '350','Area, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,6,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '356','Net weight','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,7,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '357','Net volume','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,8,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '330','Gross weight','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,14,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '331','Length or 1st dimension, logistics','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,15,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '332','Width, diameter or 2nd dimension, logist','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,16,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '333','Depth, thickness, height or 3rd dimensio','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,17,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '334','Area, logistics','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,18,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '336','Gross volume','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,19,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '349','Depth, thickness, height or 3rd dimensio','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,20,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '348','Depth, thickness, height or 3rd dimensio','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,21,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '347','Depth, thickness, height or 3rd dimensio','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,22,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '355','Area, logistics','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,23,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '354','Area, logistics','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,24,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '353','Area, logistics','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,25,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '362','Gross volume','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,28,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '335','Gross volume','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,31,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '340','Gross weight','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,32,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '343','Length or 1st dimension, logistics','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,33,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '342','Length or 1st dimension, logistics','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,34,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '341','Length or 1st dimension, logistics','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,35,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '346','Width, diameter or 2nd dimension, logist','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,36,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '345','Width, diameter or 2nd dimension, logist','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,37,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '344','Width, diameter or 2nd dimension, logist','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,38,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '00','Serial Shipping Container Code (SSCC)','N',18,'0',0,1,0,null,systimestamp,
systimestamp,'WMADMIN',1,39,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '01','Global Trade Item Number (GTIN)','N',16,'0',0,1,0,null,systimestamp,
systimestamp,'WMADMIN',2,40,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '02','GTIN of trade items contained in a logis','N',14,'0',0,1,0,null,systimestamp,
systimestamp,'WMADMIN',1,41,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '10','Batch or lot number','A',20,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,42,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '11','Production date (YYMMDD)','D',6,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,43,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '12','Due date (YYMMDD)','D',6,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,44,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '13','Packaging date (YYMMDD)','D',6,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,45,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '15','Minimum durability date (YYMMDD)','D',6,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,46,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '17','Maximum durability date (YYMMDD)','D',6,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,47,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '20','Product variant','N',2,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,48,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '21','Serial number','A',20,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,49,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '22','Secondary data for specific health indus','A',29,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,50,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '23','Lot number (transitional use)','N',19,'1',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,51,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '240','Additional product identification assign','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,52,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '250','Secondary serial number','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,53,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '241','Customer part number','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,54,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '251','Reference to Source Entity ','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,55,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '30','Variable count','N',8,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,56,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '337','Kilograms per square meter','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,57,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '37','Count of trade items contained in a logi','N',8,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,58,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '392','Amount Payable - single monetary unit','N',15,'2',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,59,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '400','Customer''s purchase order number','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,61,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '401','Consignment number','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,62,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '402','Shipment identification number','N',17,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,63,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '403','Routing code  EAN?UCC','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,64,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '420','"Ship to - Deliver to" postal code withi','A',20,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,65,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '422','Country of origin of a trade item','N',3,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,66,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '8001','Roll products - width, length, core diam','N',14,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,67,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '8002','Cellular Mobile Telephone Identifier (CM','A',20,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,68,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '8004','Global Individual Asset Identifier (GIAI','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,69,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '8005','Price per unit of measure','N',6,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,70,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '8007','International Bank Account Number (IBAN)','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,71,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '8018','Global Service Relation Number (GSRN)','N',18,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,72,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '8020','Payment slip reference number','A',25,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,73,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '91','Company internal information','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,74,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '90','Information mutually agreed between trad','A',30,'2',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,75,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '310','Net weight','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,84,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '311','Length or 1st dimension, trade ','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,85,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '312','Width, diameter or 2nd dimension, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,86,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '313','Depth, thickness, height or 3rd dimensio','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,87,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '314','Area, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,88,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '316','Net volume','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,90,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '320','Net weight','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,91,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '321','Length or 1st dimension, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,92,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '322','Length or 1st dimension, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,93,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '323','Length or 1st dimension, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,94,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '326','Width, diameter or 2nd dimension, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,96,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '325','Width, diameter or 2nd dimension, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,97,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '410','"Ship to -Deliver to" EAN/UCC Global Loc','N',13,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,98,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '411','"Bill to - Invoice to" EAN/UCC Global Lo','N',13,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,99,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '412','"Purchased from" EAN/UCC Global Location','N',13,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,100,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '413','"Ship for- Deliver for - Forward to" EAN','N',13,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,101,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '414','Identification of a physical location, E','N',13,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,102,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '415','EAN/UCC Global Location Number (GLN) of ','N',13,'0',0,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,103,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '315','Net volume','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,89,SYSTIMESTAMP,systimestamp);

INSERT INTO AI_MASTER ( AI,AI_TITLE,STRING_FORMAT_TYPE,STRING_LEN,STRING_LEN_TYPE,DECIMAL_INDICATOR,CALC_CHK_DIGIT_FLAG,INC_CHK_DIGIT_FLAG,AI_UOM,CREATE_DATE_TIME,
MOD_DATE_TIME,USER_ID,WM_VERSION_ID,AI_MASTER_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '324','Width, diameter or 2nd dimension, trade','N',6,'0',1,0,0,null,systimestamp,
systimestamp,'WMADMIN',1,95,SYSTIMESTAMP,systimestamp);

Commit;





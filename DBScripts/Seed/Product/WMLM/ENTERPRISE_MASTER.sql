set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ENTERPRISE_MASTER

INSERT INTO ENTERPRISE_MASTER ( ENTERPRISE_MASTER_ID,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,SEND_OUTPT_TO_EIS,SKU_IMAGE_FILE_PATH,ENABLE_3PL_FLAG,ZEBRA_MAXI,ZEBRA_INTL_MAXI,DFLT_PERF_CODE_ASN_OVERAGE,
DFLT_PERF_CODE_ASN_SHORT,DFLT_PERF_CODE_CASE_OVERAGE,DFLT_PERF_CODE_CASE_SHORT,DFLT_PERF_CODE_INVALID_SKU,DFLT_PERF_CODE_SKU_NOT_IN_ASN,DFLT_PERF_CODE_SKU_NOT_IN_CASE,DFLT_PERF_CODE_PASS,DFLT_PERF_CODE_FAIL,ALLOW_ASN_VERIFY_FOR_QH_LPN,ALLOW_ASN_VERIFY_FOR_QA_PE_LPN,
CREATE_DISPATCH_GUIDANCE_XML,LINES_PER_DISPATCH_GUIDANCE,DISPATCH_GUIDANCE_NBR_MAX,ELECTRONIC_TRANSFER_PRT_Q_TYPE,DISPATCH_GUIDANCE_XML_OUT_DIR,WM_VERSION_ID,DFLT_PERF_CODE_SKU_NOT_ASN,DFLT_PERF_CODE_SKU_NOT_CASE,QUAL_AUD_FAILED_LOCK_CODE,MHE_VENDOR_ID,
MHE_USER_NAME,MHE_OUTBOUND_MESSAGE_FORAMAT,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,sysdate,sysdate,'WMS','Y',null,'Y','^BD2^FH^FD*maxi_zebra_hdr*_5B)>_1E01_1D96*maxi_trkg_nbr*_1D*maxi_scac*_1D*maxi_shpr_nbr*_1D*maxi_julian*_1D_1D*maxi_x_of_y*_1D*maxi_wt*_1D*maxi_addr_vldn*_1D*maxi_prime_addr*maxi_second_addr_zeb*_1D*maxi_shipto_city*_1D*maxi_shipto_state*_1E_04^FS^M','^BD3^FH^FD*maxi_zebra_hdr*_5B)>_1E01_1D96*maxi_trkg_nbr*_1D*maxi_scac*_1D*maxi_shpr_nbr*_1D*maxi_julian*_1D_1D*maxi_x_of_y*_1D*maxi_wt*_1D*maxi_addr_vldn*_1D*maxi_prime_addr*maxi_second_addr_zeb*_1D*maxi_shipto_city*_1D*maxi_shipto_state*_1E_04^FS^M',null,
null,null,null,null,null,null,null,null,'N','N',
null,null,null,null,null,1,null,null,null,null,
null,null,SYSTIMESTAMP,systimestamp);

Commit;





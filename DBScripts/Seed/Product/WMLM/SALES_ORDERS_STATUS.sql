set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SALES_ORDERS_STATUS

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 0,'Created','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 1,'sourced','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 2,'Sourcing Failed','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 3,'Ready for Allocation','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 4,'Scheduling Failed','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 5,'Allocated','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 6,'Allocation failed','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 7,'Partially Released','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 8,'Released','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 9,'Partially DC Allocated','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 10,'DC Allocated','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 11,'Partially Shipped','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 12,'Shipped','DOM State');

INSERT INTO SALES_ORDERS_STATUS ( SALES_ORDERS_STATUS,DESCRIPTION,NOTE) 
VALUES  ( 13,'Cancel','DOM State');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SR_LAYOUT_HEADER

INSERT INTO SR_LAYOUT_HEADER ( SR_LAYOUT_HEADER_ID,SR_OBJECT_TYPE_ID,FILTER_OBJECT,ALERT_DEF_OBJECT_TYPE) 
VALUES  ( 1,1,'SHIPMENT','SHIPMENT');

INSERT INTO SR_LAYOUT_HEADER ( SR_LAYOUT_HEADER_ID,SR_OBJECT_TYPE_ID,FILTER_OBJECT,ALERT_DEF_OBJECT_TYPE) 
VALUES  ( 2,2,'CONS_OPTI',null);

Commit;





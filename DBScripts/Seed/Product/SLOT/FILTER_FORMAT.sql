SET DEFINE OFF;
Insert into FILTER_FORMAT
   (FORMAT_ID, WHSE_CODE, NAME, DESCRIPTION, FF_TYPE, 
    MOD_USER)
 Values
   (1, '*df', 'Default', 'Default item grid format', 0, 
    'admin');
Insert into FILTER_FORMAT
   (FORMAT_ID, WHSE_CODE, NAME, DESCRIPTION, FF_TYPE, 
    MOD_USER)
 Values
   (2, '*df', 'Default', 'Default slot grid format', 1, 
    'admin');
Insert into FILTER_FORMAT
   (FORMAT_ID, WHSE_CODE, NAME, DESCRIPTION, FF_TYPE, 
    MOD_USER)
 Values
   (3, '*df', 'Default', 'Default order grid format', 2, 
    'admin');
Insert into FILTER_FORMAT
   (FORMAT_ID, WHSE_CODE, NAME, DESCRIPTION, FF_TYPE, 
    MOD_USER)
 Values
   (4, '*df', 'Default', 'Default work assignment grid format', 3, 
    'admin');
Insert into FILTER_FORMAT
   (FORMAT_ID, WHSE_CODE, NAME, DESCRIPTION, FF_TYPE, 
    MOD_USER)
 Values
   (5, '*df', 'Default', 'Default WA detail grid format', 4, 
    'admin');
Insert into FILTER_FORMAT
   (FORMAT_ID, WHSE_CODE, NAME, DESCRIPTION, FF_TYPE, 
    MOD_USER)
 Values
   (6, '*df', 'Default', 'Default moves list grid format', 5, 
    'admin');
COMMIT;

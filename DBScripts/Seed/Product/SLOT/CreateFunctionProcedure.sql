--------------------------------------------------------
--  File created - Tuesday-June-16-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function GET_BIN_MOVEMENT
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION GET_BIN_MOVEMENT (v_est_movement IN NUMBER, v_actual_movement IN NUMBER, v_use_estimated_hist IN NUMBER)
RETURN NUMBER
   as
   v_movement  NUMBER(15,4);
BEGIN
   v_movement := 0;
   RETURN(v_movement);
END;

/
--------------------------------------------------------
--  DDL for Function GET_CASE_MOVEMENT
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION GET_CASE_MOVEMENT (v_est_movement IN NUMBER, v_actual_movement IN NUMBER, v_use_estimated_hist IN NUMBER)
RETURN NUMBER
   as
   v_movement  NUMBER(15,4);
BEGIN
   v_movement := 0;
   IF (v_use_estimated_hist = 0) then
      IF(v_actual_movement >= 0) then
         v_movement := v_actual_movement;
      ELSE
         IF (v_est_movement >= 0) then
            v_movement := v_est_movement;
         end if;
      end if;
   ELSE
      IF (v_use_estimated_hist = 1) then
         IF(v_est_movement >= 0) then
            v_movement := v_est_movement;
         ELSE
            IF (v_actual_movement >= 0) then
               v_movement := v_actual_movement;
            end if;
         end if;
      end if;
   end if;
   RETURN(v_movement);
END;

/
--------------------------------------------------------
--  DDL for Function GET_EACH_MOVEMENT
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION GET_EACH_MOVEMENT (v_each_per_cs IN NUMBER, v_est_movement IN NUMBER, v_actual_movement IN NUMBER, v_use_estimated_hist IN NUMBER)
RETURN NUMBER
   as
   v_movement  NUMBER(15,4);
BEGIN
   v_movement := 0;
   IF (v_use_estimated_hist = 0) then
      IF(v_actual_movement >= 0) then
         v_movement := v_actual_movement;
      ELSE
         IF (v_est_movement >= 0) then
            v_movement := v_est_movement*v_each_per_cs;
         end if;
      end if;
   ELSE
      IF (v_use_estimated_hist = 1) then
         IF(v_est_movement >= 0) then
            v_movement := v_est_movement*v_each_per_cs;
         ELSE
            IF (v_actual_movement >= 0) then
               v_movement := v_actual_movement;
            end if;
         end if;
      end if;
   end if;
   RETURN(v_movement);
END;

/
--------------------------------------------------------
--  DDL for Function GET_FORECAST_HITS
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION GET_FORECAST_HITS (v_est_hits IN NUMBER, v_actual_hits IN NUMBER, v_use_estimated_hist IN NUMBER)
RETURN NUMBER
   as
   v_hits  NUMBER(15,4);
BEGIN
   v_hits := 0;
   IF (v_use_estimated_hist = 0) then
      IF(v_actual_hits >= 0) then
         v_hits := v_actual_hits;
      ELSE
         IF (v_est_hits >= 0) then
            v_hits := v_est_hits;
         end if;
      end if;
   ELSE
      IF (v_use_estimated_hist = 1) then
         IF(v_est_hits >= 0) then
            v_hits := v_est_hits;
         ELSE
            IF (v_actual_hits >= 0) then
               v_hits := v_actual_hits;
            end if;
         end if;
      end if;
   end if;
   RETURN(v_hits);
END;

/
--------------------------------------------------------
--  DDL for Function GET_INNER_MOVEMENT
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION GET_INNER_MOVEMENT (v_inner_per_cs IN NUMBER, v_est_movement IN NUMBER, v_actual_movement IN NUMBER, v_use_estimated_hist IN NUMBER)
RETURN NUMBER
   as
   v_movement  NUMBER(15,4);
BEGIN
   v_movement := 0;
   IF (v_use_estimated_hist = 0) then
      IF(v_actual_movement >= 0) then
         v_movement := v_actual_movement;
      ELSE
         IF (v_est_movement >= 0) then
            v_movement := v_est_movement*v_inner_per_cs;
         end if;
      end if;
   ELSE
      IF (v_use_estimated_hist = 1) then
         IF(v_est_movement >= 0) then
            v_movement := v_est_movement*v_inner_per_cs;
         ELSE
            IF (v_actual_movement >= 0) then
               v_movement := v_actual_movement;
            end if;
         end if;
      end if;
   end if;
   RETURN(v_movement);
END;

/
--------------------------------------------------------
--  DDL for Function GET_PALLET_MOVEMENT
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION GET_PALLET_MOVEMENT (v_sku_id IN NUMBER, v_pallet_pattern IN NUMBER, v_est_movement IN NUMBER, v_actual_movement IN NUMBER, v_use_estimated_hist IN NUMBER)
RETURN NUMBER
   as
   v_movement  NUMBER(15,4);
   v_hi  NUMBER(10,0);
   v_ti  NUMBER(10,0);
BEGIN
   v_movement := 0;
   v_hi := 0;
   v_ti := 0;
   IF (v_pallet_pattern = 0) then
      BEGIN
         select   wh_hi, wh_ti INTO v_hi,v_ti FROM so_item_master WHERE sku_id = v_sku_id;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
   ELSE
      IF (v_pallet_pattern = 1) then
         BEGIN
            select   ord_hi, ord_ti INTO v_hi,v_ti FROM so_item_master WHERE sku_id = v_sku_id;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      ELSE
         IF (v_pallet_pattern = 2) then
            BEGIN
               select   ven_hi, ven_ti INTO v_hi,v_ti FROM so_item_master WHERE sku_id = v_sku_id;
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            END;
         end if;
      end if;
   end if;
   IF ((v_hi*v_ti) = 0) then
      RETURN v_movement;
   end if;
   IF (v_use_estimated_hist = 0) then
      IF(v_actual_movement >= 0) then
         v_movement := v_actual_movement;
      ELSE
         IF (v_est_movement >= 0) then
            v_movement := v_est_movement/(v_hi*v_ti);
         end if;
      end if;
   ELSE
      IF (v_use_estimated_hist = 1) then
         IF(v_est_movement >= 0) then
            v_movement := v_est_movement;
         ELSE
            IF (v_actual_movement >= 0) then
               v_movement := v_actual_movement;
            end if;
         end if;
      end if;
   end if;
   RETURN(v_movement);
END;

/
--------------------------------------------------------
--  DDL for Function GET_UNIQUE_IDS
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION GET_UNIQUE_IDS (v_rec_type IN varchar2)
RETURN NUMBER
   as
   v_nextnumber  NUMBER(19);

BEGIN
    --select (curr_nbr+1) into v_nextnumber from unique_ids where rec_type = v_rec_type;
    --update unique_ids set curr_nbr = v_nextnumber  where rec_type = v_rec_type;
   sp_get_unique_ids(v_rec_type, v_nextnumber);
   RETURN(v_nextnumber);
END;

/
--------------------------------------------------------
--  DDL for Function GET_VISCOSITY
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION GET_VISCOSITY (v_slotitemid IN NUMBER,v_est_or_calc IN NUMBER,v_whse_code IN CHAR)--SQLWAYS_EVAL# means calc
RETURN NUMBER
   as
   v_system  NUMBER(10,0);
   v_metric_unit  NUMBER(10,0);
   v_metric_vol  NUMBER(10,0);
   v_cube_conversion  NUMBER(13,4);
   v_base  NUMBER(13,4);
   v_exp  NUMBER(13,4);
   v_ret_val  NUMBER(15,2);
   v_hits  NUMBER(15,2);
   v_cube_mvmt  NUMBER(15,2);
BEGIN
   begin
      select   whprefs_system, metric_unit, metric_vol INTO v_system,v_metric_unit,v_metric_vol FROM    whprefs WHERE   whse_code = v_whse_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
    --SQLWAYS_EVAL# of unit is english
   IF v_system = 1 then
      v_cube_conversion := POWER(12,3);
   ELSE
      v_exp := v_metric_vol -v_metric_unit;
      v_base := POWER(10,v_exp);
      v_cube_conversion := POWER(v_base,3);
   end if;
    --ESTIMATED start
   if (v_est_or_calc  = 1) then
      begin
         select   est_hits INTO v_hits from slot_item where slotitem_id = v_slotitemid;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      begin
         select((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht*slot_item.est_movement)/v_cube_conversion) INTO v_cube_mvmt from so_item_master,slot_item where
         so_item_master.sku_id =(select sku_id from slot_item where slotitem_id  = v_slotitemid) and slot_item.slotitem_id = v_slotitemid;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;
    --ESTIMATED end
    --SQLWAYS_EVAL# rt
   if (v_est_or_calc  = 2) then
      begin
         select   calc_hits INTO v_hits from slot_item where slotitem_id = v_slotitemid;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      begin
         select((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht*slot_item.case_mov)/v_cube_conversion) INTO v_cube_mvmt from so_item_master,slot_item where
         so_item_master.sku_id =(select sku_id from slot_item where slotitem_id  = v_slotitemid) and slot_item.slotitem_id = v_slotitemid;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;
    --CALCULATED end
   if(v_hits <= 0 or v_cube_mvmt <= 0) then
      v_ret_val := -999.99;
   else
      v_ret_val := v_hits/sqrt(v_cube_mvmt);
   end if;
   RETURN(v_ret_val);
END;

/
--------------------------------------------------------
--  DDL for Function GET_WIPS_VALUE
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION GET_WIPS_VALUE (v_slotitemid IN NUMBER, v_lane IN NUMBER, v_stack IN NUMBER, v_deep IN NUMBER)
RETURN NUMBER
   as
   v_ret_val  NUMBER(15,2);
   v_case_mov_est  NUMBER(15,4);
   v_case_mov_cal  NUMBER(15,4);
   v_case_mov_final  NUMBER(15,4);
   v_est_hist  NUMBER(1,0);
   v_slot_qty  NUMBER(15,4);
   v_slot_unit  NUMBER(10,0);
   v_pallet_hi  NUMBER(10,0);
   v_pallet_pat  NUMBER(10,0);
   v_warehouse_ti  NUMBER(10,0);
   v_order_ti  NUMBER(10,0);
   v_vendor_ti  NUMBER(10,0);
   v_inner_per_case  NUMBER(10,0);
   v_each_per_case  NUMBER(10,0);
   v_bin_unit  NUMBER(10,0);
   v_bin_height  NUMBER(15,4);
   v_bin_width  NUMBER(15,4);
   v_bin_length  NUMBER(15,4);
   v_case_height  NUMBER(15,4);
   v_case_width  NUMBER(15,4);
   v_case_length  NUMBER(15,4);
   v_inner_height  NUMBER(15,4);
   v_inner_width  NUMBER(15,4);
   v_inner_length  NUMBER(15,4);
   v_each_height  NUMBER(15,4);
   v_each_width  NUMBER(15,4);
   v_each_length  NUMBER(15,4);
   v_case_vol  NUMBER(15,4);
   v_each_vol  NUMBER(15,4);
   v_inner_vol  NUMBER(15,4);
   v_bin_vol  NUMBER(15,4);
   v_bin_fillpercent  NUMBER(15,2);
BEGIN
   begin
      select   use_estimated_hist, est_movement, case_mov INTO v_est_hist,v_case_mov_est,v_case_mov_cal from slot_item where slotitem_id = v_slotitemid;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   v_case_mov_final := get_case_movement(v_case_mov_est,v_case_mov_cal,v_est_hist);
       begin
      select   SLOT_UNIT, PALLET_HI, PALLETE_PATTERN, WH_TI, ORD_TI, VEN_TI, INN_PER_CS, EACH_PER_CS, fillpercent, BIN_UNIT, height_b, width_b, length_b, CASE_HT, CASE_WID, CASE_LEN, INN_HT, INN_WID, INN_LEN, EACH_HT, EACH_WID, EACH_LEN INTO v_slot_unit,v_pallet_hi,v_pallet_pat,v_warehouse_ti,v_order_ti,v_vendor_ti,
      v_inner_per_case,v_each_per_case,v_bin_fillpercent,v_bin_unit,v_bin_height,
      v_bin_width,v_bin_length,v_case_height,v_case_width,v_case_length,
      v_inner_height,v_inner_width,v_inner_length,v_each_height,v_each_width,
      v_each_length from CURRENT_ITEMS where slotitem_id = v_slotitemid;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   IF (v_inner_per_case = 0) then
      v_inner_per_case := 1;
   end if;
   IF (v_each_per_case = 0) then
      v_each_per_case := 1;
   end if;
   v_case_vol  := v_case_height*v_case_width*v_case_length;
   v_inner_vol := v_inner_height*v_inner_width*v_inner_length;
   v_each_vol  := v_each_height*v_each_width*v_each_length;
   v_bin_vol   :=(v_bin_height*v_bin_width*v_bin_length)*(v_bin_fillpercent/100);
   IF (v_slot_unit = 1) AND (v_pallet_pat = 0) then
      v_slot_qty := v_pallet_hi*v_warehouse_ti*v_deep*v_lane*v_stack;
   ELSE
      IF (v_slot_unit = 1) AND (v_pallet_pat = 1) then
         v_slot_qty := v_pallet_hi*v_order_ti*v_deep*v_lane*v_stack;
      ELSE
         IF (v_slot_unit = 1) AND (v_pallet_pat = 2) then
            v_slot_qty := v_pallet_hi*v_vendor_ti*v_deep*v_lane*v_stack;
         ELSE
            IF (v_slot_unit = 2) then
               v_slot_qty := v_deep*v_lane*v_stack;
            ELSE
               IF (v_slot_unit = 4) then
                  v_slot_qty :=(v_deep*v_lane*v_stack)/v_inner_per_case;
               ELSE
                  IF (v_slot_unit = 8) then
                     v_slot_qty :=(v_deep*v_lane*v_stack)/v_each_per_case;
                  ELSE
                     IF (v_slot_unit = 16) AND (v_bin_unit = 2) then
                        v_slot_qty :=(v_bin_vol/v_case_vol)*v_deep*v_lane*v_stack;
                     ELSE
                        IF (v_slot_unit = 16) AND (v_bin_unit = 4) then
                           v_slot_qty :=((v_bin_vol/v_inner_vol)/v_inner_per_case)*v_deep*v_lane*v_stack;
                        ELSE
                           IF (v_slot_unit = 16) AND (v_bin_unit = 8) then
                              v_slot_qty :=((v_bin_vol/v_each_vol)/v_each_per_case)*v_deep*v_lane*v_stack;
                           ELSE
                              v_slot_qty := -999.99;
                           end if;
                        end if;
                     end if;
                  end if;
               end if;
            end if;
         end if;
      end if;
   end if;
   IF (v_case_mov_final > 0) then
      v_ret_val :=(v_slot_qty/v_case_mov_final);
   ELSE
      v_ret_val := -999.99;
   end if;
   RETURN(v_ret_val);
END;

/
--------------------------------------------------------
--  DDL for Function ISEVENODD
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION ISEVENODD (INPUT VARCHAR2)
RETURN NUMBER
as
BEGIN

if(LENGTH(TRIM(TRANSLATE(TRIM(INPUT), ' +-.0123456789', ' '))) is null) THEN
   if( mod(TRIM(INPUT),2) = 0) then
    RETURN (2);

    else
    return(1);
    end if;
ELSE
    RETURN (0);

END IF;

END;

/
--------------------------------------------------------
--  DDL for Procedure GETMAXRANGE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE GETMAXRANGE 
(
    p_sns_id              sns.sns_id%type
)
as

    v_max_range VARCHAR2(500);
    v_min_range VARCHAR2(500);
    v_area_values               sns.area_values%type;
    v_zone_values               sns.zone_values%type;
    v_aisle_values              sns.aisle_values%type;
    v_bay_values                sns.bay_values%type;
    v_level_values              sns.level_values%type;
    v_pos_values                sns.pos_values%type;
    v_fill_values                sns.fill_values%type;
    varea number;
    vzone number;
    vaisle number;
    vbay number;
    vlvl number;
    vpos number;
    vfill number;
    v_component  sns_calc.component%type;
    v_range sns_calc.range%type;
    v_areaset number;
    v_zoneset number;
    v_aisleset number;
    v_bayset number;
    v_levelset number;
    v_posset number;
    v_fill_pos number;
    v_if_exists number;

    CURSOR curs_SNS IS select component,range from sns_calc  order by range;
begin
SELECT   COUNT(*) INTO v_if_exists FROM sns_max_min;
   IF  v_if_exists > 0 then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE sns_max_min ';
   end if;

SP_FILL_SNS_CALC(p_sns_id);

  select sns.area_values, sns.zone_values, sns.aisle_values, sns.bay_values, sns.level_values, sns.pos_values,sns.fill_values into v_area_values,v_zone_values,v_aisle_values,v_bay_values,v_level_values,v_pos_values,v_fill_values  from sns sns where sns.sns_id = p_sns_id;

 varea := instr(v_area_values, '-');
    vzone := instr(v_zone_values, '-');
    vaisle := instr(v_aisle_values, '-');
    vbay := instr(v_bay_values, '-');
    vlvl := instr(v_level_values, '-');
    vpos := instr(v_pos_values, '-');
    vfill := ((length(v_fill_values)/2)+0.5);


    v_areaset := 0;
    v_zoneset := 0;
    v_aisleset := 0;
    v_bayset := 0;
    v_levelset := 0;
    v_posset := 0;
    v_fill_pos := 0;


     OPEN curs_SNS;

   FETCH curs_SNS INTO v_component,v_range;
   WHILE (curs_SNS%FOUND) LOOP
   IF(v_component = 'area' and v_areaset = 0) then
       v_max_range := v_max_range || substr(v_area_values, varea+1,length(v_area_values));
       v_min_range := v_min_range || substr(v_area_values, 1,varea-1);
       v_areaset := 1;
   end if;
   IF(v_component='zone' and v_zoneset = 0) then
       v_max_range := v_max_range || substr(v_zone_values, vzone+1,length(v_zone_values));
       v_min_range := v_min_range || substr(v_zone_values, 1,vzone-1);
       v_zoneset := 1;
   end if;
   IF(v_component='aisle' and v_aisleset = 0) then
       v_max_range := v_max_range || substr(v_aisle_values, vaisle+1,length(v_aisle_values));
       v_min_range := v_min_range || substr(v_aisle_values, 1,vaisle-1);
       v_aisleset := 1;
   end if;
   IF(v_component='bay' and v_bayset = 0) then
       v_max_range := v_max_range || substr(v_bay_values, vbay+1,length(v_bay_values));
       v_min_range := v_min_range || substr(v_bay_values, 1,vbay-1);
       v_bayset := 1;
   end if;
   IF(v_component='level' and v_levelset = 0) then
       v_max_range := v_max_range || substr(v_level_values, vlvl+1,length(v_level_values));
       v_min_range := v_min_range || substr(v_level_values, 1,vlvl-1);
       v_levelset := 1;
   end if;
   IF(v_component='pos' and v_posset = 0) then
       v_max_range := v_max_range || substr(v_pos_values, vpos+1,length(v_pos_values));
       v_min_range := v_min_range || substr(v_pos_values, 1,vpos-1);
       v_posset := 1;
   end if;
   IF(v_component='fill') then

       v_max_range := v_max_range || substr(v_fill_values, vfill+v_fill_pos+1,1);
       v_min_range := v_min_range || substr(v_fill_values, vfill+v_fill_pos+1,1);
       v_fill_pos := v_fill_pos +1;
   end if;
   FETCH curs_SNS INTO v_component,v_range;
   END LOOP;
   CLOSE curs_SNS;

insert into sns_max_min values (p_sns_id,v_max_range,v_min_range);

commit;

dbms_output.put_line ('Printing v_min_range   '||v_min_range);
dbms_output.put_line ('Printing v_max_range   '||v_max_range);
end;

/
--------------------------------------------------------
--  DDL for Procedure MANH_DISABLE_ALL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE MANH_DISABLE_ALL 

    as

     cursor  cons_name is

     select  table_name, constraint_name

    	from  user_constraints

     where  constraint_type = 'R';

    begin

   	 for cons_rec in cons_name

     loop

   		execute immediate 'alter table '|| cons_rec.table_name ||' disable constraint '|| cons_rec.constraint_name ;

     end loop;



    end MANH_DISABLE_ALL;

/
--------------------------------------------------------
--  DDL for Procedure MANH_ENABLE_ALL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE MANH_ENABLE_ALL 

      as

     cursor  cons_name is

     select  table_name, constraint_name

   	from  user_constraints

     where  constraint_type = 'R';

    begin

   	 for cons_rec in cons_name

     loop

   		execute immediate 'alter table '|| cons_rec.table_name ||' enable



   		constraint '|| cons_rec.constraint_name ;

     end loop;



    end MANH_ENABLE_ALL;

/
--------------------------------------------------------
--  DDL for Procedure MANH_TRUNC_TABLES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE MANH_TRUNC_TABLES 
   as
   v_TABLE_NAME  NVARCHAR2(50);
   v_SQLSTRING  NVARCHAR2(100);

   CURSOR CHILD_TABLES 
   IS 
  -- SELECT TABLE_NAME FROM SYSOBJECTS WHERE XTYPE = 'U' AND SIGN(STATUS) = 1
  -- AND TABLE_NAME IN(select     distinct
    --  object_name(fkeyid) as table_name
     -- from sysforeignkeys
     -- where object_name(fkeyid) not in(select object_name(rkeyid) from sysforeignkeys))
     --    WHERE OBJECTPROPERTY(OBJECT_ID(TABLE_NAME),'SQLWAYS_EVAL# ty') = 1
  --    AND TABLE_TYPE = 'BASE TABLE');
   select TABLE_NAME from user_constraints where TABLE_NAME IN(
   select distinct TABLE_NAME from user_constraints WHERE CONSTRAINT_TYPE='R')   
   and TABLE_NAME not in(SELECT
      OBJECT_NAME
      FROM USER_OBJECTS WHERE  OBJECT_TYPE='TABLE');

   CURSOR PARENT_TABLES IS 
   --SELECT TABLE_NAME FROM SYSOBJECTS WHERE XTYPE = 'U' AND SIGN(STATUS) = 1
   --AND TABLE_NAME NOT IN(select     distinct
    --  object_name(fkeyid) as table_name
     -- from sysforeignkeys
     -- where object_name(fkeyid) not in(select object_name(rkeyid) from sysforeignkeys))
   --and TABLE_NAME not in(SELECT
      --TABLE_NAME
     -- FROM INFORMATION_SCHEMA.TABLES
   --   WHERE OBJECTPROPERTY(OBJECT_ID(TABLE_NAME),'SQLWAYS_EVAL# ty') = 1
    --  AND TABLE_TYPE = 'BASE TABLE');
   select TABLE_NAME from user_constraints where TABLE_NAME NOT IN(
   select distinct TABLE_NAME from user_constraints WHERE CONSTRAINT_TYPE='R')   
   and TABLE_NAME not in(SELECT
      OBJECT_NAME
      FROM USER_OBJECTS WHERE  OBJECT_TYPE='TABLE')
   ;
   SWV_VarStr VARCHAR2(100);
BEGIN

   OPEN CHILD_TABLES;
   FETCH CHILD_TABLES INTO v_TABLE_NAME;
   MANH_DISABLE_ALL();
   WHILE CHILD_TABLES%FOUND  LOOP
      v_SQLSTRING := 'TRUNCATE TABLE ' || v_TABLE_NAME || '';
      DBMS_OUTPUT.PUT_LINE(v_SQLSTRING);
      SWV_VarStr := v_SQLSTRING;
      EXECUTE IMMEDIATE SWV_VarStr;
      FETCH CHILD_TABLES INTO v_TABLE_NAME;
   END LOOP;
   OPEN PARENT_TABLES;
   FETCH PARENT_TABLES INTO v_TABLE_NAME;
   WHILE PARENT_TABLES%FOUND  LOOP
      v_SQLSTRING := 'DELETE ' || v_TABLE_NAME || '';
      DBMS_OUTPUT.PUT_LINE(v_SQLSTRING);
      SWV_VarStr := v_SQLSTRING;
      EXECUTE IMMEDIATE SWV_VarStr;
      FETCH PARENT_TABLES INTO v_TABLE_NAME;
   END LOOP;

   MANH_ENABLE_ALL();
   CLOSE CHILD_TABLES;

   CLOSE PARENT_TABLES;

END;

/
--------------------------------------------------------
--  DDL for Procedure PICK_LINE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE PICK_LINE (v_refcur OUT SYS_REFCURSOR)
   as
   v_plg  NUMBER(10,0);
   v_node_id  NUMBER(10,0);
   v_proportion  NUMBER(5,2);
   v_tree_node_path  VARCHAR2(100);
   v_parent_node  NUMBER(10,0);
   v_cur_node_id  NUMBER(10,0);
   v_old_node_id  NUMBER(10,0);
   v_cur_proportion  NUMBER(5,2);




   CURSOR cur_plgids IS SELECT PLGID,
		NodeID,
		Proportion
   FROM tt_TBLPLGIDS;




   CURSOR populate IS SELECT 	PLG_Mbr,
		proportion
   FROM tt_TBLPLGTEST FOR UPDATE;
   v_if_exists NUMBER(10,0);
BEGIN

/*SQLWAYS_EVAL#  (
	NodeID int,
	PLGID int,
	TEST INT
)
*/


--SQLWAYS_EVAL# to pickline groups
   INSERT INTO tt_TBLPLGIDS
   SELECT td.node_id, td.pickline_grp_id, td.proportion
   FROM tree_dtl td
   WHERE td.pickline_grp_id IN(SELECT pickline_grp_id
      FROM pickline_group)
   AND td.pickline_grp_id IS NOT NULL;


--SQLWAYS_EVAL# one at a time
   OPEN cur_plgids;
   FETCH cur_plgids INTO v_plg,v_node_id,v_proportion;
   WHILE cur_plgids%FOUND  LOOP
    SELECT   COUNT(*) INTO v_if_exists FROM user_tables WHERE table_name ='TEST';
      IF  v_if_exists > 0 then
         EXECUTE IMMEDIATE ' TRUNCATE TABLE TEST ';
      end if;


	--SQLWAYS_EVAL# node path
      v_tree_node_path := '';
      v_parent_node := v_node_id;
      WHILE v_parent_node IS NOT NULL LOOP
         begin
            SELECT v_tree_node_path || '/' || RTRIM((SELECT td.name FROM tree_dtl td
               WHERE td.node_id = v_parent_node)) INTO v_tree_node_path FROM dual;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
         begin
            SELECT td.parent_node_id INTO v_parent_node FROM tree_dtl td
            WHERE td.node_id = v_parent_node;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
      END LOOP;
      sp_get_range_ids(v_node_id,'TEST');


	--SQLWAYS_EVAL# report
      INSERT INTO tt_TBLPLGTEST
      SELECT v_node_id As PLG_Mbr,
		v_plg ,
		pg.whse_code,
		pg.name,
		pg.item_attribute,
		pg.importance,
		--SQLWAYS_EVAL# removed from the pickline_group table.
		--SQLWAYS_EVAL# e,
		--SQLWAYS_EVAL# rget,
		td.tree_id,
        td.name tree_node_name,
		v_tree_node_path,
		td.parent_node_id,
--SQLWAYS_EVAL# n,
--SQLWAYS_EVAL#
		v_proportion,
		td.plb_score
      FROM tree_dtl td JOIN TEST TT 	ON
      TT.range_id = td.node_id
      JOIN pickline_group pg
      ON v_plg = pg.pickline_grp_id;
      EXECUTE IMMEDIATE ' TRUNCATE TABLE TEST ';
      FETCH cur_plgids INTO v_plg,v_node_id,v_proportion;
   END LOOP;
   CLOSE cur_plgids;



   v_old_node_id := 0;
   OPEN populate;

   FETCH populate INTO v_cur_node_id,v_cur_proportion;
   WHILE populate%FOUND  LOOP
      IF (v_cur_node_id = v_old_node_id) then

         UPDATE tt_TBLPLGTEST Set proportion = 0
         WHERE CURRENT OF populate;
      end if;
      v_old_node_id := v_cur_node_id;
      FETCH populate INTO  v_cur_node_id,v_cur_proportion;
   END LOOP;

   CLOSE populate;


   open v_refcur for select * from tt_TBLPLGTEST;
END;

/
--------------------------------------------------------
--  DDL for Procedure PICK_LINE2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE PICK_LINE2 
   as
   v_plg  NUMBER(10,0);
   v_node_id  NUMBER(10,0);
   v_proportion  NUMBER(5,2);
   v_tree_node_path  VARCHAR2(100);
   v_attribute  VARCHAR2(60);
   v_parent_node  NUMBER(10,0);
   v_whse_code  CHAR(3);
   v_total  NUMBER(15,2);

   v_if_exists NUMBER(10,0);





   CURSOR cur_plgids IS SELECT PLGID,
			NodeID,
			Proportion
   FROM tt_TBLPLGIDS2;
BEGIN

SELECT   COUNT(*) INTO v_if_exists FROM tt_TBLPLGTEST2;-- WHERE table_name = 'tt_TBLPLGTEST2';
   IF  v_if_exists > 0 then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TBLPLGTEST2 ';
   end if;

	--SQLWAYS_EVAL# to pickline groups
   INSERT INTO tt_TBLPLGIDS2
   SELECT td.node_id, td.pickline_grp_id, td.proportion
   FROM tree_dtl td
   WHERE td.pickline_grp_id IN(SELECT pickline_grp_id
      FROM pickline_group)
   AND td.pickline_grp_id IS NOT NULL;

	--SQLWAYS_EVAL# one at a time
   OPEN cur_plgids;
   FETCH cur_plgids INTO v_plg,v_node_id,v_proportion;
   WHILE cur_plgids%FOUND  LOOP

		--SQLWAYS_EVAL# node path
      v_tree_node_path := '';
      v_parent_node := v_node_id;
      WHILE v_parent_node IS NOT NULL LOOP
         IF v_tree_node_path = '' then
            begin
               SELECT RTRIM((SELECT td.name FROM tree_dtl td
                  WHERE td.node_id = v_parent_node)) INTO v_tree_node_path FROM dual;
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            end;
         ELSE
            begin
               SELECT RTRIM((SELECT td.name FROM tree_dtl td
                  WHERE td.node_id = v_parent_node)) || '/' || v_tree_node_path INTO v_tree_node_path FROM dual;
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            end;
         end if;
         begin
            SELECT td.parent_node_id INTO v_parent_node FROM tree_dtl td
            WHERE td.node_id = v_parent_node;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
      END LOOP;
      begin
         SELECT item_attribute INTO v_attribute FROM pickline_group WHERE pickline_grp_id = v_plg;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      begin
         SELECT whse_code INTO v_whse_code FROM pickline_group WHERE pickline_grp_id = v_plg;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      IF (v_attribute = 'Hits') then
         sp_plb_hits_sum2(v_node_id,v_total);
      ELSE
         IF (v_attribute = 'SQLWAYS_EVAL# ent') then
            sp_shipping_movement_sum2(v_node_id,v_total);
         ELSE
            IF (v_attribute = 'Slotting weight') then
               sp_slotting_unit_weight_sum2(v_node_id,v_total);
            ELSE
               IF (v_attribute = 'Shipping weight') then
                  sp_shipping_unit_weight_sum2(v_node_id,v_total);
               ELSE
                  IF (v_attribute = 'Cube movement') then
                     sp_cube_movement_sum2(v_node_id,v_whse_code,v_total);
                  ELSE
                     IF (v_attribute = 'Case movement') then
                        sp_case_movement_sum2(v_node_id,v_total);
                     ELSE
                        IF (v_attribute = 'SQLWAYS_EVAL# ent') then
                           sp_slotting_movement_sum2(v_node_id,v_total);
                        ELSE
                           IF (v_attribute = 'SQLWAYS_EVAL# * Shipping length') then
                              spshippingmovementlengthsum2(v_node_id,v_total);
                           end if;
                        end if;
                     end if;
                  end if;
               end if;
            end if;
         end if;
      end if;

		--SQLWAYS_EVAL# report
      INSERT INTO tt_TBLPLGTEST2
      SELECT v_node_id As PLG_Mbr,
			v_plg ,
			pg.whse_code,
			pg.name,
			pg.item_attribute,
			pg.importance,
			--SQLWAYS_EVAL# e,
			--SQLWAYS_EVAL# rget,
			td.tree_id,
				td.name tree_node_name,
			v_tree_node_path,
			td.parent_node_id,
			v_proportion,
			td.plb_score,
			v_total
      FROM tree_dtl td
      INNER JOIN pickline_group pg ON td.pickline_grp_id = pg.pickline_grp_id
      WHERE td.node_id = v_node_id;
      FETCH cur_plgids INTO v_plg,v_node_id,v_proportion;
   END LOOP;
   CLOSE cur_plgids;
EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TBLPLGIDS2 ';

   --open v_refcur for SELECT * FROM tt_TBLPLGTEST2
   --ORDER BY name NULLS FIRST,tree_node_path NULLS FIRST;
END;

/
--------------------------------------------------------
--  DDL for Procedure SET_USR_PWD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SET_USR_PWD (p_date  IN timestamp)
is

BEGIN

   begin


         update output_result_table
         set   server_mod_date_time=sysdate;

   end;

   commit;

END set_Usr_Pwd;

/
--------------------------------------------------------
--  DDL for Procedure SO_IMP_COMPLETE_SLOT_IMPORT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SO_IMP_COMPLETE_SLOT_IMPORT (
   p_user_id             IN slot.mod_user%TYPE,
   p_whse_code           IN whprefs.whse_code%TYPE,
   p_sns_id              IN tree_dtl.sns_id%TYPE,
   p_dec_adj             IN import_format_prefs.dec_adj%TYPE,
   p_update_delete       IN import_format_prefs.update_delete%TYPE,
   p_msg_lang            IN so_message_master.msg_language%TYPE,
   p_log_level           IN whse_import_prefs.log_level%TYPE,
   p_update_exist_recs   IN import_format_prefs.update_exist_recs%TYPE)
AS
   v_add_upd_msg_nbr     so_message_master.msg_number%TYPE := 467;
   v_add_upd_msg         so_message_master.msg_text%TYPE;
   v_add_upd_msg_sev     so_message_master.severity%TYPE;
   v_no_si_msg_nbr       so_message_master.msg_number%TYPE := 460;
   v_no_si_msg           so_message_master.msg_text%TYPE;
   v_no_si_msg_sev       so_message_master.severity%TYPE;
   exc_cur_nbr           NUMBER;
   seq_cur_val           NUMBER;
   v_query               VARCHAR2 (100);
   v_mult                VARCHAR2 (10);
   v_cnt                 NUMBER;
   Lcntr                 NUMBER;
   v_rec_exce_type       VARCHAR2 (100);
   v_rec_slot_type       VARCHAR2 (100);
   v_rec_slotitem_type   VARCHAR2 (100);
BEGIN
   v_rec_exce_type := 'exceptions';
   v_rec_slot_type := 'slot';
   v_rec_slotitem_type := 'slot_item';

   SELECT mm.msg_text, mm.severity
     INTO v_add_upd_msg, v_add_upd_msg_sev
     FROM so_message_master mm
    WHERE mm.msg_number = v_add_upd_msg_nbr AND mm.msg_language = p_msg_lang;

   SELECT mm.msg_text, mm.severity
     INTO v_no_si_msg, v_no_si_msg_sev
     FROM so_message_master mm
    WHERE mm.msg_number = v_no_si_msg_nbr AND mm.msg_language = p_msg_lang;



   /*if (v_add_upd_msg_sev >= p_log_level)
   then

       insert into exceptions
       (
           ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
           msg_number, descript
       )
       select get_unique_ids(v_rec_exce_type), p_whse_code, ss.dsp_slot, sysdate, sysdate,
           p_user_id, v_add_upd_msg_nbr,  replace(replace(v_add_upd_msg, '%s1', dsp_slot), '%s2', 'slot import')
       from slot_stg ss
       where not exists
           (
               select 1
               from slot s
               where s.dsp_slot = ss.dsp_slot and s.whse_code = p_whse_code
                   and s.processed = 1
           )
           and exists
           (
               select 1
               from rack_type_level rtl
               where rtl.rack_level_id = ss.rack_level_id
           );
   end if;*/

   v_mult := '1';
   v_cnt := ABS (p_dec_adj);

   FOR Lcntr IN 1 .. v_cnt
   LOOP
      v_mult := v_mult || '0';
   END LOOP;



   -- import slot data; rack_level_id needs to be populated prior to this

   -- insert into temp_table (col1)  values (5);

   --update slot s set s.ht=2 where s.whse_code=p_whse_code and s.dsp_slot='A111' and  p_update_exist_recs = 1  ;
   --  update slot s
   --set s.processed = 0
   --where s.whse_code = p_whse_code;

   MERGE INTO slot s
        USING (SELECT ss.slot_id,
                      ss.dsp_slot,
                      ss.rack_type,
                      ss.rack_level_id,
                      ss.area,
                      ss.zone,
                      ss.aisle,
                      ss.bay,
                      ss.lvl,
                      ss.posn,
                      CASE
                         WHEN p_dec_adj < 0
                         THEN
                            (COALESCE (ss.ht, rtl.level_ht)
                             / (TO_NUMBER (v_mult)))
                         ELSE
                            (COALESCE (ss.ht, rtl.level_ht)
                             * (TO_NUMBER (v_mult)))
                      END
                         ht,
                      /*case when p_dec_adj <0 then  (coalesce(ss.width,rtl.level_width)/(to_number(v_mult))) else  (coalesce(ss.width,rtl.level_width) * (to_number(v_mult))) end  width,*/
                      rtl.level_width width,
                      ss.my_range,
                      CASE
                         WHEN p_dec_adj < 0
                         THEN
                            (COALESCE (ss.DEPTH, rtl.level_depth)
                             / (TO_NUMBER (v_mult)))
                         ELSE
                            (COALESCE (ss.DEPTH, rtl.level_depth)
                             * (TO_NUMBER (v_mult)))
                      END
                         DEPTH,
                      COALESCE (ss.wt_limit, rtl.slot_wt_limit) wt_limit,
                      CASE WHEN ss.ht != rtl.level_ht THEN 1 ELSE 0 END
                         ht_override,
                      /*case when ss.width != rtl.level_width then 1 else 0 end width_override,*/
                      CASE WHEN ss.DEPTH != rtl.level_depth THEN 1 ELSE 0 END
                         depth_override,
                      CASE
                         WHEN ss.max_lanes != rtl.max_lanes THEN 1
                         ELSE 0
                      END
                         max_ln_ovr,
                      CASE
                         WHEN ss.max_stack != rtl.max_stacking THEN 1
                         ELSE 0
                      END
                         max_st_ovr,
                      CASE
                         WHEN ss.max_side_clear != rtl.side_clear THEN 1
                         ELSE 0
                      END
                         max_sc_ovr,
                      CASE
                         WHEN ss.max_ht_clear != rtl.ht_clearance THEN 1
                         ELSE 0
                      END
                         max_hc_ovr,
                      CASE
                         WHEN ss.max_lane_wt != rtl.max_lane_wt THEN 1
                         ELSE 0
                      END
                         max_lw_ovr,
                      CASE
                         WHEN COALESCE (ss.max_stack, rtl.max_stacking) > 99
                         THEN
                            99
                         ELSE
                            COALESCE (ss.max_stack, rtl.max_stacking)
                      END
                         max_stack,
                      CASE
                         WHEN p_dec_adj < 0
                         THEN
                            (COALESCE (ss.max_side_clear, rtl.side_clear)
                             / (TO_NUMBER (v_mult)))
                         ELSE
                            (COALESCE (ss.max_side_clear, rtl.side_clear)
                             * (TO_NUMBER (v_mult)))
                      END
                         max_side_clear,
                      CASE
                         WHEN p_dec_adj < 0
                         THEN
                            (COALESCE (ss.max_ht_clear, rtl.ht_clearance)
                             / (TO_NUMBER (v_mult)))
                         ELSE
                            (COALESCE (ss.max_ht_clear, rtl.ht_clearance)
                             * (TO_NUMBER (v_mult)))
                      END
                         max_ht_clear,
                      COALESCE (ss.max_lane_wt, rtl.max_lane_wt) max_lane_wt,
                      CASE
                         WHEN ss.locked = 1 THEN ss.lock_reason
                         ELSE NULL
                      END
                         lock_reason,
                      ss.locked,
                      CASE
                         WHEN ss.wt_limit != rtl.slot_wt_limit THEN 1
                         ELSE 0
                      END
                         wt_limit_override,
                      ss.label_pos,
                      1 label_pos_ovr,
                      CASE
                         WHEN COALESCE (ss.max_lanes, rtl.max_lanes) > 99
                         THEN
                            99
                         ELSE
                            COALESCE (ss.max_lanes, rtl.max_lanes)
                      END
                         max_lanes,
                      --coalesce(ss.max_lanes, rtl.max_lanes) max_lanes,
                      CASE
                         WHEN rtl.move_guides = 0 THEN 0
                         ELSE ss.allow_expand
                      END
                         allow_expand,
                      CASE
                         WHEN rtl.move_guides = 1
                              AND ss.allow_expand != rtl.move_guides
                         THEN
                            1
                         ELSE
                            0
                      END
                         allow_expand_ovr,
                      ss.old_rec_slot_width,
                      ss.x_coord,
                      ss.y_coord,
                      ss.z_coord,
                      ss.reserved_1,
                      ss.reserved_2,
                      ss.reserved_3,
                      ss.reserved_4,
                      ss.slot_type,
                      ss.travel_zone,
                      ss.travel_aisle,
                      CASE
                         WHEN ss.location_class IS NULL THEN 'A'
                         ELSE ss.location_class
                      END
                         location_class,
                      ss.slot_priority,
                      -- case when p_dec_adj <0 then  (coalesce(ss.depth, rtl.level_depth)/(to_number(v_mult))) else (coalesce(ss.depth, rtl.level_depth) * (to_number(v_mult))) end reach_dist, /* commented for SLOT-2689 old implementaion
                      -- SLOT-2689 new implementation Set Reach Distance as Smallest of Slot Depth or Rack Reach Distance
                      CASE
                         WHEN p_dec_adj < 0
                         THEN
                            CASE
                               WHEN ss.width > rtl.reach_dist
                               THEN
                                  (COALESCE (rtl.reach_dist, ss.width)
                                   / (TO_NUMBER (v_mult)))
                               ELSE
                                  (COALESCE (ss.width, rtl.reach_dist)
                                   / (TO_NUMBER (v_mult)))
                            END
                         ELSE
                            CASE
                               WHEN ss.width > rtl.reach_dist
                               THEN
                                  (COALESCE (rtl.reach_dist, ss.width)
                                   * (TO_NUMBER (v_mult)))
                               ELSE
                                  (COALESCE (ss.width, rtl.reach_dist)
                                   * (TO_NUMBER (v_mult)))
                            END
                      END
                         reach_dist,
                      CASE
                         WHEN (CASE
                                  WHEN p_dec_adj < 0
                                  THEN
                                     (COALESCE (ss.DEPTH, rtl.level_depth)
                                      / (TO_NUMBER (v_mult)))
                                  ELSE
                                     (COALESCE (ss.DEPTH, rtl.level_depth)
                                      * (TO_NUMBER (v_mult)))
                               END) > 0
                              AND (CASE
                                      WHEN p_dec_adj < 0
                                      THEN
                                         (COALESCE (ss.DEPTH,
                                                    rtl.level_depth)
                                          / (TO_NUMBER (v_mult)))
                                      ELSE
                                         (COALESCE (ss.DEPTH,
                                                    rtl.level_depth)
                                          * (TO_NUMBER (v_mult)))
                                   END) != rtl.reach_dist
                         THEN
                            1
                         ELSE
                            0
                      END
                         reach_dist_override,
                      CASE
                         WHEN rtl.move_guides = 1 AND ss.allow_expand = 1
                         THEN
                            rtl.move_guides_rgt
                         WHEN rtl.move_guides = 1 AND ss.allow_expand IS NULL
                         THEN
                            rtl.move_guides_rgt
                         ELSE
                            0
                      END
                         allow_expand_rgt,
                      CASE
                         WHEN rtl.move_guides = 1 AND ss.allow_expand = 1
                         THEN
                            rtl.move_guides_lft
                         WHEN rtl.move_guides = 1 AND ss.allow_expand IS NULL
                         THEN
                            rtl.move_guides_lft
                         ELSE
                            0
                      END
                         allow_expand_lft,
                      CASE
                         WHEN rtl.move_guides = 1
                              AND ss.allow_expand_rgt != rtl.move_guides_rgt
                         THEN
                            1
                         ELSE
                            0
                      END
                         allow_expand_rgt_ovr,
                      CASE
                         WHEN rtl.move_guides = 1
                              AND ss.allow_expand_lft != rtl.move_guides_lft
                         THEN
                            1
                         ELSE
                            0
                      END
                         allow_expand_lft_ovr
                 FROM    slot_stg ss
                      JOIN
                         rack_type_level rtl
                      ON rtl.rack_level_id = ss.rack_level_id
                         AND (ss.process_code IS NULL
                              OR (ss.process_code IS NOT NULL
                                  AND ss.process_code IN ('Z', 'B')))
                WHERE NOT EXISTS
                             (SELECT 1
                                FROM slot s
                               WHERE     s.dsp_slot = ss.dsp_slot
                                     AND s.whse_code = p_whse_code
                                     AND s.processed = 1)) iv
           ON (s.dsp_slot = iv.dsp_slot AND s.whse_code = p_whse_code)
   WHEN MATCHED
   THEN
      UPDATE SET
         s.rack_type = iv.rack_type,
         s.rack_level_id = iv.rack_level_id,
         s.area = iv.area,
         s.zone = iv.zone,
         s.aisle = iv.aisle,
         s.bay = iv.bay,
         s.lvl = iv.lvl,
         s.posn = iv.posn,
         s.ht = iv.ht,                                 /*s.width = iv.width,*/
         s.DEPTH = iv.DEPTH,
         s.wt_limit = iv.wt_limit,
         s.ht_override = iv.ht_override,
         /*s.width_override = iv.width_override,*/
         s.depth_override = iv.depth_override,
         s.max_ln_ovr = iv.max_ln_ovr,
         s.max_st_ovr = iv.max_st_ovr,
         s.max_sc_ovr = iv.max_sc_ovr,
         s.max_hc_ovr = iv.max_hc_ovr,
         s.max_lw_ovr = iv.max_lw_ovr,
         s.max_stack = iv.max_stack,
         s.max_side_clear = iv.max_side_clear,
         s.max_ht_clear = iv.max_ht_clear,
         s.max_lane_wt = iv.max_lane_wt,
         s.processed = 1,
         s.lock_reason = iv.lock_reason,
         s.locked = iv.locked,
         s.my_range = iv.my_range,
         s.wt_limit_override = iv.wt_limit_override,
         s.label_pos = iv.label_pos,
         s.label_pos_ovr = iv.label_pos_ovr,
         s.max_lanes = iv.max_lanes,
         s.my_sns = p_sns_id,
         s.allow_expand = iv.allow_expand,
         s.allow_expand_ovr = iv.allow_expand_ovr,
         s.x_coord = iv.x_coord,
         s.y_coord = iv.y_coord,
         s.z_coord = iv.z_coord,
         s.mod_date_time = SYSDATE,
         s.mod_user = p_user_id,
         s.slot_type = iv.slot_type,
         s.travel_zone = iv.travel_zone,
         s.travel_aisle = iv.travel_aisle,
         s.location_class = iv.location_class,
         s.slot_priority = iv.slot_priority,
         s.reach_dist = iv.reach_dist,
         s.reach_dist_override = iv.reach_dist_override,
         s.allow_expand_rgt = iv.allow_expand_rgt,
         s.allow_expand_lft = iv.allow_expand_lft,
         s.allow_expand_rgt_ovr = iv.allow_expand_rgt_ovr,
         s.allow_expand_lft_ovr = iv.allow_expand_lft_ovr
              WHERE (p_update_exist_recs = 1 OR p_update_delete = 1)
   WHEN NOT MATCHED
   THEN
      INSERT     (slot_id,
                  whse_code,
                  dsp_slot,
                  rack_type,
                  rack_level_id,
                  area,
                  zone,
                  aisle,
                  bay,
                  lvl,
                  posn,
                  ht,
                  width,
                  DEPTH,
                  wt_limit,
                  ht_override,
                  width_override,
                  depth_override,
                  max_ln_ovr,
                  max_st_ovr,
                  max_sc_ovr,
                  max_hc_ovr,
                  max_lw_ovr,
                  max_stack,
                  max_side_clear,
                  max_ht_clear,
                  max_lane_wt,
                  processed,
                  lock_reason,
                  locked,
                  wt_limit_override,
                  label_pos,
                  label_pos_ovr,
                  max_lanes,
                  my_sns,
                  allow_expand,
                  allow_expand_ovr,
                  old_rec_slot_width,
                  side_of_aisle,
                  x_coord,
                  y_coord,
                  z_coord,
                  reserved_1,
                  reserved_2,
                  reserved_3,
                  reserved_4,
                  create_date_time,
                  mod_date_time,
                  mod_user,
                  slot_type,
                  travel_zone,
                  travel_aisle,
                  location_class,
                  slot_priority,
                  reach_dist,
                  reach_dist_override,
                  allow_expand_rgt,
                  allow_expand_lft,
                  allow_expand_rgt_ovr,
                  allow_expand_lft_ovr,
                  my_range)
          VALUES (get_unique_ids (v_rec_slot_type),
                  p_whse_code,
                  iv.dsp_slot,
                  iv.rack_type,
                  iv.rack_level_id,
                  iv.area,
                  iv.zone,
                  iv.aisle,
                  iv.bay,
                  iv.lvl,
                  iv.posn,
                  iv.ht,
                  iv.width,
                  iv.DEPTH,
                  iv.wt_limit,
                  iv.ht_override,                        /*iv.width_override*/
                  0,
                  iv.depth_override,
                  iv.max_ln_ovr,
                  iv.max_st_ovr,
                  iv.max_sc_ovr,
                  iv.max_hc_ovr,
                  iv.max_lw_ovr,
                  iv.max_stack,
                  iv.max_side_clear,
                  iv.max_ht_clear,
                  iv.max_lane_wt,
                  1,
                  iv.lock_reason,
                  iv.locked,
                  iv.wt_limit_override,
                  iv.label_pos,
                  iv.label_pos_ovr,
                  iv.max_lanes,
                  p_sns_id,
                  iv.allow_expand,
                  iv.allow_expand_ovr,
                  iv.old_rec_slot_width,
                  0,
                  iv.x_coord,
                  iv.y_coord,
                  iv.z_coord,
                  iv.reserved_1,
                  iv.reserved_2,
                  iv.reserved_3,
                  iv.reserved_4,
                  SYSDATE,
                  SYSDATE,
                  p_user_id,
                  iv.slot_type,
                  iv.travel_zone,
                  iv.travel_aisle,
                  iv.location_class,
                  iv.slot_priority,
                  iv.reach_dist,
                  iv.reach_dist_override,
                  iv.allow_expand_rgt,
                  iv.allow_expand_lft,
                  iv.allow_expand_rgt_ovr,
                  iv.allow_expand_lft_ovr,
                  iv.my_range);


   -- we will keep this in C++ for now
   -- update range id; there should be no overlaps
   /*    update slot s
       set s.my_range =
       (
           select td.node_id
           from tree_dtl td
           where td.sns_id = p_sns_id
               and
               (
                   (td.begin_area is null or s.area between td.begin_area and td.end_area)
                   and (td.area_incr = 1 or so_imp_is_valid_incr('A', s.area, td.begin_area, td.area_incr) = 1)
               )
               and
               (
                   (td.begin_zone is null or s.zone between td.begin_zone and td.end_zone)
                   and (td.zone_incr = 1 or so_imp_is_valid_incr('Z', s.zone, td.begin_zone, td.zone_incr) = 1)
               )
               and
               (
                   (td.begin_aisle is null or s.aisle between td.begin_aisle and td.end_aisle)
                   and (td.aisle_incr = 1 or so_imp_is_valid_incr('AI', s.aisle, td.begin_aisle, td.aisle_incr) = 1)
               )
               and
               (
                   (td.begin_bay is null or s.bay between td.begin_bay and td.end_bay)
                   and (td.bay_incr = 1 or so_imp_is_valid_incr('B', s.bay, td.begin_bay, td.bay_incr)  = 1)
               )
               and
               (
                   (td.begin_lvl is null or s.lvl between td.begin_lvl and td.end_lvl)
                   and (td.lvl_incr = 1 or so_imp_is_valid_incr('L', s.lvl, td.begin_lvl, td.lvl_incr) = 1)
               )
               and
               (
                   (td.begin_posn is null or s.posn between td.begin_posn and td.end_posn)
                   and (td.posn_incr = 1 or so_imp_is_valid_incr('P', s.posn, td.begin_posn, td.posn_incr) = 1)
               )
       );
   */

   INSERT ALL
   WHEN 1 = 1
   THEN
        INTO slot_item (slotitem_id,
                        mod_user,
                        create_date_time,
                        mod_date_time,
                        slot_id,
                        ign_for_reslot,
                        cur_orientation,
                        score,
                        pallete_pattern,
                        slot_unit,
                        bin_unit,
                        primary_move,
                        opt_fluid_vol,
                        est_mvmt_can_borrow,
                        est_hits_can_borrow,
                        delete_mult)
      VALUES (get_unique_ids (v_rec_slotitem_type),
              p_user_id,
              SYSDATE,
              SYSDATE,
              slot_id,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              -999.99,
              0,
              0,
              0)
      SELECT s.slot_id, s.dsp_slot
        FROM slot s
       WHERE     s.whse_code = p_whse_code
             AND s.processed = 1
             AND NOT EXISTS
                    (SELECT 1
                       FROM slot_item si
                      WHERE si.slot_id = s.slot_id)
             AND EXISTS
                    (SELECT 1
                       FROM slot_stg ss
                      WHERE ss.dsp_slot = s.dsp_slot);

   INSERT ALL
   WHEN (v_no_si_msg_sev >= p_log_level)
   THEN
        INTO exceptions (ex_id,
                         whse_code,
                         slot,
                         create_date_time,
                         mod_date_time,
                         mod_user,
                         msg_number,
                         descript)
      VALUES (get_unique_ids (v_rec_exce_type),
              p_whse_code,
              dsp_slot,
              SYSDATE,
              SYSDATE,
              p_user_id,
              v_no_si_msg_nbr,
              v_no_si_msg)
      SELECT s.slot_id, s.dsp_slot
        FROM slot s
       WHERE     s.whse_code = p_whse_code
             AND s.processed = 1
             AND NOT EXISTS
                    (SELECT 1
                       FROM slot_item si
                      WHERE si.slot_id = s.slot_id)
             AND EXISTS
                    (SELECT 1
                       FROM slot_stg ss
                      WHERE ss.dsp_slot = s.dsp_slot);

   --SLOT-2714 When user gives SlotWidth as blank then use Width from Slot Table instead of Slot_stg table
   /* merge into slot_item si
    using
    (
        select s.slot_id, case when( p_dec_adj >0) then (ss.width * v_mult) else (ss.width/v_mult) end width
        from slot_stg ss
        join slot s on s.dsp_slot = ss.dsp_slot and s.whse_code = p_whse_code
    ) iv on (iv.slot_id = si.slot_id)
    when matched then
    update set si.slot_width = iv.width;*/
   MERGE INTO slot_item si
        USING (SELECT s.slot_id,
                      CASE
                         WHEN (p_dec_adj > 0)
                         THEN
                            CASE
                               WHEN ss.width IS NOT NULL
                               THEN
                                  (ss.width * v_mult)
                               ELSE
                                  (s.width * v_mult)
                            END
                         ELSE
                            CASE
                               WHEN ss.width IS NOT NULL
                               THEN
                                  (ss.width / v_mult)
                               ELSE
                                  (s.width / v_mult)
                            END
                      END
                         width
                 FROM    slot_stg ss
                      JOIN
                         slot s
                      ON s.dsp_slot = ss.dsp_slot
                         AND s.whse_code = p_whse_code) iv
           ON (iv.slot_id = si.slot_id)
   WHEN MATCHED
   THEN
      UPDATE SET si.slot_width = iv.width;
END;

/
--------------------------------------------------------
--  DDL for Procedure SO_IMP_DELETE_SLOTS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SO_IMP_DELETE_SLOTS 
(
    p_whse_code       in whprefs.whse_code%type,
    p_user_id         in slot.mod_user%type,
    p_msg_lang        in so_message_master.msg_language%type,
    p_log_level       in whse_import_prefs.log_level%type
)
as
    v_del_msg_nbr           so_message_master.msg_number%type:= 456;
    v_del_msg               so_message_master.msg_text%type;
    v_del_msg_sev           so_message_master.severity%type;
    v_results_msg_nbr       so_message_master.msg_number%type:= 471;
    v_results_msg           so_message_master.msg_text%type;
    v_results_msg_sev       so_message_master.severity%type;

    v_rec_exce_type varchar2(100);

begin
v_rec_exce_type := 'exceptions';
    select mm.msg_text, mm.severity
    into v_del_msg, v_del_msg_sev
    from so_message_master mm
    where mm.msg_number = v_del_msg_nbr and mm.msg_language = p_msg_lang;

    select mm.msg_text, mm.severity
    into v_results_msg, v_results_msg_sev
    from so_message_master mm
    where mm.msg_number = v_results_msg_nbr and mm.msg_language = p_msg_lang;

    -- such slots should not be deleted
    update slot s
    set s.processed = 1, s.mod_user = p_user_id, s.mod_date_time = sysdate
    where s.whse_code = p_whse_code
        and exists
        (
            select 1
            from wa_detail wd
            where wd.slot_id = s.slot_id
        )
        and not exists
        (
            select 1
            from slot_stg ss
            where ss.dsp_slot = s.dsp_slot
        );
-- todo: need to review dependencies below, in the context of delete dml's

-- check: split off unprocessed data to a tmp table?
    delete from shelf_slot_xref ssx
    where exists
    (
        select 1
        from slot s
        where s.slot_id = ssx.slot_id and s.processed = 0
    );

    -- these are generic cleanup statements - not tied to unprocessed slots
    delete from item_history ih
    where exists
    (
        select 1
        from slot_item si
        where si.slotitem_id = ih.slotitem_id and si.sku_id is null
    );

    delete from slot_item_score sis
    where exists
    (
        select 1
        from slot_item si
        where si.slotitem_id = sis.slotitem_id and si.sku_id is null
    );

    delete from slot_item si
    where si.sku_id is null
        and exists
        (
            select 1
            from slot s
            where s.slot_id = si.slot_id and s.processed = 0
        );

    delete from slot_item si
    where si.sku_id is null and si.slot_id is null;

    update slot_item si
    set si.slot_id = null
    where si.sku_id is not null
        and exists
        (
            select 1
            from slot s
            where s.slot_id = si.slot_id and s.processed = 0
        );

    insert into tmp_hn_hdr_id
    (
        hn_hdr_id
    )
    select distinct hrd.hn_hdr_id
    from haves_results_dtl hrd
    where exists
    (
        select 1
        from slot s
        where s.slot_id = hrd.slot_id and s.processed = 0
    );

    delete from haves_results_dtl hrd
    where exists
    (
        select 1
        from tmp_hn_hdr_id t
        where t.hn_hdr_id = hrd.hn_hdr_id
    );

    delete from needs_results_dtl nrd
    where exists
    (
        select 1
        from tmp_hn_hdr_id t
        where t.hn_hdr_id = nrd.hn_hdr_id
    );

    if (v_results_msg_sev >= p_log_level)
    then

        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        select get_unique_ids(v_rec_exce_type), p_whse_code, null, sysdate, sysdate,
            p_user_id, v_results_msg_nbr, replace(v_results_msg, '%s', iv.hn_run_name)
        from
        (
            select distinct hrh.hn_run_name
            from hn_results_hdr hrh
            where exists
            (
                select 1
                from tmp_hn_hdr_id t
                where t.hn_hdr_id = hrh.hn_hdr_id
            )
        ) iv;
    end if;

    delete from hn_results_hdr hrh
    where exists
    (
        select 1
        from tmp_hn_hdr_id t
        where t.hn_hdr_id = hrh.hn_hdr_id
    );

    delete from hn_failure hf
    where exists
    (
        select 1
        from tmp_hn_hdr_id t
        where t.hn_hdr_id = hf.hn_hdr_id
    );

    delete from hn_ranges hr
    where exists
    (
        select 1
        from tmp_hn_hdr_id t
        where t.hn_hdr_id = hr.hn_hdr_id
    );

    delete from hn_rack_types hrt
    where exists
    (
        select 1
        from tmp_hn_hdr_id t
        where t.hn_hdr_id = hrt.hn_hdr_id
    );

    -- relink the two linked lists by removing references to deleted slots
    update slot s
    set s.left_slot =
    (
        select min(s3.slot_id) keep(dense_rank first order by level)
        from slot s3
        where s3.processed = 1
        start with s3.slot_id = s.left_slot
        connect by nocycle slot_id = prior left_slot
    )
    where s.processed = 1
        and exists
        (
            select 1
            from slot s2
            where s2.slot_id = s.left_slot and s2.processed = 0
        );

    update slot s
    set s.right_slot =
    (
        select min(s3.slot_id) keep(dense_rank first order by level)
        from slot s3
        where s3.processed = 1
        start with s3.slot_id = s.right_slot
        connect by nocycle slot_id = prior right_slot
    )
    where s.processed = 1
        and exists
        (
            select 1
            from slot s2
            where s2.slot_id = s.right_slot and s2.processed = 0
        );

    -- remove slots not a part of the current import
    update slot s set s.left_slot = null, s.right_slot = null
    where s.processed = 0 and s.whse_code = p_whse_code;

    if (v_del_msg_sev >= p_log_level)
    then

        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        select get_unique_ids(v_rec_exce_type), p_whse_code, s.dsp_slot, sysdate, sysdate,
            p_user_id, v_del_msg_nbr, replace(v_del_msg, '%s', s.dsp_slot)
        from slot s
        where s.processed = 0 and s.whse_code = p_whse_code;
    end if;

    delete from slot s
    where s.processed = 0 and s.whse_code = p_whse_code;

    -- slot_stg is truncated prior to calling this import in the next run
end;

/
--------------------------------------------------------
--  DDL for Procedure SO_IMP_SLOT_DATA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SO_IMP_SLOT_DATA 
(
    p_user_id         in slot.mod_user%type,
    p_whse_code       in whprefs.whse_code%type,
    p_format_id       in import_formats.format_id%type
)
as
    v_dft_rack_type       import_format_prefs.dft_rack_type%type;
    v_dec_adj             import_format_prefs.dec_adj%type;
    v_sns_id              sns.sns_id%type;
    v_is_real_time        whprefs.real_time%type := 0;
    v_update_delete       import_format_prefs.update_delete%type;
    v_level_mask          sns.level_mask%type;
    v_dft_rt_name         rack_type.rt_name%type;
    v_default_blank       import_format_prefs.default_blank%type;
    v_default_zero        import_format_prefs.default_zero%type;
    v_log_level           whse_import_prefs.log_level%type;
    v_msg_lang            so_message_master.msg_language%type;
    v_exists_msg_nbr      so_message_master.msg_number%type:= 461;
    v_exists_msg          so_message_master.msg_text%type;
    v_exists_msg_sev      so_message_master.severity%type;
     v_update_msg_nbr      so_message_master.msg_number%type:= 467;
    v_update_msg          so_message_master.msg_text%type;
    v_update_msg_sev      so_message_master.severity%type;
     v_add_msg_nbr      so_message_master.msg_number%type:= 453;
    v_add_msg          so_message_master.msg_text%type;
    v_add_msg_sev      so_message_master.severity%type;
    v_update_exist_recs import_format_prefs.update_exist_recs%type;
    v_rec_exce_type varchar2(100);
    v_path_filename import_format_prefs.path_filename%type;

begin
v_rec_exce_type := 'exceptions';
  select w.real_time  into v_is_real_time  from whprefs w  where w.whse_code = p_whse_code;


   select ifp.update_delete, ifp.dft_sns, ifp.dft_rack_type, rt.rt_name, coalesce(ifp.dec_adj, 1),
           s.level_mask, ifp.default_blank, ifp.default_zero, ifp.update_exist_recs,path_filename
   into v_update_delete, v_sns_id, v_dft_rack_type, v_dft_rt_name, v_dec_adj,
        v_level_mask, v_default_blank, v_default_zero, v_update_exist_recs,v_path_filename
   from import_format_prefs ifp  join sns s on s.sns_id = ifp.dft_sns left join rack_type rt on rt.rack_type = ifp.dft_rack_type
   where ifp.whse_code = p_whse_code and ifp.format_id = p_format_id;

-- todo: account for *df and the input whse code
    select scd.business_value  into v_msg_lang
    from system_code_dtl scd
    where scd.whse_code in ('*df',p_whse_code) --and scd.code_value = ?
        and exists
        (
            select 1
            from system_code sc
            where sc.sys_code_id = scd.sys_code_id and sc.code_type = '503'
                and sc.cust_id = 'MANH'
        );


    -- log levels are 90 (errors only), 99 (no logging), 50 (warnings),
    -- 10 (everything); default level is error
    select coalesce(wip.log_level, '90') into v_log_level
    from whse_import_prefs wip
    where wip.whse_code = p_whse_code;

    if (v_is_real_time = 0 and v_update_delete = 1)
    then
          -- use dml only for initial testing
        delete from move_list where move_hdr_id in (select move_hdr_id from move_list_hdr where whse_code = p_whse_code);
        delete from  move_list_hdr where whse_code = p_whse_code;
        delete from haves_needs where whse_code = p_whse_code;

    end if;

    update slot s  set s.processed = 0 where s.whse_code = p_whse_code;

    -- stg is cleaned out and repopulated by C++ at the start of each import run
    -- each run (and all of stg) is specific to one whse_code
    if (v_is_real_time = 0)
    then
        -- delete dupes for non-real time slotting
        delete from slot_stg ss
        where ss.slot_stg_id in
        (
            select iv.slot_stg_id
            from
            (
                select ss.slot_stg_id, row_number() over(partition by ss.dsp_slot
                    order by ss.slot_stg_id desc) id
                from slot_stg ss
            ) iv
            where iv.id > 1
        );
    end if;

    -- per Sibi, dsp_slot is unique within a whse, slot_id is never provided,
    -- one import run is specific to one whse_code
    -- per Sibi, slot components are populated in slot_stg prior to this import
-- check: ss.slot_id is only used in one downstream validation - we may not need to calculate it
    merge into slot_stg ss
    using
    (
        select distinct ss.slot_stg_id, s.slot_id, rlm.rack_level_id,
           coalesce(ss.rt_name, existing_rt.rt_name, v_dft_rt_name) rt_name,
            final_rt.rack_type rack_type
        from slot_stg ss
        left join slot s on s.dsp_slot = ss.dsp_slot and s.whse_code = p_whse_code
        left join rack_type existing_rt on existing_rt.whse_code = s.whse_code
            and existing_rt.rack_type = s.rack_type
        right join rack_type final_rt on final_rt.rt_name = coalesce(ss.rt_name, existing_rt.rt_name, v_dft_rt_name)
            and final_rt.whse_code = p_whse_code
        left join rack_level_map rlm on rlm.rack_type = final_rt.rack_type
            and rlm.component_type = (case when v_level_mask is null then 5 else 4 end)
            and rlm.component_val = (case when v_level_mask is null then ss.posn else ss.lvl end)
    ) iv on (iv.slot_stg_id = ss.slot_stg_id)
    when matched then
    update set ss.slot_id = iv.slot_id, ss.rt_name = iv.rt_name,
       ss.rack_type = coalesce(iv.rack_type, -1),
        ss.rack_level_id = iv.rack_level_id;

    if (v_update_exist_recs = 0 and v_is_real_time = 0 and v_update_delete=0 )
    then
        select mm.msg_text, mm.severity
        into v_exists_msg, v_exists_msg_sev
        from so_message_master mm
        where mm.msg_number = v_exists_msg_nbr and mm.msg_language = v_msg_lang;

        if (v_exists_msg_sev >= v_log_level)
        then

            insert into exceptions
            (
                ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
                msg_number, descript
            )
            select get_unique_ids(v_rec_exce_type), p_whse_code, ss.dsp_slot, sysdate, sysdate,
                p_user_id, v_exists_msg_nbr, v_exists_msg
            from slot_stg ss
            where exists
            (
                select 1
                from slot s
                where s.dsp_slot = ss.dsp_slot and s.whse_code = p_whse_code
            );
        end if;

        -- skip data that already exists
        update slot s
        set s.processed = 1, s.mod_user = p_user_id, s.mod_date_time = sysdate
        where s.whse_code = p_whse_code
            and exists
            (
                select 1
                from slot_stg ss
                where ss.dsp_slot = s.dsp_slot
            );

        update slot_stg ss set process_code='D' where ss.dsp_slot in (select s.dsp_slot from slot s where s.whse_code = p_whse_code and s.processed= 1);
       /* delete from slot_stg ss
        where exists
        (
            select 1
            from slot s
            where s.dsp_slot = ss.dsp_slot and s.whse_code = p_whse_code
        );*/
    end if;



    so_imp_validate_slot_data(p_format_id, v_log_level, p_whse_code, p_user_id,
        v_default_blank, v_default_zero, v_msg_lang,v_sns_id);

    so_imp_complete_slot_import(p_user_id, p_whse_code, v_sns_id, v_dec_adj,
        v_update_delete, v_msg_lang, v_log_level,v_update_exist_recs);

    if (v_is_real_time = 1 and v_update_delete = 1)
    then
       --v_update_delete := 0;
       so_imp_delete_slots(p_whse_code, p_user_id, v_msg_lang, v_log_level);
    end if;

   -- if(v_update_exist_recs = 1) then
     select mm.msg_text, mm.severity
        into v_update_msg, v_update_msg_sev
        from so_message_master mm
        where mm.msg_number = v_update_msg_nbr and mm.msg_language = v_msg_lang;
         select mm.msg_text, mm.severity
        into v_add_msg, v_add_msg_sev
        from so_message_master mm
        where mm.msg_number = v_add_msg_nbr and mm.msg_language = v_msg_lang;

         insert into exceptions
            (
                ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
                msg_number, descript
            )
            select get_unique_ids(v_rec_exce_type), p_whse_code, s.dsp_slot, sysdate, sysdate,
                p_user_id, v_update_msg_nbr, replace(replace(v_update_msg, '%s1', dsp_slot), '%s2', v_path_filename)
            from slot s
            where s.whse_code = p_whse_code and s.create_date_time<>s.mod_date_time  and s.dsp_slot in (select ss.dsp_slot from slot_stg ss where (ss.process_code is null or (ss.process_code is not null and ss.process_code in ('Z','B'))));

             insert into exceptions
            (
                ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
                msg_number, descript
            )
            select get_unique_ids(v_rec_exce_type), p_whse_code, s.dsp_slot, sysdate, sysdate,
                p_user_id, v_add_msg_nbr, v_add_msg
            from slot s
            where s.whse_code = p_whse_code and s.create_date_time=s.mod_date_time and s.dsp_slot in (select dsp_slot from slot_stg ss where (ss.process_code is null or (ss.process_code is not null and ss.process_code  in ('Z','B'))));
    --end if;

end;

/
--------------------------------------------------------
--  DDL for Procedure SO_IMP_VALIDATE_SLOT_DATA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SO_IMP_VALIDATE_SLOT_DATA 
(
    p_format_id       in import_formats.format_id%type,
    p_log_level       in whse_import_prefs.log_level%type,
    p_whse_code       in whprefs.whse_code%type,
    p_user_id         in slot.mod_user%type,
    p_default_blank   in import_format_prefs.default_blank%type,
    p_default_zero    in import_format_prefs.default_zero%type,
    p_msg_lang        in so_message_master.msg_language%type,
    p_sns_id          in tree_dtl.sns_id%type
)
as
-- todo: sibi to create a new msg in lieu of 464
    v_dft_msg_nbr           so_message_master.msg_number%type:= 464;
    v_dft_msg               so_message_master.msg_text%type;
    v_dft_msg_sev           so_message_master.severity%type;
    v_dft_msg_nbr_z         so_message_master.msg_number%type:= 465;
    v_dft_msg_z               so_message_master.msg_text%type;
    v_dft_msg_sev_z           so_message_master.severity%type;
    v_sns_msg_nbr           so_message_master.msg_number%type:= 454;
    v_sns_msg               so_message_master.msg_text%type;
    v_sns_msg_sev           so_message_master.severity%type;

    v_analysis_msg_nbr      so_message_master.msg_number%type:= 458;
    v_analysis_msg          so_message_master.msg_text%type;
    v_analysis_msg_sev      so_message_master.severity%type;
    v_wrong_rt_msg_nbr      so_message_master.msg_number%type:= 459;
    v_wrong_rt_msg          so_message_master.msg_text%type;
    v_wrong_rt_msg_sev      so_message_master.severity%type;
    v_in_wa_msg_nbr         so_message_master.msg_number%type:= 152;
    v_in_wa_msg             so_message_master.msg_text%type;
    v_in_wa_msg_sev         so_message_master.severity%type;
    v_notin_wa_msg_nbr         so_message_master.msg_number%type:= 152;
    v_notin_wa_msg             so_message_master.msg_text%type;
    v_notin_wa_msg_sev         so_message_master.severity%type;
    v_no_rt_msg_nbr         so_message_master.msg_number%type:= 455;
    v_no_rt_msg             so_message_master.msg_text%type;
    v_no_rt_msg_sev         so_message_master.severity%type;
    v_inv_lc_msg_nbr        so_message_master.msg_number%type:= 463;
    v_inv_lc_msg            so_message_master.msg_text%type;
    v_inv_lc_msg_sev        so_message_master.severity%type;
-- todo: change %s twice to %s1 and %s2
    v_no_rl_msg_nbr         so_message_master.msg_number%type:= 452;
    v_no_rl_msg             so_message_master.msg_text%type;
    v_no_rl_msg_sev         so_message_master.severity%type;
    v_dft_ht                slot_stg.ht%type;
    v_dft_width             slot_stg.width%type;
    v_dft_depth             slot_stg.depth%type;
    v_dft_max_ht_clear      slot_stg.max_ht_clear%type;
    v_dft_max_side_clear    slot_stg.max_side_clear%type;
    v_dft_locked            varchar2(1);
    v_dft_locked1           slot_stg.locked%type;
    v_dft_lock_reason       slot_stg.lock_reason%type;
    v_dft_max_stack         slot_stg.max_stack%type;
    v_dft_max_lanes         slot_stg.max_lanes%type;
    v_dft_allow_expand       varchar2(1);
    v_dft_allow_expand1      slot_stg.allow_expand%type;
    v_dft_label_pos         slot_stg.label_pos%type;
    v_dft_wt_limit          slot_stg.wt_limit%type;
    v_dft_max_lane_wt       slot_stg.max_lane_wt%type;
    v_dft_x_coord           slot_stg.x_coord%type;
    v_dft_y_coord           slot_stg.y_coord%type;
    v_dft_z_coord           slot_stg.z_coord%type;
    v_dft_slot_type         slot_stg.slot_type%type;
    v_dft_travel_zone       slot_stg.travel_zone%type;
    v_dft_travel_aisle      slot_stg.travel_aisle%type;
    v_dft_location_class    slot_stg.location_class%type;
    v_dft_slot_priority     slot_stg.slot_priority%type;
    v_area_values               sns.area_values%type;
    v_zone_values               sns.zone_values%type;
    v_aisle_values              sns.aisle_values%type;
    v_bay_values                sns.bay_values%type;
    v_level_values              sns.level_values%type;
    v_pos_values                sns.pos_values%type;
    v_except_set number;
    varea number(10);
    vzone number(10);
    vaisle number(10);
    vbay number(10);
    vlvl number(10);
    vpos number(10);
    v_sns_max sns_max_min.sns_max%type;
    v_sns_min sns_max_min.sns_min%type;
    v_dft_sns_name sns.name%type;
    v_rec_exce_type varchar2(100);
    v_ex_id number(19);
    v_ss_dsp_slot slot_stg.dsp_slot%type;
    v_ss_ht slot_stg.ht%type;
    v_ss_width slot_stg.width%type;
    v_ss_depth slot_stg.depth%type;
    v_ss_max_ht_clear slot_stg.max_ht_clear%type;
    v_ss_locked slot_stg.locked%type;
    v_ss_max_side_clear slot_stg.max_side_clear%type;
    v_ss_lock_reason slot_stg.lock_reason%type;
    v_ss_max_stack slot_stg.max_stack%type;
    v_ss_max_lanes slot_stg.max_lanes%type;
    v_ss_allow_expand slot_stg.allow_expand%type;
    v_ss_label_pos slot_stg.label_pos%type;
    v_ss_wt_limit slot_stg.wt_limit%type;
    v_ss_max_lane_wt slot_stg.max_lane_wt%type;
    v_ss_x_coord slot_stg.x_coord%type;
    v_ss_y_coord slot_stg.y_coord%type;
    v_ss_z_coord slot_stg.z_coord%type;
    v_ss_slot_type slot_stg.slot_type%type;
    v_ss_travel_zone slot_stg.travel_zone%type;
    v_ss_travel_aisle slot_stg.travel_aisle%type;
    v_ss_location_class slot_stg.location_class%type;
    v_ss_slot_priority slot_stg.slot_priority%type;
    v_ss_rack_type slot_stg.rack_type%type;
    v_ss_rack_name slot_stg.rt_name%type;
    v_ss_rack_level_id slot_stg.rack_level_id%type;
    v_ss_slot_id slot_stg.slot_id%type;
    v_rt_function_type rack_type.function_type%type;
    v_ss_area slot_stg.area%type;
    v_ss_zone slot_stg.zone%type;
    v_ss_aisle slot_stg.aisle%type;
    v_ss_bay slot_stg.bay%type;
    v_ss_lvl slot_stg.lvl%type;
    v_ss_posn slot_stg.posn%type;
    v_ss_process_code slot_stg.process_code%type;
    v_count number(10);
       v_SQLString          VARCHAR2 (32000);
    vtree_count number(10);
    vtree_id number(10);/*SLOT-2772*/
    cursor slotStageCursor is select ss.dsp_slot, ss.ht, ss.width, ss.depth, ss.max_ht_clear, ss.locked,
        ss.max_side_clear, ss.lock_reason, ss.max_stack, ss.max_lanes,
        ss.allow_expand, ss.label_pos, ss.wt_limit, ss.max_lane_wt, ss.x_coord,
        ss.y_coord, ss.z_coord, ss.slot_type, ss.travel_zone, ss.travel_aisle,
        ss.location_class, ss.slot_priority, ss.rack_type, ss.rt_name, ss.rack_level_id,
        ss.slot_id, rt.function_type,ss.area,ss.zone,ss.aisle,ss.bay,ss.lvl,ss.posn, process_code
    from slot_stg ss
    left join rack_type rt on rt.rack_type = ss.rack_type;


begin
v_rec_exce_type := 'exceptions';
v_count := 0;
vtree_count := 0;
    -- log validation exceptions
-- todo: for ALL validations below, account for *df and the input whse code

    getmaxrange(p_sns_id);
    select sns.area_values, sns.zone_values, sns.aisle_values, sns.bay_values, sns.level_values, sns.pos_values into v_area_values,v_zone_values,v_aisle_values,v_bay_values,v_level_values,v_pos_values  from sns sns where sns.sns_id = p_sns_id;

    select name into v_dft_sns_name from sns where sns_id = p_sns_id;
    varea := instr(v_area_values, '-');
    vzone := instr(v_zone_values, '-');
    vaisle := instr(v_aisle_values, '-');
    vbay := instr(v_bay_values, '-');
    vlvl := instr(v_level_values, '-');
    vpos := instr(v_pos_values, '-');

    select sm.sns_max,sm.sns_min into v_sns_max , v_sns_min from sns_max_min sm where sm.sns_id = p_sns_id;

     select mm.msg_text, mm.severity
    into v_sns_msg, v_sns_msg_sev
    from so_message_master mm
    where mm.msg_number = v_sns_msg_nbr and mm.msg_language = p_msg_lang;


    select mm.msg_text, mm.severity
    into v_dft_msg, v_dft_msg_sev
    from so_message_master mm
    where mm.msg_number = v_dft_msg_nbr and mm.msg_language = p_msg_lang;

      select mm.msg_text, mm.severity
    into v_dft_msg_z, v_dft_msg_sev_z
    from so_message_master mm
    where mm.msg_number = v_dft_msg_nbr_z and mm.msg_language = p_msg_lang;

    select mm.msg_text, mm.severity
    into v_analysis_msg, v_analysis_msg_sev
    from so_message_master mm
    where mm.msg_number = v_analysis_msg_nbr and mm.msg_language = p_msg_lang;

    select mm.msg_text, mm.severity
    into v_in_wa_msg, v_in_wa_msg_sev
    from so_message_master mm
    where mm.msg_number = v_in_wa_msg_nbr and mm.msg_language = p_msg_lang;

    select mm.msg_text, mm.severity
    into v_notin_wa_msg, v_notin_wa_msg_sev
    from so_message_master mm
    where mm.msg_number = v_notin_wa_msg_nbr and mm.msg_language = p_msg_lang;

    select mm.msg_text, mm.severity
    into v_no_rt_msg, v_no_rt_msg_sev
    from so_message_master mm
    where mm.msg_number = v_no_rt_msg_nbr and mm.msg_language = p_msg_lang;

    select mm.msg_text, mm.severity
    into v_no_rl_msg, v_no_rl_msg_sev
    from so_message_master mm
    where mm.msg_number = v_no_rl_msg_nbr and mm.msg_language = p_msg_lang;

    select mm.msg_text, mm.severity
    into v_inv_lc_msg, v_inv_lc_msg_sev
    from so_message_master mm
    where mm.msg_number = v_inv_lc_msg_nbr and mm.msg_language = p_msg_lang;

	--SLOT-2836 - Control returning from here in case of None causing my_range to be NULL
    /*if (v_dft_msg_sev < p_log_level and v_no_rt_msg_sev < p_log_level
        and v_no_rl_msg_sev < p_log_level and v_inv_lc_msg_sev < p_log_level)
    then
        return;
    end if;*/

    -- pick up the configured defaults
-- todo: remove this and replace with looking at new flag on stg
    -- the 'protected' field is not used in the slot import
    select sox.dft_value  into v_dft_ht from stg_offset_xref sox where sox.format_id = p_format_id and sox.stg_col_name = 'HT';

    select sox.dft_value  into v_dft_width  from stg_offset_xref sox where sox.format_id = p_format_id and sox.stg_col_name = 'WIDTH';

    select sox.dft_value  into v_dft_depth  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'DEPTH';

    select sox.dft_value  into v_dft_max_ht_clear  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'MAX_HT_CLEAR';

    select sox.dft_value  into v_dft_max_side_clear  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'MAX_SIDE_CLEAR';

    select sox.dft_value into v_dft_locked  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'LOCKED';

    select case when (sox.dft_value ='Y') then 1 else 0 end  into v_dft_locked1  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'LOCKED';

    select sox.dft_value  into v_dft_lock_reason  from stg_offset_xref sox   where sox.format_id = p_format_id and sox.stg_col_name = 'LOCK_REASON';

    select sox.dft_value into v_dft_max_stack  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'MAX_STACK';

    select sox.dft_value  into v_dft_max_lanes  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'MAX_LANES';

    select  sox.dft_value    into v_dft_allow_expand  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'ALLOW_EXPAND';

    select  case when (sox.dft_value ='Y') then 1 else 0 end   into v_dft_allow_expand1  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'ALLOW_EXPAND';

    select sox.dft_value  into v_dft_label_pos  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'LABEL_POS';

    select sox.dft_value  into v_dft_wt_limit  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'WT_LIMIT';

    select sox.dft_value  into v_dft_max_lane_wt  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'MAX_LANE_WT';

    select sox.dft_value  into v_dft_x_coord  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'X_COORD';

    select sox.dft_value  into v_dft_y_coord  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'Y_COORD';

    select sox.dft_value  into v_dft_z_coord  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'Z_COORD';

    select sox.dft_value into v_dft_slot_type from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'SLOT_TYPE';

    select sox.dft_value  into v_dft_travel_zone  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'TRAVEL_ZONE';

    select sox.dft_value  into v_dft_travel_aisle  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'TRAVEL_AISLE';

    select sox.dft_value  into v_dft_location_class  from stg_offset_xref sox   where sox.format_id = p_format_id and sox.stg_col_name = 'LOCATION_CLASS';

    select sox.dft_value   into v_dft_slot_priority  from stg_offset_xref sox  where sox.format_id = p_format_id and sox.stg_col_name = 'SLOT_PRIORITY';

     open slotStageCursor;
     FETCH slotStageCursor INTO v_ss_dsp_slot, v_ss_ht, v_ss_width , v_ss_depth, v_ss_max_ht_clear , v_ss_locked , v_ss_max_side_clear ,   v_ss_lock_reason ,
    v_ss_max_stack ,  v_ss_max_lanes ,v_ss_allow_expand , v_ss_label_pos,  v_ss_wt_limit ,v_ss_max_lane_wt , v_ss_x_coord , v_ss_y_coord , v_ss_z_coord,
    v_ss_slot_type , v_ss_travel_zone,  v_ss_travel_aisle , v_ss_location_class , v_ss_slot_priority ,  v_ss_rack_type , v_ss_rack_name, v_ss_rack_level_id , v_ss_slot_id,
    v_rt_function_type ,  v_ss_area ,  v_ss_zone ,  v_ss_aisle ,  v_ss_bay ,  v_ss_lvl ,   v_ss_posn, v_ss_process_code ;
   WHILE (slotStageCursor%FOUND) LOOP-- may be able to have sibi set defaults and a flag to say it has been defaulted; and read the flag instead to log exceptions

--SLOT-2772
--Fetch the tree id using whse_code and then associated it to my_range of slot_stg
select tree_id into vtree_id from slot_list_tree where whse_code = p_whse_code and tree_name = 'Warehouse Areas';

v_except_set := 0;
 update slot_stg ss set ss.my_range =
 (select node_id from tree_dtl  where sns_id = p_sns_id and node_type = 1 and tree_id = vtree_id and ROWNUM = 1 and
 (( begin_area is not null and  begin_area <= v_ss_area and  end_area  >= v_ss_area and (case when (area_incr <> 1 and regexp_instr(begin_area, '[^[:digit:]]') = 0 and regexp_instr(v_ss_area, '[^[:digit:]]') = 0) then mod(begin_area + v_ss_area, area_incr) else 0 end) = 0) or (begin_area is null)) and
 (( begin_zone is not null and  begin_zone <= v_ss_zone and  end_zone  >= v_ss_zone and (case when (zone_incr <> 1 and regexp_instr(begin_zone, '[^[:digit:]]') = 0 and regexp_instr(v_ss_zone, '[^[:digit:]]') = 0) then mod(begin_zone + v_ss_zone, zone_incr) else 0 end) = 0) or (begin_zone is null)) and
 (( begin_aisle is not null and  begin_aisle <= v_ss_aisle and  end_aisle  >= v_ss_aisle and (case when (aisle_incr <> 1 and regexp_instr(begin_aisle, '[^[:digit:]]') = 0 and regexp_instr(v_ss_aisle, '[^[:digit:]]') = 0) then mod(begin_aisle + v_ss_aisle, aisle_incr) else 0 end) = 0) or (begin_aisle is null)) and
 (( begin_bay is not null and begin_bay <= v_ss_bay and  end_bay  >= v_ss_bay and (case when (bay_incr <> 1 and regexp_instr(begin_bay, '[^[:digit:]]') = 0 and regexp_instr(v_ss_bay, '[^[:digit:]]') = 0) then mod(begin_bay + v_ss_bay, bay_incr) else 0 end) = 0) or (begin_bay is null)) and
 (( begin_lvl is not null and  begin_lvl <= v_ss_lvl and  end_lvl  >= v_ss_lvl and (case when (lvl_incr <> 1 and regexp_instr(begin_lvl, '[^[:digit:]]') = 0 and regexp_instr(v_ss_lvl, '[^[:digit:]]') = 0) then mod(begin_lvl + v_ss_lvl, lvl_incr) else 0 end) = 0) or (begin_lvl is null)) and
 (( begin_posn is not null and  begin_posn <= v_ss_posn and  end_posn  >= v_ss_posn and (case when (posn_incr <> 1 and regexp_instr(begin_posn, '[^[:digit:]]') = 0 and regexp_instr(v_ss_posn, '[^[:digit:]]') = 0) then mod(begin_posn + v_ss_posn, posn_incr) else 0 end) = 0) or (begin_posn is null))) where ss.dsp_slot=v_ss_dsp_slot ;

    IF (v_in_wa_msg_sev >= p_log_level) then

        select count(*) into v_count from wa_detail wd  where wd.slot_id = (select slot_id from slot sl where sl.dsp_slot= v_ss_dsp_slot);

        if (v_count > 0) then
         insert into exceptions
          (
              ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
              msg_number, descript
          )
          values
          (
              get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
              v_in_wa_msg_nbr, v_in_wa_msg
          );
          v_count := 0;
        end if;
    end if;

    if( v_inv_lc_msg_sev >= p_log_level and v_ss_location_class is not null) then
        select count(*) into v_count from system_code_dtl scd where scd.whse_code in ('*df', p_whse_code) and scd.code_value= v_ss_location_class  and scd.sys_code_id  in (select sc.sys_code_id from system_code sc  where sc.code_type = '202' and sc.cust_id = 'MANH');
        if(v_count <= 0 ) then
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_inv_lc_msg_nbr, v_inv_lc_msg
        );
        update slot_stg s set location_class='A' where s.dsp_slot= v_ss_dsp_slot;
        end if;
        v_count := 0;
    end if;
    if( v_ss_rack_type = -1 and v_no_rt_msg_sev >= p_log_level)  then
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_no_rt_msg_nbr, replace(v_no_rt_msg, '%s', v_ss_rack_name)
        );
      end if;

    if ( v_rt_function_type = 0 and v_analysis_msg_sev >= p_log_level)  then
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_analysis_msg_nbr, v_analysis_msg
        );
      end if;


       if(v_sns_msg_sev >=p_log_level and   v_ss_dsp_slot is not null and (length(v_ss_dsp_slot) <> length(v_sns_min)) and v_except_set = 0) then

        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
             get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_sns_msg_nbr,replace(v_sns_msg, '%S', to_char(v_dft_sns_name))
        ) ;
      v_except_set := 1;
        update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;

      end if;

      if (v_ss_dsp_slot is not null and (v_ss_dsp_slot < v_sns_min or v_ss_dsp_slot > v_sns_max) and v_except_set = 0)
       then --SLOT-2836
            v_except_set := 1;
            update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;
      if(v_sns_msg_sev >=p_log_level) then

      insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
              get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_sns_msg_nbr,replace(v_sns_msg, '%S', to_char(v_dft_sns_name))
        );
         v_except_set := 1;
        update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;
      end if;
      end if;


     if (v_sns_msg_sev >=p_log_level and v_area_values is not null and (v_ss_area < substr(v_area_values, 1, varea-1) or v_ss_area > substr(v_area_values, varea+1,length(v_area_values))) and v_except_set = 0)
      then

      insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_sns_msg_nbr,replace(v_sns_msg, '%S', to_char(v_dft_sns_name))
        );
         v_except_set := 1;
        update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;
        end if;

   if (v_sns_msg_sev >=p_log_level and v_zone_values is not null and (v_ss_zone < substr(v_zone_values, 1, vzone-1) or v_ss_zone > substr(v_zone_values, vzone+1,length(v_zone_values))) and v_except_set = 0)
   then

   insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_sns_msg_nbr,replace(v_sns_msg, '%S', to_char(v_dft_sns_name))
        );
         v_except_set := 1;
        update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;
        end if;

   if (v_sns_msg_sev >=p_log_level and  v_aisle_values is not null and (v_ss_aisle < substr(v_aisle_values, 1, vaisle-1) or v_ss_aisle > substr(v_aisle_values, vaisle+1,length(v_aisle_values))) and v_except_set = 0)
        then

        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_sns_msg_nbr,replace(v_sns_msg, '%S', to_char(v_dft_sns_name))
        );
         v_except_set := 1;
        update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;
        end if;

   if (v_sns_msg_sev >=p_log_level and v_bay_values is not null and (v_ss_bay < substr(v_bay_values, 1, vbay-1) or  v_ss_bay > substr(v_bay_values, vbay+1,length(v_bay_values))) and v_except_set = 0)
      then


      insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_sns_msg_nbr,replace(v_sns_msg, '%S', to_char(v_dft_sns_name))
        );
         v_except_set := 1;
        update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;
      end if;

    if(v_sns_msg_sev >=p_log_level and  v_level_values is not null and (v_ss_lvl < substr(v_level_values, 1, vlvl-1) or v_ss_lvl > substr(v_level_values, vlvl+1,length(v_level_values))) and v_except_set = 0)
      then

      insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_sns_msg_nbr,replace(v_sns_msg, '%S', to_char(v_dft_sns_name))
        );
         v_except_set := 1;
        update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;
        end if;

    if(v_sns_msg_sev >=p_log_level and  v_pos_values is not null and (v_ss_posn < substr(v_pos_values, 1, vpos-1) or v_ss_posn > substr(v_pos_values, vpos+1,length(v_pos_values))) and v_except_set = 0)
      then

      insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_sns_msg_nbr,replace(v_sns_msg, '%S', to_char(v_dft_sns_name))
        );
         v_except_set := 1;
        update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;
        end if;

   if( v_dft_msg_sev >= p_log_level and v_dft_ht is not null and v_except_set = 0 )
    then
      if(v_ss_ht is null and p_default_blank = 1) then
      update slot_stg set ht= v_dft_ht where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr, replace(replace(v_dft_msg, '%s1', 'HT'), '%s2', v_dft_ht)
        ) ;
      end if;
        if (v_ss_ht = 0 and p_default_zero = 1 ) then
         update slot_stg set ht= v_dft_ht where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr_z, replace(replace(v_dft_msg_z, '%s1', 'HT'), '%s2', v_dft_ht)
        ) ;
        end if;
   end if;

    if( v_dft_msg_sev >= p_log_level and v_dft_width is not null and v_except_set = 0)
    then
    if(v_ss_width is null and p_default_blank = 1) then
     update slot_stg set width= v_dft_width where dsp_slot=v_ss_dsp_slot;
       insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr, replace(replace(v_dft_msg, '%s1', 'WIDTH'), '%s2', v_dft_width)
        );
      end if;
        if(v_ss_width = 0 and p_default_zero = 1) then
         update slot_stg set width= v_dft_width where dsp_slot=v_ss_dsp_slot;
          insert into exceptions
          (
              ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
              msg_number, descript
          )
          values
          (
              get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
              v_dft_msg_nbr_z, replace(replace(v_dft_msg_z, '%s1', 'WIDTH'), '%s2', v_dft_width)
          );
        end if;
    end if;

    if(  v_dft_msg_sev >= p_log_level and v_dft_depth is not null and v_except_set = 0 )
    then
    if(v_ss_depth is null and p_default_blank = 1) then
     update slot_stg set depth= v_dft_depth where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr, replace(replace(v_dft_msg, '%s1', 'DEPTH'), '%s2', v_dft_depth)
        );
        end if;
        if(v_ss_depth = 0 and p_default_zero = 1) then
         update slot_stg set depth= v_dft_depth where dsp_slot=v_ss_dsp_slot;
         insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr_z, replace(replace(v_dft_msg_z, '%s1', 'DEPTH'), '%s2', v_dft_depth)
        );
        end if;
      end if;

    if( v_dft_msg_sev >= p_log_level and v_dft_max_ht_clear is not null and v_except_set = 0 )
    then
    if(v_ss_max_ht_clear is null and p_default_blank = 1) then
     update slot_stg set max_ht_clear= v_dft_max_ht_clear where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr, replace(replace(v_dft_msg, '%s1', 'MAX_HT_CLEAR'), '%s2', v_dft_max_ht_clear)
        );
        end if;
        if(v_ss_max_ht_clear = 0 and p_default_zero = 1) then
        update slot_stg set max_ht_clear= v_dft_max_ht_clear where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr_z, replace(replace(v_dft_msg_z, '%s1', 'MAX_HT_CLEAR'), '%s2', v_dft_max_ht_clear)
        );
        end if;
        end if;

    if( v_dft_msg_sev >= p_log_level and v_dft_max_side_clear is not null and v_except_set = 0)
    then
    if(v_ss_max_side_clear is null and p_default_blank = 1) then
    update slot_stg set max_side_clear= v_dft_max_side_clear where dsp_slot=v_ss_dsp_slot;
       insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr, replace(replace(v_dft_msg, '%s1', 'MAX_SIDE_CLEAR'), '%s2', v_dft_max_side_clear)
        );
        end if;
        if(v_ss_max_side_clear = 0 and p_default_zero = 1) then
         update slot_stg set max_side_clear= v_dft_max_side_clear where dsp_slot=v_ss_dsp_slot;
         insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr_z, replace(replace(v_dft_msg_z, '%s1', 'MAX_SIDE_CLEAR'), '%s2', v_dft_max_side_clear)
        );
        end if;
        end if;

    if (  v_dft_msg_sev >= p_log_level and v_dft_locked is not null and v_except_set = 0)
    then
    if(v_ss_locked is null and p_default_blank = 1) then
     update slot_stg set locked= v_dft_locked1 where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr, replace(replace(v_dft_msg, '%s1', 'LOCKED'), '%s2', v_dft_locked)
        );
        end if;
        if(v_ss_locked = 0 and p_default_zero = 1) then
         update slot_stg set locked= v_dft_locked1 where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_dft_msg_nbr_z, replace(replace(v_dft_msg_z, '%s1', 'LOCKED'), '%s2', v_dft_locked)
        );
        end if;
        end if;

    if (  v_dft_msg_sev >= p_log_level and v_dft_lock_reason is not null and v_except_set = 0)
    then
    if(v_ss_lock_reason is null and p_default_blank = 1) then
    update slot_stg set lock_reason= v_dft_lock_reason where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'lock_reason'), '%s2', v_dft_lock_reason)
        );
        end if;
        if(v_ss_lock_reason = 0 and p_default_zero = 1) then
        update slot_stg set lock_reason= v_dft_lock_reason where dsp_slot=v_ss_dsp_slot;
         insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'lock_reason'), '%s2', v_dft_lock_reason)
        );
        end if;
        end if;

   if( v_dft_msg_sev >= p_log_level and v_dft_max_stack is not null and v_except_set = 0)
    then
    if(v_ss_max_stack is null and p_default_blank = 1) then
    update slot_stg set max_stack= v_dft_max_stack where dsp_slot=v_ss_dsp_slot;
       insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'MAX_STACK'), '%s2', v_dft_max_stack)
        ) ;
        end if;
        if(v_ss_max_stack = 0 and p_default_zero = 1) then
       update slot_stg set max_stack= v_dft_max_stack where dsp_slot=v_ss_dsp_slot;
         insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
               replace(replace(v_dft_msg_z, '%s1', 'MAX_STACK'), '%s2', v_dft_max_stack)
        ) ;
        end if;
        end if;

    if( v_dft_msg_sev >= p_log_level and v_dft_max_lanes is not null and v_except_set = 0)
    then
    if(v_ss_max_lanes is null and p_default_blank = 1) then
    update slot_stg set max_lanes= v_dft_max_lanes where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'MAX_LANES'), '%s2', v_dft_max_lanes)
        ) ;
        end if;
        if(v_ss_max_lanes = 0 and p_default_zero = 1) then
         update slot_stg set max_lanes= v_dft_max_lanes where dsp_slot=v_ss_dsp_slot;
         insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'MAX_LANES'), '%s2', v_dft_max_lanes)
        ) ;
        end if;
        end if;

    if ( v_dft_msg_sev >= p_log_level and v_dft_allow_expand is not null and v_except_set = 0)
    then
    if(v_ss_allow_expand is null and p_default_blank = 1) then
    update slot_stg set allow_expand= v_dft_allow_expand1 where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'ALLOW_EXPAND'), '%s2', v_dft_allow_expand)
        );
      end if;
      if(v_ss_allow_expand = 0 and p_default_zero = 1) then
      update slot_stg set allow_expand= v_dft_allow_expand1 where dsp_slot=v_ss_dsp_slot;
       insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'ALLOW_EXPAND'), '%s2', v_dft_allow_expand)
        );
      end if;
      end if;

    if ( v_dft_msg_sev >= p_log_level and v_dft_label_pos is not null and v_except_set = 0)
    then
    if(v_ss_label_pos is null and p_default_blank = 1) then
    update slot_stg set label_pos= v_dft_label_pos where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'LABEL_POS'), '%s2', v_dft_label_pos)
        );
        end if;
        if(v_ss_label_pos = 0 and p_default_zero = 1) then
        update slot_stg set label_pos= v_dft_label_pos where dsp_slot=v_ss_dsp_slot;
         insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'LABEL_POS'), '%s2', v_dft_label_pos)
        );
        end if;
        end if;

    if ( v_dft_msg_sev >= p_log_level and v_dft_wt_limit is not null and v_except_set = 0)
    then
    if(v_ss_wt_limit is null and p_default_blank = 1) then
    update slot_stg set wt_limit= v_dft_wt_limit where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'WT_LIMIT'), '%s2', v_dft_wt_limit)
        );
        end if;
        if(v_ss_wt_limit = 0 and p_default_zero = 1) then
         update slot_stg set wt_limit= v_dft_wt_limit where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'WT_LIMIT'), '%s2', v_dft_wt_limit)
        );
        end if;
        end if;

    if( v_dft_msg_sev >= p_log_level and v_dft_max_lane_wt is not null and v_except_set = 0)
    then
    if(v_ss_max_lane_wt is null and p_default_blank = 1) then
     update slot_stg set max_lane_wt= v_dft_max_lane_wt where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'MAX_LANE_WT'), '%s2', v_dft_max_lane_wt)
        );
      end if;
      if(v_ss_max_lane_wt = 0 and p_default_zero = 1) then
      update slot_stg set max_lane_wt= v_dft_max_lane_wt where dsp_slot=v_ss_dsp_slot;
      insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'MAX_LANE_WT'), '%s2', v_dft_max_lane_wt)
        );
      end if;
      end if;


    if( v_dft_msg_sev >= p_log_level and v_dft_x_coord is not null and v_except_set = 0)
    then
    if(v_ss_x_coord is null and p_default_blank = 1) then
     update slot_stg set x_coord= v_dft_x_coord where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'X_COORD'), '%s2', v_dft_x_coord)
        );
        end if;
        if(v_ss_x_coord = 0 and p_default_zero = 1) then
        update slot_stg set x_coord= v_dft_x_coord where dsp_slot=v_ss_dsp_slot;
         insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'X_COORD'), '%s2', v_dft_x_coord)
        );
        end if;
        end if;

    if( v_dft_msg_sev >= p_log_level and v_dft_y_coord is not null and v_except_set = 0)
    then
    if(v_ss_y_coord is null and p_default_blank = 1) then
    update slot_stg set y_coord= v_dft_y_coord where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'Y_COORD'), '%s2', v_dft_y_coord)
        ) ;
        end if;
        if(v_ss_y_coord = 0 and p_default_zero = 1) then
        update slot_stg set y_coord= v_dft_y_coord where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'Y_COORD'), '%s2', v_dft_y_coord)
        ) ;
        end if;
        end if;

    if ( v_dft_msg_sev >= p_log_level and v_dft_z_coord is not null and v_except_set = 0)
    then
    if(v_ss_z_coord is null and p_default_blank = 1) then
     update slot_stg set z_coord= v_dft_z_coord where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'Z_COORD'), '%s2', v_dft_z_coord)
        );
        end if;
        if(v_ss_z_coord = 0 and p_default_zero = 1) then
         update slot_stg set z_coord= v_dft_z_coord where dsp_slot=v_ss_dsp_slot;
         insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'Z_COORD'), '%s2', v_dft_z_coord)
        );
        end if;
        end if;

    if ( v_dft_msg_sev >= p_log_level and v_dft_slot_type is not null and v_except_set = 0)
    then
    if(v_ss_slot_type is null and p_default_blank = 1) then
     update slot_stg set slot_type= v_dft_slot_type where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'SLOT_TYPE'), '%s2', v_dft_slot_type)
        ) ;
        end if;
        if(v_ss_slot_type =0 and p_default_zero = 1) then
         update slot_stg set slot_type= v_dft_slot_type where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'SLOT_TYPE'), '%s2', v_dft_slot_type)
        ) ;
        end if;
        end if;

    if ( v_dft_msg_sev >= p_log_level and v_dft_travel_zone is not null and v_except_set = 0)
    then
    if(v_ss_travel_zone is null and p_default_blank = 1) then
    update slot_stg set travel_zone= v_dft_travel_zone where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'TRAVEL_ZONE'), '%s2', v_dft_travel_zone)
        );
        end if;
        if(v_ss_travel_zone = 0 and p_default_zero = 1) then
        update slot_stg set travel_zone= v_dft_travel_zone where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'TRAVEL_ZONE'), '%s2', v_dft_travel_zone)
        );
        end if;
        end if;

    if ( v_dft_msg_sev >= p_log_level and v_dft_travel_aisle is not null and v_except_set = 0 )
    then
    if(v_ss_travel_aisle is null and p_default_blank = 1) then
    update slot_stg set travel_aisle = v_dft_travel_aisle where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'TRAVEL_AISLE'), '%s2', v_dft_travel_aisle)
        );
        end if;
        if(v_ss_travel_aisle = 0 and p_default_zero = 1) then
        update slot_stg set travel_aisle = v_dft_travel_aisle where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'TRAVEL_AISLE'), '%s2', v_dft_travel_aisle)
        );
        end if;
        end if;

    if( v_dft_msg_sev >= p_log_level and v_dft_location_class is not null and v_except_set = 0)
    then
    if(v_ss_location_class is null and p_default_blank = 1) then
     update slot_stg set location_class = v_dft_location_class where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'LOCATION_CLASS'), '%s2', v_dft_location_class)
        );
      end if;
      if(v_ss_location_class = 0 and p_default_zero = 1) then
       update slot_stg set location_class = v_dft_location_class where dsp_slot=v_ss_dsp_slot;
       insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'LOCATION_CLASS'), '%s2', v_dft_location_class)
        );
      end if;
      end if;

    if( v_dft_msg_sev >= p_log_level and v_dft_slot_priority is not null and v_except_set = 0)
    then
    if(v_ss_slot_priority is null and p_default_blank = 1) then
     update slot_stg set slot_priority = v_dft_slot_priority where dsp_slot=v_ss_dsp_slot;
       insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr,
            replace(replace(v_dft_msg, '%s1', 'SLOT_PRIORITY'), '%s2', v_dft_slot_priority)
        );
        end if;
        if(v_ss_slot_priority =0 and p_default_zero = 1) then
         update slot_stg set slot_priority = v_dft_slot_priority where dsp_slot=v_ss_dsp_slot;
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id, v_dft_msg_nbr_z,
            replace(replace(v_dft_msg_z, '%s1', 'SLOT_PRIORITY'), '%s2', v_dft_slot_priority)
        );
        end if;
        end if;

    if( v_no_rl_msg_sev >= p_log_level and v_ss_rack_level_id is null and v_no_rl_msg_sev >= p_log_level)
    then
        insert into exceptions
        (
            ex_id, whse_code, slot, create_date_time, mod_date_time, mod_user,
            msg_number, descript
        )
        values
        (
            get_unique_ids(v_rec_exce_type), p_whse_code, v_ss_dsp_slot, sysdate, sysdate, p_user_id,
            v_no_rl_msg_nbr, replace(replace(v_no_rl_msg, '%s1', v_ss_dsp_slot), '%s2', v_ss_rack_name)
        );
         update slot_stg s set process_code='D' where s.dsp_slot= v_ss_dsp_slot;
        end if;


     FETCH slotStageCursor INTO v_ss_dsp_slot, v_ss_ht, v_ss_width , v_ss_depth, v_ss_max_ht_clear , v_ss_locked , v_ss_max_side_clear ,   v_ss_lock_reason ,
    v_ss_max_stack ,  v_ss_max_lanes ,v_ss_allow_expand , v_ss_label_pos,  v_ss_wt_limit ,v_ss_max_lane_wt , v_ss_x_coord , v_ss_y_coord , v_ss_z_coord,
    v_ss_slot_type , v_ss_travel_zone,  v_ss_travel_aisle , v_ss_location_class , v_ss_slot_priority ,  v_ss_rack_type, v_ss_rack_name , v_ss_rack_level_id , v_ss_slot_id,
    v_rt_function_type ,  v_ss_area ,  v_ss_zone ,  v_ss_aisle ,  v_ss_bay ,  v_ss_lvl ,   v_ss_posn, v_ss_process_code ;
    END LOOP;
CLOSE slotStageCursor;

  commit;
-- C++ will apply defaults
end;

/
--------------------------------------------------------
--  DDL for Procedure SPCONSOLITEMHISTUNSLOTTED
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SPCONSOLITEMHISTUNSLOTTED (v_sku_id IN NUMBER, v_slot_ship_unit IN NUMBER, v_hist_match IN NVARCHAR2, v_mult_loc_grp IN NVARCHAR2, v_no_of_slot_item_recs IN NUMBER, v_min_slotitem_id IN NUMBER ,
v_errCode    IN OUT NUMBER , v_refcur OUT SYS_REFCURSOR)


   as
   v_i_ih_nextrowid  NUMBER(10,0);
   v_i_ih_currowid  NUMBER(10,0);
   v_i_ih_loopctl  NUMBER(1,0);
   v_wh_ti  NUMBER(10,0);
   v_ven_ti  NUMBER(10,0);
   v_ord_ti  NUMBER(10,0);
   v_wh_hi  NUMBER(10,0);
   v_ven_hi  NUMBER(10,0);
   v_ord_hi  NUMBER(10,0);
   v_inn_per_cs  NUMBER(10,0);
   v_each_per_cs  NUMBER(10,0);
   v_each_per_inn  NUMBER(10,0);
   v_item_ship_unit  NUMBER(5,0);
   v_start_date  TIMESTAMP(3);
   v_end_date  TIMESTAMP(3);
   v_pallete_pattern  NUMBER(10,0);
   v_mov_pallet  NUMBER(17,2);
   v_mov_case  NUMBER(17,2);
   v_mov_each  NUMBER(17,2);
   v_mov_inner  NUMBER(17,2);
   v_inv_pallet  NUMBER(17,2);
   v_inv_case  NUMBER(17,2);
   v_inv_inner  NUMBER(17,2);
   v_inv_each  NUMBER(17,2);
   v_nbr_of_picks  NUMBER(17,2);
   v_no_of_item_hist_recs  NUMBER(10,0);
   v_hist_id  NUMBER(19,0);
   v_recs_updtd_tot  NUMBER(10,0);
   v_recs_updtd_tmp  NUMBER(10,0);
   v_bIs_SlotItem_good_to_delete  NUMBER(1,0);
   v_strSQLSt  VARCHAR2(255);
   v_rc_del  NUMBER(10,0);
   SWV_ROWCOUNT  NUMBER(10,0);
   SWV_no_of_slot_item_recs NUMBER(10,0);

   v_if_exists NUMBER(10,0);
   v_if_exists2 NUMBER(10,0);
BEGIN
   SWV_no_of_slot_item_recs := v_no_of_slot_item_recs;

   v_errCode    := 0;
   v_bIs_SlotItem_good_to_delete := 0;

      select   wh_hi, ven_hi, ord_hi, wh_ti, ven_ti, ord_ti, inn_per_cs, each_per_cs, each_per_inn INTO v_wh_hi,v_ven_hi,v_ord_hi,v_wh_ti,v_ven_ti,v_ord_ti,v_inn_per_cs,v_each_per_cs,
      v_each_per_inn from so_item_master where sku_id = v_sku_id;
    
  

   v_wh_hi := NVL(v_wh_hi,1);
   v_ven_hi := NVL(v_ven_hi,1);
   v_ord_hi := NVL(v_ord_hi,1);
   v_wh_ti := NVL(v_wh_ti,1);
   v_ven_ti := NVL(v_ven_ti,1);
   v_ord_ti := NVL(v_ord_ti,1);

   EXECUTE IMMEDIATE ' truncate table tt_TEMP_ITEM_HISTORY_SMRY2 ';

   insert into tt_TEMP_ITEM_HISTORY_SMRY2(ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date)
   select t.ship_unit as ship_unit, t.pallete_pattern as pallete_pattern, sum(t.nbr_of_picks) as nbr_of_picks,
        sum(t.mov1) as mov_pallets, sum(t.mov2) as mov_cases, sum(t.mov4) as mov_inners, sum(t.mov8) as mov_eaches,
        sum(t.inv1) as inv_pallets, sum(t.inv2) as inv_cases, sum(t.inv4) as inv_inners, sum(t.inv8) as inv_eaches,
        start_date as start_date, end_date as end_date from(select ih.ship_unit , si.pallete_pattern, sum(NVL(ih.nbr_of_picks,0)) nbr_of_picks,
            NVL(case ih.ship_unit when 1 then  sum(NVL(ih.movement,0)) end,0) as mov1,
            NVL(case ih.ship_unit when 2 then  sum(NVL(ih.movement,0)) end,0) as mov2,
            NVL(case ih.ship_unit when 4 then  sum(NVL(ih.movement,0)) end,0) as mov4,
            NVL(case ih.ship_unit when 8 then  sum(NVL(ih.movement,0)) end,0) as mov8,
            NVL(case ih.ship_unit when 1 then  sum(NVL(ih.inventory,0)) end,0) as inv1,
            NVL(case ih.ship_unit when 2 then  sum(NVL(ih.inventory,0)) end,0) as inv2,
            NVL(case ih.ship_unit when 4 then  sum(NVL(ih.inventory,0)) end,0) as inv4,
            NVL(case ih.ship_unit when 8 then  sum(NVL(ih.inventory,0)) end,0) as inv8,
            ih.start_date as start_date, ih.end_date as end_date
      from item_history ih
      inner join slot_item si  on si.slotitem_id = ih.slotitem_id
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null
      and si.delete_mult <> 1
      and si.slotitem_id <> v_min_slotitem_id
      group by ih.ship_unit,si.pallete_pattern,ih.start_date,ih.end_date) t
   group by t.ship_unit,t.pallete_pattern,t.start_date,t.end_date
   order by t.start_date NULLS FIRST,t.end_date NULLS FIRST;
   v_i_ih_loopctl := 1;

      select   min(i_ih_rowid) INTO v_i_ih_nextrowid from   tt_TEMP_ITEM_HISTORY_SMRY2;
     
 
 
      select   i_ih_rowid, ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date INTO v_i_ih_currowid,v_item_ship_unit,v_pallete_pattern,v_nbr_of_picks,v_mov_pallet,
      v_mov_case,v_mov_inner,v_mov_each,v_inv_pallet,v_inv_case,v_inv_inner,
      v_inv_each,v_start_date,v_end_date from    tt_TEMP_ITEM_HISTORY_SMRY2  where    i_ih_rowid = v_i_ih_nextrowid;
     
    
   while v_i_ih_loopctl = 1 LOOP


   SELECT  COUNT(*) INTO v_if_exists from item_history ih
      inner join slot_item si  on si.slotitem_id = ih.slotitem_id WHERE si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null
      and si.delete_mult <> 1
      and si.slotitem_id = v_min_slotitem_id
      and ih.start_date = v_start_date and ih.end_date = v_end_date AND ROWNUM <= 1;
      if v_if_exists > 0 then
         
         v_recs_updtd_tot := 0;
         v_recs_updtd_tmp := 0;
         v_no_of_item_hist_recs := 1;
         v_strSQLSt := 'update item_history set  nbr_of_picks = nbr_of_picks + (' || v_nbr_of_picks || '/ ' || v_no_of_item_hist_recs||' ) '||',MOD_DATE_TIME = SYSDATE'||' , MOD_USER= '||'''XYZ'''||
' from item_history ih '||
        ' inner join slot_item si  on si.slotitem_id = ih.slotitem_id where si.sku_id ='|| v_sku_id
         ||' and si.ship_unit ='|| v_slot_ship_unit
         ||' and si.delete_mult <> 1' 
        ||''' and NVL(si.hist_match,''0x'') = '''||v_hist_match
         ||''' and NVL(si.mult_loc_grp,''0x'') = '''||v_mult_loc_grp
         ||''' and si.slot_id is not null'
        ||''' and ih.start_date = '''||v_start_date||''' and ih.end_date = '''||v_end_date ;
                
                
                
         EXECUTE IMMEDIATE v_strSQLSt USING v_recs_updtd_tmp;
         v_errCode := SQLCODE;
        
            
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;



         UPDATE item_history ih_Upd SET(movement,inventory,mod_date_time,mod_user) =(SELECT  case
         when v_item_ship_unit = 1 then ih.movement+(v_mov_pallet/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 0 then ih.movement+((v_mov_case/(v_wh_hi*v_wh_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 1 then ih.movement+((v_mov_case/(v_ord_hi*v_ord_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 2 then ih.movement+((v_mov_case/(v_ven_hi*v_ven_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 0 then ih.movement+((v_mov_inner/(v_wh_hi*v_wh_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 1 then ih.movement+((v_mov_inner/(v_ord_hi*v_ord_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 2 then ih.movement+((v_mov_inner/(v_ven_hi*v_ven_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 0 then ih.movement+((v_mov_each/(v_wh_hi*v_wh_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 1 then ih.movement+((v_mov_each/(v_ord_hi*v_ord_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 2 then ih.movement+((v_mov_each/(v_ven_hi*v_ven_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         end,case
         when v_item_ship_unit = 1 then ih.inventory+(v_inv_pallet/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 0 then ih.inventory+((v_inv_case/(v_wh_hi*v_wh_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 1 then ih.inventory+((v_inv_case/(v_ord_hi*v_ord_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 2 then ih.inventory+((v_inv_case/(v_ven_hi*v_ven_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 0 then ih.inventory+((v_inv_inner/(v_wh_hi*v_wh_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 1 then ih.inventory+((v_inv_inner/(v_ord_hi*v_ord_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 2 then ih.inventory+((v_inv_inner/(v_ven_hi*v_ven_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 0 then ih.inventory+((v_inv_each/(v_wh_hi*v_wh_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 1 then ih.inventory+((v_inv_each/(v_ord_hi*v_ord_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 2 then ih.inventory+((v_inv_each/(v_ven_hi*v_ven_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         end,SYSTIMESTAMP,'consoldtd' FROM  item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id WHERE (si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is null
         and si.slotitem_id = v_min_slotitem_id
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 1) AND ih.ROWID = ih_Upd.ROWID) WHERE ROWID IN(SELECT ih.ROWID from item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is null
         and si.slotitem_id = v_min_slotitem_id
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 1);
         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;
         DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# in item_history for ih.ship_unit = 1: ' || SUBSTR(CAST(v_recs_updtd_tmp AS VARCHAR2),1,10) || 'SQLWAYS_EVAL# ' || SUBSTR(TO_CHAR(v_start_date,'SQLWAYS_EVAL# 4:mi:ss'),1,10) || ' end date: ' || SUBSTR(TO_CHAR(v_end_date,'SQLWAYS_EVAL# 4:mi:ss'),1,10));
         UPDATE item_history ih_Upd SET(movement,inventory,mod_date_time,mod_user) =(SELECT  case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.movement+((v_mov_pallet*v_wh_hi*v_wh_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.movement+((v_mov_pallet*v_ord_hi*v_ord_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.movement+((v_mov_pallet*v_ven_hi*v_ven_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.movement+(v_mov_case/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.movement+((v_mov_inner/v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.movement+((v_mov_each/v_each_per_cs)/v_no_of_item_hist_recs)
         end,case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.inventory+((v_inv_pallet*v_wh_hi*v_wh_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.inventory+((v_inv_pallet*v_ord_hi*v_ord_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.inventory+((v_inv_pallet*v_ven_hi*v_ven_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.inventory+(v_inv_case/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.inventory+((v_inv_inner/v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.inventory+((v_inv_each/v_each_per_cs)/v_no_of_item_hist_recs)
         end,SYSTIMESTAMP,'consoldtd' FROM  item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id WHERE (si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slotitem_id  = v_min_slotitem_id
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 2) AND ih.ROWID = ih_Upd.ROWID) WHERE ROWID IN(SELECT ih.ROWID from item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slotitem_id  = v_min_slotitem_id
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 2);
         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;
         DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# in item_history for ih.ship_unit = 2: ' || SUBSTR(CAST(v_recs_updtd_tmp AS VARCHAR2),1,10) || 'SQLWAYS_EVAL# ' || SUBSTR(TO_CHAR(v_start_date,'SQLWAYS_EVAL# 4:mi:ss'),1,10) || ' end date: ' || SUBSTR(TO_CHAR(v_end_date,'SQLWAYS_EVAL# 4:mi:ss'),1,10));
         UPDATE item_history ih_Upd SET(movement,inventory,mod_date_time,mod_user) =(SELECT  case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.movement+((v_mov_pallet*v_wh_hi*v_wh_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.movement+((v_mov_pallet*v_ord_hi*v_ord_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.movement+((v_mov_pallet*v_ven_hi*v_ven_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.movement+((v_mov_case*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.movement+(v_mov_inner/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.movement+((v_mov_each/v_each_per_inn)/v_no_of_item_hist_recs)
         end,case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.inventory+((v_inv_pallet*v_wh_hi*v_wh_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.inventory+((v_inv_pallet*v_ord_hi*v_ord_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.inventory+((v_inv_pallet*v_ven_hi*v_ven_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.inventory+((v_inv_case*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.inventory+(v_inv_inner/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.inventory+((v_inv_each/v_each_per_inn)/v_no_of_item_hist_recs)
         end,SYSTIMESTAMP,'consoldtd' FROM  item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id WHERE (si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slotitem_id  = v_min_slotitem_id
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 4) AND ih.ROWID = ih_Upd.ROWID) WHERE ROWID IN(SELECT ih.ROWID from item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slotitem_id  = v_min_slotitem_id
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 4);
         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;
         
         
         UPDATE item_history ih_Upd SET(movement,inventory,mod_date_time,mod_user) =(SELECT  case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.movement+((v_mov_pallet*v_wh_hi*v_wh_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.movement+((v_mov_pallet*v_ord_hi*v_ord_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.movement+((v_mov_pallet*v_ven_hi*v_ven_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.movement+((v_mov_case*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.movement+((v_mov_inner*v_each_per_inn)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.movement+(v_mov_each/v_no_of_item_hist_recs)
         end,case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.inventory+((v_inv_pallet*v_wh_hi*v_wh_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.inventory+((v_inv_pallet*v_ord_hi*v_ord_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.inventory+((v_inv_pallet*v_ven_hi*v_ven_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.inventory+((v_inv_case*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.inventory+((v_inv_inner*v_each_per_inn)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.inventory+(v_inv_each/v_no_of_item_hist_recs)
         end,SYSTIMESTAMP,'consoldtd' FROM  item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id WHERE (si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slotitem_id  = v_min_slotitem_id
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 8) AND ih.ROWID = ih_Upd.ROWID) WHERE ROWID IN(SELECT ih.ROWID from item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slotitem_id  = v_min_slotitem_id
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 8);
         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;
         
         if (v_recs_updtd_tot > 0) then
            
         
         
            v_strSQLSt := 'delete from item_history ' ||
                    ' where hist_id in  ( select hist_id from item_history ih'
                    ||' inner join slot_item si on si.slotitem_id = ih.slotitem_id     where si.sku_id = '||v_sku_id
         ||' and si.ship_unit ='|| v_slot_ship_unit
         ||' and si.delete_mult <> 1' 
        ||''' and NVL(si.hist_match,''0x'') = '''||v_hist_match
         ||''' and NVL(si.mult_loc_grp,''0x'') = '''||v_mult_loc_grp
         ||''' and si.slot_id is not null'
        ||''' and ih.start_date = '''||v_start_date||''' and ih.end_date = '''||v_end_date 
        ||''' and ih.ship_unit = '''|| v_item_ship_unit|| ''')';



            EXECUTE IMMEDIATE v_strSQLSt USING v_rc_del;
            v_errCode := SQLCODE;
            
         else
            
            v_errCode := -1;
            END IF;
      end if;
 
 
 
      v_i_ih_nextrowid := NULL;
   
         select   NVL(min(i_ih_rowid),0) INTO v_i_ih_nextrowid from     tt_TEMP_ITEM_HISTORY_SMRY2 where    i_ih_rowid > v_i_ih_currowid;
       

      if NVL(v_i_ih_nextrowid,0) = 0 then
         EXIT;
      end if;
       
       
       
    
         select   i_ih_rowid, ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date INTO v_i_ih_currowid,v_item_ship_unit,v_pallete_pattern,v_nbr_of_picks,v_mov_pallet,
         v_mov_case,v_mov_inner,v_mov_each,v_inv_pallet,v_inv_case,v_inv_inner,
         v_inv_each,v_start_date,v_end_date from    tt_TEMP_ITEM_HISTORY_SMRY2  where    i_ih_rowid = v_i_ih_nextrowid;
       
   END LOOP; 

   EXECUTE IMMEDIATE ' truncate table tt_TEMP_ITEM_HISTORY_SMRY2 ';
  
   SWV_no_of_slot_item_recs := 1;
    
   insert into tt_TEMP_ITEM_HISTORY_SMRY2(ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date)
   select distinct v_slot_ship_unit, si.pallete_pattern, 0,0,0,0,0,0,0,0,0, ih.start_date, ih.end_date
   from item_history ih
   inner join slot_item si  on si.slotitem_id = ih.slotitem_id
   where si.sku_id = v_sku_id
   and si.ship_unit = v_slot_ship_unit
   and si.delete_mult <> 1
   and NVL(si.hist_match,'0x') = v_hist_match
   and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
   and si.slotitem_id <> v_min_slotitem_id
   and si.slot_id is null
            
   order by ih.start_date NULLS FIRST,si.pallete_pattern NULLS FIRST;
   v_i_ih_loopctl := 1;

      select   min(i_ih_rowid) INTO v_i_ih_nextrowid from   tt_TEMP_ITEM_HISTORY_SMRY2;
 
    
   
   UPDATE tt_TEMP_ITEM_HISTORY_SMRY2 tihs_Upd SET(mov1,mov2,mov4,mov8,inv1,inv2,inv4,inv8,nbr_of_picks) =(SELECT  tout.mov1,tout.mov2,tout.mov4,tout.mov8,tout.inv1,tout.inv2,tout.inv4,tout.inv8,tout.nbr_of_picks FROM  tt_TEMP_ITEM_HISTORY_SMRY2 tihs
   inner join(select    sum(thist.mov1) mov1, sum(thist.mov2) mov2, sum(thist.mov4) mov4, sum(thist.mov8) mov8,
                        sum(thist.inv1) inv1, sum(thist.inv2) inv2, sum(thist.inv4) inv4, sum(thist.inv8) inv8,
                        thist.start_date, thist.end_date, thist.pallete_pattern, sum(thist.nbr_of_picks) nbr_of_picks
      from(select    NVL(case ih.ship_unit when 1 then  sum(NVL(ih.movement,0)) end,0) as mov1,
                        NVL(case ih.ship_unit when 2 then  sum(NVL(ih.movement,0)) end,0) as mov2,
                        NVL(case ih.ship_unit when 4 then  sum(NVL(ih.movement,0)) end,0) as mov4,
                        NVL(case ih.ship_unit when 8 then  sum(NVL(ih.movement,0)) end,0) as mov8,
                        NVL(case ih.ship_unit when 1 then  sum(NVL(ih.inventory,0)) end,0) as inv1,
                        NVL(case ih.ship_unit when 2 then  sum(NVL(ih.inventory,0)) end,0) as inv2,
                        NVL(case ih.ship_unit when 4 then  sum(NVL(ih.inventory,0)) end,0) as inv4,
                        NVL(case ih.ship_unit when 8 then  sum(NVL(ih.inventory,0)) end,0) as inv8,
                        sum(NVL(ih.nbr_of_picks,0))/SWV_no_of_slot_item_recs as nbr_of_picks,
                        ih.start_date, ih.end_date, si.pallete_pattern
         from item_history ih
         inner join slot_item si  on si.slotitem_id = ih.slotitem_id
         where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is null and si.slotitem_id <> v_min_slotitem_id
                    
         group by ih.ship_unit,ih.start_date,ih.end_date,si.pallete_pattern) thist group by thist.start_date,thist.end_date,thist.pallete_pattern) tout
   on tout.start_date = tihs.start_date
   and tout.end_date = tihs.end_date
   and tout.pallete_pattern = tihs.pallete_pattern WHERE tihs.ROWID = tihs_Upd.ROWID) WHERE ROWID IN(SELECT tihs.ROWID from tt_TEMP_ITEM_HISTORY_SMRY2 tihs
   inner join(select    sum(thist.mov1) mov1, sum(thist.mov2) mov2, sum(thist.mov4) mov4, sum(thist.mov8) mov8,
                        sum(thist.inv1) inv1, sum(thist.inv2) inv2, sum(thist.inv4) inv4, sum(thist.inv8) inv8,
                        thist.start_date, thist.end_date, thist.pallete_pattern, sum(thist.nbr_of_picks) nbr_of_picks
      from(select    NVL(case ih.ship_unit when 1 then  sum(NVL(ih.movement,0)) end,0) as mov1,
                        NVL(case ih.ship_unit when 2 then  sum(NVL(ih.movement,0)) end,0) as mov2,
                        NVL(case ih.ship_unit when 4 then  sum(NVL(ih.movement,0)) end,0) as mov4,
                        NVL(case ih.ship_unit when 8 then  sum(NVL(ih.movement,0)) end,0) as mov8,
                        NVL(case ih.ship_unit when 1 then  sum(NVL(ih.inventory,0)) end,0) as inv1,
                        NVL(case ih.ship_unit when 2 then  sum(NVL(ih.inventory,0)) end,0) as inv2,
                        NVL(case ih.ship_unit when 4 then  sum(NVL(ih.inventory,0)) end,0) as inv4,
                        NVL(case ih.ship_unit when 8 then  sum(NVL(ih.inventory,0)) end,0) as inv8,
                        sum(NVL(ih.nbr_of_picks,0))/SWV_no_of_slot_item_recs as nbr_of_picks,
                        ih.start_date, ih.end_date, si.pallete_pattern
         from item_history ih
         inner join slot_item si  on si.slotitem_id = ih.slotitem_id
         where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is null and si.slotitem_id <> v_min_slotitem_id
                    
         group by ih.ship_unit,ih.start_date,ih.end_date,si.pallete_pattern) thist group by thist.start_date,thist.end_date,thist.pallete_pattern) tout
   on tout.start_date = tihs.start_date
   and tout.end_date = tihs.end_date
   and tout.pallete_pattern = tihs.pallete_pattern);


      select   i_ih_rowid, ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date INTO v_i_ih_currowid,v_item_ship_unit,v_pallete_pattern,v_nbr_of_picks,v_mov_pallet,
      v_mov_case,v_mov_inner,v_mov_each,v_inv_pallet,v_inv_case,v_inv_inner,
      v_inv_each,v_start_date,v_end_date from    tt_TEMP_ITEM_HISTORY_SMRY2  where    i_ih_rowid = v_i_ih_nextrowid;

    
   while v_i_ih_loopctl = 1 LOOP

      open v_refcur for select
      
      v_i_ih_currowid as i_ih_currowid,    v_item_ship_unit as item_ship_unit, v_nbr_of_picks as nbr_of_picks,
            v_mov_pallet as mov_pallet, v_mov_case as mov_case, v_mov_inner as mov_inner, v_mov_each as mov_each,
            v_inv_pallet as inv_pallet, v_inv_case as inv_case, v_inv_inner as inv_inner, v_inv_each as inv_each,
            v_start_date as start_date, v_end_date as end_date, v_pallete_pattern as pallete_pattern, v_slot_ship_unit as slot_ship_unit,
            SWV_no_of_slot_item_recs as no_of_slot_item_recs  from dual;
      SELECT   COUNT(*) INTO v_if_exists2 from item_history ih
      inner join slot_item si  on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slotitem_id = v_min_slotitem_id
      and si.slot_id is null
      and ih.start_date = v_start_date and ih.end_date = v_end_date;
      if  v_if_exists2 = 0 then
         

      --   EXECUTE IMMEDIATE ' truncate table tt_TEMP_ITEM_HISTORY_INST2 ';

            select   curr_nbr INTO v_hist_id from unique_ids where rec_type = 'item_history';
        
            
            
         insert into tt_TEMP_ITEM_HISTORY_INST2(slotitem_id, start_date, end_date, ship_unit, nbr_of_picks, movement, inventory)

         select v_min_slotitem_id, v_start_date, v_end_date, v_slot_ship_unit, v_nbr_of_picks,0,0
         from slot_item si
         where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is null
         and si.slotitem_id = v_min_slotitem_id
         order by si.slotitem_id NULLS FIRST;
      
      
            SELECT ROWNUM+1 INTO v_hist_id FROM tt_TEMP_ITEM_HISTORY_INST2 WHERE rownum <= 1;
          
         UPDATE tt_TEMP_ITEM_HISTORY_INST2 SET hist_id = v_hist_id,movement =    case
         when v_slot_ship_unit = 8 and v_pallete_pattern = 0 then((v_mov_pallet*v_wh_hi*v_wh_ti*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_case*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_inner*v_each_per_inn)/SWV_no_of_slot_item_recs)+(v_mov_each/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 8 and v_pallete_pattern = 1 then((v_mov_pallet*v_ord_hi*v_ord_ti*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_case*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_inner*v_each_per_inn)/SWV_no_of_slot_item_recs)+(v_mov_each/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 8 and v_pallete_pattern = 2 then((v_mov_pallet*v_ven_hi*v_ven_ti*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_case*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_inner*v_each_per_inn)/SWV_no_of_slot_item_recs)+(v_mov_each/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 0 then((v_mov_pallet*v_wh_hi*v_wh_ti*v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_case*v_inn_per_cs)/SWV_no_of_slot_item_recs)+(v_mov_inner/SWV_no_of_slot_item_recs)+((v_mov_each/v_each_per_inn)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 1 then((v_mov_pallet*v_ord_hi*v_ord_ti*v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_case*v_inn_per_cs)/SWV_no_of_slot_item_recs)+(v_mov_inner/SWV_no_of_slot_item_recs)+((v_mov_each/v_each_per_inn)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 2 then((v_mov_pallet*v_ven_hi*v_ven_ti*v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_case*v_inn_per_cs)/SWV_no_of_slot_item_recs)+(v_mov_inner/SWV_no_of_slot_item_recs)+((v_mov_each/v_each_per_inn)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 0 then((v_mov_pallet*v_wh_hi*v_wh_ti)/SWV_no_of_slot_item_recs)+(v_mov_case/SWV_no_of_slot_item_recs)+((v_mov_inner/v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_each/v_each_per_cs)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 1 then((v_mov_pallet*v_ord_hi*v_ord_ti)/SWV_no_of_slot_item_recs)+(v_mov_case/SWV_no_of_slot_item_recs)+((v_mov_inner/v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_each/v_each_per_cs)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 2 then((v_mov_pallet*v_ven_hi*v_ven_ti)/SWV_no_of_slot_item_recs)+(v_mov_case/SWV_no_of_slot_item_recs)+((v_mov_inner/v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_mov_each/v_each_per_cs)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 0 then(v_mov_pallet/SWV_no_of_slot_item_recs)+((v_mov_case/(v_wh_hi*v_wh_ti))/SWV_no_of_slot_item_recs)+((v_mov_inner/(v_wh_hi*v_wh_ti*v_inn_per_cs))/SWV_no_of_slot_item_recs)+((v_mov_each/(v_wh_hi*v_wh_ti*v_each_per_cs))/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 1 then(v_mov_pallet/SWV_no_of_slot_item_recs)+((v_mov_case/(v_ord_hi*v_ord_ti))/SWV_no_of_slot_item_recs)+((v_mov_inner/(v_ord_hi*v_ord_ti*v_inn_per_cs))/SWV_no_of_slot_item_recs)+((v_mov_each/(v_ord_hi*v_ord_ti*v_each_per_cs))/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 2 then(v_mov_pallet/SWV_no_of_slot_item_recs)+((v_mov_case/(v_ven_hi*v_ven_ti))/SWV_no_of_slot_item_recs)+((v_mov_inner/(v_ven_hi*v_ven_ti*v_inn_per_cs))/SWV_no_of_slot_item_recs)+((v_mov_each/(v_ven_hi*v_ven_ti*v_each_per_cs))/SWV_no_of_slot_item_recs)
         end,
         inventory =    case
         when v_slot_ship_unit = 8 and v_pallete_pattern = 0 then((v_inv_pallet*v_wh_hi*v_wh_ti*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_case*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_inner*v_each_per_inn)/SWV_no_of_slot_item_recs)+(v_inv_each/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 8 and v_pallete_pattern = 1 then((v_inv_pallet*v_ord_hi*v_ord_ti*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_case*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_inner*v_each_per_inn)/SWV_no_of_slot_item_recs)+(v_inv_each/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 8 and v_pallete_pattern = 2 then((v_inv_pallet*v_ven_hi*v_ven_ti*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_case*v_each_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_inner*v_each_per_inn)/SWV_no_of_slot_item_recs)+(v_inv_each/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 0 then((v_inv_pallet*v_wh_hi*v_wh_ti*v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_case*v_inn_per_cs)/SWV_no_of_slot_item_recs)+(v_inv_inner/SWV_no_of_slot_item_recs)+((v_inv_each/v_each_per_inn)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 1 then((v_inv_pallet*v_ord_hi*v_ord_ti*v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_case*v_inn_per_cs)/SWV_no_of_slot_item_recs)+(v_inv_inner/SWV_no_of_slot_item_recs)+((v_inv_each/v_each_per_inn)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 2 then((v_inv_pallet*v_ven_hi*v_ven_ti*v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_case*v_inn_per_cs)/SWV_no_of_slot_item_recs)+(v_inv_inner/SWV_no_of_slot_item_recs)+((v_inv_each/v_each_per_inn)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 0 then((v_inv_pallet*v_wh_hi*v_wh_ti)/SWV_no_of_slot_item_recs)+(v_inv_case/SWV_no_of_slot_item_recs)+((v_inv_inner/v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_each/v_each_per_cs)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 1 then((v_inv_pallet*v_ord_hi*v_ord_ti)/SWV_no_of_slot_item_recs)+(v_inv_case/SWV_no_of_slot_item_recs)+((v_inv_inner/v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_each/v_each_per_cs)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 2 then((v_inv_pallet*v_ven_hi*v_ven_ti)/SWV_no_of_slot_item_recs)+(v_inv_case/SWV_no_of_slot_item_recs)+((v_inv_inner/v_inn_per_cs)/SWV_no_of_slot_item_recs)+((v_inv_each/v_each_per_cs)/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 0 then(v_inv_pallet/SWV_no_of_slot_item_recs)+((v_inv_case/(v_wh_hi*v_wh_ti))/SWV_no_of_slot_item_recs)+((v_inv_inner/(v_wh_hi*v_wh_ti*v_inn_per_cs))/SWV_no_of_slot_item_recs)+((v_inv_each/(v_wh_hi*v_wh_ti*v_each_per_cs))/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 1 then(v_inv_pallet/SWV_no_of_slot_item_recs)+((v_inv_case/(v_ord_hi*v_ord_ti))/SWV_no_of_slot_item_recs)+((v_inv_inner/(v_ord_hi*v_ord_ti*v_inn_per_cs))/SWV_no_of_slot_item_recs)+((v_inv_each/(v_ord_hi*v_ord_ti*v_each_per_cs))/SWV_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 2 then(v_inv_pallet/SWV_no_of_slot_item_recs)+((v_inv_case/(v_ven_hi*v_ven_ti))/SWV_no_of_slot_item_recs)+((v_inv_inner/(v_ven_hi*v_ven_ti*v_inn_per_cs))/SWV_no_of_slot_item_recs)+((v_inv_each/(v_ven_hi*v_ven_ti*v_each_per_cs))/SWV_no_of_slot_item_recs)
         end;

         v_strSQLSt := 'insert into item_history (hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks)
            select hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks from #temp_item_history_inst;
      select v_recs_updtd_tmp = SQL%%ROWCOUNT ';
         EXECUTE IMMEDIATE v_strSQLSt USING v_recs_updtd_tmp;
         v_errCode := SQLCODE;
        
         
         v_recs_updtd_tmp := 0;
         update unique_ids
         set curr_nbr = v_hist_id
         where rec_type = 'item_history';
         
         v_strSQLSt := 'delete from item_history ' ||
                    ' where hist_id in  ( select hist_id from item_history ih'
                    ||' inner join slot_item si on si.slotitem_id = ih.slotitem_id     where si.sku_id = '||v_sku_id
         ||' and si.ship_unit ='|| v_slot_ship_unit
         ||' and si.delete_mult <> 1' 
        ||''' and NVL(si.hist_match,''0x'') = '''||v_hist_match
         ||''' and NVL(si.mult_loc_grp,''0x'') = '''||v_mult_loc_grp
         ||''' and si.slot_id is not null'
        ||''' and ih.start_date = '''||v_start_date||''' and ih.end_date = '''||v_end_date 
        ||''' and ih.ship_unit = '''|| v_item_ship_unit|| ''')';
     
         EXECUTE IMMEDIATE v_strSQLSt USING v_rc_del;
         v_errCode := SQLCODE;
        
         
         v_bIs_SlotItem_good_to_delete := 1;
      end if;

      v_i_ih_nextrowid := NULL;
     
         select   NVL(min(i_ih_rowid),0) INTO v_i_ih_nextrowid from     tt_TEMP_ITEM_HISTORY_SMRY2 where    i_ih_rowid > v_i_ih_currowid;
        
      if NVL(v_i_ih_nextrowid,0) = 0 then
         EXIT;
      end if;
    
         select   i_ih_rowid, ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date INTO v_i_ih_currowid,v_item_ship_unit,v_pallete_pattern,v_nbr_of_picks,v_mov_pallet,
         v_mov_case,v_mov_inner,v_mov_each,v_inv_pallet,v_inv_case,v_inv_inner,
         v_inv_each,v_start_date,v_end_date from  tt_TEMP_ITEM_HISTORY_SMRY2  where  i_ih_rowid = v_i_ih_nextrowid;
        
   END LOOP; 
   
   
   if (v_bIs_SlotItem_good_to_delete  = 1) then
      
      delete from slot_item_score
      where slot_item_score.slotitem_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null
      and si.slotitem_id  <> v_min_slotitem_id);
      DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# in hn_failure...');
      delete from hn_failure
      where hn_failure.slotitem_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null
      and si.slotitem_id  <> v_min_slotitem_id);
      DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# in haves_needs...');
      delete from haves_needs
      where haves_needs.slot_item_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null
      and si.slotitem_id  <> v_min_slotitem_id);
      DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# in needs_results_dtl...');
      delete from needs_results_dtl
      where needs_results_dtl.slotitem_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null
      and si.slotitem_id  <> v_min_slotitem_id);
      DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# in sl_failure_reasons...');
      delete from sl_failure_reasons
      where sl_failure_reasons.slotitem_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null
      and si.slotitem_id  <> v_min_slotitem_id);
      
      
      
    v_strSQLSt := 'delete from item_history ' ||
          ' where hist_id in  ( select hist_id from item_history ih'
          ||' inner join slot_item si on si.slotitem_id = ih.slotitem_id   where si.sku_id = '||v_sku_id
         ||' and si.ship_unit ='|| v_slot_ship_unit
         ||' and si.delete_mult <> 1' 
        ||''' and NVL(si.hist_match,''0x'') = '''||v_hist_match
         ||''' and NVL(si.mult_loc_grp,''0x'') = '''||v_mult_loc_grp
         ||''' and si.slot_id is not null'
        ||''' and ih.start_date = '''||v_start_date||''' and ih.end_date = '''||v_end_date 
        ||''' and ih.ship_unit = '''|| v_item_ship_unit|| ''')';

        END IF ;
    
      EXECUTE IMMEDIATE v_strSQLSt USING v_rc_del;
      v_errCode := SQLCODE;
     
   
 
END ;

/
--------------------------------------------------------
--  DDL for Procedure SPSHIPPINGMOVEMENTLENGTHSUM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SPSHIPPINGMOVEMENTLENGTHSUM (v_range_group_id IN NUMBER, v_refcur OUT SYS_REFCURSOR)
   as
   v_movement  NUMBER(15,2);
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE12');
   select   count(*) INTO v_no_of_items from tt_RANGE_IDS_TABLE12;
   v_movement := 0.0;

   begin
      select   sum(case
      when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
         case
         when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
         case
         when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
      left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from tt_RANGE_IDS_TABLE12);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;


   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))*pallets.length
            else 0.0
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
               get_case_movement(est_movement,case_mov,use_estimated_hist)*so_item_master.case_len
            else 0.0
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*so_item_master.inn_len
            else 0.0
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))*so_item_master.each_len
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE12);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE12 ';

   if (v_movement >= 0) then

      open v_refcur for select v_movement  from dual;
   else
      v_movement := -0.1;
      open v_refcur for select v_movement  from dual;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SPSHIPPINGMOVEMENTLENGTHSUM2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SPSHIPPINGMOVEMENTLENGTHSUM2 (v_range_group_id IN NUMBER,
    v_movement IN OUT NUMBER) as
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'RANGE_IDS_TABLE13');
   select   count(*) INTO v_no_of_items from RANGE_IDS_TABLE13;
   v_movement := 0.0;

   begin
      select   sum(case
      when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
         case
         when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
         case
         when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
      left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from RANGE_IDS_TABLE13);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;


   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))*pallets.length
            else 0.0
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
               get_case_movement(est_movement,case_mov,use_estimated_hist)*so_item_master.case_len
            else 0.0
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*so_item_master.inn_len
            else 0.0
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))*so_item_master.each_len
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from RANGE_IDS_TABLE13);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE RANGE_IDS_TABLE13 ';

   if (v_movement < 0) then

      v_movement := -0.1;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SPUPDATESHIPUNITFORDEFCASEIMPT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SPUPDATESHIPUNITFORDEFCASEIMPT (v_whse_code IN NVARCHAR2,
                                    v_shp_unit IN NVARCHAR2,
                                    v_row_number IN NUMBER) as
BEGIN

   update import_temp
   set ship_unit = v_shp_unit
   WHERE whse_code = v_whse_code and
   row_num = v_row_number and
   ship_unit is NULL  or ship_unit = '';--SQLWAYS_EVAL# ('P','C','I','E','B')

END;

/
--------------------------------------------------------
--  DDL for Procedure SP_ALL_PICK_LINE_CALCULATION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_ALL_PICK_LINE_CALCULATION (v_pickline_group_id IN NUMBER,
    v_item_attrubute IN VARCHAR2,
    v_whse_code IN NVARCHAR2,
    v_TotalMovement IN NUMBER) as
   v_plg  NUMBER(10,0);
   v_range_group_id  NUMBER(10,0);
   v_sql_statement_string  VARCHAR2(255);
   SWV_TotalMovement NUMBER(15,2);

   CURSOR cur_plgids IS SELECT PLGID,
        NodeID
   FROM tt_TBLPLGIDS3;

   v_TotalActualPercent  NUMBER(15,2);
   v_system  NUMBER(10,0);
   v_metric_unit  NUMBER(10,0);
   v_metric_vol  NUMBER(10,0);

   v_cube_conversion  NUMBER(13,4);
   v_exp  NUMBER(13,4);
   v_base  NUMBER(13,4);
BEGIN
   SWV_TotalMovement := v_TotalMovement;


--SQLWAYS_EVAL# DECIMAL(15,2)
   SWV_TotalMovement := '.';

--SQLWAYS_EVAL# to pickline groups
   INSERT INTO tt_TBLPLGIDS3
   SELECT td.node_id, td.pickline_grp_id
   FROM tree_dtl td
   WHERE td.pickline_grp_id = v_pickline_group_id;
-- IN (
--SQLWAYS_EVAL# ine_grp_id
--SQLWAYS_EVAL# e_group)
--SQLWAYS_EVAL# IS NOT NULL


--SQLWAYS_EVAL# one at a time
   OPEN cur_plgids;
   FETCH cur_plgids INTO v_plg,v_range_group_id;
   WHILE cur_plgids%FOUND  LOOP

      v_TotalActualPercent := '.';

      if (v_item_attrubute = 'SQLWAYS_EVAL# ent') then

         sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE');
         begin
            select   sum(case
            when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
               case
               when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))
               else 0.0
               end
            when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
               case
               when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
                  get_case_movement(est_movement,case_mov,use_estimated_hist)
               else 0.0
               end
            when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
               case
               when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)*so_item_master.inn_per_cs)
               else 0.0
               end
            when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
               case
               when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist)*so_item_master.each_per_cs)
               else 0.0
               end
            else 0.0
            end) INTO v_TotalActualPercent from slot_item
            inner join slot  on slot.slot_id = slot_item.slot_id
            left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
            and slot.my_range in(select * from tt_RANGE_IDS_TABLE);
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
         EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
      else
         if (v_item_attrubute = 'Hits') then

            sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE');
            v_TotalActualPercent := '.';
            begin
               select   sum(case
               when est_hits > 0.0 then est_hits
               when calc_hits > 0.0 then calc_hits
               else 0.0
               end) INTO v_TotalActualPercent from slot_item
               inner join slot on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
               and slot.my_range in(select * from tt_RANGE_IDS_TABLE);
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            end;
            EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
         else
            if (v_item_attrubute = 'Slotting weight') then

               sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE');
               v_TotalActualPercent := '.';
               begin
                  select   sum(case
                  when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
                     case
                     when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
                     when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
                     when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
                     else 0.0
                     end
                  when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
                     case
                     when so_item_master.case_wt > 0 then
                        so_item_master.case_wt
                     else 0.0
                     end
                  when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# unit*/
                     case
                     when so_item_master.inn_wt > 0 then
                        so_item_master.inn_wt
                     else 0.0
                     end
                  when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# unit*/
                     case
                     when so_item_master.each_wt > 0.0 then
                        so_item_master.each_wt
                     else 0.0
                     end
                  when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# nit*/
                     case slot_item.bin_unit
                     when 2 then /*SQLWAYS_EVAL# se*/
                          (((100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.case_wt)+bins.weight
                     when 4 then /*SQLWAYS_EVAL# ner*/
                        (((100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.inn_wt)+bins.weight
                     when 8 then /*SQLWAYS_EVAL# ch*/
                        (((100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.each_wt)+bins.weight
                     else 0.0
                     end
                  else 0.0
                  end) INTO v_TotalActualPercent from slot_item
                  inner join slot on slot_item.slot_id = slot.slot_id
                  left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
                  left outer join pallets on pallets.pallet_id = slot_item.current_pallet
                  left outer join bins on bins.bin_id = slot_item.current_bin where slot_item.slot_id is not null and slot_item.sku_id is not null
                  and slot.my_range in(select * from tt_RANGE_IDS_TABLE);
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
               end;
               EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
            else
               if (v_item_attrubute = 'Shipping weight') then

                  sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE');
                  v_TotalActualPercent := '.';
                  begin
                     select   sum(case
                     when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
                        case
                        when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
                        when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
                        when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
                        else 0.0
                        end
                     when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
                        case
                        when so_item_master.case_wt > 0 then
                           so_item_master.case_wt
                        else 0.0
                        end
                     when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
                        case
                        when so_item_master.inn_wt > 0 then
                           so_item_master.inn_wt
                        else 0.0
                        end
                     when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
                        case
                        when so_item_master.each_wt > 0.0 then
                           so_item_master.each_wt
                        else 0.0
                        end
                     else 0.0
                     end) INTO v_TotalActualPercent from slot_item
                     inner join slot on slot_item.slot_id = slot.slot_id
                     left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
                     left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
                     and slot.my_range in(select * from tt_RANGE_IDS_TABLE);
                     EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  end;
                  EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
               else
                  if (v_item_attrubute = 'Cube movement') then


                     sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE');
                     begin
                        select   whprefs_system, metric_unit, metric_vol INTO v_system,v_metric_unit,v_metric_vol from whprefs where whse_code = v_whse_code;
                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           NULL;
                     end;

    --SQLWAYS_EVAL# of unit is english
                     IF v_system = 1 then

                        v_cube_conversion := POWER(12,3);
                     ELSE
                        v_exp := v_metric_vol -v_metric_unit;
                        v_base := POWER(10,v_exp);
                        v_cube_conversion := POWER(v_base,3);
                     end if;
                     begin
                        select   sum(case
                        when v_system = 1 then /*Case Cube unit*/
                           case
                           when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
                           else 0.0
                           end
                        when v_system = 2 then /*SQLWAYS_EVAL# unit*/
                           case
                           when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
                              case
                              when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
                              else 0.0
                              end
                           when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
                              case
                              when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
                              else 0.0
                              end
                           when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
                              case
                              when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*((so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/v_cube_conversion)
                              else 0.0
                              end
                           when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
                              case
                              when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))*((so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/v_cube_conversion)
                              else 0.0
                              end
                           else 0.0
                           end
                        else 0.0
                        end) INTO v_TotalActualPercent from slot_item
                        inner join slot  on slot_item.slot_id = slot.slot_id
                        left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
                        and slot.my_range in(select * from tt_RANGE_IDS_TABLE);
                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           NULL;
                     end;
                     EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
                  else
                     if (v_item_attrubute = 'Case movement') then

                        sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE');
                        v_TotalActualPercent := '.';
                        begin
                           select   sum(case
                           when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
                              get_case_movement(est_movement,case_mov,use_estimated_hist)
                           else 0.0
                           end) INTO v_TotalActualPercent from slot_item
                           inner join slot  on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
                           and slot.my_range in(select * from  tt_RANGE_IDS_TABLE);
                           EXCEPTION
                           WHEN NO_DATA_FOUND THEN
                              NULL;
                        end;
                        EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
                     else
                        if (v_item_attrubute = 'SQLWAYS_EVAL# ent') then

                           sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE');
                           v_TotalActualPercent := '.';
                           begin
                              select   sum(case
                              when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
                                 case
                                 when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                                    get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)
                                 else 0.0
                                 end
                              when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
                                 case
                                 when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                                    get_case_movement(est_movement,case_mov,use_estimated_hist)
                                 else 0.0
                                 end
                              when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# movement*/
                                 case
                                 when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                                    get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)
                                 else 0.0
                                 end
                              when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# movement*/
                                 case
                                 when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                                    get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist)
                                 else 0.0
                                 end
                              when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# movement .. handle differently.*/
                                 case
                                 when slot_item.est_movement > 0.0 then /*SQLWAYS_EVAL# movement*/
                                    case slot_item.bin_unit
                                    when 2 then /*SQLWAYS_EVAL# se*/
                                  (100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
                                    when 4 then /*SQLWAYS_EVAL# ner*/
                                (100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
                                    when 8 then /*SQLWAYS_EVAL# ch*/
                                (100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
                                    else 0.0
                                    end
                                 when slot_item.bin_mov > 0.0 then /*SQLWAYS_EVAL# ement*/
                                    slot_item.bin_mov
                                 else 0.0
                                 end
                              else 0.0
                              end) INTO v_TotalActualPercent from slot_item
                              inner join slot on slot_item.slot_id = slot.slot_id
                              left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
                              left outer join bins on slot_item.current_bin = bins.bin_id where slot_item.slot_id is not null and slot_item.sku_id is not null
                              and slot.my_range in(select * from tt_RANGE_IDS_TABLE);
                              EXCEPTION
                              WHEN NO_DATA_FOUND THEN
                                 NULL;
                           end;
                           EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
                        else
                           if (v_item_attrubute = 'SQLWAYS_EVAL# * Shipping length') then

                              sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE');
                              v_TotalActualPercent := '.';
                              begin
                                 select   sum(case
                                 when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
                                    case
                                    when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))*pallets.length
                                    else 0.0
                                    end
                                 when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
                                    case
                                    when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
                                       get_case_movement(est_movement,case_mov,use_estimated_hist)*so_item_master.case_len
                                    else 0.0
                                    end
                                 when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
                                    case
                                    when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*so_item_master.inn_len
                                    else 0.0
                                    end
                                 when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
                                    case
                                    when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)*so_item_master.each_per_cs)*so_item_master.each_len
                                    else 0.0
                                    end
                                 else 0.0
                                 end) INTO v_TotalActualPercent from slot_item
                                 inner join slot on slot_item.slot_id = slot.slot_id
                                 left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
                                 left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
                                 and slot.my_range in(select * from tt_RANGE_IDS_TABLE);
                                 EXCEPTION
                                 WHEN NO_DATA_FOUND THEN
                                    NULL;
                              end;
                              EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
                           end if;
                        end if;
                     end if;
                  end if;
               end if;
            end if;
         end if;
      end if;
      SWV_TotalMovement := SWV_TotalMovement+v_TotalActualPercent;
      FETCH cur_plgids INTO v_plg,v_range_group_id;
   END LOOP;
   CLOSE cur_plgids;





--end



   v_sql_statement_string := 'SQLWAYS_EVAL# OVEMENT' ||
   ' VALUES( '  ||
   SUBSTR(CAST(SWV_TotalMovement AS CHAR),1,20) ||
   ' )';
   EXECUTE IMMEDIATE v_sql_statement_string;
END;



--SQLWAYS_EVAL# > 0)
--begin
--SQLWAYS_EVAL# Movement
--end
--else
--begin
--SQLWAYS_EVAL# = 0.00
--SQLWAYS_EVAL# Movement
--end

/
--------------------------------------------------------
--  DDL for Procedure SP_CALCULATE_OPTIMAL_VOLUME
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_CALCULATE_OPTIMAL_VOLUME (v_whse_code IN CHAR) as
   v_wh_total_volume  NUMBER(26,8);
   v_sumflowrates  NUMBER(15,4);
   v_flowrate  NUMBER(15,4);
   v_spid  NUMBER(15,4);
   v_status  CHAR(1);
   v_system  NUMBER(15,4);
   v_metric_unit  NUMBER(15,4);
   v_metric_vol  NUMBER(15,4);
   v_cube_conversion  NUMBER(13,4);
   v_base  NUMBER(15,4);
   v_exp  NUMBER(15,4);
   v_case_mov  NUMBER(15,4);
   v_pallet_mov  NUMBER(15,4);
   v_inner_mov  NUMBER(15,4);
   v_each_mov  NUMBER(15,4);
   v_bin_mov  NUMBER(15,4);
   v_item_cube  NUMBER(15,4);
   v_case_ht  NUMBER(9,4);
   v_case_wid  NUMBER(9,4);
   v_case_len  NUMBER(9,4);
   v_flow_rate  NUMBER(36,15);
   v_sum_sqrt_flow_rate  NUMBER(15,4);
   v_slot_id  NUMBER(19,0);
   v_sku_id  NUMBER(19,0);
   v_slotitem_id  NUMBER(19,0);
   v_cube_mvmt_dimensions  NUMBER(15,4);
   v_ship_unit  NUMBER(15,4);
   v_ship_move  NUMBER(15,4);
   v_inn_ht  NUMBER(15,4);
   v_inn_wid  NUMBER(15,4);
   v_inn_len  NUMBER(15,4);
   v_each_ht  NUMBER(15,4);
   v_each_len  NUMBER(15,4);
   v_each_wid  NUMBER(15,4);
   v_ven_hi  NUMBER(15,4);
   v_ven_ti  NUMBER(15,4);
   v_wh_ti  NUMBER(15,4);
   v_wh_hi  NUMBER(15,4);
   v_ord_hi  NUMBER(15,4);
   v_ord_ti  NUMBER(15,4);
   v_pallete_pattern  NUMBER(15,4);
   v_pallet_value  NUMBER(15,4);
   v_wh_cas_inn_each_volume  NUMBER(26,8);
   v_wh_volume  NUMBER(36,15);
   v_wh_bin_volume  NUMBER(26,8);
   v_wh_pal_volume  NUMBER(26,8);
   SWO_RowCount  NUMBER(15,4) DEFAULT NULL;
   v_if_exists NUMBER(15,4);
BEGIN

   v_spid := SYS_CONTEXT('userenv','sessionid');

   begin
      select   whprefs_system, metric_unit, metric_vol INTO v_system,v_metric_unit,v_metric_vol FROM    whprefs WHERE   whse_code = v_whse_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

    --SQLWAYS_EVAL# of unit is english
   IF v_system = 1 then

      v_cube_conversion := POWER(12,3);
   ELSE
      v_exp := v_metric_vol -v_metric_unit;
      v_base := POWER(10,v_exp);
      v_cube_conversion := POWER(v_base,3);
   end if;


   v_wh_volume := 0.0;
   v_wh_cas_inn_each_volume := 0.0;
   v_wh_bin_volume := 0.0;
   v_wh_pal_volume := 0.0;
   v_wh_total_volume := 0.0;
   begin
      select   sum(CASE
      WHEN slot_item.slot_unit = 2 AND slot_item.cur_orientation = 0 THEN rack_type_level.max_overhang_pct*so_item_master.case_len/100 -- case
      WHEN slot_item.slot_unit = 2 AND slot_item.cur_orientation = 1 THEN rack_type_level.max_overhang_pct*so_item_master.case_wid/100 --
      WHEN slot_item.slot_unit = 2 AND slot_item.cur_orientation = 2 THEN rack_type_level.max_overhang_pct*so_item_master.case_ht/100 --
      WHEN slot_item.slot_unit = 2 AND slot_item.cur_orientation = 3 THEN rack_type_level.max_overhang_pct*so_item_master.case_wid/100 --
      WHEN slot_item.slot_unit = 2 AND slot_item.cur_orientation = 4 THEN rack_type_level.max_overhang_pct*so_item_master.case_ht/100 --
      WHEN slot_item.slot_unit = 2 AND slot_item.cur_orientation = 5 THEN rack_type_level.max_overhang_pct*so_item_master.case_len/100 --
      WHEN slot_item.slot_unit = 4 AND slot_item.cur_orientation = 0 THEN rack_type_level.max_overhang_pct*so_item_master.inn_len/100 -- inner
      WHEN slot_item.slot_unit = 4 AND slot_item.cur_orientation = 1 THEN rack_type_level.max_overhang_pct*so_item_master.inn_wid/100 --
      WHEN slot_item.slot_unit = 4 AND slot_item.cur_orientation = 2 THEN rack_type_level.max_overhang_pct*so_item_master.inn_ht/100 --
      WHEN slot_item.slot_unit = 4 AND slot_item.cur_orientation = 3 THEN rack_type_level.max_overhang_pct*so_item_master.inn_wid/100 --
      WHEN slot_item.slot_unit = 4 AND slot_item.cur_orientation = 4 THEN rack_type_level.max_overhang_pct*so_item_master.inn_ht/100 --
      WHEN slot_item.slot_unit = 4 AND slot_item.cur_orientation = 5 THEN rack_type_level.max_overhang_pct*so_item_master.inn_len/100 --
      WHEN slot_item.slot_unit = 8 AND slot_item.cur_orientation = 0 THEN rack_type_level.max_overhang_pct*so_item_master.each_len/100 -- Each
      WHEN slot_item.slot_unit = 8 AND slot_item.cur_orientation = 1 THEN rack_type_level.max_overhang_pct*so_item_master.each_wid/100 --
      WHEN slot_item.slot_unit = 8 AND slot_item.cur_orientation = 2 THEN rack_type_level.max_overhang_pct*so_item_master.each_ht/100 --
      WHEN slot_item.slot_unit = 8 AND slot_item.cur_orientation = 3 THEN rack_type_level.max_overhang_pct*so_item_master.each_wid/100 --
      WHEN slot_item.slot_unit = 8 AND slot_item.cur_orientation = 4 THEN rack_type_level.max_overhang_pct*so_item_master.each_ht/100 --
      WHEN slot_item.slot_unit = 8 AND slot_item.cur_orientation = 5 THEN rack_type_level.max_overhang_pct*so_item_master.each_len/100 --
      END) INTO v_wh_cas_inn_each_volume from  rack_type_level, slot, slot_item, so_item_master WHERE
      rack_type_level.rack_type = slot.rack_type and
      rack_type_level.rack_level_id = slot.rack_level_id and
      slot.slot_id = slot_item.slot_id and
      slot_item.sku_id = so_item_master.sku_id and
      slot.whse_code = v_whse_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
    --SQLWAYS_EVAL# in Pallets
   begin
      select   sum(CASE
      WHEN slot_item.slot_unit = 1 AND slot_item.cur_orientation = 0 THEN rack_type_level.max_overhang_pct*pallets.length/100 -- pallet
      WHEN slot_item.slot_unit = 1 AND slot_item.cur_orientation = 1 THEN rack_type_level.max_overhang_pct*pallets.width/100 --
      WHEN slot_item.slot_unit = 1 AND slot_item.cur_orientation = 2 THEN rack_type_level.max_overhang_pct*pallets.height/100 --
      WHEN slot_item.slot_unit = 1 AND slot_item.cur_orientation = 3 THEN rack_type_level.max_overhang_pct*pallets.width/100 --
      WHEN slot_item.slot_unit = 1 AND slot_item.cur_orientation = 4 THEN rack_type_level.max_overhang_pct*pallets.height/100 --
      WHEN slot_item.slot_unit = 1 AND slot_item.cur_orientation = 5 THEN rack_type_level.max_overhang_pct*pallets.length/100 --
      END) INTO v_wh_pal_volume from  rack_type_level, slot, slot_item, so_item_master, pallets WHERE
      rack_type_level.rack_type = slot.rack_type and
      rack_type_level.rack_level_id = slot.rack_level_id and
      slot.slot_id = slot_item.slot_id and
      slot_item.sku_id = so_item_master.sku_id and
      slot_item.current_pallet = pallets.pallet_id and
      slot.whse_code = v_whse_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
    --SQLWAYS_EVAL# in Bins
   begin
      select   sum(CASE
      WHEN slot_item.slot_unit = 16 AND slot_item.cur_orientation = 0 THEN rack_type_level.max_overhang_pct*bins.length/100 -- bins
      WHEN slot_item.slot_unit = 16 AND slot_item.cur_orientation = 1 THEN rack_type_level.max_overhang_pct*bins.width/100 --
      WHEN slot_item.slot_unit = 16 AND slot_item.cur_orientation = 2 THEN rack_type_level.max_overhang_pct*bins.height/100 --
      WHEN slot_item.slot_unit = 16 AND slot_item.cur_orientation = 3 THEN rack_type_level.max_overhang_pct*bins.width/100 --
      WHEN slot_item.slot_unit = 16 AND slot_item.cur_orientation = 4 THEN rack_type_level.max_overhang_pct*bins.height/100 --
      WHEN slot_item.slot_unit = 16 AND slot_item.cur_orientation = 5 THEN rack_type_level.max_overhang_pct*bins.length/100 --
      END) INTO v_wh_bin_volume from  rack_type_level, slot, slot_item, so_item_master, bins WHERE
      rack_type_level.rack_type = slot.rack_type and
      rack_type_level.rack_level_id = slot.rack_level_id and
      slot.slot_id = slot_item.slot_id and
      slot_item.sku_id = so_item_master.sku_id and
      slot_item.current_bin = bins.bin_id and
      slot.whse_code = v_whse_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   begin
      select   SUM(ht*width*cast(depth/v_cube_conversion as NUMBER)) INTO v_wh_volume FROM slot WHERE whse_code = v_whse_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;


    --SQLWAYS_EVAL# the below part for MACR00008736
    --SQLWAYS_EVAL# = @wh_volume + @wh_cas_inn_each_volume + @wh_bin_volume + @wh_pal_volume
   IF (v_wh_volume > 0) then

      v_wh_total_volume := v_wh_total_volume+v_wh_volume;
   end if;


   IF (v_wh_cas_inn_each_volume > 0) then

      v_wh_total_volume := v_wh_total_volume+v_wh_cas_inn_each_volume;
   end if;

   IF (v_wh_bin_volume > 0) then

      v_wh_total_volume := v_wh_total_volume+v_wh_bin_volume;
   end if;

   IF (v_wh_pal_volume > 0) then

      v_wh_total_volume := v_wh_total_volume+v_wh_pal_volume;
   end if;
    --end


    --SQLWAYS_EVAL# table was deleted as part of database cleanup, but it was required here.
   SELECT   COUNT(*) INTO v_if_exists FROM user_tables WHERE table_name = 'CASCADE CONSTRAINTS';
   if v_if_exists > 0 then
      EXECUTE IMMEDIATE 'SQLWAYS_EVAL# CASCADE CONSTRAINTS';
   end if;


   DELETE slot_item_temp
   WHERE spid = v_spid;
   INSERT INTO slot_item_temp(whse_code,
        slot_id,
        sku_id,
        slotitem_id,
        ship_unit,
        flow_rate,
        case_mov,
        bin_mov,
        pallet_mov,
        inner_mov,
        each_mov,
        pallete_pattern,
        opt_vol,
        status,
        spid)
   SELECT  v_whse_code,
        slotitem.slot_id,
        slotitem.sku_id,
        slotitem.slotitem_id,
        slotitem.ship_unit,
        NULL,
        get_case_movement(slotitem.est_movement,case_mov,slotitem.use_estimated_hist) ,
        get_bin_movement(slotitem.est_movement,bin_mov,slotitem.use_estimated_hist),
        get_pallet_movement(item.sku_id,pallete_pattern,pallet_mov,slotitem.est_movement,slotitem.use_estimated_hist)  ,
        get_inner_movement(item.inn_per_cs,slotitem.est_movement,inner_mov,slotitem.use_estimated_hist) ,
        get_each_movement(item.each_per_cs,slotitem.est_movement,each_mov,slotitem.use_estimated_hist),
        slotitem.pallete_pattern,
        NULL,
        'N',
        v_spid
   FROM    slot_item slotitem, so_item_master item
   WHERE   item.whse_code = v_whse_code
   AND    item.sku_id = slotitem.sku_id
   AND slotitem.delete_mult = 0;

    /*SQLWAYS_EVAL# code is for the feature done in office depot where there are items
    that have both a case and  each record slotted to the same location.
    updation of opt_vol column to -999, in case records for such items is taken care below*/

   UPDATE slot_item_temp slotitem1 SET(status,opt_vol,flow_rate) =(SELECT distinct 'Y',-999,-999 FROM  slot_item_temp slotitem2 WHERE slotitem1.slot_id = slotitem2.slot_id
   AND   slotitem1.ship_unit <> slotitem2.ship_unit
   AND   slotitem1.ship_unit <> 8
   AND   slotitem1.slot_id IS NOT NULL) WHERE ROWID IN(SELECT slotitem1.ROWID FROM slot_item_temp slotitem1, slot_item_temp slotitem2 WHERE slotitem1.slot_id = slotitem2.slot_id
   AND   slotitem1.ship_unit <> slotitem2.ship_unit
   AND   slotitem1.ship_unit <> 8
   AND   slotitem1.slot_id IS NOT NULL);

    /*SQLWAYS_EVAL# movement is based on case dimensions or shipping dimensions
    from the history preferences*/

   begin
      select   cube_mvmt_dimensions INTO v_cube_mvmt_dimensions FROM   whprefs WHERE  whse_code = v_whse_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   v_status := '';
   v_slot_id := '';
   v_sku_id := '';
   v_slotitem_id := '';

   SWO_RowCount := 1;
   begin
      select   status, slot_id, sku_id, slotitem_id, case_mov, bin_mov, pallet_mov, each_mov, inner_mov, ship_unit, pallete_pattern INTO v_status,v_slot_id,v_sku_id,v_slotitem_id,v_case_mov,v_bin_mov,v_pallet_mov,
      v_each_mov,v_inner_mov,v_ship_unit,v_pallete_pattern FROM slot_item_temp WHERE status = 'N'
      AND   spid = v_spid;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   SWO_RowCount := 0;

   WHILE (v_status = 'N') LOOP
      begin
         select   case_ht, case_wid, case_len, inn_ht, inn_wid, inn_len, each_ht, each_len, each_wid, ven_hi, ven_ti, wh_ti, wh_hi, ord_hi, ord_ti INTO v_case_ht,v_case_wid,v_case_len,v_inn_ht,v_inn_wid,v_inn_len,v_each_ht,
         v_each_len,v_each_wid,v_ven_hi,v_ven_ti,v_wh_ti,v_wh_hi,v_ord_hi,v_ord_ti FROM    so_item_master WHERE     whse_code = v_whse_code
         AND    sku_id = v_sku_id;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;

        /*SQLWAYS_EVAL# is based on case dimensions*/
      IF (v_cube_mvmt_dimensions = 0) then

       --  v_item_cube := v_case_ht*v_case_wid*cast(v_case_len/v_cube_conversion as DECIMAL(15,4));
          v_item_cube := v_case_ht*v_case_wid*v_case_len/v_cube_conversion ;
         IF (v_case_mov < 0.0) then
            v_flow_rate := -999;
         ELSE
            IF (v_case_mov = 0.0) then
               v_flow_rate := 0;
            ELSE
               v_flow_rate :=(v_item_cube*v_case_mov);
            end if;
         end if;
      end if;
        /*SQLWAYS_EVAL# is based on shipping dimensions*/
      IF (v_cube_mvmt_dimensions = 1) then

         IF (v_ship_unit = 2) then

            v_ship_move := v_case_mov;
            v_item_cube := v_case_ht*v_case_wid*v_case_len/v_cube_conversion;
         ELSE
            IF (v_ship_unit = 4) then

               v_ship_move := v_inner_mov;
               v_item_cube := v_inn_ht*v_inn_wid*v_inn_len/v_cube_conversion;
            ELSE
               IF (v_ship_unit = 8) then

                  v_ship_move := v_each_mov;
                  v_item_cube := v_each_ht*v_each_wid*v_each_len/v_cube_conversion;
               ELSE
                  IF (v_ship_unit = 1) then

                     v_ship_move := v_pallet_mov;
                     IF (v_pallete_pattern = 0) then
                        v_pallet_value := v_wh_ti*v_wh_hi;
                     ELSE
                        IF (v_pallete_pattern = 1) then
                           v_pallet_value := v_ord_ti*v_ord_hi;
                        ELSE
                           IF (v_pallete_pattern = 2) then
                              v_pallet_value := v_ven_ti*v_ven_hi;
                           end if;
                        end if;
                     end if;
                     v_item_cube := v_pallet_value*(v_case_ht*v_case_wid*v_case_len/v_cube_conversion);
                  ELSE
                     v_ship_move := v_case_mov;
                     v_item_cube := v_case_ht*v_case_wid*v_case_len/v_cube_conversion;
                  end if;
               end if;
            end if;
         end if;
         IF (v_ship_move < 0.0) then
            v_flow_rate := -999;
         ELSE
            IF (v_ship_move = 0.0) then
               v_flow_rate := 0;
            ELSE
               v_flow_rate := v_item_cube*v_ship_move;
            end if;
         end if;
      end if;
      UPDATE    slot_item_temp
      SET       flow_rate = v_flow_rate,status = 'Y'
      WHERE status = 'N'
      AND   spid = v_spid
      AND   NVL(slot_id,' ')  = NVL(v_slot_id,' ')
      AND   sku_id = v_sku_id
      AND   slotitem_id = v_slotitem_id;
      v_status := '';
      v_slot_id := '';
      v_sku_id := '';
      v_slotitem_id := '';
      SWO_RowCount := 1;
      begin
         select   status, slot_id, sku_id, slotitem_id, case_mov, bin_mov, pallet_mov, each_mov, inner_mov, ship_unit, pallete_pattern INTO v_status,v_slot_id,v_sku_id,v_slotitem_id,v_case_mov,v_bin_mov,v_pallet_mov,
         v_each_mov,v_inner_mov,v_ship_unit,v_pallete_pattern FROM slot_item_temp WHERE status = 'N'
         AND   spid = v_spid;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      SWO_RowCount := 0;
   END LOOP;
   begin
      select   SUM(SQRT(flow_rate)) INTO v_sum_sqrt_flow_rate FROM   slot_item_temp WHERE  spid = SYS_CONTEXT('userenv','sessionid')
      AND    flow_rate > 0;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   UPDATE slot_item_temp
   SET    opt_vol = 0
   WHERE  spid = SYS_CONTEXT('userenv','sessionid')
   AND    flow_rate = 0;

   UPDATE slot_item_temp
   SET    opt_vol = -999
   WHERE  spid = SYS_CONTEXT('userenv','sessionid')
   AND    flow_rate < 0;

   UPDATE slot_item_temp
   SET    opt_vol =(SQRT(flow_rate)/v_sum_sqrt_flow_rate)*v_wh_total_volume
   WHERE  spid = SYS_CONTEXT('userenv','sessionid')
   AND    opt_vol IS NULL;

   UPDATE slot_item slotitem SET(opt_fluid_vol) =(SELECT distinct opt_vol FROM  slot_item_temp tmp WHERE  tmp.sku_id = slotitem.sku_id
   AND    NVL(tmp.slot_id,' ')  = NVL(slotitem.slot_id,' ')
   AND    tmp.slotitem_id = slotitem.slotitem_id
   AND    tmp.spid = v_spid
   AND    tmp.opt_vol IS NOT NULL) WHERE ROWID IN(SELECT slotitem.ROWID FROM   slot_item slotitem, slot_item_temp tmp WHERE  tmp.sku_id = slotitem.sku_id
   AND    NVL(tmp.slot_id,' ')  = NVL(slotitem.slot_id,' ')
   AND    tmp.slotitem_id = slotitem.slotitem_id
   AND    tmp.spid = v_spid
   AND    tmp.opt_vol IS NOT NULL);

   DELETE slot_item_temp
   WHERE spid = v_spid;


END;

/
--------------------------------------------------------
--  DDL for Procedure SP_CASE_MOVEMENT_SUM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_CASE_MOVEMENT_SUM (v_range_group_id IN NUMBER, v_refcur OUT SYS_REFCURSOR)
   as
   v_movement  NUMBER(15,2);
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE2');
   select   count(*) INTO v_no_of_items from tt_RANGE_IDS_TABLE2;
   v_movement := 0.0;

   begin
      select   sum(case
      when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
         0
      else -1
      end) INTO v_movement from slot_item
      inner join slot  on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from  tt_RANGE_IDS_TABLE2);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
            get_case_movement(est_movement,case_mov,use_estimated_hist)
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot  on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from  tt_RANGE_IDS_TABLE2);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE2 ';

   if (v_movement >= 0) then

      open v_refcur for select v_movement  from dual;
   else
      v_movement := -0.1;
      open v_refcur for select v_movement  from dual;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_CASE_MOVEMENT_SUM2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_CASE_MOVEMENT_SUM2 (v_range_group_id IN NUMBER,
    v_movement IN OUT NUMBER) as
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'RANGE_IDS_TABLE3');
   select   count(*) INTO v_no_of_items from RANGE_IDS_TABLE3;
   v_movement := 0.0;

   begin
      select   sum(case
      when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
         0
      else -1
      end) INTO v_movement from slot_item
      inner join slot  on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from  RANGE_IDS_TABLE3);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
            get_case_movement(est_movement,case_mov,use_estimated_hist)
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot  on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from  RANGE_IDS_TABLE3);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE RANGE_IDS_TABLE3 ';

   if (v_movement < 0) then

      v_movement := -0.1;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_COLUMN_UPDATE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_COLUMN_UPDATE (p_session_id IN OUTPUT_RESULT_TABLE.sessionid%type,p_process_id IN OUTPUT_RESULT_TABLE.processid%type,p_flag IN NUMBER)
IS
BEGIN

 IF p_flag = 1 THEN
   UPDATE output_result_table
       SET server_mod_date_time = SYSDATE
       WHERE sessionid = p_session_id AND processid = p_process_id;
 ELSE
   UPDATE output_result_table
    SET ui_mod_date_time = SYSDATE
    WHERE sessionid = p_session_id AND processid = p_process_id;
 END IF;

IF SQL%FOUND THEN
  DBMS_OUTPUT.PUT_LINE('Number of records updated : '||SQL%ROWCOUNT);
ELSIF SQL%NOTFOUND THEN
    DBMS_OUTPUT.PUT_LINE('ZERO RECORDS UPDATED ');
END IF;
-- COMMIT;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('ZERO RECORDS UPDATED');
   WHEN others THEN
            DBMS_OUTPUT.PUT_LINE('Error' || SQLCODE || SQLERRM );
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_CONSOL_ITEM_HISTORY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_CONSOL_ITEM_HISTORY (v_refcur OUT SYS_REFCURSOR, v_refcur2 OUT SYS_REFCURSOR, v_refcur3 OUT SYS_REFCURSOR, v_refcur4 OUT SYS_REFCURSOR)

   as
   v_iLoopControl  NUMBER(1,0);
   v_iNextRowId  NUMBER(19,0);
   v_iMaxRowId  NUMBER(19,0);
   v_iCurrentRowId  NUMBER(10,0);
   v_slot_ship_unit  NUMBER(10,0);
   v_item_ship_unit  NUMBER(10,0);
   v_hist_id  NUMBER(19,0);
   v_sku_id  NUMBER(19,0);
   v_start_date  TIMESTAMP(3);
   v_end_date  TIMESTAMP(3);
   v_hist_match  NVARCHAR2(40);
   v_mult_loc_grp  NVARCHAR2(40);
   v_min_slotitem_id  NUMBER(19,0);
   v_no_of_slot_item_recs  NUMBER(10,0);
   v_errCode  NUMBER(10,0);
   v_if_exists NUMBER(10,0);
   SWV_Ret_Stat NUMBER(10,0);
   SWV_RCur SYS_REFCURSOR;
   SWC_Current SYS_REFCURSOR;
   variable_t  varchar2(50);
   v_cnt PLS_INTEGER := 0;
   table_name varchar2(50);
   CASE_2 varchar2(50);
BEGIN
   v_errCode := 0;
   select count(table_name) into variable_t from user_tables where table_name ='TT_TEMP_SLOT_ITEM_SLOTTED';

if v_cnt>0 then

EXECUTE IMMEDIATE('delete TT_TEMP_SLOT_ITEM_SLOTTED');

end if;

 variable_t :=0;
   if 'slot_item_unslotted' is not null then
      EXECUTE IMMEDIATE 'TRUNCATE TABLE tt_TEMP_SLOT_ITEM_UNSLOTTED';
   end if;
   if 'slot_item_interested' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_SLOT_ITEM_INTERESTED ';
   end if;
   if 'item_history_unslotted' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_ITEM_HISTORY_UNSLOTTED ';
   end if;
   if 'item_history_smry' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_ITEM_HISTORY_SMRY3 ';
   end if;
   if 'item_history_inst' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_ITEM_HISTORY_INST3 ';
   end if;
   if 'neg_mvmt_slot_item' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_NEG_MVMT_SLOT_ITEM ';
   end if;
   if 'neg_inv_slot_item' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_NEG_INV_SLOT_ITEM ';
   end if;
   if 'neg_hits_slot_item' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_NEG_HITS_SLOT_ITEM ';
   end if;
   open SWC_Current for SELECT 'SQLWAYS_EVAL# MESSAGE TAB FOR DEBUG MESSAGES' as INFO, SYSTIMESTAMP as PROCESS_START_TIME, 'v1.3 (27FEB09)' as Version  from dual;
   SWP_AssignOutRefcur4(SWC_Current,v_refcur,v_refcur2,v_refcur3,v_refcur4);
   SELECT   COUNT(*) INTO v_if_exists from version_info where CAST(version AS NUMBER(10,0)) > 80;
   if  v_if_exists = 0 then
      open SWC_Current for select 'SQLWAYS_EVAL# designed to work for Slotting 2008 and above' as ERROR  from dual;
      SWP_AssignOutRefcur4(SWC_Current,v_refcur,v_refcur2,v_refcur3,v_refcur4);
      DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# is designed to work for Slotting 2008 and above.');
      
   end if;
   DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# item history for SKUs that are currently both unslotted and slotted.');

   insert into tt_TEMP_SLOT_ITEM_UNSLOTTED(sku_id, ship_unit, hist_match, mult_loc_grp, no_of_slot_item_recs, min_slotitem_id)
   select distinct si.sku_id, NVL(si.ship_unit,0) ship_unit, NVL(si.hist_match,'0x') hist_match, NVL(si.mult_loc_grp,'0x') mult_loc_grp,
            count(1) no_of_slot_item_recs, min(si.slotitem_id) min_slotitem_id
   from slot_item  si
   where si.sku_id is not null and si.slot_id is null and si.delete_mult <> 1

   group by si.sku_id,si.ship_unit,si.hist_match,si.mult_loc_grp

   order by 1 NULLS FIRST;

   insert into tt_TEMP_SLOT_ITEM_SLOTTED(sku_id, ship_unit, hist_match, mult_loc_grp, no_of_slot_item_recs, min_slotitem_id)
   select distinct si.sku_id, NVL(si.ship_unit,0) ship_unit, NVL(si.hist_match,'0x') hist_match, NVL(si.mult_loc_grp,'0x') mult_loc_grp,
            count(1) no_of_slot_item_recs, min(si.slotitem_id) min_slotitem_id
   from slot_item  si
   where si.sku_id is not null and si.slot_id is not null and si.delete_mult <> 1

   group by si.sku_id,si.ship_unit,si.hist_match,si.mult_loc_grp

   order by 1 NULLS FIRST;

   insert into tt_TEMP_SLOT_ITEM_INTERESTED(sku_id, ship_unit, hist_match, mult_loc_grp, no_of_slot_item_recs, min_slotitem_id)
   select tsiu.sku_id, tsiu.ship_unit, tsiu.hist_match, tsiu.mult_loc_grp, tsis.no_of_slot_item_recs, tsiu.min_slotitem_id
   from tt_TEMP_SLOT_ITEM_UNSLOTTED tsiu
   inner join tt_TEMP_SLOT_ITEM_SLOTTED tsis on tsis.sku_id = tsiu.sku_id
   and tsis.ship_unit = tsiu.ship_unit
   and tsis.hist_match = tsiu.hist_match
   and tsis.mult_loc_grp = tsiu.mult_loc_grp;

   v_iLoopControl := 1;
   begin
      select   MIN(iRowId), MAX(iRowId) INTO v_iNextRowId,v_iMaxRowId FROM   tt_TEMP_SLOT_ITEM_INTERESTED;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   IF NVL(v_iNextRowId,0) = 0 then
      DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# is no group match for SKUs that are both slotted and unslotted.');
   end if;

   begin
      SELECT  iRowId, sku_id, ship_unit, hist_match, mult_loc_grp, no_of_slot_item_recs INTO v_iCurrentRowId,v_sku_id,v_slot_ship_unit,v_hist_match,v_mult_loc_grp,
      v_no_of_slot_item_recs FROM    tt_TEMP_SLOT_ITEM_INTERESTED  WHERE iRowId = v_iNextRowId AND ROWNUM <= 1;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   WHILE v_iLoopControl = 1 LOOP

      DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# group ' || SUBSTR(CAST(v_iCurrentRowId  AS VARCHAR2),1,10) || ' of ' || SUBSTR(CAST(v_iMaxRowId  AS VARCHAR2),1,10));

      if (v_errCode <> 0) then
         DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# SKU group - SKU: '
         || SUBSTR(CAST(v_sku_id  AS VARCHAR2),1,10) || 'SQLWAYS_EVAL# ' || SUBSTR(CAST(v_slot_ship_unit AS VARCHAR2),1,4) || ' , ' ||
         'hist match: ' || SUBSTR(v_hist_match,1,12)  || ' , ' ||
         'mult locn grp: ' || SUBSTR(v_mult_loc_grp,1,12));
         DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# back transaction....');
         ROLLBACK;
      end if;

      v_iNextRowId := NULL;
      begin
         select   NVL(MIN(iRowId),0) INTO v_iNextRowId FROM     tt_TEMP_SLOT_ITEM_INTERESTED WHERE    iRowId > v_iCurrentRowId;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;

      IF NVL(v_iNextRowId,0) = 0 then
         v_iLoopControl := 0;
         EXIT;
      end if;

      begin
         select   iRowId, sku_id, ship_unit, hist_match, mult_loc_grp, no_of_slot_item_recs INTO v_iCurrentRowId,v_sku_id,v_slot_ship_unit,v_hist_match,v_mult_loc_grp,
         v_no_of_slot_item_recs FROM    tt_TEMP_SLOT_ITEM_INTERESTED  WHERE  iRowId = v_iNextRowId;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   END LOOP; 
   COMMIT;
   if 'slot_item_slotted' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_SLOT_ITEM_SLOTTED ';
   end if;
   if'slot_item_unslotted' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_SLOT_ITEM_UNSLOTTED ';
   end if;
   DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# .....................................................................................................');
   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# item history for SKUs that are currently all unslotted.');

   EXECUTE IMMEDIATE ' truncate table tt_TEMP_ITEM_HISTORY_UNSLOTTED ';
   EXECUTE IMMEDIATE ' truncate table tt_TEMP_ITEM_HISTORY_SMRY3 ';
   EXECUTE IMMEDIATE ' truncate table tt_TEMP_ITEM_HISTORY_INST3 ';
   EXECUTE IMMEDIATE ' truncate table tt_TEMP_SLOT_ITEM_INTERESTED ';
   v_errCode := 0;
   insert into tt_TEMP_SLOT_ITEM_INTERESTED(sku_id, ship_unit, hist_match, mult_loc_grp, no_of_slot_item_recs, min_slotitem_id)
   select t1.sku_id, t1.ship_unit, t1.hist_match, t1.mult_loc_grp, t1.no_of_slot_item_recs, t1.min_slotitem_id
   from(select distinct si.sku_id, NVL(si.ship_unit,0) ship_unit, NVL(si.hist_match,'0x') hist_match, NVL(si.mult_loc_grp,'0x') mult_loc_grp,
            count(1) no_of_slot_item_recs, min(si.slotitem_id) min_slotitem_id
      from slot_item  si
      where si.sku_id is not null and si.slot_id is null and si.delete_mult <> 1
      group by si.sku_id,si.ship_unit,si.hist_match,si.mult_loc_grp
      having count(1) > 1) t1
   left outer join(select distinct si.sku_id, NVL(si.ship_unit,0) ship_unit, NVL(si.hist_match,'0x') hist_match, NVL(si.mult_loc_grp,'0x') mult_loc_grp,
            count(1) no_of_slot_item_recs, min(si.slotitem_id) min_slotitem_id
      from slot_item  si
      where si.sku_id is not null and si.slot_id is not null and si.delete_mult <> 1
      and si.sku_id = 0
      group by si.sku_id,si.ship_unit,si.hist_match,si.mult_loc_grp) t2
   on    t2.sku_id        = t1.sku_id
   and    t2.ship_unit    = t1.ship_unit
   and t2.hist_match    = t1.hist_match
   and t2.mult_loc_grp    = t1.mult_loc_grp;

   DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# groups that are all unslotted and does not exists in slotted group');
   open SWC_Current for select * from tt_TEMP_SLOT_ITEM_INTERESTED;
   SWP_AssignOutRefcur4(SWC_Current,v_refcur,v_refcur2,v_refcur3,v_refcur4);
   v_iLoopControl := 1;
   begin
      select   MIN(iRowId), MAX(iRowId) INTO v_iNextRowId,v_iMaxRowId FROM   tt_TEMP_SLOT_ITEM_INTERESTED;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   IF NVL(v_iNextRowId,0) = 0 then
      DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# is no group match for SKUs that are all unslotted and does not exists in slotted group');
      
   end if;

   begin
      SELECT  iRowId, sku_id, ship_unit, hist_match, mult_loc_grp, min_slotitem_id, no_of_slot_item_recs INTO v_iCurrentRowId,v_sku_id,v_slot_ship_unit,v_hist_match,v_mult_loc_grp,
      v_min_slotitem_id,v_no_of_slot_item_recs FROM    tt_TEMP_SLOT_ITEM_INTERESTED  WHERE iRowId = v_iNextRowId AND ROWNUM <= 1;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   WHILE v_iLoopControl = 1 LOOP

      DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# group ' || SUBSTR(CAST(v_iCurrentRowId  AS VARCHAR2),1,10) || ' of ' || SUBSTR(CAST(v_iMaxRowId  AS VARCHAR2),1,10));
      if (v_errCode <> 0) then
         DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# SKU group - SKU: '
         || SUBSTR(CAST(v_sku_id  AS VARCHAR2),1,10) || 'SQLWAYS_EVAL# ' || SUBSTR(CAST(v_slot_ship_unit AS VARCHAR2),1,4) || ' , ' ||
         'hist match: ' || SUBSTR(v_hist_match,1,12)  || ' , ' ||
         'mult locn grp: ' || SUBSTR(v_mult_loc_grp,1,12));
         DBMS_OUTPUT.PUT_LINE('SQLWAYS_EVAL# back transaction....');
         ROLLBACK;
      end if;
      v_iNextRowId := NULL;
      begin
         select   NVL(MIN(iRowId),0) INTO v_iNextRowId FROM     tt_TEMP_SLOT_ITEM_INTERESTED WHERE    iRowId > v_iCurrentRowId;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;

      IF NVL(v_iNextRowId,0) = 0 then
         v_iLoopControl := 0;
         EXIT;
      end if;
      begin
         select   iRowId, sku_id, ship_unit, hist_match, mult_loc_grp, min_slotitem_id, no_of_slot_item_recs INTO v_iCurrentRowId,v_sku_id,v_slot_ship_unit,v_hist_match,v_mult_loc_grp,
         v_min_slotitem_id,v_no_of_slot_item_recs FROM    tt_TEMP_SLOT_ITEM_INTERESTED  WHERE  iRowId = v_iNextRowId;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   END LOOP; 
   COMMIT;
   if 'slot_item_interested' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_SLOT_ITEM_INTERESTED ';
   end if;
   if 'slot_item_slotted' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_SLOT_ITEM_SLOTTED ';
   end if;
   if 'slot_item_unslotted' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_SLOT_ITEM_UNSLOTTED ';
   end if;
   if 'item_history_unslotted' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_ITEM_HISTORY_UNSLOTTED ';
   end if;
   if 'item_history_smry' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_ITEM_HISTORY_SMRY3 ';
   end if;
   if 'item_history_inst' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_ITEM_HISTORY_INST3 ';
   end if;
   open SWC_Current for SELECT 'SQLWAYS_EVAL# MESSAGE TAB FOR DEBUG MESSAGES' as INFO, SYSTIMESTAMP PROCESS_END_TIME  from dual;
   SWP_AssignOutRefcur4(SWC_Current,v_refcur,v_refcur2,v_refcur3,v_refcur4);
   UPDATE item_history SET(movement) =(SELECT distinct -999.99 FROM  tt_TEMP_NEG_MVMT_SLOT_ITEM tnms where item_history.slotitem_id = tnms.slotitem_id and item_history.start_date = tnms.start_date and item_history.end_date = tnms.end_date and item_history.movement = 0.00) WHERE ROWID IN(SELECT item_history.ROWID from tt_TEMP_NEG_MVMT_SLOT_ITEM tnms where item_history.slotitem_id = tnms.slotitem_id and item_history.start_date = tnms.start_date and item_history.end_date = tnms.end_date and item_history.movement = 0.00);
   UPDATE item_history SET(inventory) =(SELECT distinct -999.99 FROM  tt_TEMP_NEG_INV_SLOT_ITEM tnns where item_history.slotitem_id = tnns.slotitem_id and item_history.start_date = tnns.start_date and item_history.end_date = tnns.end_date and item_history.inventory = 0.00) WHERE ROWID IN(SELECT item_history.ROWID from tt_TEMP_NEG_INV_SLOT_ITEM tnns where item_history.slotitem_id = tnns.slotitem_id and item_history.start_date = tnns.start_date and item_history.end_date = tnns.end_date and item_history.inventory = 0.00);
   UPDATE item_history SET(nbr_of_picks) =(SELECT distinct -999.99 FROM  tt_TEMP_NEG_HITS_SLOT_ITEM tnhs where item_history.slotitem_id = tnhs.slotitem_id and item_history.start_date = tnhs.start_date and item_history.end_date = tnhs.end_date and item_history.nbr_of_picks = 0.00) WHERE ROWID IN(SELECT item_history.ROWID from tt_TEMP_NEG_HITS_SLOT_ITEM tnhs where item_history.slotitem_id = tnhs.slotitem_id and item_history.start_date = tnhs.start_date and item_history.end_date = tnhs.end_date and item_history.nbr_of_picks = 0.00);
   if 'neg_mvmt_slot_item' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_NEG_MVMT_SLOT_ITEM ';
   end if;
   if ' neg_inv_slot_item' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_NEG_INV_SLOT_ITEM ';
   end if;
   if 'neg_hits_slot_item' is not null then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_NEG_HITS_SLOT_ITEM ';
   end if;
   
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_CONSOL_ITEM_HIST_SLOTTED
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_CONSOL_ITEM_HIST_SLOTTED (v_sku_id IN NUMBER, v_slot_ship_unit IN NUMBER, v_hist_match IN NVARCHAR2, v_mult_loc_grp IN NVARCHAR2, v_no_of_slot_item_recs IN NUMBER,
v_errCode  IN OUT NUMBER)


   as
   v_i_ih_nextrowid  NUMBER(10,0);
   v_i_ih_currowid  NUMBER(10,0);
   v_i_ih_loopctl  NUMBER(1,0);
   v_wh_ti  NUMBER(10,0);
   v_ven_ti  NUMBER(10,0);
   v_ord_ti  NUMBER(10,0);
   v_wh_hi  NUMBER(10,0);
   v_ven_hi  NUMBER(10,0);
   v_ord_hi  NUMBER(10,0);
   v_inn_per_cs  NUMBER(10,0);
   v_each_per_cs  NUMBER(10,0);
   v_each_per_inn  NUMBER(10,0);
   v_item_ship_unit  NUMBER(5,0);
   v_start_date  TIMESTAMP(3);
   v_end_date  TIMESTAMP(3);
   v_pallete_pattern  NUMBER(10,0);
   v_mov_pallet  NUMBER(17,2);
   v_mov_case  NUMBER(17,2);
   v_mov_each  NUMBER(17,2);
   v_mov_inner  NUMBER(17,2);
   v_inv_pallet  NUMBER(17,2);
   v_inv_case  NUMBER(17,2);
   v_inv_inner  NUMBER(17,2);
   v_inv_each  NUMBER(17,2);
   v_nbr_of_picks  NUMBER(17,2);
   v_no_of_item_hist_recs  NUMBER(10,0);
   v_hist_id  NUMBER(19,0);
   v_recs_updtd_tot  NUMBER(10,0);
   v_recs_updtd_tmp  NUMBER(10,0);
   v_bIs_SlotItem_good_to_delete  NUMBER(1,0);
   v_strSQLSt  VARCHAR2(255);
   v_rc_del  NUMBER(10,0);
   SWV_ROWCOUNT  NUMBER(10,0);
   v_if_exists NUMBER(10,0);
   v_if_exists2 NUMBER(10,0);
   
   
BEGIN

   v_bIs_SlotItem_good_to_delete := 0;
   v_errCode := 0;
    select   wh_hi, ven_hi, ord_hi, wh_ti, ven_ti, ord_ti, inn_per_cs, each_per_cs, each_per_inn INTO v_wh_hi,v_ven_hi,v_ord_hi,v_wh_ti,v_ven_ti,v_ord_ti,v_inn_per_cs,v_each_per_cs,
      v_each_per_inn from so_item_master where sku_id = v_sku_id;
  
   
  
   v_wh_hi := NVL(v_wh_hi,1);
   v_ven_hi := NVL(v_ven_hi,1);
   v_ord_hi := NVL(v_ord_hi,1);
   v_wh_ti := NVL(v_wh_ti,1);
   v_ven_ti := NVL(v_ven_ti,1);
   v_ord_ti := NVL(v_ord_ti,1);

   EXECUTE IMMEDIATE ' truncate table tt_TEMP_ITEM_HISTORY_SMRY ';

   insert into tt_TEMP_ITEM_HISTORY_SMRY(ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date)
   select t.ship_unit as ship_unit, t.pallete_pattern as pallete_pattern, sum(t.nbr_of_picks) as nbr_of_picks,
    sum(t.mov1) as mov_pallets, sum(t.mov2) as mov_cases, sum(t.mov4) as mov_inners, sum(t.mov8) as mov_eaches,
    sum(t.inv1) as inv_pallets, sum(t.inv2) as inv_cases, sum(t.inv4) as inv_inners, sum(t.inv8) as inv_eaches,
    start_date as start_date, end_date as end_date from(select ih.ship_unit , si.pallete_pattern, sum(NVL(ih.nbr_of_picks,0)) nbr_of_picks,
      NVL(case ih.ship_unit when 1 then  sum(NVL(ih.movement,0)) end,0) as mov1,
      NVL(case ih.ship_unit when 2 then  sum(NVL(ih.movement,0)) end,0) as mov2,
      NVL(case ih.ship_unit when 4 then  sum(NVL(ih.movement,0)) end,0) as mov4,
      NVL(case ih.ship_unit when 8 then  sum(NVL(ih.movement,0)) end,0) as mov8,
      NVL(case ih.ship_unit when 1 then  sum(NVL(ih.inventory,0)) end,0) as inv1,
      NVL(case ih.ship_unit when 2 then  sum(NVL(ih.inventory,0)) end,0) as inv2,
      NVL(case ih.ship_unit when 4 then  sum(NVL(ih.inventory,0)) end,0) as inv4,
      NVL(case ih.ship_unit when 8 then  sum(NVL(ih.inventory,0)) end,0) as inv8,
      ih.start_date as start_date, ih.end_date as end_date
      from item_history ih
      inner join slot_item si  on si.slotitem_id = ih.slotitem_id
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null
      group by ih.ship_unit,si.pallete_pattern,ih.start_date,ih.end_date) t
   group by t.ship_unit,t.pallete_pattern,t.start_date,t.end_date
   order by t.start_date NULLS FIRST,t.end_date NULLS FIRST;
   v_i_ih_loopctl := 1;
 
      select   min(i_ih_rowid) INTO v_i_ih_nextrowid from   tt_TEMP_ITEM_HISTORY_SMRY;
     
 


      select   i_ih_rowid, ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date INTO v_i_ih_currowid,v_item_ship_unit,v_pallete_pattern,v_nbr_of_picks,v_mov_pallet,
      v_mov_case,v_mov_inner,v_mov_each,v_inv_pallet,v_inv_case,v_inv_inner,
      v_inv_each,v_start_date,v_end_date from    tt_TEMP_ITEM_HISTORY_SMRY  where  i_ih_rowid = v_i_ih_nextrowid;
      
  
   while v_i_ih_loopctl = 1 LOOP
   SELECT  COUNT(*) INTO v_if_exists from item_history ih
      inner join slot_item si  on si.slotitem_id = ih.slotitem_id WHERE si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is not null
      and ih.start_date = v_start_date and ih.end_date = v_end_date AND ROWNUM <= 1;
    
    
      if v_if_exists > 0 then
         
         v_recs_updtd_tot := 0;
         v_recs_updtd_tmp := 0;
         select   count(1) INTO v_no_of_item_hist_recs from item_history ih
         inner join slot_item si  on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         and ih.start_date = v_start_date and ih.end_date = v_end_date;

       

         v_strSQLSt := 'update item_history set  nbr_of_picks = nbr_of_picks + (' || v_nbr_of_picks || '/ ' || v_no_of_item_hist_recs||' ) '||',MOD_DATE_TIME = SYSDATE'||' , MOD_USER= '||'''XYZ'''||
' from item_history ih '||
        ' inner join slot_item si  on si.slotitem_id = ih.slotitem_id where si.sku_id ='|| v_sku_id
         ||' and si.ship_unit ='|| v_slot_ship_unit
         ||' and si.delete_mult <> 1' 
        ||''' and NVL(si.hist_match,''0x'') = '''||v_hist_match
         ||''' and NVL(si.mult_loc_grp,''0x'') = '''||v_mult_loc_grp
         ||''' and si.slot_id is not null'
        ||''' and ih.start_date = '''||v_start_date||''' and ih.end_date = '''||v_end_date;
        
         EXECUTE IMMEDIATE v_strSQLSt USING v_recs_updtd_tmp;
         v_errCode := SQLCODE;
        
      
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;



         UPDATE item_history ih_Upd SET(movement,inventory,mod_date_time,mod_user) =(SELECT  case
         when v_item_ship_unit = 1 then ih.movement+(v_mov_pallet/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 0 then ih.movement+((v_mov_case/(v_wh_hi*v_wh_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 1 then ih.movement+((v_mov_case/(v_ord_hi*v_ord_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 2 then ih.movement+((v_mov_case/(v_ven_hi*v_ven_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 0 then ih.movement+((v_mov_inner/(v_wh_hi*v_wh_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 1 then ih.movement+((v_mov_inner/(v_ord_hi*v_ord_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 2 then ih.movement+((v_mov_inner/(v_ven_hi*v_ven_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 0 then ih.movement+((v_mov_each/(v_wh_hi*v_wh_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 1 then ih.movement+((v_mov_each/(v_ord_hi*v_ord_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 2 then ih.movement+((v_mov_each/(v_ven_hi*v_ven_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         end,case
         when v_item_ship_unit = 1 then ih.inventory+(v_inv_pallet/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 0 then ih.inventory+((v_inv_case/(v_wh_hi*v_wh_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 1 then ih.inventory+((v_inv_case/(v_ord_hi*v_ord_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 and v_pallete_pattern = 2 then ih.inventory+((v_inv_case/(v_ven_hi*v_ven_ti))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 0 then ih.inventory+((v_inv_inner/(v_wh_hi*v_wh_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 1 then ih.inventory+((v_inv_inner/(v_ord_hi*v_ord_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 and v_pallete_pattern = 2 then ih.inventory+((v_inv_inner/(v_ven_hi*v_ven_ti*v_inn_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 0 then ih.inventory+((v_inv_each/(v_wh_hi*v_wh_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 1 then ih.inventory+((v_inv_each/(v_ord_hi*v_ord_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 and v_pallete_pattern = 2 then ih.inventory+((v_inv_each/(v_ven_hi*v_ven_ti*v_each_per_cs))/v_no_of_item_hist_recs)
         end,SYSTIMESTAMP,'consoldtd' FROM  item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id WHERE (si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 1) AND ih.ROWID = ih_Upd.ROWID) WHERE ROWID IN(SELECT ih.ROWID from item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 1);
         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;
         
         UPDATE item_history ih_Upd SET(movement,inventory,mod_date_time,mod_user) =(SELECT  case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.movement+((v_mov_pallet*v_wh_hi*v_wh_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.movement+((v_mov_pallet*v_ord_hi*v_ord_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.movement+((v_mov_pallet*v_ven_hi*v_ven_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.movement+(v_mov_case/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.movement+((v_mov_inner/v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.movement+((v_mov_each/v_each_per_cs)/v_no_of_item_hist_recs)
         end,case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.inventory+((v_inv_pallet*v_wh_hi*v_wh_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.inventory+((v_inv_pallet*v_ord_hi*v_ord_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.inventory+((v_inv_pallet*v_ven_hi*v_ven_ti)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.inventory+(v_inv_case/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.inventory+((v_inv_inner/v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.inventory+((v_inv_each/v_each_per_cs)/v_no_of_item_hist_recs)
         end,SYSTIMESTAMP,'consoldtd' FROM  item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id WHERE (si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 2) AND ih.ROWID = ih_Upd.ROWID) WHERE ROWID IN(SELECT ih.ROWID from item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 2);
         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;
         
         UPDATE item_history ih_Upd SET(movement,inventory,mod_date_time,mod_user) =(SELECT  case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.movement+((v_mov_pallet*v_wh_hi*v_wh_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.movement+((v_mov_pallet*v_ord_hi*v_ord_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.movement+((v_mov_pallet*v_ven_hi*v_ven_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.movement+((v_mov_case*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.movement+(v_mov_inner/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.movement+((v_mov_each/v_each_per_inn)/v_no_of_item_hist_recs)
         end,case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.inventory+((v_inv_pallet*v_wh_hi*v_wh_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.inventory+((v_inv_pallet*v_ord_hi*v_ord_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.inventory+((v_inv_pallet*v_ven_hi*v_ven_ti*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.inventory+((v_inv_case*v_inn_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.inventory+(v_inv_inner/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.inventory+((v_inv_each/v_each_per_inn)/v_no_of_item_hist_recs)
         end,SYSTIMESTAMP,'consoldtd' FROM  item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id WHERE (si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 4) AND ih.ROWID = ih_Upd.ROWID) WHERE ROWID IN(SELECT ih.ROWID from item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 4);
         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;
        
         UPDATE item_history ih_Upd SET(movement,inventory,mod_date_time,mod_user) =(SELECT  case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.movement+((v_mov_pallet*v_wh_hi*v_wh_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.movement+((v_mov_pallet*v_ord_hi*v_ord_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.movement+((v_mov_pallet*v_ven_hi*v_ven_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.movement+((v_mov_case*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.movement+((v_mov_inner*v_each_per_inn)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.movement+(v_mov_each/v_no_of_item_hist_recs)
         end,case
         when v_item_ship_unit = 1 and v_pallete_pattern = 0 then ih.inventory+((v_inv_pallet*v_wh_hi*v_wh_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 1 then ih.inventory+((v_inv_pallet*v_ord_hi*v_ord_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 1 and v_pallete_pattern = 2 then ih.inventory+((v_inv_pallet*v_ven_hi*v_ven_ti*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 2 then ih.inventory+((v_inv_case*v_each_per_cs)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 4 then ih.inventory+((v_inv_inner*v_each_per_inn)/v_no_of_item_hist_recs)
         when v_item_ship_unit = 8 then ih.inventory+(v_inv_each/v_no_of_item_hist_recs)
         end,SYSTIMESTAMP,'consoldtd' FROM  item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id WHERE (si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 8) AND ih.ROWID = ih_Upd.ROWID) WHERE ROWID IN(SELECT ih.ROWID from item_history ih
         inner join slot_item si on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         and ih.start_date = v_start_date and ih.end_date = v_end_date
         and ih.ship_unit = 8);
         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp+v_recs_updtd_tot;
         
        if (v_recs_updtd_tot > 0) then
       
            v_strSQLSt := 'delete from item_history ' ||
          ' where hist_id in  ( select hist_id from item_history ih'
          ||' inner join slot_item si on si.slotitem_id = ih.slotitem_id   where si.sku_id = '||v_sku_id
         ||' and si.ship_unit ='|| v_slot_ship_unit
         ||' and si.delete_mult <> 1' 
        ||''' and NVL(si.hist_match,''0x'') = '''||v_hist_match
         ||''' and NVL(si.mult_loc_grp,''0x'') = '''||v_mult_loc_grp
         ||''' and si.slot_id is not null'
        ||''' and ih.start_date = '''||v_start_date||''' and ih.end_date = '''||v_end_date 
        ||''' and ih.ship_unit = '''|| v_item_ship_unit|| ''')';
      
  


            EXECUTE IMMEDIATE v_strSQLSt USING v_rc_del;
            v_errCode := SQLCODE;
           
        
            
            v_bIs_SlotItem_good_to_delete := 1;
         else
            
            v_errCode := -1;
          
         end if;
      end if;

      v_i_ih_nextrowid := NULL;
 
         select   NVL(min(i_ih_rowid),0) INTO v_i_ih_nextrowid from     tt_TEMP_ITEM_HISTORY_SMRY where    i_ih_rowid > v_i_ih_currowid;
       
  
      if NVL(v_i_ih_nextrowid,0) = 0 then
         EXIT;
      end if;
    

         select   i_ih_rowid, ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date INTO v_i_ih_currowid,v_item_ship_unit,v_pallete_pattern,v_nbr_of_picks,v_mov_pallet,
         v_mov_case,v_mov_inner,v_mov_each,v_inv_pallet,v_inv_case,v_inv_inner,
         v_inv_each,v_start_date,v_end_date from  tt_TEMP_ITEM_HISTORY_SMRY  where  i_ih_rowid = v_i_ih_nextrowid;
        
   END LOOP; 

   EXECUTE IMMEDIATE ' truncate table tt_TEMP_ITEM_HISTORY_SMRY ';

   insert into tt_TEMP_ITEM_HISTORY_SMRY(ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date)
   select distinct v_slot_ship_unit, si.pallete_pattern, 0,0,0,0,0,0,0,0,0, ih.start_date, ih.end_date
   from item_history ih
   inner join slot_item si  on si.slotitem_id = ih.slotitem_id
   where si.sku_id = v_sku_id
   and si.ship_unit = v_slot_ship_unit
   and si.delete_mult <> 1
   and NVL(si.hist_match,'0x') = v_hist_match
   and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
   and si.slot_id is null
   order by ih.start_date NULLS FIRST,si.pallete_pattern NULLS FIRST;
   v_i_ih_loopctl := 1;

      select   min(i_ih_rowid) INTO v_i_ih_nextrowid from   tt_TEMP_ITEM_HISTORY_SMRY;
     
 
   UPDATE tt_TEMP_ITEM_HISTORY_SMRY tihs_Upd SET(mov1,mov2,mov4,mov8,inv1,inv2,inv4,inv8,nbr_of_picks) =(SELECT  tout.mov1,tout.mov2,tout.mov4,tout.mov8,tout.inv1,tout.inv2,tout.inv4,tout.inv8,tout.nbr_of_picks FROM  tt_TEMP_ITEM_HISTORY_SMRY tihs
   inner join(select  sum(thist.mov1) mov1, sum(thist.mov2) mov2, sum(thist.mov4) mov4, sum(thist.mov8) mov8,
            sum(thist.inv1) inv1, sum(thist.inv2) inv2, sum(thist.inv4) inv4, sum(thist.inv8) inv8,
            thist.start_date, thist.end_date, thist.pallete_pattern, sum(thist.nbr_of_picks) nbr_of_picks
      from(select  NVL(case ih.ship_unit when 1 then  sum(NVL(ih.movement,0)) end,0) as mov1,
            NVL(case ih.ship_unit when 2 then  sum(NVL(ih.movement,0)) end,0) as mov2,
            NVL(case ih.ship_unit when 4 then  sum(NVL(ih.movement,0)) end,0) as mov4,
            NVL(case ih.ship_unit when 8 then  sum(NVL(ih.movement,0)) end,0) as mov8,
            NVL(case ih.ship_unit when 1 then  sum(NVL(ih.inventory,0)) end,0) as inv1,
            NVL(case ih.ship_unit when 2 then  sum(NVL(ih.inventory,0)) end,0) as inv2,
            NVL(case ih.ship_unit when 4 then  sum(NVL(ih.inventory,0)) end,0) as inv4,
            NVL(case ih.ship_unit when 8 then  sum(NVL(ih.inventory,0)) end,0) as inv8,
            sum(NVL(ih.nbr_of_picks,0))/v_no_of_slot_item_recs as nbr_of_picks,
            ih.start_date, ih.end_date, si.pallete_pattern
         from item_history ih
         inner join slot_item si  on si.slotitem_id = ih.slotitem_id
         where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is null
          --SQLWAYS_EVAL# = '2008-08-07 00:00:00.000' and ih.end_date = '2008-08-13 00:00:00.000'
          --SQLWAYS_EVAL# = 0
         group by ih.ship_unit,ih.start_date,ih.end_date,si.pallete_pattern) thist group by thist.start_date,thist.end_date,thist.pallete_pattern) tout
   on tout.start_date = tihs.start_date
   and tout.end_date = tihs.end_date
   and tout.pallete_pattern = tihs.pallete_pattern WHERE tihs.ROWID = tihs_Upd.ROWID) WHERE ROWID IN(SELECT tihs.ROWID from tt_TEMP_ITEM_HISTORY_SMRY tihs
   inner join(select  sum(thist.mov1) mov1, sum(thist.mov2) mov2, sum(thist.mov4) mov4, sum(thist.mov8) mov8,
            sum(thist.inv1) inv1, sum(thist.inv2) inv2, sum(thist.inv4) inv4, sum(thist.inv8) inv8,
            thist.start_date, thist.end_date, thist.pallete_pattern, sum(thist.nbr_of_picks) nbr_of_picks
      from(select  NVL(case ih.ship_unit when 1 then  sum(NVL(ih.movement,0)) end,0) as mov1,
            NVL(case ih.ship_unit when 2 then  sum(NVL(ih.movement,0)) end,0) as mov2,
            NVL(case ih.ship_unit when 4 then  sum(NVL(ih.movement,0)) end,0) as mov4,
            NVL(case ih.ship_unit when 8 then  sum(NVL(ih.movement,0)) end,0) as mov8,
            NVL(case ih.ship_unit when 1 then  sum(NVL(ih.inventory,0)) end,0) as inv1,
            NVL(case ih.ship_unit when 2 then  sum(NVL(ih.inventory,0)) end,0) as inv2,
            NVL(case ih.ship_unit when 4 then  sum(NVL(ih.inventory,0)) end,0) as inv4,
            NVL(case ih.ship_unit when 8 then  sum(NVL(ih.inventory,0)) end,0) as inv8,
            sum(NVL(ih.nbr_of_picks,0))/v_no_of_slot_item_recs as nbr_of_picks,
            ih.start_date, ih.end_date, si.pallete_pattern
         from item_history ih
         inner join slot_item si  on si.slotitem_id = ih.slotitem_id
         where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is null
          
         group by ih.ship_unit,ih.start_date,ih.end_date,si.pallete_pattern) thist group by thist.start_date,thist.end_date,thist.pallete_pattern) tout
   on tout.start_date = tihs.start_date
   and tout.end_date = tihs.end_date
   and tout.pallete_pattern = tihs.pallete_pattern);


      select   i_ih_rowid, ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date INTO v_i_ih_currowid,v_item_ship_unit,v_pallete_pattern,v_nbr_of_picks,v_mov_pallet,
      v_mov_case,v_mov_inner,v_mov_each,v_inv_pallet,v_inv_case,v_inv_inner,
      v_inv_each,v_start_date,v_end_date from    tt_TEMP_ITEM_HISTORY_SMRY  where  i_ih_rowid = v_i_ih_nextrowid;

   while v_i_ih_loopctl = 1 LOOP

      SELECT   COUNT(*) INTO v_if_exists2 from item_history ih
      inner join slot_item si  on si.slotitem_id = ih.slotitem_id where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is not null
      and ih.start_date = v_start_date and ih.end_date = v_end_date;
      if  v_if_exists2 = 0 then
        

         EXECUTE IMMEDIATE ' truncate table tt_TEMP_ITEM_HISTORY_INST ';
      
            select   curr_nbr INTO v_hist_id from unique_ids where rec_type = 'item_history';
         

      
        
         insert into tt_TEMP_ITEM_HISTORY_INST(slotitem_id, start_date, end_date, ship_unit, nbr_of_picks, movement, inventory)
         select si.slotitem_id, v_start_date, v_end_date, v_slot_ship_unit, v_nbr_of_picks,0,0
         from slot_item si
         where si.sku_id = v_sku_id
         and si.ship_unit = v_slot_ship_unit
         and si.delete_mult <> 1
         and NVL(si.hist_match,'0x') = v_hist_match
         and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
         and si.slot_id is not null
         order by si.slotitem_id NULLS FIRST;
    
            SELECT ROWNUM+1 INTO v_hist_id FROM tt_TEMP_ITEM_HISTORY_INST WHERE rownum <= 1;
          
         UPDATE tt_TEMP_ITEM_HISTORY_INST SET hist_id = v_hist_id,movement =  case
         when v_slot_ship_unit = 8 and v_pallete_pattern = 0 then((v_mov_pallet*v_wh_hi*v_wh_ti*v_each_per_cs)/v_no_of_slot_item_recs)+((v_mov_case*v_each_per_cs)/v_no_of_slot_item_recs)+((v_mov_inner*v_each_per_inn)/v_no_of_slot_item_recs)+(v_mov_each/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 8 and v_pallete_pattern = 1 then((v_mov_pallet*v_ord_hi*v_ord_ti*v_each_per_cs)/v_no_of_slot_item_recs)+((v_mov_case*v_each_per_cs)/v_no_of_slot_item_recs)+((v_mov_inner*v_each_per_inn)/v_no_of_slot_item_recs)+(v_mov_each/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 8 and v_pallete_pattern = 2 then((v_mov_pallet*v_ven_hi*v_ven_ti*v_each_per_cs)/v_no_of_slot_item_recs)+((v_mov_case*v_each_per_cs)/v_no_of_slot_item_recs)+((v_mov_inner*v_each_per_inn)/v_no_of_slot_item_recs)+(v_mov_each/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 0 then((v_mov_pallet*v_wh_hi*v_wh_ti*v_inn_per_cs)/v_no_of_slot_item_recs)+((v_mov_case*v_inn_per_cs)/v_no_of_slot_item_recs)+(v_mov_inner/v_no_of_slot_item_recs)+((v_mov_each/v_each_per_inn)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 1 then((v_mov_pallet*v_ord_hi*v_ord_ti*v_inn_per_cs)/v_no_of_slot_item_recs)+((v_mov_case*v_inn_per_cs)/v_no_of_slot_item_recs)+(v_mov_inner/v_no_of_slot_item_recs)+((v_mov_each/v_each_per_inn)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 2 then((v_mov_pallet*v_ven_hi*v_ven_ti*v_inn_per_cs)/v_no_of_slot_item_recs)+((v_mov_case*v_inn_per_cs)/v_no_of_slot_item_recs)+(v_mov_inner/v_no_of_slot_item_recs)+((v_mov_each/v_each_per_inn)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 0 then((v_mov_pallet*v_wh_hi*v_wh_ti)/v_no_of_slot_item_recs)+(v_mov_case/v_no_of_slot_item_recs)+((v_mov_inner/v_inn_per_cs)/v_no_of_slot_item_recs)+((v_mov_each/v_each_per_cs)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 1 then((v_mov_pallet*v_ord_hi*v_ord_ti)/v_no_of_slot_item_recs)+(v_mov_case/v_no_of_slot_item_recs)+((v_mov_inner/v_inn_per_cs)/v_no_of_slot_item_recs)+((v_mov_each/v_each_per_cs)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 2 then((v_mov_pallet*v_ven_hi*v_ven_ti)/v_no_of_slot_item_recs)+(v_mov_case/v_no_of_slot_item_recs)+((v_mov_inner/v_inn_per_cs)/v_no_of_slot_item_recs)+((v_mov_each/v_each_per_cs)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 0 then(v_mov_pallet/v_no_of_slot_item_recs)+((v_mov_case/(v_wh_hi*v_wh_ti))/v_no_of_slot_item_recs)+((v_mov_inner/(v_wh_hi*v_wh_ti*v_inn_per_cs))/v_no_of_slot_item_recs)+((v_mov_each/(v_wh_hi*v_wh_ti*v_each_per_cs))/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 1 then(v_mov_pallet/v_no_of_slot_item_recs)+((v_mov_case/(v_ord_hi*v_ord_ti))/v_no_of_slot_item_recs)+((v_mov_inner/(v_ord_hi*v_ord_ti*v_inn_per_cs))/v_no_of_slot_item_recs)+((v_mov_each/(v_ord_hi*v_ord_ti*v_each_per_cs))/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 2 then(v_mov_pallet/v_no_of_slot_item_recs)+((v_mov_case/(v_ven_hi*v_ven_ti))/v_no_of_slot_item_recs)+((v_mov_inner/(v_ven_hi*v_ven_ti*v_inn_per_cs))/v_no_of_slot_item_recs)+((v_mov_each/(v_ven_hi*v_ven_ti*v_each_per_cs))/v_no_of_slot_item_recs)
         end,inventory =  case
         when v_slot_ship_unit = 8 and v_pallete_pattern = 0 then((v_inv_pallet*v_wh_hi*v_wh_ti*v_each_per_cs)/v_no_of_slot_item_recs)+((v_inv_case*v_each_per_cs)/v_no_of_slot_item_recs)+((v_inv_inner*v_each_per_inn)/v_no_of_slot_item_recs)+(v_inv_each/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 8 and v_pallete_pattern = 1 then((v_inv_pallet*v_ord_hi*v_ord_ti*v_each_per_cs)/v_no_of_slot_item_recs)+((v_inv_case*v_each_per_cs)/v_no_of_slot_item_recs)+((v_inv_inner*v_each_per_inn)/v_no_of_slot_item_recs)+(v_inv_each/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 8 and v_pallete_pattern = 2 then((v_inv_pallet*v_ven_hi*v_ven_ti*v_each_per_cs)/v_no_of_slot_item_recs)+((v_inv_case*v_each_per_cs)/v_no_of_slot_item_recs)+((v_inv_inner*v_each_per_inn)/v_no_of_slot_item_recs)+(v_inv_each/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 0 then((v_inv_pallet*v_wh_hi*v_wh_ti*v_inn_per_cs)/v_no_of_slot_item_recs)+((v_inv_case*v_inn_per_cs)/v_no_of_slot_item_recs)+(v_inv_inner/v_no_of_slot_item_recs)+((v_inv_each/v_each_per_inn)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 1 then((v_inv_pallet*v_ord_hi*v_ord_ti*v_inn_per_cs)/v_no_of_slot_item_recs)+((v_inv_case*v_inn_per_cs)/v_no_of_slot_item_recs)+(v_inv_inner/v_no_of_slot_item_recs)+((v_inv_each/v_each_per_inn)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 4 and v_pallete_pattern = 2 then((v_inv_pallet*v_ven_hi*v_ven_ti*v_inn_per_cs)/v_no_of_slot_item_recs)+((v_inv_case*v_inn_per_cs)/v_no_of_slot_item_recs)+(v_inv_inner/v_no_of_slot_item_recs)+((v_inv_each/v_each_per_inn)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 0 then((v_inv_pallet*v_wh_hi*v_wh_ti)/v_no_of_slot_item_recs)+(v_inv_case/v_no_of_slot_item_recs)+((v_inv_inner/v_inn_per_cs)/v_no_of_slot_item_recs)+((v_inv_each/v_each_per_cs)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 1 then((v_inv_pallet*v_ord_hi*v_ord_ti)/v_no_of_slot_item_recs)+(v_inv_case/v_no_of_slot_item_recs)+((v_inv_inner/v_inn_per_cs)/v_no_of_slot_item_recs)+((v_inv_each/v_each_per_cs)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 2 and v_pallete_pattern = 2 then((v_inv_pallet*v_ven_hi*v_ven_ti)/v_no_of_slot_item_recs)+(v_inv_case/v_no_of_slot_item_recs)+((v_inv_inner/v_inn_per_cs)/v_no_of_slot_item_recs)+((v_inv_each/v_each_per_cs)/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 0 then(v_inv_pallet/v_no_of_slot_item_recs)+((v_inv_case/(v_wh_hi*v_wh_ti))/v_no_of_slot_item_recs)+((v_inv_inner/(v_wh_hi*v_wh_ti*v_inn_per_cs))/v_no_of_slot_item_recs)+((v_inv_each/(v_wh_hi*v_wh_ti*v_each_per_cs))/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 1 then(v_inv_pallet/v_no_of_slot_item_recs)+((v_inv_case/(v_ord_hi*v_ord_ti))/v_no_of_slot_item_recs)+((v_inv_inner/(v_ord_hi*v_ord_ti*v_inn_per_cs))/v_no_of_slot_item_recs)+((v_inv_each/(v_ord_hi*v_ord_ti*v_each_per_cs))/v_no_of_slot_item_recs)
         when v_slot_ship_unit = 1 and v_pallete_pattern = 2 then(v_inv_pallet/v_no_of_slot_item_recs)+((v_inv_case/(v_ven_hi*v_ven_ti))/v_no_of_slot_item_recs)+((v_inv_inner/(v_ven_hi*v_ven_ti*v_inn_per_cs))/v_no_of_slot_item_recs)+((v_inv_each/(v_ven_hi*v_ven_ti*v_each_per_cs))/v_no_of_slot_item_recs)
         end;

         v_strSQLSt := 'insert into  item_history (hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks)
      select hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks from tt_temp_item_history_inst;
      select v_recs_updtd_tmp=SQL%%ROWCOUNT ' ;
      
         EXECUTE IMMEDIATE v_strSQLSt USING v_recs_updtd_tmp;
         v_errCode := SQLCODE;
         
        
         v_recs_updtd_tmp := 0;
         update unique_ids
         set curr_nbr = v_hist_id
         where rec_type = 'item_history';
         
         v_strSQLSt := 'delete from item_history ' ||
          ' where hist_id in  ( select hist_id from item_history ih'
          ||' inner join slot_item si on si.slotitem_id = ih.slotitem_id   where si.sku_id = '||v_sku_id
         ||' and si.ship_unit ='|| v_slot_ship_unit
         ||' and si.delete_mult <> 1' 
        ||''' and NVL(si.hist_match,''0x'') = '''||v_hist_match
         ||''' and NVL(si.mult_loc_grp,''0x'') = '''||v_mult_loc_grp
         ||''' and si.slot_id is not null'
        ||''' and ih.start_date = '''||v_start_date||''' and ih.end_date = '''||v_end_date 
        ||''' and ih.ship_unit = '''|| v_item_ship_unit|| ''')' ;
    
         EXECUTE IMMEDIATE v_strSQLSt USING v_rc_del;
         v_errCode := SQLCODE;
        
         
         v_bIs_SlotItem_good_to_delete := 1;
      end if;
  
      v_i_ih_nextrowid := NULL;
      
         select   NVL(min(i_ih_rowid),0) INTO v_i_ih_nextrowid from     tt_TEMP_ITEM_HISTORY_SMRY where    i_ih_rowid > v_i_ih_currowid;
      
      if NVL(v_i_ih_nextrowid,0) = 0 then
         EXIT;
      end if;
  
 
         select   i_ih_rowid, ship_unit, pallete_pattern, nbr_of_picks, mov1, mov2, mov4, mov8, inv1, inv2, inv4, inv8, start_date, end_date INTO v_i_ih_currowid,v_item_ship_unit,v_pallete_pattern,v_nbr_of_picks,v_mov_pallet,
         v_mov_case,v_mov_inner,v_mov_each,v_inv_pallet,v_inv_case,v_inv_inner,
         v_inv_each,v_start_date,v_end_date from  tt_TEMP_ITEM_HISTORY_SMRY  where  i_ih_rowid = v_i_ih_nextrowid;
       
   END LOOP; 
 
   if (v_bIs_SlotItem_good_to_delete  = 1) then
     
      delete from slot_item_score
      where slot_item_score.slotitem_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null);
      
      delete from hn_failure
      where hn_failure.slotitem_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null);
      
      delete from haves_needs
      where haves_needs.slot_item_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null);
      
      delete from needs_results_dtl
      where needs_results_dtl.slotitem_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null);
      
      delete from sl_failure_reasons
      where sl_failure_reasons.slotitem_id in(select distinct si.slotitem_id from slot_item si
      where si.sku_id = v_sku_id
      and si.ship_unit = v_slot_ship_unit
      and si.delete_mult <> 1
      and NVL(si.hist_match,'0x') = v_hist_match
      and NVL(si.mult_loc_grp,'0x') = v_mult_loc_grp
      and si.slot_id is null);
     
      v_strSQLSt := 'delete from SLOT_ITEM ' ||
          ' where slotitem_id in  ( select slotitem_id from slot_item si'
          ||' where si.sku_id = '||v_sku_id
         ||' and si.ship_unit ='|| v_slot_ship_unit
         ||' and si.delete_mult <> 1' 
        ||''' and NVL(si.hist_match,''0x'') = '''||v_hist_match
         ||''' and NVL(si.mult_loc_grp,''0x'') = '''||v_mult_loc_grp
         ||''' and si.slot_id is not null' ;
   

    end if ;
    
      EXECUTE IMMEDIATE v_strSQLSt USING v_rc_del;
      v_errCode := SQLCODE;
     
      
 
END ;

/
--------------------------------------------------------
--  DDL for Procedure SP_CONSOL_ITEM_HIST_UNSLOTTED
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_CONSOL_ITEM_HIST_UNSLOTTED (
   v_sku_id                 IN     NUMBER,
   v_slot_ship_unit         IN     NUMBER,
   v_hist_match             IN     VARCHAR2,
   v_mult_loc_grp           IN     VARCHAR2,
   v_no_of_slot_item_recs   IN OUT NUMBER,
   v_min_slotitem_id        IN     NUMBER,
   v_errCode                IN OUT NUMBER)
AS
   v_i_ih_nextrowid                NUMBER (10, 0);
   v_i_ih_currowid                 NUMBER (10, 0);
   v_i_ih_loopctl                  NUMBER (1, 0);
   v_wh_ti                         NUMBER (10, 0);
   v_ven_ti                        NUMBER (10, 0);
   v_ord_ti                        NUMBER (10, 0);
   v_wh_hi                         NUMBER (10, 0);
   v_ven_hi                        NUMBER (10, 0);
   v_ord_hi                        NUMBER (10, 0);
   v_inn_per_cs                    NUMBER (10, 0);
   v_each_per_cs                   NUMBER (10, 0);
   v_each_per_inn                  NUMBER (10, 0);
   v_item_ship_unit                NUMBER (5, 0);
   v_start_date                    TIMESTAMP (3);
   v_end_date                      TIMESTAMP (3);
   v_pallete_pattern               NUMBER (10, 0);
   v_mov_pallet                    NUMBER (17, 2);
   v_mov_case                      NUMBER (17, 2);
   v_mov_each                      NUMBER (17, 2);
   v_mov_inner                     NUMBER (17, 2);
   v_inv_pallet                    NUMBER (17, 2);
   v_inv_case                      NUMBER (17, 2);
   v_inv_inner                     NUMBER (17, 2);
   v_inv_each                      NUMBER (17, 2);
   v_nbr_of_picks                  NUMBER (17, 2);
   v_no_of_item_hist_recs          NUMBER (10, 0);
   v_hist_id                       NUMBER (19, 0);
   v_recs_updtd_tot                NUMBER (10, 0);
   v_recs_updtd_tmp                NUMBER (10, 0);
   v_bIs_SlotItem_good_to_delete   NUMBER (1, 0);
   v_strSQLSt                      VARCHAR2 (1500);
   v_rc_del                        NUMBER (10, 0);
   SWV_ROWCOUNT                    NUMBER (10, 0);
   SWV_no_of_slot_item_recs        NUMBER (10, 0);
   v_if_exists                     NUMBER (10, 0);
   v_if_exists2                    NUMBER (10, 0);
BEGIN
   SWV_no_of_slot_item_recs := v_no_of_slot_item_recs;
   v_bIs_SlotItem_good_to_delete := 0;
   v_errCode := 0;

   SELECT wh_hi,
          ven_hi,
          ord_hi,
          wh_ti,
          ven_ti,
          ord_ti,
          inn_per_cs,
          each_per_cs,
          each_per_inn
     INTO v_wh_hi,
          v_ven_hi,
          v_ord_hi,
          v_wh_ti,
          v_ven_ti,
          v_ord_ti,
          v_inn_per_cs,
          v_each_per_cs,
          v_each_per_inn
     FROM so_item_master
    WHERE sku_id = v_sku_id;

   IF (v_wh_ti IS NULL AND v_ven_ti IS NULL AND v_ord_ti IS NULL
       OR v_wh_hi IS NULL AND v_ven_hi IS NULL AND v_ord_hi IS NULL)
   THEN
      v_errCode := 0;
      DBMS_OUTPUT.PUT_LINE (
         'ERROR: Item info is missing in item master. consolidation for this SKU is skipped');
      GOTO HANDLE_ERROR;
   END IF;

   v_wh_hi := NVL (v_wh_hi, 1);
   v_ven_hi := NVL (v_ven_hi, 1);
   v_ord_hi := NVL (v_ord_hi, 1);
   v_wh_ti := NVL (v_wh_ti, 1);
   v_ven_ti := NVL (v_ven_ti, 1);
   v_ord_ti := NVL (v_ord_ti, 1);

   EXECUTE IMMEDIATE ' delete tt_TEMP_ITEM_HISTORY_SMRY ';

   INSERT INTO tt_TEMP_ITEM_HISTORY_SMRY (ship_unit,
                                          pallete_pattern,
                                          nbr_of_picks,
                                          mov1,
                                          mov2,
                                          mov4,
                                          mov8,
                                          inv1,
                                          inv2,
                                          inv4,
                                          inv8,
                                          start_date,
                                          end_date)
        SELECT t.ship_unit AS ship_unit,
               t.pallete_pattern AS pallete_pattern,
               SUM (t.nbr_of_picks) AS nbr_of_picks,
               SUM (t.mov1) AS mov_pallets,
               SUM (t.mov2) AS mov_cases,
               SUM (t.mov4) AS mov_inners,
               SUM (t.mov8) AS mov_eaches,
               SUM (t.inv1) AS inv_pallets,
               SUM (t.inv2) AS inv_cases,
               SUM (t.inv4) AS inv_inners,
               SUM (t.inv8) AS inv_eaches,
               start_date AS start_date,
               end_date AS end_date
          FROM (  SELECT ih.ship_unit,
                         si.pallete_pattern,
                         SUM (NVL (ih.nbr_of_picks, 0)) nbr_of_picks,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 1 THEN SUM (NVL (ih.movement, 0))
                            END,
                            0)
                            AS mov1,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 2 THEN SUM (NVL (ih.movement, 0))
                            END,
                            0)
                            AS mov2,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 4 THEN SUM (NVL (ih.movement, 0))
                            END,
                            0)
                            AS mov4,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 8 THEN SUM (NVL (ih.movement, 0))
                            END,
                            0)
                            AS mov8,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 1 THEN SUM (NVL (ih.inventory, 0))
                            END,
                            0)
                            AS inv1,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 2 THEN SUM (NVL (ih.inventory, 0))
                            END,
                            0)
                            AS inv2,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 4 THEN SUM (NVL (ih.inventory, 0))
                            END,
                            0)
                            AS inv4,
                         NVL (
                            CASE ih.ship_unit
                               WHEN 8 THEN SUM (NVL (ih.inventory, 0))
                            END,
                            0)
                            AS inv8,
                         ih.start_date AS start_date,
                         ih.end_date AS end_date
                    FROM    item_history ih
                         INNER JOIN
                            slot_item si
                         ON si.slotitem_id = ih.slotitem_id
                   WHERE     si.sku_id = v_sku_id
                         AND si.ship_unit = v_slot_ship_unit
                         AND si.delete_mult <> 1
                         AND NVL (si.hist_match, '0x') = v_hist_match
                         AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                         AND si.slot_id IS NULL
                         AND si.slotitem_id <> v_min_slotitem_id
                GROUP BY ih.ship_unit,
                         si.pallete_pattern,
                         ih.start_date,
                         ih.end_date) t
      GROUP BY t.ship_unit,
               t.pallete_pattern,
               t.start_date,
               t.end_date
      ORDER BY t.start_date NULLS FIRST, t.end_date NULLS FIRST;

   v_i_ih_loopctl := 1;

   SELECT MIN (i_ih_rowid)
     INTO v_i_ih_nextrowid
     FROM tt_TEMP_ITEM_HISTORY_SMRY;

   IF (NVL (v_i_ih_nextrowid, 0) = 0)
   THEN
      DBMS_OUTPUT.PUT_LINE (
         'INFO: No data in item history table to consolidate for the selected sku group.');
      v_errCode := 0;
      v_bIs_SlotItem_good_to_delete := 1;

      GOTO LAST_CALL;
   END IF;



   SELECT i_ih_rowid,
          ship_unit,
          pallete_pattern,
          nbr_of_picks,
          mov1,
          mov2,
          mov4,
          mov8,
          inv1,
          inv2,
          inv4,
          inv8,
          start_date,
          end_date
     INTO v_i_ih_currowid,
          v_item_ship_unit,
          v_pallete_pattern,
          v_nbr_of_picks,
          v_mov_pallet,
          v_mov_case,
          v_mov_inner,
          v_mov_each,
          v_inv_pallet,
          v_inv_case,
          v_inv_inner,
          v_inv_each,
          v_start_date,
          v_end_date
     FROM tt_TEMP_ITEM_HISTORY_SMRY
    WHERE i_ih_rowid = v_i_ih_nextrowid;


   WHILE v_i_ih_loopctl = 1
   LOOP
      SELECT COUNT (*)
        INTO v_if_exists
        FROM    item_history ih
             INNER JOIN
                slot_item si
             ON si.slotitem_id = ih.slotitem_id
       WHERE     si.sku_id = v_sku_id
             AND si.ship_unit = v_slot_ship_unit
             AND si.delete_mult <> 1
             AND NVL (si.hist_match, '0x') = v_hist_match
             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
             AND si.slot_id IS NULL
             AND si.slotitem_id = v_min_slotitem_id
             AND ih.start_date = v_start_date
             AND ih.end_date = v_end_date
             AND ROWNUM <= 1;


      IF v_if_exists > 0
      THEN
         DBMS_OUTPUT.PUT_LINE ('INFO: Item history exists for sku:');

         v_recs_updtd_tot := 0;
         v_recs_updtd_tmp := 0;

         v_no_of_item_hist_recs := 1;

         v_strSQLSt :=
            'update (select ih.nbr_of_picks, ih.MOD_DATE_TIME, ih.MOD_USER from item_history ih  inner join slot_item si
         on si.slotitem_id = ih.slotitem_id where si.sku_id = '
            || v_sku_id
            || ' and si.ship_unit = '
            || v_slot_ship_unit
            || ' and si.delete_mult <> '
            || 1
            || ' and NVL(si.hist_match,''0x'') = '''
            || v_hist_match
            || ''' and NVL(si.mult_loc_grp,''0x'') = '''
            || v_mult_loc_grp
            || ''' and si.slot_id is null and si.slotitem_id  = '
            || v_min_slotitem_id
            || ' and ih.start_date = '''
            || v_start_date
            || ''' and ih.end_date = '''
            || v_end_date
            || ''')'
            || ' set  nbr_of_picks = nbr_of_picks + ('
            || v_nbr_of_picks
            || ' / '
            || v_no_of_item_hist_recs
            || ')'
            || ', MOD_DATE_TIME =  SYSTIMESTAMP'
            || ', MOD_USER = '
            || '''consoldtd''';

         BEGIN
            EXECUTE IMMEDIATE v_strSQLSt;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_errCode := SQLCODE;
               GOTO HANDLE_ERROR;
         END;

         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;


         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;


         UPDATE item_history ih_Upd
            SET (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (SELECT CASE
                              WHEN v_item_ship_unit = 1
                              THEN
                                 ih.movement
                                 + (v_mov_pallet / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (v_mov_case / (v_wh_hi * v_wh_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (v_mov_case / (v_ord_hi * v_ord_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_case / (v_ven_hi * v_ven_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner
                                      / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner
                                      / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner
                                      / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (v_mov_each
                                      / (v_wh_hi * v_wh_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (v_mov_each
                                      / (v_ord_hi * v_ord_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_each
                                      / (v_ven_hi * v_ven_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                           END,
                           CASE
                              WHEN v_item_ship_unit = 1
                              THEN
                                 ih.inventory
                                 + (v_inv_pallet / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case / (v_wh_hi * v_wh_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case / (v_ord_hi * v_ord_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case / (v_ven_hi * v_ven_ti))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner
                                      / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner
                                      / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner
                                      / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each
                                      / (v_wh_hi * v_wh_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each
                                      / (v_ord_hi * v_ord_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each
                                      / (v_ven_hi * v_ven_ti * v_each_per_cs))
                                    / v_no_of_item_hist_recs)
                           END,
                           SYSTIMESTAMP,
                           'consoldtd'
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                     WHERE (    si.sku_id = v_sku_id
                            AND si.ship_unit = v_slot_ship_unit
                            AND si.delete_mult <> 1
                            AND NVL (si.hist_match, '0x') = v_hist_match
                            AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            AND si.slot_id IS NULL
                            AND si.slotitem_id = v_min_slotitem_id
                            AND ih.start_date = v_start_date
                            AND ih.end_date = v_end_date
                            AND ih.ship_unit = 1)
                           AND ih.ROWID = ih_Upd.ROWID)
          WHERE ROWID IN
                   (SELECT ih.ROWID
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                     WHERE     si.sku_id = v_sku_id
                           AND si.ship_unit = v_slot_ship_unit
                           AND si.delete_mult <> 1
                           AND NVL (si.hist_match, '0x') = v_hist_match
                           AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           AND si.slot_id IS NULL
                           AND si.slotitem_id = v_min_slotitem_id
                           AND ih.start_date = v_start_date
                           AND ih.end_date = v_end_date
                           AND ih.ship_unit = 1);

         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         UPDATE item_history ih_Upd
            SET (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (SELECT CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (v_mov_pallet * v_wh_hi * v_wh_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (v_mov_pallet * v_ord_hi * v_ord_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_pallet * v_ven_hi * v_ven_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.movement
                                 + (v_mov_case / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner / v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.movement
                                 + ( (v_mov_each / v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                           END,
                           CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (v_inv_pallet * v_wh_hi * v_wh_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (v_inv_pallet * v_ord_hi * v_ord_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_pallet * v_ven_hi * v_ven_ti)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.inventory
                                 + (v_inv_case / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner / v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each / v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                           END,
                           SYSTIMESTAMP,
                           'consoldtd'
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                     WHERE (    si.sku_id = v_sku_id
                            AND si.ship_unit = v_slot_ship_unit
                            AND si.delete_mult <> 1
                            AND NVL (si.hist_match, '0x') = v_hist_match
                            AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            AND si.slotitem_id = v_min_slotitem_id
                            AND ih.start_date = v_start_date
                            AND ih.end_date = v_end_date
                            AND ih.ship_unit = 2)
                           AND ih.ROWID = ih_Upd.ROWID)
          WHERE ROWID IN
                   (SELECT ih.ROWID
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                     WHERE     si.sku_id = v_sku_id
                           AND si.ship_unit = v_slot_ship_unit
                           AND si.delete_mult <> 1
                           AND NVL (si.hist_match, '0x') = v_hist_match
                           AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           AND si.slotitem_id = v_min_slotitem_id
                           AND ih.start_date = v_start_date
                           AND ih.end_date = v_end_date
                           AND ih.ship_unit = 2);

         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         UPDATE item_history ih_Upd
            SET (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (SELECT CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_case * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.movement
                                 + (v_mov_inner / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.movement
                                 + ( (v_mov_each / v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                           END,
                           CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case * v_inn_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.inventory
                                 + (v_inv_inner / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.inventory
                                 + ( (v_inv_each / v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                           END,
                           SYSTIMESTAMP,
                           'consoldtd'
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                     WHERE (    si.sku_id = v_sku_id
                            AND si.ship_unit = v_slot_ship_unit
                            AND si.delete_mult <> 1
                            AND NVL (si.hist_match, '0x') = v_hist_match
                            AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            AND si.slotitem_id = v_min_slotitem_id
                            AND ih.start_date = v_start_date
                            AND ih.end_date = v_end_date
                            AND ih.ship_unit = 4)
                           AND ih.ROWID = ih_Upd.ROWID)
          WHERE ROWID IN
                   (SELECT ih.ROWID
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                     WHERE     si.sku_id = v_sku_id
                           AND si.ship_unit = v_slot_ship_unit
                           AND si.delete_mult <> 1
                           AND NVL (si.hist_match, '0x') = v_hist_match
                           AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           AND si.slotitem_id = v_min_slotitem_id
                           AND ih.start_date = v_start_date
                           AND ih.end_date = v_end_date
                           AND ih.ship_unit = 4);

         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         UPDATE item_history ih_Upd
            SET (movement,
                 inventory,
                 mod_date_time,
                 mod_user) =
                   (SELECT CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.movement
                                 + ( (  v_mov_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.movement
                                 + ( (v_mov_case * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.movement
                                 + ( (v_mov_inner * v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.movement
                                 + (v_mov_each / v_no_of_item_hist_recs)
                           END,
                           CASE
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 0
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_wh_hi
                                      * v_wh_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 1
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ord_hi
                                      * v_ord_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 1
                                   AND v_pallete_pattern = 2
                              THEN
                                 ih.inventory
                                 + ( (  v_inv_pallet
                                      * v_ven_hi
                                      * v_ven_ti
                                      * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 2
                              THEN
                                 ih.inventory
                                 + ( (v_inv_case * v_each_per_cs)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 4
                              THEN
                                 ih.inventory
                                 + ( (v_inv_inner * v_each_per_inn)
                                    / v_no_of_item_hist_recs)
                              WHEN v_item_ship_unit = 8
                              THEN
                                 ih.inventory
                                 + (v_inv_each / v_no_of_item_hist_recs)
                           END,
                           SYSTIMESTAMP,
                           'consoldtd'
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                     WHERE (    si.sku_id = v_sku_id
                            AND si.ship_unit = v_slot_ship_unit
                            AND si.delete_mult <> 1
                            AND NVL (si.hist_match, '0x') = v_hist_match
                            AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                            AND si.slotitem_id = v_min_slotitem_id
                            AND ih.start_date = v_start_date
                            AND ih.end_date = v_end_date
                            AND ih.ship_unit = 8)
                           AND ih.ROWID = ih_Upd.ROWID)
          WHERE ROWID IN
                   (SELECT ih.ROWID
                      FROM    item_history ih
                           INNER JOIN
                              slot_item si
                           ON si.slotitem_id = ih.slotitem_id
                     WHERE     si.sku_id = v_sku_id
                           AND si.ship_unit = v_slot_ship_unit
                           AND si.delete_mult <> 1
                           AND NVL (si.hist_match, '0x') = v_hist_match
                           AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                           AND si.slotitem_id = v_min_slotitem_id
                           AND ih.start_date = v_start_date
                           AND ih.end_date = v_end_date
                           AND ih.ship_unit = 8);

         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;
         v_recs_updtd_tot := v_recs_updtd_tmp + v_recs_updtd_tot;

         IF (v_recs_updtd_tot > 0)
         THEN
            v_strSQLSt :=
               'delete from item_history '
               || ' where hist_id in  ( select hist_id from item_history ih'
               || ' inner join slot_item si on si.slotitem_id = ih.slotitem_id   where si.sku_id = '
               || v_sku_id
               || ' and si.ship_unit ='
               || v_slot_ship_unit
               || ' and si.delete_mult <> '
               || 1
               || ' and NVL(si.hist_match,''0x'') = '''
               || v_hist_match
               || ''' and NVL(si.mult_loc_grp,''0x'') = '''
               || v_mult_loc_grp
               || ''' and si.slot_id is null and si.slotitem_id  <> '
               || v_min_slotitem_id
               || ' and ih.start_date = '''
               || v_start_date
               || ''' and ih.end_date = '''
               || v_end_date
               || ''' and ih.ship_unit = '''
               || v_item_ship_unit
               || ''')';

            BEGIN
               EXECUTE IMMEDIATE v_strSQLSt;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_errCode := SQLCODE;
                  GOTO HANDLE_ERROR;
            END;

            v_bIs_SlotItem_good_to_delete := 1;
         ELSE
            DBMS_OUTPUT.PUT_LINE (
               'ERROR: Oops!!! For some reasons none of the item history records are updated....rolling back transaction');
            v_errCode := -1;
            GOTO HANDLE_ERROR;
         END IF;
      END IF;

      v_i_ih_nextrowid := NULL;

      SELECT NVL (MIN (i_ih_rowid), 0)
        INTO v_i_ih_nextrowid
        FROM tt_TEMP_ITEM_HISTORY_SMRY
       WHERE i_ih_rowid > v_i_ih_currowid;


      IF NVL (v_i_ih_nextrowid, 0) = 0
      THEN
         EXIT;
      END IF;


      SELECT i_ih_rowid,
             ship_unit,
             pallete_pattern,
             nbr_of_picks,
             mov1,
             mov2,
             mov4,
             mov8,
             inv1,
             inv2,
             inv4,
             inv8,
             start_date,
             end_date
        INTO v_i_ih_currowid,
             v_item_ship_unit,
             v_pallete_pattern,
             v_nbr_of_picks,
             v_mov_pallet,
             v_mov_case,
             v_mov_inner,
             v_mov_each,
             v_inv_pallet,
             v_inv_case,
             v_inv_inner,
             v_inv_each,
             v_start_date,
             v_end_date
        FROM tt_TEMP_ITEM_HISTORY_SMRY
       WHERE i_ih_rowid = v_i_ih_nextrowid;
   END LOOP;

   EXECUTE IMMEDIATE ' delete tt_TEMP_ITEM_HISTORY_SMRY ';

   v_no_of_slot_item_recs := 1;

   INSERT INTO tt_TEMP_ITEM_HISTORY_SMRY (ship_unit,
                                          pallete_pattern,
                                          nbr_of_picks,
                                          mov1,
                                          mov2,
                                          mov4,
                                          mov8,
                                          inv1,
                                          inv2,
                                          inv4,
                                          inv8,
                                          start_date,
                                          end_date)
        SELECT DISTINCT v_slot_ship_unit,
                        si.pallete_pattern,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        ih.start_date,
                        ih.end_date
          FROM    item_history ih
               INNER JOIN
                  slot_item si
               ON si.slotitem_id = ih.slotitem_id
         WHERE     si.sku_id = v_sku_id
               AND si.ship_unit = v_slot_ship_unit
               AND si.delete_mult <> 1
               AND NVL (si.hist_match, '0x') = v_hist_match
               AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
               AND si.slotitem_id <> v_min_slotitem_id
               AND si.slot_id IS NULL
      ORDER BY ih.start_date NULLS FIRST, si.pallete_pattern NULLS FIRST;

   v_i_ih_loopctl := 1;

   SELECT MIN (i_ih_rowid)
     INTO v_i_ih_nextrowid
     FROM tt_TEMP_ITEM_HISTORY_SMRY;



   IF (NVL (v_i_ih_nextrowid, 0) = 0)
   THEN
      DBMS_OUTPUT.PUT_LINE (
         'INFO: There is no data in item history table to consolidate by inserting the slot_items that do not have the history for the min. slot item id');
      GOTO LAST_CALL;
   END IF;


   UPDATE tt_TEMP_ITEM_HISTORY_SMRY tihs_Upd
      SET (mov1,
           mov2,
           mov4,
           mov8,
           inv1,
           inv2,
           inv4,
           inv8,
           nbr_of_picks) =
             (SELECT tout.mov1,
                     tout.mov2,
                     tout.mov4,
                     tout.mov8,
                     tout.inv1,
                     tout.inv2,
                     tout.inv4,
                     tout.inv8,
                     tout.nbr_of_picks
                FROM    tt_TEMP_ITEM_HISTORY_SMRY tihs
                     INNER JOIN
                        (  SELECT SUM (thist.mov1) mov1,
                                  SUM (thist.mov2) mov2,
                                  SUM (thist.mov4) mov4,
                                  SUM (thist.mov8) mov8,
                                  SUM (thist.inv1) inv1,
                                  SUM (thist.inv2) inv2,
                                  SUM (thist.inv4) inv4,
                                  SUM (thist.inv8) inv8,
                                  thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern,
                                  SUM (thist.nbr_of_picks) nbr_of_picks
                             FROM (  SELECT NVL (
                                               CASE ih.ship_unit
                                                  WHEN 1
                                                  THEN
                                                     SUM (NVL (ih.movement, 0))
                                               END,
                                               0)
                                               AS mov1,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 2
                                                  THEN
                                                     SUM (NVL (ih.movement, 0))
                                               END,
                                               0)
                                               AS mov2,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 4
                                                  THEN
                                                     SUM (NVL (ih.movement, 0))
                                               END,
                                               0)
                                               AS mov4,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 8
                                                  THEN
                                                     SUM (NVL (ih.movement, 0))
                                               END,
                                               0)
                                               AS mov8,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 1
                                                  THEN
                                                     SUM (NVL (ih.inventory, 0))
                                               END,
                                               0)
                                               AS inv1,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 2
                                                  THEN
                                                     SUM (NVL (ih.inventory, 0))
                                               END,
                                               0)
                                               AS inv2,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 4
                                                  THEN
                                                     SUM (NVL (ih.inventory, 0))
                                               END,
                                               0)
                                               AS inv4,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 8
                                                  THEN
                                                     SUM (NVL (ih.inventory, 0))
                                               END,
                                               0)
                                               AS inv8,
                                            SUM (NVL (ih.nbr_of_picks, 0))
                                            / v_no_of_slot_item_recs
                                               AS nbr_of_picks,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern
                                       FROM    item_history ih
                                            INNER JOIN
                                               slot_item si
                                            ON si.slotitem_id = ih.slotitem_id
                                      WHERE     si.sku_id = v_sku_id
                                            AND si.ship_unit = v_slot_ship_unit
                                            AND si.delete_mult <> 1
                                            AND NVL (si.hist_match, '0x') =
                                                   v_hist_match
                                            AND NVL (si.mult_loc_grp, '0x') =
                                                   v_mult_loc_grp
                                            AND si.slot_id IS NULL
                                            AND si.slotitem_id <>
                                                   v_min_slotitem_id
                                   --SQLWAYS_EVAL# = '2008-08-07 00:00:00.000' and ih.end_date = '2008-08-13 00:00:00.000'
                                   --SQLWAYS_EVAL# = 0
                                   GROUP BY ih.ship_unit,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern) thist
                         GROUP BY thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern) tout
                     ON     tout.start_date = tihs.start_date
                        AND tout.end_date = tihs.end_date
                        AND tout.pallete_pattern = tihs.pallete_pattern
               WHERE tihs.ROWID = tihs_Upd.ROWID)
    WHERE ROWID IN
             (SELECT tihs.ROWID
                FROM    tt_TEMP_ITEM_HISTORY_SMRY tihs
                     INNER JOIN
                        (  SELECT SUM (thist.mov1) mov1,
                                  SUM (thist.mov2) mov2,
                                  SUM (thist.mov4) mov4,
                                  SUM (thist.mov8) mov8,
                                  SUM (thist.inv1) inv1,
                                  SUM (thist.inv2) inv2,
                                  SUM (thist.inv4) inv4,
                                  SUM (thist.inv8) inv8,
                                  thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern,
                                  SUM (thist.nbr_of_picks) nbr_of_picks
                             FROM (  SELECT NVL (
                                               CASE ih.ship_unit
                                                  WHEN 1
                                                  THEN
                                                     SUM (
                                                        NVL (ih.movement,
                                                             0))
                                               END,
                                               0)
                                               AS mov1,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 2
                                                  THEN
                                                     SUM (
                                                        NVL (ih.movement,
                                                             0))
                                               END,
                                               0)
                                               AS mov2,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 4
                                                  THEN
                                                     SUM (
                                                        NVL (ih.movement,
                                                             0))
                                               END,
                                               0)
                                               AS mov4,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 8
                                                  THEN
                                                     SUM (
                                                        NVL (ih.movement,
                                                             0))
                                               END,
                                               0)
                                               AS mov8,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 1
                                                  THEN
                                                     SUM (
                                                        NVL (ih.inventory,
                                                             0))
                                               END,
                                               0)
                                               AS inv1,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 2
                                                  THEN
                                                     SUM (
                                                        NVL (ih.inventory,
                                                             0))
                                               END,
                                               0)
                                               AS inv2,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 4
                                                  THEN
                                                     SUM (
                                                        NVL (ih.inventory,
                                                             0))
                                               END,
                                               0)
                                               AS inv4,
                                            NVL (
                                               CASE ih.ship_unit
                                                  WHEN 8
                                                  THEN
                                                     SUM (
                                                        NVL (ih.inventory,
                                                             0))
                                               END,
                                               0)
                                               AS inv8,
                                            SUM (NVL (ih.nbr_of_picks, 0))
                                            / v_no_of_slot_item_recs
                                               AS nbr_of_picks,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern
                                       FROM    item_history ih
                                            INNER JOIN
                                               slot_item si
                                            ON si.slotitem_id =
                                                  ih.slotitem_id
                                      WHERE si.sku_id = v_sku_id
                                            AND si.ship_unit =
                                                   v_slot_ship_unit
                                            AND si.delete_mult <> 1
                                            AND NVL (si.hist_match, '0x') =
                                                   v_hist_match
                                            AND NVL (si.mult_loc_grp, '0x') =
                                                   v_mult_loc_grp
                                            AND si.slot_id IS NULL
                                            AND si.slotitem_id <>
                                                   v_min_slotitem_id
                                   GROUP BY ih.ship_unit,
                                            ih.start_date,
                                            ih.end_date,
                                            si.pallete_pattern) thist
                         GROUP BY thist.start_date,
                                  thist.end_date,
                                  thist.pallete_pattern) tout
                     ON     tout.start_date = tihs.start_date
                        AND tout.end_date = tihs.end_date
                        AND tout.pallete_pattern = tihs.pallete_pattern);


   SELECT i_ih_rowid,
          ship_unit,
          pallete_pattern,
          nbr_of_picks,
          mov1,
          mov2,
          mov4,
          mov8,
          inv1,
          inv2,
          inv4,
          inv8,
          start_date,
          end_date
     INTO v_i_ih_currowid,
          v_item_ship_unit,
          v_pallete_pattern,
          v_nbr_of_picks,
          v_mov_pallet,
          v_mov_case,
          v_mov_inner,
          v_mov_each,
          v_inv_pallet,
          v_inv_case,
          v_inv_inner,
          v_inv_each,
          v_start_date,
          v_end_date
     FROM tt_TEMP_ITEM_HISTORY_SMRY
    WHERE i_ih_rowid = v_i_ih_nextrowid;



   WHILE v_i_ih_loopctl = 1
   LOOP
      SELECT COUNT (*)
        INTO v_if_exists2
        FROM    item_history ih
             INNER JOIN
                slot_item si
             ON si.slotitem_id = ih.slotitem_id
       WHERE     si.sku_id = v_sku_id
             AND si.ship_unit = v_slot_ship_unit
             AND si.delete_mult <> 1
             AND NVL (si.hist_match, '0x') = v_hist_match
             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
             AND si.slotitem_id = v_min_slotitem_id
             AND si.slot_id IS NULL
             AND ih.start_date = v_start_date
             AND ih.end_date = v_end_date;

      IF v_if_exists2 = 0
      THEN
         EXECUTE IMMEDIATE ('delete tt_TEMP_ITEM_HISTORY_INST');


         SELECT curr_nbr
           INTO v_hist_id
           FROM unique_ids
          WHERE rec_type = 'item_history';

         v_hist_id := v_hist_id + 1;



         INSERT INTO tt_TEMP_ITEM_HISTORY_INST (slotitem_id,
                                                start_date,
                                                end_date,
                                                ship_unit,
                                                nbr_of_picks,
                                                movement,
                                                inventory)
              SELECT si.slotitem_id,
                     v_start_date,
                     v_end_date,
                     v_slot_ship_unit,
                     v_nbr_of_picks,
                     0,
                     0
                FROM slot_item si
               WHERE     si.sku_id = v_sku_id
                     AND si.ship_unit = v_slot_ship_unit
                     AND si.delete_mult <> 1
                     AND NVL (si.hist_match, '0x') = v_hist_match
                     AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                     AND si.slotitem_id = v_min_slotitem_id
                     AND si.slot_id IS NULL
            ORDER BY si.slotitem_id NULLS FIRST;



         UPDATE tt_TEMP_ITEM_HISTORY_INST
            SET hist_id = v_hist_id,
                movement =
                   CASE
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 0
                      THEN
                         ( (v_mov_pallet * v_wh_hi * v_wh_ti * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_mov_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 1
                      THEN
                         ( (  v_mov_pallet
                            * v_ord_hi
                            * v_ord_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_mov_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 2
                      THEN
                         ( (  v_mov_pallet
                            * v_ven_hi
                            * v_ven_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_mov_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 0
                      THEN
                         ( (v_mov_pallet * v_wh_hi * v_wh_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_mov_inner / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 1
                      THEN
                         ( (v_mov_pallet * v_ord_hi * v_ord_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_mov_inner / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 2
                      THEN
                         ( (v_mov_pallet * v_ven_hi * v_ven_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_mov_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_mov_inner / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 0
                      THEN
                         ( (v_mov_pallet * v_wh_hi * v_wh_ti)
                          / v_no_of_slot_item_recs)
                         + (v_mov_case / v_no_of_slot_item_recs)
                         + ( (v_mov_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 1
                      THEN
                         ( (v_mov_pallet * v_ord_hi * v_ord_ti)
                          / v_no_of_slot_item_recs)
                         + (v_mov_case / v_no_of_slot_item_recs)
                         + ( (v_mov_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 2
                      THEN
                         ( (v_mov_pallet * v_ven_hi * v_ven_ti)
                          / v_no_of_slot_item_recs)
                         + (v_mov_case / v_no_of_slot_item_recs)
                         + ( (v_mov_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 0
                      THEN
                         (v_mov_pallet / v_no_of_slot_item_recs)
                         + ( (v_mov_case / (v_wh_hi * v_wh_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner
                              / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each
                              / (v_wh_hi * v_wh_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 1
                      THEN
                         (v_mov_pallet / v_no_of_slot_item_recs)
                         + ( (v_mov_case / (v_ord_hi * v_ord_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner
                              / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each
                              / (v_ord_hi * v_ord_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 2
                      THEN
                         (v_mov_pallet / v_no_of_slot_item_recs)
                         + ( (v_mov_case / (v_ven_hi * v_ven_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_inner
                              / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_mov_each
                              / (v_ven_hi * v_ven_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                   END,
                inventory =
                   CASE
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 0
                      THEN
                         ( (v_inv_pallet * v_wh_hi * v_wh_ti * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_inv_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 1
                      THEN
                         ( (  v_inv_pallet
                            * v_ord_hi
                            * v_ord_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_inv_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 8 AND v_pallete_pattern = 2
                      THEN
                         ( (  v_inv_pallet
                            * v_ven_hi
                            * v_ven_ti
                            * v_each_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_each_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner * v_each_per_inn)
                            / v_no_of_slot_item_recs)
                         + (v_inv_each / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 0
                      THEN
                         ( (v_inv_pallet * v_wh_hi * v_wh_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_inv_inner / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 1
                      THEN
                         ( (v_inv_pallet * v_ord_hi * v_ord_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_inv_inner / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 4 AND v_pallete_pattern = 2
                      THEN
                         ( (v_inv_pallet * v_ven_hi * v_ven_ti * v_inn_per_cs)
                          / v_no_of_slot_item_recs)
                         + ( (v_inv_case * v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + (v_inv_inner / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_inn)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 0
                      THEN
                         ( (v_inv_pallet * v_wh_hi * v_wh_ti)
                          / v_no_of_slot_item_recs)
                         + (v_inv_case / v_no_of_slot_item_recs)
                         + ( (v_inv_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 1
                      THEN
                         ( (v_inv_pallet * v_ord_hi * v_ord_ti)
                          / v_no_of_slot_item_recs)
                         + (v_inv_case / v_no_of_slot_item_recs)
                         + ( (v_inv_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 2 AND v_pallete_pattern = 2
                      THEN
                         ( (v_inv_pallet * v_ven_hi * v_ven_ti)
                          / v_no_of_slot_item_recs)
                         + (v_inv_case / v_no_of_slot_item_recs)
                         + ( (v_inv_inner / v_inn_per_cs)
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each / v_each_per_cs)
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 0
                      THEN
                         (v_inv_pallet / v_no_of_slot_item_recs)
                         + ( (v_inv_case / (v_wh_hi * v_wh_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner
                              / (v_wh_hi * v_wh_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each
                              / (v_wh_hi * v_wh_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 1
                      THEN
                         (v_inv_pallet / v_no_of_slot_item_recs)
                         + ( (v_inv_case / (v_ord_hi * v_ord_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner
                              / (v_ord_hi * v_ord_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each
                              / (v_ord_hi * v_ord_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                      WHEN v_slot_ship_unit = 1 AND v_pallete_pattern = 2
                      THEN
                         (v_inv_pallet / v_no_of_slot_item_recs)
                         + ( (v_inv_case / (v_ven_hi * v_ven_ti))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_inner
                              / (v_ven_hi * v_ven_ti * v_inn_per_cs))
                            / v_no_of_slot_item_recs)
                         + ( (v_inv_each
                              / (v_ven_hi * v_ven_ti * v_each_per_cs))
                            / v_no_of_slot_item_recs)
                   END;


         v_strSQLSt :=
            'insert into  item_history (hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks)
      select hist_id,slotitem_id,start_date,end_date,ship_unit,movement,inventory,nbr_of_picks from tt_temp_item_history_inst';


         BEGIN
            EXECUTE IMMEDIATE v_strSQLSt;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_errCode := SQLCODE;
               GOTO HANDLE_ERROR;
         END;

         SWV_ROWCOUNT := SQL%ROWCOUNT;
         v_recs_updtd_tmp := SWV_ROWCOUNT;


         v_recs_updtd_tmp := 0;

         UPDATE unique_ids
            SET curr_nbr = v_hist_id
          WHERE rec_type = 'item_history';

         v_strSQLSt :=
            'delete from item_history '
            || ' where hist_id in  ( select hist_id from item_history ih'
            || ' inner join slot_item si on si.slotitem_id = ih.slotitem_id   where si.sku_id = '
            || v_sku_id
            || ' and si.ship_unit ='
            || v_slot_ship_unit
            || ' and si.delete_mult <> '
            || 1
            || ' and NVL(si.hist_match,''0x'') = '''
            || v_hist_match
            || ''' and NVL(si.mult_loc_grp,''0x'') = '''
            || v_mult_loc_grp
            || ''' and si.slot_id is null and si.slotitem_id <> '
            || v_min_slotitem_id
            || ' and ih.start_date = '''
            || v_start_date
            || ''' and ih.end_date = '''
            || v_end_date
            || ''')';

         BEGIN
            EXECUTE IMMEDIATE v_strSQLSt;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_errCode := SQLCODE;
               GOTO HANDLE_ERROR;
         END;


         v_bIs_SlotItem_good_to_delete := 1;
      END IF;

      v_i_ih_nextrowid := NULL;


      SELECT NVL (MIN (i_ih_rowid), 0)
        INTO v_i_ih_nextrowid
        FROM tt_TEMP_ITEM_HISTORY_SMRY
       WHERE i_ih_rowid > v_i_ih_currowid;

      IF NVL (v_i_ih_nextrowid, 0) = 0
      THEN
         EXIT;
      END IF;


      SELECT i_ih_rowid,
             ship_unit,
             pallete_pattern,
             nbr_of_picks,
             mov1,
             mov2,
             mov4,
             mov8,
             inv1,
             inv2,
             inv4,
             inv8,
             start_date,
             end_date
        INTO v_i_ih_currowid,
             v_item_ship_unit,
             v_pallete_pattern,
             v_nbr_of_picks,
             v_mov_pallet,
             v_mov_case,
             v_mov_inner,
             v_mov_each,
             v_inv_pallet,
             v_inv_case,
             v_inv_inner,
             v_inv_each,
             v_start_date,
             v_end_date
        FROM tt_TEMP_ITEM_HISTORY_SMRY
       WHERE i_ih_rowid = v_i_ih_nextrowid;
   END LOOP;

  <<LAST_CALL>>
   IF (v_bIs_SlotItem_good_to_delete = 1)
   THEN
      DELETE FROM slot_item_score
            WHERE slot_item_score.slotitem_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL
                             AND si.slotitem_id <> v_min_slotitem_id);

      DELETE FROM hn_failure
            WHERE hn_failure.slotitem_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL
                             AND si.slotitem_id <> v_min_slotitem_id);

      DELETE FROM haves_needs
            WHERE haves_needs.slot_item_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL
                             AND si.slotitem_id <> v_min_slotitem_id);

      DELETE FROM needs_results_dtl
            WHERE needs_results_dtl.slotitem_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL
                             AND si.slotitem_id <> v_min_slotitem_id);

      DBMS_OUTPUT.PUT_LINE ('    Deleting records in sl_failure_reasons...');

      DELETE FROM sl_failure_reasons
            WHERE sl_failure_reasons.slotitem_id IN
                     (SELECT DISTINCT si.slotitem_id
                        FROM slot_item si
                       WHERE     si.sku_id = v_sku_id
                             AND si.ship_unit = v_slot_ship_unit
                             AND si.delete_mult <> 1
                             AND NVL (si.hist_match, '0x') = v_hist_match
                             AND NVL (si.mult_loc_grp, '0x') = v_mult_loc_grp
                             AND si.slot_id IS NULL
                             AND si.slotitem_id <> v_min_slotitem_id);

      DBMS_OUTPUT.PUT_LINE (
         '    Finally deleting all the unslotted records (where slot_id is null) for selected group in slot_item recs ...');
      v_strSQLSt :=
            'delete from SLOT_ITEM '
         || ' where slotitem_id in  ( select slotitem_id from slot_item si'
         || ' where si.sku_id = '
         || v_sku_id
         || ' and si.ship_unit ='
         || v_slot_ship_unit
         || ' and si.delete_mult <>'
         || 1
         || ' and NVL(si.hist_match,''0x'') = '''
         || v_hist_match
         || ''' and NVL(si.mult_loc_grp,''0x'') = '''
         || v_mult_loc_grp
         || ''' and si.slot_id is null and si.slotitem_id <> '
         || v_min_slotitem_id
         || ')';



      BEGIN
         EXECUTE IMMEDIATE v_strSQLSt;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_errCode := SQLCODE;
            GOTO HANDLE_ERROR;
      END;
   ELSE
      DBMS_OUTPUT.PUT_LINE (
         'WARNING: slot item records are not deleted for this SKU group since there is no change made in ITEM_HISTORY');
   END IF;

   v_errCode := 0;

  <<HANDLE_ERROR>>
   NULL;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_CUBE_MOVEMENT_SUM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_CUBE_MOVEMENT_SUM (v_range_group_id IN NUMBER,
    v_whse_code IN NVARCHAR2, v_refcur OUT SYS_REFCURSOR)
   as
   v_movement  NUMBER(15,2);
   v_no_of_items  NUMBER(10,0);
   v_system  NUMBER(10,0);
   v_metric_unit  NUMBER(10,0);
   v_metric_vol  NUMBER(10,0);
   v_cube_conversion  NUMBER(15,4);
   v_exp  NUMBER(13,4);
   v_base  NUMBER(13,4);
BEGIN

   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE4');
   select   count(*) INTO v_no_of_items from tt_RANGE_IDS_TABLE4;

   begin
      select   whprefs_system, metric_unit, metric_vol INTO v_system,v_metric_unit,v_metric_vol from whprefs where whse_code = v_whse_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

--SQLWAYS_EVAL# of unit is english
   IF v_system = 1 then

      v_cube_conversion := POWER(12,3);
   ELSE
      v_exp := v_metric_vol -v_metric_unit;
      v_base := POWER(10,v_exp);
      v_cube_conversion := POWER(v_base,3);
   end if;

   begin
      select   sum(case
      when v_system = 1 then /*Case Cube unit*/
         case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when v_system = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot  on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from tt_RANGE_IDS_TABLE4);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when v_system = 1 then /*Case Cube unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
            else 0.0
            end
         when v_system = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
               case
               when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
               else 0.0
               end
            when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
               case
               when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
               else 0.0
               end
            when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
               case
               when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*((so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/v_cube_conversion)
               else 0.0
               end
            when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
               case
               when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))*((so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/v_cube_conversion)
               else 0.0
               end
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot  on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE4);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE4 ';

   if (v_movement >= 0) then

      open v_refcur for select v_movement  from dual;
   else
      v_movement := -0.1;
      open v_refcur for select v_movement  from dual;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_CUBE_MOVEMENT_SUM2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_CUBE_MOVEMENT_SUM2 (v_range_group_id IN NUMBER,
    v_whse_code IN NVARCHAR2,
    v_movement IN OUT NUMBER) as
   v_no_of_items  NUMBER(10,0);
   v_system  NUMBER(10,0);
   v_metric_unit  NUMBER(10,0);
   v_metric_vol  NUMBER(10,0);
   v_cube_conversion  NUMBER(15,4);
   v_exp  NUMBER(13,4);
   v_base  NUMBER(13,4);
BEGIN

   sp_get_range_ids(v_range_group_id,'RANGE_IDS_TABLE5');
   select   count(*) INTO v_no_of_items from RANGE_IDS_TABLE5;

   begin
      select   whprefs_system, metric_unit, metric_vol INTO v_system,v_metric_unit,v_metric_vol from whprefs where whse_code = v_whse_code;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

    --SQLWAYS_EVAL# of unit is english
   IF v_system = 1 then

      v_cube_conversion := POWER(12,3);
   ELSE
      v_exp := v_metric_vol -v_metric_unit;
      v_base := POWER(10,v_exp);
      v_cube_conversion := POWER(v_base,3);
   end if;

   begin
      select   sum(case
      when v_system = 1 then /*Case Cube unit*/
         case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when v_system = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot  on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from RANGE_IDS_TABLE5);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when v_system = 1 then /*Case Cube unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
            else 0.0
            end
         when v_system = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
               case
               when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
               else 0.0
               end
            when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
               case
               when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
               else 0.0
               end
            when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
               case
               when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*((so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/v_cube_conversion)
               else 0.0
               end
            when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
               case
               when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))*((so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/v_cube_conversion)
               else 0.0
               end
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot  on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from RANGE_IDS_TABLE5);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE RANGE_IDS_TABLE5 ';

   if (v_movement < 0) then

      v_movement := -0.1;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_CUSTOM_IMPORT_HOOK
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_CUSTOM_IMPORT_HOOK (format_id in number, called_from in number)
/*
Custom stored proc name should be SP_CUSTOM_IMPORT_SP and take the above 2 parameters as inputs

Called From Values
1 - Item master (After populating data into import_temp but before the import process
2 - After Item master import completes
3 - Slots (After populating data into the staging table but before the import process
4 - After Slots import completes
5 - Warehouse (After populating data into import_temp but before the import process
6 - After Warehouse import completes
*/
AS
  c number;
  query_str varchar2(500);
BEGIN
  SELECT count(*) into c FROM USER_OBJECTS WHERE object_type = 'PROCEDURE' AND object_name = 'SP_CUSTOM_IMPORT_SP';
  query_str := 'SP_CUSTOM_IMPORT_SP(:1, :2)';
  if (c > 0) then
      execute immediate 'begin ' || query_str || '; end;' using format_id, called_from;
  end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_DATEADD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_DATEADD (vchDatePart   IN   VARCHAR,
                                                intOP         IN INTEGER,
                                                dt     out          DATE ) AS
 dd          INT;
   mm          INT;
   yyyy        INT := 0;
   hh          INT;
   NN          INT;
   SS          INT;
   v           DATE;
   lintOP      INT;
   AM          VARCHAR2 (2);


   V_DATE      DATE;

BEGIN
   lintOP := intOP;
   dt := SYSTIMESTAMP;



   --  INcreament  Days
   IF    UPPER (vchDatePart) = 'DD'
      OR UPPER (vchDatePart) = 'DAY'
      OR UPPER (vchDatePart) = 'D'
   THEN

      dt := dt + intOP;
   END IF;



   --  INcreament  Year
   IF    UPPER (vchDatePart) = 'YYYY'
      OR UPPER (vchDatePart) = 'YY'
      OR UPPER (vchDatePart) = 'YEAR'
   THEN
      yyyy := yyyy + lintOP;
   END IF;

   -- Increment Day of the Year

   IF    UPPER (vchDatePart) = 'DY'
      OR UPPER (vchDatePart) = 'Y'
      OR UPPER (vchDatePart) = 'DAYOFYEAR'
   THEN

      dt := dt + intOP;
   END IF;

   -- Increment week
   IF    UPPER (vchDatePart) = 'WEEK'
      OR UPPER (vchDatePart) = 'WW'
      OR UPPER (vchDatePart) = 'WK'
   THEN

      dt := dt + ROUND (lintOP * 7);
   END IF;

   --  INcreament  Month.
   IF    (UPPER (vchDatePart) = 'MM')
      OR (UPPER (vchDatePart) = 'M')
      OR (UPPER (vchDatePart) = 'MONTH')
  THEN
      yyyy := yyyy + TRUNC ( (lintOP / 12), 0);
      mm := mm + MOD (lintOP, 12);
   END IF;                                                               -->MM
 --  INcreament  Hour.
   IF UPPER (vchDatePart) LIKE 'H%'
   THEN

      dt := dt + (lintOP / (24));
   END IF;                                                              --> hh

   -- Increment quarter
   IF    (UPPER (vchDatePart) = 'QQ')
      OR (UPPER (vchDatePart) = 'Q')
      OR (UPPER (vchDatePart) = 'QUARTER')
   THEN
      yyyy := yyyy + TRUNC ( (lintOP / 12), 0);
      mm := mm + MOD (ROUND (lintOP * 3), 12);
   END IF;                                                               -->QQ

   --RETURN SYSDATE+100;
   IF    (UPPER (vchDatePart) = 'MI')
      OR (UPPER (vchDatePart) = 'N')
      OR (UPPER (vchDatePart) = 'MINUTE')
   THEN

      dt := dt + (lintOP / (24 * 60));
   END IF;                                                         --> MInutes

   IF UPPER (vchDatePart) LIKE 'S%'
   THEN
      -- timezone changes not needed here
      -- as we are adding date and not accessing
      -- any localized fields.
      dt := dt + (lintOP / (24 * 60 * 60));
   END IF;                                                              --> SS

   -- Increment milliseconds
   IF UPPER (vchDatePart) = 'MS' OR UPPER (vchDatePart) = 'MILLISECOND'
   THEN

      dt := dt + (lintOP / (24 * 60 * 60 * 1000));
   END IF;

   v :=
      LAST_DAY (
         TO_DATE (
            '01/' || TO_CHAR (mm, '09') || '/' || TO_CHAR (yyyy, '0009'),
            'dd/mm/yyyy'));

   IF dd > TO_NUMBER (TO_CHAR (v, 'DD'))
   THEN
      dd := TO_NUMBER (TO_CHAR (v, 'DD'));
   END IF;

   dt :=
      to_timestamp(
            LPAD (dd, 2, '0')
         || TO_CHAR (mm, '09')
         || '/'
         || TO_CHAR (yyyy, '0009')
         || ' '
         || LPAD (hh, 2, '0')
         || ':'
         || LPAD (NN, 2, '0')
         || ':'
         || LPAD (SS, 2, '0')
         || ' '
         || LPAD (AM, 2, 0),
         'MM/DD/yyyy HH24:MI:SS');

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (
         'Error: This feature not supported in this function.');

END;

/
--------------------------------------------------------
--  DDL for Procedure SP_DROPTEMPSLOTITEM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_DROPTEMPSLOTITEM 
   as
   v_if_exists NUMBER(10,0);
BEGIN
 SELECT   COUNT(*) INTO v_if_exists FROM user_tables WHERE table_name = 'tt_TEMP_SLOT_ITEM2';
   if v_if_exists > 0 then

      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_TEMP_SLOT_ITEM2 ';
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_ENRICH_IMPORTTEMP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_ENRICH_IMPORTTEMP (v_whse_code IN NVARCHAR2) as
BEGIN


   UPDATE import_temp SET(sku_id) =(SELECT distinct im.sku_id FROM  so_item_master im where
   im.sku_name = import_temp.sku_name AND
   im.whse_code = import_temp.whse_code AND
   im.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from
   so_item_master im where
   im.sku_name = import_temp.sku_name AND
   im.whse_code = import_temp.whse_code AND
   im.whse_code = v_whse_code);

   UPDATE import_temp SET(slot_id) =(SELECT distinct s.slot_id FROM  slot s where
   s.dsp_slot = import_temp.dsp_slot AND
   s.whse_code = import_temp.whse_code AND
   s.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from
   slot s where
   s.dsp_slot = import_temp.dsp_slot AND
   s.whse_code = import_temp.whse_code AND
   s.whse_code = v_whse_code);

   UPDATE import_temp SET(slotitem_id) =(SELECT distinct si.slotitem_id FROM  slot_item si where
   import_temp.slot_id = si.slot_id and
   import_temp.sku_id = si.sku_id and
   import_temp.ship_unit_id = si.ship_unit) WHERE ROWID IN(SELECT import_temp.ROWID from
   slot_item si where
   import_temp.slot_id = si.slot_id and
   import_temp.sku_id = si.sku_id and
   import_temp.ship_unit_id = si.ship_unit);

   UPDATE import_temp SET(parent_node_id) =(SELECT distinct td.parent_node_id FROM  tree_dtl td,slot sl where
   import_temp.slot_id = sl.slot_id and
   sl.my_range = td.node_id) WHERE ROWID IN(SELECT import_temp.ROWID from
   tree_dtl td, slot sl where
   import_temp.slot_id = sl.slot_id and
   sl.my_range = td.node_id);


END;

/
--------------------------------------------------------
--  DDL for Procedure SP_ENRICH_IMPORTTEMP_FOR_ITMAS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_ENRICH_IMPORTTEMP_FOR_ITMAS (v_whse_code IN NVARCHAR2) as
BEGIN

--SQLWAYS_EVAL# --------------------------------------------------------------------------------------------------------
--SQLWAYS_EVAL# table with sku_ids

   UPDATE import_temp SET(sku_id) =(SELECT distinct im.sku_id FROM  so_item_master im where
   im.sku_name = import_temp.sku_name AND
   im.whse_code = import_temp.whse_code AND
   im.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from
   so_item_master im where
   im.sku_name = import_temp.sku_name AND
   im.whse_code = import_temp.whse_code AND
   im.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);

--SQLWAYS_EVAL# --------------------------------------------------------------------------------------------------------

--SQLWAYS_EVAL# --------------------------------------------------------------------------------------------------------

--SQLWAYS_EVAL# ids
   UPDATE import_temp SET(imp_commodity_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.commodity_class = cat_code.cat_code and
   cat_code.cat_id = 3 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.commodity_class = cat_code.cat_code and
   cat_code.cat_id = 3 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(imp_crush_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.crushability_code = cat_code.cat_code and
   cat_code.cat_id = 4 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.crushability_code = cat_code.cat_code and
   cat_code.cat_id = 4 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(imp_hazard_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.hazard_code = cat_code.cat_code and
   cat_code.cat_id = 5 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.hazard_code = cat_code.cat_code and
   cat_code.cat_id = 5 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(imp_vendor_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.vendor_code = cat_code.cat_code and
   cat_code.cat_id = 6 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.vendor_code = cat_code.cat_code and
   cat_code.cat_id = 6 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(imp_misc1_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.misc1 = cat_code.cat_code and
   cat_code.cat_id = 1 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.misc1 = cat_code.cat_code and
   cat_code.cat_id = 1 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(imp_misc2_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.misc2 = cat_code.cat_code and
   cat_code.cat_id = 2 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.misc2 = cat_code.cat_code and
   cat_code.cat_id = 2 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(imp_misc3_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.misc3 = cat_code.cat_code and
   cat_code.cat_id = 8 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.misc3 = cat_code.cat_code and
   cat_code.cat_id = 8 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(imp_misc4_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.misc4 = cat_code.cat_code and
   cat_code.cat_id = 9 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.misc4 = cat_code.cat_code and
   cat_code.cat_id = 9 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(imp_misc5_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.misc5 = cat_code.cat_code and
   cat_code.cat_id = 10 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.misc5 = cat_code.cat_code and
   cat_code.cat_id = 10 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(imp_misc6_id) =(SELECT distinct cat_code.cat_code_id FROM  cat_code where import_temp.misc6 = cat_code.cat_code and
   cat_code.cat_id = 11 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from cat_code where import_temp.misc6 = cat_code.cat_code and
   cat_code.cat_id = 11 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(existing_comm_id,existing_comm_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 3 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 3 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);

   UPDATE import_temp SET(existing_crush_id,existing_crush_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 4 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 4 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(existing_hazard_id,existing_hazard_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 5 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 5 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(existing_vendor_id,existing_vendor_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 6 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 6 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(existing_misc1_id,existing_misc1_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 1 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 1 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(existing_misc2_id,existing_mis2_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 2 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 2 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(existing_misc3_id,existing_misc3_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 8 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 8 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(existing_misc4_id,existing_misc4_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 9 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 9 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(existing_misc5_id,existing_misc5_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 10 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 10 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


   UPDATE import_temp SET(existing_misc6_id,existing_misc6_name) =(SELECT distinct item_cat_xref.cat_code_id,cat_code.cat_code FROM  item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 11 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from item_cat_xref
   inner join cat_code on item_cat_xref.cat_code_id = cat_code.cat_code_id where import_temp.sku_id = item_cat_xref.sku_id and
   item_cat_xref.cat_id = 11 and
   cat_code.whse_code = v_whse_code AND
   import_temp.whse_code = v_whse_code);


--SQLWAYS_EVAL# --------------------------------------------------------------------------------------------------------



END;

/
--------------------------------------------------------
--  DDL for Procedure SP_ENRICH_IT_BEFORE_CLEANUP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_ENRICH_IT_BEFORE_CLEANUP (v_whse_code IN VARCHAR2) as
BEGIN

   UPDATE import_temp SET(sku_id) =(SELECT distinct im.sku_id FROM  so_item_master im where
   im.sku_name = import_temp.sku_name AND
   im.whse_code = import_temp.whse_code AND
   im.whse_code = v_whse_code) WHERE ROWID IN(SELECT import_temp.ROWID from
   so_item_master im where
   im.sku_name = import_temp.sku_name AND
   im.whse_code = import_temp.whse_code AND
   im.whse_code = v_whse_code);

END;

/
--------------------------------------------------------
--  DDL for Procedure SP_FILL_SNS_CALC
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_FILL_SNS_CALC (p_sns_id sns.sns_id%type) as
    v_area_mask  sns.area_mask%type;
    v_zone_mask  sns.zone_mask%type;
    v_aisle_mask  sns.aisle_mask%type;
    v_bay_mask  sns.bay_mask%type;
    v_level_mask  sns.level_mask%type;
    v_pos_mask  sns.pos_mask%type;
    v_fill_mask  sns.fill_mask%type;
    v_area_stmt  VARCHAR2 (1000);
    v_zone_stmt  VARCHAR2 (1000);
    v_aisle_stmt  VARCHAR2 (1000);
    v_bay_stmt  VARCHAR2 (1000);
    v_level_stmt  VARCHAR2 (1000);
    v_pos_stmt  VARCHAR2 (1000);
    v_fill_stmt  VARCHAR2 (1000);
    v_fill_cnt number;
    v_if_exists number;
begin

   SELECT   COUNT(*) INTO v_if_exists FROM sns_calc;
   IF  v_if_exists > 0 then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE sns_calc ';
   end if;

select area_mask,zone_mask,aisle_mask,bay_mask,level_mask,pos_mask,fill_mask    into v_area_mask,v_zone_mask,v_aisle_mask,v_bay_mask,v_level_mask,v_pos_mask,v_fill_mask
    from sns
    where sns_id  = p_sns_id;

    FOR i IN 1..30 LOOP
      if(BITAND(v_area_mask,power(2,i-1)) = power(2,i-1) ) then
      insert into sns_calc values('area',i);
      end if;
       if(BITAND(v_zone_mask,power(2,i-1)) = power(2,i-1) ) then
      insert into sns_calc values('zone',i);
      end if;
       if(BITAND(v_aisle_mask,power(2,i-1)) = power(2,i-1) ) then
      insert into sns_calc values('aisle',i);
      end if;
       if(BITAND(v_bay_mask,power(2,i-1)) = power(2,i-1) ) then
      insert into sns_calc values('bay',i);
      end if;
       if(BITAND(v_level_mask,power(2,i-1)) = power(2,i-1) ) then
     insert into sns_calc values('level',i);
      end if;
       if(BITAND(v_pos_mask,power(2,i-1)) = power(2,i-1) ) then
       insert into sns_calc values('pos',i);
      end if;
       if(BITAND(v_fill_mask,power(2,i-1)) = power(2,i-1) ) then
      insert into sns_calc values('fill',i);

      end if;

   END LOOP;

   commit;

end SP_FILL_SNS_CALC;

/
--------------------------------------------------------
--  DDL for Procedure SP_GENERICCOUNTQUERY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_GENERICCOUNTQUERY (sqlqry in varchar2,l_cnt out number)
as
begin
   execute immediate sqlqry into l_cnt;
end;

/
--------------------------------------------------------
--  DDL for Procedure SP_GENERICUPDATEORDELQUERY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_GENERICUPDATEORDELQUERY (sqlqry in varchar2)
as
begin
   execute immediate sqlqry;
end;

/
--------------------------------------------------------
--  DDL for Procedure SP_GET_RANGE_IDS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_GET_RANGE_IDS (v_rg_id IN NUMBER,
	v_range_ids_table IN VARCHAR2) as
   v_rg_level  NUMBER(10,0);
   v_line  CHAR(20);
   v_sql_statement_string  VARCHAR2(255);
   v_sql_statement_string1  VARCHAR2(255);
   SWV_rg_id NUMBER(10,0);
 v_no_of_items1  NUMBER(10,0);
   v_if_exists NUMBER(10,0);
BEGIN
   SWV_rg_id := v_rg_id;

   INSERT INTO TT_STACK  VALUES(SWV_rg_id, 1);

   v_rg_level := 1;

   WHILE v_rg_level > 0 LOOP
      SELECT   COUNT(*) INTO v_if_exists FROM TT_STACK WHERE range_level = v_rg_level;
      IF v_if_exists > 0 then

         begin
            select   range_id INTO SWV_rg_id FROM TT_STACK WHERE range_level = v_rg_level and rownum=1;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
         DELETE FROM TT_STACK
         WHERE range_level = v_rg_level
         AND range_id = SWV_rg_id;
         INSERT INTO TT_STACK
         SELECT node_id, v_rg_level+1
         FROM tree_dtl
         WHERE parent_node_id = SWV_rg_id;
         IF SQL%ROWCOUNT > 0 then
            v_rg_level := v_rg_level+1;
         end if;
         IF SQL%ROWCOUNT = 0 then


            v_sql_statement_string := 'INSERT INTO ' ||
            v_range_ids_table ||
            ' VALUES( '  ||
            SUBSTR(CAST(SWV_rg_id AS CHAR),1,20) ||
            ' )';
            EXECUTE IMMEDIATE v_sql_statement_string;
         end if;
      ELSE
         v_rg_level := v_rg_level -1;
      end if;
   END LOOP; -- WHILE


   EXECUTE IMMEDIATE ' TRUNCATE TABLE TT_STACK ';
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_GET_UNIQUE_IDS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_GET_UNIQUE_IDS (v_rec_type IN varchar2, v_nextnumber out number)

   as


BEGIN
    select (curr_nbr+1) into v_nextnumber from unique_ids where rec_type = v_rec_type;
    update unique_ids set curr_nbr = v_nextnumber  where rec_type = v_rec_type;

  -- RETURN(v_nextnumber);
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_IH_INVENTORY_AS_EACHES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_IH_INVENTORY_AS_EACHES AS
BEGIN
EXECUTE IMMEDIATE 'create global temporary table testtemp (HIST_ID NUMBER(19), VAL NUMBER)';
EXECUTE IMMEDIATE 'create index TESTTEMPIDX on TESTTEMP(HIST_ID)';
EXECUTE IMMEDIATE 'insert into testtemp SELECT hist_id,
                (CASE IH.ship_unit
                    WHEN 2
                    then
                       (IH.INVENTORY * IM.each_per_cs)
                    WHEN 4
                    then
                       ( (IH.INVENTORY / IM.each_per_inn) * IM.each_per_cs)
                    WHEN 1
                    THEN
                       ( (IH.INVENTORY * si.pallet_hi
                          * (CASE si.pallete_pattern
                                WHEN 0 THEN IM.wh_ti
                                WHEN 1 THEN IM.ord_ti
                                WHEN 2 THEN IM.ven_ti
                             end))
                        * IM.each_per_cs)
                 END)
                   val
           FROM item_history IH
                JOIN slot_item si
                   ON si.slotitem_id = IH.slotitem_id
                JOIN item_master IM
                   on IM.SKU_ID = SI.SKU_ID
          WHERE IH.ship_unit != 8  and IH.INVENTORY > 0 ' ;


EXECUTE IMMEDIATE 'update ITEM_HISTORY a set a.INVENTORY= (select VAL from  TESTTEMP B where a.HIST_ID=B.HIST_ID) where a.SHIP_UNIT !=8 and a.INVENTORY > 0';
EXECUTE IMMEDIATE 'drop table TESTTEMP';
END SP_IH_INVENTORY_AS_EACHES;

/
--------------------------------------------------------
--  DDL for Procedure SP_IH_MOVEMENT_AS_EACHES
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_IH_MOVEMENT_AS_EACHES AS 
begin
 execute immediate 'create global temporary table testtemp (HIST_ID NUMBER(19), VAL NUMBER)';
EXECUTE IMMEDIATE 'create index TESTTEMPIDX on TESTTEMP(HIST_ID)';
EXECUTE IMMEDIATE 'insert into testtemp SELECT hist_id,
                (CASE IH.ship_unit
                    WHEN 2
                    then
                       (IH.movement * IM.each_per_cs)
                    WHEN 4
                    then
                       ( (IH.movement / IM.each_per_inn) * IM.each_per_cs)
                    WHEN 1
                    THEN
                       ( (IH.movement * si.pallet_hi
                          * (CASE si.pallete_pattern
                                WHEN 0 THEN IM.wh_ti
                                WHEN 1 THEN IM.ord_ti
                                WHEN 2 THEN IM.ven_ti
                             end))
                        * IM.each_per_cs)
                 END)
                   val
           FROM item_history IH
                JOIN slot_item si
                   ON si.slotitem_id = IH.slotitem_id
                JOIN so_item_master IM
                   on IM.SKU_ID = SI.SKU_ID
          WHERE IH.ship_unit != 8  and IH.movement > 0 ' ;         


EXECUTE IMMEDIATE 'update ITEM_HISTORY a set a.movement= (select VAL from  TESTTEMP B where a.HIST_ID=B.HIST_ID) where a.SHIP_UNIT !=8 and a.movement > 0';
EXECUTE IMMEDIATE 'drop table TESTTEMP';
END SP_IH_MOVEMENT_AS_EACHES;

/
--------------------------------------------------------
--  DDL for Procedure SP_IS_UI_RUNNING
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_IS_UI_RUNNING (p_session_id IN OUTPUT_RESULT_TABLE.sessionid%type,
                                             p_process_id IN OUTPUT_RESULT_TABLE.processid%type,
                                             p_flag OUT NUMBER)
IS
BEGIN

   SELECT COUNT(1) INTO p_flag FROM output_result_table
       WHERE sessionid = p_session_id AND
             processid = p_process_id AND
             status = 20;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('ZERO RECORDS UPDATED');
   WHEN others THEN
            DBMS_OUTPUT.PUT_LINE('Error' || SQLCODE || SQLERRM );
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_LEFT_JUSTIFY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_LEFT_JUSTIFY (v_range_group_id IN NUMBER) as
   v_slot_slot_id  NUMBER(10,0);
   v_slot_width  NUMBER(13,4);
   v_slot_item_slot_id  NUMBER(10,0);
   v_slot_item_slot_width  NUMBER(13,4);
   v_slot_item_row_num  NUMBER(10,0);
   v_slot_item_sku_id  NUMBER(10,0);
   v_slot_item_slotitem_id  NUMBER(10,0);
   v_slot_item_slot_unit  NUMBER(10,0);
   v_slot_item_cur_orientation  NUMBER(10,0);
   v_slot_item_rec_lanes  NUMBER(10,0);
   v_slot_area  CHAR(4);
   v_slot_zone  CHAR(4);
   v_slot_aisle  CHAR(4);
   v_slot_bay  CHAR(4);
   v_slot_lvl  CHAR(4);
   v_slot_rack_level_id  NUMBER(10,0);
   v_slot_rack_type  NUMBER(10,0);
   v_slot_label_pos  NUMBER(9,4);
   v_prev_slot_label_pos  NUMBER(9,4);
   v_slot_lock  NUMBER(1,0);
   v_compress_slot_lock  NUMBER(1,0);
   v_ar  CHAR(4);
   v_zo  CHAR(4);
   v_ai  CHAR(4);
   v_ba  CHAR(4);
   v_lv  CHAR(4);
   v_empty_slot_id  NUMBER(10,0);
   v_empty_slot_slotitem_id  NUMBER(10,0);
   v_rack_type_rack_type  NUMBER(10,0);
   v_rack_type_movable_label  NUMBER(10,0);
   v_rack_type_label_wid  NUMBER(13,4);
   v_rack_type_overlap_dist  NUMBER(13,4);
   v_rack_type_level_rack_type  NUMBER(10,0);
   v_rack_type_level_use_full_wid  NUMBER(10,0);
   v_rack_type_level_lane_guides  NUMBER(1,0);
   v_rack_type_level_guide_width  NUMBER(13,4);
   v_rack_type_level_side_clear  NUMBER(13,4);
   v_ok  NUMBER(10,0);
   v_collapse_slot  NUMBER(10,0);
   v_compress_slot_id  NUMBER(10,0);
   v_compress_slot_width  NUMBER(13,4);
   v__ar  CHAR(4);
   v__zo  CHAR(4);
   v__ai  CHAR(4);
   v__ba  CHAR(4);
   v__lv  CHAR(4);
   v_so_item_master_case_ht  NUMBER(9,4);
   v_so_item_master_case_len  NUMBER(9,4);
   v_so_item_master_case_wid  NUMBER(9,4);
   v_so_item_master_inn_ht  NUMBER(9,4);
   v_so_item_master_inn_len  NUMBER(9,4);
   v_so_item_master_inn_wid  NUMBER(9,4);
   v_so_item_master_each_ht  NUMBER(9,4);
   v_so_item_master_each_len  NUMBER(9,4);
   v_so_item_master_each_wid  NUMBER(9,4);
   v_ht  NUMBER(9,4);
   v_ln  NUMBER(9,4);
   v_wd  NUMBER(9,4);
   v_oriented_width  NUMBER(9,4);
   v_guide_width  NUMBER(13,4);
   v_side_clearance  NUMBER(13,4);
   v_item_width  NUMBER(13,4);
   v_total_width  NUMBER(13,4);
   v_range_cur_id  NUMBER(10,0);
   v_record_count  NUMBER(10,0);
   v_shelf_count  NUMBER(10,0);

   CURSOR range_cursor IS SELECT range_id FROM tt_RANGE_IDS_TABLE6;
   CURSOR present_slot IS SELECT
   SLOT.slot_id, SLOT.area, SLOT.zone, SLOT.aisle, SLOT.bay, SLOT.lvl, SLOT.rack_level_id, SLOT.rack_type, SLOT.width ,SLOT.LOCKED
   FROM
   slot SLOT
   WHERE
   SLOT.my_range = v_range_cur_id
   ORDER BY area NULLS FIRST,zone NULLS FIRST,aisle NULLS FIRST,bay NULLS FIRST,lvl NULLS FIRST,
   posn NULLS FIRST;
   CURSOR num_cursor  IS SELECT slot_id, width , LOCKED
   FROM slot
   WHERE
   my_range = v_range_cur_id and
   area = v_ar and
   zone = v_zo and
   aisle = v_ai and
   bay = v_ba and
   lvl = v_lv
   ORDER BY area NULLS FIRST,zone NULLS FIRST,aisle NULLS FIRST,bay NULLS FIRST,lvl NULLS FIRST,
   posn NULLS FIRST;
--SQLWAYS_EVAL# ariables

BEGIN

--SQLWAYS_EVAL# variables



   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE6');

   v_record_count := 0;

   OPEN range_cursor;
   FETCH range_cursor INTO v_range_cur_id;
   WHILE (range_cursor%FOUND) LOOP --{


    --SQLWAYS_EVAL# slot widths with original widths

      UPDATE slot_item SET(slot_width) =(SELECT distinct slot.width FROM  slot WHERE slot_item.slot_id = slot.slot_id
      AND   slot.my_range =  v_range_cur_id
      AND   slot_item.slot_width = 0.0) WHERE ROWID IN(SELECT slot_item.ROWID FROM  slot_item, slot WHERE slot_item.slot_id = slot.slot_id
      AND   slot.my_range =  v_range_cur_id
      AND   slot_item.slot_width = 0.0);

    --SQLWAYS_EVAL# widths with original widths


    --SQLWAYS_EVAL#
      OPEN present_slot;
      FETCH present_slot INTO v_slot_slot_id,v_slot_area,v_slot_zone,v_slot_aisle,v_slot_bay,v_slot_lvl,
      v_slot_rack_level_id,v_slot_rack_type,v_slot_width,v_slot_lock;
      v_record_count := v_record_count+1;
      v_shelf_count := v_record_count;
      WHILE (present_slot%FOUND) LOOP --SQLWAYS_EVAL# through all slots in the slot table]

        --SQLWAYS_EVAL# to make sure we're dealing with slots on the same shelf
         v_ar := v_slot_area;
         v_zo := v_slot_zone;
         v_ai := v_slot_aisle;
         v_ba := v_slot_bay;
         v_lv := v_slot_lvl;
         v_empty_slot_id := 0;
         v_ok := 0;/*0=FALSE*/
         v_total_width := 0.0;
         WHILE (v_ar = v_slot_area AND v_zo = v_slot_zone AND v_ai = v_slot_aisle AND v_ba = v_slot_bay
         AND v_lv = v_slot_lvl AND present_slot%FOUND) LOOP --SQLWAYS_EVAL# for each shelf]

            --SQLWAYS_EVAL# the slot has fixed lables does NOT use full widths
            begin
               select   rack_type, movable_label, label_wid, overlap_dist INTO v_rack_type_rack_type,v_rack_type_movable_label,v_rack_type_label_wid,
               v_rack_type_overlap_dist FROM rack_type WHERE rack_type = v_slot_rack_type;
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            end;
            IF (v_rack_type_rack_type IS NOT NULL) then
             --{
               begin
                  select   rack_type, use_full_wid INTO v_rack_type_level_rack_type,v_rack_type_level_use_full_wid FROM rack_type_level WHERE rack_level_id = v_slot_rack_level_id;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
               end;
               IF (v_rack_type_level_rack_type IS NOT NULL) then
                 --{
                  IF (v_rack_type_movable_label = 0) then
                     --{
                     IF (v_rack_type_level_use_full_wid = 0) then
                         --{
                        v_ok := 1;
                     end if;
                  end if;
               end if;
            end if; --}
            --SQLWAYS_EVAL# the slot has fixed lables and uses full widths

            IF (v_ok = 1) then/*SQLWAYS_EVAL# labels and not full widths*/
             --{

               begin
                  select   slot_id, sku_id, slot_width, slotitem_id, cur_orientation, rec_lanes INTO v_slot_item_slot_id,v_slot_item_sku_id,v_slot_item_slot_width,v_slot_item_slotitem_id,
                  v_slot_item_cur_orientation,v_slot_item_rec_lanes FROM slot_item WHERE slot_id = v_slot_slot_id;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
               end;

                --SQLWAYS_EVAL# is empty
               IF (v_slot_item_sku_id IS NULL and v_slot_lock = 0) then
                 --{
                    --SQLWAYS_EVAL# already remembered.
                  IF (v_empty_slot_id = 0 and v_slot_item_slot_width > 0.0) then
                     --{
                        --SQLWAYS_EVAL# slot
                     v_empty_slot_id := v_slot_slot_id;
                     v_empty_slot_slotitem_id := v_slot_item_slotitem_id;
                  end if; --}

               ELSE --{
                    --SQLWAYS_EVAL# we've previously encountered an empty slot.
                  IF (v_empty_slot_id <> 0 and v_slot_lock = 0) then
                     --{
                        --SQLWAYS_EVAL# to empty found earlier.
                     begin
                        select   slotitem_id INTO v_slot_item_slotitem_id FROM slot_item WHERE slot_id = v_slot_slot_id;
                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           NULL;
                     end;
                     UPDATE slot_item
                     SET slot_id = v_empty_slot_id
                     WHERE slotitem_id = v_slot_item_slotitem_id;
                     UPDATE slot_item
                     SET slot_id = v_slot_slot_id
                     WHERE slotitem_id = v_empty_slot_slotitem_id;
                        --SQLWAYS_EVAL# to empty found earlier.
                        --SQLWAYS_EVAL# starting of the shelf hence re-initialize @record_count

                     v_record_count := v_shelf_count;
                     FETCH present_slot
                     INTO v_slot_slot_id,v_slot_area,v_slot_zone,v_slot_aisle,v_slot_bay,v_slot_lvl,
                     v_slot_rack_level_id,v_slot_rack_type,v_slot_width,v_slot_lock;
                     EXIT;
                  end if; --}

                    /*SQLWAYS_EVAL# space this item needs. Get the oriented
                      width of the item accounting for the slot unit*/
                  begin
                     select   slot_unit INTO v_slot_item_slot_unit FROM slot_item WHERE slotitem_id = v_slot_item_slotitem_id;
                     EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  end;
                  begin
                     select   case_ht, case_len, case_wid, inn_ht, inn_len, inn_wid, each_ht, each_len, each_wid INTO v_so_item_master_case_ht,v_so_item_master_case_len,v_so_item_master_case_wid,v_so_item_master_inn_ht,
                     v_so_item_master_inn_len,v_so_item_master_inn_wid,v_so_item_master_each_ht,
                     v_so_item_master_each_len,v_so_item_master_each_wid FROM so_item_master WHERE sku_id = v_slot_item_sku_id;
                     EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  end;

                        --SQLWAYS_EVAL# Pallets or Bins
                  IF (v_slot_item_slot_unit = 2) then--Case
                     --{
                     v_ht    := v_so_item_master_case_ht;
                     v_ln := v_so_item_master_case_len;
                     v_wd := v_so_item_master_case_wid;
                  end if; --}
                  IF (v_slot_item_slot_unit = 4) then--Inner
                     --{
                     v_ht    := v_so_item_master_inn_ht;
                     v_ln := v_so_item_master_inn_len;
                     v_wd := v_so_item_master_inn_wid;
                  end if; --}
                  IF (v_slot_item_slot_unit = 8) then--Each
                     --{
                     v_ht    := v_so_item_master_each_ht;
                     v_ln := v_so_item_master_each_len;
                     v_wd := v_so_item_master_each_wid;
                  end if; --}

                  IF (v_slot_item_cur_orientation = 0 OR v_slot_item_cur_orientation = 2) then
                     --{
                     v_oriented_width := v_wd; --}
                  ELSE --{
                     IF (v_slot_item_cur_orientation = 1 OR v_slot_item_cur_orientation = 3) then
                         --{
                        v_oriented_width := v_ln; --}
                     ELSE --{
                        v_oriented_width := v_ht;
                     end if;
                  end if; --}

                    --SQLWAYS_EVAL# lanes and guides and side clearances
                  begin
                     select   lane_guides, guide_width, side_clear INTO v_rack_type_level_lane_guides,v_rack_type_level_guide_width,v_rack_type_level_side_clear FROM rack_type_level WHERE rack_level_id = v_slot_rack_level_id;
                     EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  end;
                  IF (v_rack_type_level_lane_guides = 0) then--0==False
                     --{
                     v_guide_width := 0.0; --}
                  ELSE --{
                     v_guide_width := v_rack_type_level_guide_width;
                  end if; --}
                  v_side_clearance := v_rack_type_level_side_clear;
                  v_item_width :=(v_oriented_width*v_slot_item_rec_lanes)+(v_slot_item_rec_lanes*(v_guide_width -1))+(v_slot_item_rec_lanes*(v_side_clearance+1));
                  v_total_width := v_total_width+v_item_width;

                    --SQLWAYS_EVAL# lanes and guides and side clearances


                    /*SQLWAYS_EVAL# if this item needs multiple slots by
                    seeing which labels it overlaps*/

                  v_collapse_slot := 0;
                  OPEN num_cursor;
                  FETCH num_cursor INTO
                  v_compress_slot_id,v_compress_slot_width,v_compress_slot_lock;
                  WHILE (num_cursor%FOUND) LOOP --{
                     begin
                        select   label_pos INTO v_slot_label_pos FROM slot WHERE slot_id = v_compress_slot_id;
                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           NULL;
                     end;
                     IF (v_collapse_slot = 1  and v_compress_slot_lock = 0) then
                         --{
                        IF (v_total_width >(v_slot_label_pos+v_rack_type_overlap_dist)) then
                             --{
                           UPDATE slot_item
                           SET slot_width = 0.0
                           WHERE slot_id = v_compress_slot_id; --}
                        ELSE --{
                           EXIT;
                        end if;
                     end if; --}
                     IF (v_compress_slot_id = v_slot_slot_id and v_compress_slot_lock = 0) then
                         --{
                        v_collapse_slot := 1;

                            --SQLWAYS_EVAL# item and this one overlap on the same label
                            --SQLWAYS_EVAL# t.
                        IF ((v_total_width -v_item_width) > 0.0 and(v_total_width -v_item_width) <(v_prev_slot_label_pos+v_rack_type_label_wid)) then
                             --{
                           v_total_width := v_prev_slot_label_pos+v_rack_type_label_wid+v_item_width;
                        end if; --}
                     ELSE --{
                        v_prev_slot_label_pos := v_slot_label_pos;
                     end if; --}

                     FETCH num_cursor INTO
                     v_compress_slot_id,v_compress_slot_width,v_compress_slot_lock;
                  END LOOP; --}
                  CLOSE num_cursor;
               end if;
            end if; --SQLWAYS_EVAL# = 1)]

            FETCH present_slot INTO v_slot_slot_id,v_slot_area,v_slot_zone,v_slot_aisle,v_slot_bay,v_slot_lvl,
            v_slot_rack_level_id,v_slot_rack_type,v_slot_width,v_slot_lock;
            v_record_count := v_record_count+1;
         END LOOP; --SQLWAYS_EVAL# each shelf]
         v_shelf_count := v_record_count;
      END LOOP; --SQLWAYS_EVAL# all slots in the slot table]
      CLOSE present_slot;

      FETCH range_cursor INTO v_range_cur_id;
   END LOOP;  --SQLWAYS_EVAL#

   CLOSE range_cursor;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE6 ';

END;

/
--------------------------------------------------------
--  DDL for Procedure SP_LEFT_JUSTIFY_FIXED_FULL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_LEFT_JUSTIFY_FIXED_FULL (v_range_group_id IN NUMBER) as
   v_slot_slot_id  NUMBER(10,0);
   v_slot_width  NUMBER(13,4);
   v_slot_area  CHAR(4);
   v_slot_zone  CHAR(4);
   v_slot_aisle  CHAR(4);
   v_slot_bay  CHAR(4);
   v_slot_lvl  CHAR(4);
   v_slot_rack_level_id  NUMBER(10,0);
   v_slot_rack_type  NUMBER(10,0);
   v_ar  CHAR(4);
   v_zo  CHAR(4);
   v_ai  CHAR(4);
   v_ba  CHAR(4);
   v_lv  CHAR(4);
   v_rack_type_rack_type  NUMBER(10,0);
   v_rack_type_movable_label  NUMBER(10,0);
   v_rack_type_level_rack_type  NUMBER(10,0);
   v_rack_type_level_use_full_wid  NUMBER(10,0);
   v_rack_type_level_lane_guides  NUMBER(1,0);
   v_rack_type_level_guide_width  NUMBER(13,4);
   v_rack_type_level_side_clear  NUMBER(13,4);
   v_racktypelevelonefacingperdsw  NUMBER(1,0);
   v_racktypelevelmovableguides  NUMBER(10,0);
   v_ok  NUMBER(10,0);
   v_i  NUMBER(10,0);
   v_facing  NUMBER(10,0);
   v_slot_item_slot_id  NUMBER(10,0);
   v_slot_item_slot_width  NUMBER(13,4);
   v_slot_item_sku_id  NUMBER(10,0);
   v_slot_item_slotitem_id  NUMBER(10,0);
   v_slu  NUMBER(10,0);
   v_orient  NUMBER(10,0);
   v_lanes  NUMBER(10,0);
   v_ht  NUMBER(9,4);
   v_ln  NUMBER(9,4);
   v_wd  NUMBER(9,4);
   v_ow  NUMBER(9,4);
   v_gw  NUMBER(13,4);
   v_sc  NUMBER(13,4);
   v_item_width  NUMBER(9,4);
   v_num_slots  NUMBER(10,0);
   v_temp_quotient  NUMBER(9,4);
   v_available_shelf_space  NUMBER(13,4);
   v_required_shelf_space  NUMBER(13,4);
   v_temp_items_row_num  NUMBER(10,0);
   v_temp_items_width  NUMBER(9,4);
   v_temp_items_sku_id  NUMBER(10,0);
   v_temp_items_num_slots  NUMBER(10,0);
   v_temp_items_slotitem  NUMBER(10,0);
   v_temp_items_req_width  NUMBER(9,4);
   v_slots_counter  NUMBER(10,0);
   v_temp_slots_slot_id  NUMBER(10,0);
   v_temp_slots_slotitem  NUMBER(10,0);
   v_temp_slotitems_slot  NUMBER(10,0);
   v_temp_slotitems_item  NUMBER(10,0);
   v_temp_slotitem_origin  NUMBER(10,0);
   v_temp_slotitem_dest  NUMBER(10,0);
   v_temp_slotitem_compress  NUMBER(10,0);
   v_original_slot  NUMBER(10,0);
   v_temp_slotitems_row  NUMBER(10,0);
   v_row_count  NUMBER(10,0);
   v_slot_width_req  NUMBER(9,4);
   v_range_cur_id  NUMBER(10,0);



































 --SQLWAYS_EVAL# the item
 --SQLWAYS_EVAL# for the item
 --SQLWAYS_EVAL# lanes
 --SQLWAYS_EVAL# slotting unit
 --SQLWAYS_EVAL# slotting unit
 --SQLWAYS_EVAL# slotting unit
 --SQLWAYS_EVAL# for the item
 --Guide width
 --Side clearance































--SQLWAYS_EVAL# riables

--SQLWAYS_EVAL# slot widths with original widths
--SQLWAYS_EVAL# slot widths with original widths
   CURSOR range_cursor IS SELECT range_id FROM tt_RANGE_IDS_TABLE;
   CURSOR range_cursor2 IS SELECT range_id FROM tt_RANGE_IDS_TABLE;
   CURSOR replace_cursor IS SELECT slot_id, width FROM slot
   WHERE slot.my_range = v_range_cur_id;
   CURSOR slot_cursor IS SELECT slot_id, area, zone, aisle, bay, lvl, rack_level_id, rack_type, width
   FROM slot
   WHERE slot.my_range = v_range_cur_id
   ORDER BY area NULLS FIRST,zone NULLS FIRST,aisle NULLS FIRST,bay NULLS FIRST,lvl NULLS FIRST,
   posn NULLS FIRST;
   CURSOR item_cursor IS SELECT  width, sku_id, num_slots, slotitem, slot_width_req
   FROM temp_items;
   SWV_temp_slotitems__var0 NUMBER(10,0);
BEGIN

--SQLWAYS_EVAL# procedure. It fills out a table containing all range_ids under a
--SQLWAYS_EVAL# id.
--SQLWAYS_EVAL# that gets passed to the sp_get_range_ids procedure






--SQLWAYS_EVAL# les
   OPEN range_cursor;
   FETCH range_cursor INTO v_range_cur_id;
   WHILE (range_cursor%FOUND) LOOP --1

      OPEN replace_cursor;
      FETCH replace_cursor INTO v_slot_slot_id,v_slot_width;
      WHILE (replace_cursor%FOUND) LOOP --2
         begin
            select   slot_width INTO v_slot_item_slot_width FROM slot_item WHERE slot_id = v_slot_slot_id;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
         IF (v_slot_item_slot_width = 0.0) then
                 --3
            UPDATE slot_item
            SET slot_width = v_slot_width
            WHERE slot_id = v_slot_slot_id;
         end if; --3
         FETCH replace_cursor INTO v_slot_slot_id,v_slot_width;
      END LOOP; --2
      CLOSE replace_cursor;

      FETCH range_cursor INTO v_range_cur_id;
   END LOOP; --1
   CLOSE range_cursor;

--SQLWAYS_EVAL# widths with original widths

--SQLWAYS_EVAL# stify
   OPEN range_cursor2;
   FETCH range_cursor2 INTO v_range_cur_id;
   WHILE (range_cursor2%FOUND) LOOP --SQLWAYS_EVAL#


      OPEN slot_cursor;
      FETCH slot_cursor
      INTO     v_slot_slot_id,v_slot_area,v_slot_zone,v_slot_aisle,v_slot_bay,v_slot_lvl,
      v_slot_rack_level_id,v_slot_rack_type,v_slot_width;
      WHILE (slot_cursor%FOUND) LOOP --SQLWAYS_EVAL# all the slots in the slots table


        --SQLWAYS_EVAL# tables used for each shelf


         v_ar := v_slot_area;
         v_zo := v_slot_zone;
         v_ai := v_slot_aisle;
         v_ba := v_slot_bay;
         v_lv := v_slot_lvl;
         v_available_shelf_space := 0.0;
         v_required_shelf_space := 0.0;
         v_ok := 0;
         v_num_slots := 0;
         v_facing := 0;

        --SQLWAYS_EVAL# stay on the same shelf
         WHILE (v_ar = v_slot_area AND v_zo = v_slot_zone AND v_ai = v_slot_aisle AND v_ba = v_slot_bay
         AND v_lv = v_slot_lvl AND slot_cursor%FOUND) LOOP --SQLWAYS_EVAL# each shelf

            --SQLWAYS_EVAL# the slot has fixed lables and uses full widths
            begin
               select   rack_type, movable_label INTO v_rack_type_rack_type,v_rack_type_movable_label FROM rack_type WHERE rack_type = v_slot_rack_type;
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            end;
            IF (v_rack_type_rack_type IS NOT NULL) then
             --3
               begin
                  select   rack_type, use_full_wid, lane_guides, guide_width, side_clear, one_facing_per_dsw, move_guides INTO v_rack_type_level_rack_type,v_rack_type_level_use_full_wid,v_rack_type_level_lane_guides,
                  v_rack_type_level_guide_width,v_rack_type_level_side_clear,
                  v_racktypelevelonefacingperdsw,v_racktypelevelmovableguides FROM rack_type_level WHERE rack_level_id = v_slot_rack_level_id;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
               end;
               IF (v_rack_type_level_rack_type IS NOT NULL) then
                 --4
                    --CR 9844
                  IF (v_rack_type_level_use_full_wid = 1 AND v_racktypelevelmovableguides = 1) then
                     --5
                     v_ok := 1; /*1=TRUE*/

                        --SQLWAYS_EVAL# flag is true
                     IF (v_racktypelevelonefacingperdsw = 1) then
                         --6
                        v_facing := 1;
                     end if;
                  end if;
               end if;
            end if; --3
            --SQLWAYS_EVAL# the slot has fixed lables and uses full widths

            --SQLWAYS_EVAL# labels and uses full widths.
            IF (v_ok = 1) then
             --3
                --SQLWAYS_EVAL# slotitem information
               begin
                  select   slot_id, sku_id, slot_width, slotitem_id, slot_unit, cur_orientation, rec_lanes INTO v_slot_item_slot_id,v_slot_item_sku_id,v_slot_item_slot_width,v_slot_item_slotitem_id,
                  v_slu,v_orient,v_lanes FROM slot_item WHERE slot_id = v_slot_slot_id;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
               end;

                --SQLWAYS_EVAL# table with the slot information
               INSERT INTO temp_slots
                 VALUES(v_slot_slot_id, v_slot_width, v_slot_item_slotitem_id);

               IF (v_slot_item_sku_id IS NOT NULL) then
                 --4
                    --SQLWAYS_EVAL# width and number of lanes required by the item
                  IF (v_slot_width <> 0) then
                     --SQLWAYS_EVAL#     -- First get the oriented width of the item accounting  for the slot unit.

                        --SQLWAYS_EVAL# statement for Pallets CR15360

                     IF (v_slu = 2) then --Case
                        BEGIN --6
                           select   case_ht, case_len, case_wid INTO v_ht,v_ln,v_wd FROM so_item_master WHERE so_item_master.sku_id = v_slot_item_sku_id;
                           EXCEPTION
                           WHEN NO_DATA_FOUND THEN
                              NULL;
                        END;
                     end if; --6

                     IF (v_slu = 4) then --Inner
                        BEGIN --6
                           select   inn_ht, inn_len, inn_wid INTO v_ht,v_ln,v_wd FROM so_item_master WHERE so_item_master.sku_id = v_slot_item_sku_id;
                           EXCEPTION
                           WHEN NO_DATA_FOUND THEN
                              NULL;
                        END;
                     end if; --6

                     IF (v_slu = 8) then --Each
                        BEGIN --6
                           select   each_ht, each_len, each_wid INTO v_ht,v_ln,v_wd FROM so_item_master WHERE so_item_master.sku_id = v_slot_item_sku_id;
                           EXCEPTION
                           WHEN NO_DATA_FOUND THEN
                              NULL;
                        END;
                     end if; --6

                        --SQLWAYS_EVAL# statement for Pallets CR15360

                     IF (v_orient = 0 OR v_orient = 2) then
                         --6
                        v_ow := v_wd; --6

                     ELSE
                        IF (v_orient = 1 OR v_orient = 4) then
                         --6
                           v_ow := v_ln; --6

                        ELSE --6
                           v_ow := v_ht;
                        end if;
                     end if; --6

                        --SQLWAYS_EVAL# multiple lanes and guides and side clearances.
                     IF (v_rack_type_level_lane_guides = 0) then --0 is false
                         --6
                        v_gw := 0.0; --6
                     ELSE --6
                        v_gw := v_rack_type_level_guide_width;
                     end if; --6

                     v_sc := v_rack_type_level_side_clear;
                     v_item_width :=(v_ow*v_lanes)+(v_lanes*(v_gw -1))+(v_lanes*(v_sc+1));
                     IF (v_facing = 0) then
                         --6
                            --SQLWAYS_EVAL# the number of slots this item needs.
                        v_temp_quotient := v_item_width/v_slot_width;
                        v_num_slots := CEIL(v_temp_quotient);
                     end if; --6

                     IF (v_facing = 1) then
                         --6
                        v_num_slots := v_lanes;
                     end if; --6

                        --SQLWAYS_EVAL# the slot width required.
                     v_slot_width_req := v_num_slots*v_slot_width; --5
                  ELSE --5
                     v_num_slots := 1;
                  end if; --5

                    --SQLWAYS_EVAL# information into the temp_item table
                  INSERT INTO temp_items
                     VALUES(v_slot_item_sku_id, v_item_width, v_num_slots, v_slot_item_slotitem_id, v_slot_width_req);
               end if;
            end if; --3 End: ok = 1

            FETCH slot_cursor
            INTO     v_slot_slot_id,v_slot_area,v_slot_zone,v_slot_aisle,v_slot_bay,v_slot_lvl,
            v_slot_rack_level_id,v_slot_rack_type,v_slot_width;
         END LOOP; --SQLWAYS_EVAL# each shelf

        --SQLWAYS_EVAL# far, step back one slot.


        --SQLWAYS_EVAL# shelf space available and the total shelf space required.
         begin
            select   SUM(width) INTO v_available_shelf_space FROM temp_slots;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
         begin
            select   SUM(slot_width_req) INTO v_required_shelf_space FROM temp_items;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;

        --SQLWAYS_EVAL# counter
         v_slots_counter := 1;
         OPEN item_cursor;
         FETCH item_cursor
         INTO     v_temp_items_width,v_temp_items_sku_id,v_temp_items_num_slots,
         v_temp_items_slotitem,v_temp_items_req_width;
         WHILE (item_cursor%FOUND) LOOP --2

            IF (v_available_shelf_space >=(v_required_shelf_space)) then
             --3
               v_i := 0;

                --SQLWAYS_EVAL# by the item
               WHILE (v_i < v_temp_items_num_slots) LOOP --4
                  begin
                     select   slot_id, slotitem INTO v_temp_slots_slot_id,v_temp_slots_slotitem FROM     temp_slots ;
                     EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  end;

                    --SQLWAYS_EVAL# only the slots following the slot where the item is slotted
                  IF (v_i = 0) then
                     --5
                     
                    
                     
                     INSERT INTO temp_slotitems
                         VALUES(v_temp_slots_slot_id, v_temp_items_sku_id, v_temp_items_slotitem, v_temp_slots_slotitem, 0); --5
                  ELSE --5
                     INSERT INTO temp_slotitems
                         VALUES(v_temp_slots_slot_id, v_temp_items_sku_id, v_temp_items_slotitem, v_temp_slots_slotitem, 1);
                  end if; --5

                  v_slots_counter := v_slots_counter+1;
                  v_i := v_i+1;
               END LOOP; --4

                --SQLWAYS_EVAL# item's required width from the total required width
               v_available_shelf_space := v_available_shelf_space -v_temp_items_req_width;
               v_required_shelf_space := v_required_shelf_space -v_temp_items_req_width; --SQLWAYS_EVAL# room
            ELSE --3
               begin
                  select   slot_id, slotitem INTO v_temp_slots_slot_id,v_temp_slots_slotitem FROM     temp_slots ;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
               end;
               INSERT INTO temp_slotitems
                 VALUES(v_temp_slots_slot_id, v_temp_items_sku_id, v_temp_items_slotitem, v_temp_slots_slotitem, 0);

               v_slots_counter := v_slots_counter+1;

                --SQLWAYS_EVAL# item's required width from the total required width
               v_available_shelf_space := v_available_shelf_space -v_temp_items_req_width;
               v_required_shelf_space := v_required_shelf_space -v_temp_items_req_width;
            end if; --3
            FETCH item_cursor
            INTO     v_temp_items_width,v_temp_items_sku_id,v_temp_items_num_slots,
            v_temp_items_slotitem,v_temp_items_req_width;
         END LOOP; --2

         CLOSE item_cursor;


        --SQLWAYS_EVAL# the actual changes. Loop through the temp_slotitems table
         v_row_count := 1;
         LOOP
            SELECT COUNT(*) INTO SWV_temp_slotitems__var0 FROM temp_slotitems;
            exit  when NOT (v_row_count <= SWV_temp_slotitems__var0);
            BEGIN --2
               begin
                  select    SLOT_TEMPITEMS, item, origin_slotitem, dest_slotitem, COMPRESS_TEMPITEMS INTO v_temp_slotitems_slot,v_temp_slotitems_item,v_temp_slotitem_origin,
                  v_temp_slotitem_dest,v_temp_slotitem_compress FROM temp_slotitems ;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
               end;
               IF (v_temp_slotitem_compress = 1) then
             --3
                  UPDATE slot_item
                  SET slot_width = 0.0
                  WHERE slot_id = v_temp_slotitems_slot; --3
               ELSE --3
                --SQLWAYS_EVAL# if required
                  IF (v_temp_slotitem_origin <> v_temp_slotitem_dest) then
                 --4
                     begin
                        select   slot_id INTO v_original_slot FROM slot_item WHERE slotitem_id = v_temp_slotitem_origin;
                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           NULL;
                     end;
                     UPDATE slot_item
                     SET slot_id = v_original_slot
                     WHERE slotitem_id = v_temp_slotitem_dest;
                     UPDATE slot_item
                     SET slot_id = v_temp_slotitems_slot
                     WHERE slotitem_id = v_temp_slotitem_origin;

                    --SQLWAYS_EVAL# table
                     UPDATE temp_slotitems
                     SET dest_slotitem = v_temp_slotitem_dest
                     WHERE dest_slotitem = v_temp_slotitem_origin;
                  end if;
               end if; --3
               v_row_count := v_row_count+1;
            END;
         END LOOP; --SQLWAYS_EVAL# end of temp_slotsitems

        --SQLWAYS_EVAL# and temp_items tables. A new one will be created for each shelf
         EXECUTE IMMEDIATE ' TRUNCATE TABLE temp_slots ';
         EXECUTE IMMEDIATE ' TRUNCATE TABLE temp_items ';
         EXECUTE IMMEDIATE ' TRUNCATE TABLE temp_slotitems ';
         FETCH slot_cursor
         INTO     v_slot_slot_id,v_slot_area,v_slot_zone,v_slot_aisle,v_slot_bay,v_slot_lvl,
         v_slot_rack_level_id,v_slot_rack_type,v_slot_width;
      END LOOP; --SQLWAYS_EVAL# all the slots in the slots table
    --SQLWAYS_EVAL#

    --Begin: Clean up
      CLOSE slot_cursor;

    --End: Clean up

      FETCH range_cursor2 INTO v_range_cur_id;
   END LOOP;  --SQLWAYS_EVAL#

   CLOSE range_cursor2;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_LEFT_JUSTIFY_MOVEABLE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_LEFT_JUSTIFY_MOVEABLE (v_range_group_id IN NUMBER) as
   v_slot_slot_id  NUMBER(10,0);
   v_slot_width  NUMBER(13,4);
   v_slot_lock  NUMBER(1,0);
   v_slot_item_slot_id  NUMBER(10,0);
   v_slot_item_slot_width  NUMBER(13,4);
   v_slot_item_row_num  NUMBER(10,0);
   v_slot_item_sku_id  NUMBER(10,0);
   v_slot_item_slotitem_id  NUMBER(10,0);
   v_slot_item_ign_for_reslot  NUMBER(1,0);
   v_slot_row_num  NUMBER(10,0);
   v_slot_area  CHAR(4);
   v_slot_zone  CHAR(4);
   v_slot_aisle  CHAR(4);
   v_slot_bay  CHAR(4);
   v_slot_lvl  CHAR(4);
   v_slot_rack_level_id  NUMBER(10,0);
   v_slot_rack_type  NUMBER(10,0);
   v_ar  CHAR(4);
   v_zo  CHAR(4);
   v_ai  CHAR(4);
   v_ba  CHAR(4);
   v_lv  CHAR(4);
   v_empty_slot_id  NUMBER(10,0);
   v_empty_slot_slotitem_id  NUMBER(10,0);
   v_rack_type_rack_type  NUMBER(10,0);
   v_rack_type_movable_label  NUMBER(10,0);
   v_racktypelevelmovableguides  NUMBER(10,0);
   v_rack_type_level_rack_type  NUMBER(10,0);
   v_rack_type_level_use_full_wid  NUMBER(10,0);
   v_ok  NUMBER(10,0);
   v_count  NUMBER(10,0);
   v_i  NUMBER(10,0);
   v_temp_quotient  NUMBER(13,4);
   v_num_slots  NUMBER(10,0);
   v_compress_slot_id  NUMBER(10,0);
   v_compress_slot_width  NUMBER(13,4);
   v__ar  CHAR(4);
   v__zo  CHAR(4);
   v__ai  CHAR(4);
   v__ba  CHAR(4);
   v__lv  CHAR(4);
   v_range_cur_id  NUMBER(10,0);
   v_record_count  NUMBER(10,0);
   v_shelf_count  NUMBER(10,0);

--SQLWAYS_EVAL# ariables

   CURSOR range_cursor IS SELECT range_id FROM tt_RANGE_IDS_TABLE7;
   CURSOR present_slot IS SELECT
   slot_id, area, zone, aisle, bay, lvl, rack_level_id, rack_type, width, LOCKED
   FROM slot
   WHERE slot.my_range = v_range_cur_id
   ORDER BY area NULLS FIRST,zone NULLS FIRST,aisle NULLS FIRST,bay NULLS FIRST,lvl NULLS FIRST,
   posn NULLS FIRST;
BEGIN

--SQLWAYS_EVAL# indexes
--SQLWAYS_EVAL# ON slot (area,zone,aisle,bay,lvl)
--SQLWAYS_EVAL# ON slot_item (slot_id)
--SQLWAYS_EVAL# ndexes

--SQLWAYS_EVAL# procedure. It fills out a table containing all range_ids under a
--SQLWAYS_EVAL# id.
--SQLWAYS_EVAL# that gets passed to the sp_get_range_ids procedure

   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE7');

--SQLWAYS_EVAL# variables
   OPEN range_cursor;
   FETCH range_cursor INTO v_range_cur_id;
   WHILE (range_cursor%FOUND) LOOP
      v_record_count := 0;

    --SQLWAYS_EVAL# [moveable]
      OPEN present_slot;
      FETCH present_slot INTO v_slot_slot_id,v_slot_area,v_slot_zone,v_slot_aisle,v_slot_bay,v_slot_lvl,
      v_slot_rack_level_id,v_slot_rack_type,v_slot_width,v_slot_lock;
      v_record_count := v_record_count+1;
      v_shelf_count := v_record_count;
      WHILE (present_slot%FOUND) LOOP --SQLWAYS_EVAL# through all slots in the slot table]
            --SQLWAYS_EVAL# to make sure we're dealing with slots on the same shelf
         v_ar := v_slot_area;
         v_zo := v_slot_zone;
         v_ai := v_slot_aisle;
         v_ba := v_slot_bay;
         v_lv := v_slot_lvl;
         v_empty_slot_id := 0;
         v_empty_slot_slotitem_id := '';
         v_ok := 0;/*0=FALSE*/

         WHILE (v_ar = v_slot_area AND v_zo = v_slot_zone AND v_ai = v_slot_aisle AND v_ba = v_slot_bay
         AND v_lv = v_slot_lvl AND present_slot%FOUND) LOOP --SQLWAYS_EVAL# for each shelf]

                --SQLWAYS_EVAL# the slot has movable labels and guides
            begin
               select   rack_type, movable_label INTO v_rack_type_rack_type,v_rack_type_movable_label FROM rack_type WHERE rack_type = v_slot_rack_type;
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            end;
            begin
               select   move_guides INTO v_racktypelevelmovableguides FROM rack_type_level WHERE rack_type = v_slot_rack_type AND rack_level_id = v_slot_rack_level_id;
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            end;
            IF (v_rack_type_rack_type IS NOT NULL) then
                 --3
               IF (v_rack_type_movable_label = 1 AND v_racktypelevelmovableguides = 1) then
                  v_ok := 1;
               end if;
            end if; --3
                --SQLWAYS_EVAL# the slot has movable labels

            IF (v_ok = 1) then/*SQLWAYS_EVAL# labels*/
                 --3

               begin
                  select   slot_id, sku_id, slot_width, slotitem_id, ign_for_reslot INTO v_slot_item_slot_id,v_slot_item_sku_id,v_slot_item_slot_width,v_slot_item_slotitem_id,
                  v_slot_item_ign_for_reslot FROM slot_item WHERE slot_id = v_slot_slot_id;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
               end;

                    --SQLWAYS_EVAL# is empty
               IF (v_slot_item_sku_id IS NULL) then
                     --4
                        --SQLWAYS_EVAL# already remembered.
                  IF (v_empty_slot_id = 0) then
                         --5
                     IF (v_slot_item_ign_for_reslot = 0 and v_slot_lock = 0) then
                             --6
                                --SQLWAYS_EVAL# slot
                        v_empty_slot_id := v_slot_slot_id;
                        v_empty_slot_slotitem_id := v_slot_item_slotitem_id;
                     end if;
                  end if; --4

               ELSE --4
                        --SQLWAYS_EVAL# we've previously encountered an empty slot.
                  IF (v_empty_slot_id <> 0) then
                         --5
                            --SQLWAYS_EVAL# to empty found earlier.
                     begin
                        select   slotitem_id INTO v_slot_item_slotitem_id FROM slot_item WHERE slot_id = v_slot_slot_id;
                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           NULL;
                     end;
                     UPDATE slot_item
                     SET slot_id = v_empty_slot_id
                     WHERE slotitem_id = v_slot_item_slotitem_id;
                     UPDATE slot_item
                     SET slot_id = v_slot_slot_id
                     WHERE slotitem_id = v_empty_slot_slotitem_id;
                            --SQLWAYS_EVAL# to empty found earlier.

                            --SQLWAYS_EVAL# starting of the shelf hence re-initialize @record_count
                     v_record_count := v_shelf_count;
                     FETCH present_slot
                     INTO v_slot_slot_id,v_slot_area,v_slot_zone,v_slot_aisle,v_slot_bay,v_slot_lvl,
                     v_slot_rack_level_id,v_slot_rack_type,v_slot_width,v_slot_lock;
                     EXIT;
                  end if;
               end if;
            end if; --SQLWAYS_EVAL# = 1)]

            FETCH present_slot INTO v_slot_slot_id,v_slot_area,v_slot_zone,v_slot_aisle,v_slot_bay,v_slot_lvl,
            v_slot_rack_level_id,v_slot_rack_type,v_slot_width,v_slot_lock;
            v_record_count := v_record_count+1;
         END LOOP; --SQLWAYS_EVAL# each shelf]
         v_shelf_count := v_record_count;
      END LOOP; --SQLWAYS_EVAL# all slots in the slot table]
      CLOSE present_slot;


--SQLWAYS_EVAL# WITH (UPDLOCK ROWLOCK)
--SQLWAYS_EVAL# = 0.0
--SQLWAYS_EVAL# slot
--SQLWAYS_EVAL# = slot.slot_id
--SQLWAYS_EVAL# = @range_cur_id


      FETCH range_cursor INTO v_range_cur_id;
   END LOOP;  --SQLWAYS_EVAL#

   CLOSE range_cursor;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE7 ';


END;
--SQLWAYS_EVAL# Movement

/
--------------------------------------------------------
--  DDL for Procedure SP_PICK_LINE_ATTRIBUTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_PICK_LINE_ATTRIBUTE (v_range_group_id IN NUMBER,
    v_item_attrubute IN VARCHAR2,
    v_whse_code IN VARCHAR2, v_refcur OUT SYS_REFCURSOR)
   as
   v_actualPercent  NUMBER(15,2);
   v_no_of_items  NUMBER(10,0);
   v_system  NUMBER(10,0);
   v_metric_unit  NUMBER(10,0);
   v_metric_vol  NUMBER(10,0);

   v_cube_conversion  NUMBER(13,4);
   v_exp  NUMBER(13,4);
   v_base  NUMBER(13,4);
BEGIN
   v_actualPercent := 0.0;


   if (v_item_attrubute = 'SQLWAYS_EVAL# ent') then

      sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE8');
      select   count(*) INTO v_no_of_items from tt_RANGE_IDS_TABLE8;
      v_actualPercent := 0.0;
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then
               0
            else -1
            end
         else -1
         end) INTO v_actualPercent from slot_item
         inner join slot  on slot.slot_id = slot_item.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE8);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      if(v_actualPercent <> -1*v_no_of_items) then
         begin
            select   sum(case
            when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
               case
               when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))
               else 0.0
               end
            when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
               case
               when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
                  get_case_movement(est_movement,case_mov,use_estimated_hist)
               else 0.0
               end
            when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
               case
               when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))
               else 0.0
               end
            when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
               case
               when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))
               else 0.0
               end
            else 0.0
            end) INTO v_actualPercent from slot_item
            inner join slot  on slot.slot_id = slot_item.slot_id
            left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
            and slot.my_range in(select * from tt_RANGE_IDS_TABLE8);
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
      end if;
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE8 ';
   end if;

   if (v_item_attrubute = 'Hits') then

      sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE8');
      v_actualPercent := 0.0;
      begin
         select   sum(case
         when est_hits > 0.0 then est_hits
         when calc_hits > 0.0 then calc_hits
         else 0.0
         end) INTO v_actualPercent from slot_item
         inner join slot on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE8);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE8 ';
   end if;

   if (v_item_attrubute = 'Slotting weight') then

      sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE8');
      v_actualPercent := 0.0;
      begin
         select   sum(case
         when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
            when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
            when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
            else 0.0
            end
         when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.case_wt > 0 then
               so_item_master.case_wt
            else 0.0
            end
         when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.inn_wt > 0 then
               so_item_master.inn_wt
            else 0.0
            end
         when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.each_wt > 0.0 then
               so_item_master.each_wt
            else 0.0
            end
         when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# nit*/
            case slot_item.bin_unit
            when 2 then /*SQLWAYS_EVAL# se*/
                          (((100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.case_wt)+bins.weight
            when 4 then /*SQLWAYS_EVAL# ner*/
                        (((100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.inn_wt)+bins.weight
            when 8 then /*SQLWAYS_EVAL# ch*/
                        (((100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.each_wt)+bins.weight
            else 0.0
            end
         else 0.0
         end) INTO v_actualPercent from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join pallets on pallets.pallet_id = slot_item.current_pallet
         left outer join bins on bins.bin_id = slot_item.current_bin where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE8);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE8 ';
   end if;

   if (v_item_attrubute = 'Shipping weight') then

      sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE8');
      v_actualPercent := 0.0;
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
            when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
            when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
            else 0.0
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.case_wt > 0 then
               so_item_master.case_wt
            else 0.0
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when so_item_master.inn_wt > 0 then
               so_item_master.inn_wt
            else 0.0
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when so_item_master.each_wt > 0.0 then
               so_item_master.each_wt
            else 0.0
            end
         else 0.0
         end) INTO v_actualPercent from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE8);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE8 ';
   end if;

   if (v_item_attrubute = 'Cube movement') then


      sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE8');
      begin
         select   whprefs_system, metric_unit, metric_vol INTO v_system,v_metric_unit,v_metric_vol from whprefs where whse_code = v_whse_code;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;

    --SQLWAYS_EVAL# of unit is english
      IF v_system = 1 then

         v_cube_conversion := POWER(12,3);
      ELSE
         v_exp := v_metric_vol -v_metric_unit;
         v_base := POWER(10,v_exp);
         v_cube_conversion := POWER(v_base,3);
      end if;
      begin
         select   sum(case
         when v_system = 1 then /*Case Cube unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
            else 0.0
            end
         when v_system = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
               case
               when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
               else 0.0
               end
            when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
               case
               when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
               else 0.0
               end
            when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
               case
               when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*((so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/v_cube_conversion)
               else 0.0
               end
            when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
               case
               when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))*((so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/v_cube_conversion)
               else 0.0
               end
            else 0.0
            end
         else 0.0
         end) INTO v_actualPercent from slot_item
         inner join slot  on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE8);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE8 ';
   end if;

   if (v_item_attrubute = 'Case movement') then

      sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE8');
      v_actualPercent := 0.0;
      begin
         select   sum(case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
            get_case_movement(est_movement,case_mov,use_estimated_hist)
         else 0.0
         end) INTO v_actualPercent from slot_item
         inner join slot  on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from  tt_RANGE_IDS_TABLE8);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE8 ';
   else
      if (v_item_attrubute = 'SQLWAYS_EVAL# ent') then

         sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE8');
         v_actualPercent := 0.0;
         begin
            select   sum(case
            when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
               case
               when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                  get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)
               else 0.0
               end
            when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
               case
               when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                  get_case_movement(est_movement,case_mov,use_estimated_hist)
               else 0.0
               end
            when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# movement*/
               case
               when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/

                  get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)
               else 0.0
               end
            when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# movement*/
               case
               when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                  get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist)
               else 0.0
               end
            when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# movement .. handle differently.*/
               case
               when slot_item.est_movement > 0.0 then /*SQLWAYS_EVAL# movement*/
                  case slot_item.bin_unit
                  when 2 then /*SQLWAYS_EVAL# se*/
                                  (100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
                  when 4 then /*SQLWAYS_EVAL# ner*/
                                (100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
                  when 8 then /*SQLWAYS_EVAL# ch*/
                                (100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
                  else 0.0
                  end
               when slot_item.bin_mov > 0.0 then /*SQLWAYS_EVAL# ement*/
                  slot_item.bin_mov
               else 0.0
               end
            else 0.0
            end) INTO v_actualPercent from slot_item
            inner join slot on slot_item.slot_id = slot.slot_id
            left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
            left outer join bins on slot_item.current_bin = bins.bin_id where slot_item.slot_id is not null and slot_item.sku_id is not null
            and slot.my_range in(select * from tt_RANGE_IDS_TABLE8);
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
         EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE8 ';
      end if;
   end if;

   if (v_item_attrubute = 'SQLWAYS_EVAL# * Shipping length') then

      sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE8');
      v_actualPercent := 0.0;
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))*pallets.length
            else 0.0
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
               get_case_movement(est_movement,case_mov,use_estimated_hist)*so_item_master.case_len
            else 0.0
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*so_item_master.inn_len
            else 0.0
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)*so_item_master.each_per_cs)*so_item_master.each_len
            else 0.0
            end
         else 0.0
         end) INTO v_actualPercent from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE8);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE8 ';
   end if;

   if(v_actualPercent > 0) then

      open v_refcur for select v_actualPercent  from dual;
   else
      v_actualPercent := 0.0;
      open v_refcur for select v_actualPercent  from dual;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_PICK_LINE_CALCULATION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_PICK_LINE_CALCULATION (v_pickline_group_id IN NUMBER,
    v_range_group_id IN NUMBER,
    v_item_attrubute IN VARCHAR2,
    v_whse_code IN NVARCHAR2, v_refcur OUT SYS_REFCURSOR)
   as
   v_actualPercent  NUMBER(15,2);
   v_totalRcord  NUMBER(15,2);
   v_Total_percent  NUMBER(15,2);


   CURSOR abc IS SELECT movment
   FROM MOVEMENT;
   v_movement  NUMBER(15,2);
   v_system  NUMBER(10,0);
   v_metric_unit  NUMBER(10,0);
   v_metric_vol  NUMBER(10,0);

   v_cube_conversion  NUMBER(13,4);
   v_exp  NUMBER(13,4);
   v_base  NUMBER(13,4);
BEGIN
   v_actualPercent := 0.0;



   if (v_item_attrubute = 'SQLWAYS_EVAL# ent') then


      sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE9');
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))
            else 0.0
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
               get_case_movement(est_movement,case_mov,use_estimated_hist)
            else 0.0
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)*so_item_master.inn_per_cs)
            else 0.0
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist)*so_item_master.each_per_cs)
            else 0.0
            end
         else 0.0
         end) INTO v_actualPercent from slot_item
         inner join slot  on slot.slot_id = slot_item.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE9);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
      EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE9 ';
   else
      if (v_item_attrubute = 'Hits') then

         sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE9');
         v_actualPercent := 0.0;
         begin
            select   sum(case
            when est_hits > 0.0 then est_hits
            when calc_hits > 0.0 then calc_hits
            else 0.0
            end) INTO v_actualPercent from slot_item
            inner join slot on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
            and slot.my_range in(select * from tt_RANGE_IDS_TABLE9);
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         end;
         EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE9 ';
      else
         if (v_item_attrubute = 'Slotting weight') then

            sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE9');
            v_actualPercent := 0.0;
            begin
               select   sum(case
               when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
                  case
                  when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
                  when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
                  when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
                  else 0.0
                  end
               when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
                  case
                  when so_item_master.case_wt > 0 then
                     so_item_master.case_wt
                  else 0.0
                  end
               when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# unit*/
                  case
                  when so_item_master.inn_wt > 0 then
                     so_item_master.inn_wt
                  else 0.0
                  end
               when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# unit*/
                  case
                  when so_item_master.each_wt > 0.0 then
                     so_item_master.each_wt
                  else 0.0
                  end
               when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# nit*/
                  case slot_item.bin_unit
                  when 2 then /*SQLWAYS_EVAL# se*/
                          (((100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.case_wt)+bins.weight
                  when 4 then /*SQLWAYS_EVAL# ner*/
                        (((100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.inn_wt)+bins.weight
                  when 8 then /*SQLWAYS_EVAL# ch*/
                        (((100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.each_wt)+bins.weight
                  else 0.0
                  end
               else 0.0
               end) INTO v_actualPercent from slot_item
               inner join slot on slot_item.slot_id = slot.slot_id
               left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
               left outer join pallets on pallets.pallet_id = slot_item.current_pallet
               left outer join bins on bins.bin_id = slot_item.current_bin where slot_item.slot_id is not null and slot_item.sku_id is not null
               and slot.my_range in(select * from tt_RANGE_IDS_TABLE9);
               EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            end;
            EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE9 ';
         else
            if (v_item_attrubute = 'Shipping weight') then

               sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE9');
               v_actualPercent := 0.0;
               begin
                  select   sum(case
                  when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
                     case
                     when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
                     when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
                     when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
                     else 0.0
                     end
                  when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
                     case
                     when so_item_master.case_wt > 0 then
                        so_item_master.case_wt
                     else 0.0
                     end
                  when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
                     case
                     when so_item_master.inn_wt > 0 then
                        so_item_master.inn_wt
                     else 0.0
                     end
                  when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
                     case
                     when so_item_master.each_wt > 0.0 then
                        so_item_master.each_wt
                     else 0.0
                     end
                  else 0.0
                  end) INTO v_actualPercent from slot_item
                  inner join slot on slot_item.slot_id = slot.slot_id
                  left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
                  left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
                  and slot.my_range in(select * from tt_RANGE_IDS_TABLE9);
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     NULL;
               end;
               EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE9 ';
            else
               if (v_item_attrubute = 'Cube movement') then


                  sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE9');
                  begin
                     select   whprefs_system, metric_unit, metric_vol INTO v_system,v_metric_unit,v_metric_vol from whprefs where whse_code = v_whse_code;
                     EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  end;

    --SQLWAYS_EVAL# of unit is english
                  IF v_system = 1 then

                     v_cube_conversion := POWER(12,3);
                  ELSE
                     v_exp := v_metric_vol -v_metric_unit;
                     v_base := POWER(10,v_exp);
                     v_cube_conversion := POWER(v_base,3);
                  end if;
                  begin
                     select   sum(case
                     when v_system = 1 then /*Case Cube unit*/
                        case
                        when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
                        else 0.0
                        end
                     when v_system = 2 then /*SQLWAYS_EVAL# unit*/
                        case
                        when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
                           case
                           when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
                           else 0.0
                           end
                        when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
                           case
                           when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then(get_case_movement(est_movement,case_mov,use_estimated_hist)*((so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/v_cube_conversion))
                           else 0.0
                           end
                        when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
                           case
                           when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*((so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/v_cube_conversion)
                           else 0.0
                           end
                        when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
                           case
                           when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))*((so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/v_cube_conversion)
                           else 0.0
                           end
                        else 0.0
                        end
                     else 0.0
                     end) INTO v_actualPercent from slot_item
                     inner join slot  on slot_item.slot_id = slot.slot_id
                     left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
                     and slot.my_range in(select * from tt_RANGE_IDS_TABLE9);
                     EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  end;
                  EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE9 ';
               else
                  if (v_item_attrubute = 'Case movement') then

                     sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE9');
                     v_actualPercent := 0.0;
                     begin
                        select   sum(case
                        when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
                           get_case_movement(est_movement,case_mov,use_estimated_hist)
                        else 0.0
                        end) INTO v_actualPercent from slot_item
                        inner join slot  on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
                        and slot.my_range in(select * from  tt_RANGE_IDS_TABLE9);
                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           NULL;
                     end;
                     EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE9 ';
                  else
                     if (v_item_attrubute = 'SQLWAYS_EVAL# ent') then

                        sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE9');
                        v_actualPercent := 0.0;
                        begin
                           select   sum(case
                           when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
                              case
                              when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                                 get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)
                              else 0.0
                              end
                           when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
                              case
                              when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                                 get_case_movement(est_movement,case_mov,use_estimated_hist)
                              else 0.0
                              end
                           when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# movement*/
                              case
                              when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/

                                 get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)
                              else 0.0
                              end
                           when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# movement*/
                              case
                              when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
                                 get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist)
                              else 0.0
                              end
                           when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# movement .. handle differently.*/
                              case
                              when slot_item.est_movement > 0.0 then /*SQLWAYS_EVAL# movement*/
                                 case slot_item.bin_unit
                                 when 2 then /*SQLWAYS_EVAL# se*/
                                  (100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
                                 when 4 then /*SQLWAYS_EVAL# ner*/
                                (100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
                                 when 8 then /*SQLWAYS_EVAL# ch*/
                                (100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
                                 else 0.0
                                 end
                              when slot_item.bin_mov > 0.0 then /*SQLWAYS_EVAL# ement*/
                                 slot_item.bin_mov
                              else 0.0
                              end
                           else 0.0
                           end) INTO v_actualPercent from slot_item
                           inner join slot on slot_item.slot_id = slot.slot_id
                           left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
                           left outer join bins on slot_item.current_bin = bins.bin_id where slot_item.slot_id is not null and slot_item.sku_id is not null
                           and slot.my_range in(select * from tt_RANGE_IDS_TABLE9);
                           EXCEPTION
                           WHEN NO_DATA_FOUND THEN
                              NULL;
                        end;
                        EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE9 ';
                     else
                        if (v_item_attrubute = 'SQLWAYS_EVAL# * Shipping length') then

                           sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE9');
                           v_actualPercent := 0.0;
                           begin
                              select   sum(case
                              when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
                                 case
                                 when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))*pallets.length
                                 else 0.0
                                 end
                              when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
                                 case
                                 when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then
                                    get_case_movement(est_movement,case_mov,use_estimated_hist)*so_item_master.case_len
                                 else 0.0
                                 end
                              when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
                                 case
                                 when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))*so_item_master.inn_len
                                 else 0.0
                                 end
                              when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
                                 case
                                 when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)*so_item_master.each_per_cs)*so_item_master.each_len
                                 else 0.0
                                 end
                              else 0.0
                              end) INTO v_actualPercent from slot_item
                              inner join slot on slot_item.slot_id = slot.slot_id
                              left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
                              left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
                              and slot.my_range in(select * from tt_RANGE_IDS_TABLE9);
                              EXCEPTION
                              WHEN NO_DATA_FOUND THEN
                                 NULL;
                           end;
                           EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE9 ';
                        end if;
                     end if;
                  end if;
               end if;
            end if;
         end if;
      end if;
   end if;



   v_Total_percent := 0.0;
   sp_all_pick_line_calculation(v_pickline_group_id,v_item_attrubute,v_whse_code,v_Total_percent);






   OPEN abc;

   FETCH abc into v_Total_percent;
   WHILE abc%FOUND  LOOP
      FETCH abc into v_Total_percent;
   END LOOP;

   CLOSE abc;



   if (v_actualPercent > 0 and v_Total_percent > 0) then

      open v_refcur for SELECT(v_actualPercent/v_Total_percent)  from dual;
   else
      v_actualPercent := 0.0;
      open v_refcur for select v_actualPercent  from dual;
   end if;



   EXECUTE IMMEDIATE ' TRUNCATE TABLE MOVEMENT ';
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_PLB_HITS_SUM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_PLB_HITS_SUM (v_range_group_id IN NUMBER, v_refcur OUT SYS_REFCURSOR)
   as
   v_movement  NUMBER(15,2);
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE10');
   select   count(*) INTO v_no_of_items from tt_RANGE_IDS_TABLE10;
   v_movement := 0.0;

   begin
      select   sum(case
      when get_forecast_hits(est_hits,calc_hits,use_estimated_hist) > 0.0 then(get_forecast_hits(est_hits,calc_hits,use_estimated_hist))
      else 0.0
      end) INTO v_movement from slot_item
      inner join slot on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from tt_RANGE_IDS_TABLE10);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when get_forecast_hits(est_hits,calc_hits,use_estimated_hist) > 0.0 then(get_forecast_hits(est_hits,calc_hits,use_estimated_hist))
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE10);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;
   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE10 ';

   if (v_movement >= 0) then

      open v_refcur for select v_movement  from dual;
   else
      v_movement := -0.1;
      open v_refcur for select v_movement  from dual;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_PLB_HITS_SUM2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_PLB_HITS_SUM2 (v_range_group_id IN NUMBER
	, v_movement IN out NUMBER) as
   v_no_of_items  NUMBER(10,0);
begin


   sp_get_range_ids(v_range_group_id,'RANGE_IDS_TABLE11');
   select   count(*) INTO v_no_of_items from RANGE_IDS_TABLE11;
   v_movement := 0.0;

   begin
      select   sum(case
      when get_forecast_hits(est_hits,calc_hits,use_estimated_hist) > 0.0 then(get_forecast_hits(est_hits,calc_hits,use_estimated_hist))
      else 0.0
      end) INTO v_movement from slot_item
      inner join slot on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from RANGE_IDS_TABLE11);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when get_forecast_hits(est_hits,calc_hits,use_estimated_hist) > 0.0 then(get_forecast_hits(est_hits,calc_hits,use_estimated_hist))
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot.slot_id = slot_item.slot_id where slot_item.slot_id  is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from RANGE_IDS_TABLE11);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;
   EXECUTE IMMEDIATE ' TRUNCATE TABLE RANGE_IDS_TABLE11 ';

   if (v_movement < 0) then

      v_movement := -0.1;
   end if;
end;

/
--------------------------------------------------------
--  DDL for Procedure SP_POPULATEIMPORTTEMP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_POPULATEIMPORTTEMP (v_import_id    NUMBER,
                                                   v_format_id    NUMBER,
                                                   v_whse_code    VARCHAR2)
AS
   v_SQLString          VARCHAR2 (32000);
   v_col_name           VARCHAR2 (32000);
   v_stmnt              VARCHAR2 (32000);
   v_col_part           VARCHAR2 (32000);
   v_value_part         VARCHAR2 (32000);
   v_row_cnt            NUMBER;
   v_row_cntS           NUMBER;
   v_offset_fieldname   VARCHAR2 (32000);
   v_start_posn         NUMBER (10);
   v_length             NUMBER (10);

   CURSOR TmpColInfo_cursor
   IS (SELECT DISTINCT
              (CASE WHEN iom.custom = 0 THEN (CASE (iom.offset_fieldname)
                  WHEN 'SKU' THEN 'sku_name'
                  WHEN 'SKU Description' THEN 'sku_desc'
                  WHEN 'Slot Number' THEN 'dsp_slot'
                  WHEN 'Slot Unit' THEN 'slot_unit'
                  WHEN 'Ship Unit' THEN 'ship_unit'
                  WHEN 'Current Lanes' THEN 'rec_lanes'
                  WHEN 'Current Stacking' THEN 'rec_stacking'
                  WHEN 'Optimize Plt Pattern' THEN 'opt_pallet_pattern'
                  WHEN 'Allow 1 HI Residual' THEN 'hi_residual'
                  WHEN 'Orientation' THEN 'current_orientation'
                  WHEN 'Ignore for Reslot' THEN 'ign_for_reslot'
                  WHEN 'SI Char 1' THEN 'info1'
                  WHEN 'SI Char 2' THEN 'info2'
                  WHEN 'SI Char 3' THEN 'info3'
                  WHEN 'SI Char 4' THEN 'info4'
                  WHEN 'SI Char 5' THEN 'info5'
                  WHEN 'SI Char 6' THEN 'info6'
                  WHEN 'Movement' THEN 'movement'
                  WHEN 'Hits' THEN 'nbr_of_picks'
                  WHEN 'Inventory' THEN 'inventory'
                  WHEN 'Estimated Movement' THEN 'est_movement'
                  WHEN 'Estimated Hits' THEN 'est_hits'
                  WHEN 'Estimated Inventory' THEN 'est_inventory'
                  WHEN 'History Match' THEN 'hist_match'
                  WHEN 'UPC' THEN 'upc'
                  WHEN 'Commodity Class' THEN 'commodity_class'
                  WHEN 'Crushability' THEN 'crushability_code'
                  WHEN 'Haz Code' THEN 'hazard_code'
                  WHEN 'Vendor Code' THEN 'vendor_code'
                  WHEN 'Misc 1' THEN 'misc1'
                  WHEN 'Misc 2' THEN 'misc2'
                  WHEN 'Misc 3' THEN 'misc3'
                  WHEN 'Misc 4' THEN 'misc4'
                  WHEN 'Misc 5' THEN 'misc5'
                  WHEN 'Misc 6' THEN 'misc6'
                  WHEN 'Item status' THEN 'item_status'
                  WHEN 'Case height' THEN 'case_ht'
                  WHEN 'Case length' THEN 'case_len'
                  WHEN 'Case width' THEN 'case_wid'
                  WHEN 'Case weight' THEN 'case_wt'
                  WHEN 'Each height' THEN 'each_ht'
                  WHEN 'Each length' THEN 'each_len'
                  WHEN 'Each width' THEN 'each_wid'
                  WHEN 'Each weight' THEN 'each_wt'
                  WHEN 'Inner height' THEN 'inn_ht'
                  WHEN 'Inner length' THEN 'inn_len'
                  WHEN 'Inner width' THEN 'inn_wid'
                  WHEN 'Inner weight' THEN 'inn_wt'
                  WHEN 'Warehouse TI' THEN 'wh_ti'
                  WHEN 'Warehouse HI' THEN 'wh_hi'
                  WHEN 'Vendor TI' THEN 'ven_ti'
                  WHEN 'Vendor HI' THEN 'ven_hi'
                  WHEN 'Order TI' THEN 'ord_ti'
                  WHEN 'Order HI' THEN 'ord_hi'
                  WHEN 'Exp rec dt' THEN 'ex_recpt_date'
                  WHEN 'Conveyability' THEN 'conveyable'
                  WHEN 'Max stacking' THEN 'max_stacking'
                  WHEN 'Max lanes' THEN 'max_lanes'
                  WHEN 'Alw ship units' THEN 'allow_su'
                  WHEN 'Alw slot units' THEN 'allow_slu'
                  WHEN 'Units per bin' THEN 'num_units_per_bin'
                  WHEN 'Inner pack' THEN 'eaches_per_inner'
                  WHEN 'Vendor pack' THEN 'eaches_per_case'
                  WHEN 'Rotate eaches' THEN 'allow_rotate_each'
                  WHEN 'Rotate inners' THEN 'allow_rotate_inner'
                  WHEN 'Rotate bins' THEN 'allow_rotate_bin'
                  WHEN 'Rotate cases' THEN 'allow_rotate_case'
                  WHEN 'Use 3D slotting' THEN 'use_3d_slot'
                  WHEN 'Nest eaches' THEN 'allow_nest_each'
                  WHEN 'Incremental height' THEN 'incremental_height'
                  WHEN 'Incremental length' THEN 'incremental_length'
                  WHEN 'Incremental width' THEN 'incremental_width'
                  WHEN '1' THEN 'custom_1'
                  WHEN '2' THEN 'custom_2'
                  WHEN '3' THEN 'custom_3'
                  WHEN '4' THEN 'custom_4'
                  WHEN '5' THEN 'custom_5'
                  WHEN '6' THEN 'custom_6'
                  WHEN '7' THEN 'custom_7'
                  WHEN '8' THEN 'custom_8'
                  WHEN '9' THEN 'custom_9'
                  WHEN '10' THEN 'custom_10'
                  WHEN '11' THEN 'custom_11'
                  WHEN '12' THEN 'custom_12'
                  WHEN '13' THEN 'custom_13'
                  WHEN '14' THEN 'custom_14'
                  WHEN '15' THEN 'custom_15'
                  WHEN '16' THEN 'custom_16'
                  WHEN '17' THEN 'custom_17'
                  WHEN '18' THEN 'custom_18'
                  WHEN '19' THEN 'custom_19'
                  WHEN '20' THEN 'custom_20'
                  WHEN '21' THEN 'custom_21'
                  WHEN '22' THEN 'custom_22'
                  WHEN '23' THEN 'custom_23'
                  WHEN '24' THEN 'custom_24'
                  WHEN '25' THEN 'custom_25'
                  WHEN 'Qty per grab (each)' THEN 'qty_per_grab_each'
                  WHEN 'Qty per grab (inner)' THEN 'qty_per_grab_inner'
                  WHEN 'Qty per grab (case)' THEN 'qty_per_grab_case'
                  WHEN 'Handling attr(each)' THEN 'handling_attrib_each'
                  WHEN 'Handling attr(inner)' THEN 'handling_attrib_inner'
                  WHEN 'Handling attr(case)' THEN 'handling_attrib_case'
                  WHEN 'Item Num 1' THEN 'item_num_1'
                  WHEN 'Item Num 2' THEN 'item_num_2'
                  WHEN 'Item Num 3' THEN 'item_num_3'
                  WHEN 'Item Num 4' THEN 'item_num_4'
                  WHEN 'Item Num 5' THEN 'item_num_5'
                  WHEN 'Item Char 1' THEN 'item_char_1'
                  WHEN 'Item Char 2' THEN 'item_char_2'
                  WHEN 'Item Char 3' THEN 'item_char_3'
                  WHEN 'Item Char 4' THEN 'item_char_4'
                  WHEN 'Item Char 5' THEN 'item_char_5'
                  WHEN 'SI Num 1' THEN 'si_num_1'
                  WHEN 'SI Num 2' THEN 'si_num_2'
                  WHEN 'SI Num 3' THEN 'si_num_3'
                  WHEN 'SI Num 4' THEN 'si_num_4'
                  WHEN 'SI Num 5' THEN 'si_num_5'
                  WHEN 'SI Num 6' THEN 'si_num_6'
                  WHEN 'Action code' THEN 'action_code'
                  WHEN 'Update type' THEN 'update_type'
                  WHEN 'Previous slot' THEN 'previous_slot'
                  WHEN 'Max num of slots' THEN 'max_slots'
                  WHEN 'Multiple location grp' THEN 'mult_loc_grp'
                  WHEN 'Max pallet stacking' THEN 'max_pallet_stacking'
                  WHEN 'A-Frame slot allowed' THEN 'aframe_allow'
                  WHEN 'A-frame height' THEN 'aframe_ht'
                  WHEN 'A-frame length' THEN 'aframe_len'
                  WHEN 'A-frame width' THEN 'aframe_wid'
                  WHEN 'A-frame weight' THEN 'aframe_wt'
                  WHEN 'Replenishment group' THEN 'replen_group'
               END) 
               ELSE
               (CASE (iom.custom_offset_field)
                             WHEN 'SKU' THEN 'sku_name'
                             WHEN 'SKU Description' THEN 'sku_desc'
                             WHEN 'Slot Number' THEN 'dsp_slot'
                             WHEN 'Slot Unit' THEN 'slot_unit'
                             WHEN 'Ship Unit' THEN 'ship_unit'
                             WHEN 'Current Lanes' THEN 'rec_lanes'
                             WHEN 'Current Stacking' THEN 'rec_stacking'
                             WHEN 'Optimize Plt Pattern' THEN 'opt_pallet_pattern'
                             WHEN 'Allow 1 HI Residual' THEN 'hi_residual'
                             WHEN 'Orientation' THEN 'current_orientation'
                             WHEN 'Ignore for Reslot' THEN 'ign_for_reslot'
                             WHEN 'SI Char 1' THEN 'info1'
                             WHEN 'SI Char 2' THEN 'info2'
                             WHEN 'SI Char 3' THEN 'info3'
                             WHEN 'SI Char 4' THEN 'info4'
                             WHEN 'SI Char 5' THEN 'info5'
                             WHEN 'SI Char 6' THEN 'info6'
                             WHEN 'Movement' THEN 'movement'
                             WHEN 'Hits' THEN 'nbr_of_picks'
                             WHEN 'Inventory' THEN 'inventory'
                             WHEN 'Estimated Movement' THEN 'est_movement'
                             WHEN 'Estimated Hits' THEN 'est_hits'
                             WHEN 'Estimated Inventory' THEN 'est_inventory'
                             WHEN 'History Match' THEN 'hist_match'
                             WHEN 'UPC' THEN 'upc'
                             WHEN 'Commodity Class' THEN 'commodity_class'
                             WHEN 'Crushability' THEN 'crushability_code'
                             WHEN 'Haz Code' THEN 'hazard_code'
                             WHEN 'Vendor Code' THEN 'vendor_code'
                             WHEN 'Misc 1' THEN 'misc1'
                             WHEN 'Misc 2' THEN 'misc2'
                             WHEN 'Misc 3' THEN 'misc3'
                             WHEN 'Misc 4' THEN 'misc4'
                             WHEN 'Misc 5' THEN 'misc5'
                             WHEN 'Misc 6' THEN 'misc6'
                             WHEN 'Item status' THEN 'item_status'
                             WHEN 'Case height' THEN 'case_ht'
                             WHEN 'Case length' THEN 'case_len'
                             WHEN 'Case width' THEN 'case_wid'
                             WHEN 'Case weight' THEN 'case_wt'
                             WHEN 'Each height' THEN 'each_ht'
                             WHEN 'Each length' THEN 'each_len'
                             WHEN 'Each width' THEN 'each_wid'
                             WHEN 'Each weight' THEN 'each_wt'
                             WHEN 'Inner height' THEN 'inn_ht'
                             WHEN 'Inner length' THEN 'inn_len'
                             WHEN 'Inner width' THEN 'inn_wid'
                             WHEN 'Inner weight' THEN 'inn_wt'
                             WHEN 'Warehouse TI' THEN 'wh_ti'
                             WHEN 'Warehouse HI' THEN 'wh_hi'
                             WHEN 'Vendor TI' THEN 'ven_ti'
                             WHEN 'Vendor HI' THEN 'ven_hi'
                             WHEN 'Order TI' THEN 'ord_ti'
                             WHEN 'Order HI' THEN 'ord_hi'
                             WHEN 'Exp rec dt' THEN 'ex_recpt_date'
                             WHEN 'Conveyability' THEN 'conveyable'
                             WHEN 'Max stacking' THEN 'max_stacking'
                             WHEN 'Max lanes' THEN 'max_lanes'
                             WHEN 'Alw ship units' THEN 'allow_su'
                             WHEN 'Alw slot units' THEN 'allow_slu'
                             WHEN 'Units per bin' THEN 'num_units_per_bin'
                             WHEN 'Inner pack' THEN 'eaches_per_inner'
                             WHEN 'Vendor pack' THEN 'eaches_per_case'
                             WHEN 'Rotate eaches' THEN 'allow_rotate_each'
                             WHEN 'Rotate inners' THEN 'allow_rotate_inner'
                             WHEN 'Rotate bins' THEN 'allow_rotate_bin'
                             WHEN 'Rotate cases' THEN 'allow_rotate_case'
                             WHEN 'Use 3D slotting' THEN 'use_3d_slot'
                             WHEN 'Nest eaches' THEN 'allow_nest_each'
                             WHEN 'Incremental height' THEN 'incremental_height'
                             WHEN 'Incremental length' THEN 'incremental_length'
                             WHEN 'Incremental width' THEN 'incremental_width'
                             WHEN '1' THEN 'custom_1'
                             WHEN '2' THEN 'custom_2'
                             WHEN '3' THEN 'custom_3'
                             WHEN '4' THEN 'custom_4'
                             WHEN '5' THEN 'custom_5'
                             WHEN '6' THEN 'custom_6'
                             WHEN '7' THEN 'custom_7'
                             WHEN '8' THEN 'custom_8'
                             WHEN '9' THEN 'custom_9'
                             WHEN '10' THEN 'custom_10'
                             WHEN '11' THEN 'custom_11'
                             WHEN '12' THEN 'custom_12'
                             WHEN '13' THEN 'custom_13'
                             WHEN '14' THEN 'custom_14'
                             WHEN '15' THEN 'custom_15'
                             WHEN '16' THEN 'custom_16'
                             WHEN '17' THEN 'custom_17'
                             WHEN '18' THEN 'custom_18'
                             WHEN '19' THEN 'custom_19'
                             WHEN '20' THEN 'custom_20'
                             WHEN '21' THEN 'custom_21'
                             WHEN '22' THEN 'custom_22'
                             WHEN '23' THEN 'custom_23'
                             WHEN '24' THEN 'custom_24'
                             WHEN '25' THEN 'custom_25'
                             WHEN 'Qty per grab (each)' THEN 'qty_per_grab_each'
                             WHEN 'Qty per grab (inner)' THEN 'qty_per_grab_inner'
                             WHEN 'Qty per grab (case)' THEN 'qty_per_grab_case'
                             WHEN 'Handling attr(each)' THEN 'handling_attrib_each'
                             WHEN 'Handling attr(inner)' THEN 'handling_attrib_inner'
                             WHEN 'Handling attr(case)' THEN 'handling_attrib_case'
                             WHEN 'Item Num 1' THEN 'item_num_1'
                             WHEN 'Item Num 2' THEN 'item_num_2'
                             WHEN 'Item Num 3' THEN 'item_num_3'
                             WHEN 'Item Num 4' THEN 'item_num_4'
                             WHEN 'Item Num 5' THEN 'item_num_5'
                             WHEN 'Item Char 1' THEN 'item_char_1'
                             WHEN 'Item Char 2' THEN 'item_char_2'
                             WHEN 'Item Char 3' THEN 'item_char_3'
                             WHEN 'Item Char 4' THEN 'item_char_4'
                             WHEN 'Item Char 5' THEN 'item_char_5'
                             WHEN 'SI Num 1' THEN 'si_num_1'
                             WHEN 'SI Num 2' THEN 'si_num_2'
                             WHEN 'SI Num 3' THEN 'si_num_3'
                             WHEN 'SI Num 4' THEN 'si_num_4'
                             WHEN 'SI Num 5' THEN 'si_num_5'
                             WHEN 'SI Num 6' THEN 'si_num_6'
                             WHEN 'Action code' THEN 'action_code'
                             WHEN 'Update type' THEN 'update_type'
                             WHEN 'Previous slot' THEN 'previous_slot'
                             WHEN 'Max num of slots' THEN 'max_slots'
                             WHEN 'Multiple location grp' THEN 'mult_loc_grp'
                             WHEN 'Max pallet stacking' THEN 'max_pallet_stacking'
                             WHEN 'A-Frame slot allowed' THEN 'aframe_allow'
                             WHEN 'A-frame height' THEN 'aframe_ht'
                             WHEN 'A-frame length' THEN 'aframe_len'
                             WHEN 'A-frame width' THEN 'aframe_wid'
                             WHEN 'A-frame weight' THEN 'aframe_wt'
                             WHEN 'Replenishment group' THEN 'replen_group'
               END)
               END)
                 AS offset_fieldname,
              ifo.start_posn,
              ifo.LENGTH
         FROM import_format_offsets ifo, import_offset_master iom
        WHERE     iom.import_id = v_import_id
              AND iom.offset_id = ifo.offset_id
              AND ifo.format_id = v_format_id
              AND iom.offset_type = 2);

BEGIN
   UPDATE staging
      SET whse_code = v_whse_code
    WHERE whse_code IS NULL;

   DELETE FROM staging
         WHERE whse_code = v_whse_code AND LTRIM (line) = '' OR line IS NULL;




v_col_part := 'INSERT INTO IMPORT_TEMP (whse_code,row_num';
v_value_part := ' SELECT ''' || v_whse_code || ''''||','||'import_temp_row_num.nextval ';
v_row_cnt := 0;

OPEN TmpColInfo_cursor;
LOOP
FETCH TmpColInfo_cursor INTO v_offset_fieldname,v_start_posn,v_length;

EXIT WHEN TmpColInfo_cursor%NOTFOUND;

IF (v_offset_fieldname IS NOT NULL) THEN

    v_col_name := ',' || v_offset_fieldname;
    v_stmnt := ',' || 'substr(line,'||v_start_posn||','||v_length||')';


    v_col_part := v_col_part || v_col_name;
    v_value_part := v_value_part || v_stmnt;
    v_row_cnt := v_row_cnt + 1;

END IF;     
    
END LOOP;
CLOSE TmpColInfo_cursor;

dbms_output.put_line(v_col_part);
dbms_output.put_line(v_value_part);

 v_col_part := v_col_part || ') ';
 v_value_part := v_value_part || ' FROM STAGING WHERE whse_code = ''' || v_whse_code || '''';

 v_SQLString := v_col_part || v_value_part;


DELETE FROM IMPORT_TEMP WHERE WHSE_CODE = v_whse_code;

EXECUTE IMMEDIATE v_SQLString;

update import_temp set ex_recpt_date = sysdate where ex_recpt_date = '01-01-1900';

DELETE FROM staging WHERE whse_code = v_whse_code;


COMMIT;


END;

/
--------------------------------------------------------
--  DDL for Procedure SP_POPULATESLOTSTAGING
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_POPULATESLOTSTAGING (v_import_id    NUMBER,
                                                   v_format_id    NUMBER,
                                                   v_whse_code    VARCHAR2,
						   v_area_start   NUMBER,
						   v_area_end     NUMBER,
						   v_zone_start   NUMBER,
						   v_zone_end     NUMBER,
						   v_aisle_start  NUMBER,
						   v_aisle_end    NUMBER,
						   v_bay_start    NUMBER,
						   v_bay_end      NUMBER,
						   v_lvl_start    NUMBER,
						   v_lvl_end      NUMBER,
						   v_posn_start   NUMBER,
						   v_posn_end     NUMBER)
AS
   v_SQLString          VARCHAR2 (32000);
   v_col_name           VARCHAR2 (32000);
   v_stmnt              VARCHAR2 (32000);
   v_col_part           VARCHAR2 (32000);
   v_value_part         VARCHAR2 (32000);
   v_row_cnt            NUMBER;
   v_row_cntS           NUMBER;
   v_offset_fieldname   VARCHAR2 (32000);
   v_start_posn         NUMBER (10);
   v_length             NUMBER (10);
   v_no_of_items  NUMBER(10);
   v_locked    slot_stg.locked%type;
   v_dft_rt_name         rack_type.rt_name%type;
   v_fomat_id stg_offset_xref.format_id%type;
   v_format_offset_id stg_offset_xref.format_offset_id%type;
   v_stg_col_name stg_offset_xref.stg_col_name%type;
   v_dft_value stg_offset_xref.dft_value%type;
   v_default_blank       import_format_prefs.default_blank%type;
   v_default_zero        import_format_prefs.default_zero%type;
   CURSOR TmpColInfo_cursor
   IS (SELECT DISTINCT
              (CASE (iom.offset_fieldname)
                  WHEN 'Slot Number' THEN 'dsp_slot'
                  WHEN 'Rack Type' THEN 'rt_name'
                  WHEN 'Height' THEN 'ht'
                  WHEN 'Width' THEN 'width'
                  WHEN 'Depth' THEN 'depth'
                  WHEN 'Clearance' THEN 'max_ht_clear'
                  WHEN 'Side Clearance' THEN 'max_side_clear'
                  WHEN 'Locked' THEN 'locked'
                  WHEN 'Lock Reason' THEN 'lock_reason'
                  WHEN 'Max Stacking' THEN 'max_stack'
                  WHEN 'Max Lanes' THEN 'max_lanes'
                  WHEN 'Expand Next' THEN 'allow_expand'
                  WHEN 'Label Location' THEN 'label_pos'
                  WHEN 'Max Slot Weight' THEN 'wt_limit'
                  WHEN 'Max Lane Weight' THEN 'max_lane_wt'
                  WHEN 'Slot type' THEN 'slot_type'
                  WHEN 'Travel zone' THEN 'travel_zone'
                  WHEN 'Travel aisle' THEN 'travel_aisle'
                  WHEN 'X-coordinate' THEN 'x_coord'
                  WHEN 'Y-coordinate' THEN 'y_coord'
                  WHEN 'Z-coordinate' THEN 'z_coord'
                  WHEN 'Location class' THEN 'location_class'
                  WHEN 'Slot priority' THEN 'slot_priority'
                  --WHEN 'Action code' THEN 'action_code'
               END)
                 AS offset_fieldname,
              ifo.start_posn,
              ifo.LENGTH
         FROM import_format_offsets ifo, import_offset_master iom
        WHERE     iom.import_id = v_import_id
              AND iom.offset_id = ifo.offset_id
              AND ifo.format_id = v_format_id
              AND iom.offset_type = 2);
 CURSOR TmpColInfo_cursor1 is (select format_id,format_offset_id,stg_col_name,dft_value from stg_offset_xref s where s.format_id=v_format_id);
BEGIN
   UPDATE staging
      SET whse_code = v_whse_code
    WHERE whse_code IS NULL;

   DELETE FROM staging
         WHERE whse_code = v_whse_code AND LTRIM (line) = '' OR line IS NULL;


DELETE FROM stg_offset_xref;

select default_blank,default_zero into v_default_blank,v_default_zero from import_format_prefs where format_id = v_format_id and whse_code=v_whse_code;

v_col_part := 'INSERT INTO SLOT_STG (slot_stg_id';
v_value_part := ' SELECT SEQ_SLOT_STG_ID.nextval ';
v_row_cnt := 0;

OPEN TmpColInfo_cursor;
LOOP
FETCH TmpColInfo_cursor INTO v_offset_fieldname,v_start_posn,v_length;

EXIT WHEN TmpColInfo_cursor%NOTFOUND;

IF (v_offset_fieldname IS NOT NULL and v_offset_fieldname <> 'rt_name') THEN

    v_col_name := ',' || v_offset_fieldname;
    if(v_start_posn >0 and v_length > 0) then
          v_start_posn := v_start_posn + 1;
    end if;

    v_stmnt := ',' || 'rtrim(ltrim(substr(line,'||v_start_posn||','||v_length||')))';
  if (v_offset_fieldname = 'locked' or v_offset_fieldname='allow_expand') then
    if(v_length > 0) then
       v_stmnt := ',' || 'case when (rtrim(ltrim(substr(line,'||v_start_posn||','||v_length||'))) = ''Y'') then 1 else 0 end';
    end if;
  end if;
   if (v_offset_fieldname = 'dsp_slot') then
     v_stmnt := ',' || 'substr(line,'||v_start_posn||','||v_length||')';
  end if;

    v_col_part := v_col_part || v_col_name;

    v_value_part := v_value_part || v_stmnt;
    v_row_cnt := v_row_cnt + 1;

END IF;

IF (v_offset_fieldname IS NOT NULL and v_offset_fieldname = 'rt_name') THEN

    v_col_name := ',' || v_offset_fieldname;
    if(v_start_posn >0 and v_length > 0) then
          v_start_posn := v_start_posn + 1;
    end if;
    v_stmnt := ',' || 'rtrim(ltrim(substr(line,'||v_start_posn||','||v_length||')))';


    v_col_part := v_col_part || v_col_name;

      v_value_part := v_value_part || v_stmnt;
      v_row_cnt := v_row_cnt + 1;

END IF;

END LOOP;
CLOSE TmpColInfo_cursor;

v_col_part := v_col_part || ') ';
v_value_part := v_value_part || ' FROM STAGING WHERE whse_code = ''' || v_whse_code || '''';

v_SQLString := v_col_part || v_value_part;

--dbms_output.enable(1000000);

DELETE FROM SLOT_STG; --WHERE WHSE_CODE = v_whse_code;
EXECUTE IMMEDIATE v_SQLString;

update slot_stg s  set s.rt_name = (select rt_name from rack_type where rack_type=(select dft_rack_type from import_format_prefs where format_id = v_format_id and whse_code=v_whse_code)) where s.rt_name is null;

update slot_stg s  set s.rack_type = (select rack_type from rack_type where rt_name= s.rt_name and whse_code=v_whse_code);

update slot_stg set area = substr(dsp_slot, v_area_start, v_area_end),
	zone  = substr(dsp_slot, v_zone_start ,v_zone_end),
	aisle = substr(dsp_slot, v_aisle_start, v_aisle_end),
	bay   = substr(dsp_slot, v_bay_start, v_bay_end),
	lvl   = substr(dsp_slot, v_lvl_start, v_lvl_end),
	posn  = substr(dsp_slot, v_posn_start, v_posn_end);

update slot_stg set slot_stg.my_range = (select slot.my_range from slot where slot_stg.dsp_slot = slot.dsp_slot and slot.whse_code = v_whse_code) ;

update slot_stg ss set ss.rack_level_id = (select rl.rack_level_id from rack_type_level rl, rack_level_map rm where rl.rack_level_id =  rm.rack_level_id and rl.rack_type=rm.rack_type and ss.rack_type=rm.rack_type and ss.lvl = rm.component_val and rownum=1);


insert into stg_offset_xref
SELECT DISTINCT  ifo.format_id,

              (CASE (iom.offset_fieldname)
                  WHEN 'Slot Number' THEN ('DSP_SLOT')
                  WHEN 'Rack Type' THEN 'RT_NAME'
                  WHEN 'Height' THEN 'HT'
                  WHEN 'Width' THEN 'WIDTH'
                  WHEN 'Depth' THEN 'DEPTH'
                  WHEN 'Clearance' THEN 'MAX_HT_CLEAR'
                  WHEN 'Side Clearance' THEN 'MAX_SIDE_CLEAR'
                  WHEN 'Locked' THEN 'LOCKED'
                  WHEN 'Lock Reason' THEN 'LOCK_REASON'
                  WHEN 'Max Stacking' THEN 'MAX_STACK'
                  WHEN 'Max Lanes' THEN 'MAX_LANES'
                  WHEN 'Expand Next' THEN 'ALLOW_EXPAND'
                  WHEN 'Label Location' THEN 'LABEL_POS'
                  WHEN 'Max Slot Weight' THEN 'WT_LIMIT'
                  WHEN 'Max Lane Weight' THEN 'MAX_LANE_WT'
                  WHEN 'Slot type' THEN 'SLOT_TYPE'
                  WHEN 'Travel zone' THEN 'TRAVEL_ZONE'
                  WHEN 'Travel aisle' THEN 'TRAVEL_AISLE'
                  WHEN 'X-coordinate' THEN 'X_COORD'
                  WHEN 'Y-coordinate' THEN 'Y_COORD'
                  WHEN 'Z-coordinate' THEN 'Z_COORD'
                  WHEN 'Location class' THEN 'LOCATION_CLASS'
                  WHEN 'Slot priority' THEN 'SLOT_PRIORITY'
                  WHEN 'Action code' THEN 'ACTION_CODE'
               END)
                 AS offset_fieldname ,
                  ifo.format_offset_id,ifo.default_value
         FROM import_format_offsets ifo, import_offset_master iom
        WHERE    iom.import_id = v_import_id
              AND  iom.offset_id = ifo.offset_id
              AND ifo.format_id = v_format_id
              AND iom.offset_type = 2;

DELETE FROM staging WHERE whse_code = v_whse_code;

/*OPEN TmpColInfo_cursor1;
LOOP
FETCH TmpColInfo_cursor1 INTO v_fomat_id,v_format_offset_id,v_stg_col_name,v_dft_value;

EXIT WHEN TmpColInfo_cursor1%NOTFOUND;

IF (v_stg_col_name IS NOT NULL and v_stg_col_name <> 'RT_NAME' and v_stg_col_name<>'ACTION_CODE' and  v_stg_col_name<>'DSP_SLOT') THEN
 if(v_default_blank =1 and v_dft_value is not null) then
    if(v_stg_col_name = 'LOCK_REASON' or v_stg_col_name = 'SLOT_TYPE' or v_stg_col_name = 'TRAVEL_ZONE' or v_stg_col_name = 'TRAVEL_AISLE' or v_stg_col_name = 'LOCATION_CLASS' or v_stg_col_name = 'SLOT_PRIORITY' ) then
      v_SQLString := 'update slot_stg set ' || v_stg_col_name ||' = '''|| v_dft_value ||''' , PROCESS_CODE=''B'' where ' || v_stg_col_name || ' is null ' ;
    else
      v_SQLString := 'update slot_stg set ' || v_stg_col_name ||' = '|| v_dft_value ||' , PROCESS_CODE=''B'' where ' || v_stg_col_name || ' is null ' ;

        if(v_stg_col_name = 'LOCKED' or v_stg_col_name = 'ALLOW_EXPAND') then
            if(v_dft_value ='Y') then
             v_locked := 1;
            else
             v_locked := 0;
            end if;
            v_SQLString := 'update slot_stg set ' || v_stg_col_name ||' = '|| v_locked ||' , PROCESS_CODE=''B'' where ' || v_stg_col_name || ' is null ' ;
        end if;
    end if;


    EXECUTE IMMEDIATE v_SQLString;
  end if;

   if(v_default_zero=1 and v_dft_value is not null) then
    if(v_stg_col_name = 'LOCK_REASON' or v_stg_col_name = 'SLOT_TYPE' or v_stg_col_name = 'TRAVEL_ZONE' or v_stg_col_name = 'TRAVEL_AISLE' or v_stg_col_name = 'LOCATION_CLASS' or v_stg_col_name = 'SLOT_PRIORITY' ) then
    v_SQLString := 'update slot_stg set ' || v_stg_col_name ||' = ''' || v_dft_value ||''' , PROCESS_CODE=''Z'' where ' || v_stg_col_name || ' is not null and '||v_stg_col_name||' = ''0'' ' ;
    else
    v_SQLString := 'update slot_stg set ' || v_stg_col_name ||' = ' || v_dft_value ||' , PROCESS_CODE=''Z'' where ' || v_stg_col_name || ' is not null and '||v_stg_col_name||' = 0 ' ;
      if(v_stg_col_name = 'LOCKED' or v_stg_col_name = 'ALLOW_EXPAND') then
          if(v_dft_value ='Y') then
           v_locked := 1;
          else
           v_locked := 0;
          end if;
          v_SQLString := 'update slot_stg set ' || v_stg_col_name ||' = ' || v_locked ||' , PROCESS_CODE=''Z'' where ' || v_stg_col_name || ' is not null and '||v_stg_col_name||' = 0  and '|| v_stg_col_name || ' != '|| v_locked ;

      end if;
    end if;

    EXECUTE IMMEDIATE v_SQLString;
  end if;
END IF;
END LOOP;
CLOSE TmpColInfo_cursor1;
*/

COMMIT;


END;

/
--------------------------------------------------------
--  DDL for Procedure SP_SELECTPIDSFORQUERY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_SELECTPIDSFORQUERY (tablename in varchar2,sqlqry in varchar2)
as
begin
   declare
   sql_stmt varchar(12000);
   begin
   sql_stmt := ' delete from ' || tablename || ' where mod_date_time < (sysdate - 1) ';
   execute immediate sql_stmt;
   --insert into filter_slot_item select slot_item.slotitem_id,4 from slot_item
   sql_stmt := ' insert into ' || tablename || ' ' || sqlqry;
   execute immediate sql_stmt;
   end;
end;

/
--------------------------------------------------------
--  DDL for Procedure SP_SHIPPING_MOVEMENT_SUM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_SHIPPING_MOVEMENT_SUM (v_range_group_id IN NUMBER, movementR OUT NUMBER)
   as
   v_movement  NUMBER(15,2);
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE14');
   select   count(*) INTO v_no_of_items from tt_RANGE_IDS_TABLE14;
   v_movement := 0.0;
   begin
      select   sum(case
      when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
         case
         when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
         case
         when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot  on slot.slot_id = slot_item.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from tt_RANGE_IDS_TABLE14);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))
            else 0.0
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
               get_case_movement(est_movement,case_mov,use_estimated_hist)
            else 0.0
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))
            else 0.0
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot  on slot.slot_id = slot_item.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE14);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE14 ';

   if (v_movement >= 0) then

      movementR := v_movement;
   else
      movementR := -0.1;
      --open v_refcur for select v_movement  from dual;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_SHIPPING_MOVEMENT_SUM2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_SHIPPING_MOVEMENT_SUM2 (v_range_group_id IN NUMBER,
    v_movement IN OUT NUMBER) as
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'RANGE_IDS_TABLE15');
   select   count(*) INTO v_no_of_items from RANGE_IDS_TABLE15;
   v_movement := 0.0;
   begin
      select   sum(case
      when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
         case
         when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
         case
         when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then
            0
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot  on slot.slot_id = slot_item.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from RANGE_IDS_TABLE15);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then(get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist))
            else 0.0
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then
               get_case_movement(est_movement,case_mov,use_estimated_hist)
            else 0.0
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then(get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist))
            else 0.0
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then(get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist))
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot  on slot.slot_id = slot_item.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from RANGE_IDS_TABLE15);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE RANGE_IDS_TABLE15 ';

   if (v_movement < 0) then

      v_movement := -0.1;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_SHIPPING_UNIT_WEIGHT_SUM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_SHIPPING_UNIT_WEIGHT_SUM (v_range_group_id IN NUMBER, v_refcur OUT SYS_REFCURSOR)
   as
   v_movement  NUMBER(15,2);
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE16');
   select   count(*) INTO v_no_of_items from tt_RANGE_IDS_TABLE16;
   v_movement := 0.0;

   begin
      select   sum(case
      when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when slot_item.pallete_pattern = 0 then
            0
         when slot_item.pallete_pattern = 1 then
            0
         when slot_item.pallete_pattern = 2 then
            0
         else -1
         end
      when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when so_item_master.case_wt >= 0 then
            0
         else -1
         end
      when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
         case
         when so_item_master.inn_wt >= 0 then
            0
         else -1
         end
      when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
         case
         when so_item_master.each_wt >= 0.0 then
            0
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
      left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from tt_RANGE_IDS_TABLE16);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
            when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
            when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
            else 0.0
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.case_wt > 0 then
               so_item_master.case_wt
            else 0.0
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when so_item_master.inn_wt > 0 then
               so_item_master.inn_wt
            else 0.0
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when so_item_master.each_wt > 0.0 then
               so_item_master.each_wt
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE16);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE16 ';

   if (v_movement >= 0) then

      open v_refcur for select v_movement  from dual;
   else
      v_movement := -0.1;
      open v_refcur for select v_movement  from dual;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_SHIPPING_UNIT_WEIGHT_SUM2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_SHIPPING_UNIT_WEIGHT_SUM2 (v_range_group_id IN NUMBER,
    v_movement IN OUT NUMBER) as
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'RANGE_IDS_TABLE17');
   select   count(*) INTO v_no_of_items from RANGE_IDS_TABLE17;
   v_movement := 0.0;

   begin
      select   sum(case
      when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when slot_item.pallete_pattern = 0 then
            0
         when slot_item.pallete_pattern = 1 then
            0
         when slot_item.pallete_pattern = 2 then
            0
         else -1
         end
      when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when so_item_master.case_wt >= 0 then
            0
         else -1
         end
      when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
         case
         when so_item_master.inn_wt >= 0 then
            0
         else -1
         end
      when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
         case
         when so_item_master.each_wt >= 0.0 then
            0
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
      left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from RANGE_IDS_TABLE17);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.ship_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
            when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
            when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
            else 0.0
            end
         when slot_item.ship_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.case_wt > 0 then
               so_item_master.case_wt
            else 0.0
            end
         when slot_item.ship_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when so_item_master.inn_wt > 0 then
               so_item_master.inn_wt
            else 0.0
            end
         when slot_item.ship_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when so_item_master.each_wt > 0.0 then
               so_item_master.each_wt
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join pallets on slot_item.current_pallet = pallets.pallet_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from RANGE_IDS_TABLE17);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE RANGE_IDS_TABLE17 ';

   if (v_movement < 0) then

      v_movement := -0.1;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_SLOTTING_MOVEMENT_SUM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_SLOTTING_MOVEMENT_SUM (v_range_group_id IN NUMBER, v_refcur OUT SYS_REFCURSOR)
   as
   v_movement  NUMBER(15,2);
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE18');
   select   count(*) INTO v_no_of_items from tt_RANGE_IDS_TABLE18;
   v_movement := 0.0;

   begin
      select   sum(case
      when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then /*SQLWAYS_EVAL# movement*/
            0
         else -1
         end
      when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then /*SQLWAYS_EVAL# movement*/
            0
         else -1
         end
      when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# movement*/
         case
         when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then /*SQLWAYS_EVAL# movement*/
            0
         else -1
         end
      when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# movement*/
         case
         when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then /*SQLWAYS_EVAL# movement*/
            0
         else -1
         end
      when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# movement .. handle differently.*/
         case
         when slot_item.est_movement >= 0.0 then /*SQLWAYS_EVAL# movement*/
            case slot_item.bin_unit
            when 2 then /*SQLWAYS_EVAL# se*/
               0
            when 4 then /*SQLWAYS_EVAL# ner*/
               0
            when 8 then /*SQLWAYS_EVAL# ch*/
               0
            else -1
            end
         when slot_item.bin_mov >= 0.0 then /*SQLWAYS_EVAL# ement*/
            slot_item.bin_mov
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
      left outer join bins on slot_item.current_bin = bins.bin_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from tt_RANGE_IDS_TABLE18);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
               get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)
            else 0.0
            end
         when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
               get_case_movement(est_movement,case_mov,use_estimated_hist)
            else 0.0
            end
         when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
               get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)
            else 0.0
            end
         when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
               get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist)
            else 0.0
            end
         when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# movement .. handle differently.*/
            case
            when slot_item.est_movement > 0.0 then /*SQLWAYS_EVAL# movement*/
               case slot_item.bin_unit
               when 2 then /*SQLWAYS_EVAL# se*/
                              (100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
               when 4 then /*SQLWAYS_EVAL# ner*/
                            (100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
               when 8 then /*SQLWAYS_EVAL# ch*/
                            (100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
               else 0.0
               end
            when slot_item.bin_mov > 0.0 then /*SQLWAYS_EVAL# ement*/
               slot_item.bin_mov
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join bins on slot_item.current_bin = bins.bin_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE18);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;
   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE18 ';

   if (v_movement >= 0) then

      open v_refcur for select v_movement  from dual;
   else
      v_movement := -0.1;
      open v_refcur for select v_movement  from dual;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_SLOTTING_MOVEMENT_SUM2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_SLOTTING_MOVEMENT_SUM2 (v_range_group_id IN NUMBER,
    v_movement IN OUT NUMBER) as
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'RANGE_IDS_TABLE19');
   select   count(*) INTO v_no_of_items from RANGE_IDS_TABLE19;
   v_movement := 0.0;

   begin
      select   sum(case
      when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) >= 0.0 then /*SQLWAYS_EVAL# movement*/
            0
         else -1
         end
      when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when get_case_movement(est_movement,case_mov,use_estimated_hist) >= 0.0 then /*SQLWAYS_EVAL# movement*/
            0
         else -1
         end
      when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# movement*/
         case
         when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) >= 0.0 then /*SQLWAYS_EVAL# movement*/
            0
         else -1
         end
      when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# movement*/
         case
         when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) >= 0.0 then /*SQLWAYS_EVAL# movement*/
            0
         else -1
         end
      when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# movement .. handle differently.*/
         case
         when slot_item.est_movement >= 0.0 then /*SQLWAYS_EVAL# movement*/
            case slot_item.bin_unit
            when 2 then /*SQLWAYS_EVAL# se*/
               0
            when 4 then /*SQLWAYS_EVAL# ner*/
               0
            when 8 then /*SQLWAYS_EVAL# ch*/
               0
            else -1
            end
         when slot_item.bin_mov >= 0.0 then /*SQLWAYS_EVAL# ement*/
            slot_item.bin_mov
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
      left outer join bins on slot_item.current_bin = bins.bin_id where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from RANGE_IDS_TABLE19);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
               get_pallet_movement(so_item_master.sku_id,pallete_pattern,pallet_mov,est_movement,use_estimated_hist)
            else 0.0
            end
         when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when get_case_movement(est_movement,case_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
               get_case_movement(est_movement,case_mov,use_estimated_hist)
            else 0.0
            end
         when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# movement*/
            case
            when get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
               get_inner_movement(inn_per_cs,est_movement,inner_mov,use_estimated_hist)
            else 0.0
            end
         when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# movement*/
            case
            when get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist) > 0.0 then /*SQLWAYS_EVAL# movement*/
               get_each_movement(each_per_cs,est_movement,each_mov,use_estimated_hist)
            else 0.0
            end
         when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# movement .. handle differently.*/
            case
            when slot_item.est_movement > 0.0 then /*SQLWAYS_EVAL# movement*/
               case slot_item.bin_unit
               when 2 then /*SQLWAYS_EVAL# se*/
                                  (100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
               when 4 then /*SQLWAYS_EVAL# ner*/
                                (100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
               when 8 then /*SQLWAYS_EVAL# ch*/
                                (100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent)
               else 0.0
               end
            when slot_item.bin_mov > 0.0 then /*SQLWAYS_EVAL# ement*/
               slot_item.bin_mov
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id
         left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join bins on slot_item.current_bin = bins.bin_id where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from RANGE_IDS_TABLE19);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;
   EXECUTE IMMEDIATE ' TRUNCATE TABLE RANGE_IDS_TABLE19 ';

   if (v_movement < 0) then

      v_movement := -0.1;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_SLOTTING_UNIT_WEIGHT_SUM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_SLOTTING_UNIT_WEIGHT_SUM (v_range_group_id IN NUMBER, v_refcur OUT SYS_REFCURSOR)
   as
   v_movement  NUMBER(15,2);
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE20');
   select   count(*) INTO v_no_of_items from tt_RANGE_IDS_TABLE20;
   v_movement := 0.0;

   begin
      select   sum(case
      when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when slot_item.pallete_pattern = 0 then
            0
         when slot_item.pallete_pattern = 1 then
            0
         when slot_item.pallete_pattern = 2 then
            0
         else -1
         end
      when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when so_item_master.case_wt >= 0 then
            0
         else -1
         end
      when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# unit*/
         case
         when so_item_master.inn_wt >= 0 then
            0
         else -1
         end
      when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# unit*/
         case
         when so_item_master.each_wt >= 0.0 then
            0
         else -1
         end
      when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# nit*/
         case slot_item.bin_unit
         when 2 then /*SQLWAYS_EVAL# se*/
            0
         when 4 then /*SQLWAYS_EVAL# ner*/
            0
         when 8 then /*SQLWAYS_EVAL# ch*/
            0
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
      left outer join pallets on pallets.pallet_id = slot_item.current_pallet
      left outer join bins on bins.bin_id = slot_item.current_bin where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from tt_RANGE_IDS_TABLE20);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
            when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
            when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
            else 0.0
            end
         when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.case_wt > 0 then
               so_item_master.case_wt
            else 0.0
            end
         when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.inn_wt > 0 then
               so_item_master.inn_wt
            else 0.0
            end
         when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.each_wt > 0.0 then
               so_item_master.each_wt
            else 0.0
            end
         when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# nit*/
            case slot_item.bin_unit
            when 2 then /*SQLWAYS_EVAL# se*/
                      (((100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.case_wt)+bins.weight
            when 4 then /*SQLWAYS_EVAL# ner*/
                    (((100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.inn_wt)+bins.weight
            when 8 then /*SQLWAYS_EVAL# ch*/
                    (((100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.each_wt)+bins.weight
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id  left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join pallets on pallets.pallet_id = slot_item.current_pallet
         left outer join bins on bins.bin_id = slot_item.current_bin where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from tt_RANGE_IDS_TABLE20);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE20 ';

   if (v_movement >= 0) then

      open v_refcur for select v_movement  from dual;
   else
      v_movement := -0.1;
      open v_refcur for select v_movement  from dual;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_SLOTTING_UNIT_WEIGHT_SUM2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_SLOTTING_UNIT_WEIGHT_SUM2 (v_range_group_id IN NUMBER,
    v_movement IN OUT NUMBER) as
   v_no_of_items  NUMBER(10,0);
BEGIN

   sp_get_range_ids(v_range_group_id,'RANGE_IDS_TABLE21');
   select   count(*) INTO v_no_of_items from RANGE_IDS_TABLE21;
   v_movement := 0.0;

   begin
      select   sum(case
      when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
         case
         when slot_item.pallete_pattern = 0 then
            0
         when slot_item.pallete_pattern = 1 then
            0
         when slot_item.pallete_pattern = 2 then
            0
         else -1
         end
      when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
         case
         when so_item_master.case_wt >= 0 then
            0
         else -1
         end
      when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# unit*/
         case
         when so_item_master.inn_wt >= 0 then
            0
         else -1
         end
      when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# unit*/
         case
         when so_item_master.each_wt >= 0.0 then
            0
         else -1
         end
      when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# nit*/
         case slot_item.bin_unit
         when 2 then /*SQLWAYS_EVAL# se*/
            0
         when 4 then /*SQLWAYS_EVAL# ner*/
            0
         when 8 then /*SQLWAYS_EVAL# ch*/
            0
         else -1
         end
      else -1
      end) INTO v_movement from slot_item
      inner join slot on slot_item.slot_id = slot.slot_id
      left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
      left outer join pallets on pallets.pallet_id = slot_item.current_pallet
      left outer join bins on bins.bin_id = slot_item.current_bin where slot_item.slot_id is not null and slot_item.sku_id is not null
      and slot.my_range in(select * from RANGE_IDS_TABLE21);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   if(v_movement <> -1*v_no_of_items) then
      begin
         select   sum(case
         when slot_item.slot_unit = 1 then /*SQLWAYS_EVAL# unit*/
            case
            when slot_item.pallete_pattern = 0 then(so_item_master.case_wt*(so_item_master.wh_hi*so_item_master.wh_ti))+pallets.weight
            when slot_item.pallete_pattern = 1 then(so_item_master.case_wt*(so_item_master.ord_hi*so_item_master.ord_ti))+pallets.weight
            when slot_item.pallete_pattern = 2 then(so_item_master.case_wt*(so_item_master.ven_hi*so_item_master.ven_ti))+pallets.weight
            else 0.0
            end
         when slot_item.slot_unit = 2 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.case_wt > 0 then
               so_item_master.case_wt
            else 0.0
            end
         when slot_item.slot_unit = 4 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.inn_wt > 0 then
               so_item_master.inn_wt
            else 0.0
            end
         when slot_item.slot_unit = 8 then /*SQLWAYS_EVAL# unit*/
            case
            when so_item_master.each_wt > 0.0 then
               so_item_master.each_wt
            else 0.0
            end
         when slot_item.slot_unit = 16 then /*SQLWAYS_EVAL# nit*/
            case slot_item.bin_unit
            when 2 then /*SQLWAYS_EVAL# se*/
                          (((100*slot_item.est_movement*so_item_master.case_wid*so_item_master.case_len*so_item_master.case_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.case_wt)+bins.weight
            when 4 then /*SQLWAYS_EVAL# ner*/
                        (((100*slot_item.est_movement*so_item_master.inn_per_cs*so_item_master.inn_wid*so_item_master.inn_len*so_item_master.inn_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.inn_wt)+bins.weight
            when 8 then /*SQLWAYS_EVAL# ch*/
                        (((100*slot_item.est_movement*so_item_master.each_per_cs*so_item_master.each_wid*so_item_master.each_len*so_item_master.each_ht)/(bins.length*bins.width*bins.height*bins.fillpercent))*so_item_master.each_wt)+bins.weight
            else 0.0
            end
         else 0.0
         end) INTO v_movement from slot_item
         inner join slot on slot_item.slot_id = slot.slot_id  left outer join so_item_master on slot_item.sku_id = so_item_master.sku_id
         left outer join pallets on pallets.pallet_id = slot_item.current_pallet
         left outer join bins on bins.bin_id = slot_item.current_bin where slot_item.slot_id is not null and slot_item.sku_id is not null
         and slot.my_range in(select * from RANGE_IDS_TABLE21);
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   end if;

   EXECUTE IMMEDIATE ' TRUNCATE TABLE RANGE_IDS_TABLE21 ';

   if (v_movement < 0) then

      v_movement := -0.1;
   end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_UPDATEIMPORTTEMPFORMLM
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_UPDATEIMPORTTEMPFORMLM 

   as
BEGIN



   insert into tt_COUNT_TAB  select sku_id,0,count(1) from import_temp group by sku_id;
   update tt_COUNT_TAB set count_slot_item =(select count(1) from slot_item where slot_item.sku_id = tt_COUNT_TAB.sku_id);
   update import_temp set slotitem_id = NULL where import_temp.sku_id in(select tt_COUNT_TAB.sku_id from tt_COUNT_TAB where tt_COUNT_TAB.count_slot_item <> tt_COUNT_TAB.count_imp_temp);
   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_COUNT_TAB ';

END;

/
--------------------------------------------------------
--  DDL for Procedure SP_UPDATESINUM6INIMPTEMP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_UPDATESINUM6INIMPTEMP (v_whse_code IN NVARCHAR2) as
BEGIN

   UPDATE import_temp SET si_num_6 =
   case
   when ship_unit = 'P' then 1
   else
      case
      when ship_unit = 'C' then 2
      else
         case
         when ship_unit = 'I' then 4
         else
            case
            when ship_unit = 'E' then 8
            else
               case
               when ship_unit = 'B' then 16
               end
            end
         end
      end
   end WHERE whse_code = v_whse_code;

END;

/
--------------------------------------------------------
--  DDL for Procedure SP_UPDATEUNIQUEIDS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_UPDATEUNIQUEIDS 
   as
BEGIN

--SQLWAYS_EVAL# set curr_nbr = isnull((select max(auto_id) + 1 from automation), curr_nbr) where rec_type = 'automation'
   update unique_ids set curr_nbr = NVL((select max(binxref_id)  from bin_xref),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(bin_id)  from bins),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(borrowing_id) + 1 from borrowing                     ), curr_nbr) where rec_type = 'borrowing                     '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(br_addl_items_id) + 1 from br_addl_items), curr_nbr) where rec_type = 'br_addl_items'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(br_dtl_id) + 1 from br_detail                     ), curr_nbr) where rec_type = 'br_detail                     '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(br_hdr_id) + 1 from br_header                     ), curr_nbr) where rec_type = 'br_header                     '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(br_target_id) + 1 from br_target), curr_nbr) where rec_type = 'br_target'
   update unique_ids set curr_nbr = NVL((select max(cat_id)  from category),curr_nbr) where rec_type = 'category';
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(cnstr_id) + 1 from cnstr_dtl                     ), curr_nbr) where rec_type = 'cnstr_dtl                     '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(cnstr_grp_id) + 1 from cnstr_grp                     ), curr_nbr) where rec_type = 'cnstr_grp                     '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(cnstr_rack_level_id) + 1 from cnstr_rack_level), curr_nbr) where rec_type = 'cnstr_rack_level'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(color_code_dtl_id) + 1 from color_code_dtl), curr_nbr) where rec_type = 'color_code_dtl'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(color_code_id) + 1 from color_code_hdr), curr_nbr) where rec_type = 'color_code_hdr'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(cnstr_id) + 1 from constraints                   ), curr_nbr) where rec_type = 'constraints                   '
   update unique_ids set curr_nbr = NVL((select max(cntr_type)  from container_type),curr_nbr) where rec_type = 'container_type';
   update unique_ids set curr_nbr = NVL((select max(ex_id)  from exceptions),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(field_id) + 1 from filter_field_master), curr_nbr) where rec_type = 'filter_field_master'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(format_id) + 1 from filter_format                 ), curr_nbr) where rec_type = 'filter_format                 '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(format_detail_id) + 1 from filter_format_detail          ), curr_nbr) where rec_type = 'filter_format_detail          '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(forecast_id) + 1 from forecast                      ), curr_nbr) where rec_type = 'forecast                      '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(cat_grp_code_id) + 1 from grp_cat_code                  ), curr_nbr) where rec_type = 'grp_cat_code                  '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(haves_needs_id) + 1 from haves_needs                   ), curr_nbr) where rec_type = 'haves_needs                   '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(hn_failure_id) + 1 from hn_failure                    ), curr_nbr) where rec_type = 'hn_failure                    '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(format_odbc_id) + 1 from import_format_odbc), curr_nbr) where rec_type = 'import_format_odbc'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(format_offset_id) + 1 from import_format_offsets), curr_nbr) where rec_type = 'import_format_offsets'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(import_format_prefs_id) + 1 from import_format_prefs), curr_nbr) where rec_type = 'import_format_prefs'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(format_id) + 1 from import_formats), curr_nbr) where rec_type = 'import_formats'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(import_id) + 1 from import_master), curr_nbr) where rec_type = 'import_master'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(offset_id) + 1 from import_offset_master), curr_nbr) where rec_type = 'import_offset_master'
   update unique_ids set curr_nbr = NVL((select max(itemcat_id)  from item_cat_xref),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(hist_id)  from item_history),curr_nbr) where rec_type = 'item_history';
   update unique_ids set curr_nbr = NVL((select max(sku_id)  from so_item_master),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(kit_id)  from kit),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(kititem_id) + 1 from kit_item                      ), curr_nbr) where rec_type = 'kit_item                      '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(slot_range) + 1 from location_range                ), curr_nbr) where rec_type = 'location_range                '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(msg_id) + 1 from message_master), curr_nbr) where rec_type = 'message_master'
   update unique_ids set curr_nbr = NVL((select max(move_id)  from move_list),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(move_hdr_id)  from move_list_hdr),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(move_report_id)  from move_list_report),
   curr_nbr) where rec_type = 'SQLWAYS_EVAL#              ';
   update unique_ids set curr_nbr = NVL((select max(mult_orient_calc_id)  from mult_orient_calc),curr_nbr) where rec_type = 'SQLWAYS_EVAL# lc';
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(order_dtl_id) + 1 from order_dtl                     ), curr_nbr) where rec_type = 'order_dtl                     '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(order_id) + 1 from order_hdr                     ), curr_nbr) where rec_type = 'order_hdr                     '
   update unique_ids set curr_nbr = NVL((select max(pallet_xref_id) from pallet_xref),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(pallet_id)  from pallets),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(pick_zone_id)  from pick_zone),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(pickline_grp_id)  from pickline_group),
   curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(profile_access_id) + 1 from profile_access                ), curr_nbr) where rec_type = 'profile_access                '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(profile_id) + 1 from profile_master                ), curr_nbr) where rec_type = 'profile_master                '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(query_id) + 1 from query                         ), curr_nbr) where rec_type = 'query                         '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(queryentry_id) + 1 from queryentry                    ), curr_nbr) where rec_type = 'queryentry                    '
   update unique_ids set curr_nbr = NVL((select max(rbx_id)  from rack_bin_xref),curr_nbr) where rec_type = 'rack_bin_xref';
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(rack_level_map_id) + 1 from rack_level_map                ), curr_nbr) where rec_type = 'rack_level_map                '
   update unique_ids set curr_nbr = NVL((select max(rpx_id)  from rack_pallet_xref),curr_nbr) where rec_type = 'SQLWAYS_EVAL#              ';
   update unique_ids set curr_nbr = NVL((select max(rack_type)  from rack_type),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(rack_level_id) + 1 from rack_type_level               ), curr_nbr) where rec_type = 'rack_type_level               '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(res_analysis_id) + 1 from res_analysis_result), curr_nbr) where rec_type = 'res_analysis_result'
   update unique_ids set curr_nbr = NVL((select max(res_binxref_id)  from reserve_bin_xref),curr_nbr) where rec_type = 'SQLWAYS_EVAL# ef';
   update unique_ids set curr_nbr = NVL((select max(res_pallet_xref_id)  from reserve_pallet_xref),curr_nbr) where rec_type = 'SQLWAYS_EVAL# _xref';
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(seas_id) + 1 from seasonal                      ), curr_nbr) where rec_type = 'seasonal                      '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(seasonal_dtl_id) + 1 from seasonal_detail               ), curr_nbr) where rec_type = 'seasonal_detail               '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(shelf_id) + 1 from shelf), curr_nbr) where rec_type = 'shelf'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(shelf_slot_xref_id) + 1 from shelf_slot_xref), curr_nbr) where rec_type = 'shelf_slot_xref'
   update unique_ids set curr_nbr = NVL((select max(slot_id)  from slot),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(slotitem_id)  from slot_item),curr_nbr) where rec_type = 'SQLWAYS_EVAL#                ';
   update unique_ids set curr_nbr = NVL((select max(slot_item_score_id)  from slot_item_score),curr_nbr) where rec_type = 'SQLWAYS_EVAL#               ';
END;

--SQLWAYS_EVAL# set curr_nbr = isnull((select max(tree_id) + 1 from slot_list_tree                ), curr_nbr) where rec_type = 'slot_list_tree                '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(sns_id) + 1 from sns                           ), curr_nbr) where rec_type = 'sns                           '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(sns_dtl_id) + 1 from sns_dtl), curr_nbr) where rec_type = 'sns_dtl'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(sys_code_id) + 1 from system_code), curr_nbr) where rec_type = 'system_code'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(sys_code_dtl_id) + 1 from system_code_dtl), curr_nbr) where rec_type = 'system_code_dtl'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(node_id) + 1 from tree_dtl                      ), curr_nbr) where rec_type = 'tree_dtl                      '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(txn_id) + 1 from txn_master), curr_nbr) where rec_type = 'txn_master'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(user_id) + 1 from user_master                   ), curr_nbr) where rec_type = 'user_master                   '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(user_profile_xref_id) + 1 from user_profile_xref             ), curr_nbr) where rec_type = 'user_profile_xref             '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(user_range_xref_id) + 1 from user_range_xref), curr_nbr) where rec_type = 'user_range_xref'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(wa_dtl_id) + 1 from wa_detail), curr_nbr) where rec_type = 'wa_detail'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(wa_number) + 1 from wa_header), curr_nbr) where rec_type = 'wa_header'
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(warehouse_id) + 1 from whprefs                       ), curr_nbr) where rec_type = 'whprefs                       '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(import_pref_id) + 1 from whse_import_prefs             ), curr_nbr) where rec_type = 'whse_import_prefs             '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(whse_process_id) + 1 from whse_process                  ), curr_nbr) where rec_type = 'whse_process                  '
--SQLWAYS_EVAL# set curr_nbr = isnull((select max(server_id) + 1 from whse_server), curr_nbr) where rec_type = 'whse_server'

/
--------------------------------------------------------
--  DDL for Procedure SP_UPDATE_PICKLINE2_TMP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_UPDATE_PICKLINE2_TMP 
   as
   v_if_exists NUMBER(10,0);
   SWV_PLG_Mbr NUMBER(10,0);
   SWV_PLG_ID NUMBER(10,0);
   SWV_whse_code CHAR(3);
   SWV_name VARCHAR2(65);
   SWV_item_attribute VARCHAR2(65);
   SWV_importance NUMBER(10,0);
   SWV_tree_id NUMBER(10,0);
   SWV_tree_node_name VARCHAR2(32);
   SWV_tree_node_path VARCHAR2(100);
   SWV_parent_node_id NUMBER(10,0);
   SWV_proportion NUMBER(5,2);
   SWV_plb_score NUMBER(9,2);
   SWV_calculated NUMBER(9,2);
   SWV_RCur SYS_REFCURSOR;
BEGIN
SELECT   COUNT(*) INTO v_if_exists FROM user_tables WHERE table_name = 'pick_line2_sp_as_tmp';
   IF  v_if_exists > 0 then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE pick_line2_sp_as_tmp ';
   end if;


   pick_line2(SWV_RCur);
   FETCH SWV_RCur INTO SWV_PLG_Mbr,SWV_PLG_ID,SWV_whse_code,SWV_name,SWV_item_attribute,SWV_importance,
   SWV_tree_id,SWV_tree_node_name,SWV_tree_node_path,SWV_parent_node_id,
   SWV_proportion,SWV_plb_score,SWV_calculated;
   WHILE SWV_RCur%FOUND  LOOP
      INSERT INTO pick_line2_sp_as_tmp  VALUES(SWV_PLG_Mbr, SWV_PLG_ID, SWV_whse_code, SWV_name, SWV_item_attribute, SWV_importance, SWV_tree_id, SWV_tree_node_name, SWV_tree_node_path, SWV_parent_node_id, SWV_proportion, SWV_plb_score, SWV_calculated);

      FETCH SWV_RCur INTO SWV_PLG_Mbr,SWV_PLG_ID,SWV_whse_code,SWV_name,SWV_item_attribute,SWV_importance,
      SWV_tree_id,SWV_tree_node_name,SWV_tree_node_path,SWV_parent_node_id,
      SWV_proportion,SWV_plb_score,SWV_calculated;
   END LOOP;
   CLOSE SWV_RCur;

END;

/
--------------------------------------------------------
--  DDL for Procedure SP_UPDATE_SUMMARYCHART1_TMP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_UPDATE_SUMMARYCHART1_TMP 
   as
   v_each_node_id  NUMBER(10,0);
   v_sqlQuery  NVARCHAR2(500);
   v_if_exists NUMBER(10,0);


   CURSOR summarychar2_cursor IS select node_id from tree_dtl  where
   tree_dtl.tree_id in(SELECT slot_list_tree.TREE_ID FROM
      slot_list_tree, whprefs  WHERE slot_list_tree.WHSE_CODE = whprefs.whse_code AND
      slot_list_tree.TREE_NAME = 'Warehouse Areas');
   SWV_color_id NUMBER(10,0);
   SWV_color VARCHAR2(15);
   SWV_color_count NUMBER(10,0);
   SWV_color_score NUMBER(10,0);
   SWV_node_id NUMBER(10,0);
   SWV_RCur SYS_REFCURSOR;
BEGIN
 SELECT   COUNT(*) INTO v_if_exists FROM user_tables WHERE table_name = 'summarychart1_sp_as_tmp';
   IF  v_if_exists > 0 then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE summarychart1_sp_as_tmp ';
   end if;



    --SQLWAYS_EVAL# bo].[summarychart1_sp_as_tmp]

   OPEN summarychar2_cursor;

   FETCH summarychar2_cursor INTO v_each_node_id;
   WHILE (summarychar2_cursor%FOUND) LOOP
    --in loop start
      usp_ScoreSummaryChart(v_each_node_id,SWV_RCur);
      FETCH SWV_RCur INTO SWV_color_id,SWV_color,SWV_color_count,SWV_color_score,SWV_node_id;
      WHILE SWV_RCur%FOUND  LOOP
         INSERT INTO summarychart1_sp_as_tmp  VALUES(SWV_color_id, SWV_color, SWV_color_count, SWV_color_score, SWV_node_id);

         FETCH SWV_RCur INTO SWV_color_id,SWV_color,SWV_color_count,SWV_color_score,SWV_node_id;
      END LOOP;
      CLOSE SWV_RCur;
    --in loop end
      FETCH summarychar2_cursor INTO v_each_node_id;
   END LOOP;
   CLOSE summarychar2_cursor;


END;

/
--------------------------------------------------------
--  DDL for Procedure SP_UPDATE_SUMMARYCHART2_TMP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_UPDATE_SUMMARYCHART2_TMP (v_whse_code IN CHAR) as
   v_if_exists NUMBER(10,0);
   SWV_color_id NUMBER(10,0);
   SWV_color VARCHAR2(15);
   SWV_color_rating VARCHAR2(20);
   SWV_color_count NUMBER(10,0);
   SWV_color_avg NUMBER(10,0);
   SWV_total_color_score NUMBER(10,0);
   SWV_RCur SYS_REFCURSOR;
BEGIN
   SELECT   COUNT(*) INTO v_if_exists FROM user_tables WHERE table_name = 'SUMMARYCHART2_SP_AS_TMP'; 
   IF  v_if_exists > 0 then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE summarychart2_sp_as_tmp ';
   end if;


   --SQLWAYS_EVAL#
   usp_ScoreSummaryChart2(v_whse_code,SWV_RCur);
   FETCH SWV_RCur INTO SWV_color_id,SWV_color,SWV_color_rating,SWV_color_count,SWV_color_avg,
   SWV_total_color_score;
   WHILE SWV_RCur%FOUND  LOOP
      INSERT INTO summarychart2_sp_as_tmp  VALUES(SWV_color_id, SWV_color, SWV_color_rating, SWV_color_count, SWV_color_avg, SWV_total_color_score);

      FETCH SWV_RCur INTO SWV_color_id,SWV_color,SWV_color_rating,SWV_color_count,SWV_color_avg,
      SWV_total_color_score;
   END LOOP;
   CLOSE SWV_RCur;
END;

/
--------------------------------------------------------
--  DDL for Procedure SP_UPDATE_TMP_MSGMSTR
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SP_UPDATE_TMP_MSGMSTR 
   as
   v_sqlstr1  VARCHAR2(35);
   v_if_exists NUMBER(10,0);
BEGIN

   SELECT   COUNT(*) INTO v_if_exists FROM user_tables WHERE table_name = 'message_master_tmp';
   IF  v_if_exists > 0 then
      EXECUTE IMMEDIATE ' TRUNCATE TABLE message_master_tmp ';
   end if;



   INSERT INTO message_master_tmp(msg_number, msg_txt_modified) SELECT MSG_NUMBER, MSG_TEXT FROM so_message_master;

    --SQLWAYS_EVAL# with the table
   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Misc 1';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Misc 1',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Misc 1%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Misc 2';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Misc 2',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Misc 2%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Commodity';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Commodity',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Commodity%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Crushability';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Crushability',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Crushability%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Hazard';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Hazard',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Hazard%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Vendor';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Vendor',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Vendor%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Family Group';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Family Group',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Family Group%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Misc 3';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Misc 3',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Misc 3%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Misc 4';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Misc 4',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Misc 4%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Misc 5';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Misc 5',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Misc 5%';

   begin
      select   rtrim(business_value) INTO v_sqlstr1 from system_code_dtl where code_value = 'Misc 6';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   update message_master_tmp set
   msg_txt_modified = replace(message_master_tmp.msg_txt_modified,'Misc 6',v_sqlstr1)
   where message_master_tmp.msg_txt_modified like '%Misc 6%';

END;

/
--------------------------------------------------------
--  DDL for Procedure SWP_ASSIGNOUTREFCUR4
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE SWP_ASSIGNOUTREFCUR4 (CurrentCursor IN OUT SYS_REFCURSOR, v_ref_cur IN OUT SYS_REFCURSOR, v_ref_cur2 IN OUT SYS_REFCURSOR, v_ref_cur3 IN OUT SYS_REFCURSOR, v_ref_cur4 IN OUT SYS_REFCURSOR) AS
InitCursor SYS_REFCURSOR;
BEGIN
 IF NOT v_ref_cur%IsOpen THEN
 v_ref_cur := CurrentCursor;
 ELSIF NOT v_ref_cur2%IsOpen THEN
 v_ref_cur2 := CurrentCursor;
 ELSIF NOT v_ref_cur3%IsOpen THEN
 v_ref_cur3 := CurrentCursor;
 ELSIF NOT v_ref_cur4%IsOpen THEN
 v_ref_cur4 := CurrentCursor;
 END IF;
 CurrentCursor:= InitCursor;
END;

/
--------------------------------------------------------
--  DDL for Procedure UPD_OUT_RES_TAB
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE UPD_OUT_RES_TAB (p_session_id IN OUTPUT_RESULT_TABLE.sessionid%type,p_process_id IN OUTPUT_RESULT_TABLE.processid%type,p_flag IN BOOLEAN)
IS
BEGIN

 IF p_flag THEN
   UPDATE output_result_table
     SET server_mod_date_time = SYSDATE
  WHERE sessionid = p_session_id AND processid = p_process_id;
 ELSE
   UPDATE output_result_table
    SET ui_mod_date_time = SYSDATE
    WHERE sessionid = p_session_id AND processid = p_process_id;
 END IF;

IF SQL%FOUND THEN
  DBMS_OUTPUT.PUT_LINE('Number of records updated : '||SQL%ROWCOUNT);
ELSIF SQL%NOTFOUND THEN
    DBMS_OUTPUT.PUT_LINE('ZERO RECORDS UPDATED ');
END IF;
-- COMMIT;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('ZERO RECORDS UPDATED');
   WHEN others THEN
     DBMS_OUTPUT.PUT_LINE('Error' || SQLCODE || SQLERRM );
END;

/
--------------------------------------------------------
--  DDL for Procedure USP_MOVESLISTROI
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE USP_MOVESLISTROI (v_refcur OUT SYS_REFCURSOR)
   as
   v_pvc_MoveID  NUMBER(10,0);
   v_pvc_ROIName  VARCHAR2(50);
   v_pvc_number_moves  NUMBER(19,0);
   v_pvc_BeginSlot  VARCHAR2(30);
   v_pvc_EndSlot  VARCHAR2(30);
   v_pvc_percent_pick_time  NUMBER;
   v_pvc_percent_travel_time  NUMBER;
   v_pvc_pick_path_length  NUMBER;
   v_pvc_nbr_people_in_ROI_area  NUMBER;
   v_pvc_nbr_people_in_whse  NUMBER;
   v_pvc_annual_person_cost  NUMBER;
   v_pvcpickingsavingspercentage  NUMBER;
   v_pvctravelsavingspercentage  NUMBER;
   v_pvcreductioninreplenishworkl  NUMBER;
   v_pvcreplenishsavingspermove  NUMBER;
   v_pvcreplenishsavingsperyearin  NUMBER;
   v_pvc_i  NUMBER(10,0);
   v_pvc_old_total  NUMBER;
   v_pvc_new_total  NUMBER;
   v_pvc_old_cum_sum  NUMBER;
   v_pvc_new_cum_sum  NUMBER;
   v_pvc_old_dummy  NUMBER;
   v_pvc_new_dummy  NUMBER;
   v_pvc_prev_old_value  NUMBER;
   v_pvc_prev_new_value  NUMBER;
   v_range_group_id  NUMBER(10,0);
   v_monetary_unit  NUMBER(10,0);
   v_currency  NVARCHAR2(1);
   v_fixed_travel_path  VARCHAR2(3);
   v_num_replenish_roi_area  NVARCHAR2(1);
   v_num_replenish_roi_zone  NVARCHAR2(1);
   v_annual_cost_replenish  NUMBER;
   v_calculate_picking  VARCHAR2(3);
   v_calculate_replenishing  VARCHAR2(3);
   v_pvcs_before_cost  NUMBER;
   v_pvcs_after_cost  NUMBER;
   v_pvcs_slotid  NUMBER(10,0);
   v_pvcs_old_sku_id  NUMBER(10,0);
   v_pvcs_new_sku_id  NUMBER(10,0);
   v_pvcs_slot_ht  NUMBER(9,2);
   v_pvc_slot_width  NUMBER(9,2);
   v_pvc_new_slot_width  NUMBER(9,2);
   v_pvc_slot_len  NUMBER(9,2);
   v_pvc_slot_vol  NUMBER(23,2);
   v_pvc_new_slot_vol  NUMBER(23,2);
   v_pvc_item_wt  NUMBER(9,2);
   v_pvc_item_len  NUMBER(9,2);
   v_pvc_item_ht  NUMBER(9,2);
   v_pvc_old_item_vol  NUMBER(23,2);
   v_pvc_old_movement  NUMBER(9,2);
   v_pvc_new_item_vol  NUMBER(23,2);
   v_pvc_new_movement  NUMBER(9,2);
   v_pvc_slot_unit  NUMBER(10,0);
   v_pvc_new_slot_unit  NUMBER(10,0);
   v_pvc_est_movement  NUMBER(9,2);
   v_pvc_calc_hits  NUMBER(9,2);
   v_pvc_old_est_movement  NUMBER(9,2);
   v_pvc_old_calc_hits  NUMBER(9,2);
   v_pvc_case_wid  NUMBER(9,2);
   v_pvc_case_len  NUMBER(9,2);
   v_pvc_case_ht  NUMBER(9,2);
   v_pvc_old_case_wid  NUMBER(9,2);
   v_pvc_old_case_len  NUMBER(9,2);
   v_pvc_old_case_ht  NUMBER(9,2);
   v_pvc_inn_wid  NUMBER(9,2);
   v_pvc_inn_len  NUMBER(9,2);
   v_pvc_inn_ht  NUMBER(9,2);
   v_pvc_old_inn_wid  NUMBER(9,2);
   v_pvc_old_inn_len  NUMBER(9,2);
   v_pvc_old_inn_ht  NUMBER(9,2);
   v_pvc_each_wid  NUMBER(9,2);
   v_pvc_each_len  NUMBER(9,2);
   v_pvc_each_ht  NUMBER(9,2);
   v_pvc_old_each_wid  NUMBER(9,2);
   v_pvc_old_each_len  NUMBER(9,2);
   v_pvc_old_each_ht  NUMBER(9,2);
   v_pvc_use_estimated_history  NUMBER(9,2);
   v_pvc_bin  NUMBER(10,0);
   v_pvc_pallet  NUMBER(10,0);
   v_pvc_bin_height  NUMBER(10,0);
   v_pvc_bin_width  NUMBER(10,0);
   v_pvc_bin_length  NUMBER(10,0);
   v_pvc_pallet_height  NUMBER(10,0);
   v_pvc_pallet_width  NUMBER(10,0);
   v_pvc_pallet_length  NUMBER(10,0);
   v_old_hits  NUMBER(9,2);
   v_old_est_hits  NUMBER(9,2);
   v_old_use_estimated  NUMBER(10,0);
   v_new_hits  NUMBER(9,2);
   v_new_est_hits  NUMBER(9,2);
   v_use_estimated  NUMBER(10,0);
  -- abc SYS_REFCURSOR;
   SWVtblHitsByLeveloldcostvar0 VARCHAR2(255);
   SWVtblHitsByBayoldtraveldistan VARCHAR2(255);
   
   
   CURSOR abc IS SELECT old_hits, old_est_hits,old_use_estimated_hist,new_hits, new_est_hits, use_estimated_hist
   FROM tt_TBLHITSCOMPARE FOR UPDATE;
   CURSOR abc1 IS SELECT distance
   FROM tt_TBLHITSBYBAY FOR UPDATE;
   CURSOR abc2 IS  SELECT old_perc, new_perc   FROM tt_TBLHITSBYBAY FOR UPDATE;
   CURSOR abc3 IS  SELECT old_sum_perc_power, new_sum_perc_power
   FROM tt_TBLHITSBYBAY FOR UPDATE;
   CURSOR abc4 IS  SELECT  slot_id, slot_height, slot_width,new_slot_width ,slot_length,
slot_unit,new_slot_unit, est_movement,calc_hits,old_est_movement,old_calc_hits,
case_wid,case_len,case_ht,old_case_wid, old_case_len,old_case_ht,inn_wid,
inn_len,inn_ht,old_inn_wid, old_inn_len,old_inn_ht,each_wid,each_len,
each_ht,old_each_wid,old_each_len,old_each_ht,use_estimated_history, current_bin, current_pallet,
bin_height, bin_length, bin_width, pallet_height, pallet_length, pallet_width
   FROM tt_ROI_REPLENISH FOR UPDATE;
   
   CURSOR abc5 IS  SELECT     slot_id,slot_vol, new_slot_vol, before_item_vol,after_item_vol,
    before_movement,after_movement
   FROM tt_ROI_REPLENISH_SAVING FOR UPDATE;
   
BEGIN






      select ParamValue INTO v_pvc_MoveID from ROI_Parameters where ParamID = 0;
     
  
      select ParamValue INTO v_pvc_ROIName from ROI_Parameters where ParamID = 1;
    
  
      select ParamValue INTO v_pvc_percent_pick_time from ROI_Parameters where ParamID = 2;
      
  
      select ParamValue INTO v_pvc_percent_travel_time from ROI_Parameters where ParamID = 3;
      
      select ParamValue INTO v_pvc_pick_path_length from ROI_Parameters where ParamID = 4;
     
   
      select ParamValue INTO v_pvc_nbr_people_in_ROI_area from ROI_Parameters where ParamID = 5;
    
  
      select ParamValue INTO v_pvc_nbr_people_in_whse from ROI_Parameters where ParamID = 6;
      

      select ParamValue INTO v_pvc_BeginSlot from ROI_Parameters where ParamID = 7;
   
      select ParamValue INTO v_pvc_annual_person_cost from ROI_Parameters where ParamID = 8;
  
      select ParamValue INTO v_range_group_id from ROI_Parameters where ParamID = 9;
   
      select ParamValue INTO v_monetary_unit from ROI_Parameters where ParamID = 10;
    
      select ParamValue INTO v_fixed_travel_path from ROI_Parameters where ParamID = 11;
 
      select ParamValue INTO v_num_replenish_roi_area from ROI_Parameters where ParamID = 12;
  
      select ParamValue INTO v_num_replenish_roi_zone from ROI_Parameters where ParamID = 13;
   
      select ParamValue INTO v_annual_cost_replenish from ROI_Parameters where ParamID = 14;
  
      select ParamValue INTO v_calculate_picking from ROI_Parameters where ParamID = 15;
  
      select ParamValue INTO v_calculate_replenishing from ROI_Parameters where ParamID = 16;
  



   sp_get_range_ids(v_range_group_id,'tt_RANGE_IDS_TABLE');


   INSERT INTO tt_TBLLEVEL_COSTS
   SELECT  rtl.level_number, rtl.rack_type, rack_level_id, cost_to_pick, rt_name
   from rack_type_level rtl inner join
   rack_type on rack_type.rack_type = rtl.rack_type
   where rtl.rack_level_id in(select rack_level_id from slot sl, tt_RANGE_IDS_TABLE where
      sl.my_range = range_id);


   UPDATE tt_TBLLEVEL_COSTS
   SET thelevel = thelevel+1;

   INSERT INTO tt_TBLUNPROCESSEDMOVES
   SELECT ml.src_slot_id,
     ml.dest_slot_id,
     ml.item_id,
     ml.slot_unit,
     ml.slot_width,
     ml.item,
      ml.slotitem_id
   from move_list ml
   where ml.processed = 0 and ml.move_hdr_id = v_pvc_MoveID and ml.select_move = 1;

   SELECT COUNT(*) INTO v_pvc_number_moves FROM tt_TBLUNPROCESSEDMOVES;


   INSERT INTO tt_TBLHITSCOMPARE
   SELECT sl.dsp_slot,
    sl.lvl,
    sl.bay,
    sl.slot_id,
    si.sku_id as old_sku_id,
    si.calc_hits as old_hits,
    si.est_hits as old_est_hits,
    si.use_estimated_hist as old_use_estimated_hist,
    si.sku_id as new_sku_id,
    si.calc_hits as new_hits,
    si.est_hits as new_est_hits,
    sl.lvl as slot_lvl,
    sl.rack_type as rack_type,
    sl.rack_level_id as rack_level_id,
    sl.dsp_slot as rack_type_desc,
    si.use_estimated_hist,
    si.use_estimated_hist as firt,
    si.use_estimated_hist as secon,
    si.use_estimated_hist as thir
   FROM(slot sl JOIN slot_item si
   ON sl.slot_id = si.slot_id
   left outer join rack_level_map rlm on
   sl.rack_type = rlm.rack_type and
   sl.rack_level_id = rlm.rack_level_id)
   WHERE sl.my_range in(select range_id from tt_RANGE_IDS_TABLE);


   UPDATE tt_TBLHITSCOMPARE hc_Upd SET(new_sku_id,new_hits,new_est_hits,firt,thir) =(SELECT  null,null,null,8,10 FROM(tt_TBLUNPROCESSEDMOVES um JOIN tt_TBLHITSCOMPARE hc
   ON um.src_slot_id = hc.slot_id) WHERE hc.ROWID = hc_Upd.ROWID) WHERE ROWID IN(SELECT hc.ROWID FROM(tt_TBLUNPROCESSEDMOVES um JOIN tt_TBLHITSCOMPARE hc
   ON um.src_slot_id = hc.slot_id));


   UPDATE tt_TBLHITSCOMPARE hc_Upd SET(new_sku_id,new_hits,new_est_hits,use_estimated_hist,secon,thir) =(SELECT  um.item_id,si.calc_hits,si.est_hits,si.use_estimated_hist,5,10 FROM(tt_TBLUNPROCESSEDMOVES um JOIN tt_TBLHITSCOMPARE hc
   ON um.dest_slot_id = hc.slot_id
   JOIN slot_item si ON si.slotitem_id = um.slotitem_id) WHERE hc.ROWID = hc_Upd.ROWID) WHERE ROWID IN(SELECT hc.ROWID FROM(tt_TBLUNPROCESSEDMOVES um JOIN tt_TBLHITSCOMPARE hc
   ON um.dest_slot_id = hc.slot_id
   JOIN slot_item si ON si.slotitem_id = um.slotitem_id));

   update tt_TBLHITSCOMPARE set old_hits = -999.99,old_est_hits = -999.99
   where old_sku_id is null and thir = 10;

   update tt_TBLHITSCOMPARE set new_hits = -999.99,new_est_hits = -999.99
   where new_sku_id is null and thir = 10;


   OPEN abc; 


   FETCH abc into v_old_hits,v_old_est_hits,v_old_use_estimated,v_new_hits,v_new_est_hits,
   v_use_estimated;
   WHILE abc%FOUND  LOOP
      if v_old_use_estimated = 1 then

         if v_old_est_hits < 0 then

            UPDATE tt_TBLHITSCOMPARE SET old_hits = v_old_hits
            WHERE CURRENT OF abc;
         else
            UPDATE tt_TBLHITSCOMPARE SET old_hits = v_old_est_hits
            WHERE CURRENT OF abc;
         end if;
      else
         if v_old_hits < 0 then

            UPDATE tt_TBLHITSCOMPARE SET old_hits = v_old_est_hits
            WHERE CURRENT OF abc;
         else
            UPDATE tt_TBLHITSCOMPARE SET old_hits = v_old_hits
            WHERE CURRENT OF abc;
         end if;
      end if;
      if v_use_estimated = 1 then

         if v_new_est_hits < 0 then

            UPDATE tt_TBLHITSCOMPARE SET new_hits = v_new_hits
            WHERE CURRENT OF abc;
         else
            UPDATE tt_TBLHITSCOMPARE SET new_hits = v_new_est_hits
            WHERE CURRENT OF abc;
         end if;
      else
         if v_new_hits < 0 then

            UPDATE tt_TBLHITSCOMPARE SET new_hits = v_new_est_hits
            WHERE CURRENT OF abc;
         else
            UPDATE tt_TBLHITSCOMPARE SET new_hits = v_new_hits
            WHERE CURRENT OF abc;
         end if;
      end if;
      FETCH abc into v_old_hits,v_old_est_hits,v_old_use_estimated,v_new_hits,v_new_est_hits,
      v_use_estimated;
   END LOOP;

   CLOSE abc;

   UPDATE tt_TBLHITSCOMPARE SET new_hits =(new_hits+abs(new_hits))/2,old_hits =(old_hits+abs(old_hits))/2;

   insert into tt_TBLRESULTS  Values(-8,'SQLWAYS_EVAL# moves:  ',v_pvc_number_moves);



   INSERT INTO tt_TBLHITSBYLEVEL(lvl,old_hits_by_level,new_hits_by_level,slot_lvl, rack_type, rack_level_id)
   SELECT     lvl, sum(old_hits) as old_hits_by_level, sum(new_hits) as new_hits_by_level, slot_lvl, rack_type, rack_level_id
   FROM tt_TBLHITSCOMPARE
   group by rack_type,rack_level_id,lvl,slot_lvl;

   UPDATE tt_TBLHITSBYLEVEL thbl_Upd SET(thecost,rack_type_desc,lvl) =(SELECT  tc.thecost,tc.theracktypedec,tc.thelevel FROM(tt_TBLHITSBYLEVEL thbl JOIN tt_TBLLEVEL_COSTS tc
   ON thbl.rack_type = tc.theracktype
   and thbl.rack_level_id = tc.theracklevel) WHERE thbl.ROWID = thbl_Upd.ROWID) WHERE ROWID IN(SELECT thbl.ROWID from(tt_TBLHITSBYLEVEL thbl JOIN tt_TBLLEVEL_COSTS tc
   ON thbl.rack_type = tc.theracktype
   and thbl.rack_level_id = tc.theracklevel));


   UPDATE tt_TBLHITSBYLEVEL SET old_cost = old_hits_by_level*thecost,new_cost = new_hits_by_level*thecost;


   begin
      select sum(old_cost) INTO SWVtblHitsByLeveloldcostvar0 from tt_TBLHITSBYLEVEL;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   if SWVtblHitsByLeveloldcostvar0 > 0 and v_calculate_picking = '1' and v_pvc_percent_pick_time > 0.0 then
      BEGIN
         select(sum(old_cost) -sum(new_cost))/sum(old_cost) INTO v_pvcpickingsavingspercentage from tt_TBLHITSBYLEVEL;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
   else
      v_pvcpickingsavingspercentage := 0;
   end if;
   insert into tt_TBLRESULTS  Values(-7,'SQLWAYS_EVAL# workload (%):  ',v_pvcpickingsavingspercentage);


   INSERT INTO tt_TBLHITSBYBAY(bay,rack_type, old_hits_by_bay,new_hits_by_bay)
   SELECT     bay, rack_type,sum(old_hits) as old_hits_by_bay, sum(new_hits) as new_hits_by_bay
   FROM tt_TBLHITSCOMPARE
   group by rack_type,bay
   order by rack_type NULLS FIRST;

   OPEN abc1; 

   v_pvc_i := 1;

   FETCH abc1 into v_pvc_old_dummy;
   WHILE abc1%FOUND  LOOP
      UPDATE tt_TBLHITSBYBAY SET distance = v_pvc_i
      WHERE CURRENT OF abc1;
      v_pvc_i := v_pvc_i+1;
      FETCH abc1 into v_pvc_old_dummy;
   END LOOP;

   CLOSE abc;

   begin
      select sum(old_hits_by_bay) INTO v_pvc_old_total from tt_TBLHITSBYBAY;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   begin
      select sum(new_hits_by_bay) INTO v_pvc_new_total from tt_TBLHITSBYBAY;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;

   UPDATE tt_TBLHITSBYBAY SET old_perc = old_hits_by_bay/(v_pvc_old_total+.00001),new_perc = new_hits_by_bay/(v_pvc_new_total+.00001);



   OPEN abc2; 
 

   v_pvc_old_cum_sum := 0;
   v_pvc_new_cum_sum := 0;

   FETCH abc2 into v_pvc_old_dummy,v_pvc_new_dummy;
   WHILE abc2%FOUND  LOOP
      v_pvc_old_cum_sum := v_pvc_old_cum_sum+v_pvc_old_dummy;
      v_pvc_new_cum_sum := v_pvc_new_cum_sum+v_pvc_new_dummy;
      UPDATE tt_TBLHITSBYBAY
      SET old_sum_perc = v_pvc_old_cum_sum,new_sum_perc = v_pvc_new_cum_sum
      WHERE CURRENT OF abc2;
      FETCH abc2 into v_pvc_old_dummy,v_pvc_new_dummy;
   END LOOP;

   CLOSE abc2;



   UPDATE tt_TBLHITSBYBAY SET old_sum_perc_power = power(old_sum_perc,v_pvc_pick_path_length),new_sum_perc_power = power(new_sum_perc,v_pvc_pick_path_length);




   OPEN abc3; 

   v_pvc_prev_old_value := 0;
   v_pvc_prev_new_value := 0;

   FETCH abc3 into v_pvc_old_dummy,v_pvc_new_dummy;

   WHILE abc3%FOUND  LOOP
      UPDATE tt_TBLHITSBYBAY
      SET old_unsummed = old_sum_perc_power -v_pvc_prev_old_value,new_unsummed = new_sum_perc_power -v_pvc_prev_new_value
      WHERE CURRENT OF abc3;
      v_pvc_prev_old_value := v_pvc_old_dummy;
      v_pvc_prev_new_value := v_pvc_new_dummy;
      FETCH abc3 into v_pvc_old_dummy,v_pvc_new_dummy;
   END LOOP;

   CLOSE abc3;

   UPDATE tt_TBLHITSBYBAY SET old_travel_distance = old_unsummed*distance,new_travel_distance = new_unsummed*distance;


   begin
      select sum(old_travel_distance) INTO SWVtblHitsByBayoldtraveldistan from tt_TBLHITSBYBAY;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
   end;
   if SWVtblHitsByBayoldtraveldistan > 0 then
      BEGIN
         select(sum(old_travel_distance) -sum(new_travel_distance))/sum(old_travel_distance+.000001) INTO v_pvctravelsavingspercentage from tt_TBLHITSBYBAY;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
   else
      v_pvctravelsavingspercentage := 0;
   end if;


   if v_fixed_travel_path = '1' and v_pvc_percent_travel_time > 0.0 then

      insert into tt_TBLRESULTS  Values(-6,'SQLWAYS_EVAL# workload (%):  ',v_pvctravelsavingspercentage);
   else
      insert into tt_TBLRESULTS  Values(-6,'SQLWAYS_EVAL# workload (%):  ',0);
   end if;


   if(v_monetary_unit) = 0 then

      v_currency := '$';
   else
      if(v_monetary_unit) = 1 then

         v_currency := 'C';
      else
         if(v_monetary_unit) = 2 then

            v_currency := '?';
         else
            if(v_monetary_unit) = 3 then

               v_currency := '-';
            end if;
         end if;
      end if;
   end if;

   insert into tt_ROI_REPLENISH
   SELECT
   sl.slot_id,
    sl.ht as slot_height,
    si.slot_width as slot_width,
    si.slot_width as new_slot_width,
    sl.depth as slot_length,
    si.sku_id as old_sku_id,
    si.sku_id as new_sku_id,
    si.slot_unit,
    si.slot_unit as new_slot_unit,
    si.est_movement,
    si.calc_hits,
    si.est_movement as old_est_movement,
    si.calc_hits as old_calc_hits,
    im.case_wid ,
    im.case_len,
    im.case_ht,
    im.case_wid as old_case_wid,
    im.case_len as old_case_len,
    im.case_ht as old_case_ht,
    im.inn_wid,
    im.inn_len,
    im.inn_ht,
    im.inn_wid as old_inn_wid,
    im.inn_len as old_inn_len ,
    im.inn_ht as old_inn_ht,
    im.each_wid,
    im.each_len,
    im.each_ht,
    im.each_wid as old_each_wid,
    im.each_len as old_each_len,
    im.each_ht as old_each_ht,
    si.use_estimated_hist as use_estimated_history,
    si.current_bin,
    si.current_pallet ,
    bins.height as bin_height,
    bins.width as bin_width,
    bins.length as bin_length,
    pallets.height as pallet_height,
    pallets.width as pallet_width,
    pallets.length as pallet_length
   FROM(slot sl JOIN slot_item si
   ON sl.slot_id = si.slot_id
   left join so_item_master im on si.sku_id = im.sku_id
   left outer join pallets on pallets.pallet_id = si.current_pallet
   left outer join bins on bins.bin_id = si.current_bin)
   WHERE sl.my_range in(select range_id from tt_RANGE_IDS_TABLE);


   UPDATE tt_ROI_REPLENISH roi_Upd SET(new_slot_width,new_sku_id,new_slot_unit,est_movement,calc_hits,case_wid,
   case_ht,case_len,inn_ht,inn_wid,inn_len,each_wid,each_ht,each_len) =(SELECT  null,null,null,null,null,null,null,null,null,null,null,null,null,null FROM(tt_TBLUNPROCESSEDMOVES um join tt_ROI_REPLENISH roi
   on um.item_id = roi.old_sku_id) WHERE roi.ROWID = roi_Upd.ROWID) WHERE ROWID IN(SELECT roi.ROWID FROM(tt_TBLUNPROCESSEDMOVES um join tt_ROI_REPLENISH roi
   on um.item_id = roi.old_sku_id));

   UPDATE tt_ROI_REPLENISH roi_Upd SET(new_slot_width,new_sku_id,new_slot_unit,est_movement,calc_hits,case_wid,
   case_ht,case_len,inn_ht,inn_wid,inn_len,each_wid,each_ht,each_len) =(SELECT  um.slot_width,um.item_id,um.slot_unit,si.est_movement,si.calc_hits,im.case_wid,im.case_ht,im.case_len,im.inn_ht,im.inn_wid,im.inn_len,im.each_wid,im.each_ht,im.each_len FROM  tt_TBLUNPROCESSEDMOVES um join tt_ROI_REPLENISH roi
   on um.dest_slot_id = roi.slot_id
   join slot_item si on si.sku_id = um.item_id
   join so_item_master im on si.sku_id = im.sku_id WHERE roi.ROWID = roi_Upd.ROWID) WHERE ROWID IN(SELECT roi.ROWID FROM tt_TBLUNPROCESSEDMOVES um join tt_ROI_REPLENISH roi
   on um.dest_slot_id = roi.slot_id
   join slot_item si on si.sku_id = um.item_id
   join so_item_master im on si.sku_id = im.sku_id);

   UPDATE tt_ROI_REPLENISH SET(new_slot_unit) =(SELECT distinct um.slot_unit FROM  tt_TBLUNPROCESSEDMOVES um where new_sku_id = um.item_id) WHERE ROWID IN(SELECT tt_ROI_REPLENISH.ROWID from tt_TBLUNPROCESSEDMOVES um where new_sku_id = um.item_id);


   delete from tt_ROI_REPLENISH where old_sku_id is null and new_sku_id is null;

   OPEN abc4; 

   v_pvc_item_wt := 0;
   v_pvc_item_len  := 0;
   v_pvc_item_ht := 0;
   v_pvc_old_item_vol := 0;
   v_pvc_old_movement := 0;
   v_pvc_new_item_vol := 0;
   v_pvc_new_movement := 0;
   v_pvcs_slot_ht := 0;
   v_pvc_slot_width := 0;
   v_pvc_new_slot_width := 0;
   v_pvc_slot_len := 0;
   v_pvc_slot_vol := 0;
   v_pvc_new_slot_vol := 0;
   v_pvc_slot_unit := 0;
   v_pvc_new_slot_unit := 0;
   v_pvc_est_movement := 0;
   v_pvc_calc_hits := 0;
   v_pvc_old_est_movement := 0;
   v_pvc_old_calc_hits := 0;
   v_pvc_case_wid := 0;
   v_pvc_case_len := 0;
   v_pvc_case_ht := 0;
   v_pvc_old_case_wid := 0;
   v_pvc_old_case_len := 0;
   v_pvc_old_case_ht := 0;
   v_pvc_inn_wid := 0;
   v_pvc_inn_len := 0;
   v_pvc_inn_ht := 0;
   v_pvc_old_inn_wid := 0;
   v_pvc_old_inn_len := 0;
   v_pvc_old_inn_ht := 0;
   v_pvc_each_wid := 0;
   v_pvc_each_len := 0;
   v_pvc_each_ht := 0;
   v_pvc_old_each_wid := 0;
   v_pvc_old_each_len := 0;
   v_pvc_old_each_ht := 0;
   v_pvc_use_estimated_history := 0;
   v_pvc_bin := 0;
   v_pvc_pallet := 0;
   v_pvc_bin_height := 0;
   v_pvc_bin_length := 0;
   v_pvc_bin_width := 0;
   v_pvc_pallet_height := 0;
   v_pvc_pallet_length := 0;
   v_pvc_pallet_width := 0;

   FETCH abc4 into v_pvcs_slotid,v_pvcs_slot_ht,v_pvc_slot_width,v_pvc_new_slot_width,v_pvc_slot_len,
   v_pvc_slot_unit,v_pvc_new_slot_unit,v_pvc_est_movement,v_pvc_calc_hits,
   v_pvc_old_est_movement,v_pvc_old_calc_hits,v_pvc_case_wid,
   v_pvc_case_len,v_pvc_case_ht,v_pvc_old_case_wid,v_pvc_old_case_len,v_pvc_old_case_ht,
   v_pvc_inn_wid,v_pvc_inn_len,v_pvc_inn_ht,v_pvc_old_inn_wid,
   v_pvc_old_inn_len,v_pvc_old_inn_ht,v_pvc_each_wid,v_pvc_each_len,
   v_pvc_each_ht,v_pvc_old_each_wid,v_pvc_old_each_len,v_pvc_old_each_ht,
   v_pvc_use_estimated_history,v_pvc_bin,v_pvc_pallet,v_pvc_bin_height,v_pvc_bin_length,
   v_pvc_bin_width,v_pvc_pallet_height,v_pvc_pallet_length,
   v_pvc_pallet_width;

   WHILE abc4%FOUND  LOOP
      v_pvc_slot_vol := 0;
      v_pvc_new_slot_vol := 0;
      v_pvc_slot_vol := v_pvcs_slot_ht*v_pvc_slot_width*v_pvc_slot_len;
      v_pvc_new_slot_vol := v_pvcs_slot_ht*v_pvc_new_slot_width*v_pvc_slot_len;
      v_pvc_old_movement := 0;
      v_pvc_new_movement := 0;
      v_pvc_old_item_vol := 0;
      v_pvc_new_item_vol := 0;
      if v_pvc_use_estimated_history = 1 then

         v_pvc_old_movement := v_pvc_old_est_movement;
         v_pvc_new_movement := v_pvc_est_movement;
      else
         v_pvc_old_movement := v_pvc_old_calc_hits;
         v_pvc_new_movement := v_pvc_calc_hits;
      end if;

    /*SQLWAYS_EVAL# vol*/
      if v_pvc_slot_unit = 1 then /*Pallet*/

         v_pvc_old_item_vol := v_pvc_pallet_height*v_pvc_pallet_length*v_pvc_pallet_width;
      end if;
      if v_pvc_slot_unit = 2 then /*Case*/

         v_pvc_old_item_vol := v_pvc_old_case_wid*v_pvc_old_case_len*v_pvc_old_case_ht;
      end if;
      if v_pvc_slot_unit = 4 then /*Inner*/

         v_pvc_old_item_vol := v_pvc_old_inn_wid*v_pvc_old_inn_len*v_pvc_old_inn_ht;
      end if;
      if v_pvc_slot_unit = 8 then /*Each*/

         v_pvc_old_item_vol := v_pvc_old_each_wid*v_pvc_old_each_len*v_pvc_old_each_ht;
      end if;
      if v_pvc_slot_unit = 16 then /*Bin*/

         v_pvc_old_item_vol := v_pvc_bin_height*v_pvc_bin_length*v_pvc_bin_width;
      end if;

    /*SQLWAYS_EVAL# ol*/
      if v_pvc_new_slot_unit = 1 then /*Pallet*/

         v_pvc_new_item_vol := v_pvc_pallet_height*v_pvc_pallet_length*v_pvc_pallet_width;
      end if;
      if v_pvc_new_slot_unit = 2 then /*Case*/

         v_pvc_new_item_vol := v_pvc_case_wid*v_pvc_case_len*v_pvc_case_ht;
      end if;
      if v_pvc_new_slot_unit = 4 then /*Inner*/

         v_pvc_new_item_vol := v_pvc_inn_wid*v_pvc_inn_len*v_pvc_inn_ht;
      end if;
      if v_pvc_new_slot_unit = 8 then /*Each*/

         v_pvc_new_item_vol := v_pvc_each_wid*v_pvc_each_len*v_pvc_each_ht;
      end if;
      if v_pvc_new_slot_unit = 16 then /*Bin*/

         v_pvc_new_item_vol := v_pvc_bin_height*v_pvc_bin_length*v_pvc_bin_width;
      end if;
      insert into tt_ROI_REPLENISH_SAVING(slot_id , slot_vol, new_slot_vol, before_movement, after_movement, before_item_vol, after_item_vol)
           values(v_pvcs_slotid, v_pvc_slot_vol, v_pvc_new_slot_vol,v_pvc_old_movement ,v_pvc_new_movement, v_pvc_old_item_vol,v_pvc_new_item_vol);


      FETCH abc4 into  v_pvcs_slotid,v_pvcs_slot_ht,v_pvc_slot_width,v_pvc_new_slot_width,v_pvc_slot_len,
      v_pvc_slot_unit,v_pvc_new_slot_unit,v_pvc_est_movement,v_pvc_calc_hits,
      v_pvc_old_est_movement,v_pvc_old_calc_hits,v_pvc_case_wid,
      v_pvc_case_len,v_pvc_case_ht,v_pvc_old_case_wid,v_pvc_old_case_len,v_pvc_old_case_ht,
      v_pvc_inn_wid,v_pvc_inn_len,v_pvc_inn_ht,v_pvc_old_inn_wid,
      v_pvc_old_inn_len,v_pvc_old_inn_ht,v_pvc_each_wid,v_pvc_each_len,
      v_pvc_each_ht,v_pvc_old_each_wid,v_pvc_old_each_len,v_pvc_old_each_ht,
      v_pvc_use_estimated_history,v_pvc_bin,v_pvc_pallet,v_pvc_bin_height,v_pvc_bin_length,
      v_pvc_bin_width,v_pvc_pallet_height,v_pvc_pallet_length,
      v_pvc_pallet_width;
   END LOOP;

   CLOSE abc4;

   OPEN abc5; 


   v_pvcs_before_cost := .00001;
   v_pvcs_after_cost := .00001;
   v_pvcs_slotid := .00001;
   v_pvc_slot_vol := .00001;
   v_pvc_new_slot_vol := .00001;
   v_pvc_old_item_vol := .00001;
   v_pvc_old_movement := .00001;
   v_pvc_new_item_vol := .00001;
   v_pvc_new_movement := .00001;

   FETCH abc5 into v_pvcs_slotid,v_pvc_slot_vol,v_pvc_new_slot_vol,v_pvc_old_item_vol,v_pvc_new_item_vol,
   v_pvc_old_movement,v_pvc_new_movement;

   WHILE abc5%FOUND  LOOP
      v_pvcs_before_cost :=(v_pvc_old_movement+.00001)/((v_pvc_slot_vol+.00001)/(v_pvc_old_item_vol+.00001));
      v_pvcs_after_cost :=(v_pvc_new_movement+.00001)/((v_pvc_new_slot_vol+.00001)/(v_pvc_new_item_vol+.00001));
      insert into tt_ROI_REPLENISH_COST(slot_id,before_replenish_cost,after_replenist_cost)
    values(v_pvcs_slotid,v_pvcs_before_cost,v_pvcs_after_cost);


      FETCH abc5 into v_pvcs_slotid,v_pvc_slot_vol,v_pvc_new_slot_vol,v_pvc_old_item_vol,v_pvc_new_item_vol,
      v_pvc_old_movement,v_pvc_new_movement;
   END LOOP;

   CLOSE abc5;

   if v_calculate_replenishing = '1' then
      begin
         select(sum(before_replenish_cost) -sum(after_replenist_cost))/sum(before_replenish_cost+.00001) INTO v_pvcreductioninreplenishworkl from tt_ROI_REPLENISH_COST;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      end;
   else
      v_pvcreductioninreplenishworkl := 0.0;
   end if;

   insert into tt_TBLRESULTS  Values(-5,'SQLWAYS_EVAL# workload (%):  ',v_pvcreductioninreplenishworkl);



   if v_fixed_travel_path = '1' and v_pvc_percent_travel_time > 0.0 then


      insert into tt_TBLRESULTS  Values(-4,'SQLWAYS_EVAL# (' || v_currency || '/yr/move):  ',
    (((v_pvctravelsavingspercentage*v_pvc_percent_travel_time/100+v_pvcpickingsavingspercentage*v_pvc_percent_pick_time/100)*v_pvc_nbr_people_in_ROI_area*v_pvc_annual_person_cost)+(v_pvcreductioninreplenishworkl*v_annual_cost_replenish*v_num_replenish_roi_area))/(v_pvc_number_moves+.0001));
   else

      insert into tt_TBLRESULTS  Values(-4,'SQLWAYS_EVAL# (' || v_currency || '/yr/move):  ',
    (((v_pvcpickingsavingspercentage*v_pvc_percent_pick_time/100)*v_pvc_nbr_people_in_ROI_area*v_pvc_annual_person_cost)+(v_pvcreductioninreplenishworkl*v_annual_cost_replenish*v_num_replenish_roi_area))/(v_pvc_number_moves+.0001));
   end if;

   if v_fixed_travel_path = '1' and v_pvc_percent_travel_time > 0.0 then

      insert into tt_TBLRESULTS  Values(-3,'SQLWAYS_EVAL# specified ROI area (' || v_currency || '/yr):  ',
    (v_pvctravelsavingspercentage*v_pvc_percent_travel_time/100+v_pvcpickingsavingspercentage*v_pvc_percent_pick_time/100)*v_pvc_nbr_people_in_ROI_area*v_pvc_annual_person_cost+v_pvcreductioninreplenishworkl*v_annual_cost_replenish*v_num_replenish_roi_area);
   else
      insert into tt_TBLRESULTS  Values(-3,'SQLWAYS_EVAL# specified ROI area (' || v_currency || '/yr):  ',
    (v_pvcpickingsavingspercentage*v_pvc_percent_pick_time/100)*v_pvc_nbr_people_in_ROI_area*v_pvc_annual_person_cost+v_pvcreductioninreplenishworkl*v_annual_cost_replenish*v_num_replenish_roi_area);
   end if;


   if v_fixed_travel_path = '1' and v_pvc_percent_travel_time > 0.0 then


      insert into tt_TBLRESULTS  Values(-1,'SQLWAYS_EVAL# DC/Zone-wide savings (' || v_currency || '/yr):  ',(v_pvctravelsavingspercentage*v_pvc_percent_travel_time/100+v_pvcpickingsavingspercentage*v_pvc_percent_pick_time/100)*v_pvc_nbr_people_in_whse*v_pvc_annual_person_cost+v_pvcreductioninreplenishworkl*v_annual_cost_replenish*v_num_replenish_roi_zone);
   else

    
      insert into tt_TBLRESULTS  Values(-1,'SQLWAYS_EVAL# DC/Zone-wide savings (' || v_currency || '/yr):  ',(v_pvcpickingsavingspercentage*v_pvc_percent_pick_time/100)*v_pvc_nbr_people_in_whse*v_pvc_annual_person_cost+v_pvcreductioninreplenishworkl*v_annual_cost_replenish*v_num_replenish_roi_zone);
   end if;





   Insert into tt_TBLOVERALLRESULTS(ROI_area_name) Values(v_pvc_ROIName);

   Insert into tt_TBLOVERALLRESULTS(field_id, field_label,field_value)
   select field_id, field_label,field_value
   from tt_TBLRESULTS
   order by field_id NULLS FIRST;

   Insert into tt_TBLOVERALLRESULTS(lvl,rack_type, rack_level, rack_type_desc, cost_to_pick, old_hits_by_level,new_hits_by_level,bay,bay_rack_type,old_hits_by_bay, new_hits_by_bay)
   select slot_lvl,k.rack_type, rack_level_id, rack_type_desc,
thecost,old_hits_by_level, new_hits_by_level,bay, l.rack_type,old_hits_by_bay, new_hits_by_bay
   from tt_TBLHITSBYLEVEL k,tt_TBLHITSBYBAY l
   order by lvl NULLS FIRST;

   Insert into tt_TBLOVERALLRESULTS(bay,bay_rack_type,old_hits_by_bay, new_hits_by_bay)
   select bay, rack_type,old_hits_by_bay, new_hits_by_bay
   from tt_TBLHITSBYBAY
   order by bay NULLS FIRST;


   open v_refcur for Select * from tt_TBLOVERALLRESULTS;
   EXECUTE IMMEDIATE ' TRUNCATE TABLE tt_RANGE_IDS_TABLE ';
end;

/
--------------------------------------------------------
--  DDL for Procedure USP_SCORESUMMARYCHART
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE USP_SCORESUMMARYCHART (v_prmNodeId IN NUMBER, v_refcur OUT SYS_REFCURSOR)
   as
   v_strColor1  VARCHAR2(15);
   v_strColor2  VARCHAR2(15);
   v_strColor3  VARCHAR2(15);
   v_strColor4  VARCHAR2(15);
   v_strColor5  VARCHAR2(15);
   v_intItemCount1  NUMBER(10,0);
   v_intItemCount2  NUMBER(10,0);
   v_intItemCount3  NUMBER(10,0);
   v_intItemCount4  NUMBER(10,0);
   v_intItemCount5  NUMBER(10,0);
   v_intItemColorScore  NUMBER(10,0);
   v_intNodeID  NUMBER(10,0);
   v_intSkuID  NUMBER(10,0);
   v_numScore  NUMBER(10,4);
   v_intCursFetchStat  NUMBER(10,0);
   FETCH_STATUS NUMBER(10,0);





/*item count vars*/







/*record set vars*/




/*initialize vars*/


   CURSOR curs_SlottedItems IS SELECT R.node_id, SI.sku_id, SI.score
   FROM RANGE1 R join slot S on R.node_id = S.my_range
   join slot_item SI ON S.slot_id = SI.slot_id
   WHERE R.node_id = v_prmNodeId
   AND SI.sku_id > 0
   AND SI.slot_id > 0
   AND(R.p_name IS NOT NULL OR
   R.a_name IS NOT NULL OR
   R.b_name IS NOT NULL OR
   R.c_name IS NOT NULL OR
   R.d_name IS NOT NULL OR
   R.e_name IS NOT NULL OR
   R.f_name IS NOT NULL OR
   R.g_name IS NOT NULL);
BEGIN


/*SQLWAYS_EVAL# items*/


/*color vars*/
   v_intItemCount1 := 0;
   v_intItemCount2 := 0;
   v_intItemCount3 := 0;
   v_intItemCount4 := 0;
   v_intItemCount5 := 0;
   v_intItemColorScore := 0;

   v_strColor1 := 'DARK GREEN';
   v_strColor2 := 'LIGHT GREEN';
   v_strColor3 := 'YELLOW';
   v_strColor4 := 'ORANGE';
   v_strColor5 := 'RED';

/*SQLWAYS_EVAL# table*/
   BEGIN
      INSERT INTO tt_TBLOUTPUT  Select 1, v_strColor1, 0,0,v_prmNodeId  from dual;
      INSERT INTO tt_TBLOUTPUT  Select 2, v_strColor2, 0,0,v_prmNodeId  from dual;
      INSERT INTO tt_TBLOUTPUT  Select 3, v_strColor3, 0,0,v_prmNodeId  from dual;
      INSERT INTO tt_TBLOUTPUT  Select 4, v_strColor4, 0,0,v_prmNodeId  from dual;
      INSERT INTO tt_TBLOUTPUT  Select 5, v_strColor5, 0,0,v_prmNodeId  from dual;
   END;

/*SQLWAYS_EVAL# vars*/
   OPEN curs_SlottedItems;

   FETCH curs_SlottedItems INTO v_intNodeID,v_intSkuID,v_numScore;

   CASE curs_SlottedItems%FOUND
   WHEN TRUE THEN
      FETCH_STATUS := 0;
   ELSE
      FETCH_STATUS := -1;
   END CASE;
   v_intCursFetchStat := FETCH_STATUS;

   WHILE v_intCursFetchStat = 0 LOOP
      IF v_numScore = 0 then
         UPDATE tt_TBLOUTPUT Set color_count = color_count+1,color_score = color_score+v_numScore WHERE color_id = 1;
      end if;
      IF v_numScore > 0 AND v_numScore <= 25 then
         UPDATE tt_TBLOUTPUT Set color_count = color_count+1,color_score = color_score+v_numScore WHERE color_id = 2;
      end if;
      IF v_numScore > 25 AND v_numScore <= 50 then
         UPDATE tt_TBLOUTPUT Set color_count = color_count+1,color_score = color_score+v_numScore  WHERE color_id = 3;
      end if;
      IF v_numScore > 50 AND v_numScore <= 75 then
         UPDATE tt_TBLOUTPUT Set color_count = color_count+1,color_score = color_score+v_numScore  WHERE color_id = 4;
      end if;
      IF v_numScore > 75 then
         UPDATE tt_TBLOUTPUT Set color_count = color_count+1,color_score = color_score+v_numScore WHERE color_id = 5;
      end if;
      FETCH curs_SlottedItems INTO v_intNodeID,v_intSkuID,v_numScore;
      CASE curs_SlottedItems%FOUND
      WHEN TRUE THEN
         FETCH_STATUS := 0;
      ELSE
         FETCH_STATUS := -1;
      END CASE;
      v_intCursFetchStat := FETCH_STATUS;
   END LOOP;

   CLOSE curs_SlottedItems;


/*SQLWAYS_EVAL# rt*/
   open v_refcur for SELECT OP.color_id, OP.color, OP.color_count, OP.color_score,v_prmNodeId
   FROM tt_TBLOUTPUT OP WHERE OP.color = v_strColor1
   UNION
   SELECT OP.color_id, OP.color, OP.color_count,OP.color_score,v_prmNodeId
   FROM tt_TBLOUTPUT OP WHERE OP.color = v_strColor2
   UNION
   SELECT OP.color_id, OP.color, OP.color_count,OP.color_score,v_prmNodeId
   FROM tt_TBLOUTPUT OP WHERE OP.color = v_strColor3
   UNION
   SELECT OP.color_id, OP.color, OP.color_count,OP.color_score,v_prmNodeId
   FROM tt_TBLOUTPUT OP WHERE OP.color = v_strColor4
   UNION
   SELECT OP.color_id, OP.color, OP.color_count,OP.color_score,v_prmNodeId
   FROM tt_TBLOUTPUT OP WHERE OP.color = v_strColor5
   ORDER BY 1 NULLS FIRST;


END;

/
--------------------------------------------------------
--  DDL for Procedure USP_SCORESUMMARYCHART2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE USP_SCORESUMMARYCHART2 (v_WHSECODE IN VARCHAR2, v_refcur OUT SYS_REFCURSOR)
   as
   v_strColor1  VARCHAR2(15);
   v_strColor2  VARCHAR2(15);
   v_strColor3  VARCHAR2(15);
   v_strColor4  VARCHAR2(15);
   v_strColor5  VARCHAR2(15);
   v_strColor1Rate  VARCHAR2(15);
   v_strColor2Rate  VARCHAR2(15);
   v_strColor3Rate  VARCHAR2(15);
   v_strColor4Rate  VARCHAR2(15);
   v_strColor5Rate  VARCHAR2(15);
   v_intItemCount1  NUMBER(10,0);
   v_intItemCount2  NUMBER(10,0);
   v_intItemCount3  NUMBER(10,0);
   v_intItemCount4  NUMBER(10,0);
   v_intItemCount5  NUMBER(10,0);
   v_intColorCount  NUMBER(10,0);
   v_intSkuID  NUMBER(19,0);
   v_numScore  NUMBER(13,4);
   v_total  NUMBER(13,4);
   v_total2  NUMBER(13,4);
   v_total3  NUMBER(13,4);
   v_total4  NUMBER(13,4);
   v_intCursFetchStat  NUMBER(10,0);
   FETCH_STATUS NUMBER(10,0);











/*item count vars*/







--record set vars
--SQLWAYS_EVAL# VARCHAR(3)







/*initialize vars*/


   CURSOR curs_SlottedItems IS SELECT  si.sku_id, si.score
   FROM slot_item si INNER JOIN so_item_master im ON
   si.sku_id = im.sku_id
   INNER JOIN slot sl ON
   si.slot_id = sl.slot_id
   WHERE     si.slot_id <> 0 AND si.sku_id <> 0 and
   si.slot_id is not NULL AND si.sku_id is not NULL and
   sl.my_range is not NULL and
   im.whse_code = v_WHSECODE;
BEGIN


/*SQLWAYS_EVAL# items*/


/*color vars*/
   v_intItemCount1 := 0;
   v_intItemCount2 := 0;
   v_intItemCount3 := 0;
   v_intItemCount4 := 0;
   v_intItemCount5 := 0;
   v_intColorCount := 0;

   v_strColor1 := 'Dark Green';
   v_strColor2 := 'Light Green';
   v_strColor3 := 'Yellow';
   v_strColor4 := 'Orange';
   v_strColor5 := 'Red';


   v_strColor1Rate := 'Best';
   v_strColor2Rate := 'Better';
   v_strColor3Rate := 'Good';
   v_strColor4Rate := 'Fair';
   v_strColor5Rate := 'Poor';
/*SQLWAYS_EVAL# table*/
   BEGIN
      INSERT INTO tt_TBLOUTPUT2  Select 1, v_strColor1,v_strColor1Rate,0,0,0  from dual;
      INSERT INTO tt_TBLOUTPUT2  Select 2, v_strColor2,v_strColor2Rate,0,0,0  from dual;
      INSERT INTO tt_TBLOUTPUT2  Select 3, v_strColor3,v_strColor3Rate,0,0,0  from dual;
      INSERT INTO tt_TBLOUTPUT2  Select 4, v_strColor4,v_strColor4Rate,0,0,0  from dual;
      INSERT INTO tt_TBLOUTPUT2  Select 5, v_strColor5,v_strColor5Rate,0,0,0  from dual;
   END;

/*SQLWAYS_EVAL# vars*/
   OPEN curs_SlottedItems;

   FETCH curs_SlottedItems INTO v_intSkuID,v_numScore;

   CASE curs_SlottedItems%FOUND
   WHEN TRUE THEN
      FETCH_STATUS := 0;
   ELSE
      FETCH_STATUS := -1;
   END CASE;
   v_intCursFetchStat := FETCH_STATUS;
   v_total := 0;
   v_total2 := 0;
   v_total3 := 0;
   v_total4 := 0;
   WHILE v_intCursFetchStat = 0 LOOP
      IF v_numScore = 0 then
         UPDATE tt_TBLOUTPUT2 Set color_count = color_count+1,color_avg = color_avg+v_numScore,total_color_score = total_color_score+v_numScore  WHERE color_id = 1;
      end if;
      IF v_numScore > 0 AND v_numScore <= 25 then

         v_total := v_total+v_numScore;
         UPDATE tt_TBLOUTPUT2 Set color_count = color_count+1,color_avg = color_avg+v_numScore,total_color_score = v_total WHERE color_id = 2;
      end if;
      IF v_numScore > 25 AND v_numScore <= 50 then

         v_total2 := v_total2+v_numScore;
         UPDATE tt_TBLOUTPUT2 Set color_count = color_count+1,color_avg = color_avg+v_numScore,total_color_score = v_total2 WHERE color_id = 3;
      end if;
      IF v_numScore > 50 AND v_numScore <= 75 then

         v_total3 := v_total3+v_numScore;
         UPDATE tt_TBLOUTPUT2 Set color_count = color_count+1,color_avg = color_avg+v_numScore,total_color_score = v_total3 WHERE color_id = 4;
      end if;
      IF v_numScore > 75 then

         v_total4 := v_total4+v_numScore;
         UPDATE tt_TBLOUTPUT2 Set color_count = color_count+1,color_avg = color_avg+v_numScore,total_color_score = v_total4 WHERE color_id = 5;
      end if;
      FETCH curs_SlottedItems INTO v_intSkuID,v_numScore;
      CASE curs_SlottedItems%FOUND
      WHEN TRUE THEN
         FETCH_STATUS := 0;
      ELSE
         FETCH_STATUS := -1;
      END CASE;
      v_intCursFetchStat := FETCH_STATUS;
   END LOOP;

   CLOSE curs_SlottedItems;


/*SQLWAYS_EVAL# rt*/
   open v_refcur for SELECT OP.color_id, OP.color, OP.color_rating, OP.color_count,OP.color_avg,OP.total_color_score
   FROM tt_TBLOUTPUT2 OP WHERE OP.color = v_strColor1
   UNION
   SELECT OP.color_id, OP.color, OP.color_rating, OP.color_count,OP.color_avg, OP.total_color_score
   FROM tt_TBLOUTPUT2 OP WHERE OP.color = v_strColor2
   UNION
   SELECT OP.color_id, OP.color, OP.color_rating, OP.color_count,OP.color_avg, OP.total_color_score
   FROM tt_TBLOUTPUT2 OP WHERE OP.color = v_strColor3
   UNION
   SELECT OP.color_id, OP.color, OP.color_rating, OP.color_count,OP.color_avg, OP.total_color_score
   FROM tt_TBLOUTPUT2 OP WHERE OP.color = v_strColor4
   UNION
   SELECT OP.color_id, OP.color, OP.color_rating, OP.color_count,OP.color_avg,OP.total_color_score
   FROM tt_TBLOUTPUT2 OP WHERE OP.color = v_strColor5
   ORDER BY 1 NULLS FIRST;


END;

/
--------------------------------------------------------
--  DDL for Procedure USP_UPDATE_ROI_PARAMETERS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE USP_UPDATE_ROI_PARAMETERS (v_Move_ID IN VARCHAR2,
v_Nameofrangeoverwhichtoperfor IN VARCHAR2,
v_Percentageoftimepickersspend IN VARCHAR2,
v_Percentageoftimepickersspen2 IN VARCHAR2,
v_Averagenumberofhitsperpickpa IN VARCHAR2,
v_NumberofpickersstaffedinROIa IN VARCHAR2,
v_NumberofpickersstaffedinDCor IN VARCHAR2,
v_First_Slot_In_Range IN VARCHAR2,
v_Fullyloadedannualcostofapick IN VARCHAR2,
v_range_group_id IN NUMBER,
v_monetary_unit IN NUMBER,
v_fixedtravelpath IN VARCHAR2,
v_num_of_replenisher_in_ROI IN VARCHAR2,
v_num_of_replenish_in_zone IN VARCHAR2,
v_annual_cost_of_replenisher IN VARCHAR2,
v_calculate_picking IN VARCHAR2,
v_calculate_replenishing IN VARCHAR2, v_refcur OUT SYS_REFCURSOR)
   as
   v_pvcSQLStmt  VARCHAR2(2500);
   v_if_exists NUMBER(10,0);
   SWV_SYSOBJECTS__var0 NUMBER(10,0);

BEGIN

   SELECT   COUNT(*) INTO v_if_exists FROM user_tables WHERE table_name = 'ROI_Parameters';
   IF v_if_exists > 0 then

      EXECUTE IMMEDIATE ' TRUNCATE TABLE ROI_Parameters ';
   end if;


 SELECT   COUNT(*) INTO v_if_exists FROM user_tables WHERE table_name = 'ROI_Parameters';
   IF SWV_SYSOBJECTS__var0 = 0 then


      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(0, 'Move ID', '');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(1, 'SQLWAYS_EVAL# which to perform ROI', '');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(2, 'SQLWAYS_EVAL# pickers spend picking', '50');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(3, 'SQLWAYS_EVAL# pickers spend traveling', '50');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(4, 'SQLWAYS_EVAL# of hits per pickpath', '5');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(5, 'SQLWAYS_EVAL# staffed in ROI area (FTE'')', '1');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(6, 'SQLWAYS_EVAL# staffed in DC/Zone (FTE''s)', '35');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(7, 'SQLWAYS_EVAL# range', 'aaaaaaa');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(8, 'SQLWAYS_EVAL# cost of a picker', '28000.00');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(9, 'Target RangeID', '1');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(10, 'Monetary Unit', '0');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(11, 'SQLWAYS_EVAL# ath', 'aaaaaaa');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(12, 'SQLWAYS_EVAL# staffed in ROI area (FTE''s)', '2');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(13, 'SQLWAYS_EVAL# in staffed DC/Zone area (FTE''s)', '2');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(14, 'SQLWAYS_EVAL# of a replenisher', '2000');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(15, 'SQLWAYS_EVAL# ing', 'Yes');

      INSERT INTO ROI_Parameters(ParamID, ParamName, ParamValue)
       VALUES(16, 'SQLWAYS_EVAL# enishing', 'Yes');
   end if;
   IF  LENGTH(LTRIM(v_Move_ID)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_Move_ID
      WHERE ParamID = 0;
   end if;

   IF  LENGTH(LTRIM(v_Nameofrangeoverwhichtoperfor)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_Nameofrangeoverwhichtoperfor
      WHERE ParamID = 1;
   end if;

   IF  LENGTH(LTRIM(v_Percentageoftimepickersspend)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_Percentageoftimepickersspend
      WHERE ParamID = 2;
   end if;

   IF LENGTH(LTRIM(v_Percentageoftimepickersspen2)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_Percentageoftimepickersspen2
      WHERE ParamID = 3;
   end if;

   IF LENGTH(LTRIM(v_Averagenumberofhitsperpickpa)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_Averagenumberofhitsperpickpa
      WHERE ParamID = 4;
   end if;
   IF LENGTH(LTRIM(v_NumberofpickersstaffedinROIa)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_NumberofpickersstaffedinROIa
      WHERE ParamID = 5;
   end if;

   IF LENGTH(LTRIM(v_NumberofpickersstaffedinDCor)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_NumberofpickersstaffedinDCor
      WHERE ParamID = 6;
   end if;

   IF LENGTH(LTRIM(v_First_Slot_In_Range)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_First_Slot_In_Range
      WHERE ParamID = 7;
   end if;

   IF LENGTH(LTRIM(v_Fullyloadedannualcostofapick)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_Fullyloadedannualcostofapick
      WHERE ParamID = 8;
   end if;


   IF LENGTH(LTRIM(v_range_group_id)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_range_group_id
      WHERE ParamID = 9;
   end if;

   IF LENGTH(LTRIM(v_monetary_unit)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_monetary_unit
      WHERE ParamID = 10;
   end if;

   IF LENGTH(LTRIM(v_fixedtravelpath)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_fixedtravelpath
      WHERE ParamID = 11;
   end if;

   IF LENGTH(LTRIM(v_num_of_replenisher_in_ROI)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_num_of_replenisher_in_ROI
      WHERE ParamID = 12;
   end if;


   IF LENGTH(LTRIM(v_num_of_replenish_in_zone)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_num_of_replenish_in_zone
      WHERE ParamID = 13;
   end if;

   IF LENGTH(LTRIM(v_annual_cost_of_replenisher)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_annual_cost_of_replenisher
      WHERE ParamID = 14;
   end if;


   IF LENGTH(LTRIM(v_calculate_picking)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_calculate_picking
      WHERE ParamID = 15;
   end if;

   IF LENGTH(LTRIM(v_calculate_replenishing)) > 0 then

      UPDATE ROI_Parameters
      SET ParamValue = v_calculate_replenishing
      WHERE ParamID = 16;
   end if;


   open v_refcur for Select * from ROI_Parameters;
END;

/

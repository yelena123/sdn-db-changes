SET DEFINE OFF;
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (1, 'TXN_FILTERS_ITEM_SLOT', 'FILTERS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (2, 'TXN_CONFIGURE_RACK_TYPES', 'CONFIGURE', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (3, 'TXN_CONFIGURE_BIN_TYPES', 'CONFIGURE', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (4, 'TXN_CONFIGURE_PALLET_TYPES', 'CONFIGURE', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (5, 'TXN_CONFIGURE_SEASONAL', 'CONFIGURE', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (6, 'TXN_CONFIGURE_PL_GROUPS', 'CONFIGURE', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (7, 'TXN_CONFIGURE_LIS_GROUPS', 'CONFIGURE', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (8, 'TXN_CONFIGURE_KIT_GROUPS', 'CONFIGURE', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (9, 'TXN_CONFIGURE_SNS', 'CONFIGURE', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (10, 'TXN_CAT_FG', 'CAT', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (17, 'TXN_RANGE', 'RANGE_RULES', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (18, 'TXN_RULES', 'RANGE_RULES', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (19, 'TXN_TOOLS_RECALC_HIST', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (20, 'TXN_TOOLS_SET_LAB_LOC', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (21, 'TXN_TOOLS_SET_SLOT_WID', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (22, 'TXN_TOOLS_SET_LR_PTR', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (23, 'TXN_TOOLS_LEFT_JUST', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (24, 'TXN_TOOLS_CLR_EXP', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (25, 'TXN_TOOLS_PURGE_HIST', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (26, 'TXN_IMPORT_ACCESS', 'IMPORTING', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (27, 'TXN_EXPORT_SLOTS', 'EXPORTING', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (28, 'TXN_SLOTTING_SINGLE', 'SLOTTING', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (29, 'TXN_ISGRID_RESLOT', 'SLOTTING', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (30, 'TXN_SLOTTING_MAX', 'SLOTTING', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (31, 'TXN_SLOTTING_BATCH', 'SLOTTING', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (32, 'TXN_PREF_IMPORT', 'PREFS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (33, 'TXN_PREF_SLOTTING', 'PREFS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (34, 'TXN_PREF_HISTORY', 'PREFS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (35, 'TXN_ADMIN', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (36, 'TXN_ADMIN_PROFILE', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (37, 'TXN_ADMIN_USER_MASTER', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (38, 'TXN_ADMIN_USER_MAINTAIN', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (39, 'TXN_ADMIN_WHSE_MAINTAIN', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (40, 'TXN_ADMIN_IMPORT_FORMAT', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (41, 'TXN_ADMIN_AUTO', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (42, 'TXN_ADMIN_SYSCODE', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (43, 'TXN_ML_PRINT', 'MOVES', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (44, 'TXN_ML_EXPORT', 'MOVES', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (45, 'TXN_ML_CLEAR', 'MOVES', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (46, 'TXN_FILTER', 'FILTER', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (47, 'TXN_TOOLS_RECALC_SLOT_LAYOUT', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (48, 'TXN_CBA_REPORT', 'METRICS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (49, 'TXN_TOOLS_RANK_ITEMS', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (54, 'TXN_TOOLS_CALC_3D', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (55, 'TXN_ADMIN_CUSTOM_SCRIPTS', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (56, 'TXN_ADMIN_COLOR_CODING', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (57, 'TXN_LM_ORDER_WA_RESULTS', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (58, 'TXN_LM_CONTAINER_TYPES', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (59, 'TXN_LM_PICK_ZONES', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (60, 'TXN_LM_XYZ_VALUES', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (61, 'TXN_LM_EXPORT_SLOTS_TO_LM', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (62, 'TXN_SLOTTING_CONSECUTIVE', 'SLOTTING', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (63, 'TXN_ADMIN_MESSAGE_MASTER', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (64, 'TXN_TOOLS_CREATE_SLOTS', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (65, 'TXN_ADMIN_SERVER_MAINTAIN', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (66, 'TXN_AUTOMATION_MOVES', 'SLOTTING', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (67, 'TXN_MOVE_LIST', 'ADMIN', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (68, 'TXN_ML_VIEW', 'MOVES', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (69, 'TXN_SQL_SCRIPTS', 'IMPORTING', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (70, 'TXN_PURGE_MOVESCOMP_RECDS', 'TOOLS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (71, 'TXN_AISLE_BUILDER_WIZARD', 'ANALYSIS', 'admin');
Insert into TXN_MASTER
   (TXN_ID, NAME, GROUP_NAME, MOD_USER)
 Values
   (72, 'TXN_SCENARIO_MANAGEMENT', 'ANALYSIS', 'admin');
COMMIT;

SET DEFINE OFF;
Insert into CNSTR_GRP
   (CNSTR_GRP_ID, WHSE_CODE, CNSTR_GRP_NAME, MOD_USER)
 Values
   (1, '*df', 'Warehouse constraints', 'admin');
Insert into CNSTR_GRP
   (CNSTR_GRP_ID, WHSE_CODE, CNSTR_GRP_NAME, MOD_USER)
 Values
   (2, '*df', 'Warehouse goals', 'admin');
Insert into CNSTR_GRP
   (CNSTR_GRP_ID, WHSE_CODE, CNSTR_GRP_NAME, MOD_USER)
 Values
   (3, '*df', 'Warehouse sequences', 'admin');
COMMIT;

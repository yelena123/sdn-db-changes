SET DEFINE OFF;
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (85, 10, '*df', 'Active', 'A', 
    'Active item', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (86, 10, '*df', 'Discontinued', 'D', 
    'Discontinued item', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (87, 10, '*df', 'New', 'N', 
    'New item', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (88, 10, '*df', 'OptPltPatternPrimary', 'P', 
    'Optimize Pallet Pattern - Primary', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (89, 10, '*df', 'OptPltPatternSecondary', 'S', 
    'Optimize Pallet Pattern - Secondary', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (90, 10, '*df', 'OptPltPatternBoth', 'B', 
    'Both', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (91, 10, '*df', 'True', 'T', 
    'True', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (92, 10, '*df', 'False', 'F', 
    'False', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (93, 10, '*df', 'HLW', 'HLW', 
    'Orientation - Height\Length\Width', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (94, 10, '*df', 'HWL', 'HWL', 
    'Orientation - Height\Width\Length', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (95, 10, '*df', 'LHW', 'LHW', 
    'Orientation - Length\Height\Width', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (96, 10, '*df', 'LWH', 'LWH', 
    'Orientation - Length\Width\Height', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (97, 10, '*df', 'WHL', 'WHL', 
    'Orientation - Width\Height\Length', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (98, 10, '*df', 'WLH', 'WLH', 
    'Orientation - Width\Length\Height', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (99, 11, '*df', 'CurrentLanguage', 'ENU', 
    'Current language', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (100, 12, '*df', 'DateFormat', 'mm/dd/yyyy', 
    'Date Format', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (101, 4, '*df', '502', 'Other:  Create Work Assignments', 
    'Other:  Create Work Assignments', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (102, 4, '*df', '601', 'Other:  Export Slots to LM', 
    'LM: Slots export to LM', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (103, 4, '*df', '600', 'Other:  Work Assignments to LM', 
    'LM: WA export to LM', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (104, 6, '*df', 'Extra Field', 'Extra field', 
    'Replacement text for Extra Field', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (105, 6, '*df', 'Date Field', 'Date', 
    'Replacement text for Date Field', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (106, 6, '*df', 'Custom 1', 'Custom 1', 
    'Replacement text for Custom 1', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (107, 6, '*df', 'Custom 2', 'Custom 2', 
    'Replacement text for Custom 2', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (108, 6, '*df', 'Custom 3', 'Custom 3', 
    'Replacement text for Custom 3', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (109, 6, '*df', 'Custom 4', 'Custom 4', 
    'Replacement text for Custom 4', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (110, 6, '*df', 'Custom 5', 'Custom 5', 
    'Replacement text for Custom 5', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (111, 17, '*df', 'XYZ_scale', '12', 
    'Divide the rack type dims by this value when setting XYZ coords', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (112, 4, '*df', '214', 'Imports:  Orders', 
    'Imports:  Orders', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (113, 4, '*df', '308', 'Tools:  Create slots/set xyz', 
    'Tools:  Create slots/set xyz', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (114, 20, '*df', 'R', 'R - Reserve', 
    'Reserve', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (115, 20, '*df', 'C', 'C - Case Pick', 
    'Case Pick', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (116, 20, '*df', 'A', 'A - Active', 
    'Active', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (117, 20, '*df', 'T', 'T - Transitional', 
    'Transitional', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (118, 20, '*df', 'S', 'S - Staging', 
    'Staging', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (119, 20, '*df', 'P', 'P - Pack and Hold', 
    'Pack and Hold', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (120, 20, '*df', 'Y', 'Y - Yard', 
    'Yard', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (121, 20, '*df', 'O', 'O - Order Consolidation', 
    'Order Consolidation', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (122, 21, '*df', 'ACK from LM', 'FROM LM', 
    'ACK message expected from LM', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (123, 22, '*df', 'ACK to LM', 'FROM SLOT', 
    'ACK message sent to LM', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (124, 23, '*df', 'Retry delay', '5', 
    'Retry delay for messages sent to labor(minutes)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (125, 24, '*df', 'Max attempts to retry message', '5', 
    'Maximum number of attempts for Slot to send the message to LM if unsuccessful', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (126, 15, '*df', 'Port used to send messages', '4664', 
    'This is the port set for SLOT to send messages on', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (127, 16, '*df', 'Port used to receive messages', '4665', 
    'This is the port set for SLOT to receive messages on', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (128, 6, '*df', 'Item Num 1', 'Item Num 1', 
    'Item informational field that stores numerical data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (129, 6, '*df', 'Item Num 2', 'Item Num 2', 
    'Item informational field that stores numerical data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (130, 6, '*df', 'Item Num 3', 'Item Num 3', 
    'Item informational field that stores numerical data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (131, 6, '*df', 'Item Num 4', 'Item Num 4', 
    'Item informational field that stores numerical data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (132, 6, '*df', 'Item Num 5', 'Item Num 5', 
    'Item informational field that stores numerical data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (133, 6, '*df', 'Item Char 1', 'Item Char 1', 
    'Item informational field that stores character data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (134, 6, '*df', 'Item Char 2', 'Item Char 2', 
    'Item informational field that stores character data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (135, 6, '*df', 'Item Char 3', 'Item Char 3', 
    'Item informational field that stores character data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (136, 6, '*df', 'Item Char 4', 'Item Char 4', 
    'Item informational field that stores character data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (137, 6, '*df', 'Item Char 5', 'Item Char 5', 
    'Item informational field that stores character data (item_master table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (138, 6, '*df', 'SI Num 1', 'SI Num 1', 
    'Slot item informational field that stores numerical data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (139, 6, '*df', 'SI Num 2', 'SI Num 2', 
    'Slot item informational field that stores numerical data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (140, 6, '*df', 'SI Num 3', 'SI Num 3', 
    'Slot item informational field that stores numerical data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (141, 6, '*df', 'SI Num 4', 'SI Num 4', 
    'Slot item informational field that stores numerical data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (142, 6, '*df', 'SI Num 5', 'SI Num 5', 
    'Slot item informational field that stores numerical data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (143, 6, '*df', 'SI Num 6', 'SI Num 6', 
    'Slot item informational field that stores numerical data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (144, 4, '*df', '105', 'Slotting: Real time', 
    'Slotting: Real time', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (145, 4, '*df', '602', 'Other:  Export Moves to LM', 
    'LM: Moves export to LM', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (146, 4, '*df', '106', 'Slotting:  Automated reslot', 
    'Slotting:  Automated reslot', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (147, 25, '*df', 'Version of LM', '2004R2', 
    'The version of Labor Management to which Slot will interface', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (148, 4, '*df', '604', 'Aisle Builder Wizard', 
    'Aisle Builder Wizard', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (149, 19, '*df', 'Slotting', 'Slotting', 
    'Slotting Activity', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (150, 19, '*df', 'Slot-pull', 'Slot-pull', 
    'Slot-put Activity', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (151, 19, '*df', 'Slot-put', 'Slot-put', 
    'Slot-put Activity', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (152, 26, '*df', 'Q_INIT', '0.7', 
    'Initial value of q used to ruin the current feasible solution', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (153, 26, '*df', 'MAX_TIME', '100', 
    'Maximum time available, in seconds, to run the algorithm', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (154, 26, '*df', 'MAX_RUNS', '100', 
    'Maximum number of runs', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (155, 4, '*df', '215', 'Imports: Replenishment', 
    'Imports: Replenishment', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (156, 9, '*df', 'Use rules for recalc', 'TRUE', 
    'Use goals, sequences and pick like balancing during recalc layout', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (157, 27, '*df', 'Retain comparison results', 'NO', 
    'Retain comparison results', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, DTL_DESC, 
    MOD_USER)
 Values
   (158, 28, '*df', 'HTTP server login name', 'This is the login name required to connect to the http server.', 
    'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, DTL_DESC, 
    MOD_USER)
 Values
   (159, 29, '*df', 'HTTP server password', ' This is the password required to connect to the http server.', 
    'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, DTL_DESC, 
    MOD_USER)
 Values
   (160, 30, '*df', 'HTTP Service name', ' This is the service that should be initiated on the http server when messages are sent.', 
    'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (161, 31, '*df', 'WM version for move export', 'WM09', 
    'WM version:Use WM09 for WM versions 2009 and older and WM2010 for WM versions SCPP and later.', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, DTL_DESC, 
    MOD_USER)
 Values
   (162, 32, '*df', 'HTTP parameter name', 'Parameter name for sending messages to Integration Framework Enterprise Edition.', 
    'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (163, 33, '*df', 'Use enhanced move grouping', 'NO', 
    'Include group and sequence number in slot width file and group moves on a shelf together.', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (164, 9, '*df', 'Borrow forecast weekly', 'TRUE', 
    'Do not update est_mvmt_can_borrow and est_hits_can_borrow flags', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (1, 1, '*df', 'MaxFailLogin', '5', 
    'Maximum number of failed login attempts allowed', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (2, 2, '*df', 'MaxFailExpirationDays', '300', 
    'Maximum days after which password will expire', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (3, 3, '*df', 'MaxWaitTime', '60', 
    'Maximum number of seconds a process can be in status 40', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (4, 4, '*df', '100', 'Slotting:  Batch reslot', 
    'Slotting:  Batch reslot', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (5, 4, '*df', '101', 'Slotting:  Max moves reslot', 
    'Slotting:  Max moves reslot', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (6, 4, '*df', '102', 'Slotting:  Single item reslot', 
    'Slotting:  Single item reslot', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (7, 4, '*df', '103', 'Slotting:  Reslot/unslot from a filter', 
    'Slotting:  Reslot/unslot from a filter', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (8, 4, '*df', '104', 'Slotting:  Score a range', 
    'Slotting:  Score a range', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (9, 4, '*df', '200', 'Imports:  Warehouse', 
    'Imports:  Warehouse', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (10, 4, '*df', '201', 'Imports:  Commodity Codes', 
    'Imports:  Commodity Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (11, 4, '*df', '202', 'Imports:  Family Group Relations', 
    'Imports:  Family Group Relations', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (12, 4, '*df', '203', 'Imports:  Vendor Codes', 
    'Imports:  Vendor Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (13, 4, '*df', '204', 'Imports:  Hazard Codes', 
    'Imports:  Hazard Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (14, 4, '*df', '205', 'Imports:  Crushability Codes', 
    'Imports:  Crushability Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (15, 4, '*df', '206', 'Imports:  Misc 1 Codes', 
    'Imports:  Misc 1 Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (16, 4, '*df', '207', 'Imports:  Misc 2 Codes', 
    'Imports:  Misc 2 Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (17, 4, '*df', '208', 'Imports:  Slots', 
    'Imports:  Slots', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (18, 4, '*df', '209', 'Imports:  Items', 
    'Imports:  Items', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (19, 4, '*df', '210', 'Imports:  Movement', 
    'Imports:  Movement', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (20, 4, '*df', '211', 'Imports:  Inventory History', 
    'Imports:  Inventory History', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (21, 4, '*df', '212', 'Imports:  Hits History', 
    'Imports:  Hits History', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (22, 4, '*df', '213', 'Imports:  Family Groups', 
    'Imports:  Family Groups', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (23, 4, '*df', '300', 'Tools:  Recalc history', 
    'Tools:  Recalc history', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (24, 4, '*df', '301', 'Tools:  Set slot width', 
    'Tools:  Set slot width', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (25, 4, '*df', '302', 'Tools:  Recalc slot layout', 
    'Tools:  Recalc slot layout', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (26, 4, '*df', '303', 'Tools:  Set left-right slot pointers', 
    'Tools:  Set left-right slot pointers', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (27, 4, '*df', '304', 'Tools:  Clear moves list', 
    'Tools:  Clear moves list', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (28, 4, '*df', '305', 'Tools:  Purge history', 
    'Tools:  Purge history', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (29, 4, '*df', '306', 'Tools:  Set label locations', 
    'Tools:  Set label locations', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (30, 4, '*df', '307', 'Tools:  Left justify slots', 
    'Tools:  Left justify slots', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (31, 4, '*df', '400', 'Other:  Export/clear moves list', 
    'Other:  Export/clear moves list', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (32, 4, '*df', '401', 'Other:  Export slots', 
    'Other:  Export slots', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (33, 4, '*df', '402', 'Other:  Edit from a filter', 
    'Other:  Edit from a filter', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (34, 4, '*df', '403', 'Other:  Edit range group/range', 
    'Other:  Edit range group/range', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (35, 4, '*df', '404', 'Other:  Clear exceptions', 
    'Other:  Clear exceptions', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (36, 5, '*df', 'Family Group', 'Family Group', 
    'Family Group', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (37, 5, '*df', 'Commodity', 'Commodity', 
    'Commodity', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (38, 5, '*df', 'Crushability', 'Crushability', 
    'Crushability', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (39, 5, '*df', 'Hazard', 'Hazard', 
    'Hazard', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (40, 5, '*df', 'Vendor', 'Vendor', 
    'Vendor', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (41, 5, '*df', 'Family Group Relations', 'Family Group Relations', 
    'Family Group Relations', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (42, 5, '*df', 'Misc 1', 'Misc 1', 
    'Misc 1', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (43, 5, '*df', 'Misc 2', 'Misc 2', 
    'Misc 2', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (44, 5, '*df', 'Misc 3', 'Misc 3', 
    'Misc 3', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (45, 5, '*df', 'Misc 4', 'Misc 4', 
    'Misc 4', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (46, 5, '*df', 'Misc 5', 'Misc 5', 
    'Misc 5', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (47, 5, '*df', 'Misc 6', 'Misc 6', 
    'Misc 6', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (48, 6, '*df', 'SI Char 1', 'SI Char 1', 
    'Slot item informational field that stores character data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (49, 6, '*df', 'SI Char 2', 'SI Char 2', 
    'Slot item informational field that stores character data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (50, 6, '*df', 'SI Char 3', 'SI Char 3', 
    'Slot item informational field that stores character data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (51, 6, '*df', 'SI Char 4', 'SI Char 4', 
    'Slot item informational field that stores character data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (52, 6, '*df', 'SI Char 5', 'SI Char 5', 
    'Slot item informational field that stores character data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (53, 6, '*df', 'SI Char 6', 'SI Char 6', 
    'Slot item informational field that stores character data (slot_item table)', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (54, 4, '*df', '405', 'Other:  Calculate optimal volume', 
    'Other:  Calculate optimal volume', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (55, 7, '*df', 'NumUniqueIdImport', '1000', 
    'Number of unique id values to select at one time during slots and warehouse imports', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (66, 4, '*df', '406', 'Tools:  Rank items', 
    'Tools:  Rank items', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (67, 8, '*df', 'Calculate picking', '1', 
    'Calculate picking', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (68, 8, '*df', 'Calculate replenishing', '1', 
    'Calculate replenishing', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (69, 9, '*df', 'Max # ISP for > 1 population', '30000', 
    'Maximum number of item-slot pairs to select', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (70, 4, '*df', '407', 'Imports:  Misc 3 Codes', 
    'Imports:  Misc 3 Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (71, 4, '*df', '408', 'Imports:  Misc 4 Codes', 
    'Imports:  Misc 4 Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (72, 4, '*df', '409', 'Imports:  Misc 5 Codes', 
    'Imports:  Misc 5 Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (73, 4, '*df', '410', 'Imports:  Misc 6 Codes', 
    'Imports:  Misc 6 Codes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (74, 4, '*df', '411', 'Tools:  Calculate 3D slotting values', 
    'Tools:  Calculate 3D slotting values', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (75, 4, '*df', '500', 'Analysis: Haves Needs', 
    'Analysis: Haves Needs', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (76, 4, '*df', '501', 'Analysis: Needs', 
    'Analysis: Needs', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (77, 10, '*df', 'Each', 'E', 
    'Each', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (78, 10, '*df', 'Inner', 'I', 
    'Inner', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (79, 10, '*df', 'Case', 'C', 
    'Case', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (80, 10, '*df', 'Pallet', 'P', 
    'Pallet', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (81, 10, '*df', 'Bin', 'B', 
    'Bin', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (82, 10, '*df', 'Conveyable - calculate', 'C', 
    'Conveyable - calculate', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (83, 10, '*df', 'Yes', 'Y', 
    'Yes', 'admin');
Insert into SYSTEM_CODE_DTL
   (SYS_CODE_DTL_ID, SYS_CODE_ID, WHSE_CODE, CODE_VALUE, BUSINESS_VALUE, 
    DTL_DESC, MOD_USER)
 Values
   (84, 10, '*df', 'No', 'N', 
    'No', 'admin');
COMMIT;

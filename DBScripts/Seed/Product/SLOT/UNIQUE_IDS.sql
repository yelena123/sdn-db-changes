SET DEFINE OFF;
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('seasonal_detail', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('automation', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('borrowing', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('whse_server', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('sql_script', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('move_list_report_hdr', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('bay_types', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('bay_type_level', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('abw_create_aisle_hdr', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('abw_create_aisle', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('abw_analysis_results', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('hn_results_hdr', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('haves_results_dtl', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('needs_results_dtl', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('hn_ranges', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('hn_rack_types', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('sl_failure_reasons', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('adj_grp_id', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('chain_grp_sort', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('replen_matching', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('transaction_no', '*df', 500000000, 599999999, 500000000, 
    9, 1, 500000000, 599999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('ranges_for_auto_tasks', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('session_id_server', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('filter_retreive_id', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('input_param_max_reslot', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('input_param_slot_list', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('bin_xref', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('bins', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('cat_code', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('category', '*df', 1, 9999999, 11, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('cnstr_dtl', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('cnstr_grp', '*df', 1, 9999999, 3, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('cnstr_lev_seq', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('cnstr_mult_loc', '*df', 1, 999999, 1, 
    7, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('cnstr_rack_level', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('constraints', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('exceptions', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('filter_field_master', '*df', 1, 999999, 319, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('filter_format', '*df', 1, 999999, 6, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('filter_format_detail', '*df', 1, 999999, 146, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('grp_cat_code', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('import_format_offsets', '*df', 1, 9999999, 1061, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('import_format_prefs', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('import_formats', '*df', 1, 9999999, 57, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('import_master', '*df', 1, 9999999, 22, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('import_offset_master', '*df', 1, 9999999, 525, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('item_cat_xref', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('item_history', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('item_master', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('kit', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('kit_item', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('move_list', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('move_list_hdr', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('move_list_report', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('mult_loc_zone_hdr', '*df', 1, 999999, 1, 
    7, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('mult_loc_zone', '*df', 1, 999999, 1, 
    7, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('pallet_xref', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('pallets', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('pickline_group', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('profile_access', '*df', 1, 9999999, 73, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('profile_master', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('query', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('queryentry', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('rack_bin_xref', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('rack_level_map', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('rack_pallet_xref', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('rack_type', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('rack_type_level', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('seasonal', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('shelf', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('shelf_slot_xref', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('slot', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('slot_item', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('slot_item_score', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('slot_list_tree', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('sns', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('sns_dtl', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('system_code', '*df', 1, 9999999, 32, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('system_code_dtl', '*df', 1, 9999999, 165, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('tree_dtl', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('txn_master', '*df', 1, 9999999, 68, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('user_master', '*df', 1, 9999999, 2, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('user_profile_xref', '*df', 1, 9999999, 2, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('user_range_xref', '*df', 1, 9999999, 2, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('whprefs', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('whse_import_prefs', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('whse_process', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('session_id', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('move_grp_id', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('haves_needs', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('hn_failure', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('mult_orient_calc', '*df', 1, 9999999, 1, 
    6, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('message_master', '*df', 1, 999999, 281, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('reserve_bin_xref', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('reserve_pallet_xref', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('pick_zone', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('wa_header', '*df', 1, 999999999, 1, 
    9, 1, 1, 999999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('wa_detail', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('order_hdr', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('order_dtl', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('br_header', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('br_detail', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('br_target', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('br_addl_items', '*df', 1, 999999, 1, 
    6, 1, 1, 999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('location_range', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('container_type', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('import_format_odbc', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('res_analysis_result', '*df', 1, 9999999, 1, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('color_code_dtl', '*df', 1, 9999999, 120, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('color_code_hdr', '*df', 1, 9999999, 24, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('lm_msg_id', '*df', 1, 99999, 1, 
    5, 1, 1, 99999, '1', 
    1, 'admin');
Insert into UNIQUE_IDS
   (REC_TYPE, WHSE_CODE, START_NBR, END_NBR, CURR_NBR, 
    NBR_LEN, INCR_VALUE, NXT_START_NBR, NXT_END_NBR, REPEAT_RANGE, 
    ID_TYPE, MOD_USER)
 Values
   ('forecast', '*df', 1, 9999999, 4, 
    7, 1, 1, 9999999, '1', 
    1, 'admin');
COMMIT;

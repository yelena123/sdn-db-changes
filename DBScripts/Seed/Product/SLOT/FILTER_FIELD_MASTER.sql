SET DEFINE OFF;
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (114, 'Slot Max Side Clear', 'max_side_clear', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (115, 'Slot Class', 'rack_class', 'rack_type', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (116, 'Slot Dep', 'depth', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (117, 'Slot Ht', 'ht', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (118, 'Rec TI', 'item_master', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (119, 'SNS', 'name', 'sns', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (120, 'Est Viscosity', 'est_visc', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (121, 'Calc Viscosity', 'calc_visc', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (122, 'Pick Ht', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (123, 'Bin Inv', 'bin_inven', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (124, 'Bin Mvmt', 'bin_mov', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (125, 'Shipping Height', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (126, 'Shipping Length', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (127, 'Shipping Width', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (128, 'Shipping Weight', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (129, 'Slotted Height', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (130, 'Slotted Length', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (131, 'Slotted Width', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (132, 'Slotted Weight', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (133, 'Item Rank', 'rank', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (134, 'Use 3D Slotting', 'use_3d_slot', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (135, 'Nest Eaches', 'allow_nest_each', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (136, 'Incremental Height', 'incremental_height', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (137, 'Incremental Length', 'incremental_length', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (138, 'Incremental Width', 'incremental_width', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (139, 'Rotate Eaches', 'allow_rotate_each', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (140, 'Rotate Inners', 'allow_rotate_inner', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (141, 'Rotate Cases', 'allow_rotate_case', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (142, 'Rotate Bins', 'allow_rotate_bin', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (143, 'Order Number', 'order_number', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (144, 'Line Number', 'line_number', 'order_dtl', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (145, 'Item Number', 'sku_id', 'order_dtl', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (146, 'Quantity', 'order_quantity', 'order_dtl', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (147, 'Ship Unit', 'ship_unit', 'order_dtl', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (148, 'Batch Number', 'batch_number', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (149, 'Customer Number', 'customer_number', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (150, 'Extra Field', 'extra_field', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (151, 'Date Field', 'date_field', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (152, 'Status', 'status', 'order_dtl', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (153, 'Work Assignment #', 'wa_number', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (154, 'Status', 'status', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (158, 'Pick Zone', 'pick_zone_id', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (167, 'Quantity per Grab (Each)', 'qty_per_grab_each', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (168, 'Range', 'my_range', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (169, 'Quantity per Grab (Inner)', 'qty_per_grab_inner', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (170, 'Quantity per Grab (Case)', 'qty_per_grab_case', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (171, 'Handling Attrib (Each)', 'handling_attrib_each', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (172, 'Handling Attrib (Inner)', 'handling_attrib_inner', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (173, 'Handling Attrib (Case)', 'handling_attrib_case', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (174, 'Slot Type', 'slot_type', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (175, 'Travel Zone', 'travel_zone', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (176, 'Travel Aisle', 'travel_aisle', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (177, 'Custom 1', 'custom_1', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (178, 'Custom 2', 'custom_2', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (179, 'Custom 3', 'custom_3', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (180, 'Custom 4', 'custom_4', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (181, 'Custom 5', 'custom_5', 'order_hdr', 'admin', 
    4);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (187, 'Needed Reserve Type', 'reserve_rack_type_id', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (188, 'Allowable Reserve Units', 'allow_reserve', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (189, 'Plt Pattern for Reserve', 'palpat_reserve', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (190, 'X-Coordinate', 'x_coord', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (191, 'Y-Coordinate', 'y_coord', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (192, 'Z-Coordinate', 'z_coord', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (193, 'Primary Group By', 'primary_grp_by', 'wa_ header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (194, 'Primary Value', 'primary_value', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (195, 'Secondary Group By', 'second_grp_by', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (196, 'Secondary Value', 'second_value', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (197, 'Pick Time', 'time_to_pick', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (198, 'Date Created', 'create_date_time', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (199, 'Detail Lines', 'detail_lines', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (200, 'Location Class', 'location_class', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (201, 'WA Number', 'wa_number', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (202, 'Status', 'status', 'wa_header', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (203, 'Seq #', 'seq_number', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (204, 'Item Number', 'item_number', 'item_master', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (205, 'Quantity', 'quantity', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (206, 'Ship Unit', 'ship_unit', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (207, 'Location Number', 'dsp_slot', 'slot', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (208, 'Pick Zone', 'pick_zone', 'pick_zone', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (209, 'Batch Number', 'batch_number', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (210, 'Customer Number', 'customer_number', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (211, 'Extra Field', 'extra_field', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (212, 'Date Field', 'date_field', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (213, 'Custom 1', 'custom_1', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (214, 'Custom 2', 'custom_2', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (215, 'Custom 3', 'custom_3', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (216, 'Custom 4', 'custom_4', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (217, 'Custom 5', 'custom_5', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (218, 'Slot Priority', 'slot_priority', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (219, 'Order Number', 'order_number', 'wa_detail', 'admin', 
    9);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (220, 'Item Num 1', 'item_num_1', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (221, 'Item Num 2', 'item_num_2', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (222, 'Item Num 3', 'item_num_3', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (223, 'Item Num 4', 'item_num_4', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (224, 'Item Num 5', 'item_num_5', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (225, 'Item Char 1', 'item_char_1', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (226, 'Item Char 2', 'item_char_2', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (227, 'Item Char 3', 'item_char_3', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (228, 'Item Char 4', 'item_char_4', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (229, 'Item Char 5', 'item_char_5', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (230, 'SI Num 1', 'si_num_1', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (231, 'SI Num 2', 'si_num_2', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (232, 'SI Num 3', 'si_num_3', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (233, 'SI Num 4', 'si_num_4', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (234, 'SI Num 5', 'si_num_5', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (235, 'SI Num 6', 'si_num_6', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (236, 'Slotting unit weight ', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (241, 'Date Created', 'create_date_time', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (242, 'Created By', 'mod_user', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (243, 'From Slot', 'from_slot', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (244, 'To Slot', 'to_slot', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (245, 'Item', 'item', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (246, 'Item Description', 'sku_desc', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (247, 'Status', 'status', 'move_list_hdr', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (248, 'Moves List Header ID', 'move_hdr_id', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (249, 'Ship Unit', 'shp_unit', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (250, 'Slot Unit', 'slot_unit', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (251, 'Original Score', 'orig_scr', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (252, 'New Score', 'score', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (253, 'Lanes', 'rec_lanes', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (254, 'Stacking', 'stacking', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (255, 'Deeps', 'deeps', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (256, 'Old Slot Width', 'old_slot_width', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (257, 'New Slot Width', 'slot_width', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (258, 'Oriented Height', 'height', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (259, 'Oriented Width', 'width', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (260, 'Oriented Length', 'length', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (261, 'Orientation', 'cur_orient', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (262, 'Pallet Code', 'rec_pallet', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (263, 'New Pallet HI', 'new_hi', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (264, 'Bin Code', 'rec_bin', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (265, 'Bin Unit', 'bin_unit', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (266, 'Move Payback', 'move_payback', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (267, 'Group Payback', 'group_payback', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (268, 'Sequence Number', 'move_seq', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (269, 'Cost Benefit', 'roi_saving', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (270, 'Move Group Time', 'move_grp_time', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (271, 'Move Group Cost', 'move_grp_cost', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (272, 'Move Group Score', 'move_grp_score', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (273, 'Group Run Name', 'grp_run_name', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (274, 'Calc Hits', 'calc_hits', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (275, 'Calc Case Mvmt', 'case_mov', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (276, 'SI Char 1', 'info1', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (277, 'SI Char 2', 'info2', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (278, 'SI Char 3', 'info3', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (279, 'SI Char 4', 'info4', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (280, 'SI Char 5', 'info5', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (281, 'SI Char 6', 'info6', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (282, 'SI Num 1', 'si_num_1', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (283, 'SI Num 2', 'si_num_2', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (284, 'SI Num 3', 'si_num_3', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (285, 'SI Num 4', 'si_num_4', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (286, 'SI Num 5', 'si_num_5', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (287, 'SI Num 6', 'si_num_6', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (288, 'Rank', 'rank', 'slot_item', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (289, 'Item Num 1', 'item_num_1', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (290, 'Item Num 2', 'item_num_2', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (291, 'Item Num 3', 'item_num_3', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (292, 'Item Num 4', 'item_num_4', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (293, 'Item Num 5', 'item_num_5', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (294, 'Item Char 1', 'item_char_1', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (295, 'Item Char 2', 'item_char_2', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (296, 'Item Char 3', 'item_char_3', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (297, 'Item Char 4', 'item_char_4', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (298, 'Item Char 5', 'item_char_5', 'item_master', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (299, 'Run Name', 'run_name', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (300, 'Allowable Bins', 'sku_id', 'bin_xref', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (301, 'Allowable Pallets', 'sku_id', 'pallet_xref', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (303, 'Calculated Cube Inventory', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (304, 'Forecast Type', 'slot_item', 'use_estimated_hist', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (305, 'Fully Slotted Weight', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (306, 'Height Clearance', 'slot', 'max_ht_clear', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (307, 'Label Location', 'slot', 'label_pos', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (308, 'Max Lane Weight', 'slot', 'max_lane_wt', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (309, 'Max Slot Weight', 'slot', 'wt_limit', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (310, 'OPP - For Primary Moves', 'slot_item', 'opt_pallet_pattern', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (311, 'OPP - For Secondary Moves', 'slot_item', 'opt_pallet_pattern', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (312, 'Optimize Pallet Pattern', 'slot_item', 'opt_pallet_pattern', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (313, 'Pallet Code', 'slot_item', 'current_pallet', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, MOD_USER, FILTER_TYPE)
 Values
   (314, 'PLB Score', 'slot_item_score', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (315, 'Reasons Item Does not Fit', 'slot_item', 'legal_fit_reason', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (318, 'Borrowing Object', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (319, 'Borrowing Specific', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (320, 'History Match', 'hist_match', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (321, 'Item Max Number of Slots', 'max_slots', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (322, 'Multiple Location Group', 'mult_loc_grp', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (323, 'Total Cube', 'total_Cube', 'wa_header', 'admin', 
    8);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (324, 'Slot Wid', 'width', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (325, 'Item Max Pallet Stacking', 'max_pallet_stacking', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (326, 'Properties Borrowing Object', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (327, 'Properties Borrowing Detail', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (328, 'A-Frame Height', 'aframe_ht', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (329, 'A-Frame Length', 'aframe_len', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (330, 'A-Frame Width', 'aframe_wid', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (331, 'A-Frame Weight', 'aframe_wt', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (332, 'A-Frame Volume', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (333, 'A-Frame Slotting Allowed', 'aframe_allow', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (334, 'Replenishment Group', 'replen_grp', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (335, 'Calculated Ship Mvmt / Hits', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (336, 'Estimated Ship Mvmt / Hits', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (337, 'Total Calc Case Movement', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (338, 'Total Calc Cube Movement', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (339, 'Total Calc Shipping Movement', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (340, 'Total Calc Slotting Movement', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (341, 'Total Calc Hits', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (342, 'Total Calc Viscosity', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (343, 'Total Calc Ship Mvmt/Hits', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (344, 'Total Calc Case Inventory', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (345, 'Total Calc Cube Inventory', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (346, 'Total Calc Pallet Inventory', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (350, 'Adjacent Group ID', 'adj_grp_id', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (351, 'From Slot in Adjacent Range?', 'from_adj', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (352, 'To Slot in Adjacent Range?', 'to_adj', 'move_list', 'admin', 
    5);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (353, 'Weeks In Pick Slot', 'WIPS value', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (354, 'Allow Expand Right', 'allow_expand_rgt', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (355, 'Allow Expand Left', 'allow_expand_lft', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (1, 'Allow 1 HI residual', 'opt_pallet_pattern', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (4, 'Allowable Slotting Unit', 'allow_slu', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (5, 'Calc Case Inv', 'case_inven', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (6, 'Calc Cube Inv', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (7, 'Calc Pallet Inv', 'pallet_inven', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (8, 'Bin Unit', 'bin_unit', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (10, 'Calc Case Mvmt', 'case_mov', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (11, 'Calc Cube Mvmt', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (12, 'Calc Hits', 'calc_hits', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (13, 'Calc Ship Mvmt', 'slot_item', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (14, 'Calc Slot Mvmt', 'slot_item', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (16, 'Case Volume', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (17, 'Case Ht', 'case_ht', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (18, 'Case Len', 'case_len', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (19, 'Case Wt', 'case_wt', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (20, 'Case Wid', 'case_wid', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (21, 'Commodity Code', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (22, 'Conveyable', 'conveyable', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (24, 'Crushability', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (25, 'Lanes', 'rec_lanes', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (26, 'Cur Plt Height', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (28, 'Stacking', 'rec_stacking', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (29, 'Deeps', 'deeps', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (30, 'Discontinued', 'discont', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (31, 'Disc Date', 'disc_trans', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (33, 'Each Volume', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (34, 'Each Ht', 'each_ht', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (35, 'Each Len', 'each_len', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (36, 'Each Wt', 'each_wt', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (37, 'Each Wid', 'each_wid', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (38, 'Est Case Inv', 'est_inventory', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (39, 'Est Case Mvmt', 'est_movement', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (40, 'Est Cube Mvmt', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (41, 'Est Hits', 'est_hits', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (42, 'Est Ship Mvmt', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (43, 'Est Slot Mvmt', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (44, 'Expected Receipt', 'ex_recpt_date', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (45, 'Family Group', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (46, 'Hazard', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (47, 'SI Char 1', 'info1', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (48, 'SI Char 2', 'info2', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (49, 'SI Char 3', 'info3', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (50, 'SI Char 4', 'info4', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (51, 'SI Char 5', 'info5', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (52, 'SI Char 6', 'info6', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (54, 'Inner Volume', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (55, 'Inner Ht', 'inn_ht', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (56, 'Inner Len', 'inn_len', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (57, 'Inner Pack', 'each_per_inn', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (58, 'Inner Wt', 'inn_wt', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (59, 'Inner Wid', 'inn_wid', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (60, 'Item Desc', 'sku_desc', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (61, 'Item Number', 'sku_name', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (62, 'Item Max Lanes', 'max_lanes', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (63, 'Item Max Stack', 'max_stacking', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (64, 'Misc 1 Code', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (65, 'Misc 2 Code', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (66, 'Misc 3 Code', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (67, 'Misc 4 Code', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (68, 'Misc 5 Code', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (69, 'Misc 6 Code', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (70, 'Needed Rack Types', 'needed_rack_type', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (71, 'New Item', 'is_new', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (72, 'Nbr of Inners', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (73, 'Opt Plt Pattern', 'opt_pallet_pattern', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (74, 'Order HI', 'ord_hi', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (75, 'Order Plt Ht', 'item_master', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (76, 'Order TI', 'ord_ti', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (77, 'Orientation', 'cur_orientation', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (78, 'Plt Pattern for Slotting', 'pallete_pattern', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (79, 'Rec HI', 'pallet_hi', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (80, 'Item Score', 'score', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (81, 'Ship Unit', 'ship_unit', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (83, 'Slot Last Changed', 'last_change', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (84, 'Slot Number', 'dsp_slot', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (85, 'Slotting Unit', 'slot_unit', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (86, 'Slotting Units in Slot', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (87, 'Units Per Bin', 'each_per_bin', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (88, 'UPC', 'upc', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, TABLE_NAME, MOD_USER, FILTER_TYPE)
 Values
   (89, 'Vendor', 'cat_code', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (90, 'Vendor HI', 'ven_hi', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (91, 'Vendor Pack', 'each_per_cs', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (92, 'Vendor Plt Ht', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (93, 'Vendor TI', 'ven_ti', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (94, 'Warehouse HI', 'wh_hi', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, MOD_USER, FILTER_TYPE)
 Values
   (95, 'Whse Plt Ht', 'admin', 3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (96, 'Warehouse TI', 'wh_ti', 'item_master', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (97, 'Allow Expand Next', 'allow_expand', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (99, 'Slot Max Ht Clear', 'max_ht_clear', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (100, 'Ign for Reslot', 'ign_for_reslot', 'slot_item', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (101, 'Label Loc', 'label_pos', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (102, 'Left Slot', 'left_slot', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (103, 'Locked', 'lock', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (104, 'Locked Reason', 'lock_reason', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (105, 'Slot Max Lane Wt', 'max_lane_wt', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (106, 'Slot Max Lanes', 'max_lanes', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (107, 'Slot Max Slot Wt', 'wt_limit', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (108, 'Slot Max Stack', 'max_stack', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (109, 'Slot Original Width', 'width', 'slot', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (110, 'Rack Level', 'level', 'rack_level_map', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (111, 'Rack Type', 'rt_name', 'rack_type', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (112, 'Rack Type Desc', 'rack_type_desc', 'rack_type', 'admin', 
    3);
Insert into FILTER_FIELD_MASTER
   (FIELD_ID, NAME, COLUMN_NAME, TABLE_NAME, MOD_USER, 
    FILTER_TYPE)
 Values
   (113, 'Right Slot', 'right_slot', 'slot', 'admin', 
    3);
COMMIT;

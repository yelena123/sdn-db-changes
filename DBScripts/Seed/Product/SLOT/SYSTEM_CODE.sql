SET DEFINE OFF;
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (1, '001', 'MANH', 'Maximum number of failed login attempts allowed', 0, 
    0, 1, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (2, '002', 'MANH', 'Number of days after which password will expire', 0, 
    0, 1, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (3, '003', 'MANH', 'Maximum number of seconds a process can be in status 40', 0, 
    0, 1, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (4, '004', 'MANH', 'Admin processes', 0, 
    0, 0, 0, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (5, '500', 'MANH', 'Replacement text for category fields', 1, 
    1, 1, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (6, '501', 'MANH', 'Replacement text for informational fields', 1, 
    1, 1, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (7, '005', 'MANH', 'Number of unique id values to select at one time during slots and warehouse imports', 0, 
    0, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (9, '006', 'MANH', 'Slotting: Performance options', 0, 
    0, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (10, '502', 'MANH', 'Cross-reference values for internal validations', 1, 
    0, 1, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (11, '503', 'MANH', 'Current language', 0, 
    0, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (12, '504', 'MANH', 'Date formats', 0, 
    0, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (13, '200', 'MANH', 'Equipment type for pick zone maintenance', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (14, '210', 'MANH', 'Labor Management machine ID', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (15, '211', 'MANH', 'Port Slot is sending messages and Labor Management is receiving on (send port)', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (16, '212', 'MANH', 'Port Labor Management is sending messages and Slot is receiving on (receiving port)', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (17, '101', 'MANH', 'Scale between rack type dims and XYZ coordinates', 0, 
    0, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (18, '213', 'MANH', 'Path to write the Slot location file (070) for LM', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (19, '201', 'MANH', 'Activity type for pick zone maintenance', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (20, '202', 'MANH', 'Location class for pick zone maintenance', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (21, '214', 'MANH', 'Receiving acknowledge (ACK) message from LM', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (22, '215', 'MANH', 'Sending acknowledge (ACK) message to LM', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (23, '216', 'MANH', 'Retry delay (Amount of time to wait before sending message to LM again', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (24, '217', 'MANH', 'Max number of retry attempts (while sending messages to LM)', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (25, '218', 'MANH', 'Version of Labor Management to interface with', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (26, '102', 'MANH', 'Parameters for optimize number of bay types algorithm', 0, 
    0, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (27, '103', 'MANH', 'Retain moves comparison results by changing the type', 0, 
    0, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (28, '104', 'MANH', 'HTTP server login name', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (29, '105', 'MANH', 'HTTP server password', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (30, '106', 'MANH', 'HTTP service name', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (31, '505', 'MANH', 'WM version for move export', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (32, '107', 'MANH', 'HTTP parameter name', 1, 
    1, 0, 1, 'admin');
Insert into SYSTEM_CODE
   (SYS_CODE_ID, CODE_TYPE, CUST_ID, CODE_DESC, ALLOW_ADD, 
    ALLOW_DELETE, ALLOW_CHG_MISC_FLAG, ALLOW_CHG_VALUES, MOD_USER)
 Values
   (33, '506', 'MANH', 'Move List and Slot Width Export Setting', 1, 
    1, 0, 1, 'admin');
COMMIT;

set echo on
exec quiet_drop ('TABLE', 'SLOT_WM_SKUID');

create table slot_wm_skuid
(
   slot_skuid,
   wm_skuid
)
as
   select distinct sim.sku_id, ifmw.item_facility_mapping_id
     from so_item_master_unused sim,
          item_cbo ic,
          facility f,
          item_facility_mapping_wms ifmw
    where     sim.sku_name = ic.item_name
          and sim.whse_code = f.whse
          and ic.item_id = ifmw.item_id
          and f.facility_id = ifmw.facility_id;
		  
create index slot_wm_skuidx on slot_wm_skuid (slot_skuid );

exec quiet_drop ('TABLE', 'SLOT_WM_SLOTID');

create table slot_wm_slotid
(
   slot_slotid,
   wm_slotid
)
as
   select distinct l3.slot_id, l2.pick_locn_hdr_id
     from locn_hdr l1, pick_locn_hdr l2, slot_unused l3
    where     l1.locn_hdr_id = l2.locn_hdr_id
          and l1.dsp_locn = l3.dsp_slot
          and l1.whse = l3.whse_code;

exec quiet_drop ('TABLE', 'BIN_XREF_UNUSED');

create table bin_xref_unused
as
   select * from bin_xref;

exec quiet_drop ('TABLE', 'ITEM_CAT_XREF_UNUSED');

create table item_cat_xref_unused
as
   select * from item_cat_xref;

exec quiet_drop ('TABLE', 'PALLET_XREF_UNUSED');

create table pallet_xref_unused
as
   select * from pallet_xref;

exec quiet_drop ('TABLE', 'RESERVE_BIN_XREF_UNUSED');

create table reserve_bin_xref_unused
as
   select * from reserve_bin_xref;

exec quiet_drop ('TABLE', 'RESERVE_PALLET_XREF_UNUSED');

create table reserve_pallet_xref_unused
as
   select * from reserve_pallet_xref;

exec quiet_drop ('TABLE', 'SLOT_ITEM_UNUSED');

create table haves_needs_unused as select * from haves_needs;
create table hn_failure_unused as select * from hn_failure;
create table item_history_unused as select * from item_history;
create table move_list_unused as select * from move_list;
create table slot_item_score_unused as select * from slot_item_score;
create table sl_failure_reasons_unused as select * from sl_failure_reasons;

create table slot_item_unused
as
   select * from slot_item;

commit;

create unique index so_item_master_unused_idx1
   on so_item_master_unused (whse_code, sku_name)
   tablespace slot_indx_tbs;

-- create or replace function getlrslot_wmid (
--    lr_slot_id    slot_unused.left_slot%type)
--    return slot_unused.left_slot%type
-- is
--    v_pick_loc_hdr_id   slot_unused.left_slot%type := 0;
-- begin
--    begin
--       select l2.pick_locn_hdr_id
--         into v_pick_loc_hdr_id
--         from locn_hdr l1, pick_locn_hdr l2, slot_unused l3
--        where     l3.slot_id = lr_slot_id
--              and l1.locn_hdr_id = l2.locn_hdr_id
--              and l1.dsp_locn = l3.dsp_slot
--              and l1.whse = l3.whse_code;
--    exception
--       when others
--       then
--          v_pick_loc_hdr_id := 0;
--    end;
-- 
--    return v_pick_loc_hdr_id;
-- end;
-- /

CREATE OR REPLACE PACKAGE SLOTTING_MIGRATION_PKG IS
    TYPE WHSE_TABLE IS TABLE OF VARCHAR2(3) INDEX BY BINARY_INTEGER;
    
    PROCEDURE COPY_ITEM_DATA;
    PROCEDURE COPY_SLOT_DATA;
END SLOTTING_MIGRATION_PKG;
/

CREATE OR REPLACE PACKAGE BODY SLOTTING_MIGRATION_PKG
IS
   PROCEDURE COPY_ITEM_DATA
   IS
      v_max_item_id               NUMBER;
      v_whse_codes                WHSE_TABLE;
      v_facility_row              FACILITY%ROWTYPE;
      v_facility_id               NUMBER;
      v_max_facility_mapping_id   NUMBER;
      v_case_uom_id               NUMBER;
      v_pack_uom_id               NUMBER;
      v_sub_pack_uom_id           NUMBER;
   BEGIN
      BEGIN
         SELECT NVL (MAX (ITEM_ID), 0) INTO v_max_item_id FROM ITEM_CBO;


         INSERT INTO ITEM_CBO (ITEM_ID,
                               ITEM_NAME,
                               DESCRIPTION,
                               UNIT_WEIGHT,
                               UNIT_HEIGHT,
                               UNIT_LENGTH,
                               UNIT_WIDTH,
                               ITEM_BAR_CODE,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM,
                               COMPANY_ID,
                               VERSION,
                               WEIGHT_UOM_ID,
                               DIMENSION_UOM_ID,
                               AUDIT_CREATED_SOURCE)
            SELECT v_max_item_id + SKU_ID,
                   SKU_NAME,
                   NVL (SKU_DESC, SKU_NAME),
                   EACH_WT,
                   EACH_HT,
                   EACH_LEN,
                   EACH_WID,
                   UPC,
                   CREATE_DATE_TIME,
                   MOD_DATE_TIME,
                   1,
                   0,
                   (SELECT MIN(SIZE_UOM_ID) FROM SIZE_UOM WHERE TC_COMPANY_ID=1 AND SIZE_MAPPING='WEI'),
                   (SELECT MIN(SIZE_UOM_ID) FROM SIZE_UOM WHERE TC_COMPANY_ID=1 AND SIZE_MAPPING='DIM'),
                   MOD_USER
              FROM SO_ITEM_MASTER_UNUSED;

         INSERT INTO ITEM_WMS (ITEM_ID,
                               UNITS_PER_PICK_ACTIVE,
                               UNITS_PER_PICK_CASE_PICK,
                               UNITS_PER_PICK_RESV,
                               HNDL_ATTR_ACTIVE,
                               HNDL_ATTR_CASE_PICK,
                               HNDL_ATTR_RESV,
                               CONVEY_FLAG)
            SELECT v_max_item_id + SKU_ID,
                   QTY_PER_GRAB_EACH,
                   QTY_PER_GRAB_INNER,
                   QTY_PER_GRAB_CASE,
                   HANDLING_ATTRIB_EACH,
                   HANDLING_ATTRIB_INNER,
                   HANDLING_ATTRIB_CASE,
                   CONVEYABLE
              FROM SO_ITEM_MASTER_UNUSED;

         -- insert dummy records into FACILITY
         SELECT DISTINCT WHSE_CODE
           BULK COLLECT INTO v_whse_codes
           FROM SO_ITEM_MASTER_UNUSED;

         SELECT *
           INTO v_facility_row
           FROM FACILITY
          WHERE FACILITY_ID = 1;

         SELECT NVL (MAX (FACILITY_ID), 0) INTO v_facility_id FROM FACILITY;


         FOR i IN 1 .. v_whse_codes.COUNT
         LOOP
            v_facility_row.whse := v_whse_codes (i);
            v_facility_id := v_facility_id + 1;

            INSERT INTO REGION (REGION_ID,
                                TC_COMPANY_ID,
                                REGION_NAME,
                                DESCRIPTION,
                                COMMENTS,
                                REGION_TZ,
                                MARK_FOR_DELETION,
                                CREATED_SOURCE_TYPE,
                                CREATED_SOURCE,
                                CREATED_DTTM,
                                LAST_UPDATED_SOURCE_TYPE,
                                LAST_UPDATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                TIME_FEAS_CALC,
                                IS_PLANNER_REGION,
                                IS_ACTIVE,
                                HIBERNATE_VERSION,
                                REGION_TYPE)
                    VALUES (REGION_ID_SEQ.NEXTVAL,
                            V_FACILITY_ROW.TC_COMPANY_ID,
                            'W' || '-' || V_FACILITY_ROW.WHSE,
                            'W' || '-' || V_FACILITY_ROW.WHSE,
                            NULL,
                            9,
                            V_FACILITY_ROW.MARK_FOR_DELETION,
                            V_FACILITY_ROW.CREATED_SOURCE_TYPE,
                            V_FACILITY_ROW.CREATED_SOURCE,
                            V_FACILITY_ROW.CREATED_DTTM,
                            V_FACILITY_ROW.LAST_UPDATED_SOURCE_TYPE,
                            V_FACILITY_ROW.LAST_UPDATED_SOURCE,
                            V_FACILITY_ROW.LAST_UPDATED_DTTM,
                            'F',
                            0,
                            1,
                            V_FACILITY_ROW.HIBERNATE_VERSION,
                            1);

            INSERT INTO FACILITY (FACILITY_ID,
                                  TC_COMPANY_ID,
                                  BUSINESS_PARTNER_ID,
                                  NAME,
                                  DESCRIPTION,
                                  ADDRESS_1,
                                  ADDRESS_2,
                                  ADDRESS_3,
                                  ADDRESS_KEY_1,
                                  CITY,
                                  STATE_PROV,
                                  POSTAL_CODE,
                                  COUNTY,
                                  COUNTRY_CODE,
                                  LONGITUDE,
                                  LATITUDE,
                                  EIN_NBR,
                                  IATA_CODE,
                                  GLOBAL_LOCN_NBR,
                                  TAX_ID,
                                  DUNS_NBR,
                                  INBOUNDS_RS_AREA_ID,
                                  OUTBOUND_RS_AREA_ID,
                                  INBOUND_REGION_ID,
                                  OUTBOUND_REGION_ID,
                                  FACILITY_TZ,
                                  DROP_INDICATOR,
                                  HOOK_INDICATOR,
                                  HANDLER,
                                  IS_SHIP_APPT_REQD,
                                  IS_RCV_APPT_REQD,
                                  TRACK_ONTIME_INDICATOR,
                                  ONTIME_PERF_METHOD,
                                  LOAD_FACTOR_SIZE_VALUE,
                                  MARK_FOR_DELETION,
                                  CREATED_SOURCE_TYPE,
                                  CREATED_SOURCE,
                                  CREATED_DTTM,
                                  LAST_UPDATED_SOURCE_TYPE,
                                  LAST_UPDATED_SOURCE,
                                  LAST_UPDATED_DTTM,
                                  ALLOW_OVERLAPPING_APPS,
                                  EARLY_TOLERANCE_POINT,
                                  LATE_TOLERANCE_POINT,
                                  EARLY_TOLERANCE_WINDOW,
                                  LATE_TOLERANCE_WINDOW,
                                  FACILITY_TYPE_BITS,
                                  PP_HANDLING_TIME,
                                  STOP_CONSTRAINT_BITS,
                                  IS_DOCK_SCHED_FAC,
                                  ASN_COMMUNICATION_METHOD,
                                  PICK_TIME,
                                  DISPATCH_INITIATION_TIME,
                                  PAPERWORK_TIME,
                                  VEHICLE_CHECK_TIME,
                                  VISIT_GAP,
                                  MIN_HANDLING_TIME,
                                  IS_CREDIT_AVAILABLE,
                                  BUSINESS_GROUP_ID_1,
                                  BUSINESS_GROUP_ID_2,
                                  IS_OPERATIONAL,
                                  IS_MAINTENANCE_FACILITY,
                                  DWELL_TIME,
                                  DRIVER_CHECK_IN_TIME,
                                  DISPATCH_DTTM,
                                  DSP_DR_CONFIGURATION_ID,
                                  DROP_HOOK_TIME,
                                  LOADING_END_TIME,
                                  DRIVER_DEBRIEF_TIME,
                                  ACCOUNT_CODE_NUMBER,
                                  LOAD_FACTOR_MOT_ID,
                                  LOAD_UNLOAD_MOT_ID,
                                  MIN_TRAIL_FILL,
                                  TANDEM_DROP_HOOK_TIME,
                                  STORE_TYPE,
                                  DRIVEIN_TIME,
                                  CHECKIN_TIME,
                                  DRIVEOUT_TIME,
                                  CHECKOUT_TIME,
                                  CUTOFF_DTTM,
                                  MAX_TRAILER_YARD_TIME,
                                  HAND_OVER_TIME,
                                  OVER_BOOK_PERCENTAGE,
                                  RECOMMENDATION_CRITERIA,
                                  NBR_OF_SLOTS_TO_SHOW,
                                  RESTRICT_FLEET_ASMT_TIME,
                                  GROUP_ID,
                                  IS_VTBP,
                                  AUTO_CREATE_SHIPMENT,
                                  LOGO_IMAGE_PATH,
                                  GEN_LAST_SHIPMENT_LEG,
                                  DEF_DRIVER_TYPE_ID,
                                  DEF_TRACTOR_ID,
                                  DEF_EQUIPMENT_ID,
                                  DRIVER_AVAIL_HRS,
                                  WHSE,
                                  OPEN_DATE,
                                  CLOSE_DATE,
                                  HOLD_DATE,
                                  GRP,
                                  CHAIN,
                                  ZONE,
                                  TERRITORY,
                                  REGION,
                                  DISTRICT,
                                  SHIP_MON,
                                  SHIP_TUE,
                                  SHIP_WED,
                                  SHIP_THU,
                                  SHIP_FRI,
                                  SHIP_SAT,
                                  SHIP_SU,
                                  ACCEPT_IRREG,
                                  WAVE_LABEL_TYPE,
                                  PKG_SLIP_TYPE,
                                  PRINT_CODE,
                                  CARTON_CNT_TYPE,
                                  SHIP_VIA,
                                  RTE_NBR,
                                  RTE_ATTR,
                                  RTE_TO,
                                  RTE_TYPE_1,
                                  RTE_TYPE_2,
                                  RTE_ZIP,
                                  SPL_INSTR_CODE_1,
                                  SPL_INSTR_CODE_2,
                                  SPL_INSTR_CODE_3,
                                  SPL_INSTR_CODE_4,
                                  SPL_INSTR_CODE_5,
                                  SPL_INSTR_CODE_6,
                                  SPL_INSTR_CODE_7,
                                  SPL_INSTR_CODE_8,
                                  SPL_INSTR_CODE_9,
                                  SPL_INSTR_CODE_10,
                                  ASSIGN_MERCH_TYPE,
                                  ASSIGN_MERCH_GROUP,
                                  ASSIGN_STORE_DEPT,
                                  CARTON_LABEL_TYPE,
                                  CARTON_CUBNG_INDIC,
                                  MAX_CTN,
                                  MAX_PLT,
                                  BUSN_UNIT_CODE,
                                  USE_INBD_LPN_AS_OUTBD_LPN,
                                  PRINT_COO,
                                  PRINT_INV,
                                  PRINT_SED,
                                  PRINT_CANADIAN_CUST_INVC_FLAG,
                                  PRINT_DOCK_RCPT_FLAG,
                                  PRINT_NAFTA_COO_FLAG,
                                  PRINT_OCEAN_BOL_FLAG,
                                  PRINT_PKG_LIST_FLAG,
                                  PRINT_SHPR_LTR_OF_INSTR_FLAG,
                                  AUDIT_TRANSACTION,
                                  AUDIT_PARTY_ID,
                                  CAPTURE_OTHER_MA,
                                  HIBERNATE_VERSION,
                                  STORE_TYPE_GROUPING,
                                  LOAD_RATE,
                                  UNLOAD_RATE,
                                  HANDLING_RATE,
                                  LOAD_UNLOAD_SIZE_UOM_ID,
                                  LOAD_FACTOR_SIZE_UOM_ID,
                                  PARCEL_LENGTH_RATIO,
                                  PARCEL_WIDTH_RATIO,
                                  PARCEL_HEIGHT_RATIO,
                                  METER_NUMBER,
                                  DIRECT_DELIVERY_ALLOWED,
                                  TRACK_EQUIP_ID_FLAG,
                                  STAT_CODE,
                                  HANDLES_NON_MACHINEABLE,
                                  MAINTAINS_PERPETUAL_INVTY,
                                  IS_CUST_OWNED_FACILITY,
                                  FLOWTHRU_ALLOC_SORT_PRTY,
                                  RLS_HOLD_DATE,
                                  RE_COMPUTE_FEASIBLE_EQUIPMENT,
                                  SMARTPOST_DC_NUMBER,
                                  ADDRESS_TYPE,
                                  SPLC_CODE,
                                  ERPC_CODE,
                                  R260_CODE,
                                  MAX_WAIT_TIME,
                                  FREE_WAIT_TIME,
                                  IATA_SCR_NBR,
                                  PICKUP_AT_STORE,
                                  SHIP_TO_STORE,
                                  SHIP_FROM_FACILITY,
                                  TRANS_PLAN_FLOW,
                                  AVG_HNDLG_COST_PER_LN,
                                  AVG_HNDLG_COST_PER_LN_CURCODE,
                                  PENALTY_COST,
                                  PENALTY_COST_CURRENCY_CODE,
                                  MAINTAIN_DISPOSITION,
                                  MAINTAIN_SUBLOCATION,
                                  MAINTAIN_SEGMENT,
                                  MAINTAIN_CNTRY_OF_ORGN,
                                  MAINTAIN_BATCH_NBR,
                                  MAINTAIN_PROD_STAT,
                                  MAINTAIN_INV_TYPE,
                                  MAINTAIN_ITEM_ATTR_1,
                                  MAINTAIN_ITEM_ATTR_2,
                                  MAINTAIN_ITEM_ATTR_3,
                                  MAINTAIN_ITEM_ATTR_4,
                                  MAINTAIN_ITEM_ATTR_5,
                                  GENERATE_PERPETUAL_EVENT,
                                  GENERATE_SEGMENT_EVENT,
                                  MAINTAIN_ON_HAND,
                                  MAINTAIN_IN_TRANSIT,
                                  MAINTAIN_ON_ORDER,
                                  FULFILLMENT_GROUP,
                                  REF_FIELD_6,
                                  REF_FIELD_7,
                                  REF_FIELD_8,
                                  REF_FIELD_9,
                                  REF_FIELD_10,
                                  REF_NUM1,
                                  REF_NUM2,
                                  REF_NUM3,
                                  REF_NUM4,
                                  REF_NUM5,
                                  REF_FIELD_1,
                                  REF_FIELD_2,
                                  REF_FIELD_3,
                                  REF_FIELD_4,
                                  REF_FIELD_5,
                                  WHSE_REGION)
                 VALUES (v_facility_id,
                         v_facility_row.TC_COMPANY_ID,
                         v_facility_row.BUSINESS_PARTNER_ID,
                         v_facility_row.NAME,
                         v_facility_row.DESCRIPTION,
                         v_facility_row.ADDRESS_1,
                         v_facility_row.ADDRESS_2,
                         v_facility_row.ADDRESS_3,
                         v_facility_row.ADDRESS_KEY_1,
                         v_facility_row.CITY,
                         v_facility_row.STATE_PROV,
                         v_facility_row.POSTAL_CODE,
                         v_facility_row.COUNTY,
                         v_facility_row.COUNTRY_CODE,
                         v_facility_row.LONGITUDE,
                         v_facility_row.LATITUDE,
                         v_facility_row.EIN_NBR,
                         v_facility_row.IATA_CODE,
                         v_facility_row.GLOBAL_LOCN_NBR,
                         v_facility_row.TAX_ID,
                         v_facility_row.DUNS_NBR,
                         v_facility_row.INBOUNDS_RS_AREA_ID,
                         v_facility_row.OUTBOUND_RS_AREA_ID,
                         v_facility_row.INBOUND_REGION_ID,
                         v_facility_row.OUTBOUND_REGION_ID,
                         v_facility_row.FACILITY_TZ,
                         v_facility_row.DROP_INDICATOR,
                         v_facility_row.HOOK_INDICATOR,
                         v_facility_row.HANDLER,
                         v_facility_row.IS_SHIP_APPT_REQD,
                         v_facility_row.IS_RCV_APPT_REQD,
                         v_facility_row.TRACK_ONTIME_INDICATOR,
                         v_facility_row.ONTIME_PERF_METHOD,
                         v_facility_row.LOAD_FACTOR_SIZE_VALUE,
                         v_facility_row.MARK_FOR_DELETION,
                         v_facility_row.CREATED_SOURCE_TYPE,
                         v_facility_row.CREATED_SOURCE,
                         v_facility_row.CREATED_DTTM,
                         v_facility_row.LAST_UPDATED_SOURCE_TYPE,
                         v_facility_row.LAST_UPDATED_SOURCE,
                         v_facility_row.LAST_UPDATED_DTTM,
                         v_facility_row.ALLOW_OVERLAPPING_APPS,
                         v_facility_row.EARLY_TOLERANCE_POINT,
                         v_facility_row.LATE_TOLERANCE_POINT,
                         v_facility_row.EARLY_TOLERANCE_WINDOW,
                         v_facility_row.LATE_TOLERANCE_WINDOW,
                         v_facility_row.FACILITY_TYPE_BITS,
                         v_facility_row.PP_HANDLING_TIME,
                         v_facility_row.STOP_CONSTRAINT_BITS,
                         v_facility_row.IS_DOCK_SCHED_FAC,
                         v_facility_row.ASN_COMMUNICATION_METHOD,
                         v_facility_row.PICK_TIME,
                         v_facility_row.DISPATCH_INITIATION_TIME,
                         v_facility_row.PAPERWORK_TIME,
                         v_facility_row.VEHICLE_CHECK_TIME,
                         v_facility_row.VISIT_GAP,
                         v_facility_row.MIN_HANDLING_TIME,
                         v_facility_row.IS_CREDIT_AVAILABLE,
                         v_facility_row.BUSINESS_GROUP_ID_1,
                         v_facility_row.BUSINESS_GROUP_ID_2,
                         v_facility_row.IS_OPERATIONAL,
                         v_facility_row.IS_MAINTENANCE_FACILITY,
                         v_facility_row.DWELL_TIME,
                         v_facility_row.DRIVER_CHECK_IN_TIME,
                         v_facility_row.DISPATCH_DTTM,
                         v_facility_row.DSP_DR_CONFIGURATION_ID,
                         v_facility_row.DROP_HOOK_TIME,
                         v_facility_row.LOADING_END_TIME,
                         v_facility_row.DRIVER_DEBRIEF_TIME,
                         v_facility_row.ACCOUNT_CODE_NUMBER,
                         v_facility_row.LOAD_FACTOR_MOT_ID,
                         v_facility_row.LOAD_UNLOAD_MOT_ID,
                         v_facility_row.MIN_TRAIL_FILL,
                         v_facility_row.TANDEM_DROP_HOOK_TIME,
                         v_facility_row.STORE_TYPE,
                         v_facility_row.DRIVEIN_TIME,
                         v_facility_row.CHECKIN_TIME,
                         v_facility_row.DRIVEOUT_TIME,
                         v_facility_row.CHECKOUT_TIME,
                         v_facility_row.CUTOFF_DTTM,
                         v_facility_row.MAX_TRAILER_YARD_TIME,
                         v_facility_row.HAND_OVER_TIME,
                         v_facility_row.OVER_BOOK_PERCENTAGE,
                         v_facility_row.RECOMMENDATION_CRITERIA,
                         v_facility_row.NBR_OF_SLOTS_TO_SHOW,
                         v_facility_row.RESTRICT_FLEET_ASMT_TIME,
                         v_facility_row.GROUP_ID,
                         v_facility_row.IS_VTBP,
                         v_facility_row.AUTO_CREATE_SHIPMENT,
                         v_facility_row.LOGO_IMAGE_PATH,
                         v_facility_row.GEN_LAST_SHIPMENT_LEG,
                         v_facility_row.DEF_DRIVER_TYPE_ID,
                         v_facility_row.DEF_TRACTOR_ID,
                         v_facility_row.DEF_EQUIPMENT_ID,
                         v_facility_row.DRIVER_AVAIL_HRS,
                         v_facility_row.WHSE,
                         v_facility_row.OPEN_DATE,
                         v_facility_row.CLOSE_DATE,
                         v_facility_row.HOLD_DATE,
                         v_facility_row.GRP,
                         v_facility_row.CHAIN,
                         v_facility_row.ZONE,
                         v_facility_row.TERRITORY,
                         v_facility_row.REGION,
                         v_facility_row.DISTRICT,
                         v_facility_row.SHIP_MON,
                         v_facility_row.SHIP_TUE,
                         v_facility_row.SHIP_WED,
                         v_facility_row.SHIP_THU,
                         v_facility_row.SHIP_FRI,
                         v_facility_row.SHIP_SAT,
                         v_facility_row.SHIP_SU,
                         v_facility_row.ACCEPT_IRREG,
                         v_facility_row.WAVE_LABEL_TYPE,
                         v_facility_row.PKG_SLIP_TYPE,
                         v_facility_row.PRINT_CODE,
                         v_facility_row.CARTON_CNT_TYPE,
                         v_facility_row.SHIP_VIA,
                         v_facility_row.RTE_NBR,
                         v_facility_row.RTE_ATTR,
                         v_facility_row.RTE_TO,
                         v_facility_row.RTE_TYPE_1,
                         v_facility_row.RTE_TYPE_2,
                         v_facility_row.RTE_ZIP,
                         v_facility_row.SPL_INSTR_CODE_1,
                         v_facility_row.SPL_INSTR_CODE_2,
                         v_facility_row.SPL_INSTR_CODE_3,
                         v_facility_row.SPL_INSTR_CODE_4,
                         v_facility_row.SPL_INSTR_CODE_5,
                         v_facility_row.SPL_INSTR_CODE_6,
                         v_facility_row.SPL_INSTR_CODE_7,
                         v_facility_row.SPL_INSTR_CODE_8,
                         v_facility_row.SPL_INSTR_CODE_9,
                         v_facility_row.SPL_INSTR_CODE_10,
                         v_facility_row.ASSIGN_MERCH_TYPE,
                         v_facility_row.ASSIGN_MERCH_GROUP,
                         v_facility_row.ASSIGN_STORE_DEPT,
                         v_facility_row.CARTON_LABEL_TYPE,
                         v_facility_row.CARTON_CUBNG_INDIC,
                         v_facility_row.MAX_CTN,
                         v_facility_row.MAX_PLT,
                         v_facility_row.BUSN_UNIT_CODE,
                         v_facility_row.USE_INBD_LPN_AS_OUTBD_LPN,
                         v_facility_row.PRINT_COO,
                         v_facility_row.PRINT_INV,
                         v_facility_row.PRINT_SED,
                         v_facility_row.PRINT_CANADIAN_CUST_INVC_FLAG,
                         v_facility_row.PRINT_DOCK_RCPT_FLAG,
                         v_facility_row.PRINT_NAFTA_COO_FLAG,
                         v_facility_row.PRINT_OCEAN_BOL_FLAG,
                         v_facility_row.PRINT_PKG_LIST_FLAG,
                         v_facility_row.PRINT_SHPR_LTR_OF_INSTR_FLAG,
                         v_facility_row.AUDIT_TRANSACTION,
                         v_facility_row.AUDIT_PARTY_ID,
                         v_facility_row.CAPTURE_OTHER_MA,
                         v_facility_row.HIBERNATE_VERSION,
                         v_facility_row.STORE_TYPE_GROUPING,
                         v_facility_row.LOAD_RATE,
                         v_facility_row.UNLOAD_RATE,
                         v_facility_row.HANDLING_RATE,
                         v_facility_row.LOAD_UNLOAD_SIZE_UOM_ID,
                         v_facility_row.LOAD_FACTOR_SIZE_UOM_ID,
                         v_facility_row.PARCEL_LENGTH_RATIO,
                         v_facility_row.PARCEL_WIDTH_RATIO,
                         v_facility_row.PARCEL_HEIGHT_RATIO,
                         v_facility_row.METER_NUMBER,
                         v_facility_row.DIRECT_DELIVERY_ALLOWED,
                         v_facility_row.TRACK_EQUIP_ID_FLAG,
                         v_facility_row.STAT_CODE,
                         v_facility_row.HANDLES_NON_MACHINEABLE,
                         v_facility_row.MAINTAINS_PERPETUAL_INVTY,
                         v_facility_row.IS_CUST_OWNED_FACILITY,
                         v_facility_row.FLOWTHRU_ALLOC_SORT_PRTY,
                         v_facility_row.RLS_HOLD_DATE,
                         v_facility_row.RE_COMPUTE_FEASIBLE_EQUIPMENT,
                         v_facility_row.SMARTPOST_DC_NUMBER,
                         v_facility_row.ADDRESS_TYPE,
                         v_facility_row.SPLC_CODE,
                         v_facility_row.ERPC_CODE,
                         v_facility_row.R260_CODE,
                         v_facility_row.MAX_WAIT_TIME,
                         v_facility_row.FREE_WAIT_TIME,
                         v_facility_row.IATA_SCR_NBR,
                         v_facility_row.PICKUP_AT_STORE,
                         v_facility_row.SHIP_TO_STORE,
                         v_facility_row.SHIP_FROM_FACILITY,
                         v_facility_row.TRANS_PLAN_FLOW,
                         v_facility_row.AVG_HNDLG_COST_PER_LN,
                         v_facility_row.AVG_HNDLG_COST_PER_LN_CURCODE,
                         v_facility_row.PENALTY_COST,
                         v_facility_row.PENALTY_COST_CURRENCY_CODE,
                         v_facility_row.MAINTAIN_DISPOSITION,
                         v_facility_row.MAINTAIN_SUBLOCATION,
                         v_facility_row.MAINTAIN_SEGMENT,
                         v_facility_row.MAINTAIN_CNTRY_OF_ORGN,
                         v_facility_row.MAINTAIN_BATCH_NBR,
                         v_facility_row.MAINTAIN_PROD_STAT,
                         v_facility_row.MAINTAIN_INV_TYPE,
                         v_facility_row.MAINTAIN_ITEM_ATTR_1,
                         v_facility_row.MAINTAIN_ITEM_ATTR_2,
                         v_facility_row.MAINTAIN_ITEM_ATTR_3,
                         v_facility_row.MAINTAIN_ITEM_ATTR_4,
                         v_facility_row.MAINTAIN_ITEM_ATTR_5,
                         v_facility_row.GENERATE_PERPETUAL_EVENT,
                         v_facility_row.GENERATE_SEGMENT_EVENT,
                         v_facility_row.MAINTAIN_ON_HAND,
                         v_facility_row.MAINTAIN_IN_TRANSIT,
                         v_facility_row.MAINTAIN_ON_ORDER,
                         v_facility_row.FULFILLMENT_GROUP,
                         v_facility_row.REF_FIELD_6,
                         v_facility_row.REF_FIELD_7,
                         v_facility_row.REF_FIELD_8,
                         v_facility_row.REF_FIELD_9,
                         v_facility_row.REF_FIELD_10,
                         v_facility_row.REF_NUM1,
                         v_facility_row.REF_NUM2,
                         v_facility_row.REF_NUM3,
                         v_facility_row.REF_NUM4,
                         v_facility_row.REF_NUM5,
                         v_facility_row.REF_FIELD_1,
                         v_facility_row.REF_FIELD_2,
                         v_facility_row.REF_FIELD_3,
                         v_facility_row.REF_FIELD_4,
                         v_facility_row.REF_FIELD_5,
                         REGION_ID_SEQ.CURRVAL);

            INSERT INTO FACILITY_ALIAS (FACILITY_ALIAS_ID,
                                        FACILITY_ALIAS_ID_U,
                                        FACILITY_ID,
                                        TC_COMPANY_ID,
                                        FACILITY_NAME,
                                        IS_PRIMARY,
                                        MARK_FOR_DELETION,
                                        CREATED_SOURCE_TYPE,
                                        CREATED_SOURCE,
                                        CREATED_DTTM,
                                        LAST_UPDATED_SOURCE_TYPE,
                                        LAST_UPDATED_SOURCE,
                                        LAST_UPDATED_DTTM,
                                        TC_SHIPMENT_ID_PREFIX,
                                        FACILITY_SEQ,
                                        ADDRESS_1,
                                        CITY,
                                        STATE_PROV,
                                        POSTAL_CODE,
                                        COUNTY,
                                        COUNTRY_CODE,
                                        CARRIER_ID,
                                        MOT_ID,
                                        ADDRESS_2,
                                        ADDRESS_3,
                                        AUDIT_TRANSACTION,
                                        AUDIT_PARTY_ID,
                                        HIBERNATE_VERSION)
                    VALUES (TO_CHAR('0' || V_FACILITY_ID),
                            TO_CHAR('0' || V_FACILITY_ID),
                            V_FACILITY_ID,
                            V_FACILITY_ROW.TC_COMPANY_ID,
                            V_FACILITY_ROW.NAME,
                            1,
                            V_FACILITY_ROW.MARK_FOR_DELETION,
                            V_FACILITY_ROW.CREATED_SOURCE_TYPE,
                            V_FACILITY_ROW.CREATED_SOURCE,
                            V_FACILITY_ROW.CREATED_DTTM,
                            V_FACILITY_ROW.LAST_UPDATED_SOURCE_TYPE,
                            V_FACILITY_ROW.LAST_UPDATED_SOURCE,
                            V_FACILITY_ROW.LAST_UPDATED_DTTM,
                            NULL,
                            2,
                            V_FACILITY_ROW.ADDRESS_1,
                            V_FACILITY_ROW.CITY,
                            V_FACILITY_ROW.STATE_PROV,
                            V_FACILITY_ROW.POSTAL_CODE,
                            V_FACILITY_ROW.COUNTY,
                            V_FACILITY_ROW.COUNTRY_CODE,
                            NULL,
                            NULL,
                            V_FACILITY_ROW.ADDRESS_2, 
                            V_FACILITY_ROW.ADDRESS_3, 
                            V_FACILITY_ROW.AUDIT_TRANSACTION,
                            V_FACILITY_ROW.AUDIT_PARTY_ID,
                            V_FACILITY_ROW.HIBERNATE_VERSION);

            INSERT INTO FAC_REGION (FACILITY_REGION_ID,
                                    FACILITY_ID,
                                    REGION_ID,
                                    DIRECTION,
                                    TC_COMPANY_ID,
                                    CREATED_DTTM,
                                    LAST_UPDATED_DTTM)
                    VALUES (
                              SEQ_FACILITY_REGION_ID.NEXTVAL,
                              V_FACILITY_ID,
                              REGION_ID_SEQ.CURRVAL,
                              'I',
                              V_FACILITY_ROW.TC_COMPANY_ID,
                              V_FACILITY_ROW.CREATED_DTTM,
                              V_FACILITY_ROW.LAST_UPDATED_DTTM);

            INSERT INTO FAC_REGION (FACILITY_REGION_ID,
                                    FACILITY_ID,
                                    REGION_ID,
                                    DIRECTION,
                                    TC_COMPANY_ID,
                                    CREATED_DTTM,
                                    LAST_UPDATED_DTTM)
                    VALUES (
                              SEQ_FACILITY_REGION_ID.NEXTVAL,
                              V_FACILITY_ID,
                              REGION_ID_SEQ.CURRVAL,
                              'O',
                              V_FACILITY_ROW.TC_COMPANY_ID,
                              V_FACILITY_ROW.CREATED_DTTM,
                              V_FACILITY_ROW.LAST_UPDATED_DTTM);

         END LOOP;

         SELECT NVL (MAX (ITEM_FACILITY_MAPPING_ID), 0)
           INTO v_max_facility_mapping_id
           FROM ITEM_FACILITY_MAPPING_WMS;

         INSERT INTO ITEM_FACILITY_MAPPING_WMS (ITEM_FACILITY_MAPPING_ID,
                                                ITEM_ID,
                                                FACILITY_ID,
                                                VENDOR_TIER_PER_PLT,
                                                VENDOR_CARTON_PER_TIER,
                                                TIER_PER_PLT,
                                                ORD_TIER_PER_PLT,
                                                ORD_CARTON_PER_TIER,
                                                LPN_PER_TIER,
                                                SLOT_ROTATE_EACHES_FLAG,
                                                SLOT_ROTATE_INNERS_FLAG,
                                                SLOT_ROTATE_BINS_FLAG,
                                                SLOT_ROTATE_CASES_FLAG,
                                                SLOT_NEST_EACHES_FLAG,
                                                SLOT_INCR_HT,
                                                SLOT_INCR_LEN,
                                                SLOT_INCR_WIDTH,
                                                SLOT_3D_SLOTTING_FLAG,
                                                DFLT_WAVE_PROC_TYPE,
                                                ALLOC_TYPE,
                                                AUDIT_CREATED_SOURCE,
                                                SLOT_MISC_1,
                                                SLOT_MISC_2,
                                                SLOT_MISC_3,
                                                SLOT_MISC_4,
                                                SLOT_MISC_5,
                                                SLOT_MISC_6)
            SELECT v_max_facility_mapping_id + SIM.SKU_ID,
                   v_max_item_id + SIM.SKU_ID,
                   F.FACILITY_ID,
                   SIM.VEN_HI,
                   SIM.VEN_TI,
                   SIM.WH_HI,
                   SIM.ORD_HI,
                   SIM.ORD_TI,
                   SIM.WH_TI,
                   SIM.ALLOW_ROTATE_EACH,
                   SIM.ALLOW_ROTATE_INNER,
                   SIM.ALLOW_ROTATE_BIN,
                   SIM.ALLOW_ROTATE_CASE,
                   SIM.ALLOW_NEST_EACH,
                   SIM.INCREMENTAL_HEIGHT,
                   SIM.INCREMENTAL_LENGTH,
                   SIM.INCREMENTAL_WIDTH,
                   SIM.USE_3D_SLOT,
                   3,
                   'BLK',
                   NVL(SIM.MOD_USER, 'WMADMIN'),
                   (SELECT cat_code.cat_code FROM item_cat_xref, cat_code WHERE item_cat_xref.sku_id = (v_max_facility_mapping_id + SIM.SKU_ID) AND item_cat_xref.cat_id = (1) AND item_cat_xref.cat_id = cat_code.cat_id AND item_cat_xref.cat_code_id = cat_code.cat_code_id),
                   (SELECT cat_code.cat_code FROM item_cat_xref, cat_code WHERE item_cat_xref.sku_id = (v_max_facility_mapping_id + SIM.SKU_ID) AND item_cat_xref.cat_id = (2) AND item_cat_xref.cat_id = cat_code.cat_id AND item_cat_xref.cat_code_id = cat_code.cat_code_id),
                   (SELECT cat_code.cat_code FROM item_cat_xref, cat_code WHERE item_cat_xref.sku_id = (v_max_facility_mapping_id + SIM.SKU_ID) AND item_cat_xref.cat_id = (8) AND item_cat_xref.cat_id = cat_code.cat_id AND item_cat_xref.cat_code_id = cat_code.cat_code_id),
                   (SELECT cat_code.cat_code FROM item_cat_xref, cat_code WHERE item_cat_xref.sku_id = (v_max_facility_mapping_id + SIM.SKU_ID) AND item_cat_xref.cat_id = (9) AND item_cat_xref.cat_id = cat_code.cat_id AND item_cat_xref.cat_code_id = cat_code.cat_code_id),
                   (SELECT cat_code.cat_code FROM item_cat_xref, cat_code WHERE item_cat_xref.sku_id = (v_max_facility_mapping_id + SIM.SKU_ID) AND item_cat_xref.cat_id = (10) AND item_cat_xref.cat_id = cat_code.cat_id AND item_cat_xref.cat_code_id = cat_code.cat_code_id),
                   (SELECT cat_code.cat_code FROM item_cat_xref, cat_code WHERE item_cat_xref.sku_id = (v_max_facility_mapping_id + SIM.SKU_ID) AND item_cat_xref.cat_id = (11) AND item_cat_xref.cat_id = cat_code.cat_id AND item_cat_xref.cat_code_id = cat_code.cat_code_id)
              FROM SO_ITEM_MASTER_UNUSED SIM
                   INNER JOIN FACILITY F ON F.WHSE = SIM.WHSE_CODE;

         SELECT SIZE_UOM.SIZE_UOM_ID
           INTO v_case_uom_id
           FROM SIZE_UOM SIZE_UOM, STANDARD_UOM STD_UOM
          WHERE     SIZE_UOM.STANDARD_UOM = STD_UOM.STANDARD_UOM
                AND STD_UOM.ABBREVIATION = 'C';

         SELECT SIZE_UOM.SIZE_UOM_ID
           INTO v_pack_uom_id
           FROM SIZE_UOM SIZE_UOM, STANDARD_UOM STD_UOM
          WHERE     SIZE_UOM.STANDARD_UOM = STD_UOM.STANDARD_UOM
                AND STD_UOM.ABBREVIATION = 'K';

         INSERT INTO ITEM_PACKAGE_CBO (ITEM_PACKAGE_ID,
                                       ITEM_ID,
                                       PACKAGE_UOM_ID,
                                       WIDTH,
                                       LENGTH,
                                       HEIGHT,
                                       WEIGHT,
                                       QUANTITY,
                                       IS_STD,
                                       MARK_FOR_DELETION)
            SELECT ITEM_PACKAGE_ID_SEQ.NEXTVAL,
                   v_max_item_id + SKU_ID,
                   v_case_uom_id,
                   CASE_WID,
                   CASE_LEN,
                   CASE_HT,
                   CASE_WT,
                   NVL(EACH_PER_CS,1),
                   '1',
                   0
              FROM SO_ITEM_MASTER_UNUSED
             WHERE COALESCE (CASE_WID,
                             CASE_LEN,
                             CASE_HT,
                             CASE_WT)
                      IS NOT NULL;

         INSERT INTO ITEM_PACKAGE_CBO (ITEM_PACKAGE_ID,
                                       ITEM_ID,
                                       PACKAGE_UOM_ID,
                                       WIDTH,
                                       LENGTH,
                                       HEIGHT,
                                       WEIGHT,
                                       QUANTITY,
                                       IS_STD,
                                       MARK_FOR_DELETION)
            SELECT ITEM_PACKAGE_ID_SEQ.NEXTVAL,
                   v_max_item_id + SKU_ID,
                   v_pack_uom_id,
                   INN_WID,
                   INN_LEN,
                   INN_HT,
                   INN_WT,
                   NVL(EACH_PER_INN,1),
                   '1',
                   0
              FROM SO_ITEM_MASTER_UNUSED
             WHERE COALESCE (INN_WID,
                             INN_LEN,
                             INN_HT,
                             INN_WT
                             )
                      IS NOT NULL;

         INSERT INTO ITEM_FACILITY_SLOTTING (ITEM_FACILITY_MAPPING_ID,
                                             ITEM_ID,
                                             ITEM_NAME,
                                             WHSE_CODE,
                                             UNIT_MEAS,
                                             MANUAL,
                                             IS_NEW,
                                             ALLOW_SLU,
                                             ALLOW_SU,
                                             DISC_TRANS,
                                             EACH_PER_BIN,
                                             EX_RECPT_DATE,
                                             DISCONT,
                                             PROCESSED,
                                             INN_PER_CS,
                                             MAX_STACKING,
                                             MAX_LANES,
                                             NUM_UNITS_PER_BIN,
                                             RESERVED_1,
                                             RESERVED_2,
                                             RESERVED_3,
                                             RESERVED_4,
                                             MOD_USER,
                                             CREATE_DATE_TIME,
                                             MOD_DATE_TIME,
                                             RESERVE_RACK_TYPE_ID,
                                             ALLOW_RESERVE,
                                             PALPAT_RESERVE,
                                             THREE_D_CALC_DONE,
                                             MAX_SLOTS,
                                             MAX_PALLET_STACKING,
                                             PROP_BORROWING_OBJECT,
                                             PROP_BORROWING_SPECIFIC,
                                             HEIGHT_CAN_BORROW,
                                             LENGTH_CAN_BORROW,
                                             WIDTH_CAN_BORROW,
                                             WEIGHT_CAN_BORROW,
                                             VEND_PACK_CAN_BORROW,
                                             INNER_PACK_CAN_BORROW,
                                             TI_CAN_BORROW,
                                             HI_CAN_BORROW,
                                             QTY_PER_GRAB_CAN_BORROW,
                                             HAND_ATTR_CAN_BORROW,
                                             FAM_GRP_CAN_BORROW,
                                             COMMODITY_CAN_BORROW,
                                             CRUSHABILITY_CAN_BORROW,
                                             HAZARD_CAN_BORROW,
                                             VEND_CODE_CAN_BORROW,
                                             MISC1_CAN_BORROW,
                                             MISC2_CAN_BORROW,
                                             MISC3_CAN_BORROW,
                                             MISC4_CAN_BORROW,
                                             MISC5_CAN_BORROW,
                                             MISC6_CAN_BORROW,
                                             HEIGHT_BORROWED,
                                             LENGTH_BORROWED,
                                             WIDTH_BORROWED,
                                             WEIGHT_BORROWED,
                                             VEND_PACK_BORROWED,
                                             INNER_PACK_BORROWED,
                                             TI_BORROWED,
                                             HI_BORROWED,
                                             QTY_PER_GRAB_BORROWED,
                                             HAND_ATTR_BORROWED,
                                             FAM_GRP_BORROWED,
                                             COMMODITY_BORROWED,
                                             CRUSHABILITY_BORROWED,
                                             HAZARD_BORROWED,
                                             VEND_CODE_BORROWED,
                                             MISC1_BORROWED,
                                             MISC2_BORROWED,
                                             MISC3_BORROWED,
                                             MISC4_BORROWED,
                                             MISC5_BORROWED,
                                             MISC6_BORROWED,
                                             AFRAME_HT,
                                             AFRAME_LEN,
                                             AFRAME_WID,
                                             AFRAME_WT,
                                             AFRAME_ALLOW,
                                             ITEM_NUM_1,
                                             ITEM_NUM_2,
                                             ITEM_NUM_3,
                                             ITEM_NUM_4,
                                             ITEM_NUM_5,
                                             ITEM_CHAR_1,
                                             ITEM_CHAR_2,
                                             ITEM_CHAR_3,
                                             ITEM_CHAR_4,
                                             ITEM_CHAR_5)
            SELECT IFMW.ITEM_FACILITY_MAPPING_ID,
                   v_max_item_id + SIM.SKU_ID,
                   SIM.SKU_NAME,
                   SIM.WHSE_CODE,
                   SIM.UNIT_MEAS,
                   SIM.MANUAL,
                   SIM.IS_NEW,
                   SIM.ALLOW_SLU,
                   SIM.ALLOW_SU,
                   SIM.DISC_TRANS,
                   SIM.EACH_PER_BIN,
                   SIM.EX_RECPT_DATE,
                   SIM.DISCONT,
                   SIM.PROCESSED,
                   SIM.INN_PER_CS,
                   SIM.MAX_STACKING,
                   SIM.MAX_LANES,
                   SIM.NUM_UNITS_PER_BIN,
                   SIM.RESERVED_1,
                   SIM.RESERVED_2,
                   SIM.RESERVED_3,
                   SIM.RESERVED_4,
                   SIM.MOD_USER,
                   SIM.CREATE_DATE_TIME,
                   SIM.MOD_DATE_TIME,
                   SIM.RESERVE_RACK_TYPE_ID,
                   SIM.ALLOW_RESERVE,
                   SIM.PALPAT_RESERVE,
                   SIM.THREE_D_CALC_DONE,
                   SIM.MAX_SLOTS,
                   SIM.MAX_PALLET_STACKING,
                   SIM.PROP_BORROWING_OBJECT,
                   SIM.PROP_BORROWING_SPECIFIC,
                   SIM.HEIGHT_CAN_BORROW,
                   SIM.LENGTH_CAN_BORROW,
                   SIM.WIDTH_CAN_BORROW,
                   SIM.WEIGHT_CAN_BORROW,
                   SIM.VEND_PACK_CAN_BORROW,
                   SIM.INNER_PACK_CAN_BORROW,
                   SIM.TI_CAN_BORROW,
                   SIM.HI_CAN_BORROW,
                   SIM.QTY_PER_GRAB_CAN_BORROW,
                   SIM.HAND_ATTR_CAN_BORROW,
                   SIM.FAM_GRP_CAN_BORROW,
                   SIM.COMMODITY_CAN_BORROW,
                   SIM.CRUSHABILITY_CAN_BORROW,
                   SIM.HAZARD_CAN_BORROW,
                   SIM.VEND_CODE_CAN_BORROW,
                   SIM.MISC1_CAN_BORROW,
                   SIM.MISC2_CAN_BORROW,
                   SIM.MISC3_CAN_BORROW,
                   SIM.MISC4_CAN_BORROW,
                   SIM.MISC5_CAN_BORROW,
                   SIM.MISC6_CAN_BORROW,
                   SIM.HEIGHT_BORROWED,
                   SIM.LENGTH_BORROWED,
                   SIM.WIDTH_BORROWED,
                   SIM.WEIGHT_BORROWED,
                   SIM.VEND_PACK_BORROWED,
                   SIM.INNER_PACK_BORROWED,
                   SIM.TI_BORROWED,
                   SIM.HI_BORROWED,
                   SIM.QTY_PER_GRAB_BORROWED,
                   SIM.HAND_ATTR_BORROWED,
                   SIM.FAM_GRP_BORROWED,
                   SIM.COMMODITY_BORROWED,
                   SIM.CRUSHABILITY_BORROWED,
                   SIM.HAZARD_BORROWED,
                   SIM.VEND_CODE_BORROWED,
                   SIM.MISC1_BORROWED,
                   SIM.MISC2_BORROWED,
                   SIM.MISC3_BORROWED,
                   SIM.MISC4_BORROWED,
                   SIM.MISC5_BORROWED,
                   SIM.MISC6_BORROWED,
                   SIM.AFRAME_HT,
                   SIM.AFRAME_LEN,
                   SIM.AFRAME_WID,
                   SIM.AFRAME_WT,
                   SIM.AFRAME_ALLOW,
                   ITEM_NUM_1,
                   ITEM_NUM_2,
                   ITEM_NUM_3,
                   ITEM_NUM_4,
                   ITEM_NUM_5,
                   ITEM_CHAR_1,
                   ITEM_CHAR_2,
                   ITEM_CHAR_3,
                   ITEM_CHAR_4,
                   ITEM_CHAR_5
              FROM SO_ITEM_MASTER_UNUSED SIM
                   INNER JOIN ITEM_FACILITY_MAPPING_WMS IFMW
                      ON IFMW.ITEM_ID = v_max_item_id + SIM.SKU_ID
                   INNER JOIN FACILITY F ON F.FACILITY_ID = IFMW.FACILITY_ID
             WHERE SIM.WHSE_CODE = F.WHSE;

         UPDATE BIN_XREF
            SET SKU_ID = v_max_facility_mapping_id + SKU_ID;

         DELETE FROM BIN_XREF
               WHERE     SKU_ID IS NOT NULL
                     AND SKU_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                          FROM ITEM_FACILITY_MAPPING_WMS);

         UPDATE ITEM_CAT_XREF
            SET SKU_ID = v_max_facility_mapping_id + SKU_ID;

         DELETE FROM ITEM_CAT_XREF
               WHERE     SKU_ID IS NOT NULL
                     AND SKU_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                          FROM ITEM_FACILITY_MAPPING_WMS);

         UPDATE MOVE_LIST
            SET ITEM_ID = v_max_facility_mapping_id + ITEM_ID;

         DELETE FROM MOVE_LIST
               WHERE     ITEM_ID IS NOT NULL
                     AND ITEM_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                           FROM ITEM_FACILITY_MAPPING_WMS);

         UPDATE ORDER_DTL
            SET SKU_ID = v_max_facility_mapping_id + SKU_ID;

         DELETE FROM ORDER_DTL
               WHERE     SKU_ID IS NOT NULL
                     AND SKU_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                          FROM ITEM_FACILITY_MAPPING_WMS);

         UPDATE PALLET_XREF
            SET SKU_ID = v_max_facility_mapping_id + SKU_ID;

         DELETE FROM PALLET_XREF
               WHERE     SKU_ID IS NOT NULL
                     AND SKU_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                          FROM ITEM_FACILITY_MAPPING_WMS);

         UPDATE RESERVE_BIN_XREF
            SET RES_SKU_ID = v_max_facility_mapping_id + RES_SKU_ID;

         DELETE FROM RESERVE_BIN_XREF
               WHERE     RES_SKU_ID IS NOT NULL
                     AND RES_SKU_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                              FROM ITEM_FACILITY_MAPPING_WMS);

         UPDATE RESERVE_PALLET_XREF
            SET RES_SKU_ID = v_max_facility_mapping_id + RES_SKU_ID;

         DELETE FROM RESERVE_PALLET_XREF
               WHERE     RES_SKU_ID IS NOT NULL
                     AND RES_SKU_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                              FROM ITEM_FACILITY_MAPPING_WMS);

         UPDATE SHELF_SLOT_XREF
            SET SKU_ID = v_max_facility_mapping_id + SKU_ID;

         DELETE FROM SHELF_SLOT_XREF
               WHERE     SKU_ID IS NOT NULL
                     AND SKU_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                          FROM ITEM_FACILITY_MAPPING_WMS);

         UPDATE SLOT_ITEM
            SET SKU_ID = v_max_facility_mapping_id + SKU_ID;

         DELETE FROM SLOT_ITEM
               WHERE     SKU_ID IS NOT NULL
                     AND SKU_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                          FROM ITEM_FACILITY_MAPPING_WMS);

         UPDATE WA_DETAIL
            SET SKU_ID = v_max_facility_mapping_id + SKU_ID;

         DELETE FROM WA_DETAIL
               WHERE     SKU_ID IS NOT NULL
                     AND SKU_ID NOT IN (SELECT ITEM_FACILITY_MAPPING_ID
                                          FROM ITEM_FACILITY_MAPPING_WMS);

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            ROLLBACK;
            RAISE;
      END;
   END;

   PROCEDURE COPY_SLOT_DATA
   IS
      v_max_location_id        NUMBER;
      v_max_locn_hdr_id        NUMBER;
      v_max_pick_locn_hdr_id   NUMBER;
   BEGIN
      BEGIN
         -- insert dummy records into LOCATION
         SELECT NVL (MAX (LOCATION_ID), 0)
           INTO v_max_location_id
           FROM LOCATION;


         INSERT INTO LOCATION (LOCATION_ID,
                               ILM_OBJECT_TYPE,
                               TC_COMPANY_ID,
                               LOCATION_OBJID_PK1,
                               MARK_FOR_DELETION,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
            SELECT v_max_location_id + S.SLOT_ID,
                   20,
                   1,
                   44,
                   0,
                   S.CREATE_DATE_TIME,
                   S.MOD_DATE_TIME
              FROM SLOT_UNUSED S
                   LEFT OUTER JOIN LOCATION L ON S.SLOT_ID = L.LOCATION_ID;

         SELECT NVL (MAX (LOCN_HDR_ID), 0)
           INTO v_max_locn_hdr_id
           FROM LOCN_HDR;


         INSERT INTO LOCN_HDR (LOCN_HDR_ID,
                               WHSE,
                               AREA,
                               ZONE,
                               AISLE,
                               BAY,
                               LVL,
                               POSN,
                               HT,
                               WIDTH,
                               LEN,
                               X_COORD,
                               Y_COORD,
                               Z_COORD,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM,
                               SLOT_TYPE,
                               TRAVEL_ZONE,
                               TRAVEL_AISLE,
                               LOCN_CLASS,
                               LOCN_ID,
                               LOCN_BRCD,
                               CHECK_DIGIT,
                               FACILITY_ID,
                               DSP_LOCN,
                               SKU_DEDCTN_TYPE,
                               PICK_DETRM_ZONE)
            SELECT v_max_locn_hdr_id + S.SLOT_ID,
                   S.WHSE_CODE,
                   S.AREA,
                   S.ZONE,
                   S.AISLE,
                   S.BAY,
                   S.LVL,
                   S.POSN,
                   S.HT,
                   S.WIDTH,
                   S.DEPTH,
                   NVL (S.X_COORD, 0),
                   NVL (S.Y_COORD, 0),
                   NVL (S.Z_COORD, 0),
                   S.CREATE_DATE_TIME,
                   S.MOD_DATE_TIME,
                   S.SLOT_TYPE,
                   SUBSTR (S.TRAVEL_ZONE, 1, 4),
                   SUBSTR (S.TRAVEL_AISLE, 1, 4),
                   NVL (S.LOCATION_CLASS, 'Y'),
                   v_max_locn_hdr_id + S.SLOT_ID,
                   'BC' || S.SLOT_ID,
                   ' ',
                   F.FACILITY_ID,
                   S.DSP_SLOT,
                   'P',
                   0
              FROM SLOT_UNUSED S
                   INNER JOIN FACILITY F ON S.WHSE_CODE = F.WHSE;

         SELECT NVL (MAX (PICK_LOCN_HDR_ID), 0)
           INTO v_max_pick_locn_hdr_id
           FROM PICK_LOCN_HDR;

         INSERT INTO PICK_LOCN_HDR (PICK_LOCN_HDR_ID,
                                    LOCN_HDR_ID,
                                    MAX_WT,
                                    LOCN_PUTAWAY_LOCK,
                                    LOCN_ID)
            SELECT v_max_pick_locn_hdr_id + SLOT_ID,
                   v_max_locn_hdr_id + SLOT_ID,
                   WT_LIMIT,
                   LOCK_REASON,
                   v_max_location_id + SLOT_ID
              FROM SLOT_UNUSED;

         INSERT INTO PICK_LOCN_HDR_SLOTTING (PICK_LOCN_HDR_ID,
                                             RACK_TYPE,
                                             RACK_LEVEL_ID,
                                             HT_OVERRIDE,
                                             WIDTH_OVERRIDE,
                                             DEPTH_OVERRIDE,
                                             MAX_LN_OVR,
                                             MAX_ST_OVR,
                                             MAX_SC_OVR,
                                             MAX_HC_OVR,
                                             MAX_LW_OVR,
                                             MAX_STACK,
                                             MAX_SIDE_CLEAR,
                                             MAX_HT_CLEAR,
                                             MAX_LANE_WT,
                                             PROCESSED,
                                             LOCKED,
                                             WT_LIMIT_OVERRIDE,
                                             LABEL_POS,
                                             LABEL_POS_OVR,
                                             LEFT_SLOT,
                                             RIGHT_SLOT,
                                             MAX_LANES,
                                             MY_SNS,
                                             MY_RANGE,
                                             ALLOW_EXPAND,
                                             ALLOW_EXPAND_OVR,
                                             OLD_REC_SLOT_WIDTH,
                                             SIDE_OF_AISLE,
                                             RESERVED_1,
                                             RESERVED_2,
                                             RESERVED_3,
                                             RESERVED_4,
                                             LAST_UPDATED_SOURCE,
                                             CREATED_DTTM,
                                             LAST_UPDATED_DTTM,
                                             SLOT_PRIORITY,
                                             REACH_DIST,
                                             REACH_DIST_OVERRIDE,
                                             ALLOW_EXPAND_RGT,
                                             ALLOW_EXPAND_LFT,
                                             ALLOW_EXPAND_RGT_OVR,
                                             ALLOW_EXPAND_LFT_OVR)
            SELECT v_max_pick_locn_hdr_id + SLOT_ID,
                   RACK_TYPE,
                   RACK_LEVEL_ID,
                   HT_OVERRIDE,
                   WIDTH_OVERRIDE,
                   DEPTH_OVERRIDE,
                   MAX_LN_OVR,
                   MAX_ST_OVR,
                   MAX_SC_OVR,
                   MAX_HC_OVR,
                   MAX_LW_OVR,
                   MAX_STACK,
                   MAX_SIDE_CLEAR,
                   MAX_HT_CLEAR,
                   MAX_LANE_WT,
                   PROCESSED,
                   LOCKED,
                   WT_LIMIT_OVERRIDE,
                   LABEL_POS,
                   LABEL_POS_OVR,
                   v_max_pick_locn_hdr_id + LEFT_SLOT,
                   v_max_pick_locn_hdr_id + RIGHT_SLOT,
                   MAX_LANES,
                   MY_SNS,
                   MY_RANGE,
                   ALLOW_EXPAND,
                   ALLOW_EXPAND_OVR,
                   OLD_REC_SLOT_WIDTH,
                   SIDE_OF_AISLE,
                   RESERVED_1,
                   RESERVED_2,
                   RESERVED_3,
                   RESERVED_4,
                   MOD_USER,
                   CREATE_DATE_TIME,
                   MOD_DATE_TIME,
                   SLOT_PRIORITY,
                   REACH_DIST,
                   REACH_DIST_OVERRIDE,
                   ALLOW_EXPAND_RGT,
                   ALLOW_EXPAND_LFT,
                   ALLOW_EXPAND_RGT_OVR,
                   ALLOW_EXPAND_LFT_OVR
              FROM SLOT_UNUSED;

         UPDATE MOVE_LIST
            SET SRC_SLOT_ID = v_max_pick_locn_hdr_id + SRC_SLOT_ID;

         DELETE FROM MOVE_LIST
               WHERE     SRC_SLOT_ID IS NOT NULL
                     AND SRC_SLOT_ID NOT IN (SELECT PICK_LOCN_HDR_ID
                                               FROM PICK_LOCN_HDR_SLOTTING);

         UPDATE MOVE_LIST
            SET DEST_SLOT_ID = v_max_pick_locn_hdr_id + DEST_SLOT_ID;

         DELETE FROM MOVE_LIST
               WHERE     DEST_SLOT_ID IS NOT NULL
                     AND DEST_SLOT_ID NOT IN (SELECT PICK_LOCN_HDR_ID
                                                FROM PICK_LOCN_HDR_SLOTTING);

         UPDATE SHELF_SLOT_XREF
            SET SLOT_ID = v_max_pick_locn_hdr_id + SLOT_ID;

         DELETE FROM SHELF_SLOT_XREF
               WHERE     SLOT_ID IS NOT NULL
                     AND SLOT_ID NOT IN (SELECT PICK_LOCN_HDR_ID
                                           FROM PICK_LOCN_HDR_SLOTTING);

         UPDATE SLOT_ITEM
            SET SLOT_ID = v_max_pick_locn_hdr_id + SLOT_ID;

         DELETE FROM SLOT_ITEM
               WHERE     SLOT_ID IS NOT NULL
                     AND SLOT_ID NOT IN (SELECT PICK_LOCN_HDR_ID
                                           FROM PICK_LOCN_HDR_SLOTTING);

         UPDATE WA_DETAIL
            SET SLOT_ID = v_max_pick_locn_hdr_id + SLOT_ID;

         DELETE FROM WA_DETAIL
               WHERE     SLOT_ID IS NOT NULL
                     AND SLOT_ID NOT IN (SELECT PICK_LOCN_HDR_ID
                                           FROM PICK_LOCN_HDR_SLOTTING);

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            ROLLBACK;
            RAISE;
      END;
   END;
END SLOTTING_MIGRATION_PKG;
/

show errors;

/* Assumptions:
   WM-X version and Slotting-X version (where X is one of release say 2013)
   Items Must Exist in Slotting and in WM
   Items exist in Slotting but not exist in WM will not be considerd, these items need to import either with xml or through adapter
   Items exist in WM but not exist in Slotting will not be considerd, these items need to import with xml
   Should write WM/CBO keys to Slotting tables.(should not generate any new keys)
*/

create or replace package slotting_wm_migration_pkg
as
   procedure copy_item_data;
   procedure copy_slot_data;
end slotting_wm_migration_pkg;
/

CREATE OR REPLACE package body slotting_wm_migration_pkg
is
   procedure copy_item_data
   is
      type slot_wm_skuid_type is table of slot_wm_skuid%rowtype
         index by binary_integer;

      slot_wm_skuid_list             slot_wm_skuid_type;

      type bin_xref_unused_type is table of bin_xref_unused%rowtype
         index by binary_integer;

      bin_xref_unused_list           bin_xref_unused_type;

      type item_cat_xref_unused_type is table of item_cat_xref_unused%rowtype
         index by binary_integer;

      item_cat_xref_unused_list      item_cat_xref_unused_type;

      type pallet_xref_unused_t is table of pallet_xref_unused%rowtype
         index by binary_integer;

      pallet_xref_unused_l           pallet_xref_unused_t;

      type reserve_bin_xref_unused_type
         is table of reserve_bin_xref_unused%rowtype
         index by binary_integer;

      reserve_bin_xref_unused_list   reserve_bin_xref_unused_type;

      type res_pallet_xref_unused_t
         is table of reserve_pallet_xref_unused%rowtype
         index by binary_integer;

      res_pallet_xref_unused_l       res_pallet_xref_unused_t;

      type slot_item_unused_type is table of slot_item_unused%rowtype
         index by binary_integer;

      slot_item_unused_list          slot_item_unused_type;

      type slot_item_type is table of slot_item%rowtype
         index by binary_integer;

      slot_item_list                 slot_item_type;
      v_slot_id                      slot_item.slot_id%type;
   begin
      insert into item_facility_slotting (item_facility_mapping_id,
                                          item_id,
                                          item_name,
                                          whse_code,
                                          unit_meas,
                                          manual,
                                          is_new,
                                          allow_slu,
                                          allow_su,
                                          disc_trans,
                                          each_per_bin,
                                          ex_recpt_date,
                                          discont,
                                          processed,
                                          inn_per_cs,
                                          max_stacking,
                                          max_lanes,
                                          num_units_per_bin,
                                          reserved_1,
                                          reserved_2,
                                          reserved_3,
                                          reserved_4,
                                          mod_user,
                                          create_date_time,
                                          mod_date_time,
                                          reserve_rack_type_id,
                                          allow_reserve,
                                          palpat_reserve,
                                          three_d_calc_done,
                                          max_slots,
                                          max_pallet_stacking,
                                          prop_borrowing_object,
                                          prop_borrowing_specific,
                                          height_can_borrow,
                                          length_can_borrow,
                                          width_can_borrow,
                                          weight_can_borrow,
                                          vend_pack_can_borrow,
                                          inner_pack_can_borrow,
                                          ti_can_borrow,
                                          hi_can_borrow,
                                          qty_per_grab_can_borrow,
                                          hand_attr_can_borrow,
                                          fam_grp_can_borrow,
                                          commodity_can_borrow,
                                          crushability_can_borrow,
                                          hazard_can_borrow,
                                          vend_code_can_borrow,
                                          misc1_can_borrow,
                                          misc2_can_borrow,
                                          misc3_can_borrow,
                                          misc4_can_borrow,
                                          misc5_can_borrow,
                                          misc6_can_borrow,
                                          height_borrowed,
                                          length_borrowed,
                                          width_borrowed,
                                          weight_borrowed,
                                          vend_pack_borrowed,
                                          inner_pack_borrowed,
                                          ti_borrowed,
                                          hi_borrowed,
                                          qty_per_grab_borrowed,
                                          hand_attr_borrowed,
                                          fam_grp_borrowed,
                                          commodity_borrowed,
                                          crushability_borrowed,
                                          hazard_borrowed,
                                          vend_code_borrowed,
                                          misc1_borrowed,
                                          misc2_borrowed,
                                          misc3_borrowed,
                                          misc4_borrowed,
                                          misc5_borrowed,
                                          misc6_borrowed,
                                          aframe_ht,
                                          aframe_len,
                                          aframe_wid,
                                          aframe_wt,
                                          aframe_allow,
                                          item_num_1,
                                          item_num_2,
                                          item_num_3,
                                          item_num_4,
                                          item_num_5,
                                          item_char_1,
                                          item_char_2,
                                          item_char_3,
                                          item_char_4,
                                          item_char_5)
         select ifmw.item_facility_mapping_id,
                ifmw.item_id,
                sim.sku_name,
                sim.whse_code,
                sim.unit_meas,
                sim.manual,
                sim.is_new,
                sim.allow_slu,
                sim.allow_su,
                sim.disc_trans,
                sim.each_per_bin,
                sim.ex_recpt_date,
                sim.discont,
                sim.processed,
                sim.inn_per_cs,
                sim.max_stacking,
                sim.max_lanes,
                sim.num_units_per_bin,
                sim.reserved_1,
                sim.reserved_2,
                sim.reserved_3,
                sim.reserved_4,
                sim.mod_user,
                sim.create_date_time,
                sim.mod_date_time,
                sim.reserve_rack_type_id,
                sim.allow_reserve,
                sim.palpat_reserve,
                sim.three_d_calc_done,
                sim.max_slots,
                sim.max_pallet_stacking,
                sim.prop_borrowing_object,
                sim.prop_borrowing_specific,
                sim.height_can_borrow,
                sim.length_can_borrow,
                sim.width_can_borrow,
                sim.weight_can_borrow,
                sim.vend_pack_can_borrow,
                sim.inner_pack_can_borrow,
                sim.ti_can_borrow,
                sim.hi_can_borrow,
                sim.qty_per_grab_can_borrow,
                sim.hand_attr_can_borrow,
                sim.fam_grp_can_borrow,
                sim.commodity_can_borrow,
                sim.crushability_can_borrow,
                sim.hazard_can_borrow,
                sim.vend_code_can_borrow,
                sim.misc1_can_borrow,
                sim.misc2_can_borrow,
                sim.misc3_can_borrow,
                sim.misc4_can_borrow,
                sim.misc5_can_borrow,
                sim.misc6_can_borrow,
                sim.height_borrowed,
                sim.length_borrowed,
                sim.width_borrowed,
                sim.weight_borrowed,
                sim.vend_pack_borrowed,
                sim.inner_pack_borrowed,
                sim.ti_borrowed,
                sim.hi_borrowed,
                sim.qty_per_grab_borrowed,
                sim.hand_attr_borrowed,
                sim.fam_grp_borrowed,
                sim.commodity_borrowed,
                sim.crushability_borrowed,
                sim.hazard_borrowed,
                sim.vend_code_borrowed,
                sim.misc1_borrowed,
                sim.misc2_borrowed,
                sim.misc3_borrowed,
                sim.misc4_borrowed,
                sim.misc5_borrowed,
                sim.misc6_borrowed,
                sim.aframe_ht,
                sim.aframe_len,
                sim.aframe_wid,
                sim.aframe_wt,
                sim.aframe_allow,
                item_num_1,
                item_num_2,
                item_num_3,
                item_num_4,
                item_num_5,
                item_char_1,
                item_char_2,
                item_char_3,
                item_char_4,
                item_char_5
           from so_item_master_unused sim,
                item_cbo ic,
                facility f,
                item_facility_mapping_wms ifmw
          where     sim.sku_name = ic.item_name
                and sim.whse_code = f.whse
                and ic.item_id = ifmw.item_id
                and f.facility_id = ifmw.facility_id;

      --       insert into slot_wm_skuid (slot_skuid, wm_skuid)
      --          select sim.sku_id,
      --                 item_facility_mapping_wms.item_facility_mapping_id
      --            from so_item_master_unused sim,
      --                 item_cbo ic,
      --                 facility f,
      --                 item_facility_mapping_wms ifmw
      --           where     sim.sku_name = ic.item_name
      --                 and sim.whse_code = f.whse
      --                 and ic.item_id = ifmw.item_id
      --                 and f.facility_id = ifmw.facility_id;
      
      -- since wm id is matching in slotting schema with some other item, so we need to take backup of bin_xref and need to truncate
      -- all records from bin xref, then need to get fresh data from backup.

      insert into bin_xref (sku_id,
                            bin_id,
                            binxref_id,
                            create_date_time,
                            mod_date_time,
                            mod_user)
         select slwms.wm_skuid,
                bxu.bin_id,
                bxu.binxref_id,
                bxu.create_date_time,
                bxu.mod_date_time,
                bxu.mod_user
           from bin_xref_unused bxu, slot_wm_skuid slwms
          where bxu.sku_id = slwms.slot_skuid;

      insert into item_cat_xref (cat_id,
                                 cat_code_id,
                                 sku_id,
                                 itemcat_id,
                                 create_date_time,
                                 mod_date_time,
                                 mod_user)
         select icxu.cat_id,
                icxu.cat_code_id,
                slwms.wm_skuid,
                icxu.itemcat_id,
                icxu.create_date_time,
                icxu.mod_date_time,
                icxu.mod_user
           from item_cat_xref_unused icxu, slot_wm_skuid slwms
          where icxu.sku_id = slwms.slot_skuid;

      insert into pallet_xref (pallet_xref_id,
                               sku_id,
                               pallet_id,
                               create_date_time,
                               mod_date_time,
                               mod_user)
         select pxu.pallet_xref_id,
                slwms.wm_skuid,
                pxu.pallet_id,
                pxu.create_date_time,
                pxu.mod_date_time,
                pxu.mod_user
           from pallet_xref_unused pxu, slot_wm_skuid slwms
          where pxu.sku_id = slwms.slot_skuid;

      insert into reserve_bin_xref (res_sku_id,
                                    res_bin_id,
                                    res_binxref_id,
                                    create_date_time,
                                    mod_date_time,
                                    mod_user)
         select slwms.wm_skuid,
                rbxu.res_bin_id,
                rbxu.res_binxref_id,
                rbxu.create_date_time,
                rbxu.mod_date_time,
                rbxu.mod_user
           from reserve_bin_xref_unused rbxu, slot_wm_skuid slwms
          where rbxu.res_sku_id = slwms.slot_skuid;

      insert into reserve_pallet_xref (res_pallet_xref_id,
                                       res_sku_id,
                                       res_pallet_id,
                                       create_date_time,
                                       mod_date_time,
                                       mod_user)
         select rpxu.res_pallet_xref_id,
                slwms.wm_skuid,
                rpxu.res_pallet_id,
                rpxu.create_date_time,
                rpxu.mod_date_time,
                rpxu.mod_user
           from reserve_pallet_xref_unused rpxu, slot_wm_skuid slwms
          where rpxu.res_sku_id = slwms.slot_skuid;

      insert into slot_item (slotitem_id,
                             slot_id,
                             sku_id,
                             slot_unit,
                             ship_unit,
                             max_lanes,
                             deeps,
                             slot_locked,
                             primary_move,
                             slot_width,
                             est_hits,
                             est_inventory,
                             est_movement,
                             hist_match,
                             last_change,
                             bin_unit,
                             pallete_pattern,
                             score,
                             cur_orientation,
                             ign_for_reslot,
                             rec_lanes,
                             rec_stacking,
                             pallet_mov,
                             case_mov,
                             inner_mov,
                             each_mov,
                             bin_mov,
                             pallet_inven,
                             case_inven,
                             inner_inven,
                             each_inven,
                             bin_inven,
                             calc_hits,
                             current_bin,
                             current_pallet,
                             opt_pallet_pattern,
                             allow_expand,
                             pallet_hi,
                             needed_rack_type,
                             legal_fit_reason,
                             item_cost,
                             user_defined,
                             legal_fit,
                             score_dirty,
                             use_estimated_hist,
                             opt_fluid_vol,
                             calc_visc,
                             est_visc,
                             session_id,
                             rank,
                             info1,
                             info2,
                             info3,
                             info4,
                             info5,
                             info6,
                             reserved_1,
                             reserved_2,
                             reserved_3,
                             reserved_4,
                             create_date_time,
                             mod_date_time,
                             mod_user,
                             si_num_1,
                             si_num_2,
                             si_num_3,
                             si_num_4,
                             si_num_5,
                             si_num_6,
                             slot_unit_weight,
                             total_item_wt,
                             borrowing_object,
                             borrowing_specific,
                             est_mvmt_can_borrow,
                             est_hits_can_borrow,
                             plb_score,
                             mult_loc_grp,
                             delete_mult,
                             gave_hist_to,
                             abw_temp_tag,
                             forecast_borrowed,
                             num_vert_div,
                             replen_group,
                             added_for_mlm,
                             reserved_5,
                             adj_grp_id)
         select distinct siu.slotitem_id,
                sws.wm_slotid, -- siu.slot_id,
                slwms.wm_skuid,
                siu.slot_unit,
                siu.ship_unit,
                siu.max_lanes,
                siu.deeps,
                siu.slot_locked,
                siu.primary_move,
                siu.slot_width,
                siu.est_hits,
                siu.est_inventory,
                siu.est_movement,
                siu.hist_match,
                siu.last_change,
                siu.bin_unit,
                siu.pallete_pattern,
                siu.score,
                siu.cur_orientation,
                siu.ign_for_reslot,
                siu.rec_lanes,
                siu.rec_stacking,
                siu.pallet_mov,
                siu.case_mov,
                siu.inner_mov,
                siu.each_mov,
                siu.bin_mov,
                siu.pallet_inven,
                siu.case_inven,
                siu.inner_inven,
                siu.each_inven,
                siu.bin_inven,
                siu.calc_hits,
                siu.current_bin,
                siu.current_pallet,
                siu.opt_pallet_pattern,
                siu.allow_expand,
                siu.pallet_hi,
                siu.needed_rack_type,
                siu.legal_fit_reason,
                siu.item_cost,
                siu.user_defined,
                siu.legal_fit,
                siu.score_dirty,
                siu.use_estimated_hist,
                siu.opt_fluid_vol,
                siu.calc_visc,
                siu.est_visc,
                siu.session_id,
                siu.rank,
                siu.info1,
                siu.info2,
                siu.info3,
                siu.info4,
                siu.info5,
                siu.info6,
                siu.reserved_1,
                siu.reserved_2,
                siu.reserved_3,
                siu.reserved_4,
                siu.create_date_time,
                siu.mod_date_time,
                siu.mod_user,
                siu.si_num_1,
                siu.si_num_2,
                siu.si_num_3,
                siu.si_num_4,
                siu.si_num_5,
                siu.si_num_6,
                siu.slot_unit_weight,
                siu.total_item_wt,
                siu.borrowing_object,
                siu.borrowing_specific,
                siu.est_mvmt_can_borrow,
                siu.est_hits_can_borrow,
                siu.plb_score,
                siu.mult_loc_grp,
                siu.delete_mult,
                siu.gave_hist_to,
                siu.abw_temp_tag,
                siu.forecast_borrowed,
                siu.num_vert_div,
                siu.replen_group,
                siu.added_for_mlm,
                siu.reserved_5,
                siu.adj_grp_id
           from slot_item_unused siu
           left outer join slot_wm_skuid slwms on (slwms.slot_skuid = siu.sku_id)
           left outer join slot_wm_slotid sws on (sws.slot_slotid = siu.slot_id)
           where siu.sku_id is not null or siu.slot_id is not null;

      delete from item_history where slotitem_id in (select slotitem_id from slot_item where slot_id is null and sku_id is null);
      execute immediate 'truncate table slot_item_score';
      delete from slot_item where slot_id is null and sku_id is null;

      execute immediate 'truncate table move_list';
      execute immediate 'truncate table order_dtl';
      execute immediate 'truncate table wa_detail';
   commit;
   exception
      when others
      then
         dbms_output.put_line ('COPY_ITEM_DATA SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
         rollback;
         raise;
   end copy_item_data;

   procedure copy_slot_data
   is
      type slot_wm_slotid_type is table of slot_wm_slotid%rowtype
         index by binary_integer;

      slot_wm_slotid_list     slot_wm_slotid_type;

      type slot_item_unused_type is table of slot_item_unused%rowtype
         index by binary_integer;

      slot_item_unused_list   slot_item_unused_type;
   begin
      insert into pick_locn_hdr_slotting (pick_locn_hdr_id,
                                          rack_type,
                                          rack_level_id,
                                          ht_override,
                                          width_override,
                                          depth_override,
                                          max_ln_ovr,
                                          max_st_ovr,
                                          max_sc_ovr,
                                          max_hc_ovr,
                                          max_lw_ovr,
                                          max_stack,
                                          max_side_clear,
                                          max_ht_clear,
                                          max_lane_wt,
                                          processed,
                                          locked,
                                          wt_limit_override,
                                          label_pos,
                                          label_pos_ovr,
                                          left_slot,
                                          right_slot,
                                          max_lanes,
                                          my_sns,
                                          my_range,
                                          allow_expand,
                                          allow_expand_ovr,
                                          old_rec_slot_width,
                                          side_of_aisle,
                                          reserved_1,
                                          reserved_2,
                                          reserved_3,
                                          reserved_4,
                                          last_updated_source,
                                          created_dttm,
                                          last_updated_dttm,
                                          slot_priority,
                                          reach_dist,
                                          reach_dist_override,
                                          allow_expand_rgt,
                                          allow_expand_lft,
                                          allow_expand_rgt_ovr,
                                          allow_expand_lft_ovr)
         select l2.pick_locn_hdr_id,
                l3.rack_type,
                l3.rack_level_id,
                l3.ht_override,
                l3.width_override,
                l3.depth_override,
                l3.max_ln_ovr,
                l3.max_st_ovr,
                l3.max_sc_ovr,
                l3.max_hc_ovr,
                l3.max_lw_ovr,
                l3.max_stack,
                l3.max_side_clear,
                l3.max_ht_clear,
                l3.max_lane_wt,
                l3.processed,
                l3.locked,
                l3.wt_limit_override,
                l3.label_pos,
                l3.label_pos_ovr,
                null, -- getlrslot_wmid (l3.left_slot),
                null, -- getlrslot_wmid (l3.right_slot),
                l3.max_lanes,
                l3.my_sns,
                l3.my_range,
                l3.allow_expand,
                l3.allow_expand_ovr,
                l3.old_rec_slot_width,
                l3.side_of_aisle,
                l3.reserved_1,
                l3.reserved_2,
                l3.reserved_3,
                l3.reserved_4,
                l3.mod_user,
                l3.create_date_time,
                l3.mod_date_time,
                l3.slot_priority,
                l3.reach_dist,
                l3.reach_dist_override,
                l3.allow_expand_rgt,
                l3.allow_expand_lft,
                l3.allow_expand_rgt_ovr,
                l3.allow_expand_lft_ovr
           from locn_hdr l1, pick_locn_hdr l2, slot_unused l3
          where l1.locn_hdr_id = l2.locn_hdr_id
            and l1.dsp_locn = l3.dsp_slot
            and l1.whse = l3.whse_code;

   commit;

   exception
      when others
      then
         dbms_output.put_line ('COPY_SLOT_DATA SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
         rollback;
         raise;
   end copy_slot_data;
end slotting_wm_migration_pkg;
/
show errors;

exit

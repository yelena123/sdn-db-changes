SET DEFINE OFF;
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (1, 1, 'Case movement', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (2, 2, 'Constraint', 1, 2, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (3, 3, 'Cube movement', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (4, 4, 'Deeps', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (5, 5, 'Goal or Sequence', 1, 2, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (6, 6, 'Inner pack', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (7, 7, 'Lanes', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (8, 8, 'Number of slotting units', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (9, 9, 'Pallet HI', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (10, 10, 'Pallet TI', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (11, 11, 'Score', 1, 2, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (12, 12, 'Shipping cube', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (13, 13, 'Shipping height', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (14, 14, 'Shipping movement', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (15, 15, 'Shipping weight', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (16, 16, 'Slotting height', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (17, 17, 'Slotting movement', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (18, 18, 'Slotting weight', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (19, 19, 'Stacking', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (20, 20, 'Vendor pack', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (21, 21, 'Viscosity', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (22, 22, 'Hits', 1, 1, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (23, 23, 'Item fit', 1, 2, 
    'admin');
Insert into COLOR_CODE_HDR
   (COLOR_CODE_ID, COLOR_CODE_NBR, DESCRIPTION, ACTIVE, GROUP_BY, 
    MOD_USER)
 Values
   (24, 24, 'Constraint sequence', 1, 2, 
    'admin');
COMMIT;

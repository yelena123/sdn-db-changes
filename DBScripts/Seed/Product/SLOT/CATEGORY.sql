SET DEFINE OFF;
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (1, 'Misc 1', 'ST', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (2, 'Misc 2', 'ST', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (3, 'Commodity', 'ST', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (4, 'Crushability', 'ST', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (5, 'Hazard', 'ST', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (6, 'Vendor', 'ST', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (7, 'Family Group', 'GR', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (8, 'Misc 3', 'ST', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (9, 'Misc 4', 'ST', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (10, 'Misc 5', 'ST', 'admin');
Insert into CATEGORY
   (CAT_ID, CAT_NAME, CAT_TYPE, MOD_USER)
 Values
   (11, 'Misc 6', 'ST', 'admin');
COMMIT;

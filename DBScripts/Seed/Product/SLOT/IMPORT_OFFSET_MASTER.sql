SET DEFINE OFF;
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (125, 1, 8, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (126, 2, 8, 'Family Group Code', 0, 
    'admin', 'Family Group Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (127, 2, 8, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (128, 1, 9, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (129, 1, 9, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (130, 2, 9, 'Commodity Code', 0, 
    'admin', 'Commodity Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (131, 2, 9, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (132, 1, 10, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (133, 1, 10, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (134, 2, 10, 'Family Group Code', 0, 
    'admin', 'Family Group Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (135, 2, 10, 'Commodity Code', 0, 
    'admin', 'Commodity Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (136, 1, 11, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (137, 1, 11, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (138, 2, 11, 'Hazard Code', 0, 
    'admin', 'Hazard Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (139, 2, 11, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (140, 1, 12, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (141, 1, 12, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (142, 2, 12, 'Crushability Code', 0, 
    'admin', 'Crushability Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (143, 2, 12, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (144, 1, 13, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (145, 1, 13, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (146, 2, 13, 'Misc 1 Code', 0, 
    'admin', 'Misc 1 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (147, 2, 13, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (148, 1, 14, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (149, 1, 14, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (150, 2, 14, 'Misc 2 Code', 0, 
    'admin', 'Misc 2 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (151, 2, 14, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (152, 1, 15, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (153, 1, 15, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (154, 2, 15, 'Misc 3 Code', 0, 
    'admin', 'Misc 3 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (155, 2, 15, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (156, 1, 16, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (157, 1, 16, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (158, 2, 16, 'Misc 4 Code', 0, 
    'admin', 'Misc 4 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (159, 2, 16, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (160, 1, 17, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (161, 1, 17, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (162, 2, 17, 'Misc 5 Code', 0, 
    'admin', 'Misc 5 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (163, 2, 17, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (164, 1, 18, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (165, 1, 18, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (166, 2, 18, 'Misc 6 Code', 0, 
    'admin', 'Misc 6 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (167, 2, 18, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (168, 1, 19, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (169, 1, 19, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (170, 2, 19, 'Order number', 0, 
    'admin', 'Order number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (171, 2, 19, 'Item number', 0, 
    'admin', 'Item number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (172, 2, 19, 'Quantity', 0, 
    'admin', 'Quantity');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (173, 2, 19, 'Ship unit', 0, 
    'admin', 'Ship unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (174, 2, 19, 'Batch number', 0, 
    'admin', 'Batch number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (175, 2, 19, 'Customer number', 0, 
    'admin', 'Customer number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (176, 2, 19, 'Extra field', 0, 
    'admin', 'Extra field');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (177, 2, 19, 'Date field', 0, 
    'admin', 'Date field');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (178, 2, 20, 'SKU', 0, 
    'admin', 'SKU');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (179, 2, 20, 'Slot Number', 0, 
    'admin', 'Slot Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (180, 2, 20, 'Slot Unit', 0, 
    'admin', 'Slot Unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (181, 2, 20, 'Ship Unit', 0, 
    'admin', 'Ship Unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (182, 2, 20, 'Current Lanes', 0, 
    'admin', 'Current Lanes');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (183, 2, 20, 'Current Stacking', 0, 
    'admin', 'Current Stacking');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (184, 2, 20, 'Optimize Plt Pattern', 0, 
    'admin', 'Optimize Plt Pattern');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (185, 2, 20, 'Allow 1 HI Residual', 0, 
    'admin', 'Allow 1 HI Residual');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (186, 2, 20, 'Orientation', 0, 
    'admin', 'Orientation');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (187, 2, 20, 'Ignore for Reslot', 0, 
    'admin', 'Ignore for Reslot');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (188, 2, 20, 'SI Char 1', 0, 
    'admin', 'SI Char 1');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (189, 2, 20, 'SI Char 2', 0, 
    'admin', 'SI Char 2');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (191, 2, 20, 'SI Char 4', 0, 
    'admin', 'SI Char 4');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (192, 2, 20, 'SI Char 5', 0, 
    'admin', 'SI Char 5');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (193, 2, 20, 'SI Char 6', 0, 
    'admin', 'SI Char 6');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (194, 2, 20, 'Movement', 0, 
    'admin', 'Movement');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (195, 2, 20, 'Hits', 0, 
    'admin', 'Hits');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (197, 2, 20, 'Estimated Movement', 0, 
    'admin', 'Estimated Movement');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (198, 2, 20, 'Estimated Hits', 0, 
    'admin', 'Estimated Hits');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (200, 2, 20, 'History Match', 0, 
    'admin', 'History Match');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (202, 2, 21, 'SKU', 0, 
    'admin', 'SKU');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (203, 2, 21, 'SKU Description', 0, 
    'admin', 'SKU Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (204, 2, 21, 'UPC', 0, 
    'admin', 'UPC');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (205, 2, 21, 'Commodity Class', 0, 
    'admin', 'Commodity Class');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (206, 2, 21, 'Crushability', 0, 
    'admin', 'Crushability');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (208, 2, 21, 'Vendor Code', 0, 
    'admin', 'Vendor Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (209, 2, 21, 'Misc 1', 0, 
    'admin', 'Misc 1');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (210, 2, 21, 'Misc 2', 0, 
    'admin', 'Misc 2');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (211, 2, 21, 'Misc 3', 0, 
    'admin', 'Misc 3');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (212, 2, 21, 'Misc 4', 0, 
    'admin', 'Misc 4');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (213, 2, 21, 'Misc 5', 0, 
    'admin', 'Misc 5');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (215, 2, 21, 'Item status', 0, 
    'admin', 'Item status');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (216, 2, 21, 'Case height', 0, 
    'admin', 'Case height');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (217, 2, 21, 'Case length', 0, 
    'admin', 'Case length');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (218, 2, 21, 'Case width', 0, 
    'admin', 'Case width');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (220, 2, 21, 'Each height', 0, 
    'admin', 'Each height');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (221, 2, 21, 'Each length', 0, 
    'admin', 'Each length');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (222, 2, 21, 'Each width', 0, 
    'admin', 'Each width');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (223, 2, 21, 'Each weight', 0, 
    'admin', 'Each weight');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (225, 2, 21, 'Inner length', 0, 
    'admin', 'Inner length');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (226, 2, 21, 'Inner width', 0, 
    'admin', 'Inner width');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (227, 2, 21, 'Inner weight', 0, 
    'admin', 'Inner weight');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (228, 2, 21, 'Warehouse TI', 0, 
    'admin', 'Warehouse TI');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (230, 2, 21, 'Vendor TI', 0, 
    'admin', 'Vendor TI');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (231, 2, 21, 'Vendor HI', 0, 
    'admin', 'Vendor HI');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (232, 2, 21, 'Order TI', 0, 
    'admin', 'Order TI');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (233, 2, 21, 'Order HI', 0, 
    'admin', 'Order HI');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (234, 2, 21, 'Exp rec dt', 0, 
    'admin', 'Exp rec dt');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (236, 2, 21, 'Max stacking', 0, 
    'admin', 'Max stacking');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (237, 2, 21, 'Max lanes', 0, 
    'admin', 'Max lanes');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (238, 2, 21, 'Alw ship units', 0, 
    'admin', 'Alw ship units');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (239, 2, 21, 'Alw slot units', 0, 
    'admin', 'Alw slot units');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (241, 2, 21, 'Inner pack', 0, 
    'admin', 'Inner pack');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (242, 2, 21, 'Vendor pack', 0, 
    'admin', 'Vendor pack');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (243, 2, 21, 'Rotate eaches', 0, 
    'admin', 'Rotate eaches');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (244, 2, 21, 'Rotate inners', 0, 
    'admin', 'Rotate inners');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (246, 2, 21, 'Rotate cases', 0, 
    'admin', 'Rotate cases');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (247, 2, 21, 'Use 3D slotting', 0, 
    'admin', 'Use 3D slotting');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (248, 2, 21, 'Nest eaches', 0, 
    'admin', 'Nest eaches');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (249, 2, 21, 'Incremental height', 0, 
    'admin', 'Incremental height');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (250, 2, 21, 'Incremental length', 0, 
    'admin', 'Incremental length');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (251, 2, 21, 'Incremental width', 0, 
    'admin', 'Incremental width');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (252, 2, 22, 'Slot Number', 0, 
    'admin', 'Slot Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (253, 2, 22, 'Rack Type', 0, 
    'admin', 'Rack Type');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (254, 2, 22, 'Height', 0, 
    'admin', 'Height');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (255, 2, 22, 'Width', 0, 
    'admin', 'Width');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (256, 2, 22, 'Depth', 0, 
    'admin', 'Depth');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (257, 2, 22, 'Clearance', 0, 
    'admin', 'Clearance');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (258, 2, 22, 'Side Clearance', 0, 
    'admin', 'Side Clearance');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (259, 2, 22, 'Locked', 0, 
    'admin', 'Locked');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (260, 2, 22, 'Lock Reason', 0, 
    'admin', 'Lock Reason');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (261, 2, 22, 'Max Stacking', 0, 
    'admin', 'Max Stacking');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (262, 2, 22, 'Max Lanes', 0, 
    'admin', 'Max Lanes');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (263, 2, 22, 'Expand Next', 0, 
    'admin', 'Expand Next');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (264, 2, 22, 'Label Location', 0, 
    'admin', 'Label Location');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (265, 2, 22, 'Max Slot Weight', 0, 
    'admin', 'Max Slot Weight');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (266, 2, 22, 'Max Lane Weight', 0, 
    'admin', 'Max Lane Weight');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (273, 2, 3, 'Slot type', 0, 
    'admin', 'Slot type');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (274, 2, 3, 'Travel zone', 0, 
    'admin', 'Travel zone');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (275, 2, 3, 'Travel aisle', 0, 
    'admin', 'Travel aisle');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (276, 2, 19, 'Custom 1', 0, 
    'admin', 'Custom 1');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (277, 2, 19, 'Custom 2', 0, 
    'admin', 'Custom 2');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (278, 2, 19, 'Custom 3', 0, 
    'admin', 'Custom 3');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (279, 2, 19, 'Custom 4', 0, 
    'admin', 'Custom 4');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (280, 2, 19, 'Custom 5', 0, 
    'admin', 'Custom 5');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (281, 2, 22, 'X-coordinate', 0, 
    'admin', 'X-coordinate');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (282, 2, 22, 'Y-coordinate', 0, 
    'admin', 'Y-coordinate');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (283, 2, 22, 'Z-coordinate', 0, 
    'admin', 'Z-coordinate');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (284, 2, 3, 'X-coordinate', 0, 
    'admin', 'X-coordinate');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (285, 2, 3, 'Y-coordinate', 0, 
    'admin', 'Y-coordinate');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (286, 2, 3, 'Z-coordinate', 0, 
    'admin', 'Z-coordinate');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (287, 2, 22, 'Slot type', 0, 
    'admin', 'Slot type');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (288, 2, 22, 'Travel zone', 0, 
    'admin', 'Travel zone');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (289, 2, 22, 'Travel aisle', 0, 
    'admin', 'Travel aisle');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (290, 2, 21, 'Handling attr(each)', 0, 
    'admin', 'Handling attr(each)');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (291, 2, 21, 'Handling attr(inner)', 0, 
    'admin', 'Handling attr(inner)');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (292, 2, 21, 'Handling attr(case)', 0, 
    'admin', 'Handling attr(case)');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (293, 2, 21, 'Qty per grab (each)', 0, 
    'admin', 'Qty per grab (each)');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (294, 2, 21, 'Qty per grab (inner)', 0, 
    'admin', 'Qty per grab (inner)');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (295, 2, 21, 'Qty per grab (case)', 0, 
    'admin', 'Qty per grab (case)');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (296, 2, 3, 'Location class', 0, 
    'admin', 'Location class');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (297, 2, 22, 'Location class', 0, 
    'admin', 'Location class');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (298, 2, 3, 'Slot priority', 0, 
    'admin', 'Slot priority');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (299, 2, 22, 'Slot priority', 0, 
    'admin', 'Slot priority');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (310, 2, 21, 'Item Num 1', 0, 
    'admin', 'Item Num 1');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (311, 2, 21, 'Item Num 2', 0, 
    'admin', 'Item Num 2');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (312, 2, 21, 'Item Num 3', 0, 
    'admin', 'Item Num 3');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (313, 2, 21, 'Item Num 4', 0, 
    'admin', 'Item Num 4');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (314, 2, 21, 'Item Num 5', 0, 
    'admin', 'Item Num 5');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (316, 2, 21, 'Item Char 2', 0, 
    'admin', 'Item Char 2');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (317, 2, 21, 'Item Char 3', 0, 
    'admin', 'Item Char 3');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (318, 2, 21, 'Item Char 4', 0, 
    'admin', 'Item Char 4');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (319, 2, 21, 'Item Char 5', 0, 
    'admin', 'Item Char 5');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (327, 2, 20, 'SI Num 2', 0, 
    'admin', 'SI Num 2');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (328, 2, 20, 'SI Num 3', 0, 
    'admin', 'SI Num 3');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (329, 2, 20, 'SI Num 4', 0, 
    'admin', 'SI Num 4');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (330, 2, 20, 'SI Num 5', 0, 
    'admin', 'SI Num 5');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (331, 2, 20, 'SI Num 6', 0, 
    'admin', 'SI Num 6');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (335, 2, 3, 'Action code', 0, 
    'admin', 'Action code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (336, 2, 23, 'Vendor Code', 0, 
    'admin', 'Vendor Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (338, 2, 24, 'Week Ending', 0, 
    'admin', 'Week Ending');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (339, 2, 24, 'Slot Number', 0, 
    'admin', 'Slot Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (340, 2, 24, 'Item Number', 0, 
    'admin', 'Item Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (341, 2, 24, 'Ship Unit', 0, 
    'admin', 'Ship Unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (342, 2, 24, 'Movement', 0, 
    'admin', 'Movement');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (344, 2, 25, 'Slot Number', 0, 
    'admin', 'Slot Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (345, 2, 25, 'Item Number', 0, 
    'admin', 'Item Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (346, 2, 25, 'Ship Unit', 0, 
    'admin', 'Ship Unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (347, 2, 25, 'Inventory', 0, 
    'admin', 'Inventory');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (349, 2, 26, 'Slot Number', 0, 
    'admin', 'Slot Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (350, 2, 26, 'Item Number', 0, 
    'admin', 'Item Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (351, 2, 26, 'Ship Unit', 0, 
    'admin', 'Ship Unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (352, 2, 26, 'Hits', 0, 
    'admin', 'Hits');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (354, 2, 27, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (355, 2, 28, 'Commodity Code', 0, 
    'admin', 'Commodity Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (356, 2, 28, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (357, 2, 29, 'Family Group Code', 0, 
    'admin', 'Family Group Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (359, 2, 30, 'Hazard Code', 0, 
    'admin', 'Hazard Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (360, 2, 30, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (361, 2, 31, 'Crushability Code', 0, 
    'admin', 'Crushability Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (362, 2, 31, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (364, 2, 32, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (365, 2, 33, 'Misc 2 Code', 0, 
    'admin', 'Misc 2 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (407, 1, 1, 'Date', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (408, 1, 1, 'Time', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (409, 1, 1, 'Number of Records', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (410, 1, 1, 'File type number', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (411, 2, 1, 'SKU', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (412, 2, 1, 'Slot Number', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (413, 2, 1, 'Slot Unit', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (414, 2, 1, 'Ship Unit', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (415, 2, 1, 'Current Lanes', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (416, 2, 1, 'Current Stacking', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (417, 2, 1, 'Optimize Plt Pattern', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (418, 2, 1, 'Allow 1 HI Residual', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (419, 2, 1, 'Orientation', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (366, 2, 33, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (190, 2, 20, 'SI Char 3', 0, 
    'admin', 'SI Char 3');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (196, 2, 20, 'Inventory', 0, 
    'admin', 'Inventory');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (199, 2, 20, 'Estimated Inventory', 0, 
    'admin', 'Estimated Inventory');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (207, 2, 21, 'Haz Code', 0, 
    'admin', 'Haz Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (214, 2, 21, 'Misc 6', 0, 
    'admin', 'Misc 6');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (219, 2, 21, 'Case weight', 0, 
    'admin', 'Case weight');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (224, 2, 21, 'Inner height', 0, 
    'admin', 'Inner height');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (229, 2, 21, 'Warehouse HI', 0, 
    'admin', 'Warehouse HI');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (235, 2, 21, 'Conveyability', 0, 
    'admin', 'Conveyability');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (240, 2, 21, 'Units per bin', 0, 
    'admin', 'Units per bin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (245, 2, 21, 'Rotate bins', 0, 
    'admin', 'Rotate bins');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (315, 2, 21, 'Item Char 1', 0, 
    'admin', 'Item Char 1');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (326, 2, 20, 'SI Num 1', 0, 
    'admin', 'SI Num 1');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (337, 2, 23, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (343, 2, 25, 'Week Ending', 0, 
    'admin', 'Week Ending');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (348, 2, 26, 'Week Ending', 0, 
    'admin', 'Week Ending');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (353, 2, 27, 'Family Group Code', 0, 
    'admin', 'Family Group Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (358, 2, 29, 'Commodity Code', 0, 
    'admin', 'Commodity Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (363, 2, 32, 'Misc 1 Code', 0, 
    'admin', 'Misc 1 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (84, 2, 3, 'Slot Number', 0, 
    'admin', 'Slot Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (90, 2, 3, 'Side Clearance', 0, 
    'admin', 'Side Clearance');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (96, 2, 3, 'Label Location', 0, 
    'admin', 'Label Location');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (101, 2, 4, 'Vendor Code', 0, 
    'admin', 'Vendor Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (106, 2, 5, 'Slot Number', 0, 
    'admin', 'Slot Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (111, 1, 6, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (118, 1, 7, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (420, 2, 1, 'Ignore for Reslot', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (421, 2, 1, 'SI Char 1', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (422, 2, 1, 'SI Char 2', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (423, 2, 1, 'SI Char 3', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (424, 2, 1, 'SI Char 4', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (425, 2, 1, 'SI Char 5', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (426, 2, 1, 'SI Char 6', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (427, 2, 1, 'Movement', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (428, 2, 1, 'Hits', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (429, 2, 1, 'Inventory', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (430, 2, 1, 'Estimated Movement', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (431, 2, 1, 'Estimated Hits', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (432, 2, 1, 'Estimated Inventory', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (433, 2, 1, 'History Match', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (434, 2, 1, 'SI Num 1', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (435, 2, 1, 'SI Num 2', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (436, 2, 1, 'SI Num 3', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (437, 2, 1, 'SI Num 4', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (438, 2, 1, 'SI Num 5', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (439, 2, 1, 'SI Num 6', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (440, 2, 1, 'Multiple location grp', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (441, 2, 1, 'Update type', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (442, 2, 1, 'Previous slot', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (443, 2, 1, 'Replenishment group', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (444, 1, 2, 'Date', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (445, 1, 2, 'Time', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (446, 1, 2, 'Number of Records', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (447, 2, 2, 'SKU', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (448, 2, 2, 'SKU Description', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (449, 2, 2, 'UPC', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (450, 2, 2, 'Commodity Class', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (451, 2, 2, 'Crushability', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (452, 2, 2, 'Haz Code', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (453, 2, 2, 'Vendor Code', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (454, 2, 2, 'Misc 1', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (455, 2, 2, 'Misc 2', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (456, 2, 2, 'Misc 3', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (457, 2, 2, 'Misc 4', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (458, 2, 2, 'Misc 5', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (459, 2, 2, 'Misc 6', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (460, 2, 2, 'Item status', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (461, 2, 2, 'Case height', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (462, 2, 2, 'Case length', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (463, 2, 2, 'Case width', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (464, 2, 2, 'Case weight', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (465, 2, 2, 'Each height', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (466, 2, 2, 'Each length', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (467, 2, 2, 'Each width', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (468, 2, 2, 'Each weight', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (469, 2, 2, 'Inner height', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (470, 2, 2, 'Inner length', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (471, 2, 2, 'Inner width', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (472, 2, 2, 'Inner weight', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (473, 2, 2, 'Warehouse TI', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (474, 2, 2, 'Warehouse HI', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (475, 2, 2, 'Vendor TI', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (476, 2, 2, 'Vendor HI', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (477, 2, 2, 'Order TI', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (478, 2, 2, 'Order HI', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (479, 2, 2, 'Exp rec dt', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (480, 2, 2, 'Conveyability', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (481, 2, 2, 'Max stacking', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (482, 2, 2, 'Max lanes', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (483, 2, 2, 'Alw ship units', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (484, 2, 2, 'Alw slot units', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (367, 2, 34, 'Misc 3 Code', 0, 
    'admin', 'Misc 3 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (368, 2, 34, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (369, 2, 35, 'Misc 4 Code', 0, 
    'admin', 'Misc 4 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (370, 2, 35, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (371, 2, 36, 'Misc 5 Code', 0, 
    'admin', 'Misc 5 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (372, 2, 36, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (373, 2, 37, 'Misc 6 Code', 0, 
    'admin', 'Misc 6 Code');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (374, 2, 37, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (375, 2, 38, 'Order number', 0, 
    'admin', 'Order number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (376, 2, 38, 'Item Number', 0, 
    'admin', 'Item Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (377, 2, 38, 'Quantity', 0, 
    'admin', 'Quantity');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (378, 2, 38, 'Ship unit', 0, 
    'admin', 'Ship unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (379, 2, 38, 'Batch number', 0, 
    'admin', 'Batch number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (380, 2, 38, 'Customer number', 0, 
    'admin', 'Customer number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (381, 2, 38, 'Extra field', 0, 
    'admin', 'Extra field');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (382, 2, 38, 'Date field', 0, 
    'admin', 'Date field');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (383, 2, 38, 'Custom 1', 0, 
    'admin', 'Custom 1');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (384, 2, 38, 'Custom 2', 0, 
    'admin', 'Custom 2');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (385, 2, 38, 'Custom 3', 0, 
    'admin', 'Custom 3');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (386, 2, 38, 'Custom 4', 0, 
    'admin', 'Custom 4');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (387, 2, 38, 'Custom 5', 0, 
    'admin', 'Custom 5');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (391, 2, 21, 'Max pallet stacking', 0, 
    'admin', 'Max pallet stacking');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (398, 2, 21, 'A-frame slot allowed', 0, 
    'admin', 'A-frame slot allowed');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (399, 2, 20, 'Multiple location grp', 0, 
    'admin', 'Multiple location grp');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (400, 2, 20, 'Replenishment group', 0, 
    'admin', 'Replenishment group');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (401, 2, 21, 'Max num of slots', 0, 
    'admin', 'Max num of slots');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (402, 2, 21, 'A-frame height', 0, 
    'admin', 'A-frame height');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (403, 2, 21, 'A-frame length', 0, 
    'admin', 'A-frame length');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (404, 2, 21, 'A-frame width', 0, 
    'admin', 'A-frame width');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (405, 2, 21, 'A-frame weight', 0, 
    'admin', 'A-frame weight');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (406, 2, 21, 'A-frame slot allowed', 0, 
    'admin', 'A-frame slot allowed');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (485, 2, 2, 'Units per bin', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (486, 2, 2, 'Inner pack', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (487, 2, 2, 'Vendor pack', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (488, 2, 2, 'Rotate eaches', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (489, 2, 2, 'Rotate inners', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (490, 2, 2, 'Rotate bins', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (491, 2, 2, 'Rotate cases', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (492, 2, 2, 'Use 3D slotting', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (493, 2, 2, 'Nest eaches', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (494, 2, 2, 'Incremental height', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (495, 2, 2, 'Incremental length', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (496, 2, 2, 'Incremental width', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (497, 2, 2, 'Qty per grab (each)', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (498, 2, 2, 'Qty per grab (inner)', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (499, 2, 2, 'Qty per grab (case)', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (500, 2, 2, 'Handling attr(each)', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (501, 2, 2, 'Handling attr(inner)', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (502, 2, 2, 'Handling attr(case)', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (503, 2, 2, 'Item Num 1', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (504, 2, 2, 'Item Num 2', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (505, 2, 2, 'Item Num 3', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (506, 2, 2, 'Item Num 4', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (507, 2, 2, 'Item Num 5', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (508, 2, 2, 'Item Char 1', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (509, 2, 2, 'Item Char 2', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (82, 1, 3, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (83, 1, 3, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (85, 2, 3, 'Rack Type', 0, 
    'admin', 'Rack Type');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (86, 2, 3, 'Height', 0, 
    'admin', 'Height');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (87, 2, 3, 'Width', 0, 
    'admin', 'Width');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (88, 2, 3, 'Depth', 0, 
    'admin', 'Depth');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (89, 2, 3, 'Clearance', 0, 
    'admin', 'Clearance');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (91, 2, 3, 'Locked', 0, 
    'admin', 'Locked');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (92, 2, 3, 'Lock Reason', 0, 
    'admin', 'Lock Reason');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (93, 2, 3, 'Max Stacking', 0, 
    'admin', 'Max Stacking');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (94, 2, 3, 'Max Lanes', 0, 
    'admin', 'Max Lanes');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (95, 2, 3, 'Expand Next', 0, 
    'admin', 'Expand Next');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (97, 2, 3, 'Max Slot Weight', 0, 
    'admin', 'Max Slot Weight');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (98, 2, 3, 'Max Lane Weight', 0, 
    'admin', 'Max Lane Weight');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (99, 1, 4, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (100, 1, 4, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (102, 2, 4, 'Description', 0, 
    'admin', 'Description');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (103, 1, 5, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (104, 1, 5, 'Number of Records', 0, 
    'admin', 'Number of Records');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (105, 2, 5, 'Recent First', 0, 
    'admin', 'Recent First');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (107, 2, 5, 'Item Number', 0, 
    'admin', 'Item Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (108, 2, 5, 'Ship Unit', 0, 
    'admin', 'Ship Unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (109, 2, 5, 'Movement', 0, 
    'admin', 'Movement');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (110, 1, 6, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (112, 2, 6, 'Recent First', 0, 
    'admin', 'Recent First');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (113, 2, 6, 'Slot Number', 0, 
    'admin', 'Slot Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (114, 2, 6, 'Item Number', 0, 
    'admin', 'Item Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (115, 2, 6, 'Ship Unit', 0, 
    'admin', 'Ship Unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (116, 2, 6, 'Inventory', 0, 
    'admin', 'Inventory');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (117, 1, 7, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (119, 2, 7, 'Recent First', 0, 
    'admin', 'Recent First');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (120, 2, 7, 'Slot Number', 0, 
    'admin', 'Slot Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (121, 2, 7, 'Item Number', 0, 
    'admin', 'Item Number');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (122, 2, 7, 'Ship Unit', 0, 
    'admin', 'Ship Unit');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (123, 2, 7, 'Hits', 0, 
    'admin', 'Hits');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER, OFFSET_FIELDNAME_TEMP)
 Values
   (124, 1, 8, 'Date', 0, 
    'admin', 'Date');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (510, 2, 2, 'Item Char 3', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (511, 2, 2, 'Item Char 4', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (512, 2, 2, 'Item Char 5', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (513, 2, 2, 'Action code', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (514, 2, 2, 'Max num of slots', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (515, 2, 2, 'Max pallet stacking', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (516, 2, 2, 'A-frame slot allowed', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (517, 2, 2, 'A-frame height', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (518, 2, 2, 'A-frame length', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (519, 2, 2, 'A-frame width', 0, 
    'admin');
Insert into IMPORT_OFFSET_MASTER
   (OFFSET_ID, OFFSET_TYPE, IMPORT_ID, OFFSET_FIELDNAME, CUSTOM, 
    MOD_USER)
 Values
   (520, 2, 2, 'A-frame weight', 0, 
    'admin');
COMMIT;

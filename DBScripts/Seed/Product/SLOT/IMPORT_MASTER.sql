SET DEFINE OFF;
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (1, 'Warehouse', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (2, 'Item Master', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (3, 'Slots', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (4, 'Vendor Codes', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (5, 'Movement History', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (6, 'Inventory History', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (7, 'Hits History', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (8, 'Family Group', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (9, 'Commodity', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (10, 'Family Group Relations', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (11, 'Hazard Codes', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (12, 'Crushability Codes', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (13, 'Misc 1', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (14, 'Misc 2', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (15, 'Misc 3', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (16, 'Misc 4', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (17, 'Misc 5', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (18, 'Misc 6', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (19, 'Order', 'admin', 0);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (20, 'Warehouse - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (21, 'Item Master -  ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (22, 'Slots -  ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (23, 'Vendor Codes - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (24, 'Movement History - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (25, 'Inventory History - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (26, 'Hits History - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (27, 'Family Group - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (28, 'Commodity - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (29, 'Family Group Relations - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (30, 'Hazard Codes - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (31, 'Crushability Codes - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (32, 'Misc 1 - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (33, 'Misc 2 - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (34, 'Misc 3 - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (35, 'Misc 4 - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (36, 'Misc 5 - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (37, 'Misc 6 - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (38, 'Order - ODBC', 'admin', 1);
Insert into IMPORT_MASTER
   (IMPORT_ID, IMPORT_NAME, MOD_USER, IMPORT_TYPE)
 Values
   (39, 'Replenishment - ODBC', 'admin', 1);
COMMIT;

set echo on
set serveroutput on;

prompt Copy Item and Slot Data
-- Copy Item Data
declare
    v_item int;
begin
    select count(*) into v_item from item_cbo;
    if (v_item = 0) then
        slotting_migration_pkg.copy_item_data();
        slotting_migration_pkg.copy_slot_data();
    else
        execute immediate 'truncate table bin_xref';
        execute immediate 'truncate table item_cat_xref';
        execute immediate 'truncate table pallet_xref';
        execute immediate 'truncate table reserve_bin_xref';
        execute immediate 'truncate table reserve_pallet_xref';
        execute immediate 'truncate table haves_needs';
        execute immediate 'truncate table hn_failure';
        execute immediate 'truncate table item_history';
        execute immediate 'truncate table move_list';
        execute immediate 'truncate table slot_item_score';
        execute immediate 'truncate table sl_failure_reasons';
        delete from slot_item;
        commit;
        slotting_wm_migration_pkg.copy_item_data();
        slotting_wm_migration_pkg.copy_slot_data();
    end if;
end;
/
commit;

--UPDATE QUERYENTRY TO POINT SO_ITEM_MASTER otherwise ITEM_FILER will not work
update queryentry
   set field_table = 'so_item_master'
 where field_table = 'item_master';
commit;

--UPDATE DEFAULT SNS TO WHSE_LOCN_MASK ID
update whprefs
   set def_sns =
          (select whse_locn_mask_id
             from whse_locn_mask
            where locn_class = 'A')
 where whse_code <> '*df';

--UPDATE MY SNS TO WHSE_LOCN_MASK ID in PICK_LOCN_HDR_SLOTTING
alter table pick_locn_hdr_slotting drop constraint fk_lhs_to_sns;

update pick_locn_hdr_slotting
   set my_sns =
          (select whse_locn_mask_id
             from whse_locn_mask
            where locn_class = 'A');
commit;

exit

SET DEFINE OFF;
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (80, 305, 'ENU', 'Imports', 'Hits History', 
    '90', 'Database exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (81, 306, 'ENU', 'Imports', 'Hits History', 
    '90', 'Cannot import history!  Either the ''Key On Slots'' or the ''Key On Items'' box must be checked.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (82, 307, 'ENU', 'Imports', 'Hits History', 
    '90', 'Import exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (83, 350, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Cannot import inventory!  The header date %s is invalid!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (84, 351, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Inventory Import:  Slot not found .', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (85, 352, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Inventory Import:  Slot/Item not found.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (86, 353, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Inventory Import:  Item not found.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (87, 354, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Inventory Import:  Item not in this slot.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (88, 355, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Database exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (89, 356, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Cannot import history!  Either the ''Key On Slots'' or the ''Key On Items'' box must be checked.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (90, 357, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Import exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (91, 400, 'ENU', 'Imports', 'Movement History', 
    '90', 'Cannot import movement!  The header date %s is invalid!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (92, 401, 'ENU', 'Imports', 'Movement History', 
    '90', 'Movement Import:  Slot not found.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (93, 402, 'ENU', 'Imports', 'Movement History', 
    '90', 'Movement Import:  Slot/Item not found.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (94, 403, 'ENU', 'Imports', 'Movement History', 
    '90', 'Movement Import:  Item not found.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (95, 404, 'ENU', 'Imports', 'Movement History', 
    '90', 'Movement Import:  Item not in this slot', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (96, 405, 'ENU', 'Imports', 'Movement History', 
    '90', 'Database exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (97, 406, 'ENU', 'Imports', 'Movement History', 
    '90', 'Cannot import history!  Either the ''Key On Slots'' or the ''Key On Items'' box must be checked.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (98, 407, 'ENU', 'Imports', 'Movement History', 
    '90', 'Import exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (99, 450, 'ENU', 'Imports', 'Slots', 
    '90', 'The %s is invalid %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (100, 451, 'ENU', 'Imports', 'Slots', 
    '50', 'Unable to determine level for rack type %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (101, 452, 'ENU', 'Imports', 'Slots', 
    '90', 'The slot %s1 using rack type %s2 either has a bad level number or no levels exist!  Slot not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (102, 453, 'ENU', 'Imports', 'Slots', 
    '10', 'Slot was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (103, 454, 'ENU', 'Imports', 'Slots', 
    '90', 'Slot does not adhere to SNS %S. Slot not added!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (104, 455, 'ENU', 'Imports', 'Slots', 
    '50', 'Rack type %s not found in rack type table!  (Using default)', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (105, 456, 'ENU', 'Imports', 'Slots', 
    '10', 'Slot %s not in file.  The slot has been deleted.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (106, 457, 'ENU', 'Imports', 'Slots', 
    '90', 'Database exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (107, 458, 'ENU', 'Imports', 'Slots', 
    '50', 'The imported rack type is for analysis only. Set to default rack type.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (108, 459, 'ENU', 'Imports', 'Slots', 
    '50', 'Default Rack Type (%s) not found in Rack Type table! (Slot not added)', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (109, 460, 'ENU', 'Imports', 'Slots', 
    '90', 'Slot-item record not found.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (110, 461, 'ENU', 'Imports', 'Slots', 
    '10', 'Slot already exists in Slot table - not adding!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (111, 462, 'ENU', 'Imports', 'Slots', 
    '90', 'Import exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (112, 101, 'ENU', 'Imports', 'Item Master', 
    '90', 'Commodity code %s is non-existent for item %s!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (113, 102, 'ENU', 'Imports', 'Item Master', 
    '90', 'Crushability code %s is non-existent for item %s!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (114, 103, 'ENU', 'Imports', 'Item Master', 
    '90', 'Hazard code %s is non-existent for this item!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (115, 104, 'ENU', 'Imports', 'Item Master', 
    '90', 'Vendor code %s is non-existent for item %s!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (116, 105, 'ENU', 'Imports', 'Item Master', 
    '90', '%s %s is non-existent for item %s!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (117, 111, 'ENU', 'Imports', 'Item Master', 
    '50', 'A valid ship unit was not imported.  Defaulting to case.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (118, 112, 'ENU', 'Imports', 'Item Master', 
    '90', 'Error handling item!  Skipped and not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (119, 113, 'ENU', 'Imports', 'Item Master', 
    '90', 'Database exception, rolled back the transaction. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (120, 114, 'ENU', 'Imports', 'Item Master', 
    '90', 'Unable to get process manager lock. Exiting!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (121, 115, 'ENU', 'Imports', 'Item Master', 
    '10', 'The status of the item %s has changed to %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (122, 116, 'ENU', 'Imports', 'Item Master', 
    '10', 'No value imported.  Allowable slotting unit set to %s from the Import Preferences.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (123, 117, 'ENU', 'Imports', 'Item Master', 
    '50', 'No allowable slotting UOM set up for this ship UOM.  Defaulting to case', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (124, 118, 'ENU', 'Imports', 'Item Master', 
    '10', 'Allowable slotting unit set to %s from the Import Preferences.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (125, 119, 'ENU', 'Imports', 'Item Master', 
    '10', 'No allowable slotting UOM set up for this ship UOM.  Defaulting to %s pallet type', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (126, 120, 'ENU', 'Imports', 'Item Master', 
    '50', 'Pallet not setup in warehouse.  No pallet allowable slotting UOM set.  Defaulting to case', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (127, 121, 'ENU', 'Imports', 'Item Master', 
    '10', 'No allowable slotting UOM set up for this ship UOM.  Defaulting to %s bin type', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (128, 122, 'ENU', 'Imports', 'Item Master', 
    '50', 'Bin not setup in warehouse.  No bin allowable slotting UOM set.  Defaulting to case', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (129, 123, 'ENU', 'Imports', 'Item Master', 
    '50', 'Invalid data %s.  Allowable slotting unit set to %s from the Import Preferences.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (130, 124, 'ENU', 'Imports', 'Item Master', 
    '50', 'Use 3D slotting value is invalid for item %s.  Defaulting to no 3D slotting.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (131, 125, 'ENU', 'Imports', 'Item Master', 
    '50', 'In order to use 3D slotting either each, inner, or case slotting unit must be able to rotate for item %s.  Defaulting to ''No'' for 3D slotting.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (132, 126, 'ENU', 'Imports', 'Item Master', 
    '50', 'Nesting value is invalid for item %s.  Defaulting to no nesting.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (133, 127, 'ENU', 'Imports', 'Item Master', 
    '50', ' ''Each'' is not selected as an allowable slotting unit for item %s.  Defaulting to no nesting.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (134, 128, 'ENU', 'Imports', 'Item Master', 
    '50', 'At least one of the incremental dimensions for item %s must be greater than 0.  Defaulting to no nesting.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (135, 129, 'ENU', 'Imports', 'Item Master', 
    '50', 'Incremental height for item %s is invalid.  Defaulting to 0 and no nesting.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (136, 130, 'ENU', 'Imports', 'Item Master', 
    '50', 'Incremental length for item %s is invalid.  Defaulting to 0 and no nesting.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (137, 131, 'ENU', 'Imports', 'Item Master', 
    '50', 'Incremental width for item %s is invalid.  Defaulting to 0 and no nesting.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (138, 132, 'ENU', 'Imports', 'Item Master', 
    '50', 'The allowable slotting unit is smaller than the ship unit!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (139, 133, 'ENU', 'Imports', 'Item Master', 
    '50', '%s was <= 0.0 or invalid. Changed to %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (140, 134, 'ENU', 'Imports', 'Item Master', 
    '90', 'Import exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (141, 201, 'ENU', 'Imports', 'Warehouse', 
    '50', 'The ship unit of this item has changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (142, 202, 'ENU', 'Imports', 'Warehouse', 
    '90', 'Bad data: This slot does not have a slot item record!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (143, 203, 'ENU', 'Imports', 'Warehouse', 
    '50', 'The slot unit %s imported is not an allowable slotting unit for this item.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (144, 204, 'ENU', 'Imports', 'Warehouse', 
    '50', 'A valid ship unit was not imported.  Defaulting to case.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (145, 205, 'ENU', 'Imports', 'Warehouse', 
    '50', 'More then one record exists in the import file for this empty slot.  Duplicate slot(s) removed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (146, 206, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Item did not exist in Warehouse.  Default item master record created.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (147, 207, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Multiple items are assigned to the same slot in the import file.  Keep the first record and remove all the rest.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (148, 208, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Duplicate item assignment to the same location with UOM not equal to case or each.  History summing functionality causes invalid records to be removed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (149, 209, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Multiple records that contain the same shipping unit and are located in the same slot for the item %s have been removed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (150, 210, 'ENU', 'Imports', 'Warehouse', 
    '90', 'Slot did not exist in warehouse.  You must add it.  Item has been unslotted.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (151, 211, 'ENU', 'Imports', 'Warehouse', 
    '50', 'The imported slot unit is not allowed for its rack type level.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (152, 212, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Slot unit set to the largest allowed for the rack type level.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (153, 213, 'ENU', 'Imports', 'Warehouse', 
    '50', 'This item is new to the system.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (154, 214, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Bin not set.  Item master allowable bin used.  ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (155, 215, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Bin not set.  Import Preferences default bin used.  ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (156, 216, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Bin not set and Import Preferences default bin is invalid for rack type level.  First allowable bin for rack type level used.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (157, 217, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Rack type level has no legal bins.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (158, 218, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Pallet not set.  Item master allowable pallet used.  ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (159, 219, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Pallet not set.  Import Preferences default pallet used.  ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (160, 220, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Pallet not set and Import Preferences default pallet is invalid for rack type level.  First allowable pallet for rack type level was used.  ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (161, 221, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Rack type level has no legal pallets. ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (162, 222, 'ENU', 'Imports', 'Warehouse', 
    '50', 'The value for <Movement/Hits/Inventory/Estimated(Hits/Movement/Inventory)> is invalid.  Using <Not Set> as default.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (163, 223, 'ENU', 'Imports', 'Warehouse', 
    '90', 'Item number is blank.  Not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (164, 224, 'ENU', 'Imports', 'Warehouse', 
    '90', 'Database exception, rolled back the transaction. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (165, 225, 'ENU', 'Imports', 'Warehouse', 
    '90', 'Cannot perform the warehouse import!  The header date (%s) is invalid!.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (166, 226, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Unable to get process manager lock.  Exiting!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (167, 227, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Set slotted UOM to first available bin.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (168, 228, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Set slotted UOM to first available pallet.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (169, 229, 'ENU', 'Imports', 'Warehouse', 
    '90', 'No bins setup in warehouse.  Set slotted UOM to Case.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (170, 230, 'ENU', 'Imports', 'Warehouse', 
    '90', 'No pallet setup in warehouse.  Set slotted UOM to Case.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (171, 231, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Slot unit set to the largest item master allowable slot unit for its rack type level', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (172, 232, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Slot unit set to the largest allowed for the Import Preferences and rack type level.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (173, 233, 'ENU', 'Imports', 'Warehouse', 
    '90', 'No slotted UOM setup in warehouse. Unslot item.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (174, 234, 'ENU', 'Imports', 'Warehouse', 
    '50', 'Bin cannot be a valid ship unit.  Defaulting to Each.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (175, 235, 'ENU', 'Imports', 'Warehouse', 
    '90', 'Import exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (176, 501, 'ENU', 'Imports', 'General', 
    '90', 'Recalc slot layout ended with errors.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (177, 502, 'ENU', 'Imports', 'General', 
    '90', 'Set slot width ended with errors.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (178, 503, 'ENU', 'Imports', 'General', 
    '90', 'Rescore warehouse ended with errors.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (179, 504, 'ENU', 'Imports', 'General', 
    '90', 'Set left right pointer utility ended with errors', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (180, 900, 'ENU', 'Slotting', 'Optimizer', 
    '50', 'Item could not be considered for reslot', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (181, 901, 'ENU', 'Slotting', 'Optimizer', 
    '50', 'Slot could not be considered for reslot ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (182, 902, 'ENU', 'Slotting', 'Optimizer', 
    '50', 'Set Slot Widths: Could not expand slot to accomodate the item as slotted. ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (183, 903, 'ENU', 'Slotting', 'Optimizer', 
    '90', 'Set Slot Widths: Level setup is invalid on rack ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (184, 904, 'ENU', 'Slotting', 'Optimizer', 
    '50', 'ML Entry removed: item = %s src Slot = %s dest Slot = %s ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (185, 905, 'ENU', 'Slotting', 'Optimizer', 
    '50', 'Set Slot Width: This slot''s width exceeds the maximum slot width for the rack type. ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (186, 906, 'ENU', 'Slotting', 'Optimizer', 
    '50', 'Set Slot Width: This slot is not wide enough for the item it contains. ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (187, 907, 'ENU', 'Slotting', 'Optimizer', 
    '50', 'Set Slot Width: This slot is not positioned over its fixed label.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (188, 908, 'ENU', 'Slotting', 'Optimizer', 
    '90', 'This shelf has invalid labels. ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (189, 909, 'ENU', 'Slotting', 'Optimizer', 
    '10', 'The labels were repaired. ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (190, 910, 'ENU', 'Slotting', 'Optimizer', 
    '10', 'The labels were not repaired. ', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (191, 911, 'ENU', 'Slotting', 'Optimizer', 
    '10', 'Set Slot Width:  Slot or Item has ignore for reslot set, but slot width is changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (192, 912, 'ENU', 'Slotting', 'Optimizer', 
    '10', 'Set Slot Width:  Slot is locked, but its width was changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (251, 1509, 'ENU', 'Slotting', 'Real Time', 
    '90', 'The real time slotting update type is invalid. File: %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (252, 309, 'ENU', 'Imports', 'Hits History', 
    '90', 'Hits Import: The ship unit in the import file does not match the ship unit in the item_history table. Data not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (253, 359, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Inventory Import: The ship unit in the import file does not match the ship unit in the item_history table. Data not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (254, 409, 'ENU', 'Imports', 'Movement History', 
    '90', 'Movement Import: The ship unit in the import file does not match the ship unit in the item_history table. Data not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (255, 238, 'ENU', 'Imports', 'Warehouse', 
    '10', 'Executed SQL script %s. %s row(s) affected.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (256, 239, 'ENU', 'Imports', 'Warehouse', 
    '90', 'Could not execute SQL script %s. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (257, 150, 'ENU', 'Imports', 'Item Master', 
    '10', 'Executed SQL script %s. %s row(s) affected.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (258, 151, 'ENU', 'Imports', 'Item Master', 
    '90', 'Could not execute SQL script %s. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (259, 801, 'ENU', 'Wizards', 'Aisle builder', 
    '10', 'Slot was added by aisle builder.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (260, 802, 'ENU', 'Wizards', 'Aisle builder', 
    '10', 'Slot was deleted by aisle builder.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (261, 803, 'ENU', 'Wizards', 'Aisle builder', 
    '10', 'Item was unslotted by aisle builder.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (262, 804, 'ENU', 'Wizards', 'Aisle builder', 
    '10', 'Range %s was added by aisle builder.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (263, 805, 'ENU', 'Wizards', 'Aisle builder', 
    '10', 'Range %s was deleted by aisle builder.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (264, 152, 'ENU', 'Imports', 'Slots', 
    '90', 'Slot %s exists in Work Assignments. Slot not deleted.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (265, 1510, 'ENU', 'Slotting', 'Real Time', 
    '10', 'Item %s was unassigned from slot %s. File: %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (266, 471, 'ENU', 'Imports', 'Slots', 
    '50', 'At least one imported slot exists in analysis results %s. Results deleted.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (267, 1511, 'ENU', 'Imports', 'Item Master', 
    '90', 'Cannot perform the item master import! Error Description: (%s).', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (268, 1512, 'ENU', 'Imports', 'Warehouse', 
    '90', 'Cannot perform the warehouse import! Error Description: (%s).', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (269, 650, 'ENU', 'Imports', 'Replenishment', 
    '10', 'Item %s not found in Replenishment.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (270, 651, 'ENU', 'Imports', 'Replenishment', 
    '90', 'Invalid forecast period for item %s.  Defaulting to 1 week.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (271, 652, 'ENU', 'Imports', 'Replenishment', 
    '90', 'Invalid stocking unit of measure for item %s.  Defaulting to case.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (272, 653, 'ENU', 'Imports', 'Replenishment', 
    '90', 'No Location Range Group / Range Relationship exists for a record of item %s with Location %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (273, 654, 'ENU', 'Imports', 'Replenishment', 
    '90', 'Unable to match item %s with Location %s with a record in the slot_item table.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (274, 655, 'ENU', 'Imports', 'Replenishment', 
    '90', 'The Replenishment forecast for item %s is invalid.  Setting to Not Set.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (275, 656, 'ENU', 'Imports', 'Replenishment', 
    '90', 'Could not connect to the Replenishment database.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (276, 657, 'ENU', 'Imports', 'Replenishment', 
    '50', 'Item %s exists in Replenishment but does not have a matching Location.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (277, 153, 'ENU', 'Imports', 'Slots', 
    '90', 'Cannot perform the slot import!  The header date (%s) is invalid', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (278, 606, 'ENU', 'Imports', 'Orders', 
    '90', 'Date field is not valid on order %s for item %s.  Record not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (279, 607, 'ENU', 'Imports', 'Orders', 
    '90', 'Cannot perform the order import!  The header date (%s) is invalid', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (280, 472, 'ENU', 'Imports', 'Slots', 
    '10', 'Slot %s was added or updated.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (193, 913, 'ENU', 'Slotting', 'Optimizer', 
    '50', 'Set Slot Width: Item width exceeds maximum slot width. Slot width set to maximum slot width.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (194, 914, 'ENU', 'Slotting', 'Optimizer', 
    '50', 'Set Slot Width: Slot width could not be adjusted since item uses 3D slotting and/or nesting.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (195, 600, 'ENU', 'Imports', 'Orders', 
    '10', 'Blank record ignored.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (196, 601, 'ENU', 'Imports', 'Orders', 
    '50', 'No order number defined.  Record not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (197, 602, 'ENU', 'Imports', 'Orders', 
    '90', 'Item number is not valid.  Record not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (198, 603, 'ENU', 'Imports', 'Orders', 
    '90', 'Quantity is not valid on order %s for item %s.  Record not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (199, 505, 'ENU', 'Imports', 'General', 
    '90', 'Unable to login to %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (200, 506, 'ENU', 'Imports', 'General', 
    '90', '%s does not exist in the data source.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (201, 507, 'ENU', 'Tools', 'Set Label Location', 
    '50', 'Label position is invalid. Not set.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (202, 308, 'ENU', 'Imports', 'Hits History', 
    '90', 'Hits Import: Ship unit not found for this item.  Data not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (203, 358, 'ENU', 'Imports', 'Inventory History', 
    '90', 'Inventory Import: Ship unit not found for this item.  Data not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (204, 408, 'ENU', 'Imports', 'Movement History', 
    '90', 'Movement Import: Ship unit not found for this item.  Data not imported.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (205, 135, 'ENU', 'Imports', 'Item Master', 
    '90', 'Cannot perform the item master import!  The header date (%s) is invalid', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (206, 463, 'ENU', 'Imports', 'Slots', 
    '50', 'A valid location class was not imported.  Defaulting to A - Active', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (207, 508, 'ENU', 'Tools', 'Calculate 3d Slotting', 
    '50', 'The 3d slotting values for this item could not be calculated since one of its dimensions is 0.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (208, 604, 'ENU', 'Imports', 'Orders', 
    '90', 'Database exception, rolled back the transaction. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (209, 605, 'ENU', 'Imports', 'Orders', 
    '90', 'Import exception. %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (210, 75, 'ENU', 'Imports', 'Commodity', 
    '10', 'Commodity %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (211, 76, 'ENU', 'Imports', 'Crushability', 
    '10', 'Crushability %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (212, 77, 'ENU', 'Imports', 'Hazard', 
    '10', 'Hazard %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (213, 78, 'ENU', 'Imports', 'Misc 1', 
    '10', 'Misc 1 %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (214, 79, 'ENU', 'Imports', 'Misc 2', 
    '10', 'Misc 2 %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (215, 80, 'ENU', 'Imports', 'Misc 3', 
    '10', 'Misc 3 %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (216, 81, 'ENU', 'Imports', 'Misc 4', 
    '10', 'Misc 4 %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (217, 82, 'ENU', 'Imports', 'Misc 5', 
    '10', 'Misc 5 %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (218, 83, 'ENU', 'Imports', 'Misc 6', 
    '10', 'Misc 6 %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (219, 136, 'ENU', 'Imports', 'Item Master', 
    '10', 'The %s field was blank. The default of %s was used.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (220, 137, 'ENU', 'Imports', 'Item Master', 
    '10', 'The %s field was zero.  The default of %s was used.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (221, 464, 'ENU', 'Imports', 'Slots', 
    '10', 'The %s1 field was blank. The default of %s2 was used.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (222, 465, 'ENU', 'Imports', 'Slots', 
    '10', 'The %s1 field was zero.  The default of %s2 was used.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (223, 236, 'ENU', 'Imports', 'Warehouse', 
    '10', 'The %s field was blank.  The default of %s was used.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (224, 237, 'ENU', 'Imports', 'Warehouse', 
    '10', 'The %s field was zero. The default of %s was used.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (225, 138, 'ENU', 'Imports', 'Item Master', 
    '10', 'The %s field was zero or blank.  No default field was set. Slot using ''1'' as default.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (226, 139, 'ENU', 'Imports', 'Item Master', 
    '10', 'The %s field was zero or blank.  No default field was set. Slot using %s from the %s field as default', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (227, 140, 'ENU', 'Imports', 'Item Master', 
    '90', 'Invalid action code for item %s. File: %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (228, 141, 'ENU', 'Imports', 'Item Master', 
    '90', 'Item %s is invalid. File: %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (229, 142, 'ENU', 'Imports', 'Item Master', 
    '90', 'Item %s cannot be deleted since it is slotted. File: %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (230, 143, 'ENU', 'Imports', 'Item Master', 
    '90', 'Item %s cannot be deleted since it is part of an order or work assignment.. File: %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (231, 144, 'ENU', 'Imports', 'Item Master', 
    '10', 'Item %s was deleted. File: %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (232, 466, 'ENU', 'Imports', 'Slots', 
    '90', 'Invalid action code for slot %s. File: %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (233, 467, 'ENU', 'Imports', 'Slots', 
    '10', 'Slot %s1 was added or updated. File: %s2', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (234, 468, 'ENU', 'Imports', 'Slots', 
    '90', 'Slot %s cannot be deleted since it does not exist. File: %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (235, 469, 'ENU', 'Imports', 'Slots', 
    '90', 'Slot %s cannot be deleted since it is not empty. File: %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (236, 470, 'ENU', 'Imports', 'Slots', 
    '90', 'Slot %s was deleted. File: %s', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (237, 145, 'ENU', 'Imports', 'Item Master', 
    '50', 'Commodity code %s has been removed for this item.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (238, 146, 'ENU', 'Imports', 'Item Master', 
    '50', 'Crushability code %s has been removed for this item.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (239, 147, 'ENU', 'Imports', 'Item Master', 
    '50', 'Hazard code %s has been removed for this item.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (240, 148, 'ENU', 'Imports', 'Item Master', 
    '50', 'Vendor code %s has been removed for this item.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (241, 149, 'ENU', 'Imports', 'Item Master', 
    '50', '%s code %s has been removed for this item.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (242, 1507, 'ENU', 'Slotting', 'Real Time', 
    '90', 'Item %s and slot %s combination was not found, so no new slot was assigned.  File %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (243, 1500, 'ENU', 'Slotting', 'Real Time', 
    '90', 'No slot found for item %s.  File: %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (244, 1505, 'ENU', 'Slotting', 'Real Time', 
    '90', 'No slot found for item %s.  Time limit of %s minutes has been reached.  File: %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (245, 1506, 'ENU', 'Slotting', 'Real Time', 
    '90', 'Item %s was not found in the previous slot indicated (%s), so updates could not be performed.  File: %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (246, 1501, 'ENU', 'Slotting', 'Real Time', 
    '90', 'The current slot %s is not valid.  File %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (247, 1502, 'ENU', 'Slotting', 'Real Time', 
    '90', 'Item %s could not be assigned to slot %s since that slot was not empty.  File: %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (248, 1503, 'ENU', 'Slotting', 'Real Time', 
    '50', 'Item %s was assigned to slot %s, but there was some sort of mismatch in item fit, slotting unit, and(or) shipping unit.  File: %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (249, 1504, 'ENU', 'Slotting', 'Real Time', 
    '10', 'Item %s was assigned to slot %s.  File: %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (250, 1508, 'ENU', 'Slotting', 'Real Time', 
    '90', 'Reslots cannot be performed on the entire warehouse tree since there is a sequencing constraint at a lower level in the tree.  Real time warehouse requests (update type R) will not run.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (1, 1, 'ENU', 'Imports', 'Commodity', 
    '10', 'Commodity %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (2, 2, 'ENU', 'Imports', 'Commodity', 
    '10', 'Commodity %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (3, 26, 'ENU', 'Imports', 'Commodity', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (4, 27, 'ENU', 'Imports', 'Commodity', 
    '10', 'Commodity %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (5, 51, 'ENU', 'Imports', 'Commodity', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (6, 52, 'ENU', 'Imports', 'Commodity', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (7, 3, 'ENU', 'Imports', 'Crushability', 
    '10', 'Crushability %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (8, 4, 'ENU', 'Imports', 'Crushability', 
    '10', 'Crushability %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (9, 28, 'ENU', 'Imports', 'Crushability', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (10, 29, 'ENU', 'Imports', 'Crushability', 
    '10', 'Crushability %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (11, 53, 'ENU', 'Imports', 'Crushability', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (12, 54, 'ENU', 'Imports', 'Crushability', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (13, 5, 'ENU', 'Imports', 'Family Group', 
    '10', 'Family Group %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (14, 6, 'ENU', 'Imports', 'Family Group', 
    '10', 'Family Group %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (15, 30, 'ENU', 'Imports', 'Family Group', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (16, 31, 'ENU', 'Imports', 'Family Group', 
    '10', 'Category %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (17, 55, 'ENU', 'Imports', 'Family Group', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (18, 56, 'ENU', 'Imports', 'Family Group', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (19, 7, 'ENU', 'Imports', 'Family Group Relations', 
    '50', 'Commodity %s does not exist', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (20, 8, 'ENU', 'Imports', 'Family Group Relations', 
    '50', 'Family Group %s does not exist', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (21, 32, 'ENU', 'Imports', 'Family Group Relations', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (22, 33, 'ENU', 'Imports', 'Family Group Relations', 
    '10', 'Category %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (23, 50, 'ENU', 'Imports', 'Family Group Relations', 
    '10', 'Commodity Code already exists in Family Group', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (24, 57, 'ENU', 'Imports', 'Family Group Relations', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (25, 58, 'ENU', 'Imports', 'Family Group Relations', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (26, 9, 'ENU', 'Imports', 'Hazard', 
    '10', 'Hazard %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (27, 10, 'ENU', 'Imports', 'Hazard', 
    '10', 'Hazard %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (28, 34, 'ENU', 'Imports', 'Hazard', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (29, 35, 'ENU', 'Imports', 'Hazard', 
    '10', 'Hazard %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (30, 59, 'ENU', 'Imports', 'Hazard', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (31, 60, 'ENU', 'Imports', 'Hazard', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (32, 11, 'ENU', 'Imports', 'Misc 1', 
    '10', 'Misc 1 %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (33, 12, 'ENU', 'Imports', 'Misc 1', 
    '10', 'Misc 1 %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (34, 36, 'ENU', 'Imports', 'Misc 1', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (35, 37, 'ENU', 'Imports', 'Misc 1', 
    '10', 'Misc 1 %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (36, 61, 'ENU', 'Imports', 'Misc 1', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (37, 62, 'ENU', 'Imports', 'Misc 1', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (38, 13, 'ENU', 'Imports', 'Misc 2', 
    '10', 'Misc 2 %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (39, 14, 'ENU', 'Imports', 'Misc 2', 
    '10', 'Misc 2 %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (40, 38, 'ENU', 'Imports', 'Misc 2', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (41, 39, 'ENU', 'Imports', 'Misc 2', 
    '10', 'Misc 2 %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (42, 63, 'ENU', 'Imports', 'Misc 2', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (43, 64, 'ENU', 'Imports', 'Misc 2', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (44, 15, 'ENU', 'Imports', 'Misc 3', 
    '10', 'Misc 3 %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (45, 16, 'ENU', 'Imports', 'Misc 3', 
    '10', 'Misc 3 %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (46, 40, 'ENU', 'Imports', 'Misc 3', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (47, 41, 'ENU', 'Imports', 'Misc 3', 
    '10', 'Misc 3 %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (48, 65, 'ENU', 'Imports', 'Misc 3', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (49, 66, 'ENU', 'Imports', 'Misc 3', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (50, 17, 'ENU', 'Imports', 'Misc 4', 
    '10', 'Misc 4 %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (51, 18, 'ENU', 'Imports', 'Misc 4', 
    '10', 'Misc 4 %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (52, 42, 'ENU', 'Imports', 'Misc 4', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (53, 43, 'ENU', 'Imports', 'Misc 4', 
    '10', 'Misc 4 %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (54, 67, 'ENU', 'Imports', 'Misc 4', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (55, 68, 'ENU', 'Imports', 'Misc 5', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (56, 19, 'ENU', 'Imports', 'Misc 5', 
    '10', 'Misc 5 %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (57, 20, 'ENU', 'Imports', 'Misc 5', 
    '10', 'Misc 5 %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (58, 44, 'ENU', 'Imports', 'Misc 5', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (59, 45, 'ENU', 'Imports', 'Misc 5', 
    '10', 'Misc 5 %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (60, 69, 'ENU', 'Imports', 'Misc 5', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (61, 70, 'ENU', 'Imports', 'Misc 5', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (62, 21, 'ENU', 'Imports', 'Misc 6', 
    '10', 'Misc 6 %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (63, 22, 'ENU', 'Imports', 'Misc 6', 
    '10', 'Misc 6 %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (64, 46, 'ENU', 'Imports', 'Misc 6', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (65, 47, 'ENU', 'Imports', 'Misc 6', 
    '10', 'Misc 6 %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (66, 71, 'ENU', 'Imports', 'Misc 6', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (67, 72, 'ENU', 'Imports', 'Misc 6', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (68, 23, 'ENU', 'Imports', 'Vendor', 
    '10', 'Vendor %s was added.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (69, 24, 'ENU', 'Imports', 'Vendor', 
    '10', 'Vendor %s description changed.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (70, 25, 'ENU', 'Imports', 'Vendor', 
    '10', 'Vendor %s was deleted', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (71, 48, 'ENU', 'Imports', 'Vendor', 
    '10', 'Blank record ignored', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (72, 49, 'ENU', 'Imports', 'Vendor', 
    '10', 'Vendor %s already exists', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (73, 73, 'ENU', 'Imports', 'Vendor', 
    '90', 'Database exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (74, 74, 'ENU', 'Imports', 'Vendor', 
    '90', 'Import exception. %s.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (75, 300, 'ENU', 'Imports', 'Hits History', 
    '90', 'Cannot import hits!  The header date %s is invalid!', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (76, 301, 'ENU', 'Imports', 'Hits History', 
    '90', 'Hits Import:  Slot not found .', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (77, 302, 'ENU', 'Imports', 'Hits History', 
    '90', 'Hits Import:  Slot/Item not found.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (78, 303, 'ENU', 'Imports', 'Hits History', 
    '90', 'Hits Import:  Item not found.', 'admin');
Insert into SO_MESSAGE_MASTER
   (MSG_ID, MSG_NUMBER, MSG_LANGUAGE, MODULE, SUB_MODULE, 
    SEVERITY, MSG_TEXT, MOD_USER)
 Values
   (79, 304, 'ENU', 'Imports', 'Hits History', 
    '90', 'Hits Import:  Item not in this slot.', 'admin');
COMMIT;

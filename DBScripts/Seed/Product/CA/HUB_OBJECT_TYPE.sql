set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for HUB_OBJECT_TYPE

INSERT INTO HUB_OBJECT_TYPE ( HUB_OBJECT_TYPE,DESCRIPTION) 
VALUES  ( 'ASN','ASN');

INSERT INTO HUB_OBJECT_TYPE ( HUB_OBJECT_TYPE,DESCRIPTION) 
VALUES  ( 'LPN','LPN');

INSERT INTO HUB_OBJECT_TYPE ( HUB_OBJECT_TYPE,DESCRIPTION) 
VALUES  ( 'SHIP','Shipment');

Commit;





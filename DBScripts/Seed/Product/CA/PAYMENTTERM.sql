set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for PAYMENTTERM

INSERT INTO PAYMENTTERM ( PAYMENTTERMID,DESCRIPTION) 
VALUES  ( 0,'0-15');

INSERT INTO PAYMENTTERM ( PAYMENTTERMID,DESCRIPTION) 
VALUES  ( 1,'16-30');

INSERT INTO PAYMENTTERM ( PAYMENTTERMID,DESCRIPTION) 
VALUES  ( 2,'31-45');

INSERT INTO PAYMENTTERM ( PAYMENTTERMID,DESCRIPTION) 
VALUES  ( 3,'46-60');

INSERT INTO PAYMENTTERM ( PAYMENTTERMID,DESCRIPTION) 
VALUES  ( 4,'60+');

Commit;





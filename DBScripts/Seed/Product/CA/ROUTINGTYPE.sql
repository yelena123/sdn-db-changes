set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ROUTINGTYPE

INSERT INTO ROUTINGTYPE ( ROUTINGTYPE,DESCRIPTION) 
VALUES  ( '0','First Flight Out');

INSERT INTO ROUTINGTYPE ( ROUTINGTYPE,DESCRIPTION) 
VALUES  ( '1','Prime');

INSERT INTO ROUTINGTYPE ( ROUTINGTYPE,DESCRIPTION) 
VALUES  ( '2','Deferred Economy');

Commit;





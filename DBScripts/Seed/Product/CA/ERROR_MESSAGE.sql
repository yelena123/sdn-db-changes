set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ERROR_MESSAGE

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5002,'Invalid date format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5003,'Incoming Message Exceeds Max. Length');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5004,'Outgoing Message Exceeds Max. Length');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5005,'Incoming Message Id Should be numeric');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5006,'Outgoing Message Id Should be numeric');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000002,'Carrier on message does not match existing shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110031,'Assigned carrier %s does not support mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000006,'Can not accept Shipment %s.The shipment is not in a  tendered state');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000008,'Message received while shipment was in the ''Assigned'' or earlier status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000009,'Reference Message ID does not match any Update or Recall message for the Shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000010,'Cannot create an Appointment message for a shipment in the Assigned or previous state');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000011,'Cannot create an Arrival message for a shipment in the Tendered or earlier status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000012,'Cannot create a Departure message for a shipment in the Tendered or earlier status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000013,'Cannot create a Confirmation message for a shipment in the Assigned or previous state');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000014,'Cannot create an OS&D message for a shipment in the Accepted or earlier status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000015,'Cannot create a Check Call message for a shipment in the Tendered or earlier status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000016,'Cannot create a Hold message for a shipment in the Tendered or earlier status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000017,'Tender Accept can only be created for a shipment in Tendered status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000018,'Tender Reject can only be created for a shipment in Accepted or Tendered status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000019,'Cannot respond to shipment on behalf of broker');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000020,'Shipment is not tendered to a broker');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000021,'Cannot create message commodity for the same Commodity class and Size UOM. Please choose a different class.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010000,'Message Event Date/Time is in the past');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010001,'Message Event Date/Time is in the future');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010002,'The shipment is in the Tendered status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010003,'Check Call message received for a Delivered shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010004,'Check Call message received before the Tender Response message');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010005,'Appointment message received before the Tender Response message');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010006,'Arrival message received before the Tender Response message');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010007,'Departure message received before the Tender Response message');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010008,'OS&D message received before the Tender Response message');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010009,'OS&D message received before the In Transit message');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010010,'Message out of event time sequence');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010011,'The Bill of Lading # is already populated on the shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010012,'Carrier attempted to assign Bill of Lading #%s, but the shipper has not given the carrier permission to update Bill of Lading #');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010013,'Shipment size information will not be saved.  Load completion information is not allowed for this carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010014,'Carrier attempted to assign Trailer #%s, but the shipper has not given the carrier permission to update Trailer #.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 8901003,'%s does not exist for Product Object Type %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010015,'Carrier attempted to assign Tractor #%s, but the shipper has not given the carrier permission to update Tractor #.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1020000,'%s %s has invalid number format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1020001,'%s %s has invalid timestamp format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1020002,'%s %s has invalid string length');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1020003,'%s %s has invalid number length');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1020004,'%s is a required field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1020005,'%s %s has invalid timezone value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1030001,'Warning - You have just created a rejection message for the Designated Carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1030002,'Warning - You have created a rejection for a carrier that was outside of OptiManage.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100000,'There are fewer than two stops');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100001,'First stop is not a pickup');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100002,'Last stop is not a delivery');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100003,'No region found for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100004,'Pickup window is required for shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100006,'Shipment id %s does not exist');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100007,'Shipment %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100008,'Unknown size unit of measure %s on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100009,'Unknown shipment note type %s on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100010,'No pickup or delivery window on shipment (neither first nor last stop has arrival window)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100011,'Cannot promote shipment %s.  Status must be PLANNED, AVAILABLE or DELIVERED.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100012,'Cannot promote shipment %s.  Shipment has an import error.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100013,'Cannot promote shipment %s.  Shipment has been cancelled.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100014,'Cannot demote shipment %s.  Status must be AVAILABLE or COMPLETE.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100015,'Cannot demote shipment %s.  Shipment has an import error.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100016,'Cannot demote shipment %s.  Shipment has been cancelled.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100017,'Cannot promote shipment %s.  Shipments in available status cannot be promoted if transportation responsibility is shipper.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100018,'Unknown assigned carrier %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100019,'Assigned carrier %s is inactive.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100020,'Assigned carrier %s does not own equipment %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100021,'Assigned carrier %s does not support service level %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100022,'Assigned carrier %s does not support mode %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100023,'Cannot create shipment from an address to an address because no region will be found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100024,'Direct Distance was not imported on a shipment for which all distance values should have been provided.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100025,'Direct Distance was imported on a shipment for which no distance values should have been provided.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100026,'Poolpoint shipment %s has more than two stops.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110001,'Designated equipment %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110002,'Designated equipment %s is too small');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110003,'Designated service level %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110004,'Designated carrier %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110005,'Designated carrier %s does not own equipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110009,'Designated carrier %s does not support mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110010,'%s %s is less than minimum %s %s for designated mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110011,'%s %s is greater than maximum %s %s for designated mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110012,'Designated carrier %s does not support service level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110013,'Designated equipment %s is infeasible with shipment protection level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110014,'Assigned equipment %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110015,'Assigned equipment %s is too small');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110016,'Assigned equipment %s is infeasible with shipment protection level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110017,'Protection level %s of order %s is more restrictive than %s, the protection level of the shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110018,'Designated equipment %s is infeasible with order %s protection level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110019,'Assigned equipment %s is infeasible with order %s protection level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110021,'Designated equipment %s is infeasible with facility %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110022,'Assigned equipment %s is infeasible with facility %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110023,'Stop %s does not have an actual arrival.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110024,'Stop %s does not have an actual departure.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110025,'Designated equipment %s is infeasible with shipment product class %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110026,'Assigned equipment %s is infeasible with shipment product class %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110027,'Designated equipment %s is infeasible with order %s product class %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110028,'Assigned equipment %s is infeasible with order %s product class %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110029,'Assigned carrier %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110030,'Assigned carrier %s does not own equipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300016,'There are no valid shipments on the invoice.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300017,'The net amount due on the invoice does not match the total cost of all the invoiced shipments.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300018,'Cannot generate payment for invoice.  Invoice status must be READY TO BE PAID.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300019,'Cannot send payment for invoice.  The following shipments are already in the PAID status: %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500077,'Unknown planning destination facility %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300020,'Cannot invoice this shipment since the shipment status is PAID.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310000,'Duplicate shipment invoice on invoice ID %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310001,'The following shipments are not in the DELIVERED or COMPLETE status: %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310002,'The payment due date on the invoice is before the invoice date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310003,'Spot charge currency different from the invoice currency');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520016,'%s cannot be zero or negative');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000026,'Invalid entry. Facility %s is not of type Port.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320044,'Origin Facility and Origin Consolidation Regions can not be selected together.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320045,'Destination Facility and Destination Consolidation Regions can not be selected together.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700014,'At least one of the origin consolidation region or origin facilities should be selected in filter.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700015,'At least one of the destination consolidation region or destination facilities should be selected in filter.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700016,'The filter must contain exactly one origin facility, unless a virtual facility is specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700017,'The filter must contain exactly one destination facility, unless a virtual facility is specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700018,'No origin facility selected in the filter.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700019,'No destination facility selected in the filter.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200145,'Name Contains Invalid Character');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810043,'Route Plan Path already exists.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810042,'Port1 and Port2 can not be the same.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310004,'Net amount due does not match total cost of all invoiced shipments');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910027,'TimeFeasibility Calculation/Validation Warning: Tolerance factor was applied for this shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600039,'All orders must have same origin');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600040,'Path Set not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300022,'Invalid Booking ID.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9110000,'Carrier %s has reached the maximum trailers in the yard');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9110001,'Carrier %s has reached the minimum trailers in the yard');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9110002,'Carrier %s is going to reach the maximum trailers in the yard');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9110003,'Carrier %s is going to reach the minimum trailers in the yard');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9110004,'Yard has reached its full capacity');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9110005,'Yard is going to reach it''s full capacity');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9110006,'Hot Trailer %s has checked into the yard');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9120000,'Live Trailer %s has been waiting for more than %s time on yard');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9120001,'Outbound trailer %s has been waiting for more than %s time on yard');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9120002,'Inbound trailer %s has been waiting for more than %s time on yard');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9120003,'Trailer has been loading/unloading for more than %s time');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9610000,'Appointment is past the checkin time %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9610001,'Appointment has the following %s missing fields when created');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500071,'You can only specify length, width and height for a cube shape line item, or height and diameter for a cylinder shape line item.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300023,'You cannot reject the shipment invoice. The invoice is cancelled.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300024,'You cannot reject an Incomplete status shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330055,'%s: Truck order not used');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300025,'You cannot reject the shipment invoice. The invoice status must be Not Approved or Ready to be Paid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910014,'Shipment has empty floor spaces but does meet the minimum weight criteria.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910015,'Shipment has used up all floor spaces but does not meet the minimum weight criteria.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000027,'{0} {1} is infeasible with facility {2}.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300026,'Shipment invoice %s cannot be imported in the Incomplete, Payment Requested, or Rejected status.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810039,'Pickup Date should be after Arrival Date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700080,'Sailing schedules can be created only for Facility-Facility Lanes');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700081,'Reason code already exists for the given input data');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300027,'Invoice shipment %s value of %s is outside of the company limits (%s, %s).');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810040,'Port1 and Port2 are same as Origin and Destination facilities.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140016,'%s Monetary value should not be greater than facility credit limit');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140017,'%s Monetary value should not be greater than customer credit limit');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140018,'%s Monetary value should not be greater than country credit limit');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700012,'The origin region must contain at least one facility.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700013,'The destination region must contain at least one facility.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600041,'Common main carriage path set not found for selected orders');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600042,'All selected orders must have same origin and destination');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300021,'The payment due date is earlier than the invoice date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820041,'Expiration Date must not be before Effective Date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810038,'Cutoff Date should be before Departure Date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720174,'{0} is not valid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320046,'Object Type required when Object Id given');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810037,'Arrival Date should be after Departure Date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810041,'Origin or Destination requried.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510032,'No matching Currency Code for monetary value, on line items');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810035,'Pls  Enter Carrier Ref No on the Booking.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210038,'Cannot manually assign a carrier to the shipment since it was Assigned a carrier outside TP&E');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810044,'Cannot attach shipment to the booking');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320052,'Invalid Control Number. No Appointment found for Control Number "{0}".');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320053,'Invalid Trailer Number. No Appointment with Control Number "{0}" has Trailer Number "{1}".');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320054,'Invalid {0} for Control Number "{1}" and Trailer Number "{2}".');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 125,'Current datetime is %s hours after Appointment Time at stop %s and Arrival message has not been received');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300029,'Shipment is unmatched. Match pay is not allowed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700115,'Origin and Destination facilities cannot be the same');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910016,'Order has used late tolerance factor.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910017,'Shipment has used late tolerance factor.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5000401,'Missing field(s):%s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 126,'PO %s has been pending acceptance for more than %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 127,'Purchase Order %s has been updated by and is pending acceptance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 128,'Business partner %s has accepted PO %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 129,'Business Partner %s has declined PO %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 130,'PO %s Recalled');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7510001,'Entered Freight Amount is less than system generated amount.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7500001,'Both Salvage Amount and Salvage Percentage cannot be entered.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7500002,'Salvage Amount cannot be greater than merchandise amount.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7500003,'Settlement Amount cannot be greater than balance amount.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7500004,'Summation of Paid amount and Settlement amount cannot be greater than total claim amount.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 131,'Cargo Claim Due Date will be reached in %s days. Please review or respond to the claim.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 132,'Transportation Claim Due Date will be reached in %s days. Please review or respond to the claim.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520043,'Cannot populate the form using the entered Shipment ID and Carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520044,'Cannot populate form using the entered Freight Invoice ID and Carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100063,'User does not have permission to perform %s on Shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520045,'Cannot populate form using the entered Freight Invoice ID and Carrier. Please enter a shipment Id associated with the entered Freight invoice ID');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520046,'Cannot populate form using the entered Freight Invoice ID, Shipment ID and Carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520047,'Enter Shipment ID / Freight Invoice ID and Carrier Code to Populate Form');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520048,'Enter Shipment ID or Invoice ID.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520049,'Enter Carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7500005,'The following claim fields are missing: {0}');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520006,'The Invoice associated with the claim does not exist in the system');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310007,'Invoice %s is successfully saved');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400017,'Invalid Facility. No matching facility found for {0}.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400018,'Start date/time should precede the end date/time.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400019,'Overlapping with other maintenance schedules defined.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9610002,'Trailer status is OFFYARD');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000005,'The shipment is cancelled');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320002,'%s shipment(s) could not be saved on the invoice.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320003,'%s Item(s) could not be saved on the shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320004,'%s Accessorial(s) could not be saved on the shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720100,'%s is an invalid Business Unit');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820039,'%s can not be set when %s is provided');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720101,'%s is an invalid Origin Facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720102,'%s is an invalid Origin Facility Alias');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720103,'%s is an invalid Origin State/Prov');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720104,'%s is an invalid Origin Country');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720105,'%s is an invalid Origin Zone');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720106,'%s is an invalid Destination Facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720107,'%s is an invalid Destination Facility Alias');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720108,'%s is an invalid Destination State/Prov');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720109,'%s is an invalid Destination Country');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720110,'%s is an invalid Destination Zone');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720111,'Origin Definition is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720112,'Destination Definition is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720113,'An error prevented the Lane Hierarchy value from being calculated');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720114,'Rate Calculation Method is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720115,'Rate Calculation Method (%s) is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720116,'Rate UOM (%s) is invalid for rate type %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720117,'Rate UOM is Required when Rate Calculation Method is Rate/Unit (RPD) or Charge per Weight (CWT)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720118,'Currency Code is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720119,'Currency Code (%s) is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720120,'Tariff Id (%s) does not exist for your company');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720121,'Tier (%s) is invalid. It should be a number between 1 and 8');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720122,'Tariff is Required when Rate Calculation Method is Tariff (TRF)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720123,'Minimum size is greater then the maximum size');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720124,'Minimum and Maximum sizes cannot both be zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720125,'Maximum size is required when Minimum size is given');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720126,'Minimum size is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720127,'Minimum size is required when Maximum size is given');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720128,'Maximum size is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720129,'Size UOM is required when sizes are given');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720130,'%s is an invalid Size UOM');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720131,'Minimum Distance is greater then the maximum distance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720132,'Minimum and Maximum Distances cannot both be zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720133,'Maximum Distance is required when Minimum Distance is given');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720134,'Minimum Distance is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720135,'Minimum Distance is required when Maximum Distance is given');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720136,'Maximum Distance is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720137,'Distance UOM is required when Minimum and/or Maximum distance entered');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720138,'%s is an invalid Distance UOM');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720139,'%s is an invalid Commodity Class Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720140,'Maximum Commodity Class Code is required when Minimum Commodity Class Code is Given');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720141,'%s is an invalid Commodity Class Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 8901000,'%s is a required field.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720142,'Minimum Commodity Class Code is required when Maximum Commodity Class Code is Given');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720143,'Minimum Commodity Class Code is greater then the maximum Commodity Class Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720144,'Duplicate Rate found with the same rate information. Processing rate with sequence : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720145,'%s is not a valid Accessorial Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720146,'Lane Accessorial Effective Date is required or value given is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720147,'Lane Accessorial Expiration Date is required or value given is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720148,'Expiration Date must be greater than Effective Date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720149,'Effective/Expiration Dates overlapping with other Lane Accessorials. Accessorial Code :%s Effective Date :%s Expiration Date :%s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720150,'Accessorial cannot have a source type of MANUAL and be set for Auto Approval');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720151,'Lane Detail Effective Date is required or given value is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720152,'Lane Detail Expiration Date is required or given value is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720153,'Lane Detail Expiration Date must be greater than Effective Date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720154,'%s is an invalid Carrier Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720155,'%s is an inactive Carrier Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720156,'Carrier Code can only be null if it is a Budgeted Carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720157,'%s is an invalid Mode');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720158,'%s is an invalid Service Level');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720160,'%s is an invalid Protection Level');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720161,'%s is an invalid Secondary Carrier Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720162,'%s is an inactive Secondary Carrier Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720163,'(%s or %s) and %s is not feasible');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720164,'(%s or %s) and %s is not feasible');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720165,'(%s or %s) and %s is not feasible');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720166,'%s is infeasible with equipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720167,'Origin Facility is infeasible with equipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720168,'Destination Facility is infeasible with equipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720169,'Origin Facility is infeasible with carriers %s or %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720170,'Destination Facility is infeasible with carriers %s or %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720171,'Origin Facility is infeasible with carriers %s or %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720172,'Destination Facility is infeasible with carriers %s or %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720173,'Duplicate lanes are not allowed within the same batch import. Some lane details, rates, accessorials have been lost.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720159,'%s is an invalid Equipment Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200018,'Only bookings in the New Request status can be countered');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200017,'Only bookings in the New Request status can be declined');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220011,'Booking number is a required field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220012,'Carrier comments is required field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900043,'Shipment %s has orders originating at facility %s ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 106,'Changed Fields(s): %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900044,'Shipment %s  has orders destined to  facility %s ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 107,'Order Error from %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 105,'Shipment Error from %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 108,'Potentially late on Stop(s) : %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 109,'Appointment for Stop %s at %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 110,'Appointment for Stop %s  made %s after end of arrival window');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 111,'Appointment for Stop %s  made %s before start of arrival window');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 112,'Appointment for Order made %s arrival window');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320001,'%s: %s is an unknown value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 113,'Error From %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 115,'Current datetime is less than %s hours before %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 116,'Current datatime is less than %s hours and stops missing appointment are : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 117,'Suspended for %s hours');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 118,'Unplanned {0} hours before %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 119,'PO Due Date : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 120,'Shipment Delivered %s Days After Orders Not Reconciled');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500014,'At most one from Create orders from POs or Group orders by orig. & dest. can be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500015,'Create orders from POs parameter set should be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500016,'Group orders by orig. & dest. parameter sets should be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500017,'Create orders from POs should be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500018,'Group orders by orig. & dest. should be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330054,'%s: %s - needs Carrier Relations approval');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500019,'Add orders to existing shipment parameter set should be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500020,'Consolidate orders in to shipment parameter sets should be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900045,'Shipment can not be created for order %s as it is partially planned.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520010,'Servicel Level %s is not applicable for mode %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820040,'No record found for Reference Type %s with ID: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520011,'Mode Service level specific data not allowed if generic data is entered.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810027,'Booking With Ref. Number %s already Exists');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810028,'Booking %s is in Complete Status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810029,'Booking %s is not in Countered Status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810030,'Either Departure Date/Time or Arrival Date/Time should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810031,'No Origin Address Or Facility Specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810032,'No Destination Address or Facility Specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810033,'No Origin Port Specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810034,'No Destination Port Specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320050,'User Input Required For %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320051,'Invalid Data For');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520012,'Atleast one size information is rquired for saving a row.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000001,'Invalid Stop Sequence number on message');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5001,'Process Date is a required field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4200003,'Net amount due does not match total cost of all invoiced shipments');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810036,'State and Country should be entered if city is specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5990005,'More than one driving rule defined for the given Driver Type and Country specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5990006,'No driving rule defined for the given Driver Type and Country specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110032,'Assigned carrier %s does not support service level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110033,'Assigned service level %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110034,'%s %s is less than minimum %s %s for assigned mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110035,'%s %s is greater than maximum %s %s for assigned mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110036,'Designated carrier %s is infeasible with facility %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110037,'Assigned carrier %s is infeasible with facility %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110038,'Designated carrier %s is not active');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110039,'Assigned carrier %s is not active');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110040,'Shipment %s has a tracking message problem.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110041,'Cannot demote shipment %s.  Shipment has a tracking message problem.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110042,'Designated Mode %s does not support multi-stop shipments');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110043,'Neither outbound region %s nor inbound region %s is assigned to this user');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110044,'Designated equipment %s in not feasible with dock %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110045,'Assigned equipment %s in not feasible with dock %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110046,'Product class %s is not feasible with dock %s at facility %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110047,'Movement network is not time feasible. No windows found for Shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120000,'%s: invalid number format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120001,'%s %s is an unknown code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120002,'%s %s is an unknown code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120003,'%s: the value is too long');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120004,'%s field is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120005,'Invalid timestamp');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120006,'Invalid boolean format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120007,'The value of %s cannot be changed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120008,'Order %s is not attached to any stop actions');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120009,'Received order without Id -- order has been removed from shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120010,'%s: exceeds maximum value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120011,'Cannot import shipment in ''assigned'' status unless business function is ''assigned.''');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120012,'Cannot import shipment in ''Tendered'' status.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120013,'Cannot import shipment in ''in transit'' status.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120014,'Shipment must be recalled before assignment can be changed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120015,'Order %s is not assigned to a pickup stop');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120016,'Order %s is not assigned to a delivery stop');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120017,'Order %s is assigned to neither a pickup nor a delivery stop');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120018,'Cannot import shipment with ''assigned'' business function unless status is ''assigned.''');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120019,'Unknown accessorial code %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120020,'Order %s is picked up at more than one stop.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120021,'Order %s is delivered at more than one stop.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120022,'Shipment cannot be saved as a recurring template because it does not have a %s Date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120023,'%s id %s already exists');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130000,'Carrier %s has a communication method of ''%s''.  Please manually recall the shipment in the system, and %s the carrier to notify them');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130001,'Carrier %s has a communication method of ''%s''.  Please manually create a tracking message for the shipment update in the system, and %s the carrier to notify them');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130002,'Shipment has been tendered and Carrier %s has a communication method of ''%s''.  Please manually recall the shipment in the system, and %s the carrier to notify them');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130003,'Shipment %s successfully created.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140000,'Distance not retrieved on one or more stops');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140001,'Unknown business partner id %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140002,'No Total Cost on assigned collect shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140003,'Rerated shipment with a spot charge ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140004,'The following Shipment fields are missing: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140005,'No billing method');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140006,'Pickup window start is after pickup window end on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140007,'Delivery window start is after delivery window end on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140008,'Pickup window start is after delivery window end on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140009,'Pickup window end is in the past');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200000,'Unknown facility %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200002,'Invalid address on Stop #%s.  Must specify Street, City, State/Prov, Country, and Postal Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200003,'A postal code or city and state is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200004,'Without supplying an address, a facility is required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200005,'A stop cannot have more than one action');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200006,'No stop action specified for stop.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200007,'Distance was not imported on stop #%s.  This stop belongs to a shipment for which all distance values should have been provided.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200008,'Distance was imported on stop #%s.  This stop belongs to a shipment for which no distance values should have been imported.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200009,'Invalid address on Stop #%s. Invalid Postal code, State code combination specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1200010,'Invalid address on Stop #%s. Invalid State code, Country combination specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1210000,'The arrival is after the departure');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1220000,'Invalid number format "%s"');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1220001,'Unknown code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1220002,'Unknown value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1220003,'The value is too long');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1220004,'%s field is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1220005,'Invalid timestamp');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1220006,'Invalid boolean format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1220007,'%s: exceeds maximum value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1240000,'The following fields are missing on Stop #%d: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1300000,'If a facility is supplied, a dock is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1300001,'It is invalid to supply a dock without also supplying a facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1300002,'Dock %s does not exist');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1310000,'Stop action type %s is not compatible with dock type %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1320000,'Invalid number format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1320001,'Unknown code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1320002,'Unknown value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1320003,'The value is too long');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1320004,'%s field is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1400001,'Unknown Size Unit Of Measure');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1410000,'Unknown Business Partner Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1410001,'The pickup is after the delivery');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1420000,'%s: invalid number format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1420001,'Unknown code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1420002,'Unknown value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1420003,'The value is too long');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1420004,'Invalid timestamp');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1420005,'%s field is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500000,'Order origin facility %s does not exist in the system');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500001,'Order origin dock %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500002,'Order destination facility %s does not exist in the system');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500003,'Order destination dock %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500004,'Order origin address missing and no origin facility supplied');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500005,'Invalid origin address on order.  Must specify Street, City, State/Prov, Country, and either Postal Code or County.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500006,'Order destination address missing and no destination facility supplied');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500007,'Invalid destination address on order.  Must specify Street, City, State/Prov, Country, and either Postal Code or County.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500008,'Unknown product class code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500009,'Unknown product class value %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500010,'Unknown business unit code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500011,'Unknown protection level code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500012,'Unknown protection level value %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500013,'Shipper %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500019,'Order %s already exists');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500020,'Order %s does not exist');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500021,'Order %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500022,'No region found for order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500023,'Unknown size unit of measure %s on order size');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500024,'Unknown order note type %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500025,'No pickup or delivery window on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500026,'It is invalid to supply a destination dock for an order without also supplying a destination facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500027,'It is invalid to supply an origin dock for an order without also supplying an origin facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500028,'Unknown product class code %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500029,'Unknown product class value %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500030,'Unknown protection level code %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000000,'Shipment ID does not match existing shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5990003,'Driver Exception can not be scheduled for past dates.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5990004,'Driver Schedule hours %s exceeds maximum duty time of  %s hours.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320000,'%s: %s is an unknown code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000004,'Message is a complete duplicate of message');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000003,'Message received while shipment has an import error');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410014,'The Protection Level of Shipment ID %s is not feasible with any of the Equipments specified in the CM Parameters for the Assigned carrier Code of Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410015,'The Product Class of Shipment ID %s or the Orders of Shipment %s is not feasible with the Assigned Equipent Code of Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500031,'Unknown protection level value %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500032,'Unknown business unit value %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500033,'If origin facility is specified, origin dock must be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500034,'If destination facility is specified, destination dock must be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500035,'Unknown size unit of measure %s on order line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500036,'Unknown creation type code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500037,'Unknown creation type value %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500038,'Unknown currency type code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500039,'Unknown currency type value %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500040,'Unknown designated mode code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500041,'Unknown designated mode value %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500042,'Unknown commodity class code %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500043,'Unknown commodity class value %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500044,'Unknown size uom code %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500045,'Cannot switch between PO and Order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500046,'Can only cancel a PO in the Open status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500047,'Cannot delete PO Line Item %s with status %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500048,'Cannot change uom of PO Line Item %s with status %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500049,'Unknown SKU name, %s, on PO Line Item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500050,'SKU %s on line item %s does not have size uom %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500051,'Unknown PO line item %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500052,'PO line item %s on line item %s is not ready to ship');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500053,'PO %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500054,'PO %s is cancelled');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500055,'PO %s has import errors');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510006,'Designated equipment %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510007,'Designated service level %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510008,'Designated carrier %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510009,'Designated carrier %s is not active');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510010,'Designated carrier %s does not own equipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510011,'Designated carrier %s does not support mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510012,'Designated carrier %s does not support service level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510013,'Pickup window does not intersect dock hours on order %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510014,'Delivery window does not intersect dock hours on order %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510015,'Neither outbound region %s nor inbound region %s is assigned to this user');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510017,'Due Date %s is before current date %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510018,'Cancel Date %s is before current date %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510019,'PO Line Item %s size (%s) < total size on orders (%s).');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510020,'Origin Dock % s is not feasible with Order Designated Equipment %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510021,'Destination Dock % s is not feasible with Order Designated Equipment %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510022,'Origin Dock %s is not feasible with Order Product Class %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510023,'Origin Dock %s is not feasible with Order Line Item(s) Product Class.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510024,'Destination Dock %s is not feasible with Order Product Class %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510025,'Destination Dock %s is not feasible with Order Line Item(s) Product Class.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510026,'Order %s could not be imported : the attempted origin address update caused the poolpoint shipment %s to have more than two stops.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510027,'Order %s could not be imported : the attempted destination address update caused the poolpoint shipment %s to have more than two stops.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520000,'%s %s has invalid number format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520001,'%s %s is an unknown code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520002,'%s %s is an unknown value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520003,'%s %s has invalid number length');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520004,'%s %s has invalid timestamp format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520005,'%s is a required field ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520006,'The value of %s cannot be changed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520007,'No data was provided for this order except the stop action(s) to which it is attached');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520008,'Order %s has duplicate line item with id %s -- duplicate has been removed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520009,'Order already has line item with id %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520010,'%s: exceeds maximum value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520011,'%s: invalid date format %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520012,'%s cannot be negative');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540000,'Unknown Business Partner ID %s on Order %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540001,'No baseline cost on order %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540002,'The following Order fields are missing: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540003,'Pickup window start is after pickup window end on %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540004,'Delivery window start is after delivery window end on %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540005,'Pickup window start is after delivery window end on %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540006,'Pickup end is in the past on %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540007,'No billing method on %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540008,'Invalid origin address %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540009,'Invalid destination address %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540010,'PO Line Item(s) %s have non-Open status but are not ready to ship.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540011,'PO with non-Open status had key field(s) change: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540012,'Business Unit is different on PO');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540013,'Origin is different on PO');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540014,'Destination is different on PO');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540015,'Pickup window does not overlap PO pickup window');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540016,'Delivery window does not overlap PO delivery window');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540017,'Line items larger than PO line items: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1600001,'Protection Level already exists');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1600002,'Protection Level %s: Rank %s already exists');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700000,'%s already exists. No duplicates are allowed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700001,'Update failed. Either the record was modified by someone else or it was deleted.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700002,'%s cannot have negative value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700003,'Shipper ID %d is invalid. No matching shipper company found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700004,'Shipper ID %d is invalid. No matching enabled shipper company found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700005,'The list of Facilities has duplicates. Facility with Alias "%s" and Direction "%s" is used more than once. Please check all your aliases and remove Facility-Direction pair duplicates.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700006,'A Facility could not be identified. More than one Facility with Alias "%s" exist for Shipper %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700007,'A Facility could not be identified. Facility with Alias "%s" does not exist for Shipper %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700008,'Carrier ID %d is invalid. No matching carrier company found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700009,'Carrier ID %d is invalid. No matching enabled carrier company found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700010,'The list of %s must not have duplicates. "%s" is used more than once.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700011,'Carrier Company Name does not match Carrier Company Id');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700012,'%s is too big. Please enter a smaller value.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700013,'Effective Date past Expiration Date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700014,'Exchange rate for this Date range and currencies already exists.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700015,'Exchange rate for this Date range and currencies already exists.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700017,'From currency and To Currency cannot be the same');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700019,'Invalid date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700021,'Invalid conversion rate entered.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700022,'The dock hours overlap with existing dock hours.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700023,'%s is out of range. Allowable values are between 0 and 24 hours.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700050,'Business Partner with Id "%s" does not exist.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700051,'At least one size is required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700052,'A SKU already exists with ID %s for business partner %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700053,'SKU does not exist with name %s for business partner %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700054,'Duplicate size uom %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700055,'%s must be before %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700056,'Sort Windows already exist for Destination Facility %s (eventually with a different alias). No duplicates are allowed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700057,'At least one set of start/end times is required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200062,'Performance Factor Value was modified by another user. If you still want to perform this operation please try again');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200004,'%s must be before %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200005,'A carrier code is required in order to accept');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200006,'Shipment with Id %d not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200007,'Bid process has closed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200008,'Shipment %s: No longer tendered');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200009,'Shipment Id %d: response failed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200010,'Max limit of %d master codes has been reached');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3010005,'Accessorial with code %s already exists.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200011,'Master code already exists');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200012,'Selected master code is not mapped');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200013,'Master code is invalid for this shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200014,'Shipment %s: Must be tendered, or accepted and not in transit to decline');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220000,'%s can accept up to %d characters');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220001,'%s is a required field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220002,'%s should be in mm/dd/yyyy date format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220003,'No shipment Id(s) specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220004,'Shipment with Id %d not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220005,'Shipment %s: response is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220006,'Shipment %s: spot rate has invalid number format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220007,'Shipment %s: spot rate is greater than maximum allowed value of %f');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220008,'Shipment %s: spot rate must be positive');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220009,'Shipment %s: spot rate currency type is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2220010,'Shipment %s: invalid spot rate currency type: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320001,'At least one of the required fields must be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320002,'Both start date and end date must be provided for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320003,'Invalid start date for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320004,'Invalid end date for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320005,'Start date must be less than or equal to the end date for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320006,'Invalid format date for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320007,'The difference between start date and end date for %s cannot be greater than %s days.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320008,'Field  %s has been selected more than once.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320009,'The filter name field can not be null.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320010,'The default filter must be private.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320011,'Only the owner can modify the filter.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320012,'Invalid numeric field: %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320013,'Name "%s" is already in use.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320014,'A database error prevented this filter to be validated.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320015,'One of the combinations that define a valid origin address must be provided.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320016,'City, State and Country are all required for a valid origin address.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320017,'State and Country are required for a valid origin address. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900022,'Shipment %s could not be added to the workspace successfully - %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700010,'Aggregator must be used for runs where Could Ship Through Pool Points are considered.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900029,'Cannot deconsolidate shipment %s. It depends on shipments %s which have a higher status than PLANNED.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320034,'The maximum filter description size is 250 characters.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900033,'Must-ship-through Cross Docks are not allowed in Consolidator Runs.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900034,'Alternate Paths are ignored in Regular Consolidator Runs.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900030,'Valid path. But another valid path has been used during Cons. Optimization for this order.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900031,'Path used for this Order.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900032,'New Path created for this Path Set after this Consolidation Optimization run.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900035,'The Path''s Cross Dock does not match the Run Template''s Cross Dock.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900036,'Paths with more than one could-ship-through Cross Dock are not allowed in Consolidator Runs.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900037,'Could-ship-through Cross Docks are not allowed in Poolpoint Consolidator Runs.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900038,'Could-ship-through Poolpoints are not allowed in Cross Dock Consolidator Runs.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900039,'Could-ship-through Destination Poolpoints are not allowed in Inbound Consolidator Runs.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900040,'Could-ship-through Origin Poolpoints are not allowed in Outbound Consolidator Runs.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900041,'Unexpected data found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900042,'Order size exceeds maximum size allowed for Poolpoint facilities.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320035,'Invalid unit of measurement for filter criteria');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320036,'Size UOM Value must be a valid number');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320037,'Size UOM Value must be a valid number');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220032,'Non Hazmat Carrier is Assigned To Hazmat Shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100027,'Selected shipment template is removed.Please select another shipment template.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720004,'%s for dock rate type Rate/UOM can not be zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820019,'Invalid last modification source type.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820018,'Invalid creation source type.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700105,'Error reading carrier id.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500056,'Purchase Order must have at least one line item');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2400003,'The default consolidation mode and the parcel mode cannot be set on same mode');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540018,'Total order size(s) are greater than the capacity of any equipment that exists in the system');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540019,'Product class %s of line item %s is infeasible with product class %s of line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700106,'%d %d is invalid. No matching payee company found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720005,'%s specified for this poolpoint facility does not exist');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720006,'Designated carrier specified for this poolpoint facility either does not exist or is inactive.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140010,'No Total Cost on assigned non collect shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140011,'No baseline cost on shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540020,'Order not conforming to paths');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540021,'Delivery requirement is Residential and the designated carrier does not support a parcel mode.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540022,'Designated mode is a parcel mode, and the order exceeds maximum parcel size limits.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540023,'Delivery requirement is Residential and the order exceeds maximum parcel size limits');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100028,'Unknown Delivery Required code %s on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140012,'%s mode is a parcel mode and order %s violates order size limit constraints.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140013,'%s mode is a parcel mode and the shipment has more than 99 orders.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140014,'%s mode is a parcel mode and orders have different Packaging, Residential Delivery Flag, Dropoff/Pickup, or boolean Custom Attributes.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1140015,'%s mode is not a parcel mode and shipment is marked for, or has order(s) marked for residential delivery.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130004,'Designated');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130005,'Assigned');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500057,'Unknown Delivery Required code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500058,'Unknown Drop Off Pick Up code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500059,'Unknown Packaging code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540024,'Delivery requirement is Residential and the designated mode is a non-parcel mode.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100029,'Unknown Drop Off Pick Up code %s on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100030,'Unknown Packaging code %s on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110048,'Protection Level %s is not feasible with dock %s at facility %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510031,'Destination Dock %s is not feasible with Order Line Item(s) Protection Level.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510030,'Destination Dock %s is not feasible with Order Protection Level %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510029,'Origin Dock %s is not feasible with Order Line Item(s) Protection Level.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1510028,'Origin Dock %s is not feasible with Order Protection Level %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000022,'Cannot create a Counter message for a shipment other than in Tendered state');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820020,'%s is empty, %s is a required field.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820021,'Could not set value for field: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700024,'%s already exists for %s. No duplicates are allowed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820022,'%s is invalid. It should be a number between %s and %s (inclusive).');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820023,'Finite value not found for field %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720007,'%s %s is invalid. The current facility does not have a dock with this Id.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720008,'Note %s with Dock Id %s is invalid. A Dock Comment cannot also be a reference.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720009,'Note List IS invalid. Too many REFERENCES are declared. The maximum IS $d.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320032,'When one of the required fields contains wildcards, at least one other required field must be provided. The second required field cannot contain wildcards.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5990002,'Overlapping with other Driver Schedule Exception defined.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 320008,'Invalid Billing Method');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 340003,'Invalid CM ID %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000007,'Accept Message ignored as shipment not tendered to carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210015,'Server Error. Contact System Administrator.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210016,'Shipment not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210017,'Shipment in open CF-C/CF-D cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210018,'carrier Company already invited in current cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210019,'CF-C/CF-D not allowed for the mode');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210020,'Business hours closed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210021,'Current time not within Business hours');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210022,'Business Process is Assigned');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200068,'Assigned Shipment Configuration Tender Response Time Minutes Field is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200069,'Assigned Shipment Configuration Tender Response Time should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200070,'Assigned Shipment Configuration Tender Response Time is not allowed to be zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200071,'Specified RS Area is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200072,'Specified Business Unit is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200073,'Specified Product Class is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200074,'Specified Protection Level is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200075,'Specified Equipment is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200076,'Specified Mode is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200077,'Specified Service Level is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200078,'Specified Business Partner is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200079,'Attribute Value should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200080,'Attribute is unknown');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200081,'At least one Resource Configuration Attribute should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200082,'Configuration name should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200083,'Performance Factor not saved. User is not allowed to have more than eight Static Performance Factors');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200084,'Default Value Field is invalid for %s. It should be in the range of 0 to %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200085,'Default Value Field is invalid for %s. It should be in the range of 0 to %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200086,'Another Performance Factor is defined with the same name. Performance Factor name has to be unique');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200087,'Early Tolerance-Point Hours are invalid for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200088,'Early Tolerance-Point Minutes are invalid for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200089,'Early Tolerance-Window Hours are invalid for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200090,'Early Tolerance-Window Minutes are invalid for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200091,'Late Tolerance-Point Hours are invalid for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200092,'Late Tolerance-Point Minutes are invalid for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200093,'Late Tolerance-Window Hours are invalid for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200094,'Late Tolerance-Window Minutes are invalid for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200095,'Level Applied should be selected for %s. Please select from the Level Applied drop-down');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200096,'Unexpected value selected for Level Applied for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200097,'Please specify both facility and direction');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200098,'PF Values already exist for specified carrier, facility and direction');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200099,'%s Performance Factor Value is Invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200100,'No matching lanes found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200101,'DistanceTime Engine: Origin location not found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200102,'DistanceTime Engine: Destination location not found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200103,'Country Code is required if State is specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200104,'Country Code is required if City is specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200105,'Country Code is required if Postal Code is specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320018,'One of the combinations that define a valid destination address must be provided.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320019,'City, State and Country are all required for a valid destination address. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320020,'State and Country are required for a valid destination address. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320023,'A valid origin address requires both postal code and country code.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320024,'A valid destination address requires both postal code and country code.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320025,'At least one of the origin or destination addresses must be provided. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320026,'This filter doesn''t support wildcards.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320027,'Invalid number format for the lane id or the sequence of lane ids. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320028,'Filtering on Lane ID ignores any other fields. Please clear all the other fields.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320029,'More values are expected for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320030,'The maximum filter name size is 24 characters.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320031,'Only one facility is allowed for ''All Lanes'' search.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500000,'Run template %s already exists');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500001,'Run template %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500002,'Business Unit code %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500003,'Business Unit value %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500004,'No pickup or delivery threshold window');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500005,'Product Class code %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500006,'Product Class value %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500007,'Template %s is missing engine specific parameters');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500008,'No order selection criteria specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500009,'%s must be <= %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2520000,'%s %s has invalid number format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2520001,'Unknown code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2520002,'%s %s is an unknown value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2520003,'%s %s has invalid number length');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2520004,'%s %s has invalid timestamp format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2520005,'%s is a required field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2520008,'Parameter set ''%s'' is currently used by these run templates: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2520009,'Parameter set ''%s'' does not contain any parameter values.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2600000,'Auto approval rules must have at least one constraint');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2600001,'Region %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2620000,'Valid percentage values are between 0.0 and 100.0 (inclusive)  for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700000,'Exactly one origin consolidation region must be selected, unless a Virtual Facility is specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700001,'The origin region must contain exactly one facility, unless a Virtual Facility is specified. The region must not specify any states.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700002,'Exactly one destination consolidation region must be selected, unless a Virtual Facility is specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700003,'The destination region must contain exactly one facility, unless a Virtual Facility is specified. The region must not specify any states.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700004,'A Cross Dock Facility and a Virtual Facility may not be used together.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700005,'A Cross Dock Facility and Could-Ship-Through Pool Points may not be used together.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700006,'The specified Cross Dock Facility cannot be found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700007,'The specified Virtual Facility cannot be found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700008,'The Facility specified as the Cross Dock Facility does not support Cross Dock Facility operations.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700009,'The Facility specified as the Virtual Facility does not support Pool Point operations.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2800001,'The RS area "%s" value "%s" already exists.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2800002,'A Database Error prevented the required operation to be executed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2800003,'RS Area field "%s" length larger than "%s"');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2800004,'Commitment greater than capacity for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2820001,'The field "%s" is a required field.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2900000,'%s already has this combination of Mode, Carrier, Service Level, From, and To.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2900001,'This schedule is missing a list of hours.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2920000,'Expecting yyyy-mm-dd hh:mm:ss.0 format for Last Modification Timestamp');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000001,'Rate must be greater than the minimum rate.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000002,'Accessorial %s has a data source of ''Manual'' and cannot have Auto Approve set to ''true''.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000003,'The rate details are overlapping with another rate. Duplicate Rate Sequence Id : %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000004,'The maximum quantity must be greater than the minimum quantity.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000005,'Accessorial with duplicate code %s not allowed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000006,'A currency must be specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000007,'An overlapping %s range has already been found for this carrier parameter. "%s" is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1062,'Order Status Unplanned %s days before planning due date %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000008,'A duplicate carrier parameter (carrier/equipment/business unit/date range) already exists.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000009,'A duplicate budgeted cost (mode/equipment/service level/protection level/overlapping date range) already exists on this lane.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000010,'Minimum and Maximum quantities are zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200063,'Performance Factor Value Function was modified by another user. Please refresh and try again');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500081,'Unknown acceptance status value %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500084,'Business partner ID should be specified if Acceptance Required is selected');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500085,'Bill To Facility %s does not exist in the system');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500086,'SKU %s is marked for deletion.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100000,'PO Terms ID should not be greater than 100 characters');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210025,'Invalid Continous Move');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210026,'Assigned carrier''s communication mode is phone or fax');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210027,'Shipment ID: %s has been updated by %s on %s. Your changes are no longer valid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210028,'A carrier code is required when assigning the shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210029,'Cannot start CF-C/CF-D because shipment has designated carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210030,'Unable to invite Carrier company because it does not have any active or feasible carrier code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210031,'Shipment should have Key DTTM to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210032,'Selector is currently running for the shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210033,'Shipment status should be Available or Assigned to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210034,'Shipment Key DTTM should be in future to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210035,'Shipment in waiting to open CF-C/CF-D cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210036,'Shipment status should be Tendered, Accepted, In Transit or Delivered to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220001,'The carrier code is not valid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220002,'The carrier is not feasible with the selected facilities.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220003,'The assigned carrier code is not equal to the designated carrier code.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220004,'The assigned equipment is not equal to the designated equipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220005,'The assigned service level is not equal to the designated service level.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220006,'The assigned moe is not equal to the designated mode.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220007,'The shipment size isnot feasible with the assigned equipment capacity.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220008,'The protection level is not feasible with the assigned equipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220009,'The shipment''s product class is not feasible with the assigned equipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220010,'The shipment''s order(s) product class is not feasible with the assigned equipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220011,'The shipment facilities are not feasible with the assigned equipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220012,'The assigned carrier does not support the assigned equipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220013,'The assigned carrier does not support the assigned service level.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220014,'The assigned carrier, %s, does not support the assigned mode, %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220015,'The shipment size, %s %s, is larger than the mode size limit, %s %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220016,'The shipment size is smaller than the mode size limit.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220017,'The shipment pickup end date has already passed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220018,'The shipment does not have an associated rate.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220019,'Assigned carrier code has previously rejected this shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220020,'The shipment is multistop, but the selected mode is not a multistop mode.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220021,'The assigned carrier code is not equal to the recommended carrier code.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220022,'The assigned equipment is not equal to the recommended equipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220023,'The assigned service level is not equal to the recommended service level.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200010,'Capacity Finder-Contract Cycle Carriers Invited Field should have at least one selection');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200011,'Capacity Finder-Dynamic Cycle Carriers Invited Field should have at least one selection');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200012,'Only Capacity Finder-Contract and Dynamic Cycles are allowed to have carriers Invited Field populated');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200013,'Only Capacity Finder-Contract and Dynamic Cycles are allowed to have Duration Field Specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200014,'Duration should be specified for this Cycle Type');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200015,'Duration is not allowed to be zero for this Cycle Type');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200016,'Start X hrs. before Key Date/Time field should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200017,'Key DTTM field should be specified for the first cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200018,'Cycle Key Date Time not in decreasing order relative to previous cycles');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200019,'%s Performance Factor Value should be a Float');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200020,'Performance Factor %s is currently used in one or more Optimization Value Functions and can not be deleted');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200021,'Minimum Data Field should be specified for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200022,'Minimum Data Field is invalid for %s. Allowable values are integers between 1 and 100,000');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200023,'# of weeks tracked Field should be specified for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200024,'# of weeks tracked Field is invalid for %s. Allowable values are integers from 1 to %d');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200025,'Early Tolerance-Point Field should be populated for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200026,'Early Tolerance-Point Field is out of range for %s. Allowable values are between 0 and 24 hours');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200027,'Early Tolerance-Window Field should be populated for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200028,'Early Tolerance-Window Field is out of range for %s. Allowable values are between 0 and 24 hours');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200029,'Late Tolerance-Point Field should be populated for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200030,'Late Tolerance-Point Field is out of range for %s. Allowable values are between 0 and 24 hours');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200031,'Late Tolerance-Window Field should be populated for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200032,'Late Tolerance-Window Field is out of range for %s. Allowable values are between 0 and 24 hours');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200033,'Value Function Row %s Min Value should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200034,'Value Function Row %s Min Value invalid. Please enter a positive number between 0 and 99,999');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200035,'Value Function Row %s Max Value should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200036,'Value Function Row %s Max Value invalid. Please enter a positive number between 0 and 99,999');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200037,'Value Function Row %s Penalty should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200038,'Value Function Row %s Penalty is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200039,'Value Function Row %s has its Min Value higher than Max Value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200040,'Value Function Row %s has its Min Value equal to its Max Value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200041,'Value Function Row %s Min Value is not equal to Row %s Max Value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200042,'First Value Function Row Min Value is not UNDER');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200043,'Last Value Function Row Max Value is not OVER');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200044,'Absolute Commitment Bonus is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200045,'Percentage Commitment Bonus is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200046,'CM Bonus is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200047,'Attribute value is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200048,'Attribute value is not specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200049,'Attribute defined twice');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200050,'Cycle Tender Response Time Hours Field is Invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200051,'Cycle Tender Response Time Minutes Field is Invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200052,'Cycle Duration is Invalid. Allowed values are integers from 15 to 1440');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200053,'Cycle Start Rule Field is Invalid. Allowed values are integers from 15 to 1440');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200054,'Key Date/Time Field is Invalid. Allowed values are integers from 0 to %d');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200055,'Base Configuration Tender Response Time Hours Field is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200056,'Base Configuration Tender Response Time Minutes Field is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200057,'Base Configuration Tender Response Time should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200058,'Base Configuration Tender Response Time is not allowed to be zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210023,'Shipment status should be Available to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210024,'Shipment not tendered to the carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200059,'Configuration Name is already in use by another configuration');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200060,'This configuration %s was modified by another user. The new information is displayed below. If you still want to perform this operation please re-enter your changes and save them.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500190,'Wave Option cannot be updated if order is in Planned or higher status.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500098,'Invalid Origin Facility %s for Business Partner %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200064,'Carrier Code is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200065,'Facility Alias Id %s is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200066,'Carrier Code should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200067,'Assigned Shipment Configuration Tender Response Time Hours Field is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1020006,'One of %s, %s, %s or %s should be entered. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520014,'%s has not granted you permission to create %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100031,'The frequency must be less than 1000');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100032,'The number of offset hours must be less than 10000');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100033,'The number of shipments to be created must be less than 10000');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100034,'RecurrenceRecord has no recurrence type.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100035,'RecurrenceRecord has no recurrence period.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100036,'Please specify the frequency for this recurrence pattern.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100037,'RecurrenceRecord has no company id.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100038,'RecurrenceRecord has no id.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100039,'Please specify the number of shipments to be created.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100040,'Please specify the offset in hours.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100041,'Please specify a start date for the recurrence pattern.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100042,'Start date must be earlier than end date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100043,'Shipment Template cannot be associated with a recurrence pattern because it doesnot have a %s Date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130006,'Shipment %s has hard check errors and cannot be saved');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130007,'Shipment %s has hard check errors .It cant be brought into Edit Dates screen');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130008,'Shipment %s is cancelled .It cant be brought into Edit Dates screen');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130009,'Shipment %s has status greater than Accepted.It cant be brought into Edit Dates screen.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600004,'You cannot combine less than 2 shipments.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600005,'There was an error that caused some shipments not to be retreived.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600006,'Cannot combine shipments because they do not all have the same origin destination pairs.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600007,'No orders specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600008,'No valid orders for operation ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600009,'Not all assignments were made');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600010,'Some orders could not be moved.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600011,'The order list was empty, no shipments were created.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600012,'The created combined shipment had no valid movements. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600013,'A problem occured while assigning the order to multiple shipments. Please refresh the workspace and try again.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600014,'A problem occured while unassigning the order from the current shipments ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600015,' problem occured while assigning the order to new draft shipments');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600016,'A problem occured while assigning the order to existing draft shipments ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600017,'Not all requested shipments were removed ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600018,'Could not remove the orders from all the shipments.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600019,'Fatal Exception occured while created shipment from existing movements.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600020,'The manual consolidation workspace has changed. Please refresh the workspace and try again.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600021,'No shipments specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600022,'Could not run validation on the shipment .Unable to load shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600023,'There was a problem performing this operation (%s). Exception caught: %s  ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600024,'Operation failed: Some existing shipments could not be added to the current workspace:  %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600025,'Not all requested orders removed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600026,'No Shipment Selected');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600027,'Please select shipments created outside the workspace');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600028,'Could not deconsolidate draft shipment Id %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600029,'Could not promote/demote draft shipment Id %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600030,'Error occured while trying to retreive shipment from database.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1111111,'Incoterm ID cannot be updated when the order status is Planned or higher.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600031,'Found %s shipments with the same Id. This is an invalid state. Please refresh the workspace.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600032,'Shipment %s has already been deconsolidated.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600033,'There was no shipment to be loaded with id %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600034,'Internal error occurred during full validation.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600035,'There was an error persisting shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600036,'There was a problem with the transaction processing: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600037,'There was a problem persisting stop %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3610002,'Order %s is not on shipment %s	anymore. This order could not be assigned to a new Draft Shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600038,'Order  is on shipments %s that could not be moved to the workspace: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120025,'Shipment %s, Stop %s has invalid timestamp format for arrival start : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120026,'Shipment %s, Stop %s has invalid timestamp format for arrival end : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200142,'Please specify facility and direction');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120027,'Shipment %s, Stop %s has invalid timestamp format for departure start : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120028,'Shipment %s, Stop %s has invalid timestamp format for departure end : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600001,'The Origin Facility is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600002,'The Destination Facility is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600003,'The Pickup Start Time is greater than the Pickup End Time');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600004,'The Delivery Start Time is greater than the Delivery End Time ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600006,'The Max Number of Allowed Appointments is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600005,'The Pickup End Time is greater than the Delivery Start Time');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500061,'Unknown Budgeted Cost Currency code %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600007,'Facility Alias %s is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500010,'No origin consolidation regions can be selected for a batch template run.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500011,'No destination consolidation regions can be selected for a batch template run.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500012,'%s run template %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820030,'Open Time must be before Close Time.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820031,'An exception is already set for the date:%s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100045,'Designated or Assigned Mode %s does not support multi-stop shipments.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500062,'Unknown Actual Cost Currency code %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820032,'Max Size 1 value cannot be less than the Min Size 1 value.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820033,'%s is not a valid value.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820034,'%s already has code %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820035,'%s exceeds maximum length of %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820036,'No Finite Value type specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820037,'No Finite Value code specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820038,'No Finite Value value specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500063,'Unknown Purchase Order Line Item on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500064,'Missing Purchase Order Line Item ID');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1800004,'%s "%s" exceeds max length %d.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1800005,'%s "%s" exceeds max number of digits %d.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1800006,'%s "%s" is an invalid numeric value.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1800007,'%s "%s" is an invalid date, the valid format is "YYYY-MM-DD".');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1800008,'The range of the valid values should be between %d and %d.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700112,'%s %s is invalid. No matching payee company found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2900002,'The carrier %s does not support Service Level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700025,'Poolpoint processing time is required for Poolpoint Facilities.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2900003,'The carrier %s does not support Mode %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400025,'This appointment does not exist. It may have been deleted by another user.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100046,'Invalid action.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700049,'Maximum SKU size exceeded');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1000025,'Commodity Class Size Value is too large.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000025,'%s cannot be greater than 100.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1010016,'A Tracking Message was received for a Dock Scheduling Facility.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200016,'Only bookings in the New Request status can be accepted.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 100,'Missing field(s): %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3500001,'Specific carrier for representative rate can not be chosen without a carrier for default consolidation mode.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1530000,'Order %s successfully created.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110049,'Stop sequence is not First In Last Out');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320041,'For the %s,please enter an exact date or a complete relative date per row, but not both.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210011,'No manual cycle found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210012,'Shipment transportation responsibility not shipper');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700100,'Cannot create more than %d sizes');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700101,'Size already exists with Size UOM: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700102,'Size UOM %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700104,'Maximum value cannot be less than the minimum value.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820029,'All fields are ampty.  Please provide some values.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410009,'The protection level of the Shipment is infeasible with the dock.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410008,'The product class of the Shipment is infeasible with the dock.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1520013,'Shipper %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720000,'IMPORT_RG_LANE: Value specified larger than allowed for the column');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910018,'TimeFeasibility Calculation/Validation Error : Invalid Possible Arrival and Departure Windows. Possible Arrival start is greater than Possible Arrival end at stop with sequences %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910019,'TimeFeasibility Calculation/Validation Error : Invalid Possible Arrival and Departure Windows. Possible Departure start is greater than Possible Departure end at stop with sequences %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910020,'TimeFeasibility Calculation/Validation Error : Invalid Order Windows. Pickup Start Date is greater than Pickup End Date for order %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910021,'TimeFeasibility Calculation/Validation Error : Invalid Order Windows. Delivery Start Date is greater than Delivery End Date for order %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910022,'TimeFeasibility Calculation/Validation Error : Invalid Order Windows. Pickup Start Date is greater than Delivery Start Date for order %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910023,'TimeFeasibility Calculation/Validation Error : Invalid Order Windows. Pickup End Date is greater than Delivery End Date for order %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910024,'TimeFeasibility Calculation/Validation Error : Invalid appointment Date time');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910025,'TimeFeasibility Calculation/Validation Error : Unable to find cross dock sorting windows');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000000,'Daily Surge Capacity value is less than carrier daily capacity');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000001,'%s Surge Capacity value is less than carrier %s capacity');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000002,'%s %s is invalid with this carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000003,'%s cannot have a negative value.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000004,'Non-null mode required for representative carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000005,'Representative carrier already exists with mode %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000006,'Origin type %s cannot have %s defined.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000007,'Origin type %s must have %s defined.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000008,'Destination type %s cannot have %s defined.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000009,'Destination type %s must have %s defined.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000010,'%s %s is not valid with facility %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000011,'Protection level/equipment combination is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000012,'The Facility Alias %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000013,'The date ranges for two lanes with the same data cannot overlap.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000014,'Carrier %s has been disabled.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000015,'Country code %s is not valid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000016,'State/Province code %s is not valid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000017,'A county alone is not a valid origin.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000018,'A county alone is not a valid destination.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000019,'%s should fall between lane effective dt %s and expiration dt %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000020,'The Postal Code %s is invalid.  It must be 2, 3, 4, 5, or 6 characters');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000021,'There is another lane with the same tc_lane_id %s .');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000022,'Origin type %s must have %s defined for country %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000023,'Destination type %s must have %s defined for country %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4000024,'Carrier %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4020000,'%s %s has invalid number format.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4020001,'%s must be a positive number.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4100000,'Driving Rule already exists for the Service Level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4200000,'At least one Carrier/Distribution List needs to be added in order for the Distribution List to be Saved.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4200001,'Distribution List %s for shipper %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4200002,'Error Accessing DL member information. Please contact System Administrator.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300000,'Save conflict');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300001,'Invoice Id %s for the Carrier Code %s already exists in the system. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300002,'Invalid Carrier Code %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300003,'Invalid Invoice Shipment Status.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300004,'Shipment Id %s exists on another invoice.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300005,'Shipment Id %s exists on this invoice.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300006,'Invoice cannot be updated in the READY TO BE PAID status.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300007,'Cannot approve an incomplete shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300008,'Cannot deny an incomplete shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300009,'Cannot approve shipment.  Invoice status must be NOT APPROVED or READY TO BE PAID.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300010,'Cannot deny shipment.  Invoice status must be NOT APPROVED or READY TO BE PAID.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300011,'Invoiced shipment no longer exists');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300012,'Cannot approve shipment.  Invoice is cancelled.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300013,'Cannot deny shipment.  Invoice is cancelled.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300014,'Invoiced Shipment cannot be completed because import errors exists on either the invoiced shipment or invoiced shipment accessorials or invoiced shipment line items.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220024,'The assigned mode is not equal to the recommended mode.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220025,'This shipment exceeds the carrier capacity for this lane');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220026,'The shipment is not time feasible.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3230001,'Assigned Carrier communication method is phone. Please contact carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3230002,'Assigned Carrier communication method is unknown. Please contact carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3230003,'Assigned Carrier communication method is fax. Please contact carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3300001,'Surge data %s , field ''%s'' with errors: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400001,'Save Conflict');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400002,'CM parameters have already been created for this carrier Code or for "All" and for this Equipment or for "All" for an overlapping Effective Date Range.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400003,'Invalid CM ID %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400004,'Shipment ID %s is not the last leg of the Continuous Move %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400005,'Invalid CM Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400006,'Shipment ID %s must have a Tendered, Accepted or In Transit status to be considered for a CM. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400007,'Shipment ID %s must not be cancelled to be considered for a CM.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400008,'Shipment ID %s must not have an import error to be considered for a CM.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400009,'No valid CM Parameters found for the assigned carrier code %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400010,'The CM %s is terminated.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400011,'Validation needs to be done before calling create or extend CM.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400012,'Other Location Infeasibilities have already been associated to this CM parameter with this country code and with overlapping postal codes.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400013,'Shipment status should be tendered or greater to be recalled.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400014,'Invalid Carrier Code. No matching carrier found for code %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400015,'Carrier code is not valid with mode for which Allow CM = yes according to Base Data.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3400016,'Server Error. Contact System Administrator.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410001,'Continuous move option will not be considered for a Shipment if the keydatetime is null.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410002,'For the Resource Selection Configuration that applies for the Shipment, the Optimization Parameter "Consider CM" equals "no".');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410003,'Size of the shipment is outside the lower or upper size limits for mode %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410004,'The carrierCode %s for Shipment ID %s is not active.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410005,'The assigned mode for Shipment ID %s cannot be null.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410006,'Shipment ID %s must have an assigned mode for which "Allow CM" = "yes".');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410007,'Shipment ID %s has a designated carrier code %s which does not match Shipment ID %s assigned carrier code %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410008,'Shipment ID %s has a designated equipment code %s which does not match Shipment ID %s assigned equipment code %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410009,'Shipment ID %s has a designated service level %s which does not match Shipment %s assigned service level %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410010,'Shipment ID %s has an assigned equipment code %s which is not included in the CM Parameter Equipment List.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320042,'For the %s,please enter an exact date or a complete relative date per row.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320043,'For the %s,the time should be entered in HH:MM format.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 101,'Stop %s missing field(s): %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600008,'Duplicate of facility-to-facility schedule for same day of week is not allowed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600009,'Either origin facility or destination facility is required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 102,'Populated field(s) Shipment was imported in the Available Status. Assigned field %s was populated but will be ignored by Resource Selection.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2700011,'Atleast one origin region or destination region must be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3010006,'The length of field "%s" is larger than "%s".');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020000,'A system error ( %s can not be found ) prevented your request from being completed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020001,'At least one of these fields %s, %s, %s, %s, %s must be provided.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020002,'%s is a required field. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020003,'Both %s and %s are required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020004,'A lane with Lane Id %s, Company Id %s, Mode %s, Equipment Code %s, Service Level %s already exists.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020005,'Destination Address and Origin Address are identical. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020006,'Expiration Date must be after the Effective Date. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020007,'There''s no weight UOM set for this shipper. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020008,'Carrier is required for carrier parameters that are not global.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020009,'Carrier is required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020010,'Two or more origin-destination pairs have the same lane hierarchy value (%d): %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020011,'Remove Carrier . One cannot specify a Carrier  for Global Carrier Parameters.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3020012,'Data not saved. One cannot specify CM Discounts for Global Carrier Parameters or for Budgeted Carrier Parameters that have no Carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3100000,'Auto Deliver cannot be selected with Appointment Msg. Required, Departure Msg. Required or Arrival Msg. Required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3100001,'Auto Accept has to be No if Accept Msg. is Required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3100002,'Send Updates To Broker cannot be selected unless Broker is also selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200001,'Couldn''t Save Cycle. Only 20 non-manual cycles are allowed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200002,'Couldn''t Save Manual Cycle. Only 8 Manual are allowed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200003,'Auto Tender checkbox should be checked for the CORE Cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200004,'Auto Tender checkbox should be checked for the Selector Cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200005,'Auto Tender checkbox should not be checked for the CF-D Cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200006,'Auto Recall checkbox should be checked for the CORE Cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200007,'Auto Recall checkbox should be checked for the SELECTOR Cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200008,'Cycle Tender Response Time is not allowed to be zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200009,'Cycle Tender Response Time should be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720001,'%s already exists. No duplicates are allowed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720002,'%s is used more than once here. %s has to be unique.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720003,'At least one Line Item field must have value selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1800001,'Invalid Carrier Code. No matching carrier found for code %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1800002,'Carrier %s does not own equipment "%s".');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1800003,'A database error prevented the required operation to be executed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820000,'%s: should be an integer. %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820001,'%s should be a number. %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820002,'%s should be a date/time entry. %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820003,'%s should be a time entry. %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820004,'%s should be a date entry. %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820005,'%s can accept up to %d characters. %s is too long');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820006,'%s can accept up to %d digits. %d is too long');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820007,'%s is a required field.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820008,'%s is an invalid value for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820009,'%s is a negative value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820010,'%s is required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820011,'%s should be a monetary value with two decimals. %s is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820012,'%s should be a boolean (0 or 1). %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820013,'%s must be later than or equal to %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820014,'%s (%s) must be later than or equal to %s (%s).');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820015,'%d is too large. %s should be less than %d.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820016,'Please specify only three digits for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820017,'%s is required when %s is provided.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900000,'All orders consolidated on a shipment must have the same Transportation Responsibility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900001,'All orders consolidated on a shipment must have the same Business Unit');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900002,'Order %s has status %s.  Status must be Unplanned to be available for consolidation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900003,'Order %s is cancelled');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900004,'Order %s has errors');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900005,'Shipment %s has errors');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900006,'All pickups must come before all deliveries on a consolidated shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900007,'Order %d cannot be removed from the shipment because it is not on the shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900008,'Address of Order %s does not match address of other orders on the stop.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900009,'Order %s cannot be added to the shipment because it is already attached to the shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900010,'Order %s cannot be added to the shipment because it has already been consolidated on shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900011,'Order %s has status %s.  Status must be less than %s to be added to consolidated shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900012,'Save conflict');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900013,'Cannot deconsolidate shipment %s.  Status must be PLANNED.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900014,'Shipment %s did not receive a Business Unit from its orders');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900015,'Cannot deconsolidate shipment %s.  Shipment is cancelled.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900016,'Cannot deconsolidate shipment %s.  Shipment status is complete.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900017,'Shipment %s has already been deconsolidated.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900018,'Cannot deconsolidate shipment %s.  Shipment status is after Available');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910000,'All orders on the same consolidated shipment must have the same Billing Method');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910001,'All orders on the same consolidated shipment must have the same Rating Modifier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910002,'All orders on the same consolidated shipment must have the same Routing Guide Qualifier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910003,'Stop %s does not have an arrival window');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910004,'Arrival window for stop %s does not intersect %s business hours for dock %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910005,'Estimated Cost of shipment is null');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910006,'Baseline Cost of shipment is null');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910007,'Shipment billing method %s differs from %s, the billing method of all the orders assigned to it');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910008,'Shipment rating modifier %s differs from %s, the rating modifier of all the orders assigned to it');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910009,'Shipment routing guide qualifier %s differs from %s, the routing guide qualifier of all the orders assigned to it');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910010,'Order windows do not match on stop %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910011,'Product class %s is infeasible with product class %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1910012,'Stops %s and %s with like pickup or deliver actions exceeds Max Distance between Adjacent Stops');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000000,'%s %s is not a known size uom');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000001,'action %s is not allowed for status %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000002,'For status %s actions %s and %s are incompatible');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000003,'According to alert parameters, alerts are cancelled by the system when the shipment reaches the %s status.  There is no need to configure critical change to raise alerts in any status >= this status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000004,'Unknown status code %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000005,'Unknown critical change action code %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2120001,'Invalid numeric value for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2120002,'Invalid string length for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2120003,'The field %s is a required one.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2120004,'Invalid date format for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2120005,'The field %s should not be filled in.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2120006,'Invalid value for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2120007,'Invalid value for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2120008,'The %s field cannot have more than 250 characters.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200000,'User is not the owner of region %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200001,'A region with this name already exists');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200002,'A region must have at least one postal code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200003,'Region %s can not be deleted because it is in use');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210010,'Shipment has import errors');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000011,'Minimum and Maximum distances are zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000012,'The maximum distance must be greater than the minimum distance.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000013,'The maximum commodity must be greater than the minimum commodity.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000014,'When minimum value is given , maximum value is required for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000015,'When maximum value is given , minimum value is required for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3000016,'The Accessorial details are overlapping with another one. Duplicate Accessorial Details : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3010001,'Data Source is not distance type leave unit blank');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3010002,'No Accessorials can be included for this Data Source. Please exclude them.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3010003,'Both %s Facilty and Country cannot be null');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3010004,'%s should be less than or equal to %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5990007,'Resting time between shifts should be minimum of  %s hours.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5990008,'Driver Calendar and Start Week, both can be either empty or must have a valid values.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520050,'Required Check Number, Check Amount, Check Date and Percentage of Claim');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520051,'Invalid Check Amount.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520052,'Invalid Check Date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520053,'Invalid Percentage of Claim, Valied range 1 .. 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520054,'The check cannot be applied to the selected claims. Please adjust the percentage or claims selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520055,'Only the claim(s) with Status Filed, Active or Pending Payment can be selected to apply a check.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520056,'Only the claim(s) with Status Filed, Active, Pending payment or carrier declined can be selected for settlement');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300031,'The payment due date is earlier than the invoice receive date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330028,'%s : Invalid values for original invoice');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330027,'%s : Carrier %s is inactive');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330026,'%s : Accessorial %s needs approval');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330025,'%s : Invoices over %s %s require manager approval');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330024,'%s : Invoice destination address does not match shipment destination address');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330023,'%s : Invoice origin address does not match shipment origin address');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330022,'%s : Invoice carrier %s does not match shipment carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330021,'%s : Non preferred carrier %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330020,'%s : Carrier code %s is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330019,'%s : Invoice distance does not match and is out of tolerance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330018,'%s : Invoice distance UOM and shipment distance UOM do not match');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330017,'%s : Invoice weight does not match and is out of tolerance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330016,'%s : Invoice weight UOM does not match shipment weight UOM');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330015,'%s : Accessorial %s is not authorized');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330014,'%s : The following accessorials are out of tolerance - %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330013,'%s : The following accessorials are not authorized - %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330012,'%s : Distance and Distance UOM required');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330011,'%s : Rate type value missing');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330010,'%s : Rate value missing');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330009,'%s : Invalid destination facility %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330008,'%s : Invalid origin facility %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330007,'%s : Invalid actual weight for mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330006,'%s : The following purchase order IDs are invalid : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330005,'%s : Original invoice missing');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330004,'%s : Billing method values on the invoice, BOL(s) and invoiced shipment are not equal');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330003,'%s : No Shipment ID : cannot validate billing method, accessorial costs, carrier assignment and origin/destination information.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330002,'%s : Shipment ID %s does not exist');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330001,'%s : Shipment ID required for mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700117,'A facility cannot be the same time store and distribution center.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700118,'Once a facility is created as either store or distribution center, that cannot be changed.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330034,'%s : Computed shipment cost has already been paid on original invoice');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330033,'%s : Invoice total cost does not match and is out of tolerance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330032,'%s : Invoice total cost currency code does not match shipment total cost currency code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330031,'%s : Invoice could not be rated');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330030,'%s : Invoice rate %s does not match shipment rate %s and is out of tolerance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330029,'%s : Invoice rate type does not match shipment rate type');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300033,'Invoice ID %s was updated by %s on %s. Please refresh your data and try again.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210040,'No Static Performance Factors Defined ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320005,'Original invoice %s must be in Paid or Payment Requested status.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310006,'Invoice validation rule set missing');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320064,'Claim Amount "From" field value should be less than or equal to the Claim Amount "To" field value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320063,'Both From/To field should be present for Claim Amount');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320065,'Both Strings are required for Carrier Name');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320066,'Enter a single letter for both fields');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320067,'From field should be greater than To field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320068,'Wild characters are not allowed, Enter only letters');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300030,'BOL Number %s already exists on invoice %s with the same origin, destination, and invoice date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820043,'can not contain negative value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310005,'Shipment ID %s already exists on invoice %s with a different carrier %s ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300032,'Manually Rejected the Invoice with Reason code %s :  and Description %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310008,'Invoice has been rejected more than %s times');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110054,'No feasible equipment found to carry this shipment with more than 3 temperature ranks');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110055,'Equipment {0} doesn''t have {0} compartments to carry this shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110056,'Equipment Type {0} and Tractor type {1} are infeasible');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1110057,'Equipment Type {0} and Tractor type {1} are infeasible to carry this weight, check plated weight and tax band setup');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520013,'%s should be strictly greater than %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2820002,'%s must be a AlphaNumeric Value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330040,'%s : Total invoice accessorial cost does not match and is out of tolerance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330039,'%s : Invoice accessorial cost currency code does not match shipment accessorial cost currency code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330038,'%s : Invoice stop cost does not match and is out of tolerance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330037,'%s : Invoice stop charge currency code does not match shipment stop charge currency code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330036,'%s : Invoice linehaul cost does not match and is out of tolerance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330035,'%s : Invoice linehaul cost currency code does not match shipment linehaul cost currency code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320007,'%s Invalid Protection Level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320006,'%s Invalid Mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310009,'Invoice saved for review');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300028,'Duplicate shipment invoice on invoice ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210039,'Server error. Contact system administrator.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320008,'%s Invalid Equipment Type %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320009,'%s Invalid Service Level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 6200001,'ASN is not received for the shipment %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 6300001,'Select pickup and delivery stops for ASN');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 6300002,'Select pickup stop for ASN');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 6300003,'Select delivery stop for ASN');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 6300004,'Select default size UOM for ASN');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500065,'Missing budgeted cost currency code on purchase order line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500066,'Unknown monetary value size UOM code %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500067,'Unknown monetary value currency code %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500068,'Line item detail size UOM %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500069,'Line item size is less than detail sizes on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500070,'Purchase order size is required for line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500072,'Line item %s has both SKU ID and package type.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500073,'Invalid dangerous goods ID %s on SKU %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500074,'Invalid dangerous goods ID %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 880000,'Shipment is assigned to a booking and its Origin or destination is modified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500075,'Dangerous goods number %s on line item %s is not feasible with dangerous goods number %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500076,'Unknown planning origin facility %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500078,'Parent order ID %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500079,'Line item currency is required for line item %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500080,'Unknown acceptance status code %s on order');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200061,'Performance Factor %s was modified by another user. If you still want to perform this operation please try again');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700058,'Destination facility cannot be the same as the Cross Dock facility.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410011,'Shipment ID %s has an assigned service level %s which is not included in the CM Parameter Service Level List.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410012,'Shipment ID %s ends in a Location specified in the CM Location Infeasibility for Shipment ID %s assigned carrier code.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410013,'The Protection Level of Shipment ID %s is not feasible with the Assigned Equipent Code of Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410016,'The Product Class of Shipment ID %s or the Orders of Shipment %s is not feasible with any of the Equipments specified in the CM Parameters for the Assigned carrier Code of Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410017,'The Assigned Equipment for Shipment ID %s does not have sufficient capacity for Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410018,'There is no equipment in the equipment list for the CM Parameters that has a sufficient capacity for Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410019,'The Assigned carrier Code for Shipment ID %s is not feasible with the facilities visited by Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410020,'The facilities visited by Shipment ID %s is not feasible with the Assigned Equipent Code of Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410021,'The facilities visited by Shipment ID %s is not feasible with any of the equipments in the Equipment List for the CM Parameters for the Assigned carrier Code of Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410022,'The CM Total duration exceeds the maximum duration specified in the CM Parameters for the Assigned carrier Code of Shipment ID %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410023,'The deadhead distance between Shipment ID %s and Shipment ID %s exceeds the Maximum Deadhead distance per Leg specified in the CM Parameters.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410024,'The CM Total deadhead distance exceeds the Maximum deadhead distance specified in the CM Parameters.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410025,'Time feasibility check failed. It is not possible to get from the last stop of Shipment ID %s to the first stop of Shipment ID %s on time.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410026,'Shipment status is before ''In Transit''.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410027,'Carrier Code %s is not a Lane carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410028,'No feasible Equipment found for Shipment Id %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3410029,'The CM Total distance exceeds the Maximum distance specified in the CM Parameters.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3420001,'%s must be less than %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3420002,'%s must be less than or equal to %s. %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3420003,'Missing required field Shipment Id.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3420004,'Missing required field CM ID or Shipment ID.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3420005,'%s must be a 3-digit postal code.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3430001,'The Tendered Carrier for Shipment %s has a communication method of %s; please %s the Carrier.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3500000,'According to Critical Change Logic configuration, alerts are generated by the system when the shipment reaches a status greater than or equal to %s. Alert cancellation configuration should be consistent with Critical Change Logic configuration.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520000,'%s value should be ''%s'' or ''%s''');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520001,'%s should be strictly greater than %f and strictly less than %f.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520002,'%s should be greater than or equal to %f and strictly less than %f.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520003,'%s should be strictly greater than %f and less than or equal to %f.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520004,'%s should be greater than or equal to %f and less than or equal to %f.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520005,'%s should be strictly greater than %f.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520006,'%s should be greater than or equal to %f.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520007,'%s should be strictly less than %f.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520008,'%s should be less than or equal to %f.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3520009,'%s should be greater than or equal to %d.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600000,'Shipment %s is cancelled.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600001,'Shipment %s has import errors.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600002,'Shipment %s has invalid stop information.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3600003,'Shipment %s has an order being delivered before picked up.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3610000,'Order is manually unassigned from a must poolpoint');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3610001,'Stop with drop and hook indicator is not the last stop on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3700000,'%s is not a valid carrier for this particular mode');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200106,'Cycle Key DTTM breaks decreasing order. Cycles with higher ranks have a higher Start X hrs. before Key Date/Time field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200107,'Auto move field should be checked. Next cycle Key Date field not populated');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200108,'Key Date field should be specified. Previous cycle Auto move field not checked.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200109,'%s is too big. Up to %s characters are allowed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200110,'Please specify Origin');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200111,'Please specify Destination');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200112,'State cannot be specified if origin facility is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200113,'State cannot be specified if destination facility is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200114,'City cannot be specified if origin facility is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200115,'City cannot be specified if destination facility is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200116,'Country cannot be specified if origin facility is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200117,'Country cannot be specified if destination facility is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200118,'Postal code cannot be specified if origin facility is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200119,'Postal code cannot be specified if destination facility is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200120,'Facility cannot be specified if origin state is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200121,'Facility cannot be specified if destination state is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200122,'City cannot be specified if origin state is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200123,'City cannot be specified if destination state is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200124,'County cannot be specified if origin state is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200125,'County cannot be specified if destination state is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200126,'Postal code cannot be specified if origin state is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200127,'Postal code cannot be specified if destination state is present');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200128,'Broker Configuration Tender Response Time Hours Field is invalid. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200129,'Broker Configuration Tender Response Time Minutes Field is invalid. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200130,'Broker Configuration Tender Response Time should be specified. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200131,'Broker Configuration Tender Response Time is not allowed to be zero. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200132,'Broker Configuration Addtional Number of Times Field is invalid. Valid Values 0 through 19. ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200133,'Invalid RS Configuration Cycle.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200134,'Carrier Code %s is inactive.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200135,'Carrier Code %s has no record to update.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200136,'Carrier Code %s already has specific values.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200137,'State is required if City is specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200138,'Origin address is invalid: you must provide either a facility or a complete address (with City, State (when applicable), Postal Code and Country).');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200139,'Destination address is invalid: you must provide either a facility or a complete address (with City, State (when applicable), Postal Code and Country).');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200140,'Either Origin or Destination must be a facility or a complete address (with City, State (when applicable), Postal Code and Country).');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200141,'Carrier Code %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3201000,'Invalid Postal code, State code combination specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3201001,'Invalid State code, country combination specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210001,'Save Conflict');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210002,'Shipment status should be Recommended to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210003,'Shipment status should be Assigned to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210004,'Shipment status should be Tendered to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210005,'Shipment status should be Available or Assigned to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210006,'Shipment status should be Assigned or Tendered to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210007,'Shipment is cancelled');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210008,'No resource configuration found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210009,'Shipment has data errors');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210013,'Not in CF-C/CF-D cycle');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500099,'Invalid Destination Facility %s for Business Partner %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000007,'Update TO and Remove Line Item can not be set together (Business Unit %s, field %s, PO status %s, TO status %s)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000008,'Update Shipment action is not allowed for decreasing quantity field (Business Unit %s, field %s, PO status %s, TO status %s)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000010,'Variances are not in continuous range (Business Unit %s, field %s, %s value)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2000009,'Update Shipment action can not be set when Update TO action is not set (Business Unit %s, field %s, PO status %s, TO status %s)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500124,'RTS Pickup Start on Line Item %s should be after PO Pickup Start');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500125,'RTS Pickup End on Line Item %s should be after PO Pickup End');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330041,'%s : Actual Weight does not match the sum of PO weights');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1530008,'Purchase Order %s has been locked');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1530009,'Purchase Order %s has been unlocked');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1530010,'Purchase Order %s is already been locked by somebody');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1530011,'Purchase Order %s is already been unlocked by somebody');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1120029,'Cannot import shipment in business_process Recall (''R'')');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500170,'%s for PO Line Item %s not found.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500014,'%s length exceeded for ILS integration.  The PO will not be exported to WM');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4340001,'%s: Missing Actual Weight');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4340002,'%s: Missing Service Level');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4340003,'%s: Invoice Date should not exceed current date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4340004,'%s: Missing PRO Number');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4340005,'%s: Invoice ID should not contain special character');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4340006,'%s: Missing Proof Of Delivery (POD) on shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4340007,'%s: No Purchase Order is attached with shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4340008,'%s: No sufficient information on invoice for account coding');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4340009,'%s: Invalid Commodity Code on Purchase Order(s)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900023,'Weekly Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900024,'Monthly Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900025,'Yearly Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500164,'For purchase order %s the origin facility can not blank in line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4330053,'%s: Invoice Rate/Rate Adjustment does not match system rate and is out of tolerance');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900015,'Daily Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900016,'Sunday Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900017,'Monday Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900018,'Tuesday Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900019,'Wednesday Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900020,'Thursday Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900021,'Friday Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900022,'Saturday Commitment % cannot be greater than 100');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500087,'Terms and Conditions is selected, so Acceptence Required must also be selected.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500088,'Monetary Value Size UOM is required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3500002,'Atleast one total cost tolerance value is required if invoice total cost is used for tolerances.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130010,'Shipment %s is part of a movement Network. It cannot be brought into Edit Dates screen.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320016,'Invoice Total does not match Computed Invoice Total.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320017,'No proper exchange rates defined for currency conversion.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320018,'Invoice Total does not match Balance Due.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720022,'Invalid Internal Only Option %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900051,'External System Error related to RTS on line item {0}, rts id {1}, line item detail id {2} - error code {3} and error description {4}.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720001,'IMPORT_RG_LANE: Database Exception Raised - %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720002,'Frequency is required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720003,'Frequency %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720004,'A lane (Id : %s) in active status with the same setup but a different frequency already exists. This lane is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720005,'Invalid Address, State/Country/Postal Code Required.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720006,'Invalid Address, Check State/Country/Postal Code.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720007,'%s is an invalid Business Unit');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720008,'%s is an invalid Origin Facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720009,'%s is an invalid Origin Facility Alias');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910017,'TimeFeasibility Calculation/Validation Error : Invalid Possible Arrival and Departure Windows. Possible Arrival start is greater than Possible Departure start at stop with sequences %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400000,'Invalid Shipment %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400001,'Shipment status should be tendered or greater to create a dock appointment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400002,'Shipment %s does not stop at the facility.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400003,'Shipment %s does not stop at the dock %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400004,'The appointment start must be less than or equal to the appointment end.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400005,'Since the dock action = Shipping, only Pick-ups and Pick-up alls are allowed for the stop action on the shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400006,'Since the dock action = Receiving, only Deliver and Deliver alls are allowed for the stop action on the shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400007,'The appointment end must be less than or equal to the dock hours end time.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400008,'The appointment window must not overlap any other existing appointments.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400009,'The "Total Number of Appts Allowed" (defined for the dock hours) at a dock during the dock hours must always be greater than or equal to the total number of valid appointments at the dock during the dock hours.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400010,'Save Conflict');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400019,'An appointment exists for the shipment at the facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4400020,'Invalid Dock %s for facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410000,'The dock appointment start time is outside the dock hours.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410001,'The dock appointment start is outside the arrival window for the stop.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410002,'The dock appointment end is outside the departure window for the stop.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410003,'The product class of the orders on the Shipment is infeasible with the dock.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410004,'The assigned equipment of the Shipment is infeasible with the dock.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410005,'The appointment start time is less than the current time.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410006,'The appointment end time is less than the current time.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2320033,'Invalid %s: %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3230004,'You have recalled a shipment from a TP that was Assigned outside of OptiManage.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500000,'Same Facility cannot be used for Origin and Destination');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500001,'Overlap found with other Path Sets having the same setup');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500002,'Facility %s is not of type Poolpoint');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500003,'Facility %s is not of type Cross Dock');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500004,'The same facility cannot occur twice in one path');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500005,'Overlap found with other Path Sets having the same setup & having Poolpoints in the Base Path');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500006,'Overlap found with other Path Sets having the same setup & not having Poolpoint in the Base Path');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500007,'Expiration date cannot be before Effective Date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500008,'Alternate Path %s:  Facility %s is not of type Poolpoint');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500009,'Alternate Path %s: Facility %s is not of type Cross Dock');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500010,'Alternate Path %s:  The same facility cannot occur twice in one path');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500011,'Alternate Paths %s and %s are identical');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500012,'Date Change Request Failed. Overlap found with other Path Sets having the same setup');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500013,'Date Change Request Failed. %s %s has invalid timestamp format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500014,'Date Change Request Failed. "%s" is a required field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500015,'Add Alternate Path Request Failed. Facility %s is not of type Poolpoint');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810025,'Booking %s is in Open Status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200149,'Misrouting check method should be selected for %s. Please select from the Misrouting check method dropdown');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3200150,'Unexpected value selected for Misrouting check method for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210041,'Unable to invite carrier company, because it does not have any valid rates with non-zero linehaul cost.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500097,'Move Type cannot be updated if order is in Planned or higher status.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500189,'Priority cannot be updated if order is in Planned or higher status.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200019,'Selected master code does not have a valid contract rate for this shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1111114,'Origin ship via cannot be updated when the order status is Planned or higher.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 880001,'Shipment is assigned to a booking and quantity is increased.The booking does not have capacity to carry the additional quantity.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 880002,'Shipment is assigned to a booking and the fields are modified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2460100,'You do not have permission to modify the Postal Code table');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2460101,'Postal Code cannot be blank');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2460102,'You cannot enter more than 10 characters for the Postal Code name');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2460103,'City cannot be blank');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2460104,'You cannot enter more than 40 characters for the City name');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2460105,'Invalid Longitude value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2460106,'Invalid Latitude value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2460107,'Duplicate entries not allowed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900026,'Size UOM (%s) not valid for Carrier Mode combination');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1111115,'Destination ship via cannot be updated when the order status is Planned or higher.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540097,'PO %s Pickup Start is not populated while marking the PO Line as Ready TO Ship.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540098,'PO %s Pickup End is not populated while marking the PO Line as Ready TO Ship.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 207,'Entered shipment (or shipment searched by Detention Facility ID and PO Nbr.) is Paid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1540100,'PO %s or atleast one of its lines are marked as closed and cancelled at the same time');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 6400025,'ASN %s or at least one of its details is marked as closed and cancelled at the same time.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300060,'Concurrent users are tyring to save invoice %s for carrier %s at same time. Please try again');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200020,'%s is a hazmat shipment, and the selected SCAC cannot carry hazmat');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720024,'Additional Size UOM used for value "%s" is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720025,'Additional Size UOM used for value "%s" is a duplicate.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2200022,'%s shipment has insurance check and the selected SCAC cannot satisfy');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3220033,'Carrier not sufficiently insured for this shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210043,'Carrier company has no valid Carrier codes that can carry hazmat shipments');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210044,'Carrier company has no valid Carrier codes that have adequate insurance to carry the shipment');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500016,'Add Alternate Path Request Failed. Facility %s is not of type Cross Dock');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500017,'Add Alternate Path Request Failed. The same facility cannot occur twice in one path');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500018,'Add Alternate Path Request Failed. Alternate Paths %s is identical');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500019,'Invalid Path Set');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500020,'Path Set has no paths to save');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500021,'Date Change Request Failed. Expiration date cannot be before Effective Date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500022,'Invalid number format for the path set id or the sequence path set ids');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500023,'Filtering on Path Set ID ignores any other fields. Please clear all the other fields');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500024,'Add Alternate Path Request Failed. The Facility Alias %s is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4500025,'Alternate Path %s: The Facility Alias %s is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900019,'Order %s had a status of planned or higher and could not be added to the unassigned bucket.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900020,'Order %s could not be added to the workspace successfully. This was probably because it had already been added.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100001,'PO Terms legal text should not be greater than 4000 characters');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100002,'PO Terms legal text is a mandatory field.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100003,'PO Terms ID is a mandatory field.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100100,'PO term %s already exists.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100101,'Business partner %s already assigned.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100102,'Default term already exists.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100103,'If Default is not selected at least one business partner should be assigned.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100200,'The selected PO ID does not exist.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100203,'All parties have not accepted the selected purchase order. LPNs cannot be created until the PO is accepted.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100204,'Server error while generating LPN.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100205,'No LPN generation rule was found for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100206,'The next sequence number will exceed the maximum sequence number. Please check the LPN generation rule of %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720010,'%s is an invalid Origin State/Prov');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720011,'%s is an invalid Origin Country');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720012,'%s is an invalid Origin Zone');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720013,'%s is an invalid Destination Facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720014,'%s is an invalid Destination Facility Alias');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720015,'%s is an invalid Destination State/Prov');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720016,'%s is an invalid Destination Country');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720017,'%s is an invalid Destination Zone');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720018,'Origin Definition is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720019,'Destination Definition is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720020,'An error prevented the Lane Hierarchy value from being calculated.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720021,'Routing Guide Qualifier is not appropriate');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720022,'Routing Guide Qualifier is not in the company parameter table');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720023,'Capacity/Commitment should be a numeric number');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720024,'Capacity/Commitment should be a greater than zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720025,'IMPORT_RG_LANE_DTL: Value specified larger than allowed for the column');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720026,'IMPORT_RG_LANE_DTL: Value specified larger than allowed for the numeric column');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720027,'IMPORT_RG_LANE_DTL: Database Exception Raised - %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720028,'Import Surge Capacity: Capacity should be a numeric whole number');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720029,'Import Surge Capacity: Capacity should be a greater than zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720030,'Import Surge Capacity: Value specified larger than allowed for the numeric column');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720031,'Import Surge Capacity: Database Exception Raised - %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720032,'Carrier Code: %s must have a mode specified to be selected as Rep Carrier');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720033,'Representative carrier already exists for this lane, rep_tp_flag for carrier %s is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720034,'Effective Date is greater than the Expiration Date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720035,'%s is an invalid Carrier Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720036,'%s is an invalid Mode');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720037,'%s is an invalid Service Level');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720038,'%s is an invalid Equipment Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720039,'%s is an invalid Protection Level');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720040,'(%s or %s) and %s is not feasible (Carrier_2Carrier/Equipment)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720041,'(%s or %s) and %s is not feasible (Carrier_2Carrier/ServiceLevel)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720042,'(%s or %s) and %s is not feasible (Carrier_2Carrier/MOT)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720043,'%s is infeasible with equipment type %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720044,'Origin Facility is infeasible with equipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720045,'Destination Facility is infeasible with equipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720046,'Origin Facility is infeasible with carriers %s or %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720047,'Destination Facility is infeasible with carriers %s or %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720048,'Origin Facility is infeasible with carriers %s or %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720049,'Destination Facility is infeasible with carriers %s or %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720050,'Import Surge Capacity - Effective Date is greater than the Expiration Date');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720051,'Weekly Commitment or Weekly Commitment Pct is not zero for the given mode/frequency.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720052,'Weekly Capacity must be greater than or equal to Weekly Commitment for the given frequency');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720053,'Weekly Commitment % should be greater than or equal to zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720054,'Weekly Surge Capacity must be greater than or equal to Weekly Capacity for the given frequency');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 22000200,'%s is a hazmat shipment, and the selected SCAC cannot carry hazmat');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720055,'Weekly Commitment or Weekly Capacity is null or 0 for given frequency.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720056,'Daily Commitment or Daily Commitment Pct is not zero for the given mode/frequency.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720057,'Daily Capacity must be greater than or equal to Daily Commitment for the given frequency');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720058,'Daily Commitment % should be greater than or equal to zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720059,'Daily Surge Capacity must be greater than or equal to Daily Capacity for the given frequency');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720060,'Daily Commitment or Daily Capacity is null or 0 for given frequency');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720061,'Day of Week Commitment or Day of Week Commitment Pct is not zero for the given mode/frequency(%s)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720062,'Day of Week (%s): Capacity must be greater than or equal to Commitment for the given frequency');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720063,'Day of Week (%s): Commitment % should be greater than or equal to zero');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720064,'Day of Week (%s): Surge Capacity must be greater than or equal to Capacity for the given frequency');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720065,'Day of Week (%s): Commitment or Capacity is null for given frequency');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4720066,'%s is an invalid Secondary Carrier Code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4800000,'%s %s has invalid number format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4800001,'%s %s is an unknown code');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4800002,'%s %s is an unknown value');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4800003,'%s %s has invalid number length');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4800004,'%s %s has invalid timestamp format');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4800005,'%s is a required field ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4800006,'%s: invalid date format %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4800007,'%s cannot be negative');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4800008,'Shipper %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810000,'Booking %s does not exist');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810001,'Booking origin facility %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810002,'Booking origin dock %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810003,'Booking destination facility %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810004,'Booking destination dock %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810005,'Booking origin address missing and no origin facility supplied');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810006,'Invalid origin address on booking.  Must specify Street, City, State/Prov, Country, and either Postal Code or County.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810007,'Booking destination address missing and no destination facility supplied');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810008,'Invalid destination address on booking.  Must specify Street, City, State/Prov, Country, and either Postal Code or County.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810009,'Unknown business unit code %s on booking');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810010,'Unknown business unit value %s on booking');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810011,'Unknown protection level code %s on booking');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810012,'Unknown protection level value %s on booking');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810013,'Shipper %s not found');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810014,'If origin facility is specified, origin dock must be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810015,'It is invalid to supply an origin dock for an booking without also supplying an origin facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810016,'If destination facility is specified, destination dock must be specified');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810017,'It is invalid to supply a destination dock for an booking without also supplying a destination facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810018,'Unknown mode code %s on booking');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810019,'Unknown mode value %s on booking');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810020,'Booking %s does not exist');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810021,'Booking %s has errors');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810022,'This booking %s was modified. If you still want to perform this operation please retry.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810023,'Booking %s is not in Open Status');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810024,'Booking %s is Cancelled');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210014,'CF-C/CF-D cycle not open');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700108,'If there is a Min and/or Max value then there should be a Size UOM value.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700109,'Range for size unit of measure %s on mode %s overlaps with range for the same unit of measure on mode %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910026,'TimeFeasibility Calculation/Validation Error : Unable to find valid service level for resource');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1720010,'A reference cannot also be internal.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820024,'Null %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820025,'Could not parse %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820026,'SystemLog::%s : Data Error : %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820027,'Could not set user id :%s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1820028,'Unknown %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4810026,'Booking %s must be in Open or Requested Status to perform this operation');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 103,'Populated field(s) Shipment was imported in the Available Status.  Assigned fields %s were populated but will be ignored by Resource Selection.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 104,'Missing field(s):  %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700030,'Facility Standard Code entered is not valid for Mode selected');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4600010,'Origin facility is same as Destinaion facility');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700031,'Multiple Capacity/Commitment/UOM Definitions found for Mode %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5020001,'Enter valid E-mail id');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5020002,'Select a printer.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5020003,'Specify number of copies.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 114,'Number of rejections: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500021,'Please select any of the optimization options');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2500022,'Template Name is required field');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900021,'Draft shipment %s could not be added to the workspace successfully. This was probably because it had already been added.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900023,'Shipment %s has been canceled or had import errors.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900024,'Order %s has been assigned a delivery before a pickup.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900025,'There was an error persisting the object.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900026,'Order %s cannot be assigned to this shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900027,'Order %s cannot be reassigned to this shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1900028,'Critical Error: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910000,'TimeFeasibility Calculation/Validation Error : No Valid Resource found or given.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910001,'TimeFeasibility Calculation/Validation Error : Shipment Region Data not found for shipment %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910002,'TimeFeasibility Calculation/Validation Error : A Shipment with Fixed Time Feasibility Calculation option cannot have more than two stops. Error shipment Id %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910003,'TimeFeasibility Calculation/Validation Error : Shipment should have atleast two stops. Error Shipment Id %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910004,'TimeFeasibility Calculation/Validation Error : Stop Sequence %s, in Shipment %s does not have orders.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910005,'TimeFeasibility Calculation/Validation Error : Error while finding the distance and time between stops with sequences %s and %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910006,'TimeFeasibility Calculation/Validation Error : Location could not be retreived for stop with sequence %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910007,'TimeFeasibility Calculation/Validation Error : Fixed Transit Time data not found for mode %s, Carrier Code %s, Origin Facility %s, Destination Facility %s, Origin Postal Code %s, Destination Postal Code %s and Service Level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910008,'TimeFeasibility Calculation/Validation Error : No feasible dock hours for orders at Facility. Stop Details : Shipment ID %s, Stop Sequence %s ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910009,'TimeFeasibility Calculation/Validation Error : Invalid shipment, Fixed Transit Time infeasible with stop windows between stops with sequences %s and %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910010,'TimeFeasibility Calculation/Validation Error : Order Object is null for stop with sequence %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910011,'TimeFeasibility Calculation/Validation Error : Invalid Order windows. Pickup and the Delivery Window Start/End Dates not found or invalid for order %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910012,'TimeFeasibility Calculation/Validation Error : Possible Arrival Start/End Dates invalid for stop with sequence %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910013,'TimeFeasibility Calculation/Validation Error : Possible Departure Start/End Dates not found for stop with sequence %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910014,'TimeFeasibility Calculation/Validation Error : Address Parameter Data not found for shipper Id %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910015,'TimeFeasibility Calculation/Validation Error : Invalid Shipment, Transit Time is infeasible with stop windows between stops with sequences %s and %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3910016,'TimeFeasibility Calculation/Validation Error : Invalid Possible Arrival and Departure Windows. Possible Arrival end is greater than Possible Departure end at stop with sequences %s for shipment %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500278,'Invalid Merge Facility {0} on line {1}');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500279,'Invalid Order Fulfillment Option {0} on Line {1}');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1630256,'Invalid Destination Action {0} for DO {1}');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 6100180,'LPN {0} status is not valid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100078,'First stop cannot be a relay delivery.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100079,'Last stop cannot be a relay pickup.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100080,'First stop cannot be a relay pickup.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100081,'Last stop cannot be a relay delivery.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100082,'First stop cannot be a relay.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100083,'Last stop cannot be a relay.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100084,'Shipment already has stop with different stop action.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100085,'Relay cannot be broken.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100086,'Invalid relay stop at stop sequence %s for shipment %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1100087,'Invalid relay shipment %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100207,'The newly generated LPN already exists in the database. Please check the LPN generation rule of %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100208,'The LPN %s is not associated with the particular shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100209,'%s is out of range. Allowable values are between %s and %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100210,'The Total Selected Quantity %s is invalid.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100300,'Selected Quantity is greater than Remaining Quantity');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5100301,'The Total Quantity is 0 on the LPN %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1130011,'Update not sent for a cancelled shipment.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 133,'Shipped quantity of purchase order %s is greater than total ordered quantity by %s percent.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 134,'Shipped quantity of purchase order %s is less than total ordered quantity by %s percent.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500089,'Unknown Parent line item %s on line item %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 137,'Carrier Modified The Detention');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 138,'Shipper Modified The Detention');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 139,'Detention Status Changed');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3011000,'Invalid Effective Date Format.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3011001,'Invalid Expiration Date Format.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3011002,'Effective Date should be earlier than or equal to Expiration Date.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3011003,'Effective Date should be 1 day greater than the Expiration Date of most recent entry.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3011004,'UpperLimt Price should be greater than the LowerLimt Price.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5300001,'Save Conflict while saving Business Group');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 5300002,'Business Group with same description exists %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9610003,'%s can only be a Positive Integer values (other than 0)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4300034,'Division (%s) is Invalid for Store (%s) and Department (%s) Combination');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 201,'Detention Request Received before prenotification time');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 206,'Entered shipment (or shipment searched by given detention facility id and PO#) has Billing Method Prepaid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520160,'For Claim Type : Loss, Salvage Amount should not be entered.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520161,'For loss and Non-Receipt type of claim cancellation issue code can not be PO Transfer.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 7520162,'Store-Division-Department combination does not match Account Code Reference: %s %s %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 2820003,'%s is not allowed for %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320010,'%s Invalid Service Level %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4320011,'%s %s %s combination is invalid');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3011010,'No currency exchange rate defined from fuel price currency to surcharge currency: {%s}.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3011011,'Fuel Surcharge was not updated for a fuel accessorial with no matching fuel price range. Accessorial Code: {%s}.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3010007,'Upper Price Limit should be greater than the Lower Price Limt.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3011013,'Fuel Rate cannot exceed (%s)');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310010,'No Account Coding Engine rules fired for: %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310011,'Account Coding Engine returned an invalid Store-Department (%s) combination for: %s ');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310012,'Account Coding Engine returned an Exception for %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4310013,'Multiple Shipments %s are associated with the POs specified on the Invoice.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3210050,'Valid Business Group should be specified.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 9900013,'%s : More than one match found for purchase order %s');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500095,'Pickup start is in the past on line item %s.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 140,'PO %s created successfully');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3240006,'Shipment with a collect billing method has been assigned, but no shipment cost has been retrieved from the Rating Engine');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 3240007,'Shipment with a noncollect billing method has been assigned, but no shipment cost has been retrieved from the Rating Engine');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 8800204,'Detention Amount exceeds maximum limit and require Manager Approval');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 4410007,'The protection level of the orders on the Shipment is infeasible with the dock.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1700107,'If there is a Size UOM value then there should be a Min and/or Max value.');

INSERT INTO ERROR_MESSAGE ( ERROR_MSG_ID,DESCRIPTION) 
VALUES  ( 1500100,'Invalid Bill To Facility %s for Business Partner %s');

Commit;





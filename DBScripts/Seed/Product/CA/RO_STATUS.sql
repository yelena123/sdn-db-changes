set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RO_STATUS

INSERT INTO RO_STATUS ( RO_STATUS,DESCRIPTION) 
VALUES  ( 0,'New Request');

INSERT INTO RO_STATUS ( RO_STATUS,DESCRIPTION) 
VALUES  ( 10,'Booked/Active');

INSERT INTO RO_STATUS ( RO_STATUS,DESCRIPTION) 
VALUES  ( 20,'Delivered');

INSERT INTO RO_STATUS ( RO_STATUS,DESCRIPTION) 
VALUES  ( 30,'Recalled');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RS_REASON_CODE

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'SPF_INF','Performance Factor for the carrier exceeds threshold value or has serious violations');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'INVL_MOT','Mode does not match Designated');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'MOT_INF','Mode infeasible with multi-stop shipment');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'EQTF_INF','Equipment Carrier Facility infeasibility');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'TPF_INF','Carrier infeasible with Facility');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'TPSL_INF','Service Level infeasible with Carrier Code');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'OS_INF','Order sizes infeasible with mode');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'INVL_STP','Secondary Carrier Code does not match Designated Secondary Carrier Code');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'HZM_INF','Non-hazmat carrier for hazmat shipment');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'INVL_SER','Service Level does not match Designated');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'TM_INF','Time infeasibility');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'SLM_INF','Service Level infeasible with mode');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'DRM_INF','Delivery requirement infeasible with mode');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'PPC_INF','Bharat''s Damn Mistake');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'INVL_TP','Carrier Code does not match Designated');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'EQPF_INF','Protection Level infeasible with Equipment');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'EQPC_INF','Product Class infeasible with Equipment');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'EQ_INF','Equipment infeasible with Facility');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'TPM_INF','Mode infeasible with Carrier Code');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'NOM_INF','Number of orders infeasible with mode');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'DRTP_INF','Delivery requirement infeasible with carrier');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'TPI_INF','Carrier not sufficiently insured for this shipment');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'INVL_EQP','Equipment does not match Designated');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'EQCP_INF','Equipment Capacity Exceeded');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'MCP_INF','Shipment does not meet the upper or lower size limit by mode');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'MOA_INF','The mode is parcel, and orders have different attributes');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'TPPC_INF','Product Class infeasible with Carrier');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'TP_ROPTS','Requested Options not supported by Carrier');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'SR_INF','RG Shipping Rules Infeasible');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'TPEQ_INF','Equipment infeasible with Carrier Code');

INSERT INTO RS_REASON_CODE ( RS_REASON_CODE,DESCRIPTION) 
VALUES  ( 'CUST_INF','Customer is infeasible with the carrier');

Commit;





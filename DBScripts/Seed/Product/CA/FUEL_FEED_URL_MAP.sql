set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for FUEL_FEED_URL_MAP

INSERT INTO FUEL_FEED_URL_MAP ( COUNTRY_CODE,FEED_URL) 
VALUES  ( 'US','http://www.eia.gov/petroleum/gasdiesel/includes/gas_diesel_rss.xml');

Commit;





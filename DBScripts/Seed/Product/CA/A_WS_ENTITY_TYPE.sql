set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_WS_ENTITY_TYPE

INSERT INTO A_WS_ENTITY_TYPE ( WS_ENTITY_TYPE_ID,WS_ENTITY_TYPE_NAME) 
VALUES  ( 10,'All');

INSERT INTO A_WS_ENTITY_TYPE ( WS_ENTITY_TYPE_ID,WS_ENTITY_TYPE_NAME) 
VALUES  ( 20,'Customer Order');

INSERT INTO A_WS_ENTITY_TYPE ( WS_ENTITY_TYPE_ID,WS_ENTITY_TYPE_NAME) 
VALUES  ( 30,'POS Transaction');

Commit;





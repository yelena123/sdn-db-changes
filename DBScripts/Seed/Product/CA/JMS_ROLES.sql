set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for JMS_ROLES

INSERT INTO JMS_ROLES ( ROLEID,USERID) 
VALUES  ( 'durpublisher','dynsub');

INSERT INTO JMS_ROLES ( ROLEID,USERID) 
VALUES  ( 'publisher','dynsub');

INSERT INTO JMS_ROLES ( ROLEID,USERID) 
VALUES  ( 'guest','guest');

INSERT INTO JMS_ROLES ( ROLEID,USERID) 
VALUES  ( 'j2ee','guest');

INSERT INTO JMS_ROLES ( ROLEID,USERID) 
VALUES  ( 'john','guest');

INSERT INTO JMS_ROLES ( ROLEID,USERID) 
VALUES  ( 'durpublisher','john');

INSERT INTO JMS_ROLES ( ROLEID,USERID) 
VALUES  ( 'publisher','john');

INSERT INTO JMS_ROLES ( ROLEID,USERID) 
VALUES  ( 'subscriber','john');

INSERT INTO JMS_ROLES ( ROLEID,USERID) 
VALUES  ( 'noacc','nobody');

Commit;





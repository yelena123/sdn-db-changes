set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RS300RATING_TOKEN

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','HOME_ZIP_FEAS_BEG','D',null,'Number of hours after beginning of window will arrive at get-home zip. Equals (del_home_dist/d_speed)+del_eta-home_goal_begin_dt. Positive if arrive after window. -999 if d_home_class=0(bad home location).999 if beginning of get-home window far in past.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','HOME_ZIP_FEAS_END','D',null,'Number of hours after end of window will arrive at get-home zip. Equals (del_home_dist/d_speed)+del_eta-home_goal_end_dt. Positive if arrive after window. 999 if d_home_class=0 (bad home location). -999 if end of get-home window far in future.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_IS_PART_OF_TANDEM','B',null,'Returns true if the load is part of tandem else return false');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_EQUIP_RSTR2','E',null,'first non-blank delivery stop field equip_rstr2.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_IN_ER','B',null,'Returns "T" if the driver''s home location is in the comfort zone specified by Experience Restriction. Returns "F" if the home location is not in the zone.  Returns "OTHER" if bad data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_LATE','D',null,'If home time window is bad, D_HOME_LATE = -9999.If driver ready datetime is within home tw, D_HOME_LATE = 0.  If before beginning, D_HOME_LATE = ready - beg of tw.  If after end, D_HOME_LATE = ready - end of tw.  Result is in days');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_REG','E',null,'Returns the region of the driver''s home location,whether it is a terminal or a zipcode.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_IDLE','D',null,'current local datetime - driver''s empty_dt (in hours)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_LATENESS','D',null,'current local datetime - driver''s next_avail_dt (in hours)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_LOG_HOURS','L',null,'Driver field daily_log_hr');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_MAINT_AT_HOME','B',null,'''T'' if driver''s home location is a terminal and maintenance can be performed at that terminal;''F'', otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_MAINT_MI','L',null,'Driver field mi_since_last_maint.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_MANAGER','E',null,'Retruns the manager field of the driver record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_MODEL_FLAG','B',null,'Encoded driver field model_flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEED_CLEAN_TRLR','E',null,'Returns the encoded driver field needs_clean_trlr');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','H_MOVE_34_HOUR_RESET_SHORTAGE','D',null,'Returns (parameter rest_hrs_to_reset_wkly_log_hrs - home_duration + dest_late)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','INITIAL_ACTION','E','DEFER','Encoded assignment field initial_action.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_COUNTRY','E',null,'Returns the country of the last stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_CUST_ID','E',null,'Customer id on last delivery stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_END_DOW','L',null,'Returns the day of week from the end of the first delivery stop''s window.  0(Mon) - 6(Sun) or -1 for an invalid date.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_END_TIME','L',null,'The ending time of the 1st delivery stop''s window as an integer. 3:05PM would be 1505.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_EQUIP_PRF1','E',null,'first non-blank delivery stop field equip_pref1.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','END_REG_IN_DIR','B',null,'T, if driver''s end location is in comfort zone specified by drvr_id/DIR."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','HAWK_PRIMARY_MATCH','B','DHRUV','Returns TRUE if the drvr_id field of the DRIVER table matches the preplan_drvr_id field of the LOAD table.  Otherwise returns FALSE.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','HAWK_RETURN_TRIP_MATCH','B','DHRUV','Returns TRUE when the assigned_load_id field of the DL610DRIVER table matches the load_id field of the DL610LOAD table with an''RT'' at the end of it.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','LENGTH_OF_HAUL','L',null,'Driver perceived length of haul for the load.  For loads which are split, only the miles up to the split location are considered.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','LENGTH_OF_MOVE','L',null,'Returns the summation of Deadhead miles and Length of haul.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','LOAD_BUFFER_TIME','D',null,'Beginning of the first delivery stop''s time window - the arrival datetime at the first delivery.  Bounded below by zero.  Zero if first delivery stop''s time window is open.  Replaced SPLIT_BUFFER.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','LOST_UTIL','L',null,'Number of hours of lost utilization (adjusted L move completion date - earliest L move completion date).  The adjusted L move completion date includes time required to travel circuitous miles.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','LOST_UTIL_OFF','D',null,'LOST_UTIL - LOST_UTIL_PRIME.Currently, there may be some rounding errors because LOST_UTIL is stored as an integer.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','LOST_UTIL_PRIME','D',null,'Number of hours of lost utilization during PRIME BUSINESS HOURS (adjusted L move completion date - earliest L move completion date).  The adjusted L move completion date includes time required to travel circuitous miles.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_ALREADY_SPLIT_FLAG','B',null,'Encoded load field already_split_flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_BSEG','E',null,'Encoded load field bseg.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_PASSED','D',null,'Current local datetime - end of time window for first delivery stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_RANK','E',null,'Encoded first delivery stop field priority.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_REG','E',null,'Encoded final delivery stop region for load (based on zip/terminal).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_STATE','E',null,'Load''s last delivery state.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DIVISION','E',null,'Returns the division field of the load record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DROP_AND_HOOK_FLAG','B',null,'Encoded load field drop_hook_flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DROP_DEAD_DIFF','D',null,'Maximum of l_pkup_end_diff  and (current_time minus l_drop_dead_datetime).  Both values are negative if l_drop_dead_datetime and the end of the pickup datetime window are in the future.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DTP_ZONE','E',null,'This value is the preferred comfort zone/dedicated type of the first region that includes both the load''s first pickup location and last delivery location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_EXCESS_BUFFER','D',null,'if beg of delivery tw is before end of pickup tw, L_EXCESS_BUFFER = 0.  Else, L_EXCESS_BUFFER = beg of delivery tw - end of pickup tw -approx. time required to travel from first pickup through first delivery.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_BSEG','E',null,'Encoded driver field bseg.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_CAPACITY','L',null,'Returns the driver field capacity');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_PREPLAN_FLAG','B',null,'Returns T if the driver has one or more preplans. Returns F otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_PREV_DRIVE_HR','L',null,'Returns the prev_drive_hr field of the driver record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_PREV_WORK_HR','L',null,'Returns the prev_work_hr field of the driver record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_PRODUCT_TYPE','E',null,'Encoded driver field product_type.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_PTA_DIST_BASE1','L',null,'Distance between driver''s next available locationand the base 1 location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_PTA_DIST_BASE2','L',null,'Distance between the driver''s next available location and base 2 location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_PTA_DIST_HOME','L',null,'Distance between the driver''s next available location and home location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_REPORTING_STATUS','E',null,'Encoded driver field reporting_status.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_REST_HR_ACCUM','L',null,'Returns the rest_hr_accum field of the driver record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_RQST_FLAG','B',null,'Encoded driver token:''T'' if driver requests exist;''F'', otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_STAGED_FLAG','B',null,'Description placeholder.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_COUNTRY','E',null,'Returns the country of the driver''s next available location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_LOAD_REQ','L',null,'Load field drvr_load_rqmt.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_LOAD_TYPE','E',null,'Returns load_type field value.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PRESET_DEL_FLAG','B',null,'Encoded load field preset_del_flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_MANUAL_LABOR_NUM','L',null,'Sum of Load fields drvr_load_rqmt and drvr_unload_rqmt.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','ALL_STOPS_REG_IN_DIP','B',null,'''T'', if all driver''s stop locations are in comfort zone specified by drvr_id/DIP.''F'' otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','END_REG_IN_DTP','B',null,'T, if driver''s end location is in comfort zone specified by driver''s ded_type/DTP."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_REG_IN_ER','B',null,'T, if load''s last delivery location is in comfort zone specified by driver''s experience/ER."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_REG_IN_ZONE','B',null,'T, if load''s last delivery location is in comfort zone specified by driver''s ded_type/DTP."F", otherwise."OTHER", if bad/unspecified data.Logically equivalent to DEL_REG_IN_DTP.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_RQST_DIST','L',null,'Distance from driver''s destination location to request location (closest request location for multiple location requests).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STATIC_ROUTE_ID','E',null,'Returns Static route id of the shipment');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_IS_TANDEM_CAPABLE','B',null,'If driver is tandem capable return true else false.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','M_MOVE_34_HOUR_RESET_SHORTAGE','D',null,'Returns (parameter rest_hrs_to_reset_wkly_log_hrs - maintenance_duration + dest_late).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','NON_REVENUE_MILES','L',null,'Returns deadhead_mi + circuity_mi.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','CURR_DAY_OF_WEEK','E',null,'Returns the current day of week (e.g. "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN") for the current datetime.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_DIST_BASE1','L',null,'Distance between first pickup stop and base 1.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_DIST_BASE2','L',null,'Distance between first pickup stop and base 2.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_END_DRIVER_DIFF','D',null,'End of first pickup stop time window - driver''s ready datetime.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_END_STATE_MATCH','B',null,'T, if load''s pickup location is in the same state as the driver''s destination location."F", otherwise."OTHER", if bad/insufficient data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_LATE','D',null,'If arrival at first pickup is before than end of first pickup time window, PKUP_LATE = arrival - end of first pickup time window.  Otherwise, PKUP_LATE = start of handling - end of first pickup time window.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_LATE_ACCESS_HOURS','D',null,'Returns the difference between PKUP_LATE and arrival at the pickup stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_REG_IN_DIP','B',null,'T, if load''s first pickup location is in comfort zone specified by driver''s drvr_id/DIP."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_REG_IN_DIR','B',null,'T, if load''s first pickup location is in comfort zone specified by driver''s drvr_id/DIR."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_REG_IN_DTP','B',null,'T, if load''s first pickup location is in comfort zone specified by driver''s ded_type/DTP."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_BID_ORIGIN','E',null,'Location where open bid is taking place in which load is participating');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEADHEAD_MI','L',null,'Deadhead miles incurred to travel from drivre''s next available location to the load''s first pickup stop location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_BEG_DIFF','L','DEFER','Beginning of driver''s request time window minus driver timeWhat about for multiple requests...');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_CLASS','E','DEFER','Hard coded value ( 0, 1, or 2 ). 0 if there is nodriver request or the time window in far in the past or future. 1 if the request is a zip_code.2 if the request is a terminal.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DOMICILE_FACILITY_ALIAS_ID','E',null,'Returns the domicile facility alias Id of the driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_DNLT_DIFF','D',null,'Unloaded L_PKUP_DNLT_DIFF = 0Loaded equals current time minus end of load''s first pickup tw ie. STOP.DNLT (Note that dispatch time is already included in this unlike L_PKUP_END_DIFF) ');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_DNLT_PASSED','D',null,'SYSTEMTIME- STOP.DNLT at origin, Unloaded equals 0.Loaded equals current time minus earliest end of first pickup. ');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_FACILITY_ALIAS_ID','E',null,'facility alias id of the pickup stop');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PRODUCT_CLASS','E',null,'Product class value and not id');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PROTECTION_LEVEL','E',null,'Action type which could be DL, DA, DP, RD (Deliver, Deliver All, Drop, Relay Drop)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STOP2_ACTION','E',null,'Action type which could be PU, PA, RP, HK, DL, DA, DP, RD (Pickup, PickupAll, Relay pickup, Hook, Deliver, Deliver All, Drop, Relay Drop)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STOP2_FACILITY_ALIAS_ID','E',null,'facility alias id of stop2');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STOP3_ACTION','E',null,'Action type which could be PU, PA, RP, HK, DL, DA, DP, RD (Pickup, PickupAll, Relay pickup, Hook, Deliver, Deliver All, Drop, Relay Drop)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STOP3_FACILITY_ALIAS_ID','E',null,'facility alias id of stop3');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STOP4_ACTION','E',null,'Action type which could be PU, PA, RP, HK, DL, DA, DP, RD (Pickup, PickupAll, Relay pickup, Hook, Deliver, Deliver All, Drop, Relay Drop)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STOP4_FACILITY_ALIAS_ID','E',null,'facility alias id of stop4');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STOP5_ACTION','E',null,'Action type which could be PU, PA, RP, HK, DL, DA, DP, RD (Pickup, PickupAll, Relay pickup, Hook, Deliver, Deliver All, Drop, Relay Drop)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','ALL_STOPS_REG_IN_DIR','B',null,'''T'', if all driver''s stop locations are in comfort zone specified by drvr_id/DIR.''F'' otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','ALL_STOPS_REG_IN_DTP','B',null,'''T'', if all driver''s stop locations are in comfort zone specified by driver''s ded_type/DTP.''F'' otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','ALL_STOPS_REG_IN_ER','B',null,'''T'', if all driver''s stop locations are in comfort zone specified by experience/ER.''F'' otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','AT_SAME_TERMINAL','B',null,'T, if driver''s next avail location is a terminal and load''s first pickup location is a terminal and they are the same terminal."F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','AT_SAME_ZIP','B',null,'Returns TRUE if zip code from next_avail_zip field from DRIVER table equals zip code field from STOP table for the first stop only. Otherwise returns FALSE.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','AVE_DAILY_LOAD_UTIL','L',null,'Miles per day as perceived by the driver.(LENGTH_OF_HAUL * 24.0) /      (datetime at which driver will complete load minus driver''s ready date time).Bounded above by 1000.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BASE_CIRC_RATIO','L',null,'If driver''s bseg !=''B'' or''C'', 0.If D_BASE1_DIST = 0, 9999.Else BASE_CIRC_RATIO =(DEADHEAD_MI + LENGTH_OF_HAUL + DEL_BASE1_DIST) /       D_BASE1_DIST) - 1.0) * 100.0');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BEG_DEST_LATE','D',null,'Unloaded: 0.0;Loaded: If first destination stop does not exist, 0.0; else, BEG_DEST_LATE = arrival time at first destination minus beginning of time window at first destination.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BEG_PKUP_LATE','D',null,'Unloaded: 0.0;Loaded: If first pickup does not exist, 0.0;else, BEG_DEL_LATE = arrival time at first pickup minus beginning of time window at first pickup.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','CURR_HOUR_OF_DAY','L',null,'Returns the current hour of the day ( 0 - 23 ).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','CUSTOMER_RELOAD','B',null,'T, if driver''s next avail location is not a terminal AND load''s first pickup location is not a terminal AND driver''s customer id is specified AND load and driver customer id are the same."F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','CIRCUITY_MI','L',null,'Circuitous miles incurred to travel to an intermediate stop (HRMS stop).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','CIRCUITY_MILES_PERCENT','D',null,'Returns (circuity_mi / (circuity_mi + l_mi)) x 100');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'OB','OPER_FACTOR_DOUBLE3','D',null,'operation factor double3');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','TOTAL_WAIT_TIME','D',null,'Total amount of time spent waiting by driver on load.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','END_REG_IN_ER','B',null,'T, if driver''s end location is in comfort zone specified by experience/ER."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','TERM_TO_DEL_MI','L',null,'The number of miles from the recommended enroute terminal, to the first delivery stop, through to the last delivery stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_IN_DIP','B',null,'T, if driver''s next available location is in comfort zone specified by driver''s drvr_id/DIP."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_IN_DIR','B',null,'T, if driver''s next available location is in comfort zone specified by driver''s drvr_id/DIR."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_IN_DTP','B',null,'T, if driver''s next available location is in comfort zone specified by driver''s ded_type/DTP."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_IN_ER','B',null,'T, if driver''s next available location is in comfort zone specified by driver''s experience/ER."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_IN_ZONE','B',null,'T, if driver''s next available location is in comfort zone specified by driver''s ded_type/DTP."F", otherwise."OTHER", if bad/unspecified data.Logically equivalent to D_NEXT_AVAIL_IN_DTP.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_REG','E',null,'Encoded driver''s next available region (uses next available location, zip or term).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_STATE','E',null,'Driver''s next available state.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_TIME','L',null,'The driver''s next available time as an integer. 3:05PM would be 1505.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NEXT_AVAIL_TIME_DOW','L','DHRUV','Returns the day of the week of the driver''s next available time.  0(Mon) - 6(Sun) or -1 for an invalid date.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NUM_COMPLAINTS','L',null,'Returns the number of complaints from the driver field num_complaints.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NUM_UNLOADS','L',null,'Driver field num_unloads.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_ON_LONG_ISLAND','B',null,'T if driver''s next available location is on Long Island (defined be zips 100 - 104 and 110 - 119);"F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_OO_FLAG','B',null,'Encoded driver field oo_flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_OUT_OF_SRV_FLAG','B',null,'Encoded driver field out_of_srv_flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','END_COUNTRY','E',null,'Returns the country of the driver''s end location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PTA_PKUP_STATE_MATCH','B',null,'T, if driver''s next available state is the same as the load''s pickup state."F", otherwise."OTHER", if bad/insufficient data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','END_DOW','E',null,'Returns the day of week (''MON'',''TUE'',''WED'',''THU'',''FRI'',''SAT'',''SUN'',''OTHER'') based on the driver''s arrival time at the end location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','END_REG','E',null,'Region associated with driver''s destination location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','END_REG_IN_DIP','B',null,'T, if driver''s end location is in comfort zone specified by drvr_id/DIP."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_TEAM_STATUS','E',null,'Encoded driver field team_status.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_CLASS','E',null,'Encoded driver field class.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_COMPANY','E',null,'Returns the company field of the driver record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEST_LATE','D',null,'Unloaded: 0.0.Loaded: If arrival time at first dest is later than the end of the time window at first dest, DEL_LATE = arrival time at first dest - end of tw.  Otherwise, DEST_LATE = start of handling at first dest - end of tw.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEST_LATE_ACCESS_HOURS','D',null,'Returns the difference between DEST_LATE and arrival at the delivery stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DIST_SAVED','L',null,'Percentage of load completed up to intermediate stop on load.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_BASE1_RADIUS','L',null,'Driver field max_dist_loc1.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_BASE2_RADIUS','L',null,'Driver field max_dist_loc2.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_CURR_STATUS','E',null,'Encoded driver field curr_status.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_CUR_ETA_LATE','D',null,'current time minus driver''s arrival datetime field  (arrival_dt).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DAILY_WORK_HR','L',null,'Returns the daily_work_hr field of the driver record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DED_REQUIRE_TYPE','E',null,'Encoded driver field ded_rqmt_type.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DED_TYPE','E',null,'Encoded driver field ded_type.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DEPENDABILITY','L',null,'Driver field dependability.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DIP_FLAG','B',null,'Returns TRUE if the driver has any driverIdPreference zones defined. Returns FALSE otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DIR_FLAG','B',null,'Returns TRUE if the driver has any driverIdRestriction zones defined. Returns FALSE otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_D_MOVE_END_LATE','D',null,'Returns the number of hours after the end of the home window that the driver will arrive at home, assuming a start at the ready datetime.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_ER_FLAG','B',null,'Returns TRUE if the driver has any experience restriction zones defined. Returns FALSE otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_EXPERIENCE','L',null,'Encoded driver field experience.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_FORECAST_TYPE','E',null,'Encoded driver field forecast_type.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_GOOD_NUMBER_FLAG','B',null,'Encoded driver field good_num_flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HAULER_ID','E',null,'Returns the first character of the load_id.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HAZMAT','E',null,'Encoded driver field hazmat_capable.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_CLASS','E',null,'Hard coded value (0, 1, or 2)0 if there is an error in the driver''s home location, or the driver''s home timewindow is bad, or the end of the home time window is open.2, if the driver''s home is a terminal.1, if driver''s home is a zip.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_COUNTRY','E',null,'Returns the country for the driver''s home location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_DAYS_OUT','L',null,'Difference in days between driver''s next available date time and driver''s home last date time.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_DURATION','L',null,'Driver''s home_duration');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_IN_DIP','B',null,'Returns "T" if the driver''s home location is in the comfort zone specified by Driver Id Preference.  Returns "F" if the home location is not in the zone.  Returns "OTHER" if bad data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_IN_DIR','B',null,'Returns "T" if the driver''s home location is in the comfort zone specified by Driver Id Restriction.  Returns "F" if the home location is not in the zone.  Returns "OTHER" if bad data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_HOME_IN_DTP','B',null,'Returns "T" if the driver''s home location is in the comfort zone specified by Dedicated Type Preference.  Returns "F" if the home location is not in the zone.  Returns "OTHER" if bad data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_UTIL_DIFF','L',null,'Calculated from driver fields util_mi, util_days, util_goal.  If any of the above are 0, the token equals zero.  Otherwise it equals     util_goal - 7 x ( util_mi / util_days)Did not exist in MM41');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_UTIL_MI','L',null,'Driver''s util_mi');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_UTIL_MI_PER_DAY','L',null,'Equals D_UTIL_MI / util_days.  Equals zero if util_days = zero.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','HOME_BEFORE_NOON','B',null,'T if arrival time at home location is before noon; "F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','HOME_DIST_CHANGE','L',null,'Change in distance to home from driver''s next available location to the driver''s destination location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','HOME_FEAS_BEG','D',null,'Number of hours after beginning of window will arrive at get-home loc. Equals (del_home_dist/d_speed)+del_eta-home_goal_begin_dt. Positive if arrive after window. -999 if d_home_class=0(bad home location).999 if beginning of get-home window far in past.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','HOME_FEAS_END','D',null,'Number of hours after end of window will arrive at get-home location. Equals (del_home_dist/d_speed)+del_eta-home_goal_end_dt. Positive if arrive after window. 999 if d_home_class=0 (bad home location). -999 if end of get-home window far in future.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','HOME_LATE','D',null,'Number of hours late will arrive to home for HD moves and to request(if home=request) for RQ moves. Is 0 if arrives inside window. Is negative if arrives before beginning window.  Is positive if arrives after ending window. Is -99999 otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','END_REG_IN_ZONE','B',null,'T, if driver''s end location is in comfort zone specified by driver''s ded_type/DTP."F", otherwise."OTHER", if bad/unspecified data.Logically equivalent to END_REG_IN_DTP.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','END_STATE','E',null,'State in which driver will complete load.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','ENROUTE_ETA_DIFF','D',null,'Estimated arrival time at the enroute terminal -the current time.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_END_DIFF','L','DEFER','Equals difference in days between end of request window and d_ready_datetime. Positive value means end of request in future. Negative value means end of request in past. When the driver has no pending requests, the value equals 99.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_FEAS_BEG','D','DEFER','Number of hours after beginning of request window driver will arrive at request location, starting from recommended end point. Positive value means will arrive late. If rqst_flag is''F'', = -999. If the end of the request window open-ended, = 999.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_FEAS_END','D','DEFER','Number of hours after end of request window driver will arrive at request location, starting from recommended end point. Positive value means will arrive late. If rqst_flag is''F'', = 999. If the end of the request window open-ended, = -999.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_FULFILLS_HOME','B','DEFER','T, if the driver''s home location is the same as the driver''s request location AND the additional time required to stop at home would not make the load late.  "F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_IN_DIP','B','DEFER','Returns "T" if the driver''s rqst location is in the comfort zone specified by Driver Id Preference.  Returns "F" if the rsqt location is not in the zone.  Returns "OTHER" if bad data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_IN_DIR','B','DEFER','Returns "T" if the driver''s rsqt location is in the comfort zone specified by Driver Id Restriction.  Returns "F" if the rsqt location is not in the zone.  Returns "OTHER" if bad data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_IN_DTP','B','DEFER','Returns "T" if the driver''s rsqt location is in the comfort zone specified by Dedicated Type Preference.  Returns "F" if the rsqt location is not in the zone.  Returns "OTHER" if bad data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_IN_ER','B','DEFER','Returns "T" if the driver''s rsqt location is in the comfort zone specified by Experience Restriction. Returns "F" if the rsqt location is not in the zone.  Returns "OTHER" if bad data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_IS_BEST','B','DEFER','Returns TRUE if the recommended request is the driver''s best request.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_EQUIP_PRF2','E',null,'first non-blank delivery stop field equip_pref2.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_EQUIP_REQUIRE','E',null,'first non-blank delivery stop field equip_rqmt.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_EQUIP_RSTR1','E',null,'first non-blank delivery stop field equip_rstr1.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_FINAL_DEL_COUNTRY','E',null,'Returns the country of the load field final_dest_zip.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_FINAL_DEL_REG','E','DHRUV','Encoded Load field final_dest_zip (uses region associated with zip).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_FINAL_DEL_STATE','E',null,'Encoded Load field final_dest_zip state (uses state associated with zip).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_FORECAST_TYPE','E',null,'Encoded load field forecast_type.  Not implemented yet.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_HAZMAT_FLAG','E',null,'Encoded load field hazmat.  Note this is NOT a flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_LANE_EQUIP_RSTR1','E',null,'Encoded load field lane_equip_rstr1.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_LANE_EQUIP_RSTR2','E',null,'Encoded load field lane_equip_rstr2.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_LOAD_ID1','E',null,'Returns the first character of the load_id.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BIMODAL_BONUS','D',null,'Net bonus for taking a bimodal move.  Equal to the direct cost to run via truck minus the cost to run via rail.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BIMODAL_FLAG','B',null,'True/false flag indicating if the load has a rail segment.  May be true for B, H, R, M moves');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','CAPACITY_OVER','L',null,'Returns d_capacity - l_weight.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEADHEAD_MILES_PERCENT','D',null,'Returns (deadhead_mi / (deadhead_mi + l_mi)) x 100');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEADHEAD_RV_VAR_DIFF','L','DHRUV','D_NEXT_AVAIL_RV_VAR minus L_PKUP_RV_VAR.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DED_MATCH','B',null,'T, if driver''s ded_type is specified AND ded_type equals load''s ded_type."F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_DIST_BASE1','L',null,'If driver''s base 1 location is not valid, 0.Otherwise, DEL_DIST_BASE1 is the distance from the driver''s destination location to the base 1 location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_DIST_BASE1_PTA_RATIO','L',null,'Returns (DEL_DIST_BASE1 / D_PTA_DIST_BASE1).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','SPLIT_FLAG','B',null,'T, if load will be split."F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','SPLIT_SERVICE_CAP','L',null,'Get the capacity of the split service at the enroute terminal.  -999 if no enrouteTerminal.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','SPLIT_SERVICE_COST','L',null,'The cost of the split service at the terminal.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','STAGE_CUST_ID_MATCH','B',null,'F, if driver''s stage_cust_id is specified AND does NOT match the first pickup stop''s customer id; "T", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DIVISION','E',null,'Returns the divsion field of the driver record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DTP_FLAG','B',null,'Returns TRUE if the driver has any Dedicated Type Preference zones defined. Returns FALSE otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_D_MOVE_BEG_LATE','D',null,'Returns the number of hours after the beginning of the home window that the driver will arrive at home, assuming a start at the ready datetime.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_CLASS_REQ','E',null,'Encoded Load field class_rqmt.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_COMMITTED','E',null,'Encoded value of Load field: committed.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_COMPANY','E',null,'Returns the company field of the load record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_CUSTOMER_ID','E',null,'Returns the encoded load field customer_id.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DED_REQUIRE_TYPE','E',null,'Encoded load field ded_rqmt_type.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DED_TYPE','E',null,'Encoded load field ded_type.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_BEG_TIME','L',null,'The beginning time of the 1st pickup stop''s window as an integer. 3:05PM would be 1505.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DRIVER_ID','E',null,'Returns the driver code setup in base data');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_END_REG_MATCH','B',null,'T, if the region associated with driver load''s last delivery stop location is the same as the region associated with the driver''s destination location; "F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_HOME_DIST','L',null,'If driver''s home location does not exist, DEL_HOME_DIST = 9999.  Otherwise, DEL_HOME_DIST equals the distance from the driver''s destination location to the driver''s home location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_HOME_ZIP_DIST','L',null,'Determine distance from driver''s destination location to the driver''s actual home location (home_zip overrides home_term).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_MAINT_DIST','L',null,'If closest maintenance terminal does not exist, DEL_MAINT_DIST = 9999.  Otherwise, DEL_MAINT_DIST = distance from driver''s destination location to the closest maintenance terminal.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_REG_IN_DIP','B',null,'T, if load''s last delivery location is in comfort zone specified by driver''s drvr_id/DIP."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_REG_IN_DIR','B',null,'T, if load''s last delivery location is in comfort zone specified by driver''s drvr_id/DIR."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','NEXT_MOVE_ETA','D',null,'current local datetime - NAT of driver at shipment pickup location');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_REG_IN_DTP','B',null,'T, if load''s last delivery location is in comfort zone specified by driver''s ded_type/DTP."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_LATE','D','DEFER','Number of hours late will arrive to request for QR moves and to home(if home=request) for HD moves. Is 0 if arrives inside window. Is negative if arrives before beginning window.  Is positive if arrives after ending window. Is -9999 otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_Q_MOVE_BEG_LATE','L','DEFER','Returns the number of hours after the beginning of the request window that the driver will arrive at the request,  starting at the next avail.  Uses each request for Q and R moves,  otherwise uses best request.  Undefined = -9999');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_Q_MOVE_END_LATE','L','DEFER','Returns the number of hours after the end of the request window that the driver will arrive at the request, starting at the next avail.  Uses each request for Q and R moves, otherwise uses best request. Undefined = -9999.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','RQST_TYPE','E','DEFER','Encoded driver request field rqst_type.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','R_MOVE_34_HOUR_RESET_SHORTAGE','D',null,'Returns (parameter rest_hrs_to_reset_wkly_log_hrs - request_duration + dest_late)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_DRIVER_TYPE','E','xxx','Driver type with experience');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_DIST_BASE2','L',null,'If driver''s base 2 location is not valid, 0.Otherwise, DEL_DIST_BASE2 is the distance from the driver''s destination location to the base 2 location.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_SHIFT_START','D',null,'Returns the difference between the current date time and the shift start time of the driver.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_NUMBEROFSTOPS','L',null,'SHIPMENT.NUM_STOPS or [ For empty segments,DEADHEAD_STOP.maxSTOP_SEQ - 1 ]');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_ORIGIN_ACTION','E',null,'Action type which could be PU, PA, RP, HK (Pickup, PickupAll, Relay pickup, Hook)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PART_OF_BID','B',null,'IF segment is part of an OPEN Bid return TRUE otherwise FALSE');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_ANET_DIFF','D',null,'Unloaded L_PKUP_ANET_DIFF = 0Loaded equals current time minus end of load''s first pickup tw ie. STOP.ANET');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_ANET_TIME','L',null,'The beginning time of the 1st pickup stop''s ANET as an integer. 3:05PM would be 1505.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_ANLT_DIFF','D',null,'Unloaded L_PKUP_ANLT_DIFF = 0Loaded equals current time minus end of load''s first pickup tw ie. STOP.ANLT');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_BID_ORIGIN_MATCH_PKUP','B',null,'Does Load origin match Bid Location (Load should be participating in an open Bid)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_ANLT_TIME','L',null,'The beginning time of the 1st pickup stop''s ANLT as an integer. 3:05PM would be 1505.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DESIG_DRIVER_TYPE','E',null,'Driver type as determined by Consolidation engine');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DESTINATION_ACTION','E',null,'Action type which could be DL, DA, DP, RD (Deliver, Deliver All, Drop, Relay Drop)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BEG_DEST_LATE_ANET','D',null,'Unloaded: 0.0;Loaded: If first destination stop does not exist 0.0 else arrival time at first destination minus beginning of latest time window ie ArriveNoLaterThan (STOP.ANET) at first destination.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','LP_DELIVERY_PICKUP_TIME_DIFF','D',null,'Returns the waiting time at the second load after delivering the first load.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','SHIFT_END_DIFF','L',null,'Returns the difference in minutes between the shift end time and the delivery done time of the assignment.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_MAX_DRVR_COMPLAINT','L',null,'Returns the max_drvr_complaint field of the load record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_MI','L',null,'Sum of inter stop mileages for load.  Does not consider length_of_haul.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_MODEL_FLAG','B',null,'Encoded load field model_flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_NEED_CLEAN_TRLR','E',null,'Returns the encoded load field needs_clean_trlr.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_NUM_STOPS','L',null,'Number of stop in addition to first pickup and first delivery (number of pickup stops + number of delivery stops - 2).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_ON_LONG_ISLAND','B',null,'T if load''s first pickup location is on Long Island (defined be zips 100 - 104 and 110 - 119);"F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_ORDER_MESSAGE','E',null,'Returns the order_message field of the load record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_BEG_DIFF','D',null,'Unloaded L_PKUP_BEG_DIFF = 0.Loaded equals current time minus beginning of load''s first pickup tw.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_PASSED','D',null,'Unloaded equals 0.Loaded equals     current time minus end of first pickup tw.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_RANK','E',null,'Encoded first pickup stop field priority.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_REG','E',null,'Encoded first pickup stop region (uses term/zip).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_STATE','E',null,'Load''s pickup state.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PREPLAN_STATUS','E',null,'Encoded Load field preplan_status');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PRIORITY','E',null,'Encoded Load priority.It is the maximum of all stop priorities for the load.  An external parameter is required to determine the ordering of priorities (not all carriers use JHNL as stop priorities)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PRODUCT_TYPE','E',null,'Encoded load field product_type.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_RELOAD_REQ','B',null,'Encoded load field reload_rqmt.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_SEG_MI','L',null,'For bimodal, the origin dray; otherwise L_MI.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_SPLIT_TYPE','E',null,'Encoded load field split_type.Exists in MM41 but NOT in later versions.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_SPOT_FLAG','B',null,'Encoded load field spot_flag.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_TIME_TO_HOME_BEG','D',null,'Returns the difference between the current date time and the shift end time of the driver.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','MAX_CIRC_EXCEEDED','B',null,'T if circuity exceeds system parameter maximum circuity value; "F", otherwise.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','MIN_HOME_CIRCUITY','L',null,'Minimum circuitous miles required to travel home through the load.  May occur after intermediate location is passed if load is not split.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','MIN_HOME_DIST','L',null,'Minimum of DEL_HOME_DIST and MIN_HOME_CIRCUITY.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','NON_REVENUE_MILES_PERCENT','D',null,'Returns ((deadhead_mi + circuity_mi) / (deadhead_mi + circuity_mi + l_mi)) x 100');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_BEG_DRIVER_DIFF','D',null,'Beginning of first pickup stop time window - driver''s ready datetime.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_DEAD_LATE','D',null,'If the driver arrives before the load''s drop dead date time, PKUP_DEAD_LATE = arrival at first pickup - drop dead datetime.  Otherwise, PKUP_DEAD_LATE = start of handling at first pickup - drop dead datetime.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PTA_DEL_REG_MATCH','B',null,'T, if driver''s next available region is the same as the load''s last delivery region."F", otherwise."OTHER", if bad/insufficient data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PTA_DEL_STATE_MATCH','B',null,'T, if driver''s next available state is the same as the load''s last delivery state."F", otherwise."OTHER", if bad/insufficient data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PTA_END_REG_MATCH','B',null,'T, if driver''s next available region is the same as the region the driver will finish the load in."F", otherwise."OTHER", if bad/insufficient data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PTA_END_STATE_MATCH','B',null,'T, if driver''s next available state is the same as the state the driver will finish the load in."F", otherwise."OTHER", if bad/insufficient data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STOP5_FACILITY_ALIAS_ID','E',null,'facility alias id of stop5');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_ANET_DRIVER_DIFF','D',null,'Earliest first pickup stop time window (ANET@ origin) - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_ANLT_DRIVER_DIFF','D',null,'Latest first pickup stop time window (ANLT@ origin) - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_FACILITY_TYPE_VENDOR','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_MATCH_HOME','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','NAL_MATCH_HOME','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_MATCH_HOME','B',null,'Returns TRUE if Driver''s Home location matches the Origin of shipment being evaluated');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_IS_USED','B',null,'Is the driver used now');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_ITERATION_NUMBER','L',null,'subcycle number within iterative single dispatch');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_BEG_DRIVER_DIFF','D',null,'Beginning of first delivery stop time window (AS@ first delivery) - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_DNET_DRIVER_DIFF','D',null,'Earliest departure of first delivery stop time window STOP.DNET - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_DNLT_DRIVER_DIFF','D',null,'Latest departure of first delivery stop time window STOP.DNLT - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_END_DRIVER_DIFF','D',null,'End of first delivery stop time window STOP.DE - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEST_LATE_DNET','D',null,'Unloaded: 0.0;Loaded: If first destination does not exist 0.0 else (ETA of DRIVER - First destination STOP.DNET).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEST_LATE_DNLT','D',null,'Unloaded: 0.0;Loaded: If first destination does not exist 0.0 else (ETA of DRIVER - First destination STOP.DNLT).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DL_PREFERENCE_MATCH','L',null,'Returns the number of preferences matched for a driver-load pair');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_ASSG_CARRIER_ID','E',null,'Assigned carrrier code and not id  (Accepted carrier on shipment)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DL_BID_MATCH','B',null,' if bid ID for load and driver matches return TRUE else FALSE.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DL_ASSG_CARRIER_MATCH','B',null,'if driver carrier ID matches with carrier ID assigned to load return TRUE else FALSE.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DL_DESIG_CARRIER_MATCH','B',null,' if driver carrier ID matches with load designated carrier ID return TRUE else FALSE.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_ACCEPTED_LOAD_COUNT','L',null,'No of loads accepted for driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_ACCEPTED_DRIVER','B',null,'Is driver accepted for this load?');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_ACCEPTED_DRIVER_ID','E',null,'Driver ID of the accepted driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_WAVE_ID','L',null,'L_WAVE_ID');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_LATENESS_NA','D',null,'D_LATENESS_NA');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_FACILITY','E',null,'L_PKUP_FACILITY');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_FACILITY_ID','L',null,'L_PKUP_FACILITY_ID');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_FACILITY','E',null,'L_DEL_FACILITY');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_FACILITY_ID','L',null,'L_DEL_FACILITY_ID');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_IS_PDPD','B',null,'L_IS_PDPD');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','IS_TEAM','B',null,'If assignment has a team driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','TRANSIT_TIME','L',null,'transit time for the driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','UTILIZATION_SCORE','D',null,'utilization of the driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','TOUR_UTILIZATION_SCORE','D',null,'utilization of the driver across several subcycles');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','TERM_SERVICE_CAP','L',null,'The capacity of the service being used at the terminal for the current move type. -999 if no enroute terminal service.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','TERM_SERVICE_COST','L',null,'The cost of the terminal service used for the recommended move type.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_BID_END_DIFF','D',null,'Open Bid in which Load is participating BID.END_TIME - SHIPMENT.DepartureEnd@origin (Estimated DE otherwise Planned)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_STATUS','E',null,'Encoded load field status.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_TEAM_REQ','E',null,'Encoded Load field team_rqmt.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_TRLR_POOL_SERVICE_COST','L',null,'Returns the trailer pool''s satisfied commitment as a percent.  The trailer pool in question has a terminal id that matches the customer id on the 1st pickup stop record.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_UNLOAD_REQ','L',null,'Load field drvr_unload_rqmt.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_WEIGHT','L',null,'Returns the load field weight.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_LOAD_CLOSED','B',null,'Flag to indicate that shipment has been loaded onto truck');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BEG_DEST_LATE_ANLT','D',null,'Unloaded: 0.0;Loaded: If first destination stop does not exist 0.0 else arrival time at first destination minus beginning of latest time window ie ArriveNoLaterThan (STOP.ANLT) at first destination.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BEG_DISPATCH_LATE','D',null,'Unloaded: 0.0;Loaded: If first pickup does not exist 0.0 else arrival time at first pickup minus estimated dispatch at first pickup.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BEG_DISPATCH_LATE_DNET','D',null,'Unloaded: 0.0;Loaded: If first pickup does not exist 0.0 else arrival time at first pickup minus DNET at first pickup. Note dispatch time is already included.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BEG_DISPATCH_LATE_DNLT','D',null,'Unloaded: 0.0;Loaded: If first pickup does not exist 0.0 else arrival time at first pickup minus DNLT at first pickup. Note dispatch time is already included.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BEG_PKUP_LATE_ANET','D',null,'Unloaded: 0.0;Loaded: If first pickup does not exist 0.0 else arrival time at first pickup minus beginning of earliest time window ie ANET at first pickup.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','BEG_PKUP_LATE_ANLT','D',null,'Unloaded: 0.0;Loaded: If first pickup does not exist 0.0 else arrival time at first pickup minus beginning of earliest time window ie ANLT at first pickup.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_BID_END_DIFF','D',null,'Open Bid in which driver is participating BID.END_TIME - DRIVER''S ready datetime');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_BID_ORIGIN','E',null,'Location where open bid is taking place in which driver is participating.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_BID_ORIGIN_MATCH_HOME','B',null,'Does Driver current domicile match Bid Location (Driver should be participating in an open bid)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_PART_OF_BID','B',null,'IF driver is part of an OPEN Bid return TRUE otherwise FALSE');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_PREF_COUNT','L',null,'Number of valid preferences for a driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_RANK','L',null,'driver rank based on seniority');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_ANET_DRIVER_DIFF','D',null,'Earliest first delivery stop time window (ANET@ first delivery) - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DEL_ANLT_DRIVER_DIFF','D',null,'Latest arrival of first delivery stop time window (ANLT@ first delivery) - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_DNET_DIFF','D',null,'Unloaded L_PKUP_DNET_DIFF = 0Loaded equals current time minus end of load''s first pickup tw ie. STOP.DNET (Note that dispatch time is already included in this unlike L_PKUP_END_DIFF)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_DNET_PASSED','D',null,'SYSTEMTIME-STOP.DNET at origin, Unloaded equals 0.Loaded equals current time minus earliest end of first pickup.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NAL_FACILITY_ALIAS_ID','E',null,'Returns NAL Facility alias Id of the driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_DNET_DRIVER_DIFF','D',null,'Earliest first pickup stop time window end (DNET@ origin) - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_DNET_LATE','D',null,'( DRIVER ETA - ORIGIN.DNET)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_DNLT_DRIVER_DIFF','D',null,'Latest first pickup stop time window end(DNLT@ origin) - driver''s ready datetime (Driver''s NAT or current time whichever is greater).');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_DNLT_LATE','D',null,'( DRIVER ETA - ORIGIN.DNLT)');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_CARRIER','E',null,'Carrier code from CARRIER.CARRIER_CODE corresponding to the carrier id will be retrieved. Need to see a value like USPS and not a number id.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_FACILITY_ALIAS_ID','E',null,'facility alias id of the delivery stop');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DESIG_CARRIER_ID','E',null,'Designated carrrier code and not id (Carrier designated on shipment which may or may not be accepted');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_DNET_TIME','L',null,'The earliest ending time, STOP.DNET, of the 1st delivery stop''s window as an integer. 3:05PM would be 1505.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_DNLT_TIME','L',null,'The earliest ending time, STOP.DNLT, of the 1st delivery stop''s window as an integer. 3:05PM would be 1505.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_DNET_PASSED','D',null,'Current local datetime - end of earliest time window, STOP.DNET, for first delivery stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_DNLT_PASSED','D',null,'Current local datetime - end of earliest time window, STOP.DNLT, for first delivery stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_LASTKNOWN_FACILITY_ALIAS_ID','E',null,'Returns the last known facility alias Id of the driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_TIME_TO_HOME_END','D',null,'Returns the difference between the current date time and the SHIFT_END + overtime from driver type of the driver');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_ALLOW_RETURN_PICKUP','L',null,'Returns ALLOW_RETURN_PICKUP (column) value of STATIC_ROUTE (table) of the previous accepted or recommended outbound shipment on the same trip if the NAL of the driver is not a DC.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_IS_USED_COUNT','L',null,'Return numer of SHIPMENTS (Any...outbound,inbound,return etc) accepted and being recommended within an iteration.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_COUNTRY','E',null,'Returns the country of the first stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_CUST_ID','E',null,'Returns the customer id from the load''s first pickup stop.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_END_DIFF','D',null,'Unloaded L_PKUP_END_DIFF = 0Loaded equals current time minus end of load''s first pickup tw.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PTA_PKUP_REG_MATCH','B',null,'T, if driver''s next available region is the same as the load''s pickup region."F", otherwise."OTHER", if bad/insufficient data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','AT_SAME_FACILITY_TYPE','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NAL_FACILITY_TYPE_DC','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NAL_FACILITY_TYPE_MNT','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_REG_IN_ER','B',null,'T, if load''s first pickup location is in comfort zone specified by driver''s experience/ER."F", otherwise."OTHER", if bad/unspecified data.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_REG_IN_ZONE','B',null,'T, if load''s first pickup location is in comfort zone specified by driver''s ded_type/DTP."F", otherwise."OTHER", if bad/unspecified data.Logically equivalent to PKUP_REG_IN_DTP.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','PKUP_TO_TERM_MI','L',null,'Number of miles run to from the first pickup to the intermediate terminal location.  If the intermediate location is not a terminal, PKUP_TO_TERM_MI = 9999.');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NAL_FACILITY_TYPE_STANDARD','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','D_NAL_FACILITY_TYPE_VENDOR','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_FACILITY_TYPE_DC','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_FACILITY_TYPE_MNT','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_FACILITY_TYPE_STANDARD','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_DEL_FACILITY_TYPE_VENDOR','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_FACILITY_TYPE_DC','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_FACILITY_TYPE_MNT','B',null,'description');

INSERT INTO RS300RATING_TOKEN ( VERSION_ID,RATING_TOKEN_ID,DATA_TYPE,CLIENT_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','L_PKUP_FACILITY_TYPE_STANDARD','B',null,'description');

Commit;





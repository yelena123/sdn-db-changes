set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for COMPANYTYPE

INSERT INTO COMPANYTYPE ( COMPANYTYPE,DESCRIPTION) 
VALUES  ( 0,'Transport Customer');

INSERT INTO COMPANYTYPE ( COMPANYTYPE,DESCRIPTION) 
VALUES  ( 1,'Transport Provider');

INSERT INTO COMPANYTYPE ( COMPANYTYPE,DESCRIPTION) 
VALUES  ( 2,'Logistics.com');

Commit;





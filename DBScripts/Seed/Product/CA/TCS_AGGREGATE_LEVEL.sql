set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_AGGREGATE_LEVEL

INSERT INTO TCS_AGGREGATE_LEVEL ( AGGR_LEVEL_ID,AGGR_LEVEL_NAME,DESCRIPTION) 
VALUES  ( 1,'Facility','Aggregate at facility level');

INSERT INTO TCS_AGGREGATE_LEVEL ( AGGR_LEVEL_ID,AGGR_LEVEL_NAME,DESCRIPTION) 
VALUES  ( 2,'Business Unit','Aggregate at business unit level');

Commit;





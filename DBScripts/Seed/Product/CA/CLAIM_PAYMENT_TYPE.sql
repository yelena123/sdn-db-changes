set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CLAIM_PAYMENT_TYPE

INSERT INTO CLAIM_PAYMENT_TYPE ( CLAIM_PAYMENT_TYPE,DESCRIPTION) 
VALUES  ( 2,'Check');

INSERT INTO CLAIM_PAYMENT_TYPE ( CLAIM_PAYMENT_TYPE,DESCRIPTION) 
VALUES  ( 1,'Auto Deduct');

Commit;





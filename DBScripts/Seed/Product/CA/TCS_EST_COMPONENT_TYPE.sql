set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_EST_COMPONENT_TYPE

INSERT INTO TCS_EST_COMPONENT_TYPE ( COMPONENT_TYPE_ID,COMPONENT_TYPE_NAME) 
VALUES  ( 1,'History based');

INSERT INTO TCS_EST_COMPONENT_TYPE ( COMPONENT_TYPE_ID,COMPONENT_TYPE_NAME) 
VALUES  ( 2,'Estimation formula based');

Commit;





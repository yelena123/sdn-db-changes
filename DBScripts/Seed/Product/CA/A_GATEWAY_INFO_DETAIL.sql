set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_GATEWAY_INFO_DETAIL

INSERT INTO A_GATEWAY_INFO_DETAIL ( KEY,VALUE,GATEWAY_INFO_ID) 
VALUES  ( 'targetAPIVersion','1.102',1);

INSERT INTO A_GATEWAY_INFO_DETAIL ( KEY,VALUE,GATEWAY_INFO_ID) 
VALUES  ( 'targetAPIVersion','1.102',3);

INSERT INTO A_GATEWAY_INFO_DETAIL ( KEY,VALUE,GATEWAY_INFO_ID) 
VALUES  ( 'targetAPIVersion','1.102',53);

INSERT INTO A_GATEWAY_INFO_DETAIL ( KEY,VALUE,GATEWAY_INFO_ID) 
VALUES  ( 'targetAPIVersion','1.102',51);

Commit;





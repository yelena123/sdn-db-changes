set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for REACT_HANDLER

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 22,'Changed','PUR_ORDER','com.manh.appointment.ui.handlers.POChangeHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 23,'Changed','SHIPMENT','com.manh.appointment.ui.handlers.ShipmentChangeHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 1,'Changed','SHIPMENT','com.manh.fm.trip.reacthandler.DispatchShipmentHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 2,'Accepted','SHIPMENT','com.manh.fm.trip.reacthandler.DispatchShipmentHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 3,'Created','APPOINTMENT','com.manh.tpe.trackingmsg.ilmreactions.SendUpdateToRSEngineReaction:TPE:LG');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 4,'Changed','APPOINTMENT','com.manh.tpe.trackingmsg.ilmreactions.SendUpdateToRSEngineReaction:TPE:LG');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 5,'Created','APPOINTMENT','com.manh.tpe.trackingmsg.ilmreactions.ApptTrackingMsgReaction:TPE:LG');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 6,'Changed','APPOINTMENT','com.manh.tpe.trackingmsg.ilmreactions.ApptTrackingMsgReaction:TPE:LG');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 10,'Deleted','APPOINTMENT','com.manh.tpe.trackingmsg.ilmreactions.ApptTrackingMsgReaction:TPE:LG');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 11,'Changed','SGMT','com.manh.tpe.shipment.TPEShipmentUpdateHandler:TPE:LG');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 15,'Created','DRIVER','com.manh.baseservices.drivermgr.driverShiftCache.DriverReactHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 16,'Deleted','SHIPMENT','com.manh.appointment.ui.handlers.ShipmentChangeHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 17,'Changed','ASN','com.manh.appointment.ui.handlers.ASNChangeHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 19,'Changed','LC','com.manh.fm.trip.reacthandler.DispatchLoadCloseMessageHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 20,'Created','VIRTUAL_TRIPS','com.manh.fm.trip.reacthandler.DispatchVirtualTripsHandler');

INSERT INTO REACT_HANDLER ( REACT_ID,ACTION_TYPE,OBJECT_TYPE,HANDLER_CLASS) 
VALUES  ( 21,'Deleted','VIRTUAL_TRIPS','com.manh.fm.trip.reacthandler.DispatchVirtualTripsHandler');

Commit;





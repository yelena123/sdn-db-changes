set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SPECREQHEADING

INSERT INTO SPECREQHEADING ( SPECREQHEADINGID,DESCRIPTION) 
VALUES  ( 2,'Corporate Profile');

INSERT INTO SPECREQHEADING ( SPECREQHEADINGID,DESCRIPTION) 
VALUES  ( 1,'Accessorials');

INSERT INTO SPECREQHEADING ( SPECREQHEADINGID,DESCRIPTION) 
VALUES  ( 0,'Special Instructions');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_WF_ACTIONDEF

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 1,201,'Schedule','com.manh.doms.app.common.workflowmanager.activity.impl.ScheduleActionHandler',1,1);

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 2,201,'Allocate','com.manh.doms.app.common.workflowmanager.activity.impl.AllocationActionHandler',2,0);

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 3,201,'Release','com.manh.doms.app.common.workflowmanager.activity.impl.ReleaseActionHandler',3,0);

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 4,201,'Cancel','com.manh.doms.app.common.workflowmanager.activity.impl.CancelActionHandler',4,0);

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 5,202,'Schedule','com.manh.doms.app.common.workflowmanager.activity.impl.ScheduleActionHandler',1,1);

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 6,202,'Allocate','com.manh.doms.app.common.workflowmanager.activity.impl.AllocationActionHandler',2,0);

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 7,202,'Release','com.manh.doms.app.common.workflowmanager.activity.impl.ReleaseActionHandler',3,0);

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 8,202,'Cancel','com.manh.doms.app.common.workflowmanager.activity.impl.CancelActionHandler',4,0);

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 9,201,'Transfer allocations','com.manh.doms.app.common.workflowmanager.activity.impl.RollupActionHandler',5,0);

INSERT INTO A_WF_ACTIONDEF ( ACTION_DEF_ID,DOMAIN_GROUP_ID,NAME,ACTION_HANDLER,RANK,IS_SCHEDULE) 
VALUES  ( 10,202,'Transfer allocations','com.manh.doms.app.common.workflowmanager.activity.impl.RollupActionHandler',5,0);

Commit;





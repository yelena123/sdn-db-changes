set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_TAX_CATEGORY

INSERT INTO A_TAX_CATEGORY ( TAX_CATEGORY_ID,TAX_CATEGORY_NAME) 
VALUES  ( 10,'Use Tax');

INSERT INTO A_TAX_CATEGORY ( TAX_CATEGORY_ID,TAX_CATEGORY_NAME) 
VALUES  ( 20,'Freight Tax');

INSERT INTO A_TAX_CATEGORY ( TAX_CATEGORY_ID,TAX_CATEGORY_NAME) 
VALUES  ( 30,'Sales Tax');

INSERT INTO A_TAX_CATEGORY ( TAX_CATEGORY_ID,TAX_CATEGORY_NAME) 
VALUES  ( 40,'VAT');

INSERT INTO A_TAX_CATEGORY ( TAX_CATEGORY_ID,TAX_CATEGORY_NAME) 
VALUES  ( 50,'No Tax');

Commit;





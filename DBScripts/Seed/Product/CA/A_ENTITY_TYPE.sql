set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_ENTITY_TYPE

INSERT INTO A_ENTITY_TYPE ( ENTITY_TYPE_ID,ENTITY_TYPE_NAME) 
VALUES  ( 10,'CustomerOrder');

INSERT INTO A_ENTITY_TYPE ( ENTITY_TYPE_ID,ENTITY_TYPE_NAME) 
VALUES  ( 1,'Item');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_EST_FIELD_TYPE

INSERT INTO TCS_EST_FIELD_TYPE ( FIELD_TYPE_ID,FIELD_TYPE) 
VALUES  ( 6,'Checkbox');

INSERT INTO TCS_EST_FIELD_TYPE ( FIELD_TYPE_ID,FIELD_TYPE) 
VALUES  ( 4,'Date Field');

INSERT INTO TCS_EST_FIELD_TYPE ( FIELD_TYPE_ID,FIELD_TYPE) 
VALUES  ( 2,'Drop Down');

INSERT INTO TCS_EST_FIELD_TYPE ( FIELD_TYPE_ID,FIELD_TYPE) 
VALUES  ( 5,'Drop Down with Yes/No');

INSERT INTO TCS_EST_FIELD_TYPE ( FIELD_TYPE_ID,FIELD_TYPE) 
VALUES  ( 7,'LABEL');

INSERT INTO TCS_EST_FIELD_TYPE ( FIELD_TYPE_ID,FIELD_TYPE) 
VALUES  ( 3,'LookUp');

INSERT INTO TCS_EST_FIELD_TYPE ( FIELD_TYPE_ID,FIELD_TYPE) 
VALUES  ( 1,'TextBox');

INSERT INTO TCS_EST_FIELD_TYPE ( FIELD_TYPE_ID,FIELD_TYPE) 
VALUES  ( 8,'UOM');

Commit;





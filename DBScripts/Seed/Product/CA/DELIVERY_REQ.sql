set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DELIVERY_REQ

INSERT INTO DELIVERY_REQ ( DELIVERY_REQ,DESCRIPTION,DISPLAY_ORDER) 
VALUES  ( 'RES','Residential parcel delivery required',1);

INSERT INTO DELIVERY_REQ ( DELIVERY_REQ,DESCRIPTION,DISPLAY_ORDER) 
VALUES  ( 'PCL','Non-residential parcel delivery required',2);

INSERT INTO DELIVERY_REQ ( DELIVERY_REQ,DESCRIPTION,DISPLAY_ORDER) 
VALUES  ( 'NEI','Neither',3);

Commit;





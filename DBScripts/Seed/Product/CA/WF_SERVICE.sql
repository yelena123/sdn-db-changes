set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for WF_SERVICE

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 51,'jms.queue.SELLING_INBOUND_QUEUE','Payment Processing','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','performPayment','getInstance','Invoice','Invoice',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 52,'jms.queue.SELLING_INBOUND_QUEUE','Adjust Tax','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','adjustTax','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 47,'jms.queue.SELLING_INBOUND_QUEUE','Publish Customer Communication','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','exportCommunicationOutboundMsg','getInstance','CustomerOrder',null,null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 1,'jms.queue.PROCESS_PARAMETERS_INBOUND_QUEUE','Process Parameters','com.manh.olm.fulfillment.services.bean.orderlineservice.impl.OrderLineServiceBean','executeProcessParameter','getInstance','OrderLine','OrderLine',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 2,'jms.queue.SOURCING_INBOUND_QUEUE','Source','com.manh.olm.fulfillment.services.bean.sourcingservice.impl.SourcingServiceBean','executeSourcing','getInstance','OrderLine','OrderLine',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 3,'jms.queue.COSTBASEDSOURCING_INBOUND_QUEUE','Cost Based Source','com.manh.olm.fulfillment.services.bean.sourcingservice.impl.SourcingServiceBean','executeCostBasedSourcing','getInstance','OrderLine','OrderLine',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 4,'jms.queue.PRIORITIZATION_INBOUND_QUEUE','Prioritize','com.manh.olm.fulfillment.services.bean.orderlineservice.impl.OrderLineServiceBean','executePrioritization','getInstance','OrderLine','OrderLine',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 5,'jms.queue.ALLOCATION_INBOUND_QUEUE','Allocate','com.manh.olm.fulfillment.services.bean.allocationservice.impl.AllocationServiceBean','executeAllocation','getInstance','OrderLine','Allocation',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 7,'jms.queue.FLOW_ALLOCATION_INBOUND_QUEUE','Flow Allocate','com.manh.olm.fulfillment.services.bean.allocationservice.impl.AllocationServiceBean','executeFlowAllocation','getInstance','InventoryData','Allocation',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 8,'jms.queue.DO_CREATE_INBOUND_QUEUE','Create DO','com.manh.olm.fulfillment.services.bean.doservice.impl.DOServiceBean','executeDOCreate','getInstance','Allocation','DistributionOrder',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 9,'jms.queue.DO_RELEASE_INBOUND_QUEUE','Release DO','com.manh.olm.fulfillment.services.bean.doservice.impl.DOServiceBean','executeDORelease','getInstance','DistributionOrder','DistributionOrder',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 10,'jms.queue.ORDER_LINE_INBOUND_QUEUE','SOCancel','com.manh.olm.fulfillment.services.bean.orderlineservice.impl.OrderLineServiceBean','executeSOCancellation','getInstance','OrderLine','OrderLine',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 38,'jms.queue.SELLING_INBOUND_QUEUE','Automatic Promotions','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','automaticPromotions','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 16,'jms.queue.FLOW_INBOUND_QUEUE','Cancel','com.manh.doms.app.services.bean.flowservice.FlowServiceLookup','executeFlowCancel','getInstance','InventoryData','InventoryData',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 17,'jms.queue.SOURCING_INBOUND_QUEUE','CO Source','com.manh.olm.fulfillment.services.bean.sourcingservice.impl.SourcingServiceBean','executeOrderSourcing','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 18,'jms.queue.COSTBASEDSOURCING_INBOUND_QUEUE','CO Cost Based Source','com.manh.olm.fulfillment.services.bean.sourcingservice.impl.SourcingServiceBean','executeOrderCostBasedSourcing','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 19,'jms.queue.PRIORITIZATION_INBOUND_QUEUE','CO Prioritize','com.manh.olm.fulfillment.services.bean.orderlineservice.impl.OrderLineServiceBean','executeOrderPrioritization','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 20,'jms.queue.PROCESS_PARAMETERS_INBOUND_QUEUE','CO Process Parameters','com.manh.olm.fulfillment.services.bean.orderlineservice.impl.OrderLineServiceBean','executeOrderProcessParameter','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 22,'jms.queue.ALLOCATION_INBOUND_QUEUE','CO Allocate','com.manh.olm.fulfillment.services.bean.allocationservice.impl.AllocationServiceBean','executeOrderAllocation','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 23,'jms.queue.DO_CREATE_INBOUND_QUEUE','CO Create DO','com.manh.olm.fulfillment.services.bean.doservice.impl.DOServiceBean','executeOrderDOCreate','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 24,'jms.queue.DO_RELEASE_INBOUND_QUEUE','CO Release DO','com.manh.olm.fulfillment.services.bean.doservice.impl.DOServiceBean','executeOrderDORelease','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 25,'jms.queue.SELLING_INBOUND_QUEUE','CO Cancel','com.manh.olm.fulfillment.services.bean.orderlineservice.impl.OrderLineServiceBean','executeOrderSOCancellation','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 26,'jms.queue.SELLING_INBOUND_QUEUE','Reevaluate Price','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','reEvalauatePrice','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 27,'jms.queue.SELLING_INBOUND_QUEUE','Reevaluate Tax','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','reEvalauateTax','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 28,'jms.queue.SELLING_INBOUND_QUEUE','Reevaluate Promotion','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','reEvalauatePromotion','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 29,'jms.queue.SELLING_INBOUND_QUEUE','Reevaluate Ship Charge','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','reEvalauateSnH','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 30,'jms.queue.SELLING_INBOUND_QUEUE','Immediate Authorization','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','immediateAuthorize','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 31,'jms.queue.SELLING_INBOUND_QUEUE','Batch Authorization','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','batchAuthorize','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 33,'jms.queue.SELLING_INBOUND_QUEUE','Create Shipment Invoice','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','createShipmentInvoice','getInstance','LPN','Invoice',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 34,'jms.queue.SOURCING_INBOUND_QUEUE','CO Undo Source','com.manh.olm.fulfillment.services.bean.sourcingservice.impl.SourcingServiceBean','executeUndoOrderSourcing','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 35,'jms.queue.ALLOCATION_INBOUND_QUEUE','CO Undo Allocate','com.manh.olm.fulfillment.services.bean.allocationservice.impl.AllocationServiceBean','executeUndoOrderAllocation','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 37,'jms.queue.SELLING_INBOUND_QUEUE','Create Adjustment Invoice','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','createAdjustmentInvoice','getInstance','CustomerOrder','Invoice',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 36,'jms.queue.SELLING_INBOUND_QUEUE','CO Send Outbound Msg','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','sendCustomerOrderOutboundMsg','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 40,'jms.queue.SELLING_INBOUND_QUEUE','CO Validate Order','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','validateOrder','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 41,'jms.queue.SELLING_INBOUND_QUEUE','Publish Customer Master','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','exportCustomerMasterOutboundMsg','getInstance','CustomerMaster','CustomerMaster',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 42,'jms.queue.SELLING_INBOUND_QUEUE','Update Customer Master','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','persistCustomerMaster','getInstance','CustomerOrder','CustomerMaster',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 45,'jms.queue.SELLING_INBOUND_QUEUE','Refund Tax','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','refundTax','getInstance','Invoice','Invoice',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 43,'jms.queue.SELLING_INBOUND_QUEUE','Sales Posting','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','salesPosting','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 44,null,'CreateChargeback','com.manh.eem.workflow.ejb.WorkflowServiceLookup','createChargeback','getWorkflowServiceHome','PurchaseOrder','PurchaseOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 46,'jms.queue.SELLING_INBOUND_QUEUE','Publish Invoice','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','publishInvoiceMsg','getInstance','Invoice','Invoice',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 48,'jms.queue.SELLING_INBOUND_QUEUE','Reevaluate Appeasement','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','reEvaluateAppeasement','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 49,'jms.queue.SELLING_INBOUND_QUEUE','Recalculate Payment','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','reCalculatePayment','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 50,'jms.queue.SELLING_INBOUND_QUEUE','External System Interaction','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','performCustomerOrderExtUpdate','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 53,'jms.queue.SELLING_INBOUND_QUEUE','Confirm Customer Order','com.manh.olm.selling.services.bean.sellingservices.impl.SellingServiceBean','confirmCustomerOrder','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 68,'jms.queue.RETURNS_INBOUND_QUEUE','Create Return ASN','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','createReturnASN','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 65,'jms.queue.RETURNS_INBOUND_QUEUE','Calculate Return Fees','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','applyReturnOrderFees','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 66,'jms.queue.RETURNS_INBOUND_QUEUE','Create Return Invoice','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','calculateReturnInvoice','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 76,'jms.queue.RETURNS_INBOUND_QUEUE','Create or Update Exchange Order','com.manh.olm.returns.services.exchangeorder.impl.ExchangeOrderServiceBean','createOrUpdateExchangeOrder','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 59,'jms.queue.RETURNS_INBOUND_QUEUE','Return Order Outbound','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','sendReturnOrderOutboundMsg','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 77,'jms.queue.RETURNS_INBOUND_QUEUE','Evaluate Receipt Variance','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','evaluateReceiptVariance','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 78,'jms.queue.RETURNS_INBOUND_QUEUE','Reevaluate Return SnH','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','reEvalauateReturnSnH','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 67,'jms.queue.SELLING_INBOUND_QUEUE','Create Exchange Order Invoice','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','calculateExchangeOrderInvoice','getInstance','CustomerOrder','CustomerOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 69,'jms.queue.RETURNS_INBOUND_QUEUE','Generate Return Label','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','generateReturnLabel','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 81,null,'Attach LPN','com.manh.eem.workflow.service.AttachLPNToDOBean','attachLPNsToDO','getInstance','DistributionOrder','DistributionOrder',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 71,'jms.queue.RETURNS_INBOUND_QUEUE','Cancel Return ASN','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','cancelReturnASN','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 72,'jms.queue.RETURNS_INBOUND_QUEUE','Cancel Exchange Order','com.manh.olm.returns.services.exchangeorder.impl.ExchangeOrderServiceBean','cancelExchangeOrder','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 74,'jms.queue.RETURNS_INBOUND_QUEUE','Get Return Order','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','getReturnOrder','getInstance','ASN','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 75,'jms.queue.RETURNS_INBOUND_QUEUE','Update Return Order Status','com.manh.olm.returns.services.returnorder.impl.ReturnOrderServiceBean','updateReturnOrderStatus','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 82,'jms.queue.ALLOCATION_INBOUND_QUEUE','Group Allocations','com.manh.doms.app.services.bean.allocationservice.impl.AllocationServiceBean','groupAllocations','getInstance','Allocation','Allocation',null,null,
'SO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 83,'jms.queue.RETURNS_INBOUND_QUEUE','Create RCI','com.manh.olm.returns.rci.services.impl.RCIServiceBean','validateAndCreateRCI','getInstance','ReturnOrder','ReturnOrder',null,null,
'CO',null,null,null,null,null);

INSERT INTO WF_SERVICE ( SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS,
SERVICE_CATEGORY,REF_FIELD1,REF_FIELD2,REF_FIELD3,REF_FIELD4,REF_FIELD5) 
VALUES  ( 92,'jms.queue.DOM_DO_CREATE','Batch And Tag','com.manh.eem.workflow.service.TagDOBean','tagDo','getInstance','DistributionOrder','DistributionOrder',null,null,
'SO',null,null,null,null,null);

Commit;





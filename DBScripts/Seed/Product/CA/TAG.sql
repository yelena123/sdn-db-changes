set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TAG

INSERT INTO TAG ( TAG_ID,TAG_NAME,DESCRIPTION,FACILITY_ALIAS_ID,TC_COMPANY_ID,HAS_ORDER_ATTR,HAS_ORDER_LINE_ATTR,HAS_ITEM_ATTR,HAS_INVENTORY_ATTR,RANK) 
VALUES  ( 1,'Other','Other Tag',null,null,0,0,0,0,0);

Commit;





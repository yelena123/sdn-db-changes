set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_CUSTOM_AGGR_OPTION

INSERT INTO TCS_CUSTOM_AGGR_OPTION ( AGGR_OPTION_ID,AGGR_OPTION_NAME) 
VALUES  ( 1,'Item Attributes');

INSERT INTO TCS_CUSTOM_AGGR_OPTION ( AGGR_OPTION_ID,AGGR_OPTION_NAME) 
VALUES  ( 2,'Package Type');

Commit;





SET DEFINE OFF;
SET ECHO ON;

CREATE OR REPLACE FORCE VIEW "CM_RATE_DISCOUNT_VIEW" ("ACCESSORIAL_PARAM_SET_ID", "CM_RATE_DISCOUNT_ID", "START_NUMBER", "END_NUMBER", "VALUE", "CURRENCY_CODE", "DISTANCE_UOM", "HAS_ERRORS") AS
select cmrd.accessorial_param_set_id,
      cmrd.cm_rate_discount_id,
      cmrd.start_number,
      cmrd.end_number,
      cmrd.value,
      aps.cm_rate_currency_code,
      aps.distance_uom,
      0
 from cm_rate_discount cmrd, accessorial_param_set aps
where cmrd.accessorial_param_set_id = aps.accessorial_param_set_id
 union all
 select iaps.accessorial_param_set_id,
      icmrd.cm_rate_discount_id,
      icmrd.start_number,
      icmrd.end_number,
      icmrd.value,
      aps.cm_rate_currency_code,
      aps.distance_uom,
      case icmrd.status when 4 then 1 else 0 end as has_errors
 from import_cm_rate_discount icmrd,
      import_accessorial_param_set iaps,
      accessorial_param_set aps
where     icmrd.param_id = iaps.param_id
      and iaps.accessorial_param_set_id = aps.accessorial_param_set_id
      and iaps.status = 1
 union all
 select icmrd.param_id,
      icmrd.cm_rate_discount_id,
      icmrd.start_number,
      icmrd.end_number,
      icmrd.value,
      iaps.cm_rate_currency_code,
      iaps.distance_uom,
      case icmrd.status when 4 then 1 else 0 end as has_errors
 from import_cm_rate_discount icmrd, import_accessorial_param_set iaps
where icmrd.param_id = iaps.param_id and iaps.status = 4;

CREATE OR REPLACE FORCE VIEW "CNTRY" ("CNTRY", "CNTRY_DESC", "ISO_CNTRY_CODE", "STATE_REQD_FLAG", "CURR_CODE", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select country_code,
      country_name,
      country_code,
      has_state_prov,
      dflt_currency_code,
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1))
 from country;

CREATE OR REPLACE FORCE VIEW "COGI_ITEM_DETAIL_VIEW" ("COGI_ID", "ASN_ID", "TC_COMPANY_ID", "TC_ASN_ID", "TC_ORDER_ID", "PURCHASE_ORDERS_ID", "TC_PURCHASE_ORDERS_LINE_ID", "PURCHASE_ORDERS_LINE_ITEM_ID", "DESCRIPTION", "GTIN", "PO_TOTAL_SIZE", "PO_TOTAL_SIZE_ID", "PO_UNIT_COST", "PO_UNIT_COST_CURRENCY_CODE", "SHIPPED_QTY", "SHIPPED_QTY_UOM_ID", "ASN_GROSS_AMOUNT", "ASN_GST_AMOUNT", "RECEIPT_QTY", "RECEIPT_QTY_UOM_ID", "ADM_DETAIL_ID", "ADM_DESCREPANCY_QTY", "ADM_DESCREPANCY_QTY_UOM_ID", "ADM_GROSS_AMT", "ADM_GST_AMT", "ADM_CURRENCY_CODE", "MV_UOM_ID", "BASE_UOM_ID", "QTY_CONV_FACTOR") AS
select cgi.cogi_id,
        cgi_dtl.asn_id,
        ld.tc_company_id,
        cgi_dtl.tc_asn_id,
        cgi_dtl.tc_order_id,
        ld.purchase_orders_id,
        --ld.TC_PURCHASE_ORDERS_ID,
        ld.tc_purchase_orders_line_id,
        oli.purchase_orders_line_item_id,
        oli.description,
        ld.gtin,
        oli.order_qty po_total_size,
        oli.qty_uom_id po_total_size_id,
        oli.unit_monetary_value po_unit_cost,
        oli.mv_currency_code po_unit_cost_currency_code,
        sum (ld.size_value) shipped_qty,
        ld.qty_uom_id shipped_qty_uom_id,
        round (
           sum (nvl (ld.size_value, 0) * nvl (oli.unit_monetary_value, 0)),
           4)
           asn_gross_amount,
        round (sum (nvl (ld.size_value, 0) * nvl (oli.unit_tax_amount, 0)),
               4)
           asn_gst_amount,
        sum (ld.received_qty) receipt_qty,
        ld.qty_uom_id receipt_qty_uom_id,
        adm_dtl.adm_detail_id,
        sum (nvl (ld.received_qty, 0) - nvl (ld.size_value, 0))
           adm_descrepancy_qty,
        adm_dtl.size_uom_id adm_descrepancy_qty_uom_id,
        round (
           sum (
              (nvl (ld.received_qty, 0) * nvl (oli.unit_monetary_value, 0))
              - (nvl (ld.size_value, 0) * nvl (oli.unit_monetary_value, 0))),
           4)
           adm_gross_amt,
        round (
           sum (
              (nvl (ld.received_qty, 0) * nvl (oli.unit_tax_amount, 0))
              - (nvl (ld.size_value, 0) * nvl (oli.unit_tax_amount, 0))),
           4)
           adm_gst_amt,
        max (nvl (oli.mv_currency_code, ' ')) adm_currency_code,
        oli.mv_size_uom_id mv_uom_id,
        oli.qty_uom_id_base base_uom_id,
        oli.qty_conv_factor qty_conv_factor
   from lpn_detail ld
        join purchase_orders_line_item oli
           on --   (ld.PURCHASE_ORDERS_ID = oli.PURCHASE_ORDERS_ID AND ld.INTERNAL_ORDER_DTL_ID = oli.PURCHASE_ORDERS_LINE_ITEM_ID)
              (ld.purchase_orders_id = oli.purchase_orders_id
               and ld.purchase_orders_line_id =
                      oli.purchase_orders_line_item_id)
        join lpn_movement alm
           on (ld.lpn_id = alm.lpn_id)
        join cogi_detail cgi_dtl
           on (alm.asn_id = cgi_dtl.asn_id
               and alm.tc_purchase_orders_id = cgi_dtl.tc_order_id)
        join cogi cgi
           on (cgi_dtl.cogi_id = cgi.cogi_id)
        left join adm_detail adm_dtl
           on (cgi_dtl.adm_id = adm_dtl.adm_id
               and oli.sku_id = adm_dtl.sku_id)
 --WHERE ld.gtin = NVL( adm_Dtl.gtin, ld.gtin)
 group by cgi_dtl.asn_id,
        cgi_dtl.tc_asn_id,
        cgi_dtl.tc_order_id,
        ld.purchase_orders_id,
        --ld.TC_PURCHASE_ORDERS_ID,
        ld.tc_purchase_orders_line_id,
        oli.purchase_orders_line_item_id,
        oli.description,
        ld.gtin,
        oli.order_qty,
        oli.qty_uom_id,
        ld.qty_uom_id,
        oli.unit_monetary_value,
        oli.mv_currency_code,
        adm_dtl.adm_detail_id,
        adm_dtl.size_uom_id,
        ld.tc_company_id,
        cgi.cogi_id,
        oli.mv_size_uom_id,
        oli.qty_uom_id_base,
        oli.qty_conv_factor
 union                                 -- COGI which has ASNs w/ ASN details
 select cgi.cogi_id,
        cgi_dtl.asn_id,
        oli.tc_company_id,
        cgi_dtl.tc_asn_id,
        cgi_dtl.tc_order_id,
        ad.purchase_orders_id,
        ad.tc_po_line_id,
        oli.purchase_orders_line_item_id,
        oli.description,
        ad.gtin,
        oli.order_qty po_total_size,
        oli.qty_uom_id po_total_size_id,
        oli.unit_monetary_value po_unit_cost,
        oli.mv_currency_code po_unit_cost_currency_code,
        sum (ad.shipped_qty) shipped_qty,
        ad.qty_uom_id shipped_qty_uom_id,
        round (
           sum (nvl (ad.shipped_qty, 0) * nvl (oli.unit_monetary_value, 0)),
           4)
           asn_gross_amount,
        round (
           sum (nvl (ad.shipped_qty, 0) * nvl (oli.unit_tax_amount, 0)),
           4)
           asn_gst_amount,
        sum (ad.received_qty) receipt_qty,
        ad.qty_uom_id receipt_qty_uom_id,
        adm_dtl.adm_detail_id,
        sum (nvl (ad.received_qty, 0) - nvl (ad.shipped_qty, 0))
           adm_descrepancy_qty,
        adm_dtl.size_uom_id adm_descrepancy_qty_uom_id,
        round (
           sum (
              (nvl (ad.received_qty, 0) * nvl (oli.unit_monetary_value, 0))
              - (nvl (ad.shipped_qty, 0) * nvl (oli.unit_monetary_value, 0))),
           4)
           adm_gross_amt,
        round (
           sum (
              (nvl (ad.received_qty, 0) * nvl (oli.unit_tax_amount, 0))
              - (nvl (ad.shipped_qty, 0) * nvl (oli.unit_tax_amount, 0))),
           4)
           adm_gst_amt,
        max (nvl (oli.mv_currency_code, ' ')) adm_currency_code,
        oli.mv_size_uom_id mv_uom_id,
        oli.qty_uom_id_base base_uom_id,
        oli.qty_conv_factor qty_conv_factor
   from asn_detail ad
        join purchase_orders_line_item oli
           on (ad.purchase_orders_id = oli.purchase_orders_id
               and ad.tc_po_line_id = oli.tc_po_line_id)
        join cogi_detail cgi_dtl
           on (ad.asn_id = cgi_dtl.asn_id
               and ad.tc_purchase_orders_id = cgi_dtl.tc_order_id)
        join cogi cgi
           on (cgi_dtl.cogi_id = cgi.cogi_id)
        left join adm_detail adm_dtl
           on (cgi_dtl.adm_id = adm_dtl.adm_id
               and oli.sku_id = adm_dtl.sku_id)
        join asn asn
           on (ad.asn_id = asn.asn_id and asn.asn_level = 20)
 --WHERE ad.gtin = NVL(adm_dtl.gtin, ad.gtin)
 group by cgi_dtl.asn_id,
        cgi_dtl.tc_asn_id,
        cgi_dtl.tc_order_id,
        ad.tc_po_line_id,
        oli.purchase_orders_line_item_id,
        oli.description,
        ad.gtin,
        oli.order_qty,
        oli.qty_uom_id,
        ad.qty_uom_id,
        ad.qty_uom_id,
        ad.purchase_orders_id,
        oli.unit_monetary_value,
        oli.mv_currency_code,
        adm_dtl.adm_detail_id,
        adm_dtl.size_uom_id,
        oli.tc_company_id,
        cgi.cogi_id,
        oli.mv_size_uom_id,
        oli.qty_uom_id_base,
        oli.qty_conv_factor;

CREATE OR REPLACE FORCE VIEW "COMPANY_HIERARCHY" ("TC_COMPANY_ID", "COMPANY_LEVEL", "DESCRIPTION") AS
(    select distinct
           company_id,
           ('LEVEL' || to_char (level - 1)),
           ('LEVEL' || to_char (level - 1))
      from company
connect by prior parent_company_id = company_id);

CREATE OR REPLACE FORCE VIEW "ACCESSORIAL_PARAM_SET_VIEW" ("ACCESSORIAL_PARAM_SET_ID", "TC_COMPANY_ID", "CARRIER_CODE", "CARRIER_ID", "USE_FAK_COMMODITY", "COMMODITY_CLASS", "EQUIPMENT_CODE", "EQUIPMENT_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "CM_RATE_TYPE", "CM_RATE_CURRENCY_CODE", "STOP_OFF_CURRENCY_CODE", "DISTANCE_UOM", "BUDGETED_APPROVED", "SIZE_UOM", "SIZE_UOM_ID", "IS_GLOBAL", "HAS_ERRORS", "DTL_HAS_ERRORS") AS
select aps.accessorial_param_set_id,
      aps.tc_company_id,
      cast (null as varchar (10)),
      aps.carrier_id,
      aps.use_fak_commodity,
      aps.commodity_class,
      cast (null as varchar (8)),
      aps.equipment_id,
      aps.effective_dt,
      aps.expiration_dt,
      aps.cm_rate_type,
      aps.cm_rate_currency_code,
      aps.stop_off_currency_code,
      aps.distance_uom,
      aps.budgeted_approved,
      cast (null as varchar (8)),
      aps.size_uom_id,
      aps.is_global,
      0 has_errors,
      coalesce (
         (select sum (decode (icrd.status, 4, 1, 0))
            from import_accessorial_param_set iaps,
                 import_cm_rate_discount icrd
           where aps.accessorial_param_set_id =
                    iaps.accessorial_param_set_id
                 and aps.tc_company_id = iaps.tc_company_id
                 and iaps.param_id = icrd.param_id),
         0)
      + coalesce (
           (select sum (decode (isoc.status, 4, 1, 0))
              from import_accessorial_param_set iaps,
                   import_stop_off_charge isoc
             where aps.accessorial_param_set_id =
                      iaps.accessorial_param_set_id
                   and aps.tc_company_id = iaps.tc_company_id
                   and iaps.param_id = isoc.param_id),
           0)
      + coalesce (
           (select sum (decode (iar.status, 4, 1, 0))
              from import_accessorial_param_set iaps,
                   import_accessorial_rate iar
             where aps.accessorial_param_set_id =
                      iaps.accessorial_param_set_id
                   and aps.tc_company_id = iaps.tc_company_id
                   and iaps.param_id = iar.param_id
                   and iaps.tc_company_id = iar.tc_company_id),
           0)
         dtl_has_errors
 from accessorial_param_set aps
 union all
 select iaps.param_id,
      iaps.tc_company_id,
      iaps.carrier_code,
      cast (null as int),
      iaps.use_fak_commodity,
      iaps.commodity_class,
      iaps.equipment_code,
      iaps.equipment_id,
      iaps.effective_dt,
      iaps.expiration_dt,
      iaps.cm_rate_type,
      iaps.cm_rate_currency_code,
      iaps.stop_off_currency_code,
      iaps.distance_uom,
      iaps.budgeted_approved,
      iaps.size_uom,
      cast (null as int),
      iaps.is_global,
      decode (iaps.status, 4, 1, 0) has_errors,
      coalesce ( (select sum (decode (icrd.status, 4, 1, 0))
                    from import_cm_rate_discount icrd
                   where iaps.param_id = icrd.param_id),
                0)
      + coalesce ( (select sum (decode (isoc.status, 4, 1, 0))
                      from import_stop_off_charge isoc
                     where iaps.param_id = isoc.param_id),
                  0)
      + coalesce (
           (select sum (decode (iar.status, 4, 1, 0))
              from import_accessorial_rate iar
             where iaps.param_id = iar.param_id
                   and iaps.tc_company_id = iar.tc_company_id),
           0)
         dtl_has_errors
 from import_accessorial_param_set iaps
where iaps.status = 4
      and not exists
             (select 1
                from accessorial_param_set aps
               where aps.accessorial_param_set_id = iaps.param_id);

CREATE OR REPLACE FORCE VIEW "ACCESSORIAL_RATE_DATA_VIEW" ("LANE_ACCESSORIAL_ID", "TC_COMPANY_ID", "ACCESSORIAL_ID", "ACCESSORIAL_CODE", "LANE_ID", "RATING_LANE_DTL_SEQ", "RATE", "MINIMUM_RATE", "CURRENCY_CODE", "IS_AUTO_APPROVE", "ACCESSORIAL_TYPE", "DATA_SOURCE", "DISTANCE_UOM", "IS_SHIPMENT_ACCESSORIAL", "PAYEE_CARRIER_ID", "MINIMUM_SIZE", "MAXIMUM_SIZE", "IS_TAX_ACCESSORIAL", "IS_ADD_TO_SPOT_CHARGE", "IS_APPLY_ONCE_PER_SHIP", "IS_NON_MACHINEABLE", "IS_SHIP_VIA", "IS_REQUESTED", "IS_RESIDENTIAL_DELIVERY", "IS_COST_PLUS_ACCESSORIAL") AS
select la.lane_accessorial_id,
      la.tc_company_id,
      la.accessorial_id,
      ac.accessorial_code,
      la.lane_id,
      la.rating_lane_dtl_seq,
      la.rate,
      la.minimum_rate,
      la.currency_code,
      la.is_auto_approve,
      ac.accessorial_type,
      ac.data_source,
      ac.distance_uom,
      la.is_shipment_accessorial,
      la.payee_carrier_id,
      minimum_size,
      maximum_size,
      ac.is_tax_accessorial,
      ac.is_add_to_spot_charge,
      ac.is_apply_once_per_ship,
      ac.is_non_machineable,
      ac.is_ship_via,
      ac.is_requested,
      ac.is_residential_delivery,
      ac.is_cost_plus_accessorial
 from lane_accessorial la, accessorial_code ac
where la.tc_company_id = ac.tc_company_id
      and la.accessorial_id = ac.accessorial_id;

CREATE OR REPLACE FORCE VIEW "ACCESSORIAL_RATE_VIEW" ("ACCESSORIAL_PARAM_SET_ID", "ACCESSORIAL_RATE_ID", "TC_COMPANY_ID", "MOT", "MOT_ID", "EQUIPMENT_CODE", "EQUIPMENT_ID", "SERVICE_LEVEL", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL", "PROTECTION_LEVEL_ID", "ACCESSORIAL_CODE", "RATE", "MINIMUM_RATE", "CURRENCY_CODE", "IS_AUTO_APPROVE", "EFFECTIVE_DT", "EXPIRATION_DT", "HAS_ERRORS", "PAYEE_CARRIER_CODE", "PAYEE_CARRIER_ID", "MINIMUM_SIZE", "MAXIMUM_SIZE", "SIZE_UOM", "SIZE_UOM_ID", "ZONE_ID", "IS_SHIPMENT_ACCESSORIAL", "ACCESSORIAL_ID", "INCOTERM_ID", "LONGEST_DIMENSION", "SECOND_LONGEST_DIMENSION", "LENGTH_PLUS_GRITH", "WEIGHT", "BASE_AMOUNT", "CHARGE_PER_X_FIELD1", "CHARGE_PER_X_FIELD2", "BASE_CHARGE", "RANGE_MAX_AMOUNT", "RANGE_MIN_AMOUNT", "MIN_RATE_TYPE", "MAXIMUM_RATE", "AMOUNT", "CUSTOMER_RATE", "CUSTOMER_MIN_CHARGE", "CALCULATED_RATE", "PACKAGE_TYPE_ID", "OPTION_GROUP_ID", "OPTION_GROUP_DESC", "MIN_LONGEST_DIMENSION", "MAX_LONGEST_DIMENSION", "MIN_SECOND_LONGEST_DIMENSION", "MAX_SECOND_LONGEST_DIMENSION", "MIN_THIRD_LONGEST_DIMENSION", "MAX_THIRD_LONGEST_DIMENSION", "MIN_WEIGHT", "MAX_WEIGHT", "MIN_LENGTH_PLUS_GRITH", "MAX_LENGTH_PLUS_GRITH", "BILLING_METHOD", "IS_ZONE_ORIGIN", "IS_ZONE_DEST", "IS_ZONE_STOPOFF", "INCREMENT_VAL", "ORM_TYPE", "ORM_OPERATOR", "ORM_OFSET", "COMMODITY_CODE_ID", "MAX_RANGE_COMMODITY_CODE_ID") AS
SELECT ACCESSORIAL_PARAM_SET_ID,
      ACCESSORIAL_RATE_ID,
      TC_COMPANY_ID,
      CAST (NULL AS VARCHAR (8)),
      MOT_ID,
      CAST (NULL AS VARCHAR (8)),
      EQUIPMENT_ID,
      CAST (NULL AS VARCHAR (8)),
      SERVICE_LEVEL_ID,
      CAST (NULL AS VARCHAR (8)),
      PROTECTION_LEVEL_ID,
      CAST (NULL AS VARCHAR (8)),
      RATE,
      MINIMUM_RATE,
      CURRENCY_CODE,
      IS_AUTO_APPROVE,
      EFFECTIVE_DT,
      EXPIRATION_DT,
      0,
      CAST (NULL AS VARCHAR (8)),
      PAYEE_CARRIER_ID,
      MINIMUM_SIZE,
      MAXIMUM_SIZE,
      CAST (NULL AS VARCHAR (8)),
      SIZE_UOM_ID,
      ZONE_ID,
      IS_SHIPMENT_ACCESSORIAL,
      ACCESSORIAL_ID,
      INCOTERM_ID,
      LONGEST_DIMENSION,
      SECOND_LONGEST_DIMENSION,
      LENGTH_PLUS_GRITH,
      WEIGHT,
      BASE_AMOUNT,
      CHARGE_PER_X_FIELD1,
      CHARGE_PER_X_FIELD2,
      BASE_CHARGE,                    -- Enhancement# MACR00094092 Changes
      RANGE_MAX_AMOUNT,
      RANGE_MIN_AMOUNT,
      MIN_RATE_TYPE,
      MAXIMUM_RATE,
      AMOUNT,
      CUSTOMER_RATE,
      CUSTOMER_MIN_CHARGE,
      CALCULATED_RATE,
      PACKAGE_TYPE_ID,
      OPTION_GROUP_ID,
      OPTION_GROUP_DESC,
      MIN_LONGEST_DIMENSION,
      MAX_LONGEST_DIMENSION,
      MIN_SECOND_LONGEST_DIMENSION,
      MAX_SECOND_LONGEST_DIMENSION,
      MIN_THIRD_LONGEST_DIMENSION,
      MAX_THIRD_LONGEST_DIMENSION,
      MIN_WEIGHT,
      MAX_WEIGHT,
      MIN_LENGTH_PLUS_GRITH,
      MAX_LENGTH_PLUS_GRITH,
      BILLING_METHOD,
      IS_ZONE_ORIGIN,
      IS_ZONE_DEST,
      IS_ZONE_STOPOFF,
      INCREMENT_VAL,                           -- Enhancement# MACR00075690
	  ORM_TYPE,
	   ORM_OPERATOR,
	  ORM_OFSET,
	  commodity_code_id,
	  max_range_commodity_code_id
 FROM ACCESSORIAL_RATE
 UNION ALL
 SELECT IAPS.PARAM_ID,
      IAR.ACCESSORIAL_RATE_ID,
      IAR.TC_COMPANY_ID,
      IAR.MOT,
      CAST (NULL AS INT),
      IAR.EQUIPMENT_CODE,
      CAST (NULL AS INT),
      IAR.SERVICE_LEVEL,
      CAST (NULL AS INT),
      IAR.PROTECTION_LEVEL,
      CAST (NULL AS INT),
      IAR.ACCESSORIAL_CODE,
      IAR.RATE,
      IAR.MINIMUM_RATE,
      IAR.CURRENCY_CODE,
      IAR.IS_AUTO_APPROVE,
      IAR.EFFECTIVE_DT,
      IAR.EXPIRATION_DT,
      DECODE (IAR.STATUS, 4, 1, 0) HAS_ERRORS,
      IAR.PAYEE_CARRIER_CODE,
      CAST (NULL AS INT),
      IAR.MINIMUM_SIZE,
      IAR.MAXIMUM_SIZE,
      IAR.SIZE_UOM,
      CAST (NULL AS INT),
      IAR.ZONE_ID,
      IAR.IS_SHIPMENT_ACCESSORIAL,
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      IAR.LONGEST_DIMENSION,
      IAR.SECOND_LONGEST_DIMENSION,
      IAR.LENGTH_PLUS_GRITH,
      IAR.WEIGHT,
      IAR.BASE_AMOUNT,
      IAR.CHARGE_PER_X_FIELD1,
      IAR.CHARGE_PER_X_FIELD2,
      CAST (NULL AS DECIMAL),
      IAR.HIGHER_RANGE,
      IAR.LOWER_RANGE,
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS VARCHAR (8)),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS INT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS DECIMAL)  ,                -- Enhancement# MACR00075690
	  CAST (NULL AS VARCHAR (1)),
	  CAST (NULL AS VARCHAR (1)),
	   CAST (NULL AS VARCHAR (9)),
	   CAST (NULL AS INT),
	   CAST (NULL AS INT)
 FROM IMPORT_ACCESSORIAL_RATE IAR, IMPORT_ACCESSORIAL_PARAM_SET IAPS
WHERE IAR.PARAM_ID = IAPS.PARAM_ID AND IAPS.STATUS = 1
 UNION ALL
 SELECT IAPS.PARAM_ID,
      IAR.ACCESSORIAL_RATE_ID,
      IAR.TC_COMPANY_ID,
      IAR.MOT,
      CAST (NULL AS INT),
      IAR.EQUIPMENT_CODE,
      CAST (NULL AS INT),
      IAR.SERVICE_LEVEL,
      CAST (NULL AS INT),
      IAR.PROTECTION_LEVEL,
      CAST (NULL AS INT),
      IAR.ACCESSORIAL_CODE,
      IAR.RATE,
      IAR.MINIMUM_RATE,
      IAR.CURRENCY_CODE,
      IAR.IS_AUTO_APPROVE,
      IAR.EFFECTIVE_DT,
      IAR.EXPIRATION_DT,
      DECODE (IAR.STATUS, 4, 1, 0) HAS_ERRORS,
      IAR.PAYEE_CARRIER_CODE,
      CAST (NULL AS INT),
      IAR.MINIMUM_SIZE,
      IAR.MAXIMUM_SIZE,
      IAR.SIZE_UOM,
      CAST (NULL AS INT),
      IAR.ZONE_ID,
      IAR.IS_SHIPMENT_ACCESSORIAL,
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      IAR.LONGEST_DIMENSION,
      IAR.SECOND_LONGEST_DIMENSION,
      IAR.LENGTH_PLUS_GRITH,
      IAR.WEIGHT,
      IAR.BASE_AMOUNT,
      IAR.CHARGE_PER_X_FIELD1,
      IAR.CHARGE_PER_X_FIELD2,
      CAST (NULL AS DECIMAL),
      IAR.HIGHER_RANGE,
      IAR.LOWER_RANGE,
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS VARCHAR (8)),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS INT),
      CAST (NULL AS INT),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS DECIMAL),
      CAST (NULL AS INT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS SMALLINT),
      CAST (NULL AS DECIMAL)   ,               -- Enhancement# MACR00075690
	  CAST (NULL AS VARCHAR (1)),
	   CAST (NULL AS VARCHAR (1)),
	 CAST (NULL AS VARCHAR (9)),
	 CAST (NULL AS INT),
	   CAST (NULL AS INT)
 FROM IMPORT_ACCESSORIAL_RATE IAR, IMPORT_ACCESSORIAL_PARAM_SET IAPS
WHERE IAR.PARAM_ID = IAPS.PARAM_ID AND IAPS.STATUS = 4;

CREATE OR REPLACE FORCE VIEW "ACTIVE_DRIVER_BID_VIEW" ("BID_ID", "DRIVER_ID", "START_TIME", "END_TIME", "ORIGIN_FAC_ALIAS", "IS_OPEN", "DRIVER_BID_STATUS") AS
select db1.driver_bid_id,
      dbmd1.driver_id,
      db1.bid_start_time,
      db1.bid_close_time,
      db1.bid_origin_facility_alias_id,
      db1.is_bid_open,
      dbmd1.driver_bid_status
 from driver_bid db1, driver_bid_driver_map dbmd1
where db1.driver_bid_id = dbmd1.driver_bid_id
      and db1.bid_start_time =
             (select max (bid_start_time)
                from driver_bid db, driver_bid_driver_map dbmd
               where db.driver_bid_id = dbmd.driver_bid_id
                     and dbmd.driver_id = dbmd1.driver_id);

CREATE OR REPLACE FORCE VIEW "ACTIVE_SEGMENT_BID_VIEW" ("BID_ID", "SEGMENT_ID", "START_TIME", "END_TIME", "ORIGIN_FAC_ALIAS", "IS_OPEN", "SEGMENT_BID_STATUS") AS
select db1.driver_bid_id,
      dbms1.segment_id,
      db1.bid_start_time,
      db1.bid_close_time,
      db1.bid_origin_facility_alias_id,
      db1.is_bid_open,
      dbms1.segment_bid_status
 from driver_bid db1, driver_bid_segment_map dbms1
where db1.driver_bid_id = dbms1.driver_bid_id
      and db1.bid_start_time =
             (select max (bid_start_time)
                from driver_bid db, driver_bid_segment_map dbms
               where db.driver_bid_id = dbms.driver_bid_id
                     and dbms.segment_id = dbms1.segment_id);

CREATE OR REPLACE FORCE VIEW "ACTIVE_TRACTOR_BID_VIEW" ("BID_ID", "TRACTOR_ID", "START_TIME", "END_TIME", "ORIGIN_FAC_ALIAS", "IS_OPEN", "TRACTOR_BID_STATUS") AS
select db1.driver_bid_id,
      dbtm1.tractor_id,
      db1.bid_start_time,
      db1.bid_close_time,
      db1.bid_origin_facility_alias_id,
      db1.is_bid_open,
      dbtm1.tractor_bid_status
 from driver_bid db1, driver_bid_tractor_map dbtm1
where db1.driver_bid_id = dbtm1.driver_bid_id
      and db1.bid_start_time =
             (select max (bid_start_time)
                from driver_bid db, driver_bid_tractor_map dbtm
               where db.driver_bid_id = dbtm.driver_bid_id
                     and dbtm.tractor_id = dbtm1.tractor_id);

CREATE OR REPLACE FORCE VIEW "AIRPORT_V" ("A_CODE", "C_CODE") AS
select x0.airportcode, x0.countrycode
 from airport x0;

CREATE OR REPLACE FORCE VIEW "AREAVOLUME_V" ("RFPID", "AREA", "IB_VOLUME", "OB_VOLUME") AS
SELECT x0.rfpid,
x0.area,
SUM (
CASE
  WHEN (x0.direction = 'IB')
  THEN x0.volume
  ELSE 0
END),
SUM (
CASE
  WHEN (x0.direction = 'OB')
  THEN x0.volume
  ELSE 0
END)
FROM capacityfacility_v x0
GROUP BY x0.rfpid,
x0.area;

CREATE OR REPLACE FORCE VIEW "A_DOCREATE_FAILURE_RESULT" ("A_IDENTITY", "A_ALLOCATIONID", "A_ORDERNUMBER", "A_ORDERLINENUMBER", "ORDERED_SKU", "ALLOCATED_SKU", "A_EXECUTED_TEMPLATE", "CURRENT_LINE_STATUS", "A_PROCESSRUNID", "LAST_UPDATED_DTTM", "A_REASONCODE", "TC_COMPANY_ID") AS
select ua.a_identity,
      ua.a_allocationid,
      ua.a_ordernumber,
      ua.a_orderlinenumber,
      pol.sku as ordered_sku,
      a_skuname as allocated_sku,
      ua.a_executed_template,
      polstatus.description as current_line_status,
      ua.a_processrunid,
      ua.last_updated_dttm,
      ua.a_reasoncode,
      pol.tc_company_id
 from a_unreleasable_alloc ua,
      a_orderinventoryallocation a,
      purchase_orders_line_item pol,
      purchase_orders_line_status polstatus
where ua.a_allocationid = a.a_identity
      and ua.a_order_line_id = pol.purchase_orders_line_item_id
      and pol.purchase_orders_line_status =
             polstatus.purchase_orders_line_status;

CREATE OR REPLACE FORCE VIEW "A_VD_ORDERALLOCATION" ("CURRENT_TC_COMPANY_ID", "IDENTITY", "INVALLOCATIONID", "STATUS", "ALLOCATEDQUANTITY", "INVENTORYID", "ORDERLINEID", "DELIVERYTZ", "ORDERLINENUMBER", "ORDERLINEQUANTITY", "ONHOLD", "PICKUPTZ", "COMMITTEDDELIVERYDATE", "ORDERID", "CUSTOMERID", "CUSTOMERCODE", "CHANNEL_TYPE", "ORDERNUMBER", "DISTRIBUTIONCENTERID", "DISTRIBUTIONCENTERNAME", "REQUESTEDSKUID", "REQUESTEDSKUNAME", "FILLEDSKUID", "FILLEDSKUNAME", "FILLED_SKU_UOM", "UOM", "SUPPLY", "SUPPLY_DETAIL", "IS_RELEASED", "COMMITTEDSHIPDATE", "SUBSTITUTIONTYPE", "ISVIRTUALALLOCATION", "SUPPLY_TYPE", "ORDER_TYPE", "ORDER_TYPE_STR", "DESTINATION_FACILITY_ALIAS", "IS_LOCKED", "OVERRIDE_ALLOCATED_QTY", "CONSUMED_CAPACITY", "ALLOCATED_BY", "ALLOCATION_DTTM", "ORDER_LINE_STATUS", "NET_NEEDS", "IS_FLOWTHRU", "CONFIRMED_QTY", "HAS_ALERTS", "MUSTSHIPDATE", "MUSTRELEASEDATE", "INVENTORY_SEGMENT_ID", "INVENTORY_SEGMENT_NAME", "LINE_DEST_FNAME", "LINE_DEST_LNAME", "LINE_DEST_ADDRESS_1", "LINE_DEST_ADDRESS_2", "LINE_DEST_CITY", "LINE_DEST_STATE_PROV", "LINE_DEST_POSTAL_CODE", "LINE_DEST_COUNTRY_CODE", "ORDERED_UOM", "QTY_CONV_FACTOR", "A_ALLOCATION_TYPE", "CARRIER_ID", "CARRIER", "SERVICE_LEVEL_ID", "SERVICE_LEVEL", "MOT_ID", "MOT", "INVENTORY_TYPE") AS
SELECT DISTINCT a_orderinventoryallocation.tc_company_id current_tc_company_id,
a_orderinventoryallocation.a_identity identity,
a_orderinventoryallocation.a_identity invallocationid,
a_orderinventoryallocation.a_status status,
a_orderinventoryallocation.quantity allocatedquantity,
a_orderinventoryallocation.a_inventoryid inventoryid,
a_orderinventoryallocation.a_orderlineid orderlineid,
a_orderinventoryallocation.a_deliverytz deliverytz,
purchase_orders_line_item.tc_po_line_id orderlinenumber,
purchase_orders_line_item.order_qty orderlinequantity,
purchase_orders_line_item.on_hold onhold,
a_orderinventoryallocation.a_pickuptz pickuptz,
a_orderinventoryallocation.a_deliverydate committeddeliverydate,
purchase_orders.purchase_orders_id orderid,
purchase_orders.customer_id customerid,
purchase_orders.customer_code customercode,
purchase_orders.channel_type channel_type,
purchase_orders.tc_purchase_orders_id ordernumber,
a_orderinventoryallocation.a_distributioncenterid distributioncenterid,
a_orderinventoryallocation.a_dcname distributioncentername,
purchase_orders_line_item.sku_id requestedskuid,
purchase_orders_line_item.sku requestedskuname,
filled_sku.sku_id filledskuid,
a_orderinventoryallocation.a_skuname filledskuname,
filled_sku.base_storage_uom_id filled_sku_uom,
purchase_orders_line_item.qty_uom_id_base uom,
a_orderinventoryallocation.a_invobjectid supply,
a_orderinventoryallocation.a_invobjectdtlid supply_detail,
(
CASE
  WHEN purchase_orders_line_item.is_flowthrou               = 1
  AND purchase_orders_line_item.purchase_orders_line_status > 500
  THEN 1
  ELSE 0
END) is_released,
a_orderinventoryallocation.a_shipdate committedshipdate,
a_orderinventoryallocation.a_substitutiontype substitutiontype,
a_orderinventoryallocation.is_virtual_allocation isvirtualallocation,
a_orderinventoryallocation.a_supply_type supply_type,
purchase_orders.order_category order_type,
purchase_orders.order_category order_type_str,
a_orderinventoryallocation.a_destfacilityname destination_facility_alias,
purchase_orders_line_item.is_quantity_locked is_locked,
a_orderinventoryallocation.override_allocated_qty override_allocated_qty,
a_orderinventoryallocation.consumed_capacity consumed_capacity,
a_orderinventoryallocation.allocated_by allocated_by,
a_orderinventoryallocation.allocation_dttm allocation_dttm,
purchase_orders_line_item.purchase_orders_line_status order_line_status,
purchase_orders_line_item.net_needs net_needs,
purchase_orders_line_item.is_flowthrou is_flowthru,
a_orderinventoryallocation.confirmed_qty confirmed_qty,
a_orderinventoryallocation.has_alerts has_alerts,
a_orderinventoryallocation.a_mustshipdttm mustshipdate,
a_orderinventoryallocation.a_releasedate mustreleasedate,
(
CASE
  WHEN a_orderinventoryallocation.inventory_segment_id IS NULL
  THEN NULL
  ELSE a_orderinventoryallocation.inventory_segment_id
END) inventory_segment_id,
(
CASE
  WHEN a_orderinventoryallocation.inventory_segment_id IS NULL
  THEN NULL
  ELSE syscode.code_desc
END) inventory_segment_name,
purchase_orders_line_item.d_name line_dest_fname,
purchase_orders_line_item.d_last_name line_dest_lname,
purchase_orders_line_item.d_address_1 line_dest_address_1,
purchase_orders_line_item.d_address_2 line_dest_address_2,
purchase_orders_line_item.d_city line_dest_city,
purchase_orders_line_item.d_state_prov line_dest_state_prov,
purchase_orders_line_item.d_postal_code line_dest_postal_code,
purchase_orders_line_item.d_country_code line_dest_country_code,
purchase_orders_line_item.qty_uom_id ordered_uom,
purchase_orders_line_item.qty_conv_factor qty_conv_factor,
a_orderinventoryallocation.a_allocation_type,
(
CASE
  WHEN a_orderinventoryallocation.a_carrierid IS NULL
  THEN NULL
  ELSE carrier_code.carrier_id
END) carrier_id,
(
CASE
  WHEN a_orderinventoryallocation.a_carrierid IS NULL
  THEN NULL
  ELSE carrier_code.carrier_code
END) carrier,
(
CASE
  WHEN a_orderinventoryallocation.a_servicelevelid IS NULL
  THEN NULL
  ELSE service_level.service_level_id
END) service_level_id,
(
CASE
  WHEN a_orderinventoryallocation.a_servicelevelid IS NULL
  THEN NULL
  ELSE service_level.service_level
END) service_level,
(
CASE
  WHEN a_orderinventoryallocation.a_modeid IS NULL
  THEN NULL
  ELSE mot.mot_id
END) mot_id,
(
CASE
  WHEN a_orderinventoryallocation.a_modeid IS NULL
  THEN NULL
  ELSE mot.mot
END) mot,
a_orderinventoryallocation.a_inventory_type inventory_type
FROM a_orderinventoryallocation
JOIN purchase_orders_line_item
ON (a_orderinventoryallocation.a_orderlineid = purchase_orders_line_item.purchase_orders_line_item_id)
JOIN purchase_orders
ON (purchase_orders_line_item.purchase_orders_id = purchase_orders.purchase_orders_id)
LEFT OUTER JOIN sku filled_sku
ON (a_orderinventoryallocation.a_skuid = filled_sku.sku_id)
LEFT OUTER JOIN carrier_code
ON (a_orderinventoryallocation.a_carrierid = carrier_code.carrier_id)
LEFT OUTER JOIN service_level
ON (a_orderinventoryallocation.a_servicelevelid = service_level.service_level_id)
LEFT OUTER JOIN mot
ON (a_orderinventoryallocation.a_modeid = mot.mot_id)
LEFT OUTER JOIN sys_code syscode
ON (a_orderinventoryallocation.inventory_segment_id = syscode.sys_code_id
AND syscode.rec_type                                = 'B'
AND syscode.code_type                               = '068')
WHERE a_orderinventoryallocation.a_status          <> 10;

CREATE OR REPLACE FORCE VIEW "A_VD_SOURCINGAUDIT" ("CURRENT_TC_COMPANY_ID", "IDENTITY", "RULEID", "SOURCERANK", "RULENAME", "ORDERLINEID", "VIEWDEFNAME", "VIEWRULESETNAME") AS
SELECT a_sourcingauditdetail.tc_company_id current_tc_company_id,
a_sourcingauditdetail.a_identity identity,
a_sourcingauditdetail.a_ruleid ruleid,
a_sourcingauditdetail.a_sourcerank sourcerank,
lema_rule.name rulename,
purchase_orders_line_item.purchase_orders_line_item_id orderlineid,
	viewdef.view_def_name viewdefname,
	viewruleset.view_ruleset_name viewrulesetname
FROM  ( ( ( ( a_sourcingauditdetail
LEFT OUTER JOIN lema_rule
ON lema_rule.identity = a_sourcingauditdetail.a_ruleid)
INNER JOIN purchase_orders_line_item
ON a_sourcingauditdetail.a_orderlineid = purchase_orders_line_item.purchase_orders_line_item_id))
LEFT OUTER JOIN i_view_def viewdef
ON a_sourcingauditdetail.view_id = viewdef.view_def_id
LEFT OUTER JOIN i_view_ruleset viewruleset
ON a_sourcingauditdetail.a_rule_set_id = viewruleset.view_ruleset_id)
WHERE a_sourcingauditdetail.a_status = 0;

CREATE OR REPLACE FORCE VIEW "CAPACITYFACILITY_V" ("RFPID", "FACILITYCODE", "DIRECTION", "VOLUME", "AREA") AS
SELECT x1.rfpid,
x1.facilitycode,
x1.direction,
ROUND ( COALESCE ( SUM ( ( (x0.volume * 7.0000000000000000) /
CASE
  WHEN (x0.volumefrequency = 0)
  THEN 1
  WHEN (x0.volumefrequency = 1)
  THEN 7
  WHEN (x0.volumefrequency = 2)
  THEN 14
  WHEN (x0.volumefrequency = 3)
  THEN 30.4166
  WHEN (x0.volumefrequency = 4)
  THEN 182.5
  WHEN (x0.volumefrequency = 5)
  THEN 365
  ELSE 7
END)), 0), 3),
x1.area
FROM lane x0,
lanefacility_v x1
WHERE x0.lane_type <> 2
AND ( ( (x1.rfpid   = x0.rfpid)
AND (x1.laneid      = x0.laneid))
AND (x0.round_num   =
(SELECT MAX (x2.round_num)
FROM lane x2
WHERE ( (x2.rfpid = x0.rfpid)
AND (x2.laneid    = x0.laneid))
GROUP BY x2.rfpid,
  x2.laneid
)))
GROUP BY x1.rfpid,
x1.facilitycode,
x1.direction,
x1.area;

CREATE OR REPLACE FORCE VIEW "CARGO_CLAIM_TYPE_VIEW" ("CARGO_CLAIM_TYPE", "DESCRIPTION") AS
select claim_type as cargo_claim_type, description as description
 from claim_type
where claim_category = 1;

CREATE OR REPLACE FORCE VIEW "CARRIER_DISCOUNT_VIEW" ("LANEBID_KEY_ID", "RFP_ID", "LANE_ID", "OBCARRIERCODEID", "LANEEQUIPMENTTYPEID", "PACKAGEID", "LANE_ADDED_ROUND_NUM", "DISCOUNT_GROUP_ID", "DISCOUNT_GROUP_NAME", "DISCOUNT_GROUP_DESCRIPTION", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "DISCOUNT_CRITERION_ID", "CRITERION_ADDED_ROUND_NUM", "CRITERION_VALUE", "DISCOUNT_TYPE_ID", "DISCOUNT_TYPE_DESCRIPTION", "RATE_FIELD_ID", "DISCOUNT_PERCENT", "LANEBID_COLUMN_NAME", "RATE_FIELD_DESCRIPTION") AS
select lanebid_key.lanebid_key_id as lanebid_key_id,
      lanebid_key.rfp_id as rfp_id,
      lanebid_key.lane_id as lane_id,
      lanebid_key.obcarriercodeid as obcarriercodeid,
      lanebid_key.laneequipmenttypeid as laneequipmenttypeid,
      lanebid_key.packageid as packageid,
      lanebid_discount_group_map.lane_added_round_num
         as lane_added_round_num,
      carrier_discount_group.discount_group_id as discount_group_id,
      carrier_discount_group.name as discount_group_name,
      carrier_discount_group.description as discount_group_description,
      carrier_discount_group.created_source_type as created_source_type,
      carrier_discount_group.created_source as created_source,
      carrier_discount_group.created_dttm as created_dttm,
      carrier_discount_group.last_updated_source_type
         as last_updated_source_type,
      carrier_discount_group.last_updated_source as last_updated_source,
      carrier_discount_group.last_updated_dttm as last_updated_dttm,
      carrier_discount_criteria.discount_criterion_id
         as discount_criterion_id,
      carrier_discount_criteria.criterion_added_round_num
         as criterion_added_round_num,
      carrier_discount_criteria.criterion_value as criterion_value,
      carrier_discount_type.discount_type_id as discount_type_id,
      carrier_discount_type.description as discount_type_description,
      carrier_discount.rate_field_id as rate_field_id,
      carrier_discount.discount_percent as discount_percent,
      rate_field.column_name as lanebid_column_name,
      rfpcolproperties.displayname as rate_field_description
 from lanebid_key,
      lanebid_discount_group_map,
      carrier_discount_group,
      carrier_discount_criteria,
      carrier_discount_type,
      carrier_discount,
      rate_field,
      rfpcolproperties
where lanebid_key.lanebid_key_id =
         lanebid_discount_group_map.lanebid_key_id
      and lanebid_discount_group_map.discount_group_id =
             carrier_discount_group.discount_group_id
      and carrier_discount_criteria.discount_group_id =
             carrier_discount_group.discount_group_id
      and carrier_discount_criteria.discount_type_id =
             carrier_discount_type.discount_type_id
      and carrier_discount.discount_criterion_id =
             carrier_discount_criteria.discount_criterion_id
      and rate_field.rate_field_id = carrier_discount.rate_field_id
      and rate_field.column_name = rfpcolproperties.colname
      and rfpcolproperties.datatable like 'LB'
      and rfpcolproperties.round_num =
             carrier_discount_criteria.criterion_added_round_num
      and rfpcolproperties.rfpid = lanebid_key.rfp_id;

CREATE OR REPLACE FORCE VIEW "CARRIER_MAXROUND" ("RFPID", "OBCARRIERCODEID", "ROUND_NUM", "TPCOMPANYID") AS
select x0.rfpid,
        x0.obcarriercodeid,
        max (x0.round_num),
        x0.tpcompanyid
   from rfpparticipant x0, rfp x1
  where ( ( (x1.rfpid = x0.rfpid) and (x1.round_num = x0.round_num))
         and (x1.rfpstatus > 0))
 group by x0.rfpid, x0.obcarriercodeid, x0.tpcompanyid;

CREATE OR REPLACE FORCE VIEW "CLAIMS_VIEW" ("CLAIM_ID", "TC_COMPANY_ID", "TC_CLAIM_ID", "CARRIER_CLAIM_ID", "CLAIM_CLASSIFICATION", "CLAIM_CATEGORY", "CLAIM_TYPE", "CLAIM_STATUS", "CLAIM_DATE", "TC_SHIPMENT_ID", "FREIGHT_INVOICE_ID", "MERCHANDISE_INVOICE_ID", "CARRIER_ID", "MOT_ID", "O_NAME", "O_FACILITY_ALIAS_ID", "O_FACILITY_ID", "O_ADDRESS", "O_CITY", "O_STATE_PROV", "O_POSTAL_CODE", "O_COUNTY", "O_COUNTRY", "D_NAME", "D_FACILITY_ALIAS_ID", "D_FACILITY_ID", "D_ADDRESS", "D_CITY", "D_STATE_PROV", "D_POSTAL_CODE", "D_COUNTY", "D_COUNTRY", "CURRENCY_CODE", "SHIP_DATE", "RECD_DATE", "MERCHANDISE_AMOUNT", "PROP_FREIGHT_AMOUNT", "SALVAGE_AMOUNT", "TOTAL_CLAIM_AMOUNT", "TOTAL_PAID_AMOUNT", "SETTLEMENT_AMOUNT", "ORIGINAL_CLAIM_AMOUNT", "SALVAGE_PERCENTAGE", "SHIPMENT_QTY", "SHIPMENT_WEIGHT", "SEAL_1", "SEAL_2", "TRACKING_NO", "WEIGHT_UOM", "QTY_UOM", "WEIGHT_UOM_ID", "QTY_UOM_ID", "ROLE", "ACKNOWLEDGE_DATE", "SUPPLIER_NAME", "CLAIMS_FACILITY_ID", "CLAIMS_DIVISION_ID", "EQUIPMENT_ID", "BILLING_ACCOUNT_NO", "SERVICE_LEVEL_ID", "CREATED_DTTM", "CREATED_SOURCE", "LAST_UPDATED_DTTM", "LAST_UPDATED_SOURCE", "RECOUP_AMOUNT", "TEMPERATURE_RECORDED", "PICKUP_NO", "NEW_PO_NO", "HANDLING_FEE", "PURCHASE_ORDER", "IS_APPROVED_BY_MANAGER", "CLAIM_FILED_DATE", "CLAIM_DUE_DATE", "TP_COMPANY_ID", "AP_STATUS_FLAG", "CLAIM_ACTION_CODE", "CLAIM_ACTION_TYPE", "HAS_ALERTS", "HAS_WARNINGS", "HAS_ERRORS", "BALANCE_DUE", "DAYS_ASSIGNED", "STATUS_CHANGE_DATE", "TRANS_CLAIM_AMOUNT", "CREATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE_TYPE", "REFUND_AMOUNT", "BILLING_METHOD", "INV_ID", "SUPPLIER_NUMBER", "BILL_OF_LADING_NUMBER", "PARENT_CLAIM_ID", "PARENT_TC_CLAIM_ID", "SUPPLIER_BOL_NUMBER", "HAS_ATTACHMENTS") AS
SELECT claim_id,
      tc_company_id,
      tc_claim_id,
      carrier_claim_id,
      claim_classification,
      claim_category,
      claim_type,
      claim_status,
      claim_date,
      tc_shipment_id,
      freight_invoice_id,
      merchandise_invoice_id,
      carrier_id,
      mot_id,
      o_name,
      o_facility_alias_id,
      o_facility_id,
      o_address,
      o_city,
      o_state_prov,
      o_postal_code,
      o_county,
      o_country,
      d_name,
      d_facility_alias_id,
      d_facility_id,
      d_address,
      d_city,
      d_state_prov,
      d_postal_code,
      d_county,
      d_country,
      currency_code,
      ship_date,
      recd_date,
      merchandise_amount,
      prop_freight_amount,
      salvage_amount,
      CASE
         WHEN claim_classification = 2
         THEN
            TRUNC (
               (COALESCE (trans_claim_amount, 0)
                + COALESCE (handling_fee, 0)),
               2)
         ELSE
            TRUNC (
                 COALESCE (merchandise_amount, 0)
               + COALESCE (prop_freight_amount, 0)
               + COALESCE (recoup_amount, 0)
               - COALESCE (
                    salvage_amount,
                    (COALESCE (merchandise_amount, 0)
                     * (COALESCE (salvage_percentage, 0) / 100)))
               + .005,
               2)
      END
         total_claim_amount,
      total_paid_amount,
      settlement_amount,
      original_claim_amount,
      salvage_percentage,
      shipment_qty,
      shipment_weight,
      seal_1,
      seal_2,
      tracking_no,
      (Select Size_Uom From Size_Uom Where Size_Uom_Id=Weight_Uom_Id)  weight_uom,
      (Select Size_Uom From Size_Uom Where Size_Uom_Id=Qty_Uom_Id) qty_uom,
      Weight_Uom_Id,
      Qty_Uom_Id,
      ROLE,
      acknowledge_date,
      supplier_name,
      claims_facility_id,
      claims_division_id,
      equipment_id,
      billing_account_no,
      service_level_id,
      created_dttm,
      created_source,
      last_updated_dttm,
      last_updated_source,
      recoup_amount,
      temperature_recorded,
      pickup_no,
      new_po_no,
      handling_fee,
      purchase_order,
      is_approved_by_manager,
      claim_filed_date,
      claim_due_date,
      tp_company_id,
      ap_status_flag,
      claim_action_code,
      claim_action_type,
      has_alerts,
      has_warnings,
      has_errors,
      CASE
         WHEN claim_classification = 2
         THEN
            CASE
               WHEN claim_status = 90
               THEN
                  0
               ELSE
                  CASE
                     WHEN (  COALESCE (trans_claim_amount, 0)
                           + COALESCE (handling_fee, 0)
                           - COALESCE (total_paid_amount, 0)) < 0
                     THEN
                        0
                     ELSE
                        TRUNC (
                             COALESCE (trans_claim_amount, 0)
                           + COALESCE (handling_fee, 0)
                           - COALESCE (total_paid_amount, 0),
                           2)
                  END
            END
         ELSE
            CASE
               WHEN claim_status = 90
               THEN
                  0
               ELSE
                  CASE
                     WHEN (  COALESCE (merchandise_amount, 0)
                           + COALESCE (prop_freight_amount, 0)
                           + COALESCE (recoup_amount, 0)
                           - TRUNC (
                                COALESCE (
                                   salvage_amount,
                                   TRUNC (
                                      COALESCE (merchandise_amount, 0)
                                      * (COALESCE (salvage_percentage, 0)
                                         / 100)),
                                   2),
                                2)
                           - COALESCE (total_paid_amount, 0)
                           - COALESCE (settlement_amount, 0)) < 0
                     THEN
                        0
                     ELSE
                        (TRUNC (
                              COALESCE (merchandise_amount, 0)
                            + COALESCE (prop_freight_amount, 0)
                            + COALESCE (recoup_amount, 0)
                            - COALESCE (
                                 salvage_amount,
                                 (COALESCE (merchandise_amount, 0)
                                  * (COALESCE (salvage_percentage, 0)
                                     / 100)))
                            + .005,
                            2)
                         - COALESCE (total_paid_amount, 0)
                         - COALESCE (settlement_amount, 0))
                  END
            END
      END
         balance_due,
      CASE
         WHEN (   claim_status = 70
               OR claim_status = 80
               OR claim_status = 90)
         THEN
            (TRUNC (last_updated_dttm) - TRUNC (claim_filed_date))
         ELSE
            (TRUNC (SYSDATE) - TRUNC (claim_filed_date))
      END
         days_assigned,
      status_change_date,
      trans_claim_amount,
      created_source_type,
      last_updated_source_type,
      CASE
         WHEN claim_classification = 2
         THEN
            CASE
               WHEN (  COALESCE (trans_claim_amount, 0)
                     + COALESCE (handling_fee, 0)
                     - COALESCE (total_paid_amount, 0)) < 0
               THEN
                  ABS (
                       COALESCE (trans_claim_amount, 0)
                     + COALESCE (handling_fee, 0)
                     - COALESCE (total_paid_amount, 0))
               ELSE
                  NULL
            END
         ELSE
            CASE
               WHEN (  COALESCE (merchandise_amount, 0)
                     + COALESCE (prop_freight_amount, 0)
                     + COALESCE (recoup_amount, 0)
                     - TRUNC (
                          COALESCE (
                             salvage_amount,
                             TRUNC (
                                COALESCE (merchandise_amount, 0)
                                * (COALESCE (salvage_percentage, 0) / 100)),
                             2),
                          2)
                     - COALESCE (total_paid_amount, 0)
                     - COALESCE (settlement_amount, 0)) < 0
               THEN
                  TRUNC (
                     ABS (
                          COALESCE (merchandise_amount, 0)
                        + COALESCE (prop_freight_amount, 0)
                        + COALESCE (recoup_amount, 0)
                        - TRUNC (
                             COALESCE (
                                salvage_amount,
                                TRUNC (
                                   COALESCE (merchandise_amount, 0)
                                   * (COALESCE (salvage_percentage, 0)
                                      / 100)),
                                2),
                             2)
                        - COALESCE (total_paid_amount, 0)
                        - COALESCE (settlement_amount, 0)),
                     2)
               ELSE
                  NULL
            END
      END
         refund_amount  -- formual same as balance_due, but with ABS func.
                      ,
      billing_method,
      inv_id,
      supplier_number,
      bill_of_lading_number,
      parent_claim_id,
      parent_tc_claim_id,
      supplier_bol_number,
      (COALESCE((SELECT DISTINCT CASE WHEN (SELECT COUNT( * ) FROM DOCUMENT_MANAGEMENT ) = 0 THEN 0 ELSE 1 END FROM DOCUMENT_MANAGEMENT
WHERE OBJECT_ID = CLAIM_ID),0))  has_attachments
 FROM claims;

CREATE OR REPLACE FORCE VIEW "CLS_TIMEZONE" ("CLS_TIMEZONE_ID", "WM_VERSION_ID", "TZ_ID", "POPULARITY", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "USES_DST", "RAW_OFFSET") AS
select time_zone_id,
      cast (null as varchar (1)),
      time_zone_name,
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      gmt_offset
 from time_zone;

CREATE OR REPLACE FORCE VIEW "ILM_APPOINTMENT_VIEW" ("APPOINTMENT_ID", "COMPANY_ID", "APPOINTMENTNUMBER", "APPOINTMENTDATETIME", "CARRIER", "CONTROL_ID", "CHECKIN_DATE", "APPT_TYPE", "STATUS", "DOCKTYPE", "DOORTYPE", "COMMENTS", "PURCHASEORDER", "TRAILERID", "TRAILERNUMBER", "TRACTORID", "TRACTORNUMBER", "BILLOFLADING", "SHIPMENTID", "SHIPMENTNUMBER", "SHIPMENT_TYPE", "ORDERNUMBER", "TC_ORDER_ID", "STOP_SEQ", "FACILITY_ID", "BUSINESS_PARTNER_ID", "IS_HAZMAT", "SEAL_NUMBER", "DRIVER_ID", "DRIVER_NAME", "APPT_PRIORITY", "LOADING_TYPE", "TYPE_DESCRIPTION", "STATUS_DESCRIPTION", "PRO_NUMBER") AS
select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      orders.purchase_order_number purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      orders.bill_of_lading_number billoflading,
      -1 shipmentid,
      '' shipmentnumber,
      '' shipment_type,
      orders.order_id ordernumber,
      orders.tc_order_id tc_order_id,
      -1 stop_seq,
      ilm_appointments.facility_id,
      orders.business_partner_id,
      orders.is_hazmat,
      '' seal_number,
      ilm_appointments.driver_id,
      driver.first_name || ' ' || driver.surname,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      cast (null as char) pro_number
 from ilm_appointments
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      join ilm_appointment_objects appt_obj
         on (appt_obj.appointment_id = ilm_appointments.appointment_id
             and appt_obj.company_id = ilm_appointments.company_id
             and appt_obj.appt_obj_type = 40)
      join orders
         on (orders.order_id = appt_obj.appt_obj_id)
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
 union
 select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      shipment.purchase_order purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      stop.bill_of_lading_number billoflading,
      shipment.shipment_id shipmentid,
      shipment.tc_shipment_id shipmentnumber,
      shipment.shipment_type,
      orders.order_id ordernumber,
      orders.tc_order_id tc_order_id,
      stop.stop_seq,
      ilm_appointments.facility_id,
      shipment.business_partner_id,
      shipment.is_hazmat,
      shipment.seal_number,
      ilm_appointments.driver_id,
      driver.first_name || ' ' || driver.surname,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      shipment.pro_number
 from ilm_appointments
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      join ilm_appointment_objects appt_obj
         on (appt_obj.appointment_id = ilm_appointments.appointment_id
             and appt_obj.company_id = ilm_appointments.company_id)
      join shipment
         on (shipment.shipment_id = appt_obj.appt_obj_id
             and appt_obj.appt_obj_type = 30)
      join stop
         on (stop.stop_seq = appt_obj.stop_seq
             and stop.shipment_id = shipment.shipment_id)
      join orders
         on (orders.shipment_id = shipment.shipment_id)
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
 union
 select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      shipment.purchase_order purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      stop.bill_of_lading_number billoflading,
      shipment.shipment_id shipmentid,
      shipment.tc_shipment_id shipmentnumber,
      shipment.shipment_type,
      orders.order_id ordernumber,
      orders.tc_order_id tc_order_id,
      stop.stop_seq,
      ilm_appointments.facility_id,
      shipment.business_partner_id,
      shipment.is_hazmat,
      shipment.seal_number,
      ilm_appointments.driver_id,
      driver.surname || ' ' || driver.first_name,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      shipment.pro_number
 from ilm_appointments
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      join ilm_appointment_objects appt_obj
         on (appt_obj.appointment_id = ilm_appointments.appointment_id
             and appt_obj.company_id = ilm_appointments.company_id)
      join shipment
         on (shipment.shipment_id = appt_obj.appt_obj_id
             and appt_obj.appt_obj_type = 30)
      join stop
         on (stop.stop_seq = appt_obj.stop_seq
             and stop.shipment_id = shipment.shipment_id)
      join order_movement om
         on (om.shipment_id = shipment.shipment_id)
      join orders
         on (orders.order_id = om.order_id)
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
 union
 select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      shipment.purchase_order purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      stop.bill_of_lading_number billoflading,
      shipment.shipment_id shipmentid,
      shipment.tc_shipment_id shipmentnumber,
      shipment.shipment_type,
      0 order_id,
      cast (null as char) tc_order_id,
      stop.stop_seq,
      ilm_appointments.facility_id,
      shipment.business_partner_id,
      shipment.is_hazmat,
      shipment.seal_number,
      ilm_appointments.driver_id,
      driver.surname || ' ' || driver.first_name,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      shipment.pro_number
 from ilm_appointments
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      join ilm_appointment_objects appt_obj
         on (appt_obj.appointment_id = ilm_appointments.appointment_id
             and appt_obj.company_id = ilm_appointments.company_id)
      join shipment
         on (shipment.shipment_id = appt_obj.appt_obj_id
             and appt_obj.appt_obj_type = 30)
      join stop
         on (stop.stop_seq = appt_obj.stop_seq
             and stop.shipment_id = shipment.shipment_id)
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
 union
 select distinct
      ilm_appointments.appointment_id appointment_id,
      ilm_appointments.company_id company_id,
      ilm_appointments.tc_appointment_id appointmentnumber,
      ilm_appointments.load_unload_st_dttm appointmentdatetime,
      ilm_appointments.carrier_id carrier,
      ilm_appointments.control_no control_id,
      ilm_appointments.checkin_dttm checkin_date,
      ilm_appointments.appt_type,
      ilm_appointments.appt_status status,
      ilm_appointments.door_type_id docktype,
      ilm_appointments.dock_door_id doortype,
      ilm_appointments.comments,
      cast (null as char) purchaseorder,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailerid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRAILER')
       end)
         trailernumber,
      (case
          when equipment_instance.equipment_instance_id is not null
          then
             equipment_instance.equipment_instance_id
          else
             (select max (equipment_instance_id)
                from ilm_appt_equipments_view
               where ilm_appt_equipments_view.appointment_id =
                        ilm_appointments.appointment_id
                     and ilm_appt_equipments_view.company_id =
                            ilm_appointments.company_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractorid,
      (case
          when equipment_instance.equip_inst_ref_carrier is not null
          then
             equipment_instance.equip_inst_ref_carrier
          else
             (select max (equip_inst_ref_carrier)
                from equipment_instance, ilm_appt_equipments_view
               where equipment_instance.equipment_instance_id =
                        ilm_appt_equipments_view.equipment_instance_id
                     and equipment_instance.tc_company_id =
                            ilm_appt_equipments_view.company_id
                     and ilm_appt_equipments_view.appointment_id =
                            ilm_appointments.appointment_id
                     and upper (ilm_appt_equipments_view.equipment_type) =
                            'TRACTOR')
       end)
         tractornumber,
      cast (null as char) billoflading,
      cast (null as integer) shipmentid,
      cast (null as char) shipmentnumber,
      cast (null as char) shipmenttype,
      0,
      cast (null as char) tc_order_id,
      cast (null as smallint) stopseq,
      ilm_appointments.facility_id,
      cast (null as char) business_partner_id,
      cast (null as smallint) is_hazmat,
      cast (null as char) seal_number,
      ilm_appointments.driver_id,
      driver.first_name || ' ' || driver.surname,
      ilm_appointments.appt_priority,
      ilm_appointments.loading_type,
      ilm_appointment_type.description as type_description,
      ilm_appointment_status.description as status_description,
      cast (null as char) pro_number
 from ilm_appointments
      join ilm_appointment_type
         on (ilm_appointment_type.appt_type = ilm_appointments.appt_type)
      join ilm_appointment_status
         on (ilm_appointment_status.appt_status_code =
                ilm_appointments.appt_status)
      left outer join equipment_instance
         on (equipment_instance.current_appointment_id =
                ilm_appointments.appointment_id
             and equipment_instance.tc_company_id =
                    ilm_appointments.company_id)
      left outer join driver
         on (driver.driver_id = ilm_appointments.driver_id)
where appointment_id not in
         (select appointment_id from ilm_appointment_objects);

CREATE OR REPLACE FORCE VIEW "ILM_APPT_EQUIPMENTS_VIEW" ("APPOINTMENT_ID", "COMPANY_ID", "EQUIPMENT_INSTANCE_ID", "EQUIPMENT_TYPE") AS
select appointment_id,
      company_id,
      equipment_instance_id,
      getequiptype (equipment_instance_id)
 from ilm_appt_equipments;

CREATE OR REPLACE FORCE VIEW "ILM_APPT_TRAILER" ("APPOINTMENT_ID", "COMPANY_ID", "CARRIERID", "TRAILERNUMBER", "TRAILERID") AS
select distinct ilm_appointments.appointment_id,
               ilm_appointments.company_id,
               equipment_instance.carrier_id carrierid,
               equip_inst_ref_carrier trailernumber,
               ilm_appt_equipments.equipment_instance_id trailerid
 from equipment_instance,
      ilm_appt_equipments,
      ilm_appointments,
      equipment
where ilm_appt_equipments.appointment_id =
         ilm_appointments.appointment_id
      and ilm_appt_equipments.company_id = ilm_appointments.company_id
      and equipment_instance.equipment_instance_id =
             ilm_appt_equipments.equipment_instance_id
      and equipment_instance.equipment_id = equipment.equipment_id
      and equipment.equipment_type = 16
      and not exists
                 (select 1
                    from ilm_yard_activity iya
                   where iya.appointment_id =
                            ilm_appointments.appointment_id
                         and activity_type = 50)
 union
 select ilm_appointments.appointment_id,
      ilm_appointments.company_id,
      equipment_instance.carrier_id carrierid,
      equip_inst_ref_carrier trailernumber,
      equipment_instance.equipment_instance_id trailerid
 from ilm_yard_activity iya, ilm_appointments, equipment_instance
where     iya.appointment_id = ilm_appointments.appointment_id
      and iya.equipment_id1 = equipment_instance.equipment_instance_id
      and ilm_appointments.company_id = tc_company_id
      and activity_dttm =
             (select max (activity_dttm)
                from ilm_yard_activity a
               where a.appointment_id = iya.appointment_id
                     and a.activity_type = 50)
      and activity_type = 50;

CREATE OR REPLACE FORCE VIEW "ILM_BILL_SEALNUMBER_VIEW" ("APPOINTMENT_ID", "COMPANY_ID", "BILL_OF_LADING", "SEAL_NUMBER", "BUSINESS_PARTNER_ID") AS
select distinct ilm_appointments.appointment_id appointment_id,
               ilm_appointments.company_id company_id,
               orders.bill_of_lading_number bill_of_lading,
               '' seal_number,
               orders.business_partner_id business_partner_id
 from ilm_appointments, ilm_appointment_objects appt_obj, orders
where     appt_obj.appointment_id = ilm_appointments.appointment_id
      and appt_obj.company_id = ilm_appointments.company_id
      and appt_obj.appt_obj_type = 40
      and orders.order_id = appt_obj.appt_obj_id
 union
 select distinct ilm_appointments.appointment_id appointment_id,
               ilm_appointments.company_id company_id,
               stop.bill_of_lading_number bill_of_lading,
               shipment.seal_number seal_number,
               shipment.business_partner_id
 from ilm_appointments,
      ilm_appointment_objects appt_obj,
      shipment,
      stop
where     appt_obj.appointment_id = ilm_appointments.appointment_id
      and appt_obj.company_id = ilm_appointments.company_id
      and appt_obj.appt_obj_type = 30
      and stop.stop_seq = appt_obj.stop_seq
      and stop.shipment_id = shipment.shipment_id
      and shipment.shipment_id = appt_obj.appt_obj_id;

CREATE OR REPLACE FORCE VIEW "ILM_LOCATION_VIEW" ("TYPE", "LOCATION_ID", "SEQUENCE_NO", "CURRENT_QUANTITY", "DEFINED_SVG", "ZONE_ID", "ZONE_NAME", "STATUS", "TC_COMPANY_ID", "PARENT_LOC") AS
select /*+ ALL_ROWS */ 'YZN',
      ln.location_id,
      ln.sequence_no,
      current_quantity,
      ln.is_location_defined_in_svg,
      null,
      yard_zone_name,
      null,
      ln.tc_company_id,
      null
 from yard_zone yz, location ln
where     ln.location_objid_pk1 = to_char (yz.yard_id)
      and ln.location_objid_pk2 = to_char (yz.yard_zone_id)
      and ln.ilm_object_type = 28
 union all
 select /*+ ALL_ROWS */ 'DDR',
      ln.location_id,
      ln.sequence_no,
      current_quantity,
      ln.is_location_defined_in_svg,
      null,
      dock_door_name,
      dock_door_status,
      ln.tc_company_id,
      dock_id
 from dock_door dd, location ln
where     ln.location_objid_pk1 = to_char (dd.facility_id)
      and ln.location_objid_pk2 = dd.dock_id
      and ln.location_objid_pk3 = to_char (dd.dock_door_id)
      and ln.tc_company_id = dd.tc_company_id
      and ln.ilm_object_type = 8
 union all
 select /*+ ALL_ROWS */ 'YZS',
      ln.location_id,
      ln.sequence_no,
      current_quantity,
      ln.is_location_defined_in_svg,
      yard_zone_slot_id,
      yard_zone_slot_name,
      yard_zone_slot_status,
      ln.tc_company_id,
      y.yard_zone_name
 from yard_zone_slot yz, location ln, yard_zone y
where     ln.location_objid_pk1 = to_char (yz.yard_id)
      and ln.location_objid_pk2 = to_char (yz.yard_zone_id)
      and ln.location_objid_pk3 = to_char (yz.yard_zone_slot_id)
      and ln.ilm_object_type = 32
      and y.yard_zone_id = yz.yard_zone_id
 union all
 select /*+ ALL_ROWS */ 'DCK',
      ln.location_id,
      ln.sequence_no,
      current_quantity,
      ln.is_location_defined_in_svg,
      null,
      dock_id,
      null,
      ln.tc_company_id,
      null
 from dock dck, location ln
where     ln.location_objid_pk1 = to_char (dck.facility_id)
      and ln.location_objid_pk2 = dck.dock_id
      and ln.ilm_object_type = 4;

CREATE OR REPLACE FORCE VIEW "ILM_TASKS_DEST_LOCATION_VIEW" ("LOCATION_ID", "TASK_DETAILS") AS
select getval (tsk.task_details, 'DEST_LOCATION') as location_id,
      tsk.task_details
 from ilm_tasks tsk
where tsk.task_type = 40 and tsk.status in (20, 30, 40, 50)
 with read only;

CREATE OR REPLACE FORCE VIEW "ILM_TASKS_TRAILER_VIEW" ("TRAILER", "TASK_STATUS") AS
select getval (tsk.task_details, 'TRAILER') as trailer,
      description as task_status
 from ilm_tasks tsk, ilm_task_status tss
where     tsk.status >= 10
      and tsk.status < 60
      and tsk.task_details like '%TRAILER=%'
      and tsk.status = tss.task_status
 with read only;

CREATE OR REPLACE FORCE VIEW "ILM_TRAILER_VIEW" ("LOCKED", "TRAILER_ID", "TRAILER_NAME", "TRAILER_LOC", "EQUIPMENT_STATUS", "CARRIER_CODE", "APPOINTMENT_ID", "TC_APPOINTMENT_ID", "APPT_TYPE", "APPT_STATUS", "TRAILER_STATUS", "FACILITY_ID", "FACILITY_ALIAS", "LOCATION", "PURCHASE_ORDER_ID", "PURCHASE_ORDER", "VENDOR_ID", "SHIPMENT_ID", "SHIPMENT", "BILL_OF_LADING", "ASN_ID", "ASN", "LOAD_CONFIG", "YARD", "PARENT_LOCATION", "LOCATION_ID", "CARRIER_CODE_NAME", "IS_HAZMAT", "LICENSE_TAG_NUMBER", "LICENSE_STATE_PROV", "EQUIPMENT_ID", "CONDITION_TYPE", "EQUIP_DOOR_TYPE", "SEAL_NUMBER", "BEEPER_NUMBER", "APPT_PRIORITY", "ASSIGNED_LOCATION", "CHECKIN_DTTM", "FOB_INDICATOR", "COMPANY_ID", "CURRENT_LOCN", "ASSIGNED_LOCN") AS
SELECT EQUIPMENT_INSTANCE.IS_LOCKED,
 EQUIPMENT_INSTANCE.EQUIPMENT_INSTANCE_ID,
 EQUIPMENT_INSTANCE.EQUIP_INST_REF_CARRIER,
 EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID,
 EQUIPMENT_INSTANCE.EQUIP_INST_STATUS,
 CARRIER_CODE.CARRIER_CODE,
 IL.APPOINTMENT_ID,
 IL.TC_APPOINTMENT_ID,
 IL.APPT_TYPE,
 IL.APPT_STATUS,
 ILM_STATUS.DESCRIPTION,
 IL.FACILITY_ID,
 FACILITY_ALIAS.FACILITY_ALIAS_ID,
 FN_GET_LOC_CODE(EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID) LOC,
 (SELECT MAX(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 40
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) PO_ID,
 (SELECT (case
 when count(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID) > 1 then
 'Pos'
 else
 max(PURCHASE_ORDERS.TC_PURCHASE_ORDERS_ID)
 end)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, PURCHASE_ORDERS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = PURCHASE_ORDERS.PURCHASE_ORDERS_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 40
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) PO,
 (SELECT MAX(PURCHASE_ORDERS.BUSINESS_PARTNER_ID)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, PURCHASE_ORDERS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = PURCHASE_ORDERS.PURCHASE_ORDERS_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 40
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) BP,
 (SELECT MAX(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 30
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) SHIPMENT_ID,
 (SELECT (case
 when count(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID) > 1 then
 'Shipments'
 else
 max(SHIPMENT.tc_shipment_id)
 end)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, SHIPMENT
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = SHIPMENT.SHIPMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 30
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) SHIPMENT,
 (SELECT MAX(SHIPMENT.BILL_OF_LADING_NUMBER)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, SHIPMENT
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = SHIPMENT.SHIPMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 30
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) BILLOFLADING,
 (SELECT MAX(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 10
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) ASN_ID,
 (SELECT (case
 when count(ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID) > 1 then
 'ASNs'
 else
 max(ASN.TC_ASN_ID)
 end)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, ASN
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = ASN.ASN_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 10
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) ASN,
 (SELECT MAX(ILM_APPT_LOAD_CONFIG.LOAD_CONFIG_ID)
 FROM ILM_APPT_LOAD_CONFIG, ILM_APPOINTMENTS
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPT_LOAD_CONFIG.APPOINTMENT_ID
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) LOADCONFIG,
 (SELECT YARD.YARD_NAME
 FROM YARD, YARD_ZONE_SLOT
 WHERE YARD_ZONE_SLOT.YARD_ID = YARD.YARD_ID
 AND YARD_ZONE_SLOT.YARD_ZONE_SLOT_ID=LOCATION.LOCATION_OBJID_PK3
 and LOCATION.ILM_OBJECT_TYPE= 32) YARD,
 (SELECT PARENT_LOC
 FROM ILM_LOCATION_VIEW
 WHERE LOCATION_ID = EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID) PARENT_LOC,
 LOCATION.LOCATION_ID,
 CARRIER_CODE.CARRIER_CODE_NAME,
 (SELECT MAX(SHIPMENT.IS_HAZMAT)
 FROM ILM_APPOINTMENT_OBJECTS, ILM_APPOINTMENTS, SHIPMENT
 WHERE ILM_APPOINTMENTS.APPOINTMENT_ID =
 ILM_APPOINTMENT_OBJECTS.APPOINTMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_ID = SHIPMENT.SHIPMENT_ID
 AND ILM_APPOINTMENT_OBJECTS.APPT_OBJ_TYPE = 30
 AND ILM_APPOINTMENTS.APPOINTMENT_ID = IL.APPOINTMENT_ID) IS_HAZMAT,
 EQUIPMENT_INSTANCE.LICENSE_TAG_NUMBER,
 EQUIPMENT_INSTANCE.LICENSE_STATE_PROV,
 EQUIPMENT_INSTANCE.EQUIPMENT_ID,
 EQUIPMENT_INSTANCE.CONDITION_TYPE,
 EQUIPMENT_INSTANCE.EQUIP_DOOR_TYPE,
 EQUIPMENT_INSTANCE.SEAL_NUMBER,
 IL.BEEPER_NUMBER,
 IL.APPT_PRIORITY,
 FN_GET_LOC_CODE(EQUIPMENT_INSTANCE.ASSIGNED_LOCATION_ID) ASSIGNED_LOCATION,
 IL.CHECKIN_DTTM,
 Equipment_Instance.Fob_Indicator,
 Il.Company_Id,
 (SELECT ZONE_NAME
 From Ilm_Location_View
 WHERE LOCATION_ID = EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID) current_locn,
 (SELECT ZONE_NAME
 FROM ILM_LOCATION_VIEW
 Where Location_Id = Equipment_Instance.Assigned_Location_Id) assigned_Locn
FROM EQUIPMENT_INSTANCE,
 ILM_APPOINTMENTS IL,
 CARRIER_CODE,
 ILM_STATUS,
 FACILITY_ALIAS,
 LOCATION
 WHERE EQUIPMENT_INSTANCE.CURRENT_APPOINTMENT_ID = IL.APPOINTMENT_ID
 AND EQUIPMENT_INSTANCE.EQUIP_INST_STATUS = ILM_STATUS.ILM_STATUS
 AND IL.APPT_STATUS IN (5, 6, 7, 8, 11, 12)
 AND EQUIPMENT_INSTANCE.CARRIER_ID = CARRIER_CODE.CARRIER_ID
 AND FACILITY_ALIAS.FACILITY_ID = IL.FACILITY_ID
 AND FACILITY_ALIAS.IS_PRIMARY = 1
 AND LOCATION.LOCATION_ID = EQUIPMENT_INSTANCE.CURRENT_LOCATION_ID;

CREATE OR REPLACE FORCE VIEW "IMPORT_COMB_LANE" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "RG_QUALIFIER", "FREQUENCY", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_NAME", "CUSTOMER_ID", "BILLING_METHOD", "INCOTERM_ID", "ROUTE_TO", "ROUTE_TYPE_1", "ROUTE_TYPE_2", "NO_RATING", "USE_FASTEST", "USE_PREFERENCE_BONUS", "IS_ROUTING", "IS_RATING", "IS_SAILING") AS
SELECT il.tc_company_id,
il.lane_id,
il.lane_hierarchy,
il.lane_status,
il.o_loc_type,
il.o_facility_id,
il.o_facility_alias_id,
UPPER (il.o_city),
il.o_state_prov,
UPPER (il.o_county),
il.o_postal_code,
il.o_country_code,
il.o_zone_id,
il.o_zone_name,
il.d_loc_type,
il.d_facility_id,
il.d_facility_alias_id,
UPPER (il.d_city),
il.d_state_prov,
UPPER (il.d_county),
il.d_postal_code,
il.d_country_code,
il.d_zone_id,
il.d_zone_name,
il.rg_qualifier,
il.frequency,
DECODE (il.lane_status, 4, 1, 0) has_errors,
COALESCE (
(SELECT MAX (DECODE (d.lane_dtl_status, 4, 1, 0))
FROM import_rg_lane_dtl d
WHERE d.lane_id     = il.lane_id
AND d.tc_company_id = il.tc_company_id
), 0) dtl_has_errors,
il.created_source_type,
il.created_source,
il.created_dttm,
il.last_updated_source_type,
il.last_updated_source,
il.last_updated_dttm,
il.lane_name,
il.customer_id,
il.billing_method,
il.incoterm_id,
il.route_to,
il.route_type_1,
il.route_type_2,
il.no_rating,
il.use_fastest,
il.use_preference_bonus,
1,
0,
0
FROM import_rg_lane il
/*AND NOT EXISTS
(SELECT 1
FROM comb_lane cl
WHERE il.tc_company_id = cl.tc_company_id
AND il.lane_id      = cl.lane_id
AND cl.is_routing      = 1
)*/
UNION ALL
SELECT I.TC_COMPANY_ID,
I.LANE_ID,
I.LANE_HIERARCHY,
I.LANE_STATUS,
I.O_LOC_TYPE,
I.O_FACILITY_ID,
I.O_FACILITY_ALIAS_ID,
UPPER (I.O_CITY),
I.O_STATE_PROV,
UPPER (I.O_COUNTY),
I.O_POSTAL_CODE,
I.O_COUNTRY_CODE,
I.O_ZONE_ID,
I.O_ZONE_NAME,
I.D_LOC_TYPE,
I.D_FACILITY_ID,
I.D_FACILITY_ALIAS_ID,
UPPER (I.D_CITY),
I.D_STATE_PROV,
UPPER (I.D_COUNTY),
I.D_POSTAL_CODE,
I.D_COUNTRY_CODE,
I.D_ZONE_ID,
I.D_ZONE_NAME,
'',
'',
DECODE (CAST (I.LANE_STATUS AS INT), 4, 1, 0) HAS_ERRORS,
COALESCE (
(SELECT MAX (DECODE (CAST (D.LANE_DTL_STATUS AS INT), 4, 1, 0))
FROM IMPORT_SAILING_LANE_DTL D
WHERE D.LANE_ID     = I.LANE_ID
AND D.TC_COMPANY_ID = I.TC_COMPANY_ID
), 0) DTL_HAS_ERRORS,
I.CREATED_SOURCE_TYPE,
I.CREATED_SOURCE,
I.CREATED_DTTM,
I.LAST_UPDATED_SOURCE_TYPE,
I.LAST_UPDATED_SOURCE,
I.LAST_UPDATED_DTTM,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
0, --routing
0, --rating
1  --sailing
FROM import_SAILING_LANE I
/*AND NOT EXISTS
(SELECT 1
FROM COMB_LANE CL
WHERE I.TC_COMPANY_ID = CL.TC_COMPANY_ID
AND I.LANE_ID = CL.LANE_ID
AND CL.is_SAILING     = 1
)*/
UNION ALL
SELECT i.tc_company_id,
i.lane_id,
i.lane_hierarchy,
i.lane_status,
i.o_loc_type,
i.o_facility_id,
i.o_facility_alias_id,
UPPER (i.o_city),
i.o_state_prov,
UPPER (i.o_county),
i.o_postal_code,
i.o_country_code,
I.O_ZONE_ID,
I.O_ZONE_NAME,
i.d_loc_type,
i.d_facility_id,
i.d_facility_alias_id,
UPPER (i.d_city),
i.d_state_prov,
UPPER (i.d_county),
i.d_postal_code,
i.d_country_code,
I.D_ZONE_ID,
I.D_ZONE_NAME,
'',
'',
DECODE (i.lane_status, 4, 1, 0) has_errors,
COALESCE (
(SELECT MAX (DECODE (d.rating_lane_dtl_status, 4, 1, 0))
FROM import_rating_lane_dtl d
WHERE d.lane_id     = i.lane_id
AND d.tc_company_id = i.tc_company_id
), 0) dtl_has_errors,
i.created_source_type,
i.created_source,
i.created_dttm,
i.last_updated_source_type,
i.last_updated_source,
i.last_updated_dttm,
'',
CAST (NULL AS INT),
CAST (NULL AS INT),
CAST (NULL AS INT),
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
0, --routing
1, --rating
0  --sailing
FROM import_rating_lane i
  /*AND NOT EXISTS
(SELECT 1
FROM COMB_LANE CL
WHERE i.tc_company_id = CL.tc_company_id
AND i.lane_id  = CL.lane_id
AND CL.IS_RATING      = 1
);*/
;

CREATE OR REPLACE FORCE VIEW "IMPORT_COMB_LANE_DTL" ("TC_COMPANY_ID", "LANE_ID", "LANE_DTL_STATUS", "CARRIER_ID", "MOT_ID", "EQUIPMENT_ID", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL_ID", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "EFFECTIVE_DT", "EXPIRATION_DT", "PACKAGE_NAME", "SAILING_SCHEDULE_NAME", "SP_MIN_LPN_COUNT", "SP_MAX_LPN_COUNT", "SP_MIN_WEIGHT", "SP_MAX_WEIGHT", "SP_MIN_VOLUME", "SP_MAX_VOLUME", "SP_MIN_LINEAR_FEET", "SP_MAX_LINEAR_FEET", "SP_MIN_MONETARY_VALUE", "SP_MAX_MONETARY_VALUE", "OVERRIDE_CODE", "CUBING_INDICATOR") AS
select  rating_dtl.tc_company_id, rating_dtl.lane_id,rating_dtl.import_rating_dtl_status, rating_dtl.carrier_id, rating_dtl.mot_id, rating_dtl.equipment_id, rating_dtl.service_level_id, rating_dtl.protection_level_id, rating_dtl.o_ship_via, rating_dtl.d_ship_via,rating_dtl.contract_number, rating_dtl.effective_dt,
rating_dtl.expiration_dt, rating_dtl.package_name, null as sailing_schedule_name, null as SP_MIN_LPN_COUNT, null as  SP_MAX_LPN_COUNT, null as  SP_MIN_WEIGHT, null as  SP_MAX_WEIGHT, null as  SP_MIN_VOLUME, null as  SP_MAX_VOLUME, null as SP_MIN_LINEAR_FEET, null as  SP_MAX_LINEAR_FEET, null as
SP_MIN_MONETARY_VALUE, null as SP_MAX_MONETARY_VALUE, null as  OVERRIDE_CODE, null as  CUBING_INDICATOR from import_rating_lane_dtl  rating_dtl
UNION ALL
select  rg_dtl.tc_company_id, rg_dtl.lane_id,rg_dtl.import_rg_dtl_status, rg_dtl.carrier_id, rg_dtl.mot_id, rg_dtl.equipment_id, rg_dtl.service_level_id, rg_dtl.protection_level_id, null as o_ship_via, null as d_ship_via, null as contract_number, rg_dtl.effective_dt,
rg_dtl.expiration_dt, rg_dtl.package_name, null as sailing_schedule_name, SP_MIN_LPN_COUNT, SP_MAX_LPN_COUNT, SP_MIN_WEIGHT, SP_MAX_WEIGHT, SP_MIN_VOLUME, SP_MAX_VOLUME,SP_MIN_LINEAR_FEET, SP_MAX_LINEAR_FEET,
SP_MIN_MONETARY_VALUE,SP_MAX_MONETARY_VALUE, OVERRIDE_CODE, CUBING_INDICATOR from import_rg_lane_dtl rg_dtl
UNION ALL
select  sailing_dtl.tc_company_id, sailing_dtl.lane_id, sailing_dtl.lane_dtl_status , sailing_dtl.carrier_id, sailing_dtl.mot_id,  null as equipment_id, sailing_dtl.service_level_id,  null as protection_level_id, null as o_ship_via, null as d_ship_via, null as contract_number, sailing_dtl.effective_dt,sailing_dtl.expiration_dt, null as package_name,sailing_dtl.sailing_schedule_name, null as SP_MIN_LPN_COUNT, null as  SP_MAX_LPN_COUNT, null as  SP_MIN_WEIGHT, null as  SP_MAX_WEIGHT, null as  SP_MIN_VOLUME, null as  SP_MAX_VOLUME, null as SP_MIN_LINEAR_FEET, null as  SP_MAX_LINEAR_FEET, null as
SP_MIN_MONETARY_VALUE, null as SP_MAX_MONETARY_VALUE, null as  OVERRIDE_CODE, null as  CUBING_INDICATOR from import_sailing_lane_dtl sailing_dtl;

CREATE OR REPLACE FORCE VIEW "ITEM_MASTER" ("ITEM_ID", "SEASON", "SEASON_YR", "STYLE", "STYLE_SFX", "COLOR", "COLOR_SFX", "SEC_DIM", "QUAL", "SIZE_DESC", "SKU_BRCD", "SKU_DESC", "DSP_SKU", "COLOR_DESC", "STAB_CODE", "ITEM_ORIENTATION", "PROTN_FACTOR", "CAVITY_LEN", "CAVITY_WD", "CAVITY_HT", "INCREMENTAL_LEN", "INCREMENTAL_WD", "INCREMENTAL_HT", "STACKABLE_ITEM", "MAX_NEST_NUMBER", "UNIT_HT", "UNIT_LEN", "UNIT_WIDTH", "UNIT_WT", "UNIT_VOL", "COMMODITY_LEVEL_DESC", "SKU_IMAGE_FILENAME", "STAT_CODE", "CATCH_WT", "CD_MASTER_ID", "ITEM_MASTER_ID", "CREATE_DATE_TIME", "MOD_DATE_TIME", "WM_VERSION_ID", "FTZ_HDR_ID", "UPC_PRE_DIGIT", "UPC_VENDOR_CODE", "UPC_SRL_PROD_NBR", "UPC_POST_DIGIT", "SKU_ID", "NMFC_CODE", "SIZE_RANGE_CODE", "SIZE_REL_POSN_IN_TABLE", "VOLTY_CODE", "PKG_TYPE", "PROD_SUB_GRP", "PROD_TYPE", "PROD_LINE", "SALE_GRP", "COORD_1", "COORD_2", "CARTON_TYPE", "UNIT_PRICE", "RETAIL_PRICE", "OPER_CODE", "MAX_CASE_QTY", "CUBE_MULT_QTY", "NEST_VOL", "NEST_CNT", "UNITS_PER_PICK_ACTIVE", "HNDL_ATTR_ACTIVE", "UNITS_PER_PICK_CASE_PICK", "HNDL_ATTR_CASE_PICK", "UNITS_PER_PICK_RESV", "HNDL_ATTR_RESV", "PROD_LIFE_IN_DAY", "MAX_RECV_TO_XPIRE_DAYS", "AVG_DLY_DMND", "WT_TOL_PCNT", "CONS_PRTY_DATE_CODE", "CONS_PRTY_DATE_WINDOW", "CONS_PRTY_DATE_WINDOW_INCR", "ACTVTN_DATE", "ALLOW_RCPT_OLDER_SKU", "CRITCL_DIM_1", "CRITCL_DIM_2", 
"CRITCL_DIM_3", "MFG_DATE_REQD", "XPIRE_DATE_REQD", "SHIP_BY_DATE_REQD", "SKU_ATTR_REQD", "BATCH_REQD", "PROD_STAT_REQD", "CNTRY_OF_ORGN_REQD", "VENDOR_ITEM_NBR", "PICK_WT_TOL_TYPE", "PICK_WT_TOL_AMNT", "MHE_WT_TOL_TYPE", "MHE_WT_TOL_AMNT", "LOAD_ATTR", "TEMP_ZONE", "TRLR_TEMP_ZONE", "PKT_CONSOL_ATTR", "BUYER_DISP_CODE", "CRUSH_CODE", "CONVEY_FLAG", "STORE_DEPT", "MERCH_TYPE", "MERCH_GROUP", "SPL_INSTR_CODE_1", "SPL_INSTR_CODE_2", "SPL_INSTR_CODE_3", "SPL_INSTR_CODE_4", "SPL_INSTR_CODE_5", "SPL_INSTR_CODE_6", "SPL_INSTR_CODE_7", "SPL_INSTR_CODE_8", "SPL_INSTR_CODE_9", "SPL_INSTR_CODE_10", "SPL_INSTR_1", "SPL_INSTR_2", "PROMPT_FOR_VENDOR_ITEM_NBR", "PROMPT_PACK_QTY", "ECCN_NBR", "EXP_LICN_NBR", "EXP_LICN_XP_DATE", "EXP_LICN_SYMBOL", "ORGN_CERT_CODE", "ITAR_EXEMPT_NBR", "FRT_CLASS", "DFLT_BATCH_STAT", "DFLT_INCUB_LOCK", "BASE_INCUB_FLAG", "INCUB_DAYS", "INCUB_HOURS", "SRL_NBR_BRCD_TYPE", "MINOR_SRL_NBR_REQ", "DUP_SRL_NBR_FLAG", "MAX_RCPT_QTY", "FTZ_FLAG", "HTS_NBR", "CC_UNIT_TOLER_VALUE", "CC_WGT_TOLER_VALUE", "CC_DLR_TOLER_VALUE", "CC_PCNT_TOLER_VALUE", "VOCOLLECT_BASE_WT", "VOCOLLECT_BASE_QTY", "VOCOLLECT_BASE_ITEM", "PICK_WT_TOL_AMNT_ERROR", "PRICE_TKT_TYPE", "MONETARY_VALUE", "MV_CURRENCY_CODE", "MV_SIZE_UOM", "CODE_DATE_PROMPT_METHOD_FLAG", "MIN_RECV_TO_XPIRE_DAYS", "MIN_PCNT_FOR_LPN_SPLIT", "MIN_LPN_QTY_FOR_SPLIT", 
"PROD_CATGRY", "PRICE_TIX_AVAIL_FLAG", "USER_ID", "UN_NBR", "HAZMAT_ID", "PROD_GROUP", "COMMODITY_CODE", "DSP_QTY_UOM", "DB_QTY_UOM", "STD_CASE_QTY", "STD_CASE_WT", "STD_CASE_VOL", "STD_CASE_LEN", "STD_CASE_WIDTH", "STD_CASE_HT", "STD_PACK_QTY", "STD_PACK_WT", "STD_PACK_VOL", "STD_PACK_LEN", "STD_PACK_WIDTH", "STD_PACK_HT", "STD_SUB_PACK_QTY", "STD_SUB_PACK_WT", "STD_SUB_PACK_VOL", "STD_SUB_PACK_LEN", "STD_SUB_PACK_WIDTH", "STD_SUB_PACK_HT", "STD_TIER_QTY", "STD_BUNDL_QTY", "STD_PLT_QTY", "MARKS_NBRS", "NET_COST_FLAG", "PRODUCER_FLAG", "PREF_CRIT_FLAG", "SRL_NBR_REQD") AS
(SELECT ITEM_CBO.ITEM_ID,
       ITEM_CBO.ITEM_SEASON AS SEASON,
       ITEM_CBO.ITEM_SEASON_YEAR AS SEASON_YR,
       ITEM_CBO.ITEM_STYLE AS STYLE,
       ITEM_CBO.ITEM_STYLE_SFX AS STYLE_SFX,
       ITEM_CBO.ITEM_COLOR AS COLOR,
       ITEM_CBO.ITEM_COLOR_SFX AS COLOR_SFX,
       ITEM_CBO.ITEM_SECOND_DIM AS SEC_DIM,
       ITEM_CBO.ITEM_QUALITY AS QUAL,
       ITEM_CBO.ITEM_SIZE_DESC AS SIZE_DESC,
       ITEM_CBO.ITEM_BAR_CODE AS SKU_BRCD,
       ITEM_CBO.DESCRIPTION AS SKU_DESC,
       ITEM_CBO.ITEM_NAME AS DSP_SKU,
       ITEM_CBO.COLOR_DESC AS COLOR_DESC,
       ITEM_CBO.STAB_CODE AS STAB_CODE,
       '0' AS ITEM_ORIENTATION,
       ITEM_CBO.PROTN_FACTOR AS PROTN_FACTOR,
       CAST (0 AS DECIMAL (4, 2)) AS CAVITY_LEN,
       CAST (0 AS DECIMAL (4, 2)) AS CAVITY_WD,
       CAST (0 AS DECIMAL (4, 2)) AS CAVITY_HT,
       CAST (0 AS DECIMAL (4, 2)) AS INCREMENTAL_LEN,
       CAST (0 AS DECIMAL (4, 2)) AS INCREMENTAL_WD,
       CAST (0 AS DECIMAL (4, 2)) AS INCREMENTAL_HT,
       'N' AS STACKABLE_ITEM,
       0 AS MAX_NEST_NUMBER,
       CAST (COALESCE (ITEM_CBO.UNIT_HEIGHT, 0) AS DECIMAL (16, 4))
          AS UNIT_HT,
       CAST (COALESCE (ITEM_CBO.UNIT_LENGTH, 0) AS DECIMAL (16, 4))
          AS UNIT_LEN,
       CAST (COALESCE (ITEM_CBO.UNIT_WIDTH, 0) AS DECIMAL (16, 4))
          AS UNIT_WIDTH,
       CAST (COALESCE (ITEM_CBO.UNIT_WEIGHT, 0) AS DECIMAL (16, 4))
          AS UNIT_WT,
       CAST (COALESCE (ITEM_CBO.UNIT_VOLUME, 0) AS DECIMAL (16, 4))
          AS UNIT_VOL,
       ITEM_CBO.COMMODITY_LEVEL_DESC AS COMMODITY_LEVEL_DESC,
       ITEM_CBO.ITEM_IMAGE_FILENAME AS SKU_IMAGE_FILENAME,
       CAST (COALESCE (ITEM_CBO.STATUS_CODE, 0) AS SMALLINT) AS STAT_CODE,
       CAST (ITEM_CBO.CATCH_WEIGHT_ITEM AS VARCHAR (4)) AS CATCH_WT,
       ITEM_CBO.COMPANY_ID AS CD_MASTER_ID,
       ITEM_CBO.ITEM_ID AS ITEM_MASTER_ID,
       ITEM_CBO.AUDIT_CREATED_DTTM AS CREATE_DATE_TIME,
       ITEM_CBO.AUDIT_LAST_UPDATED_DTTM AS MOD_DATE_TIME,
       1 AS WM_VERSION_ID,
       1 AS FTZ_HDR_ID,
       ITEM_CBO.ITEM_UPC_GTIN AS UPC_PRE_DIGIT,
       '' AS UPC_VENDOR_CODE,
       '' AS UPC_SRL_PROD_NBR,
       '' AS UPC_POST_DIGIT,
       ITEM_CBO.ITEM_NAME AS SKU_ID,
       ITEM_WMS.NMFC_CODE AS NMFC_CODE,
       ITEM_WMS.SIZE_RANGE_CODE AS SIZE_RANGE_CODE,
       ITEM_WMS.SIZE_REL_POSN_IN_TABLE AS SIZE_REL_POSN_IN_TABLE,
       ITEM_WMS.VOLTY_CODE AS VOLTY_CODE,
       ITEM_WMS.PKG_TYPE AS PKG_TYPE,
       ITEM_WMS.PROD_SUB_GRP AS PROD_SUB_GRP,
       ITEM_CBO.PROD_TYPE AS PROD_TYPE,
       ITEM_WMS.PROD_LINE AS PROD_LINE,
       ITEM_WMS.SALE_GRP AS SALE_GRP,
       CAST (COALESCE (ITEM_WMS.COORD_1, 0) AS INTEGER) AS COORD_1,
       CAST (COALESCE (ITEM_WMS.COORD_2, 0) AS INTEGER) AS COORD_2,
       ITEM_WMS.CARTON_TYPE AS CARTON_TYPE,
       CAST (COALESCE (ITEM_WMS.UNIT_PRICE, 0) AS DECIMAL (9, 2))
          AS UNIT_PRICE,
       CAST (COALESCE (ITEM_WMS.RETAIL_PRICE, 0) AS DECIMAL (9, 2))
          AS RETAIL_PRICE,
       ITEM_WMS.OPER_CODE AS OPER_CODE,
       CAST (COALESCE (ITEM_WMS.MAX_CASE_QTY, 0) AS DECIMAL (9, 2))
          AS MAX_CASE_QTY,
       CAST (COALESCE (ITEM_WMS.CUBE_MULT_QTY, 0) AS DECIMAL (9, 2))
          AS CUBE_MULT_QTY,
       CAST (ITEM_WMS.NEST_VOL AS DECIMAL (13, 4)) AS NEST_VOL,
       CAST (ITEM_WMS.NEST_CNT AS INTEGER) AS NEST_CNT,
       CAST (
          COALESCE (ITEM_WMS.UNITS_PER_PICK_ACTIVE, 0) AS DECIMAL (9, 2))
          AS UNITS_PER_PICK_ACTIVE,
       ITEM_WMS.HNDL_ATTR_ACTIVE AS HNDL_ATTR_ACTIVE,
       CAST (
          COALESCE (ITEM_WMS.UNITS_PER_PICK_CASE_PICK, 0) AS DECIMAL (9, 2))
          AS UNITS_PER_PICK_CASE_PICK,
       ITEM_WMS.HNDL_ATTR_CASE_PICK AS HNDL_ATTR_CASE_PICK,
       CAST (
          COALESCE (ITEM_WMS.UNITS_PER_PICK_RESV, 0) AS DECIMAL (9, 2))
          AS UNITS_PER_PICK_RESV,
       ITEM_WMS.HNDL_ATTR_RESV AS HNDL_ATTR_RESV,
       CAST (COALESCE (ITEM_WMS.PROD_LIFE_IN_DAY, 0) AS INTEGER)
          AS PROD_LIFE_IN_DAY,
       CAST (COALESCE (ITEM_WMS.MAX_RECV_TO_XPIRE_DAYS, 0) AS INTEGER)
          AS MAX_RECV_TO_XPIRE_DAYS,
       CAST (COALESCE (ITEM_WMS.AVG_DLY_DMND, 0) AS DECIMAL (13, 5))
          AS AVG_DLY_DMND,
       CAST (COALESCE (ITEM_WMS.WT_TOL_PCNT, 0) AS DECIMAL (5, 2))
          AS WT_TOL_PCNT,
       ITEM_WMS.CONS_PRTY_DATE_CODE AS CONS_PRTY_DATE_CODE,
       ITEM_WMS.CONS_PRTY_DATE_WINDOW AS CONS_PRTY_DATE_WINDOW,
       CAST (
          COALESCE (ITEM_WMS.CONS_PRTY_DATE_WINDOW_INCR, 0) AS SMALLINT)
          AS CONS_PRTY_DATE_WINDOW_INCR,
       ITEM_WMS.ACTVTN_DATE AS ACTVTN_DATE,
       ITEM_WMS.ALLOW_RCPT_OLDER_ITEM AS ALLOW_RCPT_OLDER_SKU,
       CAST (COALESCE (ITEM_WMS.CRITCL_DIM_1, 0) AS DECIMAL (7, 2))
          AS CRITCL_DIM_1,
       CAST (COALESCE (ITEM_WMS.CRITCL_DIM_2, 0) AS DECIMAL (7, 2))
          AS CRITCL_DIM_2,
       CAST (COALESCE (ITEM_WMS.CRITCL_DIM_3, 0) AS DECIMAL (7, 2))
          AS CRITCL_DIM_3,
       ITEM_WMS.MFG_DATE_REQD AS MFG_DATE_REQD,
       ITEM_WMS.XPIRE_DATE_REQD AS XPIRE_DATE_REQD,
       ITEM_WMS.SHIP_BY_DATE_REQD AS SHIP_BY_DATE_REQD,
       ITEM_WMS.ITEM_ATTR_REQD AS SKU_ATTR_REQD,
       ITEM_WMS.BATCH_REQD AS BATCH_REQD,
       ITEM_WMS.PROD_STAT_REQD AS PROD_STAT_REQD,
       ITEM_WMS.CNTRY_OF_ORGN_REQD AS CNTRY_OF_ORGN_REQD,
       ITEM_WMS.VENDOR_ITEM_NBR AS VENDOR_ITEM_NBR,
       ITEM_WMS.PICK_WT_TOL_TYPE AS PICK_WT_TOL_TYPE,
       CAST (COALESCE (ITEM_WMS.PICK_WT_TOL_AMNT, 0) AS SMALLINT)
          AS PICK_WT_TOL_AMNT,
       ITEM_WMS.MHE_WT_TOL_TYPE AS MHE_WT_TOL_TYPE,
       CAST (COALESCE (ITEM_WMS.MHE_WT_TOL_AMNT, 0) AS SMALLINT)
          AS MHE_WT_TOL_AMNT,
       ITEM_WMS.LOAD_ATTR AS LOAD_ATTR,
       ITEM_WMS.TEMP_ZONE AS TEMP_ZONE,
       ITEM_WMS.TRLR_TEMP_ZONE AS TRLR_TEMP_ZONE,
       ITEM_WMS.PKT_CONSOL_ATTR AS PKT_CONSOL_ATTR,
       ITEM_WMS.BUYER_DISP_CODE AS BUYER_DISP_CODE,
       ITEM_WMS.CRUSH_CODE AS CRUSH_CODE,
       ITEM_WMS.CONVEY_FLAG AS CONVEY_FLAG,
       ITEM_WMS.STORE_DEPT AS STORE_DEPT,
       ITEM_WMS.MERCH_TYPE AS MERCH_TYPE,
       ITEM_WMS.MERCH_GROUP AS MERCH_GROUP,
       ITEM_WMS.SPL_INSTR_CODE_1 AS SPL_INSTR_CODE_1,
       ITEM_WMS.SPL_INSTR_CODE_2 AS SPL_INSTR_CODE_2,
       ITEM_WMS.SPL_INSTR_CODE_3 AS SPL_INSTR_CODE_3,
       ITEM_WMS.SPL_INSTR_CODE_4 AS SPL_INSTR_CODE_4,
       ITEM_WMS.SPL_INSTR_CODE_5 AS SPL_INSTR_CODE_5,
       ITEM_WMS.SPL_INSTR_CODE_6 AS SPL_INSTR_CODE_6,
       ITEM_WMS.SPL_INSTR_CODE_7 AS SPL_INSTR_CODE_7,
       ITEM_WMS.SPL_INSTR_CODE_8 AS SPL_INSTR_CODE_8,
       ITEM_WMS.SPL_INSTR_CODE_9 AS SPL_INSTR_CODE_9,
       ITEM_WMS.SPL_INSTR_CODE_10 AS SPL_INSTR_CODE_10,
       ITEM_WMS.SPL_INSTR_1 AS SPL_INSTR_1,
       ITEM_WMS.SPL_INSTR_2 AS SPL_INSTR_2,
       ITEM_WMS.PROMPT_FOR_VENDOR_ITEM_NBR AS PROMPT_FOR_VENDOR_ITEM_NBR,
       ITEM_WMS.PROMPT_PACK_QTY AS PROMPT_PACK_QTY,
       ITEM_WMS.ECCN_NBR AS ECCN_NBR,
       ITEM_WMS.EXP_LICN_NBR AS EXP_LICN_NBR,
       ITEM_WMS.EXP_LICN_XP_DATE AS EXP_LICN_XP_DATE,
       ITEM_WMS.EXP_LICN_SYMBOL AS EXP_LICN_SYMBOL,
       ITEM_WMS.ORGN_CERT_CODE AS ORGN_CERT_CODE,
       ITEM_WMS.ITAR_EXEMPT_NBR AS ITAR_EXEMPT_NBR,
       ITEM_WMS.FRT_CLASS AS FRT_CLASS,
       CAST (COALESCE (ITEM_WMS.DFLT_BATCH_STAT, '0') AS VARCHAR (3))
          AS DFLT_BATCH_STAT,
       ITEM_WMS.DFLT_INCUB_LOCK AS DFLT_INCUB_LOCK,
       ITEM_WMS.BASE_INCUB_FLAG AS BASE_INCUB_FLAG,
       CAST (COALESCE (ITEM_WMS.INCUB_DAYS, 0) AS SMALLINT) AS INCUB_DAYS,
       CAST (COALESCE (ITEM_WMS.INCUB_HOURS, 0) AS DECIMAL (4, 2))
          AS INCUB_HOURS,
       ITEM_WMS.SRL_NBR_BRCD_TYPE AS SRL_NBR_BRCD_TYPE,
       CAST (COALESCE (ITEM_WMS.MINOR_SRL_NBR_REQ, 0) AS SMALLINT)
          AS MINOR_SRL_NBR_REQ,
       CAST (COALESCE (ITEM_WMS.DUP_SRL_NBR_FLAG, 0) AS SMALLINT)
          AS DUP_SRL_NBR_FLAG,
       CAST (COALESCE (ITEM_WMS.MAX_RCPT_QTY, 0) AS DECIMAL (9, 2))
          AS MAX_RCPT_QTY,
       CAST (NULL AS VARCHAR (1)) AS FTZ_FLAG,
       CAST (NULL AS VARCHAR (12)) AS HTS_NBR,
       CAST (COALESCE (ITEM_WMS.CC_UNIT_TOLER_VALUE, 0) AS INTEGER)
          AS CC_UNIT_TOLER_VALUE,
       CAST (COALESCE (ITEM_WMS.CC_WGT_TOLER_VALUE, 0) AS INTEGER)
          AS CC_WGT_TOLER_VALUE,
       CAST (COALESCE (ITEM_WMS.CC_DLR_TOLER_VALUE, 0) AS INTEGER)
          AS CC_DLR_TOLER_VALUE,
       CAST (COALESCE (ITEM_WMS.CC_PCNT_TOLER_VALUE, 0) AS INTEGER)
          AS CC_PCNT_TOLER_VALUE,
       CAST (COALESCE (ITEM_WMS.VOCOLLECT_BASE_WT, 0) AS DECIMAL (13, 4))
          AS VOCOLLECT_BASE_WT,
       CAST (COALESCE (ITEM_WMS.VOCOLLECT_BASE_QTY, 0) AS DECIMAL (9, 2))
          AS VOCOLLECT_BASE_QTY,
       ITEM_WMS.VOCOLLECT_BASE_ITEM AS VOCOLLECT_BASE_ITEM,
       CAST (COALESCE (ITEM_WMS.PICK_WT_TOL_AMNT_ERROR, 0) AS SMALLINT)
          AS PICK_WT_TOL_AMNT_ERROR,
       ITEM_WMS.PRICE_TKT_TYPE AS PRICE_TKT_TYPE,
       CAST (COALESCE (ITEM_WMS.MONETARY_VALUE, 0) AS DECIMAL (13, 2))
          AS MONETARY_VALUE,
       ITEM_WMS.MV_CURRENCY_CODE AS MV_CURRENCY_CODE,
       ITEM_WMS.MV_SIZE_UOM AS MV_SIZE_UOM,
       ITEM_WMS.CODE_DATE_PROMPT_METHOD_FLAG
          AS CODE_DATE_PROMPT_METHOD_FLAG,
       CAST (COALESCE (ITEM_WMS.MIN_RECV_TO_XPIRE_DAYS, 0) AS INTEGER)
          AS MIN_RECV_TO_XPIRE_DAYS,
       CAST (COALESCE (ITEM_WMS.MIN_PCNT_FOR_LPN_SPLIT, 0) AS SMALLINT)
          AS MIN_PCNT_FOR_LPN_SPLIT,
       CAST (
          COALESCE (ITEM_WMS.MIN_LPN_QTY_FOR_SPLIT, 0) AS DECIMAL (13, 5))
          AS MIN_LPN_QTY_FOR_SPLIT,
       ITEM_WMS.PROD_CATGRY AS PROD_CATGRY,
       ITEM_WMS.PRICE_TIX_AVAIL AS PRICE_TIX_AVAIL_FLAG,
       ITEM_CBO.Audit_Last_Updated_Source AS USER_ID,
       UN_NUMBER.UN_NUMBER_VALUE AS UN_NBR,
       --ITEM_CBO.HAZMAT_CODE AS HAZMAT_CODE,
       '' AS HAZMAT_ID,
       (SELECT PRODUCT_CLASS.PRODUCT_CLASS
          FROM PRODUCT_CLASS
         WHERE ITEM_CBO.PRODUCT_CLASS_ID = PRODUCT_CLASS.PRODUCT_CLASS_ID
               AND ITEM_CBO.COMPANY_ID = PRODUCT_CLASS.TC_COMPANY_ID)
          AS PROD_GROUP,
       (SELECT COMMODITY_CODE.DESCRIPTION_SHORT
          FROM COMMODITY_CODE
         WHERE ITEM_CBO.COMMODITY_CODE_ID =
                  COMMODITY_CODE.COMMODITY_CODE_ID
               AND ITEM_CBO.COMPANY_ID = COMMODITY_CODE.TC_COMPANY_ID)
          AS COMMODITY_CODE,
       (SELECT STANDARD_UOM.ABBREVIATION
          FROM STANDARD_UOM, SIZE_UOM
         WHERE     ITEM_CBO.DISPLAY_QTY_UOM_ID = SIZE_UOM.SIZE_UOM_ID
               AND ITEM_CBO.DISPLAY_QTY_UOM_ID = SIZE_UOM.SIZE_UOM_ID
               AND SIZE_UOM.STANDARD_UOM = STANDARD_UOM.STANDARD_UOM)
          AS DSP_QTY_UOM,
       (SELECT STANDARD_UOM.ABBREVIATION
          FROM STANDARD_UOM, SIZE_UOM
         WHERE     ITEM_CBO.DISPLAY_QTY_UOM_ID = SIZE_UOM.SIZE_UOM_ID
               AND ITEM_CBO.BASE_STORAGE_UOM_ID = SIZE_UOM.SIZE_UOM_ID
               AND SIZE_UOM.STANDARD_UOM = STANDARD_UOM.STANDARD_UOM)
          AS DB_QTY_UOM,
       CAST (COALESCE (CASE.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_CASE_QTY,
       CAST (COALESCE (CASE.STD_CASE_WT, 0) AS DECIMAL (16, 4))
          AS STD_CASE_WT,
       CAST (COALESCE (CASE.STD_CASE_VOL, 0) AS DECIMAL (16, 4))
          AS STD_CASE_VOL,
       CAST (COALESCE (CASE.STD_CASE_LEN, 0) AS DECIMAL (16, 4))
          AS STD_CASE_LEN,
       CAST (COALESCE (CASE.STD_CASE_WIDTH, 0) AS DECIMAL (16, 4))
          AS STD_CASE_WIDTH,
       CAST (COALESCE (CASE.STD_CASE_HT, 0) AS DECIMAL (16, 4))
          AS STD_CASE_HT,
       CAST (COALESCE (PACK.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_PACK_QTY,
       CAST (COALESCE (PACK.STD_PACK_WT, 0) AS DECIMAL (16, 4))
          AS STD_PACK_WT,
       CAST (COALESCE (PACK.STD_PACK_VOL, 0) AS DECIMAL (16, 4))
          AS STD_PACK_VOL,
       CAST (COALESCE (PACK.STD_PACK_LEN, 0) AS DECIMAL (16, 4))
          AS STD_PACK_LEN,
       CAST (COALESCE (PACK.STD_PACK_WIDTH, 0) AS DECIMAL (16, 4))
          AS STD_PACK_WIDTH,
       CAST (COALESCE (PACK.STD_PACK_HT, 0) AS DECIMAL (16, 4))
          AS STD_PACK_HT,
       CAST (COALESCE (SUB_PACK.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_QTY,
       CAST (COALESCE (SUB_PACK.STD_SUB_PACK_WT, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_WT,
       CAST (COALESCE (SUB_PACK.STD_SUB_PACK_VOL, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_VOL,
       CAST (COALESCE (SUB_PACK.STD_SUB_PACK_LEN, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_LEN,
       CAST (
          COALESCE (SUB_PACK.STD_SUB_PACK_WIDTH, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_WIDTH,
       CAST (COALESCE (SUB_PACK.STD_SUB_PACK_HT, 0) AS DECIMAL (16, 4))
          AS STD_SUB_PACK_HT,
       CAST (COALESCE (TIER.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_TIER_QTY,
       CAST (COALESCE (BUNDL.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_BUNDL_QTY,
       CAST (COALESCE (PLT.QUANTITY, 1) AS DECIMAL (16, 4))
          AS STD_PLT_QTY,
       'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN' AS MARKS_NBRS,
       'N' AS NET_COST_FLAG,
       'N' AS PRODUCER_FLAG,
       'N' AS PREF_CRIT_FLAG,
       CAST (COALESCE (ITEM_WMS.SRL_NBR_REQD, 0) AS SMALLINT)
          AS SRL_NBR_REQD
  FROM ITEM_CBO
       LEFT OUTER JOIN ITEM_WMS
          ON ITEM_CBO.ITEM_ID = ITEM_WMS.ITEM_ID
       LEFT OUTER JOIN UN_NUMBER
          ON ITEM_CBO.UN_NUMBER_ID = UN_NUMBER.UN_NUMBER_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID,
                               ITEM_PACKAGE_CBO.WEIGHT AS STD_CASE_WT,
                               ITEM_PACKAGE_CBO.VOLUME AS STD_CASE_VOL,
                               ITEM_PACKAGE_CBO.LENGTH AS STD_CASE_LEN,
                               ITEM_PACKAGE_CBO.WIDTH AS STD_CASE_WIDTH,
                               ITEM_PACKAGE_CBO.HEIGHT AS STD_CASE_HT
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'C'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) CASE
          ON CASE.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID,
                               ITEM_PACKAGE_CBO.WEIGHT AS STD_PACK_WT,
                               ITEM_PACKAGE_CBO.VOLUME AS STD_PACK_VOL,
                               ITEM_PACKAGE_CBO.LENGTH AS STD_PACK_LEN,
                               ITEM_PACKAGE_CBO.WIDTH AS STD_PACK_WIDTH,
                               ITEM_PACKAGE_CBO.HEIGHT AS STD_PACK_HT
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'K'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) PACK
          ON PACK.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID,
                               ITEM_PACKAGE_CBO.WEIGHT AS STD_SUB_PACK_WT,
                               ITEM_PACKAGE_CBO.VOLUME
                                  AS STD_SUB_PACK_VOL,
                               ITEM_PACKAGE_CBO.LENGTH
                                  AS STD_SUB_PACK_LEN,
                               ITEM_PACKAGE_CBO.WIDTH
                                  AS STD_SUB_PACK_WIDTH,
                               ITEM_PACKAGE_CBO.HEIGHT AS STD_SUB_PACK_HT
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'S'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) SUB_PACK
          ON SUB_PACK.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'T'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) TIER
          ON TIER.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'B'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) BUNDL
          ON BUNDL.ITEM_ID = ITEM_CBO.ITEM_ID
       LEFT OUTER JOIN (SELECT ITEM_PACKAGE_CBO.QUANTITY AS QUANTITY,
                               ITEM_PACKAGE_CBO.ITEM_ID AS ITEM_ID
                          FROM ITEM_PACKAGE_CBO
                               INNER JOIN SIZE_UOM
                                  ON SIZE_UOM.SIZE_UOM_ID =
                                        ITEM_PACKAGE_CBO.PACKAGE_UOM_ID
                               INNER JOIN STANDARD_UOM
                                  ON SIZE_UOM.STANDARD_UOM =
                                        STANDARD_UOM.STANDARD_UOM
                                     AND STANDARD_UOM.ABBREVIATION = 'P'
                                     AND ITEM_PACKAGE_CBO.IS_STD = '1'
                                     AND ITEM_PACKAGE_CBO.MARK_FOR_DELETION =
                                            0) PLT
          ON PLT.ITEM_ID = ITEM_CBO.ITEM_ID);

CREATE OR REPLACE FORCE VIEW "ITEM_QUANTITY" ("UNIT_WT", "UNIT_VOL", "SKU_ID", "ITEM_ID", "CD_MASTER_ID", "STD_BUNDL_QTY", "STD_CASE_QTY", "STD_PLT_QTY", "STD_PACK_QTY", "STD_SUB_PACK_QTY", "STD_TIER_QTY", "STD_BUNDL_WT", "STD_CASE_WT", "STD_PLT_WT", "STD_PACK_WT", "STD_SUB_PACK_WT", "STD_TIER_WT", "STD_BUNDL_VOL", "STD_CASE_VOL", "STD_PLT_VOL", "STD_PACK_VOL", "STD_SUB_PACK_VOL", "STD_TIER_VOL", "STD_BUNDL_LEN", "STD_CASE_LEN", "STD_PLT_LEN", "STD_PACK_LEN", "STD_SUB_PACK_LEN", "STD_TIER_LEN", "STD_BUNDL_WIDTH", "STD_CASE_WIDTH", "STD_PLT_WIDTH", "STD_PACK_WIDTH", "STD_SUB_PACK_WIDTH", "STD_TIER_WIDTH", "STD_BUNDL_HT", "STD_CASE_HT", "STD_PLT_HT", "STD_PACK_HT", "STD_SUB_PACK_HT", "STD_TIER_HT", "DB_QTY_UOM", "DSP_QTY_UOM") AS
(select cast (nvl (item_cbo.unit_weight, 0) as number (16, 4)) as unit_wt,
       cast (nvl (item_cbo.unit_volume, 0) as number (16, 4)) as unit_vol,
       item_cbo.item_name as sku_id,
       item_cbo.item_id as item_id,
       item_cbo.company_id as cd_master_id,
       cast (nvl (bundl.quantity, 1) as number (16, 4)) as std_bundl_qty,
       cast (nvl (case.quantity, 1) as number (16, 4)) as std_case_qty,
       cast (nvl (plt.quantity, 1) as number (16, 4)) as std_plt_qty,
       cast (nvl (pack.quantity, 1) as number (16, 4)) as std_pack_qty,
       cast (nvl (sub_pack.quantity, 1) as number (16, 4))
          as std_sub_pack_qty,
       cast (nvl (tier.quantity, 1) as number (16, 4)) as std_tier_qty,
       cast (nvl (bundl.weight, 0) as number (16, 4)) as std_bundl_wt,
       cast (nvl (case.weight, 0) as number (16, 4)) as std_case_wt,
       cast (nvl (plt.weight, 0) as number (16, 4)) as std_plt_wt,
       cast (nvl (pack.weight, 0) as number (16, 4)) as std_pack_wt,
       cast (nvl (sub_pack.weight, 0) as number (16, 4))
          as std_sub_pack_wt,
       cast (nvl (tier.weight, 0) as number (16, 4)) as std_tier_wt,
       cast (nvl (bundl.volume, 0) as number (16, 4)) as std_bundl_vol,
       cast (nvl (case.volume, 0) as number (16, 4)) as std_case_vol,
       cast (nvl (plt.volume, 0) as number (16, 4)) as std_plt_vol,
       cast (nvl (pack.volume, 0) as number (16, 4)) as std_pack_vol,
       cast (nvl (sub_pack.volume, 0) as number (16, 4))
          as std_sub_pack_vol,
       cast (nvl (tier.volume, 0) as number (16, 4)) as std_tier_vol,
       cast (nvl (bundl.length, 0) as number (16, 4)) as std_bundl_len,
       cast (nvl (case.length, 0) as number (16, 4)) as std_case_len,
       cast (nvl (plt.length, 0) as number (16, 4)) as std_plt_len,
       cast (nvl (pack.length, 0) as number (16, 4)) as std_pack_len,
       cast (nvl (sub_pack.length, 0) as number (16, 4))
          as std_sub_pack_len,
       cast (nvl (tier.length, 0) as number (16, 4)) as std_tier_len,
       cast (nvl (bundl.width, 0) as number (16, 4)) as std_bundl_width,
       cast (nvl (case.width, 0) as number (16, 4)) as std_case_width,
       cast (nvl (plt.width, 0) as number (16, 4)) as std_plt_width,
       cast (nvl (pack.width, 0) as number (16, 4)) as std_pack_width,
       cast (nvl (sub_pack.width, 0) as number (16, 4))
          as std_sub_pack_width,
       cast (nvl (tier.width, 0) as number (16, 4)) as std_tier_width,
       cast (nvl (bundl.height, 0) as number (16, 4)) as std_bundl_ht,
       cast (nvl (case.height, 0) as number (16, 4)) as std_case_ht,
       cast (nvl (plt.height, 0) as number (16, 4)) as std_plt_ht,
       cast (nvl (pack.height, 0) as number (16, 4)) as std_pack_ht,
       cast (nvl (sub_pack.height, 0) as number (16, 4))
          as std_sub_pack_ht,
       cast (nvl (tier.height, 0) as number (16, 4)) as std_tier_ht,
       (select standard_uom.abbreviation
          from standard_uom, size_uom
         where     item_cbo.display_qty_uom_id = size_uom.size_uom_id
               and item_cbo.base_storage_uom_id = size_uom.size_uom_id
               and size_uom.standard_uom = standard_uom.standard_uom)
          as db_qty_uom,
       (select standard_uom.abbreviation
          from standard_uom, size_uom
         where     item_cbo.display_qty_uom_id = size_uom.size_uom_id
               and item_cbo.display_qty_uom_id = size_uom.size_uom_id
               and size_uom.standard_uom = standard_uom.standard_uom)
          as dsp_qty_uom
  from item_cbo
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'C'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) case
          on case.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'K'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) pack
          on pack.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'S'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) sub_pack
          on sub_pack.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'B'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) bundl
          on bundl.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'P'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) plt
          on plt.item_id = item_cbo.item_id
       left outer join (select item_package_cbo.quantity as quantity,
                               item_package_cbo.weight as weight,
                               -item_package_cbo.volume as volume,
                               item_package_cbo.length as length,
                               item_package_cbo.height as height,
                               item_package_cbo.width as width,
                               item_package_cbo.item_id as item_id
                          from item_package_cbo
                               inner join size_uom
                                  on size_uom.size_uom_id =
                                        item_package_cbo.package_uom_id
                               inner join standard_uom
                                  on size_uom.standard_uom =
                                        standard_uom.standard_uom
                                     and standard_uom.abbreviation = 'T'
                                     and item_package_cbo.is_std = '1'
                                     and item_package_cbo.mark_for_deletion =
                                            0) tier
          on tier.item_id = item_cbo.item_id);

CREATE OR REPLACE FORCE VIEW "LANEFACILITY_V" ("RFPID", "LANEID", "DIRECTION", "FACILITYCODE", "AREA") AS
SELECT DISTINCT x0.rfpid,
x0.laneid,
'IB',
x0.destinationfacilitycode,
x0.destinationcapacityareacode
FROM lane x0
WHERE ((x0.destinationfacilitycode       IS NOT NULL
OR x0.destinationcapacityareacode        IS NOT NULL )
AND ((LENGTH(x0.destinationfacilitycode)  > 0 )
OR (LENGTH(x0.destinationcapacityareacode)> 0 )) )
UNION ALL
SELECT DISTINCT x1.rfpid,
x1.laneid,
'OB',
x1.originfacilitycode,
x1.origincapacityareacode
FROM lane x1
WHERE ((x1.originfacilitycode       IS NOT NULL
OR x1.origincapacityareacode        IS NOT NULL )
AND ((LENGTH(x1.originfacilitycode)  > 0 )
OR (LENGTH(x1.origincapacityareacode)> 0 )) );

CREATE OR REPLACE FORCE VIEW "LANE_DETAIL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_DTL_SEQ", "IS_RATING", "IS_ROUTING", "IS_SAILING", "CARRIER_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "RANK", "EQUIPMENT_ID", "TIER_ID", "REP_TP_FLAG", "RLD_RATE_SEQ", "RATE", "CURRENCY_CODE", "RATE_CALC_METHOD", "RATE_UOM", "MINIMUM_RATE", "IS_BUDGETED", "IS_PREFERRED", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "COMMODITY_CODE_ID", "FIXED_TRANSIT_STANDARD_UOM", "FIXED_TRANSIT_VALUE", "SAILING_FREQUENCY_TYPE", "SAILING_SCHEDULE_NAME", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5", "TT_TOLERANCE_FACTOR", "PACKAGE_ID", "PACKAGE_NAME", "SCNDR_CARRIER_ID", "MOT_ID", "PROTECTION_LEVEL_ID", "SERVICE_LEVEL_ID", "SIZE_UOM_ID", "VOYAGE", "HAS_SHIPPING_PARAM", "CUBING_INDICATOR", "OVERRIDE_CODE", "CARRIER_REJECT_PERIOD", "PREFERENCE_BONUS_VALUE", "REJECT_FURTHER_SHIPMENTS", "LANE_DTL_STATUS") AS
SELECT comb_lane_dtl.TC_COMPANY_ID,
comb_lane_dtl.lane_id,
comb_lane_dtl.lane_dtl_seq,
comb_lane_dtl.is_rating,
comb_lane_dtl.is_routing,
comb_lane_dtl.is_sailing,
comb_lane_dtl.carrier_id,
comb_lane_dtl.effective_dt,
comb_lane_dtl.expiration_dt,
comb_lane_dtl.RANK,
comb_lane_dtl.EQUIPMENT_ID,
comb_lane_dtl.tier_id,
comb_lane_dtl.rep_tp_flag,
rlr.rld_rate_seq,
rlr.rate,
rlr.currency_code,
rlr.rate_calc_method,
rlr.rate_uom,
rlr.minimum_rate,
comb_lane_dtl.IS_BUDGETED,
comb_lane_dtl.IS_PREFERRED,
comb_lane_dtl.LAST_UPDATED_SOURCE_TYPE,
comb_lane_dtl.LAST_UPDATED_SOURCE,
comb_lane_dtl.LAST_UPDATED_DTTM,
comb_lane_dtl.COMMODITY_CODE_ID,
comb_lane_dtl.FIXED_TRANSIT_STANDARD_UOM,
comb_lane_dtl.FIXED_TRANSIT_VALUE,
comb_lane_dtl.SAILING_FREQUENCY_TYPE,
comb_lane_dtl.SAILING_SCHEDULE_NAME,
comb_lane_dtl.O_SHIP_VIA,
comb_lane_dtl.D_SHIP_VIA,
comb_lane_dtl.CONTRACT_NUMBER,
comb_lane_dtl.CUSTOM_TEXT1,
comb_lane_dtl.CUSTOM_TEXT2,
comb_lane_dtl.CUSTOM_TEXT3,
comb_lane_dtl.CUSTOM_TEXT4,
comb_lane_dtl.CUSTOM_TEXT5,
comb_lane_dtl.TT_TOLERANCE_FACTOR,
comb_lane_dtl.PACKAGE_ID,
comb_lane_dtl.PACKAGE_NAME,
comb_lane_dtl.SCNDR_CARRIER_ID,
comb_lane_dtl.MOT_ID,
comb_lane_dtl.PROTECTION_LEVEL_ID,
comb_lane_dtl.SERVICE_LEVEL_ID,
comb_lane_dtl.SIZE_UOM_ID,
comb_lane_dtl.VOYAGE,
comb_lane_dtl.HAS_SHIPPING_PARAM,
comb_lane_dtl.cubing_indicator,
comb_lane_dtl.override_code,
comb_lane_dtl.carrier_reject_period,
comb_lane_dtl.preference_bonus_value,
comb_lane_dtl.reject_further_shipments,
comb_lane_dtl.LANE_DTL_STATUS
FROM comb_lane_dtl
LEFT JOIN
(SELECT *
FROM rating_lane_dtl_rate inner_rlr
WHERE inner_rlr.rld_rate_seq =
  (SELECT MIN (NVL (rld_rate_seq, -1))
  FROM rating_lane_dtl_rate
  WHERE lane_id           = inner_rlr.lane_id
  AND rating_lane_dtl_seq = inner_rlr.rating_lane_dtl_seq
  AND tc_company_id       = inner_rlr.tc_company_id
  )
) rlr
ON comb_lane_dtl.lane_dtl_seq = rlr.rating_lane_dtl_seq
AND comb_lane_dtl.LANE_ID     = rlr.LANE_ID
UNION ALL
SELECT im_rating_dtl.TC_COMPANY_ID,
IRL.RATING_LANE_ID,
im_rating_dtl.RATING_LANE_DTL_SEQ,
1 AS IS_RATING,
0 AS IS_ROUTING,
0 AS IS_SAILING,
im_rating_dtl.carrier_id,
im_rating_dtl.effective_dt,
im_rating_dtl.expiration_dt,
NULL AS RANK,
im_rating_dtl.equipment_id,
NULL AS TIER_ID,
NULL AS rep_tp_flag,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
im_rating_dtl.IS_BUDGETED,
NULL AS IS_PREFERRED,
im_rating_dtl.LAST_UPDATED_SOURCE_TYPE,
im_rating_dtl.LAST_UPDATED_SOURCE,
im_rating_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
NULL AS FIXED_TRANSIT_STANDARD_UOM,
NULL AS FIXED_TRANSIT_VALUE,
NULL AS SAILING_FREQUENCY_TYPE,
NULL AS SAILING_SCHEDULE_NAME,
im_rating_dtl.O_SHIP_VIA,
im_rating_dtl.D_SHIP_VIA,
im_rating_dtl.CONTRACT_NUMBER,
im_rating_dtl.CUSTOM_TEXT1,
im_rating_dtl.CUSTOM_TEXT2,
im_rating_dtl.CUSTOM_TEXT3,
im_rating_dtl.CUSTOM_TEXT4,
im_rating_dtl.CUSTOM_TEXT5,
NULL AS TT_TOLERANCE_FACTOR,
im_rating_dtl.PACKAGE_ID,
im_rating_dtl.PACKAGE_NAME,
im_rating_dtl.SCNDR_CARRIER_ID,
im_rating_dtl.MOT_ID,
im_rating_dtl.PROTECTION_LEVEL_ID,
im_rating_dtl.SERVICE_LEVEL_ID,
NULL AS SIZE_UOM_ID,
im_rating_dtl.VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
NULL AS CUBING_INDICATOR,
NULL AS OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
NULL AS PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
im_rating_dtl.RATING_LANE_DTL_STATUS
FROM IMPORT_RATING_LANE irl
JOIN IMPORT_RATING_LANE_DTL im_rating_dtl
ON irl.LANE_ID = im_rating_dtl.LANE_ID
JOIN COMB_LANE Cl
ON Cl.LANE_ID = irl.RATING_LANE_ID
UNION ALL
SELECT im_rating_dtl.TC_COMPANY_ID,
IRL.RATING_LANE_ID,
im_rating_dtl.RATING_LANE_DTL_SEQ,
1 AS IS_RATING,
0 AS IS_ROUTING,
0 AS IS_SAILING,
im_rating_dtl.carrier_id,
im_rating_dtl.effective_dt,
im_rating_dtl.expiration_dt,
NULL AS RANK,
im_rating_dtl.equipment_id,
NULL AS TIER_ID,
NULL AS rep_tp_flag,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
im_rating_dtl.IS_BUDGETED,
NULL AS IS_PREFERRED,
im_rating_dtl.LAST_UPDATED_SOURCE_TYPE,
im_rating_dtl.LAST_UPDATED_SOURCE,
im_rating_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
NULL AS FIXED_TRANSIT_STANDARD_UOM,
NULL AS FIXED_TRANSIT_VALUE,
NULL AS SAILING_FREQUENCY_TYPE,
NULL AS SAILING_SCHEDULE_NAME,
im_rating_dtl.O_SHIP_VIA,
im_rating_dtl.D_SHIP_VIA,
im_rating_dtl.CONTRACT_NUMBER,
im_rating_dtl.CUSTOM_TEXT1,
im_rating_dtl.CUSTOM_TEXT2,
im_rating_dtl.CUSTOM_TEXT3,
im_rating_dtl.CUSTOM_TEXT4,
im_rating_dtl.CUSTOM_TEXT5,
NULL AS TT_TOLERANCE_FACTOR,
im_rating_dtl.PACKAGE_ID,
im_rating_dtl.PACKAGE_NAME,
im_rating_dtl.SCNDR_CARRIER_ID,
im_rating_dtl.MOT_ID,
im_rating_dtl.PROTECTION_LEVEL_ID,
im_rating_dtl.SERVICE_LEVEL_ID,
NULL AS SIZE_UOM_ID,
im_rating_dtl.VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
NULL AS CUBING_INDICATOR,
NULL AS OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
NULL AS PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
im_rating_dtl.RATING_LANE_DTL_STATUS
FROM IMPORT_RATING_LANE irl
JOIN IMPORT_RATING_LANE_DTL im_rating_dtl
ON irl.LANE_ID        = im_rating_dtl.LANE_ID
WHERE IRL.LANE_STATUS = 4
UNION ALL
SELECT import_rg_dtl.TC_COMPANY_ID,
irgl.RG_LANE_ID,
import_rg_dtl.RG_LANE_DTL_SEQ,
0 AS IS_RATING,
1 AS IS_ROUTING,
0 AS IS_SAILING,
import_rg_dtl.CARRIER_ID,
import_rg_dtl.EFFECTIVE_DT,
import_rg_dtl.EXPIRATION_DT,
import_rg_dtl.RANK,
import_rg_dtl.EQUIPMENT_ID,
import_rg_dtl.TIER_ID,
import_rg_dtl.REP_TP_FLAG,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
NULL AS IS_BUDGETED,
import_rg_dtl.IS_PREFERRED,
import_rg_dtl.LAST_UPDATED_SOURCE_TYPE,
import_rg_dtl.LAST_UPDATED_SOURCE,
import_rg_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
NULL AS FIXED_TRANSIT_STANDARD_UOM,
NULL AS FIXED_TRANSIT_VALUE,
NULL AS SAILING_FREQUENCY_TYPE,
NULL AS SAILING_SCHEDULE_NAME,
NULL AS O_SHIP_VIA,
NULL AS D_SHIP_VIA,
NULL AS CONTRACT_NUMBER,
NULL AS CUSTOM_TEXT1,
NULL AS CUSTOM_TEXT2,
NULL AS CUSTOM_TEXT3,
NULL AS CUSTOM_TEXT4,
NULL AS CUSTOM_TEXT5,
import_rg_dtl.TT_TOLERANCE_FACTOR,
NULL AS PACKAGE_ID,
NULL AS PACKAGE_NAME,
import_rg_dtl.SCNDR_CARRIER_ID,
import_rg_dtl.MOT_ID,
import_rg_dtl.PROTECTION_LEVEL_ID,
import_rg_dtl.SERVICE_LEVEL_ID,
import_rg_dtl.SIZE_UOM_ID,
import_rg_dtl.VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
import_rg_dtl.CUBING_INDICATOR,
import_rg_dtl.OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
import_rg_dtl.PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
import_rg_dtl.LANE_DTL_STATUS
FROM import_rg_lane irgl
JOIN import_rg_lane_dtl import_rg_dtl
ON irgl.LANE_ID = import_rg_dtl.LANE_ID
JOIN COMB_LANE Cl
ON Cl.LANE_ID = irgl.RG_LANE_ID
UNION ALL
SELECT import_rg_dtl.TC_COMPANY_ID,
irgl.RG_LANE_ID,
import_rg_dtl.RG_LANE_DTL_SEQ,
0 AS IS_RATING,
1 AS IS_ROUTING,
0 AS IS_SAILING,
import_rg_dtl.CARRIER_ID,
import_rg_dtl.EFFECTIVE_DT,
import_rg_dtl.EXPIRATION_DT,
import_rg_dtl.RANK,
import_rg_dtl.EQUIPMENT_ID,
import_rg_dtl.TIER_ID,
import_rg_dtl.REP_TP_FLAG,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
NULL AS IS_BUDGETED,
import_rg_dtl.IS_PREFERRED,
import_rg_dtl.LAST_UPDATED_SOURCE_TYPE,
import_rg_dtl.LAST_UPDATED_SOURCE,
import_rg_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
NULL AS FIXED_TRANSIT_STANDARD_UOM,
NULL AS FIXED_TRANSIT_VALUE,
NULL AS SAILING_FREQUENCY_TYPE,
NULL AS SAILING_SCHEDULE_NAME,
NULL AS O_SHIP_VIA,
NULL AS D_SHIP_VIA,
NULL AS CONTRACT_NUMBER,
NULL AS CUSTOM_TEXT1,
NULL AS CUSTOM_TEXT2,
NULL AS CUSTOM_TEXT3,
NULL AS CUSTOM_TEXT4,
NULL AS CUSTOM_TEXT5,
import_rg_dtl.TT_TOLERANCE_FACTOR,
NULL AS PACKAGE_ID,
NULL AS PACKAGE_NAME,
import_rg_dtl.SCNDR_CARRIER_ID,
import_rg_dtl.MOT_ID,
import_rg_dtl.PROTECTION_LEVEL_ID,
import_rg_dtl.SERVICE_LEVEL_ID,
import_rg_dtl.SIZE_UOM_ID,
import_rg_dtl.VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
import_rg_dtl.CUBING_INDICATOR,
import_rg_dtl.OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
import_rg_dtl.PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
import_rg_dtl.LANE_DTL_STATUS
FROM import_rg_lane irgl
JOIN import_rg_lane_dtl import_rg_dtl
ON irgl.LANE_ID        = import_rg_dtl.LANE_ID
WHERE irgl.LANE_STATUS = 4
UNION ALL
SELECT im_sailing_dtl.TC_COMPANY_ID,
isl.SAILING_LANE_ID,
im_sailing_dtl.SAILING_LANE_DTL_SEQ,
0 AS IS_RATING,
0 AS IS_ROUTING,
1 AS IS_SAILING,
im_sailing_dtl.CARRIER_ID,
im_sailing_dtl.EFFECTIVE_DT,
im_sailing_dtl.EXPIRATION_DT,
NULL AS RANK,
NULL AS EQUIPMENT_ID,
NULL AS TIER_ID,
NULL AS REP_TP_FLAG,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
NULL AS IS_BUDGETED,
NULL AS IS_PREFERRED,
im_sailing_dtl.LAST_UPDATED_SOURCE_TYPE,
im_sailing_dtl.LAST_UPDATED_SOURCE,
im_sailing_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
im_sailing_dtl.FIXED_TRANSIT_STANDARD_UOM,
im_sailing_dtl.FIXED_TRANSIT_VALUE,
im_sailing_dtl.SAILING_FREQUENCY_TYPE,
im_sailing_dtl.SAILING_SCHEDULE_NAME,
NULL AS O_SHIP_VIA,
NULL AS D_SHIP_VIA,
NULL AS CONTRACT_NUMBER,
NULL AS CUSTOM_TEXT1,
NULL AS CUSTOM_TEXT2,
NULL AS CUSTOM_TEXT3,
NULL AS CUSTOM_TEXT4,
NULL AS CUSTOM_TEXT5,
NULL AS TT_TOLERANCE_FACTOR,
NULL AS PACKAGE_ID,
NULL AS PACKAGE_NAME,
im_sailing_dtl.SCNDR_CARRIER_ID,
im_sailing_dtl.MOT_ID,
NULL AS PROTECTION_LEVEL_ID,
im_sailing_dtl.SERVICE_LEVEL_ID,
NULL AS SIZE_UOM_ID,
NULL AS VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
NULL AS CUBING_INDICATOR,
NULL AS OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
NULL AS PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
im_sailing_dtl.LANE_DTL_STATUS
FROM import_sailing_lane isl
JOIN import_sailing_lane_dtl im_sailing_dtl
ON isl.LANE_ID = im_sailing_dtl.LANE_ID
JOIN COMB_LANE Cl
ON Cl.LANE_ID = isl.SAILING_LANE_ID
UNION ALL
SELECT im_sailing_dtl.TC_COMPANY_ID,
isl.SAILING_LANE_ID,
im_sailing_dtl.SAILING_LANE_DTL_SEQ,
0 AS IS_RATING,
0 AS IS_ROUTING,
1 AS IS_SAILING,
im_sailing_dtl.CARRIER_ID,
im_sailing_dtl.EFFECTIVE_DT,
im_sailing_dtl.EXPIRATION_DT,
NULL AS RANK,
NULL AS EQUIPMENT_ID,
NULL AS TIER_ID,
NULL AS REP_TP_FLAG,
NULL AS RLD_RATE_SEQ,
NULL AS RATE,
NULL AS CURRENCY_CODE,
NULL AS RATE_CALC_METHOD,
NULL AS RATE_UOM,
NULL AS MINIMUM_RATE,
NULL AS IS_BUDGETED,
NULL AS IS_PREFERRED,
im_sailing_dtl.LAST_UPDATED_SOURCE_TYPE,
im_sailing_dtl.LAST_UPDATED_SOURCE,
im_sailing_dtl.LAST_UPDATED_DTTM,
NULL AS COMMODITY_CODE_ID,
im_sailing_dtl.FIXED_TRANSIT_STANDARD_UOM,
im_sailing_dtl.FIXED_TRANSIT_VALUE,
im_sailing_dtl.SAILING_FREQUENCY_TYPE,
im_sailing_dtl.SAILING_SCHEDULE_NAME,
NULL AS O_SHIP_VIA,
NULL AS D_SHIP_VIA,
NULL AS CONTRACT_NUMBER,
NULL AS CUSTOM_TEXT1,
NULL AS CUSTOM_TEXT2,
NULL AS CUSTOM_TEXT3,
NULL AS CUSTOM_TEXT4,
NULL AS CUSTOM_TEXT5,
NULL AS TT_TOLERANCE_FACTOR,
NULL AS PACKAGE_ID,
NULL AS PACKAGE_NAME,
im_sailing_dtl.SCNDR_CARRIER_ID,
im_sailing_dtl.MOT_ID,
NULL AS PROTECTION_LEVEL_ID,
im_sailing_dtl.SERVICE_LEVEL_ID,
NULL AS SIZE_UOM_ID,
NULL AS VOYAGE,
NULL AS HAS_SHIPPING_PARAM,
NULL AS CUBING_INDICATOR,
NULL AS OVERRIDE_CODE,
NULL AS CARRIER_REJECT_PERIOD,
NULL AS PREFERENCE_BONUS_VALUE,
NULL AS REJECT_FURTHER_SHIPMENTS,
im_sailing_dtl.LANE_DTL_STATUS
FROM import_sailing_lane isl
JOIN import_sailing_lane_dtl im_sailing_dtl
ON isl.LANE_ID        = im_sailing_dtl.LANE_ID
WHERE isl.LANE_STATUS = 4;

CREATE OR REPLACE FORCE VIEW "LANE_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "RG_QUALIFIER", "FREQUENCY", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_NAME", "CUSTOMER_ID", "BILLING_METHOD", "INCOTERM_ID", "ROUTE_TO", "ROUTE_TYPE_1", "ROUTE_TYPE_2", "NO_RATING", "USE_FASTEST", "USE_PREFERENCE_BONUS", "IS_ROUTING", "IS_RATING", "IS_SAILING") AS
SELECT l.tc_company_id,
l.lane_id,
l.lane_hierarchy,
l.lane_status,
l.o_loc_type,
l.o_facility_id,
l.o_facility_alias_id,
UPPER (l.o_city),
l.o_state_prov,
UPPER (l.o_county),
l.o_postal_code,
l.o_country_code,
l.o_zone_id,
'',
l.d_loc_type,
l.d_facility_id,
l.d_facility_alias_id,
UPPER (l.d_city),
l.d_state_prov,
UPPER (l.d_county),
l.d_postal_code,
l.d_country_code,
l.d_zone_id,
'',
l.rg_qualifier,
l.frequency,
DECODE (l.lane_status, 4, 1, 0) has_errors,
COALESCE (
CASE
  WHEN IS_ROUTING = 1
  THEN
    (SELECT SIGN (MAX (DECODE (ID.lane_dtl_status, 4, 1, 0)))
    FROM import_rg_lane ir,
      import_rg_lane_dtl ID
    WHERE ( l.lane_id    = ir.rg_lane_id
    AND l.tc_company_id  = ir.tc_company_id
    AND ir.lane_id       = ID.lane_id
    AND ir.tc_company_id = ID.tc_company_id)
    )
  WHEN IS_RATING = 1
  THEN
    (SELECT SIGN ( MAX ( DECODE (id.rating_lane_dtl_status, 4, 1, 0)))
    FROM import_rating_lane ir,
      import_rating_lane_dtl id
    WHERE ( l.lane_id    = ir.rating_lane_id
    AND l.tc_company_id  = ir.tc_company_id
    AND ir.lane_id       = id.lane_id
    AND ir.tc_company_id = id.tc_company_id)
    )
  WHEN IS_SAILING = 1
  THEN
    (SELECT SIGN ( MAX ( DECODE (CAST (SID.LANE_DTL_STATUS AS INT), 4, 1, 0)))
    FROM IMPORT_SAILING_LANE IR,
      IMPORT_SAILING_LANE_DTL SID
    WHERE ( L.LANE_ID    = IR.SAILING_LANE_ID
    AND L.TC_COMPANY_ID  = IR.TC_COMPANY_ID
    AND IR.LANE_ID       = SID.LANE_ID
    AND IR.TC_COMPANY_ID = SID.TC_COMPANY_ID)
    )
END, 0) dtl_has_errors,
l.created_source_type,
l.created_source,
l.created_dttm,
l.last_updated_source_type,
l.last_updated_source,
l.last_updated_dttm,
l.lane_name,
l.customer_id,
l.billing_method,
l.incoterm_id,
l.route_to,
l.route_type_1,
l.route_type_2,
l.no_rating,
l.use_fastest,
l.use_preference_bonus,
IS_ROUTING,
is_rating,
IS_SAILING
FROM comb_lane l
WHERE (l.IS_ROUTING = 1
OR l.is_rating      = 1
OR l.IS_SAILING     = 1)
UNION ALL
SELECT il.tc_company_id,
il.rg_lane_id,
il.lane_hierarchy,
il.lane_status,
il.o_loc_type,
il.o_facility_id,
il.o_facility_alias_id,
UPPER (il.o_city),
il.o_state_prov,
UPPER (il.o_county),
il.o_postal_code,
il.o_country_code,
il.o_zone_id,
il.o_zone_name,
il.d_loc_type,
il.d_facility_id,
il.d_facility_alias_id,
UPPER (il.d_city),
il.d_state_prov,
UPPER (il.d_county),
il.d_postal_code,
il.d_country_code,
il.d_zone_id,
il.d_zone_name,
il.rg_qualifier,
il.frequency,
DECODE (il.lane_status, 4, 1, 0) has_errors,
COALESCE (
(SELECT MAX (DECODE (d.lane_dtl_status, 4, 1, 0))
FROM import_rg_lane_dtl d
WHERE d.lane_id     = il.lane_id
AND d.tc_company_id = il.tc_company_id
), 0) dtl_has_errors,
il.created_source_type,
il.created_source,
il.created_dttm,
il.last_updated_source_type,
il.last_updated_source,
il.last_updated_dttm,
il.lane_name,
il.customer_id,
il.billing_method,
il.incoterm_id,
il.route_to,
il.route_type_1,
il.route_type_2,
il.no_rating,
il.use_fastest,
il.use_preference_bonus,
1,
0,
0
FROM import_rg_lane il
WHERE il.lane_status = 4
AND NOT EXISTS
(SELECT 1
FROM comb_lane cl
WHERE il.tc_company_id = cl.tc_company_id
AND il.rg_lane_id      = cl.lane_id
AND cl.is_routing      = 1
)
UNION ALL
SELECT I.TC_COMPANY_ID,
I.SAILING_LANE_ID,
I.LANE_HIERARCHY,
I.LANE_STATUS,
I.O_LOC_TYPE,
I.O_FACILITY_ID,
I.O_FACILITY_ALIAS_ID,
UPPER (I.O_CITY),
I.O_STATE_PROV,
UPPER (I.O_COUNTY),
I.O_POSTAL_CODE,
I.O_COUNTRY_CODE,
I.O_ZONE_ID,
I.O_ZONE_NAME,
I.D_LOC_TYPE,
I.D_FACILITY_ID,
I.D_FACILITY_ALIAS_ID,
UPPER (I.D_CITY),
I.D_STATE_PROV,
UPPER (I.D_COUNTY),
I.D_POSTAL_CODE,
I.D_COUNTRY_CODE,
I.D_ZONE_ID,
I.D_ZONE_NAME,
'',
'',
DECODE (CAST (I.LANE_STATUS AS INT), 4, 1, 0) HAS_ERRORS,
COALESCE (
(SELECT MAX (DECODE (CAST (D.LANE_DTL_STATUS AS INT), 4, 1, 0))
FROM IMPORT_SAILING_LANE_DTL D
WHERE D.LANE_ID     = I.LANE_ID
AND D.TC_COMPANY_ID = I.TC_COMPANY_ID
), 0) DTL_HAS_ERRORS,
I.CREATED_SOURCE_TYPE,
I.CREATED_SOURCE,
I.CREATED_DTTM,
I.LAST_UPDATED_SOURCE_TYPE,
I.LAST_UPDATED_SOURCE,
I.LAST_UPDATED_DTTM,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
0, --routing
0, --rating
1  --sailing
FROM IMPORT_SAILING_LANE I
WHERE I.LANE_STATUS = 4
AND NOT EXISTS
(SELECT 1
FROM COMB_LANE CL
WHERE I.TC_COMPANY_ID = CL.TC_COMPANY_ID
AND I.SAILING_LANE_ID = CL.LANE_ID
AND CL.is_SAILING     = 1
)
UNION ALL
SELECT i.tc_company_id,
i.rating_lane_id,
i.lane_hierarchy,
i.lane_status,
i.o_loc_type,
i.o_facility_id,
i.o_facility_alias_id,
UPPER (i.o_city),
i.o_state_prov,
UPPER (i.o_county),
i.o_postal_code,
i.o_country_code,
I.O_ZONE_ID,
I.O_ZONE_NAME,
i.d_loc_type,
i.d_facility_id,
i.d_facility_alias_id,
UPPER (i.d_city),
i.d_state_prov,
UPPER (i.d_county),
i.d_postal_code,
i.d_country_code,
I.D_ZONE_ID,
I.D_ZONE_NAME,
'',
'',
DECODE (i.lane_status, 4, 1, 0) has_errors,
COALESCE (
(SELECT MAX (DECODE (d.rating_lane_dtl_status, 4, 1, 0))
FROM import_rating_lane_dtl d
WHERE d.lane_id     = i.lane_id
AND d.tc_company_id = i.tc_company_id
), 0) dtl_has_errors,
i.created_source_type,
i.created_source,
i.created_dttm,
i.last_updated_source_type,
i.last_updated_source,
i.last_updated_dttm,
'',
CAST (NULL AS INT),
CAST (NULL AS INT),
CAST (NULL AS INT),
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
0, --routing
1, --rating
0  --sailing
FROM import_rating_lane i
WHERE i.lane_status = 4
AND NOT EXISTS
(SELECT 1
FROM COMB_LANE CL
WHERE i.tc_company_id = CL.tc_company_id
AND i.rating_lane_id  = CL.lane_id
AND CL.IS_RATING      = 1
);

CREATE OR REPLACE FORCE VIEW "CWS_ORDER_LAT_LANG" ("CONS_WORKSPACE_ID", "ORDER_ID", "O_LATITUDE", "O_LONGITUDE", "D_LATITUDE", "D_LONGITUDE") AS
(select cws_order.cons_workspace_id,
       cws_order.order_id,
       o.latitude,
       o.longitude,
       d.latitude,
       d.longitude
  from cws_order,
       orders,
       postal_code d,
       postal_code o
 where     cws_order.order_id = orders.order_id
       and orders.d_postal_code = d.postal_code
       and upper (orders.d_city) = upper (d.city)
       and orders.o_postal_code = o.postal_code
       and upper (orders.o_city) = upper (o.city));

CREATE OR REPLACE FORCE VIEW "CWS_ORDER_LAT_LONG_DEST" ("CONS_WORKSPACE_ID", "ORDER_ID", "LATITUDE", "LONGITUDE") AS
(select cws_order.cons_workspace_id,
       cws_order.order_id,
       d.latitude,
       d.longitude
  from cws_order, orders, facility d
 where cws_order.order_id = orders.order_id
       and orders.d_facility_id = d.facility_id
union all
select cws_order.cons_workspace_id,
       cws_order.order_id,
       d.latitude,
       d.longitude
  from cws_order, orders, postal_code d
 where     cws_order.order_id = orders.order_id
       and orders.d_postal_code = d.postal_code
       and upper (orders.d_city) = upper (d.city)
       and orders.d_facility_id is null);

CREATE OR REPLACE FORCE VIEW "CWS_ORDER_LAT_LONG_ORIGIN" ("CONS_WORKSPACE_ID", "ORDER_ID", "LATITUDE", "LONGITUDE") AS
(select cws_order.cons_workspace_id,
       cws_order.order_id,
       o.latitude,
       o.longitude
  from cws_order, orders, facility o
 where cws_order.order_id = orders.order_id
       and orders.o_facility_id = o.facility_id
union all
select cws_order.cons_workspace_id,
       cws_order.order_id,
       o.latitude,
       o.longitude
  from cws_order, orders, postal_code o
 where     cws_order.order_id = orders.order_id
       and orders.o_postal_code = o.postal_code
       and upper (orders.o_city) = upper (o.city)
       and orders.o_facility_id is null);

CREATE OR REPLACE FORCE VIEW "CWS_SHIP_LAT_LANG" ("CONS_WORKSPACE_ID", "WS_SHIPMENT_ID", "O_LATITUDE", "O_LONGITUDE", "D_LATITUDE", "D_LONGITUDE") AS
(select o_lat_lang_info.cons_workspace_id,
       o_lat_lang_info.ws_shipment_id,
       o_latitude,
       o_longitude,
       d_latitude,
       d_longitude
  from (select cws_shipment.cons_workspace_id,
               cws_shipment.ws_shipment_id,
               latitude o_latitude,
               longitude o_longitude
          from facility, cws_shipment
         where facility_id =
                  (select facility_id
                     from cws_stop
                    where stop_seq = 1
                          and cws_stop.ws_shipment_id =
                                 cws_shipment.ws_shipment_id)) o_lat_lang_info,
       (select cws_shipment.cons_workspace_id,
               cws_shipment.ws_shipment_id,
               latitude d_latitude,
               longitude d_longitude
          from facility, cws_shipment, cws_stop
         where cws_stop.ws_shipment_id = cws_shipment.ws_shipment_id
               and cws_stop.stop_seq =
                      (select max (stop_seq)
                         from cws_stop
                        where cws_stop.ws_shipment_id =
                                 cws_shipment.ws_shipment_id)
               and facility.facility_id = cws_stop.facility_id) d_lat_lang_info
 where o_lat_lang_info.ws_shipment_id = d_lat_lang_info.ws_shipment_id);

CREATE OR REPLACE FORCE VIEW "DOCK_DOOR_VIEW" ("PARENT_ID", "STATUS", "LOCN_CLASS", "MAX_CAPACITY", "LOCN_ID", "IS_GUARD_HOUSE", "CURRENT_CAPACITY", "ID", "FACILITY_ID", "YARD_ID", "LOCATION_NAME", "PARENT_LOCATION_NAME", "LOCN_BRCD", "DSP_LOCN", "CHECK_DIGIT", "TC_COMPANY_ID", "YARD_NAME") AS
(SELECT DD.DOCK_ID AS PARENT_ID,
         DD.DOCK_DOOR_STATUS AS STATUS,
         LH.LOCN_CLASS AS LOCN_CLASS,
         1 AS MAX_CAPACITY,
         LH.LOCN_ID AS LOCN_ID,
         0 AS IS_GUARD_HOUSE,
         1 AS CURRENT_CAPACITY,
         DD.DOCK_DOOR_ID AS id,
         DD.FACILITY_ID AS FACILITY_ID,
         0 AS YARD_ID,
         DD.DOCK_DOOR_NAME AS LOCATION_NAME,
         DO.DOCK_NAME AS PARENT_LOCATION_NAME,
         LH.LOCN_BRCD AS LOCN_BRCD,
         LH.DSP_LOCN AS DSP_LOCN,
         LH.CHECK_DIGIT AS CHECK_DIGIT,
         DD.TC_COMPANY_ID AS TC_COMPANY_ID,
         '' AS YARD_NAME
    FROM DOCK_DOOR DD, LOCN_HDR LH, DOCK DO
   WHERE     DD.DOCK_ID = DO.DOCK_ID
         AND DD.FACILITY_ID = DO.FACILITY_ID
         AND DD.DOCK_DOOR_LOCN_ID = LH.LOCN_ID
         AND LH.LOCN_CLASS = 'Q');

CREATE OR REPLACE FORCE VIEW "DOCK_LOCATION_VIEW" ("TYPE", "LOCATION_ID", "SEQUENCE_NO", "CURRENT_QUANTITY", "DEFINED_SVG", "ZONE_ID", "ZONE_NAME", "STATUS", "TC_COMPANY_ID", "PARENT_LOC") AS
SELECT 'YZN',
      LN.location_id,
      LN.sequence_no,
      current_quantity,
      LN.is_location_defined_in_svg,
      CAST (NULL AS NUMBER),
      yard_zone_name,
      CAST (NULL AS NUMBER),
      LN.tc_company_id,
      CAST (NULL AS CHAR (20))
 FROM yard_zone yz, LOCATION LN
WHERE     LN.location_objid_pk1 = TO_CHAR (yz.yard_id)
      AND LN.location_objid_pk2 = TO_CHAR (yz.yard_zone_id)
      AND LN.ilm_object_type = 28
 UNION ALL
 SELECT 'DDR',
      LN.location_id,
      LN.sequence_no,
      current_quantity,
      LN.is_location_defined_in_svg,
      CAST (NULL AS NUMBER),
      dock_door_name,
      dock_door_status,
      LN.tc_company_id,
      dock_id
 FROM dock_door dd, LOCATION LN
WHERE     LN.location_objid_pk1 = TO_CHAR (dd.facility_id)
      AND LN.location_objid_pk2 = dd.dock_id
      AND LN.location_objid_pk3 = TO_CHAR (dd.dock_door_id)
      AND LN.tc_company_id = dd.tc_company_id
      AND LN.ilm_object_type = 8
 UNION ALL
 SELECT 'YZS',
      LN.location_id,
      LN.sequence_no,
      current_quantity,
      LN.is_location_defined_in_svg,
      yard_zone_slot_id,
      yard_zone_slot_name,
      yard_zone_slot_status,
      LN.tc_company_id,
      y.yard_zone_name
 FROM yard_zone_slot yz, LOCATION LN, yard_zone y
WHERE     LN.location_objid_pk1 = TO_CHAR (yz.yard_id)
      AND LN.location_objid_pk2 = TO_CHAR (yz.yard_zone_id)
      AND LN.location_objid_pk3 = TO_CHAR (yz.yard_zone_slot_id)
      AND LN.ilm_object_type = 32
      AND y.yard_zone_id = yz.yard_zone_id
 UNION ALL
 SELECT 'DCK',
      LN.location_id,
      LN.sequence_no,
      current_quantity,
      LN.is_location_defined_in_svg,
      CAST (NULL AS NUMBER),
      dock_id,
      CAST (NULL AS NUMBER),
      LN.tc_company_id,
      CAST (NULL AS CHAR (20))
 FROM dock dck, LOCATION LN
WHERE     LN.location_objid_pk1 = TO_CHAR (dck.facility_id)
      AND LN.location_objid_pk2 = TO_CHAR (dck.dock_id)
      AND LN.ilm_object_type = 4;

CREATE OR REPLACE FORCE VIEW "DOT_DRIVING_RULES" ("DRIVING_RULE_CFG_ID", "TC_COMPANY_ID", "SERVICE_LEVEL_ID", "LONG_DRIVING_TIME_RULE", "SHORT_DRIVING_TIME_RULE", "LONG_RESTING_TIME_RULE", "SHORT_RESTING_TIME_RULE", "WAITING_TIME_RULE", "DUTY_TIME_RULE", "TOTAL_DUTY_TIME_RULE", "SHORT_DUTY_TIME_RULE", "SHORT_DUTY_RESTING_TIME_RULE", "DISTANCE_RULE", "DISTANCE_UOM", "COUNTRY_CODE", "MARK_FOR_DELETION") AS
SELECT DSP_DR_SET.DSP_DR_CONFIGURATION_ID DRIVING_RULE_CFG_ID,
      DSP_DR_SET.TC_COMPANY_ID,
      DSP_DR_SET.SERVICE_LEVEL_ID SERVICE_LEVEL_ID,
      (SELECT TO_NUMBER (RULE_VALUE_1, 99.99) FROM
DSP_DR WHERE  DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND DSP_DR.DSP_DR_TYPE_ID
= 2 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID AND MARK_FOR_DELETION
= 0) LONG_DRIVING_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99) FROM
 DSP_DR WHERE  DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND DSP_DR.DSP_DR_TYPE_ID
 = 0 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID AND MARK_FOR_DELETION
 = 0) SHORT_DRIVING_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99) FROM
 DSP_DR WHERE  DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND DSP_DR.DSP_DR_TYPE_ID
 = 16 AND MARK_FOR_DELETION = 0) LONG_RESTING_TIME_RULE,
 (SELECT TO_NUMBER
 (RULE_VALUE_1, 99.99) FROM   DSP_DR WHERE  DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID
 AND DSP_DR.DSP_DR_TYPE_ID = 14 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID
 AND MARK_FOR_DELETION = 0) SHORT_RESTING_TIME_RULE,
 0.0 WAITING_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99)
 FROM DSP_DR WHERE DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND
 DSP_DR.DSP_DR_TYPE_ID = 8 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID
 AND MARK_FOR_DELETION = 0) DUTY_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99)
 FROM DSP_DR WHERE DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND
 DSP_DR.DSP_DR_TYPE_ID = 38 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID
 AND MARK_FOR_DELETION = 0) TOTAL_DUTY_TIME_RULE,
 (SELECT TO_NUMBER (RULE_VALUE_1, 99.99)
 FROM DSP_DR WHERE DSP_DR.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND
 DSP_DR.DSP_DR_TYPE_ID = 6 AND DSP_DR.TC_COMPANY_ID = DSP_DR_SET.TC_COMPANY_ID
 AND MARK_FOR_DELETION = 0) SHORT_DUTY_TIME_RULE,
 (SELECT TO_NUMBER (PARAMETER_VALUE,99.99)
 FROM DSP_DR_PARAMETER_SET WHERE DSP_DR_PARAMETER_SET.DSP_DR_SET_ID = DSP_DR_SET.DSP_DR_SET_ID AND
 DSP_DR_PARAMETER_SET.DSP_DR_PARAMETER_ID = 4 AND MARK_FOR_DELETION = 0)
 SHORT_DUTY_RESTING_TIME_RULE,
 0.0 DISTANCE_RULE,
 'MI' DISTANCE_UOM,
 COUNTRY_CODE,
 0 MARK_FOR_DELETION
 FROM DSP_DR_SET WHERE MARK_FOR_DELETION = 0;

CREATE OR REPLACE FORCE VIEW "FACILITYVOLUME_V" ("RFPID", "FACILITYCODE", "IB_VOLUME", "OB_VOLUME") AS
select x0.rfpid,
        x0.facilitycode,
        sum (case when (x0.direction = 'IB') then x0.volume else 0 end),
        sum (case when (x0.direction = 'OB') then x0.volume else 0 end)
   from capacityfacility_v x0
 group by x0.rfpid, x0.facilitycode;

CREATE OR REPLACE FORCE VIEW "FACILITY_V" ("FACILITY_ALIAS_ID", "TC_COMPANY_ID", "CITY", "STATE", "ZIP", "COUNTRY") AS
select x0.facility_alias_id,
      x0.tc_company_id,
      x1.city,
      x1.state_prov,
      x1.postal_code,
      x1.country_code
 from facility_alias x0, facility x1
where (x0.facility_id = x1.facility_id);

CREATE OR REPLACE FORCE VIEW "FAC_TO_FAC_SCHEDULE_VIEW" ("FACILITY_SCHEDULE_ID", "DAYOFWEEK", "O_FACILITY_ID", "O_FACILITY_ID_ALIAS", "D_FACILITY_ID", "D_FACILITY_ID_ALIAS", "TC_COMPANY_ID", "PICKUP_START_HH", "PICKUP_START_MM", "PICKUP_END_HH", "PICKUP_END_MM", "DELIVERY_START_HH", "DELIVERY_START_MM", "DELIVERY_END_HH", "DELIVERY_END_MM", "ALLOWED_NUM_OF_APPTS", "TRANSIT_DAYS", "PROTECTION_LEVEL_ID", "MOT_ID") AS
select a.facility_schedule_id,
      a.dayofweek,
      a.o_facility_id,
      b.facility_alias_id,
      a.d_facility_id,
      c.facility_alias_id,
      a.tc_company_id,
      a.pickup_start_hh,
      a.pickup_start_mm,
      a.pickup_end_hh,
      a.pickup_end_mm,
      a.delivery_start_hh,
      a.delivery_start_mm,
      a.delivery_end_hh,
      a.delivery_end_mm,
      a.allowed_num_of_appts,
      a.transit_days,
      protection_level_id,
      a.mot_id
 from fac_to_fac_schedule a
      left outer join facility_alias b
         on (a.o_facility_id = b.facility_id and b.is_primary = 1)
      left outer join facility_alias c
         on (a.d_facility_id = c.facility_id and c.is_primary = 1);

CREATE OR REPLACE FORCE VIEW "ORDERS_ORDER_MOVEMENT_VIEW" ("ORDER_ID", "SHIPMENT_ID", "ORDER_SPLIT_ID") AS
select distinct
      orders.order_id,
      stop_action_order.shipment_id,
      stop_action_order.order_split_id
 from    orders
      left outer join
         stop_action_order
      on stop_action_order.order_id = orders.order_id;

CREATE OR REPLACE FORCE VIEW "ORDERS_SHIPMENT_RELATION_VIEW" ("ORDER_ID", "TC_ORDER_ID", "DO_STATUS", "SHIPMENT_ID") AS
SELECT L.ORDER_ID,
  L.TC_ORDER_ID,
  O.DO_STATUS,
  L.SHIPMENT_ID
FROM LPN L,
  ORDERS O
WHERE L.ORDER_ID   = O.ORDER_ID
AND L.SHIPMENT_ID > -1
UNION ALL
SELECT SAO.ORDER_ID,
  O.TC_ORDER_ID,
  O.DO_STATUS,
  SAO.SHIPMENT_ID
FROM STOP_ACTION_ORDER SAO,
  ORDERS O
WHERE O.ORDER_ID = SAO.ORDER_ID AND O.DO_STATUS < 150
UNION ALL
SELECT O.ORDER_ID,
  O.TC_ORDER_ID,
  O.DO_STATUS,
  O.SHIPMENT_ID
FROM ORDERS O
WHERE O.SHIPMENT_ID > -1 AND O.DO_STATUS < 150;

CREATE OR REPLACE FORCE VIEW "ORDER_LINE_ITEM_SIZE_DISPLAY" ("ORDER_ID", "LINE_ITEM_ID", "SIZE_VALUE", "SIZE_UOM_ID", "TC_COMPANY_ID", "MASTER_ORDER_ID", "MO_LINE_ITEM_ID") AS
select order_id,
      line_item_id,
      size_value,
      size_uom_id,
      tc_company_id,
      master_order_id,
      mo_line_item_id
 from order_line_item unpivot ((size_value, size_uom_id)
                      for x
                      in  ((planned_weight, weight_uom_id_base),
                          (planned_volume, volume_uom_id_base),
                          (order_qty, qty_uom_id_base),
                          (size1_value, size1_uom_id),
                          (size2_value, size2_uom_id)))
where is_cancelled = '0'
 union
 select olis.order_id,
      olis.line_item_id,
      size_value,
      size_uom_id,
      olis.tc_company_id,
      master_order_id,
      mo_line_item_id
 from order_line_item_size olis, order_line_item oli
where     olis.order_id = oli.order_id
      and olis.tc_company_id = oli.tc_company_id
      and olis.line_item_id = oli.line_item_id
      and oli.is_cancelled = '0';

CREATE OR REPLACE FORCE VIEW "ORDER_LINE_ITEM_SIZE_VIEW" ("ORDER_ID", "LINE_ITEM_ID", "RECEIVED_SIZE_VALUE", "SIZE_VALUE", "SIZE_UOM_ID", "TC_COMPANY_ID", "MASTER_ORDER_ID", "MO_LINE_ITEM_ID") AS
select order_id,
      line_item_id,
      received_size_value,
      size_value,
      size_uom_id,
      tc_company_id,
      master_order_id,
      mo_line_item_id
 from order_line_item unpivot ((received_size_value,
                                size_value,
                                size_uom_id)
                      for x
                      in  ((received_weight,
                            planned_weight,
                            weight_uom_id_base),
                          (received_volume,
                           planned_volume,
                           volume_uom_id_base),
                          (received_qty, order_qty, qty_uom_id_base),
                          (received_size1, size1_value, size1_uom_id),
                          (received_size2, size2_value, size2_uom_id)))
where is_cancelled = '0'
 union
 select olis.order_id,
      olis.line_item_id,
      received_size_value,
      size_value,
      size_uom_id,
      olis.tc_company_id,
      master_order_id,
      mo_line_item_id
 from order_line_item_size olis, order_line_item oli
where     olis.order_id = oli.order_id
      and olis.tc_company_id = oli.tc_company_id
      and olis.line_item_id = oli.line_item_id
      and oli.is_cancelled = '0';

CREATE OR REPLACE FORCE VIEW "ORDER_LI_SIZES_VIEW" ("ORDER_ID", "LINE_ITEM_ID", "RECEIVED_SIZE_VALUE", "SIZE_VALUE", "SIZE_UOM_ID", "TC_COMPANY_ID", "MASTER_ORDER_ID", "MO_LINE_ITEM_ID") AS
select distinct order_id,
               line_item_id,
               received_size_value,
               size_value,
               size_uom_id,
               tc_company_id,
               master_order_id,
               mo_line_item_id
 from order_line_item unpivot ((received_size_value,
                                size_value,
                                size_uom_id)
                      for x
                      in  ((received_weight,
                            planned_weight,
                            weight_uom_id_base),
                          (received_volume,
                           planned_volume,
                           volume_uom_id_base),
                          (received_qty, order_qty, qty_uom_id_base),
                          (received_size1, size1_value, size1_uom_id),
                          (received_size2, size2_value, size2_uom_id)))
where is_cancelled = '0';

CREATE OR REPLACE FORCE VIEW "ORDER_SIZE" ("ORDER_ID", "TC_COMPANY_ID", "SIZE_UOM_ID", "SIZE_VALUE") AS
select order_id,
        tc_company_id,
        size_uom_id,
        sum (size_value) as size_value
   from order_line_item_size_view
 group by order_id, tc_company_id, size_uom_id;

CREATE OR REPLACE FORCE VIEW "ORDER_SIZE_DISPLAY" ("ORDER_ID", "TC_COMPANY_ID", "SIZE_UOM_ID", "SIZE_VALUE") AS
select order_id,
        tc_company_id,
        size_uom_id,
        sum (size_value) as size_value
   from order_line_item_size_display
 group by order_id, tc_company_id, size_uom_id;

CREATE OR REPLACE FORCE VIEW "ORDER_SIZE_VIEW" ("ORDER_ID", "SIZE_VALUE", "SIZE_UOM_ID", "SUMMARY_VIEW_FLAG") AS
select olis.order_id,
        sum (olis.size_value),
        olis.size_uom_id,
        sud.summary_view_flag
   from order_line_item_size_view olis, size_uom_display sud
  where olis.size_uom_id = sud.size_uom_id
        and olis.tc_company_id = sud.tc_company_id
 group by olis.order_id, olis.size_uom_id, sud.summary_view_flag;

CREATE OR REPLACE FORCE VIEW "ORDER_SPLIT_LINE_SIZE_VIEW" ("ORDER_ID", "ORDER_SPLIT_ID", "LINE_ITEM_ID", "SIZE_VALUE", "SIZE_UOM_ID") AS
select order_id,
      order_split_id,
      line_item_id,
      size_value,
      size_uom_id
 from order_split_line_item unpivot ((size_value, size_uom_id)
                            for x
                            in  ((planned_weight, weight_uom_id_base),
                                (planned_volume, volume_uom_id_base),
                                (order_qty, qty_uom_id_base),
                                (size1_value, size1_uom_id),
                                (size2_value, size2_uom_id)))
 union
 select order_id,
      order_split_id,
      line_item_id,
      split_size_value,
      split_size_uom_id
 from order_split_size;

CREATE OR REPLACE FORCE VIEW "ORDER_SPLIT_SIZE_VIEW" ("ORDER_SPLIT_ID", "SPLIT_SIZE_VALUE", "SPLIT_SIZE_UOM_ID") AS
select oss.order_split_id, sum (oss.size_value), oss.size_uom_id
   from order_split_line_size_view oss
 group by oss.order_split_id, oss.size_uom_id;

CREATE OR REPLACE FORCE VIEW "PARTNER_SHIPPER_VIEW" ("PARTNER_COMPANY_ID", "PARTNER_COMPANY_NAME", "SHIPPER_COMPANY_ID", "SHIPPER_COMPANY_NAME") AS
select distinct bp.bp_company_id as partner_company_id,
               co1.company_name as partner_company_name,
               bp.tc_company_id as shipper_company_id,
               co2.company_name as shipper_company_name
 from business_partner bp, company co1, company co2
where     bp.mark_for_deletion = 0
      and bp.bp_company_id = co1.company_id
      and bp.tc_company_id = co2.company_id;

CREATE OR REPLACE FORCE VIEW "PO_BOOK_VIEW" ("BOOKING_ID", "TC_PURCHASE_ORDER_ID") AS
select booking_id,
      (select tc_purchase_orders_id
         from purchase_orders
        where purchase_orders_id =
                 booking_purchase_order.purchase_orders_id)
         tc_purchase_order_id
 from booking_purchase_order;

CREATE OR REPLACE FORCE VIEW "PO_LINE_ITEM_SIZE_VIEW" ("PURCHASE_ORDERS_ID", "PURCHASE_ORDERS_LINE_ITEM_ID", "RECEIVED_SIZE_VALUE", "SIZE_VALUE", "SIZE_UOM_ID", "TC_COMPANY_ID") AS
select purchase_orders_id,
      purchase_orders_line_item_id,
      received_size_value,
      size_value,
      size_uom_id,
      tc_company_id
 from purchase_orders_line_item unpivot ((received_size_value,
                                          size_value,
                                          size_uom_id)
                                for x
                                in  ((received_weight,
                                      planned_weight,
                                      weight_uom_id_base),
                                    (received_volume,
                                     planned_volume,
                                     volume_uom_id_base),
                                    (received_qty,
                                     order_qty,
                                     qty_uom_id_base),
                                    (received_size1,
                                     size1_value,
                                     size1_uom_id),
                                    (received_size2,
                                     size2_value,
                                     size2_uom_id)))
where is_cancelled = '0'
 union
 select olis.purchase_orders_id,
      olis.purchase_orders_line_item_id,
      received_size_value,
      size_value,
      size_uom_id,
      olis.tc_company_id
 from purchase_orders_line_item_size olis, purchase_orders_line_item oli
where olis.purchase_orders_id = oli.purchase_orders_id
      and olis.tc_company_id = oli.tc_company_id
      and olis.purchase_orders_line_item_id =
             oli.purchase_orders_line_item_id
      and oli.is_cancelled = '0';

CREATE OR REPLACE FORCE VIEW "RATE_LANE_DTL_RATE_VIEW_1DTLRW" ("TC_COMPANY_ID", "LANE_ID", "RATING_LANE_DTL_SEQ", "RLD_RATE_SEQ", "MINIMUM_SIZE", "MAXIMUM_SIZE", "SIZE_UOM_ID", "RATE_CALC_METHOD", "RATE_UOM", "RATE", "MINIMUM_RATE", "CURRENCY_CODE", "TARIFF_ID", "MIN_DISTANCE", "MAX_DISTANCE", "DISTANCE_UOM", "SUPPORTS_MSTL", "HAS_BH", "HAS_RT", "MIN_COMMODITY", "MAX_COMMODITY", "PARCEL_TIER", "EXCESS_WT_RATE", "COMMODITY_CODE_ID", "PAYEE_CARRIER_ID", "MAX_RANGE_COMMODITY_CODE_ID") AS
select tc_company_id,
      lane_id,
      rating_lane_dtl_seq,
      rld_rate_seq,
      minimum_size,
      maximum_size,
      size_uom_id,
      rate_calc_method,
      rate_uom,
      rate,
      minimum_rate,
      currency_code,
      tariff_id,
      min_distance,
      max_distance,
      distance_uom,
      supports_mstl,
      has_bh,
      has_rt,
      min_commodity,
      max_commodity,
      parcel_tier,
      excess_wt_rate,
      commodity_code_id,
      payee_carrier_id,
      max_range_commodity_code_id
 from rating_lane_dtl_rate rldr
where rldr.rld_rate_seq =
         (select min (rldr_1.rld_rate_seq)
            from rating_lane_dtl_rate rldr_1
           where rldr.tc_company_id = rldr_1.tc_company_id
                 and rldr.lane_id = rldr_1.lane_id
                 and rldr.rating_lane_dtl_seq =
                        rldr_1.rating_lane_dtl_seq);

CREATE OR REPLACE FORCE VIEW "RATE_LANE_DTL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "RATING_LANE_DTL_SEQ", "CARRIER_CODE", "CARRIER_ID", "CARRIER_CODE_STATUS", "IS_BUDGETED", "MOT", "MOT_ID", "EQUIPMENT_CODE", "EQUIPMENT_ID", "SERVICE_LEVEL", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL", "PROTECTION_LEVEL_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "EFF_DT", "EXP_DT", "HAS_ERRORS", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "SCNDR_CARRIER_CODE", "SCNDR_CARRIER_ID", "RATING_LANE_DTL_STATUS", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5", "PACKAGE_ID", "PACKAGE_NAME", "VOYAGE") AS
select rld.tc_company_id,
      rld.lane_id,
      rld.rating_lane_dtl_seq,
      nvl (cc.carrier_code, 'ALL'),                             --Was NULL
      rld.carrier_id,
      cc.carrier_code_status,                                   --Was NULL
      rld.is_budgeted,
      nvl (m.mot, 'ALL'),                                       --Was NULL
      rld.mot_id,
      nvl (eq.equipment_code, 'ALL'),                           --Was NULL
      rld.equipment_id,
      nvl (sl.service_level, 'ALL'),                            --Was NULL
      rld.service_level_id,
      nvl (pl.protection_level, 'ALL'),                         --Was NULL
      rld.protection_level_id,
      rld.effective_dt,
      rld.expiration_dt,
      coalesce (rld.effective_dt, to_date ('01-JAN-1950', 'dd-mon-yyyy')),
      coalesce (rld.expiration_dt, to_date ('31-DEC-49')),
      case rld.rating_lane_dtl_status when 4 then 1 else 0 end
         as has_errors,
      rld.last_updated_source_type,
      rld.last_updated_source,
      rld.last_updated_dttm,
      null,
      rld.scndr_carrier_id,
      rld.rating_lane_dtl_status,
      rld.o_ship_via,
      rld.d_ship_via,
      rld.contract_number,
      rld.custom_text1,
      rld.custom_text2,
      rld.custom_text3,
      rld.custom_text4,
      rld.custom_text5,
      rld.package_id,
      rld.package_name,
      rld.voyage
 from rating_lane_dtl rld
      left outer join carrier_code cc
         on (rld.carrier_id = cc.carrier_id)
      left outer join equipment eq
         on (rld.equipment_id = eq.equipment_id)
      left outer join mot m
         on (rld.mot_id = m.mot_id)
      left outer join service_level sl
         on (rld.service_level_id = sl.service_level_id)
      left outer join protection_level pl
         on (rld.protection_level_id = pl.protection_level_id)
where cc.carrier_id = rld.carrier_id and rld.rating_lane_dtl_status <> 2
 union all
 select irld.tc_company_id,
      rl.lane_id,
      irld.rating_lane_dtl_seq,
      nvl (irld.carrier_code, 'ALL'),
      icc.carrier_id,                                           --Was NULL
      icc.carrier_code_status,
      irld.is_budgeted,
      coalesce (irld.mot, 'ALL'),
      im.mot_id,
      --Was NULL                                                                                                        -- MOT_ID is not present in IMPORT_RATING_LANE, so we are using NULL
      coalesce (irld.equipment_code, 'ALL'),
      ieq.equipment_id,
      --Was NULL                                                                                                       -- EQUIPMENT_ID is not present in IMPORT_RATING_LANE, so we are using NULL
      coalesce (irld.service_level, 'ALL'),
      isl.service_level_id,
      --Was NULL                                                                                                      -- SERVICE_LEVEL_ID is not present in IMPORT_RATING_LANE, so we are using NULL
      coalesce (irld.protection_level, 'ALL'),
      ipl.protection_level_id,
      --Was NULL                                                                                                -- PROTECTION_LEVEL_ID is not present in IMPORT_RATING_LANE, so we are using NULL
      irld.effective_dt,
      irld.expiration_dt,
      coalesce (irld.effective_dt,
                to_date ('01-JAN-1950', 'dd-mon-yyyy')),
      coalesce (irld.expiration_dt, to_date ('31-DEC-49')),
      case irld.rating_lane_dtl_status when 4 then 1 else 0 end
         as has_errors,
      irld.last_updated_source_type,
      irld.last_updated_source,
      irld.last_updated_dttm,
      irld.scndr_carrier_code,
      null,
      irld.rating_lane_dtl_status,
      irld.o_ship_via,
      irld.d_ship_via,
      irld.contract_number,
      irld.custom_text1,
      irld.custom_text2,
      irld.custom_text3,
      irld.custom_text4,
      irld.custom_text5,
      irld.package_id,
      irld.package_name,
      irld.voyage
 from import_rating_lane irl
      join import_rating_lane_dtl irld
         on irl.lane_id = irld.lane_id
            and irl.tc_company_id = irld.tc_company_id
      join rating_lane rl
         on rl.tc_company_id = irl.tc_company_id
            and rl.lane_id = irl.rating_lane_id
      left outer join carrier_code icc
         on (irld.carrier_id = icc.carrier_id)
      left outer join equipment ieq
         on (irld.equipment_id = ieq.equipment_id)
      left outer join mot im
         on (irld.mot_id = im.mot_id)
      left outer join service_level isl
         on (irld.service_level_id = isl.service_level_id)
      left outer join protection_level ipl
         on (irld.protection_level_id = ipl.protection_level_id)
 union all
 select irld1.tc_company_id,
      irl1.rating_lane_id,
      irld1.rating_lane_dtl_seq,
      irld1.carrier_code,
      irld1.carrier_id,                                         --Was NULL
      null,                                                     --Was NULL
      irld1.is_budgeted,
      coalesce (irld1.mot, 'ALL'),
      im1.mot_id,                                               --Was NULL
      coalesce (irld1.equipment_code, 'ALL'),
      ieq1.equipment_id,                                        --Was NULL
      coalesce (irld1.service_level, 'ALL'),
      isl1.service_level_id,                                    --Was NULL
      coalesce (irld1.protection_level, 'ALL'),
      ipl1.protection_level_id,                                 --Was NULL
      irld1.effective_dt,
      irld1.expiration_dt,
      coalesce (irld1.effective_dt,
                to_date ('01-JAN-1950', 'dd-mon-yyyy')),
      coalesce (irld1.expiration_dt, to_date ('31-DEC-49')),
      case irld1.rating_lane_dtl_status when 4 then 1 else 0 end
         as has_errors,
      irld1.last_updated_source_type,
      irld1.last_updated_source,
      irld1.last_updated_dttm,
      irld1.scndr_carrier_code,
      null,
      irld1.rating_lane_dtl_status,
      irld1.o_ship_via,
      irld1.d_ship_via,
      irld1.contract_number,
      irld1.custom_text1,
      irld1.custom_text2,
      irld1.custom_text3,
      irld1.custom_text4,
      irld1.custom_text5,
      irld1.package_id,
      irld1.package_name,
      irld1.voyage
 from import_rating_lane irl1
      join import_rating_lane_dtl irld1
         on irl1.lane_id = irld1.lane_id
            and irl1.tc_company_id = irld1.tc_company_id
      left outer join carrier_code icc1
         on (irld1.carrier_id = icc1.carrier_id)
      left outer join equipment ieq1
         on (irld1.equipment_id = ieq1.equipment_id)
      left outer join mot im1
         on (irld1.mot_id = im1.mot_id)
      left outer join service_level isl1
         on (irld1.service_level_id = isl1.service_level_id)
      left outer join protection_level ipl1
         on (irld1.protection_level_id = ipl1.protection_level_id)
where not exists
             (select 1
                from rating_lane r
               where irl1.tc_company_id = r.tc_company_id
                     and irl1.rating_lane_id = r.lane_id)
      and irl1.lane_status = 4;

CREATE OR REPLACE FORCE VIEW "RATE_LANE_VALID_DTL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "RATING_LANE_DTL_SEQ", "CARRIER_ID", "CARRIER_CODE_STATUS", "MOT_ID", "EQUIPMENT_ID", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "EFF_DT", "EXP_DT", "HAS_ERRORS", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5") AS
select rld.tc_company_id,
      rld.lane_id,
      rld.rating_lane_dtl_seq,
      rld.carrier_id,
      cc.carrier_code_status,
      nvl (rld.mot_id, -1),
      nvl (rld.equipment_id, -1),
      nvl (rld.service_level_id, -1),
      nvl (rld.protection_level_id, -1),
      rld.effective_dt,
      rld.expiration_dt,
      nvl (rld.effective_dt, to_date ('01-JAN-1950', 'DD-MON-YYYY')),
      nvl (rld.expiration_dt, to_date ('31-DEC-2049', 'DD-MON-YYYY')),
      decode (rld.rating_lane_dtl_status, 4, 1, 0) has_errors,
      rld.last_updated_source_type,
      rld.last_updated_source,
      rld.last_updated_dttm,
      rld.o_ship_via,
      rld.d_ship_via,
      rld.contract_number,
      rld.custom_text1,
      rld.custom_text2,
      rld.custom_text3,
      rld.custom_text4,
      rld.custom_text5
 from rating_lane_dtl rld, carrier_code cc
where     rld.tc_company_id = cc.tc_company_id
      and rld.carrier_id = cc.carrier_id
      and rld.rating_lane_dtl_status = 0;

CREATE OR REPLACE FORCE VIEW "RATE_LANE_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_NAME", "BILLING_METHOD", "INCOTERM_ID", "CUSTOMER_ID") AS
select l.tc_company_id,
      l.lane_id,
      l.lane_hierarchy,
      l.lane_status,
      l.o_loc_type,
      l.o_facility_id,
      l.o_facility_alias_id,
      upper (l.o_city),
      l.o_state_prov,
      upper (l.o_county),
      l.o_postal_code,
      l.o_country_code,
      l.o_zone_id,
      '',
      l.d_loc_type,
      l.d_facility_id,
      l.d_facility_alias_id,
      upper (l.d_city),
      l.d_state_prov,
      upper (l.d_county),
      l.d_postal_code,
      l.d_country_code,
      l.d_zone_id,
      '',
      decode (l.lane_status, 4, 1, 0) has_errors,
      coalesce (
         (select sign (max (decode (id.rating_lane_dtl_status, 4, 1, 0)))
            from import_rating_lane ir, import_rating_lane_dtl id
           where (    l.lane_id = ir.rating_lane_id
                  and l.tc_company_id = ir.tc_company_id
                  and ir.lane_id = id.lane_id
                  and ir.tc_company_id = id.tc_company_id)),
         0)
         dtl_has_errors,
      l.created_source_type,
      l.created_source,
      l.created_dttm,
      l.last_updated_source_type,
      l.last_updated_source,
      l.last_updated_dttm,
      l.lane_name,
      l.billing_method,
      l.incoterm_id,
      l.customer_id
 from rating_lane l
 union all
 select i.tc_company_id,
      i.rating_lane_id,
      i.lane_hierarchy,
      i.lane_status,
      i.o_loc_type,
      i.o_facility_id,
      i.o_facility_alias_id,
      upper (i.o_city),
      i.o_state_prov,
      upper (i.o_county),
      i.o_postal_code,
      i.o_country_code,
      i.o_zone_id,
      i.o_zone_name,
      i.d_loc_type,
      i.d_facility_id,
      i.d_facility_alias_id,
      upper (i.d_city),
      i.d_state_prov,
      upper (i.d_county),
      i.d_postal_code,
      i.d_country_code,
      i.d_zone_id,
      i.d_zone_name,
      decode (i.lane_status, 4, 1, 0) has_errors,
      coalesce (
         (select max (decode (d.rating_lane_dtl_status, 4, 1, 0))
            from import_rating_lane_dtl d
           where d.lane_id = i.lane_id
                 and d.tc_company_id = i.tc_company_id),
         0)
         dtl_has_errors,
      i.created_source_type,
      i.created_source,
      i.created_dttm,
      i.last_updated_source_type,
      i.last_updated_source,
      i.last_updated_dttm,
      '',
      cast (null as int),
      cast (null as int),
      cast (null as int)
 from import_rating_lane i
where i.lane_status = 4
      and not exists
                 (select 1
                    from rating_lane r
                   where i.tc_company_id = r.tc_company_id
                         and i.rating_lane_id = r.lane_id);

CREATE OR REPLACE FORCE VIEW "RATING_LANE" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "O_SEARCH_LOCATION", "D_SEARCH_LOCATION", "LANE_UNIQUE_ID", "LANE_NAME", "BILLING_METHOD", "INCOTERM_ID", "CUSTOMER_ID") AS
select tc_company_id,
      lane_id,
      lane_hierarchy,
      lane_status,
      o_loc_type,
      o_facility_id,
      o_facility_alias_id,
      o_city,
      o_state_prov,
      o_county,
      o_postal_code,
      o_country_code,
      o_zone_id,
      '',
      d_loc_type,
      d_facility_id,
      d_facility_alias_id,
      d_city,
      d_state_prov,
      d_county,
      d_postal_code,
      d_country_code,
      d_zone_id,
      '',
      created_source_type,
      created_source,
      created_dttm,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      o_search_location,
      d_search_location,
      lane_unique_id,
      lane_name,
      billing_method,
      incoterm_id,
      customer_id
 from comb_lane
where is_rating = 1;

CREATE OR REPLACE FORCE VIEW "RATING_LANE_DTL" ("TC_COMPANY_ID", "LANE_ID", "RATING_LANE_DTL_SEQ", "CARRIER_ID", "MOT_ID", "EQUIPMENT_ID", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL_ID", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "RATING_LANE_DTL_STATUS", "EFFECTIVE_DT", "EXPIRATION_DT", "IS_BUDGETED", "SCNDR_CARRIER_ID", "O_SHIP_VIA", "D_SHIP_VIA", "CONTRACT_NUMBER", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5", "PACKAGE_ID", "PACKAGE_NAME", "SAILING_SCHEDULE_NAME", "VOYAGE") AS
select tc_company_id,
      lane_id,
      lane_dtl_seq rating_lane_dtl_seq,
      carrier_id,
      mot_id,
      equipment_id,
      service_level_id,
      protection_level_id,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      lane_dtl_status rating_lane_dtl_status,
      effective_dt,
      expiration_dt,
      is_budgeted,
      scndr_carrier_id,
      o_ship_via,
      d_ship_via,
      contract_number,
      custom_text1,
      custom_text2,
      custom_text3,
      custom_text4,
      custom_text5,
      package_id,
      package_name,
      sailing_schedule_name,
      voyage
 from comb_lane_dtl
where is_rating = 1;

CREATE OR REPLACE FORCE VIEW "RETURN_CENTERS" ("FACILITY_ID", "FACILITY_ALIAS_ID", "FACILITY_NAME", "TC_COMPANY_ID", "MARK_FOR_DELETION") AS
(select FAC.FACILITY_ID, FAC_ALIAS.FACILITY_ALIAS_ID, FAC_ALIAS.FACILITY_NAME, FAC.TC_COMPANY_ID, FAC.MARK_FOR_DELETION from facility fac, FACILITY_ALIAS fac_alias where FAC.FACILITY_ID = FAC_ALIAS.FACILITY_ID and FAC.FACILITY_TYPE_BITS >= 512 and FAC.MARK_FOR_DELETION = 0);

CREATE OR REPLACE FORCE VIEW "RETURN_ORDER_TYPE" ("ORDER_TYPE_ID", "ORDER_TYPE", "DESCRIPTION", "TC_COMPANY_ID", "MARK_FOR_DELETION") AS
SELECT ORDER_TYPE_ID, ORDER_TYPE, DESCRIPTION, TC_COMPANY_ID, MARK_FOR_DELETION FROM ORDER_TYPE WHERE MARK_FOR_DELETION = 0 AND CHANNEL_TYPE = 30;

CREATE OR REPLACE FORCE VIEW "RETURN_SHIP_VIA" ("SHIP_VIA_ID", "SHIP_VIA", "TC_COMPANY_ID", "MARKED_FOR_DELETION", "ENTITY_TYPE") AS
select distinct V.SHIP_VIA_ID,
V.SHIP_VIA,V.TC_COMPANY_ID,V.MARKED_FOR_DELETION,S.ENTITY_TYPE from A_SHIPPING_CHARGE_RULE S,SHIP_VIA V
where S.SHIP_VIA_ID = V.SHIP_VIA_ID AND S.START_DATE <= TRUNC(sysdate) AND S.END_DATE >= TRUNC(sysdate) and S.ENTITY_TYPE = 2 and S.ITEM_ID = -1 ;

CREATE OR REPLACE FORCE VIEW "RG_LANE" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "RG_QUALIFIER", "FREQUENCY", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "O_SEARCH_LOCATION", "D_SEARCH_LOCATION", "LANE_NAME", "CUSTOMER_ID", "BILLING_METHOD", "INCOTERM_ID", "ROUTE_TO", "ROUTE_TYPE_1", "ROUTE_TYPE_2", "NO_RATING", "USE_FASTEST", "USE_PREFERENCE_BONUS") AS
select tc_company_id,
      lane_id,
      lane_hierarchy,
      lane_status,
      o_loc_type,
      o_facility_id,
      o_facility_alias_id,
      o_city,
      o_state_prov,
      o_county,
      o_postal_code,
      o_country_code,
      o_zone_id,
      '',
      d_loc_type,
      d_facility_id,
      d_facility_alias_id,
      d_city,
      d_state_prov,
      d_county,
      d_postal_code,
      d_country_code,
      d_zone_id,
      '',
      rg_qualifier,
      frequency,
      created_source_type,
      created_source,
      created_dttm,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      o_search_location,
      d_search_location,
      lane_name,
      customer_id,
      billing_method,
      incoterm_id,
      route_to,
      route_type_1,
      route_type_2,
      no_rating,
      use_fastest,
      use_preference_bonus
 from comb_lane
where is_routing = 1;

CREATE OR REPLACE FORCE VIEW "RG_LANE_DTL" ("TC_COMPANY_ID", "LANE_ID", "RG_LANE_DTL_SEQ", "CARRIER_ID", "MOT_ID", "EQUIPMENT_ID", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL_ID", "TIER_ID", "RANK", "REP_TP_FLAG", "WEEKLY_CAPACITY", "DAILY_CAPACITY", "CAPACITY_SUN", "CAPACITY_MON", "CAPACITY_TUE", "CAPACITY_WED", "CAPACITY_THU", "CAPACITY_FRI", "CAPACITY_SAT", "WEEKLY_COMMITMENT", "DAILY_COMMITMENT", "COMMITMENT_SUN", "COMMITMENT_MON", "COMMITMENT_TUE", "COMMITMENT_WED", "COMMITMENT_THU", "COMMITMENT_FRI", "COMMITMENT_SAT", "WEEKLY_COMMIT_PCT", "DAILY_COMMIT_PCT", "COMMIT_PCT_SUN", "COMMIT_PCT_MON", "COMMIT_PCT_TUE", "COMMIT_PCT_WED", "COMMIT_PCT_THU", "COMMIT_PCT_FRI", "COMMIT_PCT_SAT", "MONTHLY_CAPACITY", "YEARLY_CAPACITY", "MONTHLY_COMMITMENT", "YEARLY_COMMITMENT", "MONTHLY_COMMIT_PCT", "YEARLY_COMMIT_PCT", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "IS_PREFERRED", "LANE_DTL_STATUS", "EFFECTIVE_DT", "EXPIRATION_DT", "SCNDR_CARRIER_ID", "TT_TOLERANCE_FACTOR", "PACKAGE_ID", "PACKAGE_NAME", "SIZE_UOM_ID", "SP_LPN_TYPE", "SP_MIN_LPN_COUNT", "SP_MAX_LPN_COUNT", "SP_MIN_WEIGHT", "SP_MAX_WEIGHT", "SP_MIN_VOLUME", "SP_MAX_VOLUME", "SP_MIN_LINEAR_FEET", "SP_MAX_LINEAR_FEET", "SP_MIN_MONETARY_VALUE", "SP_MAX_MONETARY_VALUE", "SP_CURRENCY_CODE", "CUBING_INDICATOR", "NO_RATING", "IS_FASTEST", "IS_USE_PREFERENCE_BONUS", "PREFERENCE_BONUS_VALUE", "OVERRIDE_CODE", "RFP_PACKAGE_ID", "RFP_PACKAGE_REFERENCE", "HAS_SHIPPING_PARAM", "RG_SHIPPING_PARAM_ID", "VOYAGE", "REJECT_FURTHER_SHIPMENTS", "CARRIER_REJECT_PERIOD") AS
SELECT TC_COMPANY_ID,
      LANE_ID,
      LANE_DTL_SEQ RG_LANE_DTL_SEQ,
      CARRIER_ID,
      MOT_ID,
      EQUIPMENT_ID,
      SERVICE_LEVEL_ID,
      PROTECTION_LEVEL_ID,
      TIER_ID,
      RANK,
      REP_TP_FLAG,
      WEEKLY_CAPACITY,
      DAILY_CAPACITY,
      CAPACITY_SUN,
      CAPACITY_MON,
      CAPACITY_TUE,
      CAPACITY_WED,
      CAPACITY_THU,
      CAPACITY_FRI,
      CAPACITY_SAT,
      WEEKLY_COMMITMENT,
      DAILY_COMMITMENT,
      COMMITMENT_SUN,
      COMMITMENT_MON,
      COMMITMENT_TUE,
      COMMITMENT_WED,
      COMMITMENT_THU,
      COMMITMENT_FRI,
      COMMITMENT_SAT,
      WEEKLY_COMMIT_PCT,
      DAILY_COMMIT_PCT,
      COMMIT_PCT_SUN,
      COMMIT_PCT_MON,
      COMMIT_PCT_TUE,
      COMMIT_PCT_WED,
      COMMIT_PCT_THU,
      COMMIT_PCT_FRI,
      COMMIT_PCT_SAT,
      MONTLY_CAPACITY MONTHLY_CAPACITY,
      YEARLY_CAPACITY,
      MONTLY_COMMITMENT MONTHLY_COMMITMENT,
      YEARLY_COMMITMENT,
      MONTLY_COMMIT_PCT MONTHLY_COMMIT_PCT,
      YEARLY_COMMIT_PCT,
      LAST_UPDATED_SOURCE_TYPE,
      LAST_UPDATED_SOURCE,
      LAST_UPDATED_DTTM,
      IS_PREFERRED,
      LANE_DTL_STATUS,
      EFFECTIVE_DT,
      EXPIRATION_DT,
      SCNDR_CARRIER_ID,
      TT_TOLERANCE_FACTOR,
      PACKAGE_ID,
      PACKAGE_NAME,
      SIZE_UOM_ID,
      SP_LPN_TYPE,
      SP_MIN_LPN_COUNT,
      SP_MAX_LPN_COUNT,
      SP_MIN_WEIGHT,
      SP_MAX_WEIGHT,
      SP_MIN_VOLUME,
      SP_MAX_VOLUME,
      SP_MIN_LINEAR_FEET,
      SP_MAX_LINEAR_FEET,
      SP_MIN_MONETARY_VALUE,
      SP_MAX_MONETARY_VALUE,
      SP_CURRENCY_CODE,
      CUBING_INDICATOR,
      NO_RATING,
      IS_FASTEST,
      IS_USE_PREFERENCE_BONUS,
      PREFERENCE_BONUS_VALUE,
      OVERRIDE_CODE,
      RFP_PACKAGE_ID,
      rfP_PACKAGE_REFERENCE,
      HAS_SHIPPING_PARAM,
      RG_SHIPPING_PARAM_ID,
		  VOYAGE,
	  REJECT_FURTHER_SHIPMENTS,
	  CARRIER_REJECT_PERIOD
 FROM COMB_LANE_DTL
WHERE IS_ROUTING = 1;

CREATE OR REPLACE FORCE VIEW "RG_LANE_DTL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "RG_LANE_DTL_SEQ", "CARRIER_CODE", "CARRIER_ID", "CARRIER_CODE_STATUS", "MOT", "MOT_ID", "EQUIPMENT_CODE", "EQUIPMENT_ID", "SERVICE_LEVEL", "SERVICE_LEVEL_ID", "PROTECTION_LEVEL", "PROTECTION_LEVEL_ID", "TIER_ID", "RANK", "REP_TP_FLAG", "IS_PREFERRED", "TP_COMPANY_ID", "WEEKLY_CAPACITY", "DAILY_CAPACITY", "CAPACITY_SUN", "CAPACITY_MON", "CAPACITY_TUE", "CAPACITY_WED", "CAPACITY_THU", "CAPACITY_FRI", "CAPACITY_SAT", "WEEKLY_COMMITMENT", "DAILY_COMMITMENT", "COMMITMENT_SUN", "COMMITMENT_MON", "COMMITMENT_TUE", "COMMITMENT_WED", "COMMITMENT_THU", "COMMITMENT_FRI", "COMMITMENT_SAT", "WEEKLY_COMMIT_PCT", "DAILY_COMMIT_PCT", "COMMIT_PCT_SUN", "COMMIT_PCT_MON", "COMMIT_PCT_TUE", "COMMIT_PCT_WED", "COMMIT_PCT_THU", "COMMIT_PCT_FRI", "COMMIT_PCT_SAT", "EFFECTIVE_DT", "EXPIRATION_DT", "EFF_DT", "EXP_DT", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "HAS_ERRORS", "SCNDR_CARRIER_CODE", "SCNDR_CARRIER_ID", "LANE_DTL_STATUS", "TT_TOLERANCE_FACTOR", "MONTHLY_CAPACITY", "YEARLY_CAPACITY", "MONTHLY_COMMITMENT", "YEARLY_COMMITMENT", "MONTHLY_COMMIT_PCT", "YEARLY_COMMIT_PCT", "CUSTOM_TEXT1", "CUSTOM_TEXT2", "CUSTOM_TEXT3", "CUSTOM_TEXT4", "CUSTOM_TEXT5", "PACKAGE_ID", "PACKAGE_NAME", "SIZE_UOM_ID", "RG_SHIPPING_PARAM_ID", "SP_LPN_TYPE", "SP_MIN_LPN_COUNT", "SP_MAX_LPN_COUNT", "SP_MIN_WEIGHT", "SP_MAX_WEIGHT", "SP_MIN_VOLUME", "SP_MAX_VOLUME", "SP_MIN_LINEAR_FEET", "SP_MAX_LINEAR_FEET", "SP_MIN_MONETARY_VALUE", "SP_MAX_MONETARY_VALUE", "SP_CURRENCY_CODE", "CUBING_INDICATOR", "PREFERENCE_BONUS_VALUE", "OVERRIDE_CODE", "RFP_PACKAGE_ID", "RFP_PACKAGE_REFERENCE", "HAS_SHIPPING_PARAM", "VOYAGE", "REJECT_FURTHER_SHIPMENTS", "CARRIER_REJECT_PERIOD") AS
SELECT rld.TC_COMPANY_ID TC_COMPANY_ID,
      rld.LANE_ID LANE_ID,
      rld.LANE_DTL_SEQ RG_LANE_DTL_SEQ,
      '' CARRIER_CODE,
      rld.CARRIER_ID CARRIER_ID,
      cc.CARRIER_CODE_STATUS CARRIER_CODE_STATUS,
      '' MOT,
      rld.MOT_ID MOT_ID,
      '' EQUIPMENT_CODE,
      rld.EQUIPMENT_ID EQUIPMENT_ID,
      '' SERVICE_LEVEL,
      rld.SERVICE_LEVEL_ID SERVICE_LEVEL_ID,
      '' PROTECTION_LEVEL,
      rld.PROTECTION_LEVEL_ID PROTECTION_LEVEL_ID,
      NVL (rld.TIER_ID, 'ALL') TIER_ID,
      rld.RANK RANK,
      rld.REP_TP_FLAG REP_TP_FLAG,
      rld.IS_PREFERRED IS_PREFERRED,
      cc.TP_COMPANY_ID TP_COMPANY_ID,
      rld.WEEKLY_CAPACITY WEEKLY_CAPACITY,
      rld.DAILY_CAPACITY DAILY_CAPACITY,
      rld.CAPACITY_SUN CAPACITY_SUN,
      rld.CAPACITY_MON CAPACITY_MON,
      rld.CAPACITY_TUE CAPACITY_TUE,
      rld.CAPACITY_WED CAPACITY_WED,
      rld.CAPACITY_THU CAPACITY_THU,
      rld.CAPACITY_FRI CAPACITY_FRI,
      rld.CAPACITY_SAT CAPACITY_SAT,
      rld.WEEKLY_COMMITMENT WEEKLY_COMMITMENT,
      rld.DAILY_COMMITMENT DAILY_COMMITMENT,
      rld.COMMITMENT_SUN COMMITMENT_SUN,
      rld.COMMITMENT_MON COMMITMENT_MON,
      rld.COMMITMENT_TUE COMMITMENT_TUE,
      rld.COMMITMENT_WED COMMITMENT_WED,
      rld.COMMITMENT_THU COMMITMENT_THU,
      rld.COMMITMENT_FRI COMMITMENT_FRI,
      rld.COMMITMENT_SAT COMMITMENT_SAT,
      rld.WEEKLY_COMMIT_PCT WEEKLY_COMMIT_PCT,
      rld.DAILY_COMMIT_PCT DAILY_COMMIT_PCT,
      rld.COMMIT_PCT_SUN COMMIT_PCT_SUN,
      rld.COMMIT_PCT_MON COMMIT_PCT_MON,
      rld.COMMIT_PCT_TUE COMMIT_PCT_TUE,
      rld.COMMIT_PCT_WED COMMIT_PCT_WED,
      rld.COMMIT_PCT_THU COMMIT_PCT_THU,
      rld.COMMIT_PCT_FRI COMMIT_PCT_FRI,
      rld.COMMIT_PCT_SAT COMMIT_PCT_SAT,
      rld.EFFECTIVE_DT EFFECTIVE_DT,
      rld.EXPIRATION_DT EXPIRATION_DT,
      COALESCE (
         rld.EFFECTIVE_DT,
         TO_DATE ('1950-01-01-00.00.00', 'yyyy-mm-dd hh24:mi:ss'))
         EFF_DT,
      COALESCE (
         rld.EXPIRATION_DT,
         TO_DATE ('2049-12-31-00.00.00', 'yyyy-mm-dd hh24:mi:ss'))
         EXP_DT,
      rld.LAST_UPDATED_SOURCE_TYPE LAST_UPDATED_SOURCE_TYPE,
      rld.LAST_UPDATED_SOURCE LAST_UPDATED_SOURCE,
      rld.LAST_UPDATED_DTTM LAST_UPDATED_DTTM,
      DECODE (rld.LANE_DTL_STATUS, 4, 1, 0) HAS_ERRORS,
      '' SCNDR_CARRIER_CODE,
      rld.SCNDR_CARRIER_ID SCNDR_CARRIER_ID,
      rld.LANE_DTL_STATUS LANE_DTL_STATUS,
      rld.TT_TOLERANCE_FACTOR TT_TOLERANCE_FACTOR,
      rld.MONTLY_CAPACITY MONTHLY_CAPACITY,
      rld.YEARLY_CAPACITY YEARLY_CAPACITY,
      rld.MONTLY_COMMITMENT MONTHLY_COMMITMENT,
      rld.YEARLY_COMMITMENT YEARLY_COMMITMENT,
      rld.MONTLY_COMMIT_PCT MONTHLY_COMMIT_PCT,
      rld.YEARLY_COMMIT_PCT YEARLY_COMMIT_PCT,
      rld.CUSTOM_TEXT1 CUSTOM_TEXT1,
      rld.CUSTOM_TEXT2 CUSTOM_TEXT2,
      rld.CUSTOM_TEXT3 CUSTOM_TEXT3,
      rld.CUSTOM_TEXT4 CUSTOM_TEXT4,
      rld.CUSTOM_TEXT5 CUSTOM_TEXT5,
      rld.PACKAGE_ID PACKAGE_ID,
      rld.PACKAGE_NAME PACKAGE_NAME,
      rld.SIZE_UOM_ID SIZE_UOM_ID,
      rld.RG_SHIPPING_PARAM_ID RG_SHIPPING_PARAM_ID,
      rld.SP_LPN_TYPE SP_LPN_TYPE,      --  Gopalakrishnan 3/25/2008 Start
      rld.SP_MIN_LPN_COUNT SP_MIN_LPN_COUNT,
      rld.SP_MAX_LPN_COUNT SP_MAX_LPN_COUNT,
      rld.SP_MIN_WEIGHT SP_MIN_WEIGHT,
      rld.SP_MAX_WEIGHT SP_MAX_WEIGHT,
      rld.SP_MIN_VOLUME SP_MIN_VOLUME,
      rld.SP_MAX_VOLUME SP_MAX_VOLUME,
      rld.SP_MIN_LINEAR_FEET SP_MIN_LINEAR_FEET,
      rld.SP_MAX_LINEAR_FEET SP_MAX_LINEAR_FEET,
      rld.SP_MIN_MONETARY_VALUE SP_MIN_MONETARY_VALUE,
      rld.SP_MAX_MONETARY_VALUE SP_MAX_MONETARY_VALUE,
      rld.SP_CURRENCY_CODE SP_CURRENCY_CODE, --  Gopalakrishnan 3/25/2008 End
      rld.CUBING_INDICATOR CUBING_INDICATOR,
      rld.PREFERENCE_BONUS_VALUE PREFERENCE_BONUS_VALUE,
      rld.OVERRIDE_CODE OVERRIDE_CODE,
      rld.RFP_PACKAGE_ID RFP_PACKAGE_ID,
      rld.RFP_PACKAGE_REFERENCE RFP_PACKAGE_REFERENCE,
      rld.has_shipping_param HAS_SHIPPING_PARAM,
		  rld.voyage VOYAGE,
      rld.REJECT_FURTHER_SHIPMENTS REJECT_FURTHER_SHIPMENTS,
      rld.CARRIER_REJECT_PERIOD CARRIER_REJECT_PERIOD
 FROM comb_lane_dtl rld, carrier_code cc
WHERE     rld.carrier_id = cc.carrier_id
      AND rld.LANE_DTL_STATUS <> 2
      AND rld.IS_ROUTING = 1
 UNION ALL
 SELECT irld.TC_COMPANY_ID TC_COMPANY_ID,
      irl.RG_LANE_ID LANE_ID,
      irld.RG_LANE_DTL_SEQ RG_LANE_DTL_SEQ,
      irld.CARRIER_CODE CARRIER_CODE,
      CAST (NULL AS INT) CARRIER_ID,
      icc.CARRIER_CODE_STATUS CARRIER_CODE_STATUS,
      NVL (irld.MOT, 'ALL') MOT,
      CAST (NULL AS INT) MOT_ID,
      NVL (irld.EQUIPMENT_CODE, 'ALL') EQUIPMENT_CODE,
      CAST (NULL AS INT) EQUIPMENT_ID,
      NVL (irld.SERVICE_LEVEL, 'ALL') SERVICE_LEVEL,
      CAST (NULL AS INT) SERVICE_LEVEL_ID,
      NVL (irld.PROTECTION_LEVEL, 'ALL') PROTECTION_LEVEL,
      CAST (NULL AS INT) PROTECTION_LEVEL_ID,
      NVL (irld.TIER_ID, 'ALL') TIER_ID,
      irld.RANK RANK,
      irld.REP_TP_FLAG REP_TP_FLAG,
      irld.IS_PREFERRED IS_PREFERRED,
      icc.TP_COMPANY_ID TP_COMPANY_ID,
      irld.WEEKLY_CAPACITY WEEKLY_CAPACITY,
      irld.DAILY_CAPACITY DAILY_CAPACITY,
      irld.CAPACITY_SUN CAPACITY_SUN,
      irld.CAPACITY_MON CAPACITY_MON,
      irld.CAPACITY_TUE CAPACITY_TUE,
      irld.CAPACITY_WED CAPACITY_WED,
      irld.CAPACITY_THU CAPACITY_THU,
      irld.CAPACITY_FRI CAPACITY_FRI,
      irld.CAPACITY_SAT CAPACITY_SAT,
      irld.WEEKLY_COMMITMENT WEEKLY_COMMITMENT,
      irld.DAILY_COMMITMENT DAILY_COMMITMENT,
      irld.COMMITMENT_SUN COMMITMENT_SUN,
      irld.COMMITMENT_MON COMMITMENT_MON,
      irld.COMMITMENT_TUE COMMITMENT_TUE,
      irld.COMMITMENT_WED COMMITMENT_WED,
      irld.COMMITMENT_THU COMMITMENT_THU,
      irld.COMMITMENT_FRI COMMITMENT_FRI,
      irld.COMMITMENT_SAT COMMITMENT_SAT,
      irld.WEEKLY_COMMIT_PCT WEEKLY_COMMIT_PCT,
      irld.DAILY_COMMIT_PCT DAILY_COMMIT_PCT,
      irld.COMMIT_PCT_SUN COMMIT_PCT_SUN,
      irld.COMMIT_PCT_MON COMMIT_PCT_MON,
      irld.COMMIT_PCT_TUE COMMIT_PCT_TUE,
      irld.COMMIT_PCT_WED COMMIT_PCT_WED,
      irld.COMMIT_PCT_THU COMMIT_PCT_THU,
      irld.COMMIT_PCT_FRI COMMIT_PCT_FRI,
      irld.COMMIT_PCT_SAT COMMIT_PCT_SAT,
      irld.EFFECTIVE_DT EFFECTIVE_DT,
      irld.EXPIRATION_DT EXPIRATION_DT,
      COALESCE (
         irld.EXPIRATION_DT,
         TO_DATE ('1950-01-01-00.00.00', 'yyyy-mm-dd hh24:mi:ss'))
         EFF_DT,
      COALESCE (
         irld.EXPIRATION_DT,
         TO_DATE ('2049-12-31-00.00.00', 'yyyy-mm-dd hh24:mi:ss'))
         EXP_DT,
      irld.LAST_UPDATED_SOURCE_TYPE LAST_UPDATED_SOURCE_TYPE,
      irld.LAST_UPDATED_SOURCE LAST_UPDATED_SOURCE,
      irld.LAST_UPDATED_DTTM LAST_UPDATED_DTTM,
      DECODE (irld.LANE_DTL_STATUS, 4, 1, 0) HAS_ERRORS,
      irld.SCNDR_CARRIER_CODE SCNDR_CARRIER_CODE,
      CAST (NULL AS INT) SCNDR_CARRIER_ID,
      irld.LANE_DTL_STATUS LANE_DTL_STATUS,
      irld.TT_TOLERANCE_FACTOR TT_TOLERANCE_FACTOR,
      CAST (NULL AS INT) MONTHLY_CAPACITY,
      CAST (NULL AS INT) YEARLY_CAPACITY,
      CAST (NULL AS DECIMAL) MONTHLY_COMMITMENT,
      CAST (NULL AS DECIMAL) YEARLY_COMMITMENT,
      CAST (NULL AS DECIMAL) MONTHLY_COMMIT_PCT,
      CAST (NULL AS DECIMAL) YEARLY_COMMIT_PCT,
      '' CUSTOM_TEXT1,
      '' CUSTOM_TEXT2,
      '' CUSTOM_TEXT3,
      '' CUSTOM_TEXT4,
      '' CUSTOM_TEXT5,
      irld.PACKAGE_ID PACKAGE_ID,
      irld.PACKAGE_NAME PACKAGE_NAME,
      CAST (NULL AS INT) SIZE_UOM_ID,
      CAST (NULL AS INT) RG_SHIPPING_PARAM_ID,
      irld.SP_LPN_TYPE SP_LPN_TYPE,     --  Gopalakrishnan 3/25/2008 Start
      irld.SP_MIN_LPN_COUNT SP_MIN_LPN_COUNT,
      irld.SP_MAX_LPN_COUNT SP_MAX_LPN_COUNT,
      irld.SP_MIN_WEIGHT SP_MIN_WEIGHT,
      irld.SP_MAX_WEIGHT SP_MAX_WEIGHT,
      irld.SP_MIN_VOLUME SP_MIN_VOLUME,
      irld.SP_MAX_VOLUME SP_MAX_VOLUME,
      irld.SP_MIN_LINEAR_FEET SP_MIN_LINEAR_FEET,
      irld.SP_MAX_LINEAR_FEET SP_MAX_LINEAR_FEET,
      irld.SP_MIN_MONETARY_VALUE SP_MIN_MONETARY_VALUE,
      irld.SP_MAX_MONETARY_VALUE SP_MAX_MONETARY_VALUE,
      irld.SP_CURRENCY_CODE SP_CURRENCY_CODE, --  Gopalakrishnan 3/25/2008 End
      CAST (NULL AS INT) CUBING_INDICATOR,
      CAST (NULL AS INT) OVERRIDE_CODE,
      CAST (NULL AS INT) PREFERENCE_BONUS_VALUE,
      CAST (NULL AS INT) RFP_PACKAGE_ID,
      '' RFP_PACKAGE_REFERENCE,
      0 HAS_SHIPPING_PARAM,
		  irld.VOYAGE VOYAGE,
      0 REJECT_FURTHER_SHIPMENTS,
      0 CARRIER_REJECT_PERIOD
 FROM import_rg_lane_dtl irld
      LEFT OUTER JOIN carrier_code icc
         ON (irld.tc_company_id = icc.tc_company_id
             AND irld.carrier_code = icc.carrier_code)
      JOIN import_rg_lane irl
         ON irld.lane_id = irl.lane_id;

CREATE OR REPLACE FORCE VIEW "RG_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "RG_QUALIFIER", "FREQUENCY", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_NAME", "CUSTOMER_ID", "BILLING_METHOD", "INCOTERM_ID", "ROUTE_TO", "ROUTE_TYPE_1", "ROUTE_TYPE_2", "NO_RATING", "USE_FASTEST", "USE_PREFERENCE_BONUS") AS
select l.tc_company_id,
      l.lane_id,
      l.lane_hierarchy,
      l.lane_status,
      l.o_loc_type,
      l.o_facility_id,
      l.o_facility_alias_id,
      upper (l.o_city),
      l.o_state_prov,
      upper (l.o_county),
      l.o_postal_code,
      l.o_country_code,
      l.o_zone_id,
      '',
      l.d_loc_type,
      l.d_facility_id,
      l.d_facility_alias_id,
      upper (l.d_city),
      l.d_state_prov,
      upper (l.d_county),
      l.d_postal_code,
      l.d_country_code,
      l.d_zone_id,
      '',
      coalesce (l.rg_qualifier, 'ALL'),
      l.frequency,
      decode (l.lane_status, 4, 1, 0) has_errors,
      coalesce (
         (select sign (max (decode (id.lane_dtl_status, 4, 1, 0)))
            from import_rg_lane ir, import_rg_lane_dtl id
           where (    l.lane_id = ir.rg_lane_id
                  and l.tc_company_id = ir.tc_company_id
                  and ir.lane_id = id.lane_id
                  and ir.tc_company_id = id.tc_company_id)),
         0)
         dtl_has_errors,
      l.created_source_type,
      l.created_source,
      l.created_dttm,
      l.last_updated_source_type,
      l.last_updated_source,
      l.last_updated_dttm,
      l.lane_name,
      l.customer_id,
      l.billing_method,
      l.incoterm_id,
      l.route_to,
      l.route_type_1,
      l.route_type_2,
      l.no_rating,
      l.use_fastest,
      l.use_preference_bonus
 from rg_lane l
 union all
 select il.tc_company_id,
      il.rg_lane_id,
      il.lane_hierarchy,
      il.lane_status,
      il.o_loc_type,
      il.o_facility_id,
      il.o_facility_alias_id,
      upper (il.o_city),
      il.o_state_prov,
      upper (il.o_county),
      il.o_postal_code,
      il.o_country_code,
      il.o_zone_id,
      il.o_zone_name,
      il.d_loc_type,
      il.d_facility_id,
      il.d_facility_alias_id,
      upper (il.d_city),
      il.d_state_prov,
      upper (il.d_county),
      il.d_postal_code,
      il.d_country_code,
      il.d_zone_id,
      il.d_zone_name,
      coalesce (il.rg_qualifier, 'ALL'),
      il.frequency,
      decode (il.lane_status, 4, 1, 0) has_errors,
      coalesce (
         (select max (decode (d.lane_dtl_status, 4, 1, 0))
            from import_rg_lane_dtl d
           where d.lane_id = il.lane_id
                 and d.tc_company_id = il.tc_company_id),
         0)
         dtl_has_errors,
      il.created_source_type,
      il.created_source,
      il.created_dttm,
      il.last_updated_source_type,
      il.last_updated_source,
      il.last_updated_dttm,
      il.lane_name,
      il.customer_id,
      il.billing_method,
      il.incoterm_id,
      il.route_to,
      il.route_type_1,
      il.route_type_2,
      il.no_rating,
      il.use_fastest,
      il.use_preference_bonus
 from import_rg_lane il
where il.lane_status = 4
      and not exists
                 (select 1
                    from rg_lane r
                   where il.tc_company_id = r.tc_company_id
                         and il.rg_lane_id = r.lane_id);

CREATE OR REPLACE FORCE VIEW "SAILING_LANE" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "O_SEARCH_LOCATION", "D_SEARCH_LOCATION") AS
select tc_company_id,
      lane_id,
      lane_hierarchy,
      lane_status,
      o_loc_type,
      o_facility_id,
      o_facility_alias_id,
      o_city,
      o_state_prov,
      o_county,
      o_postal_code,
      o_country_code,
      o_zone_id,
      '',
      d_loc_type,
      d_facility_id,
      d_facility_alias_id,
      d_city,
      d_state_prov,
      d_county,
      d_postal_code,
      d_country_code,
      d_zone_id,
      '',
      created_source_type,
      created_source,
      created_dttm,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      o_search_location,
      d_search_location
 from comb_lane
where is_sailing = 1;

CREATE OR REPLACE FORCE VIEW "SAILING_LANE_DTL" ("TC_COMPANY_ID", "LANE_ID", "SAILING_LANE_DTL_SEQ", "CARRIER_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "MOT_ID", "SERVICE_LEVEL_ID", "SCNDR_CARRIER_ID", "FIXED_TRANSIT_VALUE", "FIXED_TRANSIT_STANDARD_UOM", "SAILING_SCHEDULE_NAME", "SAILING_FREQUENCY_TYPE", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "LANE_DTL_STATUS") AS
select tc_company_id,
      lane_id,
      lane_dtl_seq as sailing_lane_dtl_seq,
      carrier_id,
      effective_dt,
      expiration_dt,
      mot_id,
      service_level_id,
      scndr_carrier_id,
      fixed_transit_value,
      fixed_transit_standard_uom,
      sailing_schedule_name,
      sailing_frequency_type,
      last_updated_source_type,
      last_updated_source,
      last_updated_dttm,
      lane_dtl_status
 from comb_lane_dtl
where is_sailing = 1;

CREATE OR REPLACE FORCE VIEW "SAILING_LANE_DTL_VIEW" ("TC_COMPANY_ID", "LANE_ID", "SAILING_LANE_DTL_SEQ", "CARRIER_CODE", "CARRIER_ID", "CARRIER_CODE_STATUS", "MOT", "MOT_ID", "SERVICE_LEVEL", "SERVICE_LEVEL_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "EFF_DT", "EXP_DT", "HAS_ERRORS", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "SCNDR_CARRIER_CODE", "SCNDR_CARRIER_ID", "LANE_DTL_STATUS", "FIXED_TRANSIT_VALUE", "FIXED_TRANSIT_STANDARD_UOM", "SAILING_SCHEDULE_NAME", "SAILING_FREQUENCY_TYPE") AS
select sld.tc_company_id,
      sld.lane_id,
      sld.sailing_lane_dtl_seq,
      coalesce (cc.carrier_code, 'ALL'),
      sld.carrier_id,
      cc.carrier_code_status,
      coalesce (m.mot, 'ALL'),
      sld.mot_id,
      coalesce (sl.service_level, 'ALL'),
      sld.service_level_id,
      sld.effective_dt,
      sld.expiration_dt,
      coalesce (sld.effective_dt,
                to_date ('1950-01-01-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      coalesce (sld.expiration_dt,
                to_date ('2049-12-31-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      case sld.lane_dtl_status when 4 then 1 else 0 end as has_errors,
      sld.last_updated_source_type,
      sld.last_updated_source,
      sld.last_updated_dttm,
      cast (null as varchar (10)),
      sld.scndr_carrier_id,
      sld.lane_dtl_status,
      fixed_transit_value,
      fixed_transit_standard_uom,
      sailing_schedule_name,
      sailing_frequency_type
 from sailing_lane_dtl sld
      left outer join carrier_code cc
         on (sld.carrier_id = cc.carrier_id)
      left outer join mot m
         on (sld.mot_id = m.mot_id)
      left outer join service_level sl
         on (sld.service_level_id = sl.service_level_id)
where cc.carrier_id = sld.carrier_id and sld.lane_dtl_status <> 2
 union all
 select isld.tc_company_id,
      rl.lane_id,
      isld.sailing_lane_dtl_seq,
      coalesce (isld.carrier_code, 'ALL'),
      icc.carrier_id,
      icc.carrier_code_status,
      coalesce (isld.mot, 'ALL'),
      im.mot_id,
      coalesce (isld.service_level, 'ALL'),
      isl.service_level_id,
      isld.effective_dt,
      isld.expiration_dt,
      coalesce (isld.effective_dt,
                to_date ('1950-01-01-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      coalesce (isld.expiration_dt,
                to_date ('2049-12-31-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      case isld.lane_dtl_status when 4 then 1 else 0 end as has_errors,
      isld.last_updated_source_type,
      isld.last_updated_source,
      isld.last_updated_dttm,
      isld.scndr_carrier_code,
      cast (null as integer),
      isld.lane_dtl_status,
      fixed_transit_value,
      fixed_transit_standard_uom,
      sailing_schedule_name,
      sailing_frequency_type
 from import_sailing_lane irl
      join import_sailing_lane_dtl isld
         on irl.lane_id = isld.lane_id
            and irl.tc_company_id = isld.tc_company_id
      join sailing_lane rl
         on rl.tc_company_id = irl.tc_company_id
            and rl.lane_id = irl.sailing_lane_id
      left outer join carrier_code icc
         on (isld.carrier_id = icc.carrier_id)
      left outer join mot im
         on (isld.mot_id = im.mot_id)
      left outer join service_level isl
         on (isld.service_level_id = isl.service_level_id)
 union all
 select isld1.tc_company_id,
      irl1.sailing_lane_id,
      isld1.sailing_lane_dtl_seq,
      isld1.carrier_code,
      cast (null as integer),
      cast (null as smallint),
      coalesce (isld1.mot, 'ALL'),
      im1.mot_id,
      coalesce (isld1.service_level, 'ALL'),
      isl1.service_level_id,
      isld1.effective_dt,
      isld1.expiration_dt,
      coalesce (isld1.effective_dt,
                to_date ('1950-01-01-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      coalesce (isld1.expiration_dt,
                to_date ('2049-12-31-00.00.00', 'yyyy-mm-dd-hh24.mi.ss')),
      case isld1.lane_dtl_status when 4 then 1 else 0 end as has_errors,
      isld1.last_updated_source_type,
      isld1.last_updated_source,
      isld1.last_updated_dttm,
      isld1.scndr_carrier_code,
      cast (null as integer),
      isld1.lane_dtl_status,
      fixed_transit_value,
      fixed_transit_standard_uom,
      sailing_schedule_name,
      sailing_frequency_type
 from import_sailing_lane irl1
      join import_sailing_lane_dtl isld1
         on irl1.lane_id = isld1.lane_id
            and irl1.tc_company_id = isld1.tc_company_id
      left outer join carrier_code icc1
         on (isld1.tc_company_id = icc1.tc_company_id
             and isld1.carrier_id = icc1.carrier_id)
      left outer join mot im1
         on (isld1.mot_id = im1.mot_id)
      left outer join service_level isl1
         on (isld1.service_level_id = isl1.service_level_id)
where not exists
             (select 1
                from rating_lane r
               where irl1.tc_company_id = r.tc_company_id
                     and irl1.sailing_lane_id = r.lane_id)
      and irl1.lane_status = 4;

CREATE OR REPLACE FORCE VIEW "SAILING_LANE_VIEW" ("TC_COMPANY_ID", "LANE_ID", "LANE_HIERARCHY", "LANE_STATUS", "O_LOC_TYPE", "O_FACILITY_ID", "O_FACILITY_ALIAS_ID", "O_CITY", "O_STATE_PROV", "O_COUNTY", "O_POSTAL_CODE", "O_COUNTRY_CODE", "O_ZONE_ID", "O_ZONE_NAME", "D_LOC_TYPE", "D_FACILITY_ID", "D_FACILITY_ALIAS_ID", "D_CITY", "D_STATE_PROV", "D_COUNTY", "D_POSTAL_CODE", "D_COUNTRY_CODE", "D_ZONE_ID", "D_ZONE_NAME", "HAS_ERRORS", "DTL_HAS_ERRORS", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM") AS
select l.tc_company_id,
      l.lane_id,
      l.lane_hierarchy,
      l.lane_status,
      l.o_loc_type,
      l.o_facility_id,
      l.o_facility_alias_id,
      upper (l.o_city),
      l.o_state_prov,
      upper (l.o_county),
      l.o_postal_code,
      l.o_country_code,
      l.o_zone_id,
      '',
      l.d_loc_type,
      l.d_facility_id,
      l.d_facility_alias_id,
      upper (l.d_city),
      l.d_state_prov,
      upper (l.d_county),
      l.d_postal_code,
      l.d_country_code,
      l.d_zone_id,
      '',
      decode (cast (l.lane_status as int), 4, 1, 0) has_errors,
      coalesce (
         (select sign (
                    max (
                       decode (cast (sid.lane_dtl_status as int),
                               4, 1,
                               0)))
            from import_sailing_lane ir, import_sailing_lane_dtl sid
           where (    l.lane_id = ir.sailing_lane_id
                  and l.tc_company_id = ir.tc_company_id
                  and ir.lane_id = sid.lane_id
                  and ir.tc_company_id = sid.tc_company_id)),
         0)
         dtl_has_errors,
      l.created_source_type,
      l.created_source,
      l.created_dttm,
      l.last_updated_source_type,
      l.last_updated_source,
      l.last_updated_dttm
 from sailing_lane l
 union all
 select i.tc_company_id,
      i.sailing_lane_id,
      i.lane_hierarchy,
      i.lane_status,
      i.o_loc_type,
      i.o_facility_id,
      i.o_facility_alias_id,
      upper (i.o_city),
      i.o_state_prov,
      upper (i.o_county),
      i.o_postal_code,
      i.o_country_code,
      i.o_zone_id,
      i.o_zone_name,
      i.d_loc_type,
      i.d_facility_id,
      i.d_facility_alias_id,
      upper (i.d_city),
      i.d_state_prov,
      upper (i.d_county),
      i.d_postal_code,
      i.d_country_code,
      i.d_zone_id,
      i.d_zone_name,
      decode (cast (i.lane_status as int), 4, 1, 0) has_errors,
      coalesce (
         (select max (decode (cast (d.lane_dtl_status as int), 4, 1, 0))
            from import_sailing_lane_dtl d
           where d.lane_id = i.lane_id
                 and d.tc_company_id = i.tc_company_id),
         0)
         dtl_has_errors,
      i.created_source_type,
      i.created_source,
      i.created_dttm,
      i.last_updated_source_type,
      i.last_updated_source,
      i.last_updated_dttm
 from import_sailing_lane i
where i.lane_status = 4
      and not exists
                 (select 1
                    from sailing_lane r
                   where i.tc_company_id = r.tc_company_id
                         and i.sailing_lane_id = r.lane_id);

CREATE OR REPLACE FORCE VIEW "SCENARIO_CONSTRAINTS_VIEW" ("RFPID", "ROUND_NUM", "SCENARIOID", "OVERRIDE_ID", "ENABLE_FLAG", "SOURCE", "LEVEL_OF_DETAIL", "DISPLAY_LEVEL", "TYPE", "EQUIP_TYPE", "CONTRACT_TYPE", "MOVEMENT_TYPE", "OBCARRIERCODEID", "BID_ID", "CARRIER_GROUP_ID", "LOCATION", "COUNTRY_CODE", "HISTORICALAWARD", "AMOUNT", "SLACK", "USER_ID", "COMMENTS", "TIMESTAMP", "IS_VALID", "ADDITIONAL_CRITERIA", "SPECREQCONDITION", "CARRIERLIST", "UI_DISPLAY_TYPE", "VALUE_TYPE_ID", "SOURCE_TABLE_NAME") AS
SELECT x2.rfpid,
  x1.round_num,
  x2.scenarioid,
  x1.override_id,
  x1.enable_flag,
  'Bid',
  'ID = '
  ||x1.laneid,
  'Lane',
  'O',
  NULL,
  x3.contract_type,
  NULL,
  x1.obcarriercodeid,
  x1.laneid,
  NULL,
  NULL,
  NULL,
  x1.historicalaward,
  ROUND( (
  CASE
    WHEN (x0.volumefrequency = 0 )
    THEN (x1.lanecapacity * 7. )
    WHEN (x0.volumefrequency = 1 )
    THEN (x1.lanecapacity * 1. )
    WHEN (x0.volumefrequency = 2 )
    THEN (x1.lanecapacity * 0.5 )
    WHEN (x0.volumefrequency = 3 )
    THEN (x1.lanecapacity * 0.2301369863013749 )
    WHEN (x0.volumefrequency = 4 )
    THEN (x1.lanecapacity * 0.0383561643835616 )
    WHEN (x0.volumefrequency = 5 )
    THEN (x1.lanecapacity * 0.0191780821917808 )
    ELSE                  -1
  END),6),
  0,
  NULL,
  NULL,
  SYSDATE,
  1,
  NULL,
  NULL,
  NULL,
  'Max Vol for Carrier',
  0,
  'LANEBID'
FROM ob200lane_view x0,
  lanebid x1,
  scenarioadjustment x2,
  ob200contract_type x3
WHERE x1.laneid                     = x0.laneid
AND x1.rfpid                        = x0.rfpid
AND x2.scenarioid                   = x0.scenarioid
AND x1.rfpid                        = x2.rfpid
AND x1.lanecapacity                IS NOT NULL
AND x1.laneid                       = x2.laneid
AND x1.round_num                    = x2.bid_round_num
AND x1.laneequipmenttypeid          = x2.laneequipmenttypeid
AND x1.obcarriercodeid              = x2.obcarriercodeid
AND x1.packageid                    = x2.packageid
AND x1.status                       = 'Bid On'
AND x1.packageid                    = 0
AND x1.rfpid                        = x3.rfpid
AND x1.historicalaward              = x2.historicalaward
AND NVL (x3.protectionlevel_id, -1) = NVL (x1.protectionlevel_id, -1)
AND NVL (x3.servicetype_id,     -1) = NVL (x1.servicetype_id, -1)
AND NVL (x3.equipmenttype_id,   -1) = NVL (x1.equipmenttype_id, -1)
AND NVL (x3.mot, '-1-')             = NVL (x1.mot, '-1-')
AND NVL (x3.commoditycode_id, -1)   = NVL (x1.commoditycode_id, -1)
UNION ALL
SELECT RFPID,
  ROUND_NUM,
  SCENARIOID,
  OVERRIDE_ID,
  ENABLE_FLAG,
  SOURCE,
  CASE LEVEL_OF_DETAIL
    WHEN 'X'
    THEN ADDITIONAL_CRITERIA
    WHEN 'C'
    THEN 'All Lanes'
    WHEN 'L'
    THEN 'ID = '
      || BID_ID
  END LEVEL_OF_DETAIL,
  CASE LEVEL_OF_DETAIL
    WHEN 'X'
    THEN 'Special'
    WHEN 'C'
    THEN 'System'
    WHEN 'L'
    THEN 'Lane'
    WHEN 'P'
    THEN 'Package'
    ELSE 'N/A'
  END DISPLAY_LEVEL,
  TYPE,
  EQUIP_TYPE,
  CONTRACT_TYPE,
  MOVEMENT_TYPE,
  OBCARRIERCODEID,
  BID_ID,
  CARRIER_GROUP_ID,
  LOCATION,
  COUNTRY_CODE,
  HISTORICALAWARD,
  CASE
    WHEN MAX_VALUE < 0
    THEN MIN_VALUE
    ELSE MAX_VALUE
  END AMOUNT,
  SLACK,
  USER_ID,
  COMMENTS,
  TIMESTAMP,
  IS_VALID,
  ADDITIONAL_CRITERIA,
  SPECREQCONDITION,
  CARRIERLIST,
  CASE TYPE
    WHEN 'O'
    THEN
      CASE LEVEL_OF_DETAIL
        WHEN 'P'
        THEN 'Package'
        ELSE
          CASE MIN_VALUE
            WHEN -1
            THEN 'Max Vol for Carrier'
            ELSE 'Min Vol for Carrier'
          END
      END
    WHEN 'N'
    THEN
      CASE MIN_VALUE
        WHEN -1
        THEN 'Max Num of Carrier'
        ELSE 'Min Num of Carrier'
      END
    WHEN 'M'
    THEN 'Min Vol to Win'
    WHEN 'V'
    THEN 'Max Spend for Carrier'
    WHEN 'U'
    THEN 'Min Spend for Carrier'
    WHEN 'F'
    THEN 'SYS LOCK'
  END AS UI_DISPLAY_TYPE,
  VALUE_TYPE_ID,
  'ADVANCE_CONSTRAINT'
FROM OB200OVERRIDE_ADV;

CREATE OR REPLACE FORCE VIEW "LOCATION_VIEW" ("PARENT_ID", "STATUS", "LOCN_CLASS", "MAX_CAPACITY", "LOCN_ID", "IS_GUARD_HOUSE", "CURRENT_CAPACITY", "ID", "FACILITY_ID", "YARD_ID", "LOCATION_NAME", "PARENT_LOCATION_NAME", "LOCN_BRCD", "DSP_LOCN", "CHECK_DIGIT", "TC_COMPANY_ID", "YARD_NAME") AS
(select parent_id,
         status,
         locn_class,
         max_capacity,
         locn_id,
         is_guard_house,
         current_capacity,
         id,
         facility_id,
         yard_id,
         location_name,
         parent_location_name,
         locn_brcd,
         dsp_locn,
         check_digit,
         tc_company_id,
         yard_name
    from dock_door_view
  union all
  select parent_id,
         status,
         locn_class,
         max_capacity,
         locn_id,
         is_guard_house,
         current_capacity,
         id,
         facility_id,
         yard_id,
         location_name,
         parent_location_name,
         locn_brcd,
         dsp_locn,
         check_digit,
         tc_company_id,
         yard_name
    from yard_slot_view);

CREATE OR REPLACE FORCE VIEW "MANIFEST_EXCEPTIONS" ("TC_COMPANY_ID", "MANIFEST_ID", "LPN_ID", "TC_LPN_ID", "TC_PARENT_LPN_ID", "TC_SHIPMENT_ID", "TC_ORDER_ID", "LPN_FACILITY_STATUS") AS
(SELECT L.TC_COMPANY_ID L1,
       ML.MANIFEST_ID L2,
       L.LPN_ID L3,
       L.TC_LPN_ID L4,
       L.TC_PARENT_LPN_ID L5,
       L.TC_SHIPMENT_ID L6,
       L.TC_ORDER_ID || '-' || L.ORDER_ID L7,
       TO_CHAR (L.LPN_FACILITY_STATUS) L8
  FROM LPN L, MANIFESTED_LPN ML
 WHERE     L.SHIPMENT_ID = ML.SHIPMENT_ID
       AND ML.MANIFEST_ID IN (SELECT manifest_id
                                FROM manifest_hdr
                               WHERE manifest_status_id = 10)
       AND L.LPN_FACILITY_STATUS < 40
UNION
SELECT RS.COID,
       RS.mid L0,
       RS.LINEITEMID L1,
       RS.TCORDERID L2,
       RS.TCORDERID L3,
       TO_CHAR (RS.ORDERID) L4,
       TO_CHAR (RS.QTYVARIANCE) L5,
       RS.QTYUOM L6
  FROM (  SELECT ORD.TC_COMPANY_ID COID,
                 MLPN.MANIFEST_ID mid,
                 ORD.ORDER_ID ORDERID,
                 ORD.TC_ORDER_ID TCORDERID,
                 OLI.LINE_ITEM_ID AS LINEITEMID,
                 (COALESCE (OLI.ORDER_QTY, 0)
                  - COALESCE (OLI.USER_CANCELED_QTY, 0)
                  - SUM (
                       CASE
                          WHEN L.LPN_FACILITY_STATUS < 20
                          THEN
                             COALESCE (LD.INITIAL_QTY, 0)
                          ELSE
                             COALESCE (LD.SIZE_VALUE, 0)
                       END))
                    QTYVARIANCE,
                 MIN (SIZE_UOM.SIZE_UOM) AS QTYUOM
            FROM MANIFESTED_LPN MLPN
                 INNER JOIN ORDERS ORD
                    ON ORD.ORDER_ID = MLPN.ORDER_ID
                       AND ORD.IS_CANCELLED = 0
                       AND MLPN.MANIFEST_ID IN
                              (SELECT manifest_id
                                 FROM manifest_hdr
                                WHERE manifest_status_id = 10)
                 INNER JOIN ORDER_LINE_ITEM OLI
                    ON OLI.ORDER_ID = ORD.ORDER_ID AND OLI.IS_CANCELLED = 0
                 INNER JOIN LPN L
                    ON     L.ORDER_ID = ORD.ORDER_ID
                       AND L.LPN_FACILITY_STATUS < 99
                       AND L.LPN_TYPE = 1
                       AND L.INBOUND_OUTBOUND_INDICATOR = 'O'
                 LEFT JOIN LPN_DETAIL LD
                    ON LD.LPN_ID = L.LPN_ID
                       AND LD.DISTRIBUTION_ORDER_DTL_ID = OLI.LINE_ITEM_ID
                 LEFT JOIN SIZE_UOM
                    ON SIZE_UOM.SIZE_UOM_ID = OLI.QTY_UOM_ID_BASE
        GROUP BY ORD.TC_COMPANY_ID,
                 MLPN.MANIFEST_ID,
                 ORD.ORDER_ID,
                 ORD.TC_ORDER_ID,
                 OLI.LINE_ITEM_ID,
                 OLI.ORDER_QTY,
                 OLI.USER_CANCELED_QTY) RS
 WHERE RS.QTYVARIANCE > 0);

CREATE OR REPLACE FORCE VIEW "MV_DYNAMIC_PICK_LOCN" ("WHSE", "LH_LOCN_ID", "LOCN_ID", "MAX_NBR_OF_SKU", "REPL_FLAG", "MAX_WT", "MAX_VOL", "LOCN_CLASS", "LOCN_PICK_SEQ", "PICK_LOCN_ASSIGN_ZONE", "SKU_DEDCTN_TYPE", "PICK_LOCN_HDR_ID") AS
(select whse,
       lh.locn_id lh_locn_id,
       plh.locn_id,
       max_nbr_of_sku,
       repl_flag,
       max_wt,
       max_vol,
       locn_class,
       locn_pick_seq,
       pick_locn_assign_zone,
       sku_dedctn_type,
       plh.pick_locn_hdr_id
  from locn_hdr lh, pick_locn_hdr plh
 where     plh.locn_id = lh.locn_id
       and sku_dedctn_type = 'T'
       and (slot_unusable is null or slot_unusable != 'Y'));

CREATE OR REPLACE FORCE VIEW "NAVIGATION_VIEW" ("PARENT_TITLE", "NAVIGATION_ID", "PARENT_ID", "LEVEL", "NAV_TITLE", "URL", "TYPE", "SIDE_NAV", "UDEF_SEQ") AS
select p.title parent_title, t."NAVIGATION_ID",t."PARENT_ID",t."LEVEL",t."NAV_TITLE",t."URL",t."TYPE",t."SIDE_NAV",t."UDEF_SEQ"
from navigation p right outer join (
              select navigation_id, parent_id, level, title nav_title, url,type,side_nav,udef_seq
              from navigation
              start with parent_id is null
              connect by prior navigation_id = parent_id
              ) t
on (p.navigation_id = t.parent_id);

CREATE OR REPLACE FORCE VIEW "OB200CARRIER_CODE_VIEW" ("CARRIER_CODE", "TC_COMPANY_ID", "CARRIER_CODE_TYPE", "CONTRACT_TYPE", "EQUIP_TYPE", "OBCARRIERCODEID", "IS_VALID", "RFPID") AS
select distinct x0.obcarriercodeid,
               x1.tccompanyid,
               'US',
               'T',
               'D',
               x0.obcarriercodeid,
               1,
               x1.rfpid
 from rfpparticipant x0, rfp x1
where ( (x1.rfpid = x0.rfpid) and (x1.round_num = x0.round_num));

CREATE OR REPLACE FORCE VIEW "OB200CARRIER_GROUP_VIEW" ("GROUP_ID", "OBCARRIERCODEID", "DESCRIPTION", "TIMESTAMP", "IS_VALID", "TC_COMPANY_ID", "RFPID") AS
select distinct x0.distributionlistid,
               x3.obcarriercodeid,
               x0.distributionlistname,
               '',
               1,
               x2.tccompanyid,
               x2.rfpid
 from distributionlist x0,
      distributionlistmember x1,
      rfp x2,
      rfpparticipant x3
where     x0.distributionlistid = x1.distributionlistid
      and x1.tpcompanyid = x3.tpcompanyid
      and x1.obcarriercodeid = x3.obcarriercodeid
      and x3.rfpid = x2.rfpid
      and x2.round_num = x3.round_num;

CREATE OR REPLACE FORCE VIEW "OB200CARRIER_VIEW" ("OBCARRIERCODEID", "TC_COMPANY_ID", "NAME", "IS_VALID", "RFPID") AS
select distinct x0.obcarriercodeid,
               x1.tccompanyid,
               '',
               1,
               x1.rfpid
 from rfpparticipant x0, rfp x1
where ( (x1.rfpid = x0.rfpid) and (x1.round_num = x0.round_num));

CREATE OR REPLACE FORCE VIEW "OB200LANE_BID_VIEW" ("LANEID", "OBCARRIERCODEID", "HISTORICALAWARD", "CONTRACT_TYPE", "MIN_COST", "RATE_PER_MI", "CURRENCY_TYPE", "OPER_FACTOR_CHAR3", "OPER_FACTOR_DOUBLE4", "EFFECTIVE_COST", "OPER_FACTOR_DOUBLE1", "OPER_FACTOR_DOUBLE2", "OPER_FACTOR_DOUBLE3", "OPER_FACTOR_CHAR1", "OPER_FACTOR_CHAR2", "IS_VALID", "RFPID", "OPER_FACTOR_CHAR4", "OPER_FACTOR_CHAR5", "OPER_FACTOR_CHAR6", "OPER_FACTOR_CHAR7", "OPER_FACTOR_CHAR8", "OPER_FACTOR_CHAR9", "OPER_FACTOR_CHAR10", "OPER_FACTOR_DOUBLE5", "OPER_FACTOR_DOUBLE6", "OPER_FACTOR_DOUBLE7", "OPER_FACTOR_DOUBLE8", "OPER_FACTOR_DOUBLE9", "OPER_FACTOR_DOUBLE10", "OPER_FACTOR_INT1", "OPER_FACTOR_INT2", "OPER_FACTOR_INT3", "OPER_FACTOR_INT4", "OPER_FACTOR_INT5", "OPER_FACTOR_INT6", "OPER_FACTOR_INT7", "OPER_FACTOR_INT8", "OPER_FACTOR_INT9", "OPER_FACTOR_INT10", "LANEEQUIPMENTTYPEID", "ROUND_NUM", "SCENARIOID") AS
select distinct
      x0.laneid,
      x0.obcarriercodeid,
      x0.historicalaward,
      x2.contract_type,
      case when (x0.costperload = 0.) then 0.01 else x0.costperload end,
      '',
      'USD',
      x0.custtext3,
      x0.custdouble4,
      case when (x0.costperload = 0.) then 0.01 else x0.costperload end,
      x0.custdouble1,
      x0.custdouble2,
      ( ( ( (nvl (x1.globaladjpct, 0) + nvl (x1.facilityadjpct, 0))
           + nvl (x1.laneadjpct, 0))
         + nvl (x1.carrieradjpct, 0)))
         custdouble3,
      x0.custtext1,
      x0.custtext2,
      1,
      x1.rfpid,
      x0.custtext4,
      x0.custtext5,
      x0.custtext6,
      x0.custtext7,
      x0.custtext8,
      x0.custtext9,
      x0.custtext10,
      x0.custdouble5,
      x0.custdouble6,
      x0.custdouble7,
      x0.custdouble8,
      x0.custdouble9,
      x0.custdouble10,
      x0.custint1,
      x0.custint2,
      x0.custint3,
      x0.custint4,
      x0.custint5,
      x0.custint6,
      x0.custint7,
      x0.custint8,
      x0.custint9,
      x0.custint10,
      x0.laneequipmenttypeid,
      x0.round_num,
      x1.scenarioid
 from lanebid x0,
      scenarioadjustment x1,
      ob200contract_type x2,
      ob200lane_view x3
where     x0.historicalaward = x1.historicalaward
      and x1.scenarioid = x3.scenarioid
      and x0.rfpid = x3.rfpid
      and x0.laneid = x3.laneid
      and ( (x3.lane_type != 2)
           or exists
                 (select x4.rfp_id, x4.lane_id, x4.scenario_id
                    from scenario_additional_lanes x4
                   where     x4.rfp_id = x3.rfpid
                         and x4.lane_id = x3.laneid
                         and x4.scenario_id = x1.scenarioid))
      and x0.rfpid = x1.rfpid
      and x0.laneid = x1.laneid
      and x0.obcarriercodeid = x1.obcarriercodeid
      and x0.packageid = x1.packageid
      and x0.laneequipmenttypeid = x1.laneequipmenttypeid
      and x0.round_num = x1.bid_round_num
      and x0.status = 'Bid On'
      and x0.packageid = 0
      and x0.rfpid = x2.rfpid
      and nvl (x2.protectionlevel_id, -1) =
             nvl (x0.protectionlevel_id, -1)
      and nvl (x2.servicetype_id, -1) = nvl (x0.servicetype_id, -1)
      and nvl (x2.equipmenttype_id, -1) = nvl (x0.equipmenttype_id, -1)
      and nvl (x2.mot, '-1-') = nvl (x0.mot, '-1-')
      and nvl (x2.commoditycode_id, -1) = nvl (x0.commoditycode_id, -1);

CREATE OR REPLACE FORCE VIEW "OB200LANE_VIEW" ("LANEID", "SCENARIOID", "RFPID", "ORIGINSHIPSITE", "ORIGINSHIPSITECODE", "ORIGINPORT", "ORIGINCITY", "ORIGINSTATEPROV", "ORIGINCOUNTRYCODE", "ORIGINPOSTALCODE", "DESTINATIONSHIPSITE", "DESTINATIONPORT", "DESTINATIONSHIPSITECODE", "DESTINATIONCITY", "DESTINATIONSTATEPROV", "DESTINATIONCOUNTRYCODE", "DESTINATIONPOSTALCODE", "CUSTOMERNAME", "COMMODITYCODE", "COMMODITYCODEDESC", "HARMONIZEDCODE", "RATETYPE", "ISROUNDTRIP", "AVERAGEWEIGHT", "VOLUMEFREQUENCY", "VOLUME", "WEIGHTBREAK1", "WEIGHTBREAK2", "WEIGHTBREAK3", "WEIGHTBREAK4", "WEIGHTBREAK5", "WEIGHTBREAK6", "WEIGHTBREAK7", "WEIGHTBREAK8", "WEIGHTBREAK9", "WEIGHTUOM", "WEIGHTBREAKUOM", "MILEAGE", "DISTANCEUOM", "LCLCUBICSPACE", "LCLCUBICSPACEUOM", "PICKUPTYPE", "SERVICECRITERIA", "FREIGHTCLASS", "ROUTINGTYPE", "ISCONFERENCE", "TRANSITTIME", "TRANSITTIMEUOM", "ISOVERSIZED", "OVERSIZEDNOTE", "ISHAZARDOUS", "HAZARDOUSNOTE", "ISPERISHABLE", "PERISHABLENOTE", "OTHER1", "OTHER2", "OTHER3", "ISCANCELLED", "ISDIRTY", "CREATEDUID", "CREATEDDTTM", "LASTUPDATEDUID", "LASTUPDATEDDTTM", "CUSTTEXT1", "CUSTTEXT2", "CUSTTEXT3", "CUSTTEXT4", "CUSTTEXT5", "CUSTTEXT6", "CUSTTEXT7", "CUSTTEXT8", "CUSTTEXT9", "CUSTTEXT10", "CUSTINT1", "CUSTINT2", "CUSTINT3", "CUSTINT4", "CUSTINT5", "CUSTINT6", "CUSTINT7", "CUSTINT8", "CUSTINT9", "CUSTINT10", "CUSTDOUBLE1", "CUSTDOUBLE2", "CUSTDOUBLE3", "CUSTDOUBLE4", "CUSTDOUBLE5", "CUSTDOUBLE6", "CUSTDOUBLE7", "CUSTDOUBLE8", "CUSTDOUBLE9", "CUSTDOUBLE10", "ORIGINFACILITYCODE", "ORIGINCAPACITYAREACODE", "ORIGINAIRPORT", "DESTINATIONFACILITYCODE", "DESTINATIONCAPACITYAREACODE", "DESTINATIONAIRPORT", "AVG_LANE_COST", "NUMSTOPS", "NUMCONTAINERS", "LANEATTRIBUTEVERSION", "ROUND_NUM", "SHIPMENTRATEDCOST", "SHIPMENTVOLUME", "SHIPMENTVOLUMEERROR", "WEIGHTBREAKRATETYPE", "ORIGINZONECODE", "DESTINATIONZONECODE", "AVG_WEEKLY_VOLUME", "LANE_TYPE") AS
SELECT x0.laneid,
      x1.scenarioid,
      x0.rfpid,
      x0.ORIGINSHIPSITE,
      x0.originshipsitecode,
      x0.originport,
      x0.origincity,
      x0.originstateprov,
      x0.origincountrycode,
      x0.originpostalcode,
      x0.DESTINATIONSHIPSITE,
      x0.destinationport,
      x0.destinationshipsitecode,
      x0.destinationcity,
      x0.destinationstateprov,
      x0.destinationcountrycode,
      x0.destinationpostalcode,
      x0.customername,
      x0.commoditycode,
      x0.commoditycodedesc,
      x0.harmonizedcode,
      x0.ratetype,
      x0.isroundtrip,
      x0.averageweight,
      x0.volumefrequency,
      x0.volume,
      x0.weightbreak1,
      x0.weightbreak2,
      x0.weightbreak3,
      x0.weightbreak4,
      x0.weightbreak5,
      x0.weightbreak6,
      x0.weightbreak7,
      x0.weightbreak8,
      x0.weightbreak9,
      x0.weightuom,
      x0.weightbreakuom,
      x0.mileage,
      x0.distanceuom,
      x0.lclcubicspace,
      x0.lclcubicspaceuom,
      x0.pickuptype,
      x0.servicecriteria,
      x0.freightclass,
      x0.routingtype,
      x0.isconference,
      x0.transittime,
      x0.transittimeuom,
      x0.isoversized,
      x0.oversizednote,
      x0.ishazardous,
      x0.hazardousnote,
      x0.isperishable,
      x0.perishablenote,
      x0.other1,
      x0.other2,
      x0.other3,
      x0.iscancelled,
      x0.isdirty,
      x0.createduid,
      x0.createddttm,
      x0.lastupdateduid,
      x0.lastupdateddttm,
      x0.custtext1,
      x0.custtext2,
      x0.custtext3,
      x0.custtext4,
      x0.custtext5,
      x0.custtext6,
      x0.custtext7,
      x0.custtext8,
      x0.custtext9,
      x0.custtext10,
      x0.custint1,
      x0.custint2,
      x0.custint3,
      x0.custint4,
      x0.custint5,
      x0.custint6,
      x0.custint7,
      x0.custint8,
      x0.custint9,
      x0.custint10,
      x0.custdouble1,
      x0.custdouble2,
      x0.custdouble3,
      x0.custdouble4,
      x0.custdouble5,
      x0.custdouble6,
      x0.custdouble7,
      x0.custdouble8,
      x0.custdouble9,
      x0.custdouble10,
      x0.originfacilitycode,
		  x0.origincapacityareacode,
      x0.originairport,
      x0.destinationfacilitycode,
		  x0.destinationcapacityareacode,
      x0.destinationairport,
      x0.historicalcost,
      x0.numstops,
      x0.numcontainers,
      x0.laneattributeversion,
      x0.round_num,
      x0.shipmentratedcost,
      x0.shipmentvolume,
      x0.shipmentvolumeerror,
      x0.weightbreakratetype,
      x0.originzonecode,
      x0.destinationzonecode,
      ROUND( (CASE
  WHEN (x0.volumefrequency = 0)
  THEN (x0.volume * 7.)
  WHEN (x0.volumefrequency = 1)
  THEN (x0.volume * 1.)
  WHEN (x0.volumefrequency = 2)
  THEN (x0.volume * 0.5)
  WHEN (x0.volumefrequency = 3)
  THEN (x0.volume * 0.2301369863013749)
  WHEN (x0.volumefrequency = 4)
  THEN (x0.volume * 0.0383561643835616)
  WHEN (x0.volumefrequency = 5)
  THEN (x0.volume * 0.0191780821917808)
  ELSE            -1
END),6),
      x0.lane_type
 FROM lane x0, scenario x1, lane_active x2
WHERE ( (x0.lane_type <> 2)
       OR EXISTS
             (SELECT x3.rfp_id, x3.lane_id, x3.scenario_id
                FROM scenario_additional_lanes x3
               WHERE     x3.rfp_id = x0.rfpid
                     AND x3.lane_id = x0.laneid
                     AND x3.scenario_id = x1.scenarioid))
      AND x0.rfpid = x1.rfpid
      AND x0.rfpid = x2.rfpid
      AND x0.laneid = x2.laneid
      AND x0.round_num <= x2.round_num
      AND NVL (x0.iscancelled, 0) <> 1
      AND x2.active = 1
      AND x0.round_num =
             (SELECT MAX (x4.round_num)
                FROM lane x4
               WHERE     x4.rfpid = x0.rfpid
                     AND x4.laneid = x0.laneid
                     AND x4.round_num <= x1.round_num
                     AND x2.round_num = x1.round_num);

CREATE OR REPLACE FORCE VIEW "OB200OVERRIDE_LANEBID" ("BID_ID", "SCENARIOID", "OVERRIDE_ID", "ENABLE_FLAG", "SOURCE", "LEVEL_OF_DETAIL", "TYPE", "EQUIP_TYPE", "CONTRACT_TYPE", "MOVEMENT_TYPE", "OBCARRIERCODEID", "CARRIER_GROUP_ID", "LOCATION", "COUNTRY_CODE", "HISTORICALAWARD", "MIN_VALUE", "MAX_VALUE", "SLACK", "USER_ID", "COMMENTS", "TIMESTAMP", "IS_VALID", "ADDITIONAL_CRITERIA", "RFPID", "ROUND_NUM", "SPECREQCONDITION", "CARRIERLIST") AS
SELECT x1.laneid,
      x2.scenarioid,
      x1.override_id,
      x1.enable_flag,
      'B',
      'L',
      'O',
      NULL,
      x3.contract_type,
      NULL,
      x1.obcarriercodeid,
      NULL,
      NULL,
      NULL,
      x1.historicalaward,
      -1,
     ROUND( (CASE
  WHEN (x0.volumefrequency = 0 )
  THEN (x1.lanecapacity * 7. )
  WHEN (x0.volumefrequency = 1 )
  THEN (x1.lanecapacity * 1. )
  WHEN (x0.volumefrequency = 2 )
  THEN (x1.lanecapacity * 0.5 )
  WHEN (x0.volumefrequency = 3 )
  THEN (x1.lanecapacity * 0.2301369863013749 )
  WHEN (x0.volumefrequency = 4 )
  THEN (x1.lanecapacity * 0.0383561643835616 )
  WHEN (x0.volumefrequency = 5 )
  THEN (x1.lanecapacity * 0.0191780821917808 )
  ELSE    -1
END),6),
      0,
      NULL,
      NULL,
      SYSDATE,
      1,
      NULL,
      x2.rfpid,
      x1.round_num,
      NULL,
      NULL
 FROM ob200lane_view x0,
      lanebid x1,
      scenarioadjustment x2,
      ob200contract_type x3
WHERE     x1.laneid = x0.laneid
      AND x1.rfpid = x0.rfpid
      AND x2.scenarioid = x0.scenarioid
      AND x1.rfpid = x2.rfpid
      AND x1.lanecapacity IS NOT NULL
      AND x1.laneid = x2.laneid
      AND x1.round_num = x2.bid_round_num
      AND x1.laneequipmenttypeid = x2.laneequipmenttypeid
      AND x1.obcarriercodeid = x2.obcarriercodeid
      AND x1.packageid = x2.packageid
      AND x1.status = 'Bid On'
      AND x1.packageid = 0
      AND x1.rfpid = x3.rfpid
      AND x1.historicalaward = x2.historicalaward
      AND NVL (x3.protectionlevel_id, -1) =
             NVL (x1.protectionlevel_id, -1)
      AND NVL (x3.servicetype_id, -1) = NVL (x1.servicetype_id, -1)
      AND NVL (x3.equipmenttype_id, -1) = NVL (x1.equipmenttype_id, -1)
      AND NVL (x3.mot, '-1-') = NVL (x1.mot, '-1-')
      AND NVL (x3.commoditycode_id, -1) = NVL (x1.commoditycode_id, -1);

CREATE OR REPLACE FORCE VIEW "OB200OVERRIDE_VIEW" ("SCENARIOID", "BID_ID", "OVERRIDE_ID", "ENABLE_FLAG", "SOURCE", "LEVEL_OF_DETAIL", "TYPE", "EQUIP_TYPE", "CONTRACT_TYPE", "MOVEMENT_TYPE", "OBCARRIERCODEID", "CARRIER_GROUP_ID", "LOCATION", "COUNTRY_CODE", "HISTORICALAWARD", "MIN_VALUE", "MAX_VALUE", "SLACK", "USER_ID", "COMMENTS", "TIMESTAMP", "IS_VALID", "ADDITIONAL_CRITERIA", "RFPID", "ROUND_NUM", "SPECREQCONDITION", "CARRIERLIST") AS
select x0.scenarioid,
      x0.bid_id,
      x0.override_id,
      x0.enable_flag,
      x0.source,
      x0.level_of_detail,
      x0.type,
      x0.equip_type,
      x0.contract_type,
      x0.movement_type,
      x0.obcarriercodeid,
      x0.carrier_group_id,
      x0.location,
      x0.country_code,
      x0.historicalaward,
      x0.min_value,
      x0.max_value,
      x0.slack,
      x0.user_id,
      x0.comments,
      x0.timestamp,
      x0.is_valid,
      x0.additional_criteria,
      x0.rfpid,
      x0.round_num,
      x0.specreqcondition,
      x0.carrierlist
 from ob200override_adv x0
 union all
 select x1.scenarioid,
      x1.bid_id,
      x1.override_id,
      x1.enable_flag,
      x1.source,
      x1.level_of_detail,
      x1.type,
      x1.equip_type,
      x1.contract_type,
      x1.movement_type,
      x1.obcarriercodeid,
      null,
      x1.location,
      x1.country_code,
      x1.historicalaward,
      x1.min_value,
      x1.max_value,
      x1.slack,
      x1.user_id,
      x1.comments,
      x1.timestamp,
      x1.is_valid,
      x1.additional_criteria,
      x1.rfpid,
      x1.round_num,
      null,
      null
 from ob200override_lanebid x1;

CREATE OR REPLACE FORCE VIEW "OB200PACKAGE_BID_VIEW" ("OBCARRIERCODEID", "LANEID", "PACKAGEID", "HISTORICALAWARD", "DESIRED_VOL", "CONTRACT_TYPE", "MIN_COST", "RATE_PER_MI", "CURRENCY_TYPE", "OPER_FACTOR_CHAR3", "OPER_FACTOR_DOUBLE4", "EFFECTIVE_COST", "OPER_FACTOR_DOUBLE1", "OPER_FACTOR_DOUBLE2", "OPER_FACTOR_DOUBLE3", "OPER_FACTOR_CHAR1", "OPER_FACTOR_CHAR2", "IS_VALID", "RFPID", "OPER_FACTOR_CHAR4", "OPER_FACTOR_CHAR5", "OPER_FACTOR_CHAR6", "OPER_FACTOR_CHAR7", "OPER_FACTOR_CHAR8", "OPER_FACTOR_CHAR9", "OPER_FACTOR_CHAR10", "OPER_FACTOR_DOUBLE5", "OPER_FACTOR_DOUBLE6", "OPER_FACTOR_DOUBLE7", "OPER_FACTOR_DOUBLE8", "OPER_FACTOR_DOUBLE9", "OPER_FACTOR_DOUBLE10", "OPER_FACTOR_INT1", "OPER_FACTOR_INT2", "OPER_FACTOR_INT3", "OPER_FACTOR_INT4", "OPER_FACTOR_INT5", "OPER_FACTOR_INT6", "OPER_FACTOR_INT7", "OPER_FACTOR_INT8", "OPER_FACTOR_INT9", "OPER_FACTOR_INT10", "LANEEQUIPMENTTYPEID", "ROUND_NUM", "SCENARIOID") AS
select x1.obcarriercodeid,
      x1.laneid,
      x1.packageid,
      x1.historicalaward,
      case
         when (x0.volumefrequency = 0)
         then
            (x1.lanecapacity * 7.)
         when (x0.volumefrequency = 1)
         then
            (x1.lanecapacity * 1.)
         when (x0.volumefrequency = 2)
         then
            (x1.lanecapacity * 0.5)
         when (x0.volumefrequency = 3)
         then
            (x1.lanecapacity * 0.2301369863013749)
         when (x0.volumefrequency = 4)
         then
            (x1.lanecapacity * 0.0383561643835616)
         when (x0.volumefrequency = 5)
         then
            (x1.lanecapacity * 0.0191780821917808)
         else
            -1
      end,
      x3.contract_type,
      case when (x1.costperload = 0.) then 0.01 else x1.costperload end,
      '',
      'USD',
      x1.custtext3,
      x1.custdouble4,
      case when (x1.costperload = 0.) then 0.01 else x1.costperload end,
      x1.custdouble1,
      x1.custdouble2,
      ( ( ( (nvl (x2.globaladjpct, 0) + nvl (x2.facilityadjpct, 0))
           + nvl (x2.laneadjpct, 0))
         + nvl (x2.carrieradjpct, 0))
       - nvl (x1.custdouble3, 0)),
      x1.custtext1,
      x1.custtext1,
      1,
      x2.rfpid,
      x1.custtext4,
      x1.custtext5,
      x1.custtext6,
      x1.custtext7,
      x1.custtext8,
      x1.custtext9,
      x1.custtext10,
      x1.custdouble5,
      x1.custdouble6,
      x1.custdouble7,
      x1.custdouble8,
      x1.custdouble9,
      x1.custdouble10,
      x1.custint1,
      x1.custint2,
      x1.custint3,
      x1.custint4,
      x1.custint5,
      x1.custint6,
      x1.custint7,
      x1.custint8,
      x1.custint9,
      x1.custint10,
      x1.laneequipmenttypeid,
      x1.round_num,
      x2.scenarioid
 from ob200lane_view x0,
      lanebid x1,
      scenarioadjustment x2,
      ob200contract_type x3
where     x1.laneid = x0.laneid
      and x1.rfpid = x0.rfpid
      and x0.scenarioid = x2.scenarioid
      and x2.rfpid = x0.rfpid
      and x1.historicalaward = x2.historicalaward
      and x1.rfpid = x2.rfpid
      and x1.laneid = x2.laneid
      and x1.round_num = x2.bid_round_num
      and x1.laneequipmenttypeid = x2.laneequipmenttypeid
      and x1.obcarriercodeid = x2.obcarriercodeid
      and x1.packageid = x2.packageid
      and x1.status = 'Bid On'
      and x1.packageid > 0
      and x1.rfpid = x3.rfpid
      and nvl (x3.protectionlevel_id, -1) =
             nvl (x1.protectionlevel_id, -1)
      and nvl (x3.servicetype_id, -1) = nvl (x1.servicetype_id, -1)
      and nvl (x3.equipmenttype_id, -1) = nvl (x1.equipmenttype_id, -1)
      and nvl (x3.mot, '-1-') = nvl (x1.mot, '-1-')
      and nvl (x3.commoditycode_id, -1) = nvl (x1.commoditycode_id, -1)
      and (x1.packageid !=
              all (select distinct x6.packageid
                     from lane_active x4, scenario x5, packagelane x6
                    where     x4.rfpid = x1.rfpid
                          and x4.rfpid = x5.rfpid
                          and x4.round_num = x5.round_num
                          and x4.active = 0
                          and x5.scenarioid = x2.scenarioid
                          and x6.rfpid = x4.rfpid
                          and x6.laneid = x4.laneid));

CREATE OR REPLACE FORCE VIEW "OB200SHIP_SITE_VIEW" ("ID", "CONSOLIDATION_ID", "TYPE", "SECTOR", "NAME", "ADDRESS", "COUNTRY_CODE", "POSTAL_CODE", "DESCRIPTION", "RELOAD_DISCOUNT", "IS_VALID", "TC_COMPANY_ID", "RFPID") AS
select distinct x0.facilitycode,
               '',
               '',
               '',
               '',
               '',
               '',
               '',
               '',
               0,
               1,
               x1.tccompanyid,
               x1.rfpid
 from lanefacility x0, rfp x1
where (x1.rfpid = x0.rfpid);

CREATE OR REPLACE FORCE VIEW "SHIPMENT_SIZE_VIEW" ("SHIPMENT_ID", "SIZE_VALUE", "TC_COMPANY_ID", "SIZE_UOM_ID", "SUMMARY_VIEW_FLAG") AS
select sc.shipment_id,
        sum (sc.size_value) size_value,
        sc.tc_company_id,
        sc.size_uom_id,
        sud.summary_view_flag
   from shipment_commodity sc, size_uom_display sud
  where sc.tc_company_id = sud.tc_company_id
        and sc.size_uom_id = sud.size_uom_id
 group by sc.tc_company_id,
        sc.shipment_id,
        sc.size_uom_id,
        sud.summary_view_flag;

CREATE OR REPLACE FORCE VIEW "SHIPMENT_TEMPLATE_SIZE_VIEW" ("SHIPMENT_ID", "SIZE_VALUE", "SIZE_UOM_ID", "SUMMARY_VIEW_FLAG") AS
select sc.shipment_id,
        sum (sc.size_value),
        sc.size_uom_id,
        sud.summary_view_flag
   from shipment_commodity_template sc, size_uom_display sud
  where sc.tc_company_id = sud.tc_company_id
        and sc.size_uom_id = sud.size_uom_id
 group by sc.shipment_id, sc.size_uom_id, sud.summary_view_flag;

CREATE OR REPLACE FORCE VIEW "SHIPMENT_WAYPOINT" ("SHIPMENT_ID", "TC_COMPANY_ID", "O_FACILITY_NUMBER", "O_FACILITY_ID", "O_ADDRESS", "O_CITY", "O_STATE_PROV", "O_POSTAL_CODE", "O_COUNTY", "O_COUNTRY_CODE", "D_FACILITY_NUMBER", "D_FACILITY_ID", "D_ADDRESS", "D_CITY", "D_STATE_PROV", "D_POSTAL_CODE", "D_COUNTY", "D_COUNTRY_CODE", "CONS_RUN_ID", "PICKUP_START_DTTM", "SHIPMENT_STATUS", "PRODUCT_CLASS_ID", "PROTECTION_LEVEL_ID", "INBOUND_REGION_ID", "OUTBOUND_REGION_ID", "FACILITY_ALIAS_ID") AS
select distinct sh.shipment_id,
               sh.tc_company_id,
               sh.o_facility_number,
               sh.o_facility_id,
               sh.o_address,
               sh.o_city,
               sh.o_state_prov,
               sh.o_postal_code,
               sh.o_county,
               sh.o_country_code,
               sh.d_facility_number,
               sh.d_facility_id,
               sh.d_address,
               sh.d_city,
               sh.d_state_prov,
               sh.d_postal_code,
               sh.d_county,
               sh.d_country_code,
               sh.cons_run_id,
               sh.pickup_start_dttm,
               sh.shipment_status,
               sh.product_class_id,
               sh.protection_level_id,
               sh.inbound_region_id,
               sh.outbound_region_id,
               pw.facility_alias_id
 from shipment sh, order_movement om, path_waypoint pw
where     sh.shipment_id = om.shipment_id
      and om.path_set_id = pw.path_set_id
      and om.path_id = pw.path_id;

CREATE OR REPLACE FORCE VIEW "SHIP_BOOK_VIEW" ("BOOKING_ID", "TC_SHIPMENT_ID") AS
select booking_id,
      (select tc_shipment_id
         from shipment
        where shipment_id = bk_shp.shipment_id)
         tc_shipment_id
 from booking_shipment bk_shp;

CREATE OR REPLACE FORCE VIEW "SHIP_INFO_PO_VIEW" ("PONUMBER", "SHIPMENT_ID") AS
(Select Oi.Purchase_Order_Number, oomv.shipment_id From Order_Line_Item Oi,Orders_Order_Movement_View Oomv
Where Oi.Order_Id = Oomv.Order_Id And Oi.Purchase_Order_Number Is Not Null And Oi.Master_Order_Id Is Null
And Oi.Is_Cancelled = 0) Union
(Select Orders.Purchase_Order_Number, oomv.shipment_id From Orders Orders,
Orders_Order_Movement_View Oomv Where Orders.Order_Id = Oomv.Order_Id And Orders.Is_Cancelled = 0 And
ORDERS.PURCHASE_ORDER_NUMBER IS NOT NULL ) UNION
(Select Purchase_Order,Shipment_Id From Shipment Where Shipment.Purchase_Order Is Not Null);

CREATE OR REPLACE FORCE VIEW "SKU" ("SKU_ID", "TC_COMPANY_ID", "SKU", "COMMODITY_CLASS", "DESCRIPTION", "MARK_FOR_DELETION", "CREATED_SOURCE_TYPE", "CREATED_SOURCE", "CREATED_DTTM", "LAST_UPDATED_SOURCE_TYPE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "SKU_VALUE", "CURRENCY_CODE", "COMMODITY_CODE_ID", "UN_NUMBER_ID", "REBATE_VALUE", "PRODUCT_CLASS_ID", "PROTECTION_LEVEL_ID", "SIZE_UOM_ID", "INVENTORY_SIZE_UOM_ID", "BASE_STORAGE_UOM_ID", "DATABASE_QTY_UOM_ID", "DISPLAY_QTY_UOM_ID") AS
select item_cbo.item_id,
      item_cbo.company_id,
      item_cbo.item_name,
      item_cbo.commodity_class_id,
      item_cbo.description,
      item_cbo.mark_for_deletion,
      item_cbo.audit_created_source_type,
      item_cbo.audit_created_source,
      item_cbo.audit_created_dttm,
      item_cbo.audit_last_updated_source_type,
      item_cbo.audit_last_updated_source,
      item_cbo.audit_last_updated_dttm,
      item_tms.item_value,
      item_tms.currency_code,
      item_cbo.commodity_code_id,
      item_cbo.un_number_id,
      item_tms.rebate_value,
      item_cbo.product_class_id,
      item_cbo.protection_level_id,
      item_tms.uom_id,
      item_tms.uom_id,
      item_cbo.base_storage_uom_id,
      item_cbo.database_qty_uom_id,
      item_cbo.display_qty_uom_id
 from    item_cbo item_cbo
      left outer join
         item_tms item_tms
      on (item_cbo.item_id = item_tms.item_id);

CREATE OR REPLACE FORCE VIEW "SKU_ATTRIBUTE" ("SKU_ID", "SKU_SEASON", "SKU_SEASON_YEAR", "SKU_STYLE", "SKU_STYLE_SFX", "SKU_COLOR", "SKU_COLOR_SFX", "SKU_SECOND_DIM", "SKU_QUALITY", "SKU_SIZE_DESC", "SKU_BAR_CODE", "SKU_DESC", "SKU_DESC_SHORT", "SKU_UPC_GTIN", "IS_SEASONAL", "SEASON_CODE", "IS_BONDED", "UNIT_TAX_AMOUNT", "VARIABLE_WEIGHT", "SKU_QUALITY_CODE", "SKU_SIZE_RANGE_CODE") AS
select item_cbo.item_id,
      item_cbo.item_season,
      item_cbo.item_season_year,
      item_cbo.item_style,
      item_cbo.item_style_sfx,
      item_cbo.item_color,
      item_cbo.item_color_sfx,
      item_cbo.item_second_dim,
      item_cbo.item_quality,
      item_cbo.item_size_desc,
      item_cbo.item_bar_code,
      item_cbo.item_name,
      item_cbo.item_desc_short,
      item_cbo.item_upc_gtin,
      item_tms.is_seasonal,
      item_tms.season_code,
      item_tms.is_bonded,
      item_tms.unit_tax_amount,
      item_cbo.variable_weight,
      item_cbo.item_quality_code,
      item_wms.size_range_code
 from item_cbo item_cbo
      left outer join item_tms item_tms
         on (item_cbo.item_id = item_tms.item_id)
      left outer join item_wms item_wms
         on (item_cbo.item_id = item_wms.item_id)
where    (trim (item_bar_code) is not null)
      or (trim (item_color) is not null)
      or (trim (item_color_sfx) is not null)
      or (trim (item_desc_short) is not null)
      or (trim (item_quality) is not null)
      or item_quality_code is not null
      or (trim (item_season) is not null)
      or (trim (item_season_year) is not null)
      or (trim (item_second_dim) is not null)
      or (trim (item_size_desc) is not null)
      or (trim (item_style) is not null)
      or (trim (item_style_sfx) is not null)
      or (trim (item_upc_gtin) is not null)
      or (trim (item_wms.size_range_code) is not null)
      or (item_tms.is_seasonal != 0 and item_tms.is_seasonal is not null)
      or trim (item_tms.season_code) is not null
      or (item_tms.is_seasonal != 0 and item_tms.is_seasonal is not null)
      or (item_tms.unit_tax_amount != 0
          and item_tms.unit_tax_amount is not null)
      or (variable_weight is not null and variable_weight != 0);

CREATE OR REPLACE FORCE VIEW "SKU_ATTRIBUTE_HIDDEN" ("SKU_ID", "BATCH_REQD", "PROD_STAT_REQD", "CNTRY_OF_ORGN_REQD", "CATCH_WT", "PROD_GROUP", "PROD_SUB_GRP", "PROD_TYPE", "PROD_LINE", "SALE_GRP", "OPER_CODE", "STORE_DEPT", "ACTVTN_DATE", "COORD_1", "COORD_2", "PROD_LIFE_IN_DAY", "VOLTY_CODE", "MERCH_TYPE", "MERCH_GROUP", "CRUSH_CODE", "CONVEY_FLAG", "PKT_CONSOL_ATTR", "SIZE_RANGE_CODE", "SIZE_REL_POSN_IN_TABLE", "VENDOR_ITEM_NBR", "UNIT_PRICE", "RETAIL_PRICE", "PRICE_TKT_TYPE", "AVG_DLY_DMND", "MFG_DATE_REQD", "SHIP_BY_DATE_REQD", "XPIRE_DATE_REQD", "MAX_RECV_TO_XPIRE_DAYS", "ALLOW_RCPT_OLDER_SKU", "BUYER_DISP_CODE", "PROMPT_FOR_VENDOR_ITEM_NBR", "MAX_RCPT_QTY", "SRL_NBR_BRCD_TYPE", "DUP_SRL_NBR_FLAG", "MINOR_SRL_NBR_REQ", "INCUB_DAYS", "INCUB_HOURS", "DFLT_INCUB_LOCK", "BASE_INCUB_FLAG", "CONS_PRTY_DATE_WINDOW_INCR", "CONS_PRTY_DATE_CODE", "CONS_PRTY_DATE_WINDOW", "CRITCL_DIM_1", "CRITCL_DIM_2", "CRITCL_DIM_3", "NEST_VOL", "NEST_CNT", "CUBE_MULT_QTY", "PKG_TYPE", "CARTON_TYPE", "UNITS_PER_PICK_ACTIVE", "HNDL_ATTR_ACTIVE", "HNDL_ATTR_RESV", "PICK_WT_TOL_TYPE", "PICK_WT_TOL_AMNT", "MHE_WT_TOL_TYPE", "MHE_WT_TOL_AMNT", "WT_TOL_PCNT", "ECCN_NBR", "EXP_LICN_NBR", "EXP_LICN_XP_DATE", "EXP_LICN_SYMBOL", "ORGN_CERT_CODE", "ITAR_EXEMPT_NBR", "COMMODITY_LEVEL_DESC", "NMFC_CODE", "FRT_CLASS", "FTZ_FLAG", "HTS_NBR", "LOAD_ATTR", "TEMP_ZONE", "TRLR_TEMP_ZONE", "DSP_QTY_UOM", "DB_QTY_UOM", "MAX_CASE_QTY", "SKU_ATTR_REQD", "SPL_INSTR_CODE_1", "SPL_INSTR_CODE_2", "SPL_INSTR_CODE_3", "SPL_INSTR_CODE_4", "SPL_INSTR_CODE_5", "SPL_INSTR_CODE_6", "SPL_INSTR_CODE_7", "SPL_INSTR_CODE_8", "SPL_INSTR_CODE_9", "SPL_INSTR_CODE_10", "SPL_INSTR_1", "SPL_INSTR_2", "PROMPT_PACK_QTY", "STAT_CODE", "UNIT_HT", "UNIT_LEN", "UNIT_WIDTH", "UNIT_WT", "UNIT_VOL") AS
select item_cbo.item_id,
      item_wms.batch_reqd,
      item_wms.prod_stat_reqd,
      item_wms.cntry_of_orgn_reqd,
      item_cbo.catch_weight_item,
      item_wms.prod_catgry,
      item_wms.prod_sub_grp,
      item_cbo.prod_type,
      item_wms.prod_line,
      item_wms.sale_grp,
      item_wms.oper_code,
      item_wms.store_dept,
      item_wms.actvtn_date,
      item_wms.coord_1,
      item_wms.coord_2,
      item_wms.prod_life_in_day,
      item_wms.volty_code,
      item_wms.merch_type,
      item_wms.merch_group,
      item_wms.crush_code,
      item_wms.convey_flag,
      --ITEM_CBO.HAZMAT_CODE,
      item_wms.pkt_consol_attr,
      item_wms.size_range_code,
      item_wms.size_rel_posn_in_table,
      item_wms.vendor_item_nbr,
      item_wms.unit_price,
      item_wms.retail_price,
      item_wms.price_tkt_type,
      item_wms.avg_dly_dmnd,
      item_wms.mfg_date_reqd,
      item_wms.ship_by_date_reqd,
      item_wms.xpire_date_reqd,
      item_wms.max_recv_to_xpire_days,
      item_wms.allow_rcpt_older_item,
      item_wms.buyer_disp_code,
      item_wms.prompt_for_vendor_item_nbr,
      item_wms.max_rcpt_qty,
      item_wms.srl_nbr_brcd_type,
      item_wms.dup_srl_nbr_flag,
      item_wms.minor_srl_nbr_req,
      item_wms.incub_days,
      item_wms.incub_hours,
      item_wms.dflt_incub_lock,
      item_wms.base_incub_flag,
      item_wms.cons_prty_date_window_incr,
      item_wms.cons_prty_date_code,
      item_wms.cons_prty_date_window,
      item_wms.critcl_dim_1,
      item_wms.critcl_dim_2,
      item_wms.critcl_dim_3,
      item_wms.nest_vol,
      item_wms.nest_cnt,
      item_wms.cube_mult_qty,
      item_wms.pkg_type,
      item_wms.carton_type,
      item_wms.units_per_pick_active,
      item_wms.hndl_attr_active,
      item_wms.hndl_attr_resv,
      item_wms.pick_wt_tol_type,
      item_wms.pick_wt_tol_amnt,
      item_wms.mhe_wt_tol_type,
      item_wms.mhe_wt_tol_amnt,
      item_wms.wt_tol_pcnt,
      item_wms.eccn_nbr,
      item_wms.exp_licn_nbr,
      item_wms.exp_licn_xp_date,
      item_wms.exp_licn_symbol,
      item_wms.orgn_cert_code,
      item_wms.itar_exempt_nbr,
      item_cbo.commodity_level_desc,
      item_wms.nmfc_code,
      item_wms.frt_class,
      cast (null as varchar (1)) as ftz_flag,
      cast (null as varchar (12)) as hts_nbr,
      item_wms.load_attr,
      item_wms.temp_zone,
      item_wms.trlr_temp_zone,
      item_cbo.display_qty_uom_id,
      item_cbo.database_qty_uom_id,
      item_wms.max_case_qty,
      item_wms.item_attr_reqd,
      item_wms.spl_instr_code_1,
      item_wms.spl_instr_code_2,
      item_wms.spl_instr_code_3,
      item_wms.spl_instr_code_4,
      item_wms.spl_instr_code_5,
      item_wms.spl_instr_code_6,
      item_wms.spl_instr_code_7,
      item_wms.spl_instr_code_8,
      item_wms.spl_instr_code_9,
      item_wms.spl_instr_code_10,
      item_wms.spl_instr_1,
      item_wms.spl_instr_2,
      item_wms.prompt_pack_qty,
      item_cbo.status_code,
      item_cbo.unit_height,
      item_cbo.unit_length,
      item_cbo.unit_width,
      item_cbo.unit_weight,
      item_cbo.unit_volume
 from    item_cbo item_cbo
      left outer join
         item_wms item_wms
      on item_cbo.item_id = item_wms.item_id;

CREATE OR REPLACE FORCE VIEW "SKU_BUDGETED_COST" ("SKU_BC_ID", "SKU_ID", "BUDGETED_COST", "BC_CURRENCY_CODE", "BC_UOM", "BC_ORIGIN_STATE_PROV", "BC_ORIGIN_COUNTRY_CODE", "BC_DESTINATION_STATE_PROV", "BC_DESTINATION_COUNTRY_CODE", "COMMENTS", "BC_UOM_ID") AS
select item_budgeted_cost_tms.item_budgeted_cost_id,
      item_budgeted_cost_tms.item_id,
      item_budgeted_cost_tms.cost,
      item_budgeted_cost_tms.currency_code,
      item_budgeted_cost_tms.uom,
      item_budgeted_cost_tms.origin_state_prov,
      item_budgeted_cost_tms.origin_country_code,
      item_budgeted_cost_tms.destination_state_prov,
      item_budgeted_cost_tms.destination_country_code,
      item_budgeted_cost_tms.comments,
      item_budgeted_cost_tms.uom_id
 from item_budgeted_cost_tms item_budgeted_cost_tms;

CREATE OR REPLACE FORCE VIEW "SKU_MONETARY_VALUE" ("SKU_ID", "MONETARY_VALUE_ID", "MONETARY_VALUE", "CURRENCY_CODE", "TC_COMPANY_ID", "SIZE_UOM", "EFFECTIVE_DT", "EXPIRATION_DT", "SIZE_UOM_ID") AS
select item_cost_cbo.item_id,
      item_cost_cbo.cost_type_id,
      item_cost_cbo.unit_cost,
      item_cost_cbo.currency_code,
      item_cbo.company_id,
      size_uom.size_uom,
      item_cost_cbo.effective_dt,
      item_cost_cbo.expiration_dt,
      item_cost_cbo.uom_id
 from item_cost_cbo item_cost_cbo
      join item_cbo item_cbo
         on (item_cost_cbo.item_id = item_cbo.item_id)
      left outer join size_uom size_uom
         on (item_cost_cbo.uom_id = size_uom.size_uom_id);

CREATE OR REPLACE FORCE VIEW "SKU_SIZE" ("SKU_ID", "SIZE_UOM", "TC_COMPANY_ID", "SIZE_VALUE", "SIZE_UOM_ID") AS
select item_size_tms.item_id,
      size_uom.size_uom,
      item_cbo.company_id,
      item_size_tms.uom_value,
      item_size_tms.uom_id
 from item_size_tms item_size_tms
      join item_cbo item_cbo
         on (item_size_tms.item_id = item_cbo.item_id)
      left outer join size_uom size_uom
         on (item_size_tms.uom_id = size_uom.size_uom_id);

CREATE OR REPLACE FORCE VIEW "SKU_SIZE_UOM_PACKAGE_TYPE_MAP" ("SKU_ID", "TC_COMPANY_ID", "SIZE_UOM", "PACKAGE_TYPE_ID", "SIZE_UOM_VALUE", "SIZE_UOM_ID") AS
select item_package_cbo.item_id,
      item_cbo.company_id,
      size_uom.size_uom,
      cast (null as number (8)) as package_type_id,
      item_package_cbo.quantity,
      item_package_cbo.package_uom_id
 from item_package_cbo item_package_cbo
      join item_cbo item_cbo
         on (item_package_cbo.item_id = item_cbo.item_id)
      left outer join size_uom size_uom
         on (item_package_cbo.package_uom_id = size_uom.size_uom_id);

CREATE OR REPLACE FORCE VIEW "SKU_SUBSTITUTE_DTL" ("SKU_SUBS_DETAIL_ID", "CREATED_SOURCE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "VERSION", "CREATED_DTTM", "QUANTITY_RATIO", "RANK", "SKU_SUBSTITUTION_ID", "TC_COMPANY_ID", "SUBSTITUTED_SKU_ID") AS
select item_substitution_dtl_cbo.item_sub_dtl_id,
      item_substitution_dtl_cbo.audit_created_source,
      item_substitution_dtl_cbo.audit_last_updated_source,
      item_substitution_dtl_cbo.audit_last_updated_dttm,
      item_substitution_dtl_cbo.version,
      item_substitution_dtl_cbo.audit_created_dttm,
      item_substitution_dtl_cbo.sub_item_qty_ratio,
      item_substitution_dtl_cbo.rank,
      item_substitution_dtl_cbo.item_sub_hdr_id,
      item_substitution_cbo.company_id,
      item_substitution_dtl_cbo.sub_item_id
 from item_substitution_cbo item_substitution_cbo,
      item_substitution_dtl_cbo item_substitution_dtl_cbo
where item_substitution_cbo.item_sub_hdr_id =
         item_substitution_dtl_cbo.item_sub_hdr_id;

CREATE OR REPLACE FORCE VIEW "SKU_SUBSTITUTION" ("SKU_SUBSTITUTION_ID", "TC_COMPANY_ID", "CREATED_SOURCE", "LAST_UPDATED_SOURCE", "LAST_UPDATED_DTTM", "VERSION", "CREATED_DTTM", "SUBSTITUTION_RULE_NAME", "SUBSTITUTION_DESCRIPTION", "SUBSTITUTION_LEVEL", "SUBSTITUTION_TYPE", "BASE_SKU_ID", "START_DATE", "END_DATE", "HAS_WARNINGS") AS
select item_substitution_cbo.item_sub_hdr_id,
      item_substitution_cbo.company_id,
      item_substitution_cbo.audit_created_source,
      item_substitution_cbo.audit_last_updated_source,
      item_substitution_cbo.audit_last_updated_dttm,
      item_substitution_cbo.version,
      item_substitution_cbo.audit_created_dttm,
      item_substitution_cbo.sub_rule_name,
      item_substitution_cbo.sub_desc,
      item_substitution_cbo.sub_level,
      item_substitution_cbo.sub_type_id,
      item_substitution_cbo.base_item_id,
      item_substitution_cbo.start_date,
      item_substitution_cbo.end_date,
      item_substitution_cbo.has_warnings
 from item_substitution_cbo;

CREATE OR REPLACE FORCE VIEW "SPECIALREQDROPDOWNVALS_V" ("SPECREQID", "SPECIALREQVALUEID", "DISPLAYORDER", "DESCRIPTION", "VALUE", "DEFAULTFLAG") AS
select x1.specreqid,
      x0.specreqvalueid,
      x0.displayorder,
      x0.description,
      x0.value,
      x0.defaultflag
 from specreqvalue x0, specreqvaluemap x1
where x1.specreqvalueid = x0.specreqvalueid;

CREATE OR REPLACE FORCE VIEW "SPECREQENTRYALL_V" ("SOURCE", "RFPID", "TCCOMPANYID", "SPECREQCODE", "SPECREQDESCRIPTION", "HEADINGSUBTYPE", "HEADINGSUBTYPEDESCRIPTION", "DATASOURCE", "DATASOURCEDESCRIPTION", "UOM", "UOMDESCRIPTION", "SPECREQID", "SPECREQHEADINGID", "SUPPORTED_MODES", "TCDATATYPE", "TPDATATYPE", "PARENTSPECREQID", "DISPLAYORDER", "MAXLENGTH", "ISREQUIRED", "RELATEDSPECREQID", "RELATIONSHIPCODE") AS
select 'obl:specreqentryaccessorial',
      null,
      x0.tccompanyid,
      x0.specreqcode,
      coalesce (x2.description, x2.accessorial_code),
      x2.accessorial_type,
      x1.description,
      x2.data_source,
      case
         when (x3.description is null) then x4.description
         else x3.description
      end,
      x2.distance_uom,
      x5.description,
      x0.specreqid,
      1,
      null,
      'SB',
      '$',
      null,
      null,
      null,
      1,
      null,
      null
 from specreqentryaccessorial x0
      inner join accessorial_code x2
         on x0.specreqcode = x2.accessorial_code
            and x0.tccompanyid = x2.tc_company_id
      inner join accessorial_type x1
         on x2.accessorial_type = x1.accessorial_type
      left join data_source x3
         on x2.data_source = x3.data_source
      left join size_uom x4
         on x2.data_source = to_char (x4.size_uom_id)
      left join distance_uom x5
         on x2.distance_uom = x5.distance_uom
 union
 select 'obl:specreqentryprofilecustom',
      x6.rfpid,
      x6.tccompanyid,
      null,
      x6.specreqdescription,
      null,
      null,
      null,
      null,
      null,
      null,
      x6.specreqid,
      2,
      case when x6.modeid is null then 0 else x6.modeid end,
      x6.tcdatatype,
      x6.tpdatatype,
      null,
      9999,
      x6.maxlength,
      1,
      null,
      null
 from specreqentryprofilecustom x6
 union
 select 'obl:specreqentry',
      null,
      null,
      null,
      x7.description,
      null,
      null,
      null,
      null,
      null,
      null,
      x7.specreqid,
      x7.specreqheadingid,
      x7.supported_modes,
      x7.tcdatatype,
      x7.tpdatatype,
      x7.parentspecreqid,
      x7.displayorder,
      x7.maxlength,
      x7.isrequired,
      x7.relatedspecreqid,
      x7.relationshipcode
 from specreqentry x7
where x7.deleted = 0;

CREATE OR REPLACE FORCE VIEW "STATE_CODE" ("STATE", "CNTRY", "STATE_NAME", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID") AS
select state_prov,
      country_code,
      state_prov_name,
      cast (null as varchar (1)),
      cast (null as varchar (1)),
      cast (null as varchar (1))
 from state_prov;

CREATE OR REPLACE FORCE VIEW "STOP_OFF_CHARGE_VIEW" ("ACCESSORIAL_PARAM_SET_ID", "STOP_OFF_CHARGE_ID", "INIT_STOP_OFF", "FINAL_STOP_OFF", "STOP_CHARGE", "CURRENCY_CODE", "HAS_ERRORS") AS
select aps.accessorial_param_set_id,
      soc.stop_off_charge_id,
      soc.start_number,
      soc.end_number,
      soc.value,
      aps.stop_off_currency_code,
      0
 from accessorial_param_set aps, stop_off_charge soc
where aps.accessorial_param_set_id = soc.accessorial_param_set_id
 union all
 select iaps.accessorial_param_set_id,
      isoc.stop_off_charge_id,
      isoc.start_number,
      isoc.end_number,
      isoc.value,
      iaps.stop_off_currency_code,
      case isoc.status when 4 then 1 else 0 end as has_errors
 from import_stop_off_charge isoc, import_accessorial_param_set iaps
where isoc.param_id = iaps.param_id and iaps.status = 1
 union all
 select iaps.param_id,
      isoc.stop_off_charge_id,
      isoc.start_number,
      isoc.end_number,
      isoc.value,
      iaps.stop_off_currency_code,
      case isoc.status when 4 then 1 else 0 end as has_errors
 from import_stop_off_charge isoc, import_accessorial_param_set iaps
where isoc.param_id = iaps.param_id and iaps.status = 4;

CREATE OR REPLACE FORCE VIEW "STOP_ORDER_SIZE_VIEW" ("SHIPMENT_ID", "STOP_SEQ", "SUMMARY_VIEW_FLAG", "SIZE_VALUE") AS
select stop_action.shipment_id,
        stop_action.stop_seq,
        sud.summary_view_flag,
        sum (olis.size_value)
   from stop_action,
        stop_action_order,
        order_line_item_size olis,
        size_uom_display sud
  where     stop_action.shipment_id = stop_action_order.shipment_id
        and stop_action.stop_seq = stop_action_order.stop_seq
        and stop_action.stop_action_seq = stop_action_order.stop_action_seq
        and stop_action_order.order_id = olis.order_id
        and olis.size_uom_id = sud.size_uom_id
        and olis.tc_company_id = sud.tc_company_id
 group by stop_action.stop_seq,
        stop_action.shipment_id,
        sud.summary_view_flag;

CREATE OR REPLACE FORCE VIEW "TC_CARRIER_CODE_VIEW" ("TC_COMPANY_ID", "CARRIER_CODE", "TP_COMPANY_ID", "CARRIER_CODE_NAME", "ADDRESS", "CITY", "STATE_PROV", "POSTAL_CODE", "COUNTRY_CODE", "COMM_METHOD", "ACCEPT_MSG_REQD", "APPT_MSG_REQD", "DEPART_MSG_REQD", "ARRIVE_MSG_REQD", "IS_AUTO_DELIVER", "CONFIRM_RECALL_REQD", "CONFIRM_UPDATE_REQD", "ALLOW_BOL_ENTRY", "ALLOW_TRAILER_ENTRY", "ALLOW_TRACTOR_ENTRY", "ALLOW_LOAD_COMPLETION", "DOCK_SCHEDULE_PERMISSION", "CARRIER_ID", "ALLOW_CARRIER_BOOKING_REF", "ALLOW_FORWARDER_AWB", "ALLOW_MASTER_AWB") AS
select tc_company_id,
      carrier_code,
      tp_company_id,
      carrier_code_name,
      address,
      city,
      state_prov,
      postal_code,
      country_code,
      comm_method,
      accept_msg_reqd,
      appt_msg_reqd,
      depart_msg_reqd,
      arrive_msg_reqd,
      is_auto_deliver,
      confirm_recall_reqd,
      confirm_update_reqd,
      allow_bol_entry,
      allow_trailer_entry,
      allow_tractor_entry,
      allow_load_completion,
      dock_schedule_permission,
      carrier_id,
      allow_carrier_booking_ref,
      allow_forwarder_awb,
      allow_master_awb
 from carrier_code;

CREATE OR REPLACE FORCE VIEW "TRIPLATLONG" ("TRIP_ID", "LATITUDE", "LONGITUDE", "SPEED", "SPEED_UOM_CODE", "DIRECTION", "TRACKING_INDICATOR", "TRACKING_MSG_ID", "NXT_STOP_ETA") AS
select msg1.trip_id,
      msg1.latitude,
      msg1.longitude,
      msg1.speed,
      msg1.speed_uom_code,
      msg1.direction,
      msg1.tracking_indicator,
      msg1.tracking_msg_id,
      msg1.nxt_stop_eta
 from tracking_message msg1
where msg1.tracking_msg_id in
         (select max (msg2.tracking_msg_id)
            from tracking_message msg2
           where msg1.trip_id = msg2.trip_id
                 and msg2.nxt_stop_eta is not null);

CREATE OR REPLACE FORCE VIEW "X_ACCESSORIAL_PARAM_SET" ("ACCESSORIAL_PARAM_SET_ID", "TC_COMPANY_ID", "COMMODITY_CLASS", "EFFECTIVE_DT", "EXPIRATION_DT", "CM_RATE_TYPE", "CM_RATE_CURRENCY_CODE", "STOP_OFF_CURRENCY_CODE", "DISTANCE_UOM", "IS_GLOBAL", "EQUIPMENT_ID", "SIZE_UOM_ID", "CARRIER_ID", "BUDGETED_APPROVED", "USE_FAK_COMMODITY", "IS_VALID", "PARAM_ID") AS
select	ACCESSORIAL_PARAM_SET_ID, coalesce(TC_COMPANY_ID,0), COMMODITY_CLASS, EFFECTIVE_DT, EXPIRATION_DT, CM_RATE_TYPE, CM_RATE_CURRENCY_CODE, STOP_OFF_CURRENCY_CODE, DISTANCE_UOM,
		IS_GLOBAL, coalesce(EQUIPMENT_ID,0), SIZE_UOM_ID, coalesce(CARRIER_ID,0), BUDGETED_APPROVED, USE_FAK_COMMODITY, 1 AS IS_VALID, 0 AS PARAM_ID
from	ACCESSORIAL_PARAM_SET
union
select	ACCESSORIAL_PARAM_SET_ID, coalesce(TC_COMPANY_ID,0), COMMODITY_CLASS, EFFECTIVE_DT, EXPIRATION_DT, CM_RATE_TYPE, CM_RATE_CURRENCY_CODE, STOP_OFF_CURRENCY_CODE, DISTANCE_UOM,
		IS_GLOBAL, coalesce(EQUIPMENT_ID,0), SIZE_UOM_ID, coalesce(CARRIER_ID,0), BUDGETED_APPROVED, USE_FAK_COMMODITY, 0 AS IS_VALID, PARAM_ID
from	IMPORT_ACCESSORIAL_PARAM_SET;

CREATE OR REPLACE FORCE VIEW "YARD_SLOT_VIEW" ("PARENT_ID", "STATUS", "LOCN_CLASS", "MAX_CAPACITY", "LOCN_ID", "IS_GUARD_HOUSE", "CURRENT_CAPACITY", "ID", "FACILITY_ID", "YARD_ID", "LOCATION_NAME", "PARENT_LOCATION_NAME", "LOCN_BRCD", "DSP_LOCN", "CHECK_DIGIT", "TC_COMPANY_ID", "YARD_NAME") AS
(SELECT TO_CHAR (YZS.YARD_ZONE_ID) AS PARENT_ID,
         YZS.YARD_ZONE_SLOT_STATUS AS STATUS,
         LH.LOCN_CLASS AS LOCN_CLASS,
         YZS.MAX_CAPACITY AS MAX_CAPACITY,
         LH.LOCN_ID AS LOCN_ID,
         YZS.IS_GUARD_HOUSE AS IS_GUARD_HOUSE,
         YZS.USED_CAPACITY AS CURRENT_CAPACITY,
         YZS.YARD_ZONE_SLOT_ID AS id,
         FY.FACILITY_ID AS FACILITY_ID,
         YZS.YARD_ID AS YARD_ID,
         YZS.YARD_ZONE_SLOT_NAME AS LOCATION_NAME,
         YZ.YARD_ZONE_NAME AS PARENT_LOCATION_NAME,
         LH.LOCN_BRCD AS LOCN_BRCD,
         LH.DSP_LOCN AS DSP_LOCN,
         LH.CHECK_DIGIT AS CHECK_DIGIT,
         YD.TC_COMPANY_ID AS TC_COMPANY_ID,
         YD.YARD_NAME AS YARD_NAME
    FROM YARD_ZONE_SLOT YZS,
         LOCN_HDR LH,
         YARD_ZONE YZ,
         FACILITY_YARD FY,
         YARD YD
   WHERE     YZS.LOCN_ID = LH.LOCN_ID
         AND YZS.YARD_ZONE_ID = YZ.YARD_ZONE_ID
         AND YZS.YARD_ID = YZ.YARD_ID
         AND FY.YARD_ID = YZS.YARD_ID
         AND YD.YARD_ID = YZS.YARD_ID
         AND YZ.YARD_ID = YD.YARD_ID
         AND LH.LOCN_CLASS = 'Y');

CREATE OR REPLACE FORCE VIEW "ZONE_VIEW" ("TC_COMPANY_ID", "ZONE_ID", "ATTRIBUTE_TYPE", "ATTRIBUTE_VALUE", "COUNTRY_CODE", "ZONE_ATTRIB_SEQ") AS
select zone_attribute.tc_company_id,
      zone_attribute.zone_id,
      zone_attribute.attribute_type,
      ltrim (rtrim (zone_attribute.attribute_value)),
      zone_attribute.country_code,
      zone_attribute.zone_attrib_seq
 from zone_attribute;

CREATE OR REPLACE FORCE VIEW "VAL_JOB_DATA_VW" ("JOB_NAME", "VAL_JOB_DTL_ID", "SQL_ID", "BIND_VAL_LIST_CSV", "RESULT_LIST_CSV") AS
select vjh.job_name, vjd.val_job_dtl_id, vjd.sql_id,
  wm_get_bind_values_for_job_dtl(vjd.val_job_dtl_id),
  wm_get_exp_results_for_job_dtl(vjd.val_job_dtl_id)
from val_job_hdr vjh
join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id;

CREATE OR REPLACE FORCE VIEW "VAL_JOB_RESULT_SUMM_VW" ("FLOW_NAME", "PGM_ID", "INTRNL_JOB_NAME", "CURR_NUM_SQLS", "CURR_DISABLED", "VAL_TXN_ID", "JOB_TXN_ID", "PASSED", "FAILED", "FIRST_SQL_END_TIME", "LAST_SQL_END_TIME") AS
with curr_job_data as
(
  select vjh.job_name, vjh.val_job_hdr_id, count(*) curr_num_sqls,
      sum(case when vjd.is_locked = 1 or vs.is_locked = 1 then 1
      else 0 end) curr_disabled
  from val_job_hdr vjh
  join val_job_dtl vjd on vjd.val_job_hdr_id = vjh.val_job_hdr_id
  join val_sql vs on vs.sql_id = vjd.sql_id
  group by vjh.job_name, vjh.val_job_hdr_id
)
select coalesce(vrh.app_hook, vjs.app_hook) flow_name, coalesce(vrh.pgm_id, vjs.pgm_id) pgm_id,
  cjd.job_name intrnl_job_name, min(cjd.curr_num_sqls) curr_num_sqls,
  min(cjd.curr_disabled) curr_disabled, vrh.parent_txn_id val_txn_id, vrh.txn_id job_txn_id,
  sum(vrh.last_run_passed) passed,
  sum(decode(vrh.last_run_passed, 0, 1, decode(vrh.parent_txn_id, null, null, 0))) failed,
  min(vrh.create_date_time) first_sql_end_time, max(vrh.create_date_time) last_sql_end_time
from val_job_seq vjs
join curr_job_data cjd on cjd.job_name = vjs.job_name
join val_job_dtl vjd on vjd.val_job_hdr_id = cjd.val_job_hdr_id
left join val_result_hist vrh on vrh.val_job_dtl_id = vjd.val_job_dtl_id
  and vrh.app_hook = vjs.app_hook and vrh.pgm_id = vjs.pgm_id
group by coalesce(vrh.app_hook, vjs.app_hook), coalesce(vrh.pgm_id, vjs.pgm_id), cjd.job_name,
  vrh.txn_id, vrh.parent_txn_id;

CREATE OR REPLACE FORCE VIEW "VAL_JOB_RESULT_VW" ("VAL_TXN_ID", "JOB_TXN_ID", "CREATE_DATE_TIME", "JOB_NAME", "PASSED", "MSG", "SQL_IDENT", "SQL_TEXT", "PGM_ID", "BIND_SUBS_SQL_TEXT", "FLOW_NAME") AS
select vrh.parent_txn_id val_txn_id, vrh.txn_id job_txn_id, vrh.create_date_time, vjh.job_name,
  vrh.last_run_passed passed, vrh.msg, vs.ident sql_ident, vs.sql_text, vrh.pgm_id,
  wm_val_get_bind_subs_sql(vjd.val_job_dtl_id, vrh.txn_id, vs.sql_text) bind_subs_sql_text,
  vrh.app_hook flow_name
from val_result_hist vrh
join val_job_dtl vjd on vjd.val_job_dtl_id = vrh.val_job_dtl_id
join val_job_hdr vjh on vjh.val_job_hdr_id = vjd.val_job_hdr_id
join val_sql vs on vs.sql_id = vjd.sql_id;

CREATE OR REPLACE FORCE VIEW "VAL_JOB_SQL_VW" ("IDENT", "SQL_ID", "JOB_NAME", "DISABLED") AS
select vs.ident, vs.sql_id, vjh.job_name,
  case when vjd.is_locked = 1 or vs.is_locked = 1 then 1 else 0 end disabled
from val_sql vs
join val_job_dtl vjd on vjd.sql_id = vs.sql_id
join val_job_hdr vjh on vjh.val_job_hdr_id = vjd.val_job_hdr_id;

CREATE OR REPLACE FORCE VIEW "VAL_SQL_DATA_VW" ("IDENT", "SQL_ID", "SQL_TEXT", "RAW_TEXT", "BIND_VAR_LIST_CSV", "COL_LIST_CSV") AS
select vs.ident, vs.sql_id, vs.sql_text, vs.raw_text, vs.bind_var_list bind_var_list_csv,
  wm_val_get_col_list_for_sql(vs.sql_id) col_list_csv
from val_sql vs;

CREATE OR REPLACE FORCE VIEW "VAL_SQL_TAG_SUMM_VW" ("SQL_ID", "SQL_IDENT", "TYPE_LIST", "TAG_LIST", "TYPE_TAG_LIST") AS
select vs.sql_id, vs.ident sql_ident,
  listagg(decode(vst.type, '1', 'select', '2', 'from', '3', 'via', '4', 'module', '5', 'vertical', '6', 'desc', vst.type), ' || ')
      within group(order by vst.type) type_list,
  listagg(vst.tag, ' || ') within group(order by vst.type) tag_list,
  listagg(decode(vst.type, '1', 'select', '2', 'from', '3', 'via', '4', 'module', '5', 'vertical', '6', 'desc', vst.type) || ' '
      || vst.tag, ' || ') within group(order by vst.type) type_tag_list
from val_sql_tags vst
join val_sql vs on vs.sql_id = vst.sql_id
group by vs.sql_id, vs.ident;

CREATE OR REPLACE FORCE VIEW "VIEW_DSP_TRANSACTION" ("TXN", "DSP_ID", "TC_COMPANY_ID", "CARRIER_CODE", "DRIVER_ID", "RULE", "VALUE", "FACILITY", "START_DTTM", "END_DTTM", "CARRIER_ID") AS
select type.desription txn,
      txn.dsp_transaction_id dsp_id,
      txn.tc_company_id,
      cc.carrier_code,
      comp.driver_id,
      description rule,
      computed_value value,
      txn.facility_alias_id facility,
      txn.transaction_start_dttm start_dttm,
      txn.transaction_end_dttm end_dttm,
      comp.carrier_id carrier_id
 from dsp_driver_log_comp comp,
      dsp_dr_type,
      dsp_transaction txn,
      dsp_transaction_type type,
      carrier_code cc
where     dsp_dr_type_id = dsp_dr_id
      and dsp_index = 1
      and txn.dsp_transaction_id = comp.dsp_transaction_id
      and txn.dsp_transaction_type = type.dsp_transaction_type
      and cc.carrier_id = comp.carrier_id;

CREATE OR REPLACE FORCE VIEW "VIEW_LRF_EVENTS" ("EVENT_ID", "LRF_EVENT_ID", "LRF_EVENT_NAME", "GROUP_ID", "EVENT_START_DTTM", "EVENT_EXP_DATE", "OM_CATEGORY", "EVENT_TIMESTAMP", "EVENT_FREQ_IN_DAYS", "EVENT_FREQ_PER_DAY", "EXECUTED_DTTM", "IS_EXECUTED", "EVENT_OBJECTS", "EVENT_TYPE", "EVENT_CNT", "EVENT_FREQ_IN_DAY_OF_MONTH", "FREQ_TYPE", "FREQ_VAL_1", "FREQ_VAL_2", "SCHEDULED_DTTM", "SCHEDULED_TIME", "CREATED_DTTM", "MODIFIED_DTTM", "CREATED_BY", "MODIFIED_BY", "PREVIOUS_SCHEDULED_DTTM", "EXCLUDE_WEEKEND") AS
(select om.event_id,
       lrf_event_id,
       lrf_event_name,
       group_id,
       event_start_dttm,
       event_exp_date,
       om_category,
       event_timestamp,
       event_freq_in_days,
       event_freq_per_day,
       executed_dttm,
       is_executed,
       event_objects,
       event_type,
       event_cnt,
       event_freq_in_day_of_month,
       freq_type,
       freq_val_1,
       freq_val_2,
       scheduled_dttm,
       scheduled_time,
       lrf.created_dttm,
       modified_dttm,
       created_by,
       modified_by,
       previous_scheduled_dttm,
       exclude_weekend
  from lrf_event lrf, lrf_sched_event_report om
 where lrf.event_id = om.event_id);

CREATE OR REPLACE FORCE VIEW "VIEW_MOTS_FOR_PARCEL" ("TC_COMPANY_ID", "MOT_ID", "MOT", "DESCRIPTION") AS
select tc_company_id,
      mot_id,
      mot,
      description
 from mot
where standard_mot = 28;

CREATE OR REPLACE FORCE VIEW "VLD_ACC_PARM_SET_V" ("ACCESSORIAL_PARAM_SET_ID", "TC_COMPANY_ID", "CARRIER_ID", "USE_FAK_COMMODITY", "COMMODITY_CLASS", "EQUIPMENT_ID", "EFFECTIVE_DT", "EXPIRATION_DT", "CM_RATE_TYPE", "CM_RATE_CURRENCY_CODE", "STOP_OFF_CURRENCY_CODE", "DISTANCE_UOM", "BUDGETED_APPROVED", "SIZE_UOM_ID", "IS_GLOBAL", "IS_VALID", "DTL_HAS_ERRORS") AS
select aps.accessorial_param_set_id,
      aps.tc_company_id,
      aps.carrier_id,
      aps.use_fak_commodity,
      aps.commodity_class,
      aps.equipment_id,
      aps.effective_dt,
      aps.expiration_dt,
      aps.cm_rate_type,
      aps.cm_rate_currency_code,
      aps.stop_off_currency_code,
      aps.distance_uom,
      aps.budgeted_approved,
      aps.size_uom_id,
      aps.is_global,
      1 is_valid,
      nvl (
         (select sum (decode (icrd.status, 4, 1, 0))
            from import_accessorial_param_set iaps,
                 import_cm_rate_discount icrd
           where aps.accessorial_param_set_id =
                    iaps.accessorial_param_set_id
                 and aps.tc_company_id = iaps.tc_company_id
                 and iaps.param_id = icrd.param_id),
         0)
      + nvl (
           (select sum (decode (isoc.status, 4, 1, 0))
              from import_accessorial_param_set iaps,
                   import_stop_off_charge isoc
             where aps.accessorial_param_set_id =
                      iaps.accessorial_param_set_id
                   and aps.tc_company_id = iaps.tc_company_id
                   and iaps.param_id = isoc.param_id),
           0)
      + nvl (
           (select sum (decode (iar.status, 4, 1, 0))
              from import_accessorial_param_set iaps,
                   import_accessorial_rate iar
             where aps.accessorial_param_set_id =
                      iaps.accessorial_param_set_id
                   and aps.tc_company_id = iaps.tc_company_id
                   and iaps.param_id = iar.param_id
                   and iaps.tc_company_id = iar.tc_company_id),
           0)
         dtl_has_errors
 from accessorial_param_set aps;

CREATE OR REPLACE FORCE VIEW "VW_DB_BUILD_MAX_VERSION" ("VERSION_LABEL", "LAYER", "MAJOR", "MINOR", "REV1", "REV2", "REV3", "START_DTTM") AS
select version_label               ,regexp_substr(version_label,'[^_]+',1,1) layer               ,to_number(regexp_substr(fullver,'[^.]+',1,1)) major               ,to_number(regexp_substr(fullver,'[^.]+',1,2)) minor               ,to_number(regexp_replace(regexp_substr(fullver,'[^.]+',1,3),'[A-Za-z]*','')*1) rev1               ,to_number(coalesce(regexp_substr(fullver,'[^.]+',1,4),'0')) rev2               ,to_number(coalesce(regexp_substr(fullver,'[^.]+',1,5),'0')) rev3               ,start_dttm           from (select version_label                       ,regexp_substr(version_label,'[^_]+',1,2) fullver                       ,start_dttm                 from db_build_history                 where version_label like 'SCPP%'                 or version_label like 'MDA%'                 or version_label like 'CBO%'                 or version_label like 'LMARCH%'                 or (version_label like 'WM%'))            order by 2 desc,3 desc,4 desc,5 desc NULLS LAST,6 desc NULLS LAST,7 desc nulls last, 1 desc;

CREATE OR REPLACE FORCE VIEW "VW_WHSE_MASTER" ("WHSE", "WHSE_NAME", "WHSE_ADDR_ID", "ACTV_REPL_ORGN", "ADJ_ALG_FLAG", "AES_FILE_UPLOAD_PATH", "AES_PSWD", "ALLOW_CLOSE_LOAD_HELD_BATCH", "ALLOW_LOAD_HELD_BATCH", "ALLOW_MANIF_HELD_BATCH", "ALLOW_PAKNG_W_OUT_CASE_NBR", "ALLOW_RCV_W_OUT_CASE_ASN", "ALLOW_RCV_W_OUT_CASE_NBR", "ALLOW_RCV_W_OUT_PO_NBR", "ALLOW_RCV_W_OUT_SHPMT", "ALLOW_REPL_TASK_UPDT_PRTY", "ALLOW_RLS_QUAL_HELD_ON_RCPT", "ALLOW_SKU_NOT_ON_PO", "ALLOW_SKU_NOT_ON_SHPMT", "ALLOW_SRL_CREATE", "ALLOW_WEIGH_HELD_BATCH", "ALLOW_WORK_ORD_QTY_CHG", "ALLOW_WORK_ORD_SKU_ADDN", "ASN_CASE_LVL_VERF_PIX", "ASN_SKU_LVL_VERF_PIX", "ASSIGN_AND_PRT_BOL", "ASSIGN_AND_PRT_MANIF", "ASSIGN_EPC_IN_ACTIVE_FLAG", "ASSIGN_PRO_NBR", "AUTO_MANIF_AT_PNH", "BASE_INCUB_FLAG", "BATCH_NBR_EQ_VENDOR_BATCH_NBR", "CALC_CASE_SIZE_TYPE", "CALC_REPL_CTRL", "CALC_TO_BE_LOCD", "CARTON_BREAK_ON_AISLE_CHG", "CARTON_BREAK_ON_AREA_CHG", "CARTON_BREAK_ON_ZONE_CHG", "CARTON_TYPE", "CARTON_WT_TOL", "CASE_ALLOC_STRK", "CASE_NBR_DSP_USAGE", "CASE_WGHT_TOL", "CC_TOLER_UOM", "CC_VAR_TOLER_VALUE", "CHKIN_WINDOW", "CMBIN_REPL_PRTY", "CNTY", "CO_ID", "CO_ID_NAME", "CPCTY_EST_FLAG", "CREATE_ASN_FLAG", "CREATE_LOAD", "CREATE_MAJOR_PKT", "CREATE_SHPMT", "CUBISCAN_IGNORE_COLOR", "CUBISCAN_IGNORE_COLOR_SFX", "CUBISCAN_IGNORE_QUAL", "CUBISCAN_IGNORE_SEC_DIM", "CUBISCAN_IGNORE_SIZE_DESC", "CUBISCAN_IGNORE_STYLE_SFX", "CUBISCAN_OVERWRITE", "DATE_MASK", "DEFAULT_PRICE_TIX_LOCK_CODE", "DFLT_ALLOC_CUTOFF", "DFLT_ALLOC_TYPE", "DFLT_BOL_TYPE", "DFLT_CAPCTY_UOM", "DFLT_CASE_SIZE_TYPE", "DFLT_CONS_DATE", "DFLT_CONS_PRTY_MXD", "DFLT_CONS_PRTY_PARTL", "DFLT_CONS_PRTY_SINGL_SKU_FULL", "DFLT_CONS_SEQ_MXD", "DFLT_CONS_SEQ_PARTL", "DFLT_CONS_SEQ_SINGL_SKU_FULL", "DFLT_DB_QTY_UOM", "DFLT_DB_VOL_UOM", "DFLT_DB_WT_UOM", "DFLT_DSP_QTY_UOM", "DFLT_DSP_VOL_UOM", "DFLT_DSP_WT_UOM", "DFLT_INCUB_LOCK", "DFLT_MANIF_TYPE", "DFLT_PALLET_HT", "DFLT_PIKNG_LOCN_CODE", "DFLT_PLT_TYPE", "DFLT_PUTWY_TYPE", "DFLT_RECALL_CARTON_LOCK", 
"DFLT_SKU_SUB", "DFLT_STOP_SEQ_ORD", "DFLT_UNIT_VALUE", "DFLT_UNIT_VOL", "DFLT_UNIT_WT", "DFLT_UPC_PRE_DIGIT", "DFLT_UPC_VENDOR_CODE", "DUP_SRL_NBR_FLAG", "DWP_INTERFACE_ID", "EAN_ENABLED_FLAG", "EAN_MAX_LEN", "EAN_NBR_OF_SCANS", "EAN_PREFIX", "EAN_SEPARATOR", "ELS_INSTL", "ELS_TASK_ACTVTY_CODE", "ELS_TASK_INTERFACE", "EPC_MATCH_LOCK_CODE", "EPC_REQD_ON_ALL_CARTONS_FLAG", "EPC_REQD_ON_ALL_CASES_FLAG", "ERP_HOST_TYPE", "ERROR_RCPT_PCNT_PO_SKU", "ERROR_RCPT_PCNT_SHMT_PO_SKU", "EST_NBR_CARTONS", "FEDEX_SERVER_IP", "FEDEX_SERVER_PORT", "FIFO_MAINT_FLAG", "FTZ_TRK_FLAG", "GENRT_CARTON_ASN", "GEN_EAN_CNTR_NBR_FLAG", "GEN_ITEM_BRCD", "GET_EPC_W_WAVE_FLAG", "HAZMAT_CNTCT_LINE1", "HAZMAT_CNTCT_LINE2", "HNDL_ATTR_UOM_ACT", "HNDL_ATTR_UOM_CP", "HOT_TASK_PRTY", "I2O5_CASE_QTY_REF", "I2O5_PACK_QTY_REF", "I2O5_PLT_QTY_REF", "I2O5_TIER_QTY_REF", "INCUB_CTRL_FLAG", "INFOLINK_INSTL", "INVC_MODE", "INVN_UPD_ON_CANCEL_PKT", "INVN_UPD_ON_PAKNG_CHG", "INVN_UPD_ON_RESET_WAVE_PKT", "LAST_SLOT_DOWNLOAD_DATE", "LIMIT_USER_IN_TRVL_AISLE_FLAG", "LM_MONITOR_INSTL_FLAG", "LOAD_PLAN", "LOCN_CHK_DIGIT_FLAG", "LOG_FEDEX_TRAN_FLAG", "LOG_LANG_ID", "LOOK_UP_XREF", "LOST_IN_CYCLE_CNT_LOCK_CODE", "LOST_IN_WHSE_LOCK_CODE", "LOST_ON_PLT_LOCK_CODE", "LOST_ON_PUTWY_LOCK_CODE", "LTL_TL_RATE", "MANIF_INCMPL_ORD", "MULT_SKU_CASE_LOCK_CODE", "MXD_SKU_PUTWY_TYPE", "NBR_OF_DYN_ACTV_PICK_PER_SKU", "NBR_OF_DYN_CASE_PICK_PER_SKU", "NBR_OF_WAVE_BEFORE_LETUP", "NBR_OF_WAVE_BEFORE_REUSE", "OB_QA_LVL", "ORGN_ZIP", "OVRD_RCPT_PCNT_PO_SKU", "OVRD_RCPT_PCNT_SHMT_PO_SKU", "OVRPK_BOXES", "OVRPK_CTRL", "OVRPK_PARCL_CARTON", "OVRPK_PCNT", "PENDING_CYCLE_CNT_LOCK_CODE", "PHYS_INVN_CNT_ADJ_RSN_CODE", "PICK_LOCN_ASSIGN_SEQ_FLAG", "PKT_STAT_CHG_PIX", "PLT_TYPE", "PO_SKU_LVL_VERF_PIX", "PPICK_REPL_CTRL", "PRICE_TKT_TYPE", "PRT_CONTENT_LBL_ON_CTN_MODIFY", "PRT_PACK_SLIP_ON_CTN_MODIFY", "PRT_PKT_W_WAVE", "PURGE_APPT_STAT_CODE", "PURGE_ASN_STAT_CODE", "PURGE_CASE_BEYOND_STAT_CODE", "PUTWY_LOCN_LOCK_CODE", "QUAL_HELD_LOCK_CODE", 
"REPL_ACTV_W_WAVE", "REPL_CASE_PICK_W_WAVE", "REPL_TASK_UPDT_PRTY", "REPORT_TRANS", "RLS_HELD_RPLN_TASKS", "RTE_WITH_WAVE", "SAVE_RTE_WAVE_UNDO_INFO", "SKU_CNSTR", "SKU_LEVEL", "SLOTINFO_CRIT_NBR", "SLOTINFO_DOWNLOAD_FILE_PATH", "SLOTINFO_ONE_USER_PER_GRP", "SLOTINFO_TEMP_LOCN_ID", "SLOTINFO_UPLOAD_FILE_PATH", "SLOT_IT_USED", "SMARTINFO_INSTL", "SORT_ID_CODES", "SRL_NBR_LIST_MAX", "SRL_PIX_FLAG", "SRL_TRK_FLAG", "SUPPRESS_CTN_LBL", "SYS_TIME_ZONE", "TASK_EXCEPTION_FLAG", "TASK_PRIORITIZATION", "TI_UPD_ON_CANCEL_PKT", "TI_UPD_ON_PAKNG_CHG", "TI_UPD_ON_RESET_WAVE_PKT", "TRACK_OB_RETN_LEVEL", "TRIG_INVC_PKT_STS", "TRK_ACTIVE_MULT_PACK_QTY", "TRK_ACTV_BY_BATCH_NBR", "TRK_ACTV_BY_CNTRY_OF_ORGN", "TRK_ACTV_BY_ID_CODES", "TRK_ACTV_BY_PROD_STAT", "TRK_CASE_PICK_BY_BATCH_NBR", "TRK_CASE_PICK_BY_CNTRY_OF_ORGN", "TRK_CASE_PICK_BY_ID_CODE", "TRK_CASE_PICK_BY_PROD_STAT", "TRK_MULT_PACK_QTY", "TRK_PROD_FLAG", "UPD_ALLOC_ON_PICK_WAVE", "USE_APPT_DURTN_FOR_DOCK_DOOR", "USE_INBD_LPN_AS_OUTBD_LPN", "USE_LOCK_CODE_PRTY_PIX", "USE_LOCN_ID_AS_BRCD", "VENDOR_PERFRM_PIX", "VERF_CARTON_CONTNT", "VICS_BOL_FLAG", "VOCOLLECT_PACK_CNTR_TRACK_FLAG", "VOCOLLECT_PACK_LUT_FLAG", "VOCOLLECT_PACK_WORK_ID_DESC", "VOCOLLECT_PTS_LUT_FLAG", "VOCOLLECT_PTS_WORK_ID_DESC", "WARN_RCPT_PCNT_PO_SKU", "WARN_RCPT_PCNT_SHMT_PO_SKU", "WAVE_SUSPND_OPTN", "WHEN_TO_PRT_CTN_CONTENT_LBL", "WHEN_TO_PRT_CTN_SHIP_LBL", "WHEN_TO_PRT_PAKNG_SLIP", "WHEN_TO_PRT_PLT_LBL", "WHSE_TIME_ZONE", "WORKINFO_LOG", "WORKINFO_SERVER_FACTORY_NAME", "WORKINFO_TRAVEL_ACTVTY", "XCESS_WAVE_NEED_PROC_TYPE", "XFER_MAX_UOM", "YARD_JOCK_TASK", "ZONE_PAYMENT_STAT", "ZONE_SKIP", "ALLOW_MIXED_CD_IN_RESV_FLAG", "CLS_TIMEZONE_ID", "UPS_ADF_ACTV", "UPS_ADF_FILE_UPLOAD_PATH", "UPS_EMT_URL", "UPS_EMT_REMOTE_PATH", "UPS_EMT_USER", "UPS_EMT_PSWD", "UPS_EMT_FILE_UPLOAD_PATH", "CREATE_DATE_TIME", "MOD_DATE_TIME", "USER_ID", "ACTV_LOCN_THRESHOLD_PCNT", "CASE_PICK_LOCN_THRESHOLD_PCNT", "AUTO_GEN_CC_TASK", "QUAL_AUD_FAILED_LOCK_CODE", "MAX_CC_RECNT", 
"DELV_CONFIRMATION_ENABLED", "DISTRO_SEQ_METHOD", "FLOWTHRU_ALLOC_METHOD") AS
select cast (whse as varchar2 (3)) as whse,
      cast (whse_name as varchar2 (30)) as whse_name,
      cast (whse_addr_id as number (9)) as whse_addr_id,
      cast (actv_repl_orgn as varchar2 (1)) as actv_repl_orgn,
      cast (adj_alg_flag as varchar2 (1)) as adj_alg_flag,
      cast (aes_file_upload_path as varchar2 (250))
         as aes_file_upload_path,
      cast (aes_pswd as varchar2 (6)) as aes_pswd,
      cast (allow_close_load_held_batch as varchar2 (1))
         as allow_close_load_held_batch,
      cast (allow_load_held_batch as varchar2 (1))
         as allow_load_held_batch,
      cast (allow_manif_held_batch as varchar2 (1))
         as allow_manif_held_batch,
      cast (allow_pakng_w_out_case_nbr as varchar2 (1))
         as allow_pakng_w_out_case_nbr,
      cast (allow_rcv_w_out_case_asn as varchar2 (1))
         as allow_rcv_w_out_case_asn,
      cast (allow_rcv_w_out_case_nbr as varchar2 (1))
         as allow_rcv_w_out_case_nbr,
      cast (allow_rcv_w_out_po_nbr as varchar2 (1))
         as allow_rcv_w_out_po_nbr,
      cast (allow_rcv_w_out_shpmt as varchar2 (1))
         as allow_rcv_w_out_shpmt,
      cast (allow_repl_task_updt_prty as varchar2 (1))
         as allow_repl_task_updt_prty,
      cast (allow_rls_qual_held_on_rcpt as varchar2 (1))
         as allow_rls_qual_held_on_rcpt,
      cast (allow_sku_not_on_po as varchar2 (1)) as allow_sku_not_on_po,
      cast (allow_sku_not_on_shpmt as varchar2 (1))
         as allow_sku_not_on_shpmt,
      cast (allow_srl_create as number (1)) as allow_srl_create,
      cast (allow_weigh_held_batch as varchar2 (1))
         as allow_weigh_held_batch,
      cast (allow_work_ord_qty_chg as varchar2 (1))
         as allow_work_ord_qty_chg,
      cast (allow_work_ord_sku_addn as varchar2 (1))
         as allow_work_ord_sku_addn,
      cast (asn_case_lvl_verf_pix as varchar2 (1))
         as asn_case_lvl_verf_pix,
      cast (asn_sku_lvl_verf_pix as varchar2 (1)) as asn_sku_lvl_verf_pix,
      cast (assign_and_prt_bol as varchar2 (1)) as assign_and_prt_bol,
      cast (assign_and_prt_manif as varchar2 (1)) as assign_and_prt_manif,
      cast (assign_epc_in_active_flag as varchar2 (1))
         as assign_epc_in_active_flag,
      cast (assign_pro_nbr as varchar2 (1)) as assign_pro_nbr,
      cast (auto_manif_at_pnh as varchar2 (1)) as auto_manif_at_pnh,
      cast (base_incub_flag as varchar2 (2)) as base_incub_flag,
      cast (batch_nbr_eq_vendor_batch_nbr as varchar2 (1))
         as batch_nbr_eq_vendor_batch_nbr,
      cast (calc_case_size_type as varchar2 (1)) as calc_case_size_type,
      cast (calc_repl_ctrl as varchar2 (1)) as calc_repl_ctrl,
      cast (calc_to_be_locd as varchar2 (1)) as calc_to_be_locd,
      cast (carton_break_on_aisle_chg as varchar2 (1))
         as carton_break_on_aisle_chg,
      cast (carton_break_on_area_chg as varchar2 (1))
         as carton_break_on_area_chg,
      cast (carton_break_on_zone_chg as varchar2 (1))
         as carton_break_on_zone_chg,
      cast (carton_type as varchar2 (3)) as carton_type,
      cast (carton_wt_tol as number (5, 2)) as carton_wt_tol,
      cast (case_alloc_strk as number (3)) as case_alloc_strk,
      cast (case_nbr_dsp_usage as varchar2 (1)) as case_nbr_dsp_usage,
      cast (case_wght_tol as number (5, 2)) as case_wght_tol,
      cast (cc_toler_uom as varchar2 (1)) as cc_toler_uom,
      cast (cc_var_toler_value as number (10)) as cc_var_toler_value,
      cast (chkin_window as varchar2 (5)) as chkin_window,
      cast (cmbin_repl_prty as varchar2 (1)) as cmbin_repl_prty,
      cast (cnty as varchar2 (40)) as cnty,
      cast (co_id as number (8)) as co_id,
      cast (co_id_name as varchar2 (30)) as co_id_name,
      cast (cpcty_est_flag as number (1)) as cpcty_est_flag,
      cast (create_asn_flag as varchar2 (1)) as create_asn_flag,
      cast (create_load as varchar2 (1)) as create_load,
      cast (create_major_pkt as varchar2 (1)) as create_major_pkt,
      cast (create_shpmt as varchar2 (1)) as create_shpmt,
      cast (cubiscan_ignore_color as number (1)) as cubiscan_ignore_color,
      cast (cubiscan_ignore_color_sfx as number (1))
         as cubiscan_ignore_color_sfx,
      cast (cubiscan_ignore_qual as number (1)) as cubiscan_ignore_qual,
      cast (cubiscan_ignore_sec_dim as number (1))
         as cubiscan_ignore_sec_dim,
      cast (cubiscan_ignore_size_desc as number (1))
         as cubiscan_ignore_size_desc,
      cast (cubiscan_ignore_style_sfx as number (1))
         as cubiscan_ignore_style_sfx,
      cast (cubiscan_overwrite as number (1)) as cubiscan_overwrite,
      cast (date_mask as varchar2 (11)) as date_mask,
      cast (default_price_tix_lock_code as varchar2 (2))
         as default_price_tix_lock_code,
      cast (dflt_alloc_cutoff as number (3)) as dflt_alloc_cutoff,
      cast (dflt_alloc_type as varchar2 (3)) as dflt_alloc_type,
      cast (dflt_bol_type as varchar2 (1)) as dflt_bol_type,
      cast (dflt_capcty_uom as varchar2 (1)) as dflt_capcty_uom,
      cast (dflt_case_size_type as varchar2 (3)) as dflt_case_size_type,
      cast (dflt_cons_date as date) as dflt_cons_date,
      cast (dflt_cons_prty_mxd as varchar2 (3)) as dflt_cons_prty_mxd,
      cast (dflt_cons_prty_partl as varchar2 (3)) as dflt_cons_prty_partl,
      cast (dflt_cons_prty_singl_sku_full as varchar2 (3))
         as dflt_cons_prty_singl_sku_full,
      cast (dflt_cons_seq_mxd as varchar2 (3)) as dflt_cons_seq_mxd,
      cast (dflt_cons_seq_partl as varchar2 (3)) as dflt_cons_seq_partl,
      cast (dflt_cons_seq_singl_sku_full as varchar2 (3))
         as dflt_cons_seq_singl_sku_full,
      cast (dflt_db_qty_uom as varchar2 (2)) as dflt_db_qty_uom,
      cast (dflt_db_vol_uom as varchar2 (2)) as dflt_db_vol_uom,
      cast (dflt_db_wt_uom as varchar2 (2)) as dflt_db_wt_uom,
      cast (dflt_dsp_qty_uom as varchar2 (2)) as dflt_dsp_qty_uom,
      cast (dflt_dsp_vol_uom as varchar2 (2)) as dflt_dsp_vol_uom,
      cast (dflt_dsp_wt_uom as varchar2 (2)) as dflt_dsp_wt_uom,
      cast (dflt_incub_lock as varchar2 (2)) as dflt_incub_lock,
      cast (dflt_manif_type as varchar2 (4)) as dflt_manif_type,
      cast (dflt_pallet_ht as number (7, 2)) as dflt_pallet_ht,
      cast (dflt_pikng_locn_code as varchar2 (2)) as dflt_pikng_locn_code,
      cast (dflt_plt_type as varchar2 (3)) as dflt_plt_type,
      cast (dflt_putwy_type as varchar2 (3)) as dflt_putwy_type,
      cast (dflt_recall_carton_lock as varchar2 (2))
         as dflt_recall_carton_lock,
      cast (dflt_sku_sub as varchar2 (1)) as dflt_sku_sub,
      cast (dflt_stop_seq_ord as varchar2 (1)) as dflt_stop_seq_ord,
      cast (dflt_unit_value as number (9, 2)) as dflt_unit_value,
      cast (dflt_unit_vol as number (13, 4)) as dflt_unit_vol,
      cast (dflt_unit_wt as number (13, 4)) as dflt_unit_wt,
      cast (dflt_upc_pre_digit as varchar2 (1)) as dflt_upc_pre_digit,
      cast (dflt_upc_vendor_code as varchar2 (5)) as dflt_upc_vendor_code,
      cast (dup_srl_nbr_flag as number (1)) as dup_srl_nbr_flag,
      cast (dwp_interface_id as number (10)) as dwp_interface_id,
      cast (ean_enabled_flag as number (1)) as ean_enabled_flag,
      cast (ean_max_len as number (2)) as ean_max_len,
      cast (ean_nbr_of_scans as number (1)) as ean_nbr_of_scans,
      cast (ean_prefix as varchar2 (3)) as ean_prefix,
      cast (ean_separator as varchar2 (3)) as ean_separator,
      cast (els_instl as varchar2 (1)) as els_instl,
      cast (els_task_actvty_code as varchar2 (15))
         as els_task_actvty_code,
      cast (els_task_interface as varchar2 (1)) as els_task_interface,
      cast (epc_match_lock_code as varchar2 (2)) as epc_match_lock_code,
      cast (epc_reqd_on_all_cartons_flag as varchar2 (1))
         as epc_reqd_on_all_cartons_flag,
      cast (epc_reqd_on_all_cases_flag as varchar2 (1))
         as epc_reqd_on_all_cases_flag,
      cast (erp_host_type as varchar2 (1)) as erp_host_type,
      cast (error_rcpt_pcnt_po_sku as number (5, 2))
         as error_rcpt_pcnt_po_sku,
      cast (error_rcpt_pcnt_shmt_po_sku as number (5, 2))
         as error_rcpt_pcnt_shmt_po_sku,
      cast (est_nbr_cartons as number (1)) as est_nbr_cartons,
      cast (fedex_server_ip as varchar2 (15)) as fedex_server_ip,
      cast (fedex_server_port as varchar2 (10)) as fedex_server_port,
      cast (fifo_maint_flag as number (1)) as fifo_maint_flag,
      cast (ftz_trk_flag as varchar2 (1)) as ftz_trk_flag,
      cast (genrt_carton_asn as varchar2 (1)) as genrt_carton_asn,
      cast (gen_ean_cntr_nbr_flag as varchar2 (1))
         as gen_ean_cntr_nbr_flag,
      cast (gen_item_brcd as varchar2 (20)) as gen_item_brcd,
      cast (get_epc_w_wave_flag as varchar2 (1)) as get_epc_w_wave_flag,
      cast (hazmat_cntct_line1 as varchar2 (100)) as hazmat_cntct_line1,
      cast (hazmat_cntct_line2 as varchar2 (100)) as hazmat_cntct_line2,
      cast (hndl_attr_uom_act as varchar2 (2)) as hndl_attr_uom_act,
      cast (hndl_attr_uom_cp as varchar2 (2)) as hndl_attr_uom_cp,
      cast (hot_task_prty as number (5)) as hot_task_prty,
      cast (i2o5_case_qty_ref as varchar2 (1)) as i2o5_case_qty_ref,
      cast (i2o5_pack_qty_ref as varchar2 (1)) as i2o5_pack_qty_ref,
      cast (i2o5_plt_qty_ref as varchar2 (1)) as i2o5_plt_qty_ref,
      cast (i2o5_tier_qty_ref as varchar2 (1)) as i2o5_tier_qty_ref,
      cast (incub_ctrl_flag as varchar2 (1)) as incub_ctrl_flag,
      cast (infolink_instl as varchar2 (15)) as infolink_instl,
      cast (invc_mode as number (1)) as invc_mode,
      cast (invn_upd_on_cancel_pkt as varchar2 (2))
         as invn_upd_on_cancel_pkt,
      cast (invn_upd_on_pakng_chg as varchar2 (2))
         as invn_upd_on_pakng_chg,
      cast (invn_upd_on_reset_wave_pkt as varchar2 (2))
         as invn_upd_on_reset_wave_pkt,
      cast (last_slot_download_date as date) as last_slot_download_date,
      cast (limit_user_in_trvl_aisle_flag as varchar2 (1))
         as limit_user_in_trvl_aisle_flag,
      cast (lm_monitor_instl_flag as varchar2 (1))
         as lm_monitor_instl_flag,
      cast (load_plan as varchar2 (1)) as load_plan,
      cast (locn_chk_digit_flag as number (1)) as locn_chk_digit_flag,
      cast (log_fedex_tran_flag as varchar2 (1)) as log_fedex_tran_flag,
      cast (log_lang_id as varchar2 (3)) as log_lang_id,
      cast (look_up_xref as varchar2 (1)) as look_up_xref,
      cast (lost_in_cycle_cnt_lock_code as varchar2 (2))
         as lost_in_cycle_cnt_lock_code,
      cast (lost_in_whse_lock_code as varchar2 (2))
         as lost_in_whse_lock_code,
      cast (lost_on_plt_lock_code as varchar2 (2))
         as lost_on_plt_lock_code,
      cast (lost_on_putwy_lock_code as varchar2 (2))
         as lost_on_putwy_lock_code,
      cast (ltl_tl_rate as varchar2 (1)) as ltl_tl_rate,
      cast (manif_incmpl_ord as varchar2 (1)) as manif_incmpl_ord,
      cast (mult_sku_case_lock_code as varchar2 (2))
         as mult_sku_case_lock_code,
      cast (mxd_sku_putwy_type as varchar2 (3)) as mxd_sku_putwy_type,
      cast (nbr_of_dyn_actv_pick_per_sku as number (3))
         as nbr_of_dyn_actv_pick_per_sku,
      cast (nbr_of_dyn_case_pick_per_sku as number (3))
         as nbr_of_dyn_case_pick_per_sku,
      cast (nbr_of_wave_before_letup as number (3))
         as nbr_of_wave_before_letup,
      cast (nbr_of_wave_before_reuse as number (3))
         as nbr_of_wave_before_reuse,
      cast (ob_qa_lvl as number (1)) as ob_qa_lvl,
      cast (orgn_zip as varchar2 (11)) as orgn_zip,
      cast (ovrd_rcpt_pcnt_po_sku as number (5, 2))
         as ovrd_rcpt_pcnt_po_sku,
      cast (ovrd_rcpt_pcnt_shmt_po_sku as number (5, 2))
         as ovrd_rcpt_pcnt_shmt_po_sku,
      cast (ovrpk_boxes as number (3)) as ovrpk_boxes,
      cast (ovrpk_ctrl as varchar2 (1)) as ovrpk_ctrl,
      cast (ovrpk_parcl_carton as varchar2 (1)) as ovrpk_parcl_carton,
      cast (ovrpk_pcnt as number (5, 2)) as ovrpk_pcnt,
      cast (pending_cycle_cnt_lock_code as varchar2 (2))
         as pending_cycle_cnt_lock_code,
      cast (phys_invn_cnt_adj_rsn_code as varchar2 (2))
         as phys_invn_cnt_adj_rsn_code,
      cast (pick_locn_assign_seq_flag as varchar2 (1))
         as pick_locn_assign_seq_flag,
      cast (pkt_stat_chg_pix as varchar2 (1)) as pkt_stat_chg_pix,
      cast (plt_type as varchar2 (3)) as plt_type,
      cast (po_sku_lvl_verf_pix as varchar2 (1)) as po_sku_lvl_verf_pix,
      cast (ppick_repl_ctrl as varchar2 (1)) as ppick_repl_ctrl,
      cast (price_tkt_type as varchar2 (3)) as price_tkt_type,
      cast (prt_content_lbl_on_ctn_modify as varchar2 (1))
         as prt_content_lbl_on_ctn_modify,
      cast (prt_pack_slip_on_ctn_modify as varchar2 (1))
         as prt_pack_slip_on_ctn_modify,
      cast (prt_pkt_w_wave as varchar2 (1)) as prt_pkt_w_wave,
      cast (purge_appt_stat_code as number (2)) as purge_appt_stat_code,
      cast (purge_asn_stat_code as number (2)) as purge_asn_stat_code,
      cast (purge_case_beyond_stat_code as number (2))
         as purge_case_beyond_stat_code,
      cast (putwy_locn_lock_code as varchar2 (2)) as putwy_locn_lock_code,
      cast (qual_held_lock_code as varchar2 (2)) as qual_held_lock_code,
      cast (repl_actv_w_wave as varchar2 (1)) as repl_actv_w_wave,
      cast (repl_case_pick_w_wave as varchar2 (1))
         as repl_case_pick_w_wave,
      cast (repl_task_updt_prty as number (2)) as repl_task_updt_prty,
      cast (report_trans as varchar2 (1)) as report_trans,
      cast (rls_held_rpln_tasks as varchar2 (1)) as rls_held_rpln_tasks,
      cast (rte_with_wave as varchar2 (1)) as rte_with_wave,
      cast (save_rte_wave_undo_info as varchar2 (1))
         as save_rte_wave_undo_info,
      cast (sku_cnstr as varchar2 (1)) as sku_cnstr,
      cast (sku_level as number (1)) as sku_level,
      cast (slotinfo_crit_nbr as varchar2 (10)) as slotinfo_crit_nbr,
      cast (slotinfo_download_file_path as varchar2 (100))
         as slotinfo_download_file_path,
      cast (slotinfo_one_user_per_grp as varchar2 (1))
         as slotinfo_one_user_per_grp,
      cast (slotinfo_temp_locn_id as varchar2 (10))
         as slotinfo_temp_locn_id,
      cast (slotinfo_upload_file_path as varchar2 (100))
         as slotinfo_upload_file_path,
      cast (slot_it_used as varchar2 (1)) as slot_it_used,
      cast (smartinfo_instl as varchar2 (15)) as smartinfo_instl,
      cast (sort_id_codes as varchar2 (1)) as sort_id_codes,
      cast (srl_nbr_list_max as number (5)) as srl_nbr_list_max,
      cast (srl_pix_flag as number (1)) as srl_pix_flag,
      cast (srl_trk_flag as number (1)) as srl_trk_flag,
      cast (suppress_ctn_lbl as varchar2 (1)) as suppress_ctn_lbl,
      cast (sys_time_zone as varchar2 (3)) as sys_time_zone,
      cast (task_exception_flag as number (1)) as task_exception_flag,
      cast (task_prioritization as varchar2 (1)) as task_prioritization,
      cast (ti_upd_on_cancel_pkt as number (3)) as ti_upd_on_cancel_pkt,
      cast (ti_upd_on_pakng_chg as number (3)) as ti_upd_on_pakng_chg,
      cast (ti_upd_on_reset_wave_pkt as number (3))
         as ti_upd_on_reset_wave_pkt,
      cast (track_ob_retn_level as varchar2 (1)) as track_ob_retn_level,
      cast (trig_invc_pkt_sts as number (2)) as trig_invc_pkt_sts,
      cast (trk_active_mult_pack_qty as varchar2 (1))
         as trk_active_mult_pack_qty,
      cast (trk_actv_by_batch_nbr as varchar2 (1))
         as trk_actv_by_batch_nbr,
      cast (trk_actv_by_cntry_of_orgn as varchar2 (1))
         as trk_actv_by_cntry_of_orgn,
      cast (trk_actv_by_id_codes as varchar2 (1)) as trk_actv_by_id_codes,
      cast (trk_actv_by_prod_stat as varchar2 (1))
         as trk_actv_by_prod_stat,
      cast (trk_case_pick_by_batch_nbr as varchar2 (1))
         as trk_case_pick_by_batch_nbr,
      cast (trk_case_pick_by_cntry_of_orgn as varchar2 (1))
         as trk_case_pick_by_cntry_of_orgn,
      cast (trk_case_pick_by_id_code as varchar2 (1))
         as trk_case_pick_by_id_code,
      cast (trk_case_pick_by_prod_stat as varchar2 (1))
         as trk_case_pick_by_prod_stat,
      cast (trk_mult_pack_qty as varchar2 (1)) as trk_mult_pack_qty,
      cast (trk_prod_flag as varchar2 (1)) as trk_prod_flag,
      cast (upd_alloc_on_pick_wave as varchar2 (1))
         as upd_alloc_on_pick_wave,
      cast (use_appt_durtn_for_dock_door as varchar2 (1))
         as use_appt_durtn_for_dock_door,
      cast (use_inbd_lpn_as_outbd_lpn as varchar2 (1))
         as use_inbd_lpn_as_outbd_lpn,
      cast (use_lock_code_prty_pix as varchar2 (1))
         as use_lock_code_prty_pix,
      cast (use_locn_id_as_brcd as number (1)) as use_locn_id_as_brcd,
      cast (vendor_perfrm_pix as varchar2 (1)) as vendor_perfrm_pix,
      cast (verf_carton_contnt as varchar2 (1)) as verf_carton_contnt,
      cast (vics_bol_flag as varchar2 (1)) as vics_bol_flag,
      cast (vocollect_pack_cntr_track_flag as varchar2 (1))
         as vocollect_pack_cntr_track_flag,
      cast (vocollect_pack_lut_flag as varchar2 (1))
         as vocollect_pack_lut_flag,
      cast (vocollect_pack_work_id_desc as varchar2 (30))
         as vocollect_pack_work_id_desc,
      cast (vocollect_pts_lut_flag as varchar2 (1))
         as vocollect_pts_lut_flag,
      cast (vocollect_pts_work_id_desc as varchar2 (30))
         as vocollect_pts_work_id_desc,
      cast (warn_rcpt_pcnt_po_sku as number (5, 2))
         as warn_rcpt_pcnt_po_sku,
      cast (warn_rcpt_pcnt_shmt_po_sku as number (5, 2))
         as warn_rcpt_pcnt_shmt_po_sku,
      cast (wave_suspnd_optn as varchar2 (1)) as wave_suspnd_optn,
      cast (when_to_prt_ctn_content_lbl as varchar2 (1))
         as when_to_prt_ctn_content_lbl,
      cast (when_to_prt_ctn_ship_lbl as varchar2 (1))
         as when_to_prt_ctn_ship_lbl,
      cast (when_to_prt_pakng_slip as varchar2 (1))
         as when_to_prt_pakng_slip,
      cast (when_to_prt_plt_lbl as number (1)) as when_to_prt_plt_lbl,
      cast (whse_time_zone as varchar2 (3)) as whse_time_zone,
      cast (workinfo_log as varchar2 (1)) as workinfo_log,
      cast (workinfo_server_factory_name as varchar2 (100))
         as workinfo_server_factory_name,
      cast (workinfo_travel_actvty as varchar2 (15))
         as workinfo_travel_actvty,
      cast (xcess_wave_need_proc_type as number (1))
         as xcess_wave_need_proc_type,
      cast (xfer_max_uom as varchar2 (1)) as xfer_max_uom,
      cast (yard_jock_task as varchar2 (1)) as yard_jock_task,
      cast (zone_payment_stat as varchar2 (2)) as zone_payment_stat,
      cast (zone_skip as varchar2 (1)) as zone_skip,
      cast (allow_mixed_cd_in_resv_flag as char (1))
         as allow_mixed_cd_in_resv_flag,
      cast (cls_timezone_id as number (9)) as cls_timezone_id,
      cast (ups_adf_actv as char (1)) as ups_adf_actv,
      cast (ups_adf_file_upload_path as varchar (50))
         as ups_adf_file_upload_path,
      cast (ups_emt_url as varchar (50)) as ups_emt_url,
      cast (ups_emt_remote_path as varchar (50)) as ups_emt_remote_path,
      cast (ups_emt_user as varchar (40)) as ups_emt_user,
      cast (ups_emt_pswd as varchar (40)) as ups_emt_pswd,
      cast (ups_emt_file_upload_path as varchar (50))
         as ups_emt_file_upload_path,
      cast (create_date_time as date) as create_date_time,
      cast (mod_date_time as date) as mod_date_time,
      cast (user_id as varchar2 (15)) as user_id,
      cast (actv_locn_threshold_pcnt as number (3))
         as actv_locn_threshold_pcnt,
      cast (case_pick_locn_threshold_pcnt as number (3))
         as case_pick_locn_threshold_pcnt,
      cast (auto_gen_cc_task as varchar2 (1)) as auto_gen_cc_task,
      cast (qual_aud_failed_lock_code as varchar2 (2))
         as qual_aud_failed_lock_code,
      cast (max_cc_recnt as number (2)) as max_cc_recnt,
      cast (delv_confirmation_enabled as varchar2 (1))
         as delv_confirmation_enabled,
      cast (distro_seq_method as varchar2 (1)) as distro_seq_method,
      cast (flowthru_alloc_method as varchar2 (1))
         as flowthru_alloc_method
 from (  select wm.whse,
                wm.whse_name,
                wm.whse_addr_id,
                wm.cls_timezone_id,
                max (
                   case
                      when wmd.column_name = 'ACTV_REPL_ORGN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   actv_repl_orgn,
                max (
                   case
                      when wmd.column_name = 'ADJ_ALG_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   adj_alg_flag,
                max (
                   case
                      when wmd.column_name = 'AES_FILE_UPLOAD_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   aes_file_upload_path,
                max (
                   case
                      when wmd.column_name = 'AES_PSWD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   aes_pswd,
                max (
                   case
                      when wmd.column_name = 'ALLOW_CLOSE_LOAD_HELD_BATCH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_close_load_held_batch,
                max (
                   case
                      when wmd.column_name = 'ALLOW_LOAD_HELD_BATCH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_load_held_batch,
                max (
                   case
                      when wmd.column_name = 'ALLOW_MANIF_HELD_BATCH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_manif_held_batch,
                max (
                   case
                      when wmd.column_name = 'ALLOW_PAKNG_W_OUT_CASE_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_pakng_w_out_case_nbr,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RCV_W_OUT_CASE_ASN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rcv_w_out_case_asn,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RCV_W_OUT_CASE_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rcv_w_out_case_nbr,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RCV_W_OUT_PO_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rcv_w_out_po_nbr,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RCV_W_OUT_SHPMT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rcv_w_out_shpmt,
                max (
                   case
                      when wmd.column_name = 'ALLOW_REPL_TASK_UPDT_PRTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_repl_task_updt_prty,
                max (
                   case
                      when wmd.column_name = 'ALLOW_RLS_QUAL_HELD_ON_RCPT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_rls_qual_held_on_rcpt,
                max (
                   case
                      when wmd.column_name = 'ALLOW_SKU_NOT_ON_PO'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_sku_not_on_po,
                max (
                   case
                      when wmd.column_name = 'ALLOW_SKU_NOT_ON_SHPMT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_sku_not_on_shpmt,
                max (
                   case
                      when wmd.column_name = 'ALLOW_SRL_CREATE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_srl_create,
                max (
                   case
                      when wmd.column_name = 'ALLOW_WEIGH_HELD_BATCH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_weigh_held_batch,
                max (
                   case
                      when wmd.column_name = 'ALLOW_WORK_ORD_QTY_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_work_ord_qty_chg,
                max (
                   case
                      when wmd.column_name = 'ALLOW_WORK_ORD_SKU_ADDN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_work_ord_sku_addn,
                max (
                   case
                      when wmd.column_name = 'ASN_CASE_LVL_VERF_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   asn_case_lvl_verf_pix,
                max (
                   case
                      when wmd.column_name = 'ASN_SKU_LVL_VERF_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   asn_sku_lvl_verf_pix,
                max (
                   case
                      when wmd.column_name = 'ASSIGN_AND_PRT_BOL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   assign_and_prt_bol,
                max (
                   case
                      when wmd.column_name = 'ASSIGN_AND_PRT_MANIF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   assign_and_prt_manif,
                max (
                   case
                      when wmd.column_name = 'ASSIGN_EPC_IN_ACTIVE_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   assign_epc_in_active_flag,
                max (
                   case
                      when wmd.column_name = 'ASSIGN_PRO_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   assign_pro_nbr,
                max (
                   case
                      when wmd.column_name = 'AUTO_MANIF_AT_PNH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   auto_manif_at_pnh,
                max (
                   case
                      when wmd.column_name = 'BASE_INCUB_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   base_incub_flag,
                max (
                   case
                      when wmd.column_name =
                              'BATCH_NBR_EQ_VENDOR_BATCH_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   batch_nbr_eq_vendor_batch_nbr,
                max (
                   case
                      when wmd.column_name = 'CALC_CASE_SIZE_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   calc_case_size_type,
                max (
                   case
                      when wmd.column_name = 'CALC_REPL_CTRL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   calc_repl_ctrl,
                max (
                   case
                      when wmd.column_name = 'CALC_TO_BE_LOCD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   calc_to_be_locd,
                max (
                   case
                      when wmd.column_name = 'CARTON_BREAK_ON_AISLE_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_break_on_aisle_chg,
                max (
                   case
                      when wmd.column_name = 'CARTON_BREAK_ON_AREA_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_break_on_area_chg,
                max (
                   case
                      when wmd.column_name = 'CARTON_BREAK_ON_ZONE_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_break_on_zone_chg,
                max (
                   case
                      when wmd.column_name = 'CARTON_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_type,
                max (
                   case
                      when wmd.column_name = 'CARTON_WT_TOL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   carton_wt_tol,
                max (
                   case
                      when wmd.column_name = 'CASE_ALLOC_STRK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   case_alloc_strk,
                max (
                   case
                      when wmd.column_name = 'CASE_NBR_DSP_USAGE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   case_nbr_dsp_usage,
                max (
                   case
                      when wmd.column_name = 'CASE_WGHT_TOL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   case_wght_tol,
                max (
                   case
                      when wmd.column_name = 'CC_TOLER_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cc_toler_uom,
                max (
                   case
                      when wmd.column_name = 'CC_VAR_TOLER_VALUE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cc_var_toler_value,
                max (
                   case
                      when wmd.column_name = 'CHKIN_WINDOW'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   chkin_window,
                max (
                   case
                      when wmd.column_name = 'CMBIN_REPL_PRTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cmbin_repl_prty,
                max (
                   case
                      when wmd.column_name = 'CNTY' then wmd.column_value
                      else null
                   end)
                   cnty,
                max (
                   case
                      when wmd.column_name = 'CO_ID' then wmd.column_value
                      else null
                   end)
                   co_id,
                max (
                   case
                      when wmd.column_name = 'CO_ID_NAME'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   co_id_name,
                max (
                   case
                      when wmd.column_name = 'CPCTY_EST_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cpcty_est_flag,
                max (
                   case
                      when wmd.column_name = 'CREATE_ASN_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   create_asn_flag,
                max (
                   case
                      when wmd.column_name = 'CREATE_LOAD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   create_load,
                max (
                   case
                      when wmd.column_name = 'CREATE_MAJOR_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   create_major_pkt,
                max (
                   case
                      when wmd.column_name = 'CREATE_SHPMT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   create_shpmt,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_COLOR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_color,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_COLOR_SFX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_color_sfx,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_QUAL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_qual,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_SEC_DIM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_sec_dim,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_SIZE_DESC'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_size_desc,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_IGNORE_STYLE_SFX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_ignore_style_sfx,
                max (
                   case
                      when wmd.column_name = 'CUBISCAN_OVERWRITE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   cubiscan_overwrite,
                max (
                   case
                      when wmd.column_name = 'DATE_MASK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   date_mask,
                max (
                   case
                      when wmd.column_name = 'DEFAULT_PRICE_TIX_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   default_price_tix_lock_code,
                max (
                   case
                      when wmd.column_name = 'DFLT_ALLOC_CUTOFF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_alloc_cutoff,
                max (
                   case
                      when wmd.column_name = 'DFLT_ALLOC_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_alloc_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_BOL_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_bol_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_CAPCTY_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_capcty_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_CASE_SIZE_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_case_size_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_DATE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_date,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_PRTY_MXD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_prty_mxd,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_PRTY_PARTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_prty_partl,
                max (
                   case
                      when wmd.column_name =
                              'DFLT_CONS_PRTY_SINGL_SKU_FULL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_prty_singl_sku_full,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_SEQ_MXD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_seq_mxd,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_SEQ_PARTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_seq_partl,
                max (
                   case
                      when wmd.column_name = 'DFLT_CONS_SEQ_SINGL_SKU_FULL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_cons_seq_singl_sku_full,
                max (
                   case
                      when wmd.column_name = 'DFLT_DB_QTY_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_db_qty_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DB_VOL_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_db_vol_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DB_WT_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_db_wt_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DSP_QTY_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_dsp_qty_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DSP_VOL_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_dsp_vol_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_DSP_WT_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_dsp_wt_uom,
                max (
                   case
                      when wmd.column_name = 'DFLT_INCUB_LOCK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_incub_lock,
                max (
                   case
                      when wmd.column_name = 'DFLT_MANIF_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_manif_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_PALLET_HT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_pallet_ht,
                max (
                   case
                      when wmd.column_name = 'DFLT_PIKNG_LOCN_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_pikng_locn_code,
                max (
                   case
                      when wmd.column_name = 'DFLT_PLT_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_plt_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_PUTWY_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_putwy_type,
                max (
                   case
                      when wmd.column_name = 'DFLT_RECALL_CARTON_LOCK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_recall_carton_lock,
                max (
                   case
                      when wmd.column_name = 'DFLT_SKU_SUB'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_sku_sub,
                max (
                   case
                      when wmd.column_name = 'DFLT_STOP_SEQ_ORD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_stop_seq_ord,
                max (
                   case
                      when wmd.column_name = 'DFLT_UNIT_VALUE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_unit_value,
                max (
                   case
                      when wmd.column_name = 'DFLT_UNIT_VOL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_unit_vol,
                max (
                   case
                      when wmd.column_name = 'DFLT_UNIT_WT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_unit_wt,
                max (
                   case
                      when wmd.column_name = 'DFLT_UPC_PRE_DIGIT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_upc_pre_digit,
                max (
                   case
                      when wmd.column_name = 'DFLT_UPC_VENDOR_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dflt_upc_vendor_code,
                max (
                   case
                      when wmd.column_name = 'DUP_SRL_NBR_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dup_srl_nbr_flag,
                max (
                   case
                      when wmd.column_name = 'DWP_INTERFACE_ID'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   dwp_interface_id,
                max (
                   case
                      when wmd.column_name = 'EAN_ENABLED_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_enabled_flag,
                max (
                   case
                      when wmd.column_name = 'EAN_MAX_LEN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_max_len,
                max (
                   case
                      when wmd.column_name = 'EAN_NBR_OF_SCANS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_nbr_of_scans,
                max (
                   case
                      when wmd.column_name = 'EAN_PREFIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_prefix,
                max (
                   case
                      when wmd.column_name = 'EAN_SEPARATOR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ean_separator,
                max (
                   case
                      when wmd.column_name = 'ELS_INSTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   els_instl,
                max (
                   case
                      when wmd.column_name = 'ELS_TASK_ACTVTY_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   els_task_actvty_code,
                max (
                   case
                      when wmd.column_name = 'ELS_TASK_INTERFACE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   els_task_interface,
                max (
                   case
                      when wmd.column_name = 'EPC_MATCH_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   epc_match_lock_code,
                max (
                   case
                      when wmd.column_name = 'EPC_REQD_ON_ALL_CARTONS_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   epc_reqd_on_all_cartons_flag,
                max (
                   case
                      when wmd.column_name = 'EPC_REQD_ON_ALL_CASES_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   epc_reqd_on_all_cases_flag,
                max (
                   case
                      when wmd.column_name = 'ERP_HOST_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   erp_host_type,
                max (
                   case
                      when wmd.column_name = 'ERROR_RCPT_PCNT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   error_rcpt_pcnt_po_sku,
                max (
                   case
                      when wmd.column_name = 'ERROR_RCPT_PCNT_SHMT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   error_rcpt_pcnt_shmt_po_sku,
                max (
                   case
                      when wmd.column_name = 'EST_NBR_CARTONS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   est_nbr_cartons,
                max (
                   case
                      when wmd.column_name = 'FEDEX_SERVER_IP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   fedex_server_ip,
                max (
                   case
                      when wmd.column_name = 'FEDEX_SERVER_PORT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   fedex_server_port,
                max (
                   case
                      when wmd.column_name = 'FIFO_MAINT_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   fifo_maint_flag,
                max (
                   case
                      when wmd.column_name = 'FTZ_TRK_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ftz_trk_flag,
                max (
                   case
                      when wmd.column_name = 'GENRT_CARTON_ASN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   genrt_carton_asn,
                max (
                   case
                      when wmd.column_name = 'GEN_EAN_CNTR_NBR_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   gen_ean_cntr_nbr_flag,
                max (
                   case
                      when wmd.column_name = 'GEN_ITEM_BRCD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   gen_item_brcd,
                max (
                   case
                      when wmd.column_name = 'GET_EPC_W_WAVE_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   get_epc_w_wave_flag,
                max (
                   case
                      when wmd.column_name = 'HAZMAT_CNTCT_LINE1'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hazmat_cntct_line1,
                max (
                   case
                      when wmd.column_name = 'HAZMAT_CNTCT_LINE2'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hazmat_cntct_line2,
                max (
                   case
                      when wmd.column_name = 'HNDL_ATTR_UOM_ACT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hndl_attr_uom_act,
                max (
                   case
                      when wmd.column_name = 'HNDL_ATTR_UOM_CP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hndl_attr_uom_cp,
                max (
                   case
                      when wmd.column_name = 'HOT_TASK_PRTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   hot_task_prty,
                max (
                   case
                      when wmd.column_name = 'I2O5_CASE_QTY_REF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   i2o5_case_qty_ref,
                max (
                   case
                      when wmd.column_name = 'I2O5_PACK_QTY_REF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   i2o5_pack_qty_ref,
                max (
                   case
                      when wmd.column_name = 'I2O5_PLT_QTY_REF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   i2o5_plt_qty_ref,
                max (
                   case
                      when wmd.column_name = 'I2O5_TIER_QTY_REF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   i2o5_tier_qty_ref,
                max (
                   case
                      when wmd.column_name = 'INCUB_CTRL_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   incub_ctrl_flag,
                max (
                   case
                      when wmd.column_name = 'INFOLINK_INSTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   infolink_instl,
                max (
                   case
                      when wmd.column_name = 'INVC_MODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   invc_mode,
                max (
                   case
                      when wmd.column_name = 'INVN_UPD_ON_CANCEL_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   invn_upd_on_cancel_pkt,
                max (
                   case
                      when wmd.column_name = 'INVN_UPD_ON_PAKNG_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   invn_upd_on_pakng_chg,
                max (
                   case
                      when wmd.column_name = 'INVN_UPD_ON_RESET_WAVE_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   invn_upd_on_reset_wave_pkt,
                max (
                   case
                      when wmd.column_name = 'LAST_SLOT_DOWNLOAD_DATE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   last_slot_download_date,
                max (
                   case
                      when wmd.column_name =
                              'LIMIT_USER_IN_TRVL_AISLE_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   limit_user_in_trvl_aisle_flag,
                max (
                   case
                      when wmd.column_name = 'LM_MONITOR_INSTL_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lm_monitor_instl_flag,
                max (
                   case
                      when wmd.column_name = 'LOAD_PLAN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   load_plan,
                max (
                   case
                      when wmd.column_name = 'LOCN_CHK_DIGIT_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   locn_chk_digit_flag,
                max (
                   case
                      when wmd.column_name = 'LOG_FEDEX_TRAN_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   log_fedex_tran_flag,
                max (
                   case
                      when wmd.column_name = 'LOG_LANG_ID'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   log_lang_id,
                max (
                   case
                      when wmd.column_name = 'LOOK_UP_XREF'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   look_up_xref,
                max (
                   case
                      when wmd.column_name = 'LOST_IN_CYCLE_CNT_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lost_in_cycle_cnt_lock_code,
                max (
                   case
                      when wmd.column_name = 'LOST_IN_WHSE_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lost_in_whse_lock_code,
                max (
                   case
                      when wmd.column_name = 'LOST_ON_PLT_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lost_on_plt_lock_code,
                max (
                   case
                      when wmd.column_name = 'LOST_ON_PUTWY_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   lost_on_putwy_lock_code,
                max (
                   case
                      when wmd.column_name = 'LTL_TL_RATE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ltl_tl_rate,
                max (
                   case
                      when wmd.column_name = 'MANIF_INCMPL_ORD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   manif_incmpl_ord,
                max (
                   case
                      when wmd.column_name = 'MULT_SKU_CASE_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   mult_sku_case_lock_code,
                max (
                   case
                      when wmd.column_name = 'MXD_SKU_PUTWY_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   mxd_sku_putwy_type,
                max (
                   case
                      when wmd.column_name = 'NBR_OF_DYN_ACTV_PICK_PER_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   nbr_of_dyn_actv_pick_per_sku,
                max (
                   case
                      when wmd.column_name = 'NBR_OF_DYN_CASE_PICK_PER_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   nbr_of_dyn_case_pick_per_sku,
                max (
                   case
                      when wmd.column_name = 'NBR_OF_WAVE_BEFORE_LETUP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   nbr_of_wave_before_letup,
                max (
                   case
                      when wmd.column_name = 'NBR_OF_WAVE_BEFORE_REUSE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   nbr_of_wave_before_reuse,
                max (
                   case
                      when wmd.column_name = 'OB_QA_LVL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ob_qa_lvl,
                max (
                   case
                      when wmd.column_name = 'ORGN_ZIP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   orgn_zip,
                max (
                   case
                      when wmd.column_name = 'OVRD_RCPT_PCNT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrd_rcpt_pcnt_po_sku,
                max (
                   case
                      when wmd.column_name = 'OVRD_RCPT_PCNT_SHMT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrd_rcpt_pcnt_shmt_po_sku,
                max (
                   case
                      when wmd.column_name = 'OVRPK_BOXES'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrpk_boxes,
                max (
                   case
                      when wmd.column_name = 'OVRPK_CTRL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrpk_ctrl,
                max (
                   case
                      when wmd.column_name = 'OVRPK_PARCL_CARTON'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrpk_parcl_carton,
                max (
                   case
                      when wmd.column_name = 'OVRPK_PCNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ovrpk_pcnt,
                max (
                   case
                      when wmd.column_name = 'PENDING_CYCLE_CNT_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   pending_cycle_cnt_lock_code,
                max (
                   case
                      when wmd.column_name = 'PHYS_INVN_CNT_ADJ_RSN_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   phys_invn_cnt_adj_rsn_code,
                max (
                   case
                      when wmd.column_name = 'PICK_LOCN_ASSIGN_SEQ_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   pick_locn_assign_seq_flag,
                max (
                   case
                      when wmd.column_name = 'PKT_STAT_CHG_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   pkt_stat_chg_pix,
                max (
                   case
                      when wmd.column_name = 'PLT_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   plt_type,
                max (
                   case
                      when wmd.column_name = 'PO_SKU_LVL_VERF_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   po_sku_lvl_verf_pix,
                max (
                   case
                      when wmd.column_name = 'PPICK_REPL_CTRL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ppick_repl_ctrl,
                max (
                   case
                      when wmd.column_name = 'PRICE_TKT_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   price_tkt_type,
                max (
                   case
                      when wmd.column_name =
                              'PRT_CONTENT_LBL_ON_CTN_MODIFY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   prt_content_lbl_on_ctn_modify,
                max (
                   case
                      when wmd.column_name = 'PRT_PACK_SLIP_ON_CTN_MODIFY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   prt_pack_slip_on_ctn_modify,
                max (
                   case
                      when wmd.column_name = 'PRT_PKT_W_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   prt_pkt_w_wave,
                max (
                   case
                      when wmd.column_name = 'PURGE_APPT_STAT_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   purge_appt_stat_code,
                max (
                   case
                      when wmd.column_name = 'PURGE_ASN_STAT_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   purge_asn_stat_code,
                max (
                   case
                      when wmd.column_name = 'PURGE_CASE_BEYOND_STAT_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   purge_case_beyond_stat_code,
                max (
                   case
                      when wmd.column_name = 'PUTWY_LOCN_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   putwy_locn_lock_code,
                max (
                   case
                      when wmd.column_name = 'QUAL_HELD_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   qual_held_lock_code,
                max (
                   case
                      when wmd.column_name = 'REPL_ACTV_W_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   repl_actv_w_wave,
                max (
                   case
                      when wmd.column_name = 'REPL_CASE_PICK_W_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   repl_case_pick_w_wave,
                max (
                   case
                      when wmd.column_name = 'REPL_TASK_UPDT_PRTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   repl_task_updt_prty,
                max (
                   case
                      when wmd.column_name = 'REPORT_TRANS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   report_trans,
                max (
                   case
                      when wmd.column_name = 'RLS_HELD_RPLN_TASKS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   rls_held_rpln_tasks,
                max (
                   case
                      when wmd.column_name = 'RTE_WITH_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   rte_with_wave,
                max (
                   case
                      when wmd.column_name = 'SAVE_RTE_WAVE_UNDO_INFO'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   save_rte_wave_undo_info,
                max (
                   case
                      when wmd.column_name = 'SKU_CNSTR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   sku_cnstr,
                max (
                   case
                      when wmd.column_name = 'SKU_LEVEL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   sku_level,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_CRIT_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_crit_nbr,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_DOWNLOAD_FILE_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_download_file_path,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_ONE_USER_PER_GRP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_one_user_per_grp,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_TEMP_LOCN_ID'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_temp_locn_id,
                max (
                   case
                      when wmd.column_name = 'SLOTINFO_UPLOAD_FILE_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slotinfo_upload_file_path,
                max (
                   case
                      when wmd.column_name = 'SLOT_IT_USED'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   slot_it_used,
                max (
                   case
                      when wmd.column_name = 'SMARTINFO_INSTL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   smartinfo_instl,
                max (
                   case
                      when wmd.column_name = 'SORT_ID_CODES'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   sort_id_codes,
                max (
                   case
                      when wmd.column_name = 'SRL_NBR_LIST_MAX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   srl_nbr_list_max,
                max (
                   case
                      when wmd.column_name = 'SRL_PIX_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   srl_pix_flag,
                max (
                   case
                      when wmd.column_name = 'SRL_TRK_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   srl_trk_flag,
                max (
                   case
                      when wmd.column_name = 'SUPPRESS_CTN_LBL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   suppress_ctn_lbl,
                max (
                   case
                      when wmd.column_name = 'SYS_TIME_ZONE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   sys_time_zone,
                max (
                   case
                      when wmd.column_name = 'TASK_EXCEPTION_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   task_exception_flag,
                max (
                   case
                      when wmd.column_name = 'TASK_PRIORITIZATION'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   task_prioritization,
                max (
                   case
                      when wmd.column_name = 'TI_UPD_ON_CANCEL_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ti_upd_on_cancel_pkt,
                max (
                   case
                      when wmd.column_name = 'TI_UPD_ON_PAKNG_CHG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ti_upd_on_pakng_chg,
                max (
                   case
                      when wmd.column_name = 'TI_UPD_ON_RESET_WAVE_PKT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ti_upd_on_reset_wave_pkt,
                max (
                   case
                      when wmd.column_name = 'TRACK_OB_RETN_LEVEL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   track_ob_retn_level,
                max (
                   case
                      when wmd.column_name = 'TRIG_INVC_PKT_STS'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trig_invc_pkt_sts,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTIVE_MULT_PACK_QTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_active_mult_pack_qty,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTV_BY_BATCH_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_actv_by_batch_nbr,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTV_BY_CNTRY_OF_ORGN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_actv_by_cntry_of_orgn,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTV_BY_ID_CODES'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_actv_by_id_codes,
                max (
                   case
                      when wmd.column_name = 'TRK_ACTV_BY_PROD_STAT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_actv_by_prod_stat,
                max (
                   case
                      when wmd.column_name = 'TRK_CASE_PICK_BY_BATCH_NBR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_case_pick_by_batch_nbr,
                max (
                   case
                      when wmd.column_name =
                              'TRK_CASE_PICK_BY_CNTRY_OF_ORGN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_case_pick_by_cntry_of_orgn,
                max (
                   case
                      when wmd.column_name = 'TRK_CASE_PICK_BY_ID_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_case_pick_by_id_code,
                max (
                   case
                      when wmd.column_name = 'TRK_CASE_PICK_BY_PROD_STAT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_case_pick_by_prod_stat,
                max (
                   case
                      when wmd.column_name = 'TRK_MULT_PACK_QTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_mult_pack_qty,
                max (
                   case
                      when wmd.column_name = 'TRK_PROD_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   trk_prod_flag,
                max (
                   case
                      when wmd.column_name = 'UPD_ALLOC_ON_PICK_WAVE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   upd_alloc_on_pick_wave,
                max (
                   case
                      when wmd.column_name = 'USE_APPT_DURTN_FOR_DOCK_DOOR'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   use_appt_durtn_for_dock_door,
                max (
                   case
                      when wmd.column_name = 'USE_INBD_LPN_AS_OUTBD_LPN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   use_inbd_lpn_as_outbd_lpn,
                max (
                   case
                      when wmd.column_name = 'USE_LOCK_CODE_PRTY_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   use_lock_code_prty_pix,
                max (
                   case
                      when wmd.column_name = 'USE_LOCN_ID_AS_BRCD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   use_locn_id_as_brcd,
                max (
                   case
                      when wmd.column_name = 'VENDOR_PERFRM_PIX'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vendor_perfrm_pix,
                max (
                   case
                      when wmd.column_name = 'VERF_CARTON_CONTNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   verf_carton_contnt,
                max (
                   case
                      when wmd.column_name = 'VICS_BOL_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vics_bol_flag,
                max (
                   case
                      when wmd.column_name =
                              'VOCOLLECT_PACK_CNTR_TRACK_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pack_cntr_track_flag,
                max (
                   case
                      when wmd.column_name = 'VOCOLLECT_PACK_LUT_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pack_lut_flag,
                max (
                   case
                      when wmd.column_name = 'VOCOLLECT_PACK_WORK_ID_DESC'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pack_work_id_desc,
                max (
                   case
                      when wmd.column_name = 'VOCOLLECT_PTS_LUT_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pts_lut_flag,
                max (
                   case
                      when wmd.column_name = 'VOCOLLECT_PTS_WORK_ID_DESC'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   vocollect_pts_work_id_desc,
                max (
                   case
                      when wmd.column_name = 'WARN_RCPT_PCNT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   warn_rcpt_pcnt_po_sku,
                max (
                   case
                      when wmd.column_name = 'WARN_RCPT_PCNT_SHMT_PO_SKU'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   warn_rcpt_pcnt_shmt_po_sku,
                max (
                   case
                      when wmd.column_name = 'WAVE_SUSPND_OPTN'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   wave_suspnd_optn,
                max (
                   case
                      when wmd.column_name = 'WHEN_TO_PRT_CTN_CONTENT_LBL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   when_to_prt_ctn_content_lbl,
                max (
                   case
                      when wmd.column_name = 'WHEN_TO_PRT_CTN_SHIP_LBL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   when_to_prt_ctn_ship_lbl,
                max (
                   case
                      when wmd.column_name = 'WHEN_TO_PRT_PAKNG_SLIP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   when_to_prt_pakng_slip,
                max (
                   case
                      when wmd.column_name = 'WHEN_TO_PRT_PLT_LBL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   when_to_prt_plt_lbl,
                max (
                   case
                      when wmd.column_name = 'WHSE_TIME_ZONE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   whse_time_zone,
                max (
                   case
                      when wmd.column_name = 'WORKINFO_LOG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   workinfo_log,
                max (
                   case
                      when wmd.column_name = 'WORKINFO_SERVER_FACTORY_NAME'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   workinfo_server_factory_name,
                max (
                   case
                      when wmd.column_name = 'WORKINFO_TRAVEL_ACTVTY'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   workinfo_travel_actvty,
                max (
                   case
                      when wmd.column_name = 'XCESS_WAVE_NEED_PROC_TYPE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   xcess_wave_need_proc_type,
                max (
                   case
                      when wmd.column_name = 'XFER_MAX_UOM'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   xfer_max_uom,
                max (
                   case
                      when wmd.column_name = 'YARD_JOCK_TASK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   yard_jock_task,
                max (
                   case
                      when wmd.column_name = 'ZONE_PAYMENT_STAT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   zone_payment_stat,
                max (
                   case
                      when wmd.column_name = 'ZONE_SKIP'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   zone_skip,
                max (
                   case
                      when wmd.column_name = 'ALLOW_MIXED_CD_IN_RESV_FLAG'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   allow_mixed_cd_in_resv_flag,
                max (
                   case
                      when wmd.column_name = 'UPS_ADF_ACTV'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_adf_actv,
                max (
                   case
                      when wmd.column_name = 'UPS_ADF_FILE_UPLOAD_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_adf_file_upload_path,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_URL'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_url,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_REMOTE_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_remote_path,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_USER'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_user,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_PSWD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_pswd,
                max (
                   case
                      when wmd.column_name = 'UPS_EMT_FILE_UPLOAD_PATH'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   ups_emt_file_upload_path,
                wm.create_date_time,
                wm.mod_date_time,
                wm.user_id,
                max (
                   case
                      when wmd.column_name = 'ACTV_LOCN_THRESHOLD_PCNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   actv_locn_threshold_pcnt,
                max (
                   case
                      when wmd.column_name =
                              'CASE_PICK_LOCN_THRESHOLD_PCNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   case_pick_locn_threshold_pcnt,
                max (
                   case
                      when wmd.column_name = 'AUTO_GEN_CC_TASK'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   auto_gen_cc_task,
                max (
                   case
                      when wmd.column_name = 'QUAL_AUD_FAILED_LOCK_CODE'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   qual_aud_failed_lock_code,
                max (
                   case
                      when wmd.column_name = 'MAX_CC_RECNT'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   max_cc_recnt,
                max (
                   case
                      when wmd.column_name = 'DELV_CONFIRMATION_ENABLED'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   delv_confirmation_enabled,
                max (
                   case
                      when wmd.column_name = 'DISTRO_SEQ_METHOD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   distro_seq_method,
                max (
                   case
                      when wmd.column_name = 'FLOWTHRU_ALLOC_METHOD'
                      then
                         wmd.column_value
                      else
                         null
                   end)
                   flowthru_alloc_method
           from    whse_master wm
                inner join
                   whse_master_dtl wmd
                on wmd.whse_master_id = wm.whse_master_id
       group by wm.whse,
                wm.whse_name,
                wm.whse_addr_id,
                wm.cls_timezone_id,
                wm.create_date_time,
                wm.mod_date_time,
                wm.user_id);

set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RE_RULE_OBJ_TYPE

INSERT INTO RE_RULE_OBJ_TYPE ( RE_RULE_OBJTYPE_ID,DESCRIPTION) 
VALUES  ( 4,'Shipment');

INSERT INTO RE_RULE_OBJ_TYPE ( RE_RULE_OBJTYPE_ID,DESCRIPTION) 
VALUES  ( 8,'Order');

INSERT INTO RE_RULE_OBJ_TYPE ( RE_RULE_OBJTYPE_ID,DESCRIPTION) 
VALUES  ( 12,'Purchase Order');

INSERT INTO RE_RULE_OBJ_TYPE ( RE_RULE_OBJTYPE_ID,DESCRIPTION) 
VALUES  ( 16,'Booking');

INSERT INTO RE_RULE_OBJ_TYPE ( RE_RULE_OBJTYPE_ID,DESCRIPTION) 
VALUES  ( 28,'ASN');

INSERT INTO RE_RULE_OBJ_TYPE ( RE_RULE_OBJTYPE_ID,DESCRIPTION) 
VALUES  ( 20,'LPN');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for GRS_OPT_RUN_STATUS

INSERT INTO GRS_OPT_RUN_STATUS ( GRS_OPT_RUN_STATUS,DESCRIPTION) 
VALUES  ( 8,'Running');

INSERT INTO GRS_OPT_RUN_STATUS ( GRS_OPT_RUN_STATUS,DESCRIPTION) 
VALUES  ( 4,'Pre-Processing');

INSERT INTO GRS_OPT_RUN_STATUS ( GRS_OPT_RUN_STATUS,DESCRIPTION) 
VALUES  ( 16,'Completed');

INSERT INTO GRS_OPT_RUN_STATUS ( GRS_OPT_RUN_STATUS,DESCRIPTION) 
VALUES  ( 12,'Post-Processing');

Commit;





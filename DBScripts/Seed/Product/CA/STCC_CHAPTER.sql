set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for STCC_CHAPTER

INSERT INTO STCC_CHAPTER ( STCC_CHAPTER,DESCRIPTION) 
VALUES  ( 0,'CH-0');

INSERT INTO STCC_CHAPTER ( STCC_CHAPTER,DESCRIPTION) 
VALUES  ( 2,'CH-2');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RS300VERSION

INSERT INTO RS300VERSION ( VERSION_ID,DESCRIPTION) 
VALUES  ( 'OB','Rating system for TP Engine');

INSERT INTO RS300VERSION ( VERSION_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','Production Rating System for DriverLoad 6.1');

Commit;





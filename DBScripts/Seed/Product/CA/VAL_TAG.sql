set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for VAL_TAG

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'count','Count');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'oli','Order Line Item');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'item','Item');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'olpn','oLpn');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'sum','Sum');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'pkt','Pickticket');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'status','Status');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'store','Store');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'lpn','Lpn');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'pix','Pix');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'wave','Wave');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'retail','Retail');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'ilpn','iLpn');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'locn','Location');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'aid','Allocation');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'pld','PLD');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'invn','Inventory');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'qty','Quantity');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'order','Order');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'task','Task');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'health','Health Check');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'bool','Boolean');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'stddev','Standard Deviation');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'pw','Pack Wave');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'vas','VAS');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'asn','ASN');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'wo','Work Order');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'cc','Cycle Count');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'shpmt','Shipment');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'rte','Route');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'acttrk','Activity Tracking');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'data','Data');

INSERT INTO VAL_TAG ( TAG,UI_DESC) 
VALUES  ( 'psi','Picking Shorts');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for EM_OE_NOTIFYSTATUS

INSERT INTO EM_OE_NOTIFYSTATUS ( EVTNOTIF_STATUS_ID,DESCRIPTION) 
VALUES  ( 8,'ACKNOWLEDGED');

INSERT INTO EM_OE_NOTIFYSTATUS ( EVTNOTIF_STATUS_ID,DESCRIPTION) 
VALUES  ( 16,'ESCALATED');

INSERT INTO EM_OE_NOTIFYSTATUS ( EVTNOTIF_STATUS_ID,DESCRIPTION) 
VALUES  ( 4,'TRIGGERED');

Commit;





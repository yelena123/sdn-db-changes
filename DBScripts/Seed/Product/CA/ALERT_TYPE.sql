set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ALERT_TYPE

INSERT INTO ALERT_TYPE ( ALERT_TYPE,DESCRIPTION) 
VALUES  ( 5,'Tracking Message Appointment');

INSERT INTO ALERT_TYPE ( ALERT_TYPE,DESCRIPTION) 
VALUES  ( 1,'Update');

INSERT INTO ALERT_TYPE ( ALERT_TYPE,DESCRIPTION) 
VALUES  ( 2,'Cancellation');

INSERT INTO ALERT_TYPE ( ALERT_TYPE,DESCRIPTION) 
VALUES  ( 3,'Recall');

INSERT INTO ALERT_TYPE ( ALERT_TYPE,DESCRIPTION) 
VALUES  ( 4,'Dock Appointment');

Commit;





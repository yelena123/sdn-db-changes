set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_WF_ATTRIBUTEDEF

INSERT INTO A_WF_ATTRIBUTEDEF ( ATTRIBUTE_DEF_ID,DOMAIN_GROUP_ID,NAME,BOUND_OBJECT,BOUND_PROPERTY) 
VALUES  ( 1,201,'ETA','InventoryData','newInventoryChangeData.expectedArrivalDateString');

INSERT INTO A_WF_ATTRIBUTEDEF ( ATTRIBUTE_DEF_ID,DOMAIN_GROUP_ID,NAME,BOUND_OBJECT,BOUND_PROPERTY) 
VALUES  ( 2,202,'ETA','InventoryData','newInventoryChangeData.expectedArrivalDateString');

Commit;





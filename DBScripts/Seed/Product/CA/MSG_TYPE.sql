set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for MSG_TYPE

INSERT INTO MSG_TYPE ( MSG_TYPE,DESCRIPTION) 
VALUES  ( 'ATPOUTPUT','ATPOUTPUT');

INSERT INTO MSG_TYPE ( MSG_TYPE,DESCRIPTION) 
VALUES  ( 'InventoryOBEndSyncXML','InventoryOBEndSyncXML');

INSERT INTO MSG_TYPE ( MSG_TYPE,DESCRIPTION) 
VALUES  ( 'InventoryOBSyncXML','InventoryOBSyncXML');

INSERT INTO MSG_TYPE ( MSG_TYPE,DESCRIPTION) 
VALUES  ( 'InventoryOBSyncCSV','InventoryOBSyncCSV');

INSERT INTO MSG_TYPE ( MSG_TYPE,DESCRIPTION) 
VALUES  ( 'AvailabilityStartSync','AvailabilityStartSync');

INSERT INTO MSG_TYPE ( MSG_TYPE,DESCRIPTION) 
VALUES  ( 'AvailabilitySync','AvailabilitySync');

INSERT INTO MSG_TYPE ( MSG_TYPE,DESCRIPTION) 
VALUES  ( 'AvailabilityEndSync','AvailabilityEndSync');

INSERT INTO MSG_TYPE ( MSG_TYPE,DESCRIPTION) 
VALUES  ( 'InventoryOBStartSyncXML','InventoryOBStartSyncXML');

INSERT INTO MSG_TYPE ( MSG_TYPE,DESCRIPTION) 
VALUES  ( 'StoreLayout','Store Layout import');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for MO_EXECUTION_STATUS

INSERT INTO MO_EXECUTION_STATUS ( MO_EXECUTION_STATUS,DESCRIPTION) 
VALUES  ( 4,'Open');

INSERT INTO MO_EXECUTION_STATUS ( MO_EXECUTION_STATUS,DESCRIPTION) 
VALUES  ( 32,'Complete');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CFMF_STATUS

INSERT INTO CFMF_STATUS ( CFMF_STATUS,DESCRIPTION) 
VALUES  ( 1,'Open');

INSERT INTO CFMF_STATUS ( CFMF_STATUS,DESCRIPTION) 
VALUES  ( 2,'Closed');

INSERT INTO CFMF_STATUS ( CFMF_STATUS,DESCRIPTION) 
VALUES  ( 3,'Canceled');

INSERT INTO CFMF_STATUS ( CFMF_STATUS,DESCRIPTION) 
VALUES  ( 4,'Waiting to Open');

Commit;





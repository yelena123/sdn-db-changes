set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RESOURCES

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8571,'/ofr/claim/customrule/jsp/JERuleLoader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8572,'/ofr/claim/jsp/SettleClaimsDialog.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8573,'/ofr/claim/shipper/jsp/CargoClaimDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8574,'/ofr/claim/shipper/jsp/ClaimComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8575,'/ofr/claim/shipper/jsp/ClaimOriginDestination.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8576,'/ofr/claim/shipper/jsp/ClaimPayments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8577,'/ofr/consolidation/jsp/processSplitOrderPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8578,'/ofr/consolidation/workspace/jsp/ViewConsolidationShipmentButtons.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8579,'/ofr/consolidation/workspace/jsp/ViewUnAssignedOrdersButtons.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8580,'/cbo/transactional/location/view/CaseLocnFitList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8581,'/cbo/transactional/location/view/LocationGrpList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8999,'/obl/profile/view_tp_corporate_commodities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9000,'/cbo/transactional/purchaseorder/view/POLineItemCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9001,'/cbo/transactional/rts/view/RTSEditViewLinesTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9002,'/cbo/transactional/rts/view/RTSList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9003,'/cbo/transactional/shipment/view/EditShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9004,'/cbo/transactional/shipment/view/ShipmentEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9005,'/ofr/detention/shipper/jsp/DetentionRequestAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9006,'/ofr/detention/shipper/jsp/EditDetention.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9007,'/basedata/businesspartner/jsp/BusinessPartnerList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9008,'/basedata/carrier/view/CarrierCreateMain.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9009,'/basedata/carrier/view/CarrierEditPayInsurance.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9044,'/basedata/commoditycode/view/CommodityCode.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9045,'/basedata/country/jsp/SaveCountryCreditLimit.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9046,'/uclEditUserServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9047,'/uclRelationshipFilterServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9048,'/cbo/transactional/distributionorder/view/DistributionOrderList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9049,'/cbo/transactional/infeasibility/UnNumberUnNumber/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9050,'/cbo/transactional/infeasibility/productClassCarrier/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9051,'/basedata/staticschedule/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9052,'/basedata/suppliercategory/jsp/ViewSupplierCategory.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9053,'/basedata/unnumber/view/UnNumberList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9054,'/basedata/userdefined/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9055,'/basedata/util/AutoCreateBaseData.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9056,'/basedata/zone/jsp/ProcessZone.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9057,'/basedataDistributionListUpdate.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9058,'/cbo/admin/criticalchange/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9059,'/cbo/admin/parameter/general/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9060,'/cbo/item/ItemFacilityCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9061,'/cbo/item/ItemPackageEditView.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9062,'/cbo/item/ItemSupplierXRefListEditView.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9063,'/cbo/transactional/alerts/view/ApAlertList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9064,'/cbo/transactional/alerts/jsp/APViewAlertDefDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9065,'/cbo/transactional/alerts/jsp/POAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9066,'/cbo/transactional/alerts/jsp/ProcessSaveAlertComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9067,'/cbo/transactional/asn/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9068,'/cbo/transactional/asn/ASNDetailsAddPO.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9069,'/cbo/transactional/asn/ASNDetailsLPNLookupAndLPNList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9070,'/cbo/transactional/asn/CreateASNFromPO.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9071,'/cbo/transactional/distributionorder/view/DOBulkEditPickupDeliveryTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9072,'/cbo/transactional/distributionorder/view/DOCreateMainPO.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9073,'/fm/popup/CreateRelay.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9074,'/fm/popup/EquipmentInstancePopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9075,'/fm/trip/DispatchShipmentDetailsOverridesForCreateRelay.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9076,'/basedata/payee/PayeeCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9077,'/basedata/popup/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9078,'/basedata/currency/view/CurrencyConversionPopup.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9079,'/basedata/region/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9080,'/obl/rfp/lane/sourcing/admin_move_volume.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9081,'/obl/rfp/laneaward/delete_laneaward.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9082,'/obl/rfp/postrfp/post_rfp_include.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9083,'/obl/rfp/postrfp/post_rfp_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9084,'/obl/rfp/rfp/cancel_rfp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9085,'/obl/rfp/rfp/decline_rfp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9086,'/obl/rfp/rfp/download_rfp_data_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9087,'/obl/rfp/rfp/export_rfp_data_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9088,'/obl/rfp/rfp/process_create_base_scenario.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9089,'/obl/rfp/rfp/process_edit_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9090,'/obl/rfp/rfp/select_special_reqs_fields_for_view.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9091,'/obl/rfp/rfp/view_rfp_decline_reason.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9092,'/obl/scenario/process_scenario_advanced_overrides_add.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9093,'/obl/scenario/scenario_package_detail_print.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9094,'/obl/scenario/scenario_validate_session.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9095,'/obl/scenario/scenario_winning_packages_details.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10590,'/basedata/rating/bulkrating/jsp/NewBulkRatingRules.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10591,'/basedata/rating/lanesandrates/jsp/accessorialOptionGroupPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10592,'/basedata/rating/lanesandrates/jsp/carrierRateLookup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10593,'/basedata/rating/lanesandrates/jsp/carrierRateLookupDetails.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10594,'/basedata/rating/lanesandrates/jsp/carrierRatingLaneRateDetailsPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10595,'/basedata/rating/lanesandrates/jsp/CarrierRelationRequired.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10596,'/basedata/rating/lanesandrates/jsp/carrierRoutingLaneCarrierDetailsPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10597,'/basedata/rating/lanesandrates/jsp/laneErrorPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10598,'/basedata/rating/lanesandrates/jsp/laneRatePopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10599,'/basedata/rating/lanesandrates/jsp/laneRGPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10600,'/basedata/rating/lanesandrates/jsp/laneSailingPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10601,'/basedata/rating/lanesandrates/jsp/laneSailingWeeklyPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10602,'/basedata/rating/lanesandrates/jsp/laneTotalCostPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10603,'/basedata/rating/lanesandrates/jsp/processOrderQrl.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10604,'/basedata/rating/lanesandrates/jsp/scheduleList.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10605,'/basedata/rating/lanesandrates/jsp/shipperTPLaneDetail.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10606,'/basedata/rating/ratinglane/jsp/accessorialCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10607,'/basedata/rating/ratinglane/jsp/AccessorialExclusionPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10608,'/basedata/rating/ratinglane/jsp/adminAccessorials.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10609,'/basedata/rating/ratinglane/jsp/adminAccessorialsChangeSummaryPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10610,'/basedata/rating/ratinglane/jsp/adminRates.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10611,'/basedata/rating/ratinglane/jsp/adminAccessorialsPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10612,'/basedata/rating/ratinglane/jsp/adminRatesChangeSummaryPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10613,'/basedata/rating/ratinglane/jsp/adminRatesPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10614,'/basedata/rating/ratinglane/jsp/carrierCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10615,'/basedata/rating/ratinglane/jsp/displayLaneDataErrors.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8582,'/cbo/transactional/location/view/PickLocationHdrList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8583,'/cbo/transactional/lpn/view/AddLPNToPallet.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8584,'/cbo/transactional/lpn/view/LPNListOutboundMain.jsflps?ListType=LPN_OUTBOUND','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8585,'/cbo/transactional/purchaseorder/view/CreateCustomerOrder.jsflps?channelType=40','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8586,'/cbo/transactional/purchaseorder/view/CreateRetailOrder.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8587,'/cbo/transactional/purchaseorder/view/EditSalesOrderLine.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8588,'/ofr/admin/parameter/detention/jsp/SetUnloadAllowanceTime.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8589,'/ofr/admin/parameter/general/jsp/EditGeneralParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8590,'/ofr/admin/parameter/jsp/EditMaxWaitTime.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8591,'/ofr/admin/parameter/jsp/ViewValidationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8592,'/cbo/transactional/distributionorder/view/DistributionOrderList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8593,'/cbo/transactional/distributionorder/view/DOCreateFulfillmentTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8594,'/cbo/transactional/distributionorder/view/DOCreateMain.jsflps?isTemplate=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8595,'/cbo/transactional/distributionorder/view/DOCreateShippingTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8596,'/cbo/transactional/distributionorder/view/DODetailsChildOrders.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8597,'/cbo/transactional/distributionorder/view/DODetailsHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8598,'/fm/shipmentloaddetail/ShipmentLoadDetailForTripSheet.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8599,'/fm/trip/DispatchShipmentDetailsOBCPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8600,'/basedata/poterm/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8601,'/basedata/protectionlevel/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8602,'/basedata/region/jsp/RegionSummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8603,'/obl/rfp/lane/shipper_singlepack_edit_method.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8604,'/obl/rfp/lane/sourcing/processSourcingChanges.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8605,'/obl/rfp/rfp/administer_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8606,'/obl/rfp/rfp/attached_files_master.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8607,'/obl/rfp/rfp/award_import_configuration.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8608,'/obl/rfp/rfp/modify_invited_tps.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8609,'/obl/rfp/rfp/popup_rfp_mode_view.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8610,'/obl/rfp/rfp/processSetEmailId.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8611,'/obl/rfp/rfp/process_create_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8612,'/obl/rfp/rfp/process_invited_incumbency_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8613,'/obl/rfp/rfp/process_new_round_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8614,'/obl/scenario/process_global_rules.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8615,'/obl/scenario/scenario_advanced_overrides_add.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8616,'/obl/scenario/scenario_carrier_report_print.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8617,'/obl/scenario/scenario_facility_report.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8618,'/obl/scenario/scenario_winning_package_details_print.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8619,'/obl/updateCounts.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8620,'/obl/xml/xml_health_checker.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8621,'/scv/admin/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8622,'/scv/storelocations/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8630,'/basedata/mode/view/ModeSizeLimitView.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8631,'/basedata/packagetype/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8632,'/ofr/rsarea/jsp/ProcessResourceArea.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8633,'/ofr/trackingmsg/jsp/OverrideSoftChecksForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8634,'/ofr/trackingmsg/jsp/ProcessCreateTrackingMessage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8635,'/te/admin/parameter/jsp/EditTEParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8636,'/ofr/claim/carrier/jsp/CarrierClaimParameterResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8637,'/basedata/report/jsp/NewSummaryReportDefinition.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8638,'/cbo/transactional/distributionorder/view/DOEditMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8639,'/cbo/transactional/distributionorder/view/DOLineCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8640,'/cbo/transactional/distributionorder/view/DOLineDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8641,'/cbo/transactional/distributionorder/view/DOLineDetailsMiscellaneous.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8642,'/cbo/transactional/distributionorder/view/DOLineDetailsWaveAlloction.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8643,'/cbo/transactional/distributionorder/view/DOLineEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8644,'/cbo/transactional/distributionorder/view/DOLineEditDetailsTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8645,'/cbo/transactional/distributionorder/view/DOLineEditMiscTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8646,'/cbo/transactional/infeasibility/facilityEquipment/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8647,'/basedata/dockdispatchcapacity/jsp/DockDispatchCapacity.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8648,'/basedata/documentation/jsp/DocumentList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8649,'/basedata/equipment/jsp/EquipmentSummarySheet.jsp?equipmentOperation=create','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8650,'/basedata/equipmentinstance/jsp/EquipmentInstanceCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8651,'/basedata/equipmentinstance/jsp/PopUpEquipmentInstanceSummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8652,'/basedata/facility/jsp/ProcessFacilityRemoveComment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8653,'/basedata/inventorysegment/jsp/InventorySegment.jsflps?customFilter=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8654,'/ofr/infeasibility/dockEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8655,'/ofr/infeasibility/dockProtectionLevel/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8656,'/ofr/infeasibility/dockProtectionLevel/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8657,'/ofr/infeasibility/driverTypeEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8658,'/ofr/infeasibility/driverTypeFacility/jsp/AddInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8659,'/ofr/infeasibility/facilityTP/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8660,'/ofr/infeasibility/productClassEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8661,'/ofr/infeasibility/productClassProductClass/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8662,'/ofr/invoice/shipper/jsp/computeInvoiceTotalCost.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8663,'/ofr/invoice/shipper/jsp/InvoiceDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8664,'/ofr/invoice/shipper/jsp/InvoiceSoftChecks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8665,'/ofr/invoice/ui/getClaims.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8666,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/ConsolidationRegionCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8667,'/ofr/optimizationmgmt/consolidation/enginespecific/aggr/jsp/EngineSpecificRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8668,'/ofr/claim/import/TestClaimImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8669,'/ofr/claim/shipper/jsp/overrideSoftChecksForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8670,'/ofr/claim/shipper/jsp/ViewCargoClaimDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8671,'/ofr/consolidation/manual/scenario/ui/ManConsScenarioComparisonPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8672,'/ofr/consolidation/workspace/jsp/buttonsPanel.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8673,'/ofr/continuousmove/jsp/cmLocationInfeasibPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8674,'/ofr/continuousmove/jsp/cmParametersDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8675,'/ofr/continuousmove/jsp/processCmParametersEdit.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8676,'/ofr/detention/jsp/ShipperDetentionComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8677,'/fm/trip/WorkspaceAddShipment.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8678,'/cbo/transactional/infeasibility/jsp/InfeasibleEquipmentPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8679,'/cbo/transactional/infeasibility/productClassCarrier/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8680,'/cbo/transactional/location/view/PickLocationHdrList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8681,'/cbo/transactional/location/view/PickLocationHdrLock.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8682,'/cbo/transactional/lpn/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8683,'/cbo/transactional/lpn/view/CaseLPNSelection.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8684,'/cbo/transactional/lpn/view/EditLPNHeaderOutbound.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8685,'/cbo/transactional/lpn/view/EditLPNHeaderTPM.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8686,'/cbo/transactional/profile/view/DocumentationProfile.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8687,'/cbo/transactional/purchaseorder/view/CreateSalesOrderHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8688,'/ofr/admin/parameter/detention/jsp/EditCarrDetentionNotificationParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8689,'/ofr/admin/parameter/detention/jsp/ProcessUnloadAllowanceTime.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8690,'/ofr/admin/parameter/detention/jsp/ViewCarrDetentionNotificationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8691,'/ofr/admin/parameter/distancetime/jsp/ProcessDistanceTimeParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8692,'/ofr/admin/parameter/fleet/jsp/EditFleetParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8693,'/ofr/admin/parameter/general/jsp/EditGeneralParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8694,'/ofr/admin/parameter/jsp/ParametersInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8695,'/ofr/admin/user/jsp/UserSettings.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8696,'/ofr/archive/jsp/AlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8697,'/ofr/booking/jsp/BookingAnalysisOrderVolumeInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8223,'/obl/rfp/lane/shipper_singlepack_edit_set_variable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8224,'/obl/rfp/lane/view_lane_main.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8225,'/obl/rfp/message/message_summary_list_content.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8226,'/obl/rfp/rfp/cancel_rfp_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8227,'/obl/rfp/rfp/download_rfp_include.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8228,'/obl/rfp/rfp/rfp_essential.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8229,'/obl/rfp/rfp/save_list_popup.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8230,'/obl/scenario/process_facility_rules.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8231,'/obl/scenario/scenario_awarded_carrier.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8232,'/obl/scenario/scenario_facility_report_print.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8233,'/fm/driverkiosk/KioskAltDestPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8234,'/fm/driverkiosk/KioskTripList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8235,'/fm/debrief/DSPCollectionActivityForTripSheet.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8236,'/fm/debrief/PreTripDebrief.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8237,'/fm/debrief/StateTaxMiles.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8238,'/ofr/optimizationmgmt/consolidation/jsp/ConsolidationRunDetailsPEPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8239,'/ofr/optimizationmgmt/consolidation/jsp/GetOrdersForConsolidationRunTemplateTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8240,'/ofr/optimizationmgmt/consolidation/jsp/PrefixedOrderPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8241,'/ofr/optimizationmgmt/consolidation/jsp/SelectConsolidationEngine.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8242,'/ofr/optimizationmgmt/consolidation/jsp/SendOptiFlowMessage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8243,'/ofr/optimizationmgmt/consolidation/jsp/StartMultipleRunConfirmationPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8244,'/ofr/optimizationmgmt/consolidation/jsp/StartRunConfirmationPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8245,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditAddToExistingShipmentParams.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8246,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditSplitterParams.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8247,'/cbo/transactional/shipment/view/ShipmentEditStops.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8248,'/lps/customization/customize.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8249,'/manh/tools/ca/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8250,'/obl/filter/jsp/ApplyDefault.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8251,'/ofr/OFRClearSession.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8252,'/ofr/ra/jsp/DevTools.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8253,'/ofr/rs/jsp/test/createInYardEquipAvailEvent.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8254,'/uclEditCompanyServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8255,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditTMSLiteParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8256,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewDockDoorCapacity.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8257,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewTMSLiteParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8258,'/ofr/ra/jsp/DockLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8259,'/ofr/ra/jsp/EditMasterTpCode.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8260,'/ofr/rs/jsp/carrierPFDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8261,'/ofr/rs/jsp/processAdminShipWindowAdjParam.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8262,'/ofr/rs/jsp/processAssignedRSConfig.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8263,'/ofr/rs/jsp/processBaseResourceConfiguration.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8264,'/ofr/rs/jsp/processModeActionMapping.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8265,'/ofr/rs/jsp/processTenderDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8266,'/ofr/rs/jsp/shipperModeActionDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8267,'/ofr/rs/jsp/shipperResourceConfig.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8268,'/basedata/cmreport/jsp/processCMReportTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8269,'/obl/bid/lane_bid/carrier_one_lane_set_variable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8270,'/obl/bid/lane_bid/shipper_package_method.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8271,'/obl/common/jsp/globalFooter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8272,'/obl/common/jsp/header.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8273,'/obl/errors/rfpError.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8274,'/obl/makeSession.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8275,'/obl/popups/duplicate_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8276,'/obl/popups/new_round_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8277,'/obl/profile/transport_provider/capabilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8278,'/obl/profile/transport_provider/commodities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8279,'/obl/profile/transport_provider/fcl_mode_master.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8280,'/obl/profile/transport_provider/lcl_mode_master.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8281,'/obl/profile/transport_provider/rail_mode_master.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8282,'/obl/profile/view_tp_corporate_statistics.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8283,'/obl/rfp/capacity/capacity_bid.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8284,'/cbo/transactional/purchaseorder/view/POEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8285,'/cbo/transactional/purchaseorder/view/SalesOrderLineList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8286,'/cbo/transactional/rts/view/CreateRTSGeneral.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8287,'/cbo/transactional/shipment/view/EditCustomFields.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8288,'/cbo/transactional/shipment/view/EditStopList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8289,'/cbo/transactional/shipment/view/EditStopWRealOrders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8290,'/cbo/transactional/shipment/view/ShipmentAddComment.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8291,'/cbo/transactional/shipment/view/ShipmentCreateComments.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8292,'/cbo/transactional/shipment/view/ShipmentCreateOrders.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8293,'/ofr/detention/shipper/jsp/AssignUnassignPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8294,'/ofr/detention/shipper/jsp/OverrideDetentionSoftChecks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8295,'/ofr/detention/shipper/jsp/processAssignUnAssignCancel.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8296,'/basedata/admin/parameter/sizes/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8297,'/basedata/carrier/view/CarrierEditGeneral.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8298,'/basedata/carrierserv/driver/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8327,'/cbo/transactional/asn/ASNList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8328,'/cbo/transactional/infeasibility/facilityTP/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8329,'/cbo/transactional/infeasibility/productClassProductClass/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8330,'/basedata/addressparam/jsp/ProcessDockHours.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8331,'/basedata/skusubstitution/jsp/EditSkuSubstitutionDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8332,'/basedata/sourcing/jsp/SkuSourcingRuleList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8333,'/basedata/staticroute/jsp/NewStaticRoute.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10538,'/basedata/admin/util/jsp/ShipperSpecificHierarchy.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10539,'/basedata/admin/util/jsp/ShipperSpecificHierarchySoftChecks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10540,'/basedata/admin/util/jsp/testrateware.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10541,'/basedata/driver/jsp/DriverActivityCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10542,'/basedata/driver/jsp/DriverActivitySummaryPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10543,'/basedata/driver/jsp/DriverActivitySummaryPresentationViewOnly.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10544,'/basedata/equipmentinstance/jsp/RaEquipmentInstanceCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10545,'/basedata/imports/jsp/invokeTXMLProcessingService.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10546,'/basedata/mindensityparam/jsp/editMinDensityParam.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10547,'/basedata/mindensityparam/jsp/minDensityParamList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10548,'/basedata/mindensityparam/jsp/processMinDensityParametersCRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10549,'/basedata/path/jsp/ExportPathSet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10550,'/basedata/path/jsp/PathSetErrorsList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10551,'/basedata/path/jsp/ProcessExportPathSet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10552,'/basedata/rating/currencyconversion/jsp/CurrencyConversionAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10553,'/basedata/rating/currencyconversion/jsp/ExchangeRateList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10554,'/basedata/rating/currencyconversion/jsp/ProcessExchangeRatesCRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10555,'/basedata/rating/ratinglane/jsp/createLane.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10556,'/basedata/path/jsp/pathSetEditDates.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10557,'/basedata/path/jsp/pathSetGenerationTemplateList.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10558,'/basedata/path/jsp/pathSetHeading.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10559,'/basedata/path/jsp/pathSetInit.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10560,'/basedata/path/jsp/pathSetListBottom.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10561,'/basedata/path/jsp/pathSetListBottomBlank.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10562,'/basedata/path/jsp/pathSetListTop.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10563,'/basedata/path/jsp/pathSetOriginDestination.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10564,'/basedata/path/jsp/pathSetScriptAlerts.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8334,'/basedata/suppliercategory/jsp/NewSupplierCategory.jsp?operation=create','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8335,'/basedata/TotalCapacity/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8336,'/basedata/unnumber/view/UnNumberCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8337,'/basedata/wave/jsp/ViewAllWaves.jsp?operation=view','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8338,'/basedata/zone/jsp/createZonePage2.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8339,'/cbo/admin/criticalchange/jsp/SetupShipmentCriticalChange.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8340,'/cbo/admin/parameter/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8341,'/cbo/alert/view/ApAlertDefList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8342,'/cbo/alert/jsp/APViewAlertDefDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8343,'/cbo/businesspartner/BusinessPartnerList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8344,'/cbo/item/CreateItemMain.jsflps?isItemProfile=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8345,'/cbo/item/ItemFacilityList.jsflps?isItemProfile=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8346,'/cbo/item/ItemList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8347,'/cbo/transactional/admin/parameter/jsp/ViewSOParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8348,'/cbo/transactional/alerts/jsp/EscalationDetailPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8349,'/cbo/transactional/asn/NewASN.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8350,'/cbo/transactional/asn/NewASNHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8351,'/fm/driver/DispatchDriverActivityLog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8352,'/fm/driver/DriverDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8353,'/fm/equipment/DispatchEquipInstListColumns.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8354,'/fm/debrief/EndTripDriverActivityLog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8355,'/ofr/optimizationmgmt/consolidation/facilityschedule/jsp/ProcessRemoveFacilityToFacilitySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8356,'/ofr/optimizationmgmt/consolidation/jsp/ConsolidationRunTemplateDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8357,'/ofr/optimizationmgmt/consolidation/jsp/DeconsolidatePOSplitterConfirmation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8358,'/ofr/optimizationmgmt/consolidation/jsp/ProcessPostRunActions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8359,'/ofr/optimizationmgmt/consolidation/jsp/ViewOptiflowMachineStats.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8360,'/cbo/transactional/shipment/view/StopEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8361,'/cbo/uom/CreateUOMMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8362,'/cbo/uom/UOMConversionList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8363,'/lps/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8364,'/obl/filter/jsp/FilterDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8365,'/obl/filter/jsp/FilterList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8366,'/ofr/admin/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8367,'/ofr/admin/util/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8368,'/ofr/customization/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8369,'/ucl/admin/lps/ChannelList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8370,'/ucl/admin/lps/PermissionList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8371,'/uclCreateLocationServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8372,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditTMSLiteParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8373,'/ofr/ra/jsp/editAppointmentDetailsOY.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8374,'/ofr/ra/jsp/FacilityContact.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8375,'/ofr/rs/jsp/businessHoursExceptions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8376,'/ofr/rs/jsp/EditCarrierSpecificValues.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8377,'/ofr/rs/jsp/processAssignRoutePlanOption.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8378,'/ofr/rs/jsp/processRateDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8379,'/ofr/rs/jsp/processResourceConfiguration.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8380,'/ofr/rs/jsp/selectorAnalysis.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8381,'/ofr/rs/jsp/shipmentTender.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8382,'/ofr/rs/jsp/shipperBaseResourceConfig.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8383,'/ofr/rs/jsp/shipperModeActionMapping.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8384,'/ofr/ra/jsp/carrierToShipperSelection.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8385,'/ofr/ra/jsp/LoadBookingFilter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8386,'/ofr/ra/jsp/NewRegion.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8387,'/ofr/ra/jsp/NoteList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8388,'/ofr/ra/jsp/ProcessAcknowledgeAlerts.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8389,'/ofr/ra/jsp/RAClaimDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8390,'/ofr/ra/jsp/RATransClaimDetailsAddComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8391,'/ofr/ra/jsp/viewAppointmentDetailsOY.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8392,'/ofr/ra/jsp/ViewRegions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8393,'/basedata/cmreport/jsp/process_schedule_data_load.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8394,'/obl/bid/rfp_response/administer_special_requirements_response_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8395,'/obl/bid/rfp_response/post/post_rfp_response_check.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8396,'/obl/checkAuthorization.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8397,'/obl/common/jsp/about.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8398,'/obl/common/jsp/NavigationSetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8399,'/obl/common/jsp/topRight.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8400,'/obl/company/location/administer_obl_location.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8401,'/obl/errors/custom_attribute_error.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8402,'/obl/errors/distribution_list_error.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8403,'/obl/errors/laneError.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8404,'/obl/help/view_help.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8405,'/obl/master/javascript_methods.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8406,'/obl/master/rfpNavigation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8407,'/obl/my_optibid.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8408,'/obl/popups/duplicate_rfp_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8409,'/obl/profile/transport_provider/accessorial.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8410,'/obl/profile/view_tp_corporate_account.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8411,'/obl/rfp/basedata/process_create_accessorial.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8412,'/obl/rfp/basedata/process_edit_accessorial.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8413,'/cbo/transactional/rts/view/createAggregatedRTS.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8414,'/cbo/transactional/rts/view/CreateRTSMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8415,'/cbo/transactional/rts/view/RTSCreateLinesTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8416,'/cbo/transactional/rts/view/RTSEditMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8417,'/cbo/transactional/shipment/view/ShipmentEditFullOrderList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8418,'/ofr/detention/shipper/jsp/DetentionContactInformation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8419,'/basedata/businesspartner/jsp/CreateParametersForBP.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8420,'/basedata/carrier/view/CarrierEditContact.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8421,'/basedata/carrier/view/CarrierEditScheduleUtilization.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8422,'/basedata/carrierserv/driver/jsp/DriverActivitySummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8423,'/basedata/carrierserv/driver/jsp/DriverCalendarList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8424,'/basedata/carrierserv/driver/jsp/RaDriverSummaryPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8451,'/basedata/carrierserv/oversizerate/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10565,'/basedata/path/jsp/pathSetScripts.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10566,'/basedata/path/jsp/pathSetShippingParam.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10567,'/basedata/path/jsp/processAddAlternatePath.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10568,'/basedata/path/jsp/processAlternatePath.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10569,'/basedata/path/jsp/processCancelPathSet.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10570,'/basedata/path/jsp/processChangeDate.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10571,'/basedata/path/jsp/processEditBasePath.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10572,'/basedata/path/jsp/processEditShippingParam.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10573,'/basedata/path/jsp/processNewPathSet.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10574,'/basedata/path/jsp/processNewPathSetStep2.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10575,'/basedata/path/jsp/processNewPathSetStep3.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10576,'/basedata/path/jsp/processPathSetCreate.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10577,'/basedata/path/jsp/processPathSetCreateStep2.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10578,'/basedata/path/jsp/processPathSetDuplicate.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10579,'/basedata/path/jsp/processPathSetDuplicateStep2.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10580,'/basedata/path/jsp/processPathSetDuplicateStep3.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10581,'/basedata/path/jsp/processRemoveAlternatePath.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10582,'/basedata/path/jsp/processStartDuplicatePathSet.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10583,'/basedata/path/jsp/wmAttributesPopup.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10584,'/basedata/rating/audit/jsp/RatingAccessorialAuditTrail.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10585,'/basedata/rating/audit/jsp/RatingRateAuditTrail.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10586,'/basedata/rating/audit/jsp/ViewRateSummary.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10587,'/basedata/rating/bulkrating/jsp/BulkRatingRulesDetails.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10588,'/basedata/rating/bulkrating/jsp/BulkRatingRulesList.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10589,'/basedata/rating/bulkrating/jsp/EditBulkRatingRules.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8452,'/basedata/criteria/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8453,'/basedata/criteria/jsp/LaborCriteria.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8454,'/cbo/transactional/infeasibility/protectionEquipmentByFacility/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8455,'/ucl/admin/lps/ViewUserInternalAccessControl.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8456,'/basedata/tpparam/jsp/create_accessorials.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8457,'/wm/systemcontrol/ui/ProgramParmList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8458,'/basedata/addressparam/jsp/CreateDefaultAddressParam.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8459,'/basedata/staticschedule/jsp/StaticSchedule.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8460,'/basedata/taxband/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8461,'/cbo/admin/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8462,'/cbo/admin/criticalchange/jsp/SetupOrderCriticalChange.jsp?isFromLeftNav=1','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8463,'/cbo/admin/criticalchange/jsp/SetupRTSCriticalChange.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8464,'/cbo/admin/criticalchange/jsp/SOCriticalChangeDisplay.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8465,'/cbo/alert/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8466,'/cbo/businesspartner/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8467,'/cbo/businesspartner/BusinessPartnerContactDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8468,'/cbo/containerType/ContainerType.jsflps?tranId=2355','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8469,'/cbo/item/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8470,'/cbo/item/ItemFacilityEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8471,'/cbo/item/ItemList.jsflps?isItemProfile=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8472,'/cbo/item/ItemPackageCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8473,'/cbo/item/ItemProfileDetailsEditView.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8474,'/cbo/lpnsizetype/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8475,'/cbo/lpnsizetype/LPNSizeType.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8476,'/cbo/nextupnbr/NextUpNbr.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8477,'/cbo/transactional/admin/parameter/jsp/EditCBOTransCompanyParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8478,'/cbo/transactional/alerts/jsp/APEditAlertDef.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8479,'/cbo/transactional/asn/ASNToLPNDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8480,'/cbo/transactional/asn/ASNToLPNHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8481,'/cbo/transactional/asn/CreateASNFromPO.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8482,'/cbo/transactional/asn/NewASN.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8483,'/cbo/transactional/comments/view/CommentsList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8484,'/cbo/transactional/distributionorder/view/DOCreateHeaderTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8485,'/fm/lookup/dispatchDriverLookup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8486,'/fm/lookup/dispatchLookup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8487,'/fm/obcmessage/OBCMessageDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8488,'/fm/popup/FacilityPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8489,'/fm/trip/Errors.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8490,'/basedata/payeeserv/jsp/PayeeSummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8491,'/basedata/payeeserv/jsp/ProcessPayeeList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8492,'/basedata/protectionlevel/jsp/ProcessViewProtectionLevels.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8493,'/basedata/referenceid/jsp/ViewLPNRule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8494,'/obl/rfp/rfp/edit_optibid_event_process.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8495,'/obl/rfp/rfp/init_select_special_reqs_fields_for_view.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8496,'/obl/rfp/rfp/processSetEmailNotification.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8497,'/obl/rfp/rfp/process_modify_invited_tps.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8498,'/obl/rfp/rfp/view_rfp_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8499,'/obl/scenario/process_return_to_closed.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8500,'/obl/scenario/process_scenario_advanced_overrides.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8501,'/obl/scenario/process_scenario_lane.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8502,'/obl/scenario/scenario_lane_summary.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8503,'/obl/scenario/scenario_package_detail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8504,'/obl/scenario/scenario_transaction_log.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8505,'/obl/stateLookUp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8506,'/obl/success.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8507,'/obl/workspace/manage_workspace.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8508,'/obl/workspace/new_manage_workspace.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8509,'/obl/xml/download/download_xml_file.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8510,'/obl/xml/upload/uploadXML.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8511,'/obl/ZoneLookUp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8512,'/obl/master/new_master.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8513,'/scem/admin/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8517,'/basedata/mode/view/ModeSizeLimitEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8519,'/basedata/ordertype/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8520,'/basedata/payeeserv/jsp/PayeeContactInfoListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8521,'/basedata/customattribute/jsp/NewCustomAttribute.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8522,'/ofr/rs/jsp/VendorPFRedirectToShipmentList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8523,'/ofr/rs/jsp/VendorViewVendorPFs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8524,'/ofr/rsarea/jsp/AddFacilityToRSArea.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8525,'/ofr/trackingmsg/jsp/CreateTrackingMessageForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8526,'/te/admin/parameter/jsp/EditTEParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8527,'/te/infeasibility/servicelevelcustomer/jsp/ViewInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8528,'/te/optimizationmgmt/consolidation/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8529,'/te/optimizationmgmt/consolidation/parameter/jsp/ParameterSetListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8530,'/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8531,'/ofr/index.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8532,'/ofr/detention/carrier/jsp/processCreateDetention.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8533,'/ofr/ra/jsp/AddComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8534,'/ofr/ra/jsp/carrierAppointmentEditDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8535,'/basedata/servicelevel/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8536,'/cbo/transactional/distributionorder/view/DOEditLinesTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8537,'/cbo/transactional/distributionorder/view/DOEditMainPO.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8538,'/cbo/transactional/distributionorder/view/DOLineCreateDetailsTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8539,'/cbo/transactional/distributionorder/view/DOLineCreateWaveAllocTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8540,'/cbo/transactional/distributionorder/view/DOLineDetailsTransportation.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8541,'/cbo/transactional/errors/view/CBOHardAndSoftCheckErrorList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8542,'/cbo/transactional/globalvisibility/StatusInquiry.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8543,'/basedata/equipment/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8544,'/basedata/equipment/jsp/createInYardEquipmentAvailability.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8545,'/basedata/equipmentinstance/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8546,'/basedata/laboractivity/jsp/LaborActivityTabCopyMode.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8547,'/basedata/location/LocationDetailsMain.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8548,'/basedata/location/LocationList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8549,'/ofr/infeasibility/carrierCustomer/jsp/ViewInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8550,'/ofr/infeasibility/dockProductClass/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8551,'/ofr/infeasibility/driverFacility/jsp/ProcessUpdateInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8552,'/ofr/infeasibility/equipmentEquipment/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8553,'/ofr/infeasibility/facilityTP/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8554,'/ofr/infeasibility/jsp/InfeasibleEquipmentPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8555,'/ofr/infeasibility/modeProtectionLevel/jsp/UpdateInfeasibleModeProtectionLevel.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8556,'/ofr/infeasibility/productClassEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8557,'/ofr/infeasibility/productClassProductClass/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8558,'/ofr/infeasibility/protectionEquipmentByFacility/jsp/ProcessRemoveFeasiblePairs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8559,'/ofr/infeasibility/protectionLevelFacility/jsp/UpdateInfeasibleProtectionLevelFacility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8560,'/ofr/invoice/shipper/accountcoding/jsp/processAdminAccountCode.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8561,'/ofr/invoice/shipper/jsp/AdjustmentInvoice.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8562,'/ofr/invoice/shipper/jsp/CustomerInvoiceDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8563,'/ofr/invoice/shipper/jsp/processCarrierCodeLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8564,'/ofr/invoice/ui/InvoiceTPEObjectPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8565,'/ofr/invoice/ui/ViewOriginDestination.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8566,'/ofr/optimizationmgmt/consolidation/autoapproval/jsp/AutoApprovalTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8567,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/StateListBox.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8568,'/ofr/optimizationmgmt/consolidation/enginespecific/con4/jsp/ProcessEngineSpecificRunTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8569,'/ofr/optimizationmgmt/consolidation/enginespecific/splt/jsp/EngineSpecificRunTemplateDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8570,'/fm/trip/TripDetailsAltDestPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8831,'/cbo/syscode/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8832,'/cbo/transactional/alerts/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8833,'/cbo/transactional/asn/ASNList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8834,'/cbo/transactional/distributionorder/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8835,'/cbo/transactional/distributionorder/view/DODetailsAuditTrail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8836,'/cbo/transactional/distributionorder/view/DODetailsFulfillment.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8837,'/fm/popup/ShipmentDetailsPopUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8838,'/fm/shipmentloaddetail/ShipmentLoadDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8839,'/basedata/protectionlevel/jsp/ViewAllProtectionLevels.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8840,'/basedata/qualitylevel/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8841,'/basedata/qualitylevel/jsp/ViewAllQualityLevels.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8842,'/basedata/currency/view/CurrencyConversionHistoryList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8843,'/basedata/region/jsp/ResourceRegionFacilityPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8844,'/obl/rfp/lane/TPLaneBidAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8845,'/obl/rfp/lane/view_lane_bid_general.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8846,'/obl/rfp/message/administer_msg.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8847,'/obl/rfp/postrfpaward/success_post_rfp_award.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8848,'/obl/rfp/rfp/edit_optibid_event.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8849,'/obl/rfp/rfp/Effective_Rate_Calc.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8850,'/obl/rfp/rfp/process_bid_cost.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8851,'/obl/rfp/rfp/view_special_reqs_response.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8852,'/obl/scenario/duplicate_scenario_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8853,'/obl/scenario/process_scenario_lane_detail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8854,'/obl/scenario/scenario_facility_carriers_IB.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8855,'/obl/scenario/scenario_infeasibility_options.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8856,'/obl/scenario/scenario_infeasibility_report.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8857,'/obl/scenario/scenario_overview_include.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8858,'/obl/view_distribution_list_carriers.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8859,'/obl/xml/upload/upload_override_xml_file.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8860,'/scem/eventmgt/schedule/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8866,'/basedata/ordertype/jsp/OrderType.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8867,'/basedata/packagetype/jsp/PackageTypeCreate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8868,'/basedata/packagetype/jsp/PackageTypeList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8869,'/ofr/rs/jsp/shipperTPLaneList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8870,'/te/optimizationmgmt/consolidation/parameter/jsp/EditConsolidationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8871,'/ofr/detention/carrier/jsp/RADetentionReportProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8872,'/ofr/ra/jsp/BookingAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8873,'/basedata/region/plannerregion/jsp/ViewPlannerRegions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8874,'/cbo/transactional/distributionorder/view/DODetailsMiscellaneous.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8875,'/cbo/transactional/distributionorder/view/DOEditPickupDeliveryTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8876,'/cbo/transactional/filter/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8877,'/basedata/equipmentinstance/jsp/EquipmentInstanceDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8878,'/basedata/equipmentinstance/jsp/EquipmentInstanceSummarySheet.jsp?operation=create','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8879,'/basedata/facilityyard/jsp/createFacilityYardZone.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8880,'/basedata/location/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8881,'/ofr/infeasibility/carrierCustomer/jsp/AddInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8882,'/ofr/infeasibility/dockEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8883,'/ofr/infeasibility/driverFacility/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8884,'/ofr/infeasibility/driverTypeEquipment/jsp/ProcessUpdateInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8885,'/ofr/infeasibility/productClassEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8886,'/ofr/infeasibility/protectionLevelFacilityPair/jsp/InfeasibleProtectionLevelFacilityPairPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8887,'/ofr/infeasibility/UnNumberEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8888,'/ofr/invoice/shipper/jsp/AllAssociatedInvoices.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8889,'/ofr/invoice/shipper/jsp/carrierCodeLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8890,'/ofr/invoice/shipper/jsp/InvoiceRatingPopUp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8891,'/ofr/invoice/shipper/jsp/processInvoiceAccessorial.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8892,'/ofr/invoice/shipper/jsp/ShipmentCostPopUp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8893,'/ofr/invoice/ui/ViewInvoiceList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8894,'/ofr/invoice/ui/ViewOtherDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8895,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/ConsolidationRegionSummaryPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8896,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/ProcessConsRegion.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8897,'/ofr/optimizationmgmt/consolidation/enginespecific/con4/jsp/EngineSpecificRunTemplateDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8898,'/ofr/optimizationmgmt/consolidation/enginespecific/ofib/jsp/ViewOptiflowOrderErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8899,'/ofr/optimizationmgmt/consolidation/enginespecific/ofob/jsp/ProcessEngineSpecificRunTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8900,'/fm/trip/ShipmentList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8901,'/fm/trip/StateTaxMilesColumns.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8902,'/fm/trip/TripList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8903,'/ofr/claim/customrule/jsp/CreateNewCustomRule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8904,'/ofr/claim/shipper/jsp/ClaimAdditionalInformation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8905,'/ofr/claim/shipper/jsp/ClaimAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8906,'/ofr/claim/shipper/jsp/DisplayImportErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8907,'/ofr/claim/shipper/jsp/processClearJournalEntryErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8908,'/ofr/consolidation/jsp/MultipleDeconsolidateConfirm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8909,'/ofr/continuousmove/jsp/cmDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8910,'/ofr/continuousmove/jsp/processAutoTerminateCm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8911,'/ofr/continuousmove/jsp/processCmLocationInfeasib.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8912,'/ofr/continuousmove/jsp/processCmParametersDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8913,'/ofr/continuousmove/jsp/processManuallyTerminateCm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8914,'/ofr/detention/jsp/AppointmentAndUserInformation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8915,'/ofr/detention/jsp/IncludeShipmentList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8916,'/ofr/detention/jsp/testDetentionStatus.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8917,'/cbo/transactional/infeasibility/modeProtectionLevel/jsp/InfeasibleModeProtectionLevelPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8918,'/cbo/transactional/infeasibility/protectionEquipmentByFacility/jsp/ViewFeasiblePairs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8919,'/cbo/transactional/infeasibility/protectionLevelFacilityPair/jsp/InfeasibleProtectionLevelFacilityPairPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8920,'/cbo/transactional/infeasibility/protectionLevelProtectionLevel/jsp/InfeasibleProtectionLevelPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8921,'/cbo/transactional/location/view/itemLookupDialog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8922,'/cbo/transactional/location/view/LocationSizeList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8923,'/cbo/transactional/location/view/LocnSizeTypeUpdate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8924,'/cbo/transactional/location/view/ReserveLocationDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8925,'/cbo/transactional/location/view/WorkAreaMasterList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8926,'/cbo/transactional/lpn/view/EditLPNHeaderMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8927,'/cbo/transactional/purchaseorder/view/EditRetailOrder.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8928,'/cbo/transactional/purchaseorder/view/POCommentsEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8929,'/ofr/admin/parameter/distancetime/jsp/EditDistanceTimeParametersPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8930,'/ofr/admin/parameter/distancetime/jsp/ViewDistanceTimeParametersPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8931,'/ofr/admin/parameter/jsp/EditParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8932,'/ofr/admin/parameter/jsp/EditValidationParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8933,'/ofr/booking/jsp/BookingAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8934,'/ofr/booking/jsp/shipmentLaneDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8935,'/fm/bid/BidDetailsOBCPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8936,'/fm/dashboard/MapPopupDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8937,'/fm/dashboard/noResource.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8938,'/fm/debrief/DebriefStopsDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8939,'/fm/debrief/DebriefStopsDetailsForTripSheet.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8940,'/fm/debrief/TripDebriefHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8941,'/obl/rfp/lane/process_ship_singlepack_save_lane_bid.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8942,'/obl/rfp/lane/shipper_package.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8943,'/fm/driver/DispatchDriverDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8944,'/fm/driverkiosk/KioskTripListOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8945,'/ofr/optimizationmgmt/consolidation/jsp/CreatedShipmentList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8946,'/ofr/optimizationmgmt/consolidation/jsp/DeconsolidateShipmentsOrdersResult.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8947,'/ofr/optimizationmgmt/consolidation/jsp/EnableShipmentQueue.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8948,'/ofr/optimizationmgmt/consolidation/jsp/PrefixedPurchaseOrderPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8949,'/ofr/optimizationmgmt/consolidation/jsp/ProcessDeconsolidatePOSplitter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8950,'/cbo/transactional/shipment/view/ShipmentEditStopList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8951,'/cbo/transactional/shipment/view/ShipmentTemplateList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8952,'/cbo/transactional/shipment/view/StopEditGeneral.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8953,'/cbo/uom/CreateUOMMain.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8954,'/obl/filter/administer_filter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8955,'/ofr/rs/jsp/test/processInYardEquipAvailRefreshEvent.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8956,'/ofr/rs/jsp/test/refreshPFValues.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8957,'/ofr/test/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8958,'/te/ProcessXMLCommManagerImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8959,'/ucl/admin/jsp/SuccessPage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8960,'/ucl/admin/lps/ViewUserExternalAccessControl.jsflps?app=ucl','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8961,'/ucl/admin/lps/ViewUserInternalAccessControl.jsflps?app=ucl','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8962,'/ucl/admin/lps/CreateApplicationInstance.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8963,'/ucl/admin/lps/ListApplicationInstance.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8964,'/ucl/admin/lps/RoleList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8965,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ProtectionLevelPenalty.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8966,'/ofr/ra/jsp/EditMasterTpCodeMappings.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8967,'/ofr/ra/jsp/EditRegion.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8968,'/ofr/rs/jsp/AddAutoRecallSusp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8969,'/ofr/rs/jsp/allLaneCarriersPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8970,'/ofr/rs/jsp/businessHoursRegionList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8971,'/ofr/rs/jsp/overrideSoftChecksForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8972,'/ofr/rs/jsp/processAddRoutePlanPath.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8973,'/ofr/rs/jsp/processPFValues.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8974,'/ofr/rs/jsp/processTrackedPerformanceFactors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8975,'/ofr/rs/jsp/reasonCodePopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8976,'/ofr/rs/jsp/shipmentAssignTP.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8977,'/ofr/rs/jsp/shipperPFValueFunctionHolder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8978,'/ofr/rs/jsp/shipperPFValueFunctionPopupHolder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8979,'/ofr/ra/jsp/InternalNotes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8980,'/ofr/ra/jsp/ProcessBookingConfirmMessages.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8981,'/ofr/ra/jsp/ProcessConfirmMessages.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8982,'/ofr/ra/jsp/ProcessRegionCreation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8983,'/ofr/ra/jsp/SelectDeclanationReasonCodePopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8984,'/basedata/cmreport/jsp/cmReport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8985,'/obl/administer_distribution_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8986,'/obl/bid/lane_bid/carrier_lane_method.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8987,'/obl/bid/lane_bid/carrier_one_lane_admin_button.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8988,'/obl/bid/lane_bid/comment_input.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8989,'/obl/bid/lane_bid/package_create_result_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8990,'/obl/CMRunExport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8991,'/obl/common/jsp/popupHeader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8992,'/obl/facilityLookUp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8993,'/obl/header/topVariable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8994,'/obl/master/scenario_workspace_sidenav.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8995,'/obl/master/setup_sidenav_include.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8996,'/obl/notfound.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8997,'/obl/popups/popup_scripts.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8998,'/obl/popups/zone_details.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7646,'/ofr/infeasibility/carrierItem/jsp/ViewInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7647,'/ofr/infeasibility/dockEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7648,'/ofr/infeasibility/driverFacility/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7649,'/ofr/infeasibility/driverFacility/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7650,'/ofr/infeasibility/facilityEquipment/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7651,'/ofr/infeasibility/itemZone/jsp/AddInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7652,'/ofr/infeasibility/productClassCarrier/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7653,'/ofr/infeasibility/productClassProductClass/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7654,'/ofr/infeasibility/UnNumberUnNumber/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7655,'/ofr/invoice/shipper/jsp/InvoiceApprovalPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7656,'/ofr/invoice/shipper/jsp/InvoiceAuditTrailList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7657,'/ofr/invoice/shipper/jsp/InvoiceRejectPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7658,'/ofr/invoice/shipper/jsp/loadCustomerInvoiceDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7659,'/ofr/invoice/shipper/jsp/ProcessCustomerInvoiceTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7660,'/ofr/invoice/shipper/jsp/processInvoiceApprovePopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7661,'/ofr/invoice/shipper/jsp/processInvoiceRejectPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7662,'/ofr/invoice/shipper/jsp/TestRules.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7663,'/ofr/invoice/ui/EditInvoiceHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7664,'/ofr/invoice/ui/EditInvoiceWithHeaderOnly.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7665,'/ofr/invoice/ui/ViewAdjustment.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7666,'/ofr/invoice/ui/ViewHistory.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7667,'/ofr/invoice/ui/ViewInvoicedObjectList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7668,'/ofr/optimizationmgmt/consolidation/autoapproval/jsp/AutoApprovalProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7669,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/ZipListBox.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7670,'/ofr/optimizationmgmt/consolidation/enginespecific/con4/jsp/EngineSpecificRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7671,'/fm/trip/PCMilerDrivingDirections.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7672,'/fm/trip/SoftCheckOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7673,'/fm/trip/TripDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7674,'/fm/trip/TripDetailsViewOnly.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7675,'/ofr/claim/shipper/jsp/ProcessCargoClaimDetailsLinks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7676,'/ofr/claim/shipper/jsp/ProcessClaimShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7677,'/ofr/claim/shipper/jsp/processViewJournalEntries.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7678,'/ofr/consolidation/workspace/jsp/ViewAssignedStops.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7679,'/ofr/continuousmove/jsp/cmLegDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7680,'/ofr/continuousmove/jsp/processCmConfiguration.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7681,'/ofr/detention/jsp/DetentionContactInformation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7682,'/cbo/transactional/location/view/ItemLookup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7683,'/cbo/transactional/location/view/LocationLookup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7684,'/cbo/transactional/lpn/view/LPNListInboundMain.jsflps?ListType=LPN_INBOUND','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7685,'/cbo/transactional/purchaseorder/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7686,'/cbo/transactional/purchaseorder/view/POCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7687,'/ofr/admin/parameter/claims/jsp/ProcessClaimParameter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7688,'/ofr/admin/parameter/companyconsolidation/jsp/carrierByModeLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7689,'/ofr/admin/parameter/detention/jsp/EditCarrDetentionNotificationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7690,'/ofr/admin/parameter/distancetime/jsp/EditDistanceTimeParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7691,'/ofr/admin/parameter/doms/jsp/EditDOMParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7692,'/ofr/admin/parameter/fleet/jsp/EditFleetParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7693,'/ofr/archive/jsp/InfoOrderDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7694,'/ofr/booking/jsp/PODetailsPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7695,'/ofr/booking/jsp/processBookingAnalysisOrderVolume.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7696,'/ofr/booking/jsp/ViewPreviousOverrides.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7697,'/fm/dashboard/AlternateLoadList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7698,'/fm/dashboard/EquipChart.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7699,'/obl/rfp/capacity/TPCapacityAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8149,'/obl/master/workspace_sidenav_include.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8150,'/obl/no_cache.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8151,'/obl/popups/extend_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8152,'/obl/profile/transport_provider/air_mode_master.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8153,'/obl/profile/transport_provider/ltl_mode_master.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8154,'/obl/profile/view_tc_corporate_account.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8155,'/obl/profile/view_tp_corporate_profile.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8156,'/cbo/transactional/purchaseorder/view/POList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8157,'/cbo/transactional/rts/view/RTSCreateMain.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8158,'/cbo/transactional/shipment/view/ShipmentEditComments.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8159,'/ofr/detention/shipper/jsp/CommonInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8160,'/ofr/detention/shipper/jsp/ManageDetentionUnloadAllowanceTimes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8161,'/basedata/carrier/view/CarrierEditModeSLEquipment.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8162,'/basedata/carrierserv/driver/jsp/DriverSummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8163,'/basedata/carrierserv/driver/jsp/RaDriverSummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8190,'/basedata/carrierserv/jsp/RACarrierPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8191,'/uclEnableUserServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8192,'/cbo/transactional/infeasibility/modeProtectionLevel/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8193,'/cbo/transactional/infeasibility/productClassEquipment/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8194,'/cbo/classificationcode/ClassificationCodeList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8195,'/basedata/addressparam/jsp/dockHoursPopUpHolder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8196,'/basedata/servicelevel/view/ServiceLevelList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8197,'/basedata/staticschedule/jsp/StaticSchedule.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8198,'/basedata/wave/jsp/SaveAllWaves.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8199,'/basedata/wave/jsp/SaveWaveOption.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8200,'/basedata/xmlupload/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8201,'/basedata/zone/jsp/zoneList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8202,'/cbo/defaultburegion/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8203,'/cbo/item/ItemProfileDetailsCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8204,'/cbo/item/ItemSupplierXRefListCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10511,'/basedata/path/jsp/basePathProductClassForm.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8206,'/cbo/transactional/alerts/jsp/APProcessEditAlertDef.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8207,'/cbo/transactional/alerts/jsp/ProcessAcknowledgeAlerts.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8208,'/cbo/transactional/alerts/jsp/SalesOrderAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8209,'/cbo/transactional/asn/EditASNHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8210,'/cbo/transactional/asn/NewEditASNSummary.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8211,'/cbo/transactional/distributionorder/view/DOBulkCreatePickupDeliveryTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8212,'/cbo/transactional/distributionorder/view/DOCreateMain.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8213,'/cbo/transactional/distributionorder/view/DODetailsLPNList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8214,'/fm/infeasibility/driverTypeFacility/jsp/ViewInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8215,'/fm/popup/DriverPopup1.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8216,'/basedata/printer/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8217,'/basedata/reasoncode/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8218,'/basedata/referenceid/jsp/AdminLPNRule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8219,'/basedata/region/jsp/AddFacilitiesToRegion.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8220,'/basedata/region/jsp/RegionFacilityPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8221,'/basedata/region/view/RegionList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8222,'/obl/rfp/lane/shipper_singlepack_edit_bid.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10512,'/basedata/path/jsp/editPathSetTemplate.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10513,'/basedata/path/jsp/editPathSetTemplateRoutingPlan.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10514,'/basedata/path/jsp/editShippingParam.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10515,'/basedata/path/jsp/newPathSet.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10516,'/basedata/path/jsp/newPathSet_step2.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10517,'/basedata/path/jsp/newPathSet_step3.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10518,'/basedata/path/jsp/newPathSetInit.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10519,'/basedata/path/jsp/pathSetAddAlternatePath.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10520,'/basedata/path/jsp/pathSetAddBasePath.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10521,'/basedata/path/jsp/pathSetAddBasePathScripts.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10522,'/basedata/path/jsp/pathSetAddShippingParam.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10523,'/basedata/path/jsp/pathSetAdminPathForm.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10524,'/basedata/path/jsp/pathSetAlternatePath.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10525,'/basedata/path/jsp/pathSetAlternatePathForm.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10526,'/basedata/path/jsp/pathSetAlternatePathSetButtonSet.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10527,'/basedata/path/jsp/pathSetBasePath.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10528,'/basedata/path/jsp/pathSetButtonSet.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10529,'/basedata/path/jsp/pathSetCreateStep2.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10530,'/basedata/path/jsp/pathSetDate.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10531,'/basedata/path/jsp/pathSetDetails.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10532,'/basedata/path/jsp/pathSetDuplicate.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10533,'/basedata/path/jsp/pathSetDuplicate_step2.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10534,'/basedata/path/jsp/pathSetDuplicate_step3.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10535,'/basedata/path/jsp/pathSetEditBasePath.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10536,'/basedata/admin/tfpenaltygroup/jsp/FacilityLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10537,'/basedata/admin/util/jsp/ProcessShipperSpecificHierarchy.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9096,'/obl/ShipperSetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9097,'/scem/eventmgt/objectevent/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9098,'/scv/isf/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9099,'/doms/dom/admin/jsp/DOMDevTools.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9100,'/ofr/rsarea/jsp/shipperResourceAreaCRUDError.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9101,'/ofr/trackingmsg/jsp/NewTrackingMessages.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9102,'/te/optimizationmgmt/consolidation/parameter/jsp/ProcessParameterSet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9103,'/ofr/detention/carrier/jsp/CreateDetention.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9104,'/ofr/detention/carrier/jsp/OverrideCarrierDetentionSoftChecks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9105,'/ofr/ra/jsp/appointmentPlanningOY.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9106,'/cbo/transactional/distributionorder/view/DOEditHeaderTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9107,'/cbo/transactional/distributionorder/view/DOEditLinesTabPO.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9108,'/cbo/transactional/distributionorder/view/DOEditShippingTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9109,'/cbo/transactional/infeasibility/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9110,'/basedata/documentmanagement/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9111,'/basedata/documentmanagement/view/AddDocument.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9112,'/basedata/drivertype/jsp/DriverTypeListSheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9113,'/basedata/equipmentinstance/jsp/EquipmentInstanceSummaryPresentationViewOnly.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9114,'/basedata/equipmentinstance/jsp/EquipmentScheduleCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9115,'/basedata/inventorysegment/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9116,'/basedata/laboractivity/jsp/LaborActivityTabAddMode.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9117,'/basedata/loadConfig/jsp/LoadConfiguration.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9118,'/ofr/infeasibility/customerMode/jsp/AddInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9119,'/ofr/infeasibility/driverEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9120,'/ofr/infeasibility/driverEquipment/jsp/ProcessUpdateInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9121,'/ofr/infeasibility/equipmentEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9122,'/ofr/infeasibility/facilityEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9123,'/ofr/infeasibility/incotermIncoterm/jsp/UpdateInfeasibleIncoterm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9124,'/ofr/infeasibility/productClassCarrier/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9125,'/ofr/infeasibility/productClassEquipment/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9126,'/ofr/infeasibility/protectionEquipmentByFacility/jsp/ProcessAddFeasiblePair.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9127,'/ofr/infeasibility/protectionLevelProtectionLevel/jsp/InfeasibleProtectionLevelPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9128,'/ofr/infeasibility/UnNumberEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9129,'/ofr/invoice/ui/EditInvoice.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9130,'/ofr/invoice/ui/ViewInvoice.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9131,'/ofr/invoice/ui/ViewInvoiceManifestList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7700,'/obl/rfp/customrfpattribute/process_custom_lane_accessorial_attributes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7701,'/obl/rfp/effectiverate/edit_effective_rate.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7702,'/obl/rfp/effectiverate/popup.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7703,'/obl/rfp/file/download_file.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7704,'/obl/rfp/file/export_file.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7705,'/obl/rfp/file/update_attachment_directory.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7706,'/obl/rfp/lane/administer_lane_carrier.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7707,'/obl/rfp/lane/batch_activate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7708,'/obl/rfp/lane/process_shipper_save_lane_bid.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7709,'/obl/rfp/lane/shipper_edit_lane_bid.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7710,'/obl/rfp/lane/shipper_package_edit_bid.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7711,'/fm/debrief/TripSheet.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7712,'/fm/driver/AdjustHOSAuditTrail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7713,'/fm/driver/CheckInOutOverridesDWS.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7714,'/fm/driver/DspDriverActivityLogOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7715,'/fm/driver/LastKnownDriverLocationPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7716,'/fm/driver/SoftChecksOverridesNew.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7717,'/fm/driverkiosk/KioskTractorList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7718,'/fm/driverkiosk/logout.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7719,'/fm/debrief/DSPCollectionActivityColumns.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7720,'/ofr/optimizationmgmt/consolidation/jsp/ProcessConsolidationRun.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7721,'/ofr/optimizationmgmt/consolidation/parameter/jsp/DockDoorCapacity.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7722,'/cbo/transactional/shipment/view/ShipmentList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7723,'/cbo/uom/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7724,'/lps/cache/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7725,'/obl/go_to_url_page.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7726,'/ofr/facilitate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7727,'/ofr/help/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7728,'/ofr/rs/jsp/test/testCppGRSPostprocessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7729,'/ucl/admin/lps/UserGroupList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7730,'/uclDisableUserServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7731,'/uclEditCompanyParameterServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7732,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewAggregatorParams.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7733,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7734,'/ofr/ra/jsp/carrierChargesPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7735,'/ofr/ra/jsp/CreatesAppointmentOY.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7736,'/ofr/ra/jsp/TrackingMessageDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7737,'/ofr/rs/jsp/CarrierPFRedirectToShipmentList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7738,'/ofr/rs/jsp/editPartialShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7739,'/ofr/rs/jsp/processEditRoutePlanPreferences.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7740,'/ofr/rs/jsp/processShipperPFValuesFilter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7741,'/ofr/rs/jsp/selectorAnalysisPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7742,'/ofr/rs/jsp/shipmentCMDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7743,'/ofr/rs/jsp/shipmentLaneDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7744,'/ofr/ra/jsp/LoadTenderFilter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7745,'/ofr/ra/jsp/ProcessBookingResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7746,'/ofr/ra/jsp/ProcessClaimDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7747,'/ofr/ra/jsp/ProcessMultipleTrackingMessages.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7748,'/ofr/ra/jsp/ProcessTenderResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7749,'/ofr/ra/jsp/RATransClaimDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7750,'/ofr/ra/jsp/TrackingComment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7751,'/ofr/ra/jsp/WebOfferResponses.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7752,'/ofr/ra/jsp/WebOffers.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7753,'/basedata/cmreport/jsp/Date.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7754,'/basedata/cmreport/jsp/saveCMReport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7755,'/obl/bid/lane_bid/carrier_lane.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7756,'/obl/common/jsp/globalTop.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7757,'/obl/common/jsp/page_navigation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7758,'/obl/countryLookUp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7759,'/obl/errors/unauthorized_operation_error.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7760,'/obl/header/top_block.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7761,'/obl/help/view_help_top.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7762,'/obl/OptiBid.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7763,'/obl/popups/calendarWithTime.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7764,'/obl/popups/in_progress_message.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7765,'/obl/popups/popupHelpLink.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7766,'/obl/profile/view_tc_corporate_profile.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7767,'/obl/profile/view_tp_corporate_general.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7768,'/obl/rfp/basedata/edit_accessorials.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7769,'/cbo/transactional/purchaseorder/view/POLineDetailEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7770,'/cbo/transactional/rts/createAggregatedRTSAcknowledgement.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7771,'/cbo/transactional/rts/view/createAggregatedRTSAcknowledgement.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7772,'/cbo/transactional/rts/view/RTSCreateMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7773,'/cbo/transactional/shipment/view/EditBillTo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7774,'/cbo/transactional/shipment/view/EditComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7775,'/cbo/transactional/shipment/view/editQuickStopsAndOrders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7776,'/cbo/transactional/shipment/view/EditRecurrencePattern.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7777,'/cbo/transactional/shipment/view/EditStopDTTM.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7778,'/cbo/transactional/shipment/view/ShipmentCreate.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7779,'/cbo/transactional/shipment/view/ShipmentCreateGeneral.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7780,'/ofr/detention/shipper/jsp/DetentionAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7781,'/ofr/detention/shipper/jsp/DetentionImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7782,'/basedata/businessgroup/jsp/AdministerBusinessGroup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7783,'/basedata/carrier/view/CarrierCreateMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7784,'/basedata/carrierserv/driver/jsp/DriverCalendarEdit.jsp?Operation=create&CalID=0','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7785,'/basedata/carrierserv/driver/jsp/DriverHolidaySummary.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10807,'/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/facilityChanged','APPT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7811,'/bpe/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7831,'/basedata/facility/jsp/crossDockWindowsList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7832,'/basedata/facility/jsp/DropHookParameterPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7833,'/cbo/transactional/infeasibility/equipmentEquipment/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7834,'/cbo/transactional/infeasibility/facilityEquipment/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7835,'/cbo/transactional/infeasibility/protectionLevelFacility/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7836,'/basedata/rating/currencyconversion/jsp/EditExchangeRate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7837,'/cbo/transactional/asn/CreateASNFromPO.jsflps?isShipping=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7838,'/basedata/admin/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7839,'/basedata/skuprice/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7840,'/basedata/skusubstitution/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7841,'/basedata/zone/jsp/editZone.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7842,'/cbo/admin/criticalchange/jsp/SetupPOCriticalChange.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7843,'/cbo/admin/parameter/general/jsp/ViewGeneralParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7844,'/cbo/item/CreateItemProfile.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7845,'/cbo/transactional/admin/parameter/jsp/EditSOParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7846,'/cbo/transactional/alerts/jsp/APViewAlertDef.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7847,'/cbo/transactional/asn/AddLPNToASN.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7848,'/cbo/transactional/distributionorder/splitter/CBO-DOSplitter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7849,'/cbo/transactional/distributionorder/view/DOBulkDetailsPickupDelivery.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7850,'/cbo/transactional/distributionorder/view/DOCreateLinesTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7851,'/cbo/transactional/distributionorder/view/DOCreateLinesTabPO.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7852,'/cbo/transactional/distributionorder/view/DOCreateMain.jsflps?isBulkTemplate=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7853,'/fm/lookup/ZipCodeLookUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7854,'/fm/shipmentloaddetail/ShipmentLoadDetailColumns.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7855,'/fm/trip/AltDestinationOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7856,'/fm/trip/DispatchShipmentStopDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7857,'/fm/trip/DispatchWorkspaceOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7858,'/fm/trip/DWTripList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7859,'/basedata/reasoncode/jsp/ViewAllReasonCodes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7860,'/obl/rfp/lane/sourcing/processMoveVolume.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7861,'/obl/rfp/message/message_summary_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7862,'/obl/rfp/rfp/Confirm_the_logout_action.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7863,'/obl/rfp/rfp/delete_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7864,'/obl/rfp/rfp/edit_special_reqs.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7865,'/obl/rfp/rfp/essentials_initialization.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7866,'/obl/rfp/rfp/popup_award_comment_view.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7867,'/obl/rfp/rfp/popup_import_rebid_lanes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7868,'/obl/rfp/rfp/post_rfp_resp_popup.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7869,'/obl/rfp/rfp/processUpdateAwardDate.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7870,'/obl/rfp/rfp/process_award_configuration.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7871,'/obl/rfp/rfp/process_extend_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7872,'/obl/rfp/rfp/process_update_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7873,'/obl/rfp/rfp/view_rfp_information.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7874,'/obl/scenario/popup_import_additional_lanes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7875,'/obl/scenario/process_scenario_carrier.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7876,'/obl/scenario/process_scenario_package.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7877,'/obl/scenario/scenario_abort_log_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7878,'/obl/scenario/scenario_carrier_report.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7879,'/obl/scenario/scenario_facility_carriers_OB.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7880,'/obl/scenario/scenario_package_summary_print.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7881,'/obl/scenario/scenario_report.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7882,'/obl/scenario/scenario_winning_carrier_lane.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7883,'/obl/scenario/view_additional_lanes.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7884,'/obl/workspace/manage_workspace_inbox.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7885,'/obl/WorkSpaceBid.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7886,'/obl/xml/download/download_override_xml_file.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7887,'/obl/xml/download/download_text_file.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7888,'/scem/eventmgt/event/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7889,'/scem/objectschedulegroup/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7890,'/scv/inventory/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7891,'/obl/scenario/scenario_facility_rules.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7892,'/obl/scenario/scenario_lane_summary_print.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7893,'/obl/scenario/scenario_name_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7894,'/obl/scenario/scenario_package_summary.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7895,'/obl/scenario/scenario_report_print.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7896,'/obl/workspace/manager_workspace_content.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7897,'/obl/workspace/manager_workspace_header.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7898,'/obl/xml/upload/upload_xml_file.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7899,'/scem/eventmgt/notification/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7900,'/scem/eventmgt/rule/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7901,'/fieldvision/license/LicenseKeyEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10472,'/basedata/admin/parameter/distancetime/jsp/EditDistanceTimeParameters.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10473,'/basedata/admin/parameter/distancetime/jsp/lookUpDistanceTimeParameters.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10474,'/basedata/admin/parameter/distancetime/jsp/ProcessDistanceTimeParameters.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10475,'/basedata/admin/parameter/distancetime/jsp/ProcessDistanceTimeParametersPopup.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10476,'/basedata/admin/parameter/distancetime/jsp/ViewDistanceTimeParameters.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10477,'/basedata/admin/parameter/distancetime/jsp/ViewDistanceTimeParametersPopup.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10478,'/basedata/admin/parameter/distancetime/jsp/ViewValidationParameters.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10479,'/basedata/archive/jsp/rating/ratingLaneDetails.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10480,'/basedata/archive/jsp/rating/ratingLaneList.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10481,'/basedata/archive/jsp/routing/routingLaneDetails.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10482,'/basedata/archive/jsp/routing/routingLaneList.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10483,'/basedata/distancetime/jsp/DistanceTimeQuery.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10484,'/basedata/distancetime/jsp/DistanceTimeResp.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10485,'/basedata/distancetime/jsp/DTBulkDelete.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10486,'/basedata/distancetime/jsp/DTCacheRefresh.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10487,'/basedata/distancetime/jsp/DTCacheRefreshResp.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10488,'/basedata/distancetime/jsp/DTOverrideDetails.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10489,'/basedata/distancetime/jsp/DTOverrideList.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10490,'/basedata/distancetime/jsp/DTOverrideWindow.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10491,'/basedata/distancetime/jsp/DTValidateFacility.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10492,'/basedata/distancetime/jsp/DTValidateFacilityAndCache.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10493,'/basedata/distancetime/jsp/DTValidateFacilityResp.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10494,'/basedata/distancetime/jsp/processDTOverride.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10495,'/basedata/distancetime/jsp/processDTOverrideStateTax.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10496,'/basedata/distancetime/jsp/processDTWindow.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10497,'/basedata/distancetime/jsp/RandTest.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10498,'/basedata/fedexutil/jsp/FedExModerate.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10499,'/basedata/fedexutil/jsp/ProcessFedExModerate.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10500,'/basedata/imports/jsp/deleteLckFiles.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10501,'/basedata/imports/jsp/ProcessXMLCommManagerImport.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10502,'/basedata/imports/jsp/uploadPEFile.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10503,'/basedata/imports/jsp/XMLCommManagerImport.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10504,'/basedata/lookup/jsp/shipViaIdLookup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10505,'/basedata/lookup/jsp/tariffCarrierZoneLookup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10506,'/basedata/lookup/jsp/tariffCarrierZoneLookup1.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10507,'/basedata/path/jsp/alternatePathDayOfWeekForm.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10508,'/basedata/path/jsp/alternatePathProductClassForm.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10509,'/basedata/path/jsp/BaselinePathSetDetails.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10510,'/basedata/path/jsp/basePathDayOfWeekForm.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7907,'/basedata/payeeserv/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7908,'/basedata/payeeserv/jsp/PayeeContactInfoSummaryPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7909,'/doms/dom/tools/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7910,'/ofr/rs/jsp/vendorPFDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7911,'/ofr/rsarea/jsp/RSAreaFacilityDelete.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7912,'/ofr/rsarea/jsp/shipperResourceAreaCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7913,'/te/admin/parameter/jsp/EditParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7914,'/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationParametersInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7915,'/basedata/region/view/ResourceRegionList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7916,'/basedata/region/plannerregion/jsp/EditPlannerRegion.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7917,'/cbo/transactional/distributionorder/view/DODetailsPickUpDelivery.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7918,'/cbo/transactional/distributionorder/view/DOEditMisc.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7919,'/cbo/transactional/distributionorder/view/DOLineDetailsDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7920,'/basedata/customerserv/view/CustomerCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7921,'/basedata/equipmentinstance/jsp/EquipmentDomicileCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7922,'/basedata/facility/jsp/FacilityDock.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7923,'/basedata/laboractivity/jsp/LaborActivitiesMainView.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7924,'/ofr/detention/vendor/jsp/VendorDetentionDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7925,'/ofr/infeasibility/customerMode/jsp/ViewInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7926,'/ofr/infeasibility/driverEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7927,'/ofr/infeasibility/driverEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7928,'/ofr/infeasibility/driverTypeEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7929,'/ofr/infeasibility/driverTypeFacility/jsp/ViewInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7930,'/ofr/infeasibility/equipmentEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7931,'/ofr/infeasibility/facilityEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7932,'/ofr/infeasibility/facilityEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7933,'/ofr/infeasibility/itemCnt/jsp/AddInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7934,'/ofr/infeasibility/jsp/UpdateInfeasibleEquipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7935,'/ofr/infeasibility/productClassCarrier/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7936,'/ofr/infeasibility/productClassProductClass/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7937,'/ofr/infeasibility/UnNumberUnNumber/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7938,'/ofr/infeasibility/UnNumberUnNumber/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7939,'/ofr/infeasibility/UnNumberUnNumber/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7940,'/ofr/infeasibility/waveWave/jsp/UpdateInfeasibleWaveWave.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7941,'/ofr/invoice/shipper/jsp/CurrencyOptions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7942,'/ofr/invoice/shipper/jsp/CustomerInvoicePaymentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7943,'/ofr/invoice/shipper/jsp/InvoiceAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7944,'/ofr/invoice/shipper/jsp/InvoiceClaimRequestPopUp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7945,'/ofr/invoice/shipper/jsp/ProcessAcceptRejectCustomerInvoiceList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7946,'/ofr/invoice/shipper/jsp/viewAllInvoiceDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7947,'/ofr/invoice/ui/EditOriginDestination.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7948,'/ofr/invoice/ui/ViewCustomAttributes.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7949,'/ofr/invoice/ui/ViewInvoiceHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7950,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/ConsolidationRegionStateUpdate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7951,'/ofr/optimizationmgmt/consolidation/enginespecific/ofib/jsp/EngineSpecificRunTemplateDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7952,'/ofr/optimizationmgmt/consolidation/enginespecific/ofob/jsp/EngineSpecificRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7953,'/fm/trip/ManualSegOverRide.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7954,'/fm/trip/TripDetailsOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7955,'/fm/trip/TripStopsDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7956,'/ofr/claim/shipper/jsp/ClaimAuditInfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7957,'/ofr/claim/shipper/jsp/ClearJournalEntryErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7958,'/ofr/claim/shipper/jsp/ProcessCheckMultipleClaims.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7959,'/ofr/claim/shipper/jsp/ProcessNewCargoClaim.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7960,'/ofr/claim/shipper/jsp/TransportationDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7961,'/ofr/consolidation/workspace/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7962,'/ofr/consolidation/workspace/jsp/DistanceAnalyser.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7963,'/ofr/consolidation/workspace/jsp/ViewAssignedOrders.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7964,'/ofr/consolidation/workspace/jsp/ViewConsolidationShipments.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7965,'/ofr/consolidation/workspace/jsp/ViewUnAssignedOrders.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7966,'/ofr/detention/vendor/jsp/ProcessVendorDetention.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7967,'/cbo/transactional/infeasibility/protectionLevelFacility/jsp/InfeasibleProtectionLevelFacilityPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7968,'/cbo/transactional/location/view/AssignSkuDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7969,'/cbo/transactional/location/view/LocationGrpList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7970,'/cbo/transactional/location/view/ReserveLocationHdrDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7971,'/cbo/transactional/purchaseorder/view/CreatePOHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7972,'/cbo/transactional/purchaseorder/view/POBPCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7973,'/cbo/transactional/purchaseorder/view/PODetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7974,'/cbo/transactional/purchaseorder/view/PODetailEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7975,'/ofr/admin/parameter/companyconsolidation/jsp/modeTpPopUpHolder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7976,'/ofr/admin/parameter/companyconsolidation/jsp/ViewModeTPPairs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7977,'/ofr/admin/parameter/detention/jsp/EditDetentionParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7978,'/ofr/admin/parameter/detention/jsp/ViewSuppDetentionNotificationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7979,'/ofr/admin/parameter/doms/jsp/ViewDOMParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7980,'/ofr/admin/parameter/invoice/jsp/EditInvoiceParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7981,'/ofr/admin/parameter/jsp/EditValidationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7982,'/ofr/admin/parameter/jsp/ViewParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7983,'/ofr/archive/jsp/OrderDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7984,'/ofr/archive/jsp/StopDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7985,'/ofr/booking/jsp/BookingFeasibleResource.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7986,'/ofr/booking/jsp/overrideBookingShipmentSoftChecksForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7987,'/ofr/booking/jsp/overrideDetailSoftChecksForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7988,'/ofr/booking/jsp/overrideSoftChecksForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7989,'/ofr/booking/jsp/ProcessAddShipmentToBookingPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7990,'/bpe/CSEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7991,'/fm/bid/AddResourceToBid.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7992,'/fm/bid/TripBidDetailsOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7993,'/fm/dashboard/MapPopupList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7994,'/obl/rfp/customdownload/administer_download_colorder.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7995,'/obl/rfp/customdownload/process_reset_custom_rfp_download.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7996,'/obl/rfp/customrfpattribute/custom_attribute_lookup.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7997,'/obl/rfp/customrfpattribute/edit_custom_lanebid_attribute.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7998,'/obl/rfp/effectiverate/process_edit_effective_rate.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 7999,'/obl/rfp/lane/administer_lane_equipment_type.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8000,'/obl/rfp/lane/process_shipper_decline_lane_bid.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8001,'/fieldvision/license/LicenseKeyAdd.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8003,'/basedata/mode/view/ModeDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8005,'/basedata/mode/view/ViewAllModeList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8006,'/basedata/payee/PayeeList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8007,'/basedata/customattribute/jsp/NewCustomAttribute.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8008,'/ofr/rs/jsp/VVPFDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8009,'/ofr/rsarea/jsp/RSAreaFacilityPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8010,'/ofr/trackingmsg/jsp/ProcessCounterMessage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8011,'/ofr/trackingmsg/jsp/viewCounteredTrackingMessage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8012,'/te/infeasibility/servicelevelfacility/jsp/AddInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8013,'/te/optimizationmgmt/consolidation/parameter/jsp/EditPlanningEngineParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8014,'/te/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8015,'/ofr/claim/carrier/jsp/AddTransClaimComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8016,'/ofr/claim/carrier/jsp/CarrierClaimParameter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8017,'/cbo/transactional/distributionorder/view/DODetailsOrderLines.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8018,'/cbo/transactional/distributionorder/view/DODummyForward.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8019,'/cbo/transactional/distributionorder/view/DOLineDetailsAuditTrail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8020,'/cbo/transactional/distributionorder/view/DOLineDetailsOrderReferences.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8021,'/cbo/transactional/distributionorder/view/DOLineEditWaveAllocTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8022,'/basedata/customerserv/view/CustomerList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8023,'/basedata/distributionlist/DistributionList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8024,'/basedata/equipmentinstance/jsp/EquipmentInstanceLocation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8025,'/basedata/equipmentinstance/jsp/RaEquipmentInstanceSummaryPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8026,'/basedata/mode/jsp/modeSizeLimitList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8027,'/ofr/infeasibility/carrierItem/jsp/AddInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8028,'/ofr/infeasibility/dockProductClass/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8029,'/ofr/infeasibility/driverTypeEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8030,'/ofr/infeasibility/equipmentEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8031,'/ofr/infeasibility/facilityTP/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8032,'/ofr/infeasibility/itemCnt/jsp/ViewInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8033,'/ofr/infeasibility/itemZone/jsp/ViewInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8034,'/ofr/infeasibility/jsp/relatedCompanyId.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8035,'/ofr/infeasibility/productClassCarrier/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8036,'/ofr/infeasibility/protectionEquipmentByFacility/jsp/ViewFeasiblePairs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8037,'/ofr/infeasibility/protectionLevelFacility/jsp/InfeasibleProtectionLevelFacilityPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8038,'/ofr/infeasibility/UnNumberEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8039,'/ofr/invoice/shipper/jsp/autoCreateInvoice.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8040,'/ofr/invoice/shipper/jsp/InvoiceAccountCodingPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8041,'/ofr/invoice/shipper/jsp/ProcessAcknowledgeAlerts.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8042,'/ofr/invoice/shipper/jsp/ProcessCustomerInvoiceCRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8043,'/ofr/invoice/ui/EditCustomAttributes.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8044,'/ofr/invoice/ui/EditInvoicedObjectCollections.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8045,'/ofr/invoice/ui/ViewHeaderConditionalFields.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8046,'/ofr/invoice/ui/ViewInvoicedObjectCollections.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8047,'/ofr/optimizationmgmt/consolidation/enginespecific/hdrp/jsp/EngineSpecificRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8048,'/ofr/optimizationmgmt/consolidation/enginespecific/ofib/jsp/EngineSpecificRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8049,'/ofr/optimizationmgmt/consolidation/enginespecific/ofob/jsp/EngineSpecificRunTemplateDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8050,'/ofr/optimizationmgmt/consolidation/enginespecific/pflt/jsp/EngineSpecificRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8051,'/ofr/optimizationmgmt/consolidation/enginespecific/splt/jsp/EngineSpecificRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8052,'/fm/trip/ProcessingSummary.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8053,'/fm/trip/StateMilesForTripDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8054,'/ofr/claim/jsp/SettleClaims.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8055,'/ofr/claim/shipper/jsp/CheckMultipleClaims.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8056,'/ofr/claim/shipper/jsp/ClaimWarningList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8057,'/ofr/claim/shipper/jsp/POQueryProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8058,'/ofr/claim/shipper/jsp/SettleAndApplyCheck.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8059,'/ofr/claim/shipper/jsp/ViewJournalEntries.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8060,'/ofr/consolidation/workspace/jsp/ViewConsolidationStatisticsSummary.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8061,'/ofr/consolidation/workspace/jsp/ViewConsolidationWorkspace.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8062,'/ofr/continuousmove/jsp/cmTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8063,'/ofr/criticalchange/jsp/NewSetupBookingCriticalChange.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8064,'/ofr/criticalchange/jsp/TestTarget.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8065,'/ofr/detention/jsp/CarrierDetentionComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8066,'/ofr/detention/shipper/jsp/AppointmentAndUserInformation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8067,'/cbo/transactional/location/view/CaseLocnFitList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8068,'/cbo/transactional/location/view/ReserveLocationList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8069,'/cbo/transactional/location/view/ReserveLocationSCError.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8070,'/cbo/transactional/lookup/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8071,'/cbo/transactional/lpn/view/AddSingleLPNtoPallet.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8072,'/cbo/transactional/lpn/view/EditLPNContents.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8073,'/cbo/transactional/purchaseorder/view/PODetailCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8074,'/ofr/admin/parameter/claims/jsp/LoadClaimType.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8075,'/ofr/admin/parameter/companyconsolidation/jsp/ProcessDeleteModeTp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8076,'/ofr/admin/parameter/detention/jsp/EditSuppDetentionNotificationParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8077,'/ofr/admin/parameter/distancetime/jsp/lookUpDistanceTimeParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8078,'/ofr/admin/parameter/distancetime/jsp/ProcessDistanceTimeParametersPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8079,'/ofr/admin/parameter/doms/jsp/EditDOMReappParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8080,'/ofr/admin/parameter/doms/jsp/ViewDOMReappParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8081,'/ofr/archive/jsp/OrderList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8082,'/ofr/archive/jsp/TrackingMessageList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8083,'/ofr/booking/jsp/BookingSummaryTable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8084,'/ofr/booking/jsp/PreCarriage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8085,'/ofr/booking/jsp/ViewBookingSaveConflict.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8086,'/fm/dashboard/dashboardEquipListPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8087,'/obl/rfp/customattribute/edit_custom_attribute.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8088,'/obl/rfp/customrfpattribute/edit_custom_rfp_attribute.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8089,'/obl/rfp/lane/batch_cancel.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8090,'/obl/rfp/lane/batch_invite.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8091,'/obl/rfp/lane/capacity_edit_bid.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8092,'/obl/rfp/lane/lane_errors.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8093,'/obl/rfp/lane/shipper_package_edit_set_variable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8094,'/obl/rfp/lane/shipper_package_set_variable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8095,'/fm/driver/AdjustHOSCounters.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8096,'/fm/driver/DispatchDriverDetailsOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8097,'/fm/driverkiosk/DriverCheckIn.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8098,'/fm/debrief/DSPDriverActivityColumns.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8099,'/fm/debrief/DSPDriverActivityForTripSheet.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8100,'/fm/debrief/EndTripActivityLog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8101,'/ofr/optimizationmgmt/consolidation/jsp/PrefixedShipmentPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8102,'/ofr/optimizationmgmt/consolidation/jsp/ProcessGetOrdersForConsolidationRunTemplateTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8103,'/ofr/optimizationmgmt/consolidation/jsp/ProcessRunRejection.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8104,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditAddToExistingShipmentParamsProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8105,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditAggregatorParamsProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8106,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditCPLEXParametersPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8107,'/cbo/transactional/shipment/view/ShipmentEditHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8108,'/cbo/transactional/shipment/view/ShipmentEditOrders.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8109,'/cbo/uom/CreateUOM.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8110,'/cbo/uom/UOMList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8111,'/lcom/admin/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8112,'/obl/filter/filter_bar.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8113,'/ofr/distancetime/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8114,'/ofr/filter/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8115,'/te/filter/jsp/ApplyDefault.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8116,'/ucl/admin/jsp/AdministerApplications.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8117,'/ucl/admin/jsp/ViewRolePermissions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8118,'/ucl/common/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8119,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewAddToExistingShipmentParams.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8120,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewAggregatorParamsProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8121,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewPlanningEngineParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8122,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewTMSLiteParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8123,'/ofr/ra/jsp/FacilityIdLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8124,'/ofr/rs/jsp/AddCarrierTenderOverride.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8125,'/ofr/rs/jsp/addTP.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8126,'/ofr/rs/jsp/ProcessCarrierSpecificValuesCRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8127,'/ofr/rs/jsp/processConfirmAddTP.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8128,'/ofr/rs/jsp/processStaticPerformanceFactors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8129,'/ofr/rs/jsp/shipperConfigManCycleHolder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8130,'/ofr/rs/jsp/shipperConfigModeAction.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8131,'/ofr/rs/jsp/shipperManualStartCycleList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8132,'/ofr/ra/jsp/CommodityList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8133,'/ofr/ra/jsp/ProcessNewTrackingMessage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8134,'/ofr/ra/jsp/RAClaimList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8135,'/ofr/ra/jsp/RAClaimReportProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8136,'/ofr/ra/jsp/ShipmentAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8137,'/ofr/ra/jsp/WebBookings.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8138,'/ofr/ra/jsp/WebTenders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8139,'/ilm/appointment/jsf/scheduleAppointmentCarrier.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8140,'/basedata/cmreport/jsp/carrierDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8141,'/basedata/cmreport/jsp/EditRunTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8142,'/obl/bid/lane_bid/package_create_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8143,'/obl/bid/lane_bid/shipper_package.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8144,'/obl/common/jsp/leftNav.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8145,'/obl/common/jsp/menuList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8146,'/obl/company/location/administer_obl_location_process.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8147,'/obl/errors/invalid_session.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8148,'/obl/errors/userError.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8698,'/fm/bid/AddShipmentToBid.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8699,'/fm/dashboard/dashboardDriverListPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8700,'/fm/dashboard/dashboardSegmentListPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8701,'/fm/dashboard/FleetUtilizChart.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8702,'/fm/debrief/DebriefOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8703,'/obl/rfp/file/add_attachment_file.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8704,'/obl/rfp/lane/cancel_lane.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8705,'/obl/rfp/lane/decide_view_or_admin_lane_popup.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8706,'/obl/rfp/lane/shipper_package_method.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8707,'/fm/driver/CheckInOutOverridesLKL.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8708,'/fm/driver/DspDriverActivityHistoryOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8709,'/fm/driver/DSPDriverDetailsTripListButtonPanel.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8710,'/fm/driverkiosk/KioskBidTripList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8711,'/fm/driverkiosk/KioskDriverList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8712,'/fm/equipment/DispatchTractorList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8713,'/fm/equipment/DispatchTrailerList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8714,'/fm/debrief/DSPCollectionActivity.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8715,'/fm/debrief/DSPDriverActivity.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8716,'/ofr/optimizationmgmt/consolidation/enginespecific/splt/jsp/ProcessEngineSpecificRunTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8717,'/ofr/optimizationmgmt/consolidation/facilityschedule/jsp/EditFacilityToFacilitySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8718,'/ofr/optimizationmgmt/consolidation/jsp/PurgeRun.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8719,'/ofr/optimizationmgmt/consolidation/jsp/RejectConsRun.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8720,'/cbo/transactional/shipment/view/ShipmentList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8721,'/obl/devtools/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8722,'/ofr/rs/jsp/test/pfRefreshScheduledEvent.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8723,'/ucl/admin/lps/RegionList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8724,'/uclAppInstanceServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8725,'/uclControllerServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8726,'/uclCreateUserServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8727,'/ofr/rs/jsp/processAddTP.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8728,'/ofr/rs/jsp/processBrokerResourceConfiguration.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8729,'/ofr/rs/jsp/processFindRoutePlanOptions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8730,'/ofr/rs/jsp/shipmentInvitedDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8731,'/ofr/rs/jsp/shipperConfigCycleHolder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8732,'/ofr/rs/jsp/shipperConfigManCycle.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8733,'/ofr/rs/jsp/shipperConfigManCyclePopupHolder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8734,'/ofr/ra/jsp/DeclineClaim.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8735,'/ofr/ra/jsp/LoadFilter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8736,'/ofr/ra/jsp/NewTrackingMessage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8737,'/ofr/ra/jsp/ProcessNewInternalNote.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8738,'/ofr/ra/jsp/ProcessOfferResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8739,'/ofr/ra/jsp/RAClaimWarningList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8740,'/ofr/ra/jsp/rateDetailsPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8741,'/obl/bid/lane_bid/carrier_lane_admin_button.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8742,'/obl/bid/lane_bid/carrier_lane_set_variable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8743,'/obl/bid/lane_bid/package_carrier_create_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8744,'/obl/bid/lane_bid/shipper_package_set_variable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8745,'/obl/cancel.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8746,'/obl/common/jsp/globalHeader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8747,'/obl/errors/serverError.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8748,'/obl/header/popup_top.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8749,'/obl/init_administer_distribution_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8750,'/obl/popups/cancel_rfp_popup.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8751,'/obl/profile/master.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8752,'/obl/profile/view_tc_corporate_account_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8753,'/obl/profile/view_tp_corporate_account_and_users.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8754,'/obl/profile/view_tp_corporate_capabilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8755,'/obl/rfp/basedata/create_accessorials.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8756,'/cbo/transactional/rts/view/RTSCreateGeneral.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8757,'/cbo/transactional/rts/view/RTSEditLinesTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8758,'/cbo/transactional/shipment/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8759,'/cbo/transactional/shipment/ShipmentEditFullOrderList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8760,'/cbo/transactional/shipment/view/EditInformationalOrder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8761,'/cbo/transactional/shipment/view/QuickCreateShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8762,'/cbo/transactional/shipment/view/ReviewTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8763,'/cbo/transactional/shipment/view/ShipmentCreateStops.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8764,'/ofr/detention/shipper/jsp/ApproveNotApprovePopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8765,'/ofr/detention/shipper/jsp/DetentionDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8766,'/ofr/detention/shipper/jsp/ManageDetentionRates.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8767,'/basedata/businessgroup/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8768,'/basedata/carrier/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8769,'/basedata/carrierserv/driver/jsp/DriverActivitySummarySheet.jsp?driverActivityOperation=create','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8770,'/basedata/carrierserv/driver/jsp/DriverSummarySheet.jsp?driverOperation=create','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8771,'/basedata/carrierserv/driver/jsp/DrivingRulesDetails.jsp?isNew=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8772,'/basedata/carrierserv/driver/jsp/DrivingRulesList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10806,'/appointment/ui/jsf/appointmentList.xhtml','APPT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10449,'/basedata/rating/ratinglane/jsp/laneList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10450,'/basedata/rating/tariff/jsp/createTariffZone.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10451,'/basedata/rating/tariff/jsp/tariffZoneList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10452,'/basedata/routingguide/jsp/createLane.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10453,'/basedata/routingguide/jsp/laneList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10454,'/basedata/rule/jsp/insert_rule_details.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10455,'/basedata/sailingLane/jsp/ExportLanes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10456,'/basedata/sailingLane/jsp/ProcessExportLanes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10457,'/basedata/sailingLane/jsp/validationSummary.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10458,'/basedata/staticroute/jsp/ruleslist.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10459,'/basedata/timefeasibility/param/jsp/EditMaxTFWindowParam.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10460,'/basedata/timefeasibility/param/jsp/ProcessMaxTFWindowParam.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10461,'/basedata/timefeasibility/param/jsp/ViewMaxTFWindowParam.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10462,'/basedata/tpparam/jsp/EditCarrierParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10463,'/basedata/tpparam/jsp/editFuelIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10464,'/basedata/tpparam/jsp/FuelRate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10465,'/basedata/admin/tfpenaltygroup/ui/PenaltyGroupDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10466,'/basedata/admin/tfpenaltygroup/ui/PenaltyGroupList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10467,'/basedata/admin/tfpenaltygroup/ui/PenaltyGroupMembersEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10468,'/basedata/admin/tfpenaltygroup/ui/PenaltyGroupMembersList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10469,'/basedata/deliveryareasurcharge/jsp/DeliveryAreaSurchargeList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10470,'/basedata/deliveryareasurcharge/jsp/EditDeliveryAreaSurcharge.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10471,'/basedata/deliveryareasurcharge/jsp/NewDeliveryAreaSurcharge.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8809,'/cbo/transactional/infeasibility/protectionLevelFacilityPair/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8810,'/basedata/currencylist/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8811,'/basedata/country/jsp/ViewCountryCreditLimit.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8812,'/basedata/location/LocationEditMain.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8813,'/basedata/addressparam/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8814,'/basedata/sourcing/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8815,'/basedata/unnumberserv/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8816,'/basedata/wave/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8817,'/basedata/wave/jsp/ViewAllWaveOptions.jsp?operation=view','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8818,'/basedata/zonefacasso/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8819,'/basedataDataLoaderServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8820,'/cbo/containerType/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8821,'/cbo/country/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8822,'/cbo/country/view/CountryEnquiryList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8823,'/cbo/item/CreateItem.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8824,'/cbo/item/CreateItemMain.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8825,'/cbo/item/CreateItemMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8826,'/cbo/item/ItemFacilityList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8827,'/cbo/item/ItemSizeCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8828,'/cbo/item/ItemSourcesCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8829,'/cbo/item/ItemSourcesEditView.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 8830,'/cbo/nextupnbr/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9132,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/ConsolidationRegionCRUDError.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9133,'/ofr/optimizationmgmt/consolidation/enginespecific/ofob/jsp/ViewOptiflowOrderErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9134,'/fm/trip/SoftCheckValidations.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9135,'/ofr/claim/customrule/jsp/CustomRuleListAndFilter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9136,'/ofr/claim/shipper/jsp/ClaimAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9137,'/ofr/claim/shipper/jsp/loadClaimCheckDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9138,'/ofr/claim/shipper/jsp/POQuery.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9139,'/ofr/claim/shipper/jsp/ProcessEditCargoClaim.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9140,'/ofr/claim/shipper/jsp/SubmitModeManipulation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9141,'/ofr/consolidation/jsp/SplitOrderPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9142,'/ofr/criticalchange/jsp/bookingUpdatePopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9143,'/ofr/criticalchange/jsp/updateBookingCriticalChange.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9144,'/ofr/detention/jsp/DetentionDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9145,'/ofr/admin/parameter/claims/jsp/LoadCategoryDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9146,'/ofr/detention/shipper/jsp/ViewAllDetentionsOnShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9147,'/cbo/transactional/infeasibility/facilityTP/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9148,'/cbo/transactional/infeasibility/UnNumberUnNumber/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9149,'/cbo/transactional/location/view/PickLocationHdrDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9150,'/cbo/transactional/location/view/ReserveLocationList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9151,'/cbo/transactional/location/view/WorkAreaMasterList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9152,'/cbo/transactional/lpn/view/EditLPNHeaderInbound.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9153,'/cbo/transactional/purchaseorder/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9154,'/cbo/transactional/purchaseorder/view/CreateCustomerOrder.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9155,'/cbo/transactional/purchaseorder/view/CreateRetailOrder.jsflps?channelType=10','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9156,'/cbo/transactional/purchaseorder/view/EditCustomerOrder.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9157,'/ofr/admin/parameter/companyconsolidation/jsp/EditConsolidationParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9158,'/ofr/admin/parameter/companyconsolidation/jsp/ProcessModeTp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9159,'/ofr/admin/parameter/detention/jsp/EditDetentionParameter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9160,'/ofr/admin/parameter/doms/jsp/EditDOMParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9161,'/ofr/admin/parameter/fleet/jsp/ViewFleetParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9162,'/ofr/admin/parameter/general/jsp/ViewGeneralParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9163,'/ofr/admin/parameter/invoice/jsp/EditInvoiceParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9164,'/ofr/admin/parameter/jsp/EditMaxDistance.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9165,'/ofr/admin/parameter/jsp/EditParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9166,'/ofr/admin/parameter/jsp/ParameterTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9167,'/ofr/admin/parameter/rs/jsp/EditRSCMParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9168,'/ofr/booking/jsp/PostCarriage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9169,'/ofr/booking/jsp/ProcessBooking.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9170,'/bpe/ScriptDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9171,'/bpe/ServiceEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9172,'/fm/dashboard/LoadChart.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9173,'/obl/rfp/capacity/capacity_edit_bid.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9174,'/obl/rfp/customrfpattribute/edit_custom_lanebidaccessorial_attribute.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9175,'/obl/rfp/file/in_progress_message.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9176,'/obl/rfp/lane/batch_delete.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9177,'/obl/rfp/lane/bid_history_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9178,'/obl/rfp/lane/decide_view_or_admin_lane.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9179,'/obl/rfp/lane/select_carrier_lane_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9180,'/obl/rfp/lane/shipper_package_edit_method.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9181,'/fm/driver/CheckInOutOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9182,'/fm/driver/CheckInOutOverridesDTD.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9183,'/ofr/admin/parameter/rs/jsp/EditRSCMParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9184,'/ofr/archive/jsp/ShipmentList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9185,'/ofr/archive/jsp/ShipmentWarningList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9186,'/ofr/booking/jsp/AddShipmentToBookingPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9187,'/ofr/booking/jsp/BookingRunAnalysis.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9188,'/ofr/booking/jsp/overrideCreateSoftChecksForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9189,'/ofr/booking/jsp/ProcessBookingFeasibleResource.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9190,'/ofr/booking/jsp/processRateDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9191,'/ofr/booking/jsp/rateDetailsPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9192,'/ofr/booking/jsp/removeShipmentsFromBooking.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9193,'/fm/bid/AddDriverToBid.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9194,'/fm/bid/TripBidDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9195,'/obl/rfp/capacity/capacity_selection.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9196,'/obl/rfp/customattribute/compose_edit_custom_attribute.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9197,'/obl/rfp/customrfpattribute/edit_custom_lane_vol_dtl.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9198,'/obl/rfp/customrfpattribute/process_custom_lineitem_attributes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9199,'/obl/rfp/customrfpattribute/process_edit_custom_rfp_attribute.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9200,'/obl/rfp/file/upload_and_create_rfp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9201,'/obl/rfp/lane/invite_carrier_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9202,'/obl/rfp/lane/shipper_package_normal.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9203,'/fm/debrief/TripDebrief.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9204,'/fm/driver/DispatchDriverSchedException.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9205,'/fm/driver/DSPDriverActivityHistoryEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9206,'/fm/driverkiosk/KioskOverride.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9207,'/fm/debrief/EndTrip.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9208,'/fm/debrief/EndTripOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9209,'/ofr/optimizationmgmt/consolidation/facilityschedule/jsp/ProcessFacilityToFacilitySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9210,'/ofr/optimizationmgmt/consolidation/jsp/ClearConsolidationRunIdsFromOrders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9211,'/ofr/optimizationmgmt/consolidation/jsp/ProcessSizeMap.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9212,'/ofr/optimizationmgmt/consolidation/jsp/UnplannedPOLineItemsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9213,'/ofr/optimizationmgmt/consolidation/jsp/ViewOptiflowFileList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9214,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ConsolidationEngineSelectInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9215,'/ofr/optimizationmgmt/consolidation/parameter/jsp/CreateParameterSetPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9216,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditAggregatorParams.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9217,'/cbo/uom/CreateUOMConversion.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9218,'/lcom/common/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9219,'/obl/filter/change_sorter.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9220,'/ofr/admin/healthcheck/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9221,'/ofr/rs/jsp/test/processPFRefreshEvent.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9222,'/ofr/rs/jsp/test/refreshInYardEquipAvailValues.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9223,'/ofr/rs/jsp/test/testGRSOptimizationRun.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9224,'/ucl/admin/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9225,'/ucl/admin/lps/LocationList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9226,'/ucl/admin/lps/RelationshipList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9227,'/uclEditLocationServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9228,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ProcessEditCPLEXParametersPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9229,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ProcessProtectionLevelPenalty.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9230,'/ofr/ra/jsp/carrierAppointmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9231,'/ofr/ra/jsp/ShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9232,'/ofr/rs/jsp/EditShipperTrackedPerformanceFactors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9233,'/ofr/rs/jsp/processConfigCycle.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9234,'/ofr/rs/jsp/processExceptions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9235,'/ofr/rs/jsp/processOptimization.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9236,'/ofr/rs/jsp/processValueFunction.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9237,'/ofr/rs/jsp/shipperConfigCyclePopupHolder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9238,'/ofr/rs/jsp/shipperPFValueFunction.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9239,'/ofr/ra/jsp/LoadArchiveFilter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9240,'/ofr/ra/jsp/OrderList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9241,'/ofr/ra/jsp/ProcessRegionUpdate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9242,'/ofr/ra/jsp/RAClaimAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9243,'/ofr/ra/jsp/SelectPaymentTypePopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9244,'/ofr/ra/jsp/TenderResponseErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9711,'/ifse/resources/ViewResourceContents.jsp','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9712,'/dif/clmessage/CLMessageDetails.jsflps','DIF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9713,'/services/rest/integration/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9714,'/reports/ui/ReportDefnList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9715,'/reports/ui/ConfigureReportParam.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9716,'/reports/ui/reportScheduled.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9717,'/reports/ui/reportScheduledGrid.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9718,'/reports/ui/reportSaved.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9719,'/reports/ui/reportSavedGrid.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9720,'/reports/ui/PrintReqUserAsgnList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9721,'/reports/ui/PrintQueueMonitor.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9722,'/reports/ui/PrintReqSerAsgnList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9723,'/reports/ui/PrintRequestorList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9724,'/reports/ui/PrintQueueSyncList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9725,'/reports/ui/LRFListenerConfigList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9726,'/reports/ui/PrinterFacLookup.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9727,'/reports/ui/PrintQueueSerAsgnList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9728,'/reports/ui/runReport.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9729,'/reports/ui/reportSubmit.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9730,'/reports/ui/printReport.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9731,'/reports/ui/PrintServiceTypesReviewList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9732,'/reports/ui/ReportsUpdateSequence.xhtml','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9733,'/ifse/dataloader/DataloaderexportDialog.jsp','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9734,'/ucl/admin/lps/CompanyParameters.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9736,'/jsp.report.serv/*','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9737,'/servlet/InstantReport.serv','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9738,'/lrf.report.serv/*','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9739,'/print.report.serv','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9740,'/ifse/dataintegration/SubscriberDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9741,'/ifse/dataintegration/SubscriberEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9742,'/reports/ui/JSPInstantReport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9743,'/jsp.report.serv','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9744,'/lrf.report.serv','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9745,'/bpe/*','BPE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9746,'/services/rest/ifse/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9747,'/services/rest/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9748,'/manh/tools/rest/*','SCPP',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9749,'/dwm/*','DWM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9750,'/services/rest/cbotransactional/DistributionOrderActionService/distributionOrderActions/onClickEditDatesAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9751,'/services/rest/cbotransactional/DistributionOrderActionService/distributionOrderActions/cancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9752,'/services/rest/cbotransactional/DistributionOrderActionService/distributionOrderActions/unCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9753,'/services/rest/cbotransactional/DistributionOrderActionService/distributionOrderActions/breakSyncLock','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9754,'/services/rest/cbotransactional/ASNActionService/asnActions/enableDisableEditASNHeaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9755,'/services/rest/cbotransactional/ASNActionService/asnActions/enableDisableShipASNAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9756,'/services/rest/cbotransactional/ASNActionService/asnActions/shipASNAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9757,'/services/rest/cbotransactional/ASNActionService/asnActions/enableDisableCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9758,'/services/rest/cbotransactional/ASNActionService/asnActions/cancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9759,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableEditAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9760,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableAcceptAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9761,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableDeclineAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9762,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableReadyAcceptanceAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9763,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableRTSAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9764,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableCreateRTSAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9765,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableAddToDOAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9766,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableCreateDOAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9767,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disablePOListCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9768,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disablePOListUnCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9769,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableLockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9770,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableUnLockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9771,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/cancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9772,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/unCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9773,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/validateCreateDOAction','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9774,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/readyToShipAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9775,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/validateViewRTSAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9776,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/lockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9777,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/unLockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9778,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/acceptAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9779,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/declineAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9780,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/readyForAcceptanceAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9781,'/services/rest/cbotransactional/ShipmentListActionService/shipmentListActions/breakSyncLockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9782,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/carrierExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9783,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/calendarExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9784,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/facilityExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9785,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/equipInstExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9786,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/driverExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9787,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/lookupList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9788,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/buList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9789,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/shipmentStatusList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9790,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/poStatusList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9791,'/services/rest/cbobase/CBOFiniteValueService/cbofv/sourceType','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9792,'/services/rest/cbobase/CBOFiniteValueService/cbofv/region','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9793,'/services/rest/cbobase/CBOFiniteValueService/cbofv/carrierbuList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9794,'/services/rest/cbobase/CBOFiniteValueService/cbofv/lookupList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9795,'/services/rest/cbobase/CBOFiniteValueService/cbofv/statProv','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9245,'/basedata/cmreport/jsp/schedule_data_load.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9246,'/obl/bid/autobid/autoBidRules.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9247,'/obl/bid/lane_bid/freight_charge_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9248,'/obl/bid/rfp_response/view_special_requirements_response.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9249,'/obl/CarrierSetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9250,'/obl/errors/rfpResponseError.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9251,'/obl/errors/unexpected_error.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9252,'/obl/header/createProfileTop.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9253,'/obl/header/new_header.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9254,'/obl/navigation_controller.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9255,'/obl/page_navigation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9256,'/obl/popups/duplicate_rfp_end.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9257,'/obl/profile/transport_provider/modes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9258,'/obl/profile/transport_provider/tl_mode_master.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9259,'/cbo/transactional/purchaseorder/view/POLineDetailEditHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9260,'/cbo/transactional/purchaseorder/view/StoreOrderList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9261,'/cbo/transactional/rts/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9262,'/cbo/transactional/rts/createAggregatedRTS.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9263,'/cbo/transactional/rts/view/RTSEditViewMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9264,'/cbo/transactional/shipment/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9265,'/cbo/transactional/shipment/view/ShipmentCreateHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9266,'/cbo/transactional/shipment/view/ShipmentEditGeneral.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9267,'/ofr/detention/shipper/jsp/ProcessDetentionUnloadAllowanceTimes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9268,'/basedata/businesspartner/jsp/BusinessPartnerDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9269,'/basedata/carrier/view/CarrierCreateGeneral.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9270,'/basedata/carrierserv/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10870,'/appointment/setup/appointmentslot/jsp/FacilitySupplierCreate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10871,'/appointment/setup/appointmentslot/jsp/FacilitySupplierUpdate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10872,'/appointment/setup/appointmentslot/jsp/FaclitySupplierUpdate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10873,'/appointment/setup/appointmentslot/jsp/InvokeApptSetupProcessingWithoutScheduler.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10874,'/appointment/setup/appointmentslot/jsp/NewApptSlots.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10875,'/appointment/setup/appointmentslot/jsp/NewSuppliers.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10876,'/appointment/setup/appointmentslot/jsp/processAppointmentReport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10877,'/appointment/setup/appointmentslot/jsp/processCreateFacilityRateExceptions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9279,'/basedata/tpparam/jsp/ViewExcludedAccessorialPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10878,'/appointment/setup/appointmentslot/jsp/processCreateSlot.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10879,'/appointment/setup/appointmentslot/jsp/processCreateSlotGroup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10880,'/appointment/setup/appointmentslot/jsp/processCreateSlotSchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10881,'/appointment/setup/appointmentslot/jsp/processDeleteSlotGroup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10882,'/appointment/setup/appointmentslot/jsp/processDownload.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10883,'/appointment/setup/appointmentslot/jsp/processFacilityRateExceptions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10884,'/appointment/setup/appointmentslot/jsp/processOverBookingPercentage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10885,'/appointment/setup/appointmentslot/jsp/processSaveSlot.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10886,'/appointment/setup/appointmentslot/jsp/processSaveSlotDurations.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10887,'/appointment/setup/appointmentslot/jsp/processSlot.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10888,'/appointment/setup/appointmentslot/jsp/processSlotGroup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10889,'/appointment/setup/appointmentslot/jsp/processSlotSchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10890,'/appointment/setup/appointmentslot/jsp/processSlotScheduleDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10891,'/appointment/setup/appointmentslot/jsp/SlotScheduleDetailsList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10892,'/appointment/setup/appointmentslot/jsp/Suppliers.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9301,'/basedata/customattribute/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9302,'/cbo/transactional/infeasibility/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9303,'/cbo/transactional/infeasibility/protectionLevelProtectionLevel/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9304,'/ucl/admin/lps/ViewUserExternalAccessControl.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9305,'/wm/systemcontrol/ui/TranMaintenanceList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9306,'/dashboard/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9307,'/basedata/skusubstitution/jsp/NewSkuSubstitution.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9308,'/basedata/staticschedule/jsp/StaticScheduleList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9309,'/basedata/suppliercategory/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9310,'/basedata/taxband/jsp/TaxBandListSheet.jsp?operation=view','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9311,'/basedata/usercarrier/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9312,'/basedata/zone/jsp/createZonePage1.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9313,'/basedataFilterDispatcherServices.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9314,'/cbo/dock/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9315,'/cbo/importexl/ImportExclusionList.jsflps?importType=ITEM','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9316,'/cbo/item/ItemList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9317,'/cbo/item/ItemSizeEditView.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9319,'/cbo/syscode/SystemCodes.jsflps?','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10869,'/appointment/setup/appointmentslot/jsp/FacilityApptSlotsUpdate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10893,'/appointment/setup/appointmentslot/jsp/updateAppointmentSupplierCategory.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10894,'/appointment/setup/feasibility/jsp/ApptslotFeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10895,'/appointment/setup/feasibility/jsp/FacilityFeasibilitySetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10896,'/appointment/setup/feasibility/jsp/FacilityNTDFeasibilitySetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10897,'/appointment/setup/feasibility/jsp/FeasibilityDetailForCustWaitTime.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10898,'/appointment/setup/feasibility/jsp/FeasibilityDetailPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10899,'/appointment/setup/feasibility/jsp/FeasibilityDetailPresentationForAllFeasibles.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10900,'/appointment/setup/feasibility/jsp/FeasibilityEntry.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10901,'/appointment/setup/feasibility/jsp/FeasibilityList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10902,'/appointment/setup/feasibility/jsp/FeasibilityProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10903,'/appointment/setup/feasibility/jsp/FeasibilitySetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10904,'/appointment/setup/feasibility/jsp/FeasibilitySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10905,'/appointment/setup/feasibility/jsp/NTDFeasibilitySetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10906,'/appointment/setup/feasibility/jsp/PrioritySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10907,'/appointment/setup/feasibility/jsp/priorityScheduleDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10908,'/appointment/setup/feasibility/jsp/ProcessApptslotFeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10909,'/appointment/setup/feasibility/jsp/processPrioritySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10910,'/appointment/setup/feasibility/jsp/ProritySetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10911,'/appointment/setup/feasibility/jsp/SlotFeasibilitySetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10912,'/appointment/setup/feasibility/jsp/YardZonePrioritySetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10913,'/appointment/setup/shift/jsp/ProcessShiftList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10914,'/appointment/setup/shift/jsp/ShiftList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10915,'/appointment/setup/TotalCapacity/jsp/FacilityTotalCapacityList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10916,'/appointment/setup/TotalCapacity/jsp/processTotalCapacity.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10917,'/appointment/setup/TotalCapacity/jsp/SlotTotalCapacityList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10918,'/appointment/setup/TotalCapacity/jsp/TotalCapacity.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10919,'/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10920,'/appointment/setup/TotalCapacity/jsp/TotalCapacityDetailProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10921,'/appointment/setup/TotalCapacity/jsp/TotalCapacityList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10922,'/appointment/setup/TotalCapacity/jsp/TotalCapacityProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10923,'/appointment/setup/TotalCapacity/jsp/TotalCapacityTable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10924,'/appointment/setup/TotalCapacity/jsp/YardZoneTotalCapacityList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10925,'/appointment/shipserv/jsp/ValidateShipmentForAppointments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10926,'/appointment/softcheckerrors/jsp/SoftCheckErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10927,'/appointment/ui/jsf/appointmentAdditionalDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10928,'/appointment/ui/jsf/appointmentAuditTrail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10929,'/appointment/ui/jsf/appointmentCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10930,'/appointment/ui/jsf/appointmentDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10931,'/appointment/ui/jsf/appointmentDockDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10932,'/appointment/ui/jsf/appointmentMenu.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10933,'/appointment/ui/jsf/appointmentObjDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10934,'/appointment/ui/jsf/appointmentRecommendations.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10935,'/appointment/ui/jsf/appointmentslotDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10936,'/appointment/ui/jsf/appointmentWarnings.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10937,'/appointment/ui/jsf/ApptDockDoorLookUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10938,'/appointment/ui/jsf/ApptDockLookUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10939,'/appointment/ui/jsf/apptLevelling.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10940,'/appointment/ui/jsf/CarrierAppointmentList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10941,'/appointment/ui/jsf/DockLookUpGetsDockId.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10942,'/appointment/ui/jsf/scheduleAppointmentCarrier.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10943,'/appointment/ui/jsp/appointmentCalendar.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10944,'/appointment/ui/jsp/appointmentPlanning.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10945,'/appointment/ui/jsp/AppointmentRecurrence.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10946,'/appointment/ui/jsp/appointmentReport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10947,'/appointment/ui/jsp/apptSlotCalendar.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10948,'/appointment/ui/jsp/apptSlotReport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10949,'/appointment/ui/jsp/createAppointment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10950,'/appointment/ui/jsp/header.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10951,'/appointment/ui/jsp/popupHeaderWithPrintHelp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10952,'/appointment/ui/jsp/POShipmentSelect.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10953,'/appointment/ui/jsp/processAppointmentApprove.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10954,'/appointment/ui/jsp/ProcessAppointmentRecurrence.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10955,'/appointment/ui/jsp/ProcessPOShipmentSelect.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10956,'/appointment/ui/jsp/rejectAppointment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10957,'/appointment/ui/jsp/YardCalendar.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10958,'/appointment/util/jsf/ymsSoftcheckErrors.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10959,'/appointment/util/jsf/ymsSoftcheckErrorsTask.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10960,'/appointment/util/jsp/NumRowsPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10961,'/services/rest/tpe/TPEApptActionService/tpeApptActions/getStopApptDttm','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10962,'/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getRecommendations','APPT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10963,'/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/saveRecommendation','APPT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10965,'/appointment/ui/jsf/chartcreatorrequest.jsf','APPT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10966,'/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/updateAppointmentInfo','APPT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10967,'/appointment/ui/jsf/calendarApptTransit.xhtml','APPT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10968,'/appointment/ui/jsp/oldOrNewCalendar.jsp','APPT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10969,'/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/cleanup','VAPT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10970,'/te/manifest/ui/BundlesReport.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10971,'/te/manifest/ui/editDODestinationAddress.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10972,'/te/manifest/ui/fedexServerTranInqDetail.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10973,'/te/manifest/ui/CloseManifest.xhtml','ASHP',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10974,'/te/manifest/ui/GenerateSED.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10975,'/te/manifest/ui/InternationalDocumentList.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10976,'/te/manifest/ui/LabelReport.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10977,'/te/manifest/ui/LabelTranslations.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10978,'/te/manifest/ui/LiteralTranslationsList.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10979,'/te/manifest/ui/manifestedLpnDetail.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10980,'/te/manifest/ui/manifestedLpnExceptionList.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10981,'/te/manifest/ui/manifestHdrDetail.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10982,'/te/manifest/ui/manifestLpnChrgList.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10983,'/te/manifest/ui/manifestLpnDetailist.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10984,'/te/manifest/ui/MessageLogList.xhtml','SYSCTRL',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10985,'/te/manifest/ui/MessageMasterList.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10986,'/te/manifest/ui/warningDialog.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10987,'/te/manifest/ui/weightLpnDetail.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10988,'/te/manifest/ui/weightManifestDetail.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10989,'/te/manifest/ui/weightOrderDetail.xhtml','MNF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10993,'/te/manual/planning/jsp/AssignShipVia.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10994,'/te/manual/planning/jsp/buttonsPanel.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10995,'/te/manual/planning/jsp/CloseShipment.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10996,'/te/manual/planning/jsp/hardCheckErrors.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10997,'/te/manual/planning/jsp/softCheckErrors.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10998,'/te/manual/planning/jsp/softCheckErrorsOverride.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10999,'/te/manual/planning/jsp/softCheckErrorsOverrideTemplate.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11000,'/te/manual/planning/jsp/StaticRouteCheckResult.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9320,'/cbo/transactional/alerts/jsp/ProcessAlertListActions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9321,'/cbo/transactional/alerts/jsp/ShipmentAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9322,'/cbo/transactional/asn/AssignASNToShipment.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9323,'/cbo/transactional/auditTrail/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9324,'/cbo/transactional/distributionorder/view/DOCreateMisc.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9325,'/cbo/transactional/distributionorder/view/DOCreatePickupDeliveryTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9326,'/fm/tandem/TandemOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9327,'/fm/trip/AddShipment.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9328,'/fm/trip/DispatchShipmentDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9329,'/fm/trip/DispatchShipmentDetailsOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9330,'/fm/trip/HardCheckErrors.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9331,'/basedata/poterm/jsp/ViewPOTerm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9332,'/basedata/printer/jsp/configurePrinter.jsp?printerSetupPage=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9333,'/basedata/reasoncode/jsp/ProcessViewReasonCodes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9334,'/obl/rfp/lane/view_lane_bid.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9335,'/obl/rfp/message/message_summary_list_header.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9336,'/obl/rfp/rfp/message_center_master.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9337,'/obl/rfp/rfp/process_close_rfp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9338,'/obl/rfp/rfp/process_edit_spec_reqs.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9339,'/obl/rfp/rfp/view_invited_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9340,'/obl/rfp/rfp/view_rebid_lane.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9341,'/obl/scenario/process_scenario_facility_carrier.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9342,'/obl/scenario/scenario_advanced_overrides_edit.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9343,'/obl/scenario/scenario_facility_carriers.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9344,'/obl/scenario/scenario_global_rules.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9345,'/obl/scenario/scenario_lane_detail.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9346,'/obl/scenario/scenario_overview_print_include.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9347,'/obl/scenario/scenario_winning_carrier_lane_print.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9348,'/scem/postevent/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9349,'/scem/scheduletemplate/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9350,'/scem/scheduletemplategroup/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9353,'/ofr/trackingmsg/jsp/ProcessNewTrackingMessages.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9354,'/ofr/trackingmsg/jsp/TrackingMessageDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9355,'/te/manifest/ui/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9356,'/te/optimizationmgmt/consolidation/parameter/jsp/ConsolidationEngineSelectInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9357,'/te/optimizationmgmt/consolidation/parameter/jsp/ViewConsolidationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9358,'/ofr/ra/jsp/AppointmentContact.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9359,'/ofr/ra/jsp/BillToDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9360,'/ofr/ra/jsp/CarrierAppointmentList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9361,'/basedata/region/plannerregion/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9362,'/basedata/report/jsp/SummaryReportDefinitionList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9363,'/cbo/transactional/distributionorder/view/DODetailsShipping.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9364,'/cbo/transactional/distributionorder/view/DOEditFulfillmentTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9365,'/cbo/transactional/distributionorder/view/DOLineCreateMiscTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9366,'/cbo/transactional/distributionorder/view/DOLineCreateTransTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9367,'/cbo/transactional/distributionorder/view/DOLineEditTransTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9368,'/basedata/dock/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9369,'/basedata/drivertype/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9370,'/basedata/equipmentinstance/jsp/EquipmentInstanceSummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9371,'/basedata/equipmentinstance/jsp/RaEquipmentInstanceSummaryPresentationViewOnly.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9372,'/basedata/facility/jsp/PoolPointViewShipList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9373,'/basedata/filter/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9374,'/basedata/laboractivity/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9375,'/basedata/lookup/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9376,'/basedata/merchandizedept/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9377,'/basedata/merchandizedept/jsp/merchandizeDept.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9378,'/basedata/mode/jsp/modeSizeLimitDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9379,'/basedata/mode/jsp/SaveAllModes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9380,'/ofr/infeasibility/dockProductClass/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9381,'/ofr/infeasibility/dockProtectionLevel/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9382,'/ofr/infeasibility/facilityTP/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9383,'/ofr/infeasibility/modeProtectionLevel/jsp/InfeasibleModeProtectionLevelPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9384,'/ofr/infeasibility/protectionLevelFacilityPair/jsp/UpdateInfeasibleProtectionLevelFacilityPair.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9385,'/ofr/infeasibility/protectionLevelProtectionLevel/jsp/UpdateInfeasibleProtectionLevel.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9386,'/ofr/invoice/shipper/accountcoding/jsp/processCustomAccountCode.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9387,'/ofr/invoice/shipper/jsp/ClearRuleEngineCache.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9388,'/ofr/invoice/shipper/jsp/InvoiceAuditTrailCommentPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9389,'/ofr/invoice/shipper/jsp/loadClaimNumbers.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9390,'/ofr/invoice/shipper/jsp/loadShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9391,'/ofr/invoice/shipper/jsp/ProcessInvoice.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9392,'/ofr/invoice/shipper/jsp/RetrieveShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9393,'/ofr/invoice/ui/EditOtherDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9394,'/ofr/invoice/ui/ViewExceptionInvoiceList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9395,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/ConsolidationRegionFacilityUpdate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9396,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/FacilityListBox.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9397,'/ofr/optimizationmgmt/consolidation/enginespecific/ofib/jsp/ProcessEngineSpecificRunTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9398,'/fm/trip/TripDetailsOBCPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9399,'/ofr/claim/shipper/jsp/CancelReOpenClaim.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9400,'/ofr/claim/shipper/jsp/CargoDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9401,'/ofr/claim/shipper/jsp/ClaimItems.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9402,'/ofr/consolidation/workspace/jsp/bypassAssignConfirmation.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9403,'/ofr/consolidation/workspace/jsp/ShipmentList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9404,'/ofr/continuousmove/jsp/cmParametersEdit.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9405,'/ofr/criticalchange/jsp/SetupBookingCriticalChangeResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9406,'/ofr/criticalchange/jsp/SetupBookingCriticalChangeTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9407,'/ofr/detention/jsp/EditDetention.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9408,'/ofr/detention/jsp/InvoiceDetailsForDetention.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9409,'/fm/trip/TripStopsDetailsViewOnly.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9410,'/ofr/admin/parameter/booking/jsp/EditBookingParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9411,'/ofr/admin/parameter/booking/jsp/EditBookingParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9412,'/ofr/admin/parameter/claims/jsp/EditClaimParameter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9413,'/cbo/transactional/infeasibility/productClassEquipment/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9414,'/cbo/transactional/infeasibility/productClassProductClass/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9415,'/cbo/transactional/location/view/LocationSizeList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9416,'/cbo/transactional/location/view/PickLocationDtlList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9417,'/cbo/transactional/lpn/view/CaseLPNSelection.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9418,'/cbo/transactional/lpn/view/EditLPNHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9419,'/cbo/transactional/lpn/view/LPNListMain.jsflps?ListType=LPN','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9420,'/cbo/transactional/purchaseorder/view/EditSalesOrderHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9421,'/cbo/transactional/purchaseorder/view/POBPEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9422,'/cbo/transactional/purchaseorder/view/POCommentsCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9423,'/ofr/admin/parameter/companyconsolidation/jsp/EditConsolidationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9424,'/ofr/admin/parameter/detention/jsp/EditSuppDetentionNotificationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9425,'/ofr/admin/parameter/doms/jsp/EditDOMReappParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9426,'/ofr/archive/jsp/ShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9427,'/ofr/booking/jsp/bookingLaneDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9428,'/ofr/booking/jsp/BookingShipmentList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9429,'/ofr/booking/jsp/ViewCriticalChangeSoftChecks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9430,'/bpe/ScriptEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9431,'/fm/alert/DispatchDriverAlert.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9432,'/fm/bid/AddTripToBid.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9433,'/fm/dashboard/AlternateDriversList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9434,'/fm/dashboard/driverCalendar.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9435,'/fm/dashboard/DriverChart.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9436,'/fm/dashboard/mapspage.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9437,'/obl/rfp/customdownload/process_edit_custom_rfp_download.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9438,'/obl/rfp/customrfpattribute/edit_custom_lineitem_validation.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9439,'/obl/rfp/customrfpattribute/process_custom_lane_lanebid_attributes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9440,'/obl/rfp/customrfpattribute/process_custom_lane_vol_dtl.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9441,'/obl/rfp/file/upload_and_create_rfp_link.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9442,'/obl/rfp/file/view_audit_trail.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9443,'/obl/rfp/file/view_file_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9444,'/obl/rfp/lane/administer_lane.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9445,'/obl/rfp/lane/administer_lane_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9446,'/obl/rfp/lane/batch_inactivate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9447,'/obl/rfp/lane/carrier_packagebid_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9448,'/obl/rfp/lane/confirm_cancel_lane.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9449,'/obl/rfp/lane/select_carriers_popup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9450,'/fm/debrief/StateTaxMilesColumns.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9451,'/fm/debrief/StateTaxMilesForTripSheet.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9452,'/fm/driver/CheckInOutOverridesDDD.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9453,'/fm/driver/DispatchDriverDetailsOBCPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9454,'/fm/driver/DispatchDriverList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9455,'/fm/driverkiosk/DriverCheckout.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9456,'/fm/driverkiosk/KioskBidOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9457,'/fm/driverkiosk/KioskTripListColumns.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9458,'/fm/infeasibility/driverTypeFacility/jsp/AddInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9459,'/ofr/optimizationmgmt/consolidation/jsp/ConsolidationRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9460,'/ofr/optimizationmgmt/consolidation/jsp/DeconsolidateShipmentsConfirmation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9461,'/ofr/optimizationmgmt/consolidation/jsp/DeconsolidateShipmentsResult.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9462,'/ofr/optimizationmgmt/consolidation/jsp/ProcessConsolidationRunTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9463,'/ofr/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRunTemplates.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9464,'/ofr/optimizationmgmt/consolidation/jsp/PurgeDistTime.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9465,'/ofr/optimizationmgmt/consolidation/jsp/SmallOrderPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9466,'/ofr/optimizationmgmt/consolidation/jsp/ViewOptiflowFileText.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9467,'/ofr/optimizationmgmt/consolidation/jsp/ViewOptiFlowSendQueue.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9468,'/ofr/optimizationmgmt/consolidation/jsp/ViewOrderPathAnalysis.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9469,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ConsolidationParametersInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9470,'/ofr/optimizationmgmt/consolidation/parameter/jsp/CreateParamSetInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9471,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditConsolidationParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9472,'/manh/tools/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9473,'/manh/tools/sa/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9474,'/obl/filter/administer_filter_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9475,'/obl/filter/filter_list.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9476,'/obl/filter/filter_master.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9477,'/ofr/documentation/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9478,'/ofr/lookup/jsp/','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9479,'/ofr/ra/jsp/ClearShipperNamesCache.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9480,'/ofr/rs/jsp/test/testCppGRSOptimizationRun.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9481,'/ucl/admin/lps/CompanyCreate.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9482,'/ucl/admin/lps/UserList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9483,'/ucl/toolpages/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9484,'/ofr/optimizationmgmt/consolidation/parameter/jsp/EditSplitterParamsProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9485,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ProcessParameterSet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9486,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewConsolidationParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9487,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewCPLEXParametersPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9488,'/ofr/optimizationmgmt/consolidation/parameter/jsp/ViewSplitterParams.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9489,'/ofr/ra/jsp/carrierCreateAppointment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9490,'/ofr/ra/jsp/createAppointmentFromShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9491,'/ofr/rs/jsp/processBusinessHours.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9492,'/ofr/rs/jsp/rateDetailsPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9493,'/ofr/rs/jsp/ScorePopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9494,'/ofr/rs/jsp/shipperAssignedRSConfig.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9495,'/ofr/rs/jsp/shipperBrokerResourceConfig.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9496,'/ofr/rs/jsp/shipperConfigCycle.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9497,'/ofr/rs/jsp/shipperConfigModeActionHolder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9498,'/ofr/rs/jsp/shipperOptimization.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9499,'/ofr/ra/jsp/CommentList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9500,'/ofr/ra/jsp/NewTrackingMessages.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9501,'/ofr/ra/jsp/ProcessRegionDeletion.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9502,'/basedata/cmreport/jsp/laneDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9503,'/obl/administer_distribution_list_mode_selection.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9504,'/obl/bid/lane_bid/carrier_one_lane.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9505,'/obl/bid/rfp_response/administer_special_requirements_response.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9506,'/obl/common/jsp/ApplicationError.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9507,'/obl/common/jsp/ErrorPage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9508,'/obl/errors/account_not_initialized_error.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9509,'/obl/errors/global_error_page.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9510,'/obl/errors/popup_error_page.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9511,'/obl/header/globalTop.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9512,'/obl/header/header.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9513,'/obl/help/Unavailable_Help.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9514,'/obl/master/setup_sidenav.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9515,'/obl/master/workspace_sidenav.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9516,'/obl/popups/calendar.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9517,'/obl/popups/lane_bid_comment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9518,'/obl/popups/popup_variables.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9519,'/obl/portLookUp.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9520,'/obl/profile/process_carrier_list.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9521,'/obl/profile/transport_provider/no_mode_master.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9522,'/obl/profile/transport_provider/profile.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9523,'/obl/rfp/basedata/accessorial_details.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9524,'/cbo/transactional/purchaseorder/view/POLineItemEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9525,'/cbo/transactional/rts/view/RTSEditGeneralTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9526,'/cbo/transactional/rts/view/RTSEditViewGeneralTab.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9527,'/cbo/transactional/shipment/ShipmentEditStopList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9528,'/cbo/transactional/shipment/view/EditQuickShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9529,'/cbo/transactional/shipment/view/editStandardStopsAndOrders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9530,'/cbo/transactional/shipment/view/EditStopWInfoOrders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9531,'/cbo/transactional/shipment/view/ShipmentCreate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9532,'/ofr/detention/shipper/jsp/DetentionList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9533,'/ofr/detention/shipper/jsp/ProcessDetentionRates.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9534,'/basedata/alert/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9535,'/basedata/businesspartner/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9536,'/basedata/businesspartner/jsp/BusinessPartnerContactDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9537,'/basedata/carrier/view/CarrierEditHolidaySchedule.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9538,'/basedata/carrier/view/CarrierEditMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9539,'/basedata/carrier/view/CarrierList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9540,'/basedata/carrierserv/driver/jsp/RaDriverSummaryPresentationViewOnly.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9544,'/basedata/servicelevel/view/SLParcelView.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9568,'/basedata/commoditycode/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9569,'/basedata/customattribute/jsp/CustomAttributeList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9570,'/ucl/admin/lps/ViewRolePermissions.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9571,'/basedata/sourcing/jsp/CreateSkuSourcingRule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9572,'/basedata/usercarrier/UserCarrierRelationShipList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9573,'/basedata/userdefined/jsp/UserDefinedValuePresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9574,'/basedata/zone/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9575,'/cbo/alert/jsp/APEditAlertDef.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9576,'/cbo/alert/jsp/APProcessEditAlertDef.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9577,'/cbo/facility/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9578,'/cbo/transactional/admin/parameter/jsp/EditCBOTransCompanyParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9579,'/cbo/transactional/admin/parameter/jsp/EditSOParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9580,'/cbo/transactional/alerts/jsp/AlertDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9581,'/cbo/transactional/alerts/view/AlertList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9582,'/cbo/transactional/asn/EditASN.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9583,'/cbo/transactional/customization/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9584,'/cbo/transactional/distributionorder/view/DOCreateMain.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9585,'/cbo/transactional/distributionorder/view/DODetailsMainHeader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9586,'/fm/tandem/FeasibleTandemSegments.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9587,'/fm/trip/DispatchShipmentDetailsOverridesForDeleteSegment.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9588,'/fm/trip/DispatchShipmentDetailsOverridesForRemoveRelay.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9589,'/fm/trip/DispatchShipmentDetailsOverridesForSaveAll.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9590,'/fm/trip/DispatchWorkspaceAltDestPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9591,'/fm/trip/DispatchWorkspaceOBCPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9592,'/basedata/printer/jsp/configurePrinter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9593,'/basedata/qualitylevel/jsp/ProcessViewQualityLevels.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9594,'/obl/rfp/lane/sourcing/admin_sourcing_changes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9595,'/obl/rfp/lane/TPPackageAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9596,'/obl/rfp/laneaward/process_create_lane_award.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9597,'/obl/rfp/message/message_detail.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9598,'/obl/rfp/postrfpaward/init_post_rfp_award.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9599,'/obl/rfp/rfp/decline_rfp_process.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9600,'/obl/rfp/rfp/essentials_buttons.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9601,'/obl/rfp/rfp/view_rfp_cancel_reason_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9602,'/obl/rfp/rfp/view_special_reqs.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9603,'/obl/saveas_administer_distribution_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9604,'/obl/scenario/process_scenario.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9605,'/obl/scenario/process_scenario_lane_rules.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9606,'/obl/scenario/scenario_advanced_overrides.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9607,'/obl/scenario/scenario_lane_detail_print.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9608,'/obl/scenario/scenario_lane_rules.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9609,'/obl/xml/create_xml_txt.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9610,'/fieldvision/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9611,'/scem/eventmgt/dashboard/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9612,'/scem/scheduletemplate/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9613,'/scv/admin/parameter/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9620,'/basedata/ordertype/jsp/OrderType.jsflps?customFilter=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9621,'/basedata/ordertype/jsp/ViewAllOrderTypes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9622,'/basedata/payeeserv/jsp/PayeeSummaryPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9623,'/te/infeasibility/servicelevelcustomer/jsp/AddInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9624,'/te/infeasibility/servicelevelfacility/jsp/ViewInfeasibilities.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9625,'/te/optimizationmgmt/consolidation/parameter/jsp/EditPlanningEngineParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9626,'/ofr/claim/carrier/jsp/ViewTransClaimDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9627,'/ofr/detention/carrier/jsp/DetentionList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9628,'/ofr/ra/jsp/AcceptClaim.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9629,'/ofr/ra/jsp/BookingDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9630,'/basedata/region/jsp/ResourceRegionSummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9631,'/basedata/report/jsp/processSummaryReport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9632,'/cbo/transactional/globalvisibility/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9633,'/basedata/customerserv/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9634,'/basedata/documentation/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9635,'/basedata/documentmanagement/view/AddAndEditDocumentRevision.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9636,'/basedata/equipment/jsp/EquipmentSheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9637,'/basedata/equipment/jsp/InYardEquipmentAvailabilityList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9638,'/basedata/equipmentinstance/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9639,'/basedata/equipmentinstance/jsp/EquipmentInstanceSummaryPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9640,'/basedata/facility/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9641,'/basedata/laboractivity/jsp/LaborActivityTabEditMode.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9642,'/basedata/loadConfig/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9643,'/services/rest/lps/*','SCPP',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9644,'/services/rest/dashboard/*','SCPP',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9645,'/services/rest/see/*','SCPP',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9646,'/services/rest/oum/CommonService/commonservice/countrylist','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9647,'/services/rest/oum/CommonService/commonservice/statelist','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9648,'/services/rest/oum/CommonService/commonservice/status','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9649,'/services/rest/oum/CompanyService/companyservice/listcompanytypes','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9650,'/services/rest/oum/CompanyService/companyservice/companydetailbyid','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9651,'/services/rest/oum/CompanyService/companyservice/updatecompany','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9652,'/services/rest/oum/CompanyService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9653,'/services/rest/oum/ModuleService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9654,'/services/rest/oum/LocationService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9655,'/services/rest/oum/RegionService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9656,'/services/rest/oum/UserService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9657,'/services/rest/oum/ChannelService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9658,'/services/rest/oum/PermissionService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10616,'/basedata/rating/ratinglane/jsp/editLane.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10617,'/basedata/rating/ratinglane/jsp/editTp.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10618,'/basedata/rating/ratinglane/jsp/ExportLanes.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10619,'/basedata/rating/ratinglane/jsp/laneCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10620,'/basedata/rating/ratinglane/jsp/laneDetails.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10621,'/basedata/rating/ratinglane/jsp/LaneErrors.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10622,'/basedata/rating/ratinglane/jsp/manually_retrieve_rate.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10623,'/basedata/rating/ratinglane/jsp/pendingApprovalList.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10624,'/basedata/rating/ratinglane/jsp/pendingApprovalListReadOnly.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10625,'/basedata/rating/ratinglane/jsp/pendingApprovalListResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10626,'/basedata/rating/ratinglane/jsp/ProcessAccessorialExclusionCURDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10627,'/basedata/rating/ratinglane/jsp/ProcessExportLanes.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10628,'/basedata/rating/ratinglane/jsp/processMassUpdate.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10629,'/basedata/rating/ratinglane/jsp/rate_details_popup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10630,'/basedata/rating/ratinglane/jsp/rateCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10631,'/basedata/rating/ratinglane/jsp/RatingProfilerResults.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10632,'/basedata/rating/ratinglane/jsp/reviewLane.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10633,'/basedata/rating/ratinglane/jsp/tpDetails.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10634,'/basedata/rating/ratinglane/jsp/validationSummary.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10635,'/basedata/rating/tariff/jsp/bulkTariffDetail.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10636,'/basedata/rating/tariff/jsp/editTariffZone.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10637,'/basedata/rating/tariff/jsp/FilterDetails.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10638,'/basedata/rating/tariff/jsp/FilterList.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10639,'/basedata/rating/tariff/jsp/Reload.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10640,'/basedata/rating/tariff/jsp/TariffCodeList.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10641,'/basedata/rating/tariff/jsp/tariffLaneCRUD.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9659,'/services/rest/oum/CommonService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9660,'/services/rest/Integration/Company/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9661,'/services/rest/Integration/User/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9662,'/services/rest/Integration/Role/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9663,'/services/rest/Integration/Relationship/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9664,'/services/rest/Integration/UserGroup/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9665,'/services/rest/integration/oum/region/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9666,'/services/rest/integration/oum/location/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9667,'/ifse/crossref/CrossRefCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9668,'/ifse/crossref/CrossRefEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9669,'/ifse/crossref/CrossRefDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9670,'/ifse/alerts/AlertCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9671,'/ifse/alerts/AlertEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9672,'/ifse/alerts/AlertDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9673,'/ifse/lookup/CreateLookup.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9674,'/ifse/lookup/EditLookup.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9675,'/ifse/lookup/LookupDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9676,'/ifse/pixcrossref/PixCrossRefCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9677,'/ifse/pixcrossref/PixCrossRefEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9678,'/ifse/pixcrossref/PixCrossRefDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9679,'/ifse/router/RouterCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9680,'/ifse/router/RouterEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9681,'/ifse/router/RouterDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9682,'/ifse/listener/ListenerCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9683,'/ifse/listener/ListenerEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9684,'/ifse/listener/ListenerDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9685,'/ifse/listener/ftp/AddFTPListener.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9686,'/ifse/tranlog/TranlogDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9687,'/ifse/resources/UploadResource.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9688,'/dif/endPoint/EndPointEdit.jsflps','DIF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9689,'/dif/endPoint/EndPointDetail.jsflps','DIF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9690,'/services/rest/oum/UserService/userservice/myprofile','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9691,'/services/rest/oum/UserService/userservice/myprofile','ACM',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9692,'/services/rest/oum/CommonService/commonservice/locales','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9693,'/services/rest/oum/CommonService/commonservice/communicationmethods','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9694,'/services/rest/oum/CompanyService/companyservice/passwordcriteriaoncompanyid','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9695,'/services/rest/oum/LocationService/locationservice/locationSelectionList','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9696,'/services/rest/oum/ChannelService/channelservice/channelSelectionList','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9697,'/services/rest/oum/UserCredentialService/usercredservice/sendpasswordresetrequestbyemail','ACM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9698,'/services/rest/lps/PhoneTopService/*','SCPP',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9699,'/services/datasync/mda/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9700,'/services/datasync/subscriber/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9701,'/services/rest/integration/device/*','DIF',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9702,'/services/rest/oum/UserService/userservice/appmodpermtree','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9703,'/ifse/dataintegration/DataSyncLogDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9704,'/manh/oum/audit/audittest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9705,'/audit/audittest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9709,'/services/dwmService/*','DWM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9707,'/services/rest/lps/PhoneTopService/*','SCPP',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9708,'/services/rest/dwm/*','DWM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9710,'/ifse/tranlog/TranlogList.xhtml','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10204,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/tierList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10205,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/reasonCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10206,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/tariffList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10207,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10208,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/minRateTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10209,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/distanceUOMList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10210,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMWeightList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10211,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMAllList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10212,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMDistanceList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10213,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/auditTrailList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10214,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/pendingApprovalChangesList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10215,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10216,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10217,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10218,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10219,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/auditTrailList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10220,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/pendingApprovalChangesList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10221,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/accessorialExclusionList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10222,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10223,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10224,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10225,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10226,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10645,'/basedata/rating/tariff/jsp/tariffTransitTimeCRUD.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10646,'/basedata/rating/tariff/jsp/tariffTransitTimeList.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10227,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10228,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10229,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10230,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/lpnTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10231,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10232,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10233,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10234,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10235,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/carrierUtilizationUsageByWeek','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10236,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10237,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10238,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10239,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10240,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10241,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10242,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10243,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10244,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10245,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10246,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10247,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10248,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/accessorialCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10249,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/surchargeTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10250,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/zoneList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10251,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/serviceLevelList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10252,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/modeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10253,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/currencyList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10254,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10255,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10256,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10257,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10258,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10259,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10260,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10261,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10262,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10263,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10264,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10265,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10266,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/stopOffCurrencyList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10267,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/distanceUOMList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10268,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/rateTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10269,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/businessUnitList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10270,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/weightList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10271,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/equipmentCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10272,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/cmCurrencyList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10273,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/fakCommodityCodeClassList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10274,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/carrierCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10275,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/auditTrailList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10276,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/getStopoffCurrencies','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10277,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/getCMCurrencies','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10278,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10282,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/accessorialCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10283,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/incotermList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10284,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/currencyList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10285,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/sizeTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10286,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/rateTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10647,'/basedata/rating/tariff/jsp/tariffTransitTimePopUp.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10648,'/basedata/rating/tariff/jsp/tariffZoneCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10649,'/basedata/rating/tariff/jsp/tariffZoneRateCRUD.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10287,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/modeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10288,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/equipmentCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10289,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/serviceLevelList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10290,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/billingMethodList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10291,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/protectionLevelList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10292,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/uomList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10293,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/accessorialExclusionList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10294,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/payeeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10295,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/fuelAccessorialList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10296,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/ORMList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10297,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/updateExcludedAccessorialList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10298,'/services/rest/contractmanagement/AccessorialService/accessorialservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10299,'/services/rest/contractmanagement/AccessorialService/accessorialservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10300,'/services/rest/contractmanagement/AccessorialService/accessorialservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10301,'/services/rest/contractmanagement/AccessorialService/accessorialservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10302,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialLabelTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10303,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10304,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialOptionGroupMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10305,'/services/rest/contractmanagement/AccessorialService/accessorialservice/distanceUOMMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10306,'/services/rest/contractmanagement/AccessorialService/accessorialservice/acessorialOptionGroupDesc','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10307,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialLabelTypeDesc','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10308,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialCodesDependentForTC','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10309,'/services/rest/contractmanagement/AccessorialService/accessorialservice/validAccessorialCodesMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10310,'/services/rest/contractmanagement/AccessorialService/accessorialservice/fuelIndexMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10311,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialDataById','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10312,'/services/rest/contractmanagement/AccessorialService/accessorialservice/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10313,'/services/rest/contractmanagement/AccessorialService/accessorialservice/dataSourceMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10314,'/services/rest/contractmanagement/AccessorialService/accessorialservice/exportFuelSurcharge','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10315,'/services/rest/contractmanagement/AccessorialService/accessorialservice/sellSideRatesFlag','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10316,'/services/rest/contractmanagement/ShipViaService/shipviaservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10317,'/services/rest/contractmanagement/ShipViaService/shipviaservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10318,'/services/rest/contractmanagement/ShipViaService/shipviaservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10319,'/services/rest/contractmanagement/ShipViaService/shipviaservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10320,'/services/rest/contractmanagement/ShipViaService/shipviaservice/modeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10321,'/services/rest/contractmanagement/ShipViaService/shipviaservice/serviceLevelMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10322,'/services/rest/contractmanagement/ShipViaService/shipviaservice/shipViaAccessorialMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10323,'/services/rest/contractmanagement/ShipViaService/shipviaservice/currencyListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10324,'/services/rest/contractmanagement/ShipViaService/shipviaservice/shipViaCustomAttributeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10325,'/services/rest/contractmanagement/ShipViaService/shipviaservice/insuranceCoverTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10326,'/services/rest/contractmanagement/ShipViaService/shipviaservice/labelTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10327,'/services/rest/contractmanagement/ShipViaService/shipviaservice/parcelServiceIconList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10328,'/services/rest/contractmanagement/ShipViaService/shipviaservice/serviceLevelIndicatorList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10329,'/services/rest/contractmanagement/ShipViaService/shipviaservice/executionlevelList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10330,'/services/rest/contractmanagement/ShipViaService/shipviaservice/carrierLookUpMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10845,'/appointment/lookup/jsp/idLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10846,'/appointment/lookup/jsp/idLookupHFI.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10847,'/appointment/lookup/jsp/shipmentLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10848,'/appointment/lookup/ui/ApptPOLookUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10849,'/appointment/lookup/ui/ApptPOLookUpDialog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10850,'/appointment/lookup/ui/defaultLookupTemplate.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10851,'/appointment/lookup/ui/ILMDriverLookUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10852,'/appointment/lookup/ui/ILMSlotGroupLookUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10853,'/appointment/lookup/ui/ILMSlotGroupLookupDialog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10854,'/appointment/lookup/ui/ILMSlotLookUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10855,'/appointment/lookup/ui/ILMSlotLookupDialog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10856,'/appointment/lookup/ui/NumRowPopUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10857,'/appointment/lookup/ui/NumRowPopUpDialog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10858,'/appointment/lookup/ui/TrailerLookUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10859,'/appointment/lookup/ui/TrailerLookUpDialog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10860,'/appointment/order/jsp/ValidatePOForAppointments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10861,'/appointment/setup/appointmentslot/jsp/AppointmentAnalysisReportCreate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10862,'/appointment/setup/appointmentslot/jsp/AppointmentSlot.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10863,'/appointment/setup/appointmentslot/jsp/AppointmentSlotGroupList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10864,'/appointment/setup/appointmentslot/jsp/AppointmentSlotList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10865,'/appointment/setup/appointmentslot/jsp/AppointmentSlotSchdDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10866,'/appointment/setup/appointmentslot/jsp/ApptSlots.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10867,'/appointment/setup/appointmentslot/jsp/EditAppointmentSupplierCategory.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10868,'/appointment/setup/appointmentslot/jsp/FacilityApptSlotsCreate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11001,'/te/manual/planning/jsp/viewDistributionOrderSize.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11002,'/te/manual/planning/jsp/viewShipmentList.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11003,'/te/manual/planning/jsp/viewShipmentSize.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11004,'/te/manual/planning/jsp/viewStopResequenceDetails.xhtml','OSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11005,'/te/optimizationmgmt/consolidation/jsp/FilterListPopup.xhtml','ASHP',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11006,'/te/optimizationmgmt/consolidation/jsp/NewFilterPopup.xhtml','ASHP',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11007,'/te/parcelRate/ui/ParcelTransitTime.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11008,'/te/parcelRate/ui/ParcelZoneRate.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11009,'/te/filter/jsp/FilterList.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11010,'/te/filter/jsp/FilterOnFilters.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11011,'/te/manifest/ui/openpldfile.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11012,'/te/manual/planning/jsp/processSplitOrderPopup.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11013,'/te/manual/planning/jsp/SplitOrderPopup.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11014,'/te/manual/planning/msp/mspWorkspace.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11015,'/te/optimizationmgmt/consolidation/jsp/PrefixedOrderPresentation.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11016,'/te/optimizationmgmt/consolidation/jsp/PrefixedShipmentPresentation.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11017,'/te/optimizationmgmt/consolidation/jsp/ProcessConsolidationRun.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11018,'/te/optimizationmgmt/consolidation/jsp/ProcessReplanningOrders.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11019,'/te/optimizationmgmt/consolidation/jsp/ProcessSizeMap.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11020,'/te/optimizationmgmt/consolidation/jsp/RejectOrdersConfirmationPopup.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11021,'/te/optimizationmgmt/consolidation/jsp/ReplanOrdersConfirmationPopup.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11022,'/te/optimizationmgmt/consolidation/jsp/StartRunConfirmationPopup.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11023,'/te/optimizationmgmt/consolidation/jsp/TEConsolidationRunAnalysisOnGMap.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11024,'/te/optimizationmgmt/consolidation/jsp/TERunRejectionPage.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11025,'/te/optimizationmgmt/consolidation/jsp/ViewConsolidationWorkspace.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11026,'/te/optimizationmgmt/consolidation/jsp/ViewOrderPathAnalysis.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11033,'/te/order/jsp/OrderListPresentation.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11034,'/te/order/jsp/ProcessOrderListPresentation.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11035,'/te/ProcessMMtoVMConversion.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11037,'/te/shipserv/jsp/EditShipmentAssignedResources.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11038,'/te/shipserv/jsp/getAssignedShipments.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11039,'/te/shipserv/jsp/MovementDetails.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11040,'/te/shipserv/jsp/processShipmentAssignedResources.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11041,'/te/admin/parameter/jsp/ViewTEParameters.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11046,'/te/filter/jsp/FilterDetails.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11047,'/te/optimizationmgmt/consolidation/jsp/SelectConsolidationEngine.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11048,'/te/optimizationmgmt/consolidation/jsp/EditSizeMap.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11049,'/te/optimizationmgmt/consolidation/jsp/ParameterSetListPresentation.jsp','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11050,'/te/optimizationmgmt/consolidation/parameter/jsp/ParameterSetListPresentation.jsp ','CMA',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11051,'/manh/te/manual/planning/msp/mspWorkspace.jsp','MSHP',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11052,'/te/optimizationmgmt/consolidation/jsp/ConsolidationRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11053,'/te/optimizationmgmt/consolidation/jsp/ConsolidationRunTemplateListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11054,'/te/optimizationmgmt/consolidation/jsp/ConsolidationRunTemplateDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11055,'/te/optimizationmgmt/consolidation/jsp/ConsolidationRunListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11056,'/te/optimizationmgmt/consolidation/jsp/ProcessConsolidationRunTemplate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11057,'/te/admin/SurePostAddressDataLoader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11058,'/te/optimizationmgmt/consolidation/jsp/ProcessMultipleConsolidationRunTemplates.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11066,'/ilm/lookup/ui/ILMLocationLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11062,'/te/shipserv/jsp/teShipViaLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11065,'/te/optimizationmgmt/consolidation/jsp/StartMultipleRunConfirmationPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11067,'/ilm/lookup/ui/ILMLocationLookUpDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11068,'/ilm/lookup/ui/ILMSlotGroupLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11069,'/ilm/lookup/ui/ILMSlotGroupLookupDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11070,'/ilm/lookup/ui/ILMSlotLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11071,'/ilm/lookup/ui/ILMSlotLookupDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11072,'/ilm/lookup/ui/ILMYardLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11073,'/ilm/lookup/ui/ILMYardLookUpDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11074,'/ilm/lookup/ui/ILMYardLookUpInner.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11075,'/ilm/lookup/ui/ILMYardZoneLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11076,'/ilm/lookup/ui/ILMYardZoneLookUpDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11077,'/ilm/lookup/ui/ILMZoneSlotLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11078,'/ilm/lookup/ui/ILMZoneSlotLookUpDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11079,'/ilm/lookup/ui/TrailerLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11080,'/ilm/lookup/ui/TrailerLookUpDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11081,'/ilm/rfclient/layout/page_default_layout_RF_KeyBind.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11082,'/ilm/rfclient/layout/ProcessErrorsRF.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11083,'/ilm/rfclient/layout/RFCommonYMS.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11084,'/ilm/rfclient/ui/ProcessErrorRF.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11085,'/ilm/rfclient/ui/RFApptObjList.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11086,'/ilm/rfclient/ui/RFApptTypeList.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11087,'/ilm/rfclient/ui/RFCarrierList.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11088,'/ilm/rfclient/ui/RFCheckIn.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11089,'/ilm/rfclient/ui/RFCheckInApptObjScan.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11090,'/ilm/rfclient/ui/RFCheckInApptScan.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11091,'/ilm/rfclient/ui/RFCheckInApptTypeScan.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11092,'/ilm/rfclient/ui/RFCheckInCarrierScan.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11093,'/ilm/rfclient/ui/RFCheckInComments.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11094,'/ilm/rfclient/ui/RFCheckInLocnScan.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11095,'/ilm/rfclient/ui/RFCheckInTrailerScan.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11096,'/ilm/rfclient/ui/RFCheckOut.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10331,'/services/rest/contractmanagement/ShipViaService/shipviaservice/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10332,'/services/rest/contractmanagement/ShipViaService/shipviaservice/billShipViaMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10333,'/services/rest/contractmanagement/ShipViaService/shipviaservice/normalizationCurrency','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10334,'/services/rest/contractmanagement/ShipViaService/shipviaservice/trackingNumberRequiredMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10335,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10336,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10337,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10338,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10339,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/FuelTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10340,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/countryListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10341,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/RegionListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10342,'/services/rest/contractmanagement/FuelRateService/fuelRateService','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10343,'/services/rest/contractmanagement/FuelRateService/fuelRateService/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10344,'/services/rest/contractmanagement/FuelRateService/fuelRateService','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10345,'/services/rest/contractmanagement/FuelRateService/fuelRateService/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10346,'/services/rest/contractmanagement/FuelRateService/fuelRateService/CurrencyListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10347,'/services/rest/contractmanagement/FuelRateService/fuelRateService/NormalizationCurrencyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10348,'/services/rest/contractmanagement/FuelRateService/fuelRateService/RateUOMListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10349,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10650,'/basedata/rating/tariff/jsp/tariffZoneRateList.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10350,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/billShipviaLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10351,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10352,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/customerlookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10353,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/stateProvMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10354,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/countryMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10355,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10356,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/searchTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10357,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/errorTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10358,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/modeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10359,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierCompanyLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10360,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/shipperCompanyLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10361,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/railcarrierLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10362,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/payeeLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10363,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/businessUnitMapForLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10364,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/shipThroughLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10365,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/commodityLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10366,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10367,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10368,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10369,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10370,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10371,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10372,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10373,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10374,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10375,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10376,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/modes','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10377,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/serviceLevels','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10378,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/sizeUOMs','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10379,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/zones','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10380,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/comparisionMethods','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10381,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/baseUOMs','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10382,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10383,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10384,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10385,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10386,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10387,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getBusinessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10388,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getModeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10389,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getProtectionLevelMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10390,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getServiceLevelMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10391,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getTariffCodeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10392,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getServiceLevelMapForFilter','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10393,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getProtectionLevelMapForFilter','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10394,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getModeMapForFilter','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10395,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getBuMapForFilter','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10396,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10397,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10398,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10399,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10400,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getCurrencyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10401,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getPackageTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10402,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getCommodityClassMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10403,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getRateCalcMethodMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10404,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getSizeUOMMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10405,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10406,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10407,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10408,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10686,'/basedata/routingguide/jsp/test/GetLaneDetail.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10409,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/getZoneMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10410,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/getTimeUOMMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10411,'/services/rest/contractmanagement/MassExpirationService/massexpirationservices','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10412,'/services/rest/contractmanagement/MassExpirationService/massexpirationservices/updateRecord','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10413,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10414,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10415,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10416,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10417,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/getRateZonesList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10418,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/getZoneMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10419,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10420,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10421,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10422,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10423,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/carrCompanyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10424,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/shipCompanyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10425,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/companyTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10426,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10427,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10428,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10429,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10430,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10431,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10432,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10433,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10434,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10435,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10436,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10437,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10438,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10439,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10440,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10441,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10442,'/services/rest/contractmanagement/RailRoutService/railservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10443,'/services/rest/contractmanagement/RailRoutService/railservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10444,'/services/rest/contractmanagement/RailRoutService/railservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10445,'/services/rest/contractmanagement/RailRoutService/railservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10446,'/services/rest/contractmanagement/RailRoutService/railservice/railDetail','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10447,'/services/rest/contractmanagement/RailRoutService/railservice/railPath','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10448,'/services/rest/contractmanagement/RailRoutService/railservice/railSplc','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10651,'/basedata/rating/tariff/jsp/tariffZoneRatePopUp.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10652,'/basedata/rating/tariff/jsp/tariffZoneRateTier.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10653,'/basedata/rating/tariff/jsp/viewTariffZone.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10654,'/basedata/routingguide/jsp/add_attachment_file.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10655,'/basedata/routingguide/jsp/administerSurge.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10656,'/basedata/routingguide/jsp/carrierCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10657,'/basedata/routingguide/jsp/displayLaneDataErrors.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10658,'/basedata/routingguide/jsp/editLane.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10659,'/basedata/routingguide/jsp/editShippingParam.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10660,'/basedata/routingguide/jsp/editSurgePopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10661,'/basedata/routingguide/jsp/editTp.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10662,'/basedata/routingguide/jsp/ExportLanes.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10663,'/basedata/routingguide/jsp/in_progress_message.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10664,'/basedata/routingguide/jsp/insert_lanes.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10665,'/basedata/routingguide/jsp/insert_ra_lanes.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10666,'/basedata/routingguide/jsp/insert_rg_lanes.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10667,'/basedata/routingguide/jsp/laneCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10668,'/basedata/routingguide/jsp/laneDetails.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10669,'/basedata/routingguide/jsp/process_insert_lanes.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10670,'/basedata/routingguide/jsp/ProcessExportLanes.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10671,'/basedata/routingguide/jsp/processMassRGUpdate.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10672,'/basedata/routingguide/jsp/reviewLane.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10673,'/basedata/routingguide/jsp/shippingParamCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10674,'/basedata/routingguide/jsp/shippingParameterDetails.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10675,'/basedata/routingguide/jsp/shippingParametersContent.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10676,'/basedata/routingguide/jsp/shippingParametersPopUp.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10677,'/basedata/routingguide/jsp/shippingRulesPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10678,'/basedata/routingguide/jsp/surgeCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10679,'/basedata/routingguide/jsp/test/DecrementUse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10680,'/basedata/routingguide/jsp/test/GetAllResourceOptions.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10681,'/basedata/routingguide/jsp/test/GetAllUniqueCombResourceOptions.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10682,'/basedata/routingguide/jsp/test/GetAllUniqueResourceOptions.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10683,'/basedata/routingguide/jsp/test/GetCarrierDetail.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10684,'/basedata/routingguide/jsp/test/GetCarrierExists.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10685,'/basedata/routingguide/jsp/test/GetCombResourceOptions.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10687,'/basedata/routingguide/jsp/test/GetLaneHierarchy.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10688,'/basedata/routingguide/jsp/test/GetPrimaryTP.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10689,'/basedata/routingguide/jsp/test/GetRatingData.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10690,'/basedata/routingguide/jsp/test/GetRatingIgnoreAcc.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10691,'/basedata/routingguide/jsp/test/GetRepresentativeTP.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10692,'/basedata/routingguide/jsp/test/GetResourceOptions.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10693,'/basedata/routingguide/jsp/test/GetUse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10694,'/basedata/routingguide/jsp/test/IncrementUse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10695,'/basedata/routingguide/jsp/test/testGetRepresentativeTPResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10696,'/basedata/routingguide/jsp/tpDetails.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10697,'/basedata/routingguide/jsp/validationSummary.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10698,'/basedata/routingguide/jsp/wmAttributesPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10699,'/basedata/sailingLane/jsp/carrierCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10700,'/basedata/sailingLane/jsp/DateSailingSchedule.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10701,'/basedata/sailingLane/jsp/DateScheduleCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10702,'/basedata/sailingLane/jsp/DeleteSchedule.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10703,'/basedata/sailingLane/jsp/displayLaneDataErrors.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10704,'/basedata/sailingLane/jsp/EditDateSchedule.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10705,'/basedata/sailingLane/jsp/editLane.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10706,'/basedata/sailingLane/jsp/EditSailingCarrier.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10707,'/basedata/sailingLane/jsp/EditWeeklySchedule.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10708,'/basedata/sailingLane/jsp/insert_sa_lanes.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10709,'/basedata/sailingLane/jsp/laneCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10710,'/basedata/sailingLane/jsp/laneDetails.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10711,'/basedata/sailingLane/jsp/SailingScheduleInfo.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10712,'/basedata/sailingLane/jsp/SailingScheduleList.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10713,'/basedata/sailingLane/jsp/WeeklySailingSchedule.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10714,'/basedata/sailingLane/jsp/WeeklyScheduleCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10715,'/basedata/shipvia/createShipVia.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10716,'/basedata/shipvia/ShipViaList.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10717,'/basedata/staticroute/jsp/ActivateStaticRouteShipment.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10718,'/basedata/staticroute/jsp/EditDates.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10719,'/basedata/staticroute/jsp/EditDatesPopup.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10720,'/basedata/staticroute/jsp/EditStaticRoute.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10721,'/basedata/staticroute/jsp/EditStaticRouteCalendar.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10722,'/basedata/staticroute/jsp/NewStaticRoute.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10723,'/basedata/staticroute/jsp/NewStaticRouteCalendar.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10724,'/basedata/staticroute/jsp/ProcessEditDates.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10725,'/basedata/staticroute/jsp/ReloadStaticRouteDetails.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10726,'/basedata/staticroute/jsp/RuleBody.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10727,'/basedata/staticroute/jsp/RuleHeader.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10728,'/basedata/staticroute/jsp/RuleProcessor.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10729,'/basedata/staticroute/jsp/StaticRouteCalendarDetails.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10730,'/basedata/staticroute/jsp/StaticRouteCalendarList.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10731,'/basedata/staticroute/jsp/StaticRouteDetails.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10732,'/basedata/staticroute/jsp/StaticRouteFieldConfiguration.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10733,'/basedata/staticroute/jsp/StaticRouteList.xhtml','NMG',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10734,'/basedata/tariffimport/TariffImport.xhtml','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10735,'/basedata/tariffimport/TariffTransitTimeLookup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10736,'/basedata/tpparam/jsp/accessorial_details.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10737,'/basedata/tpparam/jsp/accessorial_summary_list.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10738,'/basedata/tpparam/jsp/AccessorialExclusionPopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10739,'/basedata/tpparam/jsp/CarrierParametersDetails.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10740,'/basedata/tpparam/jsp/CreateFuelMatrix.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10741,'/basedata/tpparam/jsp/CreateFuelMatrix_v1.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10742,'/basedata/tpparam/jsp/CreateFuelRate.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10743,'/basedata/tpparam/jsp/displayCarrierParameterDataErrors.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10744,'/basedata/tpparam/jsp/edit_accessorials.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10745,'/basedata/tpparam/jsp/EditAccessorialRates.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10746,'/basedata/tpparam/jsp/EditCmDiscounts.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10747,'/basedata/tpparam/jsp/EditCommodityClassTiers.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10748,'/basedata/tpparam/jsp/EditDeliveryAreaSurcharge.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10749,'/basedata/tpparam/jsp/EditFuelMatrix.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10750,'/basedata/tpparam/jsp/EditOversizeRate.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10751,'/basedata/tpparam/jsp/EditStopOffCharges.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10752,'/basedata/tpparam/jsp/fuelRatePopup.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10753,'/basedata/tpparam/jsp/process_create_accessorial.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10754,'/basedata/tpparam/jsp/Process_create_fuelMatrix.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10755,'/basedata/tpparam/jsp/process_edit_accessorial.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10756,'/basedata/tpparam/jsp/ProcessAccessorialExclusionCURDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10757,'/basedata/tpparam/jsp/ProcessAccessorialRatesCRUD.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10758,'/basedata/tpparam/jsp/ProcessCarrierParametersCRUD.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10759,'/basedata/tpparam/jsp/ProcessCmDiscountsCRUD.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10760,'/basedata/tpparam/jsp/ProcessDASRSCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10761,'/basedata/tpparam/jsp/ProcessEditCommodityClassTiers.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10762,'/basedata/tpparam/jsp/processFuelIndexCRUD.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10763,'/basedata/tpparam/jsp/processFuelRate.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10764,'/basedata/tpparam/jsp/ProcessOversizeRateCRUD.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10765,'/basedata/tpparam/jsp/ProcessStopOffChargesCRUD.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10766,'/basedata/tpparam/jsp/test_create_edit_delete_tp_parm.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10767,'/basedata/tpparam/jsp/test_create_edit_delete_tp_parm2.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10768,'/basedata/tpparam/jsp/viewCarrierParameterAuditTrail.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10769,'/basedata/tpparam/jsp/viewCarrierParameterSummary.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10770,'/basedata/xmlupload/jsp/add_attachment_file.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10771,'/basedata/xmlupload/jsp/in_progress_message.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10772,'/services/rest/contractmanagement/AccessorialDataService/*','CNTRMGT',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10773,'/services/rest/contractmanagement/RailRoutService/railservice/bulkDistanceUpdate','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10774,'/services/rest/contractmanagement/RailRoutService/railservice/getRailRoutErrorsLoged','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10775,'/basedata/driver/jsp/DriverActivitySummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10776,'/services/rest/contractmanagement/RailRoutService/railservice/distanceUOM','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10777,'/services/rest/contractmanagement/RailRoutService/railservice/isALKInstalled','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10778,'/services/rest/contractmanagement/SailingScheduleWeeklyService/*','CNTRMGT',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10779,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/*','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10781,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/carrierLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10782,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/billShipviaLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10783,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/zoneLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10784,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/customerlookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10785,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/stateProvMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10786,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/countryMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10787,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/zoneMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10788,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/searchTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10789,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/errorTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10790,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/modeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10791,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/carrierCompanyLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10792,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/shipperCompanyLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10793,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/railcarrierLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10794,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/payeeLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10795,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/businessUnitMapForLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10796,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/shipThroughLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10797,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/commodityLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10798,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/facilityLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10799,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/*','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10809,'/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getAppointmentsOfSlot','APPT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10801,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/errors','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10802,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/getCommodityCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10803,'/services/rest/ratequery/getRate','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10804,'/services/rest/contractmanagement/ContractManagementLookup/contractManagementLookup/shipviaLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10808,'/services/rest/appointment/AppointmentCaledarService/appointmentCaledarService/getCapacityUtilization','APPT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10810,'/appointment/admin/jsp/AppointmentImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10811,'/appointment/admin/parameter/jsp/EditAppointmentParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10812,'/appointment/admin/parameter/jsp/EditAppointmentParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10813,'/appointment/admin/parameter/jsp/EditParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10814,'/appointment/admin/parameter/jsp/EditParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10815,'/appointment/admin/parameter/jsp/ParametersInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10816,'/appointment/admin/parameter/jsp/ViewParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10817,'/appointment/import/ExportAppSetupData.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10818,'/appointment/import/ImportAppSetupDataLoader.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10819,'/appointment/infeasibility/dockEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10820,'/appointment/infeasibility/dockEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10821,'/appointment/infeasibility/dockEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10822,'/appointment/infeasibility/dockEquipment/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10823,'/appointment/infeasibility/dockProductClass/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10824,'/appointment/infeasibility/dockProductClass/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10825,'/appointment/infeasibility/dockProductClass/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10826,'/appointment/infeasibility/dockProductClass/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10827,'/appointment/infeasibility/dockProtectionLevel/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10828,'/appointment/infeasibility/dockProtectionLevel/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10829,'/appointment/infeasibility/dockProtectionLevel/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10830,'/appointment/infeasibility/dockProtectionLevel/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10831,'/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10832,'/appointment/infeasibility/jsp/AppointmentTypeDockInfeasibilitySetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10833,'/appointment/infeasibility/jsp/relatedCompanyId.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10834,'/appointment/infeasibility/jsp/SlotAppointmentTypeInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10835,'/appointment/infeasibility/jsp/SlotLoadConfigTypeInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10836,'/appointment/infeasibility/jsp/SlotProductClassInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10837,'/appointment/infeasibility/jsp/UpdateDockAppointmentTypeInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10838,'/appointment/infeasibility/jsp/UpdateSlotAppointmentTypeInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10839,'/appointment/infeasibility/jsp/UpdateSlotDockInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10840,'/appointment/infeasibility/jsp/UpdateSlotEquipmentInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10841,'/appointment/infeasibility/jsp/UpdateSlotLoadConfigTypeInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10842,'/appointment/infeasibility/jsp/UpdateSlotProductClassInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10843,'/appointment/infeasibility/jsp/UpdateSlotProtectionLevelInfeasibility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10844,'/appointment/lookup/jsp/apptPOLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9796,'/basedata/customerserv/view/CustomerDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9797,'/basedata/customerserv/view/CustomerEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9798,'/basedata/payee/PayeeEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9799,'/docManagementFileServlet.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9800,'/report.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9801,'/basedataEquipmentCreateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9802,'/basedataEquipmentUpdateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9803,'/basedataEquipmentListServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9804,'/basedataFacilityDockCreateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9805,'/basedataFacilityDockUpdateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9806,'/basedataFacilityDockHoursCreateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9807,'/basedataFacilityDockHoursUpdateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9808,'/basedataFacilityDockListServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9809,'/basedataAdvanceFilterDispatcherServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9810,'/basedataRegionCreateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9811,'/basedataRegionUpdateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9812,'/basedataRegionListServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9813,'/ilsFilterDispatcherServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9814,'/basedata/ajax.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9815,'/oblcontrollerServlet.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9816,'/basedataAdvanceFilterLoader.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9817,'/basedata/payee/PayeeDetail.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9818,'/basedata/payee/PayeeEditContact.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9819,'/basedata/payee/PayeeContact.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9820,'/cbo/rules/CBORuleColmList.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9822,'/cbo/alert/view/ApAlertDefView.jsflps','BMD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9823,'/basedata/util/ProcessVendorLogin.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9824,'/cbo/alert/view/ApAlertDefEdit.jsflps','BMD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9826,'/cbo/CreateShipment.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9827,'/cbo/EditShipmentDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9828,'/cbo/POList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9829,'/cbo/processStatusInquiry.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9830,'/cbo/PurchaseOrderCreate.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9831,'/cbo/PurchaseOrderEdit.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9832,'/cbo/SubmitCreatePO.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9833,'/cbo/SubmitCreateShipment.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9834,'/cbo/SubmitEditPO.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9835,'/cbo/SubmitShipmentDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9836,'/cbo/transactional/shipment/view/ViewDependentShipmentList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9837,'/cbo/VcpPurchaseOrderCreate.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9838,'/cbo/VcpPurchaseOrderEdit.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9839,'/cbo/ViewDependentShipmentList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9840,'/cbo/ViewPODetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9841,'/cbo/ViewShipmentDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9842,'/cbo/ViewShipmentList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9843,'/distributionorder/distributionOrderDetails.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9862,'/services/rest/cbobase/ItemLookupService/itemlookup/mask','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9863,'/cbo/classificationcode/ClassificationCodeLookupPopup.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9864,'/basedata/unnumber/view/UnNumberEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9865,'/basedata/unnumber/view/UnNumberDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9866,'/cbo/rules/CBORuleColmRef.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9867,'/lcom/index.jsp','CBO',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9868,'/cbotransactional/drillDown.serv/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9869,'/cbo/massupdate/view.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9870,'/cbo/shipperselection/ShipperSelection.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9872,'/cbo/transactional/lookup/idLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9873,'/docMgtFileDownload.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9874,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/activeCurrencyTypeList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9875,'/cbo/ZipCode/*','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 9877,'/cbo/shipperselection/ShipperSelectionDefault.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10642,'/basedata/rating/tariff/jsp/tariffLaneList.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10643,'/basedata/rating/tariff/jsp/tariffLanePopUp.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10644,'/basedata/rating/tariff/jsp/tariffTierCRUDResponse.jsp','CNTRMGT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10165,'/services/rest/contractmanagement/LaneUIServices/laneUIServices','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10166,'/services/rest/contractmanagement/LaneUIServices/*','CNTRMGT',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10167,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10168,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/incotermList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10169,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/billingMethodList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10170,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/countryListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10171,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/originStateProvMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10172,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/stateProvMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10173,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/zoneMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10174,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/frequencyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10175,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/rGQualifier','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10176,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/exportLane','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10177,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/exportAllLane','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10178,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/validateLanes','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10179,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/validationErrorDetails','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10180,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10181,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10182,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10183,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10184,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10188,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/weekDaysList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10189,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10190,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10191,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10192,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10193,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/routingTierList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10194,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/carrierRejectionPeriodList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10195,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/cubingFactorList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10196,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/sailingFrequencyTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10197,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/fixedTransitTimeUOMList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10198,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/adjustLaneDetails','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10199,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/capacityCommitmentSizeUOMMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10200,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10201,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/*','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10202,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 10203,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/*','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11097,'/ilm/rfclient/ui/RFCloseTrailer.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11098,'/ilm/rfclient/ui/RFError.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11099,'/ilm/rfclient/ui/RFLocations.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11100,'/ilm/rfclient/ui/RFMenu.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11101,'/ilm/rfclient/ui/RFMoveTask.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11102,'/ilm/rfclient/ui/RFRequestTask.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11103,'/ilm/rfclient/ui/RFSealTask.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11104,'/ilm/rfclient/ui/RFTranSearch.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11105,'/ilm/rfclient/ui/RFYard.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11106,'/ilm/rfclient/ui/RFYardAudit.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11107,'/ilm/task/ui/IlmSCError.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11108,'/ilm/task/ui/IlmTaskAuditList.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11109,'/ilm/task/ui/IlmTaskDetails.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11110,'/ilm/task/ui/YALocationDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11111,'/ilm/task/ui/YALocationLookup.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11112,'/ilm/task/ui/YardDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11113,'/ilm/task/ui/YardLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11114,'/ilm/trailer/ui/CloseTrailer.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11115,'/ilm/trailer/ui/TrailerAlrtPopup.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11116,'/ilm/trailer/ui/TrailerAuditList.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11117,'/ilm/trailer/ui/TrailerPopUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11118,'/ilm/trailer/ui/YmAssignedLocationLookup.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11119,'/ilm/trailer/ui/YmLocationDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11120,'/ilm/trailer/ui/YmLocationLookup.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11121,'/ilm/trailer/ui/YmSlotLookup.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11122,'/ilm/util/jsf/ymsSoftcheckErrors.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11123,'/ilm/util/jsf/ymsSoftcheckErrorsTask.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11124,'/ilm/yard/ui/YardSlotPopUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11125,'/services/rest/yms/CheckInService/checkInService/*','YARD',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11126,'/services/rest/yms/CheckInService/checkInService/searchAppointment','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11127,'/services/rest/yms/CheckInService/checkInService/loadCheckinData','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11128,'/services/rest/yms/CheckInService/checkInService/determineLocation','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11129,'/services/rest/yms/CheckInService/checkInService/getDriverDatabyName','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11130,'/services/rest/yms/CheckInService/checkInService/getFacility','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11131,'/services/rest/yms/CheckInService/checkInService/getTrailerInfo','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11132,'/services/rest/yms/CheckInService/checkInService/getDriverNamesLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11133,'/services/rest/yms/CheckInService/checkInService/getCustomAttributes','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11134,'/services/rest/yms/CheckInLookupService/lookupService/getPOLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11135,'/services/rest/yms/CheckInLookupService/lookupService/getASNLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11136,'/services/rest/yms/CheckInLookupService/lookupService/getShipmentLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11137,'/services/rest/yms/CheckInLookupService/lookupService/getAsnBolLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11138,'/services/rest/yms/CheckInLookupService/lookupService/getTrailerLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11139,'/services/rest/yms/CheckInLookupService/lookupService/getControlNumberLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11140,'/services/rest/yms/CheckInLookupService/lookupService/getAppointmentLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11141,'/services/rest/yms/CheckInLookupService/lookupService/getShipmentBolLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11142,'/services/rest/yms/CheckInLookupService/lookupService/getFacilityLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11143,'/services/rest/yms/CheckInLookupService/lookupService/getLocationLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11144,'/services/rest/yms/CheckInLookupService/lookupService/getTrailerTypeLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11145,'/services/rest/yms/CheckInLookupService/lookupService/getCarrierLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11146,'/services/rest/yms/CheckInLookupService/lookupService/getTractorLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11147,'/services/rest/yms/CheckInTransParamConfigurationService/TransConfigService/getTransParams','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11148,'/services/rest/yms/DropDownService/dropDownService/getAppointmentType','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11149,'/services/rest/yms/DropDownService/dropDownService/getTrailerCondition','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11150,'/services/rest/yms/DropDownService/dropDownService/getCountryCodes','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11151,'/services/rest/yms/DropDownService/dropDownService/getStateCodes','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11152,'/services/rest/yms/DropDownService/dropDownService/getTrailerLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11153,'/ilm/BusinessRulesIndex.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11154,'/ilm/SetupIndex.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11155,'/ilm/admin/jsp/CheckInImport.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11156,'/ilm/admin/jsp/CloseTrailerImport.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11157,'/ilm/admin/jsp/DevTools.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11158,'/ilm/admin/jsp/processSvgSetup.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11159,'/ilm/admin/jsp/TrailerImport.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11160,'/ilm/admin/jsp/yardViewSetup.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11161,'/ilm/admin/parameter/jsp/EditILMParameters.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11162,'/ilm/admin/parameter/jsp/EditILMParametersProcess.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11163,'/ilm/admin/parameter/jsp/EditParameters.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11164,'/ilm/admin/parameter/jsp/EditParametersProcess.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11165,'/ilm/admin/parameter/jsp/ParametersInclude.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11166,'/ilm/admin/parameter/jsp/ViewParameters.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11167,'/ilm/alert/jsp/alertAction.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11168,'/ilm/alert/jsp/AlertDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11169,'/ilm/alert/jsp/alertList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11170,'/ilm/checkincheckout/jsp/CheckInConfirmation.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11171,'/ilm/checkincheckout/jsp/CheckInDetailsOverride.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11172,'/ilm/checkincheckout/jsp/CheckInEmptyDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11173,'/ilm/checkincheckout/jsp/CheckInOutDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11174,'/ilm/checkincheckout/jsp/CheckInOutList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11175,'/ilm/checkincheckout/jsp/processEditCheckInDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11176,'/ilm/checkincheckout/jsp/processEditCheckOutDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11177,'/ilm/checkinout/jsp/CheckInEmptyDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11178,'/ilm/checkinout/jsp/CheckInInbound.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11179,'/ilm/checkinout/jsp/CheckInOutDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11180,'/ilm/checkinout/jsp/CheckInOutList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11181,'/ilm/checkinout/jsp/CheckInReverse.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11182,'/ilm/checkinout/jsp/processEditCheckInDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11183,'/ilm/checkinout/jsp/processEditCheckOutDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11184,'/ilm/checkinout/jsp/processSubmitEditCheckInDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11185,'/ilm/checkinout/jsp/processSubmitEditCheckOutDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11186,'/ilm/dockdoor/jsp/DockDoor.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11187,'/ilm/dockdoor/jsp/dockDoorAuditTrail.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11188,'/ilm/dockdoor/jsp/dockDoorEdit.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11189,'/ilm/planning/jsp/DockDoorPlanning.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11190,'/ilm/planning/jsp/DockDoorPlanningEdit.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11191,'/ilm/rfclient/RFLogin/NoPermission.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11192,'/ilm/rfclient/RFLogin/ProcessLogin.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11193,'/ilm/rfclient/RFLogin/RFError.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11194,'/ilm/rfclient/RFLogin/RFLogin.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11195,'/ilm/setup/facilityyard/jsp/createFacilityYardZone.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11196,'/ilm/setup/facilityyard/jsp/processFacilityYard.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11197,'/ilm/setup/facilityyard/jsp/processFacilityYardZone.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11198,'/ilm/setup/feasibility/jsp/NTDFeasibilitySetup.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11199,'/ilm/setup/feasibility/jsp/YardZonePrioritySetup.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11200,'/ilm/setup/locationproximity/jsp/LocationProximityListPresentation.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11201,'/ilm/setup/locationproximity/jsp/ProcessCreateLocationProximity.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11202,'/ilm/setup/locationproximity/jsp/ProcessLocationProximityDelete.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11203,'/ilm/setup/lookup/yardLookup.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11204,'/ilm/setup/lookup/yardZoneLookup.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11806,'/ofr/optimizationmgmt/consolidation/consolidationregion/jsp/ConsolidationRegionSummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11807,'/ofr/optimizationmgmt/consolidation/enginespecific/mmsp/jsp/EngineSpecificRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11808,'/ofr/optimizationmgmt/consolidation/jsp/PFLTOrderList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11809,'/ofr/optimizationmgmt/consolidation/jsp/RunTemplatePresentation.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11810,'/ofr/optimizationmgmt/consolidation/jsp/ShipmentListPresentation.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11811,'/ofr/optimizationmgmt/consolidation/orphanedorderreasoncode/ui/OrphanedOrderReasonCodeExDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11812,'/ofr/order/jsp/bulkOrderTemplateODPairDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11813,'/ofr/order/jsp/ChildOrderListView.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11814,'/ofr/order/jsp/createOrderLineItemList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11815,'/ofr/order/jsp/editBudgetedCost.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11816,'/ofr/order/jsp/EditDTTMOrders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11817,'/ofr/order/jsp/LineItemListView.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11818,'/ofr/order/jsp/MAUIProcessOrderListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11819,'/ofr/order/jsp/OrderAdditionalInformation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11820,'/ofr/order/jsp/OrderAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11821,'/ofr/order/jsp/OrderDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11822,'/ofr/order/jsp/OrderListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11823,'/ofr/order/jsp/OrderSplitIds.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11824,'/ofr/order/jsp/orderUtils.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11825,'/ofr/order/jsp/processEditBC.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11826,'/ofr/order/jsp/processEditDTTMOrders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11827,'/ofr/order/jsp/processEditOrder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11828,'/ofr/order/jsp/PurchaseOrderLineItemListView.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11829,'/ofr/order/jsp/singleOrderODPairDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11830,'/ofr/order/jsp/SplitOrderDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11831,'/ofr/order/jsp/ViewOrderSummary.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11832,'/ofr/order/ui/Buttons.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11833,'/ofr/order/ui/Menu.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11834,'/ofr/order/ui/OrderList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11835,'/ofr/order/ui/OrderListPresentation.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11836,'/ofr/order/ui/ScenarioSummary.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11837,'/ofr/order/ui/SelectedOrderSummary.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11838,'/ofr/parcel/manifest/ParcelManifestDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11839,'/ofr/parcel/manifest/ParcelManifestList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11840,'/ofr/parcel/manifest/ParcelManifestLPNList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11841,'/ofr/purchaseorder/jsp/ProcessAggregatePO.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11842,'/ofr/purchaseorder/jsp/ProcessPOList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11843,'/ofr/purchaseorder/jsp/PurchaseOrderList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11844,'/ofr/rating/jsp/RatingAccessorialAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11845,'/ofr/rating/jsp/RatingRateAuditTrail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11846,'/ofr/rating/jsp/ViewRateSummary.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11847,'/ofr/ReportingIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11848,'/ofr/RFScreensIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11849,'/ofr/rs/jsp/businessHours.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11850,'/ofr/rs/jsp/businessHoursExceptionsList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11851,'/ofr/rs/jsp/CarrierRejectPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11852,'/ofr/rs/jsp/revertCarrierReject.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11853,'/ofr/rs/jsp/shipperPerformanceFactorValues.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11854,'/ofr/rs/jsp/shipperStaticPerformanceFactors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11855,'/ofr/rsarea/jsp/RSFacilityListBox.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11856,'/ofr/rsarea/jsp/RSFacilityQuickFilter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11857,'/ofr/rsarea/jsp/shipperResourceArea.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11858,'/ofr/sample/jsp/CustomSample.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11859,'/ofr/sample/jsp/OrderSample.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11860,'/ofr/sample/jsp/SetProps.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11861,'/ofr/sample/jsp/ValidateXML.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11862,'/ofr/SelectMenuIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11863,'/ofr/SetupIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11864,'/ofr/shipmentatt/ShipmentAttributeQuery.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11865,'/ofr/shipmentatt/ShipmentAttributeResp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11866,'/ofr/shipmentloaddetail/ShipmentLoadDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11867,'/ofr/shipmentloaddetail/ShipmentLoadDetailColumns.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11868,'/ofr/shipmentloaddetail/ShipmentLoadDetailForTripSheet.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11869,'/ofr/shipserv/jsp/AddLegsToExistingShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11870,'/ofr/shipserv/jsp/add_attachment_file.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11871,'/ofr/shipserv/jsp/AggregateConfirmation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11872,'/ofr/shipserv/jsp/AssignOrdersToNetwork.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11873,'/ofr/shipserv/jsp/AuditTrailFieldSelection.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11874,'/ofr/shipserv/jsp/CancelShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11875,'/ofr/shipserv/jsp/CreateDynamicPathPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11876,'/ofr/shipserv/jsp/CreateShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11877,'/ofr/shipserv/jsp/DeleteTemplateConfirmation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11878,'/ofr/shipserv/jsp/DemoteConfirmation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11879,'/ofr/shipserv/jsp/dependentShipmentAnalysis.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11880,'/ofr/shipserv/jsp/DisplayStopDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11881,'/ofr/shipserv/jsp/EditBillTo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11882,'/ofr/shipserv/jsp/EditComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11883,'/ofr/shipserv/jsp/EditCustomFields.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11884,'/ofr/shipserv/jsp/EditInformationalOrder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11885,'/ofr/shipserv/jsp/EditStopDTTM.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11886,'/ofr/shipserv/jsp/EditStopList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11887,'/ofr/shipserv/jsp/EditStopWInfoOrders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11888,'/ofr/shipserv/jsp/EditStopWRealOrders.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11889,'/ofr/shipserv/jsp/FilterCriteriaPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11890,'/ofr/shipserv/jsp/FilterCriteriaPresentationResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11891,'/ofr/shipserv/jsp/getAssignedBookings.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11892,'/ofr/shipserv/jsp/insert_swfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11893,'/ofr/shipserv/jsp/invokeTXMLProcessingService.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11894,'/ofr/shipserv/jsp/invokeTXMLProcessingServiceWithoutScheduler.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11895,'/ofr/shipserv/jsp/MassResourceAssignment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11896,'/ofr/shipserv/jsp/MassUpdatePO.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11897,'/ofr/shipserv/jsp/MovementDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11898,'/ofr/shipserv/jsp/MultiOrderPathAnalysis.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11899,'/ofr/shipserv/jsp/PathAnalysis.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11900,'/ofr/shipserv/jsp/PathAnalysisAssignShipments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11901,'/ofr/shipserv/jsp/PopupOrderList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11902,'/ofr/shipserv/jsp/Process.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11903,'/ofr/shipserv/jsp/ProcessAggregateResponseNew.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11904,'/ofr/shipserv/jsp/ProcessBillTo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11905,'/ofr/shipserv/jsp/ProcessBulkShipmentSoftChecks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11906,'/ofr/shipserv/jsp/ProcessCreateComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11907,'/ofr/shipserv/jsp/ProcessDynamicPath.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11908,'/ofr/shipserv/jsp/ProcessEditCustFields.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11909,'/ofr/shipserv/jsp/processEditDTTM.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11910,'/ofr/shipserv/jsp/ProcessEditStop.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11911,'/ofr/shipserv/jsp/ProcessMassResourceAssignment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11912,'/ofr/shipserv/jsp/ProcessPathAnalysis.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11913,'/ofr/shipserv/jsp/ProcessPOUpdate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11914,'/ofr/shipserv/jsp/ProcessReceiveShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11915,'/ofr/shipserv/jsp/processSaveShipmentLoadDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11916,'/ofr/shipserv/jsp/ProcessSendToWMS.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11917,'/ofr/shipserv/jsp/ProcessShipmentCriticalChangeResult.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11918,'/ofr/shipserv/jsp/ProcessShipmentSoftChecks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11919,'/ofr/shipserv/jsp/ProcessShipperSizesOverride.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11920,'/ofr/shipserv/jsp/ProcessUpdateInventory.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11921,'/ofr/shipserv/jsp/ProcessUpdateShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11922,'/ofr/shipserv/jsp/Progress.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11923,'/ofr/shipserv/jsp/ProgressPOUpdate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11924,'/ofr/shipserv/jsp/PromoteConfirmation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11205,'/ilm/setup/lookup/yardZoneSlotLookup.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11206,'/ilm/setup/yard/jsp/createYardZone.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11207,'/ilm/setup/yard/jsp/createYardZoneSlot.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11208,'/ilm/setup/yard/jsp/editYard.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11209,'/ilm/setup/yard/jsp/editYardZone.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11210,'/ilm/setup/yard/jsp/editYardZoneSlot.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11211,'/ilm/setup/yard/jsp/processYardCreate.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11212,'/ilm/setup/yard/jsp/processYardList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11213,'/ilm/setup/yard/jsp/processYardWithFacility.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11214,'/ilm/setup/yard/jsp/processYardZoneCreate.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11215,'/ilm/setup/yard/jsp/processYardZoneList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11216,'/ilm/setup/yard/jsp/processYardZoneSlotCreate.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11217,'/ilm/setup/yard/jsp/processYardZoneSlotList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11218,'/ilm/setup/yard/jsp/yardEdit.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11219,'/ilm/setup/yard/jsp/yardZoneEdit.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11220,'/ilm/setup/yard/jsp/yardZoneList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11221,'/ilm/setup/yard/jsp/yardZoneSlotEdit.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11222,'/ilm/setup/yard/jsp/yardZoneSlotList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11223,'/ilm/setup/yardtpcompany/jsp/editYardTCE.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11224,'/ilm/setup/yardtpcompany/jsp/processYardTCECreate.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11225,'/ilm/setup/yardtpcompany/jsp/processYardTCEList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11226,'/ilm/softcheckerrors/jsp/SoftCheckErrors.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11227,'/ilm/svg/AllTrailers.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11228,'/ilm/svg/jsp/AllTrailers.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11229,'/ilm/svg/jsp/SearchTrailers.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11230,'/ilm/task/jsp/CreateMoveTaskForShipment.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11231,'/ilm/task/jsp/NewTask.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11232,'/ilm/task/jsp/taskAuditTrail.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11233,'/ilm/task/jsp/TaskEdit.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11234,'/ilm/task/jsp/TaskList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11235,'/ilm/task/jsp/TaskRuleSetup.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11236,'/ilm/task/jsp/YardAuditHistory.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11237,'/ilm/trailer/CreateMoveTask.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11238,'/ilm/trailer/jsp/closeTrailer.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11239,'/ilm/trailer/jsp/CloseTrailerDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11240,'/ilm/trailer/jsp/CreateMoveTask.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11241,'/ilm/trailer/jsp/CreateMoveTaskSearch.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11242,'/ilm/trailer/jsp/ILMSVGTrailer.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11243,'/ilm/trailer/jsp/ILMTrailer.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11244,'/ilm/trailer/jsp/ILMTrailerActivityTrail.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11245,'/ilm/trailer/jsp/processEditTrailer.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11246,'/ilm/trailer/jsp/saveTrailerDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11247,'/ilm/trailer/jsp/testTrailer.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11248,'/ilm/trailer/jsp/trailerAuditTrail.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11249,'/ilm/trailer/jsp/trailerDetails.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11250,'/ilm/yard/jsp/Yard.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11251,'/ilm/yard/jsp/YardEdit.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11252,'/ilm/yardmgmt/Process_Trailer.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11253,'/ilm/yardmgmt/TrailerTab.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11254,'/ilm/checkinout/ui/AppointmentPopUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11255,'/ilm/checkinout/ui/CheckInConfirm.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11256,'/ilm/checkinout/ui/CheckInDetails.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11257,'/ilm/checkinout/ui/CheckInLocationLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11258,'/ilm/checkinout/ui/CheckInPickUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11259,'/ilm/checkinout/ui/CheckInSCOverrides.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11260,'/ilm/checkinout/ui/CheckOutDetails.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11261,'/ilm/dockdoor/jsflps/DockDoorAuditTrail.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11262,'/ilm/dockdoor/jsflps/MoveTaskAlert.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11263,'/ilm/dockdoor/jsflps/YesNoCancel.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11264,'/ilm/lookup/ui/ApptPOLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11265,'/ilm/lookup/ui/ApptPOLookUpDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11266,'/ilm/lookup/ui/defaultLookupTemplate.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11267,'/ilm/lookup/ui/ILMDockDoorLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11268,'/ilm/lookup/ui/ILMDockDoorLookUpDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11269,'/ilm/lookup/ui/ILMDockLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11270,'/ilm/lookup/ui/ILMDockLookUpDialog.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11271,'/ilm/lookup/ui/ILMDockLookUpInner.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11272,'/ilm/lookup/ui/ILMDriverLookUp.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11273,'/ilm/svg/jsp/AllTrailersGraphical.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11274,'/ilm/svg/jsp/AllTrailerGraphicalConfiguration.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11275,'/ilm/allTrailers.do','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11276,'/ilm/svg/jsf/graphicalYard.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11277,'/ilm/trailer/jsp/TrailerDetailsPopup.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11278,'/reports/ui/printReport.jsflps','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11279,'/ilm/trailer/ui/IlmTrailersList.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11280,'/services/rest/yms/DropDownService/dropDownService/getCarrierList','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11281,'/services/rest/yms/CheckInService/checkInService/getDock','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11282,'/services/rest/yms/CheckInLookupService/lookupService/getDriverLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11283,'/services/rest/yms/CheckInService/checkInService','YARD',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11285,'/services/rest/yms/CheckInService/checkInService/getDefaultCountry','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11286,'/services/rest/yms/CheckInService/checkInService/getAppointmentLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11287,'/ilm/setup/yard/ui/YardZoneSlot.xhtml','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11289,'/ilm/setup/feasibility/jsp/ProcessCreateLocationProximity.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11293,'/ilm/setup/feasibility/jsp/LocationProximityListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11294,'/services/rest/yms/DropDownService/dropDownService/getPickupAppointmentType','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11295,'/services/rest/yms/CheckInService/checkInService/getPickupAppointmentLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11296,'/services/rest/yms/CheckInService/checkInService/getTrailerLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11297,'/services/rest/yms/CheckInService/checkInService/getPickupTrailerLookup','YARD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11298,'/ilm/task/ui/IlmYardAuditDetail.jsflps','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11299,'/services/rest/pod/*','POD',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11300,'/services/rest/pod/*','POD',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11301,'/services/rest/pod/UODService/*','POD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11302,'/services/rest/pod/UODService/*','POD',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11303,'/services/rest/pod/PODCommandService/*','POD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11304,'/services/rest/pod/PODCommandService/*','POD',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11305,'/services/rest/pod/PODCommandService/*','POD',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11306,'/services/rest/pod/UODService/*','POD',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11307,'/services/rest/pod/*','POD',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11309,'/ofr/optimizationmgmt/consolidation/orphanedorderreasoncode/ui/OrphanedOrderReasonCode.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11310,'/ofr/optimizationmgmt/consolidation/orphanedorderreasoncode/ui/OrphanedOrderReasonCodeDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11311,'/ofr/optimizationmgmt/consolidation/orphanedorderreasoncode/ui/OrphanedOrderReasonCodeEx.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11312,'/ofr/optimizationmgmt/consolidation/orphanedorderreasoncode/ui/OrphanedOrderReasonCodeExDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11313,'/basedata/carrierserv/driver/jsp/RaDriverCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11315,'/ofr/ra/jsp/SaveAllReasonCodes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11316,'/obl/devtools/checkRequest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11317,'/obl/devtools/dbbackup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11318,'/obl/devtools/debugOff.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11319,'/obl/devtools/debugOn.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11320,'/obl/devtools/devtoolglobalheader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11321,'/obl/devtools/devTools.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11322,'/obl/devtools/devtoolslogin.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11323,'/obl/devtools/FileUploader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11324,'/obl/devtools/invokeTXMLProcessingService.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11325,'/obl/devtools/JMSUtil.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11326,'/obl/devtools/pendingLaneBids.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11327,'/obl/devtools/pe_sm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11328,'/obl/devtools/procbackup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11329,'/obl/devtools/processdevtoolslogin.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11330,'/obl/devtools/processLaneBids.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11331,'/obl/devtools/processSetupDebugLog.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11332,'/obl/devtools/processUploadCompanyLogo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11333,'/obl/devtools/proclist.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11334,'/obl/devtools/query.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11335,'/obl/devtools/queryStatic.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11336,'/obl/devtools/queueOff.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11337,'/obl/devtools/queueOn.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11338,'/obl/devtools/rebuildhashtable.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11339,'/obl/devtools/reloadHelpFileProperties.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11340,'/obl/devtools/setupDebugLog.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11341,'/obl/devtools/synch_company_status.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11342,'/obl/devtools/systemInfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11343,'/obl/devtools/toolfordev.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11344,'/obl/devtools/trackingOff.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11345,'/obl/devtools/trackingOn.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11346,'/obl/devtools/update_company_name.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11347,'/obl/devtools/uploadCompanyLogo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11348,'/obl/devtools/ViewDBConnections.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11349,'/obl/devtools/viewDebugLog.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11350,'/obl/devtools/viewLog.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11351,'/obl/devtools/viewUCLDBConnections.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11353,'/obl/filter/jsp/FilterOnFilters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11354,'/obl/oblLogout.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11355,'/obl/preLogout.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11356,'/obl/rfp/lane/administer_lane_equipment_type.jsp.old','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11357,'/obl/rfp/lane/carrierdiscounts/CarrierDiscount.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11358,'/obl/rfp/lane/carrierdiscounts/CarrierDiscountAuditTrial.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11359,'/obl/rfp/lane/carrierdiscounts/CriteriaListColumns.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11360,'/obl/rfp/lane/carrierdiscounts/DiscountGroupDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11361,'/obl/rfp/lane/carrierdiscounts/DiscountGroupList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11362,'/obl/rfp/lane/carrierdiscounts/DiscountsCriteriaList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11363,'/obl/rfp/lane/carrierdiscounts/DiscountsCriteriaListForSpecifyDiscounts.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11364,'/obl/rfp/lane/carrierdiscounts/HardCheckErrors.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11365,'/obl/rfp/lane/carrierdiscounts/LaneList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11366,'/obl/rfp/lane/carrierdiscounts/SpecifyDiscounts.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11367,'/obl/rfp/lane/carrierdiscounts/ViewCriteria.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11368,'/obl/rfp/lane/carrierdiscounts/ViewDiscountsCriterion.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11370,'/obl/rfp/lane/view_benchmark_rates.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11371,'/obl/scenario/CompareByCarrier.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11372,'/obl/scenario/ScenarioadjustmentList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11373,'/obl/scenario/ScenarioCompareByLaneAndCarrierList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11374,'/obl/scenario/ScenarioCompareByLaneList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11375,'/obl/scenario/ScenarioList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11376,'/obl/xml/upload/uploadXML.jsp.SynchronousUpload','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11377,'/fm/admin/parameter/fleet/jsp/EditFleetParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11378,'/fm/admin/parameter/fleet/jsp/EditFleetParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11379,'/fm/admin/parameter/jsp/EditParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11380,'/fm/admin/parameter/jsp/EditParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11381,'/fm/admin/parameter/jsp/ParametersInclude.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11382,'/fm/admin/parameter/jsp/ViewParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11383,'/fm/alert/DispatchTripAlert.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11384,'/fm/alert/jsp/ProcessAlertListActions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11385,'/fm/alert/jsp/TripAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11386,'/fm/debrief/QDWEndTripBlank.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11387,'/fm/DevTools/jsp/createSegmentFromShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11388,'/fm/DevTools/jsp/FMDevTools.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11389,'/fm/DevTools/jsp/FMWebserviceTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11390,'/fm/DevTools/jsp/ProcessFMWebserviceTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11391,'/fm/DevTools/jsp/ProcessXMLCommManagerImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11392,'/fm/DevTools/jsp/ShowOptRegionMapping.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11393,'/fm/DevTools/jsp/StartTandemDispatchPreprocessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11394,'/fm/DevTools/jsp/ViewHOSLog.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11395,'/fm/DevTools/jsp/XMLCommManagerImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11396,'/fm/dialog/FMDialog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11397,'/fm/driver/DriverHolidayCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11398,'/fm/driver/DSPDriverActivityHistory.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11399,'/fm/driver/QDWCheckInOut.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11400,'/fm/driver/QDWDriverPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11401,'/fm/driverkiosk/KioskAcceptedRecommendedTripList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11402,'/fm/driverkiosk/KioskOtherTripList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11403,'/fm/driverkiosk/overrideKioskCheckInOut.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11404,'/fm/driverkiosk/processPreCheckIn.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11405,'/fm/filter/jsp/ApplyDefault.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11406,'/fm/filter/jsp/FilterDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11407,'/fm/filter/jsp/FilterError.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11408,'/fm/filter/jsp/FilterList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11409,'/fm/filter/jsp/FilterOnFilters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11410,'/fm/infeasibility/driverEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11411,'/fm/infeasibility/driverEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11412,'/fm/infeasibility/driverEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11413,'/fm/infeasibility/driverEquipment/jsp/ProcessUpdateInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11414,'/fm/infeasibility/driverEquipment/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11415,'/fm/infeasibility/driverFacility/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11416,'/fm/infeasibility/driverFacility/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11417,'/fm/infeasibility/driverFacility/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11418,'/fm/infeasibility/driverFacility/jsp/ProcessUpdateInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11419,'/fm/infeasibility/driverFacility/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11420,'/fm/infeasibility/driverTypeEquipment/jsp/AddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11421,'/fm/infeasibility/driverTypeEquipment/jsp/ProcessAddInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11422,'/fm/infeasibility/driverTypeEquipment/jsp/ProcessRemoveInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11423,'/fm/infeasibility/driverTypeEquipment/jsp/ProcessUpdateInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11424,'/fm/infeasibility/driverTypeEquipment/jsp/ViewInfeasibilities.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11425,'/fm/infeasibility/jsp/relatedCompanyId.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11426,'/fm/infeasibility/waveWave/jsp/InfeasibleWaveWavePresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11427,'/fm/infeasibility/waveWave/jsp/UpdateInfeasibleWaveWave.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11428,'/fm/jsp/AjaxActivityDuration.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11429,'/fm/jsp/AjaxShiftEnd.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11430,'/fm/jsp/AjaxValidateDate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11431,'/fm/jsp/AlternateDestinationPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11432,'/fm/jsp/DispatchEquipmentInstanceLocation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11433,'/fm/jsp/DispatchEquipmentProcessUpdateLocation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11434,'/fm/jsp/DispatchShipmentListPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11435,'/fm/jsp/DriverInstanceDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11436,'/fm/jsp/DriverRatingQueryDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11437,'/fm/jsp/DriverRatingQueryDetailsForTrip.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11438,'/fm/jsp/DriverSchedExceptionHistory.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11439,'/fm/jsp/DriverSchedExceptionHistoryNoHeader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11440,'/fm/jsp/EquipAvailReportSummary.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11441,'/fm/jsp/failedProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11442,'/fm/jsp/ImageLoader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11443,'/fm/jsp/LoadRatingQueryDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11444,'/fm/jsp/LoadRatingQueryDetailsForTrip.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11445,'/fm/jsp/ManualSegment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11446,'/fm/jsp/ManualSegmentOverride.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11447,'/fm/jsp/ModifyEstDepartureTime.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11448,'/fm/jsp/ModifyEstDepartureTimeOverrides.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11449,'/fm/jsp/ModifyLocation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11450,'/fm/jsp/ModifyStopsCustom.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11451,'/fm/jsp/obc/popup/OBCConfirmationPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11452,'/fm/jsp/overrideSplitSoftCheck.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11453,'/fm/jsp/PrintDispatchDocumentPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11454,'/fm/jsp/processAlternateDestination.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11455,'/fm/jsp/ProcessManualSegment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11456,'/fm/jsp/ProcessModifyAssign.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11457,'/fm/jsp/ProcessModifyEstDepartureTime.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11458,'/fm/jsp/ProcessModifyStopsCustom.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11459,'/fm/jsp/ProcessSplitShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11460,'/fm/jsp/QDWController.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11461,'/fm/jsp/QDWEquipmentInstanceDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11462,'/fm/jsp/QDWEquipmentInstanceSummaryPresentationViewOnly.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11463,'/fm/jsp/QDWEquipmentInstanceSummarySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11464,'/fm/jsp/QDWHardCheckErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11465,'/fm/jsp/QDWOverrides.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11466,'/fm/jsp/QDWSoftCheckOverrides.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11467,'/fm/jsp/QuickDriverList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11468,'/fm/jsp/QuickTripList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11469,'/fm/jsp/RouteDisplay.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11470,'/fm/jsp/SplitShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11471,'/fm/jsp/Success.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11472,'/fm/jsp/TotalRatingQueryDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11473,'/fm/jsp/TotalRatingQueryDetailsForTrip.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11474,'/fm/jsp/TripDetailsOverrideCustom.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11475,'/fm/jsp/TripListOBCPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11476,'/fm/jsp/TripMap.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11477,'/fm/jsp/ViewDispatchDriverList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11478,'/fm/jsp/ViewDriverWarnings.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11479,'/fm/jsp/ViewOBCMessageWarnings.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11480,'/fm/jsp/ViewTripList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11481,'/fm/jsp/ViewTripWarnings.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11482,'/fm/jsp/windowid.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11483,'/fm/popup/driverCalander_driverPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11484,'/fm/popup/FacilityDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11485,'/fm/popup/QDWParentRefresh.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11486,'/fm/shipserv/jsp/processSaveShipmentLoadDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11487,'/fm/shipserv/jsp/ViewShipmentLoadDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11488,'/fm/trip/DispatchWorkspace.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11489,'/fm/trip/ForwardQTripList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11490,'/fm/trip/obc/popup/OBCConfirmationDialog.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11491,'/fm/trip/SplitShipment.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11492,'/fm/trip/SplitShipmentOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11493,'/fm/trip/TripCCLSendUpdatePopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11494,'/fm/trip/triptype/TripTypeCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11495,'/fm/trip/triptype/TripTypeDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11496,'/fm/trip/triptype/TripTypeDetailsViewOnly.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11497,'/fm/trip/VirtualTripDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11498,'/fm/trip/VirtualTripDetailsOBCPopup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11499,'/fm/trip/VirtualTripDetailsOverrides.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11500,'/fm/trip/VirtualTripStopsDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11502,'/ofr/ra/jsp/ProcessEditMasterTpCodeMappings.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11503,'/ofr/ra/jsp/ProcessMasterTpCodeCRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11504,'/ofr/ra/jsp/ProcessRAShipmentPDF.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11505,'/ofr/ra/jsp/ProcessViewReasonCodes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11507,'/ofr/ra/jsp/shipperNames.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11508,'/ofr/ra/jsp/ViewAllComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11509,'/ofr/ra/jsp/ViewAllReasonCodes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11510,'/basedata/customization/jsp/CreateUserCustomXml.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11511,'/basedata/customization/jsp/CustomizeDisplayDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11512,'/basedata/customization/jsp/CustomizeDisplayList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11513,'/basedata/customization/jsp/ProcessCustomizeDisplayDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11514,'/basedata/customization/jsp/ProcessCustomizeDisplayList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11515,'/basedata/customization/jsp/UserCustomXml.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11516,'/lcom/admin/customize/jsp/ActionLinkList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11517,'/lcom/admin/customize/jsp/AddJS.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11518,'/lcom/admin/customize/jsp/bodyDispInfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11519,'/lcom/admin/customize/jsp/deleteActionLinkList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11520,'/lcom/admin/customize/jsp/deleteDispItem.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11521,'/lcom/admin/customize/jsp/deleteEntityItem.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11522,'/lcom/admin/customize/jsp/deleteEntityPanel.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11523,'/lcom/admin/customize/jsp/deleteJSMethod.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11524,'/lcom/admin/customize/jsp/deleteJSMethodList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11525,'/lcom/admin/customize/jsp/deleteSubEntityItem.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11526,'/lcom/admin/customize/jsp/editDispItem.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11527,'/lcom/admin/customize/jsp/editEntityItem.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11528,'/lcom/admin/customize/jsp/editEntityPanel.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11529,'/lcom/admin/customize/jsp/EditOptionalFields.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11530,'/lcom/admin/customize/jsp/editSubEntityItem.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11531,'/lcom/admin/customize/jsp/EntityDispPanelInfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11532,'/lcom/admin/customize/jsp/generateDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11533,'/lcom/admin/customize/jsp/generateInterface.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11534,'/lcom/admin/customize/jsp/generateMetaXML.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11535,'/lcom/admin/customize/jsp/generateSQLXML.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11536,'/lcom/admin/customize/jsp/generateStubForList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11537,'/lcom/admin/customize/jsp/generateUserXML.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11538,'/lcom/admin/customize/jsp/JSMethod.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11539,'/lcom/admin/customize/jsp/JSMethodList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11540,'/lcom/admin/customize/jsp/newPage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11541,'/lcom/admin/customize/jsp/pageCreate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11542,'/lcom/admin/customize/jsp/parameterList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11543,'/lcom/admin/customize/jsp/popMenu.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11544,'/lcom/admin/customize/jsp/processActionLinkList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11545,'/lcom/admin/customize/jsp/processBodyDispInfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11546,'/lcom/admin/customize/jsp/processEntityPanelDispInfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11547,'/lcom/admin/customize/jsp/processJSFileList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11548,'/lcom/admin/customize/jsp/processJSMethod.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11549,'/lcom/admin/customize/jsp/processJSMethodList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11550,'/lcom/admin/customize/jsp/processParameterList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11551,'/lcom/admin/customize/jsp/processPopMenu.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11552,'/lcom/admin/customize/jsp/saveDispItem.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11553,'/lcom/admin/customize/jsp/saveEntityItem.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11554,'/lcom/admin/customize/jsp/saveOptionalFields.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11555,'/lcom/admin/customize/jsp/savePage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11556,'/lcom/admin/customize/jsp/savePanel.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11557,'/lcom/admin/customize/jsp/saveSQLInfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11558,'/lcom/admin/customize/jsp/saveSubEntityItem.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11559,'/lcom/admin/customize/jsp/sqlInformation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11560,'/lcom/admin/util/jsp/ClearCustomListDisplayCache.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11561,'/lcom/admin/util/jsp/CustomizationRefresh.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11562,'/ofr/admin/healthcheck/DiagResults.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11563,'/ofr/admin/healthcheck/SystemHealth.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11564,'/ofr/admin/jsp/createSegmentFromShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11565,'/ofr/admin/jsp/DataBaseInfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11566,'/ofr/admin/jsp/DevTools.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11567,'/ofr/admin/jsp/DistanceTimeCheck.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11568,'/ofr/admin/jsp/EITester.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11569,'/ofr/admin/jsp/EventLogTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11570,'/ofr/admin/jsp/GRSOptimizerTester.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11571,'/ofr/admin/jsp/InitializeTransactionalData.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11572,'/ofr/admin/jsp/InternalIdLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11573,'/ofr/admin/jsp/LdapInfo.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11574,'/ofr/admin/jsp/pageNotFound.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11575,'/ofr/admin/jsp/queryStatic.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11576,'/ofr/admin/jsp/setupHOSParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11577,'/ofr/admin/jsp/shipmentNotInRS.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11578,'/ofr/admin/jsp/ShipperLogEntryListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11579,'/ofr/admin/jsp/ShipperLogEntrySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11580,'/ofr/admin/jsp/StartTandemDispatchPreprocessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11581,'/ofr/admin/jsp/SystemHealthCheck.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11582,'/ofr/admin/jsp/SystemLogEntryListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11583,'/ofr/admin/jsp/SystemLogEntrySheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11584,'/ofr/admin/jsp/WebMethodsCheck.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11585,'/ofr/admin/jsp/WebServicesCheck.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11586,'/ofr/admin/parameter/archiving/jsp/EditArchivingParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11587,'/ofr/admin/parameter/archiving/jsp/EditArchivingParametersProcess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11588,'/ofr/admin/parameter/archiving/jsp/ViewArchivingParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11589,'/ofr/admin/parameter/companyconsolidation/jsp/FacilityLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11590,'/ofr/admin/user/jsp/ProcessUserParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11591,'/ofr/admin/user/jsp/ProcessUserSettings.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11592,'/ofr/admin/user/jsp/UserAdmin.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11593,'/ofr/admin/user/jsp/UserAdminTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11594,'/ofr/admin/user/jsp/UserParameters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11595,'/ofr/admin/user/jsp/UserRegion.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11596,'/ofr/admin/user/jsp/UserRegionUpdate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11597,'/ofr/admin/util/jsp/AddNewScheduler.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11598,'/ofr/admin/util/jsp/ASPCustomersWorkflow.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11599,'/ofr/admin/util/jsp/BusinessPartner.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11600,'/ofr/admin/util/jsp/CalendarImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11601,'/ofr/admin/util/jsp/ClaimImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11602,'/ofr/admin/util/jsp/ClaimPaymentsImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11603,'/ofr/admin/util/jsp/directoryImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11604,'/ofr/admin/util/jsp/DisplayUserAccess.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11605,'/ofr/admin/util/jsp/DriverCheckInOrOut.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11606,'/ofr/admin/util/jsp/DriverImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11607,'/ofr/admin/util/jsp/EDICommManagerImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11608,'/ofr/admin/util/jsp/EDICommManagerImportWithOutQueue.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11609,'/ofr/admin/util/jsp/EquipmentImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11610,'/ofr/admin/util/jsp/ExportSalesOrder.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11611,'/ofr/admin/util/jsp/HistoricalClaimImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11612,'/ofr/admin/util/jsp/HistoricalDetentionImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11613,'/ofr/admin/util/jsp/HistoricalInvoicesImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11614,'/ofr/admin/util/jsp/LoadFromTranLog.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11615,'/ofr/admin/util/jsp/processDirectoryImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11616,'/ofr/admin/util/jsp/ProcessEDICommManagerImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11617,'/ofr/admin/util/jsp/ProcessEDICommManagerImportWithOutQueue.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11618,'/ofr/admin/util/jsp/processHistClaimImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11619,'/ofr/admin/util/jsp/processHistDetentionImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11620,'/ofr/admin/util/jsp/processHistInvoicesImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11621,'/ofr/admin/util/jsp/processImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11622,'/ofr/admin/util/jsp/processLoadFromTranLog.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11623,'/ofr/admin/util/jsp/processTimeFeasibilityInput.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11624,'/ofr/admin/util/jsp/processWebServiceImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11625,'/ofr/admin/util/jsp/processWriteTranLogToDisk.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11626,'/ofr/admin/util/jsp/ProcessXMLCommManagerImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11627,'/ofr/admin/util/jsp/QueryPerf.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11628,'/ofr/admin/util/jsp/RemoveScheduler.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11629,'/ofr/admin/util/jsp/RequestInterface.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11630,'/ofr/admin/util/jsp/SalesOrderList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11631,'/ofr/admin/util/jsp/ScheduleImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11632,'/ofr/admin/util/jsp/ScheduleTranLogEvent.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11633,'/ofr/admin/util/jsp/shipmentRequestInterface.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11634,'/ofr/admin/util/jsp/TimeFeasibilityUtil.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11635,'/ofr/admin/util/jsp/UnlockWorkspace.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11636,'/ofr/admin/util/jsp/VerifyCompanySetup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11637,'/ofr/admin/util/jsp/webMethodImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11638,'/ofr/admin/util/jsp/WebServiceImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11639,'/ofr/admin/util/jsp/WriteTranLogToDisk.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11640,'/ofr/admin/util/jsp/XMLCommManagerImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11641,'/ofr/admin/util/jsp/XMLImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11642,'/ofr/alert/jsp/AlertError.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11643,'/ofr/alert/jsp/APCreateAlertDef.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11644,'/ofr/alert/jsp/APProcessCreateAlertDef.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11645,'/ofr/alert/jsp/BookingAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11646,'/ofr/alert/jsp/DispatchDriverAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11647,'/ofr/alert/jsp/OrderAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11648,'/ofr/alert/jsp/ProcessAlertListActions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11649,'/ofr/alert/jsp/TripAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11650,'/ofr/AppointmentManagementIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11651,'/ofr/ArchiveIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11652,'/ofr/booking/jsp/AddPurchaseOrderToBooking.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11653,'/ofr/booking/jsp/BookingAssignmentWithDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11654,'/ofr/booking/jsp/BookingPurchaseOrderList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11655,'/ofr/booking/jsp/ProcessAddPurchaseOrderToBookingPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11656,'/ofr/booking/jsp/ProcessBookingPlannerRun.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11657,'/ofr/booking/jsp/PurchaseOrderListForBookingPlanner.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11658,'/ofr/booking/jsp/removePurchaseOrderFromBooking.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11659,'/ofr/booking/jsp/ReviewFilteredOrdersForBookingPlanner.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11660,'/ofr/BusinessRulesIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11661,'/ofr/claim/carrier/MoveToClaimList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11662,'/ofr/consolidation/workspace/jsp/AddOrdersToShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11663,'/ofr/consolidation/workspace/jsp/bypassOrderListPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11664,'/ofr/consolidation/workspace/jsp/bypassRemoveOrderConfirmation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11665,'/ofr/consolidation/workspace/jsp/bypassShipmentListPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11666,'/ofr/consolidation/workspace/jsp/DisplayErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11667,'/ofr/consolidation/workspace/jsp/DisplayErrorsinColor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11668,'/ofr/consolidation/workspace/jsp/EditDraftShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11669,'/ofr/consolidation/workspace/jsp/EditStopDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11670,'/ofr/consolidation/workspace/jsp/FeasibleResourcePopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11671,'/ofr/consolidation/workspace/jsp/getAssignedShipments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11672,'/ofr/consolidation/workspace/jsp/ImageLoader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11673,'/ofr/consolidation/workspace/jsp/LockedWorkspace.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11674,'/ofr/consolidation/workspace/jsp/MoveOrdersToFixedPosition.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11675,'/ofr/consolidation/workspace/jsp/navigateToMCW.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11676,'/ofr/consolidation/workspace/jsp/PrintErrorFunctions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11677,'/ofr/consolidation/workspace/jsp/ProcessAddOrdersToShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11678,'/ofr/consolidation/workspace/jsp/ProcessConsolidationWorkspace.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11679,'/ofr/consolidation/workspace/jsp/ProcessEditDraftShipmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11680,'/ofr/consolidation/workspace/jsp/ProcessEditStopDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11681,'/ofr/consolidation/workspace/jsp/processMCWActions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11682,'/ofr/consolidation/workspace/jsp/ProcessNewDraftShipments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11683,'/ofr/consolidation/workspace/jsp/ProcessSelectMainCarriageNetwork.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11684,'/ofr/consolidation/workspace/jsp/ProcessSelectPreCarriageNetwork.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11685,'/ofr/consolidation/workspace/jsp/ProcessShipmentAction.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11686,'/ofr/consolidation/workspace/jsp/RemoveOrdersConfirmation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11687,'/ofr/consolidation/workspace/jsp/RemoveShipmentsConfirmation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11688,'/ofr/consolidation/workspace/jsp/ReviewShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11689,'/ofr/consolidation/workspace/jsp/SearchConsolidationWorkspace.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11690,'/ofr/consolidation/workspace/jsp/SelectMainCarriageNetwork.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11691,'/ofr/consolidation/workspace/jsp/SelectPreCarriageNetwork.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11692,'/ofr/consolidation/workspace/jsp/SelectStaticRoute.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11693,'/ofr/consolidation/workspace/jsp/SelectStopMovement.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11694,'/ofr/consolidation/workspace/jsp/SetErrorsToRequest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11695,'/ofr/consolidation/workspace/jsp/UnlockWorkspace.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11696,'/ofr/consolidation/workspace/jsp/ViewDataErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11697,'/ofr/consolidation/workspace/jsp/ViewOrderLoadingSeq.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11698,'/ofr/ContractManagementIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11699,'/ofr/csc/CSAOptionList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11700,'/ofr/csc/EditFlatFile.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11701,'/ofr/customization/jsp/CustomDisplayListDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11702,'/ofr/customization/jsp/CustomizeDisplayDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11703,'/ofr/customization/jsp/CustomizeDisplayList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11704,'/ofr/customization/jsp/ProcessCustomDisplay.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11705,'/ofr/customization/jsp/ProcessCustomizeDisplayList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11706,'/ofr/detention/carrier/jsp/CarrierToShipperMapping.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11707,'/ofr/detention/shipper/jsp/processCreateDetention.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11708,'/ofr/DispatchIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11709,'/ofr/distancetime/jsp/DistanceTimeQuery.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11710,'/ofr/distancetime/jsp/DistanceTimeResp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11711,'/ofr/distancetime/jsp/DTBulkDelete.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11712,'/ofr/distancetime/jsp/DTCacheRefresh.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11713,'/ofr/distancetime/jsp/DTCacheRefreshResp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11714,'/ofr/distancetime/jsp/DTOverrideDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11715,'/ofr/distancetime/jsp/DTOverrideList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11716,'/ofr/distancetime/jsp/DTOverrideWindow.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11717,'/ofr/distancetime/jsp/DTValidateFacility.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11718,'/ofr/distancetime/jsp/DTValidateFacilityAndCache.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11719,'/ofr/distancetime/jsp/DTValidateFacilityResp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11720,'/ofr/distancetime/jsp/processDTOverride.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11721,'/ofr/distancetime/jsp/processDTOverrideStateTax.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11722,'/ofr/distancetime/jsp/processDTWindow.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11723,'/ofr/distancetime/jsp/RandTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11724,'/ofr/dockappointment/carrier/jsp/DockAppointmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11725,'/ofr/dockappointment/carrier/jsp/DockLevelDailySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11726,'/ofr/dockappointment/carrier/jsp/DockLevelWeeklySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11727,'/ofr/dockappointment/carrier/jsp/EditDockAppointment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11728,'/ofr/dockappointment/carrier/jsp/FacilityDockLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11729,'/ofr/dockappointment/carrier/jsp/FacilityLevelDailySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11730,'/ofr/dockappointment/carrier/jsp/ProcessDockAppointment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11731,'/ofr/dockappointment/carrier/jsp/ProcessDockAppointmentCRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11732,'/ofr/dockappointment/carrier/jsp/ViewDockCapacityForDayPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11733,'/ofr/dockappointment/shipper/jsp/DockAppointmentDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11734,'/ofr/dockappointment/shipper/jsp/DockLevelDailySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11735,'/ofr/dockappointment/shipper/jsp/DockLevelWeeklySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11736,'/ofr/dockappointment/shipper/jsp/EditDockAppointment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11737,'/ofr/dockappointment/shipper/jsp/FacilityDockLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11738,'/ofr/dockappointment/shipper/jsp/FacilityLevelDailySchedule.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11739,'/ofr/dockappointment/shipper/jsp/OverrideDockAppointmentSoftChecks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11740,'/ofr/dockappointment/shipper/jsp/ProcessDockAppointment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11741,'/ofr/dockappointment/shipper/jsp/ProcessDockAppointmentCRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11742,'/ofr/dockappointment/shipper/jsp/ViewDockCapacityForDayPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11743,'/ofr/DockManagementIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11744,'/ofr/documentation/jsp/CustDocDeliveryDtls.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11745,'/ofr/documentation/jsp/CustSaveDocsDtls.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11746,'/ofr/documentation/jsp/DocDelivery.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11747,'/ofr/documentation/jsp/DocGenerationPermissionWarning.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11748,'/ofr/documentation/jsp/GenerateDocs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11749,'/ofr/documentation/jsp/LoadDocumentationFilter.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11750,'/ofr/documentation/jsp/PreviewDocs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11751,'/ofr/documentation/jsp/ProcessDeliveryDocs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11752,'/ofr/documentation/jsp/ProcessGenerateDocs_.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11753,'/ofr/documentation/jsp/ProcessPreviewDocs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11754,'/ofr/documentation/jsp/SaveDocs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11755,'/ofr/documentupload/view/DocumentUpload.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11756,'/ofr/eventmgt/objectevent/booking/jsp/EventMgtBookingSummary.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11757,'/ofr/extension/ShipmentHeaderTLMExtDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11758,'/ofr/extension/ShipmentListPopUp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11759,'/ofr/extension/ShipmentTLMExtList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11760,'/ofr/filter/jsp/ApplyDefault.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11761,'/ofr/filter/jsp/FilterDetailsInPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11762,'/ofr/filter/jsp/FilterError.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11763,'/ofr/filter/jsp/FilterList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11764,'/ofr/filter/jsp/FilterListInPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11765,'/ofr/filter/jsp/FilterOnFilters.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11766,'/ofr/filter/jsp/SearchDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11767,'/ofr/GlobalVisibilityIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11768,'/ofr/help/jsp/shipmentDetailsTPHelp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11769,'/ofr/help/jsp/shipperHotSheetHelp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11770,'/ofr/help/jsp/shipperViewAuctionCyclesHelp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11771,'/ofr/help/jsp/tpInboxActiveHelp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11772,'/ofr/help/jsp/tpInboxArchiveHelp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11773,'/ofr/help/jsp/tpInBoxAwaitingHelp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11774,'/ofr/help/jsp/tpInBoxHelp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11775,'/ofr/invoice/shipper/jsp/CustomerInvoiceSchedulerDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11776,'/ofr/invoice/shipper/jsp/RetrieveDetentionInvoice.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11777,'/ofr/invoice/ui/MassApprovalPopUp.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11778,'/ofr/issue/carrier/jsp/AddIssueComment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11779,'/ofr/issue/carrier/jsp/EditIssue.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11780,'/ofr/issue/carrier/jsp/IssueDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11781,'/ofr/issue/carrier/jsp/ProcessIssueCRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11782,'/ofr/issue/carrier/jsp/ViewAllIssueComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11783,'/ofr/issue/shipper/jsp/AddIssueComment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11784,'/ofr/issue/shipper/jsp/EditIssue.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11785,'/ofr/issue/shipper/jsp/IssueDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11786,'/ofr/issue/shipper/jsp/ProcessIssueCRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11787,'/ofr/issue/shipper/jsp/ViewAllIssueComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11788,'/ofr/letterofinstr/EditLOI.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11789,'/ofr/letterofinstr/ProcessLOICRUD.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11790,'/ofr/letterofinstr/ProcessSendRailTender.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11791,'/ofr/lookup/jsp/dispatchLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11792,'/ofr/lookup/jsp/EquipmentLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11793,'/ofr/lookup/jsp/facilityLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11794,'/ofr/lookup/jsp/fvLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11795,'/ofr/lookup/jsp/idLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11796,'/ofr/lookup/jsp/invoiceFacilityIdLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11797,'/ofr/lookup/jsp/shipmentLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11798,'/ofr/mcw/client/jsp/AdjustGateTimes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11799,'/ofr/mcw/client/jsp/CustomizeDisplayDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11800,'/ofr/mcw/client/jsp/getGanttChart.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11801,'/ofr/mcw/client/jsp/MCWJSLocalizedMessages.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11802,'/ofr/mcw/client/jsp/planpath.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11803,'/ofr/NetworkMgmtIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11804,'/ofr/optimizationmgmt/consolidation/autoapproval/jsp/AutoApprovalListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11805,'/ofr/optimizationmgmt/consolidation/autoapproval/jsp/AutoApprovalPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11986,'/techdemo/validation/jsp/ValidationQuery.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11987,'/techdemo/validation/jsp/ValidationResp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11988,'/ofr/mcw/client/jsp/redirectToMCW.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12225,'/obl/bid/autobid/process_autoBidRules.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12226,'/obl/bid/lane_bid/view_lane_bid.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12227,'/obl/index.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12228,'/obl/master.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12229,'/obl/rfp/effectiverate/init_select_effective_rate_fields_for_view.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12230,'/obl/rfp/effectiverate/select_effective_rate_fields_for_view.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12231,'/obl/rfp/effectiverate/view_effective_rate.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12232,'/obl/rfp/lane/administer_equipment_type_line_item.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12233,'/obl/rfp/lane/administer_equipment_type_line_item_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12234,'/obl/rfp/lane/administer_lane_succeeded.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12235,'/obl/rfp/laneaward/administer_lane_award.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12236,'/obl/rfp/postrfpaward/cancel_post_for_incomplete_lanes.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12237,'/obl/rfp/postrfpaward/error.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12238,'/obl/rfp/rfp/export_rfp_include.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12239,'/obl/rfp/rfp/rfpdraft.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12240,'/obl/rfp/rfp/rfpsuccess.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12241,'/obl/rfp/rfp/tmp_select_special_reqs_fields_for_view.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12242,'/obl/select_action.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12243,'/obladministerAProcessServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12244,'/obladministerCarrLaneServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12245,'/obladministerCustomAttributeServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12246,'/obladministerCustomRfpAttributeServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12247,'/obladministerDistributionListServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12248,'/obladministerEffectiveRateServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12249,'/obladministerFilterServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12250,'/obladministerFreightChargeServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12251,'/obladministerLaneAwardServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12252,'/obladministerLaneServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12253,'/obladministerLocationServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12254,'/obladministerMsgServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12255,'/obladministerRfpResponseServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12256,'/obladministerRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12257,'/obladministerSpecReqServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12258,'/obladminsterEquipmentTypeLineItem.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12259,'/oblarchiveRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12260,'/oblcancelLaneServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12261,'/oblcancelRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12262,'/oblcapacityBidServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12263,'/oblcapacitySelectionServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12264,'/oblcarrierLaneServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12265,'/oblcloseRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12266,'/oblcreateBaseScenarioServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12267,'/obldeclineLaneServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12268,'/obldeclineRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12269,'/obldeleteMsgServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12270,'/obldeleteRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12271,'/obldownloadRfpDataServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12272,'/oblduplicateRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12273,'/oblexportRfpDataServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12274,'/oblexportRfpShellServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12275,'/oblfacilitateServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12276,'/oblmanageProfileServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12277,'/oblmanageProfileServletTC.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12278,'/oblmanageWorkspaceServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12279,'/oblmodifyInvitedTpsServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12280,'/oblmsgDetailServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12281,'/oblodLookUpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12282,'/oblpostRfpAwardServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12283,'/oblpostRfpResponseServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12284,'/oblpostRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12285,'/oblpreviewRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12286,'/oblselectEffectiveRateFieldsForViewServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12287,'/oblselectSpecialReqsFieldsForViewServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12288,'/oblupdateMsgFlagServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12289,'/oblviewLaneServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12290,'/oblviewMsgDetailServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12291,'/oblviewMsgServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12292,'/oblviewRebidLaneServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12293,'/oblviewRfpServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12294,'/ofr/order/jsp/ProcessOrderListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12298,'/obl/profile/view_company_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12299,'/obl/rfp/customattribute/administer_custom_attribute_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12300,'/obl/view_distribution_list.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12301,'/oblUploadXML.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12302,'/oblExportXMLServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12303,'/oblLinkDownloadServlet.serv','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12304,'/services/rest/tpe/PostLoginService/PostLoginService/getSelectionList','TPE',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12305,'/services/rest/tpe/TPEDistributionOrderActionService/tpeDOListActions','TPE',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12306,'/services/rest/tpe/TPEDistributionOrderActionService/tpeDOListActions/fetchSizeUOMs','TPE',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12307,'/services/rest/tpe/TPEDistributionOrderActionService/tpeDOListActions/fetchEquipments','TPE',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12308,'/services/rest/tpe/TPEDistributionOrderActionService/tpeDOListActions/onMCWRemoveSplit','MSHP',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12310,'/ofr/rs/jsp/addCarrTndrOvrdPage.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12311,'/ofr/rs/jsp/addAutoRecallSuspPage.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12312,'/ofr/rs/jsp/updCarrTndrOvrd.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12313,'/imageLoader.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12314,'/ofr/shipserv/jsp/NavToMAUIPage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12315,'/ofr/consolidation/manual/scenario/ui/chartcreatorrequest.jsf','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12316,'/services/rest/tlm/fm/*','DPTOBC',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12317,'/claimReportServices.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12318,'/ofr/consolidation/jsp/bypassListPopup.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12323,'/fieldvision/license/LicenseKeyList.xhtml','MSHP',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12324,'/basedata/season/jsp/SeasonSummarySheet.jsp?seasonOperation=create','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12325,'/basedata/season/jsp/SeasonSheet.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12326,'/basedata/season/jsp/SeasonCRUDResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12327,'/ofr/rs/jsp/delAutoRecallSusp.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12328,'/ofr/rs/jsp/delCarrTndrOvrd.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12329,'/services/rest/tlm/multipart/fm/shipmentDoc','DPTOBC',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12330,'/ofr/mcw/client/customization/jsp/CustomizeDisplayDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12331,'/ofr/mcw/client/customization/jsp/ProcessCustomizeDisplayDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12332,'/services/rest/tpe/TPEDistributionOrderActionService/tpeDOListActions/blockAutoConsolidation','ACM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12333,'/services/rest/tpe/TPEDistributionOrderActionService/tpeDOListActions/allowAutoConsolidation','ACM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12334,'/services/rest/tpe/TPEDistributionOrderActionService/tpeDOListActions/moveAllToMCW','TPE',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12335,'/services/rest/tpe/TPEDistributionOrderActionService/tpeDOListActions/moveAllToMCW/*','TPE',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12336,'/services/rest/tpe/TPEShipmentListActionService/tpeShipmentListActions/moveAllToMCW','TPE',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12337,'/services/rest/tpe/TPEShipmentListActionService/tpeShipmentListActions/moveAllToMCW/*','TPE',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12338,'/ofr/rs/jsp/RSTEError.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12339,'/services/rest/tpe/TPEMAUIFiniteValueService/tpefv/consRegions','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12340,'/ofr/order/jsp/DOMovementDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12341,'/te/manual/planning/jsp/CloseShipment.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12342,'/PDFPreview.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12343,'/obldownloadAttachmentServlet.serv','OBDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12344,'/services/rest/tpe/PostLoginService/PostLoginService/submitAction','TPE',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12345,'/raShipmentPDF.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12347,'/services/rest/eem/PostLoginService/selectionList','EEM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12348,'/services/rest/eem/PostLoginService/submit','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12349,'/services/rest/eem/Service/login/stores','EEM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12350,'/services/rest/eem/Service/login/submit','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12351,'/services/rest/eem/Service/fulfillment/do','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12352,'/services/rest/eem/Service/fulfillment/detail','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12353,'/services/rest/eem/Service/fulfillment/update','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12354,'/services/rest/eem/Service/fulfillment/submit','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12355,'/services/rest/eem/Service/cyclecount/cc','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12356,'/services/rest/eem/Service/cyclecount/detail','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12357,'/services/rest/eem/Service/cyclecount/update','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12358,'/services/rest/eem/Service/cyclecount/submit','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12359,'/services/rest/eem/Service/inventory/inv','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12360,'/services/rest/eem/Service/inventory/adjust','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12361,'/services/rest/eem/Service/file','EEM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12362,'/services/rest/eem/Service/receiving/asn','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12363,'/services/rest/eem/Service/receiving/update','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12364,'/services/rest/eem/Service/receiving/submit','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12365,'/services/rest/eem/Service/common/activity','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12366,'/services/rest/eem/Service/common/close','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12367,'/services/rest/eem/SCAGraphService/*','EEM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12368,'/services/rest/eem/remittanceadvice/*','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12369,'/services/rest/eem/chargeback/*','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12370,'/services/rest/eem/cogi/*','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12371,'/services/rest/eem/shipmenttracking/*','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12372,'/services/rest/eem/cyclecount/receivecategory','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12373,'/services/rest/eem/cyclecount/receivehierarchy','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12374,'/services/rest/eem/cyclecount/receiverequest','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12375,'/services/rest/eem/interimreceiving/receiveinterim','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12376,'/services/ASNWebService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12377,'/services/ASNService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12378,'/services/COGIWebService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12379,'/services/ChargebackWebService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12380,'/services/CreateCycleCountWebService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12381,'/services/CycleCountService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12382,'/services/EEMCommMgrServices/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12383,'/services/EEMMobileUtilsService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12384,'/services/HostSelectionService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12385,'/services/InspectionWebService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12386,'/services/LPNWebService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12387,'/services/EEMMobileSignInService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12388,'/services/TransactionLogService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12389,'/services/POWebService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12390,'/services/ReceivingService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12391,'/services/BuildLPNService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12392,'/services/StoreOrderService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12393,'/services/SyncService/*','EEM',3,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12394,'/services/rest/eem/EEMASNActionService/EEMASNListService/enableDisableReceiveASNAction','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12395,'/services/rest/eem/EEMASNActionService/EEMASNListService/receiveASNAction','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12396,'/services/rest/eem/EEMASNActionService/EEMASNListService/enableDisableReceiveAndVerifyASNAction','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12397,'/services/rest/eem/EEMASNActionService/EEMASNListService/receiveAndVerifyASNAction','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12398,'/services/rest/eem/EEMASNActionService/EEMASNListService/enableDisablePrintLabelASNAction','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12399,'/eem/printer/jsp/printerLookup.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12400,'/basedata/printer/jsp/labelPrinting.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12401,'/eem/printer/jsp/labelPrinting.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12404,'/eem/transferorder/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12405,'/eem/bulkpickpack/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12407,'/eem/negotiation/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12408,'/eem/pomodifyaccept/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12409,'/eem/popup/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12410,'/ifse/router/RouterList.xhtml','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12411,'/eem/buildlpn/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12412,'/eem/scanpack/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12413,'/eem/printer/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12414,'/eem/hubmgt/lpnsplit/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12415,'/eem/hubmgt/transfer/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12416,'/eem/dashboard/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12417,'/services/rest/eem/*','EEM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12418,'/eem/adm/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12419,'/eem/hub/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12420,'/eem/instorepickup/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12421,'/eem/manifest/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12422,'/eem/parcel/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12423,'/eem/pricetkt/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12424,'/eem/inspection/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12425,'/eem/remittanceadvice/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12426,'/eem/chargeback/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12427,'/eem/cyclecount/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12428,'/eem/mobile/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12429,'/eem/cogi/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12431,'/services/rest/eem/*','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12432,'/services/rest/eem/EEMShipmentListActionService/*','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12433,'/services/rest/scv/*','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12434,'/accountSetup.serv','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12435,'/parcel/fedex/FedexAccountSetup.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12436,'/parcel/ups/UPSAccountSetup.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12437,'/parcel/fedex/FedexRegistration.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12438,'/parcel/fedex/FedexSubscription.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12439,'/parcel/fedex/FedexFacilityInfo.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12440,'/parcel/ups/UPSLicense.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12441,'/parcel/ups/UPSLicenseAgreement.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12442,'/parcel/ups/UPSAccessLicense.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12443,'/parcel/ups/UPSLicenseAgreementPrint.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12444,'/parcel/ups/UPSRegistration.jsp','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12445,'/scv/scvplus/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12446,'/scv/scvplus/AddInventoryStep1.xhtml','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12447,'/scv/scvplus/AddInventoryStep2.xhtml','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12448,'/eem/admin/util/jsp/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12449,'/eem/admin/jsp/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12450,'/instoreimage.serv','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12451,'/eem/isf/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12452,'/manh/olm/inventory/inventorymanagement/xhtml/AddInventoryStep1.jsflps','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12453,'/manh/olm/inventory/inventorymanagement/xhtml/AddInventoryStep2.jsflps','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12454,'/manh/olm/inventory/inventorymanagement/xhtml/GlobalItemInventory.jsflps','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12455,'/manh/olm/inventory/inventorymanagement/xhtml/InventoryDataList.jsflps','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12456,'/manh/olm/inventory/inventorymanagement/xhtml/EditInventory.jsflps','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12457,'/manh/olm/inventory/inventorymanagement/xhtml/ViewInventory.jsflps','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12458,'/services/rest/Integration/CustomerMaster/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12459,'/services/rest/Integration/Promotion/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12460,'/services/rest/Integration/PromotedProductAssoc/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12461,'/services/rest/Integration/OfferedProdAssoc/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12462,'/services/rest/Integration/POSTransaction/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12463,'/services/rest/Integration/ItemPrice/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12464,'/services/rest/Integration/ShippingMatrix/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12465,'/services/rest/Integration/ShippingRule/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12466,'/services/rest/Integration/CustomerOrder/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12467,'/services/rest/Integration/CancelCustomerOrder/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12468,'/services/rest/Integration/CompletedCustomerOrder/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12469,'/services/olm/customerorder/createOrUpdate','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12470,'/services/olm/customer/customerDetailsById','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12471,'/services/olm/customer/customerFullInformationById','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12472,'/services/olm/customer/paymentInfo','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12473,'/services/olm/customerorder/priceOverrideHistory','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12474,'/services/olm/customerorder/snhChargeOverrideHistory','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12475,'/services/olm/customerorder/posTransactionDetails','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12476,'/services/olm/item/itemdetails','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12477,'/services/olm/customerorder/customerOrderDetails','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12478,'/services/olm/customerorder/customerOrderAndTransactionList','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12479,'/services/olm/customerorder/deleteOrCancelLines','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12480,'/services/olm/returns/createOrUpdate','RLM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12481,'/services/olm/returns/returnOrderDetails','RLM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12482,'/services/olm/customerorder/fireWorkFlowEvent','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12483,'/services/olm/returns/returnList','RLM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12484,'/services/atc/inventory/getInventory','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12485,'/services/atc/availability/getAvailabilityList','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12486,'/services/atc/cvDef/getViewList','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12487,'/services/atc/cvDef/saveViewDef','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12488,'/services/platform/cvFilter/getFilterLayout','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12489,'/services/platform/cvFilter/saveFilter','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12490,'/services/platform/cvFilter/getFilterDetails','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12491,'/olm/salesorder/CreateCustomerOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12492,'/olm/salesorder/CreateRetailOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12493,'/olm/salesorder/CreateSalesOrderHeader.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12494,'/olm/salesorder/EditCustomerOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12495,'/olm/salesorder/EditMultiSalesOrderLines.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12496,'/olm/salesorder/EditMultiSalesOrderLinesGrid.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12497,'/olm/salesorder/EditRetailOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12498,'/olm/salesorder/EditSalesOrderHeader.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12499,'/olm/salesorder/EditSalesOrderLine.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12500,'/olm/salesorder/MassUpdateAttributes.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12501,'/olm/salesorder/SalesOrderHeaderExtCreate.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12502,'/olm/salesorder/SalesOrderHeaderExtEdit.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12503,'/olm/salesorder/SalesOrderLineExtCreateEdit.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12504,'/olm/salesorder/SalesOrderLineExtListEdit.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12505,'/olm/salesorder/CustomerOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12506,'/olm/salesorder/OrderHeader.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12507,'/olm/salesorder/OrderLine.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12508,'/olm/salesorder/RetailOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12509,'/olm/salesorder/SalesOrderAlertList.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12510,'/olm/salesorder/SalesOrderAuditTrail.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12511,'/olm/salesorder/SalesOrderDOLineList.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12512,'/olm/salesorder/SalesOrderDOList.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12513,'/olm/salesorder/SalesOrderHeaderExtDetail.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12514,'/olm/salesorder/SalesOrderLineExtDetail.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12515,'/olm/salesorder/SalesOrderLineList.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12516,'/olm/salesorder/StoreOrderList.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12517,'/olm/salesorder/ViewCustomerOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12518,'/olm/salesorder/ViewRetailOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12519,'/olm/salesorder/ViewSalesOrderHeader.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12520,'/olm/salesorder/ViewSalesOrderLine.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12521,'/doms/dom/selling/promotion/PromotionScope.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12522,'/doms/dom/selling/promotion/PromotionDetails.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12523,'/doms/dom/selling/payment/PaymentRule.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12524,'/doms/dom/template/view/SOProcessingTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12525,'/doms/dom/template/view/DOMProcessTemplatePreviewFilteredList.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12526,'/doms/dom/template/view/DOMProcessTemplateList.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12527,'/doms/dom/template/view/PrioritizationTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12528,'/doms/dom/rule/jsp/SourcingRuleList.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12529,'/doms/dom/template/view/AllocationTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12530,'/doms/dom/template/view/DOCreateTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12531,'/doms/dom/template/view/DOReleaseTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12532,'/doms/dom/template/view/SourceExclusionTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12533,'/doms/dom/template/view/InventorySegmentationTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12534,'/doms/dom/template/view/InventorySyncTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12535,'/doms/dom/template/view/DOMProcessTemplateList.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12536,'/doms/dom/template/view/InventoryAlertTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12537,'/doms/dom/admin/parameter/jsp/EditDOMParameters.jsp','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12538,'/doms/dom/selling/admin/parameter/jsp/EditDSParameters.jsp','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12539,'/services/olm/item/itemlist','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12540,'/services/olm/item/productClassList','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12541,'/services/olm/item/promotionDetails','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12542,'/services/olm/item/itemUOM','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12543,'/services/olm/item/commonDeliveryOptionsForItems','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12544,'/services/olm/item/shippingMethodsForItems','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12545,'/services/olm/agenthistory/agenthistorylist','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12546,'/services/olm/syscodes/syscodes','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12547,'/services/olm/syscodes/syscodesdesc','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12548,'/services/olm/syscodes/syscodeByCodeId','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12549,'/services/olm/customer/customerDetailsByOrder','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12550,'/services/olm/customer/availableAddresses','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12551,'/services/olm/location/storelist','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12552,'/services/olm/location/countrylist','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12553,'/services/olm/location/statelist','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12554,'/services/olm/location/facilityDetails','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12555,'/services/olm/location/userFacilityDetails','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12556,'/services/olm/customerorder/accountTypes','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12557,'/services/olm/customerorder/echeckCustomerTypes','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12558,'/services/olm/customerorder/saveCustomerInfo','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12559,'/services/olm/customerorder/createGiftCard','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12560,'/services/olm/customerorder/reasonCodes','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12561,'/services/olm/promotions/orderPromotions','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12562,'/services/olm/promotions/itemListPromotions','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12563,'/services/olm/promotions/applyPromotionToOrder','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12564,'/services/olm/promotions/applyPromotionToOrderLine','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12565,'/services/olm/promotions/removePromotionFromOrder','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12566,'/services/olm/promotions/removePromotionFromOrderLine','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12567,'/services/olm/promotions/applyBXGYPromotionToOrderLine','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12568,'/services/olm/appeasements/appeasementList','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12569,'/services/olm/cardTypes/cardType','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12570,'/services/olm/basedata/shipVia','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12571,'/services/olm/basedata/noteType/noteTypes','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12572,'/services/olm/basedata/orderType/orderTypes','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12573,'/services/olm/basedata/companyParameter/companyParameterList','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12574,'/services/olm/search/customers','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12575,'/services/olm/supplyBalance/supplyBalanceDetails','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12576,'/services/olm/avs/verifyAddress','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12577,'/services/olm/log/saveUserActivity','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12578,'/services/CustomerMasterWebService','DOM',3,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12579,'/services/DOMWebService','DOM',3,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12580,'/services/CustomerOrderItemWebService','DOM',3,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12581,'/services/CustomerOrderWebService','DOM',3,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12582,'/services/olm/returns/itemsinorder','RLM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12583,'/services/olm/returns/returnlabels','RLM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12584,'/services/olm/returns/returnShippingMethods','RLM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12585,'/services/olm/returns/itemAvailability','RLM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12586,'/services/olm/returns/resendReturnLabel','RLM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12587,'/doms/dom/managefulfillmenttier/FulfillmentTierDetail.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12588,'/doms/dom/managefulfillmenttier/FulfillmentTierList.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12589,'/doms/dom/needslayer/jsp/NeedsLayer.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12590,'/doms/dom/selling/common/OrderList.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12591,'/olm/salesorder/StoreOrderList.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12592,'/olm/salesorder/SalesOrderLineList.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12593,'/doms/dom/orderallocation/jsp/AllocationOrderList.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12594,'/doms/dom/template/view/FlowTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12595,'/doms/dom/template/view/DOCreateTemplate.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12596,'/doms/dom/workflow/admin/jsp/WorkflowDetails.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12597,'/olm/inventorysegment/jsp/InventorySegment.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12598,'/olm/salesorder/EditRetailOrder.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12599,'/olm/salesorder/ViewRetailOrder.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12633,'/services/atc/outageRule/saveFulfillmentOutageRule','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12615,'/services/olm/syscodes/getSyscodeList','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12616,'/services/olm/item/getItemNames','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12617,'/services/olm/item/getItemStyles','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12618,'/workflow/template/view/InventorySyncTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12605,'/services/atc/cvDef/getSystemProperties','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12606,'/services/atc/cvDef/copyViewConfig','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12607,'/services/atc/cvDef/getViewDetails','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12608,'/services/atc/cvDef/rebuildView','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12609,'/services/atc/cvDef/syncView','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12640,'/services/atc/outageRule/deleteFulfillmentOutageRule','DOM',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12611,'/services/atc/inventoryEvent/processInventoryEvent','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12612,'/services/atc/inventoryEvent/processInventoryEventList','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12635,'/services/atc/outageRule/getFulfillmentOutageRules','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12619,'/workflow/template/view/InventorySegmentationTemplate.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12636,'/services/atc/outageRule/getFulfillmentOutageRuleDetails','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12623,'/services/atc/cvDef/activateView','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12624,'/services/rest/Integration/SalesOrder/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12625,'/doms/dom/selling/admin/parameter/jsp/EditDSParametersProcess.jsp','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12626,'/doms/dom/admin/parameter/jsp/EditDOMParametersProcess.jsp','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12627,'/doms/dom/template/view/DOMProcessTemplatePreviewFilteredList.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12628,'/services/olm/location/getDCTypeFacilityNames','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12629,'/services/olm/location/getStoreTypeFacilityNames','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12630,'/services/olm/location/getSupplierTypeFacilityNames','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12631,'/services/olm/location/getOtherFacilityNames','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12632,'/doms/dom/template/view/AllocationTemplate.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12638,'/services/atc/inventory/getPerpetualInventorySummary','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12642,'/services/rest/Integration/receiveInventoryEvent/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12641,'/services/atc/inventory/updatePerpetualInventoryErrorCode','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12643,'/services/rest/Integration/receiveSyncEvent/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12644,'/services/rest/Integration/receiveSegmentEvent/import','DOM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12645,'/services/atc/inventory/getIdFromTCObject','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12654,'/doms/dom/admin/jsp/time.jsp','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12647,'/services/atc/inventory/getSegmentedInventorySummary','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12648,'/services/atc/inventory/getOnHandTransactionList','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12649,'/services/atc/inventory/transferBetweenSegments','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12650,'/services/atc/inventory/getSegmentPlanList','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12651,'/services/atc/inventory/getPerpetualInventoryList','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12652,'/services/atc/inventory/getSegmentedInventoryList','DOM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12653,'/manh/olm/inventory/ForwardToObjectDetailUI.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12655,'/manh/olm/inventory/admin/parameter/*','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12656,'/services/atc/inventory/getPerpetualInventoryDetails','DOM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12657,'/doms/dom/selling/co/customerorder/EditCustomerOrder.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12658,'/doms/dom/selling/co/customerorder/EditCustomerOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12659,'/doms/dom/selling/co/customerorder/ViewCustomerOrder.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12660,'/doms/dom/selling/co/customerorder/ViewCustomerOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12661,'/doms/dom/selling/co/customerorder/ViewExchangeOrder.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12662,'/doms/dom/admin/jsp/EligibleViews.jsp','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12663,'/doms/dom/admin/jsp/RebuildQuery.jsp','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12664,'/doms/dom/ui.manageCache.do','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12665,'/rlm/admin/parameter/jsp/EditRLMParameters.jsp','RLM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12666,'/rlm/admin/parameter/jsp/EditRLMParametersProcess.jsp','RLM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12667,'/rlm/template/view/FeeTemplate.jsflps','RLM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12668,'/rlm/template/view/FeeTemplate.xhtml','RLM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12669,'/rlm/template/view/OrderFeeRuleDetails.xhtml','RLM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12670,'/rlm/template/view/OrderLineFeeRuleDetails.xhtml','RLM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12671,'/rlm/template/view/FeeRules.xhtml','RLM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12672,'/rlm/template/view/FeeRulesList.xhtml','RLM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12681,'/reports/admin/jsp/DevTools.jsp','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12682,'/reports/admin/jsp/MRFMonitoring.jsp','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12683,'/manh/tools/sa/logging/LoggerOutput.xhtml','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12684,'/manh/tools/sa/logging/LoggerMgmt.xhtml','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12685,'/services/rest/oum/RoleService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12686,'/services/rest/oum/AccessControlService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12688,'/basedata/season/jsp/ProcessSeason.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12689,'/basedata/util/PDFViewer.jsp','TRANS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12690,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/commonEnableDisablePOButtons','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12691,'/services/rest/cbotransactional/CustomParamsService/customParams','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12692,'/basedata/routingguide/jsp/saveSurge.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12693,'/docMgmtImportFileServlet.serv','APPT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12695,'/te/cdt/ui/CDTMasterData.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12696,'/te/cdt/ui/CDTZones.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12697,'/te/cdt/ui/CDTRates.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12698,'/te/cdt/ui/CDTPOAImport.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12699,'/te/parcelRate/ui/ParcelDataZones.xhtml','VPCLZONERATE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12700,'/ilm/setup/yardtpcompany/jsp/yardTCEList.jsp','YARD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12701,'/ofr/admin/user/jsp/lookup/userlookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12702,'/ofr/booking/jsp/BookingListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12703,'/fm/trip/ModifyShipment.xhtml','DPT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12704,'/fm/jsp/TeamAssignment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12705,'/fm/jsp/teamCodeLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12706,'/fm/lookup/dispatchTeamLookup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12707,'/services/rest/tpe/maui/customattribute/load','TPE',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12708,'/ofr/EDIMessage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12709,'/ofr/MessageListener.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12710,'/ofr/optimizationmgmt/consolidation/enginespecific/bkpl/jsp/EngineSpecificRunDetailsPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12711,'/fieldVision.serv','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12712,'/cbo/transactional/dashboard/*','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12713,'/scemDashboardPoTrackerChartServlet.serv','EEM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12714,'/services/rest/eem/EEMFiniteValueService/*','EEM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12715,'/services/rest/eem/EEMFiniteValueService/*','EEM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12716,'/services/rest/eem/EEMMAUIService/EEMMAUIServices/*','EEM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12717,'/doms/dom/selling/payment/ProvideECheckReportSimulationData.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12718,'/doms/dom/admin/jsp/CacheStatistics.jsp','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12719,'/doms/dom/selling/payment/PaymentTransaction.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11925,'/ofr/shipserv/jsp/ReceiveShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11926,'/ofr/shipserv/jsp/ReviewEditShipment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11927,'/ofr/shipserv/jsp/ReviewStopListForEditPopUp.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11928,'/ofr/shipserv/jsp/ShipmentAccountCodingPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11929,'/ofr/shipserv/jsp/ShipmentCRUDError.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11930,'/ofr/shipserv/jsp/ShipmentErrors.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11931,'/ofr/shipserv/jsp/ShipmentListPresentation-Old.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11932,'/ofr/shipserv/jsp/ShipmentListPresentationBulkResponse.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11933,'/ofr/shipserv/jsp/ShipmentOrderList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11934,'/ofr/shipserv/jsp/ShipperSizesOverride.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11935,'/ofr/shipserv/jsp/showDependentShipments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11936,'/ofr/shipserv/jsp/StopNotes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11937,'/ofr/shipserv/jsp/UploadOrdertXMLs.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11938,'/ofr/shipserv/jsp/ViewBulkShipmentSoftChecks.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11939,'/ofr/shipserv/jsp/ViewPathOptions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11940,'/ofr/shipserv/jsp/ViewShipmentCriticalChangeResult.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11941,'/ofr/shipserv/jsp/ViewShipmentList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11942,'/ofr/shipserv/jsp/ViewShipmentLoadDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11943,'/ofr/TaskManagementIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11944,'/ofr/test/CustomImportForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11945,'/ofr/test/CustomImportProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11946,'/ofr/test/ER.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11947,'/ofr/test/ExternalRatingPE.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11948,'/ofr/test/ExternalRatingPEProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11949,'/ofr/test/ExternalRatingTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11950,'/ofr/test/FacilityImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11951,'/ofr/test/GetRatingData_PE.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11952,'/ofr/test/OrderImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11953,'/ofr/test/OrderImportForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11954,'/ofr/test/OrderImportProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11955,'/ofr/test/ParcelTest2.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11956,'/ofr/test/ProcessGetRatingData_PE.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11957,'/ofr/test/RateTest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11958,'/ofr/test/RatingLaneImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11959,'/ofr/test/RatingLaneImportForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11960,'/ofr/test/RatingLaneImportProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11961,'/ofr/test/SWFOImport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11989,'/ofr/mcw/client/jsp/mcw.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11990,'/ofr/consolidation/jsp/MultipleShipmentsDeconsolidate.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11991,'/services/rest/tpe/TPEDistributionOrderActionService/tpeDOListActions/fetchChildOrders','ACM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11992,'/tpe/shipment/map/planning/jsp/MapLoader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12144,'/fm/driver/DriverHolidaySummary.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12145,'/fm/trip/triptype/TripTypeList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12146,'/fm/bid/ViewTripBidList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12147,'/fm/obcmessage/OBCMessageList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12148,'/fm/trip/ManualSeg.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12149,'/fm/trip/TripCCLDetails.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12150,'/common/api/map/jsp/MapLoader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12152,'/ofr/customization/globalvisibility/StatusInquiry.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12153,'/ofr/filter/jsp/FilterDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12156,'/ofr/mcw/client/jsp1/CustomizeDisplayDetail.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12157,'/ofr/mcw/client/jsp1/getGanttChart.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12158,'/ofr/mcw/client/jsp1/mcw.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12159,'/ofr/mcw/client/jsp1/MCWJSLocalizedMessages.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12160,'/ofr/mcw/client/jsp1/planpath.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12161,'/ofr/mcw/client/jsp1/redirectToMCW.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12162,'/ofr/mcw/client/jsp1/s/AdjustGateTimes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12163,'/ofr/shipserv/jsp/MultiOrderPathAnalysis.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12164,'/ilm/CreateAppointmentWithShipment.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12167,'/obluploadServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12168,'/ofr//ArchivedPOList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12169,'/ofr/ArchivedInvoiceList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12170,'/ofr/ArchivedPODetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12171,'/ofr/ArchivedPOList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12172,'/ofr/archivedQuickInvoice.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12173,'/ofr/BookingList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12174,'/ofr/consolidation/multiOrderPath.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12175,'/ofr/CreateBooking.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12176,'/ofr/distributionorder/jsp/bypassMCW.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12177,'/ofr/EditBooking.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12178,'/ofr/EditOrderDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12179,'/ofr/EditShipmentDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12180,'/ofr/PurchaseOrderCreate.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12181,'/ofr/PurchaseOrderEdit.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12182,'/ofr/quickInvoice.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12183,'/ofr/SubmitCreateBooking.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12184,'/ofr/SubmitCreatePO.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12185,'/ofr/SubmitEditBooking.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12186,'/ofr/SubmitEditPO.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12187,'/ofr/VcpPurchaseOrderCreate.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12188,'/ofr/VcpPurchaseOrderDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12189,'/ofr/VcpPurchaseOrderEdit.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12190,'/ofr/VcpSubmitCreatePO.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12191,'/ofr/VcpSubmitEditPO.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12192,'/ofr/ViewBookingDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12193,'/ofr/ViewClaimList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12194,'/ofr/ViewDependentShipmentList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12195,'/ofr/ViewDetentionRequestList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12196,'/ofr/ViewInvoiceList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12197,'/ofr/ViewOrderDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12198,'/ofr/ViewPODetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12199,'/ofr/ViewPrefixedShipmentList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12200,'/ofr/ViewRunOrderList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12201,'/ofr/ViewShipmentDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12202,'/ofr/ViewVcpClaimList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12203,'/ofr/ViewVCPShipmentList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12204,'/ofrConsolidationRegionCreateServices.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12205,'/ofrConsolidationRegionUpdateServices.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12206,'/ofrFilterDispatcherServices.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12207,'/ofrShipperLogEntryListServices.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12208,'/ofrSystemLogEntryListServices.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12209,'/rts/RTSAction.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12210,'/tpFilterDispatcherServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12211,'/transportationorder/transportationOrderDetails.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12212,'/ofr/VcpPurchaseOrderList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12213,'/ofrShipperResourceConfigSaveConfig.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12214,'/ofr/rs/jsp/processShipmentComment.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12215,'/ofr/rs/jsp/addShipmentNotes.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12216,'/ofr/rs/jsp/shipmentTenderComments.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12217,'/services/rest/tpe/TPEShipmentListActionService/tpeShipmentListActions/retrieveStops','TPE',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12218,'/ofr/consolidation/workspace/jsp/ShipmentStopList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12219,'/ofr/consolidation/workspace/jsp/ShipmentStopList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12222,'/obl/AProcess/create_AProcess.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12223,'/obl/AProcess/create_AProcess_init.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 12224,'/obl/AProcess/success.jsp','RFPM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11962,'/ofr/test/SWFOImportForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11963,'/ofr/test/SWFOImportOffline.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11964,'/ofr/test/SWFOImportOfflineProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11965,'/ofr/test/SWFOImportProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11966,'/ofr/test/TestTransitTimeWebService.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11967,'/ofr/test/TXMLImportForm.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11968,'/ofr/test/TXMLImportProcessor.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11969,'/ofr/test/unittest/UTTestNG.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11970,'/ofr/test/xri.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11971,'/ofr/trackingmsg/jsp/ViewAllMessages.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11972,'/ofr/vcp/alert/jsp/VcpAlertDetails.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11973,'/ofr/vcp/alert/jsp/VcpAlertList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11974,'/ofr/vcp/alert/jsp/VcpProcessAlertListActions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11975,'/ofr/VendorVisibilityIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11976,'/ofr/version.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11977,'/ofr/YardManagementIndex.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11978,'/techdemo/filters/DispatchPage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11979,'/techdemo/filters/FilterHeader.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11980,'/techdemo/filters/ShipmentListPresentation.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11981,'/techdemo/filters/test.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11982,'/techdemo/permissions/jsp/PermQuery.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11983,'/techdemo/permissions/jsp/ProductQuery.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11984,'/techdemo/Schedulertest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 11985,'/techdemo/test.jsp','ACM',1,null);

Commit;





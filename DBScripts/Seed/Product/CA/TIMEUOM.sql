set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TIMEUOM

INSERT INTO TIMEUOM ( TIMEUOM,DESCRIPTION) 
VALUES  ( 0,'Hour(s)');

INSERT INTO TIMEUOM ( TIMEUOM,DESCRIPTION) 
VALUES  ( 1,'Day(s)');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_INVOICE_STATUS

INSERT INTO A_INVOICE_STATUS ( INVOICE_STATUS,DESCRIPTION) 
VALUES  ( 10,'Open');

INSERT INTO A_INVOICE_STATUS ( INVOICE_STATUS,DESCRIPTION) 
VALUES  ( 20,'Closed');

INSERT INTO A_INVOICE_STATUS ( INVOICE_STATUS,DESCRIPTION) 
VALUES  ( 30,'Canceled');

Commit;





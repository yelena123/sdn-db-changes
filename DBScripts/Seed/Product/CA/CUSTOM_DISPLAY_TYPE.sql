set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CUSTOM_DISPLAY_TYPE

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'BOOKLST','Booking List','BOOKING',null,'BOOKING.BOOKING_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'MCSHPLST','Manual Consol Shipment List','SHIPMENT, ORDERS, STOP_ACTION_ORDER','(SHIPMENT.creation_type = 20 AND SHIPMENT.IS_CANCELLED = 0 AND SHIPMENT.SHIPMENT_STATUS <> 90 AND SHIPMENT.PP_SHIPMENT_ID IS NULL) and not exists (select 1 from cws_shipment a where a.ORIG_SHIPMENT_ID = shipment.SHIPMENT_ID)','SHIPMENT.SHIPMENT_ID','orders.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID and orders.order_id=stop_action_order.order_id');

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'MORLST','Master Order List','PURCHASE_ORDERS','WHERE ( (PURCHASE_ORDERS.TC_COMPANY_ID = ? AND PURCHASE_ORDERS.IS_INFO_ONLY = 0) AND (	( PURCHASE_ORDERS.OUTBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = PURCHASE_ORDERS.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''O'' ) ) OR ( PURCHASE_ORDERS.INBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = PURCHASE_ORDERS.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''I'' ) ) ) ) ','PURCHASE_ORDERS.PURCHASE_ORDERS_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'NXDPSH','Next Dependent Shipments','SHIPMENT','WHERE ( (SHIPMENT.TC_COMPANY_ID = ?) AND SHIPMENT.SHIPMENT_ID IN (SELECT DISTINCT OM2.SHIPMENT_ID FROM ORDER_MOVEMENT OM1, ORDER_MOVEMENT OM2 WHERE OM1.SHIPMENT_ID = ? AND OM1.ORDER_ID = OM2.ORDER_ID AND OM2.ORDER_SHIPMENT_SEQ = (OM1.ORDER_SHIPMENT_SEQ + 1)))','SHIPMENT.SHIPMENT_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'ORDLST','Order List','ORDERS','WHERE ( (ORDERS.TC_COMPANY_ID = ? AND ORDERS.IS_INFO_ONLY = 0 AND ORDERS.IS_MASTER_ORDER = 0) AND (	( ORDERS.OUTBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = ORDERS.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''O'' ) ) OR ( ORDERS.INBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = ORDERS.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''I'' ) ) ) )','ORDERS.ORDER_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'ORDMOV','Order Movement Detail','SHIPMENT','where (shipment.shipment_id in (select shipment_id from orders_order_movement_view where orders_order_movement_view.order_id = ?))','SHIPMENT.shipment_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'ORTLST','Order Template List','ORDERS_TEMPLATE','WHERE ( (ORDERS_TEMPLATE.TC_COMPANY_ID = ? AND ORDERS_TEMPLATE.IS_INFO_ONLY = 0 AND ORDERS_TEMPLATE.IS_MASTER_ORDER) AND (	( ORDERS.OUTBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = ORDERS_TEMPLATE.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''O'' ) ) OR ( ORDERS_TEMPLATE.INBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = ORDERS_TEMPLATE.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''I'' ) ) ) )','ORDERS.ORDER_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'PRDPSH','Previous Dependent Shipments','SHIPMENT','WHERE ( (SHIPMENT.TC_COMPANY_ID = ?) AND SHIPMENT.SHIPMENT_ID IN (SELECT DISTINCT OM2.SHIPMENT_ID FROM ORDER_MOVEMENT OM1, ORDER_MOVEMENT OM2 WHERE OM1.SHIPMENT_ID = ? AND OM1.ORDER_ID = OM2.ORDER_ID AND OM2.ORDER_SHIPMENT_SEQ = (OM1.ORDER_SHIPMENT_SEQ - 1)))','SHIPMENT.SHIPMENT_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'SHPLST','Shipment List','SHIPMENT','WHERE ( (SHIPMENT.TC_COMPANY_ID = ?) AND (	( SHIPMENT.OUTBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = SHIPMENT.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''O'' ) ) OR ( SHIPMENT.INBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = SHIPMENT.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''I'' ) ) ) )','SHIPMENT.SHIPMENT_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'SHTLST','Shipment Template List','SHIPMENT_TEMPLATE','WHERE ( (SHIPMENT_TEMPLATE.TC_COMPANY_ID = ?) AND (	( SHIPMENT_TEMPLATE.OUTBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = SHIPMENT_TEMPLATE.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''O'' ) ) OR ( SHIPMENT_TEMPLATE.INBOUND_REGION_ID IN ( SELECT CFR.REGION_ID FROM CONTACT_FACILITY_REGION CFR WHERE CFR.TC_COMPANY_ID = SHIPMENT_TEMPLATE.TC_COMPANY_ID AND CFR.CONTACT_ID = ? AND CFR.DIRECTION = ''I'' ) ) ) )','SHIPMENT_TEMPLATE.SHIPMENT_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'SLDPSH','Selected Dependent Shipment','SHIPMENT','WHERE (SHIPMENT.SHIPMENT_ID = ?)','SHIPMENT.SHIPMENT_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'VMOLST','VCP Master Order List','ORDERS','WHERE EXISTS ( SELECT TC_COMPANY_ID, BUSINESS_PARTNER_ID FROM BUSINESS_PARTNER WHERE  BUSINESS_PARTNER.BP_COMPANY_ID = ? AND BUSINESS_PARTNER.TC_COMPANY_ID = ORDERS.TC_COMPANY_ID AND BUSINESS_PARTNER.BUSINESS_PARTNER_ID  = ORDERS.BUSINESS_PARTNER_ID) AND ORDERS.IS_INFO_ONLY = 0 AND ORDERS.IS_MASTER_ORDER = 1 AND ORDERS.HAS_IMPORT_ERROR = 0 AND (  ACCEPTANCE_STATUS IS NULL OR ACCEPTANCE_STATUS <> 32 ) AND ( IS_ACCEPTANCE_REQUIRED = ( CASE WHEN ACCEPTANCE_STATUS IS NULL THEN 0  WHEN ACCEPTANCE_STATUS <> 0 THEN 1 ELSE -1 END) ) AND (datediff( RELEASE_DTTM , getdate() ) <= 0 OR RELEASE_DTTM IS NULL)','ORDERS.ORDER_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'VORLST','VCP Order List','ORDERS','WHERE EXISTS (SELECT TC_COMPANY_ID, BUSINESS_PARTNER_ID FROM BUSINESS_PARTNER WHERE BUSINESS_PARTNER.BP_COMPANY_ID = ? AND
BUSINESS_PARTNER.BUSINESS_PARTNER_ID = ORDERS.BUSINESS_PARTNER_ID) AND ORDERS.IS_INFO_ONLY = 0 AND ORDERS.IS_MASTER_ORDER = 0 AND
ORDERS.HAS_IMPORT_ERROR = 0 AND NOT EXISTS (SELECT 1 FROM SHIPMENT WHERE ORDERS.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID AND
SHIPMENT.HAS_IMPORT_ERROR = 1)','ORDERS.ORDER_ID',null);

INSERT INTO CUSTOM_DISPLAY_TYPE ( CUSTOM_DISPLAY_TYPE,DESCRIPTION,FROM_CLAUSE,WHERE_CLAUSE,ID_COLUMN,OUTER_WHERE_CLAUSE) 
VALUES  ( 'VSHLST','VCP Shipment List','ORDERS,  SHIPMENT','WHERE EXISTS (SELECT TC_COMPANY_ID, BUSINESS_PARTNER_ID FROM BUSINESS_PARTNER WHERE BUSINESS_PARTNER.BP_COMPANY_ID = ? AND BUSINESS_PARTNER.tc_company_id=SHIPMENT.tc_company_id AND SHIPMENT.tc_company_id=ORDERS.tc_company_id AND SHIPMENT.SHIPMENT_ID = ORDERS.SHIPMENT_ID AND BUSINESS_PARTNER.BUSINESS_PARTNER_ID = ORDERS.BUSINESS_PARTNER_ID AND BUSINESS_PARTNER.MARK_FOR_DELETION = 0 ) AND ORDERS.IS_MASTER_ORDER = 0 AND ORDERS.HAS_IMPORT_ERROR = 0 AND NVL(SHIPMENT.HAS_IMPORT_ERROR,0) = 0 ','SHIPMENT.SHIPMENT_ID,ORDERS.ORDER_ID','ORDERS.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID');

Commit;





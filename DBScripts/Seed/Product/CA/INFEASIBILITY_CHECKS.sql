set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for INFEASIBILITY_CHECKS

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 7,'MAX (Spend limit constraint for Carrier C on Lane L) <= MIN(spend force constraint for Carrier C on Lane L)','MAX(Spend limit constraint for Carrier C on Lane L) <= MIN(spend force constraint for Carrier C on Lane L)','infeasibility_check_07','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 8,'MAX capacity for Carrier C on Lane L <= Volume for Carrier C on Lane L created by MIN spend force','MAX capacity for Carrier C on Lane L <= Volume for Carrier C on Lane L created by MIN spend force','infeasibility_check_08','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 9,'MAX Carrier volume constraint on Lane L <= Volume for Carrier C on Lane L created by MIN spend force','MAX Carrier volume constraint on Lane L <= Volume for Carrier C on Lane L created by MIN spend force','infeasibility_check_09','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 10,'Volume for Carrier C on Lane L created by MAX spend limit <= MIN Carrier volume constraint on Lane L','Volume for Carrier C on Lane L created by MAX spend limit <= MIN Carrier volume constraint on Lane L','infeasibility_check_10','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 11,'MAX(minimum volume constraint for carrier C on area A or facility F)  <= MIN(maximum volume constraint for carrier C on area A or facility F)','MAX(minimum volume constraint for carrier C on area A or facility F)  <= MIN(maximum volume constraint for carrier C on area A or facility F)','infeasibility_check_11','Type: Min Carrier Volume [Level: Area or Facility]',1,2);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 12,'MAX(minimum volume constraint for carrier C on area A or facility F)  <= SUM(Lane capacity on all lanes coming out of Area A or Facility F for Carrier C)','MAX(minimum volume constraint for carrier C on area A or facility F)  <= SUM(Lane capacity on all lanes coming out of Area A or Facility F for Carrier C)','infeasibility_check_12','Type: Min Carrier Volume [Level: Area or Facility]',1,2);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 13,'MAX(minimum volume constraint for carrier C on area A or facility F)  <= System capacity for C','MAX(minimum volume constraint for carrier C on area A or facility F)  <= System capacity for C','infeasibility_check_13','Type: Min Carrier Volume [Level: Area or Facility]',1,2);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 14,'SUM (MAX(minimum volume constraint for carrier C on area A or facility F)), for all carriers on area A or facility F   <= SUM(lane volume for each lane L, either Inbound or Outbound, for area A or facility F)','SUM (MAX(minimum volume constraint for carrier C on area A or facility F)), for all carriers on area A or facility F   <= SUM(lane volume for each lane L, either Inbound or Outbound, for area A or facility F)','infeasibility_check_14','Type: Min Carrier Volume [Level: Area or Facility]',1,2);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 15,'SUM (MAX(minimum volume constraint for carrier C on area A or facility F)), for carrier C on all facilities    <= System capacity  for carrier C','SUM (MAX(minimum volume constraint for carrier C on area A or facility F)), for carrier C on all facilities    <= System capacity  for carrier C','infeasibility_check_15','Type: Min Carrier Volume [Level: Area or Facility]',1,2);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 16,'MAX Spend limit constraint for Carrier C on area A or facility F  <= MIN spend force constraint for Carrier C on area A or facility F','MAX Spend limit constraint for Carrier C on area A or facility F  <= MIN spend force constraint for Carrier C on area A or facility F','infeasibility_check_16','Type: Min Carrier Volume [Level: Area or Facility]',1,2);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 17,'MAX capacity for Carrier C on area A or facility F  <= Volume for Carrier C on area A or facility F created by MIN spend force','MAX capacity for Carrier C on area A or facility F <= Volume for Carrier C on area A or facility F created by MIN spend force','infeasibility_check_17','Type: Min Carrier Volume [Level: Area or Facility]',1,2);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 18,'MAX Carrier volume constraint on area A or facility F  <= Volume for Carrier C on area A or facility F created by MIN spend force','MAX Carrier volume constraint on area A or facility F  <= Volume for Carrier C on area A or facility F created by MIN spend force','infeasibility_check_18','Type: Min Carrier Volume [Level: Area or Facility]',1,2);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 19,'Volume for Carrier C on area A or facility F created by MAX spend limit  <= MIN Carrier volume constraint on area A or facility F','Volume for Carrier C on area A or facility F created by MAX spend limit  <= MIN Carrier volume constraint on area A or facility F','infeasibility_check_19','Type: Min Carrier Volume [Level: Area or Facility]',1,2);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 20,'MAX(minimum volume constraint for carrier C on system)    <= MIN(maximum volume constraint for carrier C on system)','MAX(minimum volume constraint for carrier C on system)    <= MIN(maximum volume constraint for carrier C on system)','infeasibility_check_20','Type: Min Carrier Volume [Level: System]',1,3);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 21,'MAX Spend limit constraint for Carrier C on System   <= MIN spend force constraint for Carrier C on System','MAX Spend limit constraint for Carrier C on System   <= MIN spend force constraint for Carrier C on System','infeasibility_check_21','Type: Min Carrier Volume [Level: System]',1,3);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 22,'MAX capacity for Carrier C on System  <=Volume for Carrier C on System created by MIN spend force','MAX capacity for Carrier C on System  <=Volume for Carrier C on System created by MIN spend force','infeasibility_check_22','Type: Min Carrier Volume [Level: System]',1,3);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 23,'MAX Carrier volume constraint on System <=Volume for Carrier C on System created by MIN spend force','MAX Carrier volume constraint on System <=Volume for Carrier C on System created by MIN spend force','infeasibility_check_23','Type: Min Carrier Volume [Level: System]',1,3);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 24,'Volume for Carrier C on System created by MAX spend limit <=MIN Carrier volume constraint on System','Volume for Carrier C on System created by MAX spend limit <=MIN Carrier volume constraint on System','infeasibility_check_24','Type: Min Carrier Volume [Level: System]',1,3);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 1,'MAX(minimum volume constraint for carrier C on lane L) <= MIN(maximum volume constraint for carrier C on lane L)','MAX(minimum volume constraint for carrier C on lane L) <= MIN(maximum volume constraint for carrier C on lane L)','infeasibility_check_01','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 2,'MAX(minimum volume constraint for carrier C on lane L) <= MAX (Lane and/or package capacity for carrier C on lane L)','MAX(minimum volume constraint for carrier C on lane L) <= MAX (Lane and/or package capacity for carrier C on lane L)','infeasibility_check_02','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 3,'MAX(minimum volume constraint for carrier C on lane L)   <= Area or Facility capacity, either Inbound or Outbound, for carrier C','MAX(minimum volume constraint for carrier C on lane L)   <= Area or Facility capacity, either Inbound or Outbound, for carrier C','infeasibility_check_03','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 4,'MAX(minimum volume constraint for carrier C on lane L)   <= System capacity for carrier C','MAX(minimum volume constraint for carrier C on lane L)   <= System capacity for carrier C','infeasibility_check_04','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 5,'SUM (MAX(minimum volume constraint for carrier C on lane L)), for all carriers on lane L <= Volume on lane L','SUM (MAX(minimum volume constraint for carrier C on lane L)), for all carriers on lane L <= Volume on lane L','infeasibility_check_05','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 6,'SUM (MAX(minimum volume constraint for carrier C on lane L)), for carrier C on all lanes  <= System capacity for carrier C','SUM (MAX(minimum volume constraint for carrier C on lane L)), for carrier C on all lanes  <= System capacity for carrier C','infeasibility_check_06','Type: Min Carrier Volume [Level: Lane]',1,1);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 25,'If 1 and 2 exist then, MAX(winning carrier min volume for carrier C on lane L)  <= MAX(Lane and/or package capacity for carrier C on lane L)','If 1 and 2 exist then, MAX(winning carrier min volume for carrier C on lane L)  <= MAX(Lane and/or package capacity for carrier C on lane L)','infeasibility_check_25','Type: Winning carrier min volume ',1,4);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 26,'If 3 exists then, MAX(winning carrier min volume for carrier C on area A or facility F)   <=Area or Facility capacity, either Inbound or Outbound, for carrier C','If 3 exists then, MAX(winning carrier min volume for carrier C on area A or facility F)   <=Area or Facility capacity, either Inbound or Outbound, for carrier C','infeasibility_check_26','Type: Winning carrier min volume ',1,4);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 29,'Total number of carriers having a minimum volume constraint on an Area A or facility F   <= Value of maximum number of carriers constraint','Total number of carriers having a minimum volume constraint on an Area A or facility F   <= Value of maximum number of carriers constraint','infeasibility_check_29','Type: Winning carrier min volume ',1,5);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 31,'For a winning carrier C, with a manually awarded package on lane L, MAX(winning carrier min volume for carrier C on lane L)  <= package capacity for carrier C on lane L','For a winning carrier C, with a manually awarded package on lane L, MAX(winning carrier min volume for carrier C on lane L)  <= package capacity for carrier C on lane L','infeasibility_check_31','Type: Manual awards on packages',1,6);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 32,'For a given carrier C1, if there are multiple manually forced package then no two packages should have the same lane','For a given carrier C1, if there are multiple manually forced package then no two packages should have the same lane','infeasibility_check_32','Type: Manual awards on packages',1,6);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 27,'If 4 exists then, MAX(winning carrier min volume for carrier C on system)   <= System capacity for carrier C','If 4 exists then, MAX(winning carrier min volume for carrier C on system)   <= System capacity for carrier C','infeasibility_check_27','Type: Winning carrier min volume ',1,4);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 28,'Total number of carriers having a minimum volume constraint on a lane L   <= Value of maximum number of carriers constraint','Total number of carriers having a minimum volume constraint on a lane L   <= Value of maximum number of carriers constraint','infeasibility_check_28','Type: Winning carrier min volume ',1,5);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 33,'For any constraint on carrier C and Lane L with a valid contract_type, there must be bids on that Lane L from that carrier C with the same contract_type','For any constraint on carrier C and Lane L with a valid contract_type, there must be bids on that Lane L from that carrier C with the same contract_type','infeasibility_check_33','Type: Others',1,7);

INSERT INTO INFEASIBILITY_CHECKS ( INFEASIBILITY_ID,DESCRIPTION,ERROR_DESCRIPTION,CALLS_DB_FUNCTION,UI_GROUP,ENABLE_FLAG,UI_GROUP_ORDER) 
VALUES  ( 30,'Total number of carriers having a minimum volume constraint on system   <= Value of maximum number of carriers constraint','Total number of carriers having a minimum volume constraint on system   <= Value of maximum number of carriers constraint','infeasibility_check_30','Type: Winning carrier min volume ',1,5);

Commit;





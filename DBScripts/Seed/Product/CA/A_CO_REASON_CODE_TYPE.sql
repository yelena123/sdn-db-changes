set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_CO_REASON_CODE_TYPE

INSERT INTO A_CO_REASON_CODE_TYPE ( REASON_CODE_TYPE,DESCRIPTION) 
VALUES  ( 0,'HOLD');

INSERT INTO A_CO_REASON_CODE_TYPE ( REASON_CODE_TYPE,DESCRIPTION) 
VALUES  ( 1,'CANCEL');

INSERT INTO A_CO_REASON_CODE_TYPE ( REASON_CODE_TYPE,DESCRIPTION) 
VALUES  ( 3,'Allocation Failure');

INSERT INTO A_CO_REASON_CODE_TYPE ( REASON_CODE_TYPE,DESCRIPTION) 
VALUES  ( 4,'RMA Alert');

INSERT INTO A_CO_REASON_CODE_TYPE ( REASON_CODE_TYPE,DESCRIPTION) 
VALUES  ( 5,'Resolve Receipt Variance');

INSERT INTO A_CO_REASON_CODE_TYPE ( REASON_CODE_TYPE,DESCRIPTION) 
VALUES  ( 6,'Post Void');

Commit;





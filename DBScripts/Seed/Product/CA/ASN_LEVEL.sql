set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ASN_LEVEL

INSERT INTO ASN_LEVEL ( ASN_LEVEL,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 10,'LPN Level ASN',SYSDATE,systimestamp);

INSERT INTO ASN_LEVEL ( ASN_LEVEL,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 20,'PO Line Item Level ASN',SYSDATE,systimestamp);

INSERT INTO ASN_LEVEL ( ASN_LEVEL,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 0,'None',SYSDATE,systimestamp);

Commit;





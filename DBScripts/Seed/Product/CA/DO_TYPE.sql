set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DO_TYPE

INSERT INTO DO_TYPE ( DO_TYPE,DESCRIPTION) 
VALUES  ( 10,'Store Distributions');

INSERT INTO DO_TYPE ( DO_TYPE,DESCRIPTION) 
VALUES  ( 20,'Customer Order');

INSERT INTO DO_TYPE ( DO_TYPE,DESCRIPTION) 
VALUES  ( 30,'Warehouse Transfer');

INSERT INTO DO_TYPE ( DO_TYPE,DESCRIPTION) 
VALUES  ( 40,'Return to Business Partner');

INSERT INTO DO_TYPE ( DO_TYPE,DESCRIPTION) 
VALUES  ( 50,'Putaway');

INSERT INTO DO_TYPE ( DO_TYPE,DESCRIPTION) 
VALUES  ( 60,'Store Transfer');

INSERT INTO DO_TYPE ( DO_TYPE,DESCRIPTION) 
VALUES  ( 70,'Buy Request');

INSERT INTO DO_TYPE ( DO_TYPE,DESCRIPTION) 
VALUES  ( 80,'Retail Shipment');

Commit;





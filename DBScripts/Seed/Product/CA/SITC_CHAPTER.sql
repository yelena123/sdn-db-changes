set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SITC_CHAPTER

INSERT INTO SITC_CHAPTER ( SITC_CHAPTER,DESCRIPTION) 
VALUES  ( '0','CH-0');

INSERT INTO SITC_CHAPTER ( SITC_CHAPTER,DESCRIPTION) 
VALUES  ( 'I','CH-I');

Commit;





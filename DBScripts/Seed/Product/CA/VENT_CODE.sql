set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for VENT_CODE

INSERT INTO VENT_CODE ( VENT_CODE,DESCRIPTION) 
VALUES  ( 1,'A-Vent 25% open');

INSERT INTO VENT_CODE ( VENT_CODE,DESCRIPTION) 
VALUES  ( 2,'B-Vent 50% open');

INSERT INTO VENT_CODE ( VENT_CODE,DESCRIPTION) 
VALUES  ( 3,'C-Vent 75% open');

INSERT INTO VENT_CODE ( VENT_CODE,DESCRIPTION) 
VALUES  ( 4,'D-Vent 100% open');

INSERT INTO VENT_CODE ( VENT_CODE,DESCRIPTION) 
VALUES  ( 5,'E-Closed');

INSERT INTO VENT_CODE ( VENT_CODE,DESCRIPTION) 
VALUES  ( 6,'Z-Carrier to set');

Commit;





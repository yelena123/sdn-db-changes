set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_FEE_CALCULATION_TYPE

INSERT INTO A_FEE_CALCULATION_TYPE ( FEE_CALCULATION_TYPE_ID,FEE_CALCULATION_TYPE_NAME) 
VALUES  ( 10,'Percentage');

INSERT INTO A_FEE_CALCULATION_TYPE ( FEE_CALCULATION_TYPE_ID,FEE_CALCULATION_TYPE_NAME) 
VALUES  ( 20,'Fixed Amount');

Commit;





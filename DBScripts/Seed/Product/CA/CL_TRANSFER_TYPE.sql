set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_TRANSFER_TYPE

INSERT INTO CL_TRANSFER_TYPE ( TRANSFER_TYPE_ID,TRANSFER_TYPE_NAME) 
VALUES  ( 1,'Binary');

INSERT INTO CL_TRANSFER_TYPE ( TRANSFER_TYPE_ID,TRANSFER_TYPE_NAME) 
VALUES  ( 2,'ASCII');

Commit;





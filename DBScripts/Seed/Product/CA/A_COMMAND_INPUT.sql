set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_COMMAND_INPUT

INSERT INTO A_COMMAND_INPUT ( COMMAND_INPUT_ID,VARIABLE_NAME,VARIABLE_DESCRIPTION,VARIABLE_VALUE,DATA_TYPE,INPUT_SEQUENCE,DYNAMIC_INPUT,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 100,'tc_company_id','Comapany ID',null,'LONG',1,1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for FILTER_FROM_CLAUSE

INSERT INTO FILTER_FROM_CLAUSE ( FILTER_FROM_CLAUSE_ID,FILTER_OBJECT,FROM_CLAUSE,MATCH_STRING,MATCH_GROUP_ID,MATCH_SEQUENCE_ID) 
VALUES  ( 1,'LPN','left outer join LPNUIAttributes.lpn.lpnDetailSet as lpnDetail','lpnDetail.',1,1);

Commit;





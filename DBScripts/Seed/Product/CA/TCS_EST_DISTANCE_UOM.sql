set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_EST_DISTANCE_UOM

INSERT INTO TCS_EST_DISTANCE_UOM ( DISTANCE_UOM_ID,DISTANCE_UOM_NAME) 
VALUES  ( 2,'Kilometers');

INSERT INTO TCS_EST_DISTANCE_UOM ( DISTANCE_UOM_ID,DISTANCE_UOM_NAME) 
VALUES  ( 1,'Miles');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CYCLE_COUNT_INIT_TYPE

INSERT INTO CYCLE_COUNT_INIT_TYPE ( CYCLE_COUNT_INIT_TYPE,DESCRIPTION) 
VALUES  ( 10,'Corporate');

INSERT INTO CYCLE_COUNT_INIT_TYPE ( CYCLE_COUNT_INIT_TYPE,DESCRIPTION) 
VALUES  ( 20,'Store');

Commit;





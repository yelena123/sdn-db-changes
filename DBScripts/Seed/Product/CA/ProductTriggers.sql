set scan off echo on define off;

CREATE OR REPLACE TRIGGER "ISF_BOL_A_IUD_TRG" 
AFTER INSERT OR UPDATE OR DELETE ON ISF_BOL
REFERENCING OLD  AS O NEW AS N
FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE
v_LAST_UPDATED_SOURCE        ISF_HEADER.LAST_UPDATED_SOURCE%TYPE ;
v_LAST_UPDATED_SOURCE_TYPE   ISF_HEADER.LAST_UPDATED_SOURCE_TYPE%TYPE ;

BEGIN


        BEGIN


	IF  INSERTING OR UPDATING THEN

		SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
		INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
		FROM   ISF_HEADER
		WHERE  ISF_HEADER_ID = :N.ISF_HEADER_ID ;

	ELSE

		SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
		INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
		FROM   ISF_HEADER
		WHERE  ISF_HEADER_ID = :O.ISF_HEADER_ID ;

	END IF ;

        EXCEPTION
                WHEN NO_DATA_FOUND THEN
                        v_LAST_UPDATED_SOURCE        := NULL ;
                        v_LAST_UPDATED_SOURCE_TYPE   := NULL ;

        END ;


IF INSERTING THEN
                   ISF_AU_PROC (
                        :N.ISF_HEADER_ID                  ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        :N.BOL_NUMBER                     ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;

IF UPDATING THEN

                   ISF_AU_PROC (
                        :N.ISF_HEADER_ID                  ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        :O.BOL_NUMBER                     ,              --O_BOL_NUMBER
                        :N.BOL_NUMBER                     ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;

IF DELETING THEN

                   ISF_AU_PROC (
                        :O.ISF_HEADER_ID                  ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        :O.BOL_NUMBER                     ,              --O_BOL_NUMBER
                        NULL                     	  ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;

END ;
/


CREATE OR REPLACE TRIGGER "ISF_HEADER_A_UD_TRG" 
AFTER UPDATE OR DELETE ON ISF_HEADER
REFERENCING OLD  AS O NEW AS N
FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
BEGIN

IF UPDATING THEN

         ISF_AU_PROC (
                        :N.ISF_HEADER_ID                  ,              --ISF_ID
                        :O.STATUS                         ,              --O_STATUS
                        :N.STATUS                         ,              --N_STATUS
                        :O.AMENDMENT_REASON_CODE          ,              --O_AMENDMENT_REASON_CODE
                        :N.AMENDMENT_REASON_CODE          ,             --N_AMENDMENT_REASON_CODE
                        :O.PORT_CODE                      ,              --O_PORT_CODE
                        :N.PORT_CODE                      ,              --N_PORT_CODE
                        :O.ISF_IMPORTER                   ,              --O_ISF_IMPORTER
                        :N.ISF_IMPORTER                   ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        :O.VESSEL_CODE                    ,              --O_VESSEL_CODE
                        :N.VESSEL_CODE                    ,              --N_VESSEL_CODE
                        :O.VESSEL_NAME                    ,              --O_VESSEL_NAME
                        :N.VESSEL_NAME                    ,              --N_VESSEL_NAME
                        :O.VOYAGE                         ,              --O_VOYAGE
                        :N.VOYAGE                         ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        :N.LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        :N.LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;

IF DELETING THEN

         ISF_AU_PROC (
                        :O.ISF_HEADER_ID                  ,              --ISF_ID
                        :O.STATUS                         ,              --O_STATUS
                         NULL                        	  ,              --N_STATUS
                        :O.AMENDMENT_REASON_CODE          ,              --O_AMENDMENT_REASON_CODE
                         NULL         			  ,             --N_AMENDMENT_REASON_CODE
                        :O.PORT_CODE                      ,              --O_PORT_CODE
                         NULL                     	  ,              --N_PORT_CODE
                        :O.ISF_IMPORTER                   ,              --O_ISF_IMPORTER
                        NULL                  		  ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        :O.VESSEL_CODE                    ,              --O_VESSEL_CODE
                        NULL                   		  ,              --N_VESSEL_CODE
                        :O.VESSEL_NAME                    ,              --O_VESSEL_NAME
                        NULL                   		  ,              --N_VESSEL_NAME
                        :O.VOYAGE                         ,              --O_VOYAGE
                        NULL                       	  ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        :O.LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        :O.LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;


END ;
/


CREATE OR REPLACE TRIGGER "ASN_DETAIL_A_D" 
   AFTER DELETE
   ON ASN_DETAIL
   REFERENCING OLD AS OLD
   FOR EACH ROW
 WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE
   VLU_SOURCE_TYPE   NUMBER;
   VLU_SOURCE        VARCHAR2 (32);
BEGIN
   SELECT LAST_UPDATED_SOURCE_TYPE
     INTO VLU_SOURCE_TYPE
     FROM ASN
    WHERE ASN_ID = :OLD.ASN_ID;

   SELECT LAST_UPDATED_SOURCE
     INTO VLU_SOURCE
     FROM ASN
    WHERE ASN_ID = :OLD.ASN_ID;

   INSERT INTO ASN_EVENT
               (ASN_ID, ASN_EVENT_ID, FIELD_NAME,
                OLD_VALUE, NEW_VALUE, CREATED_SOURCE_TYPE,
                CREATED_SOURCE, CREATED_DTTM
               )
        VALUES (:OLD.ASN_ID, SEQ_ASN_EVENT_ID.NEXTVAL, 'DELETED ASN DETAIL',
                TO_CHAR (:OLD.ASN_DETAIL_ID), NULL, VLU_SOURCE_TYPE,
                VLU_SOURCE, SYSDATE
               );
END;
/




CREATE OR REPLACE TRIGGER "A_CO_NOTE_AD_DOM" 
   AFTER DELETE
   ON a_co_note
   REFERENCING OLD AS o
   FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE
   string1   VARCHAR2 (50) DEFAULT '119: ';
   string2   VARCHAR2 (50) DEFAULT NULL;
   temp      VARCHAR2 (50) DEFAULT NULL;
   colon     VARCHAR2 (5)  DEFAULT ' : ';
BEGIN
   SELECT note_type
     INTO temp
     FROM a_co_note_type
    WHERE note_type_id = :o.note_type_id;

   string2 :=
      CASE temp
         WHEN 'Delivery Instrns.'
            THEN '126 : '
         WHEN 'Cancellation'
            THEN '125 : '
         WHEN 'Hold Resolution'
            THEN '120 : '
         WHEN 'Gift From'
            THEN '121 : '
         WHEN 'Gift To'
            THEN '127 : '
         WHEN 'Gift Message'
            THEN '123 : '
         WHEN 'Collateral'
            THEN '128 : '
         WHEN 'Hold'
            THEN '124 : '
         WHEN 'Gift Wrap'
            THEN '122 : '
      END;

   INSERT INTO purchase_orders_event
               (purchase_orders_event_id, purchase_orders_id,
                field_name, old_value, new_value,
                created_source_type, created_source, created_dttm,
                line_item_id
               )
        VALUES (purchase_orders_event_id_seq.NEXTVAL, :o.entity_id,
                (string1 || string2 || ' 131'
                ), :o.note_description, NULL,
                1, NULL, SYSDATE,
                :o.entity_line_id
               );
END;
/

CREATE OR REPLACE TRIGGER "ISF_FILER_IDENT_A_IUD_TRG" 
AFTER INSERT OR UPDATE OR DELETE ON ISF_FILER_IDENTIFICATION
REFERENCING OLD  AS O NEW AS N
FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE

v_LAST_UPDATED_SOURCE        ISF_HEADER.LAST_UPDATED_SOURCE%TYPE ;
v_LAST_UPDATED_SOURCE_TYPE   ISF_HEADER.LAST_UPDATED_SOURCE_TYPE%TYPE ;

BEGIN

        BEGIN


	IF  INSERTING OR UPDATING THEN

		SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
		INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
		FROM   ISF_HEADER
		WHERE  ISF_HEADER_ID = :N.ISF_HEADER_ID ;

	ELSE

		SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
		INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
		FROM   ISF_HEADER
		WHERE  ISF_HEADER_ID = :O.ISF_HEADER_ID ;

	END IF ;


        EXCEPTION
                WHEN NO_DATA_FOUND THEN
                        v_LAST_UPDATED_SOURCE        := NULL ;
                        v_LAST_UPDATED_SOURCE_TYPE   := NULL ;

        END ;


IF INSERTING THEN
                   ISF_AU_PROC (
                        :N.ISF_HEADER_ID                  ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        :N.ABI_FILER_CODE                 ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;


IF UPDATING THEN

                   ISF_AU_PROC (
                        :N.ISF_HEADER_ID                  ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        :O.ABI_FILER_CODE                 ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        :N.ABI_FILER_CODE                 ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;

IF DELETING THEN
                   ISF_AU_PROC (
                        :O.ISF_HEADER_ID                  ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        :O.ABI_FILER_CODE                 ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                 		  ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                		  ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE             ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;


END ;
/

CREATE OR REPLACE TRIGGER "ISF_COMMODITY_A_IUD_TRG" 
AFTER INSERT OR UPDATE OR DELETE ON ISF_COMMODITY
REFERENCING OLD  AS O NEW AS N
FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE
v_LAST_UPDATED_SOURCE        ISF_HEADER.LAST_UPDATED_SOURCE%TYPE ;
v_LAST_UPDATED_SOURCE_TYPE   ISF_HEADER.LAST_UPDATED_SOURCE_TYPE%TYPE ;
n_ISF_HEADER_ID              ISF_HEADER.ISF_HEADER_ID%TYPE ;
n_ISF_BOL_ID                 ISF_BOL.ISF_BOL_ID%TYPE ;
n_ISF_BOL_LINE_ID            ISF_BOL_LINE.ISF_BOL_LINE_ID%TYPE ;


BEGIN

        BEGIN


		IF  INSERTING OR UPDATING THEN

			SELECT  ISF_BOL_LINE_ID
			INTO    n_ISF_BOL_LINE_ID
			FROM    ISF_BOL_LINE_ATTR
			WHERE   ISF_BOL_LINE_ATTR_ID = :N.ISF_BOL_LINE_ATTR_ID  ;

		ELSE

			SELECT  ISF_BOL_LINE_ID
			INTO    n_ISF_BOL_LINE_ID
			FROM    ISF_BOL_LINE_ATTR
			WHERE   ISF_BOL_LINE_ATTR_ID = :O.ISF_BOL_LINE_ATTR_ID  ;


		END IF ;



                SELECT  ISF_BOL_ID
                INTO    n_ISF_BOL_ID
                FROM    ISF_BOL_LINE
                WHERE   ISF_BOL_LINE_ID = n_ISF_BOL_LINE_ID  ;

                SELECT  ISF_HEADER_ID
                INTO    n_ISF_HEADER_ID
                FROM    ISF_BOL
                WHERE   ISF_BOL_ID = n_ISF_BOL_ID;


                SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
                INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
                FROM   ISF_HEADER
                WHERE  ISF_HEADER_ID = n_ISF_HEADER_ID ;

        EXCEPTION
                WHEN NO_DATA_FOUND THEN
                        v_LAST_UPDATED_SOURCE        := NULL ;
                        v_LAST_UPDATED_SOURCE_TYPE   := NULL ;
                        n_ISF_HEADER_ID              := -1 ;
                        n_ISF_BOL_ID                 := NULL ;
                        n_ISF_BOL_LINE_ID            := NULL ;

        END ;


IF INSERTING THEN
                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        :N.HTS6_CODE                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        :N.COUNTRY_OF_ORIGIN                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;

IF UPDATING THEN

                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        :O.HTS6_CODE                      ,              --O_HTS6_CODE
                        :N.HTS6_CODE                      ,              --N_HTS6_CODE
                        :O.COUNTRY_OF_ORIGIN              ,              --O_COUNTRY_OF_ORIGIN
                        :N.COUNTRY_OF_ORIGIN              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;

IF DELETING THEN

                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        :O.HTS6_CODE                      ,              --O_HTS6_CODE
                        NULL                      	  ,              --N_HTS6_CODE
                        :O.COUNTRY_OF_ORIGIN              ,              --O_COUNTRY_OF_ORIGIN
                        NULL              		  ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;

END ;
/

CREATE OR REPLACE TRIGGER "ISF_BOND_HOLDER_A_IUD_TRG" 
AFTER INSERT OR UPDATE OR DELETE ON ISF_BOND_HOLDER
REFERENCING OLD  AS O NEW AS N
FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE

v_LAST_UPDATED_SOURCE        ISF_HEADER.LAST_UPDATED_SOURCE%TYPE ;
v_LAST_UPDATED_SOURCE_TYPE   ISF_HEADER.LAST_UPDATED_SOURCE_TYPE%TYPE ;

BEGIN

        BEGIN

	IF  INSERTING OR UPDATING THEN

		SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
		INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
		FROM   ISF_HEADER
		WHERE  ISF_HEADER_ID = :N.ISF_HEADER_ID ;

	ELSE

		SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
		INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
		FROM   ISF_HEADER
		WHERE  ISF_HEADER_ID = :O.ISF_HEADER_ID ;

	END IF ;

        EXCEPTION
                WHEN NO_DATA_FOUND THEN
                        v_LAST_UPDATED_SOURCE        := NULL ;
                        v_LAST_UPDATED_SOURCE_TYPE   := NULL ;

        END ;


IF INSERTING THEN
                   ISF_AU_PROC (
                        :N.ISF_HEADER_ID                  ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL		                  ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        :N.BOND_HOLDER                    ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;

IF UPDATING THEN

                   ISF_AU_PROC (
                        :N.ISF_HEADER_ID                  ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                 		  ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                 		  ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        :O.BOND_HOLDER                    ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        :N.BOND_HOLDER                    ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;

IF DELETING THEN
                   ISF_AU_PROC (
                        :O.ISF_HEADER_ID                  ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL		                  ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        :O.BOND_HOLDER                    ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                    	  ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;

END ;
/

CREATE OR REPLACE TRIGGER "ISF_BOL_LINE_ATTR_A_IUD_TRG" 
AFTER INSERT OR UPDATE OR DELETE ON ISF_BOL_LINE_ATTR
REFERENCING OLD  AS O NEW AS N
FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE
v_LAST_UPDATED_SOURCE        ISF_HEADER.LAST_UPDATED_SOURCE%TYPE ;
v_LAST_UPDATED_SOURCE_TYPE   ISF_HEADER.LAST_UPDATED_SOURCE_TYPE%TYPE ;
n_ISF_HEADER_ID              ISF_HEADER.ISF_HEADER_ID%TYPE ;
n_ISF_BOL_ID                 ISF_BOL.ISF_BOL_ID%TYPE ;

BEGIN

        BEGIN


		IF  INSERTING OR UPDATING THEN

			SELECT  ISF_BOL_ID
			INTO    n_ISF_BOL_ID
			FROM    ISF_BOL_LINE
			WHERE   ISF_BOL_LINE_ID = :N.ISF_BOL_LINE_ID  ;


		ELSE

			SELECT  ISF_BOL_ID
			INTO    n_ISF_BOL_ID
			FROM    ISF_BOL_LINE
                	WHERE   ISF_BOL_LINE_ID = :O.ISF_BOL_LINE_ID  ;



		END IF ;



         	SELECT  ISF_HEADER_ID
	 	INTO    n_ISF_HEADER_ID
	 	FROM    ISF_BOL
                WHERE   ISF_BOL_ID = n_ISF_BOL_ID  ;


                SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
                INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
                FROM   ISF_HEADER
                WHERE  ISF_HEADER_ID = n_ISF_HEADER_ID ;

        EXCEPTION
                WHEN NO_DATA_FOUND THEN
                        v_LAST_UPDATED_SOURCE        := NULL ;
                        v_LAST_UPDATED_SOURCE_TYPE   := NULL ;
                        n_ISF_HEADER_ID              := -1 ;
                        n_ISF_BOL_ID                 := NULL ;

        END ;


IF INSERTING THEN
                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        :N.MFG_ENTITY_IDENT_CODE_TYPE     ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        :N.MFG_ENTITY_IDENT_CODE          ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;

IF UPDATING THEN

                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        :O.MFG_ENTITY_IDENT_CODE_TYPE     ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        :N.MFG_ENTITY_IDENT_CODE_TYPE     ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        :O.MFG_ENTITY_IDENT_CODE          ,              --O_MFG_ENTITY_IDENT_CODE
                        :N.MFG_ENTITY_IDENT_CODE          ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;

IF DELETING THEN

                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        :O.MFG_ENTITY_IDENT_CODE_TYPE     ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL     			  ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        :O.MFG_ENTITY_IDENT_CODE          ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL          			  ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE             ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;


END ;
/

CREATE OR REPLACE TRIGGER "ISF_BOL_LINE_A_IUD_TRG" 
AFTER INSERT OR UPDATE OR DELETE ON ISF_BOL_LINE
REFERENCING OLD  AS O NEW AS N
FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE
v_LAST_UPDATED_SOURCE        ISF_HEADER.LAST_UPDATED_SOURCE%TYPE ;
v_LAST_UPDATED_SOURCE_TYPE   ISF_HEADER.LAST_UPDATED_SOURCE_TYPE%TYPE ;
n_ISF_HEADER_ID              ISF_HEADER.ISF_HEADER_ID%TYPE ;

BEGIN

        BEGIN


		IF  INSERTING OR UPDATING THEN

			SELECT  ISF_HEADER_ID
			INTO    n_ISF_HEADER_ID
			FROM    ISF_BOL
			WHERE   ISF_BOL_ID = :N.ISF_BOL_ID  ;

		ELSE

			SELECT  ISF_HEADER_ID
			INTO    n_ISF_HEADER_ID
			FROM    ISF_BOL
			WHERE   ISF_BOL_ID = :O.ISF_BOL_ID  ;

		END IF ;


                SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
                INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
                FROM   ISF_HEADER
                WHERE  ISF_HEADER_ID = n_ISF_HEADER_ID ;

        EXCEPTION
                WHEN NO_DATA_FOUND THEN
                        v_LAST_UPDATED_SOURCE        := NULL ;
                        v_LAST_UPDATED_SOURCE_TYPE   := NULL ;
                        n_ISF_HEADER_ID              := -1 ;

        END ;


IF INSERTING THEN
                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        :N.EQUIPMENT_INITIAL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        :N.EQUIPMENT_NUMBER                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;

IF UPDATING THEN

                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        :O.EQUIPMENT_INITIAL              ,              --O_EQUIPMENT_INITIAL
                        :N.EQUIPMENT_INITIAL              ,              --N_EQUIPMENT_INITIAL
                        :O.EQUIPMENT_NUMBER               ,              --O_EQUIPMENT_NUMBER
                        :N.EQUIPMENT_NUMBER               ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;

IF DELETING THEN

                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        NULL                              ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_ORG_ENTITY_IDENT_CODE
                        :O.EQUIPMENT_INITIAL              ,              --O_EQUIPMENT_INITIAL
                        NULL              		  ,              --N_EQUIPMENT_INITIAL
                        :O.EQUIPMENT_NUMBER               ,              --O_EQUIPMENT_NUMBER
                        NULL               		  ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;


END ;
/

CREATE OR REPLACE TRIGGER "ISF_BOL_ATTR_A_IUD_TRG" 
AFTER INSERT OR UPDATE OR DELETE ON ISF_BOL_ATTR
REFERENCING OLD  AS O NEW AS N
FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE
v_LAST_UPDATED_SOURCE        ISF_HEADER.LAST_UPDATED_SOURCE%TYPE ;
v_LAST_UPDATED_SOURCE_TYPE   ISF_HEADER.LAST_UPDATED_SOURCE_TYPE%TYPE ;
n_ISF_HEADER_ID              ISF_HEADER.ISF_HEADER_ID%TYPE ;

BEGIN

        BEGIN
		IF  INSERTING OR UPDATING THEN

			SELECT  ISF_HEADER_ID
			INTO    n_ISF_HEADER_ID
			FROM    ISF_BOL
			WHERE   ISF_BOL_ID = :N.ISF_BOL_ID  ;

		ELSE

			SELECT  ISF_HEADER_ID
			INTO    n_ISF_HEADER_ID
			FROM    ISF_BOL
			WHERE   ISF_BOL_ID = :O.ISF_BOL_ID  ;

		END IF ;

                SELECT LAST_UPDATED_SOURCE   , LAST_UPDATED_SOURCE_TYPE
                INTO   v_LAST_UPDATED_SOURCE , v_LAST_UPDATED_SOURCE_TYPE
                FROM   ISF_HEADER
                WHERE  ISF_HEADER_ID = n_ISF_HEADER_ID ;

        EXCEPTION
                WHEN NO_DATA_FOUND THEN
                        v_LAST_UPDATED_SOURCE        := NULL ;
                        v_LAST_UPDATED_SOURCE_TYPE   := NULL ;
                        n_ISF_HEADER_ID              := -1 ;

        END ;


IF INSERTING THEN
                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        NULL                              ,              --O_ORG_ENTITY_TYPE
                        :N.ORG_ENTITY_TYPE                ,              --N_ORG_ENTITY_TYPE
                        NULL                              ,              --O_ORG_ENTITY_IDENT_CODE
                        :N.ORG_ENTITY_IDENT_CODE          ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;

END IF;

IF UPDATING THEN

                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        :O.ORG_ENTITY_TYPE                ,              --O_ORG_ENTITY_TYPE
                        :N.ORG_ENTITY_TYPE                ,              --N_ORG_ENTITY_TYPE
                        :O.ORG_ENTITY_IDENT_CODE           ,              --O_ORG_ENTITY_IDENT_CODE
                        :N.ORG_ENTITY_IDENT_CODE           ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;

IF DELETING THEN

                   ISF_AU_PROC (
                        n_ISF_HEADER_ID                   ,              --ISF_ID
                        NULL                              ,              --O_STATUS
                        NULL                              ,              --N_STATUS
                        NULL                              ,              --O_AMENDMENT_REASON_CODE
                        NULL                              ,             --N_AMENDMENT_REASON_CODE
                        NULL                              ,              --O_PORT_CODE
                        NULL                              ,              --N_PORT_CODE
                        NULL                              ,              --O_ISF_IMPORTER
                        NULL                              ,              --N_ISF_IMPORTER
                        NULL                              ,              --O_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --N_ABI_FILER_CODE     ISF_FILER_IDENTIFICATION.ABI_FILER_CODE
                        NULL                              ,              --O_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --N_BOND_HOLDER        ISF_BOND_HOLDER.BOND_HOLDER
                        NULL                              ,              --O_VESSEL_CODE
                        NULL                              ,              --N_VESSEL_CODE
                        NULL                              ,              --O_VESSEL_NAME
                        NULL                              ,              --N_VESSEL_NAME
                        NULL                              ,              --O_VOYAGE
                        NULL                              ,              --N_VOYAGE
                        NULL                              ,              --O_BOL_NUMBER
                        NULL                              ,              --N_BOL_NUMBER
                        :O.ORG_ENTITY_TYPE                ,              --O_ORG_ENTITY_TYPE
                        NULL                		  ,              --N_ORG_ENTITY_TYPE
                        :O.ORG_ENTITY_IDENT_CODE          ,              --O_ORG_ENTITY_IDENT_CODE
                        NULL           			  ,              --N_ORG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_EQUIPMENT_INITIAL
                        NULL                              ,              --N_EQUIPMENT_INITIAL
                        NULL                              ,              --O_EQUIPMENT_NUMBER
                        NULL                              ,              --N_EQUIPMENT_NUMBER
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE_TYPE
                        NULL                              ,              --O_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --N_MFG_ENTITY_IDENT_CODE
                        NULL                              ,              --O_HTS6_CODE
                        NULL                              ,              --N_HTS6_CODE
                        NULL                              ,              --O_COUNTRY_OF_ORIGIN
                        NULL                              ,              --N_COUNTRY_OF_ORIGIN
                        v_LAST_UPDATED_SOURCE            ,              --N_LAST_UPDATED_SOURCE
                        v_LAST_UPDATED_SOURCE_TYPE                      --N_LAST_UPDATED_SOURCE_TYPE
                ) ;


END IF;


END ;
/

CREATE OR REPLACE TRIGGER "ORDERMVMNT_A_D_TR1" 
   AFTER DELETE
   ON ORDER_MOVEMENT
   REFERENCING OLD AS OLD
   FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
DECLARE
   L_TC_ORDER_ID               ORDERS.TC_ORDER_ID%TYPE;
   L_LAST_UPDATE_SOURCE_TYPE   ORDERS.LAST_UPDATED_SOURCE_TYPE%TYPE;
   L_LAST_UPDATE_SOURCE        ORDERS.LAST_UPDATED_SOURCE%TYPE;
BEGIN
   SELECT TC_ORDER_ID, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_SOURCE
     INTO L_TC_ORDER_ID, L_LAST_UPDATE_SOURCE_TYPE, L_LAST_UPDATE_SOURCE
     FROM ORDERS
    WHERE ORDER_ID = :OLD.ORDER_ID;

   INSERT INTO SHIPMENT_EVENT
               (SHIPMENT_ID, SHIPMENT_EVENT_ID,
                FIELD_NAME, OLD_VALUE, NEW_VALUE,
                CREATED_SOURCE_TYPE, CREATED_SOURCE, CREATED_DTTM
               )
        VALUES (:OLD.SHIPMENT_ID, SHIPMENT_EVENT_ID_SEQ.NEXTVAL,
                'ORDER REMOVED', L_TC_ORDER_ID, NULL,
                L_LAST_UPDATE_SOURCE_TYPE, L_LAST_UPDATE_SOURCE, SYSDATE
               );
END ORDERMVMNT_A_D_TR1;
/


CREATE OR REPLACE TRIGGER "CWS_ORDER_MOV_B_ID_TR_1" 
BEFORE INSERT OR DELETE
ON CWS_ORDER_MOVEMENT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (NVL(SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'),'DUMMY') != 'FROM_ARCHIVE_PKG')
BEGIN
  IF INSERTING THEN
    UPDATE CWS_ORDER set LAST_WS_UPDATED_DTTM = SYSDATE where CWS_ORDER.ORDER_ID = :NEW.ORDER_ID;
  ELSE
    UPDATE CWS_ORDER set LAST_WS_UPDATED_DTTM = SYSDATE where CWS_ORDER.ORDER_ID = :OLD.ORDER_ID;
  END IF;
END;
/

--CREATE INDEX ASN_DETAIL_OLI_IDX ON ASN_DETAIL (ORDER_LINE_ITEM_ID);

set define on;  
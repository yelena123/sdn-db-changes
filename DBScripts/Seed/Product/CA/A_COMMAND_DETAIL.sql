set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_COMMAND_DETAIL

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1028,'Are there payment authorization scheduled jobs configured?','SQL','select count(*) as numSchedulers from om_sched_event se where se.event_objects like ''%AuthorizationPaymentScheduler%'' and se.event_objects like ''%tcCompanyId=#{tc_company_id}%'' and se.is_executed=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1029,'Is validation of payment settlement required?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''REQUIRES_VALIDATION_FOR_PAYMENT_SETTLEMENT'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1030,'Are there payment Re-authorization scheduled jobs configured?','SQL','select count(*) as numSchedulers from om_sched_event se where se.event_objects like ''%ReAuthorizationPaymentScheduler%'' and se.event_objects like ''%tcCompanyId=#{tc_company_id}%'' and se.is_executed=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1031,'Are there payment settlement scheduled jobs configured?','SQL','select count(*) as numSchedulers from om_sched_event se where se.event_objects like ''%SettlementPaymentScheduler%'' and se.event_objects like ''%tcCompanyId=#{tc_company_id}%'' and se.is_executed=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1032,'Are there payment refund scheduled jobs configured?','SQL','select count(*) as numSchedulers from om_sched_event se where se.event_objects like ''%RefundPaymentScheduler%'' and se.event_objects like ''%tcCompanyId=#{tc_company_id}%'' and se.is_executed=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1033,'Are there payment transaction scheduled jobs configured?','SQL','select count(*) as numSchedulers from om_sched_event se where se.event_objects like ''%ForcePaymentTransactionStatusUpdateScheduler%'' and se.event_objects like ''%tcCompanyId=#{tc_company_id}%'' and se.is_executed=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1034,'Is validation of payment refund required for facilitating return orders processed by RLM?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''REQUIRES_VALIDATION_FOR_PAYMENT_REFUND'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1036,'Is Maintains inventory selected in your facility under consideration?','SQL','select count(*) as numOfFacWithMaintainsInvSel from facility f
where f.tc_company_id = ? and f.maintains_perpetual_invty = 1 and f.mark_for_deletion = 0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1037,'Are there any DO create templates created with parameter ''Create DO?s for ASN/PO'' selected?','SQL','select count(*) as noOfDOCTAgnstInTanstInv from a_process_template where tc_company_id = ? and dom_process_type = 20 and is_deleted = 0 and a_identity in (
select dom_process_template_id from a_process_template_param where param_name = ''DO Creation Against In Transit Inventory'' and param_value = ''1'' )
','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1001,'Is Default order type configured in the system?','SQL','select ot.order_type from order_type ot where ot.order_type_id=(select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''DEFAULT_ORDER_TYPE'' and cp.mark_for_deletion=0)','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1002,'Is Default currency configured in the system?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''CURRENCY_USED'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1003,'Check if Manhattan Integration Server settings is configured?','JAVA','com.manh.olm.tools.cfganalyzer.service.impl.IFServerConfigAnalyzer','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1004,'Is Default ship via configured in the system?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''DEFAULT_SHIP_VIA'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1005,'Is Default shipping method for ship to store delivery option configured in the system?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1006,'Are there custom workflows configured in the system?','SQL','select count(*) as numCustomWorkFlows from wf_business_process bp where bp.tc_company_id=? and bp.is_default=0','Info','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1007,'Is the external customer ID generation strategy configured in the system?','SQL','select decode(cp.param_value,0,''Auto Generation'',1,''Email'',2,''Phone No.'') as param_value
from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''EXT_CUSTOMER_ID_GENERATION'' and     cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1008,'Are router entries available for customer Master?','SQL','select count(*) as numRouterEntriesCustMas from router where msg_type = ''Customer Master'' and deleted = 0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1010,'Are there currently any active item pricing rules which were extended by DOM system previously?','SQL','select count(*) as numPricingRulesExtByDOM from a_item_price ip where ip.company_id=? and sysdate between ip.effective_start_dttm and ip.effective_end_dttm and ip.a_rank<0','Info','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1011,'Are there expired item pricing rules existing in the system?','SQL','select count(*) as numPricingRulesExpired from (select ip.item_id,ip.a_rank from a_item_price ip where ip.company_id=? and ip.effective_end_dttm < sysdate group by ip.item_id,ip.a_rank having ip.a_rank=(select min(ip2.a_rank) from a_item_price ip2 where ip2.item_id=ip.item_id))','Warning','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1012,'Is the No. of days of extension for item price expiry date defined in the system?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''ITEM_PRICE_EXPIRY_DATE_EXTENSION_DAYS'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1013,'Is the No. of days for the next schedule of item price scheduler defined in the system?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''ITEM_PRICE_SCHEDULER_INTERVAL_IN_DAYS'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1014,'Are there item price scheduled jobs configured?','SQL','select count(*) as numSchedulers from om_sched_event se where se.event_objects like ''%ItemPriceExpiryScheduler%'' and se.event_objects like ''%tcCompanyId=#{tc_company_id}%'' and se.is_executed=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1015,'Is Use store prices for item pricing configured in the system?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''VIEW_STORE_LEVEL_PRICES'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1016,'Are there store level prices defined in the system?','SQL','select count(*) as numOfActiveStorePrices  from a_item_price ip where ip.company_id=? and sysdate between ip.effective_start_dttm and ip.effective_end_dttm and ip.facility_alias_id is not null','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1017,'What is the automatic promotion value configured in the system?','SQL','select  decode(cp.param_value,0,''Lowes'',9,''Highest'') as param_value from company_parameter cp where
cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''AUTOMATIC_PROMOTION_VALUE'' and cp.mark_for_deletion=0','Warning','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1018,'Are there any automatic promotion workflow rules existing in the system?','SQL','select count(*) as numAutoPromoRules from wf_rule wr where wr.tc_company_id=? and wr.service_name=''Automatic Promotions'' and wr.business_process_id in (select bp.business_process_id from wf_business_process bp where bp.tc_company_id=wr.tc_company_id and bp.marked_for_deletion=0)','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1019,'Have you created a custom workflow for external system integration?','SQL','select count(*) as numCustomWorkFlows from wf_business_process bp where bp.tc_company_id=? and bp.is_default=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1021,'Are there any S&H rules created which are not associated with corresponding S&H matrices?','SQL','select count(*) as numSnHRulesNoMatrix from a_shipping_charge_rule sr where sr.tc_company_id=? and sr.a_identity not in (select distinct sr.a_identity from a_shipping_charge_rule sr1, a_shipping_charge_matrix sm where sr1.tc_company_id=sr.tc_company_id and sr1.rate_basis=sm.rate_basis and sr1.ship_via=sm.ship_via)','Warning','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1022,'Is delivery zone being used while calculating S&H charges?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''USE_DELIVERY_ZONE'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1023,'Are there shipping matrix records existing with zone ID populated?','SQL','select count(*) numNullZoneIDs from a_shipping_charge_matrix sm where sm.tc_company_id=? and sm.zone_name is null','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1024,'Is the simulator configured for Address Verification?','SQL','select use_simulator as use_simulator from a_gateway_info where gateway_name = ''Others''','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1026,'Is the simulator configured for Tax Calculation?','SQL','select use_simulator as use_simulator from a_gateway_info where gateway_name = ''TAX''','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

INSERT INTO A_COMMAND_DETAIL ( COMMAND_ID,COMMAND_NAME,COMMAND_TYPE,COMMAND,COMMAND_SEVERITY,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM) 
VALUES  ( 1027,'Is validation of payment authorization required?','SQL','select cp.param_value from company_parameter cp where cp.tc_company_id=? and cp.param_group_id=''DS'' and cp.param_def_id=''REQUIRES_VALIDATION_FOR_PAYMENT_AUTHORIZATION'' and cp.mark_for_deletion=0','Error','arjuna',1,systimestamp,'arjuna',1,
systimestamp);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CLAIM_ROLE_TYPE

INSERT INTO CLAIM_ROLE_TYPE ( CLAIM_ROLE_TYPE,DESCRIPTION) 
VALUES  ( 2,'Claims Manager');

INSERT INTO CLAIM_ROLE_TYPE ( CLAIM_ROLE_TYPE,DESCRIPTION) 
VALUES  ( 1,'Claims Analyst');

INSERT INTO CLAIM_ROLE_TYPE ( CLAIM_ROLE_TYPE,DESCRIPTION) 
VALUES  ( 4,'Supplier');

INSERT INTO CLAIM_ROLE_TYPE ( CLAIM_ROLE_TYPE,DESCRIPTION) 
VALUES  ( 3,'Carrier');

Commit;





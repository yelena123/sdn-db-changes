set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CUSTOM_OPERATOR_TYPE

INSERT INTO CUSTOM_OPERATOR_TYPE ( CUSTOM_OPERATOR_TYPE_ID,DESCRIPTION) 
VALUES  ( 4,'Logical');

INSERT INTO CUSTOM_OPERATOR_TYPE ( CUSTOM_OPERATOR_TYPE_ID,DESCRIPTION) 
VALUES  ( 8,'Comparison');

Commit;





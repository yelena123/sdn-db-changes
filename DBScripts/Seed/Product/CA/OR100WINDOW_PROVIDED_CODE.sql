set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for OR100WINDOW_PROVIDED_CODE

INSERT INTO OR100WINDOW_PROVIDED_CODE ( WINDOW_PROVIDED_CODE,DESCRIPTION) 
VALUES  ( 0,'none');

INSERT INTO OR100WINDOW_PROVIDED_CODE ( WINDOW_PROVIDED_CODE,DESCRIPTION) 
VALUES  ( 1,'start');

INSERT INTO OR100WINDOW_PROVIDED_CODE ( WINDOW_PROVIDED_CODE,DESCRIPTION) 
VALUES  ( 2,'end');

INSERT INTO OR100WINDOW_PROVIDED_CODE ( WINDOW_PROVIDED_CODE,DESCRIPTION) 
VALUES  ( 3,'both');

Commit;





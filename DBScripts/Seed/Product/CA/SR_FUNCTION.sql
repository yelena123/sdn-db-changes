set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SR_FUNCTION

INSERT INTO SR_FUNCTION ( SR_FUNCTION_ID,DESCRIPTION,DB_FUNCTION) 
VALUES  ( 1,'Average','AVG');

INSERT INTO SR_FUNCTION ( SR_FUNCTION_ID,DESCRIPTION,DB_FUNCTION) 
VALUES  ( 2,'Count','COUNT');

INSERT INTO SR_FUNCTION ( SR_FUNCTION_ID,DESCRIPTION,DB_FUNCTION) 
VALUES  ( 3,'Max','MAX');

INSERT INTO SR_FUNCTION ( SR_FUNCTION_ID,DESCRIPTION,DB_FUNCTION) 
VALUES  ( 4,'Min','MIN');

INSERT INTO SR_FUNCTION ( SR_FUNCTION_ID,DESCRIPTION,DB_FUNCTION) 
VALUES  ( 5,'Sum','SUM');

Commit;





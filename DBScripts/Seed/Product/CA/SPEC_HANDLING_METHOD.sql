set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SPEC_HANDLING_METHOD

INSERT INTO SPEC_HANDLING_METHOD ( SPEC_HANDLING_METHOD,DESCRIPTION) 
VALUES  ( '20','Load Bars');

INSERT INTO SPEC_HANDLING_METHOD ( SPEC_HANDLING_METHOD,DESCRIPTION) 
VALUES  ( '10','Fragile');

INSERT INTO SPEC_HANDLING_METHOD ( SPEC_HANDLING_METHOD,DESCRIPTION) 
VALUES  ( '30','Roller Bed');

Commit;





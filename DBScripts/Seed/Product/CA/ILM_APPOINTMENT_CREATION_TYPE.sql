set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ILM_APPOINTMENT_CREATION_TYPE

INSERT INTO ILM_APPOINTMENT_CREATION_TYPE ( APPT_CREATION_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 10,'At Dock Door after Load',SYSTIMESTAMP,systimestamp);

INSERT INTO ILM_APPOINTMENT_CREATION_TYPE ( APPT_CREATION_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 20,'Before Check In',SYSTIMESTAMP,systimestamp);

INSERT INTO ILM_APPOINTMENT_CREATION_TYPE ( APPT_CREATION_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 30,'During Check In',SYSTIMESTAMP,systimestamp);

Commit;





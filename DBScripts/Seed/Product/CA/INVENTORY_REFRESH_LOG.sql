set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for INVENTORY_REFRESH_LOG

INSERT INTO INVENTORY_REFRESH_LOG ( LAST_RUN_ID,SOURCE_LAST_UPDATED_DTTM,LAST_RUN_DTTM,ERROR_DETAILS) 
VALUES  ( 1,'2014-08-27 14:37:49','2014-08-27 14:37:49',null);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_SO_CHANGE_MGMT

INSERT INTO A_SO_CHANGE_MGMT ( A_NON_CCL_UPDATE_FIELDS,DESCRIPTION) 
VALUES  ( 10,'Comments(H)');

INSERT INTO A_SO_CHANGE_MGMT ( A_NON_CCL_UPDATE_FIELDS,DESCRIPTION) 
VALUES  ( 20,'Comments(L)');

INSERT INTO A_SO_CHANGE_MGMT ( A_NON_CCL_UPDATE_FIELDS,DESCRIPTION) 
VALUES  ( 30,'Earliest Deliver By Date');

INSERT INTO A_SO_CHANGE_MGMT ( A_NON_CCL_UPDATE_FIELDS,DESCRIPTION) 
VALUES  ( 40,'Committed Ship Date');

INSERT INTO A_SO_CHANGE_MGMT ( A_NON_CCL_UPDATE_FIELDS,DESCRIPTION) 
VALUES  ( 50,'Cancelled Qty');

Commit;





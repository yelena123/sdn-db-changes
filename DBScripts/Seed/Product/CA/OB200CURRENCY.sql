set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for OB200CURRENCY

INSERT INTO OB200CURRENCY ( TYPE,COUNTRY_CODE,NAME,EXCHANGE_RATE,TC_COMPANY_ID) 
VALUES  ( 'USD','US',null,1,0);

Commit;





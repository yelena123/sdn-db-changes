set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for NEG_OBJECT

INSERT INTO NEG_OBJECT ( NEG_OBJECT_ID,NEG_OBJECT_TYPE,NEG_OBJ_NAME) 
VALUES  ( 1,'PO','Purchase Order');

INSERT INTO NEG_OBJECT ( NEG_OBJECT_ID,NEG_OBJECT_TYPE,NEG_OBJ_NAME) 
VALUES  ( 2,'PO_LINE','Purchase Order Line');

Commit;





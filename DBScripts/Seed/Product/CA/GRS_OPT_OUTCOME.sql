set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for GRS_OPT_OUTCOME

INSERT INTO GRS_OPT_OUTCOME ( GRS_OPT_OUTCOME,DESCRIPTION) 
VALUES  ( 4,'Rejected');

INSERT INTO GRS_OPT_OUTCOME ( GRS_OPT_OUTCOME,DESCRIPTION) 
VALUES  ( 12,'Optimized');

INSERT INTO GRS_OPT_OUTCOME ( GRS_OPT_OUTCOME,DESCRIPTION) 
VALUES  ( 16,'Optimized And Tendered');

INSERT INTO GRS_OPT_OUTCOME ( GRS_OPT_OUTCOME,DESCRIPTION) 
VALUES  ( 8,'Tendered');

Commit;





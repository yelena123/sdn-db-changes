set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for STORE_TYPE

INSERT INTO STORE_TYPE ( STORE_TYPE_CODE,STORE_TYPE_DESC) 
VALUES  ( 40,'Normal');

INSERT INTO STORE_TYPE ( STORE_TYPE_CODE,STORE_TYPE_DESC) 
VALUES  ( 10,'In Region');

INSERT INTO STORE_TYPE ( STORE_TYPE_CODE,STORE_TYPE_DESC) 
VALUES  ( 20,'High Rotation Volume');

INSERT INTO STORE_TYPE ( STORE_TYPE_CODE,STORE_TYPE_DESC) 
VALUES  ( 30,'Inaugral');

Commit;





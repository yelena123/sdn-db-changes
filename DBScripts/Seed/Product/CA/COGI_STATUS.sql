set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for COGI_STATUS

INSERT INTO COGI_STATUS ( COGI_STATUS,DESCRIPTION) 
VALUES  ( 10,'Open');

INSERT INTO COGI_STATUS ( COGI_STATUS,DESCRIPTION) 
VALUES  ( 20,'Partially Paid');

INSERT INTO COGI_STATUS ( COGI_STATUS,DESCRIPTION) 
VALUES  ( 30,'Paid');

INSERT INTO COGI_STATUS ( COGI_STATUS,DESCRIPTION) 
VALUES  ( 40,'Cancelled');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for PRIORITY_TYPE

INSERT INTO PRIORITY_TYPE ( PRIORITY_TYPE,DESCRIPTION) 
VALUES  ( 4,'Rush');

INSERT INTO PRIORITY_TYPE ( PRIORITY_TYPE,DESCRIPTION) 
VALUES  ( 8,'Normal');

Commit;





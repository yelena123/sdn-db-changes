set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for GRS_OPERATION

INSERT INTO GRS_OPERATION ( GRS_OPERATION,DESCRIPTION) 
VALUES  ( 4,'None');

INSERT INTO GRS_OPERATION ( GRS_OPERATION,DESCRIPTION) 
VALUES  ( 12,'Optimize');

INSERT INTO GRS_OPERATION ( GRS_OPERATION,DESCRIPTION) 
VALUES  ( 8,'Tender');

INSERT INTO GRS_OPERATION ( GRS_OPERATION,DESCRIPTION) 
VALUES  ( 16,'Optimize and Tender');

Commit;





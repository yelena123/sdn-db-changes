set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_SIZE_SCOPE

INSERT INTO CL_SIZE_SCOPE ( SIZE_SCOPE_ID,SIZE_SCOPE_NAME) 
VALUES  ( 1,'Data');

INSERT INTO CL_SIZE_SCOPE ( SIZE_SCOPE_ID,SIZE_SCOPE_NAME) 
VALUES  ( 2,'Packet');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RS300CLIENT

INSERT INTO RS300CLIENT ( CLIENT_ID,DESCRIPTION) 
VALUES  ( 'TP','TPCLIENT');

INSERT INTO RS300CLIENT ( CLIENT_ID,DESCRIPTION) 
VALUES  ( 'BASE','BASE Rating system client');

Commit;





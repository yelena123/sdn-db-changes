set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TRIPCCL_ACTION

INSERT INTO TRIPCCL_ACTION ( TRIPCCL_ACTION,DESCRIPTION) 
VALUES  ( 5,'Send Update');

INSERT INTO TRIPCCL_ACTION ( TRIPCCL_ACTION,DESCRIPTION) 
VALUES  ( 10,'Create Alert');

Commit;





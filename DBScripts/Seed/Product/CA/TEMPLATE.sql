set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TEMPLATE

INSERT INTO TEMPLATE ( TEMPLATE_ID,TEMPLATE_NAME,DESCRIPTION,TEMPLATE_TYPE,RANK) 
VALUES  ( 6,'Customer Pickups','Customer Pickup Template','fulfillment',1);

INSERT INTO TEMPLATE ( TEMPLATE_ID,TEMPLATE_NAME,DESCRIPTION,TEMPLATE_TYPE,RANK) 
VALUES  ( 7,'Customer Shipments','Customer Shipment Template','fulfillment',2);

INSERT INTO TEMPLATE ( TEMPLATE_ID,TEMPLATE_NAME,DESCRIPTION,TEMPLATE_TYPE,RANK) 
VALUES  ( 8,'Store Transfers','Transfer Template','fulfillment',3);

INSERT INTO TEMPLATE ( TEMPLATE_ID,TEMPLATE_NAME,DESCRIPTION,TEMPLATE_TYPE,RANK) 
VALUES  ( 11,'DEFAULT','Default Template','fulfillment',4);

Commit;





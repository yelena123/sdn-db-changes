set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RUN_STATUS

INSERT INTO RUN_STATUS ( RUN_STATUS,DESCRIPTION) 
VALUES  ( 1,'Running');

INSERT INTO RUN_STATUS ( RUN_STATUS,DESCRIPTION) 
VALUES  ( 5,'Failed');

INSERT INTO RUN_STATUS ( RUN_STATUS,DESCRIPTION) 
VALUES  ( 0,'Preprocessing');

INSERT INTO RUN_STATUS ( RUN_STATUS,DESCRIPTION) 
VALUES  ( 4,'Cancelled');

INSERT INTO RUN_STATUS ( RUN_STATUS,DESCRIPTION) 
VALUES  ( 3,'Complete');

INSERT INTO RUN_STATUS ( RUN_STATUS,DESCRIPTION) 
VALUES  ( 2,'Postprocessing');

Commit;





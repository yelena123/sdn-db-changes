set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for I_OUTAGE_TEMPLATE_MOCK

INSERT INTO I_OUTAGE_TEMPLATE_MOCK ( TEMPLATE_ID,TEMPLATE_NAME,REASON_CODE,FACILITY_ID_LIST,ITEM_ID_LIST,EFFECTIVE_DTTM,EXPIRY_DTTM,CURRENT_PROCESS_RUN_ID) 
VALUES  ( 1,'rule1','5','102','161,323','2014-08-27 14:49:07','2014-08-27 14:49:07',0);

INSERT INTO I_OUTAGE_TEMPLATE_MOCK ( TEMPLATE_ID,TEMPLATE_NAME,REASON_CODE,FACILITY_ID_LIST,ITEM_ID_LIST,EFFECTIVE_DTTM,EXPIRY_DTTM,CURRENT_PROCESS_RUN_ID) 
VALUES  ( 2,'rule2','5','101','323','2014-08-27 14:49:07','2014-08-27 14:49:07',0);

INSERT INTO I_OUTAGE_TEMPLATE_MOCK ( TEMPLATE_ID,TEMPLATE_NAME,REASON_CODE,FACILITY_ID_LIST,ITEM_ID_LIST,EFFECTIVE_DTTM,EXPIRY_DTTM,CURRENT_PROCESS_RUN_ID) 
VALUES  ( 3,'rule3','6','101','341','2014-08-27 14:49:07','2014-08-27 14:49:07',0);

INSERT INTO I_OUTAGE_TEMPLATE_MOCK ( TEMPLATE_ID,TEMPLATE_NAME,REASON_CODE,FACILITY_ID_LIST,ITEM_ID_LIST,EFFECTIVE_DTTM,EXPIRY_DTTM,CURRENT_PROCESS_RUN_ID) 
VALUES  ( 9,'Rule11','6','156','9107','2013-11-14 18:00:38','2013-11-14 18:00:38',0);

INSERT INTO I_OUTAGE_TEMPLATE_MOCK ( TEMPLATE_ID,TEMPLATE_NAME,REASON_CODE,FACILITY_ID_LIST,ITEM_ID_LIST,EFFECTIVE_DTTM,EXPIRY_DTTM,CURRENT_PROCESS_RUN_ID) 
VALUES  ( 10,'Rule12','5','157','9107,9108','2013-11-14 18:01:02','2013-11-14 18:01:02',0);

INSERT INTO I_OUTAGE_TEMPLATE_MOCK ( TEMPLATE_ID,TEMPLATE_NAME,REASON_CODE,FACILITY_ID_LIST,ITEM_ID_LIST,EFFECTIVE_DTTM,EXPIRY_DTTM,CURRENT_PROCESS_RUN_ID) 
VALUES  ( 101,'UpdateRule11','6',null,null,'2014-08-27 14:49:10','2014-08-27 14:49:10',0);

INSERT INTO I_OUTAGE_TEMPLATE_MOCK ( TEMPLATE_ID,TEMPLATE_NAME,REASON_CODE,FACILITY_ID_LIST,ITEM_ID_LIST,EFFECTIVE_DTTM,EXPIRY_DTTM,CURRENT_PROCESS_RUN_ID) 
VALUES  ( 102,'UpdateRule12','5',null,null,'2014-08-27 14:49:10','2014-08-27 14:49:10',0);

Commit;





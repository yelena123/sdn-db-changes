set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_LDC_LAST_RUN

INSERT INTO TCS_LDC_LAST_RUN ( LAST_RUN_ID,DBLINK,SOURCE_LAST_UPDATED_DTTM,LAST_RUN_DTTM,ERROR_DETAILS) 
VALUES  ( 0,'<p_db_link>','2014-08-27 14:38:26','2014-08-27 14:38:26',null);

Commit;





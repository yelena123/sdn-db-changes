set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RS300RATING_COMP_BEHAVIOR

INSERT INTO RS300RATING_COMP_BEHAVIOR ( VERSION_ID,RATING_COMPONENT_BEHAVIOR_ID,NAME,DESCRIPTION) 
VALUES  ( 'OB','O','OPTIBID','Rating Component Behavior');

INSERT INTO RS300RATING_COMP_BEHAVIOR ( VERSION_ID,RATING_COMPONENT_BEHAVIOR_ID,NAME,DESCRIPTION) 
VALUES  ( 'DL610PROD','D','Dummy','Dummy component behavior: not used');

Commit;





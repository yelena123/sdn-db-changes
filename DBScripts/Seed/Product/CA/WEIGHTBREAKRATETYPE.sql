set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for WEIGHTBREAKRATETYPE

INSERT INTO WEIGHTBREAKRATETYPE ( WEIGHTBREAKRATETYPE,DESCRIPTION) 
VALUES  ( 0,'Per Unit');

INSERT INTO WEIGHTBREAKRATETYPE ( WEIGHTBREAKRATETYPE,DESCRIPTION) 
VALUES  ( 1,'Per 100 Unit');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for AWARDTYPE

INSERT INTO AWARDTYPE ( AWARDTYPE,DESCRIPTION) 
VALUES  ( 0,'Volume');

INSERT INTO AWARDTYPE ( AWARDTYPE,DESCRIPTION) 
VALUES  ( 2,'Rank');

INSERT INTO AWARDTYPE ( AWARDTYPE,DESCRIPTION) 
VALUES  ( 1,'Percentage');

Commit;





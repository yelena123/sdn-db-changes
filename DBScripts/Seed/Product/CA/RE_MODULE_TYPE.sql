set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RE_MODULE_TYPE

INSERT INTO RE_MODULE_TYPE ( RE_MODULE_TYPE_ID,DESCRIPTION) 
VALUES  ( 4,'EVENT_MANAGEMENT');

Commit;





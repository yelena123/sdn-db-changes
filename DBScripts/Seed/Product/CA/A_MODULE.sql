set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_MODULE

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 307,'Address Verification System','Address Verification System is an external system which validates the address provided for the customer order line and returns a valid postal address.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 308,'Tax Configuration','Tax Configuration',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 309,'Payment Configuration','Payment Configuration',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 310,'Completed Customer Order payment','Completed Customer Order payment',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 311,'Store Item Price Questions','Pricing module enables creation, maintenance and interaction with the product pricing for the customer order.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 351,'Sourcing and Scheduling Configurations','Sourcing module determines best possible sources to fulfill any demand based on configured rules. Scheduling module finds the relevant lanes between fulfillment facility and end destination and determines time to deliver goods.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 352,'Allocation Configurations','Allocation module fulfills demand from optimum fulfillment facility based on configuration.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 353,'DO Create Configurations','DO Create module creates distribution order header and detail objects from allocation objects according to user defined rules and schedules.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 401,'Do you want to generate customer ID based on email-Id, Phone etc..?','Customer Id Generation strategy allows to choose certain fields such as Phone No., Email etc from the Customer Master to form the Unique Customer ID. This is to support consistent Customer ID generation logic across multiple systems creating Customer Master records.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 402,'Will you be getting the customer information from an external system?','Import the Customer Master Data into the application as an XML. The same schema supports both Create/Update flows.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 403,'Are you trying to automatically extend the expired item price rules?','Price extension scheduler auto extends item prices based on the predefined extension days company parameter. Price extension scheduler is also configurable by company parameter.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 404,'Do you want to calculate the price based on the store from where the order is placed?','Retailers can maintain the price of an item at two levels ? one for the web store and one for the retail store. If an order is placed in store, based on the store associates login, the system can automatically detect the store and apply the price based on the store.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 405,'Do you want to apply the promotions automatically when an order is created or modified?','Automatic Promotions will be applied during the Order creation/modification process. A custom workflow needs to be created  which has the automatic promotion service  in the system.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 406,'Do you want to use Buy-One Get-One promotions for your promotion system?','Promotion offers capability to apply Buy-One Get-One promotions. This can be configured at the company level when a retailer chooses to exercise BOGO promotions for customers.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 407,'Will you be using an external system for applying the coupons?','Coupons can be maintained in an external system and Distributed Selling can be integrated to call the system to validate/apply/remove coupons.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 408,'Are you using a customized xml format when you integrate with external coupon management system?','Distributed Selling provides a default XML format to integrate with the external coupon management system. The default format can be customized by changing the velocity template.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 409,'Do you want to use delivery zone for the calculation of Shipping and Handling charges?','Delivery zone can be used to calculate the Shipping & Handling charges more accurately based on the fulfillment center and the delivery address. ',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 410,'Are you trying to use simulator for address verification?','AVS simulator always validates the address provided in the customer order line to true. This can be used during the development/testing phase for processing customer orders.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 411,'Do you want to calculate tax as part of the order creation/modifications?','Tax for an order is calculated by interacting with the external tax system',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 412,'Are you trying to use simulator for calculation of taxes?','Tax simulator returns a default tax amount for an order. This can be used during the development/testing phase for processing customer orders.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 413,'Do you want to use real time authorization service for your payment system?','Real time authorization is performed during the order creation/modification process. Distributed Selling will call the payment gateway synchronously using a webservice to get the authorization and provide the result to the user. Typically real time authorization is used when the order is created/modified in the Distributed Selling user interface.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 414,'Do you want to use batch authorization service for your payment system?','Batch authorization is a background process which runs in pre-configured time intervals and collects all open authorization requests and calls the payment gateway for authorization. Typically batch authorization is used for confirmed orders which come from eCommerce website without authorizations.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 415,'Do you want to use the settlement service for payments created in the system?','Settlement is a background process which runs in pre-configured time intervals and collects all open settlement requests and calls the payment gateway for settlement.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 416,'Will you be accepting e-check as a payment option during the ordering process?','e-Check (Electronic Check) is a form of Payment method offered as part of the DS Customer ordering process. A form of payment made via the internet that is designed to perform the same function as a conventional paper check, only that it can be processed in fewer steps and has more features. Additional features include Authentication, encryption, support for multi-shipments etc.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 417,'Do you want to validate payment-refund to facilitate return processing of orders by RLM?','During import of Customer Orders that are processed and completed by external systems.DOM system supports the import of such orders that are is in "Shipped" (or) "Cancelled" status only.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 451,'Are you using Geo-proximity in your sourcing rule?','For sourcing orders through Geo proximity rule,  system can prioritize orders based on proximity of source to destination.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 452,'Are you trying to allocate against physical inventory instead of performing force allocation ?','For processing orders, allocation can be performed against physical inventory existing in the fulfillment facility as opposed to performing force allocation.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 453,'Are you trying to create DOs against inventory of type ASN/PO?','DOs will not be created for inventories of type ASN/PO unless configured in the DO Create template used for creating DO.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 100,'Distributed Selling','Distributed Selling enables users to enter new customer orders and modify existing ones. When combined with existing DOM capabilities, Distributed Selling services will support cross-channel enabling on a common platform. Distributed Selling will also ship with a series of user interfaces built for the capture and modification of customer orders in both call centers and store environments.',null,1,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 101,'Sales Order Management','Sales Order Management is built to collect orders from a variety of selling channels, match them against various points of supply both inside and outside the enterprise, and to orchestrate and manage the process of fulfilling those orders. Comprised primarily of sales order services which act on the order/order line throughout the fulfillment phase of its lifecycle, SOM uses a workflow modeler and engine to define and drive the lifecycle of a sales order.',null,1,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 102,'Purchase Order Flow Management','Purchase Order Flow Management enables you to redirect in-flight supply directly to customers, alternate stores or DCs based on real time demand signals',null,1,'/manh/tools/cfganalyzer/moduleImages/po_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 200,'Customer Master','Customer Master module creates and maintains customer records.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 201,'Pricing','Pricing module enables creation, maintenance and interaction with the product pricing for the customer order. ',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 202,'Promotions','Promotion module enables creation and maintenance of promotion rules. Based on configured promotion rules this module applies, evaluates and removes discount on customer order.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 203,'Shipping and Handling','Shipping and Handling subsystem supports calculation of the shipping charges, and any other user defined charges that may be applicable as handling charges for the order.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 204,'AVS','The AVS module is responsible for verifying customer addresses with the external system for a customer order.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 205,'Tax','The tax processing module is responsible for interacting with the external tax system to calculate taxes for a customer order.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 206,'Payment','The Payment module is responsible for creating the authorization and charge requests, interacting with the external payment systems to authorize and charge, changing the payment status appropriately, processing refund requests and executing refunds by interacting with external payment systems.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 210,'Completed Customer Order','It facilitates the import of Customer Orders that are processed and completed by external systems.DOM system supports the import of such orders that are is in "Shipped" (or) "Cancelled" status only.The purpose is to facilitate return processing of these orders by  RLM.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 251,'Sourcing and Scheduling','Sourcing module determines best possible sources to fulfill any demand based on configured rules. Scheduling module finds the relevant lanes between fulfillment facility and end destination and determines time to deliver goods',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 252,'Prioritization','Prioritization module prioritizes order lines based on configured rules. It evaluates order lines based on rules and assign them appropriate ranks.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 253,'Allocation','Allocation module fulfills demand from optimum fulfillment facility based on configuration.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 254,'DO Create','DO Create module creates distribution order header and detail objects from allocation objects according to user defined rules and schedules.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 255,'DO Release','DO Release module releases distribution order header and detail objects to other systems (in the form of XML documents) and accepts fulfillment status updates against those objects.',null,0,'/manh/tools/cfganalyzer/moduleImages/so_flow.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 300,'Customer Id Generation','Customer Id Generation strategy allows choosing certain fields such as Phone No., Email etc from the Customer Master to form the Unique Customer ID. This is to support consistent Customer ID generation logic across multiple systems creating Customer Master records.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 301,'Customer Information Import','Import customer information from an external system',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 302,'Price Expiration Questions','Pricing module enables creation, maintenance and interaction with the product pricing for the customer order.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 303,'Automatic Promotions','Promotion modules offers capability to automatically apply promotions. This can be configured at the company level using workflows designed to call promotions to auto-apply on order create/update. ',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 304,'BOGO Promotions','Promotion modules offers capability to apply Buy-One Get-One promotions. This can be configured at the company level when a retailer chooses to exercise BOGO promotions for customers.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 305,'External Coupons','Coupons can be maintained in an external system and Distributed Selling can be integrated to call the system to validate/apply/remove coupons.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

INSERT INTO A_MODULE ( MODULE_ID,MODULE_NAME,MODULE_DESCRIPTION,DISPLAY_TYPE,ROOT_MODULE,ICON_URL,IS_ACTIVE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,
LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 306,'Shipping and Handling Questions','Shipping and Handling subsystem supports calculation of the shipping charges, and any other user defined charges that may be applicable as handling charges for the order.',null,0,'/manh/tools/cfganalyzer/moduleImages/distributed_selling.png',1,'arjuna',1,systimestamp,
'arjuna',1,systimestamp);

Commit;





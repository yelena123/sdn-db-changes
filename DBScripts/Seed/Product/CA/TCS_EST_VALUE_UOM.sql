set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_EST_VALUE_UOM

INSERT INTO TCS_EST_VALUE_UOM ( VALUE_UOM_ID,VALUE_UOM_NAME) 
VALUES  ( 1,'CIF Value');

INSERT INTO TCS_EST_VALUE_UOM ( VALUE_UOM_ID,VALUE_UOM_NAME) 
VALUES  ( 2,'PO Value');

Commit;





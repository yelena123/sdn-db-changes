set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for MOVEMENT_OPTION

INSERT INTO MOVEMENT_OPTION ( MOVEMENT_OPTION,DESCRIPTION) 
VALUES  ( 4,'Unrestricted');

INSERT INTO MOVEMENT_OPTION ( MOVEMENT_OPTION,DESCRIPTION) 
VALUES  ( 8,'Waypoints Required');

INSERT INTO MOVEMENT_OPTION ( MOVEMENT_OPTION,DESCRIPTION) 
VALUES  ( 12,'No Waypoints');

Commit;





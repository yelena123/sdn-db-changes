set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_CHARGE_TYPE

INSERT INTO TCS_CHARGE_TYPE ( CHARGE_TYPE_ID,CHARGE_TYPE_NAME) 
VALUES  ( 1,'Percentage');

INSERT INTO TCS_CHARGE_TYPE ( CHARGE_TYPE_ID,CHARGE_TYPE_NAME) 
VALUES  ( 2,'Fixed');

INSERT INTO TCS_CHARGE_TYPE ( CHARGE_TYPE_ID,CHARGE_TYPE_NAME) 
VALUES  ( 3,'Per Value');

INSERT INTO TCS_CHARGE_TYPE ( CHARGE_TYPE_ID,CHARGE_TYPE_NAME) 
VALUES  ( 4,'Per Weight');

INSERT INTO TCS_CHARGE_TYPE ( CHARGE_TYPE_ID,CHARGE_TYPE_NAME) 
VALUES  ( 5,'Per Volume');

INSERT INTO TCS_CHARGE_TYPE ( CHARGE_TYPE_ID,CHARGE_TYPE_NAME) 
VALUES  ( 6,'Per Distance');

INSERT INTO TCS_CHARGE_TYPE ( CHARGE_TYPE_ID,CHARGE_TYPE_NAME) 
VALUES  ( 7,'Per Time Period');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LOADING_TYPE

INSERT INTO LOADING_TYPE ( LOADING_TYPE,DESCRIPTION) 
VALUES  ( 'PLT','Pallet');

INSERT INTO LOADING_TYPE ( LOADING_TYPE,DESCRIPTION) 
VALUES  ( 'SLP','Slip Sheet');

INSERT INTO LOADING_TYPE ( LOADING_TYPE,DESCRIPTION) 
VALUES  ( 'FLD','Floor Load');

Commit;





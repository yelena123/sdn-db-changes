#!/bin/ksh
SchemaName=$1
SchemaPswd=$2
dbName=$3
dirName=$4
logDir=$5

LoadDmpFile() {
    lvd_fname=$1
    lvd_fromuser=$2
    lvd_touser=$3
    lvd_topswd=$4
    lvd_orasid=$5
    ldirName=$6

    imp $lvd_touser/$lvd_topswd@${lvd_orasid} file=${ldirName}/${lvd_fname} \
            fromuser=$lvd_fromuser \
            touser=$lvd_touser \
            log=${logDir}/${lvd_touser}_${lvd_fname}.log \
            ignore=y \
            grants=n \
            constraints=n \
            silent=y 2>/dev/null
}

LoadDmpFile exp_CA_20150513104319IST.dmp EXT2015_CA ${SchemaName} ${SchemaPswd} ${dbName} ${dirName}


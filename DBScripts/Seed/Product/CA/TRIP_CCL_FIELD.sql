set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TRIP_CCL_FIELD

INSERT INTO TRIP_CCL_FIELD ( TRIP_CCL_FIELD_ID,DESCRIPTION) 
VALUES  ( 5,'Trip Cancellation');

INSERT INTO TRIP_CCL_FIELD ( TRIP_CCL_FIELD_ID,DESCRIPTION) 
VALUES  ( 10,'Estimated Time at a stop');

INSERT INTO TRIP_CCL_FIELD ( TRIP_CCL_FIELD_ID,DESCRIPTION) 
VALUES  ( 15,'Stop Sequence');

INSERT INTO TRIP_CCL_FIELD ( TRIP_CCL_FIELD_ID,DESCRIPTION) 
VALUES  ( 20,'Driver Resource Change');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for EM_OBJEVT_COMPTYPE

INSERT INTO EM_OBJEVT_COMPTYPE ( OBJEVT_COMPTYPE_ID,DESCRIPTION) 
VALUES  ( 4,'Shipper');

INSERT INTO EM_OBJEVT_COMPTYPE ( OBJEVT_COMPTYPE_ID,DESCRIPTION) 
VALUES  ( 8,'Vendor');

INSERT INTO EM_OBJEVT_COMPTYPE ( OBJEVT_COMPTYPE_ID,DESCRIPTION) 
VALUES  ( 12,'Carrier');

Commit;





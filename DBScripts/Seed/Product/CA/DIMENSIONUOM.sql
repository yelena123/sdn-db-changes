set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DIMENSIONUOM

INSERT INTO DIMENSIONUOM ( DIMENSIONUOM,DESCRIPTION) 
VALUES  ( 0,'cm');

INSERT INTO DIMENSIONUOM ( DIMENSIONUOM,DESCRIPTION) 
VALUES  ( 1,'in');

Commit;





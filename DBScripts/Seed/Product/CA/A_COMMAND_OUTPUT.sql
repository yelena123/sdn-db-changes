set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_COMMAND_OUTPUT

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 118,1018,'numAutoPromoRules','0','LONG','GT','We did not find any workflow created for the automatic promotion service. Please read the OLM configuration guide under AIM for setting up automatic promotion service.','There are  #{numAutoPromoRules} workflow rules having Automatic Promotions service configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 119,1019,'numCustomWorkFlows','0','LONG','GT','There are no custom workflows defined in the system for external system integration. To check all the workflows existing in the system, please click <a href="/doms/dom/workflow/admin/jsp/WorkflowAdmin.jsflps">here</a>.','There are  #{numCustomWorkFlows} custom workflow rules configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 121,1021,'numSnHRulesNoMatrix','0','LONG','Equal','There are #{numSnHRulesNoMatrix} shipping rules which are not associated to any corresponding shipping matrix. To view the shipping matrix records, please click <a href="/doms/dom/selling/snh/ShippingMatrixView.jsflps?fromLeftNav=true&clickedid=2257&stackId=Stack1" target="_blank">here</a>.','There are no S&H rules which are not associated to corresponding shipping matrix in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 122,1022,'param_value','true','STRING','Equal','We found that the parameter ''use delivery zone to calculate shipping and handling charges'' is not selected in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to set it.','Use delivery zone to calculate Shipping and Handling charges is configured to #{param_value}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 123,1023,'numNullZoneIDs','0','LONG','Equal','We did not find any  S&H matrix records with zone Id populated. To use delivery zone for calculation of S&H, import S&H matrix record with Zone information populated. It''s XSD can be found <a href="/doms/dom/interfaces/jsp/InterfacesView.jsflps?appName=doms&interfaceType=XSD&fromLeftNav=true&clickedid=2316&stackId=Stack0" target="_blank">here</a>.','There are #{numNullZoneIDs}S&H Matrix records without Zone Information.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 124,1024,'use_simulator','1','LONG','Equal','AVS Simulator is unavailable. Please update A_GATEWAY_INFO table for the gateway "Others".','AVS simulator is available.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 126,1026,'use_simulator','1','LONG','Equal','TAX Simulator is unavailable. Please update A_GATEWAY_INFO table for the gateway "TAX".','Tax simulator is available.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 127,1027,'param_value',null,'STRING','IsNotNull','We found that the parameter ''Validation for payment authorization service'' is not configured in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to set it.','Requires validation for payment authorization is configured to #{param_value}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 128,1028,'numSchedulers','0','LONG','LT','We did not find any scheduled jobs configured for the Payment Authorization. Please go to the System Monitor tab after clicking <a href="/doms/dom/dashboard/view/Dashboard.jsflps" target="_blank">here</a> to create a payment authorization scheduler entry.','There are #{numSchedulers}Payment Authorization schedulers currently configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 129,1029,'param_value',null,'STRING','IsNotNull','We found that the parameter ''Validation for payment settlement service'' is not configured in the Distributed Selling company parameter page. Please set it <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a>.','Requires validation for payment settlement is configured to #{param_value}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 130,1030,'numSchedulers','0','LONG','LT','We did not find any scheduled jobs configured for the Payment ReAuthorization scheduler. Please go to the System Monitor tab after clicking <a href="/doms/dom/dashboard/view/Dashboard.jsflps" target="_blank">here</a> to create a Payment ReAuthorization scheduler entry.','There are #{numSchedulers} Payment ReAuthorization schedulers currently configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 131,1031,'numSchedulers','0','LONG','LT','We did not find any scheduled jobs configured for the Payment Settlement scheduler. Please go to the System Monitor tab after clicking <a href="/doms/dom/dashboard/view/Dashboard.jsflps" target="_blank">here</a> to create a Payment Settlement scheduler entry.','There are #{numSchedulers} Payment Settlement schedulers currently configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 132,1032,'numSchedulers','0','LONG','LT','We did not find any scheduled jobs configured for the Payment Refund scheduler. Please go to the System Monitor tab after clicking <a href="/doms/dom/dashboard/view/Dashboard.jsflps" target="_blank">here</a> to create a Payment Refund scheduler entry.','There are #{numSchedulers} Payment Refund schedulers currently configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 133,1033,'numSchedulers','0','LONG','LT','We did not find any scheduled jobs configured for the Payment Transaction scheduler. Please go to the System Monitor tab after clicking <a href="/doms/dom/dashboard/view/Dashboard.jsflps" target="_blank">here</a> to create a Payment Transaction scheduler entry.','There are #{numSchedulers} Payment Transaction schedulers currently configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 134,1034,'param_value',null,'STRING','IsNotNull','We found that the parameter ''Validate Payment Refund'' is not selected in the Distributed Selling company parameter page. please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to configure it.','Requires validation for payment refund  is configured to #{param_value}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 136,1036,'numOfFacWithMaintainsInvSel','0','LONG','LT','We found that none of the facilities had maintains inventory selected. Please click <a href="/basedata/location/LocationList.jsflps?fromLeftNav=true&clickedid=2171&stackId=Stack0" target="_blank">here</a> to edit your facility.','Number of Facilities with maintains inventory checked are #{numOfFacWithMaintainsInvSel}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 137,1037,'noOfDOCTAgnstInTanstInv','0','LONG','LT','We found that none of the DO Create templates had the parameter ''Create DO''s for ASN/PO'' selected.','Number of DO Create template with the parameter selected are #{noOfDOCTAgnstInTanstInv}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 101,1001,'order_type',null,'STRING','IsNotNull','We did not find a default order type configured in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1">here</a> to set it.','Default order type #{order_type} is configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 102,1002,'param_value',null,'STRING','IsNotNull','We did not find a default currency configured in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1">here</a> to set it.','Default currency #{param_value} is configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 103,1003,'OUTPUT',null,'STRING','NotEmpty','Manhattan Integration Framework server is unavailable. Please check if the webmethods properties are set in runtime.properties. runtime.properties can be found at this <a href="/lcom/admin/util/jsp/jvmProperties.jsp" target="_blank">location</a>.','Manhattan Integration Framework Server is available and it returned a ping response as #{OUTPUT}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 104,1004,'param_value',null,'STRING','IsNotNull','We did not find a default ship via configured in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to set it.','Default ship via #{param_value} is configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 105,1005,'param_value',null,'STRING','IsNotNull','We did not find a default shipping method for ship to store delivery option configured in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to set it.','Default shipping method for ship to store delivery option #{param_value} is configured in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 106,1006,'numCustomWorkFlows','0','LONG','GT','There are no custom workflows defined in the system. To check all the workflows existing in the system, please click <a href="/doms/dom/workflow/admin/jsp/WorkflowAdmin.jsflps" target="_blank">here</a>.','There are #{numCustomWorkFlows} custom workflows defined in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 107,1007,'param_value',null,'STRING','IsNotNull','We found that external customer ID generation strategy was not configured in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to set it. Customer ID will be generated based on the option you have chosen in the company parameter.','External customer ID generation strategy configured in the system is #{param_value}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 108,1008,'numRouterEntriesCustMas','0','LONG','GT','We did not find any router entry configured in the system for customer master. Please click <a href="/ifse/router/RouterList.jsflps?fromLeftNav=true&clickedid=2431&stackId=Stack0" target="_blank">here</a> to configure a router entry.','#{numRouterEntriesCustMas} router entries were found for Customer Master. Please check the <a href="/ifse/router/RouterList.jsflps?fromLeftNav=true&clickedid=2431&stackId=Stack0" target="_blank">Router List</a> page.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 110,1010,'numPricingRulesExtByDOM','0','LONG','GE','There are #{numPricingRulesExtByDOM} pricing rules extended by DOM system. To check all the item pricing rules existing in the system, please click <a href="/doms/dom/selling/pricing/view/ItemPriceList.jsflps" target="_blank">here</a>.','#{numPricingRulesExtByDOM} pricing rules are extended in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 111,1011,'numPricingRulesExpired','0','LONG','Equal','There are #{numPricingRulesExpired} pricing rules expired in the system. To check all the item pricing rules existing in the system, please click <a href="/doms/dom/selling/pricing/view/ItemPriceList.jsflps" target="_blank">here</a>.','#{numPricingRulesExpired} pricing rules are expired in the system.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 112,1012,'param_value','0','LONG','LT','We found that the parameter ''extension days to the item price expiry date'' is not set in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to set it.',' #{param_value} days is configured as the ''Item price expiry date extension days''.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 113,1013,'param_value','0','LONG','LT','We found that the parameter ''no. of  days for the next schedule run of item price scheduler'' is not configured in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to set it.','Item price scheduler next schedule date interval in days #{param_value} is configured.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 114,1014,'numSchedulers','0','LONG','LT','We did not find any scheduled jobs configured for the item price scheduler. Please go to the System Monitor tab after clicking <a href="/doms/dom/dashboard/view/Dashboard.jsflps" target="_blank">here</a> to create a item price scheduler entry.','Number of Item price schedulers configured currently in the system are #{numSchedulers}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 115,1015,'param_value','true','STRING','Equal','We found that ''use store prices'' for item pricing is not configured in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to configure it.','Use store prices for item pricing was configured to #{param_value}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 116,1016,'numOfActiveStorePrices','0','LONG','GT','We did not find any item price record with store prices defined in the system. To use store prices, import an item price record with necessary information populated. It''s XSD can be found <a href="/doms/dom/interfaces/jsp/InterfacesView.jsflps?appName=doms&interfaceType=XSD&fromLeftNav=true&clickedid=2316&stackId=Stack0" target="_blank">here</a>.','There are #{numOfActiveStorePrices} pricing rules with store prices populated.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

INSERT INTO A_COMMAND_OUTPUT ( COMMAND_OUTPUT_ID,COMMAND_ID,VARIABLE_NAME,VARIABLE_VALUE,DATA_TYPE,OPERATOR,FAILURE_MESSAGE,SUCCESS_MESSAGE,CREATED_SOURCE,CREATED_SOURCE_TYPE,
CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES  ( 117,1017,'param_value',null,'STRING','IsNotNull','We found that the parameter ''Automatic promotion value'' is not configured in the Distributed Selling company parameter page, please click <a href="/doms/dom/selling/admin/parameter/jsp/ViewDSParameters.jsp?fromLeftNav=true&clickedid=2285&stackId=Stack1" target="_blank">here</a> to set it.','Automatic promotion value is configured to #{param_value}.','arjuna',1,
systimestamp,'arjuna',1,systimestamp);

Commit;





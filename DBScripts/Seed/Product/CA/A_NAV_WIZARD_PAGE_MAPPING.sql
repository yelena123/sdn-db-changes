set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_NAV_WIZARD_PAGE_MAPPING

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 1,100,0,1,1,'/doms/dom/selling/image/navigation/addblue.gif','/doms/dom/selling/image/navigation/addgreen.gif','/doms/dom/selling/image/navigation/addgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 2,100,0,2,2,'/doms/dom/selling/image/navigation/cartblue.gif','/doms/dom/selling/image/navigation/cartgreen.gif','/doms/dom/selling/image/navigation/cartgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 3,100,0,5,3,'/doms/dom/selling/image/navigation/promoblue.gif','/doms/dom/selling/image/navigation/promogreen.gif','/doms/dom/selling/image/navigation/promogray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 4,100,0,6,4,'/doms/dom/selling/image/navigation/packblue.gif','/doms/dom/selling/image/navigation/packgreen.gif','/doms/dom/selling/image/navigation/packgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 5,100,0,9,5,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 6,100,0,3,6,'/doms/dom/selling/image/navigation/doneblue.gif','/doms/dom/selling/image/navigation/donegreen.gif','/doms/dom/selling/image/navigation/donegray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 7,110,0,10,1,'/doms/dom/selling/image/navigation/cartblue.gif','/doms/dom/selling/image/navigation/cartgreen.gif','/doms/dom/selling/image/navigation/cartgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 8,110,0,11,2,'/doms/dom/selling/image/navigation/promoblue.gif','/doms/dom/selling/image/navigation/promogreen.gif','/doms/dom/selling/image/navigation/promogray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 9,110,0,12,3,'/doms/dom/selling/image/navigation/packblue.gif','/doms/dom/selling/image/navigation/packgreen.gif','/doms/dom/selling/image/navigation/packgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 10,110,0,13,4,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 11,120,0,1,1,'/doms/dom/selling/image/navigation/addblue.gif','/doms/dom/selling/image/navigation/addgreen.gif','/doms/dom/selling/image/navigation/addgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 12,120,0,19,2,'/doms/dom/selling/image/navigation/cartblue.gif','/doms/dom/selling/image/navigation/cartgreen.gif','/doms/dom/selling/image/navigation/cartgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 13,120,0,20,3,'/doms/dom/selling/image/navigation/promoblue.gif','/doms/dom/selling/image/navigation/promogreen.gif','/doms/dom/selling/image/navigation/promogray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 14,120,0,21,4,'/doms/dom/selling/image/navigation/packblue.gif','/doms/dom/selling/image/navigation/packgreen.gif','/doms/dom/selling/image/navigation/packgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 15,120,0,22,5,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 16,120,0,17,6,'/doms/dom/selling/image/navigation/doneblue.gif','/doms/dom/selling/image/navigation/donegreen.gif','/doms/dom/selling/image/navigation/donegray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 17,130,0,23,1,'/doms/dom/selling/image/navigation/cartblue.gif','/doms/dom/selling/image/navigation/cartgreen.gif','/doms/dom/selling/image/navigation/cartgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 18,140,0,19,1,'/doms/dom/selling/image/navigation/cartblue.gif','/doms/dom/selling/image/navigation/cartgreen.gif','/doms/dom/selling/image/navigation/cartgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 19,140,0,20,2,'/doms/dom/selling/image/navigation/promoblue.gif','/doms/dom/selling/image/navigation/promogreen.gif','/doms/dom/selling/image/navigation/promogray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 20,140,0,22,3,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 21,140,0,17,4,'/doms/dom/selling/image/navigation/doneblue.gif','/doms/dom/selling/image/navigation/donegreen.gif','/doms/dom/selling/image/navigation/donegray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 22,150,0,21,1,'/doms/dom/selling/image/navigation/packblue.gif','/doms/dom/selling/image/navigation/packgreen.gif','/doms/dom/selling/image/navigation/packgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 23,150,0,22,2,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 24,150,0,17,3,'/doms/dom/selling/image/navigation/doneblue.gif','/doms/dom/selling/image/navigation/donegreen.gif','/doms/dom/selling/image/navigation/donegray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 25,160,0,20,1,'/doms/dom/selling/image/navigation/promoblue.gif','/doms/dom/selling/image/navigation/promogreen.gif','/doms/dom/selling/image/navigation/promogray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 26,160,0,22,2,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 27,160,0,17,3,'/doms/dom/selling/image/navigation/doneblue.gif','/doms/dom/selling/image/navigation/donegreen.gif','/doms/dom/selling/image/navigation/donegray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 28,170,0,22,1,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 29,170,0,17,2,'/doms/dom/selling/image/navigation/doneblue.gif','/doms/dom/selling/image/navigation/donegreen.gif','/doms/dom/selling/image/navigation/donegray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 30,180,0,24,1,'/doms/dom/selling/image/navigation/cartblue.gif','/doms/dom/selling/image/navigation/cartgreen.gif','/doms/dom/selling/image/navigation/cartgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 31,190,0,50,1,'/doms/dom/selling/image/navigation/promoblue.gif','/doms/dom/selling/image/navigation/promogreen.gif','/doms/dom/selling/image/navigation/promogray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 34,130,0,20,2,'/doms/dom/selling/image/navigation/promoblue.gif','/doms/dom/selling/image/navigation/promogreen.gif','/doms/dom/selling/image/navigation/promogray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 35,130,0,22,3,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 36,130,0,17,4,'/doms/dom/selling/image/navigation/doneblue.gif','/doms/dom/selling/image/navigation/donegreen.gif','/doms/dom/selling/image/navigation/donegray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 53,200,0,28,1,'/doms/dom/selling/image/navigation/cartblue.gif','/doms/dom/selling/image/navigation/cartgreen.gif','/doms/dom/selling/image/navigation/cartgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 54,200,0,13,2,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 55,210,0,30,1,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 56,210,0,29,2,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 57,220,0,5,1,'/doms/dom/selling/image/navigation/promoblue.gif','/doms/dom/selling/image/navigation/promogreen.gif','/doms/dom/selling/image/navigation/promogray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 58,220,0,6,2,'/doms/dom/selling/image/navigation/packblue.gif','/doms/dom/selling/image/navigation/packgreen.gif','/doms/dom/selling/image/navigation/packgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 59,220,0,9,3,'/doms/dom/selling/image/navigation/payblue.gif','/doms/dom/selling/image/navigation/paygreen.gif','/doms/dom/selling/image/navigation/paygray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 60,220,0,3,4,'/doms/dom/selling/image/navigation/doneblue.gif','/doms/dom/selling/image/navigation/donegreen.gif','/doms/dom/selling/image/navigation/donegray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 73,230,0,48,1,'/doms/dom/selling/image/navigation/cartblue.gif','/doms/dom/selling/image/navigation/cartgreen.gif','/doms/dom/selling/image/navigation/cartgray.gif');

INSERT INTO A_NAV_WIZARD_PAGE_MAPPING ( A_NAV_WIZARD_PAGE_MAPPING_ID,DOMAIN_GROUP_ID,TC_COMPANY_ID,PAGE_ID,NAV_FLOW_SEQ,PAGE_PAST_ICON_URL,PAGE_ACTIVE_ICON_URL,PAGE_FUTURE_ICON_URL) 
VALUES  ( 77,240,0,49,1,'/doms/dom/selling/image/navigation/cartblue.gif','/doms/dom/selling/image/navigation/cartgreen.gif','/doms/dom/selling/image/navigation/cartgray.gif');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for BID_RESPONSE

INSERT INTO BID_RESPONSE ( BID_RESPONSE,DESCRIPTION) 
VALUES  ( 0,'No Response');

INSERT INTO BID_RESPONSE ( BID_RESPONSE,DESCRIPTION) 
VALUES  ( 20,'Decline');

INSERT INTO BID_RESPONSE ( BID_RESPONSE,DESCRIPTION) 
VALUES  ( 10,'Accept');

INSERT INTO BID_RESPONSE ( BID_RESPONSE,DESCRIPTION) 
VALUES  ( 30,'Price Pending');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for I_VIEW_METADATA

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 1,1,'com.manh.olm.availability.view.Availability1','I_AVAILABILITY_1','com.manh.olm.availability.view.Availability1ByInventory','I_AVAIL_BY_INV_1','com.manh.olm.availability.view.exclusion.AvailabilityExclusion1','I_AVAILABILITY_EXCLUSION_1','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus1','I_AVAIL_EVENT_STATUS_1',
'com.manh.olm.availability.view.activity.Availability1Activity','I_AVAILABILITY_ACTIVITY_1');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 2,2,'com.manh.olm.availability.view.Availability2','I_AVAILABILITY_2','com.manh.olm.availability.view.Availability2ByInventory','I_AVAIL_BY_INV_2','com.manh.olm.availability.view.exclusion.AvailabilityExclusion2','I_AVAILABILITY_EXCLUSION_2','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus2','I_AVAIL_EVENT_STATUS_2',
'com.manh.olm.availability.view.activity.Availability2Activity','I_AVAILABILITY_ACTIVITY_2');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 3,4,'com.manh.olm.availability.view.Availability3','I_AVAILABILITY_3','com.manh.olm.availability.view.Availability3ByInventory','I_AVAIL_BY_INV_3','com.manh.olm.availability.view.exclusion.AvailabilityExclusion3','I_AVAILABILITY_EXCLUSION_3','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus3','I_AVAIL_EVENT_STATUS_3',
'com.manh.olm.availability.view.activity.Availability3Activity','I_AVAILABILITY_ACTIVITY_3');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 4,8,'com.manh.olm.availability.view.Availability4','I_AVAILABILITY_4','com.manh.olm.availability.view.Availability4ByInventory','I_AVAIL_BY_INV_4','com.manh.olm.availability.view.exclusion.AvailabilityExclusion4','I_AVAILABILITY_EXCLUSION_4','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus4','I_AVAIL_EVENT_STATUS_4',
'com.manh.olm.availability.view.activity.Availability4Activity','I_AVAILABILITY_ACTIVITY_4');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 5,16,'com.manh.olm.availability.view.Availability5','I_AVAILABILITY_5','com.manh.olm.availability.view.Availability5ByInventory','I_AVAIL_BY_INV_5','com.manh.olm.availability.view.exclusion.AvailabilityExclusion5','I_AVAILABILITY_EXCLUSION_5','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus5','I_AVAIL_EVENT_STATUS_5',
'com.manh.olm.availability.view.activity.Availability5Activity','I_AVAILABILITY_ACTIVITY_5');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 6,32,'com.manh.olm.availability.view.Availability6','I_AVAILABILITY_6','com.manh.olm.availability.view.Availability6ByInventory','I_AVAIL_BY_INV_6','com.manh.olm.availability.view.exclusion.AvailabilityExclusion6','I_AVAILABILITY_EXCLUSION_6','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus6','I_AVAIL_EVENT_STATUS_6',
'com.manh.olm.availability.view.activity.Availability6Activity','I_AVAILABILITY_ACTIVITY_6');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 7,64,'com.manh.olm.availability.view.Availability7','I_AVAILABILITY_7','com.manh.olm.availability.view.Availability7ByInventory','I_AVAIL_BY_INV_7','com.manh.olm.availability.view.exclusion.AvailabilityExclusion7','I_AVAILABILITY_EXCLUSION_7','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus7','I_AVAIL_EVENT_STATUS_7',
'com.manh.olm.availability.view.activity.Availability7Activity','I_AVAILABILITY_ACTIVITY_7');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 8,128,'com.manh.olm.availability.view.Availability8','I_AVAILABILITY_8','com.manh.olm.availability.view.Availability8ByInventory','I_AVAIL_BY_INV_8','com.manh.olm.availability.view.exclusion.AvailabilityExclusion8','I_AVAILABILITY_EXCLUSION_8','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus8','I_AVAIL_EVENT_STATUS_8',
'com.manh.olm.availability.view.activity.Availability8Activity','I_AVAILABILITY_ACTIVITY_8');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 9,256,'com.manh.olm.availability.view.Availability9','I_AVAILABILITY_9','com.manh.olm.availability.view.Availability9ByInventory','I_AVAIL_BY_INV_9','com.manh.olm.availability.view.exclusion.AvailabilityExclusion9','I_AVAILABILITY_EXCLUSION_9','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus9','I_AVAIL_EVENT_STATUS_9',
'com.manh.olm.availability.view.activity.Availability9Activity','I_AVAILABILITY_ACTIVITY_9');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 10,512,'com.manh.olm.availability.view.Availability10','I_AVAILABILITY_10','com.manh.olm.availability.view.Availability10ByInventory','I_AVAIL_BY_INV_10','com.manh.olm.availability.view.exclusion.AvailabilityExclusion10','I_AVAILABILITY_EXCLUSION_10','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus10','I_AVAIL_EVENT_STATUS_10',
'com.manh.olm.availability.view.activity.Availability10Activity','I_AVAILABILITY_ACTIVITY_10');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 11,1024,'com.manh.olm.availability.view.Availability11','I_AVAILABILITY_11','com.manh.olm.availability.view.Availability11ByInventory','I_AVAIL_BY_INV_11','com.manh.olm.availability.view.exclusion.AvailabilityExclusion11','I_AVAILABILITY_EXCLUSION_11','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus11','I_AVAIL_EVENT_STATUS_11',
'com.manh.olm.availability.view.activity.Availability11Activity','I_AVAILABILITY_ACTIVITY_11');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 12,2048,'com.manh.olm.availability.view.Availability12','I_AVAILABILITY_12','com.manh.olm.availability.view.Availability12ByInventory','I_AVAIL_BY_INV_12','com.manh.olm.availability.view.exclusion.AvailabilityExclusion12','I_AVAILABILITY_EXCLUSION_12','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus12','I_AVAIL_EVENT_STATUS_12',
'com.manh.olm.availability.view.activity.Availability12Activity','I_AVAILABILITY_ACTIVITY_12');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 13,4096,'com.manh.olm.availability.view.Availability13','I_AVAILABILITY_13','com.manh.olm.availability.view.Availability13ByInventory','I_AVAIL_BY_INV_13','com.manh.olm.availability.view.exclusion.AvailabilityExclusion13','I_AVAILABILITY_EXCLUSION_13','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus13','I_AVAIL_EVENT_STATUS_13',
'com.manh.olm.availability.view.activity.Availability13Activity','I_AVAILABILITY_ACTIVITY_13');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 14,8192,'com.manh.olm.availability.view.Availability14','I_AVAILABILITY_14','com.manh.olm.availability.view.Availability14ByInventory','I_AVAIL_BY_INV_14','com.manh.olm.availability.view.exclusion.AvailabilityExclusion14','I_AVAILABILITY_EXCLUSION_14','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus14','I_AVAIL_EVENT_STATUS_14',
'com.manh.olm.availability.view.activity.Availability14Activity','I_AVAILABILITY_ACTIVITY_14');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 15,16384,'com.manh.olm.availability.view.Availability15','I_AVAILABILITY_15','com.manh.olm.availability.view.Availability15ByInventory','I_AVAIL_BY_INV_15','com.manh.olm.availability.view.exclusion.AvailabilityExclusion15','I_AVAILABILITY_EXCLUSION_15','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus15','I_AVAIL_EVENT_STATUS_15',
'com.manh.olm.availability.view.activity.Availability15Activity','I_AVAILABILITY_ACTIVITY_15');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 16,32768,'com.manh.olm.availability.view.Availability16','I_AVAILABILITY_16','com.manh.olm.availability.view.Availability16ByInventory','I_AVAIL_BY_INV_16','com.manh.olm.availability.view.exclusion.AvailabilityExclusion16','I_AVAILABILITY_EXCLUSION_16','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus16','I_AVAIL_EVENT_STATUS_16',
'com.manh.olm.availability.view.activity.Availability16Activity','I_AVAILABILITY_ACTIVITY_16');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 17,65536,'com.manh.olm.availability.view.Availability17','I_AVAILABILITY_17','com.manh.olm.availability.view.Availability17ByInventory','I_AVAIL_BY_INV_17','com.manh.olm.availability.view.exclusion.AvailabilityExclusion17','I_AVAILABILITY_EXCLUSION_17','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus17','I_AVAIL_EVENT_STATUS_17',
'com.manh.olm.availability.view.activity.Availability17Activity','I_AVAILABILITY_ACTIVITY_17');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 18,131072,'com.manh.olm.availability.view.Availability18','I_AVAILABILITY_18','com.manh.olm.availability.view.Availability18ByInventory','I_AVAIL_BY_INV_18','com.manh.olm.availability.view.exclusion.AvailabilityExclusion18','I_AVAILABILITY_EXCLUSION_18','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus18','I_AVAIL_EVENT_STATUS_18',
'com.manh.olm.availability.view.activity.Availability18Activity','I_AVAILABILITY_ACTIVITY_18');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 19,262144,'com.manh.olm.availability.view.Availability19','I_AVAILABILITY_19','com.manh.olm.availability.view.Availability19ByInventory','I_AVAIL_BY_INV_19','com.manh.olm.availability.view.exclusion.AvailabilityExclusion19','I_AVAILABILITY_EXCLUSION_19','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus19','I_AVAIL_EVENT_STATUS_19',
'com.manh.olm.availability.view.activity.Availability19Activity','I_AVAILABILITY_ACTIVITY_19');

INSERT INTO I_VIEW_METADATA ( VIEW_METADATA_ID,VIEW_IDENTIFIER,AVAIL_BEAN_ID,AVAIL_TABLE_NAME,AVAIL_BY_INV_BEAN_ID,AVAIL_BY_INV_TABLE_NAME,AVAIL_EXCLUSION_BEAN_ID,AVAIL_EXCLUSION_TABLE_NAME,AVAIL_EVENT_STATUS_BEAN_ID,AVAIL_EVENT_STATUS_TABLE_NAME,
AVAIL_ACTIVITY_BEAN_ID,AVAIL_ACTIVITY_TABLE_NAME) 
VALUES  ( 20,524288,'com.manh.olm.availability.view.Availability20','I_AVAILABILITY_20','com.manh.olm.availability.view.Availability20ByInventory','I_AVAIL_BY_INV_20','com.manh.olm.availability.view.exclusion.AvailabilityExclusion20','I_AVAILABILITY_EXCLUSION_20','com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus20','I_AVAIL_EVENT_STATUS_20',
'com.manh.olm.availability.view.activity.Availability20Activity','I_AVAILABILITY_ACTIVITY_20');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for WEIGHTUOM

INSERT INTO WEIGHTUOM ( WEIGHTUOM,DESCRIPTION) 
VALUES  ( 0,'lbs');

INSERT INTO WEIGHTUOM ( WEIGHTUOM,DESCRIPTION) 
VALUES  ( 1,'kgs');

Commit;





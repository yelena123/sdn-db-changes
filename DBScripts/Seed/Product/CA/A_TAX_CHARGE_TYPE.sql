set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_TAX_CHARGE_TYPE

INSERT INTO A_TAX_CHARGE_TYPE ( TAX_CHARGE_TYPE_ID,TAX_CHARGE_TYPE) 
VALUES  ( 10,'Shipping');

INSERT INTO A_TAX_CHARGE_TYPE ( TAX_CHARGE_TYPE_ID,TAX_CHARGE_TYPE) 
VALUES  ( 20,'Charge');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for VALUE_TYPE

INSERT INTO VALUE_TYPE ( VALUE_TYPE_ID,VALUE_TYPE_DESC) 
VALUES  ( 2,'PERYEAR');

INSERT INTO VALUE_TYPE ( VALUE_TYPE_ID,VALUE_TYPE_DESC) 
VALUES  ( 0,'VOLUME');

INSERT INTO VALUE_TYPE ( VALUE_TYPE_ID,VALUE_TYPE_DESC) 
VALUES  ( 1,'PERCENTAGE');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SO_TYPE

INSERT INTO SO_TYPE ( SO_TYPE,DESCRIPTION) 
VALUES  ( 20,'Manual');

INSERT INTO SO_TYPE ( SO_TYPE,DESCRIPTION) 
VALUES  ( 24,'Expedite');

INSERT INTO SO_TYPE ( SO_TYPE,DESCRIPTION) 
VALUES  ( 4,'Regular');

INSERT INTO SO_TYPE ( SO_TYPE,DESCRIPTION) 
VALUES  ( 8,'Express');

INSERT INTO SO_TYPE ( SO_TYPE,DESCRIPTION) 
VALUES  ( 12,'Distribution');

INSERT INTO SO_TYPE ( SO_TYPE,DESCRIPTION) 
VALUES  ( 32,'Future');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CHARGEBACK_METHOD_TYPE

INSERT INTO CHARGEBACK_METHOD_TYPE ( CHARGEBACK_METHOD_TYPE,DESCRIPTION) 
VALUES  ( 10,'Per incident');

INSERT INTO CHARGEBACK_METHOD_TYPE ( CHARGEBACK_METHOD_TYPE,DESCRIPTION) 
VALUES  ( 20,'Percent of PO value');

Commit;





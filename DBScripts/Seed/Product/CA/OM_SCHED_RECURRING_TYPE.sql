set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for OM_SCHED_RECURRING_TYPE

INSERT INTO OM_SCHED_RECURRING_TYPE ( OM_SCHED_RECURRING_TYPE,DESCRIPTION) 
VALUES  ( 'AP','APPOINTMENT');

Commit;





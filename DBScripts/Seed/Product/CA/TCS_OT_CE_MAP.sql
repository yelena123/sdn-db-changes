set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_OT_CE_MAP

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 2,1);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 2,11);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,5);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,10);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,6);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,7);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,8);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,9);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,11);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,5);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,10);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,6);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,7);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,8);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,9);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,11);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,5);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,10);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,6);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,7);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,8);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,9);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,11);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 6,1);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 7,12);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 7,13);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 7,14);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 7,15);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 7,16);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 7,17);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 7,18);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,5);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,10);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,6);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,7);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,8);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,9);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,12);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,13);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,14);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,15);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,16);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,17);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,18);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,1);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,5);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,10);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,6);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,7);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,8);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,9);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,12);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,13);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,14);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,15);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,16);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,17);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,18);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,11);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 2,2);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,2);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,11);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,2);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 10,4);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 2,19);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 2,20);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 2,21);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,19);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,20);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 3,21);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,19);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,20);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 1,21);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,19);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,20);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 8,21);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 6,19);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 6,20);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 6,21);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,19);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,20);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 9,21);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,19);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,20);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 4,21);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 10,19);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 10,20);

INSERT INTO TCS_OT_CE_MAP ( OBJECT_TYPE_ID,COST_EVENT_ID) 
VALUES  ( 10,21);

Commit;





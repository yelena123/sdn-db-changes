set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_PROMOTION_TYPE

INSERT INTO A_PROMOTION_TYPE ( PROMOTION_TYPE_ID,PROMOTION_TYPE_NAME,DESCRIPTION,COMMENTS) 
VALUES  ( 1,'Percent Discount','Percentage Discount','Percentage Discount');

INSERT INTO A_PROMOTION_TYPE ( PROMOTION_TYPE_ID,PROMOTION_TYPE_NAME,DESCRIPTION,COMMENTS) 
VALUES  ( 2,'Fixed Amount Discount','Fixed Amount Discount','Fixed Amount Discount');

INSERT INTO A_PROMOTION_TYPE ( PROMOTION_TYPE_ID,PROMOTION_TYPE_NAME,DESCRIPTION,COMMENTS) 
VALUES  ( 3,'Shipping Percent Discount','Shipping Percent Discount','Shipping Percent Discount');

INSERT INTO A_PROMOTION_TYPE ( PROMOTION_TYPE_ID,PROMOTION_TYPE_NAME,DESCRIPTION,COMMENTS) 
VALUES  ( 5,'BOGO Discount','Buy One and Get same One at Discount','Buy One Get Same one at Discount');

INSERT INTO A_PROMOTION_TYPE ( PROMOTION_TYPE_ID,PROMOTION_TYPE_NAME,DESCRIPTION,COMMENTS) 
VALUES  ( 6,'BXGY - Fixed Amount Discount','Buy one and Get another at Fixed Amount Discount','Buy one product and Get another at Fixed Amount Discount');

INSERT INTO A_PROMOTION_TYPE ( PROMOTION_TYPE_ID,PROMOTION_TYPE_NAME,DESCRIPTION,COMMENTS) 
VALUES  ( 7,'BXGY - Percent Discount','Buy one and Get another at Percent Discount','Buy one product and Get another product at Percent Discount');

INSERT INTO A_PROMOTION_TYPE ( PROMOTION_TYPE_ID,PROMOTION_TYPE_NAME,DESCRIPTION,COMMENTS) 
VALUES  ( 9,'Manufacturer Rebate','Manufacturer Rebate','Manufacturer Rebate');

Commit;





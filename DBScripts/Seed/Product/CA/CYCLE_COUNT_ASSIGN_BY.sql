set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CYCLE_COUNT_ASSIGN_BY

INSERT INTO CYCLE_COUNT_ASSIGN_BY ( ASSIGN_BY_TYPE,ASSIGN_BY_DESCRIPTION) 
VALUES  ( 'Manager','Assignment by manager');

INSERT INTO CYCLE_COUNT_ASSIGN_BY ( ASSIGN_BY_TYPE,ASSIGN_BY_DESCRIPTION) 
VALUES  ( 'Associate','Self assignment by associate');

Commit;





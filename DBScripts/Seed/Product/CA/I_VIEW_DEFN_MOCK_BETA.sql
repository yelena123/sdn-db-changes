set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for I_VIEW_DEFN_MOCK_BETA

INSERT INTO I_VIEW_DEFN_MOCK_BETA ( VIEW_ID,VIEW_IDENTIFIER,VIEW_NAME,VIEW_CLASSNAME,VIEW_TABLE_NAME,AVAIL_BY_INV_CLASS_NAME,AVAIL_EXCLUSION_CLASS_NAME,ALERT_VIEW_NAME,TACKING_LEVEL,TRACKING_BEAN_ID,
AVAIL_EVENT_STATUS_CLASS_NAME,IS_ACTIVE,AVAIL_ACTIVITY_CLASS_NAME) 
VALUES  ( 1,1,'STH','com.manh.olm.availability.view.Availability1','I_AVAILABILITY1','com.manh.olm.availability.view.Availability1ByInventory','com.manh.olm.availability.view.exclusion.AvailabilityExclusion1','Availability1','NETWORK','com.manh.olm.availability.tracking.NetworkTracking',
'com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus1',1,'com.manh.olm.availability.view.activity.Availability1Activity');

INSERT INTO I_VIEW_DEFN_MOCK_BETA ( VIEW_ID,VIEW_IDENTIFIER,VIEW_NAME,VIEW_CLASSNAME,VIEW_TABLE_NAME,AVAIL_BY_INV_CLASS_NAME,AVAIL_EXCLUSION_CLASS_NAME,ALERT_VIEW_NAME,TACKING_LEVEL,TRACKING_BEAN_ID,
AVAIL_EVENT_STATUS_CLASS_NAME,IS_ACTIVE,AVAIL_ACTIVITY_CLASS_NAME) 
VALUES  ( 2,2,'View2','com.manh.olm.availability.view.Availability2','I_AVAILABILITY2','com.manh.olm.availability.view.Availability2ByInventory','com.manh.olm.availability.view.exclusion.AvailabilityExclusion2','Availability2','NETWORK','com.manh.olm.availability.tracking.NetworkTracking',
'com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus2',1,'com.manh.olm.availability.view.activity.Availability2Activity');

INSERT INTO I_VIEW_DEFN_MOCK_BETA ( VIEW_ID,VIEW_IDENTIFIER,VIEW_NAME,VIEW_CLASSNAME,VIEW_TABLE_NAME,AVAIL_BY_INV_CLASS_NAME,AVAIL_EXCLUSION_CLASS_NAME,ALERT_VIEW_NAME,TACKING_LEVEL,TRACKING_BEAN_ID,
AVAIL_EVENT_STATUS_CLASS_NAME,IS_ACTIVE,AVAIL_ACTIVITY_CLASS_NAME) 
VALUES  ( 3,4,'View3','com.manh.olm.availability.view.Availability3','I_AVAILABILITY3','com.manh.olm.availability.view.Availability3ByInventory','com.manh.olm.availability.view.exclusion.AvailabilityExclusion3','Availability3','FACILITY','com.manh.olm.availability.tracking.FacilityTracking',
'com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus3',1,'com.manh.olm.availability.view.activity.Availability3Activity');

INSERT INTO I_VIEW_DEFN_MOCK_BETA ( VIEW_ID,VIEW_IDENTIFIER,VIEW_NAME,VIEW_CLASSNAME,VIEW_TABLE_NAME,AVAIL_BY_INV_CLASS_NAME,AVAIL_EXCLUSION_CLASS_NAME,ALERT_VIEW_NAME,TACKING_LEVEL,TRACKING_BEAN_ID,
AVAIL_EVENT_STATUS_CLASS_NAME,IS_ACTIVE,AVAIL_ACTIVITY_CLASS_NAME) 
VALUES  ( 4,8,'View4','com.manh.olm.availability.view.Availability4','I_AVAILABILITY4','com.manh.olm.availability.view.Availability4ByInventory','com.manh.olm.availability.view.exclusion.AvailabilityExclusion4','Availability4','FACILITY','com.manh.olm.availability.tracking.FacilityTracking',
'com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus4',1,'com.manh.olm.availability.view.activity.Availability4Activity');

INSERT INTO I_VIEW_DEFN_MOCK_BETA ( VIEW_ID,VIEW_IDENTIFIER,VIEW_NAME,VIEW_CLASSNAME,VIEW_TABLE_NAME,AVAIL_BY_INV_CLASS_NAME,AVAIL_EXCLUSION_CLASS_NAME,ALERT_VIEW_NAME,TACKING_LEVEL,TRACKING_BEAN_ID,
AVAIL_EVENT_STATUS_CLASS_NAME,IS_ACTIVE,AVAIL_ACTIVITY_CLASS_NAME) 
VALUES  ( 5,16,'View5','com.manh.olm.availability.view.Availability5','I_AVAILABILITY5','com.manh.olm.availability.view.Availability5ByInventory','com.manh.olm.availability.view.exclusion.AvailabilityExclusion5','Availability5','FACILITY','com.manh.olm.availability.tracking.FacilityTracking',
'com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus5',1,'com.manh.olm.availability.view.activity.Availability5Activity');

INSERT INTO I_VIEW_DEFN_MOCK_BETA ( VIEW_ID,VIEW_IDENTIFIER,VIEW_NAME,VIEW_CLASSNAME,VIEW_TABLE_NAME,AVAIL_BY_INV_CLASS_NAME,AVAIL_EXCLUSION_CLASS_NAME,ALERT_VIEW_NAME,TACKING_LEVEL,TRACKING_BEAN_ID,
AVAIL_EVENT_STATUS_CLASS_NAME,IS_ACTIVE,AVAIL_ACTIVITY_CLASS_NAME) 
VALUES  ( 6,32,'View6','com.manh.olm.availability.view.Availability6','I_AVAILABILITY6','com.manh.olm.availability.view.Availability6ByInventory','com.manh.olm.availability.view.exclusion.AvailabilityExclusion6','Availability6','NETWORK','com.manh.olm.availability.tracking.NetworkTracking',
'com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus6',1,'com.manh.olm.availability.view.activity.Availability6Activity');

INSERT INTO I_VIEW_DEFN_MOCK_BETA ( VIEW_ID,VIEW_IDENTIFIER,VIEW_NAME,VIEW_CLASSNAME,VIEW_TABLE_NAME,AVAIL_BY_INV_CLASS_NAME,AVAIL_EXCLUSION_CLASS_NAME,ALERT_VIEW_NAME,TACKING_LEVEL,TRACKING_BEAN_ID,
AVAIL_EVENT_STATUS_CLASS_NAME,IS_ACTIVE,AVAIL_ACTIVITY_CLASS_NAME) 
VALUES  ( 7,64,'DeleteView','com.manh.olm.availability.view.Availability7','I_AVAILABILITY7','com.manh.olm.availability.view.Availability7ByInventory','com.manh.olm.availability.view.exclusion.AvailabilityExclusion7','Availability7','FACILITY','com.manh.olm.availability.tracking.FacilityTracking',
'com.manh.olm.availability.view.eventstatus.AvailabilityEventStatus7',1,'com.manh.olm.availability.view.activity.Availability7Activity');

Commit;





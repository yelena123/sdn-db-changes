set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for GPS_TRAC_TYPE

INSERT INTO GPS_TRAC_TYPE ( GPS_TRAC_TYPE,DESCRIPTION) 
VALUES  ( -1,'Late');

INSERT INTO GPS_TRAC_TYPE ( GPS_TRAC_TYPE,DESCRIPTION) 
VALUES  ( 0,'On Time');

INSERT INTO GPS_TRAC_TYPE ( GPS_TRAC_TYPE,DESCRIPTION) 
VALUES  ( 1,'Early');

INSERT INTO GPS_TRAC_TYPE ( GPS_TRAC_TYPE,DESCRIPTION) 
VALUES  ( 2,'Fixed Transit Time');

Commit;





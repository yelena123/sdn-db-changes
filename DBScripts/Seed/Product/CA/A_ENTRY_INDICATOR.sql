set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_ENTRY_INDICATOR

INSERT INTO A_ENTRY_INDICATOR ( ENTRY_INDICATOR_ID,ENTRY_INDICATOR_NAME) 
VALUES  ( 10,'Scanned');

INSERT INTO A_ENTRY_INDICATOR ( ENTRY_INDICATOR_ID,ENTRY_INDICATOR_NAME) 
VALUES  ( 20,'Keyed');

Commit;





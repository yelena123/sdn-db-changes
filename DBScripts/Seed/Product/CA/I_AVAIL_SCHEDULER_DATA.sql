set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for I_AVAIL_SCHEDULER_DATA

INSERT INTO I_AVAIL_SCHEDULER_DATA ( SCHEDULER_DATA_ID,SCHEDULER_HANDLER_BEAN_ID,LAST_EXECUTED_TIME,INTERVAL,LAST_UPDATED_DTTM,IS_DELETED) 
VALUES  ( 1,'com.manh.olm.availability.scheduler.handler.InventoryEventSchedulerHandler','2014-08-27 14:49:37',3,systimestamp,0);

INSERT INTO I_AVAIL_SCHEDULER_DATA ( SCHEDULER_DATA_ID,SCHEDULER_HANDLER_BEAN_ID,LAST_EXECUTED_TIME,INTERVAL,LAST_UPDATED_DTTM,IS_DELETED) 
VALUES  ( 2,'com.manh.olm.availability.scheduler.handler.CommerceEventSchedulerHandler','2014-08-27 14:49:37',86400,systimestamp,0);

INSERT INTO I_AVAIL_SCHEDULER_DATA ( SCHEDULER_DATA_ID,SCHEDULER_HANDLER_BEAN_ID,LAST_EXECUTED_TIME,INTERVAL,LAST_UPDATED_DTTM,IS_DELETED) 
VALUES  ( 3,'com.manh.olm.availability.scheduler.handler.ETASchedulerHandler','2014-08-27 14:49:37',86400,systimestamp,0);

INSERT INTO I_AVAIL_SCHEDULER_DATA ( SCHEDULER_DATA_ID,SCHEDULER_HANDLER_BEAN_ID,LAST_EXECUTED_TIME,INTERVAL,LAST_UPDATED_DTTM,IS_DELETED) 
VALUES  ( 4,'com.manh.olm.availability.scheduler.handler.FulfillmentOutageEventSchedulerHandler','2014-08-27 14:49:37',3600,systimestamp,0);

Commit;





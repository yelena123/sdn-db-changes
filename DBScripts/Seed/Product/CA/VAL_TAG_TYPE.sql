set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for VAL_TAG_TYPE

INSERT INTO VAL_TAG_TYPE ( TYPE,UI_DESC) 
VALUES  ( '1','What Selected');

INSERT INTO VAL_TAG_TYPE ( TYPE,UI_DESC) 
VALUES  ( '2','Data Source');

INSERT INTO VAL_TAG_TYPE ( TYPE,UI_DESC) 
VALUES  ( '3','Driver');

INSERT INTO VAL_TAG_TYPE ( TYPE,UI_DESC) 
VALUES  ( '4','Modules Of Interest');

INSERT INTO VAL_TAG_TYPE ( TYPE,UI_DESC) 
VALUES  ( '5','Verticals');

INSERT INTO VAL_TAG_TYPE ( TYPE,UI_DESC) 
VALUES  ( '6','Description');

Commit;





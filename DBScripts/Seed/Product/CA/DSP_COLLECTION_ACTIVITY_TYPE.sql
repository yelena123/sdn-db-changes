set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DSP_COLLECTION_ACTIVITY_TYPE

INSERT INTO DSP_COLLECTION_ACTIVITY_TYPE ( DSP_COLLECTION_ACTIVITY_TYPE,DESCRIPTION) 
VALUES  ( 8,'Returns');

INSERT INTO DSP_COLLECTION_ACTIVITY_TYPE ( DSP_COLLECTION_ACTIVITY_TYPE,DESCRIPTION) 
VALUES  ( 4,'Collect');

INSERT INTO DSP_COLLECTION_ACTIVITY_TYPE ( DSP_COLLECTION_ACTIVITY_TYPE,DESCRIPTION) 
VALUES  ( 12,'Delivered');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TRIP_STATUS

INSERT INTO TRIP_STATUS ( TRIP_STATUS,DESCRIPTION) 
VALUES  ( 4,'Available');

INSERT INTO TRIP_STATUS ( TRIP_STATUS,DESCRIPTION) 
VALUES  ( 8,'In-Transit');

INSERT INTO TRIP_STATUS ( TRIP_STATUS,DESCRIPTION) 
VALUES  ( 12,'Completed');

INSERT INTO TRIP_STATUS ( TRIP_STATUS,DESCRIPTION) 
VALUES  ( 10,'Delivered');

INSERT INTO TRIP_STATUS ( TRIP_STATUS,DESCRIPTION) 
VALUES  ( 9,'Waiting');

Commit;





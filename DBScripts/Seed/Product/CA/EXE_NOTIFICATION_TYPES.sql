set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for EXE_NOTIFICATION_TYPES

INSERT INTO EXE_NOTIFICATION_TYPES ( TYPE_ID,TYPE_NAME,TYPE_HANDLER) 
VALUES  ( 1,'EMAIL','com.manh.bpe.csf.notification.handler.email.EmailNotificationHandler');

Commit;





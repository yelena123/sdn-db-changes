set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ALERT_SEVERITY

INSERT INTO ALERT_SEVERITY ( ALERT_SEVERITY,DESCRIPTION) 
VALUES  ( 3,'Low');

INSERT INTO ALERT_SEVERITY ( ALERT_SEVERITY,DESCRIPTION) 
VALUES  ( 1,'High');

INSERT INTO ALERT_SEVERITY ( ALERT_SEVERITY,DESCRIPTION) 
VALUES  ( 2,'Medium');

Commit;





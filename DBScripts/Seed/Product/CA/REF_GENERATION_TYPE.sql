set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for REF_GENERATION_TYPE

INSERT INTO REF_GENERATION_TYPE ( REF_GENERATION_TYPE,DESCRIPTION) 
VALUES  ( 4,'airway_bill');

INSERT INTO REF_GENERATION_TYPE ( REF_GENERATION_TYPE,DESCRIPTION) 
VALUES  ( 12,'lpn_number');

INSERT INTO REF_GENERATION_TYPE ( REF_GENERATION_TYPE,DESCRIPTION) 
VALUES  ( 16,'vcp_lpn_number');

Commit;





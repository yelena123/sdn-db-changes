set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DELIVERY_METHOD

INSERT INTO DELIVERY_METHOD ( DELIVERY_METHOD,DESCRIPTION) 
VALUES  ( 4,'Printer');

INSERT INTO DELIVERY_METHOD ( DELIVERY_METHOD,DESCRIPTION) 
VALUES  ( 8,'E-Mail');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SO_PRIORITY

INSERT INTO SO_PRIORITY ( SO_PRIORITY,DESCRIPTION) 
VALUES  ( 4,'TestOne');

INSERT INTO SO_PRIORITY ( SO_PRIORITY,DESCRIPTION) 
VALUES  ( 8,'TestThree');

INSERT INTO SO_PRIORITY ( SO_PRIORITY,DESCRIPTION) 
VALUES  ( 12,'TestThree');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_RETURN_TYPE

INSERT INTO A_RETURN_TYPE ( RETURN_TYPE_ID,RETURN_TYPE_VALUE) 
VALUES  ( 10,'Credit');

INSERT INTO A_RETURN_TYPE ( RETURN_TYPE_ID,RETURN_TYPE_VALUE) 
VALUES  ( 20,'Same style');

INSERT INTO A_RETURN_TYPE ( RETURN_TYPE_ID,RETURN_TYPE_VALUE) 
VALUES  ( 30,'New style');

Commit;





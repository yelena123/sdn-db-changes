set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_INVOICE_TYPE

INSERT INTO A_INVOICE_TYPE ( INVOICE_TYPE,DESCRIPTION) 
VALUES  ( 10,'Shipment');

INSERT INTO A_INVOICE_TYPE ( INVOICE_TYPE,DESCRIPTION) 
VALUES  ( 20,'Adjustment');

INSERT INTO A_INVOICE_TYPE ( INVOICE_TYPE,DESCRIPTION) 
VALUES  ( 30,'Return');

INSERT INTO A_INVOICE_TYPE ( INVOICE_TYPE,DESCRIPTION) 
VALUES  ( 40,'Return Credit');

Commit;





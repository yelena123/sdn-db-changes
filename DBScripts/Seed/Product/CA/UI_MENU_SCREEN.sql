set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for UI_MENU_SCREEN

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 19169,14501,0,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 19170,1,1800034,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 19171,2,1800034,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 19172,1,1800035,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 19173,2,1800035,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 19175,1,40000024,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 19176,2,40000024,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 19177,1,40000025,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 19178,2,40000025,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 17170,1,990030,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 17173,1,10000,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 12170,1,9999,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 13168,7,0,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 13169,8,0,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 13171,10,0,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14169,1,40000001,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14170,1,40000002,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14171,1,40000003,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14172,1,40000004,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14173,1,40000005,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14174,1,40000006,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14175,1,40000007,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14176,1,40000008,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14177,1,40000009,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14178,1,40000010,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14179,1,40000011,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14180,1,40000012,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14181,1,40000013,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14182,1,40000014,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 14183,1,40000016,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 3,1,1800032,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 4,2,1800032,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 5,1,1800033,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 6,2,1800033,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10138,2,30000030,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10101,1,3000003,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10102,1,3000002,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10103,1,3000013,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10104,2,3000013,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10105,1,3000014,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10106,2,3000014,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10107,1,3000015,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10108,1,3000016,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10109,1,3000017,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10110,1,800008,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10111,2,800008,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10112,1,3000001,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10113,1,800010,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10114,2,800010,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10115,1,800011,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10116,2,800011,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10122,1,9001,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10123,2,9002,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10124,1,9003,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10125,1,9004,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10126,1,5052,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10127,1,5053,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10128,1,5054,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10129,1,5055,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10130,1,5056,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10131,1,5057,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10132,1,1800028,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10133,1,30000019,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10134,2,30000019,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10137,1,30000030,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10139,1,30000013,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10140,2,30000013,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10141,1,30000031,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10142,2,30000031,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10143,1,30000014,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10144,2,30000014,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10145,1,30000032,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10146,2,30000032,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10147,1,30000015,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10148,2,30000015,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10149,1,30000016,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10150,2,30000016,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10151,1,30000017,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10152,2,30000017,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10153,1,30000018,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10154,2,30000018,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10155,1,30000033,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10156,2,30000033,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10158,1,30000034,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10159,2,30000034,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10160,1,2000011,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10161,1,800032,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10162,1,1800029,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10163,1,30000035,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10164,1,1900030,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10165,1,30000036,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10166,1,2200004,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10167,1,9005,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 10168,1,9006,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16169,1,9007,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16170,1,9008,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16171,1,9009,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16173,1,99003,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16174,2,99003,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16175,1,99010,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16176,1,99005,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16177,1,99004,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16178,1,99007,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16179,1,99006,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16180,2,99006,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16181,1,99008,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16182,1,99009,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16183,1,99026,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16184,1,99000,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16185,2,99000,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16186,1,99001,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16187,1,99002,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16188,1,99020,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16189,2,99020,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16190,1,99021,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16191,1,99022,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16192,1,99023,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16193,1,99025,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16195,1,99011,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16196,1,99012,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16198,1,9997,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16199,1,9998,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16201,1,9996,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16203,1,44000028,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16205,1,990024,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 16206,1,5058,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 17172,1,40000029,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 17175,1,440000100,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 17176,2,440000100,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 17177,1,440000101,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 17178,2,440000101,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 20169,2,440000102,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 20170,1,440000102,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 20171,1,40000031,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 20172,1,40000030,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 20173,1,40000034,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 20174,1,40000035,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 20175,1,990032,'SYSTEM',null,null);

INSERT INTO UI_MENU_SCREEN ( UI_MENU_SCREEN_ID,MENU_ID,SCREEN_ID,PERSONALIZATION_LEVEL,PERSONALIZATION_ID,USER_ID) 
VALUES  ( 15541,1,99024,'SYSTEM',null,null);

Commit;





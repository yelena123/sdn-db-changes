set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SR_OBJECT_TYPE

INSERT INTO SR_OBJECT_TYPE ( SR_OBJECT_TYPE_ID,DESCRIPTION) 
VALUES  ( 1,'Shipment');

INSERT INTO SR_OBJECT_TYPE ( SR_OBJECT_TYPE_ID,DESCRIPTION) 
VALUES  ( 2,'RUN_SUMMARY');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CM_LEG_STATUS

INSERT INTO CM_LEG_STATUS ( CM_LEG_STATUS,DESCRIPTION) 
VALUES  ( 0,'Inactive');

INSERT INTO CM_LEG_STATUS ( CM_LEG_STATUS,DESCRIPTION) 
VALUES  ( 1,'Active');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for MIGRATE_FILTER_OBJECT

INSERT INTO MIGRATE_FILTER_OBJECT ( FILTER_OBJECT_ID,DESCRIPTION) 
VALUES  ( 4,'COMPANY');

INSERT INTO MIGRATE_FILTER_OBJECT ( FILTER_OBJECT_ID,DESCRIPTION) 
VALUES  ( 16,'USER');

INSERT INTO MIGRATE_FILTER_OBJECT ( FILTER_OBJECT_ID,DESCRIPTION) 
VALUES  ( 12,'USER_GROUP');

INSERT INTO MIGRATE_FILTER_OBJECT ( FILTER_OBJECT_ID,DESCRIPTION) 
VALUES  ( 8,'LOCATION');

INSERT INTO MIGRATE_FILTER_OBJECT ( FILTER_OBJECT_ID,DESCRIPTION) 
VALUES  ( 20,'ROLE');

INSERT INTO MIGRATE_FILTER_OBJECT ( FILTER_OBJECT_ID,DESCRIPTION) 
VALUES  ( 24,'RELATIONSHIP');

Commit;





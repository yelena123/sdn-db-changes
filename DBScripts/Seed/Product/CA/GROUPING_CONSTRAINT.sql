set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for GROUPING_CONSTRAINT

INSERT INTO GROUPING_CONSTRAINT ( GROUPING_CONSTRAINT,DESCRIPTION) 
VALUES  ( 10,'Order');

INSERT INTO GROUPING_CONSTRAINT ( GROUPING_CONSTRAINT,DESCRIPTION) 
VALUES  ( 20,'Product Class');

Commit;





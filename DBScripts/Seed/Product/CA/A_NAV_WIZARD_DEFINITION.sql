set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_NAV_WIZARD_DEFINITION

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 100,'Create customer order','Create customer
order','createCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.CreateCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 110,'View Customer Order','View Customer Order','viewCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.ViewCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.helper.ViewCONavigationFlowHelper');

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 120,'Add Item Task Wizard','Wizard for adding Item to a Customer Order','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 130,'Cancel Item Task Wizard','Wizard for Cancel an Item of a Customer Order','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 140,'Update Qty Task Wizard','Wizard for Updating qty of a line of a Customer Order','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 150,'Update Shipping Info Task Wizard','Wizard for Updating Shipping Info of a Customer Order','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 160,'Update Promotions Task Wizard','Wizard for Updating Promotions on a Customer Order','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 170,'Update Payment Info Task Wizard','Wizard for Updating Payment Info on a Customer Order','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 180,'Update Customer Order Header','Wizard for updating header of Customer Order','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 190,'Add Appeasements Task Wizard','Wizard for Adding Appeasements to a Customer Order','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 200,'View Exchange Order','View Exchange Order','viewCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.ViewCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.helper.ViewCONavigationFlowHelper');

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 210,'Update Payment Task Wizard For Exchange Orders',' Wizard for Updating Payment Info on a Exchange Order','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 220,'Update Additional Details Wizard','Wizard for updating additional details','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.helper.AdditionalDtlsCONavigationFlowHelper');

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 230,'Override Price','Override Price','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

INSERT INTO A_NAV_WIZARD_DEFINITION ( DOMAIN_GROUP_ID,WIZARD_NAME,WIZARD_DESCRIPTION,WIZARD_BACKING_BEAN_NAME,WIZARD_BACKING_BEAN_CLASS,WIZARD_NAV_FLOW_HELPER) 
VALUES  ( 240,'Override Shipping Charges','Override Shipping Charges','editCustomerOrderBackingBean','com.manh.olm.selling.ui.co.customerorder.EditCustomerOrderBackingBean',null);

Commit;





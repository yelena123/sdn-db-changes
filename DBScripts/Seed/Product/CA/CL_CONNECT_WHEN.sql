set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_CONNECT_WHEN

INSERT INTO CL_CONNECT_WHEN ( CONNECT_WHEN_ID,CONNECT_WHEN_NAME) 
VALUES  ( 1,'Startup');

INSERT INTO CL_CONNECT_WHEN ( CONNECT_WHEN_ID,CONNECT_WHEN_NAME) 
VALUES  ( 2,'Per Tx');

Commit;





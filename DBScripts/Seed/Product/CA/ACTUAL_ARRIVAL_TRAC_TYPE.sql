set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ACTUAL_ARRIVAL_TRAC_TYPE

INSERT INTO ACTUAL_ARRIVAL_TRAC_TYPE ( ACTUAL_ARRIVAL_TRAC_TYPE,DESCRIPTION) 
VALUES  ( 1,'Late');

INSERT INTO ACTUAL_ARRIVAL_TRAC_TYPE ( ACTUAL_ARRIVAL_TRAC_TYPE,DESCRIPTION) 
VALUES  ( 0,'On Time');

INSERT INTO ACTUAL_ARRIVAL_TRAC_TYPE ( ACTUAL_ARRIVAL_TRAC_TYPE,DESCRIPTION) 
VALUES  ( -1,'Early');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for URI_TYPE

INSERT INTO URI_TYPE ( URI_TYPE_ID,URI_TYPE_NAME) 
VALUES  ( 1,'UI');

INSERT INTO URI_TYPE ( URI_TYPE_ID,URI_TYPE_NAME) 
VALUES  ( 2,'REST');

INSERT INTO URI_TYPE ( URI_TYPE_ID,URI_TYPE_NAME) 
VALUES  ( 3,'SOAP');

Commit;





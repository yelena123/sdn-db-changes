set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RS300RATING_CLASS

INSERT INTO RS300RATING_CLASS ( VERSION_ID,RATING_CLASS_ID,DESCRIPTION) 
VALUES  ( 'OB','OPTIBID','Rating class for Optibid');

INSERT INTO RS300RATING_CLASS ( VERSION_ID,RATING_CLASS_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','ASSIGNMENT','Used to apply the network cost for an assignment');

INSERT INTO RS300RATING_CLASS ( VERSION_ID,RATING_CLASS_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DRIVER_REQUEST','Used to select the best driver request');

INSERT INTO RS300RATING_CLASS ( VERSION_ID,RATING_CLASS_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','DOUBLE_DISPATCH','Used to evaluate double dispatch assignments');

INSERT INTO RS300RATING_CLASS ( VERSION_ID,RATING_CLASS_ID,DESCRIPTION) 
VALUES  ( 'DL610PROD','TRAILER_POOL','Used to select the best trailerPool');

Commit;





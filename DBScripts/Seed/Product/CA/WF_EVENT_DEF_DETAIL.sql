set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for WF_EVENT_DEF_DETAIL

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 89,0,'CO_AUTHORIZATION_FAILURE','77',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 76,0,'OL_ON_COMPLETE_SHORT','68',null,null,15);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 77,0,'CO_SETTLEMENT_FAILURE','34',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 1,0,'CO_BILL_TO','39','com.manh.olm.selling.events.customerorder.COBillToEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 2,0,'CO_CUSTOMER_INFO','39','com.manh.olm.selling.events.customerorder.COCustomerInfoEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 3,0,'CO_NOTE','39','com.manh.olm.selling.events.customerorder.CONoteEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 4,0,'CO_OTHER_ATTR','39','com.manh.olm.selling.events.customerorder.COOtherAttrEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 5,0,'CO_GRAND_TOTAL_ATTR','39','com.manh.olm.selling.events.customerorder.COGrandTotalEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 6,0,'CO_REF_DATA','39','com.manh.olm.selling.events.customerorder.CORefDataEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 7,0,'CO_WM_PROCESS_INFO','39','com.manh.olm.selling.events.customerorder.COWMProcessInfoEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 8,0,'CO_CANCEL','64',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 9,0,'CO_HOLD','39','com.manh.olm.selling.events.customerorder.COHoldEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 10,0,'CO_CONFIRMED','39','com.manh.olm.selling.events.customerorder.COConfirmedEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 11,0,'CO_PAYMENT_STATUS','39','com.manh.olm.selling.events.customerorder.COPaymentStatusEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 12,0,'CO_CHARGE','39','com.manh.olm.selling.events.customerorder.COChargeEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 13,0,'CO_DISCOUNT','39','com.manh.olm.selling.events.customerorder.CODiscountEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 14,0,'CO_TAX','39','com.manh.olm.selling.events.customerorder.COTaxEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 15,0,'CO_PAYMENT_DETAIL','39','com.manh.olm.selling.events.customerorder.COPaymentDetailEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 16,0,'CO_ADD_LINE','39','com.manh.olm.selling.events.customerorder.COAddLineEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 17,0,'COL_QUANTITY','39','com.manh.olm.selling.events.customerorder.COLQuantityEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 18,0,'COL_PRICE_INFO','39','com.manh.olm.selling.events.customerorder.COLPriceEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 19,0,'COL_SHIPPING_ADDRESS','39','com.manh.olm.selling.events.customerorder.COLShippingAddressEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 20,0,'COL_SHIPPING_INFO','39','com.manh.olm.selling.events.customerorder.COLShippingInfoEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 21,0,'COL_SHIPPING_METHOD','39','com.manh.olm.selling.events.customerorder.COLShippingMethodEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 22,0,'COL_ALLOCATION_INFO','39','com.manh.olm.selling.events.customerorder.COLAllocationAttrEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 23,0,'COL_NOTE','39','com.manh.olm.selling.events.customerorder.COLNoteEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 24,0,'COL_OTHER_ATTR','39','com.manh.olm.selling.events.customerorder.COLOtherAttrEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 25,0,'COL_REF_DATA','39','com.manh.olm.selling.events.customerorder.COLRefDataEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 26,0,'COL_WM_PROCESS_INFO','39','com.manh.olm.selling.events.customerorder.COLWMProcessInfoEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 27,0,'COL_CANCEL','39','com.manh.olm.selling.events.customerorder.COLCancelEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 28,0,'COL_HOLD','39','com.manh.olm.selling.events.customerorder.COLHoldEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 29,0,'COL_CHARGE','39','com.manh.olm.selling.events.customerorder.COLChargeEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 30,0,'COL_DISCOUNT','39','com.manh.olm.selling.events.customerorder.COLDiscountEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 31,0,'COL_TAX','39','com.manh.olm.selling.events.customerorder.COLTaxEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 32,0,'COL_ITEM','39','com.manh.olm.selling.events.customerorder.COLItemEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 33,0,'CO_ON_CREATE_ORDER','40',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 34,0,'CO_ON_QTY_CANCEL_WM','43','com.manh.olm.selling.events.customerorder.COLCancelQtyEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 35,0,'CO_ON_BACK_ORDER_CREATION','44',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 36,0,'CO_ON_QTY_CANCEL_ALLOC','45','com.manh.olm.selling.events.customerorder.COLCancelQtyEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 37,0,'CO_ON_QTY_UPDATE_PROMOTION','46',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 38,0,'OL_ON_SUBMIT_ORDER_LINES','47',null,null,15);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 39,0,'CO_POST_CREATE_ORDER','48',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 40,0,'CO_POST_CREATE_ORDER_LINE','49',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 41,0,'CO_POST_UPDATE_ORDER','50',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 42,0,'CO_POST_QTY_CANCEL_WM','51',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 43,0,'CO_POST_BACKORDER_CREATE','52',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 44,0,'CO_POST_QTY_CANCEL_ALLOC','53',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 45,0,'CO_POST_QTY_UPDATE_PROMOTION','54',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 46,0,'OL_POST_SUBMIT_ORDER_LINES','55',null,null,15);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 51,0,'LPN_ON_CREATED','60',null,null,20);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 63,0,'CO_UPDATE_CONFIRM','62',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 97,0,'ASN_UPDATE','97','com.manh.olm.returns.events.asn.ASNVerificationEventDetector',null,70);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 65,0,'CM_ON_CREATE','65',null,0,50);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 66,0,'CM_ON_UPDATE','66','com.manh.olm.selling.events.customer.CustomerMasterUpdateEventDetector',0,50);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 67,0,'CO_CM_REGISTER','67',null,0,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 68,0,'CO_ORDER_STATUS_CHANGE','30',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 69,0,'CO_SETTLEMENT_SUCCESS','31',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 70,0,'RETURN_INVOICE_CREATED','102',null,null,90);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 72,0,'ON_DO_CREATE','222',null,null,80);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 73,0,'ON_ALLOCATION','221',null,null,100);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 74,0,'CO_AUTHORIZATION_SUCCESS','32',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 75,0,'CO_REFUND_SUCCESS','33',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 78,0,'CO_UI_COM_NOTE_CREATE','69',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 79,0,'CO_COM_NOTE_CREATE','70',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 80,0,'CO_COM_NOTE_UPDATE','71',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 81,0,'CO_ON_OVERRIDE_ITEM_PRICE','39','com.manh.olm.selling.events.customerorder.COLItemPriceOverrideEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 82,0,'CO_ON_REVERT_ITEM_PRICE','39','com.manh.olm.selling.events.customerorder.COLItemPriceRevertEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 83,0,'CO_APPLY_DISCOUNT_CODE','72',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 84,0,'CO_REMOVE_DISCOUNT_CODE','73',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 85,0,'CO_SNH_OVERRIDE','74',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 86,0,'CO_SNH_RECALC','75',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 98,0,'RO_STATUS_CHANGE','109',null,null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 88,0,'CO_UPDATE_STORE_CODE','39','com.manh.olm.selling.events.customerorder.COUpdateStoreEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 90,0,'COL_CANCEL_BY_SYSTEM','81',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 91,0,'CONFIRM_ORDER_REQUEST','79',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 92,0,'RETRY_AUTHORIZATION','80',null,null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 93,0,'CO_APPEASEMENT','39','com.manh.olm.selling.events.customerorder.COAppeasementEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 94,0,'COL_APPEASEMENT','39','com.manh.olm.selling.events.customerorder.COLAppeasementEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 95,0,'CO_OTHER_DISCOUNT','39','com.manh.olm.selling.events.customerorder.COOtherDiscountEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 96,0,'COL_OTHER_DISCOUNT','39','com.manh.olm.selling.events.customerorder.COLOtherDiscountEventDetector',null,30);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 100,0,'RO_UPDATE','109','com.manh.olm.returns.events.returnorder.ROUpdateEventDetector',null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 99,0,'RO_CREATE','42',null,null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 101,0,'RO_CANCEL','41',null,null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 102,0,'RO_CONFIRMED','109','com.manh.olm.returns.events.returnorder.ROConfirmedEventDetector',null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 103,0,'ROL_CANCEL','109','com.manh.olm.returns.events.returnorder.ROLCancelEventDetector',null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 104,0,'RO_WAIVE_FEE','109','com.manh.olm.returns.events.returnorder.ROWaiveFeeEventDetector',null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 105,0,'RO_PROCESS','110',null,null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 106,0,'RO_RESEND_RETURN_LABEL','111',null,null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 107,0,'ON_ALLOCATION_TRANSFER','78',null,null,100);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 108,0,'CREATE_RCI','98','com.manh.olm.returns.rci.events.RCIEventDetector',null,110);

INSERT INTO WF_EVENT_DEF_DETAIL ( WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES  ( 109,0,'CO_REFUND_FAILURE','99',null,null,30);

Commit;





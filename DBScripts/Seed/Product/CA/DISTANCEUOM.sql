set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DISTANCEUOM

INSERT INTO DISTANCEUOM ( DISTANCEUOM,DESCRIPTION) 
VALUES  ( 0,'MI');

INSERT INTO DISTANCEUOM ( DISTANCEUOM,DESCRIPTION) 
VALUES  ( 1,'KM');

Commit;





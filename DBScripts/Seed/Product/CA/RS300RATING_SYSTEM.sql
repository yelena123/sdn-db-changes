set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RS300RATING_SYSTEM

INSERT INTO RS300RATING_SYSTEM ( VERSION_ID,CLIENT_ID,LOCK_STATUS,USER_NAME,COMMENTS,LAST_UPDATE) 
VALUES  ( 'OB','TP',0,null,'Rating System',null);

INSERT INTO RS300RATING_SYSTEM ( VERSION_ID,CLIENT_ID,LOCK_STATUS,USER_NAME,COMMENTS,LAST_UPDATE) 
VALUES  ( 'DL610PROD','BASE',null,null,null,null);

Commit;





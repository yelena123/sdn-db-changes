set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for REPORT_DEF

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'CarrUtlCmt','CarrierUtilizationAndCommitment','Report provides the measurement of commitments by carrier, and how the shipper is meeting those demands.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'RepCList','CarrierList','To generate a report to find the carriers added for the day, week, etc.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'RepDlySchd','DailyScheduleByOriginAndCarrier','Report lists the daily delivery schedule based on the loading facilities for a specified date or date range.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'CityRep','ScenarioCityReport','City Report for the given scenario','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'StateRep','ScenarioStateReport','State Report for the given scenario','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'ScenFacRep','ScenarioFacilityReport','Facility Report for the given scenario','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'UncoverRep','ScenarioUncoveredLaneReport','Uncovered Lane Report for the given scenario','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'ZoneRep','ScenarioZoneReport','Zone Report for the given scenario','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'LaneRep','ScenarioLaneReport','Lane Report for the given scenario','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'ZipCodeRep','ScenarioZipCodeReport','Zip Code Report for the given scenario','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'CarrierRep','ScenarioCarrierReport','Carrier Report for the given scenario','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'abc123459','PrintUserTenderActivity','To Print User (Tender) Activity','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'abd1234568','ResourceSelectionOverrideReport','Statistics on the percentage of loads tendered.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef3','ContinuousMoveSummaryReport','All continuous moves that occured within a specified actual delivery date','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef5','DailyPickupSchedule','Report lists the number and percentage of pickups for load-at facilities for each day of the week as well as the single move charges for shipment segments included.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef7','ContinuousMoveSavingsByLeg','All continuous moves that occured within a specified actual delivery date grouped by Carrier Code and Continuous Movement Number.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef8','CarrierCommitmentListing','Report provides a measurement of commitments by carrier.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'COverRide','CarrierOverride','Analysis of carreir over ride by user and Origin','OracleDB','MR1');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'CStatusRes','CarrierStatusResponsiveness','Analysis of carreir Status response','OracleDB','MR1');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'PubRateTrf','PublishableRateTariff','Publishable Rate Tariff ','OracleDB','MR1');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef11','ShipmentStatusesDue','Report which shows all shipments with statuses due and which statuses they are','OracleDB','MR1');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef1','PrintUserTenderActivity1','To Print User (Tender) Activity','OracleDB','MR1');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef2','ResourceSelectionOverrideReport1','Generate statistics on the percentage of loads tendered','OracleDB','MR1');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef6','DailyScheduleByOriginAndCarrier','Report lists the daily delivery schedule based on the loading facilities for a specified date or date range.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef9','OriginSummaryReport','Report displays summary information for all shipments by origin within a specified date range.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef4','ContinuousMoveSavingsByOrigin','ContinuousMoveSavingsByOrigin_Description','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef12','RunSummaryConsolidationRun','RunSummaryConsolidationRun_desc','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repDToPay','DaysToPay','To generate a report to retreive Invoices in all status.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repRtByCar','RatesByCarrier','To generate a report to retrieve rates based on Carrier.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repCList','CarrierList','To generate a report to find the carriers added for the day, week, etc.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repdef420','InvoiceStatusCntByDollarByCarriers','Invoice Status Count And Dollar By Carriers Report','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'CarrCmtDtl','CarrierUtilizationAndCommitment','Report provides the measurement of commitments by carrier, and how the shipper is meeting those demands.','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'DDReport','Direct Delivery Shipment','Report lists a group of shipments that by-passed a cross dock facility and directly arrived at the Store','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'repPOQS','POReports','To Report PO Quick Status','OracleDB','MR');

INSERT INTO REPORT_DEF ( REPORT_DEF_ID,REPORT_NAME,DESCRIPTION,DBNAME,TYPE) 
VALUES  ( 'ApptSched','AppointmentScheduled','To generate a report for Appointments scheduled for the selected date','OracleDB','MR');

Commit;





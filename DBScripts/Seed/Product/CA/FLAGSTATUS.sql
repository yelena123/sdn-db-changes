set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for FLAGSTATUS

INSERT INTO FLAGSTATUS ( FLAGSTATUS,DESCRIPTION) 
VALUES  ( 0,'No Flag');

INSERT INTO FLAGSTATUS ( FLAGSTATUS,DESCRIPTION) 
VALUES  ( 1,'Flag to Follow Up');

INSERT INTO FLAGSTATUS ( FLAGSTATUS,DESCRIPTION) 
VALUES  ( 2,'Flag Complete');

Commit;





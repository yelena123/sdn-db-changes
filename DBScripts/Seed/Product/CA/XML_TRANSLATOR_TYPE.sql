set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for XML_TRANSLATOR_TYPE

INSERT INTO XML_TRANSLATOR_TYPE ( XML_TRANSLATOR_TYPE,DESCRIPTION) 
VALUES  ( 4,'Java');

INSERT INTO XML_TRANSLATOR_TYPE ( XML_TRANSLATOR_TYPE,DESCRIPTION) 
VALUES  ( 8,'Report');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TRIP_TYPE_TYPE

INSERT INTO TRIP_TYPE_TYPE ( TRIP_TYPE_TYPE,DESCRIPTION) 
VALUES  ( 1,'Distance');

INSERT INTO TRIP_TYPE_TYPE ( TRIP_TYPE_TYPE,DESCRIPTION) 
VALUES  ( 2,'Segment Type');

INSERT INTO TRIP_TYPE_TYPE ( TRIP_TYPE_TYPE,DESCRIPTION) 
VALUES  ( 3,'Driver Type');

Commit;





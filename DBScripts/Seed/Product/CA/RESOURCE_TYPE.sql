set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RESOURCE_TYPE

INSERT INTO RESOURCE_TYPE ( RESOURCE_TYPE,DESCRIPTION) 
VALUES  ( 'A','Assigned Resource');

INSERT INTO RESOURCE_TYPE ( RESOURCE_TYPE,DESCRIPTION) 
VALUES  ( 'N','No Resource - Imported Accessorial');

INSERT INTO RESOURCE_TYPE ( RESOURCE_TYPE,DESCRIPTION) 
VALUES  ( 'R','Recommended Resource');

Commit;





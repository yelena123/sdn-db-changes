set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for I_INVENTORY_METADATA

INSERT INTO I_INVENTORY_METADATA ( IDENTITY,INV_TYPE,INV_CLASS_NAME,INV_LOG_CLASS_NAME,ONHAND_MOVEMENT_CLASS_NAME,SUPPORTS_SUBLOC_DISP) 
VALUES  ( 1,0,'com.manh.olm.inventory.view.PerpetualInventory','com.manh.olm.inventory.event.log.impl.PerpetualViewEventDtl','com.manh.olm.inventory.event.impl.OnHandMovement',1);

INSERT INTO I_INVENTORY_METADATA ( IDENTITY,INV_TYPE,INV_CLASS_NAME,INV_LOG_CLASS_NAME,ONHAND_MOVEMENT_CLASS_NAME,SUPPORTS_SUBLOC_DISP) 
VALUES  ( 2,1,'com.manh.olm.inventory.view.SegmentedInventory','com.manh.olm.inventory.event.log.impl.SegmentedViewEventDtl',null,0);

Commit;





set echo on

@@CA_Sequences.sql
@@CA_Views.sql
@@CA_ProcFuncPkg.sql
-- @@CA_Nav_FK_Fix.sql
delete from NAVIGATION_APP_MOD_PERM where (app_id,MODULE_ID,PERMISSION_ID) not in (select app_id,MODULE_ID,PERMISSION_ID from APP_MOD_PERM);
commit;
@@CA_FKs.sql

exit

set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for A_NAV_WIZARD_PAGE_DEFINITION

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 1,'searchAndAddItem','Add Item','Search items description','/doms/dom/selling/co/item/SearchItemsCreateCustomerOrderPageContent.xhtml','/doms/dom/selling/co/item/SearchItemsCreateCustomerOrderFooter.xhtml','Item Search','coSearchItemStepContentBackingBean','coSearchItemStepFooterBackingBean','com.manh.olm.selling.ui.co.item.CustomerOrderSearchItemStepContentBackingBean',
'com.manh.olm.selling.ui.co.item.CustomerOrderSearchItemStepFooterBackingBean','com.manh.olm.selling.ui.co.item.CustomerOrderSearchItemStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 2,'orderDetailsStep','Order Details','Order details description','/doms/dom/selling/co/customerorder/CustomerOrderDetailsStep.xhtml','/doms/dom/selling/co/customerorder/CustomerOrderDetailsFooter.xhtml','Order Details','coDetailsStepContentBackingBean','coDetailsStepFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 3,'reviewOrderStep','Review Order','Review Order description','/doms/dom/selling/co/customerorder/COReviewOrderStep.xhtml','/doms/dom/selling/co/customerorder/COReviewOrderFooter.xhtml','Order Summary','coReviewOrderStepBackingBean','coReviewOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderStepBackingBean',
'com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 4,'allocationDetails','Allocation details','Allocation page description','/doms/dom/selling/co/customerorder/CustomerOrderAllocationList.xhtml',null,'Allocation Details','CustomerOrderAllocationBackingBean',null,'com.manh.olm.selling.ui.co.customerorder.CustomerOrderAllocationBackingBean',
null,null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 5,'promotionDetails','Add Promotions','Promotions','/doms/dom/selling/co/promotion/CustomerOrderPromotionDetailsStep.xhtml','/doms/dom/selling/co/promotion/CustomerOrderPromotionDetailsFooter.xhtml','Promotion Details','coPromotionDetailsStepContentBackingBean','coPromotionDetailsStepFooterBackingBean','com.manh.olm.selling.ui.co.promotion.COPromotionDetailsStepContentBackingBean',
'com.manh.olm.selling.ui.co.promotion.COPromotionDetailsStepFooterBackingBean','com.manh.olm.selling.ui.co.promotion.CustomerOrderPromotionDetailsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 6,'shippingInfo','Add Shipping Info','Shipping Information','/doms/dom/selling/co/subsystem/CustomerOrderShippingInformationStep.xhtml','/doms/dom/selling/co/subsystem/CustomerOrderShippingInformationStepFooter.xhtml','Shipping Details','coShippingInfoStepContentBackingBean','coShippingInformationStepFooterBackingBean','com.manh.olm.selling.ui.co.subsystem.CustomerOrderShippingInformationStepContentBackingBean',
'com.manh.olm.selling.ui.co.subsystem.CustomerOrderShippingInformationStepFooterBackingBean','com.manh.olm.selling.ui.co.subsystem.CustomerOrderShippingInformationStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 7,'paymentTransactions','Payment Details','Payment Details','/doms/dom/selling/co/payment/COPaymentTransactionStep.xhtml','/doms/dom/selling/co/payment/COPaymentTransactionsFooter.xhtml','Payment Transactions','coPaymentTransactionsStepContentBackingBean','coPaymentTransactionsStepFooterBackingBean','com.manh.olm.selling.ui.co.payment.COPaymentTransactionsStepContentBackingBean',
'com.manh.olm.selling.ui.co.payment.COPaymentTransactionsStepFooterBackingBean','com.manh.olm.selling.ui.co.payment.COPaymentTransactionsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 8,'additionalDetails','Additional Details','Additional Details','/doms/dom/selling/co/customerorder/CustomerOrderAdditionalInformationStep.xhtml','/doms/dom/selling/co/customerorder/COAdditionalInformationFooter.xhtml','Additional Details','coAdditionalInfoStepBackingBean','coAdditionalInfoFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.COAdditionalInfoStepBackingBean',
'com.manh.olm.selling.ui.co.customerorder.COAdditionalInfoFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderAdditionalInfoStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 9,'paymentDetails','Add Payment','Payment Details','/doms/dom/selling/co/payment/COPaymentDetailsStep.xhtml','/doms/dom/selling/co/payment/COPaymentDetailsStepFooter.xhtml','Payment Details','coPaymentDetailsBackingBean','coPaymentDetailsFooterBackingBean','com.manh.olm.selling.ui.co.payment.CustomerOrderPaymentDetailsBackingBean',
'com.manh.olm.selling.ui.co.payment.CustomerOrderPaymentDetailsFooterBackingBean','com.manh.olm.selling.ui.co.payment.CustomerOrderPaymentDetailsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 10,'viewOrderDetails','View Order Details','View Order Details','/doms/dom/selling/co/customerorder/CustomerOrderViewOrderDetailsStep.xhtml',null,'Order Details','coDetailsStepContentBackingBean','viewCustomerOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.ViewCustomerOrderFooterBackingBean',null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 11,'viewPromotionDetails','View Promotion Details','View Promotion Details','/doms/dom/selling/co/promotion/CustomerOrderViewPromotionDetailsStep.xhtml',null,'Promotion Details','coPromotionDetailsStepContentBackingBean','viewCustomerOrderFooterBackingBean','com.manh.olm.selling.ui.co.promotion.COPromotionDetailsStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.ViewCustomerOrderFooterBackingBean',null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 12,'viewShippingInfo','View Shipping Info','View Shipping Info','/doms/dom/selling/co/subsystem/CustomerOrderViewShippingInformationStep.xhtml',null,'Shipping Details','coShippingInfoStepContentBackingBean','viewCustomerOrderFooterBackingBean','com.manh.olm.selling.ui.co.subsystem.CustomerOrderShippingInformationStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.ViewCustomerOrderFooterBackingBean',null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 13,'viewPaymentDetails','View Payment Details','View Payment Details','/doms/dom/selling/co/payment/COViewPaymentDetailsStep.xhtml',null,'Payment Details','coViewPaymentDetailsStepContentBackingBean','viewCustomerOrderFooterBackingBean','com.manh.olm.selling.ui.co.payment.COViewPaymentDetailsStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.ViewCustomerOrderFooterBackingBean',null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 14,'viewAllocationDetails','View Allocation Details','View Allocation Details','/doms/dom/selling/co/customerorder/CustomerOrderAllocationList.xhtml',null,'Allocation Details','CustomerOrderAllocationBackingBean',null,'com.manh.olm.selling.ui.co.customerorder.CustomerOrderAllocationBackingBean',
null,null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 15,'viewAdditionalDetails','View Additional Details','View Additional Details','/doms/dom/selling/co/customerorder/CustomerOrderViewAdditionalInformationStep.xhtml','/doms/dom/selling/co/customerorder/ViewCOAdditionalInformationFooter.xhtml','Additional Details','coAdditionalInfoStepBackingBean','coAdditionalInfoFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.COAdditionalInfoStepBackingBean',
'com.manh.olm.selling.ui.co.customerorder.COAdditionalInfoFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderAdditionalInfoStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 16,'viewPaymentTransactions','View Payment Transactions','View Payment Transactions','/doms/dom/selling/co/payment/COPaymentTransactionStep.xhtml',null,'Payment Transactions','coPaymentTransactionsStepContentBackingBean',null,'com.manh.olm.selling.ui.co.payment.COPaymentTransactionsStepContentBackingBean',
null,null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 34,'customerCommunication','Customer Communications','Customer communication page description','/doms/dom/selling/co/communication/CustomerCommunicationsDetailStep.xhtml',null,'Customer Communications','ccDetailStepContentBackingBean',null,'com.manh.olm.selling.ui.co.communication.CustomerCommunicationDetailStepContentBackingBean',
null,null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 28,'viewExchangeOrderDetails','View ExchangeOrder Detail','View Exchange Order Details','/doms/dom/selling/co/customerorder/ExchangeOrderViewOrderDetailsStep.xhtml',null,'Exchange Order Details','coDetailsStepContentBackingBean','viewCustomerOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.ViewCustomerOrderFooterBackingBean',null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 29,'editEOConfirmChangesStep','Edit EO Confirm Changes','Edit EO Confirm Changes page','/doms/dom/selling/co/customerorder/EditEOConfirmChangesStep.xhtml','/doms/dom/selling/co/customerorder/EditEOConfirmChangesStepFooter.xhtml','Order Summary','coReviewOrderStepBackingBean','coReviewOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderStepBackingBean',
'com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 30,'editPaymentDetailsForEO','Edit Payment For EO','Add Payment description','/doms/dom/selling/co/payment/EditEOPaymentDetailsStep.xhtml','/doms/dom/selling/co/payment/EditCOPaymentDetailsFooter.xhtml','Payment Details','coPaymentDetailsBackingBean','editCOPaymentFooterBB','com.manh.olm.selling.ui.co.payment.CustomerOrderPaymentDetailsBackingBean',
'com.manh.olm.selling.ui.co.payment.EditCOPaymentDetailsFooterBackingBean','com.manh.olm.selling.ui.co.payment.CustomerOrderPaymentDetailsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 31,'viewAllocationTrace','View Allocation Trace','View Allocation Trace','/doms/dom/orderallocation/jsp/AllocationFacilityTrace.xhtml',null,'Allocation Trace','allocationFacilityTraceListBackingBean','viewCustomerOrderFooterBackingBean','com.manh.olm.fulfillment.allocation.trace.AllocationFacilityTraceListBackingBean',
'com.manh.olm.selling.ui.co.customerorder.ViewCustomerOrderFooterBackingBean',null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 32,'distributionOrderDetails','DO details','Distribution order page description','/doms/dom/selling/co/customerorder/CustomerOrderDistributionOrderList.xhtml',null,'Distribution Order Details','CustomerOrderDistributionOrderBackingBean',null,'com.manh.olm.selling.ui.co.customerorder.CustomerOrderDistributionOrderBackingBean',
null,null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 33,'editAdditionalDetailsStep','Edit Additional Details','Edit Additional Details','/doms/dom/selling/co/customerorder/COAdditionalInformationEditableStep.xhtml','/doms/dom/selling/co/customerorder/COAdditionalInformationFooter.xhtml','Additional Details','coAdditionalInfoStepBackingBean','coAdditionalInfoFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.COAdditionalInfoStepBackingBean',
'com.manh.olm.selling.ui.co.customerorder.COAdditionalInfoFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderAdditionalInfoStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 17,'editCOConfirmChangesStep','Edit CO Confirm Changes','Edit CO Confirm Changes page','/doms/dom/selling/co/customerorder/EditCOConfirmChangesStep.xhtml','/doms/dom/selling/co/customerorder/EditCOConfirmChangesStepFooter.xhtml','Order Summary','coReviewOrderStepBackingBean','coReviewOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderStepBackingBean',
'com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 18,'viewAuditDetails','View Audit Details','View Audit Details','/doms/dom/selling/co/customerorder/CustomerOrderAuditDetailsStep.xhtml',null,'Audit Details','customerOrderAuditDetailsStepBackingBean',null,'com.manh.olm.selling.ui.co.customerorder.CustomerOrderAuditDetailsStepBackingBean',
null,null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 19,'editOrderDetailsStep','Edit Order Details','Edit Order Details description','/doms/dom/selling/co/customerorder/EditCOOrderDetailsStep.xhtml','/doms/dom/selling/co/customerorder/CustomerOrderDetailsFooter.xhtml','Order Details','coDetailsStepContentBackingBean','coDetailsStepFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 20,'editPromotionDetailsStep','Edit Promotion Details','Edit Promotion Details description','/doms/dom/selling/co/promotion/EditCOPromotionDetailsStep.xhtml','/doms/dom/selling/co/promotion/CustomerOrderPromotionDetailsFooter.xhtml','Promotion Details','coPromotionDetailsStepContentBackingBean','coPromotionDetailsStepFooterBackingBean','com.manh.olm.selling.ui.co.promotion.COPromotionDetailsStepContentBackingBean',
'com.manh.olm.selling.ui.co.promotion.COPromotionDetailsStepFooterBackingBean','com.manh.olm.selling.ui.co.promotion.CustomerOrderPromotionDetailsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 21,'editShippingInfoStep','Edit Shipping Info','Edit Shipping Info description','/doms/dom/selling/co/subsystem/EditCOShippingInformationStep.xhtml','/doms/dom/selling/co/subsystem/CustomerOrderShippingInformationStepFooter.xhtml','Shipping Details','coShippingInfoStepContentBackingBean','coShippingInformationStepFooterBackingBean','com.manh.olm.selling.ui.co.subsystem.CustomerOrderShippingInformationStepContentBackingBean',
'com.manh.olm.selling.ui.co.subsystem.CustomerOrderShippingInformationStepFooterBackingBean','com.manh.olm.selling.ui.co.subsystem.CustomerOrderShippingInformationStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 22,'editPaymentDetailsStep','Edit Payment','Add Payment description','/doms/dom/selling/co/payment/EditCOPaymentDetailsStep.xhtml','/doms/dom/selling/co/payment/EditCOPaymentDetailsFooter.xhtml','Payment Details','coPaymentDetailsBackingBean','editCOPaymentFooterBB','com.manh.olm.selling.ui.co.payment.CustomerOrderPaymentDetailsBackingBean',
'com.manh.olm.selling.ui.co.payment.EditCOPaymentDetailsFooterBackingBean','com.manh.olm.selling.ui.co.payment.CustomerOrderPaymentDetailsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 23,'coDetailsCancelItemStep','Order Detail','CO Details Cancel Items Step page','/doms/dom/selling/co/customerorder/EditCODetailsCancelItemStep.xhtml','/doms/dom/selling/co/customerorder/EditCODetailsCancelItemStepFooter.xhtml','Order Details','coDetailsStepContentBackingBean','coDetailsCancelItemFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.CODetailsCancelItemFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderDetailsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 24,'coEditHeaderStep','Edit Order Header','CO Edit Header Step','/doms/dom/selling/co/customerorder/CustomerOrderViewOrderDetailsStep.xhtml','/doms/dom/selling/co/customerorder/COEditHeaderTaskStepFooter.xhtml','Order Details','coEditHeaderTaskStepContentBackingBean','coEditHeaderTaskFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.COEditHeaderTaskStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.COEditHeaderTaskStepFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.COEditHeaderStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 25,'invoice','Invoice','Invoice Summary and Details','/doms/dom/selling/co/invoice/COInvoiceDetails.xhtml',null,'Invoice Details','coInvoiceStepBackingBean',null,'com.manh.olm.selling.ui.co.invoice.InvoiceDetailsStepBackingBean',
null,null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 26,'CCOPaymentTransactions','Payment Transactions','Payment Transactions','/doms/dom/selling/co/payment/COViewPaymentTransactionStep.xhtml','/doms/dom/selling/co/payment/COPaymentTransactionsFooter.xhtml','Payment Transactions','coPaymentTransactionsStepContentBackingBean','coPaymentTransactionsStepFooterBackingBean','com.manh.olm.selling.ui.co.payment.COPaymentTransactionsStepContentBackingBean',
'com.manh.olm.selling.ui.co.payment.COPaymentTransactionsStepFooterBackingBean','com.manh.olm.selling.ui.co.payment.COPaymentTransactionsStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 27,'shipmentDetailStep','Shipment Details','Shipment Details','/doms/dom/selling/co/customerorder/COShipmentDetailStep.xhtml',null,'Shipment Details','coShipmentDetailStepBB',null,'com.manh.olm.selling.ui.co.customerorder.CustomerOrderShipmentDetailStepBackingBean',
null,null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 48,'overridePriceStep','Override Price step','Override price step page','/doms/dom/selling/co/customerorder/OverridePriceStep.xhtml','/doms/dom/selling/co/customerorder/CustomerOrderDetailsFooter.xhtml','Override Price','coReviewOrderStepBackingBean','coReviewOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderStepBackingBean',
'com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderFooterBackingBean',null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 49,'overrideSnHChargeStep','Override SnHCharge Step','Override SnHCharge step page','/doms/dom/selling/co/customerorder/OverrideSnHChargeStep.xhtml','/doms/dom/selling/co/customerorder/CustomerOrderDetailsFooter.xhtml','Override S&H','coReviewOrderStepBackingBean','coReviewOrderFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderStepBackingBean',
'com.manh.olm.selling.ui.co.customerorder.CustomerOrderReviewOrderFooterBackingBean',null,0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 50,'editCOAppeasementStep','Appeasements','Edit Appeasement Description','/doms/dom/selling/co/promotion/EditCOAppeasementDetailsStep.xhtml','/doms/dom/selling/co/customerorder/COEditHeaderTaskStepFooter.xhtml','Appeasements','coPromotionDetailsStepContentBackingBean','coEditHeaderTaskFooterBackingBean','com.manh.olm.selling.ui.co.payment.COAppeasementStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.COEditHeaderTaskStepFooterBackingBean','com.manh.olm.selling.ui.co.customerorder.COEditHeaderStepHelper',0);

INSERT INTO A_NAV_WIZARD_PAGE_DEFINITION ( PAGE_ID,PAGE_NAME,PAGE_LABEL,PAGE_DESCRIPTION,PAGE_CONTENT_URL,PAGE_FOOTER_URL,PAGE_TITLE,PAGE_CONTENT_BB_NAME,PAGE_FOOTER_BB_NAME,PAGE_CONTENT_BB_CLASS,
PAGE_FOOTER_BB_CLASS,PAGE_HELPER_CLASS,IS_EXTERNAL_PAGE) 
VALUES  ( 51,'viewCOAppeasementStep','View Appeasements','View Appeasement Description','/doms/dom/selling/co/promotion/ViewCOAppeasementDetailsStep.xhtml',null,'Appeasements','coPromotionDetailsStepContentBackingBean','viewCustomerOrderFooterBackingBean','com.manh.olm.selling.ui.co.payment.COAppeasementStepContentBackingBean',
'com.manh.olm.selling.ui.co.customerorder.ViewCustomerOrderFooterBackingBean',null,0);

Commit;





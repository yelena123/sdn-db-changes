set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for NAVIGATION_PARAMETER

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 62,null,'tranId',null,SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 63,null,'tranId',null,SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 65,null,'tranId','1',SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 66,null,'tranId','2',SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 83,2000599,'tranId','4',SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 84,2000602,'tranId','5',SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 86,2000597,'tranId','1',SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 87,2000598,'tranId','2',SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 101,2000601,'tranId','8',SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 102,2000596,'tranId','7',SYSDATE,systimestamp);

INSERT INTO NAVIGATION_PARAMETER ( NAVIGATION_PARAMETER_ID,NAVIGATION_ID,PARAMETER_CODE,PARAMETER_VALUE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 103,2000600,'tranId','9',SYSDATE,systimestamp);

Commit;





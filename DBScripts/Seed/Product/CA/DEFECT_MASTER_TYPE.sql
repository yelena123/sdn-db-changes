set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DEFECT_MASTER_TYPE

INSERT INTO DEFECT_MASTER_TYPE ( DEFECT_MASTER_TYPE,DESCRIPTION) 
VALUES  ( 10,'Item');

INSERT INTO DEFECT_MASTER_TYPE ( DEFECT_MASTER_TYPE,DESCRIPTION) 
VALUES  ( 20,'LPN');

Commit;





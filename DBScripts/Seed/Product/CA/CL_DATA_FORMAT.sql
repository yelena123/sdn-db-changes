set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_DATA_FORMAT

INSERT INTO CL_DATA_FORMAT ( DATA_FORMAT_ID,DATA_FORMAT_NAME) 
VALUES  ( 0,'Text');

INSERT INTO CL_DATA_FORMAT ( DATA_FORMAT_ID,DATA_FORMAT_NAME) 
VALUES  ( 1,'XML');

Commit;





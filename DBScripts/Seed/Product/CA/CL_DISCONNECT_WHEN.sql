set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_DISCONNECT_WHEN

INSERT INTO CL_DISCONNECT_WHEN ( DISCONNECT_WHEN_ID,DISCONNECT_WHEN_NAME) 
VALUES  ( 1,'Never');

INSERT INTO CL_DISCONNECT_WHEN ( DISCONNECT_WHEN_ID,DISCONNECT_WHEN_NAME) 
VALUES  ( 2,'Per Tx');

Commit;





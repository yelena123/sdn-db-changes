set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_EST_TRANS_TYPE

INSERT INTO TCS_EST_TRANS_TYPE ( TRANS_TYPE_ID,TRANS_TYPE_NAME,TRANS_TYPE_JAVA_CLASS) 
VALUES  ( 1,'Estimation Object','com.manh.tcs.domain.estimation.estimate.TCSEstimation');

INSERT INTO TCS_EST_TRANS_TYPE ( TRANS_TYPE_ID,TRANS_TYPE_NAME,TRANS_TYPE_JAVA_CLASS) 
VALUES  ( 2,'Estimation Object Line Item','com.manh.tcs.domain.estimation.estimate.TCSEstimationLineItem');

INSERT INTO TCS_EST_TRANS_TYPE ( TRANS_TYPE_ID,TRANS_TYPE_NAME,TRANS_TYPE_JAVA_CLASS) 
VALUES  ( 3,'Cost Schedule','com.manh.tcs.domain.estimation.estimate.costschedule.TCSEstimationCostSchedule');

INSERT INTO TCS_EST_TRANS_TYPE ( TRANS_TYPE_ID,TRANS_TYPE_NAME,TRANS_TYPE_JAVA_CLASS) 
VALUES  ( 4,'Cost Schedule Component','com.manh.tcs.domain.estimation.estimate.costschedulecomp.TCSEstimationCostScheduleComp');

INSERT INTO TCS_EST_TRANS_TYPE ( TRANS_TYPE_ID,TRANS_TYPE_NAME,TRANS_TYPE_JAVA_CLASS) 
VALUES  ( 5,'Cost Schedule Component Cost Event','com.manh.tcs.domain.estimation.estimate.costschedulecomp.TCSEstimationCostScheduleCompCE');

INSERT INTO TCS_EST_TRANS_TYPE ( TRANS_TYPE_ID,TRANS_TYPE_NAME,TRANS_TYPE_JAVA_CLASS) 
VALUES  ( 7,'Facility','com.manh.common.facility.FacilityAlias');

INSERT INTO TCS_EST_TRANS_TYPE ( TRANS_TYPE_ID,TRANS_TYPE_NAME,TRANS_TYPE_JAVA_CLASS) 
VALUES  ( 8,'Item','com.manh.cbo.domain.item.Item');

Commit;





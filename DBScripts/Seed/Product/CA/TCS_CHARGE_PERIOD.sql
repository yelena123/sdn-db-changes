set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TCS_CHARGE_PERIOD

INSERT INTO TCS_CHARGE_PERIOD ( CHARGE_PERIOD_ID,CHARGE_PERIOD_NAME) 
VALUES  ( 1,'Per Year');

INSERT INTO TCS_CHARGE_PERIOD ( CHARGE_PERIOD_ID,CHARGE_PERIOD_NAME) 
VALUES  ( 2,'Per Month');

INSERT INTO TCS_CHARGE_PERIOD ( CHARGE_PERIOD_ID,CHARGE_PERIOD_NAME) 
VALUES  ( 3,'Per Hour');

Commit;





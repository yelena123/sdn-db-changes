set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for INV_ENTRY_TYPE

INSERT INTO INV_ENTRY_TYPE ( INV_ENTRY_TYPE,DESCRIPTION) 
VALUES  ( 8,'Shipment');

INSERT INTO INV_ENTRY_TYPE ( INV_ENTRY_TYPE,DESCRIPTION) 
VALUES  ( 10,'Customer Invoice');

INSERT INTO INV_ENTRY_TYPE ( INV_ENTRY_TYPE,DESCRIPTION) 
VALUES  ( 14,'Parcel');

INSERT INTO INV_ENTRY_TYPE ( INV_ENTRY_TYPE,DESCRIPTION) 
VALUES  ( 12,'Booking');

Commit;





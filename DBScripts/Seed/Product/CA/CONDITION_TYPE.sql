set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CONDITION_TYPE

INSERT INTO CONDITION_TYPE ( CONDITION_TYPE,CONDITION_TYPE_CAT_DESC,DESCRIPTION) 
VALUES  ( 4,'EQUIPMENT_INST','Good');

INSERT INTO CONDITION_TYPE ( CONDITION_TYPE,CONDITION_TYPE_CAT_DESC,DESCRIPTION) 
VALUES  ( 8,'EQUIPMENT_INST','Damaged');

Commit;





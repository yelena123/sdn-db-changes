set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ILM_REQUESTOR_TYPE

INSERT INTO ILM_REQUESTOR_TYPE ( REQUESTOR_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'CARRIER',SYSTIMESTAMP,systimestamp);

INSERT INTO ILM_REQUESTOR_TYPE ( REQUESTOR_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'SHIPPER',SYSTIMESTAMP,systimestamp);

INSERT INTO ILM_REQUESTOR_TYPE ( REQUESTOR_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 3,'VENDOR',SYSTIMESTAMP,systimestamp);

Commit;





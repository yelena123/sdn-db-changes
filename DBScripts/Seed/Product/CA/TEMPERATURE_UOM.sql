set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TEMPERATURE_UOM

INSERT INTO TEMPERATURE_UOM ( TEMPERATURE_UOM,DESCRIPTION) 
VALUES  ( 1,'FA');

INSERT INTO TEMPERATURE_UOM ( TEMPERATURE_UOM,DESCRIPTION) 
VALUES  ( 2,'CE');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_EVENT_MASTER

INSERT INTO CL_EVENT_MASTER ( VERSION_ID,EVENT_ID,ENABLED,DFLT_MSG_PRTY,EVENT_DESC,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,6310,1,0,'Weigh oLPN',SYSDATE,systimestamp);

Commit;





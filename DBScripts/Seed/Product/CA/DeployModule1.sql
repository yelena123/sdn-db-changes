set echo on

-- create the product tables, pkeys, and synonyms
@@CA_Tables_PKs.sql
@@CA_Tables_Column_Comments.sql
@@CA_Synonyms.sql
-- load the product static data
@@CA_StaticData.sql

exit

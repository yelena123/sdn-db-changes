set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DOCUMENT_EVENT_TYPE

INSERT INTO DOCUMENT_EVENT_TYPE ( DOCUMENT_EVENT_TYPE,DESCRIPTION) 
VALUES  ( 32,'BOOK_UPDATE');

INSERT INTO DOCUMENT_EVENT_TYPE ( DOCUMENT_EVENT_TYPE,DESCRIPTION) 
VALUES  ( 40,'LETTER_OF_INSTRUCTION');

INSERT INTO DOCUMENT_EVENT_TYPE ( DOCUMENT_EVENT_TYPE,DESCRIPTION) 
VALUES  ( 48,'SHIP_PENDING');

INSERT INTO DOCUMENT_EVENT_TYPE ( DOCUMENT_EVENT_TYPE,DESCRIPTION) 
VALUES  ( 16,'SHIP_UPDATE');

INSERT INTO DOCUMENT_EVENT_TYPE ( DOCUMENT_EVENT_TYPE,DESCRIPTION) 
VALUES  ( 24,'BOOK_CONFIRM');

INSERT INTO DOCUMENT_EVENT_TYPE ( DOCUMENT_EVENT_TYPE,DESCRIPTION) 
VALUES  ( 28,'SHIP_AVAILABLE');

INSERT INTO DOCUMENT_EVENT_TYPE ( DOCUMENT_EVENT_TYPE,DESCRIPTION) 
VALUES  ( 36,'PO_CREATE');

Commit;





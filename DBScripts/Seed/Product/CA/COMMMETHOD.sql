set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for COMMMETHOD

INSERT INTO COMMMETHOD ( COMMMETHODID,DESCRIPTION) 
VALUES  ( 0,'Email');

INSERT INTO COMMMETHOD ( COMMMETHODID,DESCRIPTION) 
VALUES  ( 1,'Phone');

INSERT INTO COMMMETHOD ( COMMMETHODID,DESCRIPTION) 
VALUES  ( 2,'Fax');

Commit;





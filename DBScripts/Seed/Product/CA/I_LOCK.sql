set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for I_LOCK

INSERT INTO I_LOCK ( LOCK_ID,LOCK_NAME,HOST_NAME,LAST_LOCKING_TIME,LAST_UPDATE_TIME) 
VALUES  ( 1,'Scheduler Lock','Dummy','2014-08-27 14:49:44','2014-08-27 14:49:44');

Commit;





set scan off echo on define off;

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_BOOKING_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_BOOKING_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Bookings can be archived/purged',
               'Days after which Bookings can be archived/purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_BOOKING_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_BOOKING_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of Bookings which can be archived/purged',
               'Minimum status of Bookings which can be archived/purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '12',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_CONSRUN_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_CONSRUN_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Consolidation Run can be archived/purged',
               'Days after which Consolidation Run can be archived/purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_CONSRUN_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 'ARCHIVE_CONSRUN_STATUS',
                 'ARCHIVE',
                 '',
                 'Minimum status of Consolidation Run which can be archived/purged',
                 'Minimum status of Consolidation Run which can be archived/purged',
                 '0',
                 '0',
                 '1',
                 'TEXTBOX',
                 '',
                 '0',
                 '',
                 '0',
                 'Failed',
                 '',
                 '',
                 '0',
                 '',
                 '',
                 '',
                 '',
                 SYSDATE,
                 SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_CYCLE_COUNT_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_CYCLE_COUNT_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Cycle Count data can be Purged',
               'Days after which Cycle Count data can be Purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_CYCLE_COUNT_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_CYCLE_COUNT_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of Cycle Count data which can be purged',
               'Minimum status of Cycle Count data which can be purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '30',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_APPOINTMENT_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_APPOINTMENT_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of Appointment which can be archived',
               'Minimum status of Appointment which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '9',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_APPOINTMENT_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_APPOINTMENT_WINDOW',
               'ARCHIVE',
               '',
               'Days after which appointments can be archived',
               'Days after which appointments can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_ORDER_CATEGORY' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_ORDER_CATEGORY',
               'ARCHIVE',
               '',
               'OLM Purchase Order category which can be archived/purged',
               'OLM Purchase Order category which can be archived/purged',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_ASN_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_ASN_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of ASN which can be archived',
               'Minimum status of ASN which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '40',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_ASN_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_ASN_WINDOW',
               'ARCHIVE',
               '',
               'Days after which ASN can be archived',
               'Days after which ASN can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_PURGE_FLAG' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_PURGE_FLAG',
               'ARCHIVE',
               '',
               'Flag to decide only purge or archive',
               '0 - Does Archive and Purge, 1 - Does Purge only',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '1',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_CLAIM_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_CLAIM_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of  Claim which can be archived',
               'Minimum status of  Claim which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '70',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_CLAIM_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_CLAIM_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Claims can be archived',
               'Days after which Claims can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_COMMIT_FREQUENCY' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_COMMIT_FREQUENCY',
               'ARCHIVE',
               '',
               'Number of records after which commit needs to be issued',
               'Number of records after which commit needs to be issued',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '1000',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_RMA_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_RMA_STATUS',
               'ARCHIVE',
               '',
               'RMA Status',
               'RMA Status',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '200',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_DET_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_DET_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of Detention which can be archived',
               'Minimum status of Detention which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '256',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_DET_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_DET_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Detention can be archived',
               'Days after which Detention can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_DO_STATUS' PARAM_DEF_ID, 'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_DO_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of DO  which can be archived',
               'Minimum status of DO  which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '80',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_DO_WINDOW' PARAM_DEF_ID, 'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_DO_WINDOW',
               'ARCHIVE',
               '',
               'Days after which DO can be archived',
               'Days after which DO can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_INSPECTION_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_INSPECTION_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of Inspection which can be archived',
               'Minimum status of Inspection which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '20',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_INSPECTION_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_INSPECTION_WINDOW',
               'ARCHIVE',
               '',
               'Days after which Inspection would be archived',
               'Days after which Inspection would be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_INVOICE_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_INVOICE_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of Invoice which can be archived',
               'Minimum status of Invoice which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '40',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_INVOICE_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_INVOICE_WINDOW',
               'ARCHIVE',
               '',
               'Days after which invoices can be archived',
               'Days after which invoices can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_LPN_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_LPN_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of LPN which can be archived',
               'Minimum status of LPN which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '70',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_LPN_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_LPN_WINDOW',
               'ARCHIVE',
               '',
               'Days after which LPN would be archived',
               'Days after which LPN would be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_MANI_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_MANI_STATUS',
               'ARCHIVE',
               '',
               'Manifest status which can be picked up for archiving',
               'Manifest status which can be picked up for archiving',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_MANI_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_MANI_WINDOW',
               'ARCHIVE',
               '',
               'Days after which manifest can be archived',
               'Days after which manifest can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_PO_STATUS' PARAM_DEF_ID, 'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_PO_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of PO which can be archived',
               'Minimum status of PO which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '950',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_PO_WINDOW' PARAM_DEF_ID, 'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_PO_WINDOW',
               'ARCHIVE',
               '',
               'Days after which PO can be archived',
               'Days after which PO can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_RATING_ROUTING_LANES' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_RATING_ROUTING_LANES',
               'ARCHIVE',
               '',
               'Whether Rating and routing would need to be archived',
               'Whether Rating and routing would need to be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '1',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_RATING_ROUTING_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_RATING_ROUTING_WINDOW',
               'ARCHIVE',
               '',
               'Days after which rating and routing would be archived',
               'Days after which rating and routing would be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_RTS_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_RTS_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of RTS which can be archived',
               'Minimum status of RTS which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '30',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_RTS_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_RTS_WINDOW',
               'ARCHIVE',
               '',
               'Days after which RTS can be archived',
               'Days after which RTS can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_SHIPMENT_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_SHIPMENT_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of Shipment which can be archived',
               'Minimum status of Shipment which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '95',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_SHIPMENT_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_SHIPMENT_WINDOW',
               'ARCHIVE',
               '',
               'Days of shipment archival days',
               'Days of shipment archival days',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_TRIP_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_TRIP_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of TRIP which can be archived',
               'Minimum status of TRIP which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '12',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_TRIP_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_TRIP_WINDOW',
               'ARCHIVE',
               '',
               'Days after which trips can be archived',
               'Days after which trips can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_EEM_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_EEM_WINDOW',
               'ARCHIVE',
               '',
               'Days after which EEM Objects can be archived',
               'Days after which EEM Objects can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_TCS_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_TCS_WINDOW',
               'ARCHIVE',
               '',
               'Days after which TCS Data can be archived',
               'Days after which TCS Data can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'NO_STATUS_REQ_FOR_LAST_n_DAYS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 'NO_STATUS_REQ_FOR_LAST_n_DAYS',
                 'ARCHIVE',
                 '',
                 'Number of days for which no status check is required',
                 'This parameter will be only considered if parameter NO_STATUS_CHECK_FLAG is enabled.',
                 '0',
                 '0',
                 '1',
                 'TEXTBOX',
                 '',
                 '0',
                 '',
                 '0',
                 '365',
                 '',
                 '',
                 '0',
                 '',
                 '',
                 '',
                 '',
                 SYSDATE,
                 SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_POS_TRANS_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_POS_TRANS_WINDOW',
               'ARCHIVE',
               '',
               'Days after which POS Transaction data can be archived',
               'Days after which POS Transaction data can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '90',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_PAYMENT_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_PAYMENT_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of Payment which can be archived',
               'Minimum status of Payment which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '30',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_PAYMENT_TRANS_STATUS' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('ARCHIVE_PAYMENT_TRANS_STATUS',
               'ARCHIVE',
               '',
               'Minimum status of Payment Transaction which can be archived',
               'Minimum status of Payment Transaction which can be archived',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '',
               '0',
               '',
               '0',
               '2',
               '',
               '',
               '0',
               '',
               '',
               '',
               '',
               SYSDATE,
               SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'NO_STATUS_CHECK_FLAG' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 'NO_STATUS_CHECK_FLAG',
                 'ARCHIVE',
                 '',
                 'Default is not enabled. If this parameter is enabled then only NO_STATUS_REQ_FOR_LAST_n_DAYS will be considered',
                 'Default is not enabled. If this parameter is enabled then only NO_STATUS_REQ_FOR_LAST_n_DAYS will be considered',
                 '0',
                 '0',
                 '1',
                 'TEXTBOX',
                 '',
                 '0',
                 '',
                 '0',
                 '0',
                 '',
                 '',
                 '0',
                 '',
                 '',
                 '',
                 '',
                 SYSDATE,
                 SYSDATE);

MERGE INTO PARAM_DEF L
     USING (SELECT 'ARCHIVE_CARRIER_FORECAST_WINDOW' PARAM_DEF_ID,
                   'ARCHIVE' PARAM_GROUP_ID
              FROM DUAL) B
        ON (    L.PARAM_DEF_ID = B.PARAM_DEF_ID
            AND L.PARAM_GROUP_ID = B.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               MIN_VALUE,
               IS_MIN_INC,
               MAX_VALUE,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE,
               PARAM_CATEGORY,
               CARR_DFLT_VALUE,
               BP_DFLT_VALUE,
               PARAM_VALIDATOR_CLASS,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (
                 'ARCHIVE_CARRIER_FORECAST_WINDOW',
                 'ARCHIVE',
                 '',
                 'Days after which CARRIER FORECAST Run can be archived/purged',
                 'Days after which CARRIER FORECAST Run can be archived/purged',
                 '0',
                 '0',
                 '1',
                 'TEXTBOX',
                 '',
                 '0',
                 '',
                 '0',
                 '90',
                 '',
                 '',
                 '0',
                 '',
                 '',
                 '',
                 '',
                 SYSDATE,
                 SYSDATE);
COMMIT;

-- Filter Layout Entries
exec sequpdt('FILTER_LAYOUT','FILTER_LAYOUT_ID','SEQ_FILTER_LAYOUT_ID');

DELETE FROM FILTER_LAYOUT WHERE object_type IN ('ARCHIVED RG LANE','ARCHIVED RATING LANE');

COMMIT;

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 0, 1, 'DETENTION_REQUEST.DETENTION_ID', 'Detention ID', '=', 'LONG', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 3, 1, '(SELECT TC_PURCHASE_ORDERS_ID FROM PURCHASE_ORDERS WHERE PURCHASE_ORDERS_ID = DETENTION_REQUEST.ORDER_ID)', 'PO Nbr.', '=', 'UPPERCASE_STRING', NULL, '/cbo/transactional/lookup/idLookup.jsp?lookupType=Purchase Order&permission_code=VPO', 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 4, 1, ' (SELECT CARRIER_CODE FROM CARRIER_CODE WHERE CARRIER_ID = DETENTION_REQUEST.CARRIER_ID)', 'Carrier Code', '=,!=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Carrier&permission_code=VDTN', 'COMP', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 5, 0, 'CARRIER_CODE.TP_COMPANY_NAME', 'Carrier Name', 'FR', 'UPPERCASE_STRING', NULL, NULL, 'FROM', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'CARRIER_CODE', 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 6, 1, 'DETENTION_REQUEST.CARRIER_APPOINTMENT_TIME', 'Carrier Appointment Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 7, 1, 'DETENTION_REQUEST.CARRIER_ARRIVAL_TIME', 'Carrier Arrival Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 8, 1, 'DETENTION_REQUEST.CARRIER_COMPLETION_TIME', 'Carrier Completion Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 0, 0, 'DETENTION_EVENT.FIELD_NAME', 'Field', '=', 'STRING', NULL, '/ofr/shipserv/jsp/AuditTrailFieldSelection.jsp?objectType=detentionRequest', 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH_DETN_AUDIT_EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 3, 0, 'DETENTION_EVENT.CREATED_SOURCE', 'Source', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH_DETN_AUDIT_EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 4, 0, 'DETENTION_EVENT.CREATED_DTTM', 'Date/Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH_DETN_AUDIT_EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 2, 0, 'DETENTION_EVENT.NEW_VALUE', 'New Value', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH_DETN_AUDIT_EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 0, 'DETENTION_EVENT.OLD_VALUE', 'Old Value', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH_DETN_AUDIT_EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 0, 'RATING_EVENT.OLD_VALUE', 'Old Value', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'r.oldValue', NULL, NULL, NULL, NULL, NULL, 'ARCH RATING RATE ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 0, 0, 'RATING_EVENT.FIELD_NAME', 'Field', '=', 'UPPERCASE_STRING', NULL, '/ofr/shipserv/jsp/AuditTrailFieldSelection.jsp?objectType=ratingLaneRate', 'COMP', NULL, NULL, NULL, 0, 'r.fieldName', NULL, NULL, NULL, NULL, NULL, 'ARCH RATING RATE ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 2, 0, 'RATING_EVENT.NEW_VALUE', 'New Value', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'r.newValue', NULL, NULL, NULL, NULL, NULL, 'ARCH RATING RATE ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 3, 0, 'RATING_EVENT.LAST_UPDATED_SRC', 'Source', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'r.lastUpdatedSrc', NULL, NULL, NULL, NULL, NULL, 'ARCH RATING RATE ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 4, 0, 'RATING_EVENT.LAST_UPDATED_DTTM', 'Date/Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, 'r.lastUpdatedDttm', NULL, NULL, NULL, NULL, NULL, 'ARCH RATING RATE ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 0, 'RATING_EVENT.OLD_VALUE', 'Old Value', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'r.oldValue', NULL, 4, NULL, NULL, NULL, 'ARCH RATE ACCRL ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 2, 0, 'RATING_EVENT.NEW_VALUE', 'New Value', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'r.newValue', NULL, 4, NULL, NULL, NULL, 'ARCH RATE ACCRL ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 3, 0, 'RATING_EVENT.LAST_UPDATED_SRC', 'Source', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'r.lastUpdatedSrc', NULL, 4, NULL, NULL, NULL, 'ARCH RATE ACCRL ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 0, 0, 'RATING_EVENT.FIELD_NAME', 'Field', '=', 'UPPERCASE_STRING', NULL, '/ofr/shipserv/jsp/AuditTrailFieldSelection.jsp?objectType=ratingLaneAccrl', 'COMP', NULL, NULL, NULL, 0, 'r.fieldName', NULL, 4, NULL, NULL, NULL, 'ARCH RATE ACCRL ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 4, 0, 'RATING_EVENT.LAST_UPDATED_DTTM', 'Date/Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, 'r.lastUpdatedDttm', NULL, 4, NULL, NULL, NULL, 'ARCH RATE ACCRL ADT EVT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 36, 0, 'CLAIMS_VIEW.MERCHANDISE_INVOICE_ID', 'Supplier Invoice ID', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'c.merchandiseInvoiceId', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 30, 0, 'CLAIMS_VIEW.CLAIM_ACTION_CODE', 'Reason Code', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'c.claimActionCode', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 26, 0, 'CLAIM_JOURNAL_ENTRIES.CREDIT_ACCOUNT_SUFFIX', 'Credit Account Suffix', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'claimJournalEntry.creditAccountSuffix', NULL, NULL, NULL, NULL, 'CLAIM_JOURNAL_ENTRIES', 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 37, 0, 'CLAIMS_VIEW.TC_COMPANY_ID', 'Business Unit', '=', 'NUMBER', 'com.manh.tpe.filter.BusinessUnitClaimFilter', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 24, 0, 'CLAIM_JOURNAL_ENTRIES.CREDIT_ACCOUNT_PREFIX', 'Credit Account Prefix', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'claimJournalEntry.creditAccountPrefix', NULL, NULL, NULL, NULL, 'CLAIM_JOURNAL_ENTRIES', 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 25, 0, 'CLAIM_JOURNAL_ENTRIES.CREDIT_ACCOUNT_BODY', 'Credit Account Body', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'claimJournalEntry.creditAccountBody', NULL, NULL, NULL, NULL, 'CLAIM_JOURNAL_ENTRIES', 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 27, 0, 'CLAIM_JOURNAL_ENTRIES.DEBIT_ACCOUNT_PREFIX', 'Debit Account Prefix', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'claimJournalEntry.debitAccountPrefix', NULL, NULL, NULL, NULL, 'CLAIM_JOURNAL_ENTRIES', 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 28, 0, 'CLAIM_JOURNAL_ENTRIES.DEBIT_ACCOUNT_BODY', 'Debit Account Body', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'claimJournalEntry.debitAccountBody', NULL, NULL, NULL, NULL, 'CLAIM_JOURNAL_ENTRIES', 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 29, 0, 'CLAIM_JOURNAL_ENTRIES.DEBIT_ACCOUNT_SUFFIX', 'Debit Account Suffix', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'claimJournalEntry.debitAccountSuffix', NULL, NULL, NULL, NULL, 'CLAIM_JOURNAL_ENTRIES', 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 4, 0, 'CLAIMS_VIEW.CLAIM_CATERY', 'Catery', '=', 'NUMBER', 'com.manh.common.fv.ClaimCatery', NULL, 'SELECT', NULL, NULL, NULL, 1, 'c.claimCatery', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 5, 0, 'CLAIMS_VIEW.CLAIM_TYPE', 'Type', '=', 'NUMBER', 'com.manh.common.fv.ClaimType', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'c.claimTypeShort', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 15, 0, 'CLAIMS_VIEW.DAYS_ASSIGNED', 'Days Assigned', '=,!=,>,<,>=,<=', 'NUMBER', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'c.daysAssignedInteger', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 16, 0, 'CLAIMS_VIEW.FREIGHT_INVOICE_ID', 'Freight Invoice ID', '=', 'UPPERCASE_STRING', NULL, '/ofr/lookup/jsp/idLookup.jsp?lookupType=Freight Invoice ID&permission_code=VCLM', 'COMP', NULL, NULL, NULL, 1, 'c.freightInvoiceId', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 17, 0, 'CLAIMS_VIEW.TC_SHIPMENT_ID', 'Shipment ID', '=', 'STRING', NULL, '/cbo/transactional/lookup/idLookup.jsp?lookupType=Shipment&permission_code=VCLM', 'COMP', NULL, NULL, NULL, 1, 'c.tcShipmentId', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 18, 0, 'CLAIMS_VIEW.PURCHASE_ORDER', 'Purchase Order', '=', 'STRING', NULL, '/cbo/transactional/lookup/idLookup.jsp?lookupType=Purchase Order&permission_code=VCLM', 'COMP', NULL, NULL, NULL, 1, 'c.purchaseOrder', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 19, 0, 'CLAIM_ITEMS.PO_TYPE', 'PO Type', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'claimItem.poType', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 2, 0, 'CLAIMS_VIEW.CARRIER_CLAIM_ID', 'Carrier Claim ID', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'c.carrierClaimId', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 11, 0, 'CLAIMS_VIEW.TOTAL_CLAIM_AMOUNT', 'Claim Amt.', 'FR', 'DOUBLE', NULL, NULL, 'FROM', NULL, NULL, NULL, 1, 'c.totalClaimAmountDouble', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 13, 0, 'CLAIMS_VIEW.O_FACILITY_ALIAS_ID', 'Origin Facility', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VCLM', 'COMP', NULL, NULL, NULL, 1, 'c.originFacilityAliasId', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 14, 0, 'CLAIMS_VIEW.D_FACILITY_ALIAS_ID', 'Destination Facility', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VCLM', 'COMP', NULL, NULL, NULL, 1, 'c.destinationFacilityAliasId', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 7, 0, '(SELECT CARRIER_CODE FROM CARRIER_CODE WHERE CARRIER_ID = CLAIMS_VIEW.CARRIER_ID)', 'Carrier Code', '=,!=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Carrier&permission_code=VCLM', 'COMP', NULL, NULL, NULL, 1, 'c.carrierCode', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 8, 0, 'CARRIER_CODE.TP_COMPANY_NAME', 'Carrier Name', 'FR', 'UPPERCASE_STRING', NULL, NULL, 'FROM', NULL, NULL, NULL, 1, 'carrier.tpCompanyName', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 10, 0, 'CLAIMS_VIEW.CLAIM_DATE', 'Claim Date', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, 'c.claimDate', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 23, 0, 'CLAIMS_VIEW.TRACKING_NO', 'Tracking Nbr.', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'c.trackingNumber', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 0, 'CLAIMS_VIEW.TC_CLAIM_ID', 'Claim ID', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'c.tcClaimId', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 8, 0, 'RG_LANE.O_POSTAL_CODE', 'Origin Postal Code', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 0, 0, 'RG_LANE.TC_COMPANY_ID', 'Business Unit', '=', 'STRING', 'com.manh.common.ui.filter.BusinessUnitValueGenerator', NULL, 'MULTIPLE_STATE_COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 0, 'RG_LANE.SEARCH_TYPE', 'Search Type', '=', 'STRING', 'com.logistics.javalib.filter.layout.SearchTypeValueGenerator', NULL, 'SELECT', NULL, 'RG_LANE', 'Exact Lane', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 2, 0, 'RG_LANE.LANE_ID', 'Lane ID', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 4, 0, 'RG_LANE.KEY_DT', 'Date', '=', 'DATE', NULL, NULL, 'DATEONLY_COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 5, 0, 'RG_LANE.O_FACILITY_ALIAS_ID', 'Origin Facility', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VRGD', 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 6, 0, 'RG_LANE.O_CITY', 'Origin City', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 7, 0, 'RG_LANE.O_STATE_PROV', 'Origin State', '=', 'STRING', 'com.manh.common.ui.filter.StateProvValueGenerator', NULL, 'STATE_COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 9, 0, 'RG_LANE.O_COUNTY', 'Origin County', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 10, 0, 'RG_LANE.O_COUNTRY_CODE', 'Origin Country', '=', 'STRING', 'com.manh.common.fv.Country', NULL, 'COUNTRY_COMP', NULL, 'RG_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 11, 0, 'RG_LANE.O_ZONE_ID', 'Origin Zone', '=', 'STRING', 'com.manh.common.fv.ZoneId', NULL, 'SELECT', NULL, 'RG_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 12, 0, 'RG_LANE.D_FACILITY_ALIAS_ID', 'Destination Facility', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VRGD', 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 13, 0, 'RG_LANE.D_CITY', 'Destination City', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 14, 0, 'RG_LANE.D_STATE_PROV', 'Destination State', '=', 'STRING', 'com.manh.common.ui.filter.StateProvValueGenerator', NULL, 'STATE_COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 15, 0, 'RG_LANE.D_POSTAL_CODE', 'Destination Postal Code', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 16, 0, 'RG_LANE.D_COUNTY', 'Destination County', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 17, 0, 'RG_LANE.D_COUNTRY_CODE', 'Destination Country', '=', 'STRING', 'com.manh.common.fv.Country', NULL, 'COUNTRY_COMP', NULL, 'RG_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 18, 0, 'RG_LANE.D_ZONE_ID', 'Destination Zone', '=', 'STRING', 'com.manh.common.fv.ZoneId', NULL, 'SELECT', NULL, 'RG_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 19, 0, 'RG_LANE.PROTECTION_LEVEL', 'Protection Level', '=', 'NUMBER', 'com.manh.common.fv.ProtectionLevel', NULL, 'SELECT', NULL, 'RG_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 20, 0, '(SELECT CARRIER_CODE FROM CARRIER_CODE WHERE CARRIER_ID = RG_LANE_DTL.CARRIER_ID)', 'Carrier', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Carrier&permission_code=VRGD', 'COMP', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 21, 0, 'RG_LANE_DTL.SERVICE_LEVEL', 'Service Level', '=', 'NUMBER', 'com.manh.common.fv.ServiceLevelFiniteValue', NULL, 'SELECT', NULL, 'RG_LANE_DETAIL', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 22, 0, 'RG_LANE_DTL.EQUIPMENT_ID', 'Equipment', '=', 'NUMBER', 'com.manh.common.fv.EquipmentFiniteValue', NULL, 'SELECT', NULL, 'RG_LANE_DETAIL', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 23, 0, 'RG_LANE_DTL.MOT', 'Mode', '=', 'NUMBER', 'com.manh.common.fv.Mode', NULL, 'SELECT', NULL, 'RG_LANE_DETAIL', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 24, 0, 'RG_LANE.HAS_ERRORS', 'Error Type', '=', 'STRING', 'com.manh.common.fv.ErrorTypeFiniteValue', NULL, 'SELECT', NULL, 'RG_LANE', 'Valid Lanes', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 26, 0, 'RG_LANE_DTL.PACKAGE_NAME', 'Package Name', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 3, 0, 'RG_LANE.LANE_NAME', 'Lane Name', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 25, 0, 'RG_LANE.CUSTOMER_ID', 'Customer', '=', 'STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=CustomerName&permission_code=VRGD', 'COMP', NULL, 'RG_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 27, 0, 'RG_LANE.BILLING_METHOD', 'Billing Method', '=', 'NUMBER', 'com.manh.cbo.transactional.domain.fv.BillingMethod', NULL, 'SELECT', NULL, 'RG_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 28, 0, 'RG_LANE.INCOTERM_ID', 'Incoterm Id', '=', 'NUMBER', 'com.manh.common.fv.IncotermFiniteValue', NULL, 'SELECT', NULL, 'RG_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 29, 0, 'RG_LANE.ROUTE_TO', 'Route To', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 30, 0, 'RG_LANE.ROUTE_TYPE_1', 'Route Type 1', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 31, 0, 'RG_LANE.ROUTE_TYPE_2', 'Route Type 2', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RG_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 32, 0, 'RG_LANE_DTL.SP_MIN_LPN_COUNT', 'LPN Count', '>=,<=,=', 'INTEGER', NULL, NULL, 'COMP', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 33, 0, 'RG_LANE_DTL.SP_MIN_WEIGHT', 'Weight', '>=,<=,=', 'INTEGER', NULL, NULL, 'COMP', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 34, 0, 'RG_LANE_DTL.SP_MIN_VOLUME', 'Volume', '>=,<=,=', 'INTEGER', NULL, NULL, 'COMP', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 35, 0, 'RG_LANE_DTL.SP_MIN_LINEAR_FEET', 'Length', '>=,<=,=', 'INTEGER', NULL, NULL, 'COMP', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 36, 0, 'RG_LANE_DTL.SP_MIN_MONETARY_VALUE', 'Monetary Value', '>=,<=,=', 'INTEGER', NULL, NULL, 'COMP', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 37, 0, 'RG_LANE.NO_RATING', 'No Rating', '=,!=', 'INTEGER', 'com.manh.common.fv.BooleanStatus', NULL, 'SELECT', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 38, 0, 'RG_LANE.IS_FASTEST', 'Fastest', '=,!=', 'INTEGER', 'com.manh.common.fv.BooleanStatus', NULL, 'SELECT', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 39, 0, 'RG_LANE.IS_USE_PREFERENCE_BONUS', 'Use Preference Bonus', '=,!=', 'INTEGER', 'com.manh.common.fv.BooleanStatus', NULL, 'SELECT', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 40, 0, 'RG_LANE_DTL.OVERRIDE_CODE', 'Billing Method Override', '=,!=', 'NUMBER', 'com.manh.cbo.transactional.domain.fv.BillingMethod', NULL, 'SELECT', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 41, 0, 'RG_LANE_DTL.CUBING_INDICATOR', 'Cubing Factor', '=,!=', 'NUMBER', 'com.manh.cbo.syscode.finitevalue.CubingFV', NULL, 'SELECT', NULL, 'RG_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RG LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 3, 0, 'CLAIMS_VIEW.CLAIM_CLASSIFICATION', 'Group', '=', 'NUMBER', 'com.manh.common.fv.ClaimClassification', NULL, 'SELECT', NULL, NULL, NULL, 1, 'c.claimClassification', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 6, 0, 'CLAIMS_VIEW.ROLE', 'Role', '=', 'UPPERCASE_STRING', 'com.manh.tpe.core.domain.finitevalue.ClaimRoleType', NULL, 'SELECT', NULL, NULL, NULL, 1, 'c.role', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 0, 0, 'CLAIMS_VIEW.CLAIM_STATUS', 'Claim Status', '=,!=', 'NUMBER', 'com.manh.tpe.core.domain.finitevalue.ClaimStatus', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'c.claimStatus', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 20, 0, 'CLAIM_ITEMS.PO_DEPT', 'PO Dept', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'claimItem.poDept', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 4, 0, 'RATING_LANE.O_FACILITY_ALIAS_ID', 'Origin Facility', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility', 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 5, 0, 'RATING_LANE.O_CITY', 'Origin City', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 6, 0, 'RATING_LANE.O_STATE_PROV', 'Origin State', '=', 'STRING', 'com.manh.common.ui.filter.StateProvValueGenerator', NULL, 'STATE_COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 7, 0, 'RATING_LANE.O_POSTAL_CODE', 'Origin Postal Code', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 28, 0, 'RATING_LANE_DTL.PACKAGE_NAME', 'Package Name', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RATING_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 10, 0, 'RATING_LANE.O_ZONE_ID', 'Origin Zone', '=', 'STRING', 'com.manh.common.fv.ZoneId', NULL, 'SELECT', NULL, 'RATING_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 11, 0, 'RATING_LANE.D_FACILITY_ALIAS_ID', 'Destination Facility', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility', 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 16, 0, 'RATING_LANE.D_COUNTRY_CODE', 'Destination Country', '=', 'STRING', 'com.manh.common.fv.Country', NULL, 'COUNTRY_COMP', NULL, 'RATING_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 17, 0, 'RATING_LANE.D_ZONE_ID', 'Destination Zone', '=', 'STRING', 'com.manh.common.fv.ZoneId', NULL, 'SELECT', NULL, 'RATING_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 19, 0, 'RATING_LANE_DTL.PROTECTION_LEVEL_ID', 'Protection Level', '=', 'NUMBER', 'com.manh.common.fv.ProtectionLevel', NULL, 'SELECT', NULL, 'RATING_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 20, 0, '(SELECT CARRIER_CODE FROM CARRIER_CODE WHERE CARRIER_ID = RATING_LANE_DTL.CARRIER_ID)', 'Carrier', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Carrier&permission_code=VCR', 'COMP', NULL, 'RATING_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 21, 0, 'RATING_LANE_DTL.SERVICE_LEVEL_ID', 'Service Level', '=', 'NUMBER', 'com.manh.common.fv.ServiceLevelFiniteValue', NULL, 'SELECT', NULL, 'RATING_LANE_DETAIL', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 22, 0, 'RATING_LANE_DTL.EQUIPMENT_ID', 'Equipment', '=', 'NUMBER', 'com.manh.common.fv.EquipmentFiniteValue', NULL, 'SELECT', NULL, 'RATING_LANE_DETAIL', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 23, 0, 'RATING_LANE_DTL.MOT_ID', 'Mode', '=', 'NUMBER', 'com.manh.common.fv.Mode', NULL, 'SELECT', NULL, 'RATING_LANE_DETAIL', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 12, 0, 'RATING_LANE.D_CITY', 'Destination City', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 13, 0, 'RATING_LANE.D_STATE_PROV', 'Destination State', '=', 'STRING', 'com.manh.common.ui.filter.StateProvValueGenerator', NULL, 'STATE_COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 14, 0, 'RATING_LANE.D_POSTAL_CODE', 'Destination Postal Code', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 15, 0, 'RATING_LANE.D_COUNTY', 'Destination County', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 2, 0, 'RATING_LANE.LANE_ID', 'Lane ID', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 3, 0, 'RATING_LANE.KEY_DT', 'Date', '=', 'DATE', NULL, NULL, 'DATEONLY_COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 0, 'RATING_LANE.SEARCH_TYPE', 'Search Type', '=', 'STRING', 'com.logistics.javalib.filter.layout.SearchTypeValueGenerator', NULL, 'SELECT', NULL, 'RATING_LANE', 'Exact Lane', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 8, 0, 'RATING_LANE.O_COUNTY', 'Origin County', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 9, 0, 'RATING_LANE.O_COUNTRY_CODE', 'Origin Country', '=', 'STRING', 'com.manh.common.fv.Country', NULL, 'COUNTRY_COMP', NULL, 'RATING_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 24, 0, 'RATING_LANE.HAS_ERRORS', 'Error Type', '=', 'STRING', 'com.manh.common.fv.ErrorTypeFiniteValue', NULL, 'SELECT', NULL, 'RATING_LANE', 'Valid Lanes', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 26, 0, 'RATING_LANE_DTL.D_SHIP_VIA', 'Destination Ship Through', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/facilityIdLookup.jsp?lookupType=Facility&&permission_code=VBD&&facilityTypeString=PT', 'COMP', NULL, 'RATING_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 27, 0, 'RATING_LANE_DTL.CONTRACT_NUMBER', 'Contract Number', '=', 'STRING', NULL, NULL, 'COMP', NULL, 'RATING_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 25, 0, 'RATING_LANE_DTL.O_SHIP_VIA', 'Origin Ship Through', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/facilityIdLookup.jsp?lookupType=Facility&&permission_code=VBD&&facilityTypeString=PT', 'COMP', NULL, 'RATING_LANE_DTL', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 0, 0, 'RATING_LANE.TC_COMPANY_ID', 'Business Unit', '=', 'STRING', 'com.manh.common.ui.filter.BusinessUnitValueGenerator', NULL, 'MULTIPLE_STATE_COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 30, 0, 'RATING_LANE.BILLING_METHOD', 'Billing Method', '=', 'NUMBER', 'com.manh.cbo.transactional.domain.fv.BillingMethod', NULL, 'SELECT', NULL, 'RATING_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 31, 0, 'RATING_LANE.INCOTERM_ID', 'Incoterm Id', '=', 'NUMBER', 'com.manh.common.fv.IncotermFiniteValue', NULL, 'SELECT', NULL, 'RATING_LANE', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 32, 0, 'RATING_LANE.CUSTOMER_ID', 'Customer', '=', 'STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=CustomerName&permission_code=VRGD', 'COMP', NULL, 'RATING_LANE', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED RATING LANE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 9, 1, 'DETENTION_REQUEST.VENDOR_APPOINTMENT_TIME', 'Supplier Appointment Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 10, 1, 'DETENTION_REQUEST.VENDOR_ARRIVAL_TIME', 'Supplier Arrival Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 11, 1, 'DETENTION_REQUEST.VENDOR_COMPLETION_TIME', 'Supplier Completion Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 12, 1, 'DETENTION_REQUEST.CREATED_DTTM', 'Creation Date', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 0, 'DETENTION_REQUEST.DETENTION_STATUS', 'Detention Status', '=,!=', 'NUMBER', 'com.manh.tpe.core.domain.finitevalue.DetentionStatus', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 13, 0, 'DETENTION_REQUEST.DETENTION_TYPE', 'Detention Type', '=', 'NUMBER', 'com.manh.tpe.core.domain.finitevalue.DetentionType', NULL, 'SELECT', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 14, 0, '(SELECT REASON_CODE FROM REASON_CODE WHERE REASON_ID = DETENTION_REQUEST.REASON_ID)', 'Reason Code', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 16, 0, 'SHIPMENT.ASSIGNED_SERVICE_LEVEL_ID', 'Service Level', '=', 'NUMBER', 'com.manh.common.fv.ServiceLevelFiniteValue', NULL, 'SELECT', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 17, 0, 'DETENTION_REQUEST.VENDOR_ID', 'Vendor ID', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 18, 0, 'FACILITY_ALIAS.FACILITY_ALIAS_ID', 'Facility ID', '=', 'UPPERCASE_STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VDTN', 'COMP', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 19, 0, 'ERROR_LOG.ERROR_MSG_ID', 'Exception Code', '=', 'NUMBER', NULL, NULL, 'SELECT', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 20, 0, 'DETENTION_REQUEST.ASSIGNED_USER', 'Assign', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 2, 1, 'SHIPMENT.TC_SHIPMENT_ID', 'Shipment ID', '=', 'UPPERCASE_STRING', NULL, '/cbo/transactional/lookup/idLookup.jsp?lookupType=Shipment&permission_code=VDTN', 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 22, 0, 'DETENTION_REQUEST.IN_PROGRESS', 'In Progress', '=', 'NUMBER', 'com.manh.common.fv.YesNoFiniteValue', NULL, 'SELECT', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 21, 0, '( SELECT C.COMPANY_NAME FROM COMPANY C, BUSINESS_PARTNER B WHERE C.COMPANY_ID = B.BP_COMPANY_ID AND B.BUSINESS_PARTNER_ID = DETENTION_REQUEST.VENDOR_ID AND B.TC_COMPANY_ID = DETENTION_REQUEST.TC_COMPANY_ID )', 'Supplier Name', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 23, 0, '(select distinct reg.region_name from region reg,FAC_REGION facreg,facility fac where reg.region_id=facreg.region_id and fac.facility_id = facreg.facility_id and reg.region_id = fac.inbound_region_id and DETENTION_REQUEST.facility_id = facreg.facility_id)', 'Inbound Geo Region', '=,!=', 'STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=InboundRegion&amp;permission_code=VSH', 'COMP', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 23, 0, '(select distinct reg.region_name from region reg,FAC_REGION facreg,facility fac where reg.region_id=facreg.region_id and fac.facility_id = facreg.facility_id and reg.region_id = fac.inbound_region_id and DETENTION_REQUEST.facility_id = facreg.facility_id)', 'Inbound Geo Region', '=,!=', 'STRING', NULL, '/basedata/lookup/jsp/idLookup.jsp?lookupType=InboundRegion&amp;permission_code=VDTN', 'COMP', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 24, 0, 'DETENTION_REQUEST.TC_COMPANY_ID', 'Business Unit', '=', 'NUMBER', 'com.manh.tpe.filter.BusinessUnitValueGenerator', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCHIVED DETENTION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 0, 0, 'CLAIMS_EVENT.FIELD_NAME', 'Field', '=', 'STRING', NULL, '/ofr/shipserv/jsp/AuditTrailFieldSelection.jsp?objectType=Claim', 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH CLAIM AUDIT EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 0, 'CLAIMS_EVENT.OLD_VALUE', 'Old Value', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH CLAIM AUDIT EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 2, 0, 'CLAIMS_EVENT.NEW_VALUE', 'New Value', '=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH CLAIM AUDIT EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 3, 0, 'CLAIMS_EVENT.CREATED_SOURCE', 'Source', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH CLAIM AUDIT EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 4, 0, 'CLAIMS_EVENT.CREATED_DTTM', 'Date/Time', 'BT', 'GMT_DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ARCH CLAIM AUDIT EVENT');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 31, 0, 'CLAIMS_VIEW.PARENT_TC_CLAIM_ID', 'Parent Claim ID', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'c.parentTCClaimId', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 33, 0, 'CLAIMS_VIEW.SUPPLIER_NAME', 'Supplier Name', '=,!=', 'UPPERCASE_STRING', NULL, '/ofr/lookup/jsp/idLookup.jsp?lookupType=Supplier&permission_code=VCLM', 'COMP', NULL, NULL, NULL, 1, 'c.supplierName', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 32, 0, 'CLAIMS_VIEW.SUPPLIER_NUMBER', 'Supplier Nbr.', '=', 'NUMBER', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'c.supplierNumber', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 34, 0, 'CLAIMS_VIEW.SHIP_DATE', 'Ship Date', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, 'c.shipDate', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE)
  VALUES(SEQ_FILTER_LAYOUT_ID.NEXTVAL, 35, 0, 'CLAIMS_VIEW.RECD_DATE', 'Receive Date', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, 'c.recievedDate', NULL, NULL, NULL, NULL, NULL, 'ARCHIVED CLAIM');


COMMIT;

--- DBCR MACR00458569

DELETE FROM FILTER_LAYOUT WHERE object_type = 'ARCHINVOICE';

COMMIT;

DELETE FROM FILTER_LAYOUT WHERE object_type = 'ARCHIVE INVOICE';

COMMIT;

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 17, 0, 'INVOICED_OBJECT.SERVICE_LEVEL_ID', 'Service Level', '=,!=', 'UPPERCASE_STRING', 'com.manh.common.fv.ServiceLevelFiniteValue', 'SELECT', 1, 'invoicedObject.serviceLevelPrimaryKeyInt', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 18, 0, 'INVOICE.MARK_FOR_DELETION', 'Delete Indicator', '=,!=', 'NUMBER', 'com.manh.common.fv.IndicatorFiniteValue', 'SELECT', '0', 1, 'invoiceData.markForDeletion', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 19, 0, 'INVOICE.CHECK_NUMBER', 'Check Nbr.', '=', 'STRING', 'COMP', 1, 'invoiceData.checkNumber', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 1, 'INVOICE.PAYMENT_DUE_DT', 'Payment Due Date', 'BT', 'DATE', 'BETWEEN_DAYS_HOURS', 0, 'invoiceData.paymentDueDate', 12, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 4, 0, 'INVOICE.INVOICE_STATUS', 'Invoice Status', '=,!=', 'NUMBER', 'com.manh.tpe.core.domain.finitevalue.FilterInvoiceStatus', 'SELECT', 1, 'invoiceData.invoiceStatus.invoiceStatusCodeShort', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, SELECTION_PAGE_URL, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 5, 0, 'CARRIER_CODE.CARRIER_CODE', 'Carrier Code', '=,!=', 'UPPERCASE_STRING', '/basedata/lookup/jsp/idLookup.jsp?lookupType=Carrier&permission_code=VIN&paginReq=false', 'COMP', 0, 'carrier.carrierCode', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 8, 0, 'INVOICE.CREATED_SOURCE_TYPE', 'Source Type', '=,!=', 'NUMBER', 'com.manh.common.fv.SourceType', 'SELECT', 1, 'invoiceData.history.createdSourceDesc.sourceType.sourceTypeCodeShort', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 9, 0, 'INVOICE.INVOICE_TYPE', 'Invoice Type', '=,!=', 'NUMBER', 'com.manh.tpe.core.domain.finitevalue.FilterInvoiceType', 'SELECT', 1, 'invoiceData.invoiceType.invoiceTypeCodeShort', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, SELECTION_PAGE_URL, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, LOOKUP_ATTRIBUTE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 11, 1, 'INVOICED_OBJECT.TC_OBJECT_ID', 'Shipment Id', '=', 'STRING', '/ofr/lookup/jsp/idLookup.jsp?lookupType=Shipment&permission_code=VIN&paginReq=false', 'COMP', 0, 'invoicedObject.tcObjectId', 4, '#{cboTransFilterLookupBackingBean.getBUMap},#{cboTransFilterLookupBackingBean.getOptionConstructMap}', 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 26, 0, 'INVOICED_OBJECT.INCOTERM_ID', 'Incoterm ID', '=,!=', 'NUMBER', 'com.manh.common.fv.IncotermFiniteValue', 'SELECT', 1, 'invoicedObject.incotermID', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, SELECTION_PAGE_URL, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 7, 0, 'INVOICED_OBJECT.SEC_CARRIER_ID', 'Secondary Carrier', '=,!=', 'UPPERCASE_STRING', '/basedata/lookup/jsp/idLookup.jsp?lookupType=Carrier\&permission_code=VIN&paginReq=false', 'COMP', 0, 'secondaryCarrier.carrierCode', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 25, 0, 'INVOICE.TC_COMPANY_ID', 'Business Unit', '=', 'NUMBER', 'com.manh.tpe.filter.BusinessUnitInvoiceFilter', 'MULTIPLE_STATE_COMP', 0, 'invoiceData.shipperId.shipperId', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, SELECTION_PAGE_URL, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 16, 0, 'INVOICE.BILLING_FACILITY_NAME', 'Billing Facility ID', '=', 'UPPERCASE_STRING', '/ofr/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VIN&paginReq=false', 'COMP', 1, 'invoiceData.billingFacilityName', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 15, 0, 'INVOICE.BILLING_ACCT_NUMBER', 'Billing Account Nbr.', '=', 'UPPERCASE_STRING', 'COMP', 1, 'invoiceData.billingAccountNumber', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, SELECTION_PAGE_URL, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 14, 0, 'INVOICED_OBJECT.D_FACILITY_NAME', 'Destination Info.', '=', 'UPPERCASE_STRING', '/ofr/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VIN&paginReq=false', 'COMP', 1, 'invoicedObject.destinationFacilityAliasIdStrin!nly', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 1, 1, 'INVOICE.INVOICE_DT', 'Invoice Date', 'BT', 'DATE', 'BETWEEN_DAYS_HOURS', 0, 'invoiceData.invoiceDate', 12, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, SELECTION_PAGE_URL, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 13, 0, 'INVOICED_OBJECT.O_FACILITY_NAME', 'Origin Info.', '=', 'UPPERCASE_STRING', '/ofr/lookup/jsp/idLookup.jsp?lookupType=Facility&permission_code=VIN&paginReq=false', 'COMP', 1, 'invoicedObject.originFacilityAliasIdStrin!nly', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 6, 0, 'CARRIER_CODE.CARRIER_CODE_NAME', 'Carrier Name', 'FR', 'UPPERCASE_STRING', 'FROM', 1, 'carrier.tpCompanyName', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 22, 0, 'INVOICE_ORDER_ACCOUNT_CODE.ACCOUNT_CODE_PREFIX', 'Account Code', '=', 'STRING', 'COMP', 1, 'invoiceAllocationContainer.accountCodePrefix.stringValue', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 22, 0, 'INVOICE_ORDER_ACCOUNT_CODE.ACCOUNT_CODE_SUFFIX', 'Department', '=', 'STRING', 'COMP', 1, 'invoiceAllocationContainer.accountCodeSuffix.stringValue', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 22, 0, 'INVOICE_ORDER_ACCOUNT_CODE.ACCOUNT_CODE_BODY', 'Store', '=', 'STRING', 'COMP', 1, 'invoiceAllocationContainer.accountCodeBody.stringValue', 4, 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, SELECTION_PAGE_URL, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, LOOKUP_ATTRIBUTE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 2, 1, 'INVOICE.INVOICE_ID', 'Invoice ID', '=', 'UPPERCASE_STRING', '/ofr/lookup/jsp/idLookup.jsp?lookupType=Invoice&permission_code=VIN&paginReq=false', 'COMP', 0, 'invoiceData.tcInvoiceId.stringValue', 12, '#{cboFilterLookupBackingBean.getBUMap},#{ tpeLookupBackingBean.getOptionConstructMap}', 'ARCHINVOICE');

Insert into FILTER_LAYOUT
   (FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, SCREEN_TYPE, OBJECT_TYPE)
 Values
   (SEQ_FILTER_LAYOUT_ID.NEXTVAL, 20, 0, 'INVOICE.PAY_DATE', 'Check Date', 'BT', 'DATE', 'BETWEEN', 1, 'invoiceData.payDate', 4, 'ARCHINVOICE');
   
   
COMMIT;

set define on;  
set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ISF_ENTITY_CODE_MAP

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 11,'DNS4','9','Org Entity Ident',2);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 13,'FIRM','FR','Org Entity Ident',6);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 15,'UNLC','UN','Port Location Type',2);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 1,'CBP','ANI','Bond Holder',1);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 2,'CBP','CU','Importer',1);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 3,'EIN','24','Importer',2);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 4,'EIN','EI','Bond Holder',2);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 5,'SSN','34','Importer',3);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 6,'SSN','SY','Bond Holder',3);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 7,'TRN','7U','Filer Identification',1);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 8,'FC','FC','Filer Identification',2);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 9,'SCI','SCI','Filer Identification',3);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 10,'DUNS','1','Org Entity Ident',1);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 14,'K','K','Port Location Type',1);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 16,'UNLOCODE','UN','Port Location Type',6);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 17,'EIN','24','Org Entity Ident',4);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 18,'SSN','34','Org Entity Ident',5);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 19,'DUNS','1','Org Ent Consignee',1);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 20,'DNS4','9','Org Ent Consignee',2);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 21,'USCB','CW','Org Ent Consignee',3);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 22,'EIN','24','Org Ent Consignee',4);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 23,'SSN','34','Org Ent Consignee',5);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 24,'FIRM','FR','Org Ent Consignee',6);

INSERT INTO ISF_ENTITY_CODE_MAP ( ISF_ENTITY_CODE_MAP_ID,ENTITY_CODE_TYPE,ENTITY_CODE_IDENTIFICATION,ENTITY_TYPE,ENTITY_TYPE_PRIORITY) 
VALUES  ( 25,'ECBP','ZB','Org Ent Consignee',7);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for VOLUMEUOM

INSERT INTO VOLUMEUOM ( VOLUMEUOM,DESCRIPTION) 
VALUES  ( 0,'cft');

INSERT INTO VOLUMEUOM ( VOLUMEUOM,DESCRIPTION) 
VALUES  ( 1,'cbm');

Commit;





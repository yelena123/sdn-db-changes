set echo on  timing off scan off

delete from Xscreen_Label XL where XL.LABEL_BUNDLE_NAME in ( 'DATAFLOW', 'DS_ORD_NEXTGEN_UI', 'WORKFLOWAGENT');
commit;
delete from LABEL L where L.bundle_name in ( 'AppeasementReasonCodesFV', 'AVSErrorInfoCodes', 'AVSReasonCodes', 'AllocationLockAction', 'AllocationManagement', 'AllocationTrace', 
'AllocationType', 'AppeasementName', 'AppeasementType', 'AutomaticPromotionFV', 'CarrierCompany', 'COAppeasements', 'COAuditFieldNames', 'COAuditFieldNamesFV', 
'COAuditFieldValues', 'COPriceOverrideReasonCode', 'COReasonCode', 'COReasonCodeFV', 'COReasonCodeType', 'ChangeManagement', 'ChannelType', 'ChargeHandlingStrategy', 
'CreditCardType', 'CriticalChangeManager', 'CustomerCommunication', 'CustomerMaster', 'CustomerOrder', 'CustomerOrderAuditDetails', 'CustomerOrderLineStatus', 'CustomerOrderStatus', 
'DiscrepancyManager', 'DiscrepancyStatus', 'DiscrepancyType', 'DistributionOrderDOMStatus', 'DistributionOrderWMStatus', 'DNAFinalizerStrategy', 'DNAInventoryType', 
'DODiscrepancyType', 'DOM', 'DOMActionMessages', 'DOMAlertName', 'DOMBaseData', 'DOMDashBoard', 'DOMInStorePickUp', 'DOMInventoryType', 'DOMOrderStatus', 'DOMOrderLineStatus', 
'DOMProcessType', 'DOMRunStatus', 'DistributionOrderType', 'ExternalCustomerIDGenerationStrategy', 'FacilityLocator', 'FacilityWorkloadBalance', 'FinalizerStrategy', 
'Flow', 'FulfillStatus', 'FulfillmentMergeOptions', 'FulfillmentOrderLineStatus', 'FulfillmentOrderStatus', 'FulfillmentSequence', 'FulfillmentStrategy', 'FulfillmentTier', 
'GroupActionType', 'GroupingConstraint', 'HandlingCostOption', 'InventoryEventAction', 'InStoreOrderFilterField', 'Interfaces', 'InvSegmentInfo', 'InventorySegmentationLevel', 
'InventorySegmentationValueType', 'InventorySelection', 'InventoryState', 'InvoiceDetails', 'ItemPriceBehaviorAfterEndDate', 'NoteCategory', 'NoteTypes', 'OrderInventoryAllocationStatus', 
'OrderLinePriority', 'OrderLineStatusForWorkFlowCancellation', 'OverrideSnHChargesReasonCodesFV', 'OrderReleaseStatus', 'PaymentMethod', 'PaymentRule', 'PaymentStatus', 
'PaymentTransactionDecision', 'PaymentTransactionStatus', 'PaymentTransactionType', 'POSTransaction', 'PricingPromotionDetails', 'PrioritizationAttribute', 'PromoCategoryFV', 
'PromoTypeFV', 'PromotionDetails', 'PromotionScope', 'PromotionType', 'ReapportionmentCriteriaFiniteValue', 'ReapportionmentStrategy', 'ReleaseManagement', 'ReleaseSubType', 
'ReleaseType', 'SOFulFillStatus', 'Selling_ItemPricing', 'ShippingChargeRateBasis', 'ShippingMatrix', 'ShortAction', 'SortOrder', 'SubstitutionType', 'SupplyBalance', 
'SupplyBalanceStatus', 'SupplyType', 'SystemDefinedNoteTypes', 'SystemDefinedReasonCodes', 'TemplateManager', 'TransactionDecision', 'TransactionType', 'TransportationOrderStatus', 
'ValueTypeFiniteValue', 'ViewPaymentTransaction', 'WorkflowAdminBundle', 'WorkflowConfig', 'WMSystemType', 'YesNoFiniteValue', 'PaymentStatusFV', 'PaymentMethodFV', 'DATAFLOW', 
'DS_ORD_NEXTGEN_UI', 'ReceiptNotExpectedApprovalFV', 'WORKFLOWAGENT' );
COMMIT;
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OT', 'Others',
           'AppeasementReasonCodesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SD', 'Shipment delayed',
           'AppeasementReasonCodesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E421', 'Primary range is invalid for the street, route, or building',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E420', 'Primary range is missing',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E431', 'Urbanization needed; input is wrong or missing',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E430', 'Possible address-line matches too close to choose one',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E600', 'Marked by USPS as unsuitable for mail delivery',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E216', 'Incorrect ZIP code; cannot determine which city match to select',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E214', 'Incorrect city and ZIP or postal code',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E213', 'Incorrect city, valid state or province, and no ZIP or postal code',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E212', 'No city and incorrect ZIP or postal code',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E413', 'Possible street-name matches too close to choose one',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E412', 'Street name not found in directory',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E101', 'City, the state, or the ZIP or postal code is incorrect or missing',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E302', 'No primary address line parsed',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E505', 'Address not in USPS directories; undeliverable address',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E504', 'Overlapping ranges in ZIP+4 directory',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E503', 'ZIP not in area covered by partial ZIP+4 directory',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E429', 'Incorrect city, cannot match address',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E502', 'Input record entirely blank',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E428', 'Incorrect ZIP; cannot match address',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E501', 'Foreign address',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E427', '(Post-)Directional needed; input is wrong or missing',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E500', 'Other error',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E425', 'Suffix and directional needed; input is wrong or missing',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E423', 'Suffix needed; input is wrong or missing',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'E422', 'Predirectional needed; input is wrong or missing',
           'AVSErrorInfoCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '152', 'Error: The request was received but there was a service time-out',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '461', 'The character set used for this transaction is not supported by the address verification service',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '151', 'Error: The request was received but there was a server time-out',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '460', 'An address match cannot be found; No reason can be given',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '150', 'Error: General system failure',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '102', 'One or more fields in the request contains invalid data',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '101', 'The request is missing one or more required fields',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '100', 'Successful transaction',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '459', 'Multiple address matches were found; returned for only non-U.S. addresses',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '458', 'The address cannot be verified or corrected',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '457', 'The postal code cannot be found',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '456', 'The street name cannot be found in the specified postal code',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '703', 'The U.S. government maintains an embargo against the country associated with the IP address',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '455', 'The route service identifier cannot be found or is out of range',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '702', 'The U.S. government maintains an embargo against the country associated with the email address',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '454', 'The post office box identifier cannot be found or is out of range',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '701', 'The U.S. government maintains an embargo against the country indicated shipping address or country not in the list',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '453', 'Multiple address matches were found',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '700', 'The Customer is on list with whom trade is restricted',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '452', 'The house number or post office box identifier cannot be found on the specified street',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '451', 'The request contains insufficient the address information',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '450', 'The apartment number missing or not found',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '234', 'A problem exists with your CyberSource merchant configuration',
           'AVSReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Skip the locked SKUs', 'Skip the locked item(s)',
           'AllocationLockAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Retry after "N" seconds',
           'AllocationLockAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Skip the locked item(s)',
           'AllocationLockAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Abort allocation',
           'AllocationLockAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Abort Allocation', 'Abort allocation',
           'AllocationLockAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Retry after ''n'' secs', 'Retry after "N" seconds',
           'AllocationLockAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Go to the facility Detail page', 'Go to the Facility Detail page',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation On', 'Allocation On',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected Facility', 'Selected Facility',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Type', 'Allocation Type',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fill SKU', 'Fill SKU',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid Override Allocated Quantity', 'Invalid "Override Allocated Quantity"',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Go to the Order Detail page', 'Go to the Order Detail page',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Detail', 'Supply Detail',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Latest Release By', 'Latest Release By',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Earliest Delivery By', 'Earliest Delivery By',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Released', 'Released',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Clear All', 'Clear All',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply', 'Supply',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total Locked Quantity is greater than Available Supply on', '"Total Locked Quantity" is greater than Available Supply on',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Locked', 'Locked',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered Item', 'Ordered Item',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override Allocated Quantity cannot be negative', '"Override Allocated Quantity" cannot be negative',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Re-Allocatable', 'Re-Allocatable',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must Ship By Date', 'Must Ship By Date',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert', 'Alert',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merge', 'Merge',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation already in progress for the same SKU, Try again later', 'Allocation already in progress for the same Item.',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pickup', 'Pickup',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Allocations', 'Order Allocations',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line no', 'Line no',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Committed Delivery Date', 'Committed Delivery Date',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer ID', 'Customer ID',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Type', 'Order Type',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Date and Time', 'Allocation Date and Time',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination Facility', 'Destination Facility',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Segment', 'Inventory Segment',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Substitution Type', 'Substitution Type',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Go to the Destination Facility Detail page', 'Go to the Destination Facility Detail page',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Quantity', 'Allocated Quantity',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated By', 'Allocated By',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery', 'Delivery',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination', 'Destination',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_No', 'Line no.',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WH Facility', 'WH Facility',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reset', 'Reset',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Item', 'Allocated Item',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Committed Ship Date', 'Committed Ship Date',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override Allocated Quantity', 'Override Allocated Quantity',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order no', 'Order no',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Latest Ship By', 'Latest Ship By',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CU', 'CU',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Overridable', 'Overridable',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type', 'Supply Type',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Line Status', 'Allocation Line Status',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', '0',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SKU', 'SKU',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Error', 'Error',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total Quantity', 'Total Quantity',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reallocate', 'Reallocate',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered Quantity', 'Ordered Quantity',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert_Indicator', 'Alert Indicator',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Channel', 'Sales Channel',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must Release By Date', 'Must Release By Date',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Go to the Sku Detail page', 'Go to the SKU Detail page',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Earliest Ship By', 'Earliest Ship By',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_No', 'Order No',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected Item', 'Selected Item',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source', 'Source',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OrderAllocationList', 'Order Allocation List',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Capacity', 'Allocated Capacity',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Origin facility', 'Origin Facility',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Earliest Deliver By', 'Earliest Deliver By',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override Allocated Quantity cannot be greater than', '"Override Allocated Quantity" cannot be greater than',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination_Facility', 'Dest. Facility',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Allocation List', 'Order Allocations',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is Virtual Allocation', 'Virtual Allocation',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override Allocated Quantity should be an integer value', '"Override Allocated Quantity" should be an integer value',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Go to the Ordered Item Detail page', 'Go to the Ordered Item Detail page',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Net Needs', 'Net Needs',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Carrier_Id', 'Carrier',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Service_Level_Id', 'Service Level',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mode_Id', 'Mode',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DELIVERY', 'Delivery',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MERGE', 'Merge',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PICKUP', 'Pickup',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_Type', 'Inventory Type',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'External System', 'External System',
           'AllocationManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Allocated_Wt', 'Total allocated weight',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Result_Code', 'Result',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Log', 'Allocation Log',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_DTTM', 'Allocated on',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Handling_Cost', 'Handling Cost',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Capacity', 'Capacity',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_DTTM', 'Created DTTM',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Handling_Cost', 'Handling',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FF_Cost', 'Total cost',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Template', 'Allocation Template',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered_Qty', 'Ordered Quantity',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tier_Name', 'Tier',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Summary', 'Order Summary',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated_Weight', 'Allocated Weight',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated_Qty', 'Allocated Quantity',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OrderLine_Number', 'Line No.',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Exec_Template', 'Last executed template',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory', 'Inventory',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thread_ID', 'Thread ID',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Template_Name', 'Template Name',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered_Item', 'Ordered Item',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Allocated_Qty', 'Total allocated quantity',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility_Name', 'Facility',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Inventory_Cost', 'Inventory',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source_Exclusion', 'Exclusion',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Failure_Reason', 'Failure Reason',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduling', 'Scheduling',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_Cost', 'Inventory Cost',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Trace_Level', 'Log Level',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_FF_Cost', 'Order fulfillment cost',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Costs', 'View costs',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Log_Message', 'Description',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cartons', 'Cartons',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FF_Facility', 'Fulfillment Facility',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Strategy', 'Strategy',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Optimal_Cost', 'Cost',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Trace_ID', 'Trace ID',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Allocation_Log', 'View allocation log',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Group', 'Allocation Group',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Cost_Trace', 'Cost Optimization Results',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Number', 'Order No.',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transport_Cost', 'Transportation Cost',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Trace', 'Allocation Trace',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Name', 'Item',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment_Name', 'Segment',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cartons_Count', 'Number of cartons',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OrderLine_Status', 'Current Line Status',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility_Selection_Status', 'Status',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merge_Facility', 'Merge Facility',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Transportation_Cost', 'Transportation',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Trace_Detail_ID', 'TraceDetail ID',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Dependent lines failed',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Multiple MF defined in a group',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'Undefined container',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '4', 'Invalid container setup',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '5', 'Invalid item setup',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '6', 'Undefined handling cost',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '7', 'Undefined rating lane',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '8', 'Undefined rate/ unit',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '9', 'Merge facility is non-operational',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Cost', 'Total Cost',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Status', 'Line status',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_no', 'Order no.',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OrderLine_No', 'Line no.',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Results', 'Results',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Trace_Detail', 'Allocation Trace Detail',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '11', 'Substitution group fail to allocate',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated_Item', 'Allocated Item',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '12', 'Failed to allocate completely',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Rating lane between two facilities does not have rate per unit defined.',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '13', 'Merge facility on tier source excluded',
           'AllocationTrace')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merge', 'Merge',
           'AllocationType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery', 'Delivery',
           'AllocationType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pickup', 'Pickup',
           'AllocationType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '$Off', '$ Off',
           'AppeasementName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '%Off', '% Off',
           'AppeasementName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flat', 'Flat',
           'AppeasementType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '$Off', '$ Off',
           'AppeasementType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '%Off', '% Off',
           'AppeasementType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lowest', 'Lowest',
           'AutomaticPromotionFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Highest', 'Highest',
           'AutomaticPromotionFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Lowest',
           'AutomaticPromotionFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'highest', 'Highest',
           'AutomaticPromotionFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '9', 'Highest',
           'AutomaticPromotionFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Lowest', 'Lowest',
           'AutomaticPromotionFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fedex', 'Fedex',
           'CarrierCompany')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'United Parcel Service', 'United Parcel Service',
           'CarrierCompany')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Appeasement', 'No Appeasement Found',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasement_Type', 'Type',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Appeasement', 'Enter Appeasement',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'System_Defined', 'System Defined',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasements', 'Appeasements',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasement', 'Appeasement',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Detail_Tab', 'Details',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasement_List_Panel', 'Appeasement Left Panel',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mark_For_Deletion', 'Inactive',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Appeasement_Description', 'Enter Appeasement Description',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Appeasement_Value', 'Enter Appeasement Value',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasement_Right_Panel', 'Appeasement Right Panel',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasement_Value', 'Value',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Active', 'Active',
           'COAppeasements')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '109', 'Bill To Middle Name',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '108', 'Bill To First Name',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '107', 'Reference 2',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '106', 'Reference 1',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '105', 'Is Suspended',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '104', 'Charge Sequence',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '103', 'Currency Code',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '102', 'Name on Card',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '101', 'Security Code',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '100', 'Card Expiry Year',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM CARRIER CODE', 'Line Item Carrier Code',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM UNIT COST', 'Line Item Unit Cost',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DESIGNATED SERVICE LEVEL', 'Designated Service Level',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LAST UPDATED DATETIME', 'Last Updated Datetime',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM STATUS', 'Line Item Status',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Line Item Created', 'New Line Item Created',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM TOTAL ORDERED', 'Line Item Total Ordered',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM RELEASED QUANTITY', 'Line Item Released Quantity',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CANCEL FLAG', 'Cancel Flag',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DESIGNATED MODE', 'Designated Mode',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '99', 'Card Expiry Month',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '98', 'Card Display Number',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '97', 'Card Number',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '96', 'Card Type',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '95', 'Req Refund Amount',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '94', 'Req Settlement Amount',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '93', 'Req Authorization Amount',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '92', 'Master Card',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '91', 'Discover',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '90', 'Amex',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '89', 'Diners Club',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '88', 'JCB',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '87', 'Visa',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '86', 'Credit Card',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '85', '3rd Party Billing',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '84', 'Paper Check',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '83', 'Electronic Bank Transfer',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '82', 'Others',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DESTINATION FACILITY', 'Destination Facility',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '81', 'Gift Card',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '80', 'Payment',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM DESCRIPTION', 'Line Item Description',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM SERVICE LEVEL', 'Line Item Service Level',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '79', 'Tax Amount',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '78', 'Shipping',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '77', 'Base Price',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '76', 'Charge',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '75', 'District',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '74', 'City',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '73', 'Charge Amount',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '72', 'State',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '71', 'County',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '70', 'Tax',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM REQUESTED QUANTITY', 'Line Item Requested Quantity',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM MODE', 'Line Item Mode',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '69', 'Deleted',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PURCHASE ORDERS CATEGORY', 'Purchase Orders Category',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '68', 'Discount Amount',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM DESTINATION FACILITY ALIAS ID', 'Line Item Destination Facility Alias ID',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '67', 'Promotion',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '66', 'Others',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '65', 'Appeasement',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '64', 'Discount',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '63', 'Created',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM STORE FACILITY ALIAS ID', 'Line Item Store Facility Alias ID',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DESTINATION ADDRESS', 'Destination Address',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO HOLD', 'PO Hold',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '56', 'Must Release by Date',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '54', 'Quantity',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '153', 'Reference 3',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '152', 'Transaction Decision Desc',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '151', 'Transaction Decision',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '150', 'External Response Msg',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Email', 'Ship To Email',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Fax Number', 'Ship To Fax Number',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Phone Number', 'Ship To Phone Number',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Country', 'Ship To Country',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To County', 'Ship To County',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REQUESTED DELIVERY', 'Requested Delivery',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Postal Code', 'Ship To Postal Code',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To State Province', 'Ship To State Province',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To City', 'Ship To City',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '149', 'External Response Code',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '148', 'Reconciliation Id',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '147', 'Auth Expiry Date',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '146', 'Transaction DTTM',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '145', 'Requested DTTM',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '144', 'Follow On Token',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '143', 'Follow On ID',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '142', 'Request Token',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '141', 'Request ID',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '140', 'Merch Password',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Address3', 'Ship To Address3',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Address2', 'Ship To Address2',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Address1', 'Ship To Address1',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Last Name', 'Ship To Last Name',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To First Name', 'Ship To First Name',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '34', 'Ship To Facility',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '33', 'Service Level',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '32', 'Mode',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '31', 'Carrier Code',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship Via', 'Ship Via',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM HOLD', 'Line Item Hold',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '139', 'Merch Id',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '138', 'Requested Amount',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DESIGNATED CARRIER', 'Designated Carrier',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '137', 'Processed Amount',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM REQUESTED DELIVERY', 'Line Item Requested Delivery',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '136', 'Balancecheck',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '135', 'Refund',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '134', 'Settlement',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '133', 'Authorization',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '132', 'Transaction',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '131', 'Text Deleted',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '130', 'Text Added',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '29', 'Requested Delivery Date',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promised Delivery Date', 'Promised Delivery Date',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must Delivery Date', 'Must Delivery Date',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process Flag', 'Process Flag',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Finalizer', 'Allocation Finalizer',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority', 'Priority',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply Promotional', 'Apply Promotional',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment PO', 'Fulfillment PO',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment ASN', 'Fulfillment ASN',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Segment', 'Fulfillment Segment',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer First Name', 'Customer First Name',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer User Id', 'Customer User Id',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '129', 'Text',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer Id', 'Customer Id',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '128', 'Collateral',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Status', 'Payment Status',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '127', 'Gift To',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '126', 'Delivery Instrns.',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is Confirmed', 'Is Confirmed',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '125', 'Cancelation',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Entry Type', 'Entry Type',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '124', 'Hold',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Entered Location', 'Entered Location',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '123', 'Gift Message',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Entered By', 'Entered By',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '122', 'Gift Wrap',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '121', 'Gift From',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '120', 'Hold Resolution',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Facility', 'Fulfillment Facility',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is Cancelled', 'Is Canceled',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TOTAL VALUE', 'Total Value',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is Gift', 'Is Gift',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merch Type', 'Merch Type',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merch Group', 'Merch Group',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'External Item ID', 'External Item ID',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'STATUS', 'Status',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '12', 'Item ID',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer Type', 'Customer Type',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer Last Name', 'Customer Last Name',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BILLTO ADDRESS', 'Billto Address',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM SKU', 'Line Item Sku',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '119', 'Note Type',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE ITEM ALLOCATED QUANTITY', 'Line Item Allocated Quantity',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '118', 'Bill To Email',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '117', 'Bill To Phone Number',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '116', 'Bill To Country',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '115', 'Bill To Postal Code',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '114', 'Bill To State Province',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '113', 'Bill To City',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '112', 'Bill To Address Line 2',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '111', 'Bill To Address Line 1',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '110', 'Bill To Last Name',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason Code', 'Reason Code',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line Item Reason Code', 'Line Item Reason Code',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '154', 'Appeasement',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '155', 'Reference Field 4',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '156', 'Reference Field 5',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '157', 'Reference Field 6',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '158', 'Reference Field 7',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '159', 'Reference Field 8',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '160', 'Reference Field 9',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '161', 'Reference Field 10',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '162', 'Reference Number 1',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '163', 'Reference Number 2',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '164', 'Reference Number 3',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '165', 'Reference Number 4',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '166', 'Reference Number 5',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ONHAND', 'ONHAND',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN', 'ASN',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO', 'PO',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'IN_STORE', 'IN_STORE',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type', 'Supply Type',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created', 'Created',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Status', 'Allocation Status',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity', 'Quantity',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must Ship by Date', 'Must Ship by Date',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must Release by Date', 'Must Release by Date',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship Date', 'Ship Date',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery Date', 'Delivery Date',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SKU Name', 'SKU Name',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Object Id', 'Inventory Object Id',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Object Detail Id', 'Inventory Object Detail Id',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC Name', 'DC Name',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '167', 'Order Enquiry',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '168', 'Customer Complaint',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '169', 'Payment Enquiry',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '170', 'Note Description',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '171', 'Note Type',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '172', 'Process Count',
           'COAuditFieldNames')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason Code', 'Reason Code',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Request ID', 'Transaction: Request ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item ID', 'Item ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To Phone Number', 'Payment: Bill To Phone Number',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To Email', 'Payment: Bill To Email',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To State Province', 'Payment: Bill To State Province',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: Delivery Date', 'Supply Type: Delivery Date',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item carrier code', 'Line Item Carrier Code',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item service level', 'Line Item Service Level',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Card Display Number', 'Payment: Card Display Number',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item store facility alias id', 'Line Item Store Facility Alias ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Purchase orders category', 'Purchase Orders Category',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Fax Number', 'Ship To Fax Number',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Name on Card', 'Payment: Name on Card',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Discount: Deleted', 'Discount: Deleted',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Phone Number', 'Ship To Phone Number',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Auth Expiry Date', 'Transaction: Auth. Expiry Date',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Deleted', 'Payment: Deleted',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: External Response Code', 'Transaction: External Response Code',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Address3', 'Ship To Address3',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Address2', 'Ship To Address2',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Address1', 'Ship To Address1',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To Middle Name', 'Payment: Bill To Middle Name',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Follow On ID', 'Transaction: Follow On ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Facility', 'Fulfillment Facility',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Last Name', 'Ship To Last Name',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merch Group', 'Merch. Group',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination facility', 'Destination Facility',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Email', 'Ship To Email',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: Must Ship by Date', 'Supply Type: Must Ship by Date',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item hold', 'Line Item Hold',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: Allocation Status', 'Supply Type: Allocation Status',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To State Province', 'Ship To State Province',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To Last Name', 'Payment: Bill To Last Name',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment PO', 'Fulfillment PO',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is Confirmed', 'Is Confirmed',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Transaction Decision Desc', 'Transaction: Transaction Decision Desc.',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment ASN', 'Fulfillment ASN',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Req Refund Amount', 'Payment: Req. Refund Amount',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Currency Code', 'Payment: Currency Code',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Status', 'Payment Status',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Security Code', 'Payment: Security Code',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item status', 'Line Item Status',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process Flag', 'Process Flag',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Designated carrier', 'Designated Carrier',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item released quantity', 'Line Item Released Quantity',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Facility', 'Ship To Facility',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item destination facility alias id', 'Line Item Destination Facility Alias ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item requested quantity', 'Line Item Requested Quantity',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is Gift', 'Is Gift',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Is Suspended', 'Payment: Is Suspended',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total value', 'Total Value',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Card Expiry Month', 'Payment: Card Expiry Month',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Requested Amount', 'Transaction: Requested Amount',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Merch Id', 'Transaction: Merch. ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: Must Release by Date', 'Supply Type: Must Release by Date',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item sku', 'Line Item Sku',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item total ordered', 'Line Item Total Ordered',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To First Name', 'Payment: Bill To First Name',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested delivery', 'Requested Delivery',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Created', 'Transaction: Created',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge: Created', 'Charge: Created',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Postal Code', 'Ship To Postal Code',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Reconciliation Id', 'Transaction: Reconciliation ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax: Charge: Deleted', 'Tax: Charge: Deleted',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Entry Type', 'Entry Type',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Request Token', 'Transaction: Request Token',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Transaction Decision', 'Transaction: Transaction Decision',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Discount: Discount Amount', 'Discount: Discount Amount',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer User Id', 'Customer User ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer Type', 'Customer Type',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To County', 'Ship To County',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Entered Location', 'Entered Location',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Card Expiry Year', 'Payment: Card Expiry Year',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship Via', 'Ship Via',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New line item created', 'New Line Item Created',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: SKU Name', 'Supply Type: SKU Name',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Processed Amount', 'Transaction: Processed Amount',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer Id', 'Customer ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To City', 'Ship To City',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Note Type: Text Deleted', 'Note Type: Text Deleted',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Finalizer', 'Allocation Finalizer',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is Cancelled', 'Is Canceled',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply Promotion', 'Apply Promotion',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: Inventory Object Id', 'Supply Type: Inventory Object ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: Inventory Obejct Detail Id', 'Supply Type: Inventory Object Detail Id',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Created', 'Payment: Created',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: Ship Date', 'Supply Type: Ship Date',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: Quantity', 'Supply Type: Quantity',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Merch Password', 'Transaction: Merch. Password',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Card Number', 'Payment: Card Number',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Designated mode', 'Designated Mode',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge: Charge Amount', 'Charge: Charge Amount',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Charge Sequence', 'Payment: Charge Sequence',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination address', 'Destination Address',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item requested delivery', 'Line Item Requested Delivery',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Discount: Created', 'Discount: Created',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Reference 3', 'Payment: Reference 3',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merch Type', 'Merch. Type',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Reference 2', 'Payment: Reference 2',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Reference 1', 'Payment: Reference 1',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Card Type', 'Payment: Card Type',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To City', 'Payment: Bill To City',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'External Item ID', 'External Item ID',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Req Authorization Amount', 'Payment: Req. Authorization Amount',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To Address Line 2', 'Payment: Bill To Address Line 2',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Transaction DTTM', 'Transaction: Transaction DTTM',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To Address Line 1', 'Payment: Bill To Address Line 1',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: Created', 'Supply Type: Created',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Segment', 'Fulfillment Segment',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: External Response Msg', 'Transaction: External Response Msg.',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must Delivery Date', 'Must Delivery Date',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item description', 'Line Item Description',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Service Level', 'Service Level',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority', 'Priority',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Po hold', 'PO Hold',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mode', 'Mode',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To Country', 'Payment: Bill To Country',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last updated datetime', 'Last Updated Datetime',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item allocated quantity', 'Line Item Allocated Quantity',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax: Charge:  Tax Amount', 'Tax: Charge: Tax Amount',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promised Delivery Date', 'Promised Delivery Date',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Note Type: Text Added', 'Note Type: Text Added',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer Last Name', 'Customer Last Name',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item unit cost', 'Line Item Unit Cost',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Req Settlement Amount', 'Payment: Req. Settlement Amount',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Deleted', 'Transaction: Deleted',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested Delivery Date', 'Requested Delivery Date',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Carrier Code', 'Carrier Code',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax: Charge: Created', 'Tax: Charge: Created',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Designated service level', 'Designated Service Level',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel flag', 'Cancel Flag',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Entered By', 'Entered By',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Follow On Token', 'Transaction: Follow On Token',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billto address', 'Bill To Address',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction: Requested DTTM', 'Transaction: Requested DTTM',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type: DC Name', 'Supply Type: DC Name',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment: Bill To Postal Code', 'Payment: Bill To Postal Code',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line item mode', 'Line Item Mode',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge: Deleted', 'Charge: Deleted',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To First Name', 'Ship To First Name',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship To Country', 'Ship To Country',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer First Name', 'Customer First Name',
           'COAuditFieldNamesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fail', 'Fail',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'YDD', 'YDD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'External Description is empty', 'External Description is empty',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TWD', 'TWD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'KES', 'KES',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LKR', 'LKR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Third Party', 'Third Party',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Created', 'DO Created',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RWF', 'RWF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SDP', 'SDP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GRD', 'GRD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'IEP', 'IEP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVOICED', 'Invoiced',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BILL TO', 'BILL TO',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AFA', 'AFA',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CRC', 'CRC',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FKP', 'FKP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EEK', 'EEK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Released', 'Partially Released',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Accepted', 'Accepted',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'HKD', 'HKD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ARA', 'ARA',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'IQD', 'IQD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amex', 'Amex',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCR', 'SCR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VUV', 'VUV',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DKK', 'DKK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'KPW', 'KPW',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'IDR', 'IDR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS Unavbl.', 'AVS Unavbl.',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unavailable', 'Unavailable',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipment Delayed', 'Shipment Delayed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H Unavbl.', 'S&H Unavbl.',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED', 'Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOS', 'SOS',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AED', 'AED',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BWP', 'BWP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped', 'Shipped',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PAID', 'Paid',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cty', 'Cty',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MOP', 'MOP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FJD', 'FJD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Free Domicile', 'Free Domicile',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NIC', 'NIC',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DJF', 'DJF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Yes', 'Yes',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SBD', 'SBD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'None', 'None',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NOT_APPLICABLE', 'Not Applicable',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prepaid', 'Prepaid',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ECS', 'ECS',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'THB', 'THB',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PHP', 'PHP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LUF', 'LUF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FIM', 'FIM',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TTD', 'TTD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SZL', 'SZL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SAR', 'SAR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduled', 'Scheduled',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduling Failed', 'Scheduling Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Receiving Started', 'Receiving Started',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AUTHORIZED', 'Authorized',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Collect', 'Collect',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'HUF', 'HUF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Discover', 'Discover',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'QAR', 'QAR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'COP', 'COP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PTE', 'PTE',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cost & Freight', 'Cost & Freight',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GBP', 'GBP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ZRZ', 'ZRZ',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BIF', 'BIF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INR', 'INR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UYP', 'UYP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MZM', 'MZM',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AOK', 'AOK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivered', 'Delivered',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PGK', 'PGK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SYP', 'SYP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OMR', 'OMR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NGN', 'NGN',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Partially Created', 'DO Partially Created',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Shipped', 'Partially Shipped',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CNY', 'CNY',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS Failure', 'AVS Failure',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GNF', 'GNF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Misc.', 'Misc.',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'HTG', 'HTG',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MAD', 'MAD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Internal Code not found.', 'Internal Code not found.',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MMK', 'MMK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MYR', 'MYR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Shipper Acceptance', 'Pending Shipper Acceptance',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LSL', 'LSL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SLL', 'SLL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BHD', 'BHD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BTN', 'BTN',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TRL', 'TRL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No', 'No',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'KMF', 'KMF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ANG', 'ANG',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated', 'Allocated',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UPS Third Party Freight Collect', 'UPS Third Party Freight Collect',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'KYD', 'KYD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other', 'Other',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GMD', 'GMD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DEALLOCATED', 'Deallocated',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Failed', 'Payment Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CAD', 'CAD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BGL', 'BGL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VEB', 'VEB',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MXP', 'MXP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ILS', 'ILS',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GYD', 'GYD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED_CHARGE', 'Settlement Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CYP', 'CYP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LRD', 'LRD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PEI', 'PEI',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED_AUTH', 'Authorization Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Fraud', 'Payment Fraud',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BSD', 'BSD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In Work', 'In Work',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CLP', 'CLP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'XPF', 'XPF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FRF', 'FRF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Recalled', 'Recalled',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H Failed', 'S&H Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REALLOCATED', 'Reallocated',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALL', 'ALL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BRL', 'BRL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Free on Board', 'Free on Board',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipper rejected', 'Shipper rejected',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BRC', 'BRC',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DEM', 'DEM',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'XCD', 'XCD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'KWD', 'KWD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NPR', 'NPR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OPEN', 'Open',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GWP', 'GWP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'YUN', 'YUN',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SVC', 'SVC',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Visa', 'Visa',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'JPY', 'JPY',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'XOF', 'XOF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially DC Allocated', 'Partially DC Allocated',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Vendor rejected', 'Vendor rejected',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TOP', 'TOP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BEF', 'BEF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourced', 'Sourced',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_PAYINFO', 'Awaiting Payment Info',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Vendor countered', 'Vendor countered',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SUR', 'SUR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Master Card', 'Master Card',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RON', 'RON',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ROL', 'ROL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BDT', 'BDT',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REFUNDED', 'Refunded',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Received', 'Received',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWG', 'AWG',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'City/ST/ZIP', 'City/ST/ZIP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NOK', 'NOK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ZAR', 'ZAR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MUR', 'MUR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Scheduled', 'Partially Scheduled',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid', 'Invalid',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VND', 'VND',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Vendor Acceptance', 'Pending Vendor Acceptance',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'JCB', 'JCB',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TZS', 'TZS',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GIP', 'GIP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction Success', 'Transaction Success',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TND', 'TND',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing Failed', 'Sourcing Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UGX', 'UGX',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CVE', 'CVE',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ZMK', 'ZMK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Diners Club', 'Diners Club',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'JOD', 'JOD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LBP', 'LBP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC Allocated', 'DC Allocated',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'STD', 'STD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Consignee Bill', 'Consignee Bill',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WST', 'WST',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_CREATED', 'DO Created',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'KHR', 'KHR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DOP', 'DOP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EUR', 'EUR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Addr', 'Addr',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_CLOSED', 'Partially Shipped',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Released', 'Released',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MTL', 'MTL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUP', 'CUP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery Duty Paid', 'Delivery Duty Paid',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax Service Unavbl.', 'Tax Service Unavbl.',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Success', 'Success',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SGD', 'SGD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_AUTHORIZATION', 'Awaiting Authorization',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Sourced', 'Partially Sourced',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NZD', 'NZD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'USD', 'USD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BOB', 'BOB',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'HNL', 'HNL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PAB', 'PAB',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ITL', 'ITL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GTQ', 'GTQ',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back Ordered', 'Back Ordered',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AUD', 'AUD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GHC', 'GHC',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LAK', 'LAK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fraud', 'Fraud',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipper countered', 'Shipper countered',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MGF', 'MGF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Default Cancel', 'Default Cancel',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Failed', 'Allocation Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BBD', 'BBD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CHF', 'CHF',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction Failed', 'Transaction Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PYG', 'PYG',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PLZ', 'PLZ',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'YER', 'YER',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created', 'Created',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ATS', 'ATS',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax Service Failed', 'Tax Service Failed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ETB', 'ETB',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Addrs', 'Addrs',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Canceled', 'Canceled',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BND', 'BND',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EGP', 'EGP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'JMD', 'JMD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ctry', 'Ctry',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DZD', 'DZD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SRG', 'SRG',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ISK', 'ISK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LYD', 'LYD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'The request is missing one or more required fields.', 'The request is missing one or more required fields.',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay. Service Unavbl.', 'Pay. Service Unavbl.',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Closed', 'Closed',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BZD', 'BZD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Allocated', 'Partially Allocated',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ESP', 'ESP',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NLG', 'NLG',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'KRW', 'KRW',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MRO', 'MRO',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_REFUND', 'Awaiting Refund',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ZWD', 'ZWD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SEL', 'SEL',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CSK', 'CSK',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'IRR', 'IRR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PKR', 'PKR',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CLOSED', 'Shipped',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BMD', 'BMD',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility', 'Facility',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DRAFT', 'Draft',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_RELEASED', 'DO Released',
           'COAuditFieldValues')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MD', 'Manual markdown',
           'COPriceOverrideReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PM', 'Competitor''s price match',
           'COPriceOverrideReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PI', 'BOGO Promotional Item',
           'COPriceOverrideReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RO', 'Revert to original price',
           'COPriceOverrideReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OT', 'Others',
           'COPriceOverrideReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MU', 'Manual markup',
           'COPriceOverrideReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Reason_Code', 'No Reason Codes Found',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_Type', 'Type',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_Code_Type', 'Reason Type',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Reason_Code', 'Enter Reason Code',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_Codes', 'Reason Codes',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Reason_Code_Description', 'Enter Reason Code Description',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'System_Defined', 'System Defined',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_Code_List_Panel', 'Reason Code Left Panel',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_Code_Right_Panel', 'Reason Code Right Panel',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_Code', 'Reason Code',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Detail_Tab', 'Details',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mark_For_Deletion', 'Inactive',
           'COReasonCode')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax Service Unavbl.', 'Tax Service Unavbl.',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay. Service Unavbl.', 'Pay. Service Unavbl.',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Fraud', 'Payment Fraud',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS Failure', 'AVS Failure',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other', 'Other',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Failed', 'Payment Failed',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Misc.', 'Misc.',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Failed', 'Allocation Failed',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H Failed', 'S&H Failed',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax Service Failed', 'Tax Service Failed',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H Unavbl.', 'S&H Unavbl.',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS Unavbl.', 'AVS Unavbl.',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipment Delayed', 'Shipment Delayed',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Default Cancel', 'Default Cancel',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Receipt Not Expected', 'Receipt Not Expected',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Condition Variance', 'Condition Variance',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity Variance', 'Quantity Variance',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity and Condition Variance', 'Quantity and Condition Variance',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unexpected Item', 'Unexpected Item',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Resolved', 'Resolved',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel un-received quantity', 'Cancel un-received quantity',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mark un-received quantity as received', 'Mark un-received quantity as received',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge additional fees', 'Charge additional fees',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No additional fees', 'No additional fees',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address not Verified', 'Address not Verified',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitAddrssDtl', 'AwaitAddrssDtl',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitAddrssNPymntDtl', 'AwaitAddrssNPymntDtl',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitItemRcpt', 'AwaitItemRcpt',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitPymntDtl', 'AwaitPymntDtl',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel un-recvd qty', 'Cancel un-recvd qty',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge Addn fees', 'Charge Addn fees',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mark un-recvd qty', 'Mark un-recvd qty',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Addn fees', 'No Addn fees',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Qty and Cond Vari', 'Qty and Cond Vari',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'XONeedsUserAction', 'XONeedsUserAction',
           'COReasonCodeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Appeasement',
           'COReasonCodeType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Cancel',
           'COReasonCodeType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Hold',
           'COReasonCodeType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'Allocation Failure',
           'COReasonCodeType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '4', 'RMA Alert',
           'COReasonCodeType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '5', 'Resolve Receipt Variance',
           'COReasonCodeType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '6', 'Post Void',
           'COReasonCodeType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancelled Qty', 'Canceled quantity',
           'ChangeManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Committed Ship Date', 'Earliest ship by date(Alloc)',
           'ChangeManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Send complete order',
           'ChangeManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Comments(L)', 'Order line notes',
           'ChangeManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Earliest Deliver By Date', 'Earliest deliver by date(Alloc)',
           'ChangeManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '20', 'Send modified lines with order header',
           'ChangeManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Comments(H)', 'Order notes',
           'ChangeManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Retail', 'Retail',
           'ChannelType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return', 'Return',
           'ChannelType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Retail',
           'ChannelType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '20', 'Customer',
           'ChannelType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '30', 'Return',
           'ChannelType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer', 'Customer',
           'ChannelType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '(select one)', '(select one)',
           'ChannelType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '40', 'B2B',
           'ChannelType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'B2B', 'B2B',
           'ChannelType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prorated based on the Line Total', 'Prorated based on the Line Total',
           'ChargeHandlingStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Prorated based on the Line Total',
           'ChargeHandlingStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Collect S&H during  first Invoice creation',
           'ChargeHandlingStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Collect S&H during first Invoice creation', 'Collect S&H during  first Invoice creation',
           'ChargeHandlingStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add SnH during first invoice creation', 'Collect S&H during  first Invoice creation',
           'ChargeHandlingStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prorate SnH charges', 'Prorated based on the Line Total',
           'ChargeHandlingStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Discover', 'Discover',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '60', 'Diners Club',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Diners Club', 'Diners Club',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Visa', 'Visa',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '30', 'Master Card',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Master Card', 'Master Card',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amex', 'Amex',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '50', 'JCB',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '20', 'Discover',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '40', 'Visa',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Amex',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'JCB', 'JCB',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Diners_Club', 'Diners Club',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_Type', 'Card Type',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Detail_Tab', 'Details',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mark_For_Deletion', 'Inactive',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Card_Name', 'Enter Card Name',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'System_Defined', 'System Defined',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Master_Card', 'Master Card',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Details_Tab', 'Details',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Credit_Card_Type', 'Credit Card Type',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Credit_Card_Type', 'No Credit Card Type',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_Type_List_Panel', 'Card Type Left Panel',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_Type_Right_Panel', 'Card Type Right Panel',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_Name_Special_Characters', 'Card Name cannot contain special characters',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_name', 'Card name',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Description', 'Enter Description',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_type', 'Card type',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_Name', 'Card Name',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Card_Type', 'Card type is required field',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_Type_Special_Characters', 'Card type cannot contain special characters',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Category', 'Category',
           'CreditCardType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Header Fields', 'Order Header Fields',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must deliver by date', 'Must deliver by date',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 10', 'Reference field 10',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination postal code', 'Destination postal code',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer ID', 'Customer ID',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination state', 'Destination state',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order type', 'Order type',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Line Fields', 'Order Line Fields',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination county', 'Destination county',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination first name', 'Destination first name',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination last name', 'Destination last name',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Critical_Change', 'Critical Change',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO ID', 'PO ID',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination city', 'Destination city',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered UOM', 'Ordered UOM',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply promotion', 'Apply promotion',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process flag', 'Process flag',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested delivery date', 'Requested delivery date',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Critical Change Fields', 'Critical Change Fields',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination country', 'Destination country',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN ID', 'ASN ID',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Designated service level', 'Designated service level',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Required capacity per unit', 'Required capacity per unit',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination address', 'Destination address',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must Delivery Date', 'Must delivery Date',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered quantity', 'Ordered quantity',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered item', 'Ordered item',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 9', 'Reference field 9',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 8', 'Reference field 8',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 7', 'Reference field 7',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 6', 'Reference field 6',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 5', 'Reference field 5',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 4', 'Reference field 4',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 3', 'Reference field 3',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 2', 'Reference field 2',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 1', 'Reference field 1',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OrderLine Must Delivery Date', 'OrderLine Must Delivery Date',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process type', 'Process type',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination facility', 'Destination facility',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Designated carrier', 'Designated carrier',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Designated mode', 'Designated mode',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merge facility', 'Merge facility',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference number 1', 'Reference number 1',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference number 2', 'Reference number 2',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery options', 'Delivery options',
           'CriticalChangeManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Emails_Found', 'No emails found',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Date', 'Date',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Subject', 'Subject',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Open_In_New_Tab', 'Open mail in new tab',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expand', 'Expand',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Collapse', 'Collapse',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Go_To_Bottom', 'Go to bottom',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Go_To_Top', 'Go to top',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'From:', 'From:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'To:', 'To:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'More_Fields', 'More Fields',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CCAddress:', 'Cc:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BCCAddress:', 'Bcc:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CreatedSource:', 'Source:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ReplyToAddress:', 'Reply to:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status:', 'Status:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Type', 'Type',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_Comm_Notes', 'Add Note',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Note', 'Note',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes_Required', 'Notes text is a required field',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Type:', 'Type:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes', 'Notes',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Notes_Found', 'No notes found',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Emails', 'Emails',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit_Text_Notes', 'Edit text notes',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Details', 'Details',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes_Should_not_exceed_1000_characters', 'Notes should not exceed 1000 characters',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last Updated Date', 'Last Updated Date',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last Updated By', 'Last Updated By',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last updated date:', 'Last updated date:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last updated by:', 'Last updated by:',
           'CustomerCommunication')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferred', 'Preferred',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'preferred_payment_info', 'Preferred payment info.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address_nickname', 'Address nickname',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address_verified', 'Address verified',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Others_panel', 'Payment info: "Others" panel',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address_line2', 'Address line 2',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Gift_card_panel', 'Payment info: Gift card panel',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferences_payment_info_reference_fields', 'Preferences: payment info reference fields',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address_line1', 'Address line 1',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_details', 'Customer details',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Fields', 'Reference Fields',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_expiry_month', 'Card expiry month',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferred_shipping_address_is_not_available', 'Preferred shipping address is not available',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name_on_card', 'Name on card',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Email', 'Email',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_name', 'Last name',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'register_customer', 'Register customer',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_method', 'Payment method',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferences_payment_info_billing_address', 'Preferences: payment info billing address',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expired_image', 'Expired image',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'register_customer_title', 'Register Customer',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferred_image', 'Preferred image',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferences', 'Preferences',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'City_State_Zip_code', 'City, State, Zip code',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save_address_info', 'Save address info.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billing_Addresses', 'Billing Addresses',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Addresses', 'Shipping Addresses',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field9', 'Reference field 9',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_ID', 'Customer ID',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Country', 'Country',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Name', 'Last Name',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field8', 'Reference field 8',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field7', 'Reference field 7',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field6', 'Reference field 6',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_no_5', 'Reference number 5',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field5', 'Reference field 5',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Credit_card_reference_fields', 'Payment info: Credit card reference fields',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_no_4', 'Reference number 4',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field4', 'Reference field 4',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_no_3', 'Reference number 3',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field3', 'Reference field 3',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_no_2', 'Reference number 2',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field2', 'Reference field 2',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_no_1', 'Reference number 1',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field1', 'Reference field 1',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_expiry_year', 'Card expiry year',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_ID', 'Payment info. ID',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fax_no', 'Fax no.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Credit_card_panel', 'Payment info: Credit card panel',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Export_All_Customers', 'Export All Customers',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Numbers', 'Reference Numbers',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_customers_found_for_the_given_criteria', 'No customers found for the given criteria',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address_ID', 'Address ID',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'preferred_shipping_method', 'Preferred shipping method',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'preferred_billing_address', 'Preferred billing address',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expired', 'Expired',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billing_Address', 'Billing Address',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'preferred_shipping_address', 'Preferred shipping address',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save_payment_info', 'Save payment info.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Info', 'Payment Info.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone_no', 'Phone no.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_type', 'Card type',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferred_payment_info_is_not_available', 'Preferred payment info. is not available',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferences_billing_address', 'Preferences: billing address',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Gift_card_reference_fields', 'Payment info: Gift card reference fields',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_type', 'Customer type',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customers_list', 'Customers list',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'First_name', 'First name',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone_No', 'Phone No.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customers_DTC', 'Customers (DTC)',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name', 'Name',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Export_Customer(s)', 'Export Customer(s)',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_addresses_are_not_available', 'Shipping addresses are not available',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Address', 'Shipping Address',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'registered_customer', 'Registered customer',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_method', 'Shipping method',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferred_billing_address_is_not_available', 'Preferred billing address is not available',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_no', 'Card no.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Details', 'Details',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_reference_fields', 'Customer reference fields',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Type', 'Customer Type',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferences_payment_info', 'Preferences: payment info',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Information', 'Customer Information',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Others', 'Other(s)',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_is_not_available', 'Payment info. is not available',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Credit_card_billing_address', 'Payment info: Credit card billing address',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'update_customer_title', 'Update Customer',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Credit_cards_panel', 'Payment info: "Credit cards" panel',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferences_shipping_address', 'Preferences: shipping address',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'First_Name', 'First Name',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift_Cards', 'Gift Card(s)',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Other_payment_info_reference_fields', 'Payment info: Other payment info reference fields',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Credit_Cards', 'Credit Card(s)',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferences_reference_fields', 'Preferences: reference fields',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Master_Data', 'Master Data',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Gift_cards_panel', 'Payment info: "Gift cards" panel',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billing_addresses_are_not_available', 'Billing addresses are not available',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_field10', 'Reference field 10',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CitySpStateSpZip', 'City State Zip code',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save_Customer', 'Save Customer',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customers_BTC', 'Customers (B2C)',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'add_customer', 'Add Customer',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_info_Other_panel', 'Payment info: "Other" panel',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Echecks', 'E-Check(s)',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account_Type', 'Account type',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account_No', 'Account no.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Routing_Number', 'Routing no.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Drivers_License_Number', 'Driver''s license no.',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Drivers_License_State', 'Driver''s license state',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Drivers_License_Country', 'Driver''s license country',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Business_Name', 'Business name',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Business_Tax_Id', 'Business tax ID',
           'CustomerMaster')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_shippingInfo', 'Back to Shipping Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ShippingInformation', 'Shipping Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'description', 'Description',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'alert_cannot_create_DO_some_orderLine(s)', 'DOs cannot be created for some of the selected order line(s).',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_Line_No', 'DO Line No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PaymentRef3', 'Payment reference 3',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PaymentRef2', 'Payment reference 2',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Credit_card_no', 'Credit card no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PaymentRef1', 'Payment reference 1',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Remove', 'Remove',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Deallocate', 'Deallocate',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerOrderErrors', 'Errors',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotionDetails_step_icon_lbl_100', 'Add Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_Status', 'DO Status',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'color_suffix', 'Color suffix',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Origin_Facility', 'Origin Facility',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CreatedSource', 'Created source',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_viewShippingInfo', 'Back to Shipping Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store_id', 'Store id',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Earliest_Ship_By', 'Earliest Ship By',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OpenSettlementAmt', 'Open Settlement Amount',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LastUpdatedDate', 'Last updated date',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AcceptRecommendedAddr', 'Accept this recommended address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Suspend', 'Suspend',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'available_promotions', 'Available Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search_Item', 'Search Item',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_promotionDetails', 'Back to Promotion Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'update_order', 'Update Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release_DO', 'Release DO',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'comments_for_cancel', 'CustomerOrder',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Comments for Cancel', 'CustomerOrder',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_sub_total', 'Item(s) subtotal',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description_colon', 'Description:',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Returnable', 'Returnable',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotional', 'Promotional',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item Search', 'Item Search',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PaymentDetails', 'Payment Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Discounts', 'Discount',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotionDetails_step_icon_lbl', 'Add Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reset', 'Reset',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AmountRefunded', 'Amount Refunded',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Response_Code_colon', 'Reason Code:',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_No', 'Line No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPromotionDetailsStep_step_icon_lbl_160', 'Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'First_Name', 'First name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FirstName', 'First name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editShippingInfoStep_step_icon_lbl_120', 'Shipping Info.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PromotionName', 'Promotion Name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reason_code_for_cancel', 'Reason Code for Cancel',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_discounts', 'Total discounts',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Audit Details', 'Audit Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Current', 'Current',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Bill_To_Information', 'Bill To Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'First_Name_is_required_field', 'First name is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'searchAndAddItem_step_icon_lbl_120', 'Add Items',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update_Shipping_Information', 'Update Shipping Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Details', 'Payment Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expiry_month_cannot_be_less_than', 'Expiry month cannot be less than',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'value', 'Value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Discounts', 'Total Discounts',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddEditNotes', 'Add/Edit Notes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CCNumber', 'CC#',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Atleast_one_order_line_is_required', 'Atleast one order line is required.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_type', 'Order type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_viewPromotionDetails', 'Back to Promotion Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'product_class', 'Product class',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate', 'Allocate',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SnH_Taxes', 'Shipping and handling taxes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotions_available', 'Promotions Available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Order_Details_Item_List', 'Customer Order Details Item List',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_type_is_required_field', 'Card type is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Country', 'Country',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Zip_Code', 'Zip code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'details', 'Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Carton_break', 'Carton break',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update_Order_Header_Details', 'Update Order Header Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ReviewRecommendedAddrAndChoose', 'Please review the recommended shipping address and choose the appropriate one',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift_Card_Number', 'GC#',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Credit_card', 'Credit card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Description', 'Item Description',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'phone', 'Phone',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_Items', 'Cancel Item(s)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billing_Address_First_Line_Cst', 'Billing address: Line 1',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'status', 'Status',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Full_Address', 'View full address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'YouEntered', 'You entered:',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay_By_Gift_Card', 'Pay by Gift Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPromotionDetailsStep_step_icon_lbl_140', 'Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update_Payment_Information', 'Update Payment Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Estimated_Total', 'Estimated Total',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'External_item_ID', 'External item ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete', 'Delete',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply_Detail', 'Supply Detail',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'First_name', 'First name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'searchAndAddItem_step_icon_lbl_100', 'Add Items',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expiry_Date', 'Expiry date',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Base', 'Price= Base Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'alert_cannot_allocate_some_orderLine(s)', 'Some of the selected order line(s) cannot be allocated.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total Discounts', 'Total Discounts',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Find_Customer', 'Find Customer',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OpenAuthAmount', 'Open Authorization Amount',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'type', 'Type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_List', 'Item List',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Miscellaneous_Details', 'Miscellaneous Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_editOrderDetailsStep', 'Back to Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'First_Name_Header', 'First Name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentDetails_step_icon_lbl', 'Add Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPromotionDetailsStep_step_icon_lbl_130', 'Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'item_id', 'Item ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UOM', 'UOM',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Published', 'Published',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WeFound', 'We found:',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_Date', 'Created Date',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippingInfo_step_icon_lbl', 'Add Shipping Info.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply_Type', 'Supply Type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Method', 'Shipping Method',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Latest_Ship_By', 'Latest Ship By',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'no_offered_items_found', 'No offered items found',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_no', 'Card no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'add_item', 'Add to Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Review_Order', 'Review Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPromotionDetailsStep_step_icon_lbl_120', 'Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'City_is_required_field', 'City is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Transactions', 'Payment Transactions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipping_mode_description', 'Shipping Mode Description',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_segment', 'Inventory segment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name_is_required_field', 'Name is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address_Line_1_is_required_field', 'Address Line 1 is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Edit_Order_Details', 'View/Edit Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'City', 'City',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'list_of_offered_items', 'List of Offered Items',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Force_Allocate', 'Force Allocate',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Zip_code_is_required_field', 'Zip code is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Yes', 'Yes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'e-mail', 'Email',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'created', 'Created',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_valid_email_id', 'Please enter valid email id',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'order_Type', 'Order Type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'KeepAddrAsEntered', 'Keep my address as entered',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NameOnCard', 'Name on card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merchandise_group', 'Merchandise group',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift_card_number_is_required_field', 'Gift card no. is a required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'x_Value', 'X Value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'success_message', 'Order has been created successfully.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Estimated_SnH', 'Estimated shipping and handling',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Information_UnAvailable', 'Payment Information is not provided',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return_Credits', 'Return Credits',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Choose_a_card', '(Choose a card)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit', 'Edit',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Carrier', 'Carrier',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unhold_order', 'Unhold Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Earliest_Deliver_By', 'Earliest Deliver By',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'hide_advanced_search_option', 'Hide Advanced Search Options',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ViewNotes', 'View Notes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reviewOrderStep_step_icon_lbl_100', 'Confirm Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created', 'Created',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Zip_Code_Required', 'Please enter a zip code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_address', 'Shipping address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Offered_Items_Found', 'No Offered Items found',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Place_Order', 'Place Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay_By_Credit_Card', 'Pay by Credit Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone_number_is_required_field', 'Phone number is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_orderDetailsStep', 'Back to Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appeasements', 'Appeasements',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipment_No', 'Shipment No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ItemId', 'Item ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'available', 'Availability',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay_By_New_Credit_Card', 'Pay by New Credit Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'product_Class', 'Product Class',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Updated_Successfully', 'Order has been updated successfully.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirmed', 'Confirmed',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_filter_criteria', 'No orders found for the given criteria.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name', 'Name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes_not_available', 'Notes Not Available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_ID', 'Line ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billing_Address_Second_Line_Cst', 'Billing address: Line 2',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available', 'Available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Address', 'Shipping Address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Status', 'Order Status',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Details', 'Line Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LineTotal', 'Line Total',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'order_type', 'Order type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EnterShipToAddress2', 'address for this line.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EnterShipToAddress1', 'Please enter the ship to',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_Item', 'Add Item',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OrderLevelPromotions', 'Order Level Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LastName', 'Last name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SelectState', 'Select a State',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Remaining', 'Remaining',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_Where_I_Came_From', 'Back to where I came from',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'State', 'State',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone', 'Phone',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'find_facility', 'Find Facility',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search_Customer', 'Search Customer',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'External_line_no', 'External line no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'comments_for_appeasement', 'Comments for Appeasement',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_customer_first_name', 'Please enter customer first name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply', 'Supply',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone_Number', 'Phone number',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add', 'Add',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewPaymentDetails_step_icon_lbl', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderDetailsStep_step_icon_lbl', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered_Item', 'Ordered Item',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_a_valid_email_address_in_this_format', 'Please enter a valid e-mail address in this format:',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_coEditHeaderStep', 'Back to Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Valid_Credit_Card_Information', 'Enter Valid Credit Card Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping', 'Estimated shipping and handling',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_viewPaymentDetails', 'Back to Payment Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reason_code_for_appeasement', 'Reason Code for Appeasement',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'style', 'Style',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped_Quantity', 'Shipped Quantity',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentDetails_step_icon_lbl_100', 'Add Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Code', 'Code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update_Quantity', 'Update Quantity',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'not_available', 'Not available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing_results_cannot_be_viewed_for_the_order_line', 'Sourcing results cannot be viewed for the order line.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'first_name', 'First name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Name_Header', 'Last Name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'alert_confirm_on_source_button_click', 'Some of the selected order line(s) will be re-sourced and processed further as per workflow configuration. Are you sure you want to continue?',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Full_Comments', 'View full comments',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PutOrderLineOnHold', 'Put the order line on hold',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DiscountAmount', 'Discount',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility_Information', 'Facility Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SaveAddressAsIs', 'Save the address as it is',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift_Card_Summary', 'Gift Card Summary',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Additional_Details', 'Order Additional Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Note_Type_Required', 'Note Type is a required field on row number:',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'search', 'Search',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OPEN', 'Open',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Name_Popup', 'Name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'alert_cannot_force_allocate_some_orderLine', 'Some of the selected order line(s) cannot be Force Allocated.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PhNumber', 'Phone no.:',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancel_order', 'Cancel Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'phone_no', 'Phone no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'mm', 'mm',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Information', 'Item Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Not_Available', 'Not Available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SnH', 'Estimated shipping and handling',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes_Required', 'Notes is a required field on row number:',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SelectCountry', 'Select country',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply', 'Apply',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddressVerificationFailed', 'Shipping address validation is failed',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source', 'Source',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_editPaymentDetailsStep', 'Back to Payment Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UseAddressAsBillingAddress', 'Use the above address as billing address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'State_is_required_field', 'State is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_finalizer', 'Allocation finalizer',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Sourcing_Results', 'View Sourcing Results',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderDetailsStep_step_icon_lbl_100', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'order_no', 'Order no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'parent_order_id', 'Parent Order ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Details', 'Customer Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Information', 'Payment Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift_Card_Details', 'Gift Card Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ShippingMethod', 'Shipping Method',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'search_items_create_order', 'Search Items & Create Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_Edit_Notes', 'Add/Edit Notes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Promotion_Item_List_Found', 'No Item List Found',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddNewAddress', 'Add a new address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CardExpiryYr', 'Card expiry year',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_paymentDetails', 'Back to Payment Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddShippingAddress', 'Add Shipping Address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift_Card_To_Be_Applied', 'Gift Card to be applied',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Available_Promotions', 'No available promotions found',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ShipToAddress', 'Ship To Address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'more', 'More',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Name', 'Last name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewPromotionDetails_step_icon_lbl_110', 'Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Number5', 'Reference number 5',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Number4', 'Reference number 4',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Number3', 'Reference number 3',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Number2', 'Reference number 2',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_4_Digits', 'Last 4 digits',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Number1', 'Reference number 1',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_only_one_order_line', 'Please select only one order line.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Name_is_required_field', 'Last name is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_No', 'DO No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reviewOrderStep_step_icon_lbl', 'Confirm Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotion_details', 'Promotion Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Estimated_taxes', 'Estimated taxes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'External_no', 'External order no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Confirmed', 'Order Confirmed',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Authorization_Success_Failure_Indicator', 'Authorization Success/Failure Indicator',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Customers_Found', 'No Customers found for search criteria',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping Details', 'Shipping Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CreditCardSummary', 'Credit Card Summary',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DeleteItem', 'Delete Item(s)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectOne', '(select one)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply_the_Gift_Card', 'Apply the Gift Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Total', 'Line total',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Total', 'Order Total',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return_Fees', 'Return fees',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ChooseAddAnotherAddress', 'Choose or add another address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_Type', 'Card type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'add_item_and_proceed', 'Add to Order and Proceed',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_shipment_information_found', 'No Shipments associated',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Subtotal', 'Item(s) Subtotal',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Email_Id_is_required_field', 'Email Id is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_ID', 'Item ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice Details', 'Invoice Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_ID', 'Customer ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_190', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Adjustment', 'Adjustment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_Appeasements', 'Add Appeasement',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'YES', 'Yes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'yyyy', 'yyyy',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'alert_cannot_de_allocate_some_orderLine(s)', 'Some of the selected order line(s) cannot be deallocated.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OrderPromotions', 'Order Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selling_error_message', 'An unexpected error has occured. Please contact administrator for further assistance.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Exchange', 'Exchange Order(s)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'last_name', 'Last name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field10', 'Reference field 10',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'IsComplete', 'Is complete',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'flat_Appeasement', 'Flat appeasement',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update_Order', 'Update Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total Appeasements', 'Total Appeasements',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_name', 'Last name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddressLine2', 'Address line 2',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddressLine1', 'Address line 1',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AmountCharged', 'Amount Charged',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_to_View_Order_Details', 'Back to View Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ph', 'Ph.:',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customer_id', 'Customer ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billing_address', 'Billing address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes_Type', 'Notes Type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Stack_trace', 'Stack trace',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_One', 'Select one',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing_Details', 'Sourcing Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Create_DO', 'Create DO',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ListOfOfferedItems', 'List of Offered Items',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Type_Popup', 'Type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment_ASN', 'Fulfillment ASN',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'search_txt_default', 'Search for item ID/Description',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Email', 'Email',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Not_Yet_Applied', 'Not yet applied',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Summary', 'Order Summary',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'IsSuspended', 'Is suspended',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'comments_for_hold', 'Is suspended',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Comments for Hold', 'CustomerOrder',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update_Promotions', 'Update Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_170', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone_no', 'Phone no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amount_charged', 'Amount charged',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'searchAndAddItem_step_icon_lbl', 'Add Items',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'size', 'Size',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'no_promotions_available', 'No promotions available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Order_Details', 'View Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Order', 'Customer Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated_On', 'Allocated On',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay_By_Existing_Credit_Card', 'Pay by Existing Credit Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_the_valid_credit_card_Number', 'Enter the valid credit card number',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVSUnavailable', 'Address verification service is unavailable',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Latest_Release_By', 'Latest Release By',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered_Quantity', 'Ordered Quantity',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amount_To_Be_Charged', 'Amount to be charged',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment_facility', 'Fulfillment facility',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'alert_cannot_source_some_orderLine(s)', 'Some of the selected order line(s) cannot be sourced.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are_you_sure_that_you_want_to_DeAllocate_the_selected_Order?', 'Are you sure that you want to deallocate the selected order?',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Address_Line_5_Cst', 'Shipping Address : Line 5',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes', 'Notes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Promotion_Details', 'Promotion Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amount', 'Amount',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift_card_no', 'Gift card no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsStep_step_icon_lbl_190', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CreateReturn', 'Create Return',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Security_code_is_required_field', 'Security code is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No', 'No',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_Orderline(s)', 'Please select at least one order line.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipment Details', 'Shipment Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_160', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OK', 'OK',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UnitPrice', 'Unit Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Security_Code', 'Security code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirm_Changes', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CardType', 'Card Type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewOrderDetails_step_icon_lbl_110', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'comma', ',',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Additional Details', 'Additional Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PIN', 'Pin',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Charges', 'Order Charges',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Details', 'Item Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search_Orders', 'Search Orders',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Type', 'Customer type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewShippingInfo_step_icon_lbl_110', 'Shipping Info.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LinePromotions', 'Line Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PhoneNumber', 'Phone no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Zip_Code_Should_Contain_5_Digits', 'Zip Code should contain 5 digits',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Details', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority_group', 'Priority group',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AmountAuthorized', 'Amount Authorized',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'calculate', '(Calculate)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editOrderDetailsStep_step_icon_lbl_140', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expiry_year_is_required_field', 'Expiry year is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promised_delivery_by', 'Promised delivery by',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addANewGiftCard', 'Add a new gift card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped_Date', 'Shipped Date',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Type_Header', 'Customer Type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NO', 'No',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ZipCode', 'Zip code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_150', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Estimated_Taxes', 'Estimated Taxes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Applied_Gift_Cards', 'Applied Gift Cards',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unit_Price', 'Unit Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Address_Line_4_Cst', 'Shipping Address : Line 4',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancel', 'Cancel',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested_delivery_by', 'Requested delivery by',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tracking_No', 'Tracking No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated_Item', 'Allocated Item',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return', 'Return',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address', 'Address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsStep_step_icon_lbl_170', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_viewOrderDetails', 'Back to Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LastUpdatedSource', 'Last updated source',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_no', 'Line No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'astrix', '*',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_140', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Details', 'Allocation Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'order_No', 'Order No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unit_price', 'Unit price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddNew', 'Add New',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_searchAndAddItem', 'Back to Item Search',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total Taxes', 'Total Taxes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hold_Order', 'Hold Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'coEditHeaderStep_step_icon_lbl_180', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are_you_sure_that_you_want_to_deallocate_the_selected_Orderline(s)_?', 'Are you sure that you want to deallocate the selected order line(s)?',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ShippingAddress', 'Shipping Address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editOrderDetailsStep_step_icon_lbl_120', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Replace_SnH_Fees', 'Replace Shipping and handling fees',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'search_item_search_result_list_panel', 'Search item result list panel',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_editPromotionDetailsStep', 'Back to Promotion Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsStep_step_icon_lbl_160', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'store_id', 'Store ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'currency', 'Currency',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated_Quantity', 'Allocated Quantity',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_email_id', 'Please enter email id',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_130', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Address_Line_3_Cst', 'Shipping Address : Line 3',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PIN_is_required_field', 'PIN is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'hold_order', 'Hold Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billing_Address_Third_Line_Cst', 'Billing address: Line 3',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery_Details', 'Delivery Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Error_Pop_Up_Title', 'Message',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'none', '(none)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'chooseAddAnotherGiftCard', 'Choose or add another gift card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CardNumber', 'Card No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion Details', 'Promotion Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Remaining_to_pay', 'Remaining to pay',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_coDetailsCancelItemStep', 'Back to Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amount_to_be_charged', 'Amount to be charged',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'save', 'Save',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total Charges', 'Total Charges',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_number_is_required_field', 'Credit card no. is a required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewPromotionDetails_step_icon_lbl', 'Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Canceled_Qty', 'Canceled Qty',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ExistingCards', 'Existing card(s)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsStep_step_icon_lbl_150', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'color', 'Color',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity', 'Quantity',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Applied', 'Applied',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available_Promotions', 'Available Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_120', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Use_Shipping_Address_As_Billing_Address', 'Use shipping address as billing address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_Item', 'Cancel Item',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipping_methods_available', 'Shipping Methods Available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'no_shipping_methods_available', 'No shipping methods available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DefaultStringForPromotionsPopup', 'Search by Item ID/Description',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_reviewOrderStep', 'Back to Order Summary',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'chargeSequence', 'Charge sequence',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_a_valid_email', 'Please enter a valid e-mail address in this format: yourname@domain.com',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hold', 'Hold',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewPaymentDetails_step_icon_lbl_110', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CreatedDate', 'Applied Date',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit_Order_Details', 'Edit Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Header_Promotion_Details', 'Promotion Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddressVerifiedMessage', 'Shipping address is verified',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Zip_Code_Is_A_Required_Field', 'Zip Code is a required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amount_to_be_Charged', 'Amount to be charged',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_Code', 'Reason Code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Address_Line_2_Cst', 'Shipping Address : Line 2',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PaymentMethod', 'Payment Method',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsStep_step_icon_lbl_140', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewShippingInfo_step_icon_lbl', 'Shipping Info.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment_Facility', 'Fulfillment Facility',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_editShippingInfoStep', 'Back to Shipping Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merchandise_type', 'Merchandise type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'back', 'Back',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UseAddressForAllItems', 'Use the above address for all items',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_amount_to_be_charged', 'Total amount to be charged',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipment_Details', 'Shipment Detail(s)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_customer_last_name', 'Please enter customer last name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CardExpiryMonth', 'Card expiry month',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipment', 'Shipment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Additional_Details', 'Additional Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OpenRefundAmt', 'Open Refund Amount',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'proceed', 'Proceed',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'zip_Code', 'zip code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Message', 'Message',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ShipToDetails', 'Ship To Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment_PO', 'Fulfillment PO',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'valid_email_address', 'yourname@domain.com',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'item_added_msg', 'Item is Added!!',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone_Header', 'Phone No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Clear', 'Clear',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewOrderDetails_step_icon_lbl', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expiry_month_is_required_field', 'Expiry month is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'coDetailsCancelItemStep_step_icon_lbl_130', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'style_suffix', 'Style suffix',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Comments', 'Comments',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsStep_step_icon_lbl_130', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'no_results_found_txt', 'No results found for the given search criteria',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field9', 'Reference field 9',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'next', 'Next',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field8', 'Reference field 8',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field7', 'Reference field 7',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination', 'Destination',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'productDetails', 'Product Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field6', 'Reference field 6',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field5', 'Reference field 5',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '$', '$',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Estimated_total', 'Estimated total',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field4', 'Reference field 4',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Name', 'Customer Name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field3', 'Reference field 3',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field2', 'Reference field 2',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address_Line2', 'Address line 2',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference_Field1', 'Reference field 1',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address_Line1', 'Address line 1',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must_delivery_by', 'Must delivery by',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPromotionDetailsStep_step_icon_lbl_190', 'Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reason_code_for_hold', 'Reason Code for Hold',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone_No', 'Phone No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Paranthesis2', ')',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editShippingInfoStep_step_icon_lbl_150', 'Shipping Info.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Paranthesis1', '(',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_phone_no', 'Please enter phone number',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_a_valid_amount', 'Please enter valid amount',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Expired', 'Customer Expired',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Address_Line_1_Cst', 'Shipping Address : Line 1',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Country_is_required_field', 'Country is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'hold', 'Hold',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CustomerId_Header', 'Customer ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total Order Amount', 'Total Order Amount',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Level_Promotions', 'Line Level Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'payment_status', 'Payment status',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippingInfo_step_icon_lbl_100', 'Add Shipping Info.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Address_Pop_up', 'Shipping address pop up',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsStep_step_icon_lbl_120', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_date', 'Select date',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'show_advanced_search_option', 'Advanced Search Options',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PaymentStatus', 'Payment Status',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_Order', 'Cancel Order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source_by_Rate', 'Source by Rate',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_editCOConfirmChangesStep', 'Back to Order Summary',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Additional_Details', 'Line Additional Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'wait', 'Please Wait...',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Email_Header', 'Email',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_Line_Status', 'DO Line Status',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'sold_in_stores', 'Sold in stores',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'delivery_Options', 'Delivery Options',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship_To', 'Ship To',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DeliveryOption', 'Delivery Option',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship_to_an_address', 'Ship to an address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddressNotAppliedToAllLinesMessage', 'Address not applied to all the lines as few of them do not support ship to an address delivery option',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PleaseSelectAStore', 'Please select a store',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SelectAStore', 'Select a Store',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ChangeTheStore', 'Change Store',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply_Balance_available_to_ship', 'Supply Balance - Available to Ship',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Min_Order_Line_Qty_Popup', 'Min. Orderline Quantity',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Max_Order_Line_Qty_Popup', 'Max. Orderline Quantity',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Min_Order_Line_Value_Popup', 'Min. Orderline Value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Max_Order_Line_Value_Popup', 'Max. Orderline Value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Min_Order_Value_Popup', 'Min. order value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Max_Order_Value_Popup', 'Max. order value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Availability', 'Availability',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ItemDetails', 'Item Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AdditionalDetails', 'Additional Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ETA', 'ETA',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewExchangeOrderDetails_step_icon_lbl_200', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewPaymentDetails_step_icon_lbl_200', 'Payment Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsForEO_step_icon_lbl_210', 'Edit Payment Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_210', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editEOConfirmChangesStep_step_icon_lbl_210', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_total', 'Item(s) total',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unit_Price_Paid', 'Unit Price Paid',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax_Included', 'Tax Included',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_viewExchangeOrderDetails', 'Back To Exchange Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EndingWith', 'ending with',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Minimize_shipments', 'Minimize shipments',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merge_facility', 'Merge facility',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Valid_Billing_Information', 'Enter valid billing information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'rma_no', 'RMA No',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_editEOConfirmChangesStep', 'Back to EOPayment Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_Segment', 'Inventory Segment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Type', 'Allocation Type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mode', 'Mode',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Service_Level', 'Service Level',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ItemAddedToOrderMessage', 'Item is added to the order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CU', 'CU',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ReqCapacityPerUnit', 'Required capacity/unit',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BackorderedQty', 'Backordered Qty',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotionDetails_step_icon_lbl_220', 'Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippingInfo_step_icon_lbl_220', 'Shipping Info',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentDetails_step_icon_lbl_220', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reviewOrderStep_step_icon_lbl_220', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update_Additional_Details', 'Update Additional Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'alert_cannot_cancel_released_orderLine(s)', 'Cannot cancel Released Orderline(s)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Check_number', 'Check number',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Routing_number', 'Routing number',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account_number', 'Account number',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account_type', 'Account type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Businesss_name', 'Business name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Federal_tax_id', 'Federal tax ID',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Drivers_license_no', 'Driver''s license no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Drivers_license_state', 'Driver''s license state',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Drivers_license_country', 'Driver''s license country',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Choose_Address', 'Choose address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Bill_To', 'Bill To',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_Information', 'Card Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Credit_Card', 'Credit Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ECheck', 'E-Check',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Pending_Authorization', 'Payment Pending Authorization',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'same_as_shipping_address', 'Same as shipping address',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_Credit_Card', 'New Credit Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_Gift_Card', 'New Gift Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_ECheck', 'New E-Check',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_types', 'Customer type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Routing_number_is_required', 'Routing no. is a required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account_number_is_required', 'Account no. is a required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account_type_is_required', 'Account type is a required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_type_is_required', 'Customer type is a required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Missing_Drivers_license_number', 'Driver''s license no. is missing',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Missing_Drivers_license_sate', 'Driver''s license state is missing',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Missing_Drivers_license_country', 'Driver''s license country is missing',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Business_name_is_required', 'Business name is a required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Federal_tax_ID_is_required', 'Federal tax id is a required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CHECKING', 'Checking',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SAVINGS', 'Savings',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BusinessChecking', 'Business Checking',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account_Information', 'Account Information',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BUSINESS', 'Business',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INDIVIDUAL', 'Individual',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_Code_Description', 'Reason Description',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Echeck_Account_No', 'E-Check account no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirm_Removal', 'Confirm Removal',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RoutingTransitNumber', 'Routing number',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift_Card', 'Gift Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amount_Entered_Cannot_be_blank', 'Amount entered cannot be blank',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save_payment_information', 'Save before proceeding?',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Existing_accounts', 'Existing account(s)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_TO_PaymentDetailsForEO', 'Back to Payment Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Routing_number_not_valid', 'Routing no. not valid',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account_number_not_valid', 'Account no. not valid',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Check_no', 'Check no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Routing_no', 'Routing no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account_no', 'Account no.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expiry_date_is_required_field', 'Expiry date is required field',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expiry_date_cannot_be_less_than_today', 'Expiry date cannot be less than today',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Choose_an_account', '(Choose an account)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_a_new_account', 'Add a new account',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_a_new_card', 'Add a new card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_Type', 'DO Type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SO_Line_No', 'SO Line No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click_on_Add_icon_to_add_a_new_Credit_Card', 'Click on Add icon to add a new Credit Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click_on_Add_icon_to_add_a_new_ECheck', 'Click on Add icon to add a new E-Check',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click_on_Add_icon_to_add_a_new_Gift_Card', 'Click on Add icon to add a new Gift Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Do_You_Want_To_Delete', 'Do you want to delete it?',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override_price', 'Override price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_price_override_history', 'View price override history',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Price_Override_History', 'View Price Override History',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override_Price', 'Override Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Price_Override_History', 'Price Override History',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Current_price', 'Current price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Original_Price', 'Original Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_price', 'New price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason', 'Reason',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Comment', 'Comment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Revert_to_original_price', 'Revert to original price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_Price', 'New Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_No', 'Order No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'User', 'User',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Time', 'Time',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'This_order', 'This order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'All_orders', 'All orders',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_valid_current_price', 'Please enter a valid price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_code_is_required_field', 'Please select a reason',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_price_cannot_be_same_as_current_price', 'New price cannot be same as current price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_price_cannot_be_same_as_original_price', 'New price cannot be same as original price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'overridePriceStep_step_icon_lbl_230', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPromotionDetailsStep_step_icon_lbl_230', 'Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsStep_step_icon_lbl_230', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_230', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cannot_revert_price_as_price_is_not_overridden', 'Price cannot be reverted as it is not overridden yet',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Added_By', 'User',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Action', 'Action',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'date', 'Created Dttm',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flat_Amount', 'Flat  amount',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percent_Off', '% Off',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Dollar_Off', '$ Off',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_Code_Validate_Error', 'Please select the reason',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flat_Amount_Validate_Error', 'Flat amount value is required',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Dollar_Off_Validate_Error', '$ Off value is required',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percent_Off_Validate_Error', '% Off value is required',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No _Appeasements _Applied', 'No Appeasements Applied',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Choose_a_Flat_Appeasement', 'Choose a Flat Appeasement',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Choose_a_Reason_Code', 'Choose a Reason Code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override_Snh_Charges', 'Override Shipping and Handling Charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override_snh_charge', 'Override S&H charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override_SnH_Charge', 'Override S&H Charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_snh_charge_override_history', 'View S&H charge override history',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_SnH_Charge_Override_History', 'View S&H Charge Override History',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SnH_Charge_History_Tab', 'Charge Override History',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cannot_recal_snhcharge_as_snh_is_not_overridden', 'S&H charge cannot be recalculated as it is not overridden yet',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Recalc_to_original_snh_charge', 'Recalculate S&H charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Current_SnH_Charge', 'Current S&H charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_SnH_Charge', 'New Charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Original_Charge', 'Original Charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Overridden_Date', 'Overidden Date',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_price_greater_than_original_price', 'New price is greater than current price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_SnH_charge', 'New charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason_code', 'Reason code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_charge_cannot_be_same_as_current_charge', 'New charge cannot be same as current charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_a_valid_charge', 'Please enter a valid charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_charge_cannot_be_same_as_original_charge', 'New charge cannot be same as original charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_charge_cannot_be_greater_than_original_charge', 'New charge cannot be equal or greater than original charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override_Validation', 'Override Validation',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_description', 'Item description',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOConfirmChangesStep_step_icon_lbl_240', 'Confirm Changes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'overrideSnHChargeStep_step_icon_lbl_240', 'Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editPaymentDetailsStep_step_icon_lbl_240', 'Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Make_Payment', 'Make Payment',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'entered_value', 'Entered Value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'applied_value', 'Applied Value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'add_appeasement', 'Add Appeasement',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Level_Appeasements', 'Item List',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasement_Types', 'Appeasement Types',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Appeasement', 'Line Appeasement',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Appeasements', 'Line Appeasements',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Flat_Appeasements_Defined', 'No flat appeasements defined',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RS', 'Recalculate S&H charges',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MD', 'Manual mark down',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CM', 'Competitor''s charge match',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override_Shipping_Charges', 'Override Shipping Charges',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Completed_Externally', 'Order processed externally',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO_Line_No', 'CO Line No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_Line_Qty', 'DO Line Quantity',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Show_item_prices_for_Store', 'Show item prices for store',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store_Price', 'Store Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Web_Price', 'Web Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store_Name', 'Store Name',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCOAppeasementStep_step_icon_lbl_190', 'Appeasements',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Error_Code', 'Error Code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Order_Line', 'No Order Line available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pending_payments_to_be_made', 'There are pending payments to be made',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percent_Off_Exceed_Error', '% Off value cannot exceed 100',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Dollar_Off_NUM_Validate_Error', '$ Off should be a numeric value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Appeasements', 'Order Appeasements',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percent_Off_Num_Validate_Error', '% Off should be a numeric value',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OT', 'Others',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SD', 'Shipment delayed',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'add_discount_code', 'Add discount code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_status_not_applied', 'Not applied',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_codes', 'Discount Codes',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'no_discount_codes_available', 'No discount codes available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_code', 'Discount Code',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'remove_all', 'Remove All',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_details', 'Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_data_not_available', 'No data available.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_status_for_the_order', 'for the order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_status_for_the_items', 'for the items',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_code_item_wise_breakdown', 'Item-wise Breakdown',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_status', 'Discount Status',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_Not_Present', 'Customer not present in system',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'inventoryOnHand', 'Inventory is available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'inventoryNotAvailable', 'Inventory is not available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'inventoryInTransit', 'Inventory is in transit',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Error_occurred_during_external_system_processing', 'Error occurred during external system processing',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'refresh_prices', 'Refresh Prices',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'find_store', 'Find Store',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Error_Description', 'Error Description',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Appeasements_Applied', 'No appeasements applied',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_overrides_on_Item_Price', 'No overrides on item price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_overrides_on_Shipping_Handling_charge', 'No overrides on Shipping & Handling charge',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Not_Applicable', 'Not Applicable',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NA', 'NA',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override_Date', 'Override Date',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'no_delivery_options_available', 'No delivery options available',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search_offered_items', 'Search offered items',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_editCOAppeasementStep', 'Back to Appeasements',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return_Order', 'Return Order(s)',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total', 'Total',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Added_by', 'Added by',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Added_on', 'Added on',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Applied_by', 'Applied by',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Applied_on', 'Applied on',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Offered_line', 'Offered line',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flat_Type', 'Flat  type',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Do_You_Want_To_Delete_Selected_Record', 'Do you want to delete the selected record(s)?',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BackorderReason', 'Backorder reason',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Not Applicable', 'Not Applicable',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return Charges', 'Return charges',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override Price', 'Override Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override S&H', 'Override S&H',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Price', 'Price',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Trace', 'Allocation Trace',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer Communications', 'Customer Communications',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasements', 'Appeasements',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution Order Details', 'Distribution Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_overridePriceStep', 'Back to Order Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back_To_editAdditionalDetailsStep', 'Back to Edit Additional Details',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Non_exclusive_promotion', 'Can be combined with any Promotions',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Entity_level_exclusive_promotion', 'This Promotion may not be combined with other Promotions for an order',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Globally_exclusive_promotion', 'If multiple promotions are available for this Order then only one of the Promotions can be applied',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PAYPAL_ACCOUNT', 'PayPal account',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PayPal', 'PayPal',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Routing number', 'Routing No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Account number', 'Account No.',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ending with', 'ending with',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card', 'Card',
           'CustomerOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updated_On', 'Updated On',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updated_By', 'Updated By',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_No', 'Line No.',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updated_To', 'Updated To',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line', 'Line',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_On', 'Created On',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updated_From', 'Updated From',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_By', 'Created By',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attribute_Name', 'Attribute Name',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Audit_Summary', 'Audit Summary',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Header', 'Header',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Header_Or_Line', 'Header/Line',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Name', 'Item Name',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Description', 'Item Description',
           'CustomerOrderAuditDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_DC_ALLOCATED', 'Partially DC Allocated',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RELEASED', 'Released',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC_ALLOCATED', 'DC Allocated',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_RELEASED', 'Partially Released',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPED', 'Shipped',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_CREATED', 'DO Created',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION_FAILED', 'Allocation Failed',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CANCELED', 'Canceled',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED', 'Scheduled',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCING_FAILED', 'Sourcing Failed',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BACK_ORDERED', 'Back Ordered',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_SHIPPED', 'Partially Shipped',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OPEN', 'Created',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CREATED', 'Created',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATED', 'Allocated',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVALID', 'Invalid',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULING_FAILED', 'Scheduling Failed',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCED', 'Sourced',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SUBSTITUTED', 'Substituted',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_PICKED-UP', 'Partially Picked Up',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PICKED-UP', 'Picked Up',
           'CustomerOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_DC_ALLOCATED', 'Partially DC Allocated',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_SOURCED', 'Partially Sourced',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RELEASED', 'Released',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC_ALLOCATED', 'DC Allocated',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_RELEASED', 'Partially Released',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_PARTIALLY_CREATED', 'DO Partially Created',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_ALLOCATED', 'Partially Allocated',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPED', 'Completed',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_CREATED', 'DO Created',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION_FAILED', 'Allocation Failed',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CANCELED', 'Canceled',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCING_FAILED', 'Sourcing Failed',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_SHIPPED', 'Partially Completed',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OPEN', 'Created',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CREATED', 'Created',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATED', 'Allocated',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVALID', 'Invalid',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'READY_FOR_ALLOCATION', 'Ready for Allocation',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULING_FAILED', 'Scheduling Failed',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCED', 'Sourced',
           'CustomerOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Discrepancy_Manager', 'Data Consistency Management',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FindFacility', 'Find Facility',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Configuration', 'Configuration',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'next_run', 'Next Run',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduler', 'Scheduler',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Report', 'Report',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'History', 'History',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility', 'Facility',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'System_Type', 'System type',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WM_DBString', 'WM Database configuration',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Machine_IP', 'Instance name',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Remote_Env', 'Hostname I/P',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Path_On_Remote_Machine', 'Flat file location',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_Username', 'Username',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_Password', 'Password',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Local_Script_Directory', 'Local Script Directory',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduled_On', 'Scheduled on:',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Start_Time', 'Start time:',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click_here_to_select_time_from_popup_Time_Entry', 'Click here to select time from popup Time Entry',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Every', 'Run every',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '(1-23) Hours', '(1-23) Hours',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'End_After', 'End after',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduled day', 'Scheduled day',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Runs', 'Runs',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Monday', 'Monday',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thursday', 'Thursday',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tuesday', 'Tuesday',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Friday', 'Friday',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Wednesday', 'Wednesday',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Saturday', 'Saturday',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sunday', 'Sunday',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run', 'Run',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Id', 'Run ID',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Status', 'Status',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Started', 'Started',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ended', 'Ended',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Open_Variances', 'No of Variances',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fixed_Variances', 'Fixed Variances',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply', 'Apply',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Run', 'Last run',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'In Progress',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Failed',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DUPLICATE', 'Duplicate',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Succeeded',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SUCCEEDED', 'Succeeded',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INPROGRESS', 'In Progress',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'QUEUED', 'Queued',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED', 'Failed',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution_Order', 'Distribution Orders',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_ID', 'DO No.',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_LineNo', 'DO Line',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'item_desc', 'Item Description',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'wm_status', 'WM Status',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'dom_status', 'DOM Status',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipped_qty_wm', 'Ship Qty WM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipped_qty_dom', 'Ship Qty DOM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'canceled_qty_wm', 'Canceled Qty WM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'canceled_qty_dom', 'Canceled Qty DOM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'last_updated_DTTM', 'Last Updated',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'no_of_ASNs', 'No. of ASNs',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'no_of_LPNs', 'No. of LPNs',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '(', '(',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, ')', ')',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'items_in_wrong_status', 'items in wrong status',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'items_in_wrong_quantity', 'items with wrong quantity',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'items_in_other_variance', 'items with other variances',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN', 'Advance Shipment Notices',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN_No', 'ASN No.',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO_No', 'PO No.',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'wm_avail_qty', 'Available Qty WM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'dom_avail_qty', 'Available Qty DOM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Purchase_Order', 'Purchase Orders',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO_LineNo', 'PO Line',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'wm_orig_qty', 'Original Qty WM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'dom_orig_qty', 'Original Qty DOM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory', 'Inventory',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'dom_unavail_qty', 'Unavailable Qty DOM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'wm_unavail_qty', 'Unavailable Qty WM',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sync', 'Sync',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sync_All', 'Sync All',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Run_Start_Time', 'Please enter run start time',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Every_Should_Be_Between_1_and_23', 'Please enter a no. between 1 and 23',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fixed', 'Fixed',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Additional_runs', 'Additional runs',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'hrs', 'hrs',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DOM_DBString', 'DOM Database configuration',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirm_Sync_All', 'Are you sure you want to sync all the records?',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_at_least_one_DO_Line_Item_from_List', 'Please select at least one DO Line Item from List',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_Webservice_details', 'Login webservice details',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'End_Point_Reference', 'End point reference',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Target_Namespace', 'Target namespace',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Service_Name', 'Service name',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Operation_Name', 'Operation name',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Environment_Code', 'Environment code',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UserName', 'Username',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Password', 'Password',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sync_Webservice_details', 'Sync webservice details',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reset', 'Reset',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'System_type_is_a_required_field', 'System type is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_end_Point_reference_is_a_required_field', 'Login end Point reference is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_target_namespace_is_a_required_field', 'Login target namespace is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_service_name_is_a_required_field', 'Login service name is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_operation_name_is_a_required_field', 'Login operation name is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_environment_code_is_a_required_field', 'Login environment code is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_username_is_a_required_field', 'Login username is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Login_password_is_a_required_field', 'Login password is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sync_end_Point_reference_is_a_required_field', 'Sync end Point reference is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sync_target_namespace_is_a_required_field', 'Sync target namespace is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sync_service_name_is_a_required_field', 'Sync service name is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sync_operation_name_is_a_required_field', 'Sync operation name is a required field',
           'DiscrepancyManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Open',
           'DiscrepancyStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Fixed',
           'DiscrepancyStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Open', 'Open',
           'DiscrepancyStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fixed', 'Fixed',
           'DiscrepancyStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Quantity',
           'DiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Other',
           'DiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity', 'Quantity',
           'DiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other', 'Other',
           'DiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Released',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '20', 'DC Allocated',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '30', 'Partially Shipped',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '40', 'Shipped',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '50', 'Canceled',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '60', 'Partially DC Allocated',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Released', 'Released',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC Allocated', 'DC Allocated',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Shipped', 'Partially Shipped',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped', 'Shipped',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Canceled', 'Canceled',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially DC Allocated', 'Partially DC Allocated',
           'DistributionOrderDOMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Released',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '20', 'DC Allocated',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '30', 'Partially Shipped',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '40', 'Shipped',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '50', 'Canceled',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '60', 'Partially DC Allocated',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Released', 'Released',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC Allocated', 'DC Allocated',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Shipped', 'Partially Shipped',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped', 'Shipped',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Canceled', 'Canceled',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially DC Allocated', 'Partially DC Allocated',
           'DistributionOrderWMStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Putaway',
           'DNAFinalizerStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Even days of supply',
           'DNAFinalizerStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO', 'PO',
           'DNAInventoryType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN', 'ASN',
           'DNAInventoryType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Status',
           'DODiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Quantity',
           'DODiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Other',
           'DODiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'DODiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity', 'Quantity',
           'DODiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other', 'Other',
           'DODiscrepancyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Schedule Details', 'Schedule Details',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Delivery Schedule List found', 'No Delivery Schedule List found',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Operator*', '*Operator',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prioritize_facilities_tier', 'Prioritize Facilities In a Tier By',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FulfillmentTier', 'Fulfillment Tier',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FacilitySour', 'Facility(s)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'rank_required', 'Rank is a required field',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination Facility', 'Destination Facility',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Period_Start', 'Period Start',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tue_Max_Cap', 'Maximum Capacity(Tue)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GP', 'Geo proximity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source*', '*Source',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Remove', 'Remove',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RankHighest', 'Rank highest>>',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New', 'New',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference Line ID', 'Reference Line ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Capacity_Value_cannot_be_empty', 'Capacity Value cannot be empty',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'low', 'low',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'sourcing_rule_rank_should_be_positive', '"Rank" should be a positive integer',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RankingMethodology', 'Ranking Methodology',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item', 'Item',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Normal', 'Normal',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Supply Found', 'No Supply Found',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MultipleUpdateMsg', 'Enter fulfillment end time  and maximum capacity for selected facilities',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Actions', 'Actions',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select at least one Delivery Schedule Detail for deletion.', 'Please select at least one delivery schedule detail for deletion.',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FulfillmentPerformed', 'Fulfillment Performed',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Need Type', 'Need Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ETA', 'ETA',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LessThan', 'Less than',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reset', 'Reset',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Segment', 'Inventory Segment',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Location_Needs', 'Item Location Needs',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution Order List', 'Distribution Order List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'group_nbr_required_on_row', '"Group no." is required on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete_Source', 'Delete Source',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FulfillmentTierSour', 'Fulfillment tier(s)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Type', 'Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'There should be at least one Delivery Schedule Detail Record.', 'There should be at least one delivery schedule detail record.',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_only_one_facility_to_update', 'Please select only one facility to update',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Order Line List', 'Sales Order Line List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Setup', 'Setup',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to delete the selected Delivery Schedule(s) ?', 'Are you sure that you want to delete the selected delivery schedule(s)?',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Operator', 'Operator',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'rank_required_on_row', '"Rank" is required on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate', 'Allocate',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Un-Allocatable Quantity', 'Un-Allocatable Quantity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Id', 'Supply ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'duplicate_target_pcnts_on_rows', 'Duplicate combination of "Group no.", "Rank", "Product Class", "Store Type" and "Priority Group" on rows',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click here to select date and time from popup calendar', 'Click here to select date and time from popup calendar',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Low', 'Low',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manual_Min', 'Manual Min',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tuesday', 'Tuesday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Selection', 'Order Selection',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Capacity_Value_should_be_a_positive_Integer', 'Capacity Value should be a positive Integer',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Wed_Max_Cap', 'Maximum Capacity(Wed)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Id', 'Order ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RakingMethodology', 'Ranking methodology',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thursday', 'Thursday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click here to select Source', 'Click here to select Source.',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cannot_delete_all_sourcing_options', 'At least one "Source from" is required',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete', 'Delete',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Sales Order Line Found', 'No Sales Order Line found',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On Sales Order', 'On Sales Order',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CU', 'CU',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NONE', '(none)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Details', 'Details',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing Rule', 'Sourcing Rules',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select a row', 'Please select atleast one row.',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Wednesday', 'Wednesday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested Delivery', 'Requested Delivery',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inactive', 'Inactive',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply List', 'Supply List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UOM', 'UOM',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'click_to_select_source', 'Click here to select Source',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cut Off Time', 'Cutoff Time',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attribute Name', 'Attribute Name',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Calendar', 'Calendar',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Line', 'Order Line',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Service Level', 'Service Level',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Days', 'Days',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MaximumCapacity', 'Maximum Capacity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WorkSpace', 'WorkSpace',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'please_select_one_sourcing_option', 'Please select at least one Sourcing Option',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Need_Type_Code', 'Need type code is a required field',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_Source', 'Add Source',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Need_Type', 'Need type  is a required field',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Time', 'Time',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Quantity', 'Allocated Quantity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Watermark_Quantity', 'Watermark Quantity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line ID', 'Line ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attribute Value', 'Attribute Value',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Use_for_watermarking', 'Use For Watermarking',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thur_Max_Cap', 'Maximum Capacity(Thu)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit', 'Edit',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UpdateCapMsg', 'Enter fulfillment end time time and maximum capacity for selected facility',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add Another', 'Add Another',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attribute', 'Attribute',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'please_select_one_rule_element', 'Please select at least one Order Selection',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing Options', 'Sourcing Options',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attribute Name*', '*Attribute Name',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PerformMultipleUpdates', 'Perform Multiple Updates',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'target_pcnt_should_be_less_than_100%_on_row', '"Target %" should be less than or equal to 100% on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Critical', 'Critical',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Value', 'Value',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Create', 'Create',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is Virtual Allocation', 'Virtual Allocation',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'please_enter_a_value_for_either_product_class_store_type_or_priority_group', 'Please select a value for either "Product Class" or "Store Type" or "Priority Group" on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AND', 'And',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Product Class', 'Product Class',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View Sourcing Rule', 'View Sourcing Rule',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Date List', 'Date List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Need Type Code', 'Need Type Code',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hours', 'Hours',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total Quantity', 'Total Quantity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name', 'Name',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'if_rule_grp_priority_combination_is_same_ftp_should_be_same', 'Please Enter Same "Target %", since another row has same "Rank"',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Band_Selection', 'Band Selection',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Order Type', 'Sales Order Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Management', 'Order Management',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RankLower', 'Rank lower<',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Status', 'Supply Status',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Warehouse Facility', 'WH Facility',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated SKU', 'Allocated SKU',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order without an order line', 'Cannot confirm or reserve an order without an order line',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UpdateCapacity', 'Update Capacity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Recurring', 'Recurring',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Type', 'Order Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule Group', 'Rule Group',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'rank_should_be_positive_on_row', '"Rank" should be a positive integer on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Latest Release By Date', 'Latest Release By Date',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Delivery Schedule', 'New Delivery Schedule',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply', 'Supply',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Order ID', 'Sales Order ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add', 'Add',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fill_Target_Percentage_Page_Header', 'Target Percentage Reapportionment',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'rank_should_be_positive', 'Rank should be a positive integer',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery Schedule List', 'Delivery Schedule List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule Rank', 'Rank',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'High Rotation Volume', 'High Rotation Volume',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Detail Id', 'Supply Detail ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click here to select Destination', 'Click here to select a destination',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Allocation List', 'Order Allocation List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Lead Time', 'Lead Time',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Type', 'Inventory Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule Element List', 'Rule Element List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination State', 'Destination State',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility_Information', 'Facility Information',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flow_Through', 'Flow Through',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule Name*', 'Rule Name*',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ACTIONS_MENU', 'Actions',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View Allocation List', 'View Allocation List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing Results', 'Sourcing Results',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested Quantity', 'Requested Quantity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sun_Max_Cap', 'Maximum Capacity(Sun)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manual_Max', 'Manual Max',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are_you_sure_you_want_to_delete_the_selected_rule(s)_?', 'Are you sure you want to delete the selected rule(s) ?',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Information', 'Item Information',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule_Name', 'Rule Name',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Type', 'Allocation Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Putaway', 'Putaway',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inaugral', 'Inaugural',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Origin', 'Origin',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete_Band', 'Delete Band',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation', 'Allocation',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'attribute_value_required', '"Attribute Value" is a required field on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority within Rule Group', 'Priority within Rule Group',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested SKU', 'Requested SKU',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source', 'Source',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Uncheck All', 'Clear All',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facilities', 'Facilities',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Detail ID', 'Inventory Detail ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manufacturing Date', 'Manufacturing Date',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Friday', 'Friday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_Attribute', 'Add Attribute',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MultiLangRegExp', '^[A-Za-z]*$',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer ID', 'Customer ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINKS_MENU', 'Links',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click here to select time from popup', 'Click here to select time from popup',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sat_Max_Cap', 'Maximum Capacity(Sat)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Forecast_policy', 'Forecast policy',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fill_Target', 'Target %',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Need Types', 'Need Types',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_facilities_to_update', 'Please select facilities to update',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete_Attribute', 'Delete Attribute',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sunday', 'Sunday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Day Of Week', 'Day of Week',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'sourcing_rule_rank_required', '"Rank" is a required field',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rank*', '*Rank',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '% of selected need type', '% of selected need type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Substitution Type', 'Substitution Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No allocations found', 'No allocations found',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Value_can_not_be_empty', 'Value(s) can not be empty',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Status', 'Inventory Status',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference ID', 'Reference ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type', 'Supply Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination_Facility', 'Destination Facility',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source Type', 'Source Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority Group', 'Priority Group',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Network/Route SetUp', 'Network/Route Setup',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'High', 'High',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FacilityInformation', 'Facility Information',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FacilityId', 'Facility Id',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add row', 'Add Row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Use_for_watermarking_SC', 'Use for watermarking',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Sourcing Rule', 'New Sourcing Rule',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Carrier Code', 'Carrier Code',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Never Out', 'Never Out',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid_Need_Type', 'Invalid need type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Need Type Code_SC', 'Need Type code',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory ID', 'Inventory ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_atleast_one_band', 'Add atleast one band',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RankLowest', 'Rank lowest<<',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Confirmed', 'Order Confirmed',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Parent Line ID', 'Parent Line ID',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DOM Process Template', 'DOM Process Template',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Calculate_inventory_watermark_as_SC', 'Calculate inventory watermark as',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination Type', 'Destination Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Delivery Schedule', 'Edit Delivery Schedule',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested Date', 'Requested Date',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment', 'Segment',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule Rank*', 'Rule Rank*',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select at least one Delivery Schedule for deletion', 'Please select at least one delivery schedule for deletion',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Zone', 'Zone',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Effective Date', 'Effective Date',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Earliest Ship By Date', 'Earliest Ship By Date',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ManualRanking', 'Manual Ranking of Facilities',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Order Line Status', 'Sales Order Line Status',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Close', 'Close',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_Band', 'Add Band',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OR', 'Or',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SELECT_ONE', '(select one)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'attribute_name_required', '"Attribute" is a required field on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In Region', 'In Region',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preferred WH Facility', 'Preferred WH Facility',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Committed Ship Date', 'Committed Ship Date',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Day', 'Day',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SKU_Location_Needs_List', 'SKU Location Needs List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DistanceBand', 'Distance Bands Definition',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_Rule_for_deletion', 'Please select atleast one Rule for deletion.',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid_Need_Type_Code', 'Invalid need type code',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'sourcing_rule_name_required', '"Rule Name" is a required field',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Capacity_Value_cannot_be_a_decimal_number', 'Capacity Value cannot be a decimal number',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'target_pcnt_required_on_row', '"Target %" is required on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Group_no.', 'Group no.',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution Center List', 'Distribution Center List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MR', 'Manual ranking',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Applicable only for Recurring Type Schedules', 'Applicable only for Recurring Type Schedules',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UOM_Can_Not_Be_Different', 'UOMs should be same for all bands',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manage_Daily_Capacity', 'Manage Daily Capacity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source_From', 'Source From',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule Name', 'Rule Name',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Saturday', 'Saturday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Monday', 'Monday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EndTime', 'Fulfillment End Time',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'target_pcnt_should_be_positive_decimal_on_row', '"Target %" should be a positive decimal on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Sourcing Rule', 'Edit Sourcing Rule',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_Enter_valid_Fulfillment_End_Time', 'Please Enter Valid Fulfillment End Time',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Detail', 'Supply Detail',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Medium', 'Medium',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Needs Types', 'Needs Types',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity', 'Quantity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO Id', 'PO Id',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SKU', 'SKU',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Higher_Band_Value_Can_Not_Be_Less', 'Higher band value can not be less than lower band value',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination Zip', 'Destination Zip',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RankHigher', 'Rank higher>',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Forecast', 'Forecast',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attribute Value*', '*Attribute Value',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule Description', 'Description',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Logical Operator', 'Logical Operator',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority', 'Priority',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Error', 'Error',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Data List', 'Data List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Band_Value_Can_Not_Be_Equal', 'Values can not be equal for two bands',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Daily_Capacity', 'View Daily Capacity',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Time_Entry', 'Click here to select time from popup Time Entry',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Check All', 'Check All',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rank', 'Rank',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '7', 'Saturday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Multiple', 'Allocation Multiple',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '6', 'Friday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'group_nbr_should_be_positive_on_row', '"Group no." should be positive integer on row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '5', 'Thursday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '4', 'Wednesday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'Tuesday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Monday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Sunday',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expiration Date', 'Expiration Date',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View Distribution Order List', 'View Distribution Order List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourced', 'Sourced',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Schedule Type', 'Schedule Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store Type', 'Store Type',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility', 'Facility',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fri_Max_Cap', 'Maximum Capacity(Fri)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Forecast_Details', 'Forecast Details',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_a_facility_to_update', 'Please select a facility to update',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Filter_Name', 'Filter Name',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Order List', 'Sales Order List',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cannot_delete_all_rule_elements', 'At least one "Order Selection" is required',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_more_than_one_facility_to_update', 'Please select more than one facility to perform multiple update',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Latest Ship By Date', 'Latest Ship By Date',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mon_Max_Cap', 'Maximum Capacity(Mon)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mode', 'Mode',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'facility_required', 'Please select a Facility in row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'fulfillment_required', 'Please select a fulfillment tier in row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment_Tier_Name_Empty', 'Please select a tier in this row',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery Options', 'Delivery Options',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'N/A', 'N/A',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'hh', 'hh',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'mm', 'mm',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Band', 'Band',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Lead_Time_In_Hrs', 'Lead Time (hrs)',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Week', 'Week',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DS Company Parameters', 'DS Company Parameters',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution_Order_Creation', 'DOCreate',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution_Order_Release', 'DORelease',
           'DOM')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RO_EVENT_DELETED', 'Supply Chain Optimizer event {0} deleted',
           'DOMActionMessages')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'dispatch.error', 'dispatch error',
           'DOMActionMessages')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OP_EVENT_DELETED', 'Order Promiser event {0} deleted',
           'DOMActionMessages')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EXCEPTION_FILL_BY_OVERBOOK', 'Exception Filled via overbook',
           'DOMActionMessages')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EXCEPTION_FILL_BY_SUBSTITUTION', 'Exception Filled via product substitution {0} by {1}',
           'DOMActionMessages')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVALID_TOKEN', 'invalid token or form already submitted',
           'DOMActionMessages')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Raise alerts for In Store Pick Up Order Lines with pending pick up status for more than x minutes', 'Raise alerts for In Store Pick Up Order Lines with pending pick up status for more than x minutes',
           'DOMAlertName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Pick Up', 'Pending Pick Up',
           'DOMAlertName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Line Fulfillment Type update Alert', 'DO Line Fulfillment Type update Alert',
           'DOMAlertName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Processing', 'Pending Processing',
           'DOMAlertName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Raise alerts for In Store Pick Up Order Lines with pending processing status for more than x minutes', 'Raise alerts for In Store Pick Up Order Lines with pending processing status for more than x minutes',
           'DOMAlertName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item Price Expiry Date Extended Alert', 'Item Price Expiry Date Extended Alert',
           'DOMAlertName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Availability', 'Pending Availability',
           'DOMAlertName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Raise alerts for In Store Pick Up Order Lines with pending availability status for more than x minutes', 'Raise alerts for In Store Pick Up Order Lines with pending availability status for more than x minutes',
           'DOMAlertName')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Credit Card Type',
           'DOMBaseData')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'startDateTime', 'Started',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Interface Errors', 'Interface Errors',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Purchase Orders', 'Purchase Orders',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Orders', 'Sales Orders',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility', 'Facility',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Object Type', 'Object Type',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name', 'Scheduler Name',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Locks', 'No.of locks',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order line cancellation', 'Order line cancellation',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Object Lock', 'Object Locks',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Interface_Errors', 'Interface Errors',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Interface', 'Interface Name',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ATP', 'Allocation monitor',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'runId', 'Run ID',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Job Monitor', 'Job Monitor',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ActiveJobsPanel', 'Active/Scheduled Jobs',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'name', 'Template Name',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory', 'Inventory',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Error Monitor', 'Error Monitor',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'submittedDateTime', 'Submitted Start',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'actualDateTime', 'Actual Start',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source exclusion event handler', 'Source exclusion event handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory rise handler', 'Inventory rise handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item', 'Item',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Clear', 'Clear',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduledstart', 'Scheduled start',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'System Monitor', 'System Monitor',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'JobsHistoryPanel', 'Jobs History',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Service', 'Order Service',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DashBoard', 'DashBoard',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Advance Shipment Notices', 'Advanced Shipment Notices',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'status', 'Status',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduler Name', 'Scheduler Name',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory drop handler', 'Inventory drop handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply', 'Apply',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Re-allocation handler', 'Re-allocation handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Dash_Board', 'Dashboard',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ActualStart', 'Actual Start',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Activate', 'Activate',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DashBoardDetails', 'System Monitor',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduler Monitor', 'Scheduler Monitor',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OrderLine', 'OrderLine',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Next Run', 'Next run',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Previous Run', 'Previous run',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation', 'Allocation',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'completedDateTime', 'Ended',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Refresh', 'Refresh',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Errors', 'No.of errors',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DeActivate', 'De-activate',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DashBoard Details', 'DashBoard Details',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO', 'Distribution Order',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory alert handler', 'Inventory alert handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Multi echelon handler', 'Multi echelon handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Authorization handler', 'Authorization handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Re-Authorization handler', 'Re-Authorization handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Settlement handler', 'Settlement handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Refund handler', 'Refund handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Price expiry handler', 'Price expiry handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Down', 'Down',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Active', 'Active',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduled', 'Scheduled',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Not Applicable', 'Not Applicable',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Orderlines', 'Sales Orderlines',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Force Payment TransactionStatus Update handler', 'Force Payment TransactionStatus Update handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Workflow Retry handler', 'Workflow Retry handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Order Alert handler', 'Fulfillment Order Alert handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Key Recycle handler', 'Payment Key Recycle handler',
           'DOMDashBoard')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Acknowledge_Alert', 'Acknowledge Alert',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Audit', 'Audit',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created Date', 'Created date',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected_order(s)_cannot_be_{0}_due_to_ineligible_status', 'Selected order(s) cannot be {0} due to ineligible status',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ItemDescription', 'Item Description',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Pick Up', 'Pending Pick Up',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Clear Inventory Search', 'Clear Inventory Search',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to save changes?', 'Are you sure that you want to save changes?',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirm Pick Up', 'Confirm Pick Up',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search Order', 'Search Order',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Details', 'Order Details',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Clear Order Search', 'Clear Order Search',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Lastupdated', 'Last Updated',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcetype', 'Source Type',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Total', 'Order Total',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Header_Line', 'Header Line',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure you want to cancel selected line(s)?', 'Are you sure you want to cancel selected line(s)?',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return To Order List', 'Return To Order List',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updated_On', 'Updated On',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Count', 'Count',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Print', 'Print',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Pickup', 'Pending Pickup',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updated_By', 'Updated By',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available In Transit', 'Available In Transit',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select atleast one Order', 'Please select atleast one Order',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total In Store Pick-Up Orders', 'Total In Store Pick-Up Orders',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add', 'Add',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert Message', 'Alert Message',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NoAuditTrail', 'No Audit Trail',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Acknowledge Alert', 'Acknowledge Alert',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search', 'Search',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attribute_Name', 'Attribute Name',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert Notes', 'Alert Notes',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In Store Pick-Up Orders', 'In Store Pick-Up Orders',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last Name', 'Last name',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item', 'Item',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert List', 'Alert List',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updated_To', 'Updated To',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Total', 'Order Total',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store ID', 'Store ID',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store Details', 'Store Details',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NoteType', 'Note Type',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Availability', 'Pending Availability',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search Inventory', 'Search Inventory',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Available', 'Partially Available',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updatedon', 'Updated On',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pick-Up Order Summary', 'Pick-Up Order Summary',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer Details', 'Customer Details',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updated_From', 'Updated From',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search Alert', 'Search Alert',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No available quantity is being entered yet.', 'No available quantity is being entered yet.',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last Modified Date And', 'Last Modified Date And',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure you want to cancel entire order?', 'Are you sure you want to cancel entire order?',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to acknowledge the selected alert(s)?', 'Are you sure that you want to acknowledge the selected alert(s)?',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone No.', 'Phone No.',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last Modified Date Between', 'Last Modified Date Between',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In Store Pick-Up Order Details', 'In Store Pick-Up Order Details',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Qty', 'Order Qty',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Availability', 'Inventory Availability',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'City/State', 'City/State',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AuditSummary', 'Audit Summary',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_No', 'Line No',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CityState', 'City/State',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel Line', 'Cancel Line',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes', 'Notes',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Zipcode', 'Zip Code',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert Type', 'Alert Type',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Open', 'Open',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'And', 'And',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirm Availability', 'Confirm Availability',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to cancel the selected order(s) ?', 'Are you sure that you want to cancel the selected order(s) ?',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available On Hand', 'Available On Hand',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No. of Order Lines', 'No. of Order Lines',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Processing', 'Pending Processing',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available', 'Available',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address', 'Address',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirm Pick-Up', 'Confirm Pick Up',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'StateUS', 'State',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Clear', 'Clear',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Status', 'Line Status',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Picked Up', 'Picked Up',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_at_east_one_Alert', 'Please select at east one Alert',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Total', 'Line Total',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Createdon', 'Created on',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'alertDetails', 'Alert Details',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created', 'Created',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to delete existing notes?', 'Are you sure that you want to delete existing notes?',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel Order', 'Cancel Order',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Create Date', 'Create Date',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AuditTrail', 'Audit Trail',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Line Information', 'Order Line Information',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to acknowledge the alert', 'Are you sure that you want to acknowledge the alert',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process_Order', 'Process Order',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last Modified', 'Last Modified',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item List', 'Item List',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone', 'Phone',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name', 'Name',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Notes', 'Order Notes',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'canceled', 'canceled',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'First Name', 'First name',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert Summary', 'Alert Summary',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected_alert(s)_cannot_be_{0}_due_to_ineligible_status', 'Selected alert(s) cannot be {0} due to ineligible status',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In Process', 'In Process',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order No.', 'Order no.',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unit Price', 'Unit Price',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line Total', 'Line Total',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select at least one line to cancel', 'Please select at least one line to cancel',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add Notes', 'Add Notes',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ETA', 'ETA',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source', 'Source',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Note Sequence', 'Note Sequence',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert Status', 'Alert Status',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancelled', 'Canceled',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete', 'Delete',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available Qty', 'Available Qty',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to acknowledge the alert?', 'Are you sure that you want to acknowledge the alert?',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Status', 'Order Status',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'State', 'State',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process Order', 'Process Order',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line No.', 'Line No.',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order List', 'Order List',
           'DOMInStorePickUp')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Onhand', 'On Hand',
           'DOMInventoryType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO', 'PO',
           'DOMInventoryType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN', 'ASN',
           'DOMInventoryType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InStore', 'In Store',
           'DOMInventoryType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid', 'Invalid',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created', 'Created',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Sourced', 'Partially Sourced',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourced', 'Sourced',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing Failed', 'Sourcing Failed',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduling Failed', 'Scheduling Failed',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Allocated', 'Partially Allocated',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated', 'Allocated',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Failed', 'Allocation Failed',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Released', 'Partially Released',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Released', 'Released',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially DC Allocated', 'Partially DC Allocated',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC Allocated', 'DC Allocated',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Completed', 'Partially Completed',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Completed', 'Completed',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Canceled', 'Canceled',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Partially Created', 'DO Partially Created',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Created', 'DO Created',
           'DOMOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created', 'Created',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid', 'Invalid',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourced', 'Sourced',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing Failed', 'Sourcing Failed',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduling Failed', 'Scheduling Failed',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated', 'Allocated',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Failed', 'Allocation Failed',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Back Ordered', 'Back Ordered',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Released', 'Partially Released',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Released', 'Released',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially DC Allocated', 'Partially DC Allocated',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC Allocated', 'DC Allocated',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Shipped / Partially Picked-up', 'Partially Shipped / Partially Picked-up',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped / Picked-up', 'Shipped / Picked-up',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Canceled', 'Canceled',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Created', 'DO Created',
           'DOMOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution_Order_Release', 'DO Release',
           'DOMProcessType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales_Order_Processing', 'Sales Order',
           'DOMProcessType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation', 'Allocation',
           'DOMProcessType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Prioritization', 'Prioritization',
           'DOMProcessType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InvSegmentation', 'Inventory Segmentation',
           'DOMProcessType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution_Order_Creation', 'DO Create',
           'DOMProcessType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'In Progress',
           'DOMRunStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Failed',
           'DOMRunStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DUPLICATE', 'Duplicate',
           'DOMRunStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Succeeded',
           'DOMRunStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SUCCEEDED', 'Succeeded',
           'DOMRunStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INPROGRESS', 'In Progress',
           'DOMRunStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'QUEUED', 'Queued',
           'DOMRunStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED', 'Failed',
           'DOMRunStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '60', 'Store Transfer',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '30', 'Warehouse Transfer',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '-1', '(select one)',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '50', 'Putaway',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '20', 'Customer Order',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '70', 'Buy Request',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '40', 'Return to Business Partner',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Store Distributions',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '(select one)', '(select one)',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '80', 'Retail Shipment',
           'DistributionOrderType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Phone No.',
           'ExternalCustomerIDGenerationStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Email',
           'ExternalCustomerIDGenerationStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Auto Generation',
           'ExternalCustomerIDGenerationStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Email', 'Email',
           'ExternalCustomerIDGenerationStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Auto Generation', 'Auto Generation',
           'ExternalCustomerIDGenerationStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Phone No.', 'Phone No.',
           'ExternalCustomerIDGenerationStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pick_up_at_store', 'Pick Up in Store',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship_to_store', 'Ship to Store',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'store_Locator', 'Store Locator',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pickFromStoreNotSupportedByItem', 'Item does not support this delivery option',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipToStoreNotSupportedByItem', 'Item does not support this delivery option',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pickFromStoreNotSupportedByFacility', 'Store does not support this delivery option',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipToStoreNotSupportedByFacility', 'Store does not support this delivery option',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemNotAvailableForStorePickup', 'Item is not available for store pick up',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item', 'Item',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UOM', 'UOM',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available_to_ship', 'Available to ship',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Zip_code', 'Zip code',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'City', 'City',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Country', 'Country',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'State', 'State',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Within_radius_of', 'Within radius of',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Find_nearby_stores', 'Find Stores',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store_List', 'Store List',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_stores_found', 'No stores found',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store', 'Store',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Availability', 'Availability',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distance', 'Distance',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery_Options', 'Delivery Options',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MI', 'miles',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'KM', 'km',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PostalCodeNotValidMessage', 'Postal code is not valid',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ZipCodeOrStateMandatory', 'Please enter the Zip code or the state where the store is located',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CityAndStateMandatory', 'Please enter the City and State where the store is located',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NA', 'N/A',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'inventoryOnHand', 'Inventory On Hand',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'inventoryInTransit', 'Inventory In Transit',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'inventoryNotAvailable', 'Inventory Not Available',
           'FacilityLocator')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Time Zone', 'Time Zone',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override Capacity', 'Override Capacity',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Capacity Value should be Integer', 'Capacity Value should be Integer',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Day', 'Day',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override Available to', 'Override maximum to',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View Facility Workload', 'View Facility Workload',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Today', 'Today',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility', 'Facility',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'The Capacity Value cannot be empty', 'The Capacity Value cannot be empty',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Capacity Value should be Positive Integer', 'Capacity Value should be Positive Integer',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Capacity', 'Allocated Capacity',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please enter only numeric values for overriding capacity', 'Please enter only numeric values for overriding capacity',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Capacity (Tomorrow)', 'Allocated Capacity (Tomorrow)',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available Capacity (Today)', 'Maximum Capacity (Today)',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available Capacity', 'Maximum Capacity',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Utilization (Yesterday)', 'Utilization (Yesterday)',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available Capacity (Tomorrow)', 'Maximum Capacity (Tomorrow)',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility Time Zone', 'Facility Time Zone',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last Updated', 'Last Updated',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility Type', 'Facility Type',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available Capacity (Yesterday)', 'Maximum Capacity (Yesterday)',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Utilization (Today)', 'Utilization (Today)',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Override Available Capacity', 'Override Maximum Capacity',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason For Override', 'Reason For Override',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Capacity (Today)', 'Allocated Capacity (Today)',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Updated By', 'Updated By',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Utilization (Tomorrow)', 'Utilization (Tomorrow)',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tomorrow', 'Tomorrow',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Capacity (Yesterday)', 'Allocated Capacity (Yesterday)',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'System', 'System',
           'FacilityWorkloadBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '4', 'Cancel order line',
           'FinalizerStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'Cancel',
           'FinalizerStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Procure',
           'FinalizerStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Backorder',
           'FinalizerStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Business_Process', 'Business Process',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'List of all the Allocation Templates', 'List of All Allocation Templates',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'day(0-365)', 'days (0-365)',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected Allocation Templates', 'Selected Allocation Templates',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Filter List', 'Filter List',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '* Must select template for immediate release', '* Must select template for immediate release',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reallocate_on_change_to_Committed_Delivery_Date', 'Reallocate on change to committed delivery date',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete', 'Delete',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name', 'Name',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Specify_Order_Selection_Filter', 'Specify order selection filter',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are_you_sure_that_you_want_to_delete_the_selected_Workflow(s)_?', 'Are you sure that you want to delete the selected Workflow(s)?',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate', 'Allocate',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit_Sales_Order_Processing_Rule', 'Edit Sales Order Processing Rule',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expire_After', 'Expire after',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attempt to bring back the order line to its last status', 'Attempt to Bring Back Order Line to its Last Status',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WorkFlow List', 'Workflow List',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_Expired_Orders', 'Cancel expired orders?',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please Select at least one workflow', 'Please select at least one workflow',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prioritize', 'Prioritize',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prioritization Required?', 'Prioritization Required',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Associate Allocation Templates', 'Associate Allocation Templates',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rank', 'Rank',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release', 'Release',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'When_null_compute_Requested_Delivery_Date_as_N_days_from_order_creation_date', 'When blank, compute requested delivery date as N days from order creation date',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process_Parameter', 'Sales Order Processing Rule',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Days_To_Hold', 'Days to hold',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No WorkFlow List', 'Found=No Workflow List found',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Configuration Parameter', 'Configuration Parameter',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected Release Templates', 'Selected Release Templates',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release Immediately?', 'Release Immediately',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Workflow', 'Workflow',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Work Flow', 'Edit Workflow',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prioritization', 'Prioritization',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '* Must select template for immediate allocation', '* Must select template for immediate allocation',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New_Sales_Order_Processing_Rule', 'New Sales Order Processing Rule',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_Status', 'Cancel status',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Workflow_List', 'Workflow List',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'List of all the Release Templates', 'List of All Release Templates',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'min(0-59)', 'min(0-59)',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Filter', 'New Filter',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process_Determination_List', 'Sales Order Processing Rules',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Workflow Name', 'Workflow Name',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate Immediately?', 'Allocate Immediately',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Associate Release Templates', 'Associate Release Templates',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel Reserved Order After Time Interval', 'Cancel reserved order after time interval',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Work Flow', 'New Workflow',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Check All', 'Check All',
           'Flow')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '105', 'Set to Pre-released',
           'FulfillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '110', 'Set to Released',
           'FulfillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '-1', 'No status change',
           'FulfillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', '(None)',
           'FulfillmentMergeOptions')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Always',
           'FulfillmentMergeOptions')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Opportunistic',
           'FulfillmentMergeOptions')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY RELEASED', 'Partially Released',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'READY FOR ALLOCATION', 'Ready for Allocation',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_DC_ALLOCATED', 'Partially DC Allocated',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY DC ALLOCATED', 'Partially DC Allocated',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SHIPPED', 'Partially Shipped',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RELEASED', 'Released',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC_ALLOCATED', 'DC Allocated',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_RELEASED', 'Partially Released',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPED', 'Shipped',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC ALLOCATED', 'DC Allocated',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_CREATED', 'DO Created',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION_FAILED', 'Allocation Failed',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CANCELED', 'Canceled',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCING_FAILED', 'Sourcing Failed',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED', 'Scheduled',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION FAILED', 'Allocation Failed',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCING FAILED', 'Sourcing Failed',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BACKORDERED', 'Backordered',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BACK_ORDERED', 'Back Ordered',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY_SHIPPED', 'Partially Shipped',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OPEN', 'Open',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CREATED', 'Created',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATED', 'Allocated',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVALID', 'Invalid',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULING_FAILED', 'Scheduling Failed',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCED', 'Sourced',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SUBSTITUTED', 'Substituted',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULING FAILED', 'Scheduling Failed',
           'FulfillmentOrderLineStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY RELEASED', 'Partially Released',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'READY FOR ALLOCATION', 'Ready for Allocation',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY DC ALLOCATED', 'Partially DC Allocated',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SHIPPED', 'Partially Shipped',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RELEASED', 'Released',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPED', 'Shipped',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SCHEDULED', 'Partially Scheduled',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC ALLOCATED', 'DC Allocated',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CANCELED', 'Canceled',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED', 'Scheduled',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION FAILED', 'Allocation Failed',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCING FAILED', 'Sourcing Failed',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SOURCED', 'Partially Sourced',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY ALLOCATED', 'Partially Allocated',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OPEN', 'Open',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CREATED', 'Created',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATED', 'Allocated',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVALID', 'Invalid',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCED', 'Sourced',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULING FAILED', 'Scheduling Failed',
           'FulfillmentOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'By supply types then locations',
           'FulfillmentSequence')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'By locations then supply types',
           'FulfillmentSequence')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'Complete single source',
           'FulfillmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'move_up', 'move up',
           'FulfillmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Partial multi source',
           'FulfillmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'move_down', 'move down',
           'FulfillmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Complete multi source',
           'FulfillmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Fulfillment cost optimized',
           'FulfillmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '-1', '(select one)',
           'FulfillmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Time Zone', 'Time Zone',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select one or more facilities to create a fulfillment tier', 'Please select one or more facilities to create a fulfillment tier',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Tier', 'Fulfillment tier',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility', 'Facility',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to delete the selected Tier(s)?', 'Are you sure that you want to delete the selected Tier(s)?',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add Facilities', 'Add Facilities',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add Fulfillment Tier', 'Add Fulfillment Tier',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Country', 'Country',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select facilities to remove from fulfillment tier', 'Select facilities to remove from fulfillment tier. Go to "Add Facilities" TAB to add more facilities',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete', 'Delete',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected Facilities', 'Selected Facilities',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Default', 'Default',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Details', 'Details',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Street', 'Street',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manage Fulfillment Tiers', 'Manage Fulfillment Tiers',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel Update', 'Cancel Update',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'create fulfillment tier', 'Select facilities to create a fulfillment tier',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'State', 'State',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click_here_to_select_facility', 'Click here to select facility',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update Tier', 'Update Tier',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility Type', 'Facility Type',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View Fulfillment Tiers', 'View Fulfillment Tiers',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'edit fulfillment tier', 'Select to add more facilities to the fulfillment tier',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Postal Code', 'Postal Code',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add to Tier', 'Add To Tier',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merge Facility', 'Merge Facility',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add', 'Add',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit', 'Edit',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment tier is a required field', 'Fulfillment tier is a required field',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply', 'Apply',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference field 1', 'Reference Field 1',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'City', 'City',
           'FulfillmentTier')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'Group',
           'GroupActionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Variance',
           'GroupActionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Match',
           'GroupActionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', '(none)',
           'GroupActionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Order',
           'GroupingConstraint')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '(none)', '(none)',
           'GroupingConstraint')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '20', 'Product Class',
           'GroupingConstraint')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Average cost', 'Average cost',
           'HandlingCostOption')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total cost to serve', 'Total cost to serve',
           'HandlingCostOption')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'Basic reapportionment with "even distribution"',
           'InventoryEventAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Reallocate',
           'InventoryEventAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ReApportionment With Even Distribution', 'Basic reapportionment with "even distribution"',
           'InventoryEventAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reallocate affected orderlines', 'Reallocate',
           'InventoryEventAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rollup allocations first', 'Roll over',
           'InventoryEventAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Roll over',
           'InventoryEventAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ORDER_TYPE', 'Order Type',
           'InStoreOrderFilterField')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfacesSummary', 'Interfaces',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfaceName', 'Name',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfaceModule', 'Application',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfaceMappingFile', 'Mapping File',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfaceSampleXML', 'Sample XML',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfaceDescription', 'Description',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfaceDownload', 'Download',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfaceDetailsName', 'Interface Name',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfaceDetails', 'Details',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'XSD', 'XSDs',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'XSL', 'XSLTs',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VM', 'Velocity Templates',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InterfaceDownloadAll', 'Download All',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MD', 'Mapping Documents',
           'Interfaces')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_Segment', 'Inventory Segment',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invDetailHeading', 'Segment Details',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'segmentName', 'Segment Name',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reset', 'Reset',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reservedForDemand', 'Reserved For Demand',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'originalQuantity', 'Original Quantity',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'availableQuantity', 'Available Quantity',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_Segment_Information', 'Inventory Segment Information',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'skuName', 'SKU Name',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'segmentRank', 'Segment Rank',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply', 'Apply',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'facility', 'Facility',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invSegInfoFilterName', 'Segment Details Filter',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'inventoryId', 'Inventory ID',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invSegmentTemplate', 'Inventory Segmentation Template',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invSegmentInfoTitle', 'Inventory Segment Information',
           'InvSegmentInfo')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On Hand Inventory Only', 'Rules based segmentation',
           'InventorySegmentationLevel')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'None', 'No segmentation',
           'InventorySegmentationLevel')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'PO/ASN based segmentation',
           'InventorySegmentationLevel')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Rules based segmentation',
           'InventorySegmentationLevel')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'No segmentation',
           'InventorySegmentationLevel')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'All Inventories', 'PO/ASN based segmentation',
           'InventorySegmentationLevel')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'By fixed value',
           'InventorySegmentationValueType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'By percentage',
           'InventorySegmentationValueType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'Quality level',
           'InventorySelection')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Minimum inventory',
           'InventorySelection')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Maximum inventory',
           'InventorySelection')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'None',
           'InventorySelection')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UNALLOCATABLE', 'Unallocatable',
           'InventoryState')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATABLE', 'Allocatable',
           'InventoryState')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Line_Number', 'Order Line No.',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unit_Price', 'Unit Price',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Description', 'Item Description',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Name', 'Item Name',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Header_Charges', 'Header Charges',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Creation_Date', 'Creation Date',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipment_Number', 'Shipment Number',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Type', 'Invoice Type',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Line_Number', 'Invoice Line No.',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Discount', 'Total Discount',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Number', 'Invoice No.',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Details', 'Invoice Details',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Quantity', 'Invoiced Quantity',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Tax', 'Line Tax',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Discounts', 'Line Discounts',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Summary', 'Invoice Summary',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Amount', 'Invoice Amount',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Settled', 'Settled',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Charges', 'Total Charges',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Line_List', 'Invoice Line(s)',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Total', 'Line Total',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Tax', 'Total Tax',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amount_Settled', 'Amount Settled',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoiced', 'Invoiced',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_Return_Order_Amount', 'Total Return Order Amount',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amount_Processed', 'Amount Processed',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Header_Discounts', 'Header Discounts',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Header_Tax', 'Header Tax',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RMA_No', 'RMA No.',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NA', 'N/A',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax_Included', 'Tax Included',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RMA_Line_No', 'RMA Line No.',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return_Type', 'Return Type',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Charges', 'Line Charges',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Processed', 'Processed',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered_Item', 'Ordered Item',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped_Item', 'Shipped Item',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped_Quantity', 'Shipped Quantity',
           'InvoiceDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ignore', 'Ignore',
           'ItemPriceBehaviorAfterEndDate')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert', 'Alert',
           'ItemPriceBehaviorAfterEndDate')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Instructions', 'Instructions',
           'NoteCategory')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Communication', 'Communication',
           'NoteCategory')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Create RMA', 'Create RMA',
           'NoteCategory')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Resolve RMA Variance', 'Resolve RMA Variance',
           'NoteCategory')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Approve RMA Line', 'Approve RMA Line',
           'NoteCategory')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel RMA Line', 'Cancel RMA Line',
           'NoteCategory')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MultiLangRegExp', '^[A-Za-z]*$',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Note_Type', 'Note Type cannot be blank',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Note_Type', 'Note Type',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'System_Defined', 'System Defined',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Note_Types_Right_Panel', 'Note Types Detail Panel',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Detail_Tab', 'Details',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mark_For_Deletion', 'Inactive',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Note_Types_List_Panel', 'Note Types List Panel',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Note_Type_Cannot_Contain_Special_Chars', 'Note Type cannot contain special characters.',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_Note_Type_Description', 'Note Description cannot be blank',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Category', 'Category',
           'NoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Released', 'DO Released',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reallocated', 'Reallocated',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Shipped', 'Partially Shipped',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Created', 'DO Created',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped', 'Shipped',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Failed', 'Failed',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Closed', 'Shipped',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Closed', 'Partially Shipped',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Open', 'Open',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Deallocated', 'Deallocated',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Draft', 'Draft',
           'OrderInventoryAllocationStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Critical', 'Critical',
           'OrderLinePriority')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'High', 'High',
           'OrderLinePriority')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Medium', 'Medium',
           'OrderLinePriority')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Low', 'Low',
           'OrderLinePriority')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '400', 'Allocated',
           'OrderLineStatusForWorkFlowCancellation')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '650', 'Partially DC allocated',
           'OrderLineStatusForWorkFlowCancellation')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '(select one)', '(select one)',
           'OrderLineStatusForWorkFlowCancellation')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '550', 'Partially released',
           'OrderLineStatusForWorkFlowCancellation')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MD', 'Manual mark down',
           'OverrideSnHChargesReasonCodesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CM', 'Competitor''s charge match',
           'OverrideSnHChargesReasonCodesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OT', 'Others',
           'OverrideSnHChargesReasonCodesFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Shipped', 'Partially Shipped',
           'OrderReleaseStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sent', 'Sent',
           'OrderReleaseStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped', 'Shipped',
           'OrderReleaseStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancelled', 'Canceled',
           'OrderReleaseStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created', 'Created',
           'OrderReleaseStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC Allocated', 'DC Allocated',
           'OrderReleaseStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Processed', 'Processed',
           'OrderReleaseStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '60', 'Others',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '30', 'Third Party Billing',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Others', 'Others',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Credit Card', 'Credit Card',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '50', 'Gift Card',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '20', 'Paper Check',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift Card', 'Gift Card',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Paper Check', 'Paper Check',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Electronic Bank Transfer', 'Electronic Bank Transfer',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '40', 'Credit Card',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'Electronic Bank Transfer',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3rd Party Billing', 'Third Party Billing',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'e-Check', 'E-Check',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '70', 'E-Check',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Paypal', 'Paypal',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '90', 'Paypal',
           'PaymentMethod')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return_Sequence', 'Return Sequence',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Yes', 'Yes',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reset', 'Reset',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Methods', 'Payment Methods',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Type', 'Payment Type',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No', 'No',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'com_payment', 'com payment',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hit_Count', 'Hit Count',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Selection', 'Order Selection',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Rule', 'Payment Rule',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Authorization_Required', 'Authorization Required',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule_Name', 'Rule Name',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Rule_Table', 'Payment Rule Table',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge_Sequence', 'Charge Sequence',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Auth_expiration_Days', 'Auth. Expiration Days',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule_Name_And_Desc_Panel', 'Rule Name and Desc panel',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tries_is_not_valid_number', 'Total no of tries should be a number, greater than  zero',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_report_user_is_required_field', 'Transaction report user is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_report_password_is_required_field', 'Transaction report password is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_report_user', 'Transaction report user',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_report_password', 'Transaction report password',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Detail_Tab', 'Details',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MerchantInfo_tab', 'Merchant Info.',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Method_Detail', 'Payment method',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge_Sequence_Detail', 'Charge sequence',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Refund_Sequence_Detail', 'Refund sequence',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Authorization_Required_Detail', 'Authorization required',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Auth_Expiration_Days', 'Authorization expiration days',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Settlement_Expiration_Days', 'Settlement expiration days',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status_Check_Days', 'Status check days',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Of_Tries', 'Total no. of tries',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merchant_Name', 'Merchant name',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merchant_ID', 'Merchant ID',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merchant_Password', 'Merchant password',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Auth_Expiration_Days_is_required_field', 'Authorization expiration days is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Settlement_Expiration_Days_is_required_field', 'Settlement expiration days is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status_Check_Days_is_required_field', 'Status check days is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Retries_is_required_field', 'Total no. of tries is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merchant_Name_is_required_field', 'Merchant name is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MerchantID_is_required_field', 'Merchant ID is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merchant_Password_is_required_field', 'Merchant password is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Auth_Expiration_Days_is_not_valid_number', 'Authorization expiration days should be a number, greater than or equal to zero',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Settlement_Expiration_Days_is_not_valid_number', 'Settlement expiration days should be a number, greater than or equal to zero',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status_Check_Days_is_not_valid_number', 'Status check days should be a number, greater than or equal to zero',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Method', 'Payment Method',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Refund_Sequence', 'Refund Sequence',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Valid_For_Return', 'Valid for refund',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Valid_for_Return', 'Valid for Refund',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentRuleAdvanceAuthReq', 'Advance Authorization Required',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merchant_Signature', 'Merchant signature',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merchant_Signature_is_required_field', 'Merchant signature is a required field',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NA', 'NA',
           'PaymentRule')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AUTHORIZED', 'Authorized',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVOICED', 'Invoiced',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED_CHARGE', 'Settlement Failed',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REFUNDED', 'Refunded',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NOT_APPLICABLE', 'Not Applicable',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_AUTHORIZATION', 'Await Auth',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED_AUTH', 'Authorization Failed',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_PAYINFO', 'Await Payment',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_REFUND', 'Awaiting Refund',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PAID', 'Paid',
           'PaymentStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Failure', 'Failure',
           'PaymentTransactionDecision')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Success', 'Success',
           'PaymentTransactionDecision')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unavailable', 'Unavailable',
           'PaymentTransactionDecision')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fraud', 'Fraud',
           'PaymentTransactionDecision')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Open', 'Open',
           'PaymentTransactionStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inprogress', 'In progress',
           'PaymentTransactionStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Closed', 'Closed',
           'PaymentTransactionStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Settlement', 'Settlement',
           'PaymentTransactionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Refund', 'Refund',
           'PaymentTransactionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Balancecheck', 'Balance check',
           'PaymentTransactionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Authorization', 'Authorization',
           'PaymentTransactionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OrderNo', 'Order No.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FirstName', 'First Name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LastName', 'Last Name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TransactionNo', 'Transaction No.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Date', 'Transaction Date',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amount', 'Amount',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CustomerName', 'Customer Name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Email', 'Email',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PhoneNo', 'Phone No.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CustomerID', 'Customer ID',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ItemID', 'Item ID',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity', 'Quantity',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ItemDetails', 'Item Details',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SearchOrders', 'Search Orders',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Orders', 'Orders',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'POSTransactions', 'POS Transactions',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ViewDetails', 'View Details',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PaymentMethod', 'Payment Method',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PamentDetails', 'Pament Details',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Point_of_Sale_Transaction', 'POS Transaction Details',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customer_Name', 'Customer name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'store_Number', 'Store name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'transaction_Type', 'Transaction type',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'transaction_No', 'Transaction no.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'associate_Name', 'Associate name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'transaction_Status', 'Transaction status',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'transaction_Date', 'Transaction date',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'associate_ID', 'Associate ID',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'special_Order_No', 'Special order no.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'transaction_Charges', 'Transaction Charges',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_sub_total', 'Item(s) subtotal',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_discounts', 'Total discounts',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total_charges', 'Total charges',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Taxes', 'Taxes',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_total', 'Transaction total',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Purchased_Items', 'Purchased Items',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Details', 'Payment Details',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'More_Info', 'More Info.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'line_Number', 'Line No.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_description', 'Item Description',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Id', 'Item ID',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unit_price', 'Unit Price',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Discount_Amount', 'Discount Amount',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax_amount', 'Tax Amount',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'line_total', 'Line Total',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Details', 'Item Details',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_info', 'Shipping Info.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'external_item_ID', 'External item ID',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'is_gift', 'Is gift',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'is_exchange', 'Is exchange',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'product_class', 'Product class',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'is_package', 'Is package',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'is_returnable', 'Is returnable',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'style', 'Style',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'color_suffix', 'Color suffix',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'color', 'Color',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'style_suffix', 'Style suffix',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'size', 'Size',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'name', 'Name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'email', 'Email',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ship_to_facility', 'Ship to facility',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'address', 'Address',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'requested_delivery_by', 'Requested delivery by',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'delivery_option', 'Delivery option',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ship_date', 'Ship date',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'tracking_no', 'Tracking No.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'phone', 'Phone no.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ship_via', 'Ship via',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discounts', 'Discounts',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_type', 'Discount type',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_amount', 'Discount amount',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount_name', 'Discount name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'charge_category', 'Charge category',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'charge_amount', 'Charge amount',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'charge_name', 'Charge name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'tax_category', 'Tax category',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'tax_amount_label', 'Tax amount',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'tax_name', 'Tax name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customer_details', 'Customer Details',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customer_id', 'Customer ID',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customer_type', 'Customer type',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Others', 'Others',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PaperCheck', 'Paper Check',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GiftCards', 'Gift Card(s)',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PayPal', 'PayPal',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cash', 'Cash',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CreditCards', 'Credit Card(s)',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TransactionItemDetail', 'Transaction Item Detail',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TransactionList', 'Transaction List',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enter_filter_criteria', 'No transactions found for the given criteria.',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'additional_charges', 'Additional Charges',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'tax', 'Tax',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_discounts_available', 'No discounts available',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_tax_available', 'No taxes available',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_additional_charges_available', 'No additional charges available',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_payment_info_available', 'No payment information available',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PaymentDetails', 'Payment Details',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No', 'No',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Yes', 'Yes',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'not_available', 'Not available',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Paymentmethod', 'Payment method',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_first_name', 'Customer first name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer_last_name', 'Customer last name',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cashamount', 'Cash amount',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_method', 'Shipping method',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship_to_store', 'Ship to store',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Additional_charges', 'Additional charges',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_Type', 'Transaction Type',
           'POSTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Available', 'Promotions Available',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item_Name', 'Item',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Publish_Date', 'Publish Date',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'End_Date', 'End Date',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promoted_prod_details', 'Active Prices and Promotions',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Type', 'Promotion Type',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PromotedProdDetails', 'PromotedProductDetails',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Active_Price', 'Active Price',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Start_Date', 'Start Date',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotions', 'Promotions',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Name', 'Promotion Name',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Product_Keyword', 'Product Keyword',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Active', 'Active',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inactive', 'Inactive',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Category', 'Promotion Category',
           'PricingPromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference Number 2', 'Reference Number 2',
           'PrioritizationAttribute')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference Number 1', 'Reference Number 1',
           'PrioritizationAttribute')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must Delivery Date', 'Must Delivery Date',
           'PrioritizationAttribute')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promised Delivery Date', 'Promised Delivery Date',
           'PrioritizationAttribute')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority Group', 'Priority Group',
           'PrioritizationAttribute')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Creation Date/Time', 'Order Creation Date/Time',
           'PrioritizationAttribute')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested Quantity', 'Requested Quantity',
           'PrioritizationAttribute')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested Delivery Date', 'Requested Delivery Date',
           'PrioritizationAttribute')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion', 'Promotion',
           'PromoCategoryFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Coupon', 'Coupon',
           'PromoCategoryFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY - Percent Discount', 'BXGY - Percent Discount',
           'PromoTypeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BOGO Discount', 'BOGO Discount',
           'PromoTypeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping Percent Discount', 'Shipping Percent Discount',
           'PromoTypeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fixed Amount Discount', 'Fixed Amount Discount',
           'PromoTypeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percent Discount', 'Percent Discount',
           'PromoTypeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manufacturer Rebate', 'Manufacturer Rebate',
           'PromoTypeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY - Fixed Amount Discount', 'BXGY - Fixed Amount Discount',
           'PromoTypeFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Min_line_quantity_UOM', 'Min. line quantity UOM:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Max_order_line_value_currency', 'Max. line value currency:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percent Discount', 'Percent Discount',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Application_Level', 'Application Level',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Line', 'Order Line',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Type', 'Promotion Type',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PERCENT_DISCOUNT', 'PERCENT DISCOUNT',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY - Fixed Amount Discount', 'BXGY - Fixed Amount Discount',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Details', 'Promotion Details',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Scope', 'Promotion Scope',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Min_line_quantity', 'Min. line quantity:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LineItemExclusive', 'Line Item Exclusive',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Combination_Condition', 'Exclusive Condition',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Value', 'Promotion Value',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping Percent Discount', 'Shipping Percent Discount',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Max_order_value_currency', 'Max. order value currency:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'External_Promotion_Id', 'External Promotion ID',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fixed Amount Discount', 'Fixed Amount Discount',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Min_order_line_value_currency', 'Min. line value currency:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPING_FIXEDAMOUNT_DISCOUNT', 'SHIPPING FIXEDAMOUNT DISCOUNT',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Name', 'Promotion Name',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NonExclusive', 'Non Exclusive',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MANUFACTURER_REBATE', 'MANUFACTURER REBATE',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Comments', 'Comments',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BOGO Discount', 'BOGO Discount',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manufacturer Rebate', 'Manufacturer Rebate',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FIXED_AMOUNT_DISCOUNT', 'FIXED AMOUNT DISCOUNT',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY_FIXED_AMOUNT_DISCOUNT', 'BXGY FIXED AMOUNT DISCOUNT',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY_PERCENT_DISCOUNT', 'BXGY PERCENT DISCOUNT',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Max_line_quantity_UOM', 'Max. line quantity UOM:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPING_PERCENT_DISCOUNT', 'SHIPPING PERCENT DISCOUNT',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Min_order_value', 'Min. order value:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'com_promotions', 'com promotions',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INSTANT_REBATE', 'INSTANT REBATE',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Header', 'Order Header',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Max_order_line_value', 'Max. line value:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Details', 'Details',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Provider', 'Promotion Provider',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Max_line_quantity', 'Max. line quantity:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GlobalExclusive', 'Global Exclusive',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PromotionDetails', 'PromotionDetails',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Max_order_value', 'Max. order value:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY - Percent Discount', 'BXGY - Percent Discount',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Min_order_value_currency', 'Min. order value currency:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Min_order_line_value', 'Min. line value:',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BOGO_DISCOUNT', 'BOGO DISCOUNT',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Category', 'Promotion Category',
           'PromotionDetails')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'End_Date', 'End Date',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Publish_from_Date', 'Publish From Date',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Active', 'Active',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Scope_Detail', 'Promotion Scope Detail',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name', 'Name',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Start_Date', 'Start Date',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Name', 'Promotion Name',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inactive', 'Inactive',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Scope', 'Promotion Scope',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Category', 'Promotion Category',
           'PromotionScope')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Master_Promotion_Types', 'Master Promotion Types',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Buy one product and Get another product at Percent Discount', 'Buy one product and Get another product at Percent Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Fixed_Amount_Discount', 'Shipping Fixed Amount Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Buy One and Get same One at Discount', 'Buy One and Get same One at Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Percent_Discount', 'Shipping Percent Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fixed Amount Discount', 'Fixed Amount Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BOGO Discount', 'BOGO Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percent_Discount', 'Percent Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY_Fixed_Amount_Discount', 'BXGY - Fixed Amount Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Instant_Rebate', 'Instant Rebate',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Comments', 'Comments',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Buy one product and Get another at Fixed Amount Discount', 'Buy one product and Get another at Fixed Amount Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY - Fixed Amount Discount', 'BXGY - Fixed Amount Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY_Percent_Discount', 'BXGY Percent Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BOGO_Discount', 'BOGO Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Buy One Get Same one at Discount', 'Buy One Get Same one at Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_List', 'Promotion List',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fixed_Amount_Discount', 'Fixed Amount Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Buy one and Get another at Fixed Amount Discount', 'Buy one and Get another at Fixed Amount Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotions', 'Promotions',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manufacturer_Rebate', 'Manufacturer Rebate',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manufacturer Rebate', 'Manufacturer Rebate',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BXGY - Percent Discount', 'BXGY - Percent Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion_Type', 'Promotion Type',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percentage Discount', 'Percentage Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping Percent Discount', 'Shipping Percent Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percent Discount', 'Percent Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Buy one and Get another at Percent Discount', 'Buy one and Get another at Percent Discount',
           'PromotionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'In descending order of ordered quantity',
           'ReapportionmentCriteriaFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Minimum Quantity Orders First', 'In ascending order of ordered quantity',
           'ReapportionmentCriteriaFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'In ascending order of ordered quantity',
           'ReapportionmentCriteriaFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Maximum Quantity Orders First', 'In descending order of ordered quantity',
           'ReapportionmentCriteriaFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'By latest ship date',
           'ReapportionmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'To target percentage',
           'ReapportionmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Across all order lines',
           'ReapportionmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'None',
           'ReapportionmentStrategy')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store Facility', 'Store Facility',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select at least one Release Line record', 'Please select at least one release line record',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested Quantity', 'Requested Quantity',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Line Id', 'Order Line ID',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manufacturing Date', 'Manufacturing Date',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Send To WMS', 'Send to WMS',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped Quantity', 'Shipped Quantity',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to send the selected Release Line record to WMS?', 'Are you sure that you want to send the selected release line record to WMS?',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Must Ship By Date', 'Must Ship By Date',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped SKU', 'Shipped SKU',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference Type', 'Reference Type',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release Lines List', 'Release Lines List',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Type', 'Type',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Committed Ship Date', 'Committed Ship Date',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sub Type', 'Sub Type',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Quantity', 'Allocated Quantity',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release Date', 'Release Date',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped Date', 'Shipped Date',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'releaseLinesList', 'Release Lines List',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release Id', 'Release ID',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference Line Id', 'Reference Line ID',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WH Facility', 'WH Facility',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference Id', 'Reference ID',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is Emergency', 'Emergency',
           'ReleaseManagement')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Onhand', 'On Hand',
           'ReleaseSubType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO Internal', 'PO Internal',
           'ReleaseSubType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO Vendor', 'PO Vendor',
           'ReleaseSubType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply', 'Supply',
           'ReleaseSubType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pick Ticket', 'Pickticket',
           'ReleaseType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Purchase Order', 'Purchase Order',
           'ReleaseType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distro', 'Distro',
           'ReleaseType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Putaway', 'Putaway',
           'ReleaseType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store Transfer', 'Store Transfer',
           'ReleaseType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release', 'Release',
           'ReleaseType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '200', 'SOURCED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '550', 'PARTIALLY ALLOCATED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '290', 'SOURCING FAILED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULING FAILED', 'SCHEDULING FAILED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '100', 'OPEN',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '900', 'CANCELED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVALID', 'INVALID',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SHIPPED', 'PARTIALLY SHIPPED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY RELEASED', 'PARTIALLY RELEASED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '990', 'INVALID',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '350', 'PARTIALLY SCHEDULED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION FAILED', 'ALLOCATION FAILED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '800', 'SHIPPED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCED', 'SOURCED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '250', 'PARTIALLY SOURCED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SCHEDULED', 'PARTIALLY SCHEDULED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '700', 'DC ALLOCATED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC ALLOCATED', 'DC ALLOCATED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '600', 'RELEASED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED', 'SCHEDULED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CANCELLED', 'CANCELED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'READY FOR ALLOCATION', 'READY FOR ALLOCATION',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '500', 'ALLOCATED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCING FAILED', 'SOURCING FAILED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '850', 'PARTIALLY SHIPPED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPED', 'SHIPPED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '590', 'ALLOCATION FAILED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY ALLOCATED', 'PARTIALLY ALLOCATED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RELEASED', 'RELEASED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '400', 'READY FOR ALLOCATION',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OPEN', 'OPEN',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '750', 'PARTIALLY DC ALLOCATED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SOURCED', 'PARTIALLY SOURCED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY DC ALLOCATED', 'PARTIALLY DC ALLOCATED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '300', 'SCHEDULED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '650', 'PARTIALLY RELEASED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATED', 'ALLOCATED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '390', 'SCHEDULING FAILED',
           'SOFulFillStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created', 'Created',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'End_Date', 'End Date',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item', 'Item',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply_filter_to_display_data', 'Apply filter to display data',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Behavior_After_End_Date', 'Behavior After End Date',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert', 'Alert',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Price_History', 'Price Data',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Currency', 'Currency',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales_Price', 'Sales Price',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Updated', 'Last Updated',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On', 'On',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Start_Date', 'Start Date',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rank', 'Rank',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Type_and_ID', 'Type & ID',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Audit', 'Audit',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Store_Id', 'Store ID',
           'Selling_ItemPricing')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3', 'OrderValue',
           'ShippingChargeRateBasis')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '2', 'Volume',
           'ShippingChargeRateBasis')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ORDERVALUE', 'OrderValue',
           'ShippingChargeRateBasis')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VOLUME', 'Volume',
           'ShippingChargeRateBasis')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Weight',
           'ShippingChargeRateBasis')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WEIGHT', 'Weight',
           'ShippingChargeRateBasis')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Matrix_Description', 'Description',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Range_UOM', 'Range UOM',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Delivery_Zone', 'Delivery Zone',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Matrix', 'Shipping Matrix',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Header_Tab', 'Header',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Matrix_List_Panel', 'Shipping Matrix List Panel',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Additional_Charges_Per_Item', 'Additional Charges/Item',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Matrix_HeaderTab_Panel', 'Shipping Matrix Header Tab',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Range_End', 'Range End',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Base_Shipping_Charges_Order_Line_Level', 'Base Charges/Order Line',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No_Shipping_Matrix', 'No Shipping Matrix',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Matrix_LineTab_Panel', 'Shipping Matrix Lines Tab',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Range_Start', 'Range Start',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hazmat_Charges', 'Additional Hazmat Charges',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Lines_Tab', 'Lines',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Matrix_Name', 'Matrix Name',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Mode', 'Shipping Mode',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Matrix_Currency', 'Currency',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Channel_Type', 'Channel Type',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Rate_Basis', 'Rate Basis',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Base_Shipping_Charges_Order_Level', 'Base Charges/Order',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipping_Matrix_Comments', 'Comments',
           'ShippingMatrix')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reapportionment', 'Reapportionment',
           'ShortAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reallocate OrderLine', 'Reallocate',
           'ShortAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Reapportionment',
           'ShortAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Reallocate',
           'ShortAction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Descending',
           'SortOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Ascending',
           'SortOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '-1', '(select one)',
           'SortOrder')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Immediate', 'Immediate',
           'SubstitutionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'When out', 'When out',
           'SubstitutionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In Addition', 'In Addition',
           'SubstitutionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'When Not out', 'When Not out',
           'SubstitutionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promotion', 'Promotion',
           'SubstitutionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Accessorial', 'Accessorial',
           'SubstitutionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SELECT_ONE', '(select one)',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit', 'Edit',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update_Quantities', 'Update Quantities',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility', 'Facility',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated(Future)', 'Allocated (Future)',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segmentation_type', 'Segmentation type',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Total', 'Total',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click_here_to_select_facility', 'Click here to select facility',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated', 'Allocated',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transfer_Quantity_should_be_a_positive_integer', 'Transfer Quantity should be a positive integer',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cannot_transfer_between_segments', 'Cannot transfer inventory between segments since On Hand Inventory does not exist for the selected record',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALL', 'ALL',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply_Balance_Page_Header', 'Supply Balance',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manufactured_Date', 'Manufactured Date',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segmentation_Type', 'Segmentation Type',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Future', 'Future',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Future_Supply', 'View Future Supply',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transfer_Quantity', 'Transfer Quantity',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unallocatable', 'Unallocatable',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item', 'Item',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click_here_to_select_item', 'Click here to select item',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity_to_transfer_should_be_less_than_Available_Quantity_Transfer', 'Quantity to transfer should be less than Available Quantity to transfer',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Future_Supply', 'Future Supply',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply_Balance', 'Supply Balance',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Status', 'Status',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search_Result_Details', 'Search Result Details',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Not_Applicable', 'N/A',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transfer_Quantity_should_be_less_than_or_equal_to_Available_For_Transfer', 'Transfer Quantity should be less than or equal to Available For Transfer',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply_Detail_Id', 'Supply Detail Id',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On_hand', 'On hand',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_Details', 'View Details',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transfer_To', 'Transfer To',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete_Item', 'Delete Item',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On Hand', 'On Hand',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search_Results', 'Search Results',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Search_Criteria', 'Search Criteria',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add_Item', 'Add Item',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply_Id', 'Supply Id',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_Transfer_Quantity', 'Please enter Transfer Quantity',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated_On_Hand', 'Allocated (On Hand)',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_add_at_least_one_item', 'Please add at least one item',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Forward_Consumption_Days', 'Forward Consumption (Days)',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On_Hand', 'On Hand',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Future Supply', 'Future Supply',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Future_Supply_(Days)', 'Future Supply(Days)',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Get_Supply_Balance', 'Get Supply Balance',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transfer_On_Hand_Inventory_Between_Segments', 'Transfer On Hand Inventory Between Segments',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'By_facility', 'By facility',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available', 'Available',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'From', 'From',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity_to_transfer_should_be_a_positive_integer', 'Quantity to transfer should be a positive integer',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated_Future', 'Allocated (Future)',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated(On_Hand)', 'Allocated (On Hand)',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'From_Date', 'From Date',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment_Name', 'Segment Name',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'By_network', 'By network',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Fixed value',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Percentage',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SKU', 'SKU',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment', 'Segment',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'To', 'To',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Error', 'Error',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Future_Supply_Days', 'Future Supply(Days)',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available_For_Transfer', 'Available For Transfer',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply_Type', 'Supply Type',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ETA', 'ETA',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'All', 'All',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source', 'Source',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_segment_to_which_quantity_should_be_transferred', 'Please select segment to which quantity should be transferred',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete', 'Delete',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'By_segment', 'By segment',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transfer_Segment_Inventory', 'Transfer Segment Inventory',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selection', 'Selection',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Lot', 'Lot',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply', 'Apply',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_Type', 'Inventory Type',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In_Receiving', 'In Receiving',
           'SupplyBalance')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Do_you_wish_to_mark_on_hand_inventory_for_selected_records_as_unavailable_?', 'Do you wish to mark on hand inventory for selected records as unavailable? Orders allocated against selected inventory items will get de-allocated.',
           'SupplyBalanceStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_record', 'Please select atleast one record.',
           'SupplyBalanceStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MarkAvailable_UnAvailable', 'MarkAvailable / UnAvailable',
           'SupplyBalanceStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Not_Applicable', 'N/A',
           'SupplyBalanceStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_watermark', 'Inventory Watermark',
           'SupplyBalanceStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On_Hand_Status', 'On Hand Status',
           'SupplyBalanceStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Available',
           'SupplyBalanceStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Unavailable',
           'SupplyBalanceStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO', 'PO',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipment', 'Shipment',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LPN', 'LPN',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'External', 'External',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OnHand', 'OnHand',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALL', 'All',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Future Supply', 'Future Supply',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On Hand', 'On Hand',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Onhand', 'Onhand',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN', 'ASN',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other', 'Other',
           'SupplyType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift message', 'Gift message',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Instrns. to the Carrier for delivery', 'Instrns. to the Carrier for delivery',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes entered when a hold is resolved', 'Notes entered when a hold is resolved',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes to cancel an order', 'Notes to cancel an order',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Marketing material to be sent with the order', 'Marketing material to be sent with the order',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift To Name', 'Gift To Name',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hold Resolution', 'Hold Resolution',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancellation', 'Cancelation',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift to name', 'Gift to name',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift from name', 'Gift from name',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Instrns. to the DC for gift wrapping', 'Instrns. to the DC for gift wrapping',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery Instrns.', 'Delivery Instrns.',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift Message', 'Gift Message',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift Wrap', 'Gift Wrap',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hold', 'Hold',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes to hold an order', 'Notes to hold an order',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift To', 'Gift To',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift From', 'Gift From',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Collateral', 'Collateral',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasement', 'Appeasement',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer Complaint', 'Customer Complaint',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Enquiry', 'Order Enquiry',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Enquiry', 'Payment Enquiry',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other', 'Other',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Notes entered when appeasement is applied', 'Notes entered when appeasement is applied',
           'SystemDefinedNoteTypes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS Failure', 'AVS Failure',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay. service returned response code as "Fraud"', 'Pay. service returned response code as "Fraud"',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H service failed due to bus. or sys. exceptions', 'S&H service failed due to bus. or sys. exceptions',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Default Cancel', 'Default Cancel',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay. Service Unavbl.', 'Pay. Service Unavbl.',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay. service returned response code as "Failed"', 'Pay. service returned response code as "Failed"',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H Unavbl.', 'S&H. Unavbl.',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alln. failed during create or edit order process', 'Alln. failed during create or edit order process',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Failed', 'Allocation Failed',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax Service Unavbl.', 'Tax Service Unavbl.',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H service is unavailable', 'S&H service is unavailable',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS failure', 'AVS failure',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Misc.', 'Misc.',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Failed', 'Payment Failed',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other', 'Other',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment service is unavailable', 'Payment service is unavailable',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipment Delayed', 'Shipment Delayed',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax Service Failed', 'Tax Service Failed',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Fraud', 'Payment Fraud',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay. service returned response code as "Fraud', 'Pay. service returned response code as "Fraud"',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax service is unavailable', 'Tax service is unavailable',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax service failed due to bus. or sys. exceptions', 'Tax service failed due to bus. or sys. exceptions',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS Unavbl.', 'AVS Unavbl.',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Miscellaneous reason', 'Miscellaneous reason',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS is unavailable', 'AVS is unavailable',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS Cancel', 'AVS Cancel',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H Failed', 'S&H Failed',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address not Verified', 'Address not Verified',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitAddrssDtl', 'AwaitAddrssDtl',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitAddrssNPymntDtl', 'AwaitAddrssNPymntDtl',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitItemRcpt', 'AwaitItemRcpt',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitPymntDtl', 'AwaitPymntDtl',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel un-recvd qty', 'Cancel un-recvd qty',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge Addn fees', 'Charge Addn fees',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mark un-recvd qty', 'Mark un-recvd qty',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Addn fees', 'No Addn fees',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Qty and Cond Vari', 'Qty and Cond Vari',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'XONeedsUserAction', 'XONeedsUserAction',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason code for making XO on hold  for awaiting address details', 'Reason code for making XO on hold  for awaiting address details',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason code for making XO on hold  for awaiting address and payment details', 'Reason code for making XO on hold  for awaiting address and payment details',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason code for making XO on hold  for awaiting item receipt', 'Reason code for making XO on hold  for awaiting item receipt',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason code for making XO on hold  for awaiting Payment details', 'Reason code for making XO on hold  for awaiting Payment details',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel un-received quantity', 'Cancel un-received quantity',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge additional fees', 'Charge additional fees',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mark un-received quantity as received', 'Mark un-received quantity as received',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No additional fees', 'No additional fees',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity and Condition Variance', 'Quantity and Condition Variance',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason code for making RMA on hold  for XO Needs user Action', 'Reason code for making RMA on hold  for XO Needs user Action',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Condition Variance', 'Condition Variance',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity Variance', 'Quantity Variance',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Receipt Not Expected', 'Receipt Not Expected',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Resolved', 'Resolved',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unexpected Item', 'Unexpected Item',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Miscellaneous Reason', 'Miscellaneous Reason',
           'SystemDefinedReasonCodes')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selection_constraints', 'Selection constraints',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete Size', 'Delete Size',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hours (0-23)', 'Hours (0-23)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destination Facility', 'Destination Facility',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit DO Release Template', 'Edit DO Release Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Type', 'Order Type',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule_description_label', 'Rule description',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flow Templates', 'Flow Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Remove', 'Remove',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_order_lines_to_exclude_them_from_the_allocation', 'Select order lines to exclude them from the allocation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_needs_layer_operator_can_not_be_Add', 'Last needs layer operator can not be Add.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Limit to single PO', 'Limit to single PO',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_reserved_orders_after_mins_should_be_an_integer_in_the_range_0_to_59', '"Cancel reserved orders after" Minutes should be in range(0-59)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Include_Past_Due_POs/ASNs', 'Include Past Due POs/ASNs',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item', 'Item',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Template Executed', 'Template Executed',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Days_past_due_date_to_consider_should_be_an_integer_in_the_range_1_to_365', 'Use Pos/ASNs less than should be in range(1-365)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Effective', 'Effective',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Inventory_Alert_Template', 'Run Inventory Alert Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit DO Create Template', 'Edit DO Create Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Template', 'Allocation Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Business Unit', 'Business Unit',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution_Percentage_should_be_less_than_or_equals_to_100', 'Distribution Percentage should be less than or equals to 100% for Segment',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_number_of_days_after_creation_date_to_calculate_requested_delivery_date', 'Please define "Days" for "Calculate requested delivery date by adding entered number of days to order creation date" rule selection.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_Nbr', 'Order No.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Need Type', 'Need Types',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Has Split', 'Has Split',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_order_lines_to_exclude_them_from_the_prioritization', 'Select order lines to exclude them from the prioritization',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_reserved_orders_after_hours_should_be_an_integer_in_the_range_0_to_23', '"Cancel reserved orders after" Hours should be in range(0-23)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Color', 'Color',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_inventory_to_exclude_them_from_the_segmentation', 'Select inventory to exclude them from the segmentation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_Increment_ETA_by', 'Please define "Calculate ETA  as" for "Include Past Due POs/ASNs" rule selection',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reset', 'Reset',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Nbr', 'Allocation no.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_allocation_process', 'Select allocation process',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select at least one element', 'Please select atleast one element',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Monetary_Value_is_a_required_field_for_Currency', 'Monetary Value is a required field for Currency.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please unselect all selected filters', 'Please unselect all selected filters',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution order type', 'Distribution order type',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Creation Rules', 'DO Creation Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Overrun_available_capacity', 'Overrun available capacity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Filter_Description', 'Filter Description',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN', 'ASN',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid_Line_Items_Per_XML', 'Number of line items per XML should be a positive integer',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Monetary_Value_should_be_a_positive_value_for_Currency', 'Monetary Value should be a positive for Currency.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Processed On', 'Processed On',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Default', 'Default',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prioritization Template', 'Prioritization Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Prioritization Template', 'New Prioritization Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Publish allocated quantity for Item-Locations', 'Publish allocated quantity for item-locations',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Currency_is_a_required_field', 'Currency is a required field.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Consider ASNs and POs in decreasing order of ETA', 'Consider ASNs and POs in decreasing order of ETA',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate', 'Allocate',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line Attributes', 'Line Attributes',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Segmentation Template', 'Inventory Segmentation Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Item', 'Allocated Item',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Failure Reason', 'Failure Reason',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are_you_sure_that_you_want_to_delete_the_selected_template', 'Are you sure that you want to delete the selected template(s)?',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Latest Release By', 'Latest Release By',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered Quantity', 'Ordered Quantity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sun', 'Sun',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Flow Template', 'New Flow Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tuesday', 'Tuesday',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected_Filters', 'Selected Filters',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_order_line_after_days_should_be_should_be_a_non_negative_integer', '"Cancel order lines after" Days should be positive integer.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preview_Supply', 'Preview Supply',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment Name', 'Segment Name',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run every', 'Run every',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_strategy', 'Allocation strategy',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'End after', 'End after',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Id', 'Order ID',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thursday', 'Thursday',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Size_Value_should_be_an_integer_value_for_Size_UOM', 'Size Value should be an integer for UOM.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected', 'Selected',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply ETA', 'Supply ETA',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure that you want to delete the selected Record(s)', 'Are you sure that you want to delete the selected Record(s)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_Create_Allocations_Against_ASNs/POs', 'Create DOs for allocations against ASNs or POs if ASN/PO ETA within -',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete', 'Delete',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Sales Order Processing Template', 'New Sales Order Processing Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Template Name', 'Template name',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Merge_Shipment', 'Merge shipment',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfilling_part_of_the_order', 'Select the one fulfilling part of the order',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source Exclusion Template', 'Source Exclusion Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Result_Tab_Headline', 'Allocations listed below failed DO Creation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Current Line Status', 'Current Line Status',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Details', 'Details',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Inventory Sync Template', 'New Inventory Sync Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Sync Templates', 'Inventory Sync Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click_here_to_select_time_from_popup_Time_Entry', 'Click here to select time from popup Time Entry',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Size Description', 'Size Description',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Wednesday', 'Wednesday',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference Number 2', 'Reference Number 2',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference Number 1', 'Reference Number 1',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please define atleast one needs layer', 'Please define atleast one allocation layer',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UOM', 'UOM',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allow_partial_multi_source', 'Allow partial allocation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Days from current date and time', 'Days from current date and time',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment_tier_label', 'Fulfillment tier',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales_Channel', 'Sales Channel',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Destinations', 'Destination(s)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add Segment', 'Add Segment',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Move Up', 'Move Up',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ended', 'Ended',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transportation_cost', 'Transportation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Run Summaries found', 'No run summaries found',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert Settings', 'Alert Settings',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '-Remove', '< Remove',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Manage Source Exclusion', 'Add Source Exclusion',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Line', 'Order Line',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Flow_Template', 'Run Allocation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release DO to Warehouse Management', 'Release DO to fulfillment system',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment_Value_is_a_required_field_for_Segment', 'Segment Value is a required field for Segment',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC_Based_Substitution_Consumption_is_allowed_for_Even_Distribution_only', 'DC based substitution consumption is allowed for even distribution only',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Balance_workload', 'Balance workload',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Days', 'Days',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected_PO_ASN_Lines', 'Selected PO/ASN Lines',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release DO to host system', 'Release DO to host system',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add Size', 'Add Size',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_allocation_to_exclude_them_from_the_DO_creation', 'Select allocation to exclude them from the DO creation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_days_or_hours_for_cancel_order_lines_after', 'Please define "Days" or "Hours" for "Cancel order lines after" rule selection.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Number of constraints cannot exceed', 'Number of constraints cannot exceed',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Days Old', 'days old',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reapportionment', 'Reapportion',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ignore_watermark_levels', 'Consider onhand inventory below watermark',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Release Rules', 'DO Release Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Alert Levels', 'Alert Levels',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order_ID', 'Order no.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_Alert_Level_One_Less_Than_Level_Two', 'Level two alert inventory should be greater than level one alert',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated Quantity', 'Allocated Quantity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please define atleast one rank constraint', 'Please define atleast one Rank Constraint.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Rows to select', 'No rows to select',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Days_Old', 'Days old',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Goal Strategy', 'Fulfillment Goal Strategy',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit', 'Edit',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Types', 'Supply Types',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Size limits', 'Size limits',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_reserved_orders_after_days_should_be_a_non_negative_integer', '"Cancel reserved orders after" Days should be positive integer.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate_ASNs_in_Descending_order_of_ETA_is_allowed_for_Even_Distribution_only', 'Allocate ASNs in descending order of ETA is allowed for even distribution only',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Inventory_Segmentation_Template', 'Run Segmentation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attribute', 'Attribute',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Excluded Facility', 'Excluded Facility',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run Start Time', 'Start Time',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested_delivery_date', 'Req. delivery date',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation is successful if all lines matching the parameters defined can be filled completely.', 'Allocation is successful if all lines matching the parameters defined can be filled completely.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Color Suffix', 'Color Suffix',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Group allocations using', 'Group allocations using',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Value', 'Value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DOM_RUN_STATUS', 'Status',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Product Class', 'Product Class',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New DO Release Template', 'New DO Release Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'None Selected', 'None Selected',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Started', 'Started',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please Select at least one Attribute from the list', 'Please select at least one attribute from the list',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New DO Create Template', 'New DO Create Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_Create_Allocations_Against_Un_Releasable_Inventory', 'Disallow DO Creation for allocations against locked allocatable inventory',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Filter Name', 'Filter Name',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hours', 'Hours (0-23)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete Attribute', 'Delete Attribute',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sec. Dimension', 'Sec. Dimension',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Level One Alert Inventory', 'Level one alert inventory',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid_Alert_Level_Value_for_Level_Two', 'Level two alert inventory should be a non negative integer',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Status', 'Fulfillment Status',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected_Orders', 'Selected Orders',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Layer', 'Layer',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available', 'Available',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No status change', 'No status change',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Templates', 'Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Status', 'Supply Status',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order attributes', 'Order attributes',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Flow Template', 'Edit Flow Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_Templates', 'Allocation Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SKU attributes', 'Item attributes',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Goal Optimization', 'Fulfillment Goal Optimization',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reason', 'Reason',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply', 'Supply',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Inventory Alert Template', 'Edit Inventory Alert Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add', 'Add',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered_Item', 'Ordered Item',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rules', 'Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Business Partner', 'Business Partner',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thu', 'Thu',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unallocated quantity', 'Unallocated quantity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Exclusion Filters', 'Exclusion Filters',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Consolidate_Items', 'Merge items at the preferred fulfillment facility',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation_sequence', 'Allocation sequence',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation considerations', 'Allocation considerations',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '(none)', '(none)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Owner', 'Owner',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_only_one_template', 'Please select only one template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prioritization Rules', 'Prioritization Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_DO_Release_Template', 'Run Release',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Create Templates', 'DO Create Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Excluded Sources', 'Excluded Sources',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Calculate_ETA_as', 'Calculate ETA  as',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Replenish_consolidating_facility', 'Replenish merge facility by rounding to',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Move Down', 'Move Down',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Do_not_create_DO_until_confirmation_for_lines_being_procured_is_received', 'Do not create DO until confirmation for lines being procured is received',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_define_atleast_one_allocation_rule', 'Please define atleast one allocation rule',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Alert Templates', 'Inventory Alert Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Level Two Alert Inventory', 'Level two alert inventory',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Filters', 'Filters',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other', 'Other',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Templates', 'Allocation Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected_Order_Lines', 'Selected Order Lines',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Only_one_Template_should_be_selected_at_a_time', 'Only one Template should be selected at a time.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution Order Type', 'Distribution Order Type',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocator_Tier', 'Tier allocator',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Atleast_one_constraint_should_be_added', 'Atleast one constraint should be added',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Prioritization_Template', 'Run Prioritization',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Number of line items per XML', 'Number of line items per XML',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Parent Type', 'Parent Type',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transportation Status', 'Transportation Status',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preview_Allocations', 'Preview Allocations',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Increment_ETA_by_should_be_an_integer_in_the_range_0_to_365', 'Calculate ETA  as should be in range(0-365)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Release Template', 'DO Release Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Publish Orderline Status', 'Publish order updates  when selected fields are updated.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_PO_ASN_lines_to_exclude_them_from_the_allocation', 'Select PO/ASN lines to exclude them from the allocation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Origin Facility', 'Origin Facility',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prioritization Templates', 'Prioritization Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On_hand', 'On hand',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attempt to set order lines to its last status on critical change', 'Attempt to set order lines to its last status on critical change',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please Select at least one rule', 'Please select at least one rule',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Results', 'Results',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Segmentation Templates', 'Inventory Segmentation Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Rules', 'Allocation Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Number of days before requested delivery date order lines should be put on hold', 'Number of days before requested delivery date order lines should be put on hold',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flow Management', 'Flow Management',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Uncheck All', 'Clear All',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Prioritization Template', 'Edit Prioritization Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel reserved orders after', 'Cancel reserved orders after',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Friday', 'Friday',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Style Suffix', 'Style Suffix',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_fulfillment_tier', 'Please select atleast one fulfillment tier',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Consider_next_day_capacity', 'Consider next day`s capacity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_lines_with_allocated_quantity_equal_to_0', 'Cancel order lines with allocated quantity = 0',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Additional Parameters', 'Additional Parameters',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Allocation_Template', 'Run Allocation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Executed Template', 'Executed Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO transportation status', 'DO transportation status',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Descending', 'Descending',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please define atleast one allocation rule', 'Please define atleast one allocation rule.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_sequence', 'Run Sequence',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click here to select time from popup time', 'Click here to select time from popup time',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select atleast one allocation strategy', 'Please select atleast one allocation strategy.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_allocation_strategy', 'Please select atleast one allocation strategy',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sunday', 'Sunday',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Filter*', 'Filter*',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_of_the_Release_Rules', 'Please select atleast one of the release rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fri', 'Fri',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Excess supply distribution', 'Excess supply distribution',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Perform Second Cycle with MDD', 'Perform second cycle with MDD',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Fulfillment Order Line Found', 'No Fulfillment Order Line Found',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment_Value_should_be_a_positive_value_for_Segment', 'Segment Value should be a positive value for Segment',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_supply_type', 'Please select atleast one supply type',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Size_UOM_is_a_required_field_for_Size_Value', 'UOM is a required field for size value.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Calculate requested delivery date by adding entered number of days to order creation date', 'Calculate requested delivery date by adding entered number of days to order creation date',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is_Default', 'Default',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Sales Order Processing Template', 'Edit Sales Order Processing Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Inventory Sync Template', 'Edit Inventory Sync Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'All lines within an order must allocate', 'Group by sales order no.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Substitute item follows ordered item sourcing rule', 'Substitute item follows ordered item sourcing rule',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'when status is less than', 'when status is less than',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Type', 'Supply Type',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Use_POs_ASNs_less_than', 'Use POs/ASNs less than',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Inventory Segmentation Template', 'New Inventory Segmentation Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_template', 'Please select atleast one template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate_all_Purchase_Orders_for_the_same_Item_Location_Combination', 'Allocate all purchase orders for the same item-location combination',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_at_least_one_cost_option', 'Please select at least one cost',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add Attribute', 'Add Attribute',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_number_of_days_to_hold', 'Please define Days for "Number of days before requested delivery date order lines should be put on hold" rule selection.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid_Segment_Value_for_Segment', 'Invalid Segment Value for Segment',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocator_Facility', 'Facility allocator',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Allocation Template', 'Edit Allocation Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution Order no.', 'Distribution Order No.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment_Value_should_be_an_integer_value_for_Segment', 'Segment Value should be an integer value for Segment',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected_Allocations', 'Selected Allocations',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release DO to Transportation Management', 'Release DO to transportation management',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Manage Source Exclusion', 'Edit Source Exclusion',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Style', 'Style',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'De-allocate only', 'De-allocate only',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segmentation Rules', 'Segmentation Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Description', 'Description',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preview_Order_Lines', 'Preview Order Lines',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Split Rules', 'Split Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Line Constraints', 'Order line constraints',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid_Size_Value_for_Size_UOM', 'Invalid Size Value for UOM.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_On', 'Created On',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Maximum 3 size restrictions allowed', 'Maximum 3 size restrictions allowed.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution Strategy', 'Strategy',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment Order Line List', 'Fulfillment Order Line List',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Template ID', 'Template ID',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_DO_Create_Template', 'Run DO Creation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Layers', 'Allocation Layers',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment', 'Segment',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_define_atleast_one_processing_rule', 'At least one Sales order processing Rule or Publish Orderline status should be selected.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO', 'PO',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Number_of_days_to_hold_should_be_a_natural_number', '"Number of days before requested delivery date order lines should be put on hold" should be positive integer value.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are_you_sure_that_you_want_to_delete_the_selected_Template(s)_?', 'Are you sure that you want to delete the selected Template(s) ?',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_orders_to_exclude_them_from_the_release', 'Select orders to exclude them from the release',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'If all attribute rows are deleted, order lines will be prioritized based on the template rank only.', 'If all attribute rows are deleted, order lines will be prioritized based on the template rank only.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DOM_Process_Template', 'DOM Process Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In store', 'In store',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Release Templates', 'DO Release Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Template List Start Time', 'First Run',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered_Quantity', 'Ordered Quantity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preview_DO', 'Preview DO',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Aggregation', 'Aggregation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quality Level', 'Quality Level',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_cancel_reserved_orders_after', 'Please define either of "Days" or "Hours" or "Minutes" for "Cancel reserved orders after" rule selection.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Limit_to_single_PO', 'Limit to single PO',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sorting Values', 'Sort Values',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ranking Constraints', 'Ranking Constraints',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Nbr', 'Line No.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OK', 'OK',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Strategy', 'Strategy',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete Layer', 'Delete Layer',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel_order_line_after_hours_should_be_should_be_an_integer_in_the_range_0_to_23', '"Cancel order lines after" Hours should be in range(1-23)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Order Processing Template', 'Sales Order Processing Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Use', 'Use',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flow Template', 'Flow Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Monetary limit', 'Monetary limit',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run Summary Id', 'Run ID',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Demand Grouping Rules', 'Demand Grouping Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item selection', 'Item selection',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sync Settings', 'Sync Settings',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ordered Item', 'Ordered Item',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click here to select Facility', 'Click here to select Facility',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Preview_Inventory', 'Preview Inventory',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Limit_to_single_ASN', 'Limit to single ASN',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Template Found', 'No template found',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Create allocation group', 'Create allocation group',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Change Management', 'Change Management',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Save', 'Save',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'select one', 'select one',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Order Processing Rules', 'Processing Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Earliest Ship By', 'Earliest Ship By',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_merge_facility_resulting_in_optimized_cost', 'Merge required',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Prioritization Rules Found', 'No prioritization rules found',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Wed', 'Wed',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cost', 'Fulfillment cost',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Filter(s)', 'Filter(s)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Size Value', 'Size Value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'select_one', '(select one)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Capacity_management', 'Capacity management',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Re-allocate order lines on change of earliest deliver by date', 'Re-allocate order lines on change of earliest deliver by date',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Minutes (0-59)', 'Minutes (0-59)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Inventory Alert Template', 'New Inventory Alert Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Previous Line Status', 'Previous Line Status',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ascending', 'Ascending',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Are you sure you want to delete the selected Prioritization Rule(s)', 'Are you sure you want to delete the selected prioritization rule(s)',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Only one Template can be run at a time', 'Only one Template can be run at a time',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Saturday', 'Saturday',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please define atleast one segment', 'Please define atleast one segment.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Header Constraints', 'Order header constraints',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory selection', 'Inventory selection',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Promised Delivery Date', 'Promised Delivery Date',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Days_from_current_date_and_time', 'days from current date and time',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Segment_is_a_required_field', 'Segment is a required field.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Monday', 'Monday',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Strategy', 'Allocation Strategy',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment_tier', 'Fulfillment Tier',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'STEAL_FROM_LP_DISTRIBUTION_ORDERS', 'If required, steal allocatable inventory from other low priority DOs',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select atleast one filter', 'Please select atleast one filter',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delete Segment', 'Delete Segment',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Filter Selection', 'Filter Selection',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_cost', 'Inventory',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Instore', 'Instore',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Increase_priority', 'Increase priority',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'none', 'none',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory Segments', 'Inventory Segments',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add Layer', 'Add Layer',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Steal_inventory_from_lower_priority_lines', 'Steal inventory from lower priority lines',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run_Inventory_Sync_Template', 'Run Inventory Sync Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_define_atleast_one_change_management_option', 'At least one Change Management "option" should be selected.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Supply Detail', 'Supply Detail',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Runs', 'Runs',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Size_Value_is_a_required_field_for_Size_UOM', 'Size Value is a required field for UOM.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Selected_Inventory', 'Selected Inventory',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Schedule', 'Schedule',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity', 'Quantity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other_options', 'Other options',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Order Processing Templates', 'Sales Order Processing Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reservedForSegmentation', 'Reserved For Segmentation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please define atleast one filter for the template', 'Please define atleast one filter for the template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_order_line_status', 'Please define "when status is less than" for "Cancel order lines after" rule selection.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cumulative_of_Distribution_Percentage_on_all_the_Inventory_Segments_should_be_equal_to_100', 'Cumulative of Distribution Percentage on all the Inventory Segments should be equal to 100%.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduled day', 'Scheduled day',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_Alert_Level_One_Required', 'Level one alert inventory is a required field',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Size_Value_should_be_a_positive_value_for_Size_UOM', 'Size Value should be a positive for UOM.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule_description', 'Rule Description',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tue', 'Tue',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unallocatable Quantity', 'Unallocatable Quantity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mon', 'Mon',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manage_capacity', 'Manage capacity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In transit Allocation', 'In Transit Allocation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Copy', 'Copy',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'List Template Name', 'Template Name',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Allocation Template', 'New Allocation Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Onhand', 'Onhand',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO fulfillment status', 'DO fulfillment status',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Generate outbound xml', 'Publish information using xml',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Excluded for all orders', 'Excluded for all orders',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel order lines after', 'Cancel order lines after',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Create Alert after Allocation', 'Create alert after allocation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate ASNs in Descending order of ETA', 'Consider ASNs in decreasing order of ETA',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Check All', 'Check All',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Create Template', 'DO Create Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rank', 'Rank',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Add>', 'Add >',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '(1-23) Hours', '(1-23) Hours',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'History', 'History',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Run ID', 'Run ID',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available_Inventory_in_the_Segments_will_be_Recalculated', 'Available inventory in the segments will be recalculated.  Do you want to proceed?',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sat', 'Sat',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Part of allocation group',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Allocated against excluded facility',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Limit to single ASN', 'Limit to single ASN',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Expires', 'Expires',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Comments', 'Comments',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Days_before_arrival_should_be_an_integer_in_the_range_1_to_365', 'Days before arrival should be an integer in the range 1 to 365',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Available Quantity', 'Available Quantity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Latest Ship By', 'Latest Ship By',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Facility', 'Facility',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rule_description_can_not_be_empty', 'Rule description can not be blank',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Finalizer Strategy', 'Finalizer Strategy',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inventory_Alert_Level_Two_Required', 'Level two alert inventory is a required field',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Handling_cost', 'Handling',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_enter_days_past_due_date', 'Please define "Use Pos/ASNs less than" for "Include Past Due POs/ASNs" rule selection',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Decrease_priority', 'Decrease priority',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate order line across all supply types', 'Allocate order line across all supply types',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Filter_Name', 'Filter Name',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid_Alert_Level_Value_for_Level_One', 'Level one alert inventory should be a non negative integer',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested_Delivery_By', 'Requested Delivery By',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Inventory Segmentation Template', 'Edit Inventory Segmentation Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Items_Per_XML_Required', 'Number of line items per XML is a required field',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reservedForDemand', 'Reserved For Demand',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Select_UOM_For_Replenishing_Consolidating_Facility', 'UOM is required value for replenishing consolidating facility',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirm_Template_Delete', 'Are you sure that you want to delete the selected Template(s)?',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_Status', 'Line Status',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SaveApply', 'Run Segmentation',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid_Monetary_Value_for_Currency', 'Invalid Monetary Value for Currency.',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hours_before_arrival_should_be_an_integer_in_the_range_0_to_23', 'Hours before arrival should be an integer in the range 0 to 23',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_substn_type', 'Please select at least one substitution strategy',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fulfillment_Substitution_Type', 'Substitute',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Revert_allocations_from_traversed_rules_on_reset', 'Include allocations from previous rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Consider_Lead_Time', 'Consider lead time for scheduling',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '4', 'Immediate',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '16', 'When out',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please_select_atleast_one_field_to_save', 'Please select atleast one field to save',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Number_of_days_after_creation_date_to_calculate_requested_delivery_date_should_be_a_non_negative_integer', 'Number of days after creation date to calculate requested delivery date should be positive integer',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transportation_responsibility', 'Transportation responsibility:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billing_method', 'Billing method:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority', 'Priority:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Packaging', 'Packaging:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pack_and_Hold', 'Pack and Hold:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Billing_account_number', 'Billing account number:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partial_ship_confirm_flag', 'Partial ship confirm flag:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Move_type', 'Move type:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Movement_option', 'Movement option:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Drop-off_Pickup', 'Drop-off/Pickup:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Incoterm', 'Incoterm:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Block_auto-create', 'Block auto-create:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Block_auto-consolidation', 'Block auto-consolidation:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pre-sticker_code', 'Pre-sticker code:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LPN_cubing_indicator', 'LPN cubing indicator:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pallet_cubing_indicator', 'Pallet cubing indicator:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Prepack_cubing_indicator', 'Prepack cubing indicator:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partial_LPN_option', 'Partial LPN option:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Importer', 'Importer:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Duty_n_tax_payment_type', 'Duty and tax payment type:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allow_pre-billing', 'Allow pre-billing:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transportation_planning_owner', 'Transportation planning owner:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pre-Bill_Status', 'Pre-Bill Status:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LPN_label_type', 'LPN label type :',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pallet_content_label_type', 'Pallet content label type:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Content_label_type', 'Content label type:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Packing_slip_type', 'Packing slip type:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VAS_process_type', 'VAS process type :',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Line_type', 'Line type:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partial_fill', 'Partial fill:',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE_TYPE_ITEM', 'Item',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LINE_TYPE_PACKAGE', 'Package',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Boolean_true', 'True',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Boolean_False', 'False',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority_should_be_numeric', 'Priority should be a numeric value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority_should_be_in_range', 'Priority value should be between 1 and 999',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution_order_header_attributes', 'Distribution order header attributes',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Distribution_order_line_attributes', 'Distribution order line attributes',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Define_Distribution_Order_Attributes', 'Define Distribution Order Attributes',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Define_default_values', 'Define default values',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Priority_should_be_non_decimal', 'Please enter integer value for priority',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manage Source Exclusions', 'Manage Source Exclusions',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Number of attributes cannot exceed', 'Number of attributes cannot exceed',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Fee', 'Order Fee',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Line Fee', 'Order Line Fee',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fee Rules', 'Fee Rules',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fee type', 'Fee type',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fee Name', 'Fee Name',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fee_name', 'Fee name',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please define atleast one fee rule', 'Please define atleast one fee rule',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select fee name', 'Please select fee name',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please select fee type', 'Please select fee type',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please enter percentage fee value', 'Please enter percentage fee value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please enter flat fee value', 'Please enter flat fee value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please enter flat fee value per quantity', 'Please enter flat fee value per quantity',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please enter valid percentage value', 'Please enter valid percentage value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Value cannot be negative, Please enter valid percentage value', 'Value cannot be negative, Please enter valid percentage value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please enter percentage value between 0 and 100', 'Please enter percentage value between 0 and 100',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please enter valid flat fee value', 'Please enter valid flat fee value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Value cannot be negative, Please enter valid flat fee value', 'Value cannot be negative, Please enter valid flat fee value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Please enter valid flat fee value per quantity value', 'Please enter valid flat fee value per quantity value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Value cannot be negative, Please enter valid flat fee per quantity value', 'Value cannot be negative, Please enter valid flat fee per quantity value',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Duplicate fee name', 'Duplicate fee name',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Order Line Fee Template', 'Edit Order Line Fee Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Line Fee Templates', 'Order Line Fee Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Order Line Fee Template', 'New Order Line Fee Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Fee Templates', 'Order Fee Templates',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Order Fee Template', 'Edit Order Fee Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Order Fee Template', 'New Order Fee Template',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '% fees', '% fees',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flat fee', 'Flat fee',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flat fee per qty fee', 'Flat fee per qty fee',
           'TemplateManager')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Failure', 'Failure',
           'TransactionDecision')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Success', 'Success',
           'TransactionDecision')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unavailable', 'Unavailable',
           'TransactionDecision')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fraud', 'Fraud',
           'TransactionDecision')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Settlement', 'Settlement',
           'TransactionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Refund', 'Refund',
           'TransactionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Balancecheck', 'Balance check',
           'TransactionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Authorization', 'Authorization',
           'TransactionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'All', 'All',
           'TransactionType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '5', 'Set to Unplanned',
           'TransportationOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '-1', 'No status change',
           'TransportationOrderStatus')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fixed Value', 'Fixed value',
           'ValueTypeFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Fixed value',
           'ValueTypeFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'Percentage',
           'ValueTypeFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Percentage', 'Percentage',
           'ValueTypeFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reconcillation_Id', 'Reconciliation ID',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tries_left', 'No. of tries left',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Request_Token', 'Request token',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Details', 'Payment Details',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Trans_Decs_Desc', 'Transaction decision description',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_Exp_Date', 'Transaction expiration date',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'mark_As_Success', 'Mark as Success',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_Request_Id', 'Transaction Request ID',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ext_Resp_Code', 'External response code',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ext_Resp_Msg', 'External response message',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Card_No', 'Card No.',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_Decision', 'Transaction Decision',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Number', 'Invoice Number',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Method', 'Payment Method',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Record_Status', 'Transaction record status',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Transaction_Date', 'Transaction Date',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Follow_On_Token', 'Follow on token',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Additional_Details', 'Additional Details',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Transaction_id', 'Payment transaction ID',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Transaction_Type', 'Transaction Type',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Requested_Amount', 'Requested Amount',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Follow_On_Id', 'Follow on ID',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Processed_Amount', 'Processed Amount',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Trans_Req_Date', 'Transaction request date',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Transaction_Details', 'Payment Transactions Details',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Internal_Response_Code', 'Internal response code',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Internal_Response_Message', 'Internal response message',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Process_Parameter', 'Payment process parameter',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Bypass', 'Bypass',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Combo_Request_Id', 'Correlation ID',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Combo_Request_Token', 'Correlation token',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_Source', 'Created source',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_Source_Type', 'Created source type',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_Date', 'Created date',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Updated_Source', 'Last updated source',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Updated_Source_Type', 'Last updated source type',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Updated_Date', 'Last updated date',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_Updated_Process', 'Last updated process',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Transaction', 'Payment Transaction Details',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Audit_Details', 'Audit Details',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment_Transactions', 'Payment Transactions',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Details', 'Invoice Details',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Type', 'Invoice Type',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Created_Date', 'Invoice Created Date',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Amount', 'Invoice Amount',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoice_Amount_For_Transaction', 'Invoice Amount For Transaction',
           'ViewPaymentTransaction')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Deactivate', 'Deactivate',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_modified_by', 'Last Modified By',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Is_Active', 'Active',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Activate', 'Activate',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Name', 'Name',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Workflow_detail', 'Workflow Detail',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WF_Admin_Title', 'Workflow Administration',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Details', 'Details',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_on', 'Created on',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Versions', 'Versions',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View_active_flow', 'View Active Workflow',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Domain_group', 'Workflow Type',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'status', 'Status',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Workflow_Administration', 'Workflow Administration',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_by', 'Created by',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Id', 'Id',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'View', 'View',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Last_modified_on', 'Last Modified On',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rank', 'Rank',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Created', 'DO Created',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing Rule Set', 'Sourcing Rule Set',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN Rule Set', 'ASN Rule Set',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PO Rule Set', 'PO Rule Set',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sales Order', 'Sales Order',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On Success Allocation', 'On Success Allocation',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On Success Prioritization', 'On Success Prioritization',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'On Success Sourcing', 'On Success Sourcing',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EventManagement', 'EventManagement',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Static Route Rules', 'Static Route Rules',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return Order', 'Return Order',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EEM Workflow', 'EEM Workflow',
           'WorkflowAdminBundle')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Period', 'Period',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Event Name', 'Event name',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enabled', 'Enabled',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Update', 'Update',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Immediately', 'Immediately',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Object Type', 'Object type',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Edit Event Template', 'Edit Event Template',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Error', 'Error',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Operator', 'Operator',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rank', 'Rank',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Service Template', 'Service template',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release', 'Release',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Condition', 'Condition',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Configuration Rank is required field', 'Configuration rank is required field',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION_TEMPLATE', 'Allocation Template',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rank should be greater then 0', 'Rank should be greater then 0',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Value', 'Value',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'you can not create disabled Event Configuration', 'you can not create disabled event configuration',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'New Event Template', 'New Event Template',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Services', 'Order Services',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel', 'Cancel',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduled', 'Scheduled',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply On', 'Apply on',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Roll Up', 'Roll Up',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Event Description', 'Event description',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Create', 'Create',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Rank should be valid Number', 'Rank should be valid number',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Before / After', 'Before / After',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'of', 'of',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Service Name', 'Service name',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Order Service Execution', 'Order Service Execution',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Configuration Name', 'Configuration name',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Logical Operator', 'Logical operator',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Event Template List', 'Event Template List',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Event Template', 'Event Template',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Configuration Name is required field', 'Configuration name is required field',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation', 'Allocation',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Attribute', 'Attribute',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RELEASE_TEMPLATE', 'Release Template',
           'WorkflowConfig')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '10', 'WMiS',
           'WMSystemType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '20', 'WMOS',
           'WMSystemType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WMiS', 'WMiS',
           'WMSystemType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WMOS', 'WMOS',
           'WMSystemType')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No', 'No',
           'YesNoFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Yes', 'Yes',
           'YesNoFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '1', 'Yes',
           'YesNoFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '0', 'No',
           'YesNoFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Y', 'Yes',
           'YesNoFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'y', 'Yes',
           'YesNoFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'N', 'No',
           'YesNoFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'n', 'No',
           'YesNoFiniteValue')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NOT_APPLICABLE', 'Not Applicable',
           'PaymentStatusFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_PAYINFO', 'Await Payment',
           'PaymentStatusFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_AUTHORIZATION', 'Await Auth',
           'PaymentStatusFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AUTHORIZED', 'Authorized',
           'PaymentStatusFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVOICED', 'Invoiced',
           'PaymentStatusFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PAID', 'Paid',
           'PaymentStatusFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'e-Check', 'E-Check',
           'PaymentMethodFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Others', 'Others',
           'PaymentMethodFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift Card', 'Gift Card',
           'PaymentMethodFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Credit Card', 'Credit Card',
           'PaymentMethodFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Started', 'Started',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ProcessSyncTitle', 'Thread Details',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InProgress', 'In Progress',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thread End Time', 'Thread End Time',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thread Start Time', 'Thread Start Time',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thread Status', 'Thread Status',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process ID', 'Process ID',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process Name', 'Process Name',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process Status', 'Process Status',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process Start Time', 'Process Start Time',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process End Time', 'Process End Time',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Elapsed Time', 'Elapsed Time',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Object Count', 'Object Count',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Server Name', 'Server Name',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Failed', 'Failed',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Completed', 'Completed',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thread ID', 'Thread ID',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click to see exception', 'Click to see exception',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ExceptionTitle', 'Exception',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Event Detail', 'Service Name',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Reference Type', 'Object Type',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ReferenceID', 'Object ID',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ObjectSyncTitle', 'Object Details',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thread Name', 'Thread Name',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process Type', 'Process Type',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Object ID', 'TC Object ID',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Object ID Tip', 'Get Detail',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Get ID', 'Get ID',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Additional Details', 'Additional Details',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASYNC_WORKFLOW_SERVICE_LOGGING_ENABLED', 'Workflow logging enabled for asynchronous services',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASYNC_WORKFLOW_SERVICE_LOGGING_ENABLED_DESC', 'Workflow logging enabled for asynchronous services',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Configuration Warning Note', '*Note : Please consult technical team before modifying the parameter values. Default value will be preserved for some cases.',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WORKFLOW_AGENT_LOGGING_ENABLED', 'Log data flow for workflow agent',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VIEW_HISTORY_TABLE_DATA', 'Show data from history table',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PROCESS_VIEW_HISTORY_SCHEDULER_INTERVAL', 'Frequency (in hours) to move data to history table (from active table)',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NO_OF_ROWS_PER_BATCH_FROM_A_PROCESS_LOG', 'Batch size for persisting data to active table (from in-memory)',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NO_OF_DAYS_FOR_CLEARING_PROCESS_VIEW_LOG_TABLE', 'Duration (in days) to keep data in active table',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PROCESSVIEW_SCHEDULER_INTERVAL', 'Frequency (in seconds) to push in-memory data to active table',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PROCESSVIEW_LOGGING_BATCH_SIZE', 'Batch size for persisting data from active to history table',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PROCESS_VIEW_LOGGING_ENABLED', 'Data flow logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVENTORY_RISE_LOGGING_ENABLED', 'Inventory rise event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_UPDATE_LOGGING_ENABLED', 'DO update logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ORDER_STATUS_PROPOGATION_LOGGING_ENABLED', 'Order header status propagation logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVENTORY_DROP_LOGGING_ENABLED', 'Inventory drop event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WORKFLOW_DIRTY_READ_SCHEDULER_LOGGING_ENABLED', 'Workflow Dirty Read scheduler logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED_ALLOCATION_TEMPLATE_LOGGING_ENABLED', 'Scheduled Allocation template logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED_DO_CREATE_TEMPLATE_LOGGING_ENABLED', 'Scheduled DO create template logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED_DO_RELEASE_TEMPLATE_LOGGING_ENABLED', 'Scheduled DO release template logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SUBMIT_ORDERLINE_EVENT_LOGGING_ENABLED', 'Submit order line workflow event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MAX_IN_MEMORY_MAP_SIZE', 'Automatically shut down data flow persistence when in-memory object count reaches to',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'THRESHOLD_IN_MEMORY_MAP', 'Automatically disable data flow logging when in-memory object count reaches to',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PROCESS_VIEW_LOGGING_ENABLED_DESC', 'Data flow logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WORKFLOW_AGENT_LOGGING_ENABLED_DESC', 'Log data flow for workflow agent (for Sourcing, Allocation, DO Create and DO Release services)',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVENTORY_RISE_LOGGING_ENABLED_DESC', 'Inventory rise event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_UPDATE_LOGGING_ENABLED_DESC', 'DO update logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ORDER_STATUS_PROPOGATION_LOGGING_ENABLED_DESC', 'Order header status propagation logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVENTORY_DROP_LOGGING_ENABLED_DESC', 'Inventory drop event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WORKFLOW_DIRTY_READ_SCHEDULER_LOGGING_ENABLED_DESC', 'Workflow Dirty Read scheduler logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED_ALLOCATION_TEMPLATE_LOGGING_ENABLED_DESC', 'Scheduled Allocation template logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED_DO_CREATE_TEMPLATE_LOGGING_ENABLED_DESC', 'Scheduled DO create template logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED_DO_RELEASE_TEMPLATE_LOGGING_ENABLED_DESC', 'Scheduled DO release template logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SUBMIT_ORDERLINE_EVENT_LOGGING_ENABLED_DESC', 'Submit order line workflow event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VIEW_HISTORY_TABLE_DATA_DESC', 'Show data from history table',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PROCESS_VIEW_HISTORY_SCHEDULER_INTERVAL_DESC', 'Frequency (in seconds) to push in-memory data to active table',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NO_OF_ROWS_PER_BATCH_FROM_A_PROCESS_LOG_DESC', 'Batch size for persisting data to active table (from in-memory)',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NO_OF_DAYS_FOR_CLEARING_PROCESS_VIEW_LOG_TABLE_DESC', 'Duration (in days) to keep data in active table',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PROCESSVIEW_SCHEDULER_INTERVAL_DESC', 'Frequency (in seconds) to push in-memory data to active table',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PROCESSVIEW_LOGGING_BATCH_SIZE_DESC', 'Batch size for persisting data from active to history table',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MAX_IN_MEMORY_MAP_SIZE_DESC', 'Automatically shut down data flow persistence when in-memory object count reaches to',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'THRESHOLD_IN_MEMORY_MAP_DESC', 'Automatically disable data flow logging when in-memory object count reaches to',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Disabled', 'Disabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Enabled', 'Enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ChangeOrModify', 'Change/Modify',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Restore to Default', 'Restore to Default',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Propagate to Cluster', 'Propagate to Cluster',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Parameter Name', 'Parameter Name',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Parameter Value', 'Parameter Value',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Parameter Scope', 'Scope',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Action Local', 'Action',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DataFlowTitle', 'Monitor Data Flow',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Configuration', 'Configuration',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ConfigScreenTitle', 'Configuration',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Dashboard', 'Dashboard',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process', 'Processes',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thread', 'Threads',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Object', 'Objects',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Logging', 'Basic',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Advanced', 'Advanced',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WORKFLOW_SERVICE_LOGGING', 'Log data flow for workflow services',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WORKFLOW_SERVICE_LOGGING_DESC', 'Log data flow for workflow services',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Memory Map Size', 'Object queue size',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Refresh Now', 'Refresh Now',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SUBMIT ORDERLINES EVENT', 'Submit OrderLines Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WORKFLOW AGENT', 'Workflow Agent',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'startTimeFilter', 'Process History',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'refreshTimeFilter', 'Refresh Every',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Running Threads', 'Active Threads',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Running Objects', 'Active Objects',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Running Process', 'Active Processes',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process History', 'Process History',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Thread History', 'Thread History',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Object History', 'Object History',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'START', 'Started',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INPROGRESS', 'Active',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'END', 'Completed',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED', 'Failed',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Minutes Back', 'Minutes',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Apply', 'Apply',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Minutes', 'Minutes',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION TEMPLATE SCHEDULER', 'Allocation Template Scheduler',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DOCREATE TEMPLATE SCHEDULER', 'DO Create Template Scheduler',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DORELEASE TEMPLATE SCHEDULER', 'DO Release Template Scheduler',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO UPDATE', 'DO Update',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INV RISE SCHED PROCESS', 'Inventory Rise Scheduler',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ORDER HEADER PROPAGATION', 'Order Header Propagation',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SUBMIT ORDER LINES', 'Submit Order Line',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WORKFLOW PROCESS', 'Workflow',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION TEMPLATE', 'Allocation Template',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DISTRIBUTION ORDER', 'Distribution Order',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO LINE', 'DO Line',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVENTORY', 'Inventory',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SALES ORDER', 'Sales Order',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SALES ORDERLINE', 'Sales Orderline',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WORKFLOW', 'Workflow',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INV DROP SCHED PROCESS', 'Inventoy Drop Scheduler',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER ORDER CREATE EVENT', 'Customer Order Create Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER ORDER UPDATE EVENT', 'Customer Order Update Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UPDATE QUANTITY BY PROMOTION EVENT', 'Update Quantity By Promotion Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CONFIRM ORDER UPDATE EVENT', 'Confirm Order Update Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER ORDER CANCEL EVENT', 'Customer Order Cancel Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SETTLEMENT SUCCESS EVENT', 'Settlement Success Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AUTHORIZATION SUCCESS EVENT', 'Authorization Success Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REFUND SUCCESS EVENT', 'Refund Success Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SETTLEMENT FAILURE EVENT', 'Settlement Failure Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'COMMUNICATION NOTES UI CREATE EVENT', 'Communication Notes UI Create Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'COMMUNICATION NOTES CREATE EVENT', 'Communication Notes Create Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'COMMUNICATION NOTES UPDATE EVENT', 'Communication Notes Update Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SNH CHARGE OVERRIDE EVENT', 'S&H Charge Override Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RECALCULATE SNH CHARGE EVENT', 'Recalculate S&H Charge Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AUTHORIZATION FAILURE EVENT', 'Authorization Failure Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'APPLY DISCOUNT CODES EVENT', 'Apply Discount Codes Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'APPLY APPEASEMENT EVENT', 'Apply Appeasement Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REMOVE APPEASEMENT EVENT', 'Remove Appeasement Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REMOVE DISCOUNT CODES EVENT', 'Remove Discount Codes Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPMENT CREATION EVENT', 'Shipment Creation Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ON DO CREATE EVENT', 'On DO Create Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ON ALLOCATE EVENT', 'On Allocate Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BACKORDER CREATE EVENT', 'Backorder Create Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TRANSFER_ALLOCATION_EVENT_LOGGING_ENABLED', 'Transfer allocation event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TRANSFER_ALLOCATION_EVENT_LOGGING_ENABLED_DESC', 'Transfer allocation event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REALLOCATION_EVENT_LOGGING_ENABLED', 'Re-allocation event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REALLOCATION_EVENT_LOGGING_ENABLED_DESC', 'Re-allocation event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BACKORDERED_EVENT_LOGGING_ENABLED', 'Backorder create event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BACKORDERED_EVENT_LOGGING_ENABLED_DESC', 'Backorder create  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION_EVENT_LOGGING_ENABLED', 'On Allocate event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION_EVENT_LOGGING_ENABLED_DESC', 'On Allocate  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_CREATE_EVENT_LOGGING_ENABLED', 'On DO create event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO_CREATE_EVENT_LOGGING_ENABLED_DESC', 'On DO create event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPMENT_CREATION_EVENT_LOGGING_ENABLED', 'Shipment creation  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPMENT_CREATION_EVENT_LOGGING_ENABLED_DESC', 'Shipment creation  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DISCOUNT_APPEASEMENT_EVENT_LOGGING_ENABLED', 'Appeasement/Discount  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DISCOUNT_APPEASEMENT_EVENT_LOGGING_ENABLED_DESC', 'Appeasement/Discount  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SNH_CHARGE_EVENT_LOGGING_ENABLED', 'S&H override/re-calculate  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SNH_CHARGE_EVENT_LOGGING_ENABLED_DESC', 'S&H override/re-calculate  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'COMMUNICATION_NOTES_EVENT_LOGGING_ENABLED', 'CO communication notes  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'COMMUNICATION_NOTES_EVENT_LOGGING_ENABLED_DESC', 'CO communication notes  event logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AUTH_SETTLEMENT_REFUND_EVENT_LOGGING_ENABLED', 'Authorization, refund and settlement events logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AUTH_SETTLEMENT_REFUND_EVENT_LOGGING_ENABLED_DESC', 'Authorization, refund and settlement events logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO_CREATE_UPDATE_CANCEL_EVENT_LOGGING_ENABLED', 'Customer Order create, update and cancel  events logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO_CREATE_UPDATE_CANCEL_EVENT_LOGGING_ENABLED_DESC', 'Customer Order create, update and cancel  events logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SELF_HEALING_ENABLED_FOR_WORKFLOW', 'Auto recovery for Workflow services/events enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SELF_HEALING_ENABLED_FOR_WORKFLOW_DESC', 'Auto recovery for Workflow services/events enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Retry Count', 'Retry Count',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION', 'Allocation',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER ORDER', 'Customer Order',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER ORDERLINE', 'Customer Orderline',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'LPN', 'LPN',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PURCHASE ORDER', 'Purchase Order',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ASN', 'ASN',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVOICE', 'Invoice',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RETURN ORDER', 'Return Order',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER MASTER', 'Customer Master',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invoke/Retrieve Workflow Service', 'Workflow',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REALLOCATION EVENT', 'Reallocation Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RETURN ORDER UPDATE EVENT', 'Return Order Update Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RETURN ORDER CREATE EVENT', 'Return Order Create Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RETURN ORDER ASN RECEIVED VERIFICATION EVENT', 'Return Order ASN Received Verification Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RETURN ORDER CANCEL EVENT', 'Return Order Cancel Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RETURN ORDER PROCESS EVENT', 'Return Order Process Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RESEND RETURN LABEL EVENT', 'Resend Return Label Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER MASTER CREATE EVENT', 'Customer Master Create Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER MASTER UPDATE EVENT', 'Customer Master Update Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER MASTER REGISTER EVENT', 'Customer Master Register Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER_MASTER_EVENT_LOGGING_ENABLED', 'Customer Master events logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CUSTOMER_MASTER_EVENT_LOGGING_ENABLED_DESC', 'Customer Master events logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RETURN_ORDER_EVENT_LOGGING_ENABLED', 'Return Order events logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RETURN_ORDER_EVENT_LOGGING_ENABLED_DESC', 'Return Order events logging enabled',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Propagation_Failed', 'Propagation Failed',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Propagation_Succeeded', 'Propagation Succeeded',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Propagation_Succeeded_Partially', 'Propagation Succeeded Partially',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Progress_Text', 'Please wait�',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Response', 'Response',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CLUSTER_INFO_DESC', 'Mention all the servers delimited by semicolon (for e.g. Host1/IP1:SpringRMIPort1;Host2/IP2:SpringRMIPort2)',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CLUSTER_INFO', 'Servers in cluster',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Propagation_Not_Happened', 'Propagation Not Happened',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Click to see original exception', 'Click to see original exception',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ON_HAND', 'On hand',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'TC_OBJECT_ID_NOT_EXIST', 'TC Object ID does not exist',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Source', 'Source',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocate', 'Allocate',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Create DO', 'Create DO',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Release', 'Release',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fetch Inventory Event', 'Fetch Inventory Event',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WF:Source', 'WF:Source',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WF:Allocate', 'WF:Allocate',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WF:Create DO', 'WF:Create DO',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'WF:Release DO', 'WF:Release DO',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Header Status Propagation', 'Header Status Propagation',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Process Parameters', 'Process Parameters',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO Source', 'CO Source',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO Cost Based Source', 'CO Cost Based Source',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO Prioritize', 'CO Prioritize',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO Process Parameters', 'CO Process Parameters',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO Allocate', 'CO Allocate',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO Create DO', 'CO Create DO',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO Release DO', 'CO Release DO',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO Undo Source', 'CO Undo Source',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CO Undo Allocate', 'CO Undo Allocate',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Undo Service', 'Undo Service',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'UNDO DCALLOCATE', 'UNDO DCALLOCATE',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO ALLOCATE', 'DO ALLOCATE',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO REVIVE', 'DO REVIVE',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO SHIPPED', 'DO SHIPPED',
           'DATAFLOW')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO SHORT', 'DO SHORT',
           'DATAFLOW')
/
INSERT INTO LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL, 'PROCESSVIEW_AUTO_RECOVER_SCHEDULER_INTERVAL', 'Frequency (in seconds) to auto recover failed processes',
           'DATAFLOW')
/		   
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'save', 'Save',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'saveUC', 'SAVE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'putLinesOnHold', 'Put the order line(s) on hold',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderNo', 'Order No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'comments', 'Comments',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'status', 'Status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemSubtotal', 'Item subtotal',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemSubtotalUC', 'ITEM SUB TOTAL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'estimatedSnH', 'Estimated S & H',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'estimatedTaxes', 'Estimated taxes',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'estimatedTaxesUC', 'ESTIMATED TAXES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderTotalUC', 'ORDER TOTAL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderTotal', 'Order total',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'color', 'Color',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'colorUC', 'COLOR',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'size', 'Size',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'sizeUC', 'SIZE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unitPrice', 'Unit price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'qty', 'Qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unitPriceUC', 'UNIT PRICE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lineSubtotal', 'Line subtotal',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lineTotal', 'Line total',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'apply', 'Apply',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'applyUC', 'APPLY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'user', 'User',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'updatedOn', 'Updated on',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reasonCode', 'Reason',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'chooseReasonCode', 'Choose a reason code',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'updatedBy', 'Updated by',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectReason', 'Select a reason',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'describeReason', 'Describe reason',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addressVerificationFailed', 'Address verification failed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'keepAddress', 'Save the address as is',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'avsHoldReason', 'Reason: AVS Failure',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippingAddressValid', 'Shipping address is valid',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'youEntered', 'You entered',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'weFound', 'We found',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'chooseRecommendedAddress', 'Please review the recommended address(es) and choose one',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addressVerification', 'Address verification',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'disableSnHChargeOverrideToopTipMsg', 'S & H charge cannot be recalculated as it is not overridden yet',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'disablePriceOverrideToopTipMsg', 'Price cannot be reverted as it is not overridden yet',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'couponsUC', 'COUPONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'coupons', 'Coupons',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'couponEmptyText', 'Separate the coupon codes with comma',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'couponMultiSelectHdrMsg', 'SELECT ONE OR MORE COUPONS TO REMOVE FROM THE ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'couponHdrTitle', 'ITEM & COUPONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'couponActionConfirmationMsg', 'ARE YOU SURE YOU WANT TO DELETE THE SELECTED COUPONS?',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'more', 'more',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'moreUC', 'MORE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discount', 'Discount',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discountUC', 'DISCOUNT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'code', 'Code',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemdiscounts', 'Item discounts',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalCoupons', 'Total coupons',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'couponValue', 'Total coupon value',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalCouponsValue', 'Total coupons value',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noCouponsMsg', 'NO COUPONS APPLIED YET',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'overridePriceSC', 'Override Price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'overridePrice', 'OVERRIDE PRICE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'currentPrice', 'Current price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'newPrice', 'New price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'revertToOriginalprice', 'Revert to original price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'thisOrder', 'This Order',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'allOrders', 'All Orders',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noOverridesOnItemPrice', 'No overrides on item price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'originalPrice', 'Original price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'changedPrice', 'Changed price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'overrideShippingAndHandlingCharge', 'OVERRIDE S&H CHARGES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'overrideShippingAndHandlingChargeSC', 'Override S&H charges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'currentSnHCharge', 'Current S&H charges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'newCharge', 'New charges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'recalculateSnHCharge', 'Recalculate S&H charges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noOverridesOnSnHCharge', 'No overrides on S&H charges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'originalCharge', 'Original charges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'changedCharge', 'Changed charges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'priceOverrideHistory', 'Price override history',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'snhOverrideHistory', 'S&H override history',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerDetailsUC', 'CUSTOMER DETAILS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'identifyRegisteredCustomerUC', 'IDENTIFY REGISTERED CUSTOMER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'nameUC', 'NAME',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'name', 'Name',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addressUC', 'ADDRESS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerIdUC', 'CUSTOMER ID',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'emailUC', 'EMAIL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'email', 'Email',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'phoneNoUC', 'PHONE NO.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'createOrderForNewCustomer', 'CREATE ORDER FOR NEW CUSTOMER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'firstNameUC', 'FIRST NAME',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'firstName', 'First Name',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lastName', 'Last Name',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lastNameUC', 'LAST NAME',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'enterAtLeastOneOfTheFollowing', 'Enter at least one of the following',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pleaseCorrectTheHighlightedFields', 'Please correct the highlighted fields',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'done', 'Done',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'doneUC', 'DONE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'sameAsShippingAddress', 'SAME AS SHIPPING ADDRESS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'chooseAddress', 'CHOOSE ADDRESS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cardInformationUC', 'CARD INFORMATION',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cardType', 'Card type',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cardTypeUC', 'CARD TYPE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cardNo', 'Card No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cardNoUC', 'CARD NO.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'expDateUC', 'EXP DATE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'securityCode', 'Security code',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'securityCodeUC', 'SECURITY CODE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amount', 'Amount  ($)',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amountUC', 'AMOUNT  ($)',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'add', 'Add',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addUC', 'ADD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reset', 'Reset',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'resetUC', 'RESET',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'checkNo', 'Check No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'checkNoUC', 'CHECK NO.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'routingNo', 'Routing No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'routingNoUC', 'ROUTING NO.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'accountNo', 'Account No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'accountNoUC', 'ACCOUNT NO.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'accountType', 'Account  type',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'accountTypeUC', 'ACCOUNT  TYPE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerType', 'Customer type',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerTypeUC', 'CUSTOMER TYPE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'businessName', 'Business name',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'businessNameUC', 'BUSINESS NAME',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'driversLicenseNo', 'Drivers license no.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'driversLicenseNoUC', 'DRIVERS LICENSE NO.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'federalTaxId', 'FEDERAL TAX ID',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'driversLicenseState', 'Drivers license state',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'driversLicenseStateUC', 'DRIVERS LICENSE STATE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'driversLicenseCountry', 'Drivers license country',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'driversLicenseCountryUC', 'DRIVERS LICENSE COUNTRY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pin', 'PIN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'phone', 'Phone',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'phoneUC', 'PHONE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'expiryDate', 'Expiry date',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'accNo', 'Acc. No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'accType', 'Acc. type',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'fedaralTaxId', 'Federal tax ID',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'edit', 'Edit',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editUC', 'EDIT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentUC', 'PAYMENT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'date', 'Date',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'dateUC', 'DATE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancel', 'Cancel',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancelUC', 'CANCEL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addressLine', 'Address Line',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addressLineUC', 'ADDRESS LINE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'city', 'city',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cityUC', 'CITY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'state', 'state',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'stateUC', 'STATE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'zipCode', 'zip code',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'zipCodeUC', 'ZIP CODE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'country', 'country',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'countryUC', 'COUNTRY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amountPaid', 'Amount paid',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amountPaidUC', 'AMOUNT PAID',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'remainingToPay', 'Remaining to pay',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'remainingToPayUC', 'REMAINING TO PAY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentOptions', 'PAYMENT OPTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'nameOnCard', 'Name on card',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'availableUC', 'AVAILABLE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'available', 'Available',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'applied', 'Applied',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appliedUC', 'APPLIED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'remaining', 'Remaining',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reasonDescription', 'Reason description',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pickUp', 'PICK UP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipTo', 'SHIP TO',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noteTypeUC', 'NOTE TYPE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noteType', 'NOTE TYPE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'note', 'NOTE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noteUC', 'NOTE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'yes', 'Yes',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'yesUC', 'YES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'no', 'No',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noUC', 'NO',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'locateStoresFor', 'LOCATE STORES FOR',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'within', 'within',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'of', 'of',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'or', 'or',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'go', 'GO',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'goUC', 'GO',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'store', 'STORE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'distance', 'DISTANCE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'actions', 'ACTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noStoresFound', 'No stores found',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'availableToShip', 'AVAILABLE TO SHIP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderStatusLink', 'Order Status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderStatus', 'Order status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editOrder', 'Edit order',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'createReturn', 'Create return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'createReturnUC', 'CREATE RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'relatedReturns', 'Related returns',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerTransactions', 'Customer transactions',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerTransactionsUC', 'CUSTOMER  TRANSACTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'createOrder', 'Create order',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lastOrder', 'Last order',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lastReturn', 'Last return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnNo', 'Return No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnStatusUC', 'RETURN STATUS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnStatus', 'Return status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'relatedOrderStatus', 'Related order status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'useAsBillingAddress', 'USE AS BILLING ADDRESS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'useThis', 'USE THIS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerAddresses', 'CUSTOMER ADDRESSES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'storeAddresses', 'STORE ADDRESSES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'checkAvailability', 'CHECK AVAILABILITY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'update', 'UPDATE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'updateUC', 'UPDATE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'proceedToPayment', 'PROCEED TO PAYMENT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addNewStore', 'ADD NEW STORE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'storeSelector', 'STORE SELECTOR',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'changeStore', 'Change Store',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemList', 'Item List',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'searchUC', 'SEARCH',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'priceSortUC', 'PRICE SORT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'productClass', 'Product class',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'productClassUC', 'PRODUCT CLASS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'style', 'Style',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'styleUC', 'STYLE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemSearchUC', 'ITEM SEARCH',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addItemUC', 'ADD ITEM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addItemProceedUC', 'ADD ITEM & PROCEED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'businessUnit', 'BUSINESS UNIT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemDescription', 'ITEM DESCRIPTION',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemComponents', 'ITEM COMPONENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'season', 'Season',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'seasonYear', 'Season Year',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'styleSuffix', 'Style Suffix',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'colorSuffix', 'Color Suffix',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'secondDimension', 'Second Dimension',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'sizeDescription', 'Size Description',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'specification', 'SPECIFICATION',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'nmfcFreightClass', 'NMFC Freight Class',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'commodityCode', 'Commodity Code',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'commodityLevel', 'Commodity Level',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'description', 'Description',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'productType', 'Product Type',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'protectionLevel', 'Protection Level',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'hazardousMaterialCode', 'Hazardous Material Code',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'barCode', 'Bar Code',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'gtin', 'GTIN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'bundleQuantity', 'Bundle Quantity',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'catchWeightItem', 'Catch Weight Item',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'variableWeightItem', 'Variable Weight Item',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'baseUom', 'Base UOM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'quantityUom', 'Quantity UOM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'dimensionUom', 'Dimension UOM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unitLength', 'Unit Length',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unitWidth', 'Unit Width',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unitHeight', 'Unit Height',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unitWeight', 'Unit Weight',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unitVolume', 'Unit Volume',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'colorDescription', 'Color Description',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'qualityLevel', 'Quality Level',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'channelType', 'Channel Type',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'receivingParameters', 'RECEIVING PARAMETERS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'expDateRequired', 'Exp. Date Required',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'maxDaysBeforeExp', 'Max. Days before Exp.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'minDaysBeforeExp', 'Min. Days before Exp.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'sku', 'SKU',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'skuUC', 'SKU',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'storeUnitPriceUC', 'STORE UNIT PRICE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemDetails', 'Item Details',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemDetailsUC', 'ITEM DETAILS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'checkout', 'CHECKOUT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'checkoutUC', 'CHECKOUT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotionsDiscountsUC', 'PROMOTIONS & DISCOUNTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noPromotionsOrDiscountsOnThisItem', 'No promotions or discounts on this item.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'deliveryOptionsUC', 'DELIVERY OPTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipToAddress', 'Ship to address',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pickUpAtStore', 'Pick up at store',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectAStore', 'Select a store',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipToStore', 'Ship to store',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippingMethodsUC', 'SHIPPING METHODS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noShippingMethodsAvailableOnThisItem', 'No shipping methods available on this item.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'otherDeliveryOptions', 'Other delivery options',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewDescription', 'View Description',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderPromotionsUC', 'ORDER PROMOTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderCouponsUC', 'ORDER COUPONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancelOrderUC', 'CANCEL  ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotionCouponsPopUpComingSoonUC', 'Promotion Coupons Pop Up Coming Soon',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ok', 'Ok',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'okUC', 'OK',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalDiscountUC', 'TOTAL DISCOUNT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'estimatedSH', 'Estimated S&H',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'estimatedSHUC', 'ESTIMATED S&H',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'registerCustomerUC', 'REGISTER CUSTOMER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SaveAddressInformationUC', 'SAVE ADDRESS INFORMATION',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'preferredShippingAddress', 'Preferred shipping address',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'preferredBillingAddress', 'Preferred billing address',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SavePaymentInformationUC', 'SAVE PAYMENT INFORMATION',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'preferredPaymentInformation', 'Preferred payment information',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'preferredShippingMethod', 'Preferred shipping method',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'updateCustomerDetailsForUC', 'UPDATE CUSTOMER DETAILS FOR',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'saveUpdatedAddressInformation', 'Save updated address information',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'saveUpdatedPaymentInformation', 'Save updated payment information',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'created', 'Created',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalDiscounts', 'Total discounts',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noAppeasementsAppliedUC', 'NO APPEASEMENTS APPLIED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appeasementsUC', 'APPEASEMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderItemAppeasementsUC', 'ORDER & ITEM APPEASEMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'areYouSureYouWantToRemoveTheAppeasementUC', 'ARE YOU SURE YOU WANT TO REMOVE THE APPEASEMENT?',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'type', 'Type',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'typeUC', 'TYPE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'enteredValue', 'Entered value',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appliedValue', 'Applied value',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appliedBy', 'Applied by',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appliedOn', 'Applied on',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'reason', 'Reason',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'discounts', 'Discounts',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noCouponsAppliedYetUC', 'NO COUPONS APPLIED YET',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pleaseEnterCouponCode', 'Please enter coupon code',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'removeUC', 'REMOVE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'remove', 'Remove',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotionNameUC', 'PROMOTION NAME',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'statusOfPromotionsUC', 'STATUS OF PROMOTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noItemPromotionsAvailable', 'NO ITEM PROMOTIONS AVAILABLE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotionsUC', 'PROMOTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotions', 'Promotions',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemPromotionsUC', 'ITEM & PROMOTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'close', 'CLOSE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'closeUC', 'CLOSE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noOrderPromotionsAvailable', 'NO ORDER PROMOTIONS AVAILABLE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'startsFrom', 'Starts from',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'expiresOn', 'Expires on',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'restrictionsUC', 'RESTRICTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'minimumQtyToBeOrderedToBeEligible', 'Minimum Qty. to be ordered to be eligible',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'maximumQtyToWhichThePromotionCanBeApplied', 'Maximum Qty. to which the promotion can be applied',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'minimumPurchaseAmountToBeEligible', 'Minimum purchase amount to be eligible',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'maximumPurchaseAmountToWhichThePromotionCanBeApplied', 'Maximum purchase amount to which the promotion can be applied',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'updateQtyUC', 'UPDATE QTY.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'onHoldUC', 'ON HOLD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'onHold', 'On Hold',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'GiftUC', 'Gift',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addAMessage', 'Add a message',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'comment', 'Comment',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'statusSummary', 'STATUS SUMMARY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'statusSummaryUC', 'STATUS SUMMARY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemsInTheOrder', 'ITEMS IN THE ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemsInTheOrderUC', 'ITEMS IN THE ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'return', 'Return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnUC', 'RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'hold', 'Hold',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipReturnsFromUC', 'SHIP RETURNS FROM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'standardDeliveryUC', 'STANDARD DELIVERY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalReturnAmount', 'Total return amount',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalFees', 'Total fees',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalFeesUC', 'TOTAL FEES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalCharge', 'Total charge',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'extimatedRefund', 'ESTIMATED REFUND',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'estimatedRefund', 'Estimated refund',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'estimatedRefundUC', 'ESTIMATED REFUND',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noItemsPresentOrRetrieved', 'NO ITEMS PRESENT OR RETRIEVED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'exchangeUC', 'EXCHANGE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnQtyUC', 'RETURN QTY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnQty', 'Return Qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paid', 'PAID',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnableQty', 'Returnable Qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'condition', 'Condition',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pleaseProvideReasonForReturns', 'Please provide reason for returns',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pleaseProvideExpectedItemCondition', 'Please provide expected item condition',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemName', 'Item name',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pleaseProvideAReasonForTheAction', 'Please provide a reason for the action',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noHeaderFees', 'No header fees',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectItemsToReturnOrExchange', 'SELECT ITEMS TO RETURN OR EXCHANGE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnType', 'Return type',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returns', 'RETURNS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'exchanges', 'EXCHANGES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'proceedToReview', 'PROCEED TO REVIEW',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'fees', 'Fees',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemsReturnFromAddressNotAvailable', 'Items return from address not available',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'exchangesShipToAddressNotAvailable', 'Exchanges ship to address not available',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnOtherItemsInTheOrder', 'Return other items in the order',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'waiveFee', 'Waive Fee',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'waiveFeeUC', 'WAIVE FEE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippingMethod', 'Shipping Method',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippingMethodUC', 'SHIPPING METHOD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancelReturn', 'Cancel Return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancelReturnUC', 'CANCEL RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'confirmReturn', 'Confirm Return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'confirmReturnUC', 'CONFIRM RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'useAnotherPaymentMethod', 'Use another payment method',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnItemsFrom', 'RETURN ITEMS FROM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'emailLabelsTo', 'EMAIL LABELS TO',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pleaseEnterAValidEmailAddress', 'Please enter a valid email',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'numberOfPackages', 'NUMBER OF PACKAGES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'backToReturnDetails', 'BACK TO RETURN DETAILS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'chargeTotal', 'Charge total',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'chargeTotalUC', 'CHARGE TOTAL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'availableAddresses', 'AVAILABLE ADDRESSES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'availableAddressesUC', 'AVAILABLE ADDRESSES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unexpectedItems', 'UNEXPECTED ITEMS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invalidItemThisItemIsNotPartOfTheOriginalReturn', 'Invalid item: This item is not part of the original return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'receiptNotExpectedVerifyReturnAndApprove', 'Receipt not expected. Verify return and approve.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'approveReturnUC', 'APPROVE RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'receivedQty', 'Received Qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'receivedCondition', 'Received condition',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unexpectedItemUC', 'UNEXPECTED ITEM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noVariancesExistForThisReturnUC', 'NO VARIANCES EXIST FOR THIS RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemCondition', 'Item condition',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'identifiedVariance', 'Identified variance',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'actionTaken', 'Action taken',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'actionReasoning', 'Action reasoning',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'aboutToTick', 'About to tick',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cusUC', 'CUS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itmUC', 'ITM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ordUC', 'ORD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'strUC', 'STR',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'retUC', 'RET',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderDiscounts', 'Order Discounts',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addMoreItemToTheOrder', 'Add more items to the order',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentStatus', 'Payment status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderType', 'Order type',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'storeNo', 'Store No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appeasmentsUC', 'APPEASEMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'proceedToSummaryUC', 'PROCEED TO SUMMARY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentUnavailable', 'Payment Authorization is not successful. Do you want to try later ?',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'deleteOrderLineMsg', 'Selected order line cannot be deleted, order should have atleast one line.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'relatedOrder', 'RELATED ORDERS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'relatedReturn', 'RELATED RETURNS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'relatedExchange', 'EXCHANGE ORDERS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noRelatedOrderMessage', 'No returns or exchanges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'original', 'Original',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'changed', 'Changed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'headerDetailsUC', 'HEADER DETAILS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnAmt', 'Return Amt.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'exchangeStatusScreen', 'XO STATUS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnCharge', 'Return charge',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'exchangeAmt', 'Exchange Amt.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnChargeWithTax', 'Return charge with taxes',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'processReturn', 'PROCESS RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customer', 'customer',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'order', 'order',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnLC', 'return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'item', 'item',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noDataToDisplay', 'No data to display',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'viewItems', 'View items',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'offerDetails', 'Offer details',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'hasBeenCreatedSuccessfully', 'HAS BEEN CREATED SUCCESSFULLY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'makeIt', 'MAKE IT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'exp', 'Exp',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemsToReturn', 'ITEMS TO RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemsToShip', 'ITEMS TO SHIP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noPaymentDetailsAvailable', 'No payment details available',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addNewPayment', 'Add new payment',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'priceUC', 'PRICE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'price', 'Price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemsInOrder', 'ITEMS IN ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'keyWordSearch', 'Keyword Search',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalSummaryTitleUC', 'ESTIMATED PAYMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'extimatedTaxes', 'Estimated taxes',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentSattlementNotPresentUC', 'NO SETTLEMENTS OR REFUNDS FOR THIS ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'checkNumber', 'Check number',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amountToBeCharged', 'Amount to be charged',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amountCharged', 'Amount charged',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'createdDate', 'Created',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'storeId', 'Store No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'confirmed', 'Confirmed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentInfoNotAvailableUC', 'PAYMENT INFORMATION NOT AVAILABLE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderedItemTitleUC', 'ORDERED ITEMS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemIdUC', 'SKU',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'etaUC', 'ETA',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderedUC', 'ORDERED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'allocatedUC', 'ALLOCATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderedQty', 'Ordered Qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'plannedQty', 'Planned Qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'releasedUC', 'RELEASED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'backOrderedUC', 'BACK ORDERED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'effectiveQty', 'New effective qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'originalOrderedQty', 'Original ordered qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancelledQty', 'Cancelled qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentSettlementUC', 'PAYMENTS & SETTLEMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipmentsHeaderUC', 'COMPLETED SHIPMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippedQty', 'Shipped Qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippedUC', 'SHIPPED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippedQtyUC', 'SHIPPED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemsInFullfillmentHeaderUC', 'PLANNED SHIPMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'plannedShipmentsHeaderUC', 'IN PROCESS SHIPMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'inProcessQty', 'In process Qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'formattedPrice', 'Unit price',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'formattedLineTotal', 'Line total',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemStyle', 'Style',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemColor', 'Color',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemSize', 'Size',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'dateCreated', 'Date created',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderNumber', 'Order No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'total', 'Total',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'transactionStatus', 'Status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnLink', 'Return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'unholdLink', 'UnHold',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancelLink', 'Cancel',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editRMA', 'Edit RMA',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'resendLabels', 'Resend Labels',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnorExchangeUC', 'RETURNS / EXCHANGES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnNumber', 'Return No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lineRefund', 'Line refund',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipmentNotPresentForPOSUC', 'NO SHIPMENT DETAILS FOR THIS POS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipments', 'shipments',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invoiceNbr', 'Invoice No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invoiceStatus', 'Invoice status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invoiceDate', 'Invoice date',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invoiceAmount', 'Invoice Amt.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invoiceQty', 'Invoice Qty.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'extimatedSnH', 'Estimated S&H',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'otherCharges', 'Other charges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipmentTotalUC', 'SHIPMENT TOTAL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'transactionHeaderUC', 'TRANSACTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'entityTypeOrders', 'Orders',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'entityTypePOS', 'Point of sale',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'allEntityType', 'All',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnEntity', 'Return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'last30Days', 'Last 30 days',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'last60Days', 'Last 60 days',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lastYear', 'Last year',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'showAll', 'Show all',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectStartAndEndDates', 'Select start and end dates',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'dateDefaultText', 'mm/dd/yyyy',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'transactionDate', 'Transaction date',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amountRefunded', 'Amount refunded',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'facility', 'Facility',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'projectedShipment', 'Projected shipment',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'labelETAUC', 'ETA',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'trackingNo', 'Tracking No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'exchangeStatus', 'Exchange Status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnsUC', 'RETURNS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemsInThePOS', 'POS Details',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippedOn', 'Shipped on',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pickedOn', 'Picked on',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippedFromLabel', 'Shipped from',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pickedFrom', 'Picked from',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippingAddressesUC', 'SHIPPING ADDRESSES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentMethodsUC', 'PAYMENT METHODS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'defaultTextUC', 'DEFAULT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentInfoNotExist', 'No payment methods exist',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'estimatedChargeUC', 'ESTIMATED CHARGE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'refund', 'Return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentInfoUC', 'PAYMENT INFO.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentReturnDetailsUC', 'RETURN PAYMENT DETAILS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnDetailsUC', 'RETURN DETAILS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnCenterUC', 'RETURN CENTER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'estimatedCharges', 'Estimated charges',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemdescription', 'item description',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'nameEmailAndPhone', 'name, email, phone',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderNumberForTile', 'order number',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnNumberForTile', 'return number',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noMatchingResultsFound', 'No matching results found.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'clickToReset', 'Click to reset',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'errorsUC', 'ERRORS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'all', '- All -',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'availableInFuture', 'Available in future',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'notAvailable', 'Not available',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectOneItem', 'SELECT ONE ITEM (',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalForSelectOneItem', 'TOTAL)',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addressNotSelected', 'Address not selected',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'storeNotSelected', 'Store not selected',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemSupports', 'ITEM SUPPORTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'notApplicable', 'NOT APPLICABLE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addCreditCardTitle', 'ADD CREDIT CARD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addGiftCardTitle', 'ADD GIFT CARD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addECheckTitle', 'ADD E CHECK',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderLineNotes', 'ORDER LINE NOTES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderNotes', 'ORDER NOTES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerOrderTitle', 'CUSTOMER ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'exchangeOrderTitle', 'EXCHANGE ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemsSelected', 'ITEMS SELECTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'deliveryOptionUC', 'DELIVERY OPTION',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipToAddressUC', 'SHIP TO ADDRESS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pickUpAtStoreUC', 'PICK UP AT STORE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipToStoreUC', 'SHIP TO STORE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shippingTab', 'SHIPPING',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentTab', 'PAYMENT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'confirmTab', 'CONFIRM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'summaryTab', 'SUMMARY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addNewAddress', 'ADD NEW ADDRESS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addressUsedInThisOrder', 'ADDRESSES USED IN THIS ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalDiscount', 'Total discount',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'confirmRemoval', 'CONFIRM REMOVAL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemDetailsAndShipping', 'ITEM DETAILS & SHIPPING',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'placeOrder', 'PLACE ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'confirmChanges', 'CONFIRM CHANGES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderMessage', 'ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderCreatedMsg', 'HAS BEEN CREATED SUCCESSFULLY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderUpdatedMsg', 'HAS BEEN UPDATED SUCCESSFULLY',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'customerInfoWasSaved', 'Customer Information was saved',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'holdOrder', 'Hold Order',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancelOrder', 'Cancel Order',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'availableAndAppliedPromotions', 'AVAILABLE AND APPLIED PROMOTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalPromotions', 'Total promotions',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalAppliedValue', 'Total applied value',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invalidCreditCardNumber', 'Invalid credit card number',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'resendLabelsUC', 'RESEND LABELS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentInformationNotAvailableUC', 'PAYMENT INFORMATION NOT AVAILABLE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'notAValidPhoneNumber', 'Not a valid phone number.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'notAValidZipCode', 'Not a valid zip code.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'separateCouponCodes', 'Separate the coupon codes with comma.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderNotConfirmText', 'Order is not confirmed yet',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderAppeasementsUC', 'ORDER APPEASEMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appeasementsApplied', 'Appeasements applied',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appeasementsValue', 'Appeasements value',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'addAppeasements', 'ADD APPEASEMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'appliedAppeasements', 'APPLIED APPEASEMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalAppeasements', 'Total appeasements',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalAppeasementsValue', 'Total appeasements value',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectAReason', 'Select a reason',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'describeTheReason', 'Describe the reason',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectOne', 'Select one',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'deleteSelectedCoupons', 'ARE YOU SURE YOU WANT TO DELETE THE SELECTED COUPONS?',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectOneOrMoreCoupons', 'SELECT ONE OR MORE COUPONS TO REMOVE FROM THE ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotionsAnd', 'Promotions  &',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noShipmentsArePlanned', 'NO SHIPMENTS ARE PLANNED FOR THIS ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipmentNotPresent', 'NO SHIPMENTS ARE COMPLETED FOR THIS ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'plannedShipmentNotPresent', 'NO SHIPMENTS ARE CURRENTLY IN PROCESS FOR THIS ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderDetailsUC', 'ORDER DETAILS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'orderStatusScreen', 'ORDER STATUS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'transactionList', 'Transaction List',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnList', 'Return List',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipmentHeaderTitle', 'SHIPMENTS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'findOrders', 'FIND ORDERS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'findReturns', 'FIND RETURNS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noReturnsFound', 'No returns found',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'receiptExpected', 'Receipt expected',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'receiptNotExpected', 'Receipt not expected',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'lineCharge', 'Line charge',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'inboundUC', 'INBOUND',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'nbrOfPackages', 'No. of packages',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'receivedOn', 'Received on',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipFromUC', 'SHIP FROM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipReturnToUC', 'SHIP RETURN TO',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'outboundUC', 'OUTBOUND',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'hideDescription', 'Hide Description',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pleaseWait', 'Please wait ...',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'storesWithinUC', 'STORES WITHIN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'modificationNotPossible', 'Modifications are not possible since the payment has already been authorized',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amountToBeRefunded', 'Amount to be refunded',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noQuantityAvailableForReturn', 'No quantity available to return',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnRefNumber', 'Return Ref. No.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invalidRoutingNumber', 'Invalid routing number',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paymentNotPresentForReturn', 'NO PAYMENTS EXIST FOR THIS RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'charge', 'Charge',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'refundSC', 'Refund',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'notyetReceived', 'Not yet received',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalReturnAmt', 'Total return Amt.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalExchangeAmt', 'Total exchange Amt.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'awaitingPayment', 'Awaiting Payment',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ofUC', 'OF',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'READY FOR PICKUP_UC', 'READY FOR PICKUP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PICKED UP_UC', 'PICKED UP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PICKED UP', 'PICKED UP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'READY FOR PICKUP', 'READY FOR PICKUP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Open_UC', 'OPEN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'OPEN', 'OPEN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'invoicedUC', 'INVOICED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'canceledUC', 'CANCELED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'receivedUC', 'RECEIVED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Closed', 'Closed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid', 'Invalid',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created', 'Created',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipper rejected', 'Shipper rejected',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipper countered', 'Shipper countered',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Shipper Acceptance', 'Pending Shipper Acceptance',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Vendor Acceptance', 'Pending Vendor Acceptance',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Recalled', 'Recalled',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Accepted', 'Accepted',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourced', 'Sourced',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Sourced', 'Partially Sourced',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing Failed', 'Sourcing Failed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduled', 'Scheduled',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Scheduled', 'Partially Scheduled',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduling Failed', 'Scheduling Failed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated', 'Allocated',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Allocated', 'Partially Allocated',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Failed', 'Allocation Failed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Released', 'Released',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Released', 'Partially Released',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC Allocated', 'DC Allocated',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially DC Allocated', 'Partially DC Allocated',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In Work', 'In Work',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped', 'Shipped',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Shipped', 'Partially Shipped',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Received', 'Received',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Canceled', 'Canceled',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Created', 'DO Created',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Partially Created', 'DO Partially Created',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Vendor rejected', 'Vendor rejected',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Vendor countered', 'Vendor countered',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Receiving Started', 'Receiving Started',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirmed', 'Confirmed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending', 'Pending',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Exchange Order Confirmed', 'Exchange Order Confirmed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return Invoice Created', 'Return Invoice Created',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Invalid_UC', 'INVALID',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVALID_UC', 'INVALID',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Created_UC', 'CREATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CREATED_UC', 'CREATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipper rejected_UC', 'SHIPPER REJECTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPER REJECTED_UC', 'SHIPPER REJECTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipper countered_UC', 'SHIPPER COUNTERED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPER COUNTERED_UC', 'SHIPPER COUNTERED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Shipper Acceptance_UC', 'PENDING SHIPPER ACCEPTANCE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PENDING SHIPPER ACCEPTANCE_UC', 'PENDING SHIPPER ACCEPTANCE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending Vendor Acceptance_UC', 'PENDING VENDOR ACCEPTANCE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PENDING VENDOR ACCEPTANCE_UC', 'PENDING VENDOR ACCEPTANCE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Recalled_UC', 'RECALLED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RECALLED_UC', 'RECALLED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Accepted_UC', 'ACCEPTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ACCEPTED_UC', 'ACCEPTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourced_UC', 'SOURCED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCED_UC', 'SOURCED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Sourced_UC', 'PARTIALLY SOURCED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SOURCED_UC', 'PARTIALLY SOURCED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Sourcing Failed_UC', 'SOURCING FAILED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SOURCING FAILED_UC', 'SOURCING FAILED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduled_UC', 'SCHEDULED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULED_UC', 'SCHEDULED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Scheduled_UC', 'PARTIALLY SCHEDULED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SCHEDULED_UC', 'PARTIALLY SCHEDULED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Scheduling Failed_UC', 'SCHEDULING FAILED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SCHEDULING FAILED_UC', 'SCHEDULING FAILED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocated_UC', 'ALLOCATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATED_UC', 'ALLOCATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Allocated_UC', 'PARTIALLY ALLOCATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY ALLOCATED_UC', 'PARTIALLY ALLOCATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Failed_UC', 'ALLOCATION FAILED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ALLOCATION FAILED_UC', 'ALLOCATION FAILED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Released_UC', 'RELEASED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RELEASED_UC', 'RELEASED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Released_UC', 'PARTIALLY RELEASED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY RELEASED_UC', 'PARTIALLY RELEASED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC Allocated_UC', 'DC ALLOCATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DC ALLOCATED_UC', 'DC ALLOCATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially DC Allocated_UC', 'PARTIALLY DC ALLOCATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY DC ALLOCATED_UC', 'PARTIALLY DC ALLOCATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'In Work_UC', 'IN WORK',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'IN WORK_UC', 'IN WORK',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'COMPLETED_UC', 'COMPLETED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Completed_UC', 'COMPLETED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Completed_UC', 'PARTIALLY COMPLETED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY COMPLETED_UC', 'PARTIALLY COMPLETED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Shipped_UC', 'SHIPPED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipped', 'shipped',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SHIPPED_UC', 'SHIPPED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipped_UC', 'SHIPPED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Shipped_UC', 'PARTIALLY SHIPPED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'partially shipped_UC', 'PARTIALLY SHIPPED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PARTIALLY SHIPPED_UC', 'PARTIALLY SHIPPED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'partially shipped', 'partially shipped',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Received_UC', 'RECEIVED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RECEIVED_UC', 'RECEIVED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Canceled_UC', 'CANCELED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CANCELED_UC', 'CANCELED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Closed_UC', 'CLOSED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CLOSED_UC', 'CLOSED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Created_UC', 'DO CREATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO CREATED_UC', 'DO CREATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO Partially Created_UC', 'DO PARTIALLY CREATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'DO PARTIALLY CREATED_UC', 'DO PARTIALLY CREATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Vendor rejected_UC', 'VENDOR REJECTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VENDOR REJECTED_UC', 'VENDOR REJECTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Vendor countered_UC', 'VENDOR COUNTERED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'VENDOR COUNTERED_UC', 'VENDOR COUNTERED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Receiving Started_UC', 'RECEIVING STARTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RECEIVING STARTED_UC', 'RECEIVING STARTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Confirmed_UC', 'CONFIRMED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CONFIRMED_UC', 'CONFIRMED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pending_UC', 'PENDING',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PENDING_UC', 'PENDING',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INTRANSIT_UC', 'IN TRANSIT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'InTransit_UC', 'IN TRANSIT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'intransit', 'In transit',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Exchange Order Confirmed_UC', 'EXCHANGE ORDER CONFIRMED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'EXCHANGE ORDER CONFIRMED_UC', 'EXCHANGE ORDER CONFIRMED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Return Invoice Created_UC', 'RETURN INVOICE CREATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RETURN INVOICE CREATED_UC', 'RETURN INVOICE CREATED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hold', 'Hold',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancellation', 'Cancellation',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Hold Resolution', 'Hold Resolution',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift Wrap', 'Gift Wrap',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery Instructions', 'Delivery Instructions',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Delivery Instrns.', 'Delivery Instrns.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift From', 'Gift From',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift To', 'Gift To',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift Message', 'Gift Message',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Collateral', 'Collateral',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Appeasement', 'Appeasement',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'NOT_APPLICABLE', 'Not Applicable',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_PAYINFO', 'Awaiting Payment',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_AUTHORIZATION', 'Awaiting Auth',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AUTHORIZED', 'Authorized',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INVOICED', 'Invoiced',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'PAID', 'Paid',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED_AUTH', 'Authorization Failed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED_CHARGE', 'Settlement Failed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AWAIT_REFUND', 'Awaiting Refund',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'REFUNDED', 'Refunded',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Amex', 'Amex',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Discover', 'Discover',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Master Card', 'Master Card',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Visa', 'Visa',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'JCB', 'JCB',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Diners Club', 'Diners Club',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Completed', 'Completed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Partially Completed', 'Partially Completed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Checking', 'Checking',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Savings', 'Savings',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'BusinessChecking', 'BusinessChecking',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS Unavbl.', 'AVS Unavbl.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Fraud', 'Payment Fraud',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel un-recvd qty', 'Cancel un-recvd qty',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Addn fees', 'No Addn fees',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitItemRcpt', 'AwaitItemRcpt',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitAddrssDtl', 'AwaitAddrssDtl',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitRMAConfirm', 'AwaitRMAConfirm',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Quantity Variance', 'Quantity Variance',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Resolved', 'Resolved',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Test_SV', 'Test_SV',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Item Variance', 'Item Variance',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pay. Service Unavbl.', 'Pay. Service Unavbl.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Misc.', 'Misc.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitPymntDtl', 'AwaitPymntDtl',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'XONeedsUserAction', 'XONeedsUserAction',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Allocation Failed123', 'Allocation Failed123',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Other', 'Other',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Unexpected Item', 'Unexpected Item',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Charge Addn fees', 'Charge Addn fees',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CodeName', 'CodeName',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MACR00685685Hold', 'MACR00685685Hold',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Retest', 'Retest',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CancelOrder', 'CancelOrder',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AwaitAddrssNPymntDtl', 'AwaitAddrssNPymntDtl',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AddressNotAvailable', 'AddressNotAvailable',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax Service Unavbl.', 'Tax Service Unavbl.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Payment Failed', 'Payment Failed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Condition Variance', 'Condition Variance',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Mark un-recvd qty', 'Mark un-recvd qty',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RCAF1', 'RCAF1',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Akanksha01', 'Akanksha01',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'MACR00685685Cancel', 'MACR00685685Cancel',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H Unavbl.', 'S&H Unavbl.',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'S&H Failed', 'S&H Failed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Tax Service Failed', 'Tax Service Failed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'AVS Failure', 'AVS Failure',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Address not Verified', 'Address not Verified',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'RC1', 'RC1',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'SJCancel', 'SJCancel',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Misc', 'Misc',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'No Variance', 'No Variance',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Fraud', 'Fraud',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Inactive test', 'Inactive test',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Receipt Not Expected', 'Receipt Not Expected',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Qty and Cond Vari', 'Qty and Cond Vari',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'HoldOrder', 'HoldOrder',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ItemNeedsUserAction', 'ItemNeedsUserAction',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel Line', 'Cancel Line',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CANCEL LINE', 'CANCEL LINE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cancel RMA', 'Cancel RMA',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'CANCEL RMA', 'CANCEL RMA',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'changeStoreSC', 'Change store',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectedStoreUC', 'SELECTED STORE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amountWithOutCurrency', 'Amount',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'amountWithOutCurrencyUC', 'AMOUNT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'pickedUpUC', 'PICKED UP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'readyForPickupUC', 'READY FOR PICKUP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnOrderNotes', 'RETURN ORDER NOTES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnOrderLineNotes', 'RETURN ORDER LINE NOTES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'usedPaymentOptionsUC', 'USED PAYMENT OPTIONS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'creditCardUC', 'CREDIT CARD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editCreditCradUC', 'EDIT CREDIT CARD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'giftCardUC', 'GIFT CARD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'eCheckUC', 'E CHECK',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'editECheckUC', 'EDIT E CHECK',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'suspendMsg', 'Are you sure you want to suspend',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectOneItemUC', 'SELECT ONE ITEM',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'totalUC', 'TOTAL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'chargeLessThanRefundMessageUC', 'TOTAL AMOUNT CHARGED ON CORRESPONDING CO IS LESS THAN ESTIMATED REFUND',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'promotionsApplied', 'Promotions applied',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship to address_UC', 'SHIP TO ADDRESS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Ship to store_UC', 'SHIP TO STORE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Pick up at store_UC', 'PICK UP AT STORE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Customer pickup_UC', 'PICK UP AT STORE',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'selectUC', 'SELECT',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Cash_UC', 'CASH',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Paypal_UC', 'PAYPAL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Electronic Bank Transfer_UC', 'ELECTRONIC BANK TRANSFER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Paper Check_UC', 'PAPER CHECK',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '3rd Party Billing_UC', '3rd PARTY BILLING',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Credit Card_UC', 'CREDIT CARD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Gift Card_UC', 'GIFT CARD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Others_UC', 'OTHERS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'e-Check_UC', 'E-CHECK',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnStatusLink', 'Return Status',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'returnsAndExchangesUC', 'RETURNS & EXCHANGES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'chargeHeader', 'CHARGES',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'refundHeader', 'REFUNDS',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'createReturnStatusFailureReason', 'Returns cannot be created',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'paypalUC', 'PAYPAL',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'partially picked up_UC', 'PARTIALLY PICKED UP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'picked up_UC', 'PICKED UP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'itemToShipUC', 'ITEMS TO SHIP',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'holdMsgText', 'Order is on hold',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'cancelMsgText', 'Order is canceled',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'removeHoldUC', 'REMOVE HOLD',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'holdOrderUC', 'HOLD ORDER',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '$Off', '$Off',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, '%Off', '%Off',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Flat', 'Flat',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Not Valid', 'Not Valid',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'exchangeOrderNeedsUserAction', 'XO needs user action',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'STORE ACCEPTED_UC', 'STORE ACCEPTED',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'shipmentNotExist', 'NO SHIPMENTS EXIST FOR THIS RETURN',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Applied', 'Applied',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Not Applied', 'Not Applied',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Removed', 'Removed',
           'DS_ORD_NEXTGEN_UI')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Manual', 'Manual',
           'ReceiptNotExpectedApprovalFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'Auto', 'Auto',
           'ReceiptNotExpectedApprovalFV')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'started', 'Started',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'ended', 'Ended',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'status', 'Status',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'workflowName', 'Workflow Name',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'runID', 'Run ID',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'agentName', 'Agent Name',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'noOfObjects', 'No. of Objects',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'failureReason', 'Failure Reason',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'COMPLETED', 'Completed',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'FAILED', 'Failed',
           'WORKFLOWAGENT')
/
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
 values (SEQ_LABEL_ID.NEXTVAL, 'INPROGRESS', 'In Progress',
           'WORKFLOWAGENT');
		   COMMIT;

exit;

SET DEFINE OFF;
SET ECHO ON;

MERGE INTO DB_BUILD_HISTORY D
     USING (SELECT 'DOM_20130503.2013.1.18.03' VERSION_LABEL, 'DOM_20130503.2013.1.18.03' APP_BLD_NO
              FROM DUAL) B
        ON (D.VERSION_LABEL = B.VERSION_LABEL AND D.APP_BLD_NO = B.APP_BLD_NO)
WHEN NOT MATCHED
THEN
INSERT(VERSION_LABEL, START_DTTM, END_DTTM, APP_BLD_NO) 
VALUES('DOM_20130503.2013.1.18.03', systimestamp, systimestamp, 'DOM_20130503.2013.1.18.03');

commit;

-- DBTicket MACR00755619

CREATE TABLE A_PROCESS_LOG 
( "PROCESS_LOG_ID" NUMBER NOT NULL, 
"SERVER_NAME" VARCHAR2(50 CHAR), 
"THREAD_ID" number(10,0)NOT NULL, 
"THREAD_NAME" varchar2(50 CHAR) NOT NULL, 
"PROCESS_ID" number(10,0) NOT NULL, 
"PROCESS_NAME" VARCHAR2(30 CHAR)NOT NULL, 
"PROCESS_TYPE" VARCHAR2(16 CHAR), 
"EVENT_TIMESTAMP" TIMESTAMP (6), 
"EVENT_NAME" VARCHAR2(30 CHAR), 
"REFERENCE_ID" VARCHAR2(16 CHAR), 
"REFERENCE_TYPE" VARCHAR2(20 CHAR), 
"ADDITIONAL_DETAIL" varchar2(500 CHAR),
"OBJECT_ID" varchar2(50 CHAR),
"OBJECT_DETAIL_ID" varchar2(50 CHAR),
"EXCEPTION_DETAIL" varchar2(4000 CHAR), 
"EVENT_DETAIL" varchar2(50 CHAR), 
"PROCESS_STATUS" number(1,0),
"THREAD_STATUS" number(1,0),
"COMPANY_ID" NUMBER(9,0),
"RETRY_CNT" number(1,0),
"PARENT_PROC_ID" number(10,0)
)TABLESPACE DOM_DT_TBS;

COMMENT ON table A_PROCESS_LOG IS 'Table for the tracking process';

COMMENT ON column A_PROCESS_LOG.PROCESS_LOG_ID IS 'The primary key for A_PROCESS_LOG';

COMMENT ON column A_PROCESS_LOG.THREAD_ID IS 'Unique Id for each thread for tracking process';

COMMENT ON column A_PROCESS_LOG.THREAD_NAME IS 'Store java thread name for each thread for tracking process';

COMMENT ON column A_PROCESS_LOG.PROCESS_ID IS 'Unique Id for each process';

COMMENT ON column A_PROCESS_LOG.PROCESS_NAME IS 'Unique process name for each process';

COMMENT ON column A_PROCESS_LOG.PROCESS_TYPE IS 'Indicate whether process is UI or system driven';

COMMENT ON column A_PROCESS_LOG.EVENT_TIMESTAMP IS 'Store system timestamp';

COMMENT ON column A_PROCESS_LOG.EVENT_NAME IS 'Store information of thread (Start,Inprogress,end or failed)';

COMMENT ON column A_PROCESS_LOG.REFERENCE_ID IS 'Store Id based on finite value reference type';

COMMENT ON column A_PROCESS_LOG.REFERENCE_TYPE IS 'Finite value of ReferenceType';

COMMENT ON column A_PROCESS_LOG.ADDITIONAL_DETAIL IS 'Store more information of process';

COMMENT ON column A_PROCESS_LOG.OBJECT_ID IS 'Store more information of process';

COMMENT ON column A_PROCESS_LOG.OBJECT_DETAIL_ID IS 'Store event name based on each process';

COMMENT ON column A_PROCESS_LOG.EXCEPTION_DETAIL IS 'Store Information of Exception for particular thread of process';

COMMENT ON column A_PROCESS_LOG.EVENT_DETAIL IS 'Store more information of process';

COMMENT ON column A_PROCESS_LOG.PROCESS_STATUS IS 'Store information of process (Start,Inprogress,end or failed)';

COMMENT ON column A_PROCESS_LOG.THREAD_STATUS IS 'Store information of thread (Start,Inprogress,end or failed)';

COMMENT ON column A_PROCESS_LOG.COMPANY_ID IS 'Store information of companyId';

COMMENT ON column A_PROCESS_LOG.RETRY_CNT IS 'Store information of retry count for failed process';

COMMENT ON column A_PROCESS_LOG.PARENT_PROC_ID IS 'Store information of parent process';
ALTER TABLE "A_PROCESS_LOG" ADD CONSTRAINT "A_PROCESS_LOG_PK" PRIMARY KEY ("PROCESS_LOG_ID");
ALTER TABLE A_PROCESS_LOG ADD 
CONSTRAINT A_PROCESS_LOG_FK
FOREIGN KEY (COMPANY_ID) 
REFERENCES COMPANY (COMPANY_ID);

CREATE SEQUENCE  "SEQ_PROCESS_LOG_ID"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  CYCLE ;

CREATE SEQUENCE  "A_PROCESS_VIEW_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  CYCLE ;

CREATE SEQUENCE  "A_THREAD_VIEW_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  CYCLE ;
-- DBTicket MACR00757291

alter table A_PO_LINE_DOM_EXT modify PURCHASE_ORDERS_ID NUMBER(10);

-- DBTicket MACR00766128

update A_PAYMENT_DETAIL set REF_FIELD3 = 0;
alter table A_PAYMENT_DETAIL modify REF_FIELD3  default 0 not null;

update A_CUSTOMER_PAYMENT_INFO set REF_FIELD3 = 0;
alter table A_CUSTOMER_PAYMENT_INFO modify REF_FIELD3  default 0 not null;
-- DBTicket MACR00784302
UPDATE wf_event_def_detail
SET event_detail_name = 'CO_REMOVE_DISCOUNT_CODE'
WHERE event_detail_name = ' CO_REMOVE_DISCOUNT_CODE ';

COMMIT;


-- DBTicket MACR00788958

CREATE TABLE A_PROCESS_LOG_CONFIG
(
A_PROCESS_LOG_CONFIG_ID  NUMBER(8,0),
CONFIG_PARAM_NAME        VARCHAR2(50 BYTE),
CONFIG_PARAM_GROUP       VARCHAR2(25 BYTE),
CONFIG_PARAM_VALUE       VARCHAR2(25 BYTE),
CONFIG_PARAM_DISP_STATUS NUMBER(1,0),
IS_BASIC                 NUMBER(1,0),
CONFIG_PARAM_MIN_VALUE   NUMBER(8,0),
CONFIG_PARAM_MAX_VALUE   NUMBER(8,0)
)
TABLESPACE DOM_DT_TBS;


COMMENT ON COLUMN "A_PROCESS_LOG_CONFIG"."A_PROCESS_LOG_CONFIG_ID" IS 'Primary key';
COMMENT ON COLUMN "A_PROCESS_LOG_CONFIG"."CONFIG_PARAM_NAME" IS 'Name of Config Paramater';
COMMENT ON COLUMN "A_PROCESS_LOG_CONFIG"."CONFIG_PARAM_GROUP" IS 'Group to which Config Paramater belongs';
COMMENT ON COLUMN "A_PROCESS_LOG_CONFIG"."CONFIG_PARAM_VALUE" IS 'Value of Config Paramater';
COMMENT ON COLUMN "A_PROCESS_LOG_CONFIG"."CONFIG_PARAM_DISP_STATUS" IS 'Is Config Parameter displayable on UI or not';
COMMENT ON COLUMN "A_PROCESS_LOG_CONFIG"."IS_BASIC" IS 'Is Config Parameter Basic or Advanced';
COMMENT ON COLUMN "A_PROCESS_LOG_CONFIG"."CONFIG_PARAM_MIN_VALUE" IS 'Config Parameter minimum value allowed';
COMMENT ON COLUMN "A_PROCESS_LOG_CONFIG"."CONFIG_PARAM_MAX_VALUE" IS 'Config Parameter maximum value allowed';

ALTER TABLE A_PROCESS_LOG_CONFIG ADD  CONSTRAINT A_PRO_LOG_CONFIG_PK PRIMARY KEY 
(A_PROCESS_LOG_CONFIG_ID)
USING INDEX
TABLESPACE DOM_IDX_TBS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
INITIAL          64K
NEXT             1M
MINEXTENTS       1
MAXEXTENTS       UNLIMITED
PCTINCREASE      0
);

CREATE UNIQUE INDEX A_PROC_CON_IDX
ON A_PROCESS_LOG_CONFIG (CONFIG_PARAM_NAME)
TABLESPACE DOM_IDX_TBS;


CREATE SEQUENCE SEQ_PROCESS_LOG_CONFIG_ID
START WITH 1
MAXVALUE 9999999999999999999999999999
MINVALUE 1
NOCYCLE
NOCACHE
NOORDER;
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'VIEW_HISTORY_TABLE_DATA','DATAFLOW','false',1,0,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'PROCESS_VIEW_HISTORY_SCHEDULER_INTERVAL','DATAFLOW','24',1,0,24,168);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'NO_OF_ROWS_PER_BATCH_FROM_A_PROCESS_LOG','DATAFLOW','1000',1,0,1000,100000);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'NO_OF_DAYS_FOR_CLEARING_PROCESS_VIEW_LOG_TABLE','DATAFLOW','1',1,0,1,7);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'PROCESSVIEW_SCHEDULER_INTERVAL','DATAFLOW','120',1,0,30,300);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'PROCESSVIEW_LOGGING_BATCH_SIZE','DATAFLOW','2000',1,0,1000,5000);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'PROCESS_VIEW_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'INVENTORY_RISE_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'DO_UPDATE_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'ORDER_STATUS_PROPOGATION_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'INVENTORY_DROP_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'WORKFLOW_DIRTY_READ_SCHEDULER_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'WORKFLOW_AGENT_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SCHEDULED_ALLOCATION_TEMPLATE_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SCHEDULED_DO_CREATE_TEMPLATE_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SCHEDULED_DO_RELEASE_TEMPLATE_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'MAX_IN_MEMORY_MAP_SIZE','DATAFLOW','1000000',1,0,1000000,2000000);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'THRESHOLD_IN_MEMORY_MAP','DATAFLOW','500000',1,0,500000,1000000);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'ASYNC_WORKFLOW_SERVICE_LOGGING_ENABLED','DATAFLOW','false',1,1,null,null);
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'WORKFLOW_SERVICE_LOGGING','DATAFLOW','false',1,1,null,null);


-- DBTicket MACR00807778

DELETE FROM FILTER_LAYOUT WHERE OBJECT_TYPE = 'INVENTORY_SYNC';
INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 0, 1, 'INVENTORY_SYNC.facility', 'Facility', '=', 'STRING', NULL, '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=FacilityId&permission_code=VBD', 'COMP', NULL, NULL, NULL, 0, 'inventory.facilityAliasId', NULL, 4, NULL, '#{cbolookupBackingBean.getBUMap},#{cbolookupBackingBean.getOptionConstructMap}', NULL, 'INVENTORY_SYNC');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 1, 1, 'INVENTORY_SYNC.supplyType', 'Supply type', '=', 'STRING', 'com.manh.doms.util.finitevalue.SupplyType', NULL, 'SELECT', NULL, NULL, NULL, 1, 'inventory.supplyType', NULL, 4, NULL, NULL, NULL, 'INVENTORY_SYNC');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 2, 0, 'INVENTORY_SYNC.item', 'Item', '=', 'STRING', NULL, '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=ItemName&permission_code=VBD', 'COMP', NULL, NULL, NULL, 0, 'sku.sku', NULL, 4, NULL, '#{cbolookupBackingBean.getBUMap},#{cbolookupBackingBean.getOptionConstructMap}', NULL, 'INVENTORY_SYNC');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 3, 0, 'INVENTORY_SYNC.productClass', 'Product Class', '=', 'NUMBER', 'com.manh.common.fv.ProductClass', '(null)', 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'sku.productType', NULL, 4, NULL, NULL, NULL, 'INVENTORY_SYNC');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 4, 0, 'INVENTORY_SYNC.segmentId', 'Segment', '=', 'NUMBER', 'com.manh.common.fv.InventorySegment', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'segmentedInventory.segmentId', NULL, 4, NULL, NULL, NULL, 'INVENTORY_SYNC');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 5, 0, 'INVENTORY_SYNC.fromDate', 'Expected delivery', '=', 'DATE', NULL, NULL, 'DATEONLY_COMP', NULL, NULL, NULL, 1, 'inventory.etaDttm', NULL, 4, NULL, NULL, NULL, 'INVENTORY_SYNC');


COMMIT;
-- DBTicket MACR00757867
ALTER TABLE A_SHIPPING_CHARGE_RULE ADD (ENTITY_TYPE NUMBER(2) DEFAULT 1 NOT NULL);

-- DBTicket MACR00808964
alter table 
a_process_log
modify 
( 
COMPANY_ID number(9,0),
THREAD_STATUS number(1,0),
THREAD_NAME varchar2(50 CHAR)
);

-- DBTicket DOM-1364

CREATE TABLE "A_PROCESS_LOG_HISTORY" 
(  "PROCESS_LOG_ID" NUMBER, 
"SERVER_NAME" VARCHAR2(50 CHAR), 
"THREAD_ID"    number(10,0), 
"THREAD_NAME" varchar2(50 CHAR), 
"PROCESS_ID"   number(10,0), 
"PROCESS_NAME" VARCHAR2(30 CHAR), 
"PROCESS_TYPE" VARCHAR2(16 CHAR), 
"EVENT_TIMESTAMP" TIMESTAMP (6), 
"EVENT_NAME" VARCHAR2(30 CHAR), 
"REFERENCE_ID" VARCHAR2(16 CHAR), 
"REFERENCE_TYPE" VARCHAR2(20 CHAR), 
"ADDITIONAL_DETAIL" varchar2(500 CHAR),
"OBJECT_ID" varchar2(50 CHAR),
"OBJECT_DETAIL_ID" varchar2(50 CHAR),
"EXCEPTION_DETAIL" varchar2(4000 CHAR), 
"EVENT_DETAIL" varchar2(50 CHAR), 
"PROCESS_STATUS" number(1,0),
"THREAD_STATUS" number(1,0),
"COMPANY_ID" NUMBER(9,0),
"RETRY_CNT" number(1,0),
"PARENT_PROC_ID" number(10,0)                
)TABLESPACE DOM_DT_TBS;

COMMENT ON table A_PROCESS_LOG_HISTORY IS 'Table for the archiving A_PROCESS_LOG content';
COMMENT ON column A_PROCESS_LOG_HISTORY.PROCESS_LOG_ID IS 'The primary key for A_PROCESS_LOG_HISTORY';
COMMENT ON column A_PROCESS_LOG_HISTORY.THREAD_ID IS 'Unique Id for each thread for tracking process';
COMMENT ON column A_PROCESS_LOG_HISTORY.THREAD_NAME IS 'Store java thread name for each thread for tracking process';
COMMENT ON column A_PROCESS_LOG_HISTORY.PROCESS_ID IS 'Unique Id for each process';
COMMENT ON column A_PROCESS_LOG_HISTORY.PROCESS_NAME IS 'Unique process name for each process';
COMMENT ON column A_PROCESS_LOG_HISTORY.PROCESS_TYPE IS 'Indicate whether process is UI or system driven';
COMMENT ON column A_PROCESS_LOG_HISTORY.EVENT_TIMESTAMP IS 'Store system timestamp';
COMMENT ON column A_PROCESS_LOG_HISTORY.EVENT_NAME IS 'Store information of thread (Start,Inprogress,end or failed)';
COMMENT ON column A_PROCESS_LOG_HISTORY.REFERENCE_ID IS 'Store Id based on finite value reference type';
COMMENT ON column A_PROCESS_LOG_HISTORY.REFERENCE_TYPE IS 'Finite value of ReferenceType';
COMMENT ON column A_PROCESS_LOG_HISTORY.ADDITIONAL_DETAIL IS 'Store more information of process';
COMMENT ON column A_PROCESS_LOG_HISTORY.OBJECT_ID IS 'Store more information of process';
COMMENT ON column A_PROCESS_LOG_HISTORY.OBJECT_DETAIL_ID IS 'Store event name based on each process';
COMMENT ON column A_PROCESS_LOG_HISTORY.EXCEPTION_DETAIL IS 'Store Information of Exception for particular thread of process';
COMMENT ON column A_PROCESS_LOG_HISTORY.PROCESS_STATUS IS 'Store information of process (Start,Inprogress,end or failed)';
COMMENT ON column A_PROCESS_LOG_HISTORY.THREAD_STATUS IS 'Store information of thread (Start,Inprogress,end or failed)';
COMMENT ON column A_PROCESS_LOG_HISTORY.COMPANY_ID IS 'Store information of companyId';
COMMENT ON column A_PROCESS_LOG_HISTORY.RETRY_CNT IS 'Store information of retry count for failed process';
COMMENT ON column A_PROCESS_LOG.PARENT_PROC_ID IS 'Store information of parent process';

-- DBTicket MACR00822337
update A_INVOICE set INVOICE_AMOUNT_PROCESSED = 0.0 where INVOICE_AMOUNT_PROCESSED is null;
commit;
-- DBTicket MACR00824580
update PARAM_DEF set PARAM_VALIDATOR_CLASS = 'com.manh.doms.selling.util.SellingParameterValidator' where PARAM_DEF_ID in ('DEFAULT_SHIP_VIA_FOR_SHIP_TO_STORE','DEFAULT_SHIP_VIA') and PARAM_GROUP_ID  = 'DS';
update PARAM_DEF set PARAM_VALIDATOR_CLASS = 'com.manh.doms.selling.util.DOMParameterValidator' where PARAM_DEF_ID in ('MAX_LINES_ALLOWED_BY_ATP') and PARAM_GROUP_ID = 'DOM';
commit;
-- DBTicket MACR00833472
--it is for both 2013 and 2013.1
update lrf_report_def set report_name='Late Orders', report_file_name='Late Orders' where report_def_id='101';

update lrf_report_def set report_name='Stuck Orders', report_file_name='Stuck Orders' where report_def_id='102';

update lrf_report_def set report_name='Payment Transactions', report_file_name='Payment Transactions' where report_def_id='103';

update lrf_report_def set report_name='Promotion Application', report_file_name='Promotion Application' where report_def_id='104';
commit;
-- DBTicket MACR00833637
create or replace
PROCEDURE             "AIE_UPDASYNC_PROC"
(
pTC_COMPANY_ID IN      NUMBER,
pFACILITY_ID   IN      NUMBER,
pSYNC_MODE     IN      INTEGER
)
AS
cursor cur_inv_avalunal (C_TCCOMPANY_ID NUMBER, C_FACILITYID NUMBER) is
select
INVENTORY_ID,
SYNC_NEW_QUANTITY - SYNC_ORIG_QUANTITY  AVAIL_CHANGED_VALUE ,
sync_new_quantity - sync_orig_quantity - nvl((SYNC_UNALLOC - SYNC_ORIG_UNALLOC),0) AVAIL_MINUS_UNALLOC_CHANGED  ,
UN_ALLOCATABLE_QTY - SYNC_ORIG_UNALLOC UNALLOC_CHANGED_VALUE  ,
sku_id
from inventory where (SYNC_QTY_HAS_CHANGED = 1 OR SYNC_UNALLOCQTY_HAS_CHANGED = 1) and TC_COMPANY_ID = C_TCCOMPANY_ID and FACILITY_ID = C_FACILITYID;
cursor cur_inv_avail (C_TCCOMPANY_ID NUMBER, C_FACILITYID NUMBER) is
select
inventory_id,
sync_new_quantity - sync_orig_quantity AVAIL_CHANGED_VALUE,
sku_id
from inventory where SYNC_UNALLOCQTY_HAS_CHANGED = 1 and TC_COMPANY_ID = C_TCCOMPANY_ID and FACILITY_ID = C_FACILITYID;
cursor cur_inv_unall (C_TCCOMPANY_ID NUMBER, C_FACILITYID NUMBER) is
select
inventory_id,
UN_ALLOCATABLE_QTY - SYNC_ORIG_UNALLOC  UNALLOC_CHANGED_VALUE,
sku_id
from inventory where SYNC_UNALLOCQTY_HAS_CHANGED = 1 and TC_COMPANY_ID = C_TCCOMPANY_ID and FACILITY_ID = C_FACILITYID;
recinvav cur_inv_avail%ROWTYPE;
recinvun cur_inv_unall%ROWTYPE;
recinvavun cur_inv_avalunal%ROWTYPE;
l_paramval varchar2(1000);
l_evnt_type varchar2(2);
l_ign_delta_proc number(1) := 0;

nsqlcode                         NUMBER;
vsqlerrm                         VARCHAR2 (255);
gvprocedurename                  VARCHAR2 (100);

BEGIN
If pSYNC_MODE = 0 then
--Put logic to insert and update
gvprocedurename := 'DOM111';


open cur_inv_avalunal(pTC_COMPANY_ID, pFACILITY_ID);
LOOP
FETCH cur_inv_avalunal INTO recinvavun;
exit when cur_inv_avalunal%NOTFOUND;
Begin
select PARAM_VALUE  into l_paramval from COMPANY_PARAMETER where  PARAM_DEF_ID = 'INVENTORY_SEGMENTATION_STRATEGY' and TC_COMPANY_ID = pTC_COMPANY_ID;
EXCEPTION
WHEN NO_DATA_FOUND THEN
l_paramval := 0;
END;

If (l_paramval = 1 OR l_paramval = 2) then

l_evnt_type := 0;
--new changes
l_ign_delta_proc := 0;


Else
select DECODE(SIGN(NVL(RECINVAVUN.AVAIL_CHANGED_VALUE,0)-NVL(RECINVAVUN.UNALLOC_CHANGED_VALUE, 0)), -1, 0, 1, 1) into L_EVNT_TYPE from DUAL;
--new changes

If nvl(recinvavun.AVAIL_CHANGED_VALUE,0) > 0 then
l_ign_delta_proc := 0;

Elsif nvl(recinvavun.AVAIL_CHANGED_VALUE,0) < 0 then
l_ign_delta_proc := 1;
End If;

End If;
If (l_evnt_type = 0) or (l_evnt_type = 1) --MACR00573962 FIX
Then --MACR00573962 FIX
if (recinvavun.AVAIL_CHANGED_VALUE <>0 or recinvavun.UNALLOC_CHANGED_VALUE <> 0) then
insert into A_INVENTORY_EVENT
(A_IDENTITY,
INVENTORY_ID,
AVAIL_CHANGED_VALUE,
UNALLOC_CHANGED_VALUE,
CREATED_DTTM        ,
EVENT_TYPE        ,
ITEM_ID          ,
TC_COMPANY_ID    ,
IGNORE_DELTA_PROCESS )
values
(SEQ_A_INVENTORY_EVENT.nextval,
recinvavun.inventory_id,
nvl(recinvavun.AVAIL_MINUS_UNALLOC_CHANGED, 0),
nvl(recinvavun.UNALLOC_CHANGED_VALUE, 0),
sysdate,
l_evnt_type,
recinvavun.sku_id,
pTC_COMPANY_ID,
l_ign_delta_proc
);
End If; --MACR00573962 FIX
ENd if;
end loop;
END IF;

gvprocedurename := 'DOM222';



If pSYNC_MODE = 1 then
open cur_inv_avail(pTC_COMPANY_ID, pFACILITY_ID);
LOOP
FETCH cur_inv_avail INTO recinvav;
exit when cur_inv_avail%NOTFOUND;
BEGIN
select PARAM_VALUE into l_paramval from COMPANY_PARAMETER where  PARAM_DEF_ID = 'INVENTORY_SEGMENTATION_STRATEGY' and TC_COMPANY_ID = pTC_COMPANY_ID;
EXCEPTION
WHEN NO_DATA_FOUND THEN
l_paramval := 0;
END;

If (l_paramval = 1 OR l_paramval = 2) then
l_evnt_type := 0;
--new changes
l_ign_delta_proc := 0;

Else
select decode(SIGN(nvl(recinvav.AVAIL_CHANGED_VALUE, 0)), -1, 0, 1, 1) into l_evnt_type from dual;

--new changes

If nvl(recinvavun.AVAIL_CHANGED_VALUE,0) > 0 then
l_ign_delta_proc := 0;

Elsif nvl(recinvavun.AVAIL_CHANGED_VALUE,0) < 0 then
l_ign_delta_proc := 1;
End If;

end if;

If (l_evnt_type = 0) or (l_evnt_type = 1) --MACR00573962 FIX
then --MACR00573962 FIX
if (recinvavun.AVAIL_CHANGED_VALUE <>0) then
insert into A_INVENTORY_EVENT
(A_IDENTITY,
INVENTORY_ID,
AVAIL_CHANGED_VALUE,
UNALLOC_CHANGED_VALUE,
CREATED_DTTM,
EVENT_TYPE,
ITEM_ID,
TC_COMPANY_ID,
IGNORE_DELTA_PROCESS)
values
(SEQ_A_INVENTORY_EVENT.nextval,
recinvav.inventory_id,
nvl(recinvav.AVAIL_CHANGED_VALUE, 0),
0,
sysdate,
l_evnt_type,
recinvav.sku_id,
pTC_COMPANY_ID,
l_ign_delta_proc
);
end if; --MACR00573962 FIX
ENd if; 
end loop;
end if;
gvprocedurename := 'DOM333';


If pSYNC_MODE = 2 then
open cur_inv_unall(pTC_COMPANY_ID, pFACILITY_ID);
LOOP
FETCH cur_inv_unall INTO recinvun;
exit when cur_inv_unall%NOTFOUND;
BEGIN
select PARAM_VALUE  into l_paramval from COMPANY_PARAMETER where  PARAM_DEF_ID = 'INVENTORY_SEGMENTATION_STRATEGY' and TC_COMPANY_ID = pTC_COMPANY_ID;
EXCEPTION
WHEN NO_DATA_FOUND THEN
l_paramval := 0;
END;

If (l_paramval = 1 OR l_paramval = 2) then
l_evnt_type := 0;
--new changes
l_ign_delta_proc := 0;
Else
select decode(SIGN(nvl(recinvun.UNALLOC_CHANGED_VALUE, 0)), -1, 1, 1, 0) into l_evnt_type from dual;
l_ign_delta_proc := 0;
end if;

If (l_evnt_type = 0) or (l_evnt_type = 1) --MACR00573962 FIX
then --MACR00573962 FIX
if (recinvavun.UNALLOC_CHANGED_VALUE <>0) then
insert into A_INVENTORY_EVENT
(A_IDENTITY,
INVENTORY_ID,
AVAIL_CHANGED_VALUE,
UNALLOC_CHANGED_VALUE,
CREATED_DTTM        ,
EVENT_TYPE        ,
ITEM_ID          ,
TC_COMPANY_ID,
IGNORE_DELTA_PROCESS
)
values
(SEQ_A_INVENTORY_EVENT.nextval,
recinvun.inventory_id,
0,
nvl(recinvun.UNALLOC_CHANGED_VALUE, 0),
sysdate,
l_evnt_type,
recinvun.sku_id,
pTC_COMPANY_ID,
l_ign_delta_proc
);
end if; --MACR00573962 FIX
ENd if; 
end loop;
end if;
gvprocedurename := 'DOM444';


COMMIT;
EXCEPTION
WHEN OTHERS THEN
nsqlcode := SQLCODE;
vsqlerrm := SQLERRM;
insert into scvsync_err_log values (gvprocedurename, nsqlcode, vsqlerrm, sysdate);
COMMIT;
RAISE_APPLICATION_ERROR(-20002, 'Stop error');
END;
/


-- DBTicket MACR00833779
--2013 and 2013.1
update A_NAV_WIZARD_PAGE_DEFINITION set page_content_url='/doms/dom/selling/co/customerorder/COAdditionalInformationEditableStep.xhtml' where PAGE_NAME like '%editAdditionalDetailsStep%';
commit;
-- DBTicket MACR00834830
--For 2013 and 2013.1
create or replace
PROCEDURE             "AIE_UPDASYNC_PROC"
(
pTC_COMPANY_ID IN      NUMBER,
pFACILITY_ID   IN      NUMBER,
pSYNC_MODE     IN      INTEGER
)
AS
cursor cur_inv_avalunal (C_TCCOMPANY_ID NUMBER, C_FACILITYID NUMBER) is
select
INVENTORY_ID,
AVAILABLE_QUANTITY - SYNC_ORIG_QUANTITY  AVAIL_CHANGED_VALUE ,
AVAILABLE_QUANTITY - sync_orig_quantity - nvl((UN_ALLOCATABLE_QTY - SYNC_ORIG_UNALLOC),0) AVAIL_MINUS_UNALLOC_CHANGED  ,
UN_ALLOCATABLE_QTY - SYNC_ORIG_UNALLOC UNALLOC_CHANGED_VALUE  ,
sku_id
from inventory where (SYNC_QTY_HAS_CHANGED = 1 OR SYNC_UNALLOCQTY_HAS_CHANGED = 1) and TC_COMPANY_ID = C_TCCOMPANY_ID and FACILITY_ID = C_FACILITYID;
cursor cur_inv_avail (C_TCCOMPANY_ID NUMBER, C_FACILITYID NUMBER) is
select
inventory_id,
AVAILABLE_QUANTITY - sync_orig_quantity AVAIL_CHANGED_VALUE,
sku_id
from inventory where SYNC_UNALLOCQTY_HAS_CHANGED = 1 and TC_COMPANY_ID = C_TCCOMPANY_ID and FACILITY_ID = C_FACILITYID;
cursor cur_inv_unall (C_TCCOMPANY_ID NUMBER, C_FACILITYID NUMBER) is
select
inventory_id,
UN_ALLOCATABLE_QTY - SYNC_ORIG_UNALLOC  UNALLOC_CHANGED_VALUE,
sku_id
from inventory where SYNC_UNALLOCQTY_HAS_CHANGED = 1 and TC_COMPANY_ID = C_TCCOMPANY_ID and FACILITY_ID = C_FACILITYID;
recinvav cur_inv_avail%ROWTYPE;
recinvun cur_inv_unall%ROWTYPE;
recinvavun cur_inv_avalunal%ROWTYPE;
l_paramval varchar2(1000);
l_evnt_type varchar2(2);
l_ign_delta_proc number(1) := 0;

nsqlcode                         NUMBER;
vsqlerrm                         VARCHAR2 (255);
gvprocedurename                  VARCHAR2 (100);

BEGIN
If pSYNC_MODE = 0 then
--Put logic to insert and update
gvprocedurename := 'DOM111';


open cur_inv_avalunal(pTC_COMPANY_ID, pFACILITY_ID);
LOOP
FETCH cur_inv_avalunal INTO recinvavun;
exit when cur_inv_avalunal%NOTFOUND;
Begin
select PARAM_VALUE  into l_paramval from COMPANY_PARAMETER where  PARAM_DEF_ID = 'INVENTORY_SEGMENTATION_STRATEGY' and TC_COMPANY_ID = pTC_COMPANY_ID;
EXCEPTION
WHEN NO_DATA_FOUND THEN
l_paramval := 0;
END;

If (l_paramval = 1 OR l_paramval = 2) then

l_evnt_type := 0;
--new changes
l_ign_delta_proc := 0;


Else
select DECODE(SIGN(NVL(RECINVAVUN.AVAIL_CHANGED_VALUE,0)-NVL(RECINVAVUN.UNALLOC_CHANGED_VALUE, 0)), -1, 0, 1, 1) into L_EVNT_TYPE from DUAL;
--new changes

If nvl(recinvavun.AVAIL_CHANGED_VALUE,0) > 0 then
l_ign_delta_proc := 0;

Elsif nvl(recinvavun.AVAIL_CHANGED_VALUE,0) < 0 then
l_ign_delta_proc := 1;
End If;

End If;
If (l_evnt_type = 0) or (l_evnt_type = 1) --MACR00573962 FIX
Then --MACR00573962 FIX
if (recinvavun.AVAIL_CHANGED_VALUE <>0 or recinvavun.UNALLOC_CHANGED_VALUE <> 0) then
insert into A_INVENTORY_EVENT
(A_IDENTITY,
INVENTORY_ID,
AVAIL_CHANGED_VALUE,
UNALLOC_CHANGED_VALUE,
CREATED_DTTM        ,
EVENT_TYPE        ,
ITEM_ID          ,
TC_COMPANY_ID    ,
IGNORE_DELTA_PROCESS )
values
(SEQ_A_INVENTORY_EVENT.nextval,
recinvavun.inventory_id,
nvl(recinvavun.AVAIL_MINUS_UNALLOC_CHANGED, 0),
nvl(recinvavun.UNALLOC_CHANGED_VALUE, 0),
sysdate,
l_evnt_type,
recinvavun.sku_id,
pTC_COMPANY_ID,
l_ign_delta_proc
);
End If; --MACR00573962 FIX
ENd if;
end loop;
END IF;

gvprocedurename := 'DOM222';



If pSYNC_MODE = 1 then
open cur_inv_avail(pTC_COMPANY_ID, pFACILITY_ID);
LOOP
FETCH cur_inv_avail INTO recinvav;
exit when cur_inv_avail%NOTFOUND;
BEGIN
select PARAM_VALUE into l_paramval from COMPANY_PARAMETER where  PARAM_DEF_ID = 'INVENTORY_SEGMENTATION_STRATEGY' and TC_COMPANY_ID = pTC_COMPANY_ID;
EXCEPTION
WHEN NO_DATA_FOUND THEN
l_paramval := 0;
END;

If (l_paramval = 1 OR l_paramval = 2) then
l_evnt_type := 0;
--new changes
l_ign_delta_proc := 0;

Else
select decode(SIGN(nvl(recinvav.AVAIL_CHANGED_VALUE, 0)), -1, 0, 1, 1) into l_evnt_type from dual;

--new changes

If nvl(recinvavun.AVAIL_CHANGED_VALUE,0) > 0 then
l_ign_delta_proc := 0;

Elsif nvl(recinvavun.AVAIL_CHANGED_VALUE,0) < 0 then
l_ign_delta_proc := 1;
End If;

end if;

If (l_evnt_type = 0) or (l_evnt_type = 1) --MACR00573962 FIX
then --MACR00573962 FIX
if (recinvavun.AVAIL_CHANGED_VALUE <>0) then
insert into A_INVENTORY_EVENT
(A_IDENTITY,
INVENTORY_ID,
AVAIL_CHANGED_VALUE,
UNALLOC_CHANGED_VALUE,
CREATED_DTTM,
EVENT_TYPE,
ITEM_ID,
TC_COMPANY_ID,
IGNORE_DELTA_PROCESS)
values
(SEQ_A_INVENTORY_EVENT.nextval,
recinvav.inventory_id,
nvl(recinvav.AVAIL_CHANGED_VALUE, 0),
0,
sysdate,
l_evnt_type,
recinvav.sku_id,
pTC_COMPANY_ID,
l_ign_delta_proc
);
end if; --MACR00573962 FIX
ENd if; 
end loop;
end if;
gvprocedurename := 'DOM333';


If pSYNC_MODE = 2 then
open cur_inv_unall(pTC_COMPANY_ID, pFACILITY_ID);
LOOP
FETCH cur_inv_unall INTO recinvun;
exit when cur_inv_unall%NOTFOUND;
BEGIN
select PARAM_VALUE  into l_paramval from COMPANY_PARAMETER where  PARAM_DEF_ID = 'INVENTORY_SEGMENTATION_STRATEGY' and TC_COMPANY_ID = pTC_COMPANY_ID;
EXCEPTION
WHEN NO_DATA_FOUND THEN
l_paramval := 0;
END;

If (l_paramval = 1 OR l_paramval = 2) then
l_evnt_type := 0;
--new changes
l_ign_delta_proc := 0;
Else
select decode(SIGN(nvl(recinvun.UNALLOC_CHANGED_VALUE, 0)), -1, 1, 1, 0) into l_evnt_type from dual;
l_ign_delta_proc := 0;
end if;

If (l_evnt_type = 0) or (l_evnt_type = 1) --MACR00573962 FIX
then --MACR00573962 FIX
if (recinvavun.UNALLOC_CHANGED_VALUE <>0) then
insert into A_INVENTORY_EVENT
(A_IDENTITY,
INVENTORY_ID,
AVAIL_CHANGED_VALUE,
UNALLOC_CHANGED_VALUE,
CREATED_DTTM        ,
EVENT_TYPE        ,
ITEM_ID          ,
TC_COMPANY_ID,
IGNORE_DELTA_PROCESS
)
values
(SEQ_A_INVENTORY_EVENT.nextval,
recinvun.inventory_id,
0,
nvl(recinvun.UNALLOC_CHANGED_VALUE, 0),
sysdate,
l_evnt_type,
recinvun.sku_id,
pTC_COMPANY_ID,
l_ign_delta_proc
);
end if; --MACR00573962 FIX
ENd if; 
end loop;
end if;
gvprocedurename := 'DOM444';


COMMIT;
EXCEPTION
WHEN OTHERS THEN
nsqlcode := SQLCODE;
vsqlerrm := SQLERRM;
insert into scvsync_err_log values (gvprocedurename, nsqlcode, vsqlerrm, sysdate);
COMMIT;
RAISE_APPLICATION_ERROR(-20002, 'Stop error');
END;
/
-- DBTicket MACR00835622
--2013 and 2013.1
DECLARE   v_index_count number;
BEGIN 
SELECT count(*)
INTO v_index_count
FROM user_ind_columns
WHERE table_name = 'PURCHASE_ORDERS_LINE_ITEM' and column_name='GROUP_ID';
IF v_index_count=0
THEN
EXECUTE IMMEDIATE 'CREATE INDEX POLN_GROUP_ID_IDX ON PURCHASE_ORDERS_LINE_ITEM (GROUP_ID) COMPUTE STATISTICS TABLESPACE CBO_TXPOLN_IDX_TBS';
END IF;
END;
/
exec sequpdt ('WF_VARIABLE', 'WF_VAR_ID' , 'SEQ_WF_VAR_ID');


-- DBTicket MACR00841811
--IT IS FOR 2013, 2013.1

--insert statement to a_co_audit_field_name

MERGE INTO A_CO_AUDIT_FIELD_NAME A USING
(SELECT
'Transaction: Process Count' as AUDIT_FIELD_NAME,
'132*153' as AUDIT_FIELD_VALUE
FROM DUAL) B
ON (A.AUDIT_FIELD_NAME = B.AUDIT_FIELD_NAME AND A.AUDIT_FIELD_VALUE=B.AUDIT_FIELD_VALUE )
WHEN NOT MATCHED THEN 
INSERT (
AUDIT_FIELD_NAME, AUDIT_FIELD_VALUE)
VALUES (
B.AUDIT_FIELD_NAME, B.AUDIT_FIELD_VALUE);

COMMIT;
--PYMT_TRAN_AU_DOM - trigger changes

create or replace
TRIGGER PYMT_TRAN_AU_DOM
AFTER UPDATE
ON A_PAYMENT_TRANSACTION
REFERENCING OLD AS o NEW AS n
FOR EACH ROW
DECLARE
string1   VARCHAR2 (50) DEFAULT '132: ';
string2   VARCHAR2 (50) DEFAULT NULL;
string3   VARCHAR2 (50) DEFAULT NULL;
string4   VARCHAR2 (50) DEFAULT NULL;
string5   VARCHAR2 (50) DEFAULT NULL;
TIME_NOW DATE DEFAULT sysdate;


BEGIN

string2 :=
CASE :o.PAYMENT_TRANS_TYPE
WHEN   1 THEN '133 : ' --Authorization
WHEN   2 THEN '134 : ' --Settlement
WHEN   3 THEN '135 : ' --Refund
WHEN   4 THEN '136 : ' --Balancecheck
END;

select ENTITY_ID INTO  string3 from A_PAYMENT_DETAIL where PAYMENT_DETAIL_ID = :n.PAYMENT_DETAIL_ID;

IF (:o.PROCESS_COUNT <> :n.PROCESS_COUNT)
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 153'),
TO_CHAR(:o.PROCESS_COUNT,'00'),
TO_CHAR(:n.PROCESS_COUNT,'00'),
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (:o.PROCESSED_AMOUNT <> :n.PROCESSED_AMOUNT)
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 137'),
TO_CHAR(:o.PROCESSED_AMOUNT,'999999999999999.00'),
TO_CHAR(:n.PROCESSED_AMOUNT,'999999999999999.00'),
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (:o.REQUESTED_AMOUNT <> :n.REQUESTED_AMOUNT)
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 138'),
TO_CHAR(:o.REQUESTED_AMOUNT,'999999999999999.00'),
TO_CHAR(:n.REQUESTED_AMOUNT,'999999999999999.00'),
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.MERCH_ID, ' ') <> NVL(:n.MERCH_ID, ' '))
THEN

INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 139'),
:o.MERCH_ID,
:n.MERCH_ID,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.MERCH_PASSWORD, ' ') <> NVL(:n.MERCH_PASSWORD, ' '))
THEN

INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 140'),
:o.MERCH_PASSWORD,
:n.MERCH_PASSWORD,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.REQUEST_ID, ' ') <> NVL(:n.REQUEST_ID, ' '))
THEN

INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 141'),
:o.REQUEST_ID,
:n.REQUEST_ID,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.REQUEST_TOKEN, ' ') <> NVL(:n.REQUEST_TOKEN, ' '))
THEN

INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 142'),
:o.REQUEST_TOKEN,
:n.REQUEST_TOKEN,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.FOLLOW_ON_ID, ' ') <> NVL(:n.FOLLOW_ON_ID, ' '))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 143'),
:o.FOLLOW_ON_ID,
:n.FOLLOW_ON_ID,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.FOLLOW_ON_TOKEN, ' ') <> NVL(:n.FOLLOW_ON_TOKEN, ' '))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 144'),
:o.FOLLOW_ON_TOKEN,
:n.FOLLOW_ON_TOKEN,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.REQUESTED_DTTM, TIME_NOW) <> NVL(:n.REQUESTED_DTTM, TIME_NOW))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 145'),
TO_CHAR (:o.REQUESTED_DTTM, 'MM/DD/YYYY HH24:MI:SS'),
TO_CHAR (:n.REQUESTED_DTTM, 'MM/DD/YYYY HH24:MI:SS'),
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.TRANSACTION_DTTM, TIME_NOW) <> NVL(:n.TRANSACTION_DTTM, TIME_NOW))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 146'),
TO_CHAR (:o.TRANSACTION_DTTM, 'MM/DD/YYYY HH24:MI:SS'),
TO_CHAR (:n.TRANSACTION_DTTM, 'MM/DD/YYYY HH24:MI:SS'),
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.TRANSACTION_EXP_DATE, TIME_NOW) <> NVL(:n.TRANSACTION_EXP_DATE, TIME_NOW))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 147'),
TO_CHAR (:o.TRANSACTION_EXP_DATE, 'MM/DD/YYYY HH24:MI:SS'),
TO_CHAR (:n.TRANSACTION_EXP_DATE, 'MM/DD/YYYY HH24:MI:SS'),
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.RECONCILLATION_ID, ' ') <> NVL(:n.RECONCILLATION_ID, ' '))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 148'),
:o.RECONCILLATION_ID,
:n.RECONCILLATION_ID,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.EXTERNAL_RESPONSE_CODE, ' ') <> NVL(:n.EXTERNAL_RESPONSE_CODE, ' '))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 149'),
:o.EXTERNAL_RESPONSE_CODE,
:n.EXTERNAL_RESPONSE_CODE,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.EXTERNAL_RESPONSE_MSG, ' ') <> NVL(:n.EXTERNAL_RESPONSE_MSG, ' '))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 150'),
:o.EXTERNAL_RESPONSE_MSG,
:n.EXTERNAL_RESPONSE_MSG,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.TRANS_RESP_DECISION, 0) <> NVL(:n.TRANS_RESP_DECISION, 0))
THEN

string4 :=
CASE :o.TRANS_RESP_DECISION
WHEN   1 THEN 'Success'
WHEN   2 THEN 'Failure'
WHEN   3 THEN 'Unavailable'
WHEN   4 THEN 'Fraud'
ELSE ' '
END;

string5 :=
CASE :n.TRANS_RESP_DECISION
WHEN   1 THEN 'Success'
WHEN   2 THEN 'Failure'
WHEN   3 THEN 'Unavailable'
WHEN   4 THEN 'Fraud'
ELSE ' '
END;


INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 151'),
string4,
string5,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF (NVL(:o.TRANS_RESP_DECISION_DESC, ' ') <> NVL(:n.TRANS_RESP_DECISION_DESC, ' '))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 152'),
:o.TRANS_RESP_DECISION_DESC,
:n.TRANS_RESP_DECISION_DESC,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

IF ((:o.IS_DELETED <> :n.IS_DELETED) AND
(:n.IS_DELETED = 1))
THEN
INSERT INTO PURCHASE_ORDERS_EVENT
(purchase_orders_event_id,
purchase_orders_id, field_name,
old_value,
new_value,
created_source_type, created_source,
created_dttm
)
VALUES (purchase_orders_event_id_seq.NEXTVAL,
TO_NUMBER(string3),(string1 || string2 || ' 69'),
NULL,
NULL,
:n.last_updated_source_type, :n.last_updated_source,
:n.last_updated_dttm
);
END IF;

END;
/

-- DBTicket DOM-847
exec sequpdt ('WF_SERVICE','SERVICE_ID','SEQ_WF_SERVICE');
insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS)
values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.SELLING_INBOUND_QUEUE','Confirm Customer Order','com.manh.doms.selling.services.bean.sellingservices.impl.SellingServiceBean',
'confirmCustomerOrder','getInstance','CustomerOrder','CustomerOrder',null,null);

insert into WF_EVENT_DEF_DETAIL (WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID)
values((select max(WF_EVENT_DEF_DETAIL_ID)+1 from WF_EVENT_DEF_DETAIL ),0,'CONFIRM_ORDER_REQUEST', 79, null,null,30);

insert into WF_EVENT_DEF_DETAIL (WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID)
values((select max(WF_EVENT_DEF_DETAIL_ID)+1 from WF_EVENT_DEF_DETAIL ),0,'RETRY_AUTHORIZATION', 80, null,null,30);
COMMIT;
-- DBTicket MACR00842495
--2011,2012,2013,2013.1
create or replace
PROCEDURE             "AIE_UPDASYNC_PROC"
(
pTC_COMPANY_ID IN      NUMBER,
pFACILITY_ID   IN      NUMBER,
pSYNC_MODE     IN      INTEGER
)
AS
cursor cur_inv_avalunal (C_TCCOMPANY_ID NUMBER, C_FACILITYID NUMBER) is
select
INVENTORY_ID,
SYNC_NEW_QUANTITY - SYNC_ORIG_QUANTITY  AVAIL_CHANGED_VALUE ,
SYNC_NEW_QUANTITY - sync_orig_quantity - nvl((UN_ALLOCATABLE_QTY - SYNC_ORIG_UNALLOC),0) AVAIL_MINUS_UNALLOC_CHANGED  ,
UN_ALLOCATABLE_QTY - SYNC_ORIG_UNALLOC UNALLOC_CHANGED_VALUE  ,
sku_id
from inventory where (SYNC_QTY_HAS_CHANGED = 1 OR SYNC_UNALLOCQTY_HAS_CHANGED = 1) and TC_COMPANY_ID = C_TCCOMPANY_ID and FACILITY_ID = C_FACILITYID;
cursor cur_inv_avail (C_TCCOMPANY_ID NUMBER, C_FACILITYID NUMBER) is
select
inventory_id,
SYNC_NEW_QUANTITY - sync_orig_quantity AVAIL_CHANGED_VALUE,
sku_id
from inventory where SYNC_UNALLOCQTY_HAS_CHANGED = 1 and TC_COMPANY_ID = C_TCCOMPANY_ID and FACILITY_ID = C_FACILITYID;
cursor cur_inv_unall (C_TCCOMPANY_ID NUMBER, C_FACILITYID NUMBER) is
select
inventory_id,
UN_ALLOCATABLE_QTY - SYNC_ORIG_UNALLOC  UNALLOC_CHANGED_VALUE,
sku_id
from inventory where SYNC_UNALLOCQTY_HAS_CHANGED = 1 and TC_COMPANY_ID = C_TCCOMPANY_ID and FACILITY_ID = C_FACILITYID;
recinvav cur_inv_avail%ROWTYPE;
recinvun cur_inv_unall%ROWTYPE;
recinvavun cur_inv_avalunal%ROWTYPE;
l_paramval varchar2(1000);
l_evnt_type varchar2(2);
l_ign_delta_proc number(1) := 0;

nsqlcode                         NUMBER;
vsqlerrm                         VARCHAR2 (255);
gvprocedurename                  VARCHAR2 (100);

BEGIN
If pSYNC_MODE = 0 then
--Put logic to insert and update
gvprocedurename := 'DOM111';


open cur_inv_avalunal(pTC_COMPANY_ID, pFACILITY_ID);
LOOP
FETCH cur_inv_avalunal INTO recinvavun;
exit when cur_inv_avalunal%NOTFOUND;
Begin
select PARAM_VALUE  into l_paramval from COMPANY_PARAMETER where  PARAM_DEF_ID = 'INVENTORY_SEGMENTATION_STRATEGY' and TC_COMPANY_ID = pTC_COMPANY_ID;
EXCEPTION
WHEN NO_DATA_FOUND THEN
l_paramval := 0;
END;

If (l_paramval = 1 OR l_paramval = 2) then

l_evnt_type := 0;
--new changes
l_ign_delta_proc := 0;


Else
select DECODE(SIGN(NVL(RECINVAVUN.AVAIL_CHANGED_VALUE,0)-NVL(RECINVAVUN.UNALLOC_CHANGED_VALUE, 0)), -1, 0, 1, 1) into L_EVNT_TYPE from DUAL;
--new changes

If nvl(recinvavun.AVAIL_CHANGED_VALUE,0) > 0 then
l_ign_delta_proc := 0;

Elsif nvl(recinvavun.AVAIL_CHANGED_VALUE,0) < 0 then
l_ign_delta_proc := 1;
End If;

End If;
If (l_evnt_type = 0) or (l_evnt_type = 1) --MACR00573962 FIX
Then --MACR00573962 FIX
if (recinvavun.AVAIL_CHANGED_VALUE <>0 or recinvavun.UNALLOC_CHANGED_VALUE <> 0) then
insert into A_INVENTORY_EVENT
(A_IDENTITY,
INVENTORY_ID,
AVAIL_CHANGED_VALUE,
UNALLOC_CHANGED_VALUE,
CREATED_DTTM        ,
EVENT_TYPE        ,
ITEM_ID          ,
TC_COMPANY_ID    ,
IGNORE_DELTA_PROCESS )
values
(SEQ_A_INVENTORY_EVENT.nextval,
recinvavun.inventory_id,
nvl(recinvavun.AVAIL_MINUS_UNALLOC_CHANGED, 0),
nvl(recinvavun.UNALLOC_CHANGED_VALUE, 0),
sysdate,
l_evnt_type,
recinvavun.sku_id,
pTC_COMPANY_ID,
l_ign_delta_proc
);
End If; --MACR00573962 FIX
ENd if;
end loop;
END IF;

gvprocedurename := 'DOM222';



If pSYNC_MODE = 1 then
open cur_inv_avail(pTC_COMPANY_ID, pFACILITY_ID);
LOOP
FETCH cur_inv_avail INTO recinvav;
exit when cur_inv_avail%NOTFOUND;
BEGIN
select PARAM_VALUE into l_paramval from COMPANY_PARAMETER where  PARAM_DEF_ID = 'INVENTORY_SEGMENTATION_STRATEGY' and TC_COMPANY_ID = pTC_COMPANY_ID;
EXCEPTION
WHEN NO_DATA_FOUND THEN
l_paramval := 0;
END;

If (l_paramval = 1 OR l_paramval = 2) then
l_evnt_type := 0;
--new changes
l_ign_delta_proc := 0;

Else
select decode(SIGN(nvl(recinvav.AVAIL_CHANGED_VALUE, 0)), -1, 0, 1, 1) into l_evnt_type from dual;

--new changes

If nvl(recinvavun.AVAIL_CHANGED_VALUE,0) > 0 then
l_ign_delta_proc := 0;

Elsif nvl(recinvavun.AVAIL_CHANGED_VALUE,0) < 0 then
l_ign_delta_proc := 1;
End If;

end if;

If (l_evnt_type = 0) or (l_evnt_type = 1) --MACR00573962 FIX
then --MACR00573962 FIX
if (recinvavun.AVAIL_CHANGED_VALUE <>0) then
insert into A_INVENTORY_EVENT
(A_IDENTITY,
INVENTORY_ID,
AVAIL_CHANGED_VALUE,
UNALLOC_CHANGED_VALUE,
CREATED_DTTM,
EVENT_TYPE,
ITEM_ID,
TC_COMPANY_ID,
IGNORE_DELTA_PROCESS)
values
(SEQ_A_INVENTORY_EVENT.nextval,
recinvav.inventory_id,
nvl(recinvav.AVAIL_CHANGED_VALUE, 0),
0,
sysdate,
l_evnt_type,
recinvav.sku_id,
pTC_COMPANY_ID,
l_ign_delta_proc
);
end if; --MACR00573962 FIX
ENd if; 
end loop;
end if;
gvprocedurename := 'DOM333';


If pSYNC_MODE = 2 then
open cur_inv_unall(pTC_COMPANY_ID, pFACILITY_ID);
LOOP
FETCH cur_inv_unall INTO recinvun;
exit when cur_inv_unall%NOTFOUND;
BEGIN
select PARAM_VALUE  into l_paramval from COMPANY_PARAMETER where  PARAM_DEF_ID = 'INVENTORY_SEGMENTATION_STRATEGY' and TC_COMPANY_ID = pTC_COMPANY_ID;
EXCEPTION
WHEN NO_DATA_FOUND THEN
l_paramval := 0;
END;

If (l_paramval = 1 OR l_paramval = 2) then
l_evnt_type := 0;
--new changes
l_ign_delta_proc := 0;
Else
select decode(SIGN(nvl(recinvun.UNALLOC_CHANGED_VALUE, 0)), -1, 1, 1, 0) into l_evnt_type from dual;
l_ign_delta_proc := 0;
end if;

If (l_evnt_type = 0) or (l_evnt_type = 1) --MACR00573962 FIX
then --MACR00573962 FIX
if (recinvavun.UNALLOC_CHANGED_VALUE <>0) then
insert into A_INVENTORY_EVENT
(A_IDENTITY,
INVENTORY_ID,
AVAIL_CHANGED_VALUE,
UNALLOC_CHANGED_VALUE,
CREATED_DTTM        ,
EVENT_TYPE        ,
ITEM_ID          ,
TC_COMPANY_ID,
IGNORE_DELTA_PROCESS
)
values
(SEQ_A_INVENTORY_EVENT.nextval,
recinvun.inventory_id,
0,
nvl(recinvun.UNALLOC_CHANGED_VALUE, 0),
sysdate,
l_evnt_type,
recinvun.sku_id,
pTC_COMPANY_ID,
l_ign_delta_proc
);
end if; --MACR00573962 FIX
ENd if; 
end loop;
end if;
gvprocedurename := 'DOM444';


COMMIT;
EXCEPTION
WHEN OTHERS THEN
nsqlcode := SQLCODE;
vsqlerrm := SQLERRM;
insert into scvsync_err_log values (gvprocedurename, nsqlcode, vsqlerrm, sysdate);
COMMIT;
RAISE_APPLICATION_ERROR(-20002, 'Stop error');
END;
/
-- DBTicket DOM-1363

CREATE TABLE "A_PROCESS_LOG_ERROR" 
(	
"PROCESS_LOG_ERROR_ID" NUMBER not null, 
"PROCESS_ID" NUMBER(10,0) not null, 
"THREAD_ID" NUMBER(10,0), 
"FACTORY_HEALER_CLASS" VARCHAR2(500 CHAR),
"FACTORY_HEALER_METHOD" VARCHAR2(500 CHAR),
"IS_LOCKED" NUMBER(1,0), 
"IS_EXECUTED" NUMBER(1,0), 
"IS_SUCCEEDED" NUMBER(1,0), 
"EVENT_DETAIL" VARCHAR2(250 CHAR), 
"ADDITIONAL_DETAIL" VARCHAR2(1000 CHAR), 
"REFERENCE_ID" VARCHAR2(16 CHAR), 
"REFERENCE_TYPE" VARCHAR2(20 CHAR), 
"EXCEPTION_TYPE" VARCHAR2(500 CHAR), 
"RETRY_CNT" NUMBER(1,0), 
"COMPANY_ID" NUMBER(9,0)
)TABLESPACE DOM_DT_TBS;

COMMENT ON table A_PROCESS_LOG_ERROR IS 'Table for the auto recovery failed process';

COMMENT ON column A_PROCESS_LOG_ERROR.PROCESS_LOG_ERROR_ID IS 'The primary key for A_PROCESS_LOG_ERROR';

COMMENT ON column A_PROCESS_LOG_ERROR.THREAD_ID IS 'Populate threadId from A_PROCESS_LOG,if logging is enabled';

COMMENT ON column A_PROCESS_LOG_ERROR.FACTORY_HEALER_CLASS IS 'class name help to provide healer instance';

COMMENT ON column A_PROCESS_LOG_ERROR.FACTORY_HEALER_METHOD IS 'method name help to provide healer instance';

COMMENT ON column A_PROCESS_LOG_ERROR.IS_LOCKED IS 'For concurrency handling';

COMMENT ON column A_PROCESS_LOG_ERROR.IS_EXECUTED IS '1:Retry done 0:Still Retry pending';

COMMENT ON column A_PROCESS_LOG_ERROR.IS_SUCCEEDED IS '1:Retry Succeeded';

COMMENT ON column A_PROCESS_LOG_ERROR.EVENT_DETAIL IS 'Store more information of process';

COMMENT ON column A_PROCESS_LOG_ERROR.ADDITIONAL_DETAIL IS 'Store more information of process';

COMMENT ON column A_PROCESS_LOG_ERROR.REFERENCE_ID IS 'Store Id based on finite value reference type';

COMMENT ON column A_PROCESS_LOG_ERROR.REFERENCE_TYPE IS 'Finite value of ReferenceType';

COMMENT ON column A_PROCESS_LOG_ERROR.RETRY_CNT IS 'Number of Retry Count';

COMMENT ON column A_PROCESS_LOG_ERROR.COMPANY_ID IS 'Store companyId';


ALTER TABLE "A_PROCESS_LOG_ERROR" ADD CONSTRAINT "A_PROCESS_LOG_ERROR_PK" PRIMARY KEY ("PROCESS_LOG_ERROR_ID");

-- DBTicket DOM-1360

delete from A_PROCESS_LOG_CONFIG;
commit;
exec sequpdt ('A_PROCESS_LOG_CONFIG','A_PROCESS_LOG_CONFIG_ID','SEQ_PROCESS_LOG_CONFIG_ID');
Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,
CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'VIEW_HISTORY_TABLE_DATA','DATAFLOW-ADV','false',1,0,null,null);
--Show data from history table

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'PROCESS_VIEW_HISTORY_SCHEDULER_INTERVAL','DATAFLOW-ADV','24',1,0,24,168);
--Frequency (in hours) to move data to history table (from active table) 

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'NO_OF_ROWS_PER_BATCH_FROM_A_PROCESS_LOG','DATAFLOW-ADV','1000',1,0,1000,100000);
--Batch size for persisting data to active table (from in-memory)

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'NO_OF_DAYS_FOR_CLEARING_PROCESS_VIEW_LOG_TABLE','DATAFLOW-ADV','1',1,0,1,7);
--Duration (in days) to keep data in active table

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'PROCESSVIEW_SCHEDULER_INTERVAL','DATAFLOW-ADV','120',1,0,30,300);
--Frequency (in seconds) to push in-memory data to active table 

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'PROCESSVIEW_LOGGING_BATCH_SIZE','DATAFLOW-ADV','2000',1,0,1000,5000);
--Batch size for persisting data from active to history table

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'PROCESS_VIEW_LOGGING_ENABLED','DATAFLOW-ADV','true',1,0,null,null);
--Data flow logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'INVENTORY_RISE_LOGGING_ENABLED','DATAFLOW-INV','true',1,1,null,null);
--Inventory rise event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'DO_UPDATE_LOGGING_ENABLED','DATAFLOW-DO','true',1,1,null,null);
--DO update logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'ORDER_STATUS_PROPOGATION_LOGGING_ENABLED','DATAFLOW-ORDER','true',1,1,null,null);
--Order header status propagation logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'INVENTORY_DROP_LOGGING_ENABLED','DATAFLOW-INV','true',1,1,null,null);
-- Inventory drop event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'WORKFLOW_DIRTY_READ_SCHEDULER_LOGGING_ENABLED','DATAFLOW-WF','false',1,1,null,null);
--Workflow Dirty Read scheduler logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'WORKFLOW_AGENT_LOGGING_ENABLED','DATAFLOW-WF','false',1,1,null,null);
-- Workflow agent logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SCHEDULED_ALLOCATION_TEMPLATE_LOGGING_ENABLED','DATAFLOW-SCH','true',1,1,null,null);
-- Allocation template scheduler logging enabled? //change

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SCHEDULED_DO_CREATE_TEMPLATE_LOGGING_ENABLED','DATAFLOW-SCH','false',1,1,null,null);
--DO create template scheduler logging enabled? //change

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SCHEDULED_DO_RELEASE_TEMPLATE_LOGGING_ENABLED','DATAFLOW-SCH','false',1,1,null,null);
--DO release template scheduler logging enabled? //change

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'MAX_IN_MEMORY_MAP_SIZE','DATAFLOW-ADV','1000000',1,0,1000000,2000000);
-- Automatically shut down data flow persistence when in-memory object count reaches to 

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'THRESHOLD_IN_MEMORY_MAP','DATAFLOW-ADV','500000',1,0,500000,1000000);
-- Automatically disable data flow logging when in-memory object count reaches to 

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'ASYNC_WORKFLOW_SERVICE_LOGGING_ENABLED','DATAFLOW-WF','false',1,1,null,null);
--Workflow logging enabled for asynchronous services?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SELF_HEALING_ENABLED_FOR_WORKFLOW','DATAFLOW-ADV','false',1,0,null,null);
-- Auto recovery for Workflow services/events enabled?


Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'CO_CREATE_UPDATE_CANCEL_EVENT_LOGGING_ENABLED','DATAFLOW-CO','false',1,1,null,null);
-- Customer Order create, update and cancel workflow events logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'AUTH_SETTLEMENT_REFUND_EVENT_LOGGING_ENABLED','DATAFLOW-CO','false',1,1,null,null);
-- Authorization, refund and settlement workflow events logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'COMMUNICATION_NOTES_EVENT_LOGGING_ENABLED','DATAFLOW-CO','false',1,1,null,null);
-- CO communication notes workflow event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SNH_CHARGE_EVENT_LOGGING_ENABLED','DATAFLOW-CO','false',1,1,null,null);
-- S&H override/re-calculate workflow event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'DISCOUNT_APPEASEMENT_EVENT_LOGGING_ENABLED','DATAFLOW-CO','false',1,1,null,null);
-- Appeasement/Discount workflow event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SHIPMENT_CREATION_EVENT_LOGGING_ENABLED','DATAFLOW-COMMON','false',1,1,null,null);
-- Shipment creation workflow event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'DO_CREATE_EVENT_LOGGING_ENABLED','DATAFLOW-COMMON','false',1,1,null,null);
-- DO create workflow event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'ALLOCATION_EVENT_LOGGING_ENABLED','DATAFLOW-COMMON','false',1,1,null,null);
-- On Allocate workflow event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'BACKORDERED_EVENT_LOGGING_ENABLED','DATAFLOW-COMMON','false',1,1,null,null);
-- Backorder create workflow event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'REALLOCATION_EVENT_LOGGING_ENABLED','DATAFLOW-COMMON','false',1,1,null,null);
-- Re-allocation workflow event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'TRANSFER_ALLOCATION_EVENT_LOGGING_ENABLED','DATAFLOW-COMMON','false',1,1,null,null);
-- Transfer allocation workflow event logging enabled?

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'SUBMIT_ORDERLINE_EVENT_LOGGING_ENABLED','DATAFLOW-COMMON','false',1,1,null,null);
-- Submit order line workflow event logging enabled?

commit;


-- DBTicket DOM-1361

CREATE TABLE A_PROCESS_LOG_ASYNC_TRACK 
(	"PROCESS_ID" number(10,0) NOT NULL, 
"ACTIVE_THREAD_COUNT" number(2,0), 
"CREATED_DTTM" TIMESTAMP (6) DEFAULT SYSDATE NOT NULL, 
"LAST_UPDATED_DTTM" TIMESTAMP (6) DEFAULT SYSDATE NOT NULL
)TABLESPACE DOM_DT_TBS;
COMMENT ON COLUMN "A_PROCESS_LOG_ASYNC_TRACK"."CREATED_DTTM" IS 'Created Date';
COMMENT ON COLUMN "A_PROCESS_LOG_ASYNC_TRACK"."LAST_UPDATED_DTTM" IS 'Last updated date';
COMMENT ON COLUMN "A_PROCESS_LOG_ASYNC_TRACK"."ACTIVE_THREAD_COUNT" IS 'Total number of threads active for process';
COMMENT ON COLUMN "A_PROCESS_LOG_ASYNC_TRACK"."PROCESS_ID" IS 'primary key populated from A_process_log table';
ALTER TABLE A_PROCESS_LOG_ASYNC_TRACK ADD CONSTRAINT A_PROCESS_ASYNC_FLOW_TRAC_PK PRIMARY KEY (PROCESS_ID);

-- DBTicket DOM-1378

-- insert for Monitor data flow and Agent Status History in manu item, app and permission

MERGE INTO PERMISSION P
     USING (SELECT 'Admin Data Flow' PERMISSION_NAME, 'DOM_ADF' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);

MERGE INTO PERMISSION P
     USING (SELECT 'Administer Workflow details' PERMISSION_NAME, 'DOM_AWF' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);

MERGE INTO PERMISSION P
     USING (SELECT 'View Data Flow' PERMISSION_NAME, 'DOM_VDF' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);

MERGE INTO PERMISSION P
     USING (SELECT 'View Workflow details' PERMISSION_NAME, 'DOM_VWF' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);

Insert Into XMENU_ITEM (Xmenu_Item_Id,Name,Short_Name,Navigation_Key,Tile_Key,Icon,Bgcolor,Screen_Mode,Screen_Version,Base_Section_Name,Is_Default) Values((select max(xmenu_item_id)+1 from XMENU_ITEM where XMENU_ITEM_ID like '28%'),'Monitor Data Flow','Monitor Data Flow','MDF.screen.MonitorDataFlowScreen','','default-icon','#8AF03A',0,2,'Order Lifecycle',0);
Insert Into XMENU_ITEM (Xmenu_Item_Id,Name,Short_Name,Navigation_Key,Tile_Key,Icon,Bgcolor,Screen_Mode,Screen_Version,Base_Section_Name,Is_Default) Values((select max(xmenu_item_id)+1 from XMENU_ITEM where XMENU_ITEM_ID like '28%'),'Agent Status History','Agent Status History','AGENT.screen.AgentHistoryListScreen','','default-icon','#8AF03A',0,2,'Order Lifecycle',0);
insert into XMENU_ITEM_APP (XMENU_ITEM_ID,APP_ID) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Monitor Data Flow')),(select app_id from app where upper(app_short_name) like upper ('%DOM%')));
Insert Into XMENU_ITEM_APP (Xmenu_Item_Id,App_Id) Values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Agent Status History')),(select app_id from app where upper(app_short_name) like upper ('%DOM%')));
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Monitor Data Flow')),'DOM_VDF');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Monitor Data Flow')),'DOM_ADF');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Agent Status History')),'DOM_VWF');
Insert Into XMENU_ITEM_PERMISSION (Xmenu_Item_Id,Permission_Code) Values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Agent Status History')),'DOM_AWF');

-- update xbase menu part from transit Lanes to Lanes
Update Xbase_Menu_Part Set Name = 'Lanes' Where Xbase_Menu_Part_Id = (select xbase_menu_part_id from xbase_menu_part where name='Transit Lanes' And Xbase_Menu_Section_Id = (select xbase_menu_part_id from xbase_menu_part where name='Tariff'));

-- delete and insert 28 series to the base menu item with OLM
delete From Xbase_Menu_Item Where Xbase_Menu_part_Id = (select Xbase_Menu_Part_Id from xbase_menu_part where name ='Lanes') and xmenu_item_id !=((select xmenu_item_id from xmenu_item where name ='Lanes' and base_section_name='Transportation Data'));
update Xbase_Menu_Item set XBASE_MENU_PART_ID = (select xbase_menu_part_id from xbase_menu_part where name='Lanes') where XMENU_ITEM_ID = (select xmenu_item_id from xmenu_item where name ='Lanes' and base_section_name='Transportation Data');
insert into XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values(SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,(select xbase_menu_part_id from xbase_menu_part where name='Administration' and xbase_menu_section_id= (select xbase_menu_section_id from xbase_menu_section where name='Order Lifecycle Management')),(select XMENU_ITEM_ID from XMENU_ITEM where name='Monitor Data Flow' and base_section_name='Order Lifecycle' and base_part_name='GENERAL'),73);
insert into XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values(SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,(select xbase_menu_part_id from xbase_menu_part where name='Workflow' and xbase_menu_section_id= (select xbase_menu_section_id from xbase_menu_section where name='Order Lifecycle Management')),(select XMENU_ITEM_ID from XMENU_ITEM where name='Agent Status History' and base_section_name='Order Lifecycle' and base_part_name='GENERAL'),82);

-- delete Transit lane query
Delete From Xmenu_Item_Permission Where Xmenu_Item_Id=(select xmenu_item_id from xmenu_item where name='Transit Lanes' and base_section_name='Master Data');
Delete From Xmenu_Item_App Where Xmenu_Item_Id=(select xmenu_item_id from xmenu_item where name='Transit Lanes' and base_section_name='Master Data');
Delete from XBASE_MENU_ITEM Where Xmenu_Item_Id=(select xmenu_item_id from xmenu_item where name='Transit Lanes' and base_section_name='Master Data');
Delete from Xmenu_Item where name='Transit Lanes' and base_section_name='Master Data';

commit;
-- DBTicket DOM-1444

exec sequpdt ('A_PROCESS_LOG_CONFIG','A_PROCESS_LOG_CONFIG_ID','SEQ_PROCESS_LOG_CONFIG_ID');

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'CUSTOMER_MASTER_EVENT_LOGGING_ENABLED','DATAFLOW-CO','false',1,1,null,null);
--Customer Master events logging enabled

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) values (SEQ_PROCESS_LOG_CONFIG_ID.NEXTVAL,'RETURN_ORDER_EVENT_LOGGING_ENABLED','DATAFLOW-RETORD','false',1,1,null,null);
--Return Order events logging enabled
commit;

-- DBTicket MACR00755907


CREATE TABLE "WF_AGENT_STATUS_LOG"
(
"WF_AGENT_STATUS_LOG_ID"        NUMBER (8) NOT NULL,
"TC_COMPANY_ID"                 NUMBER (8, 0) NOT NULL,
"WF_AGENT_NAME"                 VARCHAR2 (50 BYTE) NOT NULL,
"WF_BATCH_SCHEDULER_ID"   NUMBER (9, 0) NOT NULL,
"WF_BUSINESS_PROCESS_ID"        NUMBER (19, 0),
"START_DTTM"                    TIMESTAMP (6),
"END_DTTM"                      TIMESTAMP (6),
"AGENT_STATUS"                  NUMBER (2, 0) NOT NULL,
"NO_OF_OBJECTS"                 NUMBER (8, 0),
"FAILURE_REASON_CODE"           NUMBER (2, 0)
)
TABLESPACE DOM_DT_TBS;

ALTER TABLE  WF_AGENT_STATUS_LOG ADD CONSTRAINT WF_AGENT_STATUS_LOG_PK PRIMARY KEY (WF_AGENT_STATUS_LOG_ID) USING INDEX TABLESPACE DOM_IDX_TBS; 

COMMENT ON COLUMN WF_AGENT_STATUS_LOG.WF_AGENT_STATUS_LOG_ID IS 'PK';
COMMENT ON COLUMN WF_AGENT_STATUS_LOG.TC_COMPANY_ID IS 'tc company id';
COMMENT ON COLUMN WF_AGENT_STATUS_LOG.WF_AGENT_NAME IS 'agent name';
COMMENT ON COLUMN WF_AGENT_STATUS_LOG.WF_BATCH_SCHEDULER_ID IS
'agent id';
COMMENT ON COLUMN WF_AGENT_STATUS_LOG.WF_BUSINESS_PROCESS_ID IS
'business process id';
COMMENT ON COLUMN WF_AGENT_STATUS_LOG.START_DTTM IS 'Agent start dttm';
COMMENT ON COLUMN WF_AGENT_STATUS_LOG.END_DTTM IS 'Agent end dttm';
COMMENT ON COLUMN WF_AGENT_STATUS_LOG.AGENT_STATUS IS 'Agent status';
COMMENT ON COLUMN WF_AGENT_STATUS_LOG.NO_OF_OBJECTS IS
'No of objects picked for processing by agent';
COMMENT ON COLUMN WF_AGENT_STATUS_LOG.FAILURE_REASON_CODE IS
'agent is failed then reason';


CREATE SEQUENCE SEQ_WF_AGENT_STATUS_LOG_ID
START WITH 1
MAXVALUE 9999999999999999999999999999
INCREMENT BY 1;

-- DBTicket MACR00757370

delete from  wf_service where service_name in ('Batch Settlement','Refund');
commit;
-- DBTicket MACR00765944

ALTER TABLE wf_agent_status_log ADD 
CONSTRAINT WF_AGSTA_FK1 
FOREIGN KEY (TC_COMPANY_ID) 
REFERENCES COMPANY (COMPANY_ID);

ALTER TABLE wf_agent_status_log ADD 
CONSTRAINT WF_AGSTA_FK12 
FOREIGN KEY (WF_BUSINESS_PROCESS_ID) 
REFERENCES WF_BUSINESS_PROCESS (BUSINESS_PROCESS_ID);

ALTER TABLE wf_agent_status_log ADD 
CONSTRAINT WF_AGSTA_FK13 
FOREIGN KEY (WF_BATCH_SCHEDULER_ID) 
REFERENCES wf_batch_schedule (WF_BATCH_SCHEDULE_ID);

-- DBTicket MACR00776818

Alter table wf_agent_status_log add
(
ENTITY_TYPE	VARCHAR2(50),
GROUPING_FIELD	VARCHAR2(100),
WM_JNDI_NAME	VARCHAR2(50),
MAX_NO_OF_OBJECTS_TO_PROCESS	NUMBER(8),
MAX_NO_OF_OBJECTS_PER_THREAD	NUMBER(8),
BUSINESS_PROCESS_NAME	VARCHAR2(50) 
);
COMMENT ON COLUMN wf_agent_status_log.ENTITY_TYPE IS 'Entity Type';
COMMENT ON COLUMN wf_agent_status_log.GROUPING_FIELD IS 'Grouping Field';
COMMENT ON COLUMN wf_agent_status_log.WM_JNDI_NAME IS 'Work manager JNDI Name';
COMMENT ON COLUMN wf_agent_status_log.MAX_NO_OF_OBJECTS_TO_PROCESS IS 'Max no. of objects to process';
COMMENT ON COLUMN wf_agent_status_log.MAX_NO_OF_OBJECTS_PER_THREAD IS 'Max no. of objects to process per thread';
COMMENT ON COLUMN wf_agent_status_log.BUSINESS_PROCESS_NAME IS 'Workflow name';


-- DBTicket DOM-1483

update a_process_log_config set config_param_value='false' where config_param_value='true' ;
commit;

update db_build_history set end_dttm=sysdate where version_label='DOM_20130503.2013.1.18.03';

commit;


insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('DOM_20130503.2013.1.18.03_01',SYSDATE,SYSDATE,'DOM DB Incremental Build - 18 for 2013.1','DOM_20130503.2013.1.18.03_01');

commit;

-- DBTicket DOM-1474
-- Please confirm with application team 

UPDATE XMenu_Item
SET Name            = 'Distance Time Parameter - CBO',
Short_Name        = 'Distance Time Parameter - CBO'
WHERE Xmenu_Item_Id = 2800029;

COMMIT;

-- DBTicket DOM-1625

DELETE FROM WF_EVENT_DEF_DETAIL WHERE EVENT_DEF_ID = 63;
DELETE FROM WF_EVENT_DEF_DETAIL WHERE EVENT_DEF_ID = 76;
INSERT
INTO WF_EVENT_DEF_DETAIL
(
WF_EVENT_DEF_DETAIL_ID,
TC_COMPANY_ID,
EVENT_DETAIL_NAME,
EVENT_DEF_ID,
EVENT_DETECTOR_CLASS,
ENTITY_ID,
ENTITY_TYPE_ID
)
VALUES
(
(SELECT MAX(WF_EVENT_DEF_DETAIL_ID)+1 FROM WF_EVENT_DEF_DETAIL
)
,
0,
'CO_APPEASEMENT',
'39',
'com.manh.doms.selling.events.customerorder.COAppeasementEventDetector',
NULL,
30
);
INSERT
INTO WF_EVENT_DEF_DETAIL
(
WF_EVENT_DEF_DETAIL_ID,
TC_COMPANY_ID,
EVENT_DETAIL_NAME,
EVENT_DEF_ID,
EVENT_DETECTOR_CLASS,
ENTITY_ID,
ENTITY_TYPE_ID
)
VALUES
(
(SELECT MAX(WF_EVENT_DEF_DETAIL_ID)+1 FROM WF_EVENT_DEF_DETAIL
)
,
0,
'COL_APPEASEMENT',
'39',
'com.manh.doms.selling.events.customerorder.COLAppeasementEventDetector',
NULL,
30
);
INSERT
INTO WF_EVENT_DEF_DETAIL
(
WF_EVENT_DEF_DETAIL_ID,
TC_COMPANY_ID,
EVENT_DETAIL_NAME,
EVENT_DEF_ID,
EVENT_DETECTOR_CLASS,
ENTITY_ID,
ENTITY_TYPE_ID
)
VALUES
(
(SELECT MAX(WF_EVENT_DEF_DETAIL_ID)+1 FROM WF_EVENT_DEF_DETAIL
)
,
0,
'CO_OTHER_DISCOUNT',
'39',
'com.manh.doms.selling.events.customerorder.COOtherDiscountEventDetector',
NULL,
30
);
INSERT
INTO WF_EVENT_DEF_DETAIL
(
WF_EVENT_DEF_DETAIL_ID,
TC_COMPANY_ID,
EVENT_DETAIL_NAME,
EVENT_DEF_ID,
EVENT_DETECTOR_CLASS,
ENTITY_ID,
ENTITY_TYPE_ID
)
VALUES
(
(SELECT MAX(WF_EVENT_DEF_DETAIL_ID)+1 FROM WF_EVENT_DEF_DETAIL
)
,
0,
'COL_OTHER_DISCOUNT',
'39',
'com.manh.doms.selling.events.customerorder.COLOtherDiscountEventDetector',
NULL,
30
);
COMMIT;

-- Activity MACR00853941

alter table a_payment_transaction modify process_count number(3);
--Activivty DOM-1868
CREATE TABLE A_USER_ACTIVITY_LOG
(
EVENT_ID NUMBER(5,0),
LOG_CATEGORY VARCHAR2(20 CHAR),
LOG_LEVEL VARCHAR2(20 CHAR),
TRANSACTION_ID VARCHAR2(20 CHAR),
EVENT_NAME VARCHAR2(20 CHAR),
SCREEN_ID VARCHAR2(20 CHAR),
USER_NAME VARCHAR2(20 CHAR),
ADDITIONAL_INFO VARCHAR2(300),
BROWSER_NAME VARCHAR2(20 CHAR),
BROWSER_VERSION VARCHAR2(20 CHAR),
EVENT_DTTM VARCHAR2(25),
CREATED_SOURCE  VARCHAR2(25),
CREATED_SOURCE_TYPE NUMBER(2),
CREATED_DTTM TIMESTAMP(6)
) TABLESPACE DOM_DT_TBS;
ALTER TABLE "A_USER_ACTIVITY_LOG" ADD CONSTRAINT "USR_ACTIVITY_PK" PRIMARY KEY ("EVENT_ID") USING INDEX TABLESPACE DOM_IDX_TBS;

CREATE SEQUENCE SEQ_USR_ACT_LOG MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;


update db_build_history set end_dttm=sysdate where version_label='DOM_20130503.2013.1.18.03_01';

commit;


insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('RLM_20130503.2013.18.03',SYSDATE,SYSDATE,'RLM DB Incremental Build - 18 for 2013.1','RLM_20130503.2013.18.03');

commit;
-- DBTicket MACR00751961

EXEC SEQUPDT('WF_SERVICE','SERVICE_ID','SEQ_WF_SERVICE');

Insert into WF_EVENT_DEF_DETAIL(WF_EVENT_DEF_DETAIL_ID, TC_COMPANY_ID, EVENT_DETAIL_NAME, EVENT_DEF_ID, EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
values((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL), 0, 'ASN_UPDATE', 97,'com.manh.olm.rlm.events.returnorder.ASNVerificationDetector', null, 70);

Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS)
values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.SELLING_INBOUND_QUEUE','Create RMA','com.manh.olm.rlm.inbound.app.services.bean.rmaservice.impl.ReturnOrderServiceBean','persistReturnOrder','getInstance','ASN','ReturnOrder',null,null);

Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS)
values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.SELLING_INBOUND_QUEUE','Return Order Outbound','com.manh.olm.rlm.inbound.app.services.bean.rmaservice.impl.ReturnOrderServiceBean','sendReturnOrderOutboundMsg','getInstance','ReturnOrder',null,null,null);


commit;

-- DBTicket MACR00755242

DELETE FROM WF_SERVICE
WHERE SERVICE_NAME = 'Confirm RMA';

INSERT INTO WF_SERVICE (SERVICE_ID,
INBOUND_QUEUE,
SERVICE_NAME,
EJB_LOOKUP_CLASS,
BUSINESS_METHOD,
LOOKUP_METHOD,
INPUT_ENTITY_TYPE,
OUTPUT_ENTITY_TYPE,
BUSINESS_CLASS,
DELEGATOR_CLASS)
VALUES (
SEQ_WF_SERVICE.NEXTVAL,
'jms.queue.SELLING_INBOUND_QUEUE',
'Confirm RMA',
'com.manh.olm.rlm.inbound.app.services.bean.rmaservice.impl.ReturnOrderServiceBean',
'confirmReturnOrder',
'getInstance',
'ReturnOrder',
'ReturnOrder',
NULL,
NULL);

COMMIT;


-- DBTicket MACR00755139
CREATE TABLE RLM_RETURN_ORDERS_LINE_ITEM (
RETURN_ORDERS_ID              NUMBER(10)  NOT NULL, 
RETURN_ORDERS_LINE_ITEM_ID    NUMBER(10)  NOT NULL,  
RETURN_REASON_ID              NUMBER(10),
RETURN_ACTION_ID              NUMBER(10),
EXPECTED_RECEIVING_CONDITION  VARCHAR2(15),
IS_RECEIVABLE                 NUMBER(1)   NOT NULL,
RECEIVING_VARIANCE_CODE       NUMBER(8),
EXCHANGE_SHIP_VIA             varchar2(4)
)
TABLESPACE RLM_COMMON_DATA;


ALTER TABLE RLM_RETURN_ORDERS_LINE_ITEM ADD (
CONSTRAINT RLM_RET_ORD_LINE_ITEM_PK1
PRIMARY KEY (RETURN_ORDERS_LINE_ITEM_ID)
USING INDEX TABLESPACE RLM_COMMON_INDX );

ALTER TABLE RLM_RETURN_ORDERS_LINE_ITEM ADD (
CONSTRAINT RLM_RET_ORD_LINE_ITEM_FK1
FOREIGN KEY (RETURN_ORDERS_LINE_ITEM_ID)
REFERENCES   PURCHASE_ORDERS_LINE_ITEM(PURCHASE_ORDERS_LINE_ITEM_ID));


comment on column RLM_RETURN_ORDERS_LINE_ITEM.RETURN_ORDERS_ID is 'Return order id-same as purchase orders id';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.RETURN_ORDERS_LINE_ITEM_ID is 'Return order line item id-same as purchase orders line item id';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.RETURN_REASON_ID is 'Return reason id is reason for returns';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.RETURN_ACTION_ID is 'Return action id is credit/replace(refund/exchange) action, the value is from CBO system code 01-Credit(Credit), 02-Replace(Exchange)';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.EXPECTED_RECEIVING_CONDITION is 'This gives the condition of the receiving items';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.IS_RECEIVABLE is 'Is the flag for item receivable or not';
COMMENT ON COLUMN RLM_RETURN_ORDERS_LINE_ITEM.RECEIVING_VARIANCE_CODE IS 'code to indicate if any variances in received item condition';

-- DBTicket MACR00755705
delete from WF_EVENT_DEF_DETAIL where EVENT_DETAIL_NAME = 'ASN_UPDATE';

insert into WF_EVENT_DEF_DETAIL(WF_EVENT_DEF_DETAIL_ID, TC_COMPANY_ID, EVENT_DETAIL_NAME, EVENT_DEF_ID, EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
values((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL), 0, 'ASN_UPDATE', 97,'com.manh.olm.rlm.returns.events.asn.ASNVerificationEventDetector', null, 70);

delete from WF_SERVICE where SERVICE_NAME = 'Create RMA';

Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS)
values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.SELLING_INBOUND_QUEUE','Create Return Order','com.manh.olm.rlm.returns.app.services.bean.returnorder.impl.ReturnOrderServiceBean','persistReturnOrder','getInstance','ASN','ReturnOrder',null,null);

delete from WF_SERVICE where SERVICE_NAME = 'Return Order Outbound';

Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS)
values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.SELLING_INBOUND_QUEUE','Return Order Outbound','com.manh.olm.rlm.returns.app.services.bean.returnorder.impl.ReturnOrderServiceBean','sendReturnOrderOutboundMsg','getInstance','ReturnOrder',null,null,null);

delete from WF_SERVICE where SERVICE_NAME = 'Confirm RMA';

Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS)
values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.SELLING_INBOUND_QUEUE','Confirm Return Order','com.manh.olm.rlm.returns.app.services.bean.returnorder.impl.ReturnOrderServiceBean','confirmReturnOrder','getInstance','ReturnOrder','ReturnOrder',null,null);

commit;

-- DBTicket MACR00764803

EXEC SEQUPDT('WF_SERVICE','SERVICE_ID','SEQ_WF_SERVICE');

INSERT INTO WF_EVENT_DEF_DETAIL(WF_EVENT_DEF_DETAIL_ID, TC_COMPANY_ID, EVENT_DETAIL_NAME, EVENT_DEF_ID, EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL), 0, 'RO_STATUS_CHANGE', 109, null, null, 110);


Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS)
values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.SELLING_INBOUND_QUEUE','Create Exchange Order',
'com.manh.olm.rlm.returns.services.bean.exchangeorderservices.impl.ExchangeOrderServiceBean','createExchangeOrder','getInstance','ReturnOrder','CustomerOrder',null,null);

update wf_service set EJB_LOOKUP_CLASS='com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean' where SERVICE_NAME='Create Return Order';
update wf_service set EJB_LOOKUP_CLASS='com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean' where SERVICE_NAME='Confirm Return Order';
update wf_service set EJB_LOOKUP_CLASS='com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean' where SERVICE_NAME='Return Order Outbound';

CREATE TABLE RLM_RETURN_ORDERS (
RETURN_ORDERS_ID           NUMBER(10) NOT NULL   PRIMARY KEY,
RETURN_ORDERS_XO_CREATED   NUMBER(1),
CONSTRAINT RLM_RETURN_ORDERS_FK FOREIGN KEY (RETURN_ORDERS_ID) REFERENCES PURCHASE_ORDERS(PURCHASE_ORDERS_ID)
)
TABLESPACE RLM_COMMON_DATA;

commit;

-- DBTicket MACR00762775


CREATE TABLE RLM_FEE (
FEE_ID                NUMBER(12) NOT NULL ,
TEMPLATE_ID           NUMBER(12) NOT NULL,
TC_COMPANY_ID         NUMBER(9) NOT NULL,
FEE_TYPE              VARCHAR2(15) NOT NULL,
FEE_NAME              VARCHAR2(10) NOT NULL,
FEE_VALUE             NUMBER(10,2) NOT NULL,
FEE_APPLY_TYPE        NUMBER(2) NOT NULL,         
FEE_APPLICATION_LEVEL NUMBER(2) NOT NULL, 
MARK_FOR_DELETION     NUMBER(2)  DEFAULT 0,
CREATED_SOURCE        VARCHAR2(50) NOT NULL,
CREATED_DTTM          TIMESTAMP(6) NOT NULL,      
LAST_UPDATED_SOURCE   VARCHAR2(50) NOT NULL, 
LAST_UPDATED_DTTM     TIMESTAMP(6) NOT NULL 
)TABLESPACE RLM_COMMON_DATA;

ALTER TABLE RLM_FEE ADD 
CONSTRAINT RLM_FEE_PK
PRIMARY KEY
(FEE_ID)
USING INDEX TABLESPACE RLM_COMMON_INDX;

COMMENT ON COLUMN RLM_FEE.FEE_ID IS 'Primary key. Database sequence Name for generating unique id is SEQ_RLM_FEE';
COMMENT ON COLUMN RLM_FEE.TEMPLATE_ID IS 'Fee Template Id. Foreign key to A_PROCESS_TEMPLATE table, column A_IDENTITY.';
COMMENT ON COLUMN RLM_FEE.TC_COMPANY_ID IS 'Company ID';
COMMENT ON COLUMN RLM_FEE.FEE_TYPE IS 'Type of fees. Value corresponds to SYS_CODE table.';
COMMENT ON COLUMN RLM_FEE.FEE_NAME IS 'Name of the fees';
COMMENT ON COLUMN RLM_FEE.FEE_VALUE IS 'Value of fees';
COMMENT ON COLUMN RLM_FEE.FEE_APPLY_TYPE IS 'Fee application type. 1-Flat fee, 2-Percentage';
COMMENT ON COLUMN RLM_FEE.FEE_APPLICATION_LEVEL IS 'Fees to be applied at header level (1) or line level (2)';
COMMENT ON COLUMN RLM_FEE.MARK_FOR_DELETION IS 'Defines Fee is associated with the template or not. 1-Deleted, 0-Active';
COMMENT ON COLUMN RLM_FEE.CREATED_SOURCE IS 'Created by user id';
COMMENT ON COLUMN RLM_FEE.CREATED_DTTM IS 'Created date and time';
COMMENT ON COLUMN RLM_FEE.LAST_UPDATED_SOURCE IS 'Last updated by user id';
COMMENT ON COLUMN RLM_FEE.LAST_UPDATED_DTTM IS 'Last updated date and time of this record';


ALTER TABLE RLM_FEE ADD (
CONSTRAINT RLM_FEE_TMPL_FK1 
FOREIGN KEY (TEMPLATE_ID) 
REFERENCES A_PROCESS_TEMPLATE (A_IDENTITY));

CREATE SEQUENCE SEQ_RLM_FEE MINVALUE 1 
MAXVALUE 999999999999 INCREMENT BY 1 
START WITH 1 CACHE 20  ;

-- DBTicket MACR00767676

EXEC SEQUPDT('WF_SERVICE','SERVICE_ID','SEQ_WF_SERVICE');

INSERT INTO WF_SERVICE (SERVICE_ID,
INBOUND_QUEUE,
SERVICE_NAME,
EJB_LOOKUP_CLASS,
BUSINESS_METHOD,
LOOKUP_METHOD,
INPUT_ENTITY_TYPE,
OUTPUT_ENTITY_TYPE,
BUSINESS_CLASS,
DELEGATOR_CLASS)
VALUES (
SEQ_WF_SERVICE.NEXTVAL,
'jms.queue.SELLING_INBOUND_QUEUE',
'Update Return Order',
'com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean',
'updateReturnOrder',
'getInstance',
'ReturnOrder',
'ReturnOrder',
NULL,
NULL);

INSERT INTO WF_SERVICE (SERVICE_ID,
INBOUND_QUEUE,
SERVICE_NAME,
EJB_LOOKUP_CLASS,
BUSINESS_METHOD,
LOOKUP_METHOD,
INPUT_ENTITY_TYPE,
OUTPUT_ENTITY_TYPE,
BUSINESS_CLASS,
DELEGATOR_CLASS)
VALUES (
SEQ_WF_SERVICE.NEXTVAL,
'jms.queue.SELLING_INBOUND_QUEUE',
'Apply Fees',
'com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean',
'applyReturnOrderFees',
'getInstance',
'ReturnOrder',
'ReturnOrder',
NULL,
NULL);


ALTER TABLE RLM_RETURN_ORDERS_LINE_ITEM
ADD UNIT_SHIPPING_HANDLING_CHARGE NUMBER(13,4)  DEFAULT 0.0 NOT NULL;  

-- DBTicket MACR00768738

ALTER TABLE RLM_RETURN_ORDERS_LINE_ITEM
ADD TOTAL_FEE NUMBER(13,4)  DEFAULT 0.0 NOT NULL;

-- DBTicket MACR00768596

DELETE FROM WF_SERVICE WHERE SERVICE_NAME='Update Return Order';
update wf_service set SERVICE_NAME='Calculate Return Fees' where SERVICE_NAME='Apply Fees';

commit;


-- DBTicket MACR00769088

INSERT INTO A_PROCESS_TYPE (DOM_PROCESS_TYPE, DESCRIPTION)
VALUES (91, 'Fee Template');

COMMIT;

-- DBTicket MACR00769981

INSERT INTO WF_SERVICE (SERVICE_ID,
INBOUND_QUEUE,
SERVICE_NAME,
EJB_LOOKUP_CLASS,
BUSINESS_METHOD,
LOOKUP_METHOD,
INPUT_ENTITY_TYPE,
OUTPUT_ENTITY_TYPE,
BUSINESS_CLASS,
DELEGATOR_CLASS)
VALUES (
SEQ_WF_SERVICE.NEXTVAL,
'jms.queue.SELLING_INBOUND_QUEUE',
'Create Return Invoice',
'com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean',
'calculateReturnInvoice',
'getInstance',
'ReturnOrder',
'ReturnOrder',
NULL,
NULL);

COMMIT;
-- DBTicket MACR00775631

INSERT INTO WF_SERVICE (SERVICE_ID,
INBOUND_QUEUE,
SERVICE_NAME,
EJB_LOOKUP_CLASS,
BUSINESS_METHOD,
LOOKUP_METHOD,
INPUT_ENTITY_TYPE,
OUTPUT_ENTITY_TYPE,
BUSINESS_CLASS,
DELEGATOR_CLASS)
VALUES (
SEQ_WF_SERVICE.NEXTVAL,
'jms.queue.SELLING_INBOUND_QUEUE',
'Create Exchange Order Invoice',
'com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean',
'calculateExchangeOrderInvoice',
'getInstance',
'CustomerOrder',
'CustomerOrder',
NULL,
NULL);

COMMIT;
-- DBTicket MACR00779442 

insert into PARAM_DEF (PARAM_DEF_ID,PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_NAME,DESCRIPTION,IS_DISABLED,IS_REQUIRED,DATA_TYPE,UI_TYPE,MIN_VALUE,
IS_MIN_INC,MAX_VALUE,IS_MAX_INC,DFLT_VALUE,DISPLAY_ORDER,VALUE_GENERATOR_CLASS,IS_LOCALIZABLE,PARAM_CATEGORY,CARR_DFLT_VALUE,BP_DFLT_VALUE,
PARAM_VALIDATOR_CLASS,CREATED_DTTM,LAST_UPDATED_DTTM) 
values ('DEFAULT_RETURN_ORDER_TYPE','DS','CustOrd','Default Return order type','Default Return Order Type',0,1,11,'DROPDOWN',null,
0,null,0,null,1,'com.manh.common.fv.OrderType',1,null,null,null,
null,SYSDATE,SYSDATE);
commit;
-- DBTicket MACR00780857
--insert into PARAM_GROUP (PARAM_GROUP_ID,PARAM_GROUP_NAME,CREATED_DTTM,LAST_UPDATED_DTTM) 
--values ('RLM','Reverse Logistics Management',sysdate,sysdate);


--insert into PARAM_SUBGROUP (PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_SUBGROUP_NAME,DISPLAY_ORDER,CREATED_DTTM,LAST_UPDATED_DTTM) 
-- values ('RLM','RetOrd','Return Order',1,sysdate,sysdate);
--It is applied in ORA_Incr_Build_CBO_20121127.2013.09.00.sql 


insert into PARAM_DEF (PARAM_DEF_ID,PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_NAME,DESCRIPTION,IS_DISABLED,IS_REQUIRED,DATA_TYPE,UI_TYPE,MIN_VALUE,
IS_MIN_INC,MAX_VALUE,IS_MAX_INC,DFLT_VALUE,DISPLAY_ORDER,VALUE_GENERATOR_CLASS,IS_LOCALIZABLE,PARAM_CATEGORY,CARR_DFLT_VALUE,BP_DFLT_VALUE,
PARAM_VALIDATOR_CLASS,CREATED_DTTM,LAST_UPDATED_DTTM) 
values ('DEFAULT_RETURN_CENTER','RLM','RetOrd','Default Return center','Default Return center',0,1,11,'TEXTBOX',null,
0,null,0,null,1,null,1,null,null,null,
null,sysdate,sysdate);


delete from PARAM_DEF where PARAM_DEF_ID = 'DEFAULT_RETURN_ORDER_TYPE' and PARAM_GROUP_ID ='DS';					   


insert into PARAM_DEF (PARAM_DEF_ID,PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_NAME,DESCRIPTION,IS_DISABLED,IS_REQUIRED,DATA_TYPE,UI_TYPE,MIN_VALUE,
IS_MIN_INC,MAX_VALUE,IS_MAX_INC,DFLT_VALUE,DISPLAY_ORDER,VALUE_GENERATOR_CLASS,IS_LOCALIZABLE,PARAM_CATEGORY,CARR_DFLT_VALUE,BP_DFLT_VALUE,
PARAM_VALIDATOR_CLASS,CREATED_DTTM,LAST_UPDATED_DTTM) 
values ('DEFAULT_RETURN_ORDER_TYPE','RLM','RetOrd','Default Return order type','Default Return Order Type',0,1,11,'DROPDOWN',null,
0,null,0,null,1,'com.manh.common.fv.OrderType',1,null,null,null,
null,sysdate,sysdate);
COMMIT;	

-- DBTicket MACR00782078

INSERT INTO PARAM_DEF (PARAM_DEF_ID,
PARAM_GROUP_ID,
PARAM_SUBGROUP_ID,
PARAM_NAME,
DESCRIPTION,
IS_DISABLED,
IS_REQUIRED,
DATA_TYPE,
UI_TYPE,
MIN_VALUE,
IS_MIN_INC,
MAX_VALUE,
IS_MAX_INC,
DFLT_VALUE,
DISPLAY_ORDER,
VALUE_GENERATOR_CLASS,
IS_LOCALIZABLE,
PARAM_CATEGORY,
CARR_DFLT_VALUE,
BP_DFLT_VALUE,
PARAM_VALIDATOR_CLASS,
CREATED_DTTM,
LAST_UPDATED_DTTM)
VALUES ('DEFAULT_RETURN_SHIP_VIA',
'RLM',
'RetOrd',
'Default Return Ship via',
'Default Return Ship Via',
0,
1,
11,
'TEXTBOX',
NULL,
0,
NULL,
0,
NULL,
1,
NULL,
1,
NULL,
NULL,
NULL,
NULL,
SYSDATE,
SYSDATE);

COMMIT;
-- DBTicket MACR00787438
alter table RLM_RETURN_ORDERS_LINE_ITEM 
add 
(
R_ADDRESS_1 varchar2(75),  
R_ADDRESS_2 varchar2(75),  
R_ADDRESS_3 varchar2(75),  
R_CITY      varchar2(40),  
R_STATE_PROV      varchar2(3),   
R_POSTAL_CODE     varchar2(10),  
R_COUNTY          varchar2(40),  
R_COUNTRY_CODE    varchar2(2),   
R_PHONE_NUMBER    varchar2(32),  
R_FAX_NUMBER      varchar2(20),  
R_EMAIL           varchar2(256)
);

comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_ADDRESS_1 is 'First line of the reship address';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_ADDRESS_2 is 'Second line of the reship address';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_ADDRESS_3 is 'Third line of the reship address';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_CITY is 'Reship city';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_COUNTRY_CODE is 'Reship country code - ISO 3166-1 Country code';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_COUNTY is 'Reship county';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_EMAIL is 'Reship Email';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_FAX_NUMBER is 'Reship FAX number';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_PHONE_NUMBER is 'Reship phone number';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_POSTAL_CODE is 'Reship postal code';
comment on column RLM_RETURN_ORDERS_LINE_ITEM.R_STATE_PROV is 'Reship state/province - ISO 3166-2 Country subdivision code';

-- DBTicket MACR00789355

update PARAM_DEF set UI_TYPE='DROPDOWN', DATA_TYPE = 11, VALUE_GENERATOR_CLASS='com.manh.olm.rlm.common.fv.ReturnCenterFiniteValue' where PARAM_DEF_ID = 'DEFAULT_RETURN_CENTER';

update PARAM_DEF set UI_TYPE='DROPDOWN', DATA_TYPE = 11, VALUE_GENERATOR_CLASS='com.manh.olm.rlm.common.fv.ReturnShipViaFiniteValue' where PARAM_DEF_ID = 'DEFAULT_RETURN_SHIP_VIA';

update PARAM_DEF set UI_TYPE='DROPDOWN', DATA_TYPE = 11, VALUE_GENERATOR_CLASS='com.manh.olm.rlm.common.fv.ReturnOrderTypeFiniteValue' where PARAM_DEF_ID = 'DEFAULT_RETURN_ORDER_TYPE';
commit;
-- DBTicket MACR00789352


create or replace view RETURN_SHIP_VIA as select distinct V.SHIP_VIA_ID,
V.SHIP_VIA,V.TC_COMPANY_ID,V.MARKED_FOR_DELETION,S.ENTITY_TYPE from A_SHIPPING_CHARGE_RULE S,SHIP_VIA V
where S.SHIP_VIA_ID = V.SHIP_VIA_ID AND S.START_DATE <= TRUNC(sysdate) AND S.END_DATE >= TRUNC(sysdate) and S.ENTITY_TYPE = 2 and S.ITEM_ID = -1 ;

create or replace view return_order_type as select order_type_id, DESCRIPTION, tc_company_id from order_type where MARK_FOR_DELETION = 0 and CHANNEL_TYPE = 30;

create or replace view return_centers as (select FAC.FACILITY_ID, FAC_ALIAS.FACILITY_ALIAS_ID, FAC_ALIAS.FACILITY_NAME, FAC.TC_COMPANY_ID, FAC.MARK_FOR_DELETION from facility fac, FACILITY_ALIAS fac_alias where FAC.FACILITY_ID = FAC_ALIAS.FACILITY_ID and FAC.FACILITY_TYPE_BITS >= 512 and FAC.MARK_FOR_DELETION = 0);

-- DBTicket MACR00789494


CREATE OR REPLACE VIEW return_order_type
AS
SELECT order_type_id,
DESCRIPTION,
tc_company_id,
MARK_FOR_DELETION
FROM order_type
WHERE MARK_FOR_DELETION = 0 AND CHANNEL_TYPE = 30;

-- DBTicket MACR00793768
update rlm_return_orders_line_item set RETURN_REASON_ID = null;
commit;
ALTER  TABLE rlm_return_orders_line_item MODIFY RETURN_REASON_ID varchar2(15 CHAR);
comment on column rlm_return_orders_line_item.RETURN_REASON_ID is 'Return reason code id for return item, the value is from sys_code table with rec_type = B and code_type = 032';

-- DBTicket MACR00800539

CREATE TABLE RLM_PACKAGES
(PACKAGE_ID number(16,0) , 
TC_COMPANY_ID number(9,0) not null,
RMA_NUMBER varchar2(50 char) not null, 
PACKAGE_DESC varchar2(250 char) not null,
CREATED_DTTM timestamp(6) not null, 
CREATED_SOURCE varchar2(50 char) not null, 
LAST_UPDATED_DTTM timestamp(6) not null, 
LAST_UPDATED_SOURCE varchar2(50 char) not null,
MARK_FOR_DELETION number(1) default '0' not null,
CONSTRAINT RLM_PACKAGES_PK primary key (PACKAGE_ID)
)
TABLESPACE RLM_COMMON_DATA;

comment on column RLM_PACKAGES.CREATED_DTTM is 'Created date and time';
comment on column RLM_PACKAGES.CREATED_SOURCE is 'Created by user id';
comment on column RLM_PACKAGES.LAST_UPDATED_DTTM is 'Last updated date and time';
comment on column RLM_PACKAGES.LAST_UPDATED_SOURCE is 'Last updated by user id';
comment on column RLM_PACKAGES.MARK_FOR_DELETION is 'Defines ReturnPackage is active or not. 1-Deleted, 0-Active';
comment on column RLM_PACKAGES.TC_COMPANY_ID is 'Company ID';
comment on column RLM_PACKAGES.PACKAGE_ID is 'Primary key. Database sequence Name for generating unique id is SEQ_RLM_PACKAGES_ID';
comment on column RLM_PACKAGES.RMA_NUMBER is 'Return Order Number';
comment on column RLM_PACKAGES.PACKAGE_DESC is 'Return Order Label Id(Package Description)';

CREATE SEQUENCE  "SEQ_RLM_PACKAGES_ID"  MINVALUE 1 MAXVALUE 9999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  CYCLE;
-- DBTicket MACR00800266
insert into PARAM_DEF 
(PARAM_DEF_ID,PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_NAME,DESCRIPTION,IS_DISABLED,IS_REQUIRED,DATA_TYPE,UI_TYPE,MIN_VALUE,IS_MIN_INC,MAX_VALUE,
IS_MAX_INC,DFLT_VALUE,DISPLAY_ORDER,VALUE_GENERATOR_CLASS,IS_LOCALIZABLE,PARAM_CATEGORY,
CARR_DFLT_VALUE,BP_DFLT_VALUE,PARAM_VALIDATOR_CLASS,CREATED_DTTM,LAST_UPDATED_DTTM) values 
('MERCHANT_CODE','RLM','RetOrd','Default Merchant code','Default Merchant code',0,1,4,'TEXTBOX',null,0,4,0,null,1,null,1,null,null,null,null,sysdate,sysdate);
commit;

-- DBTicket MACR00800289

--Note: Please replace 640 value with appropriate DB sequence value in below 1st insert query(XMENU_ITEM).

Insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,
BASE_PART_NAME,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,IS_DEFAULT)		
values (2800167,'Reverse Logistics Management','Reverse Logistics Management','/rlm/admin/parameter/jsp/ViewRLMParameters.jsp',null,'default-icon','#8AF03A',0,1,'Common Business Object - Base',
'Company Parameters',null,1,sysdate,null,1,sysdate,0);

insert into XMENU_ITEM_APP (XMENU_ITEM_ID,APP_ID,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
values ((select XMENU_ITEM_ID from XMENU_ITEM where name = 'Reverse Logistics Management'),32,null,1,sysdate,null,1,sysdate);

insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
values ((select XMENU_ITEM_ID from XMENU_ITEM where name = 'Reverse Logistics Management'),'VBR',null,1,sysdate,null,1,sysdate);

insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
values ((select XMENU_ITEM_ID from XMENU_ITEM where name = 'Reverse Logistics Management'),'ABR',null,1,sysdate,null,1,sysdate);
commit;

update XMENU_ITEM_APP set APP_ID = 28 where XMENU_ITEM_ID = 2800167;

UPDATE XMENU_ITEM SET BASE_SECTION_NAME = 'Order Process Configuration' WHERE XMENU_ITEM_ID = 2800167;
commit;
-- DBTicket MACR00807244
Insert into A_CO_REASON_CODE 
(REASON_CODE_ID,TC_COMPANY_ID,REASON_CODE,DESCRIPTION,REASON_CODE_TYPE,MARK_FOR_DELETION,SYSTEM_DEFINED) 
values (SEQ_CO_REASON_CODE.nextval,0,'AwaitRMAConfirm','Reason code for making XO on hold for awaiting RMA confirmation',0,0,1);
commit;

-- DBTicket MACR00802048

insert into WF_EVENT_DEF_DETAIL(WF_EVENT_DEF_DETAIL_ID, TC_COMPANY_ID, EVENT_DETAIL_NAME, EVENT_DEF_ID, EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
values((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL), 0, 'RO_CREATE', 42, null, null, 110);

insert into WF_EVENT_DEF_DETAIL(WF_EVENT_DEF_DETAIL_ID, TC_COMPANY_ID, EVENT_DETAIL_NAME, EVENT_DEF_ID, EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
values((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL), 0, 'RO_UPDATE', 109, 'com.manh.olm.rlm.returns.events.ro.ROUpdateEventDetector', null, 110);

insert into WF_EVENT_DEF_DETAIL(WF_EVENT_DEF_DETAIL_ID, TC_COMPANY_ID, EVENT_DETAIL_NAME, EVENT_DEF_ID, EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
values((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL), 0, 'RO_CANCEL', 41, null, null, 110);

insert into WF_EVENT_DEF_DETAIL(WF_EVENT_DEF_DETAIL_ID, TC_COMPANY_ID, EVENT_DETAIL_NAME, EVENT_DEF_ID, EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
values((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL), 0, 'RO_CONFIRMED', 109, 'com.manh.olm.rlm.returns.events.ro.ROConfirmedEventDetector', null, 110);

insert into WF_EVENT_DEF_DETAIL(WF_EVENT_DEF_DETAIL_ID, TC_COMPANY_ID, EVENT_DETAIL_NAME, EVENT_DEF_ID, EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
values((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL), 0, 'ROL_CANCEL', 109, 'com.manh.olm.rlm.returns.events.ro.ROLCancelEventDetector', null, 110);

insert into WF_SERVICE values ((select max(service_id)+1 from wf_service),'jms.queue.SELLING_INBOUND_QUEUE',
'Create Return ASN','com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean',
'createReturnASN','getInstance', 'ReturnOrder', 'ReturnOrder', '', '');

insert into WF_SERVICE values ((select max(SERVICE_ID)+1 from WF_SERVICE),'jms.queue.SELLING_INBOUND_QUEUE',
'Generate Return Label','com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean',
'generateReturnLabel','getInstance', 'ReturnOrder', 'ReturnOrder', '', '');

insert into WF_SERVICE values ((select max(SERVICE_ID)+1 from WF_SERVICE),'jms.queue.SELLING_INBOUND_QUEUE',
'Send Email','com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean',
'sendReturnConfirmationEmail','getInstance', 'ReturnOrder', 'ReturnOrder', '', '');
commit;
-- DBTicket MACR00811556
INSERT
INTO PARAM_DEF
(
PARAM_DEF_ID,
PARAM_GROUP_ID,
PARAM_SUBGROUP_ID,
PARAM_NAME,
DESCRIPTION,
IS_DISABLED,
IS_REQUIRED,
DATA_TYPE,
UI_TYPE,
MIN_VALUE,
IS_MIN_INC,
MAX_VALUE,
IS_MAX_INC,
DFLT_VALUE,
DISPLAY_ORDER,
VALUE_GENERATOR_CLASS,
IS_LOCALIZABLE,
PARAM_CATEGORY,
CARR_DFLT_VALUE,
BP_DFLT_VALUE,
PARAM_VALIDATOR_CLASS,
CREATED_DTTM,
LAST_UPDATED_DTTM
)
VALUES
(
'REFUND_SHIPPING_AND_HANDLING_CHARGES_PAID',
'RLM',
'RetOrd',
'Refund S&H paid',
'Refund shipping and handling charges paid for a customer order',
0,0,6,
'CHECKBOX',
NULL,
0,
NULL,
0,
'false',
4,
NULL,
1,
NULL,
NULL,
NULL,
NULL,
sysdate,
sysdate
);
commit;

-- DBTicket RLM-83

--New DOM process type for Order Fee Template and Order Line Fee Template configuration
insert into A_PROCESS_TYPE values (92, 'Order_Fee_Template');
insert into A_PROCESS_TYPE values (93, 'OrderLine_Fee_Template');
--New filter objects for Order Fee Template and Order Line Fee Template configuration
insert into FILTER_OBJECT values ('ORDER_FEE',4);
insert into FILTER_OBJECT values ('ORDER_LINE_FEE',4);

commit;

-- DBTicket RLM-93

INSERT INTO PARAM_DEF (PARAM_DEF_ID,PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_NAME,DESCRIPTION,IS_DISABLED,IS_REQUIRED,DATA_TYPE,UI_TYPE,MIN_VALUE,
IS_MIN_INC,MAX_VALUE,IS_MAX_INC,DFLT_VALUE,DISPLAY_ORDER,VALUE_GENERATOR_CLASS,IS_LOCALIZABLE,PARAM_CATEGORY,CARR_DFLT_VALUE,BP_DFLT_VALUE,
PARAM_VALIDATOR_CLASS,CREATED_DTTM,LAST_UPDATED_DTTM) 
values ('RECIEPT_NOT_EXPECTED_APPROVAL','RLM','RetOrd','Receipt not expected approval','Receipt not expected approval',0,1,11,'DROPDOWN',null,
0,null,0,null,1,'com.manh.olm.rlm.common.fv.ReceiptNotExpectedApprovalFV',1,null,null,null,
null,sysdate,sysdate);
commit;

-- DBTicket RLM-98

--Rally user story - US4746 Fees Template : Ability to configure fee templates 

alter table RLM_FEE rename column FEE_TYPE to FEE_DESCRIPTION;
exec sequpdt ('XMENU_ITEM','XMENU_ITEM_ID','SEQ_XMENU_ITEM_ID');
INSERT
INTO XMENU_ITEM
(
XMENU_ITEM_ID,
NAME,
SHORT_NAME,
NAVIGATION_KEY,
TILE_KEY,
ICON,
BGCOLOR,
SCREEN_MODE,
SCREEN_VERSION,
BASE_SECTION_NAME,
BASE_PART_NAME,
CREATED_SOURCE,
CREATED_SOURCE_TYPE,
CREATED_DTTM,
LAST_UPDATED_SOURCE,
LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,
IS_DEFAULT
)
VALUES
(
SEQ_XMENU_ITEM_ID.nextval,
'Order Fee Template',
'Order Fee Template',
'/doms/dom/template/view/DOMProcessTemplateList.jsflps?processType=Order_Fee_Template&customFilter=true',
NULL,
'default-icon',
'#8AF03A',
0,1,
'Order Lifecycle',
'GENERAL',
NULL,
1,
sysdate,
NULL,
1,
sysdate,
0
);
INSERT
INTO XMENU_ITEM_APP
(
XMENU_ITEM_ID,
APP_ID,
CREATED_SOURCE,
CREATED_SOURCE_TYPE,
CREATED_DTTM,
LAST_UPDATED_SOURCE,
LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM
)
VALUES
(
(SELECT XMENU_ITEM_ID
FROM XMENU_ITEM
WHERE SHORT_NAME = 'Order Fee Template'
)
,
28,
NULL,
1,
sysdate,
NULL,
1,
sysdate
);

INSERT
INTO XMENU_ITEM_PERMISSION
(
XMENU_ITEM_ID,
PERMISSION_CODE,
CREATED_SOURCE,
CREATED_SOURCE_TYPE,
CREATED_DTTM,
LAST_UPDATED_SOURCE,
LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM
)
VALUES
(
(SELECT XMENU_ITEM_ID
FROM XMENU_ITEM
WHERE SHORT_NAME = 'Order Fee Template'
)
,
'DOM_VALT',
NULL,
1,
sysdate,
NULL,
1,
sysdate
);

INSERT
INTO XBASE_MENU_ITEM
(
XBASE_MENU_ITEM_ID,
XBASE_MENU_PART_ID,
XMENU_ITEM_ID,
sequence,
CREATED_SOURCE,
CREATED_SOURCE_TYPE,
CREATED_DTTM,
LAST_UPDATED_SOURCE,
LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM
)
VALUES
(
SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
328,
(SELECT XMENU_ITEM_ID FROM XMENU_ITEM WHERE SHORT_NAME = 'Order Fee Template'
),
49,
NULL,
1,
sysdate,
NULL,
1,
sysdate
);

INSERT
INTO XMENU_ITEM
(
XMENU_ITEM_ID,
NAME,
SHORT_NAME,
NAVIGATION_KEY,
TILE_KEY,
ICON,
BGCOLOR,
SCREEN_MODE,
SCREEN_VERSION,
BASE_SECTION_NAME,
BASE_PART_NAME,
CREATED_SOURCE,
CREATED_SOURCE_TYPE,
CREATED_DTTM,
LAST_UPDATED_SOURCE,
LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,
IS_DEFAULT
)
VALUES
(
SEQ_XMENU_ITEM_ID.nextval,
'Order Line Fee Template',
'Order Line Fee Template',
'/doms/dom/template/view/DOMProcessTemplateList.jsflps?processType=OrderLine_Fee_Template&customFilter=true',
NULL,
'default-icon',
'#8AF03A',
0,1,
'Order Lifecycle',
'GENERAL',
NULL,
1,
sysdate,
NULL,
1,
sysdate,
0
);

INSERT
INTO XMENU_ITEM_APP
(
XMENU_ITEM_ID,
APP_ID,
CREATED_SOURCE,
CREATED_SOURCE_TYPE,
CREATED_DTTM,
LAST_UPDATED_SOURCE,
LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM
)
VALUES
(
(SELECT XMENU_ITEM_ID
FROM XMENU_ITEM
WHERE SHORT_NAME = 'Order Line Fee Template'
)
,
28,
NULL,
1,
sysdate,
NULL,
1,
sysdate
);

INSERT
INTO XMENU_ITEM_PERMISSION
(
XMENU_ITEM_ID,
PERMISSION_CODE,
CREATED_SOURCE,
CREATED_SOURCE_TYPE,
CREATED_DTTM,
LAST_UPDATED_SOURCE,
LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM
)
VALUES
(
(SELECT XMENU_ITEM_ID
FROM XMENU_ITEM
WHERE SHORT_NAME = 'Order Line Fee Template'
)
,
'DOM_VALT',
NULL,
1,
sysdate,
NULL,
1,
sysdate
);

INSERT
INTO XBASE_MENU_ITEM
(
XBASE_MENU_ITEM_ID,
XBASE_MENU_PART_ID,
XMENU_ITEM_ID,
SEQUENCE,
CREATED_SOURCE,
CREATED_SOURCE_TYPE,
CREATED_DTTM,
LAST_UPDATED_SOURCE,
LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM
)
VALUES
(
SEQ_XBASE_MENU_ITEM_ID.nextval,
328,
(SELECT XMENU_ITEM_ID
FROM XMENU_ITEM
WHERE SHORT_NAME = 'Order Line Fee Template'
),
49,
NULL,
1,
sysdate,
NULL,
1,
sysdate
);

commit;
DELETE FROM FILTER_LAYOUT WHERE OBJECT_TYPE = 'ORDER_FEE';

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 1, 0, 'PURCHASE_ORDERS.ORDER_CATEGORY', 'Order type', '=,!=', 'NUMBER', 'com.manh.common.fv.OrderType', NULL, 'SELECT', NULL, NULL, NULL, 1, 'ro.orderTypeId', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 2, 0, 'PURCHASE_ORDERS.RMA_STATUS', 'Order status', '=,!=', 'NUMBER', 'com.manh.cbo.syscode.finitevalue.ReturnOrderStatusFV', NULL, 'SELECT', NULL, NULL, NULL, 1, 'ro.rmaStatus', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');
INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 3, 0, 'CO_CREATED_DTTM', 'CO created date', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 1, 'co.createdDTTM', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 4, 0, 'PURCHASE_ORDERS.CREATED_DTTM', 'RMA created date', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 1, 'ro.createdDTTM', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 5, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.RETURN_ACTION_ID', 'Return type', '=', 'NUMBER', 'com.manh.olm.rlm.returns.common.fv.basedata.ReturnActionFV', NULL, 'SELECT', NULL, NULL, NULL, 1, 'rol.returnOrderExt.returnAction', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 6, 0, 'CUSTOMER_TYPE', 'Customer type', '=', 'NUMBER', NULL, NULL, 'SELECT', NULL, NULL, NULL, 1, 'co.customerType', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 7, 0, 'PURCHASE_ORDERS.O_COUNTRY_CODE', 'Origin country', '=,!=', 'UPPERCASE_STRING', NULL, '#{cboFilterDependentListBackingBean.primaryMap},#{cboFilterDependentListBackingBean.optionConstructList}', 'PRIMARY_COMBO', NULL, NULL, NULL, 1, 'ro.shipFromAddressCountryCode', NULL, 4, NULL, '8', NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 8, 0, 'PURCHASE_ORDERS.O_STATE_PROV', 'Origin state', '=,!=', 'UPPERCASE_STRING', NULL, '#{cboFilterDependentListBackingBean.optionConstructList}', 'DEPENDENT_COMBO', NULL, NULL, NULL, 1, 'ro.shipFromAddressStateProv', NULL, 4, NULL, '7', NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 9, 0, 'PURCHASE_ORDERS.O_CITY', 'Origin city', '=,!=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'ro.shipFromAddressCity', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 10, 0, 'PURCHASE_ORDERS.O_POSTAL_CODE', 'Origin zip', '=,!=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'ro.shipFromAddressPostalCode', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 11, 0, 'PURCHASE_ORDERS.D_COUNTRY_CODE', 'Destination country', '=,!=', 'UPPERCASE_STRING', NULL, '#{cboFilterDependentListBackingBean.primaryMap},#{cboFilterDependentListBackingBean.optionConstructList}', 'PRIMARY_COMBO', NULL, NULL, NULL, 1, 'ro.destinationCountryCode', NULL, 4, NULL, '12', NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 12, 0, 'PURCHASE_ORDERS.D_STATE_PROV', 'Destination state', '=,!=', 'UPPERCASE_STRING', NULL, '#{cboFilterDependentListBackingBean.optionConstructList}', 'DEPENDENT_COMBO', NULL, NULL, NULL, 1, 'ro.destinationStateProv', NULL, 4, NULL, '11', NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 13, 0, 'PURCHASE_ORDERS.D_CITY', 'Destination city', '=,!=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'ro.destinationCity', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 14, 0, 'PURCHASE_ORDERS.D_POSTAL_CODE', 'Destination zip', '=,!=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'ro.destinationPostalCode', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');



DELETE FROM FILTER_LAYOUT WHERE OBJECT_TYPE = 'ORDER_LINE_FEE';

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 1, 0, 'ITEM.PRODUCT_CLASS', 'Product class', '=,!=', 'NUMBER', 'com.manh.common.fv.ProductClass', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.productClassInt', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 2, 0, 'PURCHASE_ORDERS_LINE_ITEM.SKU', 'Item', '=,!=', 'UPPERCASE_STRING', NULL, '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=ItemName&permission_code=VBD&paginReq=false', 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.skuName', NULL, 4, NULL, '#{cbolookupBackingBean.getBUMap},#{cbolookupBackingBean.getOptionConstructMap}', NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 3, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.RETURN_ACTION_ID', 'Return type', '=,!=', 'NUMBER', 'com.manh.olm.rlm.returns.common.fv.basedata.ReturnActionFV', NULL, 'SELECT', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.returnAction', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 4, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.RETURN_REASON_ID', 'Return reason', '=,!=', 'STRING', 'com.manh.olm.rlm.common.fv.ReturnReasonCode', NULL, 'SELECT', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.returnReason', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 5, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.EXPECTED_RECEIVING_CONDITION', 'Item condition', '=,!=', 'STRING', 'com.manh.cbo.syscode.finitevalue.ExpectedRecvCondnCode', NULL, 'SELECT', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.expectedReceivingCondition', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 6, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.IS_RECEIVABLE', 'Receipt expected', '=', 'STRING', NULL, NULL, 'CHECKBOX', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.receiptExpected', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 7, 0, 'PURCHASE_ORDERS_LINE_ITEM.RMA_LINE_STATUS', 'Line status', '=,!=', 'STRING', 'com.manh.cbo.syscode.finitevalue.ReturnOrderLineStatusFV', NULL, 'SELECT', NULL, NULL, NULL, 1, 'rol.rmaLineStatus', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');
commit;

-- DBTicket RLM-113
alter table RLM_FEE modify (FEE_DESCRIPTION varchar2(50));
COMMENT ON column RLM_FEE.FEE_APPLY_TYPE is 'Fee apply type. 1-Percentage, 2-Flat fee, 3-Flat fee per qty';
COMMENT ON column RLM_FEE.FEE_DESCRIPTION is 'Fee rule description';
COMMENT ON column RLM_FEE.FEE_NAME is 'Fee name.Code value corresponds to Syscode B038 in SYS_CODE table';
-- DBTicket RLM-121
alter table A_CHARGE_DETAIL modify (CHARGE_NAME varchar2(100));
-- DBTicket RLM-124

DELETE FROM FILTER_LAYOUT WHERE OBJECT_TYPE = 'ORDER_FEE';

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 1, 0, 'PURCHASE_ORDERS.ORDER_CATEGORY', 'Order type', '=,!=', 'NUMBER', 'com.manh.common.fv.OrderType', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'ro.orderTypeId', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 2, 0, 'PURCHASE_ORDERS.RMA_STATUS', 'Order status', '=,!=', 'NUMBER', 'com.manh.cbo.syscode.finitevalue.ReturnOrderStatusFV', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'ro.rmaStatusLong', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 3, 0, 'CUSTOMER_ORDER.CREATED_DTTM', 'CO created date', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 1, 'ro.parentOrder.createdDTTM', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 4, 0, 'PURCHASE_ORDERS.CREATED_DTTM', 'RMA created date', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 1, 'ro.createdDTTM', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 5, 0, 'PURCHASE_ORDERS.RETURN_ORDER_TYPE', 'Return type', '=', 'NUMBER', 'com.manh.olm.rlm.returns.common.fv.basedata.ReturnOrderTypeFV', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'ro.returnOrderType', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 6, 0, 'CUSTOMER_TYPE', 'Customer type', '=,!=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'ro.customerInfo.customerType', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 7, 0, 'PURCHASE_ORDERS.O_COUNTRY_CODE', 'Origin country', '=,!=', 'UPPERCASE_STRING', NULL, '#{cboFilterDependentListBackingBean.primaryMap},#{cboFilterDependentListBackingBean.optionConstructList}', 'PRIMARY_COMBO', NULL, NULL, NULL, 1, 'ro.shipFromAddressCountryCode', NULL, 4, NULL, '8', NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 8, 0, 'PURCHASE_ORDERS.O_STATE_PROV', 'Origin state', '=,!=', 'UPPERCASE_STRING', NULL, '#{cboFilterDependentListBackingBean.optionConstructList}', 'DEPENDENT_COMBO', NULL, NULL, NULL, 1, 'ro.shipFromAddressStateProv', NULL, 4, NULL, '7', NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 9, 0, 'PURCHASE_ORDERS.O_CITY', 'Origin city', '=,!=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'ro.shipFromAddressCity', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 10, 0, 'PURCHASE_ORDERS.O_POSTAL_CODE', 'Origin zip', '=,!=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'ro.shipFromAddressPostalCode', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 11, 0, 'PURCHASE_ORDERS.D_COUNTRY_CODE', 'Destination country', '=,!=', 'UPPERCASE_STRING', NULL, '#{cboFilterDependentListBackingBean.primaryMap},#{cboFilterDependentListBackingBean.optionConstructList}', 'PRIMARY_COMBO', NULL, NULL, NULL, 1, 'ro.destinationCountryCode', NULL, 4, NULL, '12', NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 12, 0, 'PURCHASE_ORDERS.D_STATE_PROV', 'Destination state', '=,!=', 'UPPERCASE_STRING', NULL, '#{cboFilterDependentListBackingBean.optionConstructList}', 'DEPENDENT_COMBO', NULL, NULL, NULL, 1, 'ro.destinationStateProv', NULL, 4, NULL, '11', NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 13, 0, 'PURCHASE_ORDERS.D_CITY', 'Destination city', '=,!=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'ro.destinationCity', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 14, 0, 'PURCHASE_ORDERS.D_POSTAL_CODE', 'Destination zip', '=,!=', 'UPPERCASE_STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 1, 'ro.destinationPostalCode', NULL, 4, NULL, NULL, NULL, 'ORDER_FEE');
commit;



DELETE FROM FILTER_LAYOUT WHERE OBJECT_TYPE = 'ORDER_LINE_FEE';

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 1, 0, 'PURCHASE_ORDERS_LINE_ITEM.PRODUCT_CLASS', 'Product class', '=,!=', 'NUMBER', 'com.manh.common.fv.ProductClass', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.productClassInt', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 2, 0, 'PURCHASE_ORDERS_LINE_ITEM.SKU', 'Item', '=,!=', 'UPPERCASE_STRING', NULL, '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=ItemName&permission_code=VBD&paginReq=false', 'COMP', NULL, NULL, NULL, 1, 'rol.skuName', NULL, 4, NULL, '#{cbolookupBackingBean.getBUMap},#{cbolookupBackingBean.getOptionConstructMap}', NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 3, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.RETURN_ACTION_ID', 'Return type', '=,!=', 'NUMBER', 'com.manh.olm.rlm.returns.common.fv.basedata.ReturnActionFV', NULL, 'SELECT', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.returnAction', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 4, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.RETURN_REASON_ID', 'Return reason', '=,!=', 'STRING', 'com.manh.olm.rlm.common.fv.ReturnReasonCode', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.returnReason', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 5, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.EXPECTED_RECEIVING_CONDITION', 'Item condition', '=,!=', 'STRING', 'com.manh.cbo.syscode.finitevalue.ExpectedRecvCondnCode', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.expectedReceivingCondition', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 6, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.IS_RECEIVABLE', 'Receipt expected', '=', 'STRING', NULL, NULL, 'CHECKBOX', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.receiptExpected', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 7, 0, 'PURCHASE_ORDERS_LINE_ITEM.RMA_LINE_STATUS', 'Line status', '=,!=', 'STRING', 'com.manh.cbo.syscode.finitevalue.ReturnOrderLineStatusFV', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.rmaLineStatusStr', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

commit;
-- DBTicket RLM-165
INSERT INTO A_CO_REASON_CODE (REASON_CODE_ID,TC_COMPANY_ID,REASON_CODE,DESCRIPTION,REASON_CODE_TYPE,MARK_FOR_DELETION,SYSTEM_DEFINED) 
values (SEQ_CO_REASON_CODE.nextval,0,'ItemNeedsUserAction','Item Needs User Action',4,0,1);
COMMIT;
-- DBTicket RLM-166

ALTER TABLE RLM_RETURN_ORDERS ADD (IS_AUTOMATED_RMA NUMBER(1,0) DEFAULT 0);
COMMENT ON COLUMN RLM_RETURN_ORDERS.IS_AUTOMATED_RMA IS '1-Automated RMA, 0-RMA Created through UI';
-- DBTicket RLM-184
Insert into WF_EVENT_DEF_DETAIL (WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
values ((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL),0,'RO_WAIVE_FEE','109','com.manh.olm.rlm.returns.events.ro.ROWaiveFeeEventDetector',null,110);
commit;
-- DBTicket RLM-217
exec sequpdt('WF_SERVICE','SERVICE_ID','SEQ_WF_SERVICE');
Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS) values 
(SEQ_WF_SERVICE.NEXTVAL,'jms.queue.SELLING_INBOUND_QUEUE','Cancel Return ASN','com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean','cancelReturnASN','getInstance',
'ReturnOrder','ReturnOrder',null,null);

Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS)
values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.SELLING_INBOUND_QUEUE','Cancel Exchange Order','com.manh.olm.rlm.returns.services.bean.exchangeorderservices.impl.ExchangeOrderServiceBean','cancelExchangeOrder',
'getInstance','ReturnOrder','ReturnOrder',null,null);
commit;

-- DBTicket RLM-209

Insert into WF_EVENT_DEF_DETAIL (WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
values ((select max(WF_EVENT_DEF_DETAIL_ID)+ 1 from WF_EVENT_DEF_DETAIL),0,'RO_PROCESS','110',null,null,110);
COMMIT;
-- DBTicket RLM-210
--Reason code for 'No Variance'
INSERT INTO A_CO_REASON_CODE (REASON_CODE_ID,TC_COMPANY_ID,REASON_CODE,DESCRIPTION,REASON_CODE_TYPE,MARK_FOR_DELETION,SYSTEM_DEFINED) 
values (SEQ_CO_REASON_CODE.NEXTVAL,0,'No Variance','No Variance',4,0,1);
COMMIT;

-- DBTicket RLM-198
-- New Reason Code Added for Item Variance.
INSERT INTO A_CO_REASON_CODE (REASON_CODE_ID,TC_COMPANY_ID,REASON_CODE,DESCRIPTION,REASON_CODE_TYPE,MARK_FOR_DELETION,SYSTEM_DEFINED) 
values (SEQ_CO_REASON_CODE.NEXTVAL,0,'Item Variance','Item Variance',4,0,1);

-- Addition of new Service Evaluate Variance.
INSERT
INTO WF_SERVICE
(
SERVICE_ID,
INBOUND_QUEUE,
SERVICE_NAME,
EJB_LOOKUP_CLASS,
BUSINESS_METHOD,
LOOKUP_METHOD,
INPUT_ENTITY_TYPE,
OUTPUT_ENTITY_TYPE,
BUSINESS_CLASS,
DELEGATOR_CLASS
)
VALUES
(
SEQ_WF_SERVICE.NEXTVAL,
'jms.queue.SELLING_INBOUND_QUEUE',
'Evaluate Variance',
'com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean',
'evaluateVariance',
'getInstance',
'ReturnOrder',
'ReturnOrder',
NULL,
NULL
);

commit;
-- DBTicket RLM-225
--New CO Reason Code ADDRESS_NOT_AVAILABLE
INSERT INTO A_CO_REASON_CODE (REASON_CODE_ID,TC_COMPANY_ID,REASON_CODE,DESCRIPTION,REASON_CODE_TYPE,MARK_FOR_DELETION,SYSTEM_DEFINED) 
values (SEQ_CO_REASON_CODE.NEXTVAL,0,'AddressNotAvailable','Address Not Available',4,0,1);
commit;
-- DBTicket RLM236
UPDATE A_CO_REASON_CODE SET REASON_CODE_TYPE = 0 WHERE REASON_CODE = 'AddressNotAvailable';
commit;
--Activivty RLM-416
--New RLM workflow event - RO_RESEND_RETURN_LABEL
INSERT INTO WF_EVENT_DEF_DETAIL (WF_EVENT_DEF_DETAIL_ID,TC_COMPANY_ID,EVENT_DETAIL_NAME,EVENT_DEF_ID,EVENT_DETECTOR_CLASS,ENTITY_ID,ENTITY_TYPE_ID) 
VALUES ((SELECT MAX(WF_EVENT_DEF_DETAIL_ID)+ 1 FROM WF_EVENT_DEF_DETAIL),0,'RO_RESEND_RETURN_LABEL','111',null,null,111);

-- DBTicket RLM-417
UPDATE WF_SERVICE SET INBOUND_QUEUE = 'jms.queue.RETURNS_INBOUND_QUEUE' WHERE SERVICE_NAME 
IN ('Return Order Outbound','Calculate Return Fees','Create Return Order','Confirm Return Order','Create Return Invoice','Create Return ASN','Generate Return Label','Cancel Return ASN');

-- DBTicket RLM-147

DELETE FROM FILTER_LAYOUT WHERE OBJECT_TYPE = 'ORDER_LINE_FEE';
commit;
INSERT INTO FILTER_LAYOUT
(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES 
((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 1, 0, 'PURCHASE_ORDERS_LINE_ITEM.PRODUCT_CLASS', 'Product class', '=,!=', 
'NUMBER', 'com.manh.common.fv.ProductClass', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.productClassInt', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');


INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, 
SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, 
HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES 
((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 2, 0, 'PURCHASE_ORDERS_LINE_ITEM.SKU', 'Item', '=,!=', 'UPPERCASE_STRING', 
NULL, '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=ItemName&permission_code=VBD&paginReq=false', 'COMP', NULL, NULL, NULL, 1, 'rol.skuName', 
NULL, 4, NULL, '#{cbolookupBackingBean.getBUMap},#{cbolookupBackingBean.getOptionConstructMap}', NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, 
SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES 
((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 3, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.RETURN_ACTION_ID', 'Return type', '=,!=', 
'NUMBER', 'com.manh.olm.rlm.returns.common.fv.basedata.ReturnActionFV', NULL, 'SELECT', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.returnAction', 
NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, 
OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, 
LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES 
((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 4, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.RETURN_REASON_ID', 'Return reason', '=,!=', 
'STRING', 'com.manh.olm.rlm.common.fv.ReturnReasonCode', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.returnReason', 
NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 5, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.EXPECTED_RECEIVING_CONDITION', 'Item condition', '=,!=', 
'STRING', 'com.manh.cbo.syscode.finitevalue.ExpectedRecvCondnCode', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.expectedReceivingCondition',
NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, 
FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 6, 0, 'RLM_RETURN_ORDERS_LINE_ITEM.IS_RECEIVABLE', 'Receipt expected', '=', 'STRING', NULL, NULL, 'CHECKBOX', NULL, NULL, NULL, 1, 'rol.returnOrderLineExt.receiptExpectedStr', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES 
((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 7, 0, 'PURCHASE_ORDERS_LINE_ITEM.RMA_LINE_STATUS', 'Line status', '=,!=', 
'STRING', 'com.manh.cbo.syscode.finitevalue.ReturnOrderLineStatusFV', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'rol.rmaLineStatusStr', NULL, 4, NULL, NULL, NULL, 'ORDER_LINE_FEE');

commit;
-- DBTicket RLM-458

delete from WF_SERVICE where SERVICE_NAME = 'Apply Fees' and BUSINESS_METHOD = 'applyReturnOrderFees' and INPUT_ENTITY_TYPE = 'ReturnOrder';

delete from WF_SERVICE where SERVICE_NAME = 'Create Return Order' and INPUT_ENTITY_TYPE = 'ASN';
COMMIT;

Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS) values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.RETURNS_INBOUND_QUEUE','Get Return Order','com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean','getReturnOrder','getInstance','ASN','ReturnOrder',null,null);

delete from WF_SERVICE where SERVICE_NAME = 'Confirm Return Order' and INPUT_ENTITY_TYPE = 'ReturnOrder';
COMMIT;
Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS) values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.RETURNS_INBOUND_QUEUE','Update Return Order Status','com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean','updateReturnOrderStatus','getInstance','ReturnOrder','ReturnOrder',null,null);

delete from WF_SERVICE where INPUT_ENTITY_TYPE = 'ReturnOrder' and SERVICE_NAME = 'Create Exchange Order';
commit;
Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS) values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.RETURNS_INBOUND_QUEUE','Create or Update Exchange Order','com.manh.olm.rlm.returns.services.bean.exchangeorderservices.impl.ExchangeOrderServiceBean','createOrUpdateExchangeOrder','getInstance','ReturnOrder','ReturnOrder',null,null);

delete from WF_SERVICE where SERVICE_NAME = 'Evaluate Variance' and INPUT_ENTITY_TYPE = 'ReturnOrder';
Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS) values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.RETURNS_INBOUND_QUEUE','Evaluate Receipt Variance','com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean','evaluateReceiptVariance','getInstance','ReturnOrder','ReturnOrder',null,null);
commit;

update WF_SERVICE set INBOUND_QUEUE = 'jms.queue.RETURNS_INBOUND_QUEUE' where SERVICE_NAME = 'Cancel Exchange Order' and INPUT_ENTITY_TYPE = 'ReturnOrder';

update WF_SERVICE set INBOUND_QUEUE = 'jms.queue.RETURNS_INBOUND_QUEUE' where SERVICE_NAME = 'Return Order Outbound' and INPUT_ENTITY_TYPE = 'ReturnOrder';

delete from WF_SERVICE where SERVICE_NAME = 'Send Email' and INPUT_ENTITY_TYPE = 'ReturnOrder';
commit;
update WF_SERVICE set OUTPUT_ENTITY_TYPE = 'ReturnOrder' where SERVICE_NAME = 'Return Order Outbound' and INPUT_ENTITY_TYPE = 'ReturnOrder';
commit;

-- DBTicket RLM-492

--US12740 Name: RLM Permissions and Status check 
--Delete 2013.0 RLM XMENU entries

delete from XMENU_ITEM_PERMISSION where XMENU_ITEM_ID in 
(select XMENU_ITEM_ID from XMENU_ITEM where NAVIGATION_KEY like '/rlm%' and (XMENU_ITEM_ID > 3200000 and XMENU_ITEM_ID < 3200100));

delete from XMENU_ITEM_APP where XMENU_ITEM_ID in 
(select XMENU_ITEM_ID from XMENU_ITEM where NAVIGATION_KEY like '/rlm%' and (XMENU_ITEM_ID > 3200000 and XMENU_ITEM_ID < 3200100));

delete from XBASE_MENU_ITEM where XMENU_ITEM_ID in 
(select XMENU_ITEM_ID from XMENU_ITEM where NAVIGATION_KEY like '/rlm%' and (XMENU_ITEM_ID > 3200000 and XMENU_ITEM_ID < 3200100));

delete from XMENU_ITEM where NAVIGATION_KEY like '/rlm%' and (XMENU_ITEM_ID > 3200000 and XMENU_ITEM_ID < 3200100);

delete from XMENU_ITEM_PERMISSION where XMENU_ITEM_ID = 3200001;
delete from XMENU_ITEM_APP where XMENU_ITEM_ID = 3200001;
delete from XBASE_MENU_ITEM where XMENU_ITEM_ID = 3200001;
delete from XMENU_ITEM where XMENU_ITEM_ID = 3200001;


--Delete 2013.1 RLM XMENU entries

delete from XMENU_ITEM_PERMISSION where XMENU_ITEM_ID in 
(select XMENU_ITEM_ID from XMENU_ITEM where SHORT_NAME in ('Reverse Logistics Management', 'Company Parameter - RLM', 'Order Fee Template','Order Line Fee Template'));

delete from XMENU_ITEM_APP where XMENU_ITEM_ID in 
(select XMENU_ITEM_ID from XMENU_ITEM where SHORT_NAME in ('Reverse Logistics Management', 'Company Parameter - RLM', 'Order Fee Template','Order Line Fee Template'));

delete from XBASE_MENU_ITEM where XMENU_ITEM_ID in 
(select XMENU_ITEM_ID from XMENU_ITEM where SHORT_NAME in ('Reverse Logistics Management', 'Company Parameter - RLM', 'Order Fee Template','Order Line Fee Template'));

delete from XMENU_ITEM where SHORT_NAME in ('Reverse Logistics Management', 'Company Parameter - RLM', 'Order Fee Template','Order Line Fee Template');


-- Re-Create 2013.1 RLM XMENU ENTRIES

MERGE INTO PERMISSION P
     USING (SELECT 'Administer Order Fee Template' PERMISSION_NAME, 'RLM_AOFT' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);  

MERGE INTO PERMISSION P
     USING (SELECT 'View Order Fee Template' PERMISSION_NAME, 'RLM_VOFT' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);	

MERGE INTO PERMISSION P
     USING (SELECT 'Administer Order Line Fee Template' PERMISSION_NAME, 'RLM_AOLFT' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);	 

MERGE INTO PERMISSION P
     USING (SELECT 'View Order Line Fee Template' PERMISSION_NAME, 'RLM_VOLFT' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);	

MERGE INTO PERMISSION P
     USING (SELECT 'Company Parameter - RLM' PERMISSION_NAME, 'RLM_VBR' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);	   

insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select NVL(MAX(XMENU_ITEM_ID), 3200000)+1 from XMENU_ITEM where XMENU_ITEM_ID like '32%'),'Order Fee Template','Order Fee Template','/doms/dom/template/view/DOMProcessTemplateList.jsflps?processType=Order_Fee_Template&customFilter=true','','default-icon','#8AF03A',0,1,'Order Lifecycle',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select max(xmenu_item_id)+1 from XMENU_ITEM where XMENU_ITEM_ID like '32%'),'Order Line Fee Template','Order Line Fee Template','/doms/dom/template/view/DOMProcessTemplateList.jsflps?processType=OrderLine_Fee_Template&customFilter=true','','default-icon','#8AF03A',0,1,'Order Lifecycle',0);
insert into XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) values((select max(xmenu_item_id)+1 from XMENU_ITEM where XMENU_ITEM_ID like '32%'),'Company Parameter - RLM','Company Parameter - RLM','/rlm/admin/parameter/jsp/ViewRLMParameters.jsp','','default-icon','#8AF03A',0,1,'Order Lifecycle',0);

insert into XMENU_ITEM_APP (XMENU_ITEM_ID,APP_ID) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Order Fee Template')),(select app_id from app where upper(app_short_name) like upper ('%RLM%')));
insert into XMENU_ITEM_APP (XMENU_ITEM_ID,APP_ID) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Order Line Fee Template')),(select app_id from app where upper(app_short_name) like upper ('%RLM%')));
insert into XMENU_ITEM_APP (XMENU_ITEM_ID,APP_ID) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Company Parameter - RLM')),(select app_id from app where upper(app_short_name) like upper ('%RLM%')));

insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Order Fee Template')),'RLM_AOFT');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Order Fee Template')),'RLM_VOFT');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Order Line Fee Template')),'RLM_AOLFT');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Order Line Fee Template')),'RLM_VOLFT');
insert into XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,PERMISSION_CODE) values((select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Company Parameter - RLM')),'RLM_VBR');

insert into XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values(SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,(select XBASE_MENU_PART_ID from xbase_menu_part where upper(name) = upper('Processing')),(select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Order Fee Template')),52);
insert into XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values(SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,(select XBASE_MENU_PART_ID from xbase_menu_part where upper(name) = upper('Processing')),(select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Order Line Fee Template')),53);
insert into XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,XBASE_MENU_PART_ID,XMENU_ITEM_ID,SEQUENCE) values(SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,(select XBASE_MENU_PART_ID from xbase_menu_part where upper(name) = upper('Processing')),(select XMENU_ITEM_ID from XMENU_ITEM where upper(SHORT_NAME) like upper('Company Parameter - RLM')),63);

commit;



update db_build_history set end_dttm=sysdate where version_label='RLM_20130503.2013.1.18.03';

commit;


insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('RLM_20130503.2013.1.18.03_01',SYSDATE,SYSDATE,'RLM DB Incremental Build - 18 for 2013.1','RLM_20130503.2013.1.18.03_01');

commit;

-- DBTicket RLM-636
CREATE OR REPLACE FORCE VIEW "RETURN_ORDER_TYPE" ("ORDER_TYPE_ID", "ORDER_TYPE", "DESCRIPTION", "TC_COMPANY_ID", "MARK_FOR_DELETION") AS 
SELECT ORDER_TYPE_ID, ORDER_TYPE, DESCRIPTION, TC_COMPANY_ID, MARK_FOR_DELETION FROM ORDER_TYPE WHERE MARK_FOR_DELETION = 0 AND CHANNEL_TYPE = 30;

-- DBTicket RLM-678
INSERT INTO BUNDLE_META_DATA (BUNDLE_META_DATA_ID,BUNDLE_NAME,DESCRIPTION,DEF_SCREEN_TYPE_ID,DEF_APP_ID,HINT,IS_ACTIVE,IS_LABEL_BUNDLE,DEF_BU_ID) values (SEQ_BUNDLE_META_DATA_ID.NEXTVAL,'DATAFLOW','DATAFLOW','10',null,null,'1','1',-1); 
INSERT INTO BUNDLE_META_DATA (BUNDLE_META_DATA_ID,BUNDLE_NAME,DESCRIPTION,DEF_SCREEN_TYPE_ID,DEF_APP_ID,HINT,IS_ACTIVE,IS_LABEL_BUNDLE,DEF_BU_ID) values (SEQ_BUNDLE_META_DATA_ID.NEXTVAL,'DS_ORD_NEXTGEN_UI','Order screen next gen UI bundle name ','10',null,null,'1','1',-1);


update db_build_history set end_dttm=sysdate where version_label='RLM_20130503.2013.1.18.03';

commit;


insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('DOM_20130607.2013.1.18.05.01',SYSDATE,SYSDATE,'DOM DB Incremental Build - 18 for 2013.1','DOM_20130607.2013.1.18.05.01');

commit;

-- DBTicket DOM-1792

MERGE INTO LABEL L
USING (SELECT 'CustomerOrder' BUNDLE_NAME,
'Security_code_req_for_card' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT     (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (
SEQ_LABEL_ID.NEXTVAL,
'Security_code_req_for_card',
'Security code is required for credit card - {0}',
'CustomerOrder');

Commit;	

-- DBTicket DOM-1896
ALTER TABLE A_USER_ACTIVITY_LOG MODIFY TRANSACTION_ID  VARCHAR2(50 CHAR);

-- DBTicket DOM-1877

ALTER TABLE A_PROCESS_LOG_ERROR ADD ("CREATED_DTTM" TIMESTAMP (6) NOT NULL ENABLE);

-- DBTicket DOM-1811

DELETE FROM FILTER_LAYOUT WHERE OBJECT_TYPE = 'A_VD_ORDERALLOCATION';
COMMIT;
INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 0, 0, 'A_VD_ORDERALLOCATION.CHANNEL_TYPE', 'Sales channels', '=,!=', 'NUMBER', 'com.manh.common.fv.ChannelType', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'obj.channeltype', NULL, 8, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 1, 1, 'A_VD_ORDERALLOCATION.CHANNEL_TYPE', 'Sales channels', '=,!=', 'NUMBER', 'com.manh.common.fv.ChannelType', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'obj.channeltype', NULL, 4, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 2, 0, 'A_VD_ORDERALLOCATION.ORDER_TYPE', 'Order types', '=,!=', 'NUMBER', 'com.manh.common.fv.OrderType', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'obj.orderType', NULL, 8, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 3, 1, 'A_VD_ORDERALLOCATION.ORDER_TYPE', 'Order types', '=,!=', 'NUMBER', 'com.manh.common.fv.OrderType', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'obj.orderType', NULL, 4, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 4, 0, 'A_VD_ORDERALLOCATION.SUPPLY_TYPE', 'Supply types', '=,!=', 'NUMBER', 'com.manh.doms.ui.filter.valuegenerators.orderallocationmanager.SupplyType', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'obj.supplyType', NULL, 8, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 5, 1, 'A_VD_ORDERALLOCATION.SUPPLY_TYPE', 'Supply types', '=,!=', 'NUMBER', 'com.manh.doms.ui.filter.valuegenerators.orderallocationmanager.SupplyType', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'obj.supplyType', NULL, 4, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 6, 0, 'A_VD_ORDERALLOCATION.ORDERNUMBER', 'Order no', '=', 'STRING', NULL, '/cbo/transactional/lookup/idLookup.jsp?lookupType=SalesOrderId&=No&permission_code=VSO&paginReq=false', 'COMP', NULL, NULL, NULL, 0, 'obj.orderNumber', NULL, 12, NULL, '#{cboTranslookupBackingBean.getBUMap},#{cboTranslookupBackingBean.getOptionConstructMap}', NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 7, 0, 'A_VD_ORDERALLOCATION.ORDERLINEID', 'Line no', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'obj.orderLineNumber', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 8, 0, 'A_VD_ORDERALLOCATION.IS_LOCKED', 'Locked', '=', 'NUMBER', 'com.manh.common.fv.YesNoFiniteValue', NULL, 'SELECT', NULL, NULL, NULL, 1, 'obj.isLocked', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 9, 0, 'A_VD_ORDERALLOCATION.CUSTOMERID', 'Customer ID', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'obj.customerCode', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 10, 0, 'A_VD_ORDERALLOCATION.DESTINATION_FACILITY_ALIAS', 'Destination', '=,!=', 'STRING', NULL, '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=FacilityId&permission_code=VOR&paginReq=false', 'COMP', NULL, NULL, NULL, 0, 'obj.destinationFacilityAlias', NULL, 12, NULL, '#{cbolookupBackingBean.getBUMap},#{cbolookupBackingBean.getOptionConstructMap}', NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 11, 0, 'A_VD_ORDERALLOCATION.REQUESTEDSKUNAME', 'Ordered item', '=,!=', 'STRING', NULL, '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=ItemName&permission_code=VOR&paginReq=false', 'COMP', NULL, NULL, NULL, 0, 'obj.requestedSKUName', NULL, 12, NULL, '#{cbolookupBackingBean.getBUMap},#{cbolookupBackingBean.getOptionConstructMap}', NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 12, 0, 'A_VD_ORDERALLOCATION.FILLEDSKUNAME', 'Allocated item', '=,!=', 'STRING', NULL, '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=ItemName&permission_code=VOR&paginReq=false', 'COMP', NULL, NULL, NULL, 0, 'obj.filledSkuName', NULL, 12, NULL, '#{cbolookupBackingBean.getBUMap},#{cbolookupBackingBean.getOptionConstructMap}', NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 13, 0, 'A_VD_ORDERALLOCATION.INVENTORY_SEGMENT_ID', 'Inventory segment', '=,!=', 'NUMBER', 'com.manh.common.fv.InventorySegment', NULL, 'SELECT', NULL, NULL, NULL, 1, 'obj.inventorySegmentId', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 14, 0, 'A_VD_ORDERALLOCATION.DISTRIBUTIONCENTERNAME', 'Origin facility', '=,!=', 'STRING', NULL, '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=FacilityId&permission_code=VOR&paginReq=false', 'COMP', NULL, NULL, NULL, 0, 'obj.distributionCenterName', NULL, 12, NULL, '#{cbolookupBackingBean.getBUMap},#{cbolookupBackingBean.getOptionConstructMap}', NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 15, 0, 'A_VD_ORDERALLOCATION.SUPPLY', 'Supply', '=,!=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'obj.supply', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 16, 0, 'A_VD_ORDERALLOCATION.SUPPLY_DETAIL', 'Supply detail', '=,!=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'obj.supplyDetail', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 17, 0, 'A_VD_ORDERALLOCATION.SUBSTITUTIONTYPE', 'Substitution type', '=,!=', 'NUMBER', 'com.manh.doms.ui.filter.valuegenerators.orderallocationmanager.SubstitutionType', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'obj.substitutionType', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 18, 0, 'A_VD_ORDERALLOCATION.ISVIRTUALALLOCATION', 'Virtual allocation', '=', 'NUMBER', 'com.manh.common.fv.YesNoFiniteValue', NULL, 'SELECT', NULL, NULL, NULL, 1, 'obj.isVirtualAllocation', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 19, 0, 'A_VD_ORDERALLOCATION.MUSTRELEASEDATE', 'Latest release by', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, 'obj.mustReleaseDate', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 20, 0, 'A_VD_ORDERALLOCATION.COMMITTEDSHIPDATE', 'Earliest ship by', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, 'obj.committedShipDate', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 21, 0, 'A_VD_ORDERALLOCATION.COMMITTEDDELIVERYDATE', 'Earliest delivery', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, 'obj.committedDeliveryDate', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 22, 0, 'A_VD_ORDERALLOCATION.MUSTSHIPDATE', 'Latest ship by', 'BT', 'DATE', NULL, NULL, 'BETWEEN', NULL, NULL, NULL, 0, 'obj.mustShipDate', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 23, 0, 'A_VD_ORDERALLOCATION.ALLOCATED_BY', 'Allocated by', '=', 'STRING', NULL, NULL, 'COMP', NULL, NULL, NULL, 0, 'obj.allocatedBy', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 24, 0, 'A_VD_ORDERALLOCATION.STATUS', 'Statuses', '=,!=', 'NUMBER', 'com.manh.doms.ui.filter.valuegenerators.orderallocationmanager.OrderInventoryAllocationStatus', NULL, 'MULTIPLE_STATE_COMP', NULL, NULL, NULL, 1, 'obj.status', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');

INSERT INTO FILTER_LAYOUT(FILTER_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, SELECTION_PAGE_URL, OPERATOR_TYPE, FILTER_SUB_OBJECT_ID, TABLE_NAME, DEFAULT_VALUE, IS_LOCALIZABLE, HIBERNATE_FIELD_NAME, PRODUCTS_AVAILABLE, SCREEN_TYPE, COLUMNSCREEN_TYPE, LOOKUP_ATTRIBUTE, SUB_OBJECT_TYPE, OBJECT_TYPE) 
VALUES ((SELECT MAX(FILTER_LAYOUT_ID) + 1 FROM FILTER_LAYOUT), 25, 0, 'A_VD_ORDERALLOCATION.IS_RELEASED', 'Overridable', '=', 'NUMBER', 'com.manh.common.fv.YesNoFiniteValue', NULL, 'SELECT', NULL, NULL, NULL, 1, 'obj.isReleased', NULL, 12, NULL, NULL, NULL, 'A_VD_ORDERALLOCATION');
COMMIT;
-- DBTicket DOM-1892

update A_SEGMENTED_INVENTORY set UN_ALLOCATABLE_QTY=0 where UN_ALLOCATABLE_QTY is null;
commit;
alter table A_SEGMENTED_INVENTORY modify UN_ALLOCATABLE_QTY default 0 not null;

commit;

update db_build_history set end_dttm=sysdate where version_label='DOM_20130607.2013.118.05.01';

commit;


insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('RLM_20130503.2013.1.18.05_01',SYSDATE,SYSDATE,'RLM DB Incremental Build - 18 for 2013.1','RLM_20130503.2013.1.18.05_01');

commit;

-- DBTicket RLM-684
update WF_SERVICE set INBOUND_QUEUE = 'jms.queue.RETURNS_INBOUND_QUEUE' 
where SERVICE_NAME in ('Cancel Return ASN','Create Return Invoice','Create Return ASN','Return Order Outbound','Calculate Return Fees','Generate Return Label','Get Return Order','Update Return Order Status');
COMMIT;

-- DBTicket RLM-685
update Filter_layout set operator_type = 'MULTIPLE_STATE_COMP' where OBJECT_TYPE = 'ORDER_LINE_FEE' and Field_label = 'Return type';
commit;
-- DBTicket RLM-704
ALTER TABLE RLM_RETURN_ORDERS_LINE_ITEM DROP (
R_ADDRESS_1,
R_ADDRESS_2,
R_ADDRESS_3,
R_CITY,
R_STATE_PROV,
R_POSTAL_CODE,
R_COUNTY,
R_COUNTRY_CODE,
R_PHONE_NUMBER,
R_FAX_NUMBER,
R_EMAIL,
EXCHANGE_SHIP_VIA);


-- DBTicket RLM-692

DELETE FROM A_PROCESS_TYPE WHERE DOM_PROCESS_TYPE= 91;

COMMIT;

update db_build_history set end_dttm=sysdate where version_label='RLM_20130503.2013.1.18.0501';

commit;



insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('RLM_20130704.2013.1.18.06',SYSDATE,SYSDATE,'RLM DB Incremental Build - 18 for 2013.1','RLM_20130704.2013.1.18.06');

commit;

-- DBTicket RLM-821

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'CO created date', 'CO created date',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'RMA created date', 'RMA created date',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Return type', 'Return type',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Customer type', 'Customer type',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Origin country', 'Origin country',
'FilterFieldLabel');
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Origin state', 'Origin state',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Origin city', 'Origin city',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Origin zip', 'Origin zip',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Destination country', 'Destination country',
'FilterFieldLabel');  

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Destination zip', 'Destination zip',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Return reason', 'Return reason',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Item condition', 'Item condition',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Receipt expected', 'Receipt expected',
'FilterFieldLabel');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Line status', 'Line status',
'FilterFieldLabel');

COMMIT;
-- DBTicket RLM-822

Insert into BUNDLE_META_DATA (BUNDLE_META_DATA_ID,BUNDLE_NAME,DESCRIPTION,DEF_SCREEN_TYPE_ID,DEF_APP_ID,HINT,IS_ACTIVE,IS_LABEL_BUNDLE,DEF_BU_ID) 
values (SEQ_BUNDLE_META_DATA_ID.nextval,'ReceiptNotExpectedApprovalFV','ReceiptNotExpectedApprovalFV',10,null,null,1,1,-1);
commit;
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'RetOrd', 'Return Order',
'ParamSubGroup');


insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'DEFAULT_RETURN_CENTER', 'Default Return center',
'ParamDef');


insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'DEFAULT_RETURN_ORDER_TYPE', 'Default Return order type',
'ParamDef');


insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'DEFAULT_RETURN_SHIP_VIA', 'Default Return Ship via',
'ParamDef');


insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'MERCHANT_CODE', 'Default Merchant code',
'ParamDef');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'REFUND_SHIPPING_AND_HANDLING_CHARGES_PAID', 'Refund S&H paid',
'ParamDef');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'RECIEPT_NOT_EXPECTED_APPROVAL', 'RECIEPT_NOT_EXPECTED_APPROVAL',
'ParamDef');
COMMIT;
-- DBTicket RLM-835

UPDATE FILTER_LAYOUT SET OPERATOR_TYPE = 'SELECT', VALUE_GENERATOR_CLASS = 'com.manh.common.fv.YesNoFiniteValue' ,
HIBERNATE_FIELD_NAME = 'rol.returnOrderLineExt.receiptExpectedIntStr'
WHERE OBJECT_TYPE = 'ORDER_LINE_FEE' AND FIELD_NAME = 'RLM_RETURN_ORDERS_LINE_ITEM.IS_RECEIVABLE';
commit;

update db_build_history set end_dttm=sysdate where version_label='RLM_20130704.2013.1.18.06';

commit;











insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('DOM_20130704.2013.18.06.01',SYSDATE,SYSDATE,'DOM DB Incremental Build  for 2013','DOM_20130704.2013.18.06.01');

commit;

-- DBTicket DOM-2123

UPDATE A_PROCESS_LOG_CONFIG  SET CONFIG_PARAM_VALUE = 2000, CONFIG_PARAM_MAX_VALUE = 5000 WHERE CONFIG_PARAM_NAME = 'NO_OF_ROWS_PER_BATCH_FROM_A_PROCESS_LOG';
UPDATE A_PROCESS_LOG_CONFIG  SET CONFIG_PARAM_VALUE = 1000 WHERE CONFIG_PARAM_NAME = 'PROCESSVIEW_LOGGING_BATCH_SIZE';
commit;

-- DBTicket DOM-2119
Drop sequence SEQ_DOM_PROC_RUN;
COMMENT ON COLUMN A_PROCESS_RUN.A_PROCESS_RUN_ID IS 'Surrogate Key (SEQ_DOM_PROC_RUN)';

-- DBTicket MACR00862378

UPDATE FILTER_LAYOUT SET VALUE_GENERATOR_CLASS = 'com.manh.doms.common.basedata.fv.CreditCardType' WHERE OBJECT_TYPE = 'CUSTOMER_ORDER' AND FIELD_NAME = 'A_PAYMENT_DETAIL.CARD_TYPE_ID';
commit;

-- DBTicket MACR00861857
update a_nav_wizard_definition set WIZARD_NAV_FLOW_HELPER='com.manh.doms.selling.ui.co.customerorder.helper.AdditionalDtlsCONavigationFlowHelper' where domain_group_id=220;
commit;


update db_build_history set end_dttm=sysdate where version_label='DOM_20130704.2013.18.06.01';

commit;



insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('RLM_20130704.2013.1.18.06.01',SYSDATE,SYSDATE,'RLM DB Incremental Build - 18 for 2013.1','RLM_20130704.2013.1.18.06.01');

commit;

-- DBTicket RLM-892
--Update ORIGINAL_PO_LINE_ITEM_ID for old return and exchange orders
UPDATE PURCHASE_ORDERS_LINE_ITEM POLI SET POLI.ORIGINAL_PO_LINE_ITEM_ID = POLI.SHELF_DAYS WHERE ORIGINAL_PO_LINE_ITEM_ID IS NULL AND POLI.SHELF_DAYS IS NOT NULL
AND POLI.SHELF_DAYS IN (SELECT PURCHASE_ORDERS_LINE_ITEM_ID FROM PURCHASE_ORDERS_LINE_ITEM);
commit;
-- DBTicket RLM-902

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'Company Parameter - RLM', 'Company Parameter - RLM',
'Navigation');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'RLM Company Parameters', 'RLM Company Parameters',
'OM');
COMMIT;
-- DBTicket RLM-908	
Insert into WF_SERVICE (SERVICE_ID,INBOUND_QUEUE,SERVICE_NAME,EJB_LOOKUP_CLASS,BUSINESS_METHOD,LOOKUP_METHOD,INPUT_ENTITY_TYPE,OUTPUT_ENTITY_TYPE,BUSINESS_CLASS,DELEGATOR_CLASS) 
values (SEQ_WF_SERVICE.NEXTVAL,'jms.queue.RETURNS_INBOUND_QUEUE','Reevaluate Return SnH','com.manh.olm.rlm.returns.services.bean.returnorderservices.impl.ReturnOrderServiceBean',
'reEvalauateReturnSnH','getInstance','ReturnOrder','ReturnOrder',null,null);
commit;


update db_build_history set end_dttm=sysdate where version_label='RLM_20130704.2013.1.18.06.01';

commit;











insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('DOM_20130725.2013.18.07',SYSDATE,SYSDATE,'DOM DB Incremental Build  for 2013.1','DOM_20130725.2013.18.07');

commit;

--NO DB CHANGES

update db_build_history set end_dttm=sysdate where version_label='DOM_20130725.2013.18.07';

commit;












insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('RLM_20130725.2013.1.18.07',SYSDATE,SYSDATE,'RLM DB Incremental Build - 18 for 2013.1','RLM_20130725.2013.1.18.07');

commit;

-- DBTicket RLM-924
delete from LABEL where key = 'RECIEPT_NOT_EXPECTED_APPROVAL' and bundle_name = 'ParamDef';
commit;
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'RECIEPT_NOT_EXPECTED_APPROVAL', 'Receipt not expected approval',
'ParamDef');
Commit;
-- DBTicket RLM-935

Insert into BUNDLE_META_DATA (BUNDLE_META_DATA_ID,BUNDLE_NAME,DESCRIPTION,DEF_SCREEN_TYPE_ID,DEF_APP_ID,HINT,IS_ACTIVE,IS_LABEL_BUNDLE,DEF_BU_ID) 
values (SEQ_BUNDLE_META_DATA_ID.nextval,'RLM','Reverse Logistics Management',10,null,null,1,1,-1);

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'RLM Company Parameters', 'RLM Company Parameters',
'RLM');
COMMIT;

update db_build_history set end_dttm=sysdate where version_label='RLM_20130725.2013.1.18.07';

commit;











insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('DOM_20130725.2013.18.07.01',SYSDATE,SYSDATE,'DOM DB Incremental Build  for 2013.1','DOM_20130725.2013.18.07.01');

commit;

-- DBTicket DOM-1931
update BUNDLE_META_DATA set bundle_name = 'DS_ORD_NEXTGEN_UI'  WHERE BUNDLE_NAME = 'DS_ORD_NEXTGEN_UI ';
COMMIT;
-- DBTicket DOM-2274
Alter table wf_agent_status_log drop constraint  WF_AGSTA_FK13;

-- DBTicket DOM-4594
ALTER TABLE A_PROCESS_LOG_CONFIG MODIFY CONFIG_PARAM_VALUE VARCHAR2(150);

Insert into A_PROCESS_LOG_CONFIG (A_PROCESS_LOG_CONFIG_ID,CONFIG_PARAM_NAME,CONFIG_PARAM_GROUP,CONFIG_PARAM_VALUE,CONFIG_PARAM_DISP_STATUS,
IS_BASIC,CONFIG_PARAM_MIN_VALUE,CONFIG_PARAM_MAX_VALUE) 
values ((SELECT MAX(A_PROCESS_LOG_CONFIG_ID)+1 FROM A_PROCESS_LOG_CONFIG),'CLUSTER_INFO','DATAFLOW-ADV','',1,0,null,null);
COMMIT;

-- DBTicket DOM-2307

--deleting the existing records if any
DELETE FROM A_PAYMENT_RESPONSE_MAPPING;
COMMIT;

--for credit card
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 100,0, '100' , 'Successful transaction.' , ' Successful transaction.' , 1, 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 101,0, '101' , 'The request is missing one or more required fields.'  , 
' The request is missing one or more required fields. ' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 102,0, '102' , 'One or more fields in the request contains invalid data.'  , 
'One or more fields in the request contains invalid data.' , 2 , 40, 5)

;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 103,0, '150' ,
'Error: General system failure. See the documentation for your CyberSource client (SDK) for information about how to handle retries in the case of system errors.'  ,  
'Error: General system failure. See the documentation for your CyberSource client (SDK) for information about how to handle retries in the case of system errors.' , 3, 40, 5)

;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 104,0, '151' , 'Error: The request was received but there was a server timeout.This error does not include timeouts between the client and the server.'  , 'Error: The request was received but there was a server timeout.This error does not include timeouts between the client and the server.' , 3, 40, 5)

;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 105,0, '152' , 'Error: The request was received, but a service did not finish running in time.'  ,  ' Error: The request was received but there was a server timeout. This error does not include timeouts between the client and the server.' , 3, 40, 5)

;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 106,0, '200' , 'The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the Address Verification Service (AVS) check.'  ,  'The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the Address Verification Service (AVS) check.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 107,0, '201' , 'The issuing bank has questions about the request. You do not receive an authorization code programmatically, but you might receive one verbally by calling the processor.'  ,  'The issuing bank has questions about the request. You do not receive an authorization code programmatically, but you might receive one verbally by calling the processor.' , 1, 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 108,0, '202' , 'Expired card. You might also receive this if the expiration date you provided does not match the date the issuing bank has on file.'  ,  'Expired card. You might also receive this if the expiration date you provided does not match the date the issuing bank has on file.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 109,0, '203' , 'General decline of the card. No other information provided by the issuing bank.'  ,  ' General decline of the card. No other information provided by the issuing bank. ' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 110,0, '204' , 'Insufficient funds in the account.'  ,  'Insufficient funds in the account.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 111,0, '205' , 'Stolen or lost card.'  ,  'Stolen or lost card.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 112,0, '207' , 'Issuing bank unavailable.'  ,  'Issuing bank unavailable.' , 3, 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 113,0, '208' , 'Inactive card or card not authorized for card-not-present transactions.'  ,  'Inactive card or card not authorized for card-not-present transactions.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 114,0, '209' , 'American Express Card Identification Digits (CID) did not match.'  ,  'American Express Card Identification Digits (CID) did not match.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 115,0, '210' , 'The card has reached the credit limit.'  ,  ' The card has reached the credit limit. ' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 116,0, '211' , 'Invalid card verification number.'  ,  ' Invalid card verification number. ' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 117,0, '221' , 'The customer matched an entry on the processorís negative file.'  ,  ' The customer matched an entry on the processorís negative file. ' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 118,0, '230' , 'The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the card verification (CV) check.'  ,  'The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the card verification (CV) check.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 119,0, '231' , 'Invalid account number.'  ,  'Invalid account number.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 120,0, '232' , 'The card type is not accepted by the payment processor.'  ,  'The card type is not accepted by the payment processor.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 121,0, '233' , 'General decline by the processor.'  ,  'General decline by the processor.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 122,0, '234' , 'There is a problem with your CyberSource merchant configuration.'  ,  'There is a problem with your CyberSource merchant configuration.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 123,0, '235' ,  
'The requested amount exceeds the originally authorized amount. Occurs, for example, if you try to capture an amount larger than the original authorization amount.'  , 
'The requested amount exceeds the originally authorized amount. Occurs, for example, if you try to capture an amount larger than the original authorization amount.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 124,0, '236' , 'Processor failure.'  ,  ' Processor failure.' , 3, 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 125,0, '237' , 'The authorization has already been reversed.'  ,  'The authorization has already been reversed.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 126,0, '238' , 'The authorization has already been captured.'  ,  'The authorization has already been captured.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 127,0, '239' , 'The requested transaction amount must match the previous transaction amount.'  ,  ' The requested transaction amount must match the previous transaction amount. ' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 128,0, '240' , 'The card type sent is invalid or does not correlate with the credit card number.'  ,  'The card type sent is invalid or does not correlate with the credit card number.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 129,0, '241' , 'The referenced request id is invalid for all follow-on transactions.'  ,  'The referenced request id is invalid for all follow-on transactions.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 130,0, '242' , 'You requested a capture, but there is no corresponding, unused authorization record. Occurs if there was not a previously successful authorization request or if the previously successful authorization has already been used by another capture request.'  ,  'You requested a capture, but there is no corresponding, unused authorization record. Occurs if there was not a previously successful authorization request or if the previously successful authorization has already been used by another capture request.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 131,0, '243' , 'The transaction has already been settled or reversed.'  ,  'The transaction has already been settled or reversed.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 132,0, '246' , 'The capture or credit is not voidable because the capture or credit information has already been submitted to your processor. Or, you requested a void for a type of transaction that cannot be voided.'  ,  'The capture or credit is not voidable because the capture or credit information has already been submitted to your processor. Or, you requested a void for a type of transaction that cannot be voided.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 133,0, '247' , 'You requested a credit for a capture that was previously voided.'  ,  'You requested a credit for a capture that was previously voided.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 134,0, '250' , 'Error: The request was received, but there was a timeout at the payment processor.'  ,  'Error: The request was received, but there was a timeout at the payment processor.' , 2 , 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 135,0, '400' , 'The fraud score exceeds your threshold.' , 'The fraud score exceeds your threshold' , 4, 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 136,0, '480' , 'The order is marked for review by Decision Manager.'  ,  'The order is marked for review by Decision Manager.' , 4, 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 137,0, '481' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 4, 40, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 138,0, '520' , 'The authorization request was approved by the issuing bank but declined by CyberSource based on your Smart Authorization settings.'  ,  'The authorization request was approved by the issuing bank but declined by CyberSource based on your Smart Authorization settings.' , 4, 40, 5)
;

------------ for echeck 
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 200,0, '100' , 'Successful transaction.' , ' Successful transaction.' , 1, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 201,0, '101' , 'The request is missing one or more required fields.'  , 
' The request is missing one or more required fields. ' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 202,0, '102' , 'One or more fields in the request contains invalid data.'  , 
'One or more fields in the request contains invalid data.' , 2 , 70, 5)

;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 203,0, '150' ,
'Error: General system failure. See the documentation for your CyberSource client (SDK) for information about how to handle retries in the case of system errors.'  ,  
'Error: General system failure. See the documentation for your CyberSource client (SDK) for information about how to handle retries in the case of system errors.' , 3, 70, 5);

INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 204,0, '151' , 'Error: The request was received but there was a server timeout.This error does not include timeouts between the client and the server.'  , 'Error: The request was received but there was a server timeout.This error does not include timeouts between the client and the server.' , 3, 70, 5)
;

INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 205,0, '152' , 'Error: The request was received, but a service did not finish running in time.'  ,  ' Error: The request was received but there was a server timeout. This error does not include timeouts between the client and the server.' , 3, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 206,0, '200' , 'The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the Address Verification Service (AVS) check.'  ,  'The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the Address Verification Service (AVS) check.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 207,0, '201' , 'The issuing bank has questions about the request. You do not receive an authorization code programmatically, but you might receive one verbally by calling the processor.'  ,  'The issuing bank has questions about the request. You do not receive an authorization code programmatically, but you might receive one verbally by calling the processor.' , 1, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 208,0, '202' , 'Expired card. You might also receive this if the expiration date you provided does not match the date the issuing bank has on file.'  ,  'Expired card. You might also receive this if the expiration date you provided does not match the date the issuing bank has on file.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 209,0, '203' , 'General decline of the card. No other information provided by the issuing bank.'  ,  ' General decline of the card. No other information provided by the issuing bank. ' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 210,0, '204' , 'Insufficient funds in the account.'  ,  'Insufficient funds in the account.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 211,0, '205' , 'Stolen or lost card.'  ,  'Stolen or lost card.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 212,0, '207' , 'Issuing bank unavailable.'  ,  'Issuing bank unavailable.' , 3, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 213,0, '208' , 'Inactive card or card not authorized for card-not-present transactions.'  ,  'Inactive card or card not authorized for card-not-present transactions.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 214,0, '209' , 'American Express Card Identification Digits (CID) did not match.'  ,  'American Express Card Identification Digits (CID) did not match.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 215,0, '210' , 'The card has reached the credit limit.'  ,  ' The card has reached the credit limit. ' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 216,0, '211' , 'Invalid card verification number.'  ,  ' Invalid card verification number. ' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 217,0, '221' , 'The customer matched an entry on the processorís negative file.'  ,  ' The customer matched an entry on the processorís negative file. ' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 218,0, '230' , 'The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the card verification (CV) check.'  ,  'The authorization request was approved by the issuing bank but declined by CyberSource because it did not pass the card verification (CV) check.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 219,0, '231' , 'Invalid account number.'  ,  'Invalid account number.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 220,0, '232' , 'The card type is not accepted by the payment processor.'  ,  'The card type is not accepted by the payment processor.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 221,0, '233' , 'General decline by the processor.'  ,  'General decline by the processor.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 222,0, '234' , 'There is a problem with your CyberSource merchant configuration.'  ,  'There is a problem with your CyberSource merchant configuration.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 223,0, '235' ,  
'The requested amount exceeds the originally authorized amount. Occurs, for example, if you try to capture an amount larger than the original authorization amount.'  , 
'The requested amount exceeds the originally authorized amount. Occurs, for example, if you try to capture an amount larger than the original authorization amount.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 224,0, '236' , 'Processor failure.'  ,  ' Processor failure.' , 3, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 225,0, '237' , 'The authorization has already been reversed.'  ,  'The authorization has already been reversed.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 226,0, '238' , 'The authorization has already been captured.'  ,  'The authorization has already been captured.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 227,0, '239' , 'The requested transaction amount must match the previous transaction amount.'  ,  ' The requested transaction amount must match the previous transaction amount. ' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 228,0, '240' , 'The card type sent is invalid or does not correlate with the credit card number.'  ,  'The card type sent is invalid or does not correlate with the credit card number.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 229,0, '241' , 'The referenced request id is invalid for all follow-on transactions.'  ,  'The referenced request id is invalid for all follow-on transactions.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 230,0, '242' , 'You requested a capture, but there is no corresponding, unused authorization record. Occurs if there was not a previously successful authorization request or if the previously successful authorization has already been used by another capture request.'  ,  'You requested a capture, but there is no corresponding, unused authorization record. Occurs if there was not a previously successful authorization request or if the previously successful authorization has already been used by another capture request.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 231,0, '243' , 'The transaction has already been settled or reversed.'  ,  'The transaction has already been settled or reversed.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 232,0, '246' , 'The capture or credit is not voidable because the capture or credit information has already been submitted to your processor. Or, you requested a void for a type of transaction that cannot be voided.'  ,  'The capture or credit is not voidable because the capture or credit information has already been submitted to your processor. Or, you requested a void for a type of transaction that cannot be voided.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 233,0, '247' , 'You requested a credit for a capture that was previously voided.'  ,  'You requested a credit for a capture that was previously voided.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 234,0, '250' , 'Error: The request was received, but there was a timeout at the payment processor.'  ,  'Error: The request was received, but there was a timeout at the payment processor.' , 2 , 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 235,0, '400' , 'The fraud score exceeds your threshold.' , 'The fraud score exceeds your threshold' , 4, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 236,0, '480' , 'The order is marked for review by Decision Manager.'  ,  'The order is marked for review by Decision Manager.' , 4, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 237,0, '481' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 4, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 238,0, '520' , 'The authorization request was approved by the issuing bank but declined by CyberSource based on your Smart Authorization settings.'  ,  'The authorization request was approved by the issuing bank but declined by CyberSource based on your Smart Authorization settings.' , 4, 70, 5)
;

------ specific to ECHECK

INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 239,0, 'Payment' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 1, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 240,0, 'Refund' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 1, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 241,0, 'Completed' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 1, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 242,0, 'Correction' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 243,0, 'Declined' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 244,0, 'Error' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 245,0, 'Failed' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 246,0, 'Final NSF' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 247,0, 'First NSF' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 248,0, 'NSF' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 249,0, 'Other' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 250,0, 'Second NSF' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 251,0, 'Stop Payment' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;
INSERT INTO A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
VALUES(SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal, 252,0, 'Void' , 'The order is rejected by Decision Manager.'  ,  'The order is rejected by Decision Manager.' , 2, 70, 5)
;

------------ for PayPal
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 300,0, '10002' , 'This error can be caused by an incorrect API username, an incorrect API password, or an invalid API signature. Make sure that all three of these values are correct. For your security, PayPal does not report exactly which of these three values might be in error' 
,'This error can be caused by an incorrect API username, an incorrect API password, or an invalid API signature. Make sure that all three of these values are correct. For your security, PayPal does not report exactly which of these three values might be in error' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 301,0, '10001' , 'Transaction failed due to internal error' ,'Transaction failed due to internal error' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 302,0, '10004' , 'Invalid argument' ,'Invalid argument' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 303,0, '10007' , 'You do not have permissions to make this API call' ,'You do not have permissions to make this API call' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 304,0, '10009' , 'Account is locked or inactive' ,'Account is locked or inactive' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 305,0, '10010' , 'Invalid argument' ,'Invalid argument' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 306,0, '10202' , 'Transaction would exceed user''s monthly maximum' ,'Transaction would exceed user''s monthly maximum' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 307,0, '10600' , 'Authorization is voided.' ,'Authorization is voided.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 308,0, '10601' , 'Authorization has expired.' ,'Authorization has expired.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 309,0, '10602' , 'Authorization has already been completed.' ,'Authorization has already been completed.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 310,0, '10603' , 'The buyer account is restricted.' ,'The buyer account is restricted.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 311,0, '10604' , 'Authorization transaction cannot be unilateral. It must include both buyer and seller to make an auth.' ,'Authorization transaction cannot be unilateral. It must include both buyer and seller to make an auth.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 312,0, '10605' , 'Currency is not supported.' ,'Currency is not supported.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 313,0, '10606' , 'Transaction rejected, please contact the buyer.' ,'Transaction rejected, please contact the buyer.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 314,0, '10607' , 'Authorization & Capture feature unavailable.' ,'Authorization & Capture feature unavailable.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 315,0, '10608' , 'The funding source is missing.' ,'The funding source is missing.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 316,0, '10609' , 'Transaction id is invalid.' ,'Transaction id is invalid.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 317,0, '10610' , 'Amount specified exceeds allowable limit.' ,'Amount specified exceeds allowable limit.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 318,0, '10611' , 'Authorization & Capture feature is not enabled for the merchant. Contact customer service.' ,'Authorization & Capture feature is not enabled for the merchant. Contact customer service.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 319,0, '10612' , 'Maxmimum number of allowable settlements has been reached. No more settlement for the authorization.' ,'Maxmimum number of allowable settlements has been reached. No more settlement for the authorization.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 320,0, '10613' , 'Currency of capture must be the same as currency of authorization.' ,'Currency of capture must be the same as currency of authorization.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 321,0, '10614' , 'You can void only the original authorization, not a reauthorization.' ,'You can void only the original authorization, not a reauthorization.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 322,0, '10615' , 'You can reauthorize only the original authorization, not a reauthorization.' ,'You can reauthorize only the original authorization, not a reauthorization.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 323,0, '10616' , 'Maximum number of reauthorization allowed for the auth is reached.' ,'Maximum number of reauthorization allowed for the auth is reached.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 324,0, '10617' , 'Maximum number of reauthorization allowed for the auth is reached.' ,'Maximum number of reauthorization allowed for the auth is reached.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 325,0, '10618' , 'Reauthorization is not allowed inside honor period.' ,'Reauthorization is not allowed inside honor period.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 326,0, '10619' , 'Invoice ID value exceeds maximum allowable length.' ,'Invoice ID value exceeds maximum allowable length.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 327,0, '10620' , 'Order has already been voided, expired or completed.' ,'Order has already been voided, expired or completed.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 328,0, '10621' , 'Order has expired.' ,'Order has expired.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 329,0, '10622' , 'Order is voided.' ,'Order is voided.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 330,0, '10623' , 'Maximum number of authorization allowed for the order is reached.' ,'Maximum number of authorization allowed for the order is reached.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 331,0, '10624' , 'Payment has already been made for this InvoiceID.' ,'Payment has already been made for this InvoiceID.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 332,0, '10625' , 'The amount exceeds the maximum amount for a single transaction.' ,'The amount exceeds the maximum amount for a single transaction.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 333,0, '10626' , 'Transaction refused due to risk model' ,'Transaction refused due to risk model' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 334,0, '10627' , 'The invoice ID field is not supported for basic authorizations' ,'The invoice ID field is not supported for basic authorizations' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 335,0, '10628' , 'This transaction cannot be processed at this time. Please try again later.' ,'This transaction cannot be processed at this time. Please try again later.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 336,0, '10629' , 'Reauthorization is not allowed for this type of authorization.' ,'Reauthorization is not allowed for this type of authorization.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 337,0, '10630' , 'Item amount is invalid.' ,'Item amount is invalid.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 338,0, '11094' , 'This authorization can only be handled through the marketplace which created it. It cannot directly be voided, reauthorized, or captured against.' ,'This authorization can only be handled through the marketplace which created it. It cannot directly be voided, reauthorized, or captured against.' , 2, 90, 5)
;
insert into A_PAYMENT_RESPONSE_MAPPING (RESP_CODE_ID, RESPONSE_CODE_ID, COMPANY_ID, EXTERNAL_RESP_CODE, EXTERNAL_RESP_MSG, INTERNAL_RESP_DECISION_DESC, INTERNAL_RESP_DECISION, PAYMENT_METHOD, PAYMENT_TRANS_TYPE)
values(SEQ_PAYMENT_INT_EXT_RESP_ID.NEXTVAL, 339,0, '10011' , 'Transaction refused because of an invalid transaction id value' ,'Transaction refused because of an invalid transaction id value' , 2, 90, 5)
;
-- DBTicket DOM-2360
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Apply Coupons', 'NXGN Apply Coupons',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Apply Promotions', 'NXGN Apply Promotions',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Cancel Customer Order', 'NXGN Cancel Customer Order',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Create Customer Order', 'NXGN Create Customer Order',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Create or Update Customer Master', 'NXGN Create or Update Customer Master',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Create Order for all stores', 'NXGN Create Order for all stores',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Edit Customer Order', 'NXGN Edit Customer Order',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Hold Customer Order', 'NXGN Hold Customer Order',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Override Price', 'NXGN Override Price',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Override SnH Charges', 'NXGN Override SnH Charges',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN Override Validations for Price Override', 'NXGN Override Validations for Price Override',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_NXGN View Prices for all stores', 'NXGN View Prices for all stores',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_View Data Flow', 'View Data Flow',
'UCL');
COMMIT;

-- DBTicket DOM-4564

MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Source Exclusion Templates' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Source Exclusion Templates','Source Exclusion Templates','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Order Fee Template' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Order Fee Template','Order Fee Template','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Order Line Fee Template' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Order Line Fee Template','Order Line Fee Template','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Distance Time Parameter - CBO' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Distance Time Parameter - CBO','Distance Time Parameter - CBO','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Size Parameter - CBO' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Size Parameter - CBO','Size Parameter - CBO','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Selling Configurations' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Selling Configurations','Selling Configurations','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Monitor Data Flow' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Monitor Data Flow','Monitor Data Flow','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Lanes' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Lanes','Lanes','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Agent Status History' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Agent Status History','Agent Status History','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Business Partner' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Business Partner','Business Partner','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Order Lifecycle Management' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Order Lifecycle Management','Order Lifecycle Management','Navigation');
MERGE INTO LABEL L USING (SELECT 'Navigation' BUNDLE_NAME,'Scope Studio Administration' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Scope Studio Administration','Scope Studio Administration','Navigation');

COMMIT;		   

-- DBTicket DOM-4565

MERGE INTO LABEL L
USING (SELECT 'ParamSubGroup' BUNDLE_NAME,
'S&H' KEY
FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT     (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (
SEQ_LABEL_ID.NEXTVAL,
'S&H',
'S&H',
'ParamSubGroup');

COMMIT;

update db_build_history set end_dttm=sysdate where version_label='DOM_20130725.2013.18.07.01';

commit;











insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('RLM_20130725.2013.1.18.07.01',SYSDATE,SYSDATE,'RLM DB Incremental Build - 18 for 2013.1','RLM_20130725.2013.1.18.07.01');

commit;
-- DBTicket RLM-958
insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Administer Order Fee Template', 'Administer Order Fee Template',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Administer Order Line Fee Template', 'Administer Order Line Fee Template',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Approve return line', 'Approve return line',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Cancel return', 'Cancel return',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Company Parameter - RLM', 'Company Parameter - RLM',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Create return', 'Create return',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Override fees', 'Override fees',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Override non-exchangeable items', 'Override non-exchangeable items',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Override non-returnable items', 'Override non-returnable items',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Process return', 'Process return',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Re-send return label', 'Re-send return label',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_Return status', 'Return status',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_View Notes', 'View Notes',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_View Order Fee Template', 'View Order Fee Template',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLPermissions_View Order Line Fee Template', 'View Order Line Fee Template',
'UCL');

insert into LABEL (LABEL_ID, KEY, VALUE, BUNDLE_NAME)
values (SEQ_LABEL_ID.NEXTVAL, 'UCLAppModule_Reverse Logistics Management', 'Reverse Logistics Management',
'UCL');
COMMIT;

update db_build_history set end_dttm=sysdate where version_label='RLM_20130725.2013.1.18.07.01';

commit;












insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('DOM_20130808.2013.1.18.08',SYSDATE,SYSDATE,'DOM DB Incremental Build  for 2013.1','DOM_20130808.2013.1.18.08');

commit;
-- DBTicket DOM-4578
exec sequpdt ('A_PAYMENT_RESPONSE_MAPPING','RESPONSE_CODE_ID','SEQ_PAYMENT_INT_EXT_RESP_ID');
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,12,'DOM003','External Description not found','Internal Description not found',2,90,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,3,'DOM001','Gateway Service Unavailable','Gateway Service Unavailable',3,40,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,4,'DOM001','Gateway Service Unavailable','Gateway Service Unavailable',3,50,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,5,'DOM001','Gateway Service Unavailable','Gateway Service Unavailable',3,60,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,6,'DOM001','Gateway Service Unavailable','Gateway Service Unavailable',3,70,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,7,'DOM002','Dummy Authorization Record','Dummy Authorization Record',1,70,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,8,'DOM003','External Description not found','Internal Description not found',2,40,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,9,'DOM003','External Description not found','Internal Description not found',2,50,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,10,'DOM003','External Description not found','Internal Description not found',2,60,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,11,'DOM003','External Description not found','Internal Description not found',2,70,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,24,'DOM004','Dummy Settlement Record','Dummy Settlement Record',1,40,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,25,'DOM004','Dummy Settlement Record','Dummy Settlement Record',1,50,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,26,'DOM004','Dummy Settlement Record','Dummy Settlement Record',1,60,5);
Insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,27,'DOM004','Dummy Settlement Record','Dummy Settlement Record',1,70,5);
insert into A_PAYMENT_RESPONSE_MAPPING (RESPONSE_CODE_ID,COMPANY_ID,RESP_CODE_ID,EXTERNAL_RESP_CODE,EXTERNAL_RESP_MSG,INTERNAL_RESP_DECISION_DESC,INTERNAL_RESP_DECISION,PAYMENT_METHOD,PAYMENT_TRANS_TYPE) values (SEQ_PAYMENT_INT_EXT_RESP_ID.nextVal,0,28,'DOM004','Dummy Settlement Record','Dummy Settlement Record',1,90,5);
COMMIT;

-- DBTicket MACR00861857
update a_nav_wizard_definition set WIZARD_NAV_FLOW_HELPER='com.manh.doms.selling.ui.co.customerorder.helper.AdditionalDtlsCONavigationFlowHelper' where domain_group_id=220;
commit;

-- DBTicket DOM-6937
Update filter_layout set field_name='A_VD_ORDERALLOCATION.SUPPLY_TYPE' , hibernate_field_name='obj.supplyType' where field_name ='A_VD_ORDERALLOCATION.ALLOCATION_TYPE' and  
object_type='A_VD_ORDERALLOCATION';
Commit;

-- DBTicket DOM-6977
exec sequpdt ('ITEM_CBO', 'ITEM_ID' , 'ITEM_ID_SEQ');

-- DBTicket TCS-1370
--- DOM-7069
delete from LRF_REPORT_DEF_LAYOUT where value_generator_class like '%tcs%';
commit;

update db_build_history set end_dttm=sysdate where version_label='DOM_20130808.2013.1.18.08';

commit;











insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('DOM_20130808.2013.1.18.08_1',SYSDATE,SYSDATE,'DOM DB Incremental Build  for 2013.1','DOM_20130808.2013.1.18.08_1');

commit;

-- DBTicket DOM-7049
update a_process_log_config set is_basic=1 where config_param_name='CLUSTER_INFO';
COMMIT;

update db_build_history set end_dttm=sysdate where version_label='DOM_20130808.2013.1.18.08_1';

commit;











insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('DOM_20130808.2013.1.18.08_2',SYSDATE,SYSDATE,'DOM DB Incremental Build  for 2013.1','DOM_20130808.2013.1.18.08_2');

commit;

-- DBTicket DOM-7070

Insert into BUNDLE_META_DATA (BUNDLE_META_DATA_ID,BUNDLE_NAME,DESCRIPTION,DEF_SCREEN_TYPE_ID,DEF_APP_ID,HINT,IS_ACTIVE,IS_LABEL_BUNDLE,DEF_BU_ID) values (SEQ_BUNDLE_META_DATA_ID.NEXTVAL,'WORKFLOWAGENT','WORKFLOWAGENT',10,null,null,1,1,-1);

update xfield set xurl='/services/rest/dom/AgentHistoryService/agentHistoryService/workFlowAgentStatus' where name='status' and xscreen_id='280009';

commit;

update db_build_history set end_dttm=sysdate where version_label='DOM_20130808.2013.1.18.08_2';

commit;











insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('DOM_20130808.2013.1.18.08_3',SYSDATE,SYSDATE,'DOM DB Incremental Build  for 2013.1','DOM_20130808.2013.1.18.08_3');

commit;

-- DBTicket DOM-7080
update label set value= 'Create backorder in case of shorts/cancellations' where key='CREATE_BACKORDERS_ON_RELEASE_CANCELLATION' and bundle_name='ParamDef';
commit;

update db_build_history set end_dttm=sysdate where version_label='DOM_20130808.2013.1.18.08_3';

commit;











insert into db_build_history
(version_label, start_dttm, end_dttm, version_range, app_bld_no)
values 
('RLM_20130808.2013.1.18.08',SYSDATE,SYSDATE,'RLM DB Incremental Build - 18 for 2013.1','RLM_20130808.2013.1.18.08');

commit;
-- DBTicket RLM-961

create or replace
PACKAGE BODY                             rtlGsCustOrderFilterXML AS
FUNCTION rtlGsCustOrderFilterXML (
pobId           IN NUMBER,
custCode        IN VARCHAR2,
psFilterXML     IN VARCHAR2,
piStartIndex    IN NUMBER,
piEndIndex      IN NUMBER
)
RETURN cur IS p_cursor cur;
/******************************************************************************
Procedure:    rtlGsCustOrderFilterXML
Description:  Retrieves data from PURCHASE_ORDER

Build  Create    Who  What
1      07/23/09  GB   Initial Code
2      11/05/09  DPS  Changed ORD_DATE operater to >=. CR MACR00378231.
3      11/25/09  GB   added pobId parameter
4      12/16/09  GB   Excluding exchange customer order from purchase orders search
5      01/28/10  DPS  Updated for CSR flow. enabled search without CUSTOMER_CODE when it is passed null.
6      02/05/10  GB   Added  condition to search only sales order
7      02/24/10  DPS  Merged rtlGsCustOrderLineItems (Line item selction) with order seach.
8      05/03/10  LB   getting item quantity returned (assigned to RMA) to calculate quantity available
this code is added after removal of temp tables
9      05/05/10  LB   DS integration mapping changes
10     07/28/10  GB   added FREIGHT_ALLOWANCE in POB_CON_RMA: CBO 2011 changes
11     11/10/10  DPS  changed PO.TC_PURCHASE_ORDERS_ID to PO.PURCHASE_ORDERS_ID in line 115
11     11/11/10  GB   OrderNum compared with  PO.TC_PURCHASE_ORDERS_ID and orderId with PO.PURCHASE_ORDERS_ID.
12     12/10/10  GB   Subtracting cancelled quantity while calculating RMA_INVENTORY_QTY
13     01/06/10  GB   CBO-5.0.15 using A_ORDERLINE_QUANTITY_STATUS.QUANTITY as shipped quandity
14     01/10/11  DPS  CBO5.0.15 Added retrival of POLI.O_FACILITY_ID.
15     03/08/11  GB   CBO-5.0.20 Fix for : MACR00568178. fix for avail qty calculation issue with same sku in different lines
16     03/11/11  LB   getting NO_OF_RECORDS count: fix for CR MACR00584262
17     06/28/11  DD   CBO5.3.2 retreiving IS_RETURNABLE from PURCHASE_ORDER_LINE_ITEM table
18     07/20/11  KD   Putting condition for Select Sum of POLI1.ORDER_QTY as PCROI1.CREDIT_ISSUED = 1
and CREDIT_ISSUED is set to 1 in VRDUPPOBCONRMAORDITEMCREDIT.
19     09/14/11  DD   CBO 5.6.1.01  MACR00651972: Added ORDER BY RX to get the Item in consistent manner.
20     12/09/11  LB   Getting SKU_ID and removed reference of PURCHASE_ORDERS_CREDITCARD table which is
removed from 2012 First CBO release.
21     01/04/12  LB   removed mylog entry
22     17/01/2013 AKM QA CR MACR00810952  Order quantity is not correct in Create return page
23     02/Aug/2013 sbalasubramanyam JIRA Defect RLM-922 RMA lines are duplicated for a single CO line
Comments: PURCHASE_ORDERS_NOTE is not joined anymore, as it is not used by API based approach
to support web service based approach, it take much more effort
Reviewer: Ganapathi Bhaskar
**********************************************************************************************/
sSQL VARCHAR2(32767);
sFROMSQL VARCHAR2(1000);
sWhere VARCHAR2(7000);
sCR CHAR(1);
pValue VARCHAR2(1000);
pType VARCHAR2(1000);
ItemName VARCHAR2(1000);
sOrderBy VARCHAR2(1000);
xmldata    XMLType;
CURSOR cGetFilter IS
SELECT
EXTRACTVALUE(VALUE(filter), '/Parameter/@type') AS parmType,
EXTRACTVALUE(VALUE(filter), '/Parameter/@value') AS paramValue
FROM
TABLE(XMLSEQUENCE(EXTRACT(xmldata, '/SearchCrit/Parameter') ) ) filter;

CURSOR cGetOrderDate IS
SELECT
EXTRACTVALUE(VALUE(filter), '/OrderStartDate/@type') AS ordDateType,
EXTRACTVALUE(VALUE(filter), '/OrderStartDate/@value') AS ordStartDate
FROM
TABLE(XMLSEQUENCE(EXTRACT(xmldata, '/SearchCrit/OrderStartDate') ) ) filter;

BEGIN
--Parse XML document.
xmldata := XMLType.createXML(psFilterXML);
sCR := CHR(13);

-- CHANNEL_TYPE=20 : Customer Order

sSQL :='SELECT PO.PURCHASE_ORDERS_ID, PO.TC_PURCHASE_ORDERS_ID,PO.PURCHASE_ORDERS_DATE_DTTM,PO.GRAND_TOTAL,PO.TAX_CHARGES,
APD.BILL_TO_FIRST_NAME ||'' ''||APD.BILL_TO_LAST_NAME AS BILL_TO_NAME, APD.BILL_TO_ADDRESS_LINE1 AS BILL_TO_ADDRESS_1,
APD.BILL_TO_ADDRESS_LINE2 AS BILL_TO_ADDRESS_2, APD.BILLTO_STATE_PROV AS BILL_TO_STATE_PROV, APD.BILL_TO_CITY,
APD.BILL_TO_POSTAL_CODE, APD.BILL_TO_COUNTRY_CODE, APD.BILL_TO_PHONE_NUMBER, APD.BILL_TO_EMAIL, PO.BILL_TO_FAX_NUMBER,
PO.PAYMENT_MODE,
NULL AS FREIGHT_ALLOWANCE,NULL AS NOTE,
POLI.PURCHASE_ORDERS_LINE_ITEM_ID,POLI.O_FACILITY_ID, POLI.O_FACILITY_ALIAS_ID,POLI.SKU,POLI.SKU_ID,IC.DESCRIPTION,
IC.PRODUCT_CLASS_ID AS PRODUCT_CLASS,POLI.PURCHASE_ORDERS_LINE_STATUS AS POB_ITEM_STAT_IDEN,
POLI.STD_BUNDL_QTY,(CASE WHEN ( POLI.ORDER_QTY < AOQS.QUANTITY) THEN AOQS.QUANTITY ELSE POLI.ORDER_QTY END) AS ORDER_QTY,
POLI.RETAIL_PRICE,POLI.MV_CURRENCY_CODE,POLI.UNIT_MONETARY_VALUE,
POLI.PICKUP_START_DTTM,POLI.ACTUAL_COST,POLI.UNIT_TAX_AMOUNT,POLI.QTY_UOM_ID_BASE,POLI.STACK_LENGTH_VALUE,
POLI.STACK_WIDTH_VALUE, POLI.STACK_HEIGHT_VALUE,POLIRF.REF_FIELD1,POLIRF.REF_FIELD2,POLIRF.REF_FIELD3, POLIRF.REF_FIELD4,
POLIRF.REF_FIELD5,POLIRF.REF_FIELD6, IC.ITEM_BAR_CODE,POLI.IS_RETURNABLE,
NVL(((SELECT SUM(NVL(POLI1.ORDER_QTY, 0))
FROM PURCHASE_ORDERS_LINE_ITEM POLI1,
POB_CON_RMA_ORD_ITEM      PCROI1
WHERE PCROI1.ORD_LINE_NUM = POLI.PURCHASE_ORDERS_LINE_ITEM_ID
AND POLI1.TC_COMPANY_ID = PO.TC_COMPANY_ID
AND PCROI1.ORD_NUM = PO.TC_PURCHASE_ORDERS_ID
AND POLI1.PURCHASE_ORDERS_LINE_ITEM_ID =
PCROI1.POB_CON_RMA_ORD_ITEM_ID
AND PCROI1.REMOVED = 0
AND PCROI1.CREDIT_ISSUED = 1
AND PCROI1.ALLOW_IN_RMA = 0)-(SELECT SUM(NVL(POLI1.CANCELLED_QTY, 0))
FROM PURCHASE_ORDERS_LINE_ITEM POLI1,
POB_CON_RMA_ORD_ITEM      PCROI1
WHERE PCROI1.ORD_LINE_NUM = POLI.PURCHASE_ORDERS_LINE_ITEM_ID
AND POLI1.TC_COMPANY_ID = PO.TC_COMPANY_ID
AND PCROI1.ORD_NUM = PO.TC_PURCHASE_ORDERS_ID
AND POLI1.PURCHASE_ORDERS_LINE_ITEM_ID =
PCROI1.POB_CON_RMA_ORD_ITEM_ID
AND PCROI1.REMOVED = 0
AND PCROI1.ALLOW_IN_RMA = 0)-(SELECT NVL((SELECT SUM(NVL(AOQS.QUANTITY, 0))
FROM PURCHASE_ORDERS             XO,
PURCHASE_ORDERS_LINE_ITEM   XOLINE,
A_ORDERLINE_QUANTITY_STATUS AOQS,
POB_CON_RMA_ORD_ITEM        ROPCROI
WHERE XO.PARENT_PURCHASE_ORDERS_ID IN
(SELECT ROPO.PURCHASE_ORDERS_ID
FROM PURCHASE_ORDERS ROPO
WHERE ROPO.PARENT_PURCHASE_ORDERS_ID = PO.PURCHASE_ORDERS_ID
AND ROPO.CHANNEL_TYPE = 30)
AND XO.PURCHASE_ORDERS_ID = XOLINE.PURCHASE_ORDERS_ID
AND AOQS.PURCHASE_ORDERS_LINE_ITEM_ID =
XOLINE.PURCHASE_ORDERS_LINE_ITEM_ID
AND AOQS.SUBSTITUTION_TYPE IS NULL
AND AOQS.ITEM_STATUS=850
AND XOLINE.SHELF_DAYS = ROPCROI.POB_CON_RMA_ORD_ITEM_ID
AND ROPCROI.ORD_LINE_NUM=POLI.PURCHASE_ORDERS_LINE_ITEM_ID
AND AOQS.PURCHASE_ORDERS_ID = XOLINE.PURCHASE_ORDERS_ID),
0)
FROM DUAL
)),
0) RMA_INVENTORY_QTY, AOQS.QUANTITY AS SHIPPED_QTY, AOQS.QUANTITY AS SHIPPED_SIZE_VALUE';

sFROMSQL := ' FROM PURCHASE_ORDERS PO, PURCHASE_ORDERS_LINE_ITEM POLI,
PO_LINE_ITEM_REF_FIELDS POLIRF, ITEM_CBO IC, A_PAYMENT_DETAIL APD,A_ORDERLINE_QUANTITY_STATUS AOQS
';

IF custCode IS NOT NULL THEN
sWhere :=' WHERE PO.CUSTOMER_CODE=' || '''' ||  custCode || ''' AND ' ;
ELSE
sWhere := ' WHERE ';
END IF;
sWhere :=sWhere || ' PO.TC_COMPANY_ID=' || pobId;
sWhere :=sWhere || ' AND PO.PARENT_PURCHASE_ORDERS_ID IS NULL AND PO.CHANNEL_TYPE=20 AND PO.PURCHASE_ORDERS_TYPE=2
AND POLI.PURCHASE_ORDERS_ID=PO.PURCHASE_ORDERS_ID
AND POLI.PURCHASE_ORDERS_LINE_STATUS IN (400, 492, 500, 550, 600, 650, 800, 850)
AND POLI.PURCHASE_ORDERS_LINE_ITEM_ID=POLIRF.PURCHASE_ORDERS_LINE_ITEM_ID(+)
AND POLI.SKU_ID=IC.ITEM_ID
AND AOQS.PURCHASE_ORDERS_LINE_ITEM_ID = POLI.PURCHASE_ORDERS_LINE_ITEM_ID
AND AOQS.PURCHASE_ORDERS_ID = POLI.PURCHASE_ORDERS_ID
AND AOQS.SUBSTITUTION_TYPE IS NULL
AND AOQS.ITEM_STATUS=850
AND PO.PURCHASE_ORDERS_ID = APD.ENTITY_ID
AND APD.PAYMENT_DETAIL_ID = (SELECT MAX(PAYMENT_DETAIL_ID) FROM A_PAYMENT_DETAIL APD WHERE APD.ENTITY_ID = PO.PURCHASE_ORDERS_ID) ';

FOR lRow IN cGetFilter LOOP
pValue := lRow.paramValue;
pType := lRow.parmType;

IF pType='OrderNum' THEN
sWhere :=sWhere || '  AND PO.TC_PURCHASE_ORDERS_ID='|| '''' ||  pValue || '''';
END IF;
IF pType='OrderId' THEN
sWhere :=sWhere || '  AND PO.PURCHASE_ORDERS_ID='|| '''' ||  pValue || '''';
END IF;
IF pType='ZipCode' THEN
sWhere :=sWhere || ' AND PO.BILL_TO_POSTAL_CODE='|| '''' ||  pValue || '''';
END IF;
IF pType='ConsumerName' THEN
--sFROMSQL :=sFROMSQL||' , CUSTOMER C ';
--sWhere :=sWhere || ' AND C.TC_COMPANY_ID = PO.TC_COMPANY_ID AND C.CUSTOMER_CODE = PO.CUSTOMER_CODE AND C.CUSTOMER_NAME LIKE '|| '''%' ||  pValue || '%'' ';
sFROMSQL :=sFROMSQL||' , A_CUSTOMER_INFO C ';
sWhere :=sWhere || ' AND C.COMPANY_ID = PO.TC_COMPANY_ID AND C.EXTERNAL_CUSTOMER_ID = PO.CUSTOMER_CODE AND (C.FIRST_NAME||'' ''||C.LAST_NAME) LIKE '|| '''%' ||  pValue || '%'' ';
END IF;
IF pType='ItemName' THEN
ItemName := '''' || '%' || UPPER(ItemName) || '%' ||'''';
sWhere :=sWhere ||' AND UPPER(POLI.SKU) LIKE ' || ItemName ;
END IF;
IF pType='SKU' THEN
sWhere :=sWhere || ' AND POLI.SKU_ID IN (SELECT ITEM_ID FROM ITEM_CBO WHERE ITEM_BAR_CODE=''' || pValue ||''')';
END IF;
END LOOP;

FOR lRow IN cGetOrderDate LOOP
pType := lRow.ordDateType;
pValue := lRow.ordStartDate;
--sWhere := sWhere || ' AND PO.PURCHASE_ORDERS_DATE_DTTM >= TO_DATE(''' || pValue || ''', ''MM/DD/YYYY'')';
sWhere := sWhere || ' AND PO.PURCHASE_ORDERS_DATE_DTTM >= TO_DATE(''' || pValue || ''', ''MM/DD/YYYY'') AND PO.PURCHASE_ORDERS_DATE_DTTM <= TO_DATE(''' || pValue || ''', ''MM/DD/YYYY'')+1';
END LOOP;
--sOrderBy := ' ORDER BY PO.PURCHASE_ORDERS_ID';
sOrderBy := ' ORDER BY ROWNUM';


sSQL := 'SELECT * FROM ( SELECT COUNT(*) AS NO_OF_RECORDS FROM ('|| sSQL || sFROMSQL ||sWhere || sCR ||')) S,
(SELECT ROWNUM AS RX, T.*  FROM ('|| sSQL || sFROMSQL ||sWhere || sCR || sorderBy||') T ORDER BY RX ) WHERE RX >'|| piStartIndex||' AND RX <= '||piEndIndex;

/* Execute the SQL String */
OPEN p_cursor FOR sSQL;
RETURN (p_cursor);
CLOSE p_cursor;
END rtlGsCustOrderFilterXML;
END rtlGsCustOrderFilterXML;
/

create or replace
procedure RtlInPurchaseOrdersLineItem(
piPobId                 IN NUMBER,
piPobConRmaID           IN NUMBER,
piRMAStatID             IN NUMBER,
piItemId                IN NUMBER,
piReturnTypeId          IN NUMBER,
piItemQty               IN NUMBER,
piReturnReasonId        IN NUMBER,
piPobUsrID              IN NUMBER,
Piisreceivable          IN NUMBER,
psReceivingVarianceCode IN VARCHAR2,
psExpectedRcvngCondCode IN VARCHAR2,
piHasAlerts             IN Number,
piTcCompanyId           IN NUMBER,
piRetVal                OUT NUMBER
)
IS
/******************************************************************************
Procedure:   RtlInPurchaseOrdersLineItem
Description: Inserts into PURCHASE_ORDERS_LINE_ITEM

Return:
piRetVal = PK value of PURCHASE_ORDERS_LINE_ITEM
-1 = Record Not Inserted

Build  Create    Who  What
1      05/22/06  LB  Initial Code
2      06/24/10  LB  storing CO created date time as CO order date in POB_CON_RMA_ORD_ITEM
earlier it was referring PURCHASE_ORDERS_DATE_DTTM
3    07/28/10    GB   added FREIGHT_ALLOWANCE in POB_CON_RMA: CBO 2011 changes
4    10/27/10    DPS  As restoking fee is saved in POB_CON_RMA_ORD_ITEM_FEE table. commented the TOT_FEE value calculation.
5    11/17/10    GB   FACILITY_TYPE Return centre migration
6    11/23/10    GB   using RET_ACTION_ID instead of RET_ACTION
7    12/08/10    DPS  Included APPROVE_POB_RET_ACTION_ID in the Item updation statement. CR MACR00547348.
8    01/10/11    DPS  CBO5.0.15. Line no : 239, Storing iPobShipAddressID in POLI instead of CO.Facility.
9    02/21/11    GB  CBO5.0.18. populating DSG_SHIP_VIA from CO line to RO line as default
10   03/08/11    GB  CBO5.0.20. fix for : MACR00571062. populating  SHELF_DAYS  for RO
11   03/15/11    LB  getting the RO item ids from CO item id comparing to SHELF_DAYS column
(earlier updation of RO items never used to happen rather every time new line
item used to get inserted though the item is same)
12   06/28/11    DD   CBO5.3.2 inserting and updating the IS_RECEIVABLE and ON_HOLD in POB_CON_RMA_ORD_ITEM
and PURCHASE_ORDERS_LINE_ITEM table respectively.
13   06/30/11    DD   CBO5.3.2  inserting Tc_Po_Line_Id same as Purchase_Orders_Line_Item_Id
14   07/12/11    DD    CBO 5.3.5 inserting receiving variance code and expected receiving condition
in POB_CON_RMA_ORD_ITEM and removed ON_HOLD insertion.
15   07/29/11    DPS  CBO5.4.1 : Seting ALLOW_IN_RMA to 0 as that is no more in uesed.
16   08/05/11    DPS  CBO5.4.2 : Added 3 new column CANCELLED_QTY,QTY_UOM_ID,ALLOCATED_QTY with dummy value on DS Request.
17   08/17/11    DD   CBO 5.5.1 Intermediate: Updating RECEIVING_VARIANCE_CODE & EXPECTED_RECEIVING_CONDITION & HAS_ALERTS
18   09/26/11    DPS  CBO 5.6.1 Intermediate: Line247. Populating ORIG_ORDER_QTY for sales posting. Not used for RLM.
19   01/25/11    GB Added MARK_FOR_DELETION in FACILITY where clause
20   02/23/13    LB   Fixed CR MACR00814495, getting operational and not deleted facility as default return center
20   02/23/13    LB   Fixed CR MACR00814495, getting operational and not deleted facility as default return center  
21     02/Aug/2013 sbalasubramanyam JIRA Defect RLM-922 RMA lines are duplicated for a single CO line
Comments: PURCHASE_ORDERS_NOTE is not joined anymore, as it is not used by API based approach
to support web service based approach, it take much more effort
Reviewer: Ganapathi Bhaskar
*******************************************************************************/
ipurOrdLineID NUMBER(10);
aliasId varchar2(100);
facilityId NUMBER(10);
add1 varchar2(100);
add2 varchar2(100);
add3 varchar2(100);
city varchar2(50);
stateProv varchar2(10);
postalCode varchar2(15);
country varchar2(50);
countryCode varchar2(50);
phone varchar2(50);
email varchar2(100);
faxNo varchar2(50);
origRetItemAmt FLOAT;
orgPayMeth NUMBER(10);
iPobShipAddressID NUMBER(10);
iItemCount NUMBER;
iRmaItemId NUMBER;
icustomerOrderId NUMBER;
iReturnActionId NUMBER;
iGrandTotal NUMBER(13,4);
IRECEIVINGVARIANCECODE number;
I number;
iOrderTypeId NUMBER(8,0);


CURSOR cGetDefaultShipAddress IS
SELECT
FACILITY_ID
FROM
FACILITY PSA
WHERE
PSA.TC_COMPANY_ID = PIPOBID and
PSA.IS_OPERATIONAL = 1 AND
PSA.MARK_FOR_DELETION=0 AND
rtlisreturncenter(PSA.Facility_Type_Bits)=1;


CURSOR cGetItem IS
SELECT PO.PURCHASE_ORDERS_ID, PO.TC_PURCHASE_ORDERS_ID,PO.CREATED_DTTM,PO.GRAND_TOTAL,PO.TAX_CHARGES,
PO.BILL_TO_NAME,PO.BILL_TO_ADDRESS_1,PO.BILL_TO_ADDRESS_2,PO.BILL_TO_STATE_PROV,PO.BILL_TO_CITY,PO.BILL_TO_POSTAL_CODE,
PO.BILL_TO_COUNTRY_CODE, PO.BILL_TO_PHONE_NUMBER,PO.BILL_TO_FAX_NUMBER,PO.BILL_TO_EMAIL,PO.PAYMENT_MODE,
null,
POLI.PURCHASE_ORDERS_LINE_ITEM_ID,POLI.O_FACILITY_ALIAS_ID,POLI.SKU, IC.DESCRIPTION,
IC.PRODUCT_CLASS_ID AS PRODUCT_CLASS,POLI.PURCHASE_ORDERS_LINE_STATUS AS POB_ITEM_STAT_IDEN,
POLI.STD_BUNDL_QTY,POLI.ORDER_QTY,POLI.RETAIL_PRICE,POLI.MV_CURRENCY_CODE,POLI.UNIT_MONETARY_VALUE,
POLI.PICKUP_START_DTTM,POLI.UNIT_MONETARY_VALUE AS ACTUAL_COST,POLI.UNIT_TAX_AMOUNT,POLI.QTY_UOM_ID_BASE,POLI.STACK_LENGTH_VALUE,
POLI.STACK_WIDTH_VALUE, POLI.STACK_HEIGHT_VALUE,POLIRF.REF_FIELD1,POLIRF.REF_FIELD2,POLIRF.REF_FIELD3, POLIRF.REF_FIELD4,
POLIRF.REF_FIELD5,POLIRF.REF_FIELD6, IC.ITEM_BAR_CODE, POLI.SKU_ID,

POLI.D_NAME, POLI.D_LAST_NAME, POLI.TC_COMPANY_ID, POLI.D_ADDRESS_1, POLI.D_ADDRESS_2, POLI.D_COUNTRY_CODE, POLI.D_STATE_PROV,
POLI.D_CITY, POLI.D_POSTAL_CODE, POLI.D_PHONE_NUMBER, POLI.D_FAX_NUMBER, POLI.D_EMAIL,POLI.DSG_SHIP_VIA,
PO.D_NAME DNAME, PO.D_LAST_NAME DLAST_NAME, PO.D_ADDRESS_1 DADDRESS_1, PO.D_ADDRESS_2 DADDRESS_2, PO.D_COUNTRY_CODE DCOUNTRY_CODE,
PO.D_STATE_PROV DSTATE_PROV, PO.D_CITY DCITY, PO.D_POSTAL_CODE DPOSTAL_CODE, PO.D_PHONE_NUMBER DPHONE_NUMBER,
PO.D_FAX_NUMBER DFAX_NUMBER, PO.D_EMAIL DEMAIL

FROM PURCHASE_ORDERS PO, PURCHASE_ORDERS_LINE_ITEM POLI,
PURCHASE_ORDERS_LINE_STATUS PIS, PO_LINE_ITEM_REF_FIELDS POLIRF, ITEM_CBO IC

WHERE
PO.TC_COMPANY_ID=piPobId
AND POLI.PURCHASE_ORDERS_LINE_ITEM_ID=piItemId
AND PO.PURCHASE_ORDERS_ID = POLI.PURCHASE_ORDERS_ID
AND POLI.PURCHASE_ORDERS_LINE_STATUS = PIS.PURCHASE_ORDERS_LINE_STATUS(+)
AND POLI.PURCHASE_ORDERS_LINE_ITEM_ID=POLIRF.PURCHASE_ORDERS_LINE_ITEM_ID(+)
AND POLI.SKU_ID=IC.ITEM_ID;

CURSOR getOrgiPaymeth IS
SELECT PO.PAYMENT_MODE
FROM
PURCHASE_ORDERS PO,
PURCHASE_ORDERS_LINE_ITEM POLI,
POB_CON_RMA_ORD_ITEM PCROI
WHERE
POLI.PURCHASE_ORDERS_ID=piPobConRmaID AND
POLI.PURCHASE_ORDERS_LINE_ITEM_ID=PCROI.POB_CON_RMA_ORD_ITEM_ID AND
PCROI.ORD_NUM=PO.TC_PURCHASE_ORDERS_ID;

BEGIN
iReceivingVarianceCode := rtlGetVarianceCode(psReceivingVarianceCode,piTcCompanyId);
IF piReturnTypeId = 0 THEN
DELETE POB_CON_RMA_ORD_ITEM PCROI WHERE PCROI.POB_CON_RMA_ORD_ITEM_ID = piItemId;
DELETE PURCHASE_ORDERS_LINE_ITEM POLI WHERE POLI.PURCHASE_ORDERS_LINE_ITEM_ID = piItemId;
piRetVal := SQL%ROWCOUNT;
ELSE
/*Get the Pob_Ret_action_id for selected return action */
IF piReturnTypeId = 1 THEN -- 1=Credit, 2=Replace
SELECT PRA.POB_RET_ACTION_ID INTO iReturnActionId FROM POB_RET_ACTION PRA  WHERE PRA.RET_ACTION_ID = 10 AND PRA.POB_ID=piPobId;
ELSE
SELECT PRA.POB_RET_ACTION_ID INTO iReturnActionId FROM POB_RET_ACTION PRA  WHERE PRA.RET_ACTION_ID = 20 AND PRA.POB_ID=piPobId;
END IF;

/* Get Default Shipping address */
OPEN cGetDefaultShipAddress;
FETCH cGetDefaultShipAddress Into iPobShipAddressID;
CLOSE cGetDefaultShipAddress;

SELECT COUNT(*) INTO iItemCount FROM PURCHASE_ORDERS_LINE_ITEM POLI, POB_CON_RMA_ORD_ITEM PCROI
WHERE POLI.PURCHASE_ORDERS_LINE_ITEM_ID = piItemId and POLI.PURCHASE_ORDERS_LINE_ITEM_ID = PCROI.POB_CON_RMA_ORD_ITEM_ID;
/*
From Java code whenever u say update items the piItemId is nothing but the RO line item id.
but in pagination and other time is CO item id thats why the below if else block is been added
comments from Lingaraj
*/
IF iItemCount > 0 THEN
iRmaItemId := piItemId;
ELSE
SELECT COUNT(*) INTO iItemCount FROM PURCHASE_ORDERS_LINE_ITEM POLI, POB_CON_RMA_ORD_ITEM PCROI
WHERE POLI.PURCHASE_ORDERS_ID = piPobConRmaID AND POLI.SHELF_DAYS = piItemId AND
POLI.PURCHASE_ORDERS_LINE_ITEM_ID = PCROI.POB_CON_RMA_ORD_ITEM_ID;
IF iItemCount > 0 THEN
SELECT POLI.PURCHASE_ORDERS_LINE_ITEM_ID
INTO  iRmaItemId
FROM PURCHASE_ORDERS_LINE_ITEM POLI
WHERE POLI.PURCHASE_ORDERS_ID = piPobConRmaID
AND POLI.SHELF_DAYS = piItemId;
END IF;
END IF;

IF iItemCount > 0 THEN
Update Purchase_Orders_Line_Item Poli
Set Poli.Order_Qty = Piitemqty
WHERE POLI.PURCHASE_ORDERS_LINE_ITEM_ID = iRmaItemId;

UPDATE POB_CON_RMA_ORD_ITEM PCROI
SET PCROI.POB_RET_REAS_ID = piReturnReasonId,
PCROI.RET_ITEM_TYPE = piReturnTypeId,
PCROI.ORIG_RET_ITEM_QUAN = piItemQty,
PCROI.ORIG_RET_ITEM_AMT = NVL(PCROI.ITEM_SHIP_COST*piItemQty, 0),
PCROI.RET_ITEM_AMT = NVL(PCROI.ITEM_SHIP_COST*piItemQty, 0),
PCROI.APPROVE_POB_RET_ACTION_ID = iReturnActionId,
PCROI.IS_RECEIVABLE = piIsReceivable,
PCROI.RECEIVING_VARIANCE_CODE= iReceivingVarianceCode,
PCROI.EXPECTED_RECEIVING_CONDITION= psExpectedRcvngCondCode
WHERE PCROI.POB_CON_RMA_ORD_ITEM_ID = iRmaItemId;
piRetVal := SQL%ROWCOUNT;

UPDATE PURCHASE_ORDERS
SET
HAS_ALERTS=piHasAlerts
WHERE PURCHASE_ORDERS_ID=piPobConRmaID;

ELSE
FOR lRow IN cGetItem LOOP
origRetItemAmt :=NVL(lRow.ACTUAL_COST, 0)*piItemQty;
ICUSTOMERORDERID := LROW.PURCHASE_ORDERS_ID;
IGRANDTOTAL := LROW.GRAND_TOTAL;

SELECT
F.FACILITY_ID,
FA.FACILITY_ALIAS_ID,
F.ADDRESS_1,
F.ADDRESS_2,
F.ADDRESS_3,
F.CITY,
F.STATE_PROV,
F.POSTAL_CODE,
F.COUNTY,
F.COUNTRY_CODE,
FC.TELEPHONE_NUMBER,
FC.EMAIL_ADDRESS,
FC.FAX_NUMBER
INTO
facilityId,
aliasId,
add1,
add2,
add3,
city,
stateProv,
postalCode,
country,
countryCode,
phone,
email,
faxNo
FROM
FACILITY F,
FACILITY_ALIAS FA,
FACILITY_CONTACT FC
WHERE
F.FACILITY_ID=iPobShipAddressID
AND FC.CONTACT_TYPE(+)=0
AND FA.IS_PRIMARY=1
AND F.FACILITY_ID=FA.FACILITY_ID
AND FC.FACILITY_ID(+)=F.FACILITY_ID;

Select PO_LINE_ITEM_ID_SEQ.NEXTVAL into ipurOrdLineID from DUAL;
INSERT INTO PURCHASE_ORDERS_LINE_ITEM
(PURCHASE_ORDERS_LINE_ITEM_ID,
TC_PO_LINE_ID,
PURCHASE_ORDERS_ID,
TC_COMPANY_ID,
SKU,
DESCRIPTION,
PRODUCT_CLASS_ID,
PURCHASE_ORDERS_LINE_STATUS,
ORDER_QTY,
ORIG_ORDER_QTY, --Populated for sales posting requirement.
UNIT_MONETARY_VALUE,
MV_CURRENCY_CODE,
UNIT_TAX_AMOUNT,
QTY_UOM_ID_BASE,
SKU_ID,
IS_GIFT,
STACK_LENGTH_VALUE,
STACK_WIDTH_VALUE,
STACK_HEIGHT_VALUE,
D_FACILITY_ID,
D_FACILITY_ALIAS_ID,
D_ADDRESS_1,
D_ADDRESS_2,
D_ADDRESS_3,
D_CITY,
D_STATE_PROV,
D_POSTAL_CODE,
D_COUNTY,
D_COUNTRY_CODE,
D_PHONE_NUMBER,
D_EMAIL,
D_FAX_NUMBER,
DSG_SHIP_VIA,
SHELF_DAYS,
CREATED_DTTM,
CANCELLED_QTY,
QTY_UOM_ID,
ALLOCATED_QTY)
VALUES
(Ipurordlineid,
ipurOrdLineID,
piPobConRmaID,
piPobId,
lRow.SKU,
lRow.DESCRIPTION,
lRow.PRODUCT_CLASS,
lRow.POB_ITEM_STAT_IDEN,
piItemQty,
piItemQty,
lRow.UNIT_MONETARY_VALUE,
lRow.MV_CURRENCY_CODE,
lRow.UNIT_TAX_AMOUNT,
lRow.QTY_UOM_ID_BASE,
lRow.SKU_ID,
0,
lRow.STACK_LENGTH_VALUE,
lRow.STACK_WIDTH_VALUE,
lRow.STACK_HEIGHT_VALUE,
iPobShipAddressID,
aliasId,
add1,
add2,
add3,
city,
stateProv,
postalCode,
country,
countryCode,
phone,
email,
faxNo,
lRow.Dsg_Ship_Via,
lrow.Purchase_Orders_Line_Item_Id,
Sysdate,
0,
lRow.QTY_UOM_ID_BASE,
0);
piRetVal := SQL%ROWCOUNT;

INSERT INTO POB_CON_RMA_ORD_ITEM
(POB_CON_RMA_ORD_ITEM_ID,
UNIQUE_ORD_ITEM_IDEN,
BUNDLE,
BUNDLE_UNIQUE_ORD_ITEM_IDEN,
BUNDLE_POB_CON_RMA_ORD_ITEM_ID,
BUNDLE_UNIT_QUAN,
ITEM_TRANS,
ITEM_CATALOG_IDEN,
MANUFACTURER,
PART_NUM,
ITEM_SERIALIZED,
SERIAL_NUM,
POB_RET_REAS_ID,
RET_ITEM_TYPE,
ITEM_QUAN,
ORIG_RET_ITEM_QUAN,
ORIG_RET_ITEM_AMT,
RET_ITEM_AMT,
RETAIL_PRICE,
RETAIL_PRICE_FOR,
RETAIL_PRICE_UNIT_ID,
ITEM_PURCH_PRICE_FOR,
ITEM_SHIP_COST,
ITEM_COMMENT,
ITEM_TYPE,
ORD_NUM,
ORD_DATE,
ORD_LINE_NUM,
ORD_SHIP_FACILITY,
INVOICE_NUM,
INVOICE_DATE,
DATE_SHIPPED,
TOT_FEE,
PCT_OFF_RET,
ORIG_PCT_OFF_RET,
MISC1,
MISC2,
MISC3,
MISC4,
MISC5,
MISC6,
LINK_TO_PICTURE,
SHIP_COST_PAID_BY,
WARRANTY_STAT,
WARRANTY_CODE,
REPLACE_ITEM_ORD,
POB_DISPOSITION_ID,
POB_RESPONSIBLE_ID,
ORIG_NON_RETURNABLE,
ORIG_NON_EXCHANGEABLE,
ALLOW_IN_RMA,
REMOVED,
APPROVE_POB_RET_ACTION_ID,
RMA_STAT_ID,
CREDIT_ISSUED,
Pob_Usr_Id,
Is_Receivable,
RECEIVING_VARIANCE_CODE,
EXPECTED_RECEIVING_CONDITION)
VALUES
(ipurOrdLineID,
lRow.SKU_ID,
0,
null,
null,
null,
null,
null,
null,
null,
0,
null,
piReturnReasonId,
piReturnTypeId,
lRow.ORDER_QTY,
piItemQty,
origRetItemAmt,
NVL(origRetItemAmt,0),
lRow.RETAIL_PRICE,
null,
(SELECT C.CURRENCY_ID FROM CURRENCY C WHERE C.CURRENCY_CODE=lRow.MV_CURRENCY_CODE),
null,
lRow.ACTUAL_COST,
null,
null,
lRow.TC_PURCHASE_ORDERS_ID,
lRow.CREATED_DTTM,
lRow.PURCHASE_ORDERS_LINE_ITEM_ID,
null,
null,
null,
null,
null,
null,
null,
lRow.REF_FIELD1,
lRow.REF_FIELD2,
lRow.REF_FIELD3,
lRow.REF_FIELD4,
lRow.REF_FIELD5,
lRow.REF_FIELD6,
null,
2,
null,
'false',
null,
null,
null,
0,
0,
0,
0,
iReturnActionId,
NVL(piRMAStatID, 100),
0,
Pipobusrid,
Piisreceivable,
iReceivingVarianceCode,
psExpectedRcvngCondCode);
END LOOP;

UPDATE PURCHASE_ORDERS
SET
D_ADDRESS_1=add1,
D_ADDRESS_2=add2,
D_ADDRESS_3=add3,
D_CITY=city,
D_COUNTRY_CODE=countryCode,
D_COUNTY=country,
D_EMAIL=email,
D_FACILITY_ALIAS_ID=aliasId,
D_FACILITY_ID=facilityId,
D_FAX_NUMBER=faxNo,
D_PHONE_NUMBER=phone,
D_POSTAL_CODE=postalCode,
D_STATE_PROV=stateProv,
PARENT_PURCHASE_ORDERS_ID=icustomerOrderId,
HAS_ALERTS=piHasAlerts,
GRAND_TOTAL=NVL(iGrandTotal, 0)
WHERE PURCHASE_ORDERS_ID=piPobConRmaID;

OPEN getOrgiPaymeth;
FETCH getOrgiPaymeth Into orgPayMeth;
CLOSE getOrgiPaymeth;

UPDATE POB_CON_RMA SET ORIG_BILL_POB_PAY_METH_ID=orgPayMeth  WHERE POB_CON_RMA_ID=piPobConRmaID;
END IF;
END IF;
COMMIT;
End;
/
-- DBTicket RLM-678

MERGE INTO BUNDLE_META_DATA A
USING (SELECT 'DATAFLOW' AS BUNDLE_NAME FROM DUAL) B
ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT     (BUNDLE_META_DATA_ID,
BUNDLE_NAME,
DESCRIPTION,
DEF_SCREEN_TYPE_ID,
IS_ACTIVE,
IS_LABEL_BUNDLE,
DEF_BU_ID)
VALUES (SEQ_BUNDLE_META_DATA_ID.NEXTVAL,
'DATAFLOW',
'DATAFLOW',
10,
1,
1,
-1);


MERGE INTO BUNDLE_META_DATA A
USING (SELECT 'DS_ORD_NEXTGEN_UI' AS BUNDLE_NAME FROM DUAL) B
ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT     (BUNDLE_META_DATA_ID,
BUNDLE_NAME,
DESCRIPTION,
DEF_SCREEN_TYPE_ID,
IS_ACTIVE,
IS_LABEL_BUNDLE,
DEF_BU_ID)
VALUES (SEQ_BUNDLE_META_DATA_ID.NEXTVAL,
'DS_ORD_NEXTGEN_UI',
'Order screen next gen UI bundle name',
10,
1,
1,
-1);

COMMIT;	
-- DBTicket RLM-1707
update a_co_reason_code set description = 'Awaiting RMA confirmation' where reason_code = 'AwaitRMAConfirm';
update a_co_reason_code set description = 'Awaiting address details' where reason_code = 'AwaitAddrssDtl';
update a_co_reason_code set description = 'Awaiting address and payment details' where reason_code = 'AwaitAddrssNPymntDtl';
update a_co_reason_code set description = 'Awaiting item receipt' where reason_code = 'AwaitItemRcpt';
update a_co_reason_code set description = 'Awaiting payment details' where reason_code = 'AwaitPymntDtl';
update a_co_reason_code set description = 'Order needs user action' where reason_code = 'XONeedsUserAction';
COMMIT;

update db_build_history set end_dttm=sysdate where version_label='RLM_20130808.2013.1.18.08';

commit;



-- ************************************************ --

delete from MESSAGE_MASTER MM where MM.ILS_MODULE in ('dom','rlm')
/


/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '110406', '110406', 'dom', null, '{0} is  invalid in {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '282002', '282002', 'dom', null, '"Destination Facility" entered is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '282003', '282003', 'dom', null, '"Item" entered is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '282017', '282017', 'dom', null, 'The operator {0} is not supported for multiple values of the attribute {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '282018', '282018', 'dom', null, '"Logical operator" is invalid. It should be None if there is only one row.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '282019', '282019', 'dom', null, 'Select at least one Attribute for Order Selection.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2222227', '2222227', 'dom', null, 'At least one of the fields must be selected.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880001', '2880001', 'dom', null, 'Channel Id:{0} is invalid for Item Price with ID:{1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880002', '2880002', 'dom', null, 'Store Name:{0} is invalid for Item Price with ID:{1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880003', '2880003', 'dom', null, 'Item Name is required for Item Price with ID:{0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880004', '2880004', 'dom', null, 'Item Name:{0} is invalid for Item Price with ID:{1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880005', '2880005', 'dom', null, 'Item Price ID is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880006', '2880006', 'dom', null, 'Item Price ID :{0} exceeds maximum length:{1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880007', '2880007', 'dom', null, 'Cannot create an Item Price with ID :{0} since an Item Price with same ID already exists.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880008', '2880008', 'dom', null, 'Cannot update Item Price with ID :{0} since it does not exist.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880009', '2880009', 'dom', null, 'Price is required for Item Price with ID:{0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880010', '2880010', 'dom', null, 'Price :{0} is invalid for Item Price with ID:{1}.It should be a positive decimal.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880011', '2880011', 'dom', null, 'Price :{0} exceeds the maximum limit:{1} for Item Price with ID:{2}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880012', '2880012', 'dom', null, 'Rank is required for Item Price with ID:{0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880013', '2880013', 'dom', null, 'Rank:{0} is invalid for Item Price with ID:{1}.It should be a positive integer.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880014', '2880014', 'dom', null, 'Rank:{0} exceeds maximum length:{1} for Item Price with ID:{2}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880015', '2880015', 'dom', null, 'Currency was missing for for Item Price with ID:{0}.Default value:{1} is set.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880016', '2880016', 'dom', null, 'Currency:{0} not equal to Currency used by the company for Item Price with ID:{1}.Default value:{2} is set.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880017', '2880017', 'dom', null, 'Effective Start Date is required for Item Price with ID:{0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880018', '2880018', 'dom', null, 'Effective Start Date:{0} is invalid for Item Price with ID:{1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880019', '2880019', 'dom', null, 'Effective Start Date:{0} for Item Price with ID:{1} cannot be in past.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880020', '2880020', 'dom', null, 'Effective End Date is required for Item Price with ID:{0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880021', '2880021', 'dom', null, 'Effective End Date:{0} is invalid for Item Price with ID:{1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880022', '2880022', 'dom', null, 'Effective End Date:{0} for Item Price with ID:{1} cannot be in past.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880023', '2880023', 'dom', null, 'Effective End Date:{0} is before Effective Start Date:{1} for Item Price with ID:{2}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880024', '2880024', 'dom', null, 'Item Price with Rank:{0} already exists between the time period {1} to {2}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880025', '2880025', 'dom', null, 'Behavior After End Date was missing for Item Price with ID:{0}.Default value:{1} is set.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880026', '2880026', 'dom', null, 'Behavior After End Date :{0} is invalid for Item Price with ID:{1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880027', '2880027', 'dom', null, 'Item Price with same Item Id:{0} and Rank:{1} already present in the same message input.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880028', '2880028', 'dom', null, 'Item Name is required in Get Effective Price request message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880029', '2880029', 'dom', null, 'Item Name:{0} is invalid in Get Effective Price request message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880030', '2880030', 'dom', null, 'Channel Type is required in Get Effective Price request message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880031', '2880031', 'dom', null, 'Channel Type:{0} is invalid in Get Effective Price request message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880032', '2880032', 'dom', null, 'Effective On Date:{0} is invalid in Get Effective Price request message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880033', '2880033', 'dom', null, 'Store Name:{0} is invalid in Get Effective Price request message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880034', '2880034', 'dom', null, 'Header element not present in Item Price message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880035', '2880035', 'dom', null, 'Company Id not present in Item Price message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880036', '2880036', 'dom', null, 'Company Id:{0} is invalid in Item Price message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880037', '2880037', 'dom', null, 'Message element not present in Item Price message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880038', '2880038', 'dom', null, 'Action Type not present in Item Price message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880039', '2880039', 'dom', null, 'Action Type:{0} is not valid in Item Price message.Valid Action Types are {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880040', '2880040', 'dom', null, 'Item Price List element not present in Item Price message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880041', '2880041', 'dom', null, 'Item Price Element(s) not present in Item Price message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880042', '2880042', 'dom', null, 'Locale not present in Item Price message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880043', '2880043', 'dom', null, 'ItemPriceRequestData element not present in Item Price Message.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880044', '2880044', 'dom', null, 'Company ID is not specified in the userContext input.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880045', '2880045', 'dom', null, 'ItemPriceRequestData input is null.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880046', '2880046', 'dom', null, 'Item ID is required in ItemPriceRequestData input.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880047', '2880047', 'dom', null, 'Effective Item Price not present for the requested criteria.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880048', '2880048', 'dom', null, 'Source in Header element is too long.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880049', '2880049', 'dom', null, 'Currency is not configured at company level.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880050', '2880050', 'dom', null, 'Caught JDOMException while importing Item Prices:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880051', '2880051', 'dom', null, 'Caught IOException while importing Item Prices:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880052', '2880052', 'dom', null, 'Caught ItemPricingException while importing Item Prices:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880053', '2880053', 'dom', null, 'Item Price with same Item Price Id:{0} already present in the same message input.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880054', '2880054', 'dom', null, 'Channel Id:{0} exceeds maximum length :{1} for Item Price with ID:{2}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880055', '2880055', 'dom', null, 'Importing Item Price XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880056', '2880056', 'dom', null, 'Found errors:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880057', '2880057', 'dom', null, 'Found warnings:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880058', '2880058', 'dom', null, 'XML Document is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880059', '2880059', 'dom', null, 'Invalid Header Element:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2880061', '2880061', 'dom', null, 'Customer store "{0}" not found', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '7110301', '7110301', 'dom', null, 'Needs layer is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '7110302', '7110302', 'dom', null, 'Max Length Exceeded for {0}.Valid max length for {1} is {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '7110303', '7110303', 'dom', null, 'Duplicate Needs layer code', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '7110304', '7110304', 'dom', null, 'Duplicate Needs layer name', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '7110305', '7110305', 'dom', null, 'Only one need type can be associated with inventory watermark', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '7110306', '7110306', 'dom', null, 'Needs code does not exist', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '7110307', '7110307', 'dom', null, 'Needs code not on sales order', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '7110308', '7110308', 'dom', null, 'Invalid Needs Layer Code', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '7110309', '7110309', 'dom', null, 'Invalid Needs Layer', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '8008026', '8008026', 'dom', null, 'Order line :{0} was not released to the fulfillment location by its latest release by date. All allocations were deallocated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100001', '28100001', 'dom', null, 'Invalid integer value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100002', '28100002', 'dom', null, 'Invalid float value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100003', '28100003', 'dom', null, 'Invalid double value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100004', '28100004', 'dom', null, 'Invalid long value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100005', '28100005', 'dom', null, 'Invalid boolean value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100006', '28100006', 'dom', null, 'Invalid Timestamp value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100007', '28100007', 'dom', null, 'Invalid finite value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100008', '28100008', 'dom', null, 'Invalid Identity value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100009', '28100009', 'dom', null, 'Invalid localized constant value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100010', '28100010', 'dom', null, 'String value {0} is too long for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100011', '28100011', 'dom', null, 'Invalid string value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100012', '28100012', 'dom', null, 'Field Value {0} for field {1} cannot be negative.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100013', '28100013', 'dom', null, 'Unexpected error has occured please contact system administrator', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100014', '28100014', 'dom', null, 'Invalid {0} :{1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100015', '28100015', 'dom', null, 'No data found.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28100016', '28100016', 'dom', null, 'Invalid integer value {0} for field {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110001', '28110001', 'dom', null, 'Problem encountered while processing Order', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110002', '28110002', 'dom', null, 'Importing Customer Order XML. Caught JDOM Exception:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110003', '28110003', 'dom', null, 'Importing Customer Order XML. Caught IO Exception:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110004', '28110004', 'dom', null, 'XML Document is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110005', '28110005', 'dom', null, 'Message Element is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110006', '28110006', 'dom', null, 'Order Element in XML is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110007', '28110007', 'dom', null, 'Exception caught while processing order element No:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110008', '28110008', 'dom', null, 'Errors added for Order No: {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110009', '28110009', 'dom', null, 'Order cannot be imported without order lines.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110010', '28110010', 'dom', null, 'Order cannot be updated with the current status.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110011', '28110011', 'dom', null, 'Confirmed Order cannot be reserved.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110012', '28110012', 'dom', null, 'Invalid Header Element:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110013', '28110013', 'dom', null, 'Errors added for OrderLine Number:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110014', '28110014', 'dom', null, 'Version cannot be negative', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110015', '28110015', 'dom', null, 'Invalid version', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110016', '28110016', 'dom', null, 'Order cannot be updated in status {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110017', '28110017', 'dom', null, 'Order Line cannot be updated with line in status {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110018', '28110018', 'dom', null, 'Order Line cannot be canceled with line in status {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110019', '28110019', 'dom', null, 'Canceled order cannot be un-cancelled', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110020', '28110020', 'dom', null, 'Concurrent Exception caught while processing order element No:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110021', '28110021', 'dom', null, 'Canceled CustomerOrderLine cannot be un-canceled', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110022', '28110022', 'dom', null, 'Message Details Element {0} is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110023', '28110023', 'dom', null, 'Message Details Element {0} under {1} Element is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110024', '28110024', 'dom', null, 'Can''t find any Customer Orders matching with the criteria', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110025', '28110025', 'dom', null, 'Getting Customer Order Details XML. Found Errors:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110026', '28110026', 'dom', null, 'Getting Customer Order List XML. Found Errors:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110027', '28110027', 'dom', null, 'Errors added for Order:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110028', '28110028', 'dom', null, 'Completed Order {0} cannot be updated', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110029', '28110029', 'dom', null, 'Errors added for LPNs', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110030', '28110030', 'dom', null, 'Errors added for Distribution Order', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110031', '28110031', 'dom', null, 'Errors added for Distribution Order Line', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110032', '28110032', 'dom', null, 'Distribution Order Element in XML is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110033', '28110033', 'dom', null, 'Errors added for Invoice(s)', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110035', '28110035', 'dom', null, 'Invoice Element in XML is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110036', '28110036', 'dom', null, 'Order Number is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110037', '28110037', 'dom', null, 'Orders Element in XML is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110038', '28110038', 'dom', null, 'Multiple Orders Element present in XML. Cannot process XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110040', '28110040', 'dom', null, 'Modification category(s) ({0}) does not exist in the system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110041', '28110041', 'dom', null, 'Unable to retrieve order for the input {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110042', '28110042', 'dom', null, 'Invalid value for the field Child Order', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110201', '28110201', 'dom', null, 'Input value {0}-{1} has to be a positive number in the {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110202', '28110202', 'dom', null, '{0}-Too long Value is not allowed for {1} in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110203', '28110203', 'dom', null, '{0}-Value is too long in {1} in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110204', '28110204', 'dom', null, '{0}-is required field in {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110205', '28110205', 'dom', null, '{0}-{1} has invalid date in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110206', '28110206', 'dom', null, 'Time Zone invalid in {0}-{1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110207', '28110207', 'dom', null, '{0}-{1} has invalid format in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110208', '28110208', 'dom', null, '{0}-Invalid Value for {1} in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110209', '28110209', 'dom', null, '{0}-{1} has invalid number format in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110210', '28110210', 'dom', null, 'Parent Line Id {0}-{1} cannot be present for {2}-{3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110211', '28110211', 'dom', null, '{0}-Value is too long in {1} for {2} with Id {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110212', '28110212', 'dom', null, '{0}-is required field for {1} with Id {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110213', '28110213', 'dom', null, '{0}-{1} has invalid value for {2} with Id {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110214', '28110214', 'dom', null, '{0}-Note is invalid for {1} in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110215', '28110215', 'dom', null, '{0}-{1} is invalid in {2}-{3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110216', '28110216', 'dom', null, '{0} : Requested Delivery Date in {1}-{2} should be after current date', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110217', '28110217', 'dom', null, '{0} : Promised Delivery Date in {1}-{2} should be after current date', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110218', '28110218', 'dom', null, '{0} : Must Delivery Date in {1}-{2} should be after current date', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110219', '28110219', 'dom', null, 'Invalid input value {0} - {1} should be less than or equal to {2} for {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110221', '28110221', 'dom', null, 'Fulfillment facility and Ship to facility should match for the delivery option ''Pickup at store'' on order line- {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110222', '28110222', 'dom', null, 'Ship via "{0}" on order line- {1} should match the default ship via "{2}" for the delivery option ''Pickup at store', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110223', '28110223', 'dom', null, 'Item "{0}" does not support the delivery option "{1}"', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110224', '28110224', 'dom', null, 'Facility "{0}" does not support the delivery option "{1}"', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110225', '28110225', 'dom', null, 'Customer''s shipping address not available', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110226', '28110226', 'dom', null, '{0}  ({1}) of {2}  ({3}) must lie between {4} and {5}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110227', '28110227', 'dom', null, 'Atleast one InvoiceLine should be provided for Invoice: {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110228', '28110228', 'dom', null, '{0} of {1} is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110229', '28110229', 'dom', null, '{0} of {1} ({2}) is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110230', '28110230', 'dom', null, '{0} ({1}) of {2} must not exceed {3} characters.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110231', '28110231', 'dom', null, '{0} ({1}) of {2} ({3}) must not exceed {4} characters.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110232', '28110232', 'dom', null, '{0} ({1}) of {2} ({3}) must be of a valid number format.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110233', '28110233', 'dom', null, '{0} of {1} of {2} ({3}) is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110234', '28110234', 'dom', null, '{0} of {1} ({2}) of {3} ({4})  is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110235', '28110235', 'dom', null, '{0} ({1}) of {2}  of {4} ({5}) must not exceed {6} characters.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110236', '28110236', 'dom', null, '{0} ({1}) of {2} ({3}) of {4} ({5})  must not exceed {6} characters.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110237', '28110237', 'dom', null, '{0} ({1}) of {2} ({3}) of {4} ({5}) must lie between {6} and {7}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110238', '28110238', 'dom', null, '{0} ({1}) of {2} ({3}) of  {4} ({5}) must be of a valid number format.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110240', '28110240', 'dom', null, '{0} ({1}) / {2} is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110241', '28110241', 'dom', null, '{0} of {1} should be boolean value {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110242', '28110242', 'dom', null, '{0}:{1} negative value is not allowed in {2} {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110245', '28110245', 'dom', null, '{0} ({1}) is not a valid {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110246', '28110246', 'dom', null, '{0} ({1}) / {2} ({3}) must not exceed {4} characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110247', '28110247', 'dom', null, '{0} exceeded the maximum length supported by the system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110248', '28110248', 'dom', null, 'If {0} is {1} then {2} is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110249', '28110249', 'dom', null, '{0} ({1}) is of invalid {2} format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110250', '28110250', 'dom', null, 'Errors found in Cancel Order Lines webservice :', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110251', '28110251', 'dom', null, '{0} ({1}) must be a valid boolean value(0/1)', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110252', '28110252', 'dom', null, '{0} ({1}) is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110253', '28110253', 'dom', null, '{0} ({1}) of{2} ({3}) mudt be leass than or equal to {4}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110254', '28110254', 'dom', null, 'Duplicate InvoiceNumber ({0}) is not allowed in ExternalPaymentTransactionId ({1})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110255', '28110255', 'dom', null, 'Invoice Detail is not present for the ExternalPaymentTransactionId ({0}) \ InvoiceNumber ({1})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110401', '28110401', 'dom', null, 'Shipping Address Country Code is invalid in {0} with number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110402', '28110402', 'dom', null, 'Shipping Address State Province is invalid in {0} with number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110403', '28110403', 'dom', null, 'Shipping Address State is not found in {0} with number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110404', '28110404', 'dom', null, 'Shipping Address PostalCode is invalid in {0} with number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110405', '28110405', 'dom', null, 'Shipping Address PostalCode or State Province not found in {0} with number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110406', '28110406', 'dom', null, 'Charge Type {0} is not valid in charge detail with external id {1} in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110407', '28110407', 'dom', null, 'Charge Type cannot be ''Base Price'' in charge detail with external id {0} in {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110408', '28110408', 'dom', null, 'External Charge Id {0} is duplicate', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110409', '28110409', 'dom', null, 'External Payment Transaction Id {0} is duplicate', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110410', '28110410', 'dom', null, 'Invalid Transaction Type {0} in Payment Transaction with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110411', '28110411', 'dom', null, 'Invalid PaymentRecord Status {0} in Payment Transaction with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110412', '28110412', 'dom', null, 'Invalid Transaction Decision {0} Payment Transaction with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110413', '28110413', 'dom', null, 'Payment Transaction Type, {0} not supported in Payment Transaction with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110414', '28110414', 'dom', null, 'DOM does not support Authorization/Settlement and Transaction decision cannot be {0} in Payment Transaction with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110415', '28110415', 'dom', null, 'Payment Method {0} is invalid in Payment Detail with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110416', '28110416', 'dom', null, 'Payment Method {0} is not supported in Payment Detail with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110417', '28110417', 'dom', null, 'Card Type {0} is invalid in Payment Detail with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110418', '28110418', 'dom', null, 'Charge Sequence, {0} already exists Payment Detail with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110419', '28110419', 'dom', null, '{0} is required field for payment type-{1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110420', '28110420', 'dom', null, '{0}={1} is invalid in Payment Detail with external id {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110421', '28110421', 'dom', null, 'Payment Transaction with external transaction id {0} cannot be modified in payment detail with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110422', '28110422', 'dom', null, 'External Payment detail id {0} is duplicate.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110423', '28110423', 'dom', null, 'Payment detail with external id {0} cannot be modified for attribute {1} with new value "{2}", since Payment transaction status is success.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110424', '28110424', 'dom', null, 'Card Expiry Date "{0} /{1}" of Payment detail with external id {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110425', '28110425', 'dom', null, 'Card Expiry Month of Payment detail with external id {0} cannot be null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110426', '28110426', 'dom', null, 'Card Expiry Year of Payment detail with external id {0} cannot be null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110427', '28110427', 'dom', null, '{0}-{1} is not same as default code-{2} in Payment Detail with external id {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110428', '28110428', 'dom', null, 'In Payment detail with external id {2} requested Authorization amount "{0}" is less than total processed amount "{1}"', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110429', '28110429', 'dom', null, 'In Payment detail with external id {2}, requested Settlement amount "{0}" is less than total processed amount "{1}"', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110430', '28110430', 'dom', null, 'External Tax detail id {0} is duplicate', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110431', '28110431', 'dom', null, 'Tax Catery {0} is invalid in Tax Detail with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110432', '28110432', 'dom', null, 'Reason Code - {0} provided is invalid in {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110433', '28110433', 'dom', null, 'Inbound/Outbound region has not been configured for Ship to Facility in {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110434', '28110434', 'dom', null, 'Currency Code "{0}"  is not valid in {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110435', '28110435', 'dom', null, 'Currency Code "{0}"  does not match default code "{1}" in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110436', '28110436', 'dom', null, 'Item Id is not found in {0} with line number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110437', '28110437', 'dom', null, 'UOM Id is not found in {0} with line number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110438', '28110438', 'dom', null, 'Item does not support requested uom {0} in {1} with line number {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110439', '28110439', 'dom', null, 'ShipVia invalid in {0} with line number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110440', '28110440', 'dom', null, 'Order Line Number {0} is duplicate', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110441', '28110441', 'dom', null, 'Line Priority "{0}" provided in {1} - {2} does not exist in system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110442', '28110442', 'dom', null, 'Inventory Segment "{0}" provided in Order Line {1} does not exist in system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110443', '28110443', 'dom', null, 'Fulfillment Facility "{0}" provided in Order Line {1} does not exist in system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110444', '28110444', 'dom', null, '{1}- {0} in discount detail with external id {2} is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110445', '28110445', 'dom', null, '{1}- {0} is not found in {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110446', '28110446', 'dom', null, 'External Discount Detail Id {0} is duplicate', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110447', '28110447', 'dom', null, '{1}- {0} not valid in {2}-{3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110448', '28110448', 'dom', null, '{0}- {1} not included in {2}-{3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110449', '28110449', 'dom', null, '{1}- {0} not valid in {2}-{3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110450', '28110450', 'dom', null, 'Bill To Country Code invalid in {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110451', '28110451', 'dom', null, 'Bill To State Prov invalid in {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110452', '28110452', 'dom', null, 'Bill To Postal Code invalid in {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110453', '28110453', 'dom', null, 'Ship To Country Code invalid in {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110454', '28110454', 'dom', null, 'Ship To State Prov invalid in {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110455', '28110455', 'dom', null, 'Ship To Postal Code invalid in {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110456', '28110456', 'dom', null, 'Default Reason Code not defined for company', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110457', '28110457', 'dom', null, 'Reason Code is not applicable for Hold/Cancel', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110458', '28110458', 'dom', null, '{0} - {1} on {2} - {3} does not exist in system.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110459', '28110459', 'dom', null, 'Merge Facility "{0}" provided in Order Line- {1} does not exist in system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110460', '28110460', 'dom', null, '"Merge Facility" and "{0}" combination is not supported for the customer order line- {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110461', '28110461', 'dom', null, 'UOM Package is not found in item {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110462', '28110462', 'dom', null, 'Note type should be of category Communication for type - {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110463', '28110463', 'dom', null, 'Note type should be of category Instructions for type - {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110464', '28110464', 'dom', null, 'Account type {0} is not supported in Payment Detail with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110465', '28110465', 'dom', null, 'Payment Entry Type {0} is not supported in Payment Detail with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110466', '28110466', 'dom', null, 'Account type {0} is invalid in Payment Detail with external id {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110468', '28110468', 'dom', null, '{0} is required field for payment type-{1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110469', '28110469', 'dom', null, 'DriversLicenseCountry ({0}) and DriversLicenseState ({1}) combination in Payment Detail with external id  ({2}) is not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110470', '28110470', 'dom', null, 'Order Status should be SHIPPPED or CANCELLED for Order {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110471', '28110471', 'dom', null, 'Order Line Status should be SHIPPPED or CANCELLED for Order line {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110472', '28110472', 'dom', null, 'Atleast one order line should be in shipped status for Shipped order {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110473', '28110473', 'dom', null, 'Payment details are not valid for refund to be performed', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110474', '28110474', 'dom', null, '{0} - is a required field in {1} {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110475', '28110475', 'dom', null, 'The  discount status is not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110476', '28110476', 'dom', null, 'DO with id {0} is already present in the system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110477', '28110477', 'dom', null, 'Atleast one DO lineItem should be provided for DO: {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110479', '28110479', 'dom', null, '{0} ({1}) of {2} ({3}) of {4} ({5}) is not a valid item.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110480', '28110480', 'dom', null, '{0} ({1}) of {2} ({3}) of {4} ({5})  is not defined for the item {6}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110481', '28110481', 'dom', null, '{0} ({1}) of {2} ({3}) of {4} ({5}) is not a valid finite value.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110482', '28110482', 'dom', null, '{0} ({1}) of  {2} ({3})  is not a valid finite value.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110483', '28110483', 'dom', null, 'ExtDiscountId ({0})  is invalid for DiscountType ({1})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110484', '28110484', 'dom', null, 'DiscountValue  of ExtDiscountId ({0})  is required for Disocunt type({1})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110485', '28110485', 'dom', null, 'DiscountValue ({0}) of ExtDiscountId ({1}) for  entity ({2}) should lie between {3} and {4}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110486', '28110486', 'dom', null, 'DiscountValue ({0}) of ExtDiscountId ({1}) is invalid for DiscountType ({2})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110487', '28110487', 'dom', null, 'Total appeasements applied is equal to the Order Total', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110488', '28110488', 'dom', null, 'Total appeasements applied is equal to the Order Line Total', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110489', '28110489', 'dom', null, 'Customer store "{0}" not found', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110490', '28110490', 'dom', null, 'Entered location "{0}"  is an invalid customer store', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110491', '28110491', 'dom', null, '{0} ({1}) should not be provided for {2} ({3})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110492', '28110492', 'dom', null, 'Note Sequence, {0} already exists in Customer Order with Order Number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110493', '28110493', 'dom', null, 'Note Sequence, {0} already exists in Customer Order Line with Order Line Number {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110494', '28110494', 'dom', null, 'Order Number does not exist in system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110495', '28110495', 'dom', null, 'Line Number {0} does not exist in system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110496', '28110496', 'dom', null, 'Please check if the KeyStore Properties are set and verify the value for {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110497', '28110497', 'dom', null, 'Charge Override is not supported for ExtChargeDetailId({0})\ChargeCategory({1})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110801', '28110801', 'dom', null, 'Duplicate Note Type.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28110901', '28110901', 'dom', null, 'Duplicate Reason Code', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111001', '28111001', 'dom', null, 'Duplicate Appeasement', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111002', '28111002', 'dom', null, 'Appeasement Value is not a valid number', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111003', '28111003', 'dom', null, 'Appeasement cannot be applied as it is exceeding the order total.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111004', '28111004', 'dom', null, 'Appeasement value is exceeding the maximum allowable value', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111101', '28111101', 'dom', null, 'Name is required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111102', '28111102', 'dom', null, 'Name has exceeded max length', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111103', '28111103', 'dom', null, 'Description has exceeded max length', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111104', '28111104', 'dom', null, '{0} should be valid value', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111105', '28111105', 'dom', null, '{0} should be valid finite value', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28111106', '28111106', 'dom', null, 'Duplicate {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120001', '28120001', 'dom', null, 'Invalid Action Type {0} at Header Level.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120002', '28120002', 'dom', null, 'Invalid Action Type {0} at Message Level.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120003', '28120003', 'dom', null, 'Mandatory Attribute {0} is missing.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120004', '28120004', 'dom', null, 'Invalid Action Type {0} at Message level when Header Level Action Type is Create.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120005', '28120005', 'dom', null, 'Invalid Action Type {0} at Message level when Header Level Action Type is Update.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120006', '28120006', 'dom', null, 'Invalid value {0} for Quantity UOM.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120007', '28120007', 'dom', null, 'External Promotion Id {0} is not present in the system.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120008', '28120008', 'dom', null, 'Promotion Id {0} is not present in the system.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120009', '28120009', 'dom', null, 'Either External Promotion Id or Promotion Id is required when Action Type is Update or Delete.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120010', '28120010', 'dom', null, 'External Promotion Id {0} already present in the system for Company Id {1} when Action Type is Create.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120011', '28120011', 'dom', null, 'Promotion Type cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120012', '28120012', 'dom', null, 'Application Level cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120013', '28120013', 'dom', null, 'Currency Code is required if one or more of Min/Max Order Line Value or Min/Max Order Value are present.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120014', '28120014', 'dom', null, 'Quantity UOM is required if one or more of Min/Max Order Line Qty are present.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120015', '28120015', 'dom', null, 'If Application Level is Order Line, then one of Min/Max Order Line Qty or Min/Max Order Line Value is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120016', '28120016', 'dom', null, 'If Application Level is Order, then one of Min/Max Order Value is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120017', '28120017', 'dom', null, 'Invalid value {0} for Exclusive Condition.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120018', '28120018', 'dom', null, 'Quantity UOM cannot be updated because of Active Product Association with Promotion.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120019', '28120019', 'dom', null, 'Min Quantity cannot be grater than Max Quantity.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120020', '28120020', 'dom', null, 'Min Order Line Value cannot be greater than Max Order Line Value.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120021', '28120021', 'dom', null, 'Min Order Value cannot be greater than Max Order Value.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120022', '28120022', 'dom', null, 'Malformatted URL {0} .', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120023', '28120023', 'dom', null, 'Either Promoted Product Association Id or Promotion Scope Id is required when Action Type is Update or Delete.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120024', '28120024', 'dom', null, 'Promoted Product Association Id {0} is not present in the system.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120025', '28120025', 'dom', null, 'Promotion Scope Id {0} is not present in the system.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120026', '28120026', 'dom', null, 'Promoted product association Record already exists with Entity Type {0}, Entity Id {1}, Promo ID {2}, Promotion Scope Id {3} combination.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120027', '28120027', 'dom', null, 'Base UOM of the Item associated with Promotion should be same as that of the UOM specified in Promotion Detail.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120028', '28120028', 'dom', null, 'Promotion Id cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120029', '28120029', 'dom', null, 'Promoted Product Association Id cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120030', '28120030', 'dom', null, 'Promotion Scope Id cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120031', '28120031', 'dom', null, 'Promoted Entity Id cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120032', '28120032', 'dom', null, 'Promoted Entity Type cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120033', '28120033', 'dom', null, 'Promotion Start Time cannot be greater than Promotion End Time.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120034', '28120034', 'dom', null, 'Promotion Start Time can only be updated back till Current Time.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120035', '28120035', 'dom', null, 'Promotion End Time can only be updated back till Current Time or Start Time whichever is greater.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120036', '28120036', 'dom', null, 'Promotion Publish Time cannot be greater than Promotion Start Time.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120037', '28120037', 'dom', null, 'Entity Id in Promoted Product Association cannot be empty if Quantity UOM from Promotion Detail is not null.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120038', '28120038', 'dom', null, 'Offered Product Association already present in the system.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120039', '28120039', 'dom', null, 'Base UOM of the Offered Item should be same as that of the UOM specified in Offered Product Association.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120040', '28120040', 'dom', null, 'Either one of Min/Max Order Line Qty must be present, if Quantity UOM is present.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120041', '28120041', 'dom', null, 'Either one of Min/Max Order Line Value must be present, if Currency Code is present.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120042', '28120042', 'dom', null, 'Offered Product Association Id cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120043', '28120043', 'dom', null, 'Offered Entity Id cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120044', '28120044', 'dom', null, 'Offered Entity Type cannot be updated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120045', '28120045', 'dom', null, 'Invalid value {0} for Shipping Method.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120046', '28120046', 'dom', null, 'Either External Promotion Id or Promotion Id is required for Offered Product Association.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120047', '28120047', 'dom', null, 'Either Promoted Product Association Id or Promotion Scope Id is required for Offered Product Association.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120048', '28120048', 'dom', null, 'Either External Promotion Id or Promotion Id is required in Promoted Product Association XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120049', '28120049', 'dom', null, '{0} should not be present for Order Header level Promotions.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120050', '28120050', 'dom', null, 'Entity with Entity Name {0} is not Valid for the entity type.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120051', '28120051', 'dom', null, 'Offered Product Association does not exist.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120052', '28120052', 'dom', null, 'Shipping method should be present only if Promotion Type is Shipping Discount.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120053', '28120053', 'dom', null, 'Shipping method is required if Promotion Type is Shipping Discount.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120054', '28120054', 'dom', null, 'Entity Type {0} association is not supported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120055', '28120055', 'dom', null, 'Store with Store name {0} does not exist.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120056', '28120056', 'dom', null, 'Promotion Detail does not exist.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120057', '28120057', 'dom', null, 'Promoted Product Association does not exist.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120058', '28120058', 'dom', null, 'Promotion value can only be 100% if promotion type is Shipping Percent Discount.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120059', '28120059', 'dom', null, 'Promotion value cannot be greater than 100% if promotion type is Percent Discount or Shipping Percent Discount.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120060', '28120060', 'dom', null, 'Entity Type {0} association is not supported for Application Level {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120061', '28120061', 'dom', null, 'Promoted Product Entity and Offered Product Entity should match for BOGO Promotion.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120062', '28120062', 'dom', null, 'Promotion Type {0} not valid for Offered Product Association.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120063', '28120063', 'dom', null, 'Promotion Category specified is  not valid specified promotion type', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120064', '28120064', 'dom', null, 'Please remove duplicate discount code (s): {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28120065', '28120065', 'dom', null, '{0} is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130001', '28130001', 'dom', null, 'Order Created DTTM is required for order with order ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130002', '28130002', 'dom', null, 'Order Number is required for order with order ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130003', '28130003', 'dom', null, 'Order Modified Data required for order with order ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130004', '28130004', 'dom', null, 'Order ID required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130005', '28130005', 'dom', null, 'Order line Id is required for order ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130006', '28130006', 'dom', null, 'Order Line last modified date required on line ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130007', '28130007', 'dom', null, 'Order Line Creation Date required for order line ID{0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130008', '28130008', 'dom', null, 'Product required on line ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130009', '28130009', 'dom', null, 'Product class required on line ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130010', '28130010', 'dom', null, 'Postal Code required for line ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130011', '28130011', 'dom', null, 'Unit price is required for line ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130012', '28130012', 'dom', null, 'Quantity is required for line ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130013', '28130013', 'dom', null, 'UOM is required on line ID {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130014', '28130014', 'dom', null, 'Jurisdiction Name {1} with entity ID {1} has exceeded maximum length.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130015', '28130015', 'dom', null, 'Tax Name {1} with entity ID {0} has exceeded maximum length.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130016', '28130016', 'dom', null, 'Tax Amount with entity ID {0} is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130017', '28130017', 'dom', null, 'Tax Amount {1} with entity ID {0} has to be a positive number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130018', '28130018', 'dom', null, 'Tax Amount {1} with entity ID {0} has exceeded maximum length.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130019', '28130019', 'dom', null, 'Tax Rate with entity ID {0} is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130020', '28130020', 'dom', null, 'Tax Rate {1} with entity ID {0} has to be a positive number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130021', '28130021', 'dom', null, 'Tax Rate {1} with entity ID {0} has exceeded maximum length.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130022', '28130022', 'dom', null, 'Taxable Amount with entity ID {0} is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130023', '28130023', 'dom', null, 'Taxable Amount {1} with entity ID {0} has to be a positive number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130024', '28130024', 'dom', null, 'Taxable Amount {1} with entity ID {0} has exceeded maximum length.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130025', '28130025', 'dom', null, 'Tax Amount at Tax Spilt Up with entity ID {0} is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130026', '28130026', 'dom', null, 'Tax Amount at Tax Spilt Up {1} with entity ID {0} has to be a positive number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130027', '28130027', 'dom', null, 'Tax Amount at Tax Spilt Up {1} with entity ID {0} has exceeded maximum length.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130028', '28130028', 'dom', null, 'Tax Data LineID with entity ID {0} is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130029', '28130029', 'dom', null, 'Tax Data LineID {1} with entity ID {0} has to be a positive number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130030', '28130030', 'dom', null, 'Tax Data LineID {1} with entity ID {0} has exceeded maximum  length.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130031', '28130031', 'dom', null, 'Tax Amount at line with entity lineID {0} is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130032', '28130032', 'dom', null, 'Tax Amount at line {1} with entity lineID {0} has to be a positive number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130033', '28130033', 'dom', null, 'Tax Amount at line {1} with entity lineID {0} has exceeded maximum length.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130034', '28130034', 'dom', null, 'Tax DataLine with lineItemID {1} and entity ID {0} is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130035', '28130035', 'dom', null, 'Order Line with order lineID {1} and order ID {0} is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130036', '28130036', 'dom', null, 'Tax Spilt Up with entity ID {0} is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130037', '28130037', 'dom', null, 'All tax lines with entity ID {0} are not taxed.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130038', '28130038', 'dom', null, 'Tax Amount {1} with entity ID {0} has to be a number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130039', '28130039', 'dom', null, 'Tax Amount at line {1} with entity lineID {0} has to be a number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130040', '28130040', 'dom', null, 'Tax Amount at Tax Spilt Up {1} with entity ID {0} has to be a number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130041', '28130041', 'dom', null, 'Taxable Amount {1} with entity ID {0} has to be a number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130042', '28130042', 'dom', null, 'Tax Rate {1} with entity ID {0} has to be a number', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130043', '28130043', 'dom', null, 'Tax Data LineID {1} with entity ID {0} has to be a number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130044', '28130044', 'dom', null, 'At least one order line has to be present for the order with order id {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130045', '28130045', 'dom', null, 'TaxDataLine map is not populated in response translation for entity id {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130046', '28130046', 'dom', null, 'ChargeID with entity ID {0} is required.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130047', '28130047', 'dom', null, 'ChargeID {1} with entity ID {0} has to be a positive number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28130048', '28130048', 'dom', null, 'ChargeID {1} with entity ID {0} has exceeded maximum  length.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140001', '28140001', 'dom', null, 'Shipping Matrix Name is Required', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140002', '28140002', 'dom', null, 'Shipping Matrix Name should not exceed 20 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140003', '28140003', 'dom', null, 'Shipping Matrix Name Cannot Have Special Characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140004', '28140004', 'dom', null, 'Shipping Matrix Description Length Should not exceed 100 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140005', '28140005', 'dom', null, 'Shipping Matrix Comments Length Should not exceed 200 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140006', '28140006', 'dom', null, 'Range Start Cannot Be Null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140007', '28140007', 'dom', null, 'Range End Cannot Be Null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140008', '28140008', 'dom', null, 'Base Shipping Charges is Required', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140009', '28140009', 'dom', null, 'Base Shipping Charges Per Order not in Double Format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140010', '28140010', 'dom', null, 'Base Shipping Charges Per Order Line  not in Double Format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140011', '28140011', 'dom', null, 'Additional Charges Per Item  not in Double Format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140012', '28140012', 'dom', null, 'Additional Hazmat Charges not in Double Format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140013', '28140013', 'dom', null, 'Channel Type is  required in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140014', '28140014', 'dom', null, 'Shipping Rate Basis is  required in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140015', '28140015', 'dom', null, 'Ship Via Name is Required in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140016', '28140016', 'dom', null, 'Matrix Tc Id is a Required Field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140017', '28140017', 'dom', null, 'Is Delete Flag cannot be Y when XML is Create Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140018', '28140018', 'dom', null, 'Range UOM is required in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140019', '28140019', 'dom', null, 'Use Delivery Zone Parameter is Set to NO, Delivery Zone should be NULL', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140020', '28140020', 'dom', null, 'Delivery Zone Name is Required since Use Delivery Zone Parameter is set to True', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140021', '28140021', 'dom', null, 'Correct IsDelete Values are TRUE/FALSE only', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140030', '28140030', 'dom', null, 'Shipping Rule Name is Required', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140031', '28140031', 'dom', null, 'Shipping Rule Name  Length Should be Less Than 50', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140032', '28140032', 'dom', null, 'Shipping Rule Name Cannot Have Special Characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140033', '28140033', 'dom', null, 'Shipping Rule Description Length Should not exceed 100 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140034', '28140034', 'dom', null, 'Shipping rule record with ship via: {0} and item name: {1} combination already exists with Shipping Rule ID: {2}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140035', '28140035', 'dom', null, '{0}: {1} is invalid in the Shipping Rule XML with Rule ID: {2}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140036', '28140036', 'dom', null, '{0} is required in the Shipping Rule XML with Rule ID: {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140037', '28140037', 'dom', null, 'Rule Tc Id is a Required Field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140038', '28140038', 'dom', null, 'Is Delete Flag cannot be true when XML is Create Shipping Rule XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140039', '28140039', 'dom', null, '{0}: {1} attribute exceeds max length: {2} in the Shipping Rule XML with Rule ID: {3}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140060', '28140060', 'dom', null, 'Delivery Zone Name is Required', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140061', '28140061', 'dom', null, 'Delivery Zone Name  Length Should not exceed 50 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140062', '28140062', 'dom', null, 'Delivery Zone  Name Cannot Have Special Characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140063', '28140063', 'dom', null, 'Delivery Zone Description Length Should not exceed 200 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140064', '28140064', 'dom', null, 'Zone Tc Id is a Required Field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140065', '28140065', 'dom', null, 'Is Delete Flag cannot be Y when XML is Create Delivery Zone XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140066', '28140066', 'dom', null, 'For Delivery Zone, Origin country is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140067', '28140067', 'dom', null, 'For Delivery Zone, Destination Country is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140068', '28140068', 'dom', null, 'For Delivery Zone, Origin From Zip Code is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140069', '28140069', 'dom', null, 'For Delivery Zone, Origin To Zip Code is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140070', '28140070', 'dom', null, 'For Delivery Zone, Destination From Zip Code is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140071', '28140071', 'dom', null, 'For Delivery Zone, Destination To Zip Code is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140072', '28140072', 'dom', null, 'For Delivery Zone, Origin From Zip Code contains special characters.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140073', '28140073', 'dom', null, 'For Delivery Zone, Origin To Zip Code contains special characters.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140074', '28140074', 'dom', null, 'For Delivery Zone, Destination From Zip Code contains special characters.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140075', '28140075', 'dom', null, 'For Delivery Zone, Destination To Zip Code contains special characters.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140076', '28140076', 'dom', null, 'Origin From Zip code cannot exceed 10 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140077', '28140077', 'dom', null, 'Origin To Zip code cannot exceed 10 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140078', '28140078', 'dom', null, 'Destination From Zip code cannot exceed 10 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140079', '28140079', 'dom', null, 'Destination To Zip code cannot exceed 10 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140101', '28140101', 'dom', null, 'Range Start Should be Non Negative', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140102', '28140102', 'dom', null, 'Range End Should be Non Negative', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140103', '28140103', 'dom', null, 'Range Start should be less than Range End', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140104', '28140104', 'dom', null, 'Ranges should be in the Double Format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140105', '28140105', 'dom', null, 'Base Shipping Charges cannot be less than zero', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140106', '28140106', 'dom', null, 'Additional Charges Per Item cannot be less than zero', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140107', '28140107', 'dom', null, 'Base Shipping Charges Per Order Line cannot be less than zero', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140108', '28140108', 'dom', null, 'Additional Hazmat Charges cannot be less than zero', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140109', '28140109', 'dom', null, 'Channel Id is  Invalid in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140110', '28140110', 'dom', null, 'Shipping Rate Basis is Invalid in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140111', '28140111', 'dom', null, 'Ship Via Name is Invalid in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140112', '28140112', 'dom', null, 'Exception While Trying to Get Ship Via', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140113', '28140113', 'dom', null, 'Failed To get Currency Code from the Company Parameters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140114', '28140114', 'dom', null, 'Warning: Currency Code is  Required in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140115', '28140115', 'dom', null, 'Warning: Currency Code should be USD in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140116', '28140116', 'dom', null, 'Warning: Currency Code is  invalid in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140117', '28140117', 'dom', null, 'Range UOM is Invalid in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140118', '28140118', 'dom', null, 'For Shipping Rate Basis as  Order Value, Range UOM is not USD', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140119', '28140119', 'dom', null, 'For Shipping Rate Basis as  Order Value, Range UOM is Invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140120', '28140120', 'dom', null, 'Delivery Zone Name given is Invalid in the Shipping Matrix XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140121', '28140121', 'dom', null, 'Range Start should be in double format.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140122', '28140122', 'dom', null, 'Range End should be in double format.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140123', '28140123', 'dom', null, 'Exception While Trying to Get Matrix Name', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140124', '28140124', 'dom', null, 'Exception While trying to get Use Delivery Zone Parameter.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140125', '28140125', 'dom', null, 'Base Shipping Charge Per Order Data Length Exceeded', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140126', '28140126', 'dom', null, 'Base Shipping Charge Per Order Line Data Length Exceeded', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140127', '28140127', 'dom', null, 'Additional Charges Per Item Data Length Exceeded', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140128', '28140128', 'dom', null, 'Additional Hazmat Charges Data Length Exceeded', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140129', '28140129', 'dom', null, 'Range Start Data Length Exceeded', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140130', '28140130', 'dom', null, 'Range End Data Length Exceeded', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140131', '28140131', 'dom', null, 'Duplicate Shipping Rule Name Found', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140132', '28140132', 'dom', null, 'Channel Id is  Invalid in the Shipping Rule XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140133', '28140133', 'dom', null, 'Shipping Rate Basis is Invalid in the Shipping Rule XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140134', '28140134', 'dom', null, 'Ship Via Name is Invalid in the Shipping Rule XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140135', '28140135', 'dom', null, 'Item is Invalid in the Shipping Rule XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140136', '28140136', 'dom', null, 'Start Date should be After Current Date', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140137', '28140137', 'dom', null, 'End Date should be After Current Date', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140138', '28140138', 'dom', null, 'End Date Should be After Start Date', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140139', '28140139', 'dom', null, 'Start/End Dates Given are overlapping with Dates in the DataBase', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140141', '28140141', 'dom', null, 'Transportation Cost length exceeded.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140142', '28140142', 'dom', null, 'Transportation Cost should not be negative.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140143', '28140143', 'dom', null, 'Transportation Cost is not a valid number.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140144', '28140144', 'dom', null, 'Expiration date is in improper format.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140145', '28140145', 'dom', null, 'Expiration date should be after current date.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140146', '28140146', 'dom', null, 'Transportation Cost value exceeded.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140147', '28140147', 'dom', null, 'Entity Type: {0} is Invalid in the Shipping Rule XML with Rule ID: {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140160', '28140160', 'dom', null, 'Delivery Zone name is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140161', '28140161', 'dom', null, 'For Delivery Zone, Origin Country is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140162', '28140162', 'dom', null, 'For Delivery Zone, Destination Country is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140163', '28140163', 'dom', null, 'For Delivery Zone, Origin From Zip should be less than Origin To Zip.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140164', '28140164', 'dom', null, 'For Delivery Zone, Destination From Zip should be less than Destination To Zip.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140165', '28140165', 'dom', null, 'Exception While Trying to Get Delivery Zone', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28140166', '28140166', 'dom', null, 'Delivery zone ID length should not exceed 30 characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150001', '28150001', 'dom', null, 'Error Message Header', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150002', '28150002', 'dom', null, 'Caught JDOM Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150003', '28150003', 'dom', null, 'Caught IO Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150004', '28150004', 'dom', null, 'XML Document is null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150005', '28150005', 'dom', null, 'XML Message Element is null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150006', '28150006', 'dom', null, 'XML Request Element is Empty', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150007', '28150007', 'dom', null, 'Exception Caught While Processing Request Element', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150008', '28150008', 'dom', null, 'Invalid Header Element', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150009', '28150009', 'dom', null, 'Invalid Request Type', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150010', '28150010', 'dom', null, 'Caught Finite Value Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150011', '28150011', 'dom', null, 'Country not Present in State Request', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150012', '28150012', 'dom', null, 'UserID not Present in Facility Info Request', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150013', '28150013', 'dom', null, 'Caught Max Length Exceeded Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150014', '28150014', 'dom', null, 'Caught Exception While Fetching Facility List', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150015', '28150015', 'dom', null, 'Item Search Criteria not Present in Request', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150016', '28150016', 'dom', null, 'Caught Customer Order Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150017', '28150017', 'dom', null, 'Page Navigation Details not Present in Request', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150018', '28150018', 'dom', null, 'Sorting Criterion not Present in Request', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150019', '28150019', 'dom', null, 'Store Search Criteria not Present in Request', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150020', '28150020', 'dom', null, 'Caught Location Manager Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150021', '28150021', 'dom', null, 'Caught Parse Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150022', '28150022', 'dom', null, 'Caught Remote Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150023', '28150023', 'dom', null, 'Caught Promotion Service Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150024', '28150024', 'dom', null, 'Caught Snh Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150025', '28150025', 'dom', null, 'Caught Common Exception', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150026', '28150026', 'dom', null, 'Caught Exception While Fetching Company Parameters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28150027', '28150027', 'dom', null, 'Parameter Type not Present in Request', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160001', '28160001', 'dom', null, '{0} is Required', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160002', '28160002', 'dom', null, '{0} is Invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160003', '28160003', 'dom', null, '{0} has Exceeded its Length', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160004', '28160004', 'dom', null, '{0} has Special Characters which are not allowed', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160005', '28160005', 'dom', null, '{0} Finite Value is Not Found', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160006', '28160006', 'dom', null, '{0} has Special Characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160011', '28160011', 'dom', null, 'Payment Detail is Not complete', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160012', '28160012', 'dom', null, 'Primary Check on Credit Card Number Failed', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160013', '28160013', 'dom', null, 'Credit Card number is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160021', '28160021', 'dom', null, 'Response Order Number is Not Matching', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160022', '28160022', 'dom', null, 'Processed Amount is Negative', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160023', '28160023', 'dom', null, 'Processed Amount is in Format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160024', '28160024', 'dom', null, 'Transaction Decision Finite Value Code is not in Number Format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160031', '28160031', 'dom', null, 'Authorization Expiration Days should be Integer', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160033', '28160033', 'dom', null, '{0} cannot be changed as it has a child entity', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160034', '28160034', 'dom', null, '{0} ({1}) cannot be less than {2} ({3})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160035', '28160035', 'dom', null, '{0} ({1}) cannot be less than ({2}) used by {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160036', '28160036', 'dom', null, 'Transaction cannot be undeleted as it does not have an active parent', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160037', '28160037', 'dom', null, '{0} ({1}) cannot be changed for {2} transactions', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28160038', '28160038', 'dom', null, '{0} ({1}) cannot be more than {2} for {3} transactions', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170001', '28170001', 'dom', null, 'Customer master xml header not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170002', '28170002', 'dom', null, 'Customer master xml document is null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170003', '28170003', 'dom', null, 'Custmer master xml header version should not be negative', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170004', '28170004', 'dom', null, 'Customer master xml header version is not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170005', '28170005', 'dom', null, 'Customer master xml has invalid header element', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170006', '28170006', 'dom', null, 'Customer master xml has invalid message type element, it should be ''Customer Master', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170007', '28170007', 'dom', null, 'Customer master xml message element is null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170008', '28170008', 'dom', null, 'Customer master xmls CustomerList must have atleast one CustomerInfo', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170009', '28170009', 'dom', null, 'ExternalCustomerId is required', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170010', '28170010', 'dom', null, 'ExternalCustomerId ({0}) must not exceed {1} characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170011', '28170011', 'dom', null, '{0} of CustomerInfo ({1}) is required', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170012', '28170012', 'dom', null, '{0} ({1}) of CustomerInfo ({2}) must not exceed {3} characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170013', '28170013', 'dom', null, '{0} ({1}) of CustomerInfo ({2}) must {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170014', '28170014', 'dom', null, '{0} ({1}) of CustomerInfo ({2}) must lie between {3} and {4}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170015', '28170015', 'dom', null, 'PostalCode ({0}) for the City ({1}), State ({2}) and CountryCode ({3}) of CustomerInfo ({4}) must be valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170016', '28170016', 'dom', null, 'State ({0}) for CountryCode ({1}) of CustomerInfo ({2}) must be valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170017', '28170017', 'dom', null, '{0} for {1} of CustomerInfo ({2}) is required', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170018', '28170018', 'dom', null, '{0}({1})\{2}({3}) must not exceed {4} characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170019', '28170019', 'dom', null, '{0} ({1}) for {2} of CustomerInfo ({3}) must be {4}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170020', '28170020', 'dom', null, '{0} ({1}) for {2} of CustomerInfo ({3}) must lie between {4} and {5}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170021', '28170021', 'dom', null, '{0} of CustomerInfo ({1}) is required for the given payment method', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170022', '28170022', 'dom', null, '{0} ({1}) and {2} ({3}) of CustomerInfo ({4}) cannot be in past', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170023', '28170023', 'dom', null, 'ExternalCustomerId ({0}) must be unique across customers', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170024', '28170024', 'dom', null, '{0} ({1}) must be unique for CustomerInfo ({2})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170025', '28170025', 'dom', null, '{0} of CustomerInfo ({1}) is required if the payment method is  ({2})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170026', '28170026', 'dom', null, 'DriversLicenseCountry ({0}) and DriversLicenseState ({1}) combination of CustomerInfo ({2}) is not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170027', '28170027', 'dom', null, '{0} of CustomerInfo ({1}) is required if the payment method is eCheck', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170028', '28170028', 'dom', null, 'SaveAddressInfo ({0}) of CustomerInfo ({1}) must be True when SavePaymentInfo is ({2})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170029', '28170029', 'dom', null, '{0}({1})\{2}({3}) must not exceed {4} characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170030', '28170030', 'dom', null, '{0}({1})\{2}({3}) is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28170031', '28170031', 'dom', null, '{0} must have atleast one {1} for {2} ({3})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180001', '28180001', 'dom', null, 'Object type [{0}] requested is not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180002', '28180002', 'dom', null, 'View Type [{0}] is not valid for Object type {1}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180003', '28180003', 'dom', null, '{0} [{1}] exceeds {2} characters supported for Object type {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180004', '28180004', 'dom', null, 'Order number is required if Object type is CO and View type is Detail', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180005', '28180005', 'dom', null, 'Order number [{0}] requested is not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180006', '28180006', 'dom', null, 'Requested To date [{0}] has to be after the From date [{1}]', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180007', '28180007', 'dom', null, 'Requested To date [{0}] is in invalid format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180008', '28180008', 'dom', null, 'Requested From date [{0}] is in invalid format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180009', '28180009', 'dom', null, 'Both From date and To date must be provided for Created date', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180010', '28180010', 'dom', null, 'At least one search parameter should be provided when the Object type is CO and View type is List', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180011', '28180011', 'dom', null, 'User does not have the permission to view the Customer Order', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180012', '28180012', 'dom', null, 'Caught exception while validating the Request DTO', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180013', '28180013', 'dom', null, 'Caught exception while setting other request attributes', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180014', '28180014', 'dom', null, 'Caught exception while dispatching the URL', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180015', '28180015', 'dom', null, 'Order number is required if Object type is POS and View type is Detail', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180016', '28180016', 'dom', null, 'At least one search parameter should be provided when the Object type is POS and View type is List', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28180017', '28180017', 'dom', null, 'User does not have the permission to view the POS Transaction', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28200001', '28200001', 'dom', null, 'Item will be added with Web Price.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28200002', '28200002', 'dom', null, 'Order will be created with {0} price.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911101', '28911101', 'dom', null, 'Problem encountered while processing Transaction', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911102', '28911102', 'dom', null, 'Importing POS Transaction XML. Caught JDOM Exception:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911103', '28911103', 'dom', null, 'Importing POS Transaction XML. Caught IO Exception:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911104', '28911104', 'dom', null, 'XML Document is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911105', '28911105', 'dom', null, 'Message Element is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911106', '28911106', 'dom', null, 'Invalid Header Element:', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911107', '28911107', 'dom', null, 'Transactions Element in XML is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911108', '28911108', 'dom', null, 'Transaction Element in XML is null. Cannot process XML.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911109', '28911109', 'dom', null, 'Errors added in TransactionNumber ({0}):', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911110', '28911110', 'dom', null, 'Errors added in TransactionNumber ({0}) / TrxLineNumber ({1}):', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911111', '28911111', 'dom', null, 'Transaction cannot be imported without Transaction Details.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911201', '28911201', 'dom', null, '{0} is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911202', '28911202', 'dom', null, '{0} ({1}) must not exceed {2} characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911203', '28911203', 'dom', null, '{0} ({1}) should lie between {2} and {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911204', '28911204', 'dom', null, '{0} ({1}) must be a valid boolean value(true/false)', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911205', '28911205', 'dom', null, '{0} ({1}) is of invalid Date format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911206', '28911206', 'dom', null, '{0} ({1}) / {2} is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911207', '28911207', 'dom', null, '{0} ({1}) / {2} ({3}) must not exceed {4} characters', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911208', '28911208', 'dom', null, '{0} ({1}) / {2} ({3}) should lie between {4} and {5}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911209', '28911209', 'dom', null, '{0} ({1}) / {2} ({3}) must be a valid boolean value(true/false)', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911301', '28911301', 'dom', null, '{0} ({1}) is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911302', '28911302', 'dom', null, '{0} ({1}) / {2} ({3}) is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911303', '28911303', 'dom', null, 'Transaction with Transaction Number {0} already exists', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911401', '28911401', 'dom', null, '"Return Center" is a required field. Record {0} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911402', '28911402', 'dom', null, '{0} is not a valid "Return Center". Record with return center:{1} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911403', '28911403', 'dom', null, 'Length of the "Return Center" is greater than {0}. Record with return center name {1} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911404', '28911404', 'dom', null, '{0} is not a valid "Created Date". Record with return center: {1} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911405', '28911405', 'dom', null, 'Length of the "Return Reference Number" is greater than {0}. Record with return center:{1} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911406', '28911406', 'dom', null, 'Length of the "Return Reference Number" is greater than {0}. Record with return center:{1} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911407', '28911407', 'dom', null, '{0} is not a valid "Return Reference Number". Record with return center: {1} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911408', '28911408', 'dom', null, '{0} is not a valid "Return Tracking Number". Record with return center: {1} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911409', '28911409', 'dom', null, '{0} is not a valid "Last Updated Date". Record with return center: {1} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911410', '28911410', 'dom', null, '{0} is not a valid "Delivery Start Date". Record with return center: {1} could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911411', '28911411', 'dom', null, 'Either "Return Tracking#" or "Return Reference#" should be populated. Record with return center:  {0}  could not be imported.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911606', '28911606', 'dom', null, 'Please provide any one of the search criteria.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911611', '28911611', 'dom', null, 'Default return order type is not defined.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911612', '28911612', 'dom', null, 'Default return order type {0} is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911613', '28911613', 'dom', null, 'Default return center is not defined.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911614', '28911614', 'dom', null, 'Default return center {0} is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911615', '28911615', 'dom', null, 'Default return ship via is not defined.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911616', '28911616', 'dom', null, 'Default return ship via {0} is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911617', '28911617', 'dom', null, 'Remaining returnable quantity is zero.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911618', '28911618', 'dom', null, 'Number of packages value specified is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911619', '28911619', 'dom', null, 'Max number of packages allowed is 26', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911620', '28911620', 'dom', null, 'Number of packages value is empty', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911621', '28911621', 'dom', null, 'Number of packages must be zero', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911622', '28911622', 'dom', null, 'Return action {0} is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911623', '28911623', 'dom', null, 'Merchant Code {0} is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911624', '28911624', 'dom', null, 'Default merchant code is not defined', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911625', '28911625', 'dom', null, 'Refund S&H paid parameter is not specified', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911626', '28911626', 'dom', null, '{0} is invalid return order number', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911627', '28911627', 'dom', null, '{0} is invalid return order line id', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911628', '28911628', 'dom', null, 'Return order {0} is already canceled', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911629', '28911629', 'dom', null, 'Return order line id {0} is already canceled', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911630', '28911630', 'dom', null, 'Unable to retrieve return order {0}. Please contact system administrator', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911631', '28911631', 'dom', null, 'Receipt not expected approval parameter is specified', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911632', '28911632', 'dom', null, 'Note type {0} is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911633', '28911633', 'dom', null, 'Unable to validate return order notes', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911634', '28911634', 'dom', null, 'Unable to validate return order line notes', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911635', '28911635', 'dom', null, 'Invalid receipt not expected approval parameter', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911636', '28911636', 'dom', null, 'Custom attribute is not defined for ship via {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911637', '28911637', 'dom', null, 'Service code is not defined for ship via {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911638', '28911638', 'dom', null, 'Unable to retrieve info for ship via {0}. Please contact system administrator.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911639', '28911639', 'dom', null, 'Cannot add new line after confirmation of return order', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911640', '28911640', 'dom', null, 'User does not have permission to create return', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911641', '28911641', 'dom', null, 'User does not have permission to overrirde non-returnable item', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911642', '28911642', 'dom', null, 'User does not have permission to overrirde non-exchangeable item', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911643', '28911643', 'dom', null, 'User does not have permission to waive fees', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911644', '28911644', 'dom', null, 'User does not have permission to cancel return', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911645', '28911645', 'dom', null, 'User does not have permission to approve line item', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911646', '28911646', 'dom', null, 'User does not have permission to resend return label', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911647', '28911647', 'dom', null, 'User does not have permission to process return', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911648', '28911648', 'dom', null, 'User does not have permission to view notes', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911649', '28911649', 'dom', null, 'User does not have permission to view return status', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911650', '28911650', 'dom', null, 'Error while generating return order outbound message', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911651', '28911651', 'dom', null, 'Caught exception. Please contact system administrator', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911652', '28911652', 'dom', null, 'Exchange order lines are not in canceleable status', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911653', '28911653', 'dom', null, 'Customer order number {0} is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911654', '28911654', 'dom', null, 'Customer order number is empty', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170001', '30170001', 'dom', null, 'Communication xml header not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170002', '30170002', 'dom', null, 'Communication xml document is null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170003', '30170003', 'dom', null, 'Communication xml header version should not be negative', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170004', '30170004', 'dom', null, 'Communication xml header version is not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170005', '30170005', 'dom', null, 'Communication xml has invalid message type element, it should be CustomerCommunication', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170006', '30170006', 'dom', null, 'Communication xml header not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170007', '30170007', 'dom', null, 'Communication xml message element is null', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170008', '30170008', 'dom', null, 'Communication xmls CustomerCommunicationList must have atleast one CustomerCommunication', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170009', '30170009', 'dom', null, '{0} is required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170010', '30170010', 'dom', null, '{0} ({1}) must not exceed {2} characters for {3} ({4})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170011', '30170011', 'dom', null, '{0} ({1}) must be a valid {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170012', '30170012', 'dom', null, '{0} is required for {1} ({2})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170013', '30170013', 'dom', null, '{0} ({1}) must be a valid {2} for {3} ({4})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170014', '30170014', 'dom', null, '{0} in Communication XML has invalid entity type ({1})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170015', '30170015', 'dom', null, '{0} in Communication XML should be a valid finite value ({1})', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170016', '30170016', 'dom', null, 'If URL is not present in Communication XML, then From Address, To Address, Subject and Sent DTTM must be present', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '30170017', '30170017', 'dom', null, 'Atleast an Email or a Note is required for the given Order', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100100', '287100100', 'dom', null, '{0} : {1} is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100101', '287100101', 'dom', null, '{0} is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100102', '287100102', 'dom', null, 'Duplicate {0} found : {1} already exist.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100103', '287100103', 'dom', null, '{0} : {1} should be on or before {2} : {3}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100107', '287100107', 'dom', null, '{0} field cannot exceed {1} characters.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100110', '287100110', 'dom', null, '{0} : {1} cannot be before current date.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100111', '287100111', 'dom', null, '{0} cannot be in past.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100113', '287100113', 'dom', null, 'Duplicate {0} found.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100114', '287100114', 'dom', null, 'Rank should be within the range 0-99.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100115', '287100115', 'dom', null, '{0} is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100116', '287100116', 'dom', null, 'No change is available to save.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100117', '287100117', 'dom', null, 'The order line Attributes should be selected either from product class and quality level or from other attributes', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100118', '287100118', 'dom', null, 'Single shipment and single PO should be selected with valid rank at fulfillment strategy nbr. {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100123', '287100123', 'dom', null, 'Only one order line of type Variance should be selected', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100124', '287100124', 'dom', null, 'Order line of type Variance should be selected with Group or Match constraints', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100125', '287100125', 'dom', null, 'Complete Allocation should not have Finalizer type as Cancel Unfilled Quantity', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100126', '287100126', 'dom', null, 'Reapportionment Strategy should be blank with Group constraints', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100127', '287100127', 'dom', null, 'Reapportionment Strategy should be blank with Allocate Complete Single Source or Allocate Complete Multisource', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100128', '287100128', 'dom', null, 'Inventory Selection Strategy should be blank with no Group Type selected in Order Line Constraints', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100129', '287100129', 'dom', null, 'Inventory Selection Strategy should be Quality Level when order line attribute is Quality Level with type Variance', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100130', '287100130', 'dom', null, 'Inventory Selection Strategy should not be blank for SKU Attribute Constraints', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100131', '287100131', 'dom', null, 'Partial Allocation should not have Finalizer type Cancel Entire Order Line', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100132', '287100132', 'dom', null, 'Only Allocate Partial Multisource Strategy is allowed with Even Distribution', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100133', '287100133', 'dom', null, 'Multiple Strategies should not be selected with Even Distribution', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100134', '287100134', 'dom', null, 'Finalizer Strategy is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100135', '287100135', 'dom', null, 'Inventory Selection Strategy should be Max or Min for SKU Attribute Constraints', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100136', '287100136', 'dom', null, 'Reapportionment Strategy should be blank for tier based allocation', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100137', '287100137', 'dom', null, 'Even distribution is not allowed for tier based allocation', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100138', '287100138', 'dom', null, 'DC based substitution consumption is not valid for tier based allocation', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100139', '287100139', 'dom', null, 'No grouping is permitted for tier based allocation', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100140', '287100140', 'dom', null, 'Tier Allocation should not have Finalizer type as Procure Unfilled Quantity', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100141', '287100141', 'dom', null, 'Please configure Allocation Rules', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100150', '287100150', 'dom', null, 'Invalid Value for Distribution Strategy.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100151', '287100151', 'dom', null, 'Distribution Percentage should be less than or equals to 100% for segment {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100152', '287100152', 'dom', null, 'Invalid {0} for segment {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100153', '287100153', 'dom', null, '{0} should be an Integer Value for segment {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100154', '287100154', 'dom', null, '{0} should be a positive value for segment {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100155', '287100155', 'dom', null, '{0} is a required field for segment {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100156', '287100156', 'dom', null, 'Cumulative of Distribution Percentage on all the Inventory Segments should be less than or equal to 100%.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100157', '287100157', 'dom', null, 'Duplicate Inventory Segments cannot be defined for the Inventory Segmentation Template.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100158', '287100158', 'dom', null, 'Invalid Inventory Segment on row number {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100159', '287100159', 'dom', null, '{0} is a required field on row number {1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100160', '287100160', 'dom', null, 'Size Value is a required field on row no. {0} in Size Limits.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100161', '287100161', 'dom', null, '{0} should be an integer value on row no. {2} in size Limits. {1} is an invalid value.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100162', '287100162', 'dom', null, '{0} should be a positive value on row no. {2} in size Limits. {1} is an invalid value.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100163', '287100163', 'dom', null, '{1} is an invalid value for {0} on row no. {2}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100164', '287100164', 'dom', null, 'Distribution Order Type is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100165', '287100165', 'dom', null, 'A maximum of 3 size restrictions can be defined.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100166', '287100166', 'dom', null, '{0} should be a positive value.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100167', '287100167', 'dom', null, '{0} is invalid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100168', '287100168', 'dom', null, 'Size UOM is a required field on row no. {0} in size limits.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100169', '287100169', 'dom', null, 'Unique Size UOM is a required field on row no. {0} in size limits.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100170', '287100170', 'dom', null, 'At least on of the release parameters should be selected.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100171', '287100171', 'dom', null, '{0} : {1} is an invalid entry. Possible value can be between 1 and 23.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100172', '287100172', 'dom', null, '{0} is a required field when value for {1} has been given.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100173', '287100173', 'dom', null, 'Only one Template can be marked as default at a time. Template {0} is already marked as default.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100175', '287100175', 'dom', null, 'Facility is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100176', '287100176', 'dom', null, 'Supply type is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100177', '287100177', 'dom', null, 'Invalid facility entered.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100178', '287100178', 'dom', null, 'Level two alert inventory should be greater than level one alert inventory.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100179', '287100179', 'dom', null, 'Either product class or item is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100180', '287100180', 'dom', null, 'Facility {0} does not exist', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100181', '287100181', 'dom', null, 'Facility and merge facility cannot be same.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100182', '287100182', 'dom', null, 'Merge facility {0} should be part of the tier', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100183', '287100183', 'dom', null, 'Facility removed - {0} - should not be a merge facility for any facility in the tier.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100184', '287100184', 'dom', null, 'Reapportionment Strategy should be blank with Allocate order line across all supply types', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100185', '287100185', 'dom', null, 'Multiple filters cannot be associated to this template', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100400', '287100400', 'dom', null, 'Item name is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100401', '287100401', 'dom', null, 'Facility is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100402', '287100402', 'dom', null, 'Supply type is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100403', '287100403', 'dom', null, 'Item name exceeds max length of {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100404', '287100404', 'dom', null, 'Facility name exceeds max length of {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100405', '287100405', 'dom', null, 'Supply type exceeds max length of {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100406', '287100406', 'dom', null, '{0} is not a valid Item name', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100407', '287100407', 'dom', null, '{0} is not a valid Facility name', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100408', '287100408', 'dom', null, '{0} is not a valid Supply type', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100409', '287100409', 'dom', null, 'Invalid date entered', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100410', '287100410', 'dom', null, 'From date:{0} is in invalid format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100411', '287100411', 'dom', null, 'To date:{0} is in invalid format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100412', '287100412', 'dom', null, 'No of ATP Line Items exceeds allowable limit of {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100414', '287100414', 'dom', null, '{0} invalid consumption days', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100415', '287100415', 'dom', null, 'Requested delivery date:{0} is in invalid format', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100416', '287100416', 'dom', null, 'Requested delivery date:{0} is in past', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100417', '287100417', 'dom', null, 'Duplicate Item name {0} in search lines', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100418', '287100418', 'dom', null, 'No ATP Lines added', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100419', '287100419', 'dom', null, 'From date is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100420', '287100420', 'dom', null, 'Future supply days is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100421', '287100421', 'dom', null, 'Segment {0} does not exist.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100422', '287100422', 'dom', null, 'Only one supply balance search line item with items specified in SQL is allowed.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100501', '287100501', 'dom', null, 'Please enter a valid facility', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100502', '287100502', 'dom', null, 'Please select a facility', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100503', '287100503', 'dom', null, 'Error while tryin to fetch run summary list- Message: {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100504', '287100504', 'dom', null, 'Error while trying to get header data- Message: {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100505', '287100505', 'dom', null, 'Error while trying to get configuration template- Message: {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100506', '287100506', 'dom', null, 'Error while trying to save scheduler- Message: {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100507', '287100507', 'dom', null, 'Error while trying to save configuration template- Message: {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100508', '287100508', 'dom', null, 'Please create a configuration template', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287100509', '287100509', 'dom', null, 'Configuration template failed to create', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110200', '287110200', 'dom', null, '"Rule name" already exists. Please change the rule name.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110201', '287110201', 'dom', null, '"Logical operator" is invalid. None is allowed for last row only.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110202', '287110202', 'dom', null, '"Logical operator" for the last row should be none.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110203', '287110203', 'dom', null, '"Attribute" is a required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110204', '287110204', 'dom', null, '"Operator":{0} is not supported for multiple values for the "Attribute":{1}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110205', '287110205', 'dom', null, '"Operator":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110206', '287110206', 'dom', null, '"{0}":{1} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110207', '287110207', 'dom', null, '"Item":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110208', '287110208', 'dom', null, '"Order Type":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110209', '287110209', 'dom', null, '"Destination State":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110210', '287110210', 'dom', null, '"Customer ID":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110211', '287110211', 'dom', null, '"Carrier Code":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110212', '287110212', 'dom', null, '"Mode":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110213', '287110213', 'dom', null, '"Service Level":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110214', '287110214', 'dom', null, '"Product Class":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110215', '287110215', 'dom', null, '"Store Type":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110216', '287110216', 'dom', null, '"Never Out" condition:{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110217', '287110217', 'dom', null, 'At least one attribute should be added in "Order Selection".', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110218', '287110218', 'dom', null, '"Rank":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110219', '287110219', 'dom', null, 'Duplicate combination of "Facility" and "Segment":[{0}] is not allowed.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110220', '287110220', 'dom', null, 'Duplicate "Rank" not allowed in "Source From" section.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110221', '287110221', 'dom', null, 'Duplicate combination of "Tier" and "Segment":[{0}] is not allowed.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110222', '287110222', 'dom', null, 'Band Definitions are not defined in row {0}.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110223', '287110223', 'dom', null, '"Destination Country Code":{0} is not valid.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110301', '287110301', 'dom', null, 'Needs layer is a required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110302', '287110302', 'dom', null, 'Max Length Exceeded for {0}.Valid max length for {1} is {2}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110303', '287110303', 'dom', null, 'Duplicate Needs layer code', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110304', '287110304', 'dom', null, 'Duplicate Needs layer name', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110305', '287110305', 'dom', null, 'Only one need type can be associated with inventory watermark', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110306', '287110306', 'dom', null, 'Needs code does not exist', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110307', '287110307', 'dom', null, 'Needs code not on sales order', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110308', '287110308', 'dom', null, 'Invalid Needs Layer Code', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110309', '287110309', 'dom', null, 'Invalid Needs Layer', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110310', '287110310', 'dom', null, 'Percentage value cannot be greater than 100', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287110311', '287110311', 'dom', null, 'Percentage value cannot be specified in decimals', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287800001', '287800001', 'dom', null, 'Company ID is required in the Header Information while importing XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287800002', '287800002', 'dom', null, 'Company ID is Invalid in the Header Information while importing XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287800003', '287800003', 'dom', null, 'Company ID does not exist in the system', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287800004', '287800004', 'dom', null, 'Generic Exception caught while trying to validate Heaeder Information in the XML', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287800005', '287800005', 'dom', null, 'Action type is required field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287800006', '287800006', 'dom', null, 'Action type is not valid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287800007', '287800007', 'dom', null, '{0} {1} is invalid', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287810001', '287810001', 'dom', null, '{0} {1} is not a valid integer', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287810002', '287810002', 'dom', null, '{0} is the required field.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287810003', '287810003', 'dom', null, '(Current Page Number * Rows Per Page) cannot be greater than the Total No of Records', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287810004', '287810004', 'dom', null, '{0} is not a valid sorting field', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287810005', '287810005', 'dom', null, '{0} should be either ASC or DESC', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287810006', '287810006', 'dom', null, 'The customer order with order number : {0} does not exist', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '287820001', '287820001', 'dom', null, 'The customer information was saved successfully', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '288008026', '288008026', 'dom', null, 'Order line :{0} was not released to the fulfillment location by its latest release by date. All allocations were deallocated.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '288008029', '288008029', 'dom', null, 'Order line :{0} was deallocated because the ETA of supply type was beyond the allowed past due date.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '2814028140', '2814028140', 'dom', null, 'Exception While Trying to Get Rule Name', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911655', '28911655', 'dom', null, 'Return order number is empty', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911656', '28911656', 'dom', null, 'Please provide expected item condition', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911657', '28911657', 'dom', null, 'Please provide reason for returns', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911658', '28911658', 'dom', null, 'Order {0} is not in a returnable status.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911659', '28911659', 'dom', null, 'Return order line number {0} is invalid for cancelation.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911660', '28911660', 'dom', null, 'Return order line number {0} is invalid or Return Order is not in a updateable status.', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911661', '28911661', 'dom', null, 'No items available to return or exchange for order {0}', 
'ErrorMessage', null, null, null, null)
/
INSERT INTO MESSAGE_MASTER(MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG , BUNDLE_NAME , MSG_CLASS , MSG_TYPE , OVRIDE_ROLE , LOG_FLAG)  
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '28911662', '28911662', 'dom', null, 'Exchange Order type not defined.', 
'ErrorMessage', null, null, null, null);

COMMIT;

exit;

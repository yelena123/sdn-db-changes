MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-2277' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-2320' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'RLM-909' as CR_NUMBER,
  'RLMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-4611' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-4596' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-2360' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-4624' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-7022' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-7024' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-7095' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'RLM-1224' as CR_NUMBER,
  'RLMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'RLM-958' as CR_NUMBER,
  'RLMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'MACR00850866' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'MACR00781559' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'MACR00840743' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-1520' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-1368' as CR_NUMBER,
  'DOMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'RLM-493' as CR_NUMBER,
  'RLMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'RLM-451' as CR_NUMBER,
  'RLMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'RLM-620' as CR_NUMBER,
  'RLMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

COMMIT;


exit;

update db_build_history set version_label = 'DOMBASE_20130710.2013.18.06' where version_label = 'DOMBASE_20130704.2013.18.06';
COMMIT;

update db_build_history set version_label = 'DOMBASE_20130822.2013.18.08_3' where version_label = 'DOMBASE_20130808.2013.18.08_3';
COMMIT;

update db_build_history set version_label = 'DOMBASE_20131011.2013.1.18.09' where version_label = 'DOMBASE_20131010.2013.1.18.09';
COMMIT;

update db_build_history set version_label = 'DOM_20130813.2013.18.08' where version_label = 'DOM_20130808.2013.18.08';
COMMIT;

update db_build_history set version_label = 'DOM_20130822.2013.1.18.08_3' where version_label = 'DOM_20130808.2013.1.18.08_3';
COMMIT;

update db_build_history set version_label = 'DOM_20131011.2013.1.18.09' where version_label = 'DOM_20131010.2013.1.18.09';
COMMIT;

Update DB_BUILD_HISTORY set VERSION_LABEL='DOM_20130813.2013.18.08' where VERSION_LABEL='20130808.2013.18.08' and VERSION_RANGE like 'DOM%';
COMMIT;

Update DB_BUILD_HISTORY set VERSION_LABEL='DOM_20131010.2013.18.09' where VERSION_LABEL='20131010.2013.18.09' and VERSION_RANGE like 'DOM%';
COMMIT!

delete from db_build_history where version_label='DOM_20131402.21.00';

delete from db_build_history where version_label='DOM_20131004.22.00';

delete from db_build_history where version_label like 'DOM_BBB_INC01';

delete from db_build_history where version_label like 'DOM419';

delete from db_build_history where version_label like 'CBOBASE_WIP.2014.01.00';

delete from db_build_history where version_label like 'SCPPBASE_WIP.2014.01.00';

update db_build_history set version_label = 'DOM_20140108.2013.1.18.10' where version_label = 'DOM_20131206.2013.1.18.10';
update db_build_history set version_label = 'DOMBASE_20140108.2013.1.18.10' where version_label = 'DOMBASE_20131206.2013.1.18.10';

COMMIT;

exit;

SET DEFINE OFF;
SET ECHO ON;

INSERT INTO DB_BUILD_HISTORY
(VERSION_LABEL, START_DTTM, END_DTTM, VERSION_RANGE, APP_BLD_NO)
VALUES 
('EEMBASE_20130808.2013.1.18.08',SYSDATE,SYSDATE,'EEM DB INCREMENTAL BUILD - 18.8 FOR 2013','EEMBASE_20130808.2013.18.08');

COMMIT;

update app set  app_name= 'Store Commerce Activation',  APP_SHORT_NAME='SCA' where  APP_SHORT_NAME='SIF';


-- DBTicket MACR00862915

DELETE FROM PERMISSION_INHERITANCE WHERE  PARENT_PERMISSION_ID=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME LIKE 'Store Manager');
DELETE FROM PERMISSION_INHERITANCE WHERE  PARENT_PERMISSION_ID=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME LIKE 'Store Associate');
DELETE FROM PERMISSION_INHERITANCE WHERE  PARENT_PERMISSION_ID=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME LIKE 'Regional Manager');

commit;


UPDATE DB_BUILD_HISTORY SET END_DTTM=SYSDATE WHERE VERSION_LABEL='EEMBASE_20130808.2013.1.18.08';

COMMIT;










INSERT INTO DB_BUILD_HISTORY
(VERSION_LABEL, START_DTTM, END_DTTM, VERSION_RANGE, APP_BLD_NO)
VALUES 
('EEMBASE_20130808.2013.1.18.08.02',SYSDATE,SYSDATE,'EEM DB INCREMENTAL BUILD - 18.8 FOR 2013','EEMBASE_20130808.2013.18.08.02');

COMMIT;
EXEC SEQUPDT ('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
EXEC SEQUPDT ('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
EXEC SEQUPDT ('COMPANY_APP','ROW_UID','SEQ_COAPP_ROWUID');

-- DBTicket SIF-455

-- PICK_SORT_LIST - Mobile Order Picking - Sort
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES(SEQ_PERMISSION_ID.NEXTVAL,'Mobile Order Picking - Sort',1,'PICK_SORT_LIST');                                                                                                                                               

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_SORT_LIST'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_SORT_LIST'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_SORT_LIST'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

--PICK_FLTR_LIST - Mobile Order Picking - Filter
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Picking - Filter',1,'PICK_FLTR_LIST');                                                                                                                                                   

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_FLTR_LIST'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_FLTR_LIST'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_FLTR_LIST'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

--PICK_PRINT -	Mobile Order Picking - Print
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Picking - Print',1,'PICK_PRINT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_PRINT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_PRINT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_PRINT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

--PICK_SHRT_ALL	- Mobile Order Picking - Short All Pending
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Picking - Short All Pending',1,'PICK_SHRT_ALL');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_SHRT_ALL'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_SHRT_ALL'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_SHRT_ALL'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

--PICK_HIDE_CMPL -	Mobile Order Picking - Hide Complete
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Picking - Hide Complete',1,'PICK_HIDE_CMPL');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_HIDE_CMPL'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_HIDE_CMPL'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_HIDE_CMPL'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

--PICK_COLL_CAT -	Mobile Order Picking - Collapse Categories
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Picking - Collapse Categories',1,'PICK_COLL_CAT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_COLL_CAT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_COLL_CAT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_COLL_CAT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

--PICK_ADD_MAN -	Mobile Order Picking - Add Manually
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Picking - Add Manually',1,'PICK_ADD_MAN');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_ADD_MAN'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_ADD_MAN'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_ADD_MAN'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- PICK_CAM_SCAN -	Mobile Order Picking - Camera Scan
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Picking - Camera Scan',1,'PICK_CAM_SCAN');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_CAM_SCAN'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_CAM_SCAN'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_CAM_SCAN'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);


-- PACK_SORT_LIST	- Mobile Order Packing - Sort
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Packing - Sort',1,'PACK_SORT_LIST');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PACK_SORT_LIST'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PACK_SORT_LIST'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PACK_SORT_LIST'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- PACK_FLTR_LIST	- Mobile Order Packing - Filter
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Packing - Filter',1,'PACK_FLTR_LIST');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PACK_FLTR_LIST'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PACK_FLTR_LIST'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PACK_FLTR_LIST'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- PACK_PRINT	- Mobile Order Packing - Print
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Packing - Print',1,'PACK_PRINT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PACK_PRINT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PACK_PRINT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PACK_PRINT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- PACK_SHRT_ALL -	Mobile Order Packing - Short All Pending
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Packing - Short All Pending',1,'PACK_SHRT_ALL');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PACK_SHRT_ALL'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PACK_SHRT_ALL'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PACK_SHRT_ALL'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- PACK_HIDE_CMPL	- Mobile Order Packing - Hide Complete
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Packing - Hide Complete',1,'PACK_HIDE_CMPL');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PACK_HIDE_CMPL'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PACK_HIDE_CMPL'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PACK_HIDE_CMPL'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- PACK_COLL_CAT -	Mobile Order Packing - Collapse Categories
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Packing - Collapse Categories',1,'PACK_COLL_CAT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PACK_COLL_CAT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PACK_COLL_CAT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PACK_COLL_CAT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- PACK_ADD_MAN	- Mobile Order Packing - Add Manually
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Packing - Add Manually',1,'PACK_ADD_MAN');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PACK_ADD_MAN'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PACK_ADD_MAN'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PACK_ADD_MAN'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- PACK_CAM_SCAN	- Mobile Order Packing - Camera Scan
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Packing - Camera Scan',1,'PACK_CAM_SCAN');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PACK_CAM_SCAN'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PACK_CAM_SCAN'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PACK_CAM_SCAN'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- CC_SORT_LIST	- Mobile Cycle Count - Sort
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Cycle Count - Sort',1,'CC_SORT_LIST');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='CC_SORT_LIST'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='CC_SORT_LIST'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='CC_SORT_LIST'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- CC_FLTR_LIST	- Mobile Cycle Count - Filter
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Cycle Count - Filter',1,'CC_FLTR_LIST');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='CC_FLTR_LIST'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='CC_FLTR_LIST'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='CC_FLTR_LIST'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- CC_SEARCH	- Mobile Cycle Count - Search
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Cycle Count - Search',1,'CC_SEARCH');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='CC_SEARCH'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='CC_SEARCH'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='CC_SEARCH'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- CC_PRINT	- Mobile Cycle Count - Print
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Cycle Count - Print',1,'CC_PRINT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='CC_PRINT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='CC_PRINT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='CC_PRINT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- CC_EXP_CAT	- Mobile Cycle Count - Expand Categories
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Cycle Count - Expand Categories',1,'CC_EXP_CAT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='CC_EXP_CAT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='CC_EXP_CAT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='CC_EXP_CAT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- SEARCH_SEARCH -	Mobile  - Search
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile - Search',1,'SEARCH_SEARCH');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='SEARCH_SEARCH'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='SEARCH_SEARCH'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='SEARCH_SEARCH'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- SEARCH_EXP_CAT	- Mobile - Expand Categories
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile - Expand Categories',1,'SEARCH_EXP_CAT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='SEARCH_EXP_CAT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='SEARCH_EXP_CAT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='SEARCH_EXP_CAT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);



--CC_COLL_CAT -	Mobile Cycle Count - Collapse Categories
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Cycle Count - Collapse Categories',1,'CC_COLL_CAT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='CC_COLL_CAT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='CC_COLL_CAT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='CC_COLL_CAT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);


--INV_SORT -	Mobile View Inventory - Sort
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile View Inventory - Sort',1,'INV_SORT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='INV_SORT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='INV_SORT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='INV_SORT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

--CP_SORT	- Mobile Confirm Pickup - Sort
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Confirm Pickup - Sort',1,'CP_SORT');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='CP_SORT'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='CP_SORT'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='CP_SORT'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);


--CP_FLTR	- Mobile Confirm Pickup - Filter
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Confirm Pickup - Filter',1,'CP_FLTR');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='CP_FLTR'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='CP_FLTR'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='CP_FLTR'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);


--PICK_SEL_UNSEL	- Mobile Order Picking - Select/Unselect All
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Picking - Select/Unselect All',1,'PICK_SEL_UNSEL');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_SEL_UNSEL'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_SEL_UNSEL'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_SEL_UNSEL'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);


--PICK_EXP_COL	- Mobile Order Picking - Expand / Collapse All
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)  
VALUES((select max(permission_id) from permission)+4,'Mobile Order Picking - Expand / Collapse All',1,'PICK_EXP_COL');                                                                                                                                                       

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),
(select permission_id from permission where permission_code='PICK_EXP_COL'),
(select max(row_uid) from app_mod_perm)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='PICK_EXP_COL'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);


INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Store Commerce Activation'),
(select module_id from module where module_code='EEM'),(select permission_id from permission where permission_code='PICK_EXP_COL'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

commit;
EXEC SEQUPDT ('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
EXEC SEQUPDT ('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
EXEC SEQUPDT ('COMPANY_APP','ROW_UID','SEQ_COAPP_ROWUID');


UPDATE DB_BUILD_HISTORY SET END_DTTM=SYSDATE WHERE VERSION_LABEL='EEMBASE_20130808.2013.1.18.08.02';

COMMIT;



alter table DB_BUILD_HISTORY modify VERSION_LABEL VARCHAR2(50);

INSERT INTO DB_BUILD_HISTORY
(VERSION_LABEL, START_DTTM, END_DTTM, VERSION_RANGE, APP_BLD_NO)
VALUES 
('EEMBASE_20130808.2013.1.18.08.06',SYSDATE,SYSDATE,'EEM DB INCREMENTAL BUILD - 18.8 FOR 2013','EEMBASE_20130808.2013.1.18.08.06');

COMMIT;

-- DBTicket SIF-855

UPDATE LRF_REPORT_DEF
SET CATEGORY = 'SIF'
WHERE SUBCATEGORY = 'CYCL' AND CATEGORY = 'EEM';

COMMIT;

UPDATE DB_BUILD_HISTORY SET END_DTTM=SYSDATE WHERE VERSION_LABEL='EEMBASE_20130808.2013.1.18.08.06';

COMMIT;



INSERT INTO DB_BUILD_HISTORY (VERSION_LABEL, START_DTTM, END_DTTM, VERSION_RANGE, APP_BLD_NO) 
VALUES ('DOMBASE_20131010.2013.1.18.09',SYSDATE,SYSDATE,'DOM incremental Release for 2013','DOMBASE_20131010.2013.1.18.09');
commit;

-- DBTicket DOM-9051
delete from PERMISSION_INHERITANCE where child_permission_id=(select permission_id from permission where PERMISSION_NAME='Create Or Update Customer Master') 
and PARENT_PERMISSION_ID in (select permission_id from permission where PERMISSION_NAME in ('Create Customer Order', 'Edit Customer Order'));
Commit;

UPDATE DB_BUILD_HISTORY SET END_DTTM=SYSDATE WHERE VERSION_LABEL='DOMBASE_20131010.2013.1.18.09';

UPDATE APP SET APP_name='Store Inventory & Fulfillment',APP_SHORT_NAME='SIF' WHERE APP_SHORT_NAME='SCA';

COMMIT;

exit;

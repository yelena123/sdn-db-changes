MERGE INTO DB_BUILD_HISTORY D
     USING (SELECT 'DOMBASE_20140108.2013.1.18.10' VERSION_LABEL
              FROM DUAL) B
        ON (D.VERSION_LABEL = B.VERSION_LABEL )
WHEN NOT MATCHED
THEN
INSERT(VERSION_LABEL, START_DTTM, END_DTTM, APP_BLD_NO) 
VALUES('DOMBASE_20140108.2013.1.18.10', systimestamp, systimestamp, 'DOMBASE_20140108.2013.1.18.10');

commit;

-- DBTicket DOM-7358

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Distributed Order Management'
                   AND MODULE.MODULE_CODE = 'DOM'
                   AND PERMISSION.PERMISSION_CODE = 'DOM_CCO') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Distributed Order Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'DOM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'DOM_CCO'),
               (SELECT MAX (ROW_UID) + 4 FROM APP_MOD_PERM));


MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Distributed Order Management'
                   AND MODULE.MODULE_CODE = 'DOM'
                   AND PERMISSION.PERMISSION_CODE = 'DOM_ECO') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Distributed Order Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'DOM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'DOM_ECO'),
               (SELECT MAX (ROW_UID) + 4 FROM APP_MOD_PERM));
               
COMMIT;               

update db_build_history set end_dttm=systimestamp where version_label='DOMBASE_20131206.2013.1.18.10';

commit;


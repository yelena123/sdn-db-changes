SET SCAN OFF;
SET DEFINE OFF;
SET ECHO ON;


ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS';
ALTER SESSION SET NLS_TIMESTAMP_FORMAT='YYYY-MM-DD HH24:MI:SS';


INSERT INTO DB_BUILD_HISTORY
   (VERSION_LABEL, START_DTTM, END_DTTM, VERSION_RANGE, APP_BLD_NO)
VALUES 
   ('EEM_20131206.2013.1.18.10',SYSDATE,SYSDATE,'EEM DB INCREMENTAL BUILD - 18.9 FOR 2013','EEM_20131206.2013.1.18.10');

COMMIT;

-- DBTicket EEM-5396

EXEC SEQUPDT ('NAVIGATION', 'NAVIGATION_ID', 'NAVIGATION_ID_SEQ');

DELETE FROM navigation_app_mod_perm
      WHERE navigation_id IN
               (SELECT navigation_id
                  FROM navigation
                 WHERE url IN
                          ('/eem/*',
                           '/eem/adm/*',
                           '/eem/chargeback/*',
                           '/eem/cogi/*',
                           '/eem/cyclecount/*',
                           '/eem/hub/*',
                           '/eem/inspection/*',
                           '/eem/instorepickup/*',
                           '/eem/manifest/*',
                           '/eem/mobile/*',
                           '/eem/parcel/*',
                           '/eem/pricetkt/*',
                           '/eem/remittanceadvice/*'));

DELETE FROM navigation
      WHERE url IN
               ('/eem/*',
                '/eem/adm/*',
                '/eem/chargeback/*',
                '/eem/cogi/*',
                '/eem/cyclecount/*',
                '/eem/hub/*',
                '/eem/inspection/*',
                '/eem/instorepickup/*',
                '/eem/manifest/*',
                '/eem/mobile/*',
                '/eem/parcel/*',
                '/eem/pricetkt/*',
                '/eem/remittanceadvice/*');

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/adm/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES ( (SELECT NAVIGATION_ID
                 FROM NAVIGATION
                WHERE URL = '/eem/adm/*' AND CREATED_SOURCE = 'EEM'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'eem'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'VADM'));


INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/chargeback/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES ( (SELECT NAVIGATION_ID
                 FROM NAVIGATION
                WHERE URL = '/eem/chargeback/*' AND CREATED_SOURCE = 'EEM'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'eem'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'VCHB'));

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/cogi/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES ( (SELECT NAVIGATION_ID
                 FROM NAVIGATION
                WHERE URL = '/eem/cogi/*' AND CREATED_SOURCE = 'EEM'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'eem'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'VCOGI'));

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/cyclecount/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

----INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
----                                     APP_ID,
----                                     MODULE_ID,
----                                     PERMISSION_ID)
----     VALUES ( (SELECT NAVIGATION_ID
----                 FROM NAVIGATION
----                WHERE URL = '/eem/cyclecount/*' AND CREATED_SOURCE = 'EEM'),
----             (SELECT APP_ID
----                FROM APP
----               WHERE APP_SHORT_NAME = 'eem'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'EEM'),
----             (SELECT PERMISSION_ID
----                FROM PERMISSION
----               WHERE PERMISSION_CODE = 'VIEWCYCLCNT'));

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/hub/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES ( (SELECT NAVIGATION_ID
                 FROM NAVIGATION
                WHERE URL = '/eem/hub/*' AND CREATED_SOURCE = 'EEM'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'eem'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'RCVASN'));


INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/inspection/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES ( (SELECT NAVIGATION_ID
                 FROM NAVIGATION
                WHERE URL = '/eem/inspection/*' AND CREATED_SOURCE = 'EEM'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'eem'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'VINSP'));

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/instorepickup/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

----INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
----                                     APP_ID,
----                                     MODULE_ID,
----                                     PERMISSION_ID)
----     VALUES (
----               (SELECT NAVIGATION_ID
----                  FROM NAVIGATION
----                 WHERE URL = '/eem/instorepickup/*'
----                       AND CREATED_SOURCE = 'EEM'),
----               (SELECT APP_ID
----                  FROM APP
----                 WHERE APP_SHORT_NAME = 'eem'),
----               (SELECT MODULE_ID
----                  FROM MODULE
----                 WHERE MODULE_CODE = 'EEM'),
----               (SELECT PERMISSION_ID
----                  FROM PERMISSION
----                 WHERE PERMISSION_CODE = 'VIEWSTOREORDER'));

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/manifest/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES ( (SELECT NAVIGATION_ID
                 FROM NAVIGATION
                WHERE URL = '/eem/manifest/*' AND CREATED_SOURCE = 'EEM'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'eem'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'PRCL'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'VMFST'));

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/mobile/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES ( (SELECT NAVIGATION_ID
                 FROM NAVIGATION
                WHERE URL = '/eem/mobile/*' AND CREATED_SOURCE = 'EEM'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'eem'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'VRECS'));

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/parcel/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES ( (SELECT NAVIGATION_ID
                 FROM NAVIGATION
                WHERE URL = '/eem/parcel/*' AND CREATED_SOURCE = 'EEM'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'eem'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'PRCL'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'VTATP'));

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/pricetkt/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES ( (SELECT NAVIGATION_ID
                 FROM NAVIGATION
                WHERE URL = '/eem/pricetkt/*' AND CREATED_SOURCE = 'EEM'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'eem'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'PPTKT'));

INSERT INTO NAVIGATION (NAVIGATION_ID,
                        PARENT_ID,
                        TITLE,
                        URL,
                        TYPE,
                        SIDE_NAV,
                        UDEF_SEQ,
                        CREATED_SOURCE)
     VALUES (NAVIGATION_ID_SEQ.NEXTVAL,
             NULL,
             NULL,
             '/eem/remittanceadvice/*',
             '-1',
             'F',
             (SELECT MAX (UDEF_SEQ) + 1 FROM navigation),
             'EEM');

INSERT INTO NAVIGATION_APP_MOD_PERM (NAVIGATION_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID)
     VALUES (
               (SELECT NAVIGATION_ID
                  FROM NAVIGATION
                 WHERE URL = '/eem/remittanceadvice/*'
                       AND CREATED_SOURCE = 'EEM'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'eem'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'VRA'));

COMMIT;

-- DBTicket SIF-1132

update label set key = 'Store Commerce' where key = 'StoreCommerce' and bundle_name like 'Navigation';

update label set key = 'Event Management' where key = 'EventManagement' and bundle_name like 'Navigation';
update label set key = 'Order Management' where key = 'OrderManagement' and bundle_name like 'Navigation';
update label set key = 'Store Location' where key = 'StoreLocation' and bundle_name like 'Navigation';
update label set key = 'Value Added Services' where key = 'ValueAddedServices' and bundle_name like 'Navigation';


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Hazadous material' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Hazadous material',
               'Hazadous material',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Printer Configuration' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Printer Configuration',
               'Printer Configuration',
               'Navigation');

update label set key = 'Add Event Definition' where key = 'AddEventDefinition' and bundle_name like 'Navigation';
update label set key = 'Add Schedule Template' where key = 'AddScheduleTemplate' and bundle_name like 'Navigation';
update label set key = 'Add Schedule Template Group' where key = 'AddScheduleTemplateGroup' and bundle_name like 'Navigation';
update label set key = 'Add Schedule Template Group Rule' where key = 'AddScheduleTemplateRule' and bundle_name like 'Navigation';
update label set key = 'Global Item Inventory' where key = 'GlobalItemInventory' and bundle_name like 'Navigation';
update label set key = 'Inventory By Source' where key = 'Inventory_By_Source' and bundle_name like 'Navigation';
update label set key = 'Parcel Manifests' where key = 'ParcelManifests' and bundle_name like 'Navigation';
update label set key = 'Print Price Tickets' where key = 'PrintPriceTickets' and bundle_name like 'Navigation';
update label set key = 'Print Service Assignment' where key = 'PrintServiceAssignment' and bundle_name like 'Navigation';
update label set key = 'Reconciliation Summary' where key = 'ReconciliationSummary' and bundle_name like 'Navigation';
update label set key = 'Schedule Template Group Rules' where key = 'ScheduleTemplateRules' and bundle_name like 'Navigation';
update label set key = 'Schedule Template Groups' where key = 'ScheduleTemplateGroups' and bundle_name like 'Navigation';
update label set key = 'Track Parcels' where key = 'TrackParcels' and bundle_name like 'Navigation';


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Add Hazardous Material' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Add Hazardous Material',
               'Add Hazardous Material',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Cycle Count' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Cycle Count',
               'Cycle Count',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Drop Shipments' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Drop Shipments',
               'Drop Shipments',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Fulfillment tasks' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Fulfillment tasks',
               'Fulfillment tasks',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Hazardous Materials' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Hazardous Materials',
               'Hazardous Materials',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Pickups' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Pickups',
               'Pickups',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Receipts' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Receipts',
               'Receipts',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Shipment-PKGS' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Shipment-PKGS',
               'Shipment-PKGS',
               'Navigation');
			   
update XMenu_Item set name = 'Add Transit Lane',short_name = 'Add Transit Lane' where name = 'Add Transit Lanes';

commit;

-- DBTicket SIF-1137

MERGE INTO BUNDLE_META_DATA A
     USING (SELECT 'SCA' AS BUNDLE_NAME FROM DUAL) B
        ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (BUNDLE_META_DATA_ID,
               BUNDLE_NAME,
               DESCRIPTION,
               DEF_SCREEN_TYPE_ID,
               IS_ACTIVE,
               IS_LABEL_BUNDLE)
       VALUES (SEQ_BUNDLE_META_DATA_ID.NEXTVAL,
               'SCA',
               'Store Commerce',
               10,
               1,
               1);

MERGE INTO XSCREEN A USING
(SELECT
  9000128 as XSCREEN_ID,
  'SCA' as NAME,
  'Store Commerce' as DESCRIPTION,
  1 as SCREEN_TYPE,
  4 as SCREEN_VERSION,
  0 as SCREEN_MODE,
  1 as IS_RESIZABLE,
  NULL as CREATED_SOURCE,
  1 as CREATED_SOURCE_TYPE,
  systimestamp as CREATED_DTTM,
  NULL as LAST_UPDATED_SOURCE,
  1 as LAST_UPDATED_SOURCE_TYPE,
  systimestamp as LAST_UPDATED_DTTM,
  125 as DEFAULT_X,
  125 as DEFAULT_Y,
  600 as DEFAULT_WIDTH,
  400 as DEFAULT_HEIGHT
  FROM DUAL) B
ON (A.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED THEN 
INSERT (
  XSCREEN_ID, NAME, DESCRIPTION, SCREEN_TYPE, SCREEN_VERSION, 
  SCREEN_MODE, IS_RESIZABLE, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, 
  LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM, DEFAULT_X, DEFAULT_Y, 
  DEFAULT_WIDTH, DEFAULT_HEIGHT)
VALUES (
  B.XSCREEN_ID, B.NAME, B.DESCRIPTION, B.SCREEN_TYPE, B.SCREEN_VERSION, 
  B.SCREEN_MODE, B.IS_RESIZABLE, B.CREATED_SOURCE, B.CREATED_SOURCE_TYPE, B.CREATED_DTTM, 
  B.LAST_UPDATED_SOURCE, B.LAST_UPDATED_SOURCE_TYPE, B.LAST_UPDATED_DTTM, B.DEFAULT_X, B.DEFAULT_Y, 
  B.DEFAULT_WIDTH, B.DEFAULT_HEIGHT);
   

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'LastToFirstScan' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LastToFirstScan',
               'Last to First Scanned (Default)',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'QtyAdjustedIncreasing' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'QtyAdjustedIncreasing',
               'Qty Adjusted Increasing',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'QtyAdjustedDecreasing' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'QtyAdjustedDecreasing',
               'Qty Adjusted Decreasing',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'TypeUPC' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TypeUPC',
               'Type UPC',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'ShipIdisrequired' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ShipIdisrequired',
               'Shipment Id is required',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'AtleastOnefieldRequired' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AtleastOnefieldRequired',
               'At least one field is required',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'TypePackageId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TypePackageId',
               'Type package Id',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'Senttoprinter' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Senttoprinter',
               'sent to printer',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'PackedUnits' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PackedUnits',
               'Packed Unit(s)',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'ContinueAndScan' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ContinueAndScan',
               'Continue  package contents',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'Continue' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Continue',
               'Continue',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'Resume' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Resume',
               'Resume',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'Inprogressactivityexists' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Inprogressactivityexists',
               'In progress activity exists',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'TtlUnits' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TtlUnits',
               'Total Unit(s)',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'OrderedUnits' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'OrderedUnits',
               'Ordered Unit(s)',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'Savedactivityfound' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Savedactivityfound',
               'Saved activity found',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'SelectAStore' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectAStore',
               'Select a Store',
               'SCA');

MERGE INTO LABEL L
     USING (SELECT 'SCA' BUNDLE_NAME, 'Pause' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Pause',
               'Pause',
               'SCA');


MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'LastToFirstScan' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'LastToFirstScan',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'QtyAdjustedIncreasing' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'QtyAdjustedIncreasing',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'QtyAdjustedDecreasing' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'QtyAdjustedDecreasing',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'TypeUPC' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'TypeUPC',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'ShipIdisrequired' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'ShipIdisrequired',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'AtleastOnefieldRequired' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'AtleastOnefieldRequired',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'TypePackageId' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'TypePackageId',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Senttoprinter' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Senttoprinter',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'PackedUnits' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'PackedUnits',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'ContinueAndScan' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'ContinueAndScan',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Continue' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Continue',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Resume' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Resume',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Inprogressactivityexists' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Inprogressactivityexists',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'TtlUnits' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'TtlUnits',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'OrderedUnits' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'OrderedUnits',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Savedactivityfound' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Savedactivityfound',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'SelectAStore' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'SelectAStore',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Pause' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Pause',
               'SCA');
               


MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280115' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280115',
               '1280115',
               'sca',
               'Enter at least one of the fields',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280116' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280116',
               '1280116',
               'sca',
               'Package Id {0} is already scanned',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280117' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280117',
               '1280117',
               'sca',
               'UPC is required',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280118' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280118',
               '1280118',
               'sca',
               '{0} Order(s) have been picked',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280119' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280119',
               '1280119',
               'sca',
               '{0} has been received already',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280120' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280120',
               '1280120',
               'sca',
               'Order pickup successful',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280121' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280121',
               '1280121',
               'sca',
               'Package Id is required',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280122' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280122',
               '1280122',
               'sca',
               'Activity {0} already assigned',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280123' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280123',
               '1280123',
               'sca',
               'Multiple open activities found',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280124' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280124',
               '1280124',
               'sca',
               'Invalid quantity',
               'ErrorMessage');

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1280125' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '1280125',
               '1280125',
               'sca',
               'Picked quantity exceeds ordered',
               'ErrorMessage');               

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280115' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280115',
               'Enter at least one of the fields',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280116' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280116',
               'Package Id {0} is already scanned',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280117' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280117',
               'UPC is required',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280118' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280118',
               '{0} Order(s) have been picked',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280119' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280119',
               '{0} has been received already',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280120' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280120',
               'Order pickup successful',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280121' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280121',
               'Package Id is required',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280122' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280122',
               'Activity {0} already assigned',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280123' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280123',
               'Multiple open activities found',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280124' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280124',
               'Invalid quantity',
               'ErrorMessage');

MERGE INTO LABEL L
     USING (SELECT 'ErrorMessage' BUNDLE_NAME, '1280125' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               '1280125',
               'Picked quantity exceeds ordered',
               'ErrorMessage');
               
MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280115' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280115',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280116' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280116',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280117' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280117',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280118' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280118',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280119' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280119',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280120' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280120',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280121' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280121',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280122' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280122',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280123' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280123',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280124' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280124',
               'ErrorMessage');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT '1280125' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'ErrorMessage' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               '1280125',
               'ErrorMessage');
COMMIT;

-- DBTicket SIF-1194

update filter_layout set hibernate_field_name = '(1*decode(nvl(cyclecountitems.systemCount,0),0,0,((abs(cyclecountitems.systemCount-cyclecountitems.pendingReviewCount))/cyclecountitems.systemCount*100)))' 
where object_type = 'CC_ITEM_REVIEW_NO_LOC' and field_position = 1;

update filter_layout set hibernate_field_name = '(1*decode(nvl(cyclecountitems.systemCount,0),0,0,((abs(cyclecountitems.systemCount-cyclecountitems.pendingReviewCount))/cyclecountitems.systemCount*100)))' 
where object_type = 'CC_ITEM_REVIEW_NO_LOC' and field_position = 2;

commit;

-- DBTicket EEM-5505
 

MERGE INTO LABEL L
     USING (SELECT 'BuildLPN' BUNDLE_NAME,
                   'LPNDetailList' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (Seq_Label_Id.Nextval, 'LPNDetailList', 'LPN Detail List','BuildLPN');

COMMIT;

-- DBTicket EEM-5512

MERGE INTO LABEL L
     USING (SELECT 'ScanPack' BUNDLE_NAME,
                   'ALERTPleaseSelectAPrinterToPrintTheContentLabels' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ALERTPleaseSelectAPrinterToPrintTheContentLabels',
               'Please select a printer to print the content labels.',
               'ScanPack');

MERGE INTO LABEL L
     USING (SELECT 'ScanPack' BUNDLE_NAME,
                   'ALERTPleaseEableThePrintLabelCheckbox' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ALERTPleaseEableThePrintLabelCheckbox',
               'Please enable the Print label checkbox',
               'ScanPack');
               
COMMIT;    

-- DBTicket EEM-5540

MERGE INTO MESSAGE_MASTER L
     USING (SELECT '1350053' KEY, 'ErrorMessage' bundle_name FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.message_master_id,
               L.msg_id,
               L.key,
               L.ils_module,
               L.MSG,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_MESSAGE_MASTER_ID.NEXTVAL,
                 '1350053',
                 '1350053',
                 'eem',
                 'Unable to cross dock. PO destination facility is same as outbound ASN�s origin facility.',
                 'ErrorMessage');


-- DBTicket SIF-1218


MERGE INTO PARAM_DEF P
     USING (SELECT 'TIME_IN_HOURS_TO_CLEAR_MOBILE_TRANS_DATA_PARAM'
                      PARAM_DEF_ID,
                   'COM_VNDR' PARAM_GROUP_ID
              FROM DUAL) D
        ON (P.PARAM_DEF_ID = D.PARAM_DEF_ID
            AND P.PARAM_GROUP_ID = D.PARAM_GROUP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (PARAM_DEF_ID,
               PARAM_GROUP_ID,
               PARAM_SUBGROUP_ID,
               PARAM_CATEGORY,
               PARAM_NAME,
               DESCRIPTION,
               IS_DISABLED,
               IS_REQUIRED,
               DATA_TYPE,
               UI_TYPE,
               IS_MIN_INC,
               IS_MAX_INC,
               DFLT_VALUE,
               DISPLAY_ORDER,
               VALUE_GENERATOR_CLASS,
               IS_LOCALIZABLE)
       VALUES ('TIME_IN_HOURS_TO_CLEAR_MOBILE_TRANS_DATA_PARAM',
               'COM_VNDR',
               'Mobile',
               'Mobile',
               'Time in hours to clear mobile transactional data',
               'Time in hours to clear mobile transactional data',
               '0',
               '0',
               '1',
               'TEXTBOX',
               '0',
               '0',
               NULL,
               (SELECT MAX (display_order)
                  FROM PARAM_DEF
                 WHERE PARAM_SUBGROUP_ID = 'Mobile')
               + 1,
               NULL,
               '1');


MERGE INTO LABEL L
     USING (SELECT 'ParamDef' BUNDLE_NAME,
                   'TIME_IN_HOURS_TO_CLEAR_MOBILE_TRANS_DATA_PARAM' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TIME_IN_HOURS_TO_CLEAR_MOBILE_TRANS_DATA_PARAM',
               'Time in hours to clear mobile transactional data',
               'ParamDef');
               
COMMIT;               

-- DBTicket EEM-5531

create or replace 
PROCEDURE PRC_MENU_COUNT_IOS
AS
BEGIN
  EXECUTE immediate 'DELETE from menu_count where CREATED_DTTM < sysdate -1';
  -- IOS : Customer Orders=> Pick
  -- Fullfilment->Customer Pickups->Pick [USER count only]
  -- Level 4(Insert) START
  INSERT
  INTO menu_count
    (
      created_dttm ,
      last_updated_dttm ,
      menu_count_id ,
      company_id ,
      permission_code ,
      menu_count ,
      last_updated_source ,
      facility_id
    )
    (
      -- Level 3 (Sequence) START
      SELECT sysdate ,
        sysdate ,
        SEQ_MENU_COUNT_ID.nextval ,
        tc_company_id ,
        'DO_CUST_ORDER_PICKUPS' ,
        SUM ,
        usr ,
        o_facility_id
      FROM
        (
        -- Level 2 (Sum) START
        SELECT tc_company_id ,
          o_facility_id ,
          usr ,
          COUNT(1) SUM
        FROM
          (
          -- Level 1(Decode) START
          SELECT DISTINCT orders.order_id ,
            orders.tc_company_id ,
            'DO_CUST_ORDER_PICKUPS',
            NULL ,
            orders.o_facility_id ,
            decode(orders.do_status,
                                100, --Open
                                'EXTERNAL', -- Open needs to be counted for all users. Hence mark as EXTERNAL so that it is counted for all users.
                                110,  -- Accepted
                                DECODE(orders.last_updated_source_type,2,'EXTERNAL',1,orders.last_updated_source )
                  ) usr
          FROM orders orders ,
            order_line_item oli
          WHERE orders.order_id                = oli.order_id
          AND oli.item_id                     IS NOT NULL
          AND orders.o_facility_id            IS NOT NULL
          -- Order type =  customer
          AND orders.do_type                   = 20
          AND ( ( orders.delivery_options      = '01' )
          OR (orders.delivery_options           = '03')
          OR ( orders.delivery_options         = '02'
          AND orders.destination_action        = '01' ) )
          AND ( orders.do_status               = 100
          OR ( orders.do_status               = 110
          AND orders.last_updated_source_type IN ( '1' , '2' ) ) )
          AND oli.allocation_source            ='10'
          AND oli.do_dtl_status!               =200
          AND orders.has_import_error          =0
          AND orders.order_id  not in   ( select line.order_id from order_line_item line  where  line.allocation_source != 10 and oli.order_id=orders.order_id  )
            AND ( (SELECT COUNT(1)
                FROM order_line_item oli
                WHERE orders.order_id = oli.order_id
                AND oli.is_cancelled != 1 )  > 0 )
            -- Level 1(Decode) END
          )
        GROUP BY tc_company_id ,
          o_facility_id ,
          usr
          -- Level 2(Sum) END
        )
        -- Level 3(Sequence) END
    )
    -- Level 4(Insert) END
    ;
  --iOS :: Customer Orders=>Pack
  -- Fulfilment->Ship to customer->Pick/Pack
  --SHIP2CUST_PICK_OR_PACK
  -- level 4 (insert) start
  INSERT
  INTO menu_count
    (
      created_dttm ,
      last_updated_dttm ,
      menu_count_id ,
      company_id ,
      permission_code ,
      menu_count ,

      last_updated_source,
      facility_id
    )
  -- level 3 ( sequence) start
  SELECT sysdate ,
    sysdate ,
    SEQ_MENU_COUNT_ID.nextval ,
    inner_select.tc_company_id ,
    'DO_CUST_ORDER' ,
    inner_select.s2c_pick_pack_count ,
    usr ,
    inner_select.o_facility_id
  FROM
    (
    -- level 2(count) start
    SELECT tc_company_id ,
      'DO_CUST_ORDER' ,
      COUNT(o_facility_id) s2c_pick_pack_count ,
      usr ,
      o_facility_id
    FROM
      -- level 1(data) start
      (
      SELECT DISTINCT orders.order_id ,
        orders.tc_company_id ,
        'DO_CUST_ORDER' ,
        decode(orders.do_status,
              110, -- Released
              DECODE(
                    orders.last_updated_source_type,
                    2,
                    'EXTERNAL',
                    1,
                    orders.last_updated_source
                    ),
              130, -- DC Allocated or Ready for Packing
              'EXTERNAL' , -- If it is ready for packing, then it should be accounted for all users. Hence mark as EXTERNAL.
              140 , -- In Packing
              orders.last_updated_source ,
              185, -- Partially Shipped
              'EXTERNAL' -- If it is partially shipped, then any user should get this order. Hence mark as EXTERNAL.
         )   usr ,
        orders.o_facility_id
      FROM orders orders ,
        order_line_item oli
      WHERE orders.order_id           = oli.order_id
      AND oli.item_id                 IS NOT NULL
      AND orders.o_facility_id        IS NOT NULL
      AND orders.do_status         IN ( 110,130,140,185 )
       -- Order type =  customer
      AND orders.do_type              = 20
      AND ( orders.delivery_options   = '03'
      OR (orders.delivery_options     = '02'
      AND orders.destination_action   = '01' ) )
      AND oli.allocation_source     ='10'
      AND oli.do_dtl_status!        =200
      AND orders.has_import_error   =0
      AND  orders.order_id  not in  (  select line.order_id from order_line_item line  where   line.allocation_source != 10 and   line.order_id=orders.order_id  )
      AND ( (SELECT COUNT(1)
                FROM order_line_item oli
                WHERE orders.order_id = oli.order_id
                AND oli.is_cancelled != 1 )  > 0 )
      )
      -- level 1(data) end
    GROUP BY tc_company_id ,
      o_facility_id, usr
      -- level 2(count) end
    ) inner_select
    -- level 3(sequence) end
    -- level 4 (insert) end
    ;
  -- iOS : Transfer-> Pick
  -- Fulfilment->Ship to location->XFER-Pick
  -- XFER_PICK
  -- Level 4(Insert) START
  INSERT
  INTO menu_count
    (
      created_dttm ,
      last_updated_dttm ,
      menu_count_id ,
      company_id ,
      permission_code ,
      menu_count ,
      last_updated_source ,
      facility_id
    )
    (
      -- Level 3 (Sequence) START
      SELECT sysdate ,
        sysdate ,
        SEQ_MENU_COUNT_ID.nextval ,
        tc_company_id ,
        'DO_SHIP_TO_LOCN_PICK' ,
        SUM ,
        usr ,
        o_facility_id
      FROM (
        -- Level 2(Count) START
        (SELECT tc_company_id ,
          o_facility_id ,
          usr ,
          COUNT(1) SUM
        FROM
          (
          -- Level 1(Decode) START
            SELECT DISTINCT orders.order_id ,
            orders.tc_company_id ,
            'DO_SHIP_TO_LOCN_PICK' ,
            NULL ,
            orders.o_facility_id ,
            decode(orders.do_status,
                        100, --Open
                        'EXTERNAL', -- If it is Open, then any associate can pick it up. Hence mark it as EXTERNAL.
                        110, -- Accepted
                        DECODE(orders.last_updated_source_type,
                                2, -- External System
                                'EXTERNAL',
                                1, -- User
                                orders.last_updated_source )
                   ) usr
          FROM orders orders ,
            order_line_item oli
          WHERE orders.o_facility_id   IS NOT NULL
          AND
          (
             ( orders.do_status   =   100 )
          or ( orders.do_status   =   110 )
          )
          AND orders.do_type                   = 60 -- 1.    Order type = transfer
          AND orders.has_import_error          = 0
          AND orders.order_id                  = oli.order_id
          AND oli.allocation_source            ='10'
          AND oli.do_dtl_status!               =200
          AND orders.order_id  not in   ( select line.order_id from order_line_item line  where  line.allocation_source != 10 and oli.order_id=orders.order_id  )
          AND orders.last_updated_source_type IN ( '1' , '2' )
          AND oli.item_id                     IS NOT NULL
          and  orders.order_id  not in   (
            select line.order_id from ORDER_LINE_ITEM line  where
            line.allocation_source != 10 and
            oli.order_id=orders.order_id  )
          and ( (SELECT COUNT(1)
                FROM order_line_item oli
                WHERE orders.order_id = oli.order_id
                AND oli.is_cancelled != 1 )  > 0)
            -- Level 1(Decode) END
          )
        GROUP BY tc_company_id ,
          o_facility_id ,
          usr
        )
        -- Level 2(Count) END
        )
        -- Level 3(Sequence) END
    )
    ;


  -- iOS : Transfer->pack.
  -- Fulfilment->Ship to location->XFER-Pick/Pack
  -- XFER_PICK_PACK
  -- level 4 ( insert) start
  INSERT
  INTO menu_count
    (
      created_dttm ,
      last_updated_dttm ,
      menu_count_id ,
      company_id ,
      permission_code ,
      menu_count ,
      user_id ,
      last_updated_source ,
      facility_id
    )
  -- level 3 ( sequence) start
  SELECT sysdate ,
    sysdate ,
    SEQ_MENU_COUNT_ID.nextval ,
    inner_select.tc_company_id ,
    'DO_TRANSFER' ,
    inner_select.xfer_pick_pack ,
    NULL ,
    usr ,
    inner_select.o_facility_id
  FROM
    (
    -- level 2 ( count) start
    SELECT tc_company_id ,
      'DO_TRANSFER' ,
      COUNT(o_facility_id) xfer_pick_pack ,
      NULL ,
      usr ,
      o_facility_id
    FROM
      -- level 1 ( data) start
      (
      SELECT DISTINCT orders.order_id ,
        orders.tc_company_id ,
        'DO_TRANSFER' ,
        NULL ,
        decode(orders.do_status,
              110, -- Released
              DECODE(
                    orders.last_updated_source_type,
                    2,
                    'EXTERNAL',
                    1,
                    orders.last_updated_source
                    ),
              130, -- DC Allocated or Ready for Packing
              'EXTERNAL' , -- Mark as EXTERNAL as these need to be displayed irrespective of who the order is assigned to.
              140 , -- In Packing
              orders.last_updated_source ,
              185,  -- Partially Shipped
              'EXTERNAL' -- Mark as EXTERNAL as these need to be displayed irrespective of who the order is assigned to.
         )   usr ,
        orders.o_facility_id
      FROM orders orders ,
        order_line_item oli
      WHERE orders.o_facility_id          IS NOT NULL
      AND orders.do_status                IN (110,130,140,185)
      AND orders.do_type                   = 60
      AND orders.has_import_error          =0
      AND orders.order_id                  = oli.order_id
      AND oli.allocation_source            = '10'
      AND oli.do_dtl_status!               =200
      AND orders.order_id  not in   ( select line.order_id from order_line_item line  where  line.allocation_source != 10 and oli.order_id=orders.order_id  )
      AND oli.order_id                     =orders.order_id
      AND oli.item_id                     IS NOT NULL
      AND orders.last_updated_source_type IN ( '1' , '2' )
      and  orders.order_id  not in   (
      select line.order_id from ORDER_LINE_ITEM line  where
      line.allocation_source != 10 and
      oli.order_id=orders.order_id  )
      and ( (SELECT COUNT(1)
                FROM order_line_item oli
                WHERE orders.order_id = oli.order_id
                AND oli.is_cancelled != 1 )  > 0 )
      )
      -- level 1 ( data) end
    GROUP BY tc_company_id ,
      o_facility_id ,
      usr
      -- level 2 ( count) end
    ) inner_select
    -- level 3 ( sequence) end
    -- level 4 ( insert) end
    ;
    
    
  -- iOS : Inventory=>Cycle Count
  --Inventory-> Cycle Count
  -- CYCLE_COUNT
  INSERT
  INTO menu_count
    (
      created_dttm ,
      last_updated_dttm ,
      menu_count_id ,
      company_id ,
      permission_code ,
      menu_count ,
      user_id ,
      facility_id
    )
    (
      -- Level 4 Start  ( Menu Count Sequence )
      SELECT sysdate ,
        sysdate ,
        SEQ_MENU_COUNT_ID.nextval ,
        count_inner_select.tc_company_id ,
        'CYCLE_COUNT' ,
        count_inner_select.cyccnt_count ,
        count_inner_select.assigned_user_id ,
        count_inner_select.facility_Id
      FROM
        (
        -- Level 3 Start  ( Group and count per user)
        SELECT tc_company_id ,
          facility_Id ,
          'CYCLE_COUNT' ,
          COUNT(assigned_user_id) cyccnt_count ,
          assigned_user_id ,
          NULL
        FROM
          (
          -- Level 2 Start ( Filter Data rank = 1)
          SELECT tc_company_id,
            facility_Id,
            cycle_Count_Request_Id,
            tc_Cycle_Count_Request_Id,
            cycle_count_request_name,
            cycle_Count_Request_Store_Id,
            scheduled_Dttm,
            complete_By_Dttm,
            earliest_start_by_dttm,
            CYCLE_COUNT_REQUEST_STATUS,
            assigned_User_Id
          FROM
            (
            -- Level 1 Start ( join ccr , ccrs , cci , ccia )
            SELECT ccr.tc_company_id ,
              ccrs.facility_Id,
              ccr.cycle_Count_Request_Id,
              ccr.tc_Cycle_Count_Request_Id,
              CCR.cycle_count_request_name,
              ccrs.cycle_Count_Request_Store_Id,
              ccrs.scheduled_Dttm,
              ccrs.complete_By_Dttm,
              ccrs.earliest_start_by_dttm,
              ccrs.CYCLE_COUNT_REQUEST_STATUS,
              NVL(ccia.assigned_User_Id,-1) assigned_User_Id,
              DECODE(ccia.cycle_count_assign_status,30,30,NULL) assign_status,
              RANK() OVER (PARTITION BY ccr.TC_CYCLE_COUNT_REQUEST_ID ORDER BY ccia.assigned_User_Id ) datarank
            FROM Cycle_Count_Request ccr
            LEFT JOIN cycle_Count_Request_Store ccrs
            ON ccrs.cycle_count_request_id = ccr.cycle_count_request_id
            LEFT JOIN cycle_Count_Items cci
            ON cci.cycle_count_request_store_id = ccrs.cycle_count_request_store_id
            LEFT JOIN cycle_Count_Item_Assignment ccia
            ON ccia.cycle_count_item_id          = cci.cycle_count_item_id
            WHERE ccr.is_Cancelled               = 0
            AND ccrs.CYCLE_COUNT_REQUEST_STATUS != 40
            AND ccrs.CYCLE_COUNT_REQUEST_STATUS !=30
            AND cci.CYCLE_COUNT_ITEM_STATUS!     =40
            AND cci.CYCLE_COUNT_ITEM_STATUS!     =60
            AND cci.CYCLE_COUNT_ITEM_STATUS!     =70
            
            AND ( ( ccrs.scheduled_dttm         >= trunc(sysdate)
            AND ccrs.scheduled_dttm              < trunc(sysdate + 1) )
            OR ( ccrs.scheduled_dttm            IS NULL
            AND ccrs.earliest_start_by_dttm      < trunc(sysdate + 1)
            AND ccrs.complete_by_dttm           >= trunc(sysdate) ) )
            
            GROUP BY ccr.tc_company_id ,
              ccrs.facility_Id ,
              ccr.cycle_Count_Request_Id,
              ccr.tc_Cycle_Count_Request_Id,
              ccr.cycle_count_request_name,
              ccrs.cycle_Count_Request_Store_Id,
              ccrs.scheduled_Dttm,
              ccrs.complete_By_Dttm,
              ccrs.earliest_start_by_dttm,
              ccrs.CYCLE_COUNT_REQUEST_STATUS,
              ccia.assigned_User_Id,
              ccia.cycle_count_assign_status
              -- Level 1 End ( join ccr , ccrs , cci , ccia )
            )
          WHERE datarank     = 1
          AND assign_status IS NULL
          GROUP BY tc_company_id,
            facility_Id,
            cycle_Count_Request_Id,
            tc_Cycle_Count_Request_Id,
            cycle_count_request_name,
            cycle_Count_Request_Store_Id,
            scheduled_Dttm,
            complete_By_Dttm,
            earliest_start_by_dttm,
            CYCLE_COUNT_REQUEST_STATUS,
            assigned_User_Id
          ORDER BY complete_By_Dttm ASC
            -- Level 2 End ( Filter Data rank = 1)
          )
        GROUP BY tc_company_id ,
          facility_Id ,
          assigned_user_id
          -- Level 3 End  ( Group and count per user)
        ) count_inner_select
        -- Level 4 End  ( Menu Count Sequence )
    );
    
    
-- Cycle count. Manager level count. user_id as -2 indicates Manager and user_id as -1 indicates unassigned.
  INSERT
  INTO menu_count
    (
      created_dttm ,
      last_updated_dttm ,
      menu_count_id ,
      company_id ,
      permission_code ,
      menu_count ,
      user_id ,
      facility_id
    )
    (      -- Level 4 Start  ( Menu Count Sequence )
      SELECT sysdate ,
        sysdate ,
        SEQ_MENU_COUNT_ID.nextval ,
        count_inner_select.tc_company_id ,
        'CYCLE_COUNT' ,
        count_inner_select.cyccnt_count ,
        -2 , -- Manager
        count_inner_select.facility_Id
      FROM
        (
        -- Level 3 Start  ( Group and count per user)
        SELECT tc_company_id ,
          facility_Id ,
          'CYCLE_COUNT' ,
          COUNT(assigned_user_id) cyccnt_count ,
          -2 ,
          NULL
        FROM
          (
          -- Level 2 Start ( Filter Data rank = 1)
          SELECT tc_company_id,
            facility_Id,
            cycle_Count_Request_Id,
            tc_Cycle_Count_Request_Id,
            cycle_count_request_name,
            cycle_Count_Request_Store_Id,
            scheduled_Dttm,
            complete_By_Dttm,
            earliest_start_by_dttm,
            CYCLE_COUNT_REQUEST_STATUS,
            assigned_User_Id
          FROM
            (
            -- Level 1 Start ( join ccr , ccrs , cci , ccia )
            SELECT ccr.tc_company_id ,
              ccrs.facility_Id,
              ccr.cycle_Count_Request_Id,
              ccr.tc_Cycle_Count_Request_Id,
              CCR.cycle_count_request_name,
              ccrs.cycle_Count_Request_Store_Id,
              ccrs.scheduled_Dttm,
              ccrs.complete_By_Dttm,
              ccrs.earliest_start_by_dttm,
              ccrs.CYCLE_COUNT_REQUEST_STATUS,
              NVL(ccia.assigned_User_Id,-1) assigned_User_Id,
              DECODE(ccia.cycle_count_assign_status,30,30,NULL) assign_status,
              RANK() OVER (PARTITION BY ccr.TC_CYCLE_COUNT_REQUEST_ID ORDER BY ccia.assigned_User_Id ) datarank
            FROM Cycle_Count_Request ccr
            LEFT JOIN cycle_Count_Request_Store ccrs
            ON ccrs.cycle_count_request_id = ccr.cycle_count_request_id
            LEFT JOIN cycle_Count_Items cci
            ON cci.cycle_count_request_store_id = ccrs.cycle_count_request_store_id
            LEFT JOIN cycle_Count_Item_Assignment ccia
            ON ccia.cycle_count_item_id          = cci.cycle_count_item_id
            WHERE ccr.is_Cancelled               = 0
            AND ccrs.CYCLE_COUNT_REQUEST_STATUS != 40
            AND ccrs.CYCLE_COUNT_REQUEST_STATUS !=30
            AND cci.CYCLE_COUNT_ITEM_STATUS!     =40
            AND cci.CYCLE_COUNT_ITEM_STATUS!     =60
            AND cci.CYCLE_COUNT_ITEM_STATUS!     =70
                              
            AND ( ( ccrs.scheduled_dttm         >= trunc(sysdate)
            AND ccrs.scheduled_dttm              < trunc(sysdate + 1) )
            OR ( ccrs.scheduled_dttm            IS NULL
            AND ccrs.earliest_start_by_dttm      < trunc(sysdate + 1)
            AND ccrs.complete_by_dttm           >= trunc(sysdate) ) )
            
            GROUP BY ccr.tc_company_id ,
              ccrs.facility_Id ,
              ccr.cycle_Count_Request_Id,
              ccr.tc_Cycle_Count_Request_Id,
              ccr.cycle_count_request_name,
              ccrs.cycle_Count_Request_Store_Id,
              ccrs.scheduled_Dttm,
              ccrs.complete_By_Dttm,
              ccrs.earliest_start_by_dttm,
              ccrs.CYCLE_COUNT_REQUEST_STATUS,
              ccia.assigned_User_Id,
              ccia.cycle_count_assign_status
              -- Level 1 End ( join ccr , ccrs , cci , ccia )
            )
          WHERE datarank     = 1
          AND assign_status IS NULL
          GROUP BY tc_company_id,
            facility_Id,
            cycle_Count_Request_Id,
            tc_Cycle_Count_Request_Id,
            cycle_count_request_name,
            cycle_Count_Request_Store_Id,
            scheduled_Dttm,
            complete_By_Dttm,
            earliest_start_by_dttm,
            CYCLE_COUNT_REQUEST_STATUS,
            assigned_User_Id
          ORDER BY complete_By_Dttm ASC
            -- Level 2 End ( Filter Data rank = 1)
          )
        GROUP BY tc_company_id ,
          facility_Id
          -- Level 3 End  ( Group and count per user)
        ) count_inner_select
        -- Level 4 End  ( Menu Count Sequence )
 )
 ;
  --Adds the EXTERNAL counts to the user level counts
  --UPDATE menu_count m1
  --SET m1.menu_count = ( NVL(m1.menu_count,0) + NVL(
   -- (SELECT NVL(m2.menu_count,0)
   -- FROM menu_count m2
   -- WHERE m2.company_id        = m1.company_id
   -- AND m1.facility_id         =m2.facility_id
  --  AND m2.PERMISSION_CODE     = m1.PERMISSION_CODE
  --  AND m2.LAST_UPDATED_SOURCE = 'EXTERNAL'
  --  ),0) )
 -- WHERE m1.PERMISSION_CODE   IN ( 'DO_CUST_ORDER_PICKUPS' , 'DO_CUST_ORDER' , 'DO_SHIP_TO_LOCN_PICK' , 'DO_TRANSFER' )
--  AND m1.LAST_UPDATED_SOURCE <> 'EXTERNAL';

  COMMIT;
END PRC_MENU_COUNT_IOS;
/ 

create or replace
PROCEDURE  PRC_MENU_COUNT_WINMOBILE AS
BEGIN

  execute immediate 'DELETE from menu_count where CREATED_DTTM < sysdate -1';

-- Fullfilment->Customer Pickups->Pick [USER count only]
-- Level 4(Insert) START
   insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , last_updated_source , facility_id )
(
-- Level 3 (Sequence) START
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , tc_company_id , 'DO_CUST_ORDER_PICKUPS' , sum ,  usr ,  o_facility_id   from (
-- Level 2 (Sum) START
select  tc_company_id ,  o_facility_id , usr , count(1) sum from  (
-- Level 1(Decode) START
select  distinct orders.order_id , orders.tc_company_id , 'DO_CUST_ORDER_PICKUPS', null ,  orders.o_facility_id ,  decode(orders.last_updated_source_type,2,'EXTERNAL',1,orders.last_updated_source ) usr
from orders orders , order_line_item oli  where
  orders.order_id = oli.order_id and
  orders.o_facility_id  is not null and
  orders.do_type = 20 and
  orders.delivery_options = '01' and
  orders.do_status in ( 100 , 110 ) and
  oli.allocation_source ='10' and
  oli.do_dtl_status!=200 and
  orders.has_import_error=0 and
  orders.last_updated_source_type in ( '1' , '2' ) and
  oli.do_dtl_status!=200
  -- Level 1(Decode) END
  ) group by tc_company_id , o_facility_id , usr
  -- Level 2(Sum) END
  )
  -- Level 3(Sequence) END
  )
  -- Level 4(Insert) END
  ;

-- Fulfilment->Ship to customer->Pick
-- SHIP2CUST_PICK
-- LEVEL 4(Insert) START
insert into menu_count ( created_dttm , last_updated_dttm ,menu_count_id , company_id , permission_code , menu_count , last_updated_source , facility_id )
(
-- LEVEL 3(Sequence) START
select sysdate , sysdate ,  SEQ_MENU_COUNT_ID.nextval , tc_company_id , 'DO_SHIP_TO_CUST_PICK' , sum , usr , o_facility_id from
  (
-- Level 2(Sum) START
(
select  tc_company_id ,  o_facility_id , usr , count(1) sum from  (
-- Level 1(Decode) START
select distinct orders.order_id , orders.tc_company_id , 'DO_SHIP_TO_CUST_PICK' ,  null ,  orders.o_facility_id , decode(orders.last_updated_source_type,2,'EXTERNAL',1,orders.last_updated_source ) usr  from orders orders , order_line_item oli  where

orders.order_id = oli.order_id and

  orders.o_facility_id is not null and
  orders.do_type = 20 and
  orders.do_status in ( 100 , 110) and

  (
     (orders.delivery_options = '02' and orders.destination_action = '01' )
   or(orders.delivery_options = '03')
   ) and

oli.allocation_source ='10' and
orders.has_import_error=0 and
oli.do_dtl_status!=200 and

    orders.last_updated_source_type in ( '1' , '2' )

-- Level 1(Decode) END
    )
   group by tc_company_id , o_facility_id , usr
   )
-- Level 2(Sum) END
   )
-- Level 3(Sequence) END
   );
-- Level 4(Insert) END

-- Fulfilment->Ship to customer->Pick/Pack
--SHIP2CUST_PICK_OR_PACK
-- level 4 (insert) start
insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , user_id , facility_id )
-- level 3 ( sequence) start
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , inner_select.tc_company_id , 'DO_CUST_ORDER' , inner_select.s2c_pick_pack_count , null , inner_select.o_facility_id
  from (
  -- level 2(count) start
   select tc_company_id , 'DO_CUST_ORDER' , count(o_facility_id) s2c_pick_pack_count , null ,  o_facility_id from
  -- level 1(data) start
  (
   select distinct orders.order_id , orders.tc_company_id , 'DO_CUST_ORDER' ,  null ,  orders.o_facility_id from orders orders , order_line_item oli  where
   orders.o_facility_id is not null and
  orders.do_status in  ( 110,130,140,185 ) and
  orders.do_type = 20  and

(
orders.delivery_options = '03' or
(orders.delivery_options = '02' and orders.destination_action = '01'  )
) and
  orders.has_import_error=0 and
  orders.order_id =  oli.order_id and
  oli.allocation_source ='10' and
  oli.do_dtl_status!=200

  )
  -- level 1(data) end
  group by tc_company_id , o_facility_id
  -- level 2(count) end
   )
inner_select
-- level 3(sequence) end
-- level 4 (insert) end
;


-- Fulfilment->Ship to location->XFER-Pick
-- XFER_PICK
-- Level 4(Insert) START
  insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , last_updated_source , facility_id )
  (
  -- Level 3 (Sequence) START
  select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , tc_company_id , 'DO_SHIP_TO_LOCN_PICK' , sum ,  usr ,  o_facility_id from
  (
  -- Level 2(Count) START
  (
select  tc_company_id ,  o_facility_id , usr , count(1) sum from  (
  -- Level 1(Decode) START
  select distinct orders.order_id ,  orders.tc_company_id , 'DO_SHIP_TO_LOCN_PICK' ,  null ,  orders.o_facility_id , decode(orders.last_updated_source_type,2,'EXTERNAL',1,orders.last_updated_source ) usr from orders orders , order_line_item oli where

  orders.o_facility_id is not null and
  orders.do_status in ( 100 , 110 ) and
  orders.do_type = 60 and
  orders.has_import_error = 0  and
  orders.order_id =  oli.order_id and
  oli.allocation_source ='10' and
  oli.do_dtl_status!=200 and
  orders.last_updated_source_type in ( '1' , '2' )
   -- Level 1(Decode) END
  )
  group by tc_company_id , o_facility_id  , usr
  )
  -- Level 2(Count) END
  )
-- Level 3(Sequence) END
   )
   -- Level 4(Insert) END
   ;

-- Fulfilment->Ship to location->XFER-Pick/Pack
-- XFER_PICK_PACK
-- level 4 ( insert) start
insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , user_id , facility_id )
-- level 3 ( sequence) start
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , inner_select.tc_company_id , 'DO_TRANSFER' , inner_select.xfer_pick_pack , null , inner_select.o_facility_id
  from (
   -- level 2 ( count) start
  select tc_company_id , 'DO_TRANSFER' , count(o_facility_id) xfer_pick_pack , null ,  o_facility_id from
  -- level 1 ( data) start
 (
  select distinct orders.order_id , orders.tc_company_id , 'DO_TRANSFER' ,  null ,  orders.o_facility_id from orders orders , order_line_item oli  where

   orders.o_facility_id is not null and
   orders.do_status in (110,130,140,185) and
   orders.do_type = 60    and
   (
   (orders.delivery_options = '03')  or
   (orders.delivery_options = '02' and orders.destination_action = '01')
   )   and

   orders.has_import_error=0 and
   orders.order_id =  oli.order_id and
   oli.allocation_source ='10' and
   oli.do_dtl_status!=200 and
   oli.order_id=orders.order_id
    )
    -- level 1 ( data) end
   group by tc_company_id ,  o_facility_id
     -- level 2 ( count) end
     )
inner_select
-- level 3 ( sequence) end
-- level 4 ( insert) end
;

-- 'RTNS_PICK_PACK
-- Fulfilment->Ship to location->Returns->Pick/Pack
-- level 4 ( insert) end
insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , user_id , facility_id )
-- level 3 ( sequence) start
select sysdate , sysdate , SEQ_MENU_COUNT_ID.nextval , inner_select.tc_company_id , 'DO_RETURNS' , inner_select.xfer_pick_pack , null , inner_select.o_facility_id
  from
  (
  -- level 2(count) start
   select tc_company_id , 'DO_RETURNS' , count(o_facility_id) xfer_pick_pack ,  o_facility_id from
-- level 1 (list) start
   (
  select distinct  orders.order_id ,  orders.tc_company_id , 'DO_RETURNS' , null ,  orders.o_facility_id from orders orders , order_line_item oli
  where

orders.o_facility_id is not null and
orders.do_status in ( 110 , 130 , 140 , 185  ) and
orders.do_type = 40 and

orders.has_import_error=0 and
orders.order_id =  oli.order_id and
oli.allocation_source ='10' and
oli.do_dtl_status!=200
)
-- level 1(list) end
group by tc_company_id , o_facility_id

-- level 2( count) end
  )  inner_select
  -- level 3 ( sequence) end
  -- level 4 ( insert) end


  ;

--Inventory-> Cycle Count
  -- CYC_COUNT
  insert into menu_count ( created_dttm , last_updated_dttm , menu_count_id , company_id , permission_code , menu_count , user_id , facility_id )  (SELECT sysdate ,
  sysdate ,
  SEQ_MENU_COUNT_ID.nextval ,
  count_inner_select.tc_company_id ,
  'CYCLE_COUNT' ,
  count_inner_select.cyccnt_count ,
  count_inner_select.assigned_user_id ,
  NULL
FROM
  (SELECT tc_company_id ,
    'CYCLE_COUNT' ,
    COUNT(assigned_user_id) cyccnt_count ,
    assigned_user_id ,
    NULL
  FROM
    (SELECT tc_company_id,
      cycle_Count_Request_Id,
      tc_Cycle_Count_Request_Id,
      cycle_count_request_name,
      cycle_Count_Request_Store_Id,
      scheduled_Dttm,
      complete_By_Dttm,
      earliest_start_by_dttm,
      CYCLE_COUNT_REQUEST_STATUS,
      assigned_User_Id
    FROM
      (SELECT tc_company_id,
        cycle_Count_Request_Id,
        tc_Cycle_Count_Request_Id,
        cycle_count_request_name,
        cycle_Count_Request_Store_Id,
        scheduled_Dttm,
        complete_By_Dttm,
        earliest_start_by_dttm,
        CYCLE_COUNT_REQUEST_STATUS,
        assigned_User_Id
      FROM
        (SELECT ccr.tc_company_id ,
          ccr.cycle_Count_Request_Id,
          ccr.tc_Cycle_Count_Request_Id,
          CCR.cycle_count_request_name,
          ccrs.cycle_Count_Request_Store_Id,
          ccrs.scheduled_Dttm,
          ccrs.complete_By_Dttm,
          ccrs.earliest_start_by_dttm,
          ccrs.CYCLE_COUNT_REQUEST_STATUS,
          nvl(ccia.assigned_User_Id,-1) assigned_User_Id,
          DECODE(ccia.cycle_count_assign_status,30,30,NULL) assign_status,
          RANK() OVER (PARTITION BY ccr.TC_CYCLE_COUNT_REQUEST_ID ORDER BY ccia.assigned_User_Id ) datarank
        FROM Cycle_Count_Request ccr
        LEFT JOIN cycle_Count_Request_Store ccrs
        ON ccrs.cycle_count_request_id = ccr.cycle_count_request_id
        LEFT JOIN cycle_Count_Items cci
        ON cci.cycle_count_request_store_id = ccrs.cycle_count_request_store_id
        LEFT JOIN cycle_Count_Item_Assignment ccia
        ON ccia.cycle_count_item_id          = cci.cycle_count_item_id
        WHERE ccr.is_Cancelled               = 0
        AND ccrs.CYCLE_COUNT_REQUEST_STATUS != 40
        AND ccrs.CYCLE_COUNT_REQUEST_STATUS !=30
        AND cci.CYCLE_COUNT_ITEM_STATUS!     =40
        AND cci.CYCLE_COUNT_ITEM_STATUS!     =60
        AND cci.CYCLE_COUNT_ITEM_STATUS!     =70
        AND ( ( ccrs.scheduled_dttm         >= sysdate -1
        AND ccrs.scheduled_dttm              < sysdate )
        OR ( ccrs.scheduled_dttm            IS NULL
        AND ccrs.earliest_start_by_dttm      < sysdate
        AND ccrs.complete_by_dttm           >= sysdate -1 ) )
        GROUP BY ccr.tc_company_id ,
          ccrs.facility_Id ,
          nvl(ccia.assigned_User_Id,-1) ,
          ccr.cycle_Count_Request_Id,
          ccr.tc_Cycle_Count_Request_Id,
          ccr.cycle_count_request_name,
          ccrs.cycle_Count_Request_Store_Id,
          ccrs.scheduled_Dttm,
          ccrs.complete_By_Dttm,
          ccrs.earliest_start_by_dttm,
          ccrs.CYCLE_COUNT_REQUEST_STATUS,
          ccia.assigned_User_Id,
          ccia.cycle_count_assign_status
        )
      WHERE datarank     = 1
      AND assign_status IS NULL
      GROUP BY tc_company_id,
        cycle_Count_Request_Id,
        tc_Cycle_Count_Request_Id,
        cycle_count_request_name,
        cycle_Count_Request_Store_Id,
        scheduled_Dttm,
        complete_By_Dttm,
        earliest_start_by_dttm,
        CYCLE_COUNT_REQUEST_STATUS,
        assigned_User_Id
      ORDER BY complete_By_Dttm ASC
      )
    )
  GROUP BY tc_company_id ,
    assigned_user_id
  ) count_inner_select
);

  --Adds the EXTERNAL counts to the user level counts
  update menu_count m1 set m1.menu_count =
( nvl(m1.menu_count,0) +
nvl((select nvl(m2.menu_count,0) from menu_count m2 where m2.company_id = m1.company_id and m1.facility_id=m2.facility_id and  m2.PERMISSION_CODE = m1.PERMISSION_CODE and m2.LAST_UPDATED_SOURCE = 'EXTERNAL'),0)
)
where m1.PERMISSION_CODE in (  'DO_CUST_ORDER_PICKUPS' , 'DO_SHIP_TO_CUST_PICK' , 'DO_SHIP_TO_LOCN_PICK'  )  and m1.LAST_UPDATED_SOURCE <> 'EXTERNAL';

--Adds the no-user assigned cycle counts to the user level counts ( user_id = -1)  
UPDATE menu_count m1
SET m1.menu_count = ( NVL(m1.menu_count,0) + NVL(
  (SELECT NVL(m2.menu_count,0)
  FROM menu_count m2
  WHERE m2.company_id    = m1.company_id
  AND m2.PERMISSION_CODE = m1.PERMISSION_CODE
  AND m2.user_id         = -1
  ),0) )
WHERE m1.PERMISSION_CODE   IN ( 'CYCLE_COUNT' )
AND m1.user_id           <> -1 ;


commit;

END PRC_MENU_COUNT_WINMOBILE;
/



-- DBTicket EEM-5597

CREATE OR REPLACE TRIGGER MOBILE_TRANSACTION_MESSAGE_TR
   AFTER INSERT
   ON MOBILE_TRANSACTION_MESSAGE
   REFERENCING NEW AS NEW
   FOR EACH ROW
   WHEN (NEW.MESSAGE_ID = 1850019 AND NEW.OBJECT_TYPE IN (11, 13))  --1850019 is Queued for Processing and ObjectType is DO/CycleCount
DECLARE
   V_OPERATION         MOBILE_TRANSACTION_LOG.OPERATION%TYPE;
   V_USER_ID           MOBILE_SESSION.USER_ID%TYPE;
   V_USER_NAME         UCL_USER.USER_NAME%TYPE;
   V_PERMISSION_CODE   MENU_COUNT.PERMISSION_CODE%TYPE;
   V_DO_TYPE           ORDERS.DO_TYPE%TYPE;
   V_FACILITY_ID       ORDERS.O_FACILITY_ID%TYPE;
   V_MOBILE_SESSION_ID    MOBILE_TRANSACTION_LOG.MOBILE_SESSION_ID%TYPE ;
BEGIN
   SELECT OPERATION, MOBILE_SESSION_ID
     INTO V_OPERATION, V_MOBILE_SESSION_ID
     FROM MOBILE_TRANSACTION_LOG
    WHERE MOBILE_TRANSACTION_LOG_ID = :NEW.MOBILE_TRANSACTION_LOG_ID;
    
   SELECT FACILITY_ID, USER_ID INTO V_FACILITY_ID, V_USER_ID FROM MOBILE_SESSION WHERE MOBILE_SESSION_ID = V_MOBILE_SESSION_ID;

   SELECT USER_NAME INTO V_USER_NAME FROM UCL_USER WHERE UCL_USER_ID = V_USER_ID	 ;

   IF :NEW.OBJECT_TYPE = 11  --OBJECT_TYPE: 11-DO
   THEN
      
      SELECT DO_TYPE INTO V_DO_TYPE FROM ORDERS WHERE ORDER_ID = :NEW.OBJECT_ID ;     
       
       IF V_DO_TYPE=20 THEN  --DO TYPE: 20-Customer Order
          IF V_OPERATION = 10 THEN   --OPERATION: 10-Pack
            --CO Pack
            V_PERMISSION_CODE :='DO_CUST_ORDER';
          ELSIF V_OPERATION = 24 THEN  --OPERATION:  24-Pick
            
            V_PERMISSION_CODE :='DO_CUST_ORDER_PICKUPS';
          END IF;
      ELSIF V_DO_TYPE=60 THEN   --DO TYPE: 60-Store Transfer
          IF V_OPERATION = 10 THEN   --OPERATION: 10-Pack
            
            V_PERMISSION_CODE :='DO_TRANSFER';
          ELSIF V_OPERATION = 24 THEN  --OPERATION:  24-Pick
            
            V_PERMISSION_CODE :='DO_SHIP_TO_LOCN_PICK';
           END IF;
      END IF;

      
	  IF V_PERMISSION_CODE IS NOT NULL THEN
		
		UPDATE MENU_COUNT SET MENU_COUNT = MENU_COUNT -1
		WHERE	FACILITY_ID = V_FACILITY_ID 
		AND PERMISSION_CODE = V_PERMISSION_CODE	
		AND (LAST_UPDATED_SOURCE = V_USER_NAME OR LAST_UPDATED_SOURCE = 'External' OR LAST_UPDATED_SOURCE IS NULL)	
		AND 	ROWNUM <= 1;
		
	  END IF;

	ELSIF :NEW.OBJECT_TYPE = 13 AND V_OPERATION = 18 THEN
		--is tran log inserted for anything other than save/submit
		-- Cycle Count Save/Submit
		V_PERMISSION_CODE :='CYCLE_COUNT';
		
		UPDATE MENU_COUNT SET MENU_COUNT = MENU_COUNT -1 
		WHERE FACILITY_ID = V_FACILITY_ID 
		AND	PERMISSION_CODE = V_PERMISSION_CODE
		AND	(LAST_UPDATED_SOURCE = V_USER_NAME OR LAST_UPDATED_SOURCE = 'External' OR LAST_UPDATED_SOURCE IS NULL)
		AND	ROWNUM <= 1;
		
	END IF;


DELETE FROM MENU_COUNT
      WHERE MENU_COUNT <= 0 
	    AND PERMISSION_CODE = V_PERMISSION_CODE
      AND FACILITY_ID=V_FACILITY_ID;
      
EXCEPTION 
 WHEN NO_DATA_FOUND THEN NULL;
END;
/

-- DBTicket SIF-520


DELETE FROM XSCREEN_LABEL WHERE LABEL_BUNDLE_NAME='ErrorMessage' AND XSCREEN_ID=9000128 AND LABEL_KEY IN  (
'1280001', '1280002', '1280003', '1280004', '1280005', '1280006', '1280007', '1280008', '1280009', '1280010', '1280011', '1280012', '1280013', '1280014', '1280015', '1280016', '1280017', '1280018', '1280019', '1280020', 
'1280021', '1280022', '1280023', '1280024', '1280025', '1280026', '1280027', '1280028', '1280029', '1280030', '1280031', '1280032', '1280033', '1280034', '1280035', '1280036', '1280037', '1280038', '1280039', '1280040', 
'1280041', '1280042', '1280043', '1280044', '1280045', '1280046', '1280047', '1280048', '1280049', '1280050', '1280051', '1280052', '1280053', '1280054', '1280055', '1280056', '1280057', '1280058', '1280059', '1280060', 
'1280061', '1280062', '1280063', '1280064', '1280065', '1280066', '1280067', '1280068', '1280069', '1280070', '1280071', '1280072', '1280073', '1280074', '1280075', '1280076', '1280077', '1280078', '1280079', '1280080', 
'1280081', '1280082', '1280083', '1280084', '1280085');

DELETE FROM LABEL WHERE BUNDLE_NAME='ErrorMessage' AND KEY IN (
'1280001', '1280002', '1280003', '1280004', '1280005', '1280006', '1280007', '1280008', '1280009', '1280010', '1280011', '1280012', '1280013', '1280014', '1280015', '1280016', '1280017', '1280018', '1280019', '1280020', 
'1280021', '1280022', '1280023', '1280024', '1280025', '1280026', '1280027', '1280028', '1280029', '1280030', '1280031', '1280032', '1280033', '1280034', '1280035', '1280036', '1280037', '1280038', '1280039', '1280040', 
'1280041', '1280042', '1280043', '1280044', '1280045', '1280046', '1280047', '1280048', '1280049', '1280050', '1280051', '1280052', '1280053', '1280054', '1280055', '1280056', '1280057', '1280058', '1280059', '1280060', 
'1280061', '1280062', '1280063', '1280064', '1280065', '1280066', '1280067', '1280068', '1280069', '1280070', '1280071', '1280072', '1280073', '1280074', '1280075', '1280076', '1280077', '1280078', '1280079', '1280080', 
'1280081', '1280082', '1280083', '1280084', '1280085');


DELETE from MESSAGE_DISPLAY where disp_key in  (
'1280001', '1280002', '1280003', '1280004', '1280005', '1280006', '1280007', '1280008', '1280009', '1280010', '1280011', '1280012', '1280013', '1280014', '1280015', '1280016', '1280017', '1280018', '1280019', '1280020', 
'1280021', '1280022', '1280023', '1280024', '1280025', '1280026', '1280027', '1280028', '1280029', '1280030', '1280031', '1280032', '1280033', '1280034', '1280035', '1280036', '1280037', '1280038', '1280039', '1280040', 
'1280041', '1280042', '1280043', '1280044', '1280045', '1280046', '1280047', '1280048', '1280049', '1280050', '1280051', '1280052', '1280053', '1280054', '1280055', '1280056', '1280057', '1280058', '1280059', '1280060', 
'1280061', '1280062', '1280063', '1280064', '1280065', '1280066', '1280067', '1280068', '1280069', '1280070', '1280071', '1280072', '1280073', '1280074', '1280075', '1280076', '1280077', '1280078', '1280079', '1280080', 
'1280081', '1280082', '1280083', '1280084', '1280085');


DELETE FROM MESSAGE_MASTER WHERE ILS_MODULE='sca' AND KEY IN (
'1280001', '1280002', '1280003', '1280004', '1280005', '1280006', '1280007', '1280008', '1280009', '1280010', '1280011', '1280012', '1280013', '1280014', '1280015', '1280016', '1280017', '1280018', '1280019', '1280020', 
'1280021', '1280022', '1280023', '1280024', '1280025', '1280026', '1280027', '1280028', '1280029', '1280030', '1280031', '1280032', '1280033', '1280034', '1280035', '1280036', '1280037', '1280038', '1280039', '1280040', 
'1280041', '1280042', '1280043', '1280044', '1280045', '1280046', '1280047', '1280048', '1280049', '1280050', '1280051', '1280052', '1280053', '1280054', '1280055', '1280056', '1280057', '1280058', '1280059', '1280060', 
'1280061', '1280062', '1280063', '1280064', '1280065', '1280066', '1280067', '1280068', '1280069', '1280070', '1280071', '1280072', '1280073', '1280074', '1280075', '1280076', '1280077', '1280078', '1280079', '1280080', 
'1280081', '1280082', '1280083', '1280084', '1280085');



INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280001','1280001', 'sca', NULL, 'Your advanced search returned {0} result(s)', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280002','1280002', 'sca', NULL, '{0} Cycle count(s) in total', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280004','1280004', 'sca', NULL, 'UPC not found!', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280006','1280006', 'sca', NULL, '{0} selected', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280007','1280007', 'sca', NULL, '{0} Order(s) found', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280008','1280008', 'sca', NULL, '{0} Item(s) in {1} package(s)', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280009','1280009', 'sca', NULL, '{0} Item(s) in total', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280010','1280010', 'sca', NULL, 'Received {0} item(s) in total', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280011','1280011', 'sca', NULL, '{0} Order(s) in total', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280012','1280012', 'sca', NULL, '{0} Order(s) selected', 'ErrorMessage',NULL,NULL,NULL,NULL);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280013','1280013', 'sca', NULL, '{0} Item(s) have been picked', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280017','1280017', 'sca', NULL, 'Short pending {0} unit(s)', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280018','1280018', 'sca', NULL, '{0} Item(s) picked', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280019','1280019', 'sca', NULL, '{0} Item(s) shorted', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280020','1280020', 'sca', NULL, '{0} Item(s) pending', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280023','1280023', 'sca', NULL, '{0} Item(s) successfully shorted', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280024','1280024', 'sca', NULL, 'All pending {0} item(s) will be shorted', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280025','1280025', 'sca', NULL, '{0} Shipment(s) in progress', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280026','1280026', 'sca', NULL, 'Shipment {0} selected', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280027','1280027', 'sca', NULL, '{0} Item(s) received', 'ErrorMessage',NULL,NULL,NULL,NULL);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280029','1280029', 'sca', NULL, 'Package destined to another store!', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280031','1280031', 'sca', NULL, 'Activity is successfully saved', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280032','1280032', 'sca', NULL, 'Pick list sent to printer', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280033','1280033', 'sca', NULL, 'Scan an item to start an order packing', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280034','1280034', 'sca', NULL, 'No order packed', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280035','1280035', 'sca', NULL, 'Documents sent to printer', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280036','1280036', 'sca', NULL, 'Item not found', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280038','1280038', 'sca', NULL, 'No internet connection', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280040','1280040', 'sca', NULL, 'No data found', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280041','1280041', 'sca', NULL, 'Scan failure', 'ErrorMessage',NULL,NULL,NULL,NULL);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280042','1280042', 'sca', NULL, 'Save failure', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280043','1280043', 'sca', NULL, 'Cannot connect to the server', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280044','1280044', 'sca', NULL, 'If a different user logs in all the saved data would be lost. Logout?', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280045','1280045', 'sca', NULL, 'Activity is successfully deleted', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280046','1280046', 'sca', NULL, 'UPC {0} was not found', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280048','1280048', 'sca', NULL, 'Its no longer required to count', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280050','1280050', 'sca', NULL, 'Delete item scan?', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280051','1280051', 'sca', NULL, 'Item removed from scans', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280052','1280052', 'sca', NULL, 'Choose adjustment type before scanning items to adjust', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280053','1280053', 'sca', NULL, 'Activity is saved', 'ErrorMessage',NULL,NULL,NULL,NULL);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280054','1280054', 'sca', NULL, 'Store order pickedup', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280057','1280057', 'sca', NULL, 'Item has not been ordered', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280058','1280058', 'sca', NULL, 'Cannot pick more than ordered', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280059','1280059', 'sca', NULL, 'No of orders exceeded!', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280062','1280062', 'sca', NULL, 'All the data saved during activity would be lost. Delete?', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280064','1280064', 'sca', NULL, 'UPC / Item Id is required', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280065','1280065', 'sca', NULL, 'Are you sre you want to remove the item from Scans?', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280066','1280066', 'sca', NULL, 'Please scan atleast one item before proceeding to include this item', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280070','1280070', 'sca', NULL, '{0} ASN(s) in total', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280071','1280071', 'sca', NULL, '{0} ASN(s) selected', 'ErrorMessage',NULL,NULL,NULL,NULL);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280072','1280072', 'sca', NULL, '{0} ASN(s) received', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280073','1280073', 'sca', NULL, 'Picked quantity exceeded the ordered quantity', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280075','1280075', 'sca', NULL, '{0} Item(s) packed', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280076','1280076', 'sca', NULL, 'You will be now packing items for this order', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280077','1280077', 'sca', NULL, 'The items will be added to following package', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280080','1280080', 'sca', NULL, 'Cannot pack more than ordered', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280082','1280082', 'sca', NULL, '{0} Package(s) loaded', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280083','1280083', 'sca', NULL, 'Shipment cannot be processed!', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280084','1280084', 'sca', NULL, 'Selected shipment is already completed', 'ErrorMessage',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, OVRIDE_ROLE, LOG_FLAG)  VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL, '1280085','1280085', 'sca', NULL, 'No packages found for shipping', 'ErrorMessage',NULL,NULL,NULL,NULL);

MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280001' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280001','Your advanced search returned {0} result(s)','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280002' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280002','{0} Cycle count(s) in total','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280004' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280004','UPC not found!','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280006' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280006','{0} selected','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280007' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280007','{0} Order(s) found','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280008' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280008','{0} Item(s) in {1} package(s)','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280009' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280009','{0} Item(s) in total','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280010' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280010','Received {0} item(s) in total','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280011' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280011','{0} Order(s) in total','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280012' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280012','{0} Order(s) selected','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280013' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280013','{0} Item(s) have been picked','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280017' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280017','Short pending {0} unit(s)','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280018' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280018','{0} Item(s) picked','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280019' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280019','{0} Item(s) shorted','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280020' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280020','{0} Item(s) pending','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280023' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280023','{0} Item(s) successfully shorted','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280024' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280024','All pending {0} item(s) will be shorted','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280025' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280025','{0} Shipment(s) in progress','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280026' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280026','Shipment {0} selected','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280027' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280027','{0} Item(s) received','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280029' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280029','Package destined to another store!','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280031' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280031','Activity is successfully saved','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280032' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280032','Pick list sent to printer','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280033' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280033','Scan an item to start an order packing','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280034' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280034','No order packed','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280035' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280035','Documents sent to printer','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280036' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280036','Item not found','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280038' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280038','No internet connection','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280040' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280040','No data found','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280041' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280041','Scan failure','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280042' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280042','Save failure','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280043' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280043','Cannot connect to the server','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280044' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280044','If a different user logs in all the saved data would be lost. Logout?','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280045' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280045','Activity is successfully deleted','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280046' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280046','UPC {0} was not found','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280048' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280048','Its no longer required to count','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280050' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280050','Delete item scan?','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280051' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280051','Item removed from scans','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280052' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280052','Choose adjustment type before scanning items to adjust','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280053' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280053','Activity is saved','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280054' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280054','Store order pickedup','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280057' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280057','Item has not been ordered','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280058' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280058','Cannot pick more than ordered','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280059' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280059','No of orders exceeded!','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280062' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280062','All the data saved during activity would be lost. Delete?','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280064' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280064','UPC / Item Id is required','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280065' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280065','Are you sre you want to remove the item from Scans?','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280066' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280066','Please scan atleast one item before proceeding to include this item','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280070' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280070','{0} ASN(s) in total','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280071' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280071','{0} ASN(s) selected','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280072' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280072','{0} ASN(s) received','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280073' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280073','Picked quantity exceeded the ordered quantity','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280075' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280075','{0} Item(s) packed','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280076' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280076','You will be now packing items for this order','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280077' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280077','The items will be added to following package','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280080' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280080','Cannot pack more than ordered','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280082' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280082','{0} Package(s) loaded','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280083' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280083','Shipment cannot be processed!','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280084' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280084','Selected shipment is already completed','ErrorMessage');
MERGE INTO LABEL L USING (SELECT 'ErrorMessage' BUNDLE_NAME,'1280085' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'1280085','No packages found for shipping','ErrorMessage');


INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280001');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280002');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280004');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280006');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280007');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280008');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280009');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280010');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280011');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280012');

INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280013');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280017');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280018');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280019');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280020');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280023');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280024');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280025');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280026');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280027');

INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280029');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280031');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280032');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280033');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280034');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280035');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280036');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280038');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280040');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280041');

INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280042');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280043');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280044');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280045');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280046');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280048');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280050');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280051');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280052');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280053');

INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280054');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280057');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280058');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280059');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280062');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280064');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280065');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280066');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280070');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280071');

INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280072');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280073');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280075');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280076');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280077');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280080');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280082');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280083');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280084');
INSERT INTO XSCREEN_LABEL (XSCREEN_LABEL_ID,XSCREEN_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,9000128,'ErrorMessage','1280085');

MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'ScanItems' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ScanItems','Scan Items','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'CustomerPickup' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'CustomerPickup','Customer Pickup','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'StoreStock' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'StoreStock','Store Stock','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'Noactivitiesfound' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Noactivitiesfound','No activities found','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'Nocyclecountfound' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Nocyclecountfound','No cycle count found','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'UserName' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'UserName','User Name','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'OrdersWithNoShorting' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'OrdersWithNoShorting','Orders With No Shorting','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'ShortAll' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ShortAll','Short All','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'ServerError' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ServerError','Server Error','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'Error' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Error','Error','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'ThePackageDetailsAreNotAvailable' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ThePackageDetailsAreNotAvailable','Package details are not available','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'ActivityHasNotYetBeenCreated' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ActivityHasNotYetBeenCreated','Activity has not yet been created','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'ORDERPICKLIST' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ORDERPICKLIST','ORDER PICKLIST','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'FinishPackageId' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'FinishPackageId','Finish package id','SCA');
MERGE INTO LABEL L USING (SELECT 'SCA' BUNDLE_NAME,'Orderpending' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Orderpending','Order pending!','SCA');


MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'ScanItems' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'ScanItems',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'CustomerPickup' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'CustomerPickup',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'StoreStock' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'StoreStock',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Noactivitiesfound' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Noactivitiesfound',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Nocyclecountfound' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Nocyclecountfound',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'UserName' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'UserName',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'OrdersWithNoShorting' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'OrdersWithNoShorting',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'ShortAll' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'ShortAll',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'ServerError' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'ServerError',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Error' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Error',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'ThePackageDetailsAreNotAvailable' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'ThePackageDetailsAreNotAvailable',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'ActivityHasNotYetBeenCreated' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'ActivityHasNotYetBeenCreated',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'ORDERPICKLIST' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'ORDERPICKLIST',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'FinishPackageId' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'FinishPackageId',
               'SCA');

MERGE INTO XSCREEN_LABEL L
     USING (SELECT 'Orderpending' LABEL_KEY,
                   9000128 XSCREEN_ID,
                   'SCA' LABEL_BUNDLE_NAME
              FROM DUAL) B
        ON (    L.LABEL_KEY = B.LABEL_KEY
            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
            AND L.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XSCREEN_LABEL_ID,
               XSCREEN_ID,
               LABEL_KEY,
               LABEL_BUNDLE_NAME)
       VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
               '9000128',
               'Orderpending',
               'SCA');
COMMIT;

UPDATE DB_BUILD_HISTORY SET END_DTTM=SYSDATE WHERE VERSION_LABEL='EEM_20131206.2013.1.18.10';

COMMIT;

exit; 
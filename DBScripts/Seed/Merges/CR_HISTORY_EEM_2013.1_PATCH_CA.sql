MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'EEM-666' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-107' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'MACR00859076' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'EEM-688' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'EEM-683' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-229' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-132' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'EEM-701' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-276' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-316' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'MACR00864710' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-362' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-399' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-438' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-439' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-440' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-465' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-467' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-520' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-553' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-564' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-502' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-644' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-698' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-705' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-718' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-719' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-760' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-788' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-781' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-799' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-811' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-835' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-836' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-837' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-838' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-872' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-896' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'MACR00863790' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'MACR00863612' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-9127' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-9180' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-9183' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'EEM-2280' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-658' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'EEM-4196' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-9096' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-9142' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-9213' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-9008' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-9272' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-927' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-932' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-902' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'EEM-4212' as CR_NUMBER,
  'EEM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-1038' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-1052' as CR_NUMBER,
  'DOM' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

COMMIT;

exit;

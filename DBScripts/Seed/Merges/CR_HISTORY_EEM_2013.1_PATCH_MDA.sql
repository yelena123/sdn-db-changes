MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'MACR00862915' as CR_NUMBER,
  'EEMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-455' as CR_NUMBER,
  'EEMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'SIF-855' as CR_NUMBER,
  'EEMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'MACR00864379' as CR_NUMBER,
  'EEMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

MERGE INTO DB_CR_HISTORY A USING
 (SELECT
  'DOM-9051' as CR_NUMBER,
  'EEMBASE' as DB_LAYER,
  '2013.1' as RELEASE_VERSION
  FROM DUAL) B
ON (A.CR_NUMBER = B.CR_NUMBER)
WHEN NOT MATCHED THEN 
INSERT (
  DB_CR_HISTORY_ID, CR_NUMBER, DB_LAYER, RELEASE_VERSION)
VALUES (
  SEQ_DB_CR_HISTORY_ID.nextval, B.CR_NUMBER, B.DB_LAYER, B.RELEASE_VERSION);

COMMIT;

exit;

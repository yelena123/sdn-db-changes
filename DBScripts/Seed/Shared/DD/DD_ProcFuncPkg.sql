SET DEFINE OFF;
SET ECHO ON;

CREATE OR REPLACE PROCEDURE DD_CHECK_TRIGGERS (P_RC OUT NUMBER)
AS
 DISABLED_COUNT   NUMBER;
BEGIN
 P_RC := 0;
 SELECT COUNT (*) INTO DISABLED_COUNT
   FROM (SELECT A.TABLE_NAME
           FROM DD_REPLICATED_TABLES A
          WHERE EXISTS
                   (SELECT B.TRIGGER_NAME
                      FROM user_triggers B
                     WHERE B.TRIGGER_NAME = A.DD_TRIG_NAME
                           AND B.STATUS = 'DISABLED')
         UNION
         SELECT A.TABLE_NAME
           FROM DD_REPLICATED_TABLES A
          WHERE NOT EXISTS
                   (SELECT B.TRIGGER_NAME
                      FROM user_triggers B
                     WHERE B.TRIGGER_NAME = A.DD_TRIG_NAME));
 IF DISABLED_COUNT > 0
 THEN
    P_RC := -1;
 END IF;
END;
/

CREATE OR REPLACE PROCEDURE DD_PURGE_SYNC_LOG (
 V_NO_OF_DAYS    IN NUMBER,
 V_SUBCRB_NAME   IN VARCHAR2,
 V_STATUS        IN VARCHAR2)
AS
 TYPE CURSOR_TYPE IS REF CURSOR;
 st_cursor       CURSOR_TYPE;
 v_row_count     PLS_INTEGER := 0;
 V_SQL           VARCHAR2 (2000);
 TYPE dd_tran_type IS RECORD
(tran_id number,
 sub_id number
 );
 dd_tran_rec  dd_tran_type;
 TYPE TRAN_T IS TABLE OF dd_tran_rec%type
                   INDEX BY BINARY_INTEGER;
 V_TRAN_ARR      TRAN_T;
 TYPE TRAN_ID_T IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
 V_TRAN_ID_ARR   TRAN_ID_T;
l_start  NUMBER;
l_end NUMBER;
BEGIN
 l_start := DBMS_UTILITY.get_time;
DBMS_OUTPUT.PUT_LINE ('Purge Data Sync start time:' || SYSTIMESTAMP);
 V_SQL :=
    'SELECT TRANSACTION_ID,subscriber_id
                       FROM  DD_SYNC_STATUS  WHERE LAST_UPDATED_DTTM < (SYSDATE - '
    || V_NO_OF_DAYS
    || ')';
 IF V_SUBCRB_NAME IS NOT NULL
 THEN
    V_SQL :=
       V_SQL || ' AND SUBSCRIBER_ID IN (SELECT SUBSCRIBER_ID FROM DD_SUBSCRIBER WHERE SUBSCRIBER_NAME IN ' || '''' || V_SUBCRB_NAME || ''''||')';
 END IF;
 IF V_STATUS IS NOT NULL
 THEN
    V_SQL := V_SQL || ' AND STATUS IN (''' ||  V_STATUS || ''')';
 END IF;
 OPEN st_cursor FOR V_SQL;
 LOOP
    FETCH st_cursor
    BULK COLLECT INTO V_TRAN_ARR
    LIMIT 20000;
    EXIT WHEN v_row_count = st_cursor%ROWCOUNT;
    v_row_count := st_cursor%ROWCOUNT;
    FORALL i IN 1 .. V_TRAN_ARR.COUNT
       INSERT INTO DD_TRAN_GTT
            VALUES (V_TRAN_ARR (i).tran_id,V_TRAN_ARR (i).sub_id);
    COMMIT;
 INSERT INTO DD_ERR_LOG_DTL_GTT(ERROR_LOG_DTL_ID)  ( SELECT DELD.ERROR_LOG_DETAIL_ID
                                                                 FROM DD_ERROR_LOG DEL,DD_TRAN_GTT DTGT,DD_ERROR_LOG_DETAIL DELD
                                                                 WHERE DELD.ERROR_LOG_ID = DEL.ERROR_LOG_ID AND DEL.TRANSACTION_ID = DTGT.TRANID AND DEL.SUBSCRIBER_ID = DTGT.SUBID);
DELETE FROM DD_ERROR_LOG_DETAIL_SQL
    WHERE ERROR_LOG_DETAIL_ID IN
             (SELECT ERROR_LOG_DTL_ID FROM DD_ERR_LOG_DTL_GTT);
 COMMIT;
 DELETE FROM DD_ERROR_LOG_DETAIL
             WHERE ERROR_LOG_DETAIL_ID IN (SELECT ERROR_LOG_DTL_ID  FROM DD_ERR_LOG_DTL_GTT);
     COMMIT;
DELETE FROM DD_ERROR_LOG
             WHERE (TRANSACTION_ID,SUBSCRIBER_ID) IN (SELECT TRANID,SUBID FROM DD_TRAN_GTT);
  COMMIT;
     END LOOP;
 IF V_SUBCRB_NAME IS NULL
THEN
 INSERT INTO DD_SEQ_GTT
 (SELECT do.sequence_id
    FROM DD_TRAN_GTT DT, DD_OUTBOUND_STAGING_DATA DO,DD_OUTBOUND_STAGING_DATA_DTL DOSDD
   WHERE DOSDD.SEQUENCE_ID = DO.SEQUENCE_ID
         AND DT.TRANID = DO.TRANSACTION_ID);
DELETE FROM DD_OUTBOUND_STAGING_DATA_DTL
          WHERE SEQUENCE_ID IN (SELECT SEQUENCE_ID FROM DD_SEQ_GTT);
     COMMIT;
    DELETE FROM DD_OUTBOUND_STAGING_DATA DOSD
          WHERE EXISTS
                    (SELECT 1 FROM DD_TRAN_GTT DTG
                        WHERE DOSD.TRANSACTION_ID = DTG.tranid);
       COMMIT;
 ELSE
INSERT INTO DD_TRAN_ID_GTT SELECT DS.TRANSACTION_ID
        FROM DD_TRAN_GTT DT, DD_SYNC_STATUS DS
       WHERE DT.TRANID = DS.TRANSACTION_ID
    GROUP BY DS.TRANSACTION_ID
      HAVING COUNT (*) <=1;
COMMIT;
 INSERT INTO DD_SEQ_GTT (SELECT DSDL.sequence_id
                         FROM DD_OUTBOUND_STAGING_DATA DOSD,DD_TRAN_ID_GTT DD_TRAN,DD_OUTBOUND_STAGING_DATA_DTL DSDL
                        WHERE DSDL.SEQUENCE_ID = DOSD.SEQUENCE_ID AND DOSD.TRANSACTION_ID = DD_TRAN.TRAN_ID );
COMMIT;
DELETE FROM DD_OUTBOUND_STAGING_DATA_DTL WHERE SEQUENCE_ID IN (SELECT SEQUENCE_ID FROM DD_SEQ_GTT);
COMMIT;
DELETE FROM DD_OUTBOUND_STAGING_DATA
             WHERE TRANSACTION_ID IN (SELECT TRAN_ID FROM DD_TRAN_ID_GTT);
COMMIT;
END IF;
INSERT INTO DD_TRAN_GTT(TRANID,SUBID)  select  transaction_id,SUBSCRIBER_ID
from dd_sync_status a
where not exists  (select 1 from dd_subscriber b where a.SUBSCRIBER_ID = b.SUBSCRIBER_ID );
COMMIT;
DELETE FROM DD_SYNC_STATUS
          WHERE (TRANSACTION_ID,SUBSCRIBER_ID) IN (SELECT TRANID,SUBID FROM DD_TRAN_GTT);
COMMIT;
 l_end := DBMS_UTILITY.get_time;
DBMS_OUTPUT.PUT_LINE ('Purge Data Sync end time:' || SYSTIMESTAMP);
DBMS_OUTPUT.put_line('Elapsed Time: ' ||round( (l_end - l_start)/100, 2 ) || ' secs');
EXCEPTION
 WHEN OTHERS
 THEN
    DBMS_OUTPUT.PUT_LINE (SQLERRM);
END;
/

CREATE OR REPLACE PACKAGE DATA_INTG_PKG
AS
 O_DATA   CLOB := ' ';
 PROCEDURE CLEAR_VAL;
 PROCEDURE CHECK_VAL (L_CNAME   IN VARCHAR2,
                      L_OLD     IN VARCHAR2,
                      L_NEW     IN VARCHAR2);
 PROCEDURE CHECK_VAL (L_CNAME IN VARCHAR2, L_OLD IN DATE, L_NEW IN DATE);
 PROCEDURE CHECK_VAL (L_CNAME   IN VARCHAR2,
                      L_OLD     IN TIMESTAMP,
                      L_NEW     IN TIMESTAMP);
 PROCEDURE CHECK_VAL (L_CNAME   IN VARCHAR2,
                      L_OLD     IN NUMBER,
                      L_NEW     IN NUMBER);
 FUNCTION GET_VAL
    RETURN VARCHAR2;
 FUNCTION IS_DATA_CHANGED
    RETURN CHAR;
 FUNCTION GET_TIMESTAMP_UTC_MILLIS (I_DATA TIMESTAMP)
    RETURN NUMBER;
 FUNCTION GET_DATE_UTC_MILLIS (I_DATA DATE)
    RETURN NUMBER;
 PROCEDURE GET_PK_DATA_STRING (I_COLUMN_NAME     VARCHAR2,
                               I_COLUMN_VALUE    VARCHAR2);
 PROCEDURE GET_PK_DATA_STRING (I_COLUMN_NAME     VARCHAR2,
                               I_COLUMN_VALUE    NUMBER);
 PROCEDURE GET_PK_DATA_STRING (I_COLUMN_NAME VARCHAR2, I_COLUMN_VALUE DATE);
 PROCEDURE GET_PK_DATA_STRING (I_COLUMN_NAME     VARCHAR2,
                               I_COLUMN_VALUE    TIMESTAMP);
 PROCEDURE ADD_TO_OUTBOUND_STAGING (I_ACTION_TYPE    VARCHAR2,
                                    I_TABLE_NAME     VARCHAR2);
END;
/

CREATE OR REPLACE PACKAGE BODY DATA_INTG_PKG
AS
 PROCEDURE CLEAR_VAL
 IS
 BEGIN
              IF (O_DATA <> ' ') THEN
                              O_DATA := ' ';
              END IF;
 END;
 PROCEDURE CHECK_VAL (
    L_CNAME      IN   VARCHAR2,
    L_OLD        IN   VARCHAR2,
    L_NEW        IN   VARCHAR2
 )
 IS
  CHANGE_DATA VARCHAR2(4000) := ' ';
 BEGIN
    IF (   L_NEW <> L_OLD
        OR (L_NEW IS NULL AND L_OLD IS NOT NULL)
        OR (L_NEW IS NOT NULL AND L_OLD IS NULL)
       )
    THEN
      IF (L_NEW IS NULL) THEN
        CHANGE_DATA := '@NULL@';
      ELSE
        CHANGE_DATA := replace(L_NEW, ',', '\,');
        CHANGE_DATA := replace(CHANGE_DATA, '', '\');
        CHANGE_DATA := replace(CHANGE_DATA, '''', '''''');
      END IF;
      IF (O_DATA <> ' ') THEN
        O_DATA := O_DATA || ',' ;
      END IF;
      O_DATA := O_DATA || L_CNAME || '=' || CHANGE_DATA;
    END IF;
	  dbms_output.put_line( O_DATA);
 END;
 PROCEDURE CHECK_VAL (
    L_CNAME      IN   VARCHAR2,
    L_OLD        IN   DATE,
    L_NEW        IN   DATE
 )
 IS
  CHANGE_DATA VARCHAR2(4000) := ' ';
 BEGIN
    IF (   L_NEW <> L_OLD
        OR (L_NEW IS NULL AND L_OLD IS NOT NULL)
        OR (L_NEW IS NOT NULL AND L_OLD IS NULL)
       )
    THEN
      IF (O_DATA <> ' ') THEN
        O_DATA := O_DATA || ',' ;
      END IF;
      IF (L_NEW IS NULL) THEN
        O_DATA := O_DATA || L_CNAME || '=' || '@NULL@';
      ELSE
        O_DATA := O_DATA || L_CNAME || '=' || GET_DATE_UTC_MILLIS(L_NEW);
      END IF;
    END IF;
	  dbms_output.put_line( O_DATA);
 END;
 PROCEDURE CHECK_VAL (
    L_CNAME      IN   VARCHAR2,
    L_OLD        IN   TIMESTAMP,
    L_NEW        IN   TIMESTAMP
 )
 IS
 BEGIN
    IF (   L_NEW <> L_OLD
        OR (L_NEW IS NULL AND L_OLD IS NOT NULL)
        OR (L_NEW IS NOT NULL AND L_OLD IS NULL)
       )
    THEN
      IF (O_DATA <> ' ') THEN
        O_DATA := O_DATA || ',' ;
      END IF;
      IF (L_NEW IS NULL) THEN
        O_DATA := O_DATA || L_CNAME || '=' || '@NULL@';
      ELSE
        O_DATA := O_DATA || L_CNAME || '=' || GET_TIMESTAMP_UTC_MILLIS(L_NEW);
      END IF;
    END IF;
	  dbms_output.put_line( O_DATA);
 END;
 PROCEDURE CHECK_VAL (
    L_CNAME      IN   VARCHAR2,
    L_OLD        IN   NUMBER,
    L_NEW        IN   NUMBER
 )
 IS
 BEGIN
    IF (   L_NEW <> L_OLD
        OR (L_NEW IS NULL AND L_OLD IS NOT NULL)
        OR (L_NEW IS NOT NULL AND L_OLD IS NULL)
       )
    THEN
      IF (O_DATA <> ' ') THEN
        O_DATA := O_DATA || ',' ;
      END IF;
      IF (L_NEW IS NULL) THEN
        O_DATA := O_DATA || L_CNAME || '=' || '@NULL@';
      ELSE
        O_DATA := O_DATA || L_CNAME || '=' || L_NEW;
      END IF;
    END IF;
	  dbms_output.put_line( O_DATA);
 END;
 FUNCTION GET_VAL
 RETURN VARCHAR2
 IS
 BEGIN
    RETURN O_DATA;
 END;
 FUNCTION IS_DATA_CHANGED
 RETURN CHAR
 IS
   CHANGE_DATA CHAR := 'Y';
 BEGIN
    IF (O_DATA = ' ') THEN
      CHANGE_DATA := 'N';
    END IF;
    RETURN CHANGE_DATA;
 END;
 FUNCTION GET_TIMESTAMP_UTC_MILLIS (
    I_DATA TIMESTAMP
 )
 RETURN NUMBER
 IS
   UTC_MILLIS NUMBER := 0;
 BEGIN
    select (to_char(sys_extract_utc(I_DATA),'J') * 86400 * 1000 +
            to_char(sys_extract_utc(I_DATA),'HH24')* 3600 * 1000 +
            to_char(sys_extract_utc(I_DATA),'MI')* 60 * 1000 +
            to_char(sys_extract_utc(I_DATA),'SS') * 1000 +
            to_char(sys_extract_utc(I_DATA),'FF3') )
            - ( to_char(to_timestamp('01-JAN-1970 00:00:00.000000', 'DD-MON-YYYY HH24:MI:SSxFF'),'J') * 86400 * 1000 ) into UTC_MILLIS
    from dual;
    RETURN UTC_MILLIS;
 END;
 FUNCTION GET_DATE_UTC_MILLIS (
	I_DATA DATE
	)
 RETURN NUMBER
 IS
 UTC_MILLIS NUMBER := 0;
 BEGIN
	SELECT (I_DATA - TO_DATE('01-JAN-1970','DD-MON-YYYY')) * (24 * 60 * 60 * 1000) into UTC_MILLIS FROM DUAL;
 RETURN UTC_MILLIS;
 END;
 PROCEDURE GET_PK_DATA_STRING (
    I_COLUMN_NAME  VARCHAR2,
    I_COLUMN_VALUE VARCHAR2
 )
 IS
    TEMP_DATA VARCHAR2(4000) := 'PK@';
 BEGIN
      IF (O_DATA <> ' ') THEN
        O_DATA := O_DATA || ',' ;
      END IF;
      O_DATA := O_DATA || TEMP_DATA || I_COLUMN_NAME || '=' || I_COLUMN_VALUE;
 END;
 PROCEDURE GET_PK_DATA_STRING (
    I_COLUMN_NAME  VARCHAR2,
    I_COLUMN_VALUE NUMBER
 )
 IS
    TEMP_DATA VARCHAR2(4000) := 'PK@';
 BEGIN
      IF (O_DATA <> ' ') THEN
        O_DATA := O_DATA || ',' ;
      END IF;
      O_DATA := O_DATA || TEMP_DATA || I_COLUMN_NAME || '=' || I_COLUMN_VALUE;
 END;
 PROCEDURE GET_PK_DATA_STRING (
    I_COLUMN_NAME  VARCHAR2,
    I_COLUMN_VALUE DATE
 )
 IS
    TEMP_DATA VARCHAR2(4000) := 'PK@';
 BEGIN
      IF (O_DATA <> ' ') THEN
        O_DATA := O_DATA || ',' ;
      END IF;
      O_DATA := O_DATA || TEMP_DATA || I_COLUMN_NAME || '=' || I_COLUMN_VALUE;
 END;
 PROCEDURE GET_PK_DATA_STRING (
    I_COLUMN_NAME  VARCHAR2,
    I_COLUMN_VALUE TIMESTAMP
 )
 IS
    TEMP_DATA VARCHAR2(4000) := 'PK@';
 BEGIN
      IF (O_DATA <> ' ') THEN
        O_DATA := O_DATA || ',' ;
      END IF;
      O_DATA := O_DATA || TEMP_DATA || I_COLUMN_NAME || '=' || I_COLUMN_VALUE;
 END;
 PROCEDURE ADD_TO_OUTBOUND_STAGING (
    I_ACTION_TYPE  VARCHAR2,
    I_TABLE_NAME   VARCHAR2
 )
 IS
    TRANS_ID NUMBER(9);
    SEQ_ID NUMBER(9);
    REC_COUNT NUMBER(9);
    LEN NUMBER(9);
    POS NUMBER(9);
    BUSINESS_GRP VARCHAR2(150);
	  TEMP_DATA VARCHAR2(4000);
    CHUNK_SIZE NUMBER(9);
    Lin_Num Number(9);
	  SUBSCRIBER_COUNT Number(9) := 0;
	  CURSOR SUB_CUR IS SELECT SUBSCRIBER_ID FROM DD_SUBSCRIBER WHERE IS_ENABLE = 1;
	BEGIN
		SELECT COUNT(transaction_id) into REC_COUNT from global_transaction_tab;
		IF (REC_COUNT = 0) THEN
			--First Transaction
			Begin
				FOR SUBSCRIBER in SUB_CUR
				LOOP
					IF (SUBSCRIBER_COUNT = 0) THEN
						--Start transaction only if subscribers are found
						INSERT INTO global_transaction_tab values(SEQ_GLOBAL_TRANSACTION.nextval);
						SELECT transaction_id into TRANS_ID from global_transaction_tab;
					END IF;
					INSERT INTO DD_SYNC_STATUS(TRANSACTION_ID, SUBSCRIBER_ID, STATUS)
					VALUES (TRANS_ID, SUBSCRIBER.SUBSCRIBER_ID, 'NEW');
					SUBSCRIBER_COUNT := SUBSCRIBER_COUNT + 1;
				END LOOP;
			END;
		ELSE
			--Child Transactions
			SELECT transaction_id into TRANS_ID from global_transaction_tab;
		END IF;
		IF (TRANS_ID IS NOT NULL) THEN
			--Subscriber found and transaction created
			BEGIN
				SELECT SEQ_DD_STAGING_DATA.NEXTVAL into SEQ_ID from dual;
				BEGIN
					SELECT BUSINESS_GROUP into BUSINESS_GRP FROM DD_BUSINESS_GROUP WHERE TABLE_NAME = I_TABLE_NAME AND IS_ACTIVE = 1;
				EXCEPTION
					WHEN NO_DATA_FOUND THEN
					BUSINESS_GRP := NULL;
				END;
				INSERT INTO DD_OUTBOUND_STAGING_DATA
				values(TRANS_ID
					, SEQ_ID
					, I_ACTION_TYPE
					, I_TABLE_NAME
					, 'NEW'
					, NULL
					, systimestamp
					, BUSINESS_GRP);
				CHUNK_SIZE := 4000;
				LEN := length(O_DATA);
				POS := 1;
				LIN_NUM := 1;
				WHILE POS < LEN LOOP
				IF ((POS + CHUNK_SIZE) > LEN) THEN
				  TEMP_DATA := substr(O_DATA, POS, LEN-POS+1);
				ELSE
				  TEMP_DATA := substr(O_DATA, POS, CHUNK_SIZE);
				END IF;
				INSERT INTO DD_OUTBOUND_STAGING_DATA_DTL
				VALUES(SEQ_ID
					, LIN_NUM
					, TEMP_DATA);
				POS := POS + CHUNK_SIZE;
				LIN_NUM := LIN_NUM + 1;
				END LOOP;
			END;
		END IF;
 End;
END DATA_INTG_PKG;
/

CREATE OR REPLACE TRIGGER ACCESS_CONTROL_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON ACCESS_CONTROL
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('BUSINESS_UNIT_ID',
		                        :old.BUSINESS_UNIT_ID,
		                        :new.BUSINESS_UNIT_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('GEO_REGION_ID',
		                        :old.GEO_REGION_ID,
		                        :new.GEO_REGION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_INTERNAL_CONTROL',
		                        :old.IS_INTERNAL_CONTROL,
		                        :new.IS_INTERNAL_CONTROL
		                       );
		DATA_INTG_PKG.CHECK_VAL('UCL_USER_ID',
		                        :old.UCL_USER_ID,
		                        :new.UCL_USER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARTNER_COMPANY_ID',
		                        :old.PARTNER_COMPANY_ID,
		                        :new.PARTNER_COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ACCESS_CONTROL_ID',
		                        :old.ACCESS_CONTROL_ID,
		                        :new.ACCESS_CONTROL_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_GROUP_ID',
		                        :old.USER_GROUP_ID,
		                        :new.USER_GROUP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROLE_ID',
		                        :old.ROLE_ID,
		                        :new.ROLE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('ACCESS_CONTROL_ID',
		                        		:old.ACCESS_CONTROL_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'ACCESS_CONTROL'
	                                          );
	END IF;
END;
/

ALTER TRIGGER ACCESS_CONTROL_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_DD_DEL_TRG
 BEFORE  DELETE
 ON APP
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER APP_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON APP
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('APP_NAME',
		                        :old.APP_NAME,
		                        :new.APP_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_SHORT_NAME',
		                        :old.APP_SHORT_NAME,
		                        :new.APP_SHORT_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_ID',
		                        		:old.APP_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'APP'
	                                          );
	END IF;
END;
/

ALTER TRIGGER APP_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_INSTANCE_DD_DEL_TRG
 BEFORE  DELETE
 ON APP_INSTANCE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER APP_INSTANCE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_INSTANCE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON APP_INSTANCE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('HOST_NAME_INTERNAL',
		                        :old.HOST_NAME_INTERNAL,
		                        :new.HOST_NAME_INTERNAL
		                       );
		DATA_INTG_PKG.CHECK_VAL('PORT_INTERNAL',
		                        :old.PORT_INTERNAL,
		                        :new.PORT_INTERNAL
		                       );
		DATA_INTG_PKG.CHECK_VAL('MINOR_VERSION',
		                        :old.MINOR_VERSION,
		                        :new.MINOR_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('INS_TYPE',
		                        :old.INS_TYPE,
		                        :new.INS_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_INSTANCE_ID',
		                        :old.APP_INSTANCE_ID,
		                        :new.APP_INSTANCE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DEFAULT_PAGE_ADDR',
		                        :old.DEFAULT_PAGE_ADDR,
		                        :new.DEFAULT_PAGE_ADDR
		                       );
		DATA_INTG_PKG.CHECK_VAL('SHORT_NAME',
		                        :old.SHORT_NAME,
		                        :new.SHORT_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('HOST_NAME',
		                        :old.HOST_NAME,
		                        :new.HOST_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTEXT_PATH',
		                        :old.CONTEXT_PATH,
		                        :new.CONTEXT_PATH
		                       );
		DATA_INTG_PKG.CHECK_VAL('PORT',
		                        :old.PORT,
		                        :new.PORT
		                       );
		DATA_INTG_PKG.CHECK_VAL('MAJOR_VERSION',
		                        :old.MAJOR_VERSION,
		                        :new.MAJOR_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('DEFAULT_PAGE_TITLE',
		                        :old.DEFAULT_PAGE_TITLE,
		                        :new.DEFAULT_PAGE_TITLE
		                       );
		DATA_INTG_PKG.CHECK_VAL('PROTOCOL',
		                        :old.PROTOCOL,
		                        :new.PROTOCOL
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('UDEF_SEQ',
		                        :old.UDEF_SEQ,
		                        :new.UDEF_SEQ
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_INSTANCE_ID',
		                        		:old.APP_INSTANCE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'APP_INSTANCE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER APP_INSTANCE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_MODULE_DD_DEL_TRG
 BEFORE  DELETE
 ON APP_MODULE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER APP_MODULE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_MODULE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON APP_MODULE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('MODULE_ID',
		                        :old.MODULE_ID,
		                        :new.MODULE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROW_UID',
		                        :old.ROW_UID,
		                        :new.ROW_UID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_ID',
		                        		:old.APP_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('MODULE_ID',
		                        		:old.MODULE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'APP_MODULE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER APP_MODULE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_MOD_PERM_DD_DEL_TRG
 BEFORE  DELETE
 ON APP_MOD_PERM
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER APP_MOD_PERM_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_MOD_PERM_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON APP_MOD_PERM
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('MODULE_ID',
		                        :old.MODULE_ID,
		                        :new.MODULE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROW_UID',
		                        :old.ROW_UID,
		                        :new.ROW_UID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PERMISSION_ID',
		                        :old.PERMISSION_ID,
		                        :new.PERMISSION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_ID',
		                        		:old.APP_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('MODULE_ID',
		                        		:old.MODULE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('PERMISSION_ID',
		                        		:old.PERMISSION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'APP_MOD_PERM'
	                                          );
	END IF;
END;
/

ALTER TRIGGER APP_MOD_PERM_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER BUSINESS_PARTNER_TYPE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON BUSINESS_PARTNER_TYPE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('BUSINESS_PARTNER_TYPE_ID',
		                        :old.BUSINESS_PARTNER_TYPE_ID,
		                        :new.BUSINESS_PARTNER_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('BUSINESS_PARTNER_TYPE_ID',
		                        		:old.BUSINESS_PARTNER_TYPE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'BUSINESS_PARTNER_TYPE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER BUSINESS_PARTNER_TYPE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER BUSINESS_PART_TYPE_DD_DEL_TRG
 BEFORE  DELETE
 ON BUSINESS_PARTNER_TYPE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER BUSINESS_PART_TYPE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER CHANNEL_DD_DEL_TRG
 BEFORE  DELETE
 ON CHANNEL
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER CHANNEL_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER CHANNEL_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON CHANNEL
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('CHANNEL_ID',
		                        :old.CHANNEL_ID,
		                        :new.CHANNEL_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE_ID',
		                        :old.LAST_UPDATED_SOURCE_TYPE_ID,
		                        :new.LAST_UPDATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CHANNEL_NAME',
		                        :old.CHANNEL_NAME,
		                        :new.CHANNEL_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CHANNEL_TYPE_ID',
		                        :old.CHANNEL_TYPE_ID,
		                        :new.CHANNEL_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CHANNEL_IDEN',
		                        :old.CHANNEL_IDEN,
		                        :new.CHANNEL_IDEN
		                       );
		DATA_INTG_PKG.CHECK_VAL('CHANNEL_DESCRIPTION',
		                        :old.CHANNEL_DESCRIPTION,
		                        :new.CHANNEL_DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE_ID',
		                        :old.CREATED_SOURCE_TYPE_ID,
		                        :new.CREATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('CHANNEL_ID',
		                        		:old.CHANNEL_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'CHANNEL'
	                                          );
	END IF;
END;
/

ALTER TRIGGER CHANNEL_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER CHANNEL_TYPE_UCL_DD_DEL_TRG
 BEFORE  DELETE
 ON CHANNEL_TYPE_UCL
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER CHANNEL_TYPE_UCL_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER CHANNEL_TYPE_UCL_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON CHANNEL_TYPE_UCL
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('CHANNEL_TYPE_ID',
		                        :old.CHANNEL_TYPE_ID,
		                        :new.CHANNEL_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CHANNEL_TYPE_NAME',
		                        :old.CHANNEL_TYPE_NAME,
		                        :new.CHANNEL_TYPE_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('CHANNEL_TYPE_ID',
		                        		:old.CHANNEL_TYPE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'CHANNEL_TYPE_UCL'
	                                          );
	END IF;
END;
/

ALTER TRIGGER CHANNEL_TYPE_UCL_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMM_METHOD_UCL_DD_DEL_TRG
 BEFORE  DELETE
 ON COMM_METHOD_UCL
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER COMM_METHOD_UCL_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMM_METHOD_UCL_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON COMM_METHOD_UCL
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMM_METHOD_CODE',
		                        :old.COMM_METHOD_CODE,
		                        :new.COMM_METHOD_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMM_METHOD_ID',
		                        :old.COMM_METHOD_ID,
		                        :new.COMM_METHOD_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('COMM_METHOD_ID',
		                        		:old.COMM_METHOD_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'COMM_METHOD_UCL'
	                                          );
	END IF;
END;
/

ALTER TRIGGER COMM_METHOD_UCL_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_APP_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON COMPANY_APP
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('ROW_UID',
		                        :old.ROW_UID,
		                        :new.ROW_UID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('COMPANY_ID',
		                        		:old.COMPANY_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_ID',
		                        		:old.APP_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'COMPANY_APP'
	                                          );
	END IF;
END;
/

ALTER TRIGGER COMPANY_APP_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_APP_MODULE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON COMPANY_APP_MODULE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('MODULE_ID',
		                        :old.MODULE_ID,
		                        :new.MODULE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROW_UID',
		                        :old.ROW_UID,
		                        :new.ROW_UID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('COMPANY_ID',
		                        		:old.COMPANY_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_ID',
		                        		:old.APP_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('MODULE_ID',
		                        		:old.MODULE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'COMPANY_APP_MODULE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER COMPANY_APP_MODULE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_DD_DEL_TRG
 BEFORE  DELETE
 ON COMPANY
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER COMPANY_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON COMPANY
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('PROC_WHSE_XFER',
		                        :old.PROC_WHSE_XFER,
		                        :new.PROC_WHSE_XFER
		                       );
		DATA_INTG_PKG.CHECK_VAL('SKU_MASK',
		                        :old.SKU_MASK,
		                        :new.SKU_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('BILLING_POSTAL_CODE',
		                        :old.BILLING_POSTAL_CODE,
		                        :new.BILLING_POSTAL_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('SIZE_DESC_OFFSET',
		                        :old.SIZE_DESC_OFFSET,
		                        :new.SIZE_DESC_OFFSET
		                       );
		DATA_INTG_PKG.CHECK_VAL('LOCK_CODE_INVALID',
		                        :old.LOCK_CODE_INVALID,
		                        :new.LOCK_CODE_INVALID
		                       );
		DATA_INTG_PKG.CHECK_VAL('SEASON_SEPTR',
		                        :old.SEASON_SEPTR,
		                        :new.SEASON_SEPTR
		                       );
		DATA_INTG_PKG.CHECK_VAL('COLOR_SFX_MASK',
		                        :old.COLOR_SFX_MASK,
		                        :new.COLOR_SFX_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('FAX_NUMBER',
		                        :old.FAX_NUMBER,
		                        :new.FAX_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_DESCRIPTION',
		                        :old.COMPANY_DESCRIPTION,
		                        :new.COMPANY_DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_EMAIL',
		                        :old.CONTACT_EMAIL,
		                        :new.CONTACT_EMAIL
		                       );
		DATA_INTG_PKG.CHECK_VAL('QUAL_MASK',
		                        :old.QUAL_MASK,
		                        :new.QUAL_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('CASE_LOCK_CODE_EXP_REC',
		                        :old.CASE_LOCK_CODE_EXP_REC,
		                        :new.CASE_LOCK_CODE_EXP_REC
		                       );
		DATA_INTG_PKG.CHECK_VAL('SIZE_DESC_MASK',
		                        :old.SIZE_DESC_MASK,
		                        :new.SIZE_DESC_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('UCC_EAN_CO_PFX',
		                        :old.UCC_EAN_CO_PFX,
		                        :new.UCC_EAN_CO_PFX
		                       );
		DATA_INTG_PKG.CHECK_VAL('BILLING_CITY',
		                        :old.BILLING_CITY,
		                        :new.BILLING_CITY
		                       );
		DATA_INTG_PKG.CHECK_VAL('QUAL_SEPTR',
		                        :old.QUAL_SEPTR,
		                        :new.QUAL_SEPTR
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE_ID',
		                        :old.LAST_UPDATED_SOURCE_TYPE_ID,
		                        :new.LAST_UPDATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('SEASON_YR_MASK',
		                        :old.SEASON_YR_MASK,
		                        :new.SEASON_YR_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('COUNTRY_CODE',
		                        :old.COUNTRY_CODE,
		                        :new.COUNTRY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('COLOR_OFFSET',
		                        :old.COLOR_OFFSET,
		                        :new.COLOR_OFFSET
		                       );
		DATA_INTG_PKG.CHECK_VAL('COLOR_SFX_SEPTR',
		                        :old.COLOR_SFX_SEPTR,
		                        :new.COLOR_SFX_SEPTR
		                       );
		DATA_INTG_PKG.CHECK_VAL('SEASON_YR_OFFSET',
		                        :old.SEASON_YR_OFFSET,
		                        :new.SEASON_YR_OFFSET
		                       );
		DATA_INTG_PKG.CHECK_VAL('BILLING_STATE_PROV',
		                        :old.BILLING_STATE_PROV,
		                        :new.BILLING_STATE_PROV
		                       );
		DATA_INTG_PKG.CHECK_VAL('BILLING_COUNTRY_CODE',
		                        :old.BILLING_COUNTRY_CODE,
		                        :new.BILLING_COUNTRY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('COLOR_SEPTR',
		                        :old.COLOR_SEPTR,
		                        :new.COLOR_SEPTR
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_FAX_NUMBER',
		                        :old.CONTACT_FAX_NUMBER,
		                        :new.CONTACT_FAX_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('INVOICE_COMPARE_METHOD',
		                        :old.INVOICE_COMPARE_METHOD,
		                        :new.INVOICE_COMPARE_METHOD
		                       );
		DATA_INTG_PKG.CHECK_VAL('STYLE_SFX_SEPTR',
		                        :old.STYLE_SFX_SEPTR,
		                        :new.STYLE_SFX_SEPTR
		                       );
		DATA_INTG_PKG.CHECK_VAL('CASE_LOCK_CODE_HELD',
		                        :old.CASE_LOCK_CODE_HELD,
		                        :new.CASE_LOCK_CODE_HELD
		                       );
		DATA_INTG_PKG.CHECK_VAL('STYLE_SFX_OFFSET',
		                        :old.STYLE_SFX_OFFSET,
		                        :new.STYLE_SFX_OFFSET
		                       );
		DATA_INTG_PKG.CHECK_VAL('DFLT_BATCH_STAT_CODE',
		                        :old.DFLT_BATCH_STAT_CODE,
		                        :new.DFLT_BATCH_STAT_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('STYLE_SFX_MASK',
		                        :old.STYLE_SFX_MASK,
		                        :new.STYLE_SFX_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('SEC_DIM_OFFSET',
		                        :old.SEC_DIM_OFFSET,
		                        :new.SEC_DIM_OFFSET
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSP_ITEM_DESC_FLAG',
		                        :old.DSP_ITEM_DESC_FLAG,
		                        :new.DSP_ITEM_DESC_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('POSTAL_CODE',
		                        :old.POSTAL_CODE,
		                        :new.POSTAL_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CITY',
		                        :old.CITY,
		                        :new.CITY
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_CODE',
		                        :old.COMPANY_CODE,
		                        :new.COMPANY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('STYLE_OFFSET',
		                        :old.STYLE_OFFSET,
		                        :new.STYLE_OFFSET
		                       );
		DATA_INTG_PKG.CHECK_VAL('SEC_DIM_SEPTR',
		                        :old.SEC_DIM_SEPTR,
		                        :new.SEC_DIM_SEPTR
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE_ID',
		                        :old.CREATED_SOURCE_TYPE_ID,
		                        :new.CREATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARENT_COMPANY_ID',
		                        :old.PARENT_COMPANY_ID,
		                        :new.PARENT_COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('RECV_BATCH',
		                        :old.RECV_BATCH,
		                        :new.RECV_BATCH
		                       );
		DATA_INTG_PKG.CHECK_VAL('BATCH_ROLE_ID',
		                        :old.BATCH_ROLE_ID,
		                        :new.BATCH_ROLE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COLOR_MASK',
		                        :old.COLOR_MASK,
		                        :new.COLOR_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_NAME',
		                        :old.CONTACT_NAME,
		                        :new.CONTACT_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_NAME',
		                        :old.COMPANY_NAME,
		                        :new.COMPANY_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_MULTIPLE_LOGON_RESTRICTED',
		                        :old.IS_MULTIPLE_LOGON_RESTRICTED,
		                        :new.IS_MULTIPLE_LOGON_RESTRICTED
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_TELEPHONE_NUMBER',
		                        :old.CONTACT_TELEPHONE_NUMBER,
		                        :new.CONTACT_TELEPHONE_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('STATE_PROV',
		                        :old.STATE_PROV,
		                        :new.STATE_PROV
		                       );
		DATA_INTG_PKG.CHECK_VAL('BUSINESS_NUMBER',
		                        :old.BUSINESS_NUMBER,
		                        :new.BUSINESS_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('DUNS_NUMBER',
		                        :old.DUNS_NUMBER,
		                        :new.DUNS_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('BATCH_CTRL_FLAG',
		                        :old.BATCH_CTRL_FLAG,
		                        :new.BATCH_CTRL_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('SEASON_YR_SEPTR',
		                        :old.SEASON_YR_SEPTR,
		                        :new.SEASON_YR_SEPTR
		                       );
		DATA_INTG_PKG.CHECK_VAL('SEASON_MASK',
		                        :old.SEASON_MASK,
		                        :new.SEASON_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('SKU_OFFSET_MASK',
		                        :old.SKU_OFFSET_MASK,
		                        :new.SKU_OFFSET_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_URL',
		                        :old.COMPANY_URL,
		                        :new.COMPANY_URL
		                       );
		DATA_INTG_PKG.CHECK_VAL('STYLE_MASK',
		                        :old.STYLE_MASK,
		                        :new.STYLE_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_TITLE',
		                        :old.CONTACT_TITLE,
		                        :new.CONTACT_TITLE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('TELEPHONE_NUMBER',
		                        :old.TELEPHONE_NUMBER,
		                        :new.TELEPHONE_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('PICK_LOCK_CODE_HELD',
		                        :old.PICK_LOCK_CODE_HELD,
		                        :new.PICK_LOCK_CODE_HELD
		                       );
		DATA_INTG_PKG.CHECK_VAL('QUAL_OFFSET',
		                        :old.QUAL_OFFSET,
		                        :new.QUAL_OFFSET
		                       );
		DATA_INTG_PKG.CHECK_VAL('PICK_LOCK_CODE_EXP_REC',
		                        :old.PICK_LOCK_CODE_EXP_REC,
		                        :new.PICK_LOCK_CODE_EXP_REC
		                       );
		DATA_INTG_PKG.CHECK_VAL('ADDRESS_1',
		                        :old.ADDRESS_1,
		                        :new.ADDRESS_1
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ADDRESS_2',
		                        :old.ADDRESS_2,
		                        :new.ADDRESS_2
		                       );
		DATA_INTG_PKG.CHECK_VAL('BILLING_ADDRESS_2',
		                        :old.BILLING_ADDRESS_2,
		                        :new.BILLING_ADDRESS_2
		                       );
		DATA_INTG_PKG.CHECK_VAL('HAS_LOGO',
		                        :old.HAS_LOGO,
		                        :new.HAS_LOGO
		                       );
		DATA_INTG_PKG.CHECK_VAL('ADDRESS_3',
		                        :old.ADDRESS_3,
		                        :new.ADDRESS_3
		                       );
		DATA_INTG_PKG.CHECK_VAL('BILLING_ADDRESS_1',
		                        :old.BILLING_ADDRESS_1,
		                        :new.BILLING_ADDRESS_1
		                       );
		DATA_INTG_PKG.CHECK_VAL('SEC_DIM_MASK',
		                        :old.SEC_DIM_MASK,
		                        :new.SEC_DIM_MASK
		                       );
		DATA_INTG_PKG.CHECK_VAL('STYLE_SEPTR',
		                        :old.STYLE_SEPTR,
		                        :new.STYLE_SEPTR
		                       );
		DATA_INTG_PKG.CHECK_VAL('AUTO_CREATE_BATCH_FLAG',
		                        :old.AUTO_CREATE_BATCH_FLAG,
		                        :new.AUTO_CREATE_BATCH_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('COLOR_SFX_OFFSET',
		                        :old.COLOR_SFX_OFFSET,
		                        :new.COLOR_SFX_OFFSET
		                       );
		DATA_INTG_PKG.CHECK_VAL('BILLING_ADDRESS_3',
		                        :old.BILLING_ADDRESS_3,
		                        :new.BILLING_ADDRESS_3
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_TYPE_ID',
		                        :old.COMPANY_TYPE_ID,
		                        :new.COMPANY_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('SIZE_DESC_SEPTR',
		                        :old.SIZE_DESC_SEPTR,
		                        :new.SIZE_DESC_SEPTR
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_INITIALIZED',
		                        :old.IS_INITIALIZED,
		                        :new.IS_INITIALIZED
		                       );
		DATA_INTG_PKG.CHECK_VAL('SEASON_OFFSET',
		                        :old.SEASON_OFFSET,
		                        :new.SEASON_OFFSET
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('COMPANY_ID',
		                        		:old.COMPANY_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'COMPANY'
	                                          );
	END IF;
END;
/

ALTER TRIGGER COMPANY_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_PARAM_DD_DEL_TRG
 BEFORE  DELETE
 ON COMPANY_PARAM
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER COMPANY_PARAM_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_PARAM_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON COMPANY_PARAM
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('UI_TYPE',
		                        :old.UI_TYPE,
		                        :new.UI_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARAMETER_ID',
		                        :old.PARAMETER_ID,
		                        :new.PARAMETER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARAMETER_NAME',
		                        :old.PARAMETER_NAME,
		                        :new.PARAMETER_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARAMETER_VALUE',
		                        :old.PARAMETER_VALUE,
		                        :new.PARAMETER_VALUE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('PARAMETER_ID',
		                        		:old.PARAMETER_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'COMPANY_PARAM'
	                                          );
	END IF;
END;
/

ALTER TRIGGER COMPANY_PARAM_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_TYPE_DD_DEL_TRG
 BEFORE  DELETE
 ON COMPANY_TYPE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER COMPANY_TYPE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_TYPE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON COMPANY_TYPE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('COMPANY_TYPE_ID',
		                        :old.COMPANY_TYPE_ID,
		                        :new.COMPANY_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('COMPANY_TYPE_ID',
		                        		:old.COMPANY_TYPE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'COMPANY_TYPE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER COMPANY_TYPE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_TYPE_PERM_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON COMPANY_TYPE_PERM
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('COMPANY_TYPE_ID',
		                        :old.COMPANY_TYPE_ID,
		                        :new.COMPANY_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROW_UID',
		                        :old.ROW_UID,
		                        :new.ROW_UID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PERMISSION_ID',
		                        :old.PERMISSION_ID,
		                        :new.PERMISSION_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('COMPANY_TYPE_ID',
		                        		:old.COMPANY_TYPE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('PERMISSION_ID',
		                        		:old.PERMISSION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'COMPANY_TYPE_PERM'
	                                          );
	END IF;
END;
/

ALTER TRIGGER COMPANY_TYPE_PERM_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER COUNTRY_DD_DEL_TRG
 BEFORE  DELETE
 ON COUNTRY
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER COUNTRY_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER COUNTRY_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON COUNTRY
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('CNTRY_A2_CODE',
		                        :old.CNTRY_A2_CODE,
		                        :new.CNTRY_A2_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('MOD_DATE_TIME',
		                        :old.MOD_DATE_TIME,
		                        :new.MOD_DATE_TIME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATE_DATE_TIME',
		                        :old.CREATE_DATE_TIME,
		                        :new.CREATE_DATE_TIME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CNTRY_ID',
		                        :old.CNTRY_ID,
		                        :new.CNTRY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('HAS_COUNTY',
		                        :old.HAS_COUNTY,
		                        :new.HAS_COUNTY
		                       );
		DATA_INTG_PKG.CHECK_VAL('INTL_FLAG',
		                        :old.INTL_FLAG,
		                        :new.INTL_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('PRINT_NAFTA_COO_FLAG',
		                        :old.PRINT_NAFTA_COO_FLAG,
		                        :new.PRINT_NAFTA_COO_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('ADDR_FORMAT',
		                        :old.ADDR_FORMAT,
		                        :new.ADDR_FORMAT
		                       );
		DATA_INTG_PKG.CHECK_VAL('VOL_UOM',
		                        :old.VOL_UOM,
		                        :new.VOL_UOM
		                       );
		DATA_INTG_PKG.CHECK_VAL('DEPART_AIRPORT',
		                        :old.DEPART_AIRPORT,
		                        :new.DEPART_AIRPORT
		                       );
		DATA_INTG_PKG.CHECK_VAL('PRINT_OCEAN_BOL_FLAG',
		                        :old.PRINT_OCEAN_BOL_FLAG,
		                        :new.PRINT_OCEAN_BOL_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('PRINT_CANADIAN_CUST_INVC_FLAG',
		                        :old.PRINT_CANADIAN_CUST_INVC_FLAG,
		                        :new.PRINT_CANADIAN_CUST_INVC_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('PRINT_PKG_LIST_FLAG',
		                        :old.PRINT_PKG_LIST_FLAG,
		                        :new.PRINT_PKG_LIST_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('ACCEPTS_ORMD_US_ORIGIN',
		                        :old.ACCEPTS_ORMD_US_ORIGIN,
		                        :new.ACCEPTS_ORMD_US_ORIGIN
		                       );
		DATA_INTG_PKG.CHECK_VAL('DFLT_CURRENCY_CODE',
		                        :old.DFLT_CURRENCY_CODE,
		                        :new.DFLT_CURRENCY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('COUNTRY_NAME',
		                        :old.COUNTRY_NAME,
		                        :new.COUNTRY_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('PRINT_INV',
		                        :old.PRINT_INV,
		                        :new.PRINT_INV
		                       );
		DATA_INTG_PKG.CHECK_VAL('ACCEPTS_HAZMAT_FROM_US_ORIGIN',
		                        :old.ACCEPTS_HAZMAT_FROM_US_ORIGIN,
		                        :new.ACCEPTS_HAZMAT_FROM_US_ORIGIN
		                       );
		DATA_INTG_PKG.CHECK_VAL('DEST_AIRPORT',
		                        :old.DEST_AIRPORT,
		                        :new.DEST_AIRPORT
		                       );
		DATA_INTG_PKG.CHECK_VAL('CNTRY_A3_CODE',
		                        :old.CNTRY_A3_CODE,
		                        :new.CNTRY_A3_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('PRINT_SHPR_LTR_OF_INSTR_FLAG',
		                        :old.PRINT_SHPR_LTR_OF_INSTR_FLAG,
		                        :new.PRINT_SHPR_LTR_OF_INSTR_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('GEO_REGION_ID',
		                        :old.GEO_REGION_ID,
		                        :new.GEO_REGION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_ID',
		                        :old.USER_ID,
		                        :new.USER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('HAS_STATE_PROV',
		                        :old.HAS_STATE_PROV,
		                        :new.HAS_STATE_PROV
		                       );
		DATA_INTG_PKG.CHECK_VAL('DIM_UOM',
		                        :old.DIM_UOM,
		                        :new.DIM_UOM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PRINT_COO',
		                        :old.PRINT_COO,
		                        :new.PRINT_COO
		                       );
		DATA_INTG_PKG.CHECK_VAL('COUNTRY_CODE',
		                        :old.COUNTRY_CODE,
		                        :new.COUNTRY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('WT_UOM',
		                        :old.WT_UOM,
		                        :new.WT_UOM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PRINT_SED',
		                        :old.PRINT_SED,
		                        :new.PRINT_SED
		                       );
		DATA_INTG_PKG.CHECK_VAL('ZIP_REQD_FLAG',
		                        :old.ZIP_REQD_FLAG,
		                        :new.ZIP_REQD_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('PRINT_DOCK_RCPT_FLAG',
		                        :old.PRINT_DOCK_RCPT_FLAG,
		                        :new.PRINT_DOCK_RCPT_FLAG
		                       );
		DATA_INTG_PKG.CHECK_VAL('ISO_CNTRY_CODE',
		                        :old.ISO_CNTRY_CODE,
		                        :new.ISO_CNTRY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('ACCEPTS_CON_COM_ORMD_US_ORIGIN',
		                        :old.ACCEPTS_CON_COM_ORMD_US_ORIGIN,
		                        :new.ACCEPTS_CON_COM_ORMD_US_ORIGIN
		                       );
		DATA_INTG_PKG.CHECK_VAL('DISPLAY_ORDER',
		                        :old.DISPLAY_ORDER,
		                        :new.DISPLAY_ORDER
		                       );
		DATA_INTG_PKG.CHECK_VAL('LANG_ID',
		                        :old.LANG_ID,
		                        :new.LANG_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('COUNTRY_CODE',
		                        		:old.COUNTRY_CODE
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'COUNTRY'
	                                          );
	END IF;
END;
/

ALTER TRIGGER COUNTRY_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER DD_SYNC_STAT_TRG_UPDATE
BEFORE UPDATE ON DD_SYNC_STATUS
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
BEGIN
  if :old.last_updated_dttm is not null and :old.sync_start_dttm is null then
      :new.sync_start_dttm := systimestamp;
  end if;
END DD_SYNC_STAT_TRG;
/

ALTER TRIGGER DD_SYNC_STAT_TRG_UPDATE ENABLE;

CREATE OR REPLACE TRIGGER DSHBRD_COGNOS_PTLT_DTL_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON DSHBRD_COGNOS_PTLT_DTL
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_COGNOS_PTLT_DTL_ID',
		                        :old.DSHBRD_COGNOS_PTLT_DTL_ID,
		                        :new.DSHBRD_COGNOS_PTLT_DTL_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('COGNOS_REPORT_NAME',
		                        :old.COGNOS_REPORT_NAME,
		                        :new.COGNOS_REPORT_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PORTLET_ID',
		                        :old.DSHBRD_PORTLET_ID,
		                        :new.DSHBRD_PORTLET_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('DSHBRD_COGNOS_PTLT_DTL_ID',
		                        		:old.DSHBRD_COGNOS_PTLT_DTL_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'DSHBRD_COGNOS_PTLT_DTL'
	                                          );
	END IF;
END;
/

ALTER TRIGGER DSHBRD_COGNOS_PTLT_DTL_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER DSHBRD_PORTLET_DD_DEL_TRG
 BEFORE  DELETE
 ON DSHBRD_PORTLET
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER DSHBRD_PORTLET_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER DSHBRD_PORTLET_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON DSHBRD_PORTLET
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('SUPPORT_MOBILE',
		                        :old.SUPPORT_MOBILE,
		                        :new.SUPPORT_MOBILE
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONFIG_URL',
		                        :old.CONFIG_URL,
		                        :new.CONFIG_URL
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_INSTANCE_ID',
		                        :old.APP_INSTANCE_ID,
		                        :new.APP_INSTANCE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PORTLET_HEIGHT_TYPE_ID',
		                        :old.PORTLET_HEIGHT_TYPE_ID,
		                        :new.PORTLET_HEIGHT_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_RSS_FEED',
		                        :old.IS_RSS_FEED,
		                        :new.IS_RSS_FEED
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PORTLET_TYPE_ID',
		                        :old.DSHBRD_PORTLET_TYPE_ID,
		                        :new.DSHBRD_PORTLET_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PORTLET_WIDTH_TYPE_ID',
		                        :old.PORTLET_WIDTH_TYPE_ID,
		                        :new.PORTLET_WIDTH_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('IMAGE_PATH',
		                        :old.IMAGE_PATH,
		                        :new.IMAGE_PATH
		                       );
		DATA_INTG_PKG.CHECK_VAL('URL_TYPE',
		                        :old.URL_TYPE,
		                        :new.URL_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('URL',
		                        :old.URL,
		                        :new.URL
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PORTLET_ID',
		                        :old.DSHBRD_PORTLET_ID,
		                        :new.DSHBRD_PORTLET_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PORTLET_NAME',
		                        :old.PORTLET_NAME,
		                        :new.PORTLET_NAME
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('DSHBRD_PORTLET_ID',
		                        		:old.DSHBRD_PORTLET_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'DSHBRD_PORTLET'
	                                          );
	END IF;
END;
/

ALTER TRIGGER DSHBRD_PORTLET_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER DSHBRD_PORTLET_PERMSSN_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON DSHBRD_PORTLET_PERMISSION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PORTLET_PERMISSION_ID',
		                        :old.DSHBRD_PORTLET_PERMISSION_ID,
		                        :new.DSHBRD_PORTLET_PERMISSION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PORTLET_ID',
		                        :old.DSHBRD_PORTLET_ID,
		                        :new.DSHBRD_PORTLET_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PERMISSION_CODE',
		                        :old.PERMISSION_CODE,
		                        :new.PERMISSION_CODE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('DSHBRD_PORTLET_PERMISSION_ID',
		                        		:old.DSHBRD_PORTLET_PERMISSION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'DSHBRD_PORTLET_PERMISSION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER DSHBRD_PORTLET_PERMSSN_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER DSHBRD_PORTLET_TAGS_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON DSHBRD_PORTLET_TAGS
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('TAGS',
		                        :old.TAGS,
		                        :new.TAGS
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PORTLET_TAGS_ID',
		                        :old.DSHBRD_PORTLET_TAGS_ID,
		                        :new.DSHBRD_PORTLET_TAGS_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PORTLET_ID',
		                        :old.DSHBRD_PORTLET_ID,
		                        :new.DSHBRD_PORTLET_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('DSHBRD_PORTLET_TAGS_ID',
		                        		:old.DSHBRD_PORTLET_TAGS_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'DSHBRD_PORTLET_TAGS'
	                                          );
	END IF;
END;
/

ALTER TRIGGER DSHBRD_PORTLET_TAGS_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER DSHBRD_PORTLT_PARM_DEF_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON DSHBRD_PORTLET_PARAM_DEF
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PTLT_PARAM_DEF_ID',
		                        :old.DSHBRD_PTLT_PARAM_DEF_ID,
		                        :new.DSHBRD_PTLT_PARAM_DEF_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PORTLET_PARAM_DEF_NAME',
		                        :old.PORTLET_PARAM_DEF_NAME,
		                        :new.PORTLET_PARAM_DEF_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('PORTLET_ID',
		                        :old.PORTLET_ID,
		                        :new.PORTLET_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARAM_LABEL',
		                        :old.PARAM_LABEL,
		                        :new.PARAM_LABEL
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARAM_VALUE',
		                        :old.PARAM_VALUE,
		                        :new.PARAM_VALUE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('DSHBRD_PTLT_PARAM_DEF_ID',
		                        		:old.DSHBRD_PTLT_PARAM_DEF_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'DSHBRD_PORTLET_PARAM_DEF'
	                                          );
	END IF;
END;
/

ALTER TRIGGER DSHBRD_PORTLT_PARM_DEF_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER DSHBRD_UIB_PTLT_DTL_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON DSHBRD_UIB_PTLT_DTL
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('UIB_VIEW_PROPERTY_SET_ID',
		                        :old.UIB_VIEW_PROPERTY_SET_ID,
		                        :new.UIB_VIEW_PROPERTY_SET_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_UIB_PTLT_DTL_ID',
		                        :old.DSHBRD_UIB_PTLT_DTL_ID,
		                        :new.DSHBRD_UIB_PTLT_DTL_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_QUERY_ID',
		                        :old.UIB_QUERY_ID,
		                        :new.UIB_QUERY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('DATA_SOURCE_ID',
		                        :old.DATA_SOURCE_ID,
		                        :new.DATA_SOURCE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_VIEW_TYPE_ID',
		                        :old.UIB_VIEW_TYPE_ID,
		                        :new.UIB_VIEW_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PORTLET_ID',
		                        :old.DSHBRD_PORTLET_ID,
		                        :new.DSHBRD_PORTLET_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('TITLE',
		                        :old.TITLE,
		                        :new.TITLE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('DSHBRD_UIB_PTLT_DTL_ID',
		                        		:old.DSHBRD_UIB_PTLT_DTL_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'DSHBRD_UIB_PTLT_DTL'
	                                          );
	END IF;
END;
/

ALTER TRIGGER DSHBRD_UIB_PTLT_DTL_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER DS_DATA_SOURCE_DD_DEL_TRG
 BEFORE  DELETE
 ON DS_DATA_SOURCE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER DS_DATA_SOURCE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER DS_DATA_SOURCE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON DS_DATA_SOURCE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DRIVER_CLASS',
		                        :old.DRIVER_CLASS,
		                        :new.DRIVER_CLASS
		                       );
		DATA_INTG_PKG.CHECK_VAL('DATA_SOURCE_NAME',
		                        :old.DATA_SOURCE_NAME,
		                        :new.DATA_SOURCE_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_NAME',
		                        :old.USER_NAME,
		                        :new.USER_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('DATA_SOURCE_ID',
		                        :old.DATA_SOURCE_ID,
		                        :new.DATA_SOURCE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_INST_SHORT_NAME',
		                        :old.APP_INST_SHORT_NAME,
		                        :new.APP_INST_SHORT_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PASSWORD',
		                        :old.PASSWORD,
		                        :new.PASSWORD
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('MAX_POOL_SIZE',
		                        :old.MAX_POOL_SIZE,
		                        :new.MAX_POOL_SIZE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONNECTION_PROPERTIES',
		                        :old.CONNECTION_PROPERTIES,
		                        :new.CONNECTION_PROPERTIES
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONNECTION_URL',
		                        :old.CONNECTION_URL,
		                        :new.CONNECTION_URL
		                       );
		DATA_INTG_PKG.CHECK_VAL('DATA_SOURCE_TYPE_ID',
		                        :old.DATA_SOURCE_TYPE_ID,
		                        :new.DATA_SOURCE_TYPE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('DATA_SOURCE_ID',
		                        		:old.DATA_SOURCE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'DS_DATA_SOURCE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER DS_DATA_SOURCE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER LANGUAGE_DD_DEL_TRG
 BEFORE  DELETE
 ON LANGUAGE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER LANGUAGE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER LANGUAGE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON LANGUAGE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('LANGUAGE_ID',
		                        :old.LANGUAGE_ID,
		                        :new.LANGUAGE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LANGUAGE_SUFFIX',
		                        :old.LANGUAGE_SUFFIX,
		                        :new.LANGUAGE_SUFFIX
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('LANGUAGE_ID',
		                        		:old.LANGUAGE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'LANGUAGE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER LANGUAGE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER LOCALE_DD_DEL_TRG
 BEFORE  DELETE
 ON LOCALE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER LOCALE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER LOCALE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON LOCALE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('LANGUAGE_ID',
		                        :old.LANGUAGE_ID,
		                        :new.LANGUAGE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COUNTRY_CODE',
		                        :old.COUNTRY_CODE,
		                        :new.COUNTRY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LOCALE_ID',
		                        :old.LOCALE_ID,
		                        :new.LOCALE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LOCALE_NAME',
		                        :old.LOCALE_NAME,
		                        :new.LOCALE_NAME
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('LOCALE_ID',
		                        		:old.LOCALE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'LOCALE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER LOCALE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER LOCATION_SCHEDULE_DD_DEL_TRG
 BEFORE  DELETE
 ON LOCATION_SCHEDULE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER LOCATION_SCHEDULE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER LOCATION_SCHEDULE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON LOCATION_SCHEDULE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('CLOSING_TIME_MINUTE',
		                        :old.CLOSING_TIME_MINUTE,
		                        :new.CLOSING_TIME_MINUTE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LOCATION_ID',
		                        :old.LOCATION_ID,
		                        :new.LOCATION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DAY_OF_WEEK',
		                        :old.DAY_OF_WEEK,
		                        :new.DAY_OF_WEEK
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_CLOSED',
		                        :old.IS_CLOSED,
		                        :new.IS_CLOSED
		                       );
		DATA_INTG_PKG.CHECK_VAL('OPENING_TIME_MINUTE',
		                        :old.OPENING_TIME_MINUTE,
		                        :new.OPENING_TIME_MINUTE
		                       );
		DATA_INTG_PKG.CHECK_VAL('OPENING_TIME_HOUR',
		                        :old.OPENING_TIME_HOUR,
		                        :new.OPENING_TIME_HOUR
		                       );
		DATA_INTG_PKG.CHECK_VAL('CLOSING_TIME_HOUR',
		                        :old.CLOSING_TIME_HOUR,
		                        :new.CLOSING_TIME_HOUR
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('LOCATION_ID',
		                        		:old.LOCATION_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('DAY_OF_WEEK',
		                        		:old.DAY_OF_WEEK
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'LOCATION_SCHEDULE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER LOCATION_SCHEDULE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER LOCATION_UCL_DD_DEL_TRG
 BEFORE  DELETE
 ON LOCATION_UCL
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER LOCATION_UCL_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER LOCATION_UCL_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON LOCATION_UCL
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_NAME',
		                        :old.CONTACT_NAME,
		                        :new.CONTACT_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_TELEPHONE_NUMBER',
		                        :old.CONTACT_TELEPHONE_NUMBER,
		                        :new.CONTACT_TELEPHONE_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('FAX_NUMBER',
		                        :old.FAX_NUMBER,
		                        :new.FAX_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_EMAIL',
		                        :old.CONTACT_EMAIL,
		                        :new.CONTACT_EMAIL
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_TITLE',
		                        :old.CONTACT_TITLE,
		                        :new.CONTACT_TITLE
		                       );
		DATA_INTG_PKG.CHECK_VAL('TIME_ZONE_ID',
		                        :old.TIME_ZONE_ID,
		                        :new.TIME_ZONE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('TELEPHONE_NUMBER',
		                        :old.TELEPHONE_NUMBER,
		                        :new.TELEPHONE_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_DST_OBSERVED',
		                        :old.IS_DST_OBSERVED,
		                        :new.IS_DST_OBSERVED
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE_ID',
		                        :old.LAST_UPDATED_SOURCE_TYPE_ID,
		                        :new.LAST_UPDATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ADDRESS_1',
		                        :old.ADDRESS_1,
		                        :new.ADDRESS_1
		                       );
		DATA_INTG_PKG.CHECK_VAL('ADDRESS_2',
		                        :old.ADDRESS_2,
		                        :new.ADDRESS_2
		                       );
		DATA_INTG_PKG.CHECK_VAL('COUNTRY_CODE',
		                        :old.COUNTRY_CODE,
		                        :new.COUNTRY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LOCATION_NAME',
		                        :old.LOCATION_NAME,
		                        :new.LOCATION_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CONTACT_FAX_NUMBER',
		                        :old.CONTACT_FAX_NUMBER,
		                        :new.CONTACT_FAX_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('STATE_PROV_CODE',
		                        :old.STATE_PROV_CODE,
		                        :new.STATE_PROV_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LOCATION_ID',
		                        :old.LOCATION_ID,
		                        :new.LOCATION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CITY',
		                        :old.CITY,
		                        :new.CITY
		                       );
		DATA_INTG_PKG.CHECK_VAL('POSTAL_CODE',
		                        :old.POSTAL_CODE,
		                        :new.POSTAL_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE_ID',
		                        :old.CREATED_SOURCE_TYPE_ID,
		                        :new.CREATED_SOURCE_TYPE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('LOCATION_ID',
		                        		:old.LOCATION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'LOCATION_UCL'
	                                          );
	END IF;
END;
/

ALTER TRIGGER LOCATION_UCL_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER MODULE_DD_DEL_TRG
 BEFORE  DELETE
 ON MODULE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER MODULE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER MODULE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON MODULE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('MODULE_ID',
		                        :old.MODULE_ID,
		                        :new.MODULE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('MODULE_NAME',
		                        :old.MODULE_NAME,
		                        :new.MODULE_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('MODULE_CODE',
		                        :old.MODULE_CODE,
		                        :new.MODULE_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('MODULE_ID',
		                        		:old.MODULE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'MODULE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER MODULE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER PERMISSION_DD_DEL_TRG
 BEFORE  DELETE
 ON PERMISSION
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER PERMISSION_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER PERMISSION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON PERMISSION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE_ID',
		                        :old.LAST_UPDATED_SOURCE_TYPE_ID,
		                        :new.LAST_UPDATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PERMISSION_NAME',
		                        :old.PERMISSION_NAME,
		                        :new.PERMISSION_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE_ID',
		                        :old.CREATED_SOURCE_TYPE_ID,
		                        :new.CREATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('PERMISSION_CODE',
		                        :old.PERMISSION_CODE,
		                        :new.PERMISSION_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('PERMISSION_ID',
		                        :old.PERMISSION_ID,
		                        :new.PERMISSION_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('PERMISSION_ID',
		                        		:old.PERMISSION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'PERMISSION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER PERMISSION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER PERMISSION_INHERITANCE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON PERMISSION_INHERITANCE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('CHILD_PERMISSION_ID',
		                        :old.CHILD_PERMISSION_ID,
		                        :new.CHILD_PERMISSION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARENT_PERMISSION_ID',
		                        :old.PARENT_PERMISSION_ID,
		                        :new.PARENT_PERMISSION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('PARENT_PERMISSION_ID',
		                        		:old.PARENT_PERMISSION_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('CHILD_PERMISSION_ID',
		                        		:old.CHILD_PERMISSION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'PERMISSION_INHERITANCE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER PERMISSION_INHERITANCE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER REGION_DD_DEL_TRG
 BEFORE  DELETE
 ON REGION
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER REGION_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER REGION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON REGION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('MARK_FOR_DELETION',
		                        :old.MARK_FOR_DELETION,
		                        :new.MARK_FOR_DELETION
		                       );
		DATA_INTG_PKG.CHECK_VAL('TIME_FEAS_CALC',
		                        :old.TIME_FEAS_CALC,
		                        :new.TIME_FEAS_CALC
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMMENTS',
		                        :old.COMMENTS,
		                        :new.COMMENTS
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('REGION_ID',
		                        :old.REGION_ID,
		                        :new.REGION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('TC_COMPANY_ID',
		                        :old.TC_COMPANY_ID,
		                        :new.TC_COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('REGION_NAME',
		                        :old.REGION_NAME,
		                        :new.REGION_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_PLANNER_REGION',
		                        :old.IS_PLANNER_REGION,
		                        :new.IS_PLANNER_REGION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('REGION_TZ',
		                        :old.REGION_TZ,
		                        :new.REGION_TZ
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('REGION_ID',
		                        		:old.REGION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'REGION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER REGION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER RELATIONSHIP_DD_DEL_TRG
 BEFORE  DELETE
 ON RELATIONSHIP
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER RELATIONSHIP_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER RELATIONSHIP_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON RELATIONSHIP
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('RELATIONSHIP_DESCRIPTION',
		                        :old.RELATIONSHIP_DESCRIPTION,
		                        :new.RELATIONSHIP_DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE_ID',
		                        :old.LAST_UPDATED_SOURCE_TYPE_ID,
		                        :new.LAST_UPDATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('RELATIONSHIP_ID',
		                        :old.RELATIONSHIP_ID,
		                        :new.RELATIONSHIP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMM_METHOD',
		                        :old.COMM_METHOD,
		                        :new.COMM_METHOD
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('BUSINESS_NUMBER',
		                        :old.BUSINESS_NUMBER,
		                        :new.BUSINESS_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID_RELATED',
		                        :old.COMPANY_ID_RELATED,
		                        :new.COMPANY_ID_RELATED
		                       );
		DATA_INTG_PKG.CHECK_VAL('BUSINESS_PARTNER_TYPE_ID',
		                        :old.BUSINESS_PARTNER_TYPE_ID,
		                        :new.BUSINESS_PARTNER_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ACCREDITED_BP',
		                        :old.ACCREDITED_BP,
		                        :new.ACCREDITED_BP
		                       );
		DATA_INTG_PKG.CHECK_VAL('RELATIONSHIP_NAME',
		                        :old.RELATIONSHIP_NAME,
		                        :new.RELATIONSHIP_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE_ID',
		                        :old.CREATED_SOURCE_TYPE_ID,
		                        :new.CREATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('RELATIONSHIP_TYPE_ID',
		                        :old.RELATIONSHIP_TYPE_ID,
		                        :new.RELATIONSHIP_TYPE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('RELATIONSHIP_ID',
		                        		:old.RELATIONSHIP_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'RELATIONSHIP'
	                                          );
	END IF;
END;
/

ALTER TRIGGER RELATIONSHIP_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER RELATIONSHIP_TYPE_DD_DEL_TRG
 BEFORE  DELETE
 ON RELATIONSHIP_TYPE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER RELATIONSHIP_TYPE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER RELATIONSHIP_TYPE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON RELATIONSHIP_TYPE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('REL_TYPE_UCL_ABBREV',
		                        :old.REL_TYPE_UCL_ABBREV,
		                        :new.REL_TYPE_UCL_ABBREV
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_TYPE_ID',
		                        :old.COMPANY_TYPE_ID,
		                        :new.COMPANY_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('MODULE_ID_FOR_FILTER',
		                        :old.MODULE_ID_FOR_FILTER,
		                        :new.MODULE_ID_FOR_FILTER
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('RELATIONSHIP_TYPE_ID',
		                        :old.RELATIONSHIP_TYPE_ID,
		                        :new.RELATIONSHIP_TYPE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('RELATIONSHIP_TYPE_ID',
		                        		:old.RELATIONSHIP_TYPE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'RELATIONSHIP_TYPE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER RELATIONSHIP_TYPE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER RELTP_APP_MOD_PERM_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON RELTP_APP_MOD_PERM
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('IS_PERM_IMPLIED',
		                        :old.IS_PERM_IMPLIED,
		                        :new.IS_PERM_IMPLIED
		                       );
		DATA_INTG_PKG.CHECK_VAL('MODULE_ID',
		                        :old.MODULE_ID,
		                        :new.MODULE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROW_UID',
		                        :old.ROW_UID,
		                        :new.ROW_UID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PERMISSION_ID',
		                        :old.PERMISSION_ID,
		                        :new.PERMISSION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('RELATIONSHIP_TYPE_ID',
		                        :old.RELATIONSHIP_TYPE_ID,
		                        :new.RELATIONSHIP_TYPE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('RELATIONSHIP_TYPE_ID',
		                        		:old.RELATIONSHIP_TYPE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_ID',
		                        		:old.APP_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('MODULE_ID',
		                        		:old.MODULE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('PERMISSION_ID',
		                        		:old.PERMISSION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'RELTP_APP_MOD_PERM'
	                                          );
	END IF;
END;
/

ALTER TRIGGER RELTP_APP_MOD_PERM_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER REL_APP_MOD_PERM_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON REL_APP_MOD_PERM
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('MODULE_ID',
		                        :old.MODULE_ID,
		                        :new.MODULE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROW_UID',
		                        :old.ROW_UID,
		                        :new.ROW_UID
		                       );
		DATA_INTG_PKG.CHECK_VAL('RELATIONSHIP_ID',
		                        :old.RELATIONSHIP_ID,
		                        :new.RELATIONSHIP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PERMISSION_ID',
		                        :old.PERMISSION_ID,
		                        :new.PERMISSION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('RELATIONSHIP_ID',
		                        		:old.RELATIONSHIP_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_ID',
		                        		:old.APP_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('MODULE_ID',
		                        		:old.MODULE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('PERMISSION_ID',
		                        		:old.PERMISSION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'REL_APP_MOD_PERM'
	                                          );
	END IF;
END;
/

ALTER TRIGGER REL_APP_MOD_PERM_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER REL_GEO_REGION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON REL_GEO_REGION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('ROW_UID',
		                        :old.ROW_UID,
		                        :new.ROW_UID
		                       );
		DATA_INTG_PKG.CHECK_VAL('GEO_REGION_ID',
		                        :old.GEO_REGION_ID,
		                        :new.GEO_REGION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('RELATIONSHIP_ID',
		                        :old.RELATIONSHIP_ID,
		                        :new.RELATIONSHIP_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('RELATIONSHIP_ID',
		                        		:old.RELATIONSHIP_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('GEO_REGION_ID',
		                        		:old.GEO_REGION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'REL_GEO_REGION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER REL_GEO_REGION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ROLE_APP_MOD_PERM_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON ROLE_APP_MOD_PERM
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('MODULE_ID',
		                        :old.MODULE_ID,
		                        :new.MODULE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROW_UID',
		                        :old.ROW_UID,
		                        :new.ROW_UID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROLE_ID',
		                        :old.ROLE_ID,
		                        :new.ROLE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PERMISSION_ID',
		                        :old.PERMISSION_ID,
		                        :new.PERMISSION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('ROLE_ID',
		                        		:old.ROLE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_ID',
		                        		:old.APP_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('MODULE_ID',
		                        		:old.MODULE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('PERMISSION_ID',
		                        		:old.PERMISSION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'ROLE_APP_MOD_PERM'
	                                          );
	END IF;
END;
/

ALTER TRIGGER ROLE_APP_MOD_PERM_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ROLE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON ROLE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE_ID',
		                        :old.LAST_UPDATED_SOURCE_TYPE_ID,
		                        :new.LAST_UPDATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROLE_TYPE_ID',
		                        :old.ROLE_TYPE_ID,
		                        :new.ROLE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('APPLY_TO_BUSINESS_PARTNERS',
		                        :old.APPLY_TO_BUSINESS_PARTNERS,
		                        :new.APPLY_TO_BUSINESS_PARTNERS
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROLE_NAME',
		                        :old.ROLE_NAME,
		                        :new.ROLE_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROLE_ID',
		                        :old.ROLE_ID,
		                        :new.ROLE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ROLE_PRIVATE',
		                        :old.IS_ROLE_PRIVATE,
		                        :new.IS_ROLE_PRIVATE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE_ID',
		                        :old.CREATED_SOURCE_TYPE_ID,
		                        :new.CREATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('ROLE_ID',
		                        		:old.ROLE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'ROLE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER ROLE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ROLE_TEMPLATE_REFERENCE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON ROLE_TEMPLATE_REFERENCE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('REFERENCED_ROLE_TEMPLATE_ID',
		                        :old.REFERENCED_ROLE_TEMPLATE_ID,
		                        :new.REFERENCED_ROLE_TEMPLATE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROLE_ID',
		                        :old.ROLE_ID,
		                        :new.ROLE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('ROLE_ID',
		                        		:old.ROLE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('REFERENCED_ROLE_TEMPLATE_ID',
		                        		:old.REFERENCED_ROLE_TEMPLATE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'ROLE_TEMPLATE_REFERENCE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER ROLE_TEMPLATE_REFERENCE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ROLE_TYPE_DD_DEL_TRG
 BEFORE  DELETE
 ON ROLE_TYPE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER ROLE_TYPE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER ROLE_TYPE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON ROLE_TYPE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROLE_TYPE_ID',
		                        :old.ROLE_TYPE_ID,
		                        :new.ROLE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('ROLE_TYPE_ID',
		                        		:old.ROLE_TYPE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'ROLE_TYPE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER ROLE_TYPE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER SOURCE_TYPE_DD_DEL_TRG
 BEFORE  DELETE
 ON SOURCE_TYPE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER SOURCE_TYPE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER SOURCE_TYPE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON SOURCE_TYPE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('SOURCE_TYPE',
		                        :old.SOURCE_TYPE,
		                        :new.SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('SOURCE_TYPE',
		                        		:old.SOURCE_TYPE
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'SOURCE_TYPE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER SOURCE_TYPE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER STATE_PROV_DD_DEL_TRG
 BEFORE  DELETE
 ON STATE_PROV
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER STATE_PROV_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER STATE_PROV_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON STATE_PROV
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('COUNTRY_CODE',
		                        :old.COUNTRY_CODE,
		                        :new.COUNTRY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('STATE_PROV_NAME',
		                        :old.STATE_PROV_NAME,
		                        :new.STATE_PROV_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('STATE_PROV',
		                        :old.STATE_PROV,
		                        :new.STATE_PROV
		                       );
		DATA_INTG_PKG.CHECK_VAL('STATE_PROV_ID',
		                        :old.STATE_PROV_ID,
		                        :new.STATE_PROV_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('STATE_PROV',
		                        		:old.STATE_PROV
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('COUNTRY_CODE',
		                        		:old.COUNTRY_CODE
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'STATE_PROV'
	                                          );
	END IF;
END;
/

ALTER TRIGGER STATE_PROV_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER TIME_ZONE_DD_DEL_TRG
 BEFORE  DELETE
 ON TIME_ZONE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER TIME_ZONE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER TIME_ZONE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON TIME_ZONE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('TIME_ZONE_DISPLAY_NAME',
		                        :old.TIME_ZONE_DISPLAY_NAME,
		                        :new.TIME_ZONE_DISPLAY_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('TIME_ZONE_ID',
		                        :old.TIME_ZONE_ID,
		                        :new.TIME_ZONE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('GMT_OFFSET',
		                        :old.GMT_OFFSET,
		                        :new.GMT_OFFSET
		                       );
		DATA_INTG_PKG.CHECK_VAL('TIME_ZONE_NAME',
		                        :old.TIME_ZONE_NAME,
		                        :new.TIME_ZONE_NAME
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('TIME_ZONE_ID',
		                        		:old.TIME_ZONE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'TIME_ZONE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER TIME_ZONE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UCL_GROUP_TYPE_DD_DEL_TRG
 BEFORE  DELETE
 ON UCL_GROUP_TYPE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER UCL_GROUP_TYPE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER UCL_GROUP_TYPE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UCL_GROUP_TYPE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('UCL_GROUP_TYPE_ID',
		                        :old.UCL_GROUP_TYPE_ID,
		                        :new.UCL_GROUP_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('UCL_GROUP_TYPE_ID',
		                        		:old.UCL_GROUP_TYPE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UCL_GROUP_TYPE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UCL_GROUP_TYPE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UCL_PARAM_DEF_DD_DEL_TRG
 BEFORE  DELETE
 ON UCL_PARAM_DEF
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER UCL_PARAM_DEF_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER UCL_PARAM_DEF_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UCL_PARAM_DEF
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('COMPANY_TYPE',
		                        :old.COMPANY_TYPE,
		                        :new.COMPANY_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('UI_TYPE',
		                        :old.UI_TYPE,
		                        :new.UI_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('DEFAULT_VALUE',
		                        :old.DEFAULT_VALUE,
		                        :new.DEFAULT_VALUE
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARAM_DEF_ID',
		                        :old.PARAM_DEF_ID,
		                        :new.PARAM_DEF_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('PARAM_DEF_ID',
		                        		:old.PARAM_DEF_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UCL_PARAM_DEF'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UCL_PARAM_DEF_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UCL_USER_DD_DEL_TRG
 BEFORE  DELETE
 ON UCL_USER
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER UCL_USER_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER UCL_USER_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UCL_USER
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CHANNEL_ID',
		                        :old.CHANNEL_ID,
		                        :new.CHANNEL_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('PASSWORD_RESET_DATE_TIME',
		                        :old.PASSWORD_RESET_DATE_TIME,
		                        :new.PASSWORD_RESET_DATE_TIME
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMM_METHOD_ID_AFTER_BH_1',
		                        :old.COMM_METHOD_ID_AFTER_BH_1,
		                        :new.COMM_METHOD_ID_AFTER_BH_1
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_EMAIL_2',
		                        :old.USER_EMAIL_2,
		                        :new.USER_EMAIL_2
		                       );
		DATA_INTG_PKG.CHECK_VAL('TAX_ID_NBR',
		                        :old.TAX_ID_NBR,
		                        :new.TAX_ID_NBR
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMM_METHOD_ID_AFTER_BH_2',
		                        :old.COMM_METHOD_ID_AFTER_BH_2,
		                        :new.COMM_METHOD_ID_AFTER_BH_2
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_EMAIL_1',
		                        :old.USER_EMAIL_1,
		                        :new.USER_EMAIL_1
		                       );
		DATA_INTG_PKG.CHECK_VAL('LOCALE_ID',
		                        :old.LOCALE_ID,
		                        :new.LOCALE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('FAX_NUMBER',
		                        :old.FAX_NUMBER,
		                        :new.FAX_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_LAST_NAME',
		                        :old.USER_LAST_NAME,
		                        :new.USER_LAST_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_TITLE',
		                        :old.USER_TITLE,
		                        :new.USER_TITLE
		                       );
		DATA_INTG_PKG.CHECK_VAL('ISPASSWORDMANAGEDINTERNALLY',
		                        :old.ISPASSWORDMANAGEDINTERNALLY,
		                        :new.ISPASSWORDMANAGEDINTERNALLY
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMM_METHOD_ID_DURING_BH_2',
		                        :old.COMM_METHOD_ID_DURING_BH_2,
		                        :new.COMM_METHOD_ID_DURING_BH_2
		                       );
		DATA_INTG_PKG.CHECK_VAL('DEFAULT_WHSE_REGION_ID',
		                        :old.DEFAULT_WHSE_REGION_ID,
		                        :new.DEFAULT_WHSE_REGION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMM_METHOD_ID_DURING_BH_1',
		                        :old.COMM_METHOD_ID_DURING_BH_1,
		                        :new.COMM_METHOD_ID_DURING_BH_1
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_LOGIN_DTTM',
		                        :old.LAST_LOGIN_DTTM,
		                        :new.LAST_LOGIN_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_MIDDLE_NAME',
		                        :old.USER_MIDDLE_NAME,
		                        :new.USER_MIDDLE_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('TELEPHONE_NUMBER',
		                        :old.TELEPHONE_NUMBER,
		                        :new.TELEPHONE_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMMON_NAME',
		                        :old.COMMON_NAME,
		                        :new.COMMON_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_FIRST_NAME',
		                        :old.USER_FIRST_NAME,
		                        :new.USER_FIRST_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE_ID',
		                        :old.LAST_UPDATED_SOURCE_TYPE_ID,
		                        :new.LAST_UPDATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('ADDRESS_1',
		                        :old.ADDRESS_1,
		                        :new.ADDRESS_1
		                       );
		DATA_INTG_PKG.CHECK_VAL('ADDRESS_2',
		                        :old.ADDRESS_2,
		                        :new.ADDRESS_2
		                       );
		DATA_INTG_PKG.CHECK_VAL('COUNTRY_CODE',
		                        :old.COUNTRY_CODE,
		                        :new.COUNTRY_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('UCL_USER_ID',
		                        :old.UCL_USER_ID,
		                        :new.UCL_USER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LOGGED_IN',
		                        :old.LOGGED_IN,
		                        :new.LOGGED_IN
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_PASSWORD',
		                        :old.USER_PASSWORD,
		                        :new.USER_PASSWORD
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_TYPE_ID',
		                        :old.USER_TYPE_ID,
		                        :new.USER_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_PREFIX',
		                        :old.USER_PREFIX,
		                        :new.USER_PREFIX
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
		DATA_INTG_PKG.CHECK_VAL('PASSWORD_TOKEN',
		                        :old.PASSWORD_TOKEN,
		                        :new.PASSWORD_TOKEN
		                       );
		DATA_INTG_PKG.CHECK_VAL('NUMBER_OF_INVALID_LOGINS',
		                        :old.NUMBER_OF_INVALID_LOGINS,
		                        :new.NUMBER_OF_INVALID_LOGINS
		                       );
		DATA_INTG_PKG.CHECK_VAL('STATE_PROV_CODE',
		                        :old.STATE_PROV_CODE,
		                        :new.STATE_PROV_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('DEFAULT_BUSINESS_UNIT_ID',
		                        :old.DEFAULT_BUSINESS_UNIT_ID,
		                        :new.DEFAULT_BUSINESS_UNIT_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LOCATION_ID',
		                        :old.LOCATION_ID,
		                        :new.LOCATION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_PASSWORD_CHANGE_DTTM',
		                        :old.LAST_PASSWORD_CHANGE_DTTM,
		                        :new.LAST_PASSWORD_CHANGE_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_NAME',
		                        :old.USER_NAME,
		                        :new.USER_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('BIRTH_DATE',
		                        :old.BIRTH_DATE,
		                        :new.BIRTH_DATE
		                       );
		DATA_INTG_PKG.CHECK_VAL('GENDER_ID',
		                        :old.GENDER_ID,
		                        :new.GENDER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('EMP_START_DATE',
		                        :old.EMP_START_DATE,
		                        :new.EMP_START_DATE
		                       );
		DATA_INTG_PKG.CHECK_VAL('POSTAL_CODE',
		                        :old.POSTAL_CODE,
		                        :new.POSTAL_CODE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CITY',
		                        :old.CITY,
		                        :new.CITY
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE_ID',
		                        :old.CREATED_SOURCE_TYPE_ID,
		                        :new.CREATED_SOURCE_TYPE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('UCL_USER_ID',
		                        		:old.UCL_USER_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UCL_USER'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UCL_USER_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UCL_USER_FUNCTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UCL_USER_FUNCTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('USER_FUNCTION_ID',
		                        :old.USER_FUNCTION_ID,
		                        :new.USER_FUNCTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('UCL_USER_ID',
		                        :old.UCL_USER_ID,
		                        :new.UCL_USER_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('UCL_USER_ID',
		                        		:old.UCL_USER_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('USER_FUNCTION_ID',
		                        		:old.USER_FUNCTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UCL_USER_FUNCTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UCL_USER_FUNCTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UIB_QUERY_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UIB_QUERY
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('UIB_QUERY_ID',
		                        :old.UIB_QUERY_ID,
		                        :new.UIB_QUERY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('QUERY_DESCRIPTION',
		                        :old.QUERY_DESCRIPTION,
		                        :new.QUERY_DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('QUERY_NAME',
		                        :old.QUERY_NAME,
		                        :new.QUERY_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CACHE_TIMEOUT_ID',
		                        :old.CACHE_TIMEOUT_ID,
		                        :new.CACHE_TIMEOUT_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('QUERY_TYPE_ID',
		                        :old.QUERY_TYPE_ID,
		                        :new.QUERY_TYPE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('UIB_QUERY_ID',
		                        		:old.UIB_QUERY_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UIB_QUERY'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UIB_QUERY_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UIB_QUERY_DETAILS_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UIB_QUERY_DETAILS
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('QUERY_COLUMNS',
		                        :old.QUERY_COLUMNS,
		                        :new.QUERY_COLUMNS
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_QUERY_ID',
		                        :old.UIB_QUERY_ID,
		                        :new.UIB_QUERY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_QUERY_DETAILS_ID',
		                        :old.UIB_QUERY_DETAILS_ID,
		                        :new.UIB_QUERY_DETAILS_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('QUERY_WHERE_CLAUSE',
		                        :old.QUERY_WHERE_CLAUSE,
		                        :new.QUERY_WHERE_CLAUSE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_SQL_FLAVOR_ID',
		                        :old.UIB_SQL_FLAVOR_ID,
		                        :new.UIB_SQL_FLAVOR_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('QUERY_TABLES',
		                        :old.QUERY_TABLES,
		                        :new.QUERY_TABLES
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('UIB_QUERY_DETAILS_ID',
		                        		:old.UIB_QUERY_DETAILS_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UIB_QUERY_DETAILS'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UIB_QUERY_DETAILS_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UIB_SMRY_VIEW_DTL_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UIB_SMRY_VIEW_DTL
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('STYLE',
		                        :old.STYLE,
		                        :new.STYLE
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_SMRY_VIEW_DTL_ID',
		                        :old.UIB_SMRY_VIEW_DTL_ID,
		                        :new.UIB_SMRY_VIEW_DTL_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('URL',
		                        :old.URL,
		                        :new.URL
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('OPTIONS',
		                        :old.OPTIONS,
		                        :new.OPTIONS
		                       );
		DATA_INTG_PKG.CHECK_VAL('DSHBRD_PORTLET_ID',
		                        :old.DSHBRD_PORTLET_ID,
		                        :new.DSHBRD_PORTLET_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('UIB_SMRY_VIEW_DTL_ID',
		                        		:old.UIB_SMRY_VIEW_DTL_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UIB_SMRY_VIEW_DTL'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UIB_SMRY_VIEW_DTL_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UIB_SP_DETAILS_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UIB_SP_DETAILS
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('UIB_QUERY_ID',
		                        :old.UIB_QUERY_ID,
		                        :new.UIB_QUERY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('SP_PARAM_LIST',
		                        :old.SP_PARAM_LIST,
		                        :new.SP_PARAM_LIST
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('SP_NAME',
		                        :old.SP_NAME,
		                        :new.SP_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_SP_DETAILS_ID',
		                        :old.UIB_SP_DETAILS_ID,
		                        :new.UIB_SP_DETAILS_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('UIB_SP_DETAILS_ID',
		                        		:old.UIB_SP_DETAILS_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UIB_SP_DETAILS'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UIB_SP_DETAILS_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UIB_VIEW_PROP_SET_DTL_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UIB_VIEW_PROP_SET_DTL
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('UIB_VIEW_PROPERTY_SET_ID',
		                        :old.UIB_VIEW_PROPERTY_SET_ID,
		                        :new.UIB_VIEW_PROPERTY_SET_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_VIEW_PROPERTY_DEF_ID',
		                        :old.UIB_VIEW_PROPERTY_DEF_ID,
		                        :new.UIB_VIEW_PROPERTY_DEF_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('INDEX_LVL_1',
		                        :old.INDEX_LVL_1,
		                        :new.INDEX_LVL_1
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('VALUE',
		                        :old.VALUE,
		                        :new.VALUE
		                       );
		DATA_INTG_PKG.CHECK_VAL('INDEX_LVL_2',
		                        :old.INDEX_LVL_2,
		                        :new.INDEX_LVL_2
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_VIEW_PROP_SET_DTL_ID',
		                        :old.UIB_VIEW_PROP_SET_DTL_ID,
		                        :new.UIB_VIEW_PROP_SET_DTL_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('UIB_VIEW_PROP_SET_DTL_ID',
		                        		:old.UIB_VIEW_PROP_SET_DTL_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UIB_VIEW_PROP_SET_DTL'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UIB_VIEW_PROP_SET_DTL_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER UIB_VIW_PROP_SETS_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON UIB_VIEW_PROPERTY_SETS
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('UIB_VIEW_PROPERTY_SET_ID',
		                        :old.UIB_VIEW_PROPERTY_SET_ID,
		                        :new.UIB_VIEW_PROPERTY_SET_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_DEFAULT',
		                        :old.IS_DEFAULT,
		                        :new.IS_DEFAULT
		                       );
		DATA_INTG_PKG.CHECK_VAL('PROPERTY_SET_NAME',
		                        :old.PROPERTY_SET_NAME,
		                        :new.PROPERTY_SET_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('UIB_VIEW_TYPE_ID',
		                        :old.UIB_VIEW_TYPE_ID,
		                        :new.UIB_VIEW_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_PRIVATE',
		                        :old.IS_PRIVATE,
		                        :new.IS_PRIVATE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('UIB_VIEW_PROPERTY_SET_ID',
		                        		:old.UIB_VIEW_PROPERTY_SET_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'UIB_VIEW_PROPERTY_SETS'
	                                          );
	END IF;
END;
/

ALTER TRIGGER UIB_VIW_PROP_SETS_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_DEFAULT_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON USER_DEFAULT
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('USER_DEFAULT_ID',
		                        :old.USER_DEFAULT_ID,
		                        :new.USER_DEFAULT_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARAMETER_NAME',
		                        :old.PARAMETER_NAME,
		                        :new.PARAMETER_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('UCL_USER_ID',
		                        :old.UCL_USER_ID,
		                        :new.UCL_USER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('PARAMETER_VALUE',
		                        :old.PARAMETER_VALUE,
		                        :new.PARAMETER_VALUE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('USER_DEFAULT_ID',
		                        		:old.USER_DEFAULT_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'USER_DEFAULT'
	                                          );
	END IF;
END;
/

ALTER TRIGGER USER_DEFAULT_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_FUNCTION_DD_DEL_TRG
 BEFORE  DELETE
 ON USER_FUNCTION
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER USER_FUNCTION_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_FUNCTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON USER_FUNCTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('USER_FUNCTION_ID',
		                        :old.USER_FUNCTION_ID,
		                        :new.USER_FUNCTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('USER_FUNCTION_ID',
		                        		:old.USER_FUNCTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'USER_FUNCTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER USER_FUNCTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_GROUP_DD_DEL_TRG
 BEFORE  DELETE
 ON USER_GROUP
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER USER_GROUP_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_GROUP_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON USER_GROUP
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('HIBERNATE_VERSION',
		                        :old.HIBERNATE_VERSION,
		                        :new.HIBERNATE_VERSION
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE_ID',
		                        :old.LAST_UPDATED_SOURCE_TYPE_ID,
		                        :new.LAST_UPDATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_GROUP_NAME',
		                        :old.USER_GROUP_NAME,
		                        :new.USER_GROUP_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('GROUP_LEAD_USER_ID',
		                        :old.GROUP_LEAD_USER_ID,
		                        :new.GROUP_LEAD_USER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE_ID',
		                        :old.CREATED_SOURCE_TYPE_ID,
		                        :new.CREATED_SOURCE_TYPE_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_GROUP_ID',
		                        :old.USER_GROUP_ID,
		                        :new.USER_GROUP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_GROUP_DESCRIPTION',
		                        :old.USER_GROUP_DESCRIPTION,
		                        :new.USER_GROUP_DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('IS_ACTIVE',
		                        :old.IS_ACTIVE,
		                        :new.IS_ACTIVE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('USER_GROUP_ID',
		                        		:old.USER_GROUP_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'USER_GROUP'
	                                          );
	END IF;
END;
/

ALTER TRIGGER USER_GROUP_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_TYPE_DD_DEL_TRG
 BEFORE  DELETE
 ON USER_TYPE
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER USER_TYPE_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_TYPE_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON USER_TYPE
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('ABBREVIATION',
		                        :old.ABBREVIATION,
		                        :new.ABBREVIATION
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_TYPE_ID',
		                        :old.USER_TYPE_ID,
		                        :new.USER_TYPE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('USER_TYPE_ID',
		                        		:old.USER_TYPE_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'USER_TYPE'
	                                          );
	END IF;
END;
/

ALTER TRIGGER USER_TYPE_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_TYPE_FUNCTION_DD_DEL_TRG
 BEFORE  DELETE
 ON USER_TYPE_FUNCTION
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER USER_TYPE_FUNCTION_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_TYPE_FUNCTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON USER_TYPE_FUNCTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('USER_FUNCTION_ID',
		                        :old.USER_FUNCTION_ID,
		                        :new.USER_FUNCTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('USER_TYPE_ID',
		                        :old.USER_TYPE_ID,
		                        :new.USER_TYPE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('USER_TYPE_ID',
		                        		:old.USER_TYPE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('USER_FUNCTION_ID',
		                        		:old.USER_FUNCTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'USER_TYPE_FUNCTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER USER_TYPE_FUNCTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER VENDOR_ROLE_ASSIGNMENT_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON VENDOR_ROLE_ASSIGNMENT
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('ROLE_ID',
		                        :old.ROLE_ID,
		                        :new.ROLE_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('ROLE_ID',
		                        		:old.ROLE_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('COMPANY_ID',
		                        		:old.COMPANY_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'VENDOR_ROLE_ASSIGNMENT'
	                                          );
	END IF;
END;
/

ALTER TRIGGER VENDOR_ROLE_ASSIGNMENT_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XCOMPANY_GROUP_COMPANY_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XCOMPANY_GROUP_COMPANY
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('XCOMPANY_GROUP_ID',
		                        :old.XCOMPANY_GROUP_ID,
		                        :new.XCOMPANY_GROUP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('XCOMPANY_GROUP_COMPANY_ID',
		                        :old.XCOMPANY_GROUP_COMPANY_ID,
		                        :new.XCOMPANY_GROUP_COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XCOMPANY_GROUP_COMPANY_ID',
		                        		:old.XCOMPANY_GROUP_COMPANY_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XCOMPANY_GROUP_COMPANY'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XCOMPANY_GROUP_COMPANY_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XCOMPANY_GROUP_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XCOMPANY_GROUP
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('NAME',
		                        :old.NAME,
		                        :new.NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('XCOMPANY_GROUP_ID',
		                        :old.XCOMPANY_GROUP_ID,
		                        :new.XCOMPANY_GROUP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XCOMPANY_GROUP_ID',
		                        		:old.XCOMPANY_GROUP_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XCOMPANY_GROUP'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XCOMPANY_GROUP_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XCOMPANY_RESTRICTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XCOMPANY_RESTRICTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('XRESTRICTION_ID',
		                        :old.XRESTRICTION_ID,
		                        :new.XRESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('XCOMPANY_RESTRICTION_ID',
		                        :old.XCOMPANY_RESTRICTION_ID,
		                        :new.XCOMPANY_RESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XCOMPANY_RESTRICTION_ID',
		                        		:old.XCOMPANY_RESTRICTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XCOMPANY_RESTRICTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XCOMPANY_RESTRICTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XCOMP_GRP_RESTRICTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XCOMPANY_GROUP_RESTRICTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('XCOMPANY_GROUP_RESTRICTION_ID',
		                        :old.XCOMPANY_GROUP_RESTRICTION_ID,
		                        :new.XCOMPANY_GROUP_RESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('XRESTRICTION_ID',
		                        :old.XRESTRICTION_ID,
		                        :new.XRESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('XCOMPANY_GROUP_ID',
		                        :old.XCOMPANY_GROUP_ID,
		                        :new.XCOMPANY_GROUP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XCOMPANY_GROUP_RESTRICTION_ID',
		                        		:old.XCOMPANY_GROUP_RESTRICTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XCOMPANY_GROUP_RESTRICTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XCOMP_GRP_RESTRICTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XFIELD_RESTRICTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XFIELD_RESTRICTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('XFIELD_ID',
		                        :old.XFIELD_ID,
		                        :new.XFIELD_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('XRESTRICTION_ID',
		                        :old.XRESTRICTION_ID,
		                        :new.XRESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('XFIELD_RESTRICTION_ID',
		                        :old.XFIELD_RESTRICTION_ID,
		                        :new.XFIELD_RESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XFIELD_RESTRICTION_ID',
		                        		:old.XFIELD_RESTRICTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XFIELD_RESTRICTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XFIELD_RESTRICTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XRESTRICTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XRESTRICTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('NAME',
		                        :old.NAME,
		                        :new.NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('XRESTRICTION_ID',
		                        :old.XRESTRICTION_ID,
		                        :new.XRESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('RESTRICTION_TYPE',
		                        :old.RESTRICTION_TYPE,
		                        :new.RESTRICTION_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XRESTRICTION_ID',
		                        		:old.XRESTRICTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XRESTRICTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XRESTRICTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XSOLUTION_APP_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XSOLUTION_APP
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('XSOLUTION_ID',
		                        :old.XSOLUTION_ID,
		                        :new.XSOLUTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('APP_ID',
		                        :old.APP_ID,
		                        :new.APP_ID
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XSOLUTION_ID',
		                        		:old.XSOLUTION_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('APP_ID',
		                        		:old.APP_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XSOLUTION_APP'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XSOLUTION_APP_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XSOLUTION_DD_DEL_TRG
 BEFORE  DELETE
 ON XSOLUTION
 REFERENCING OLD AS O NEW AS N
 FOR EACH ROW
 BEGIN
 raise_application_error(-20001,'MANH Error - Data DELETE not allowed on this table');
 END;
/

ALTER TRIGGER XSOLUTION_DD_DEL_TRG ENABLE;

CREATE OR REPLACE TRIGGER XSOLUTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XSOLUTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('NAME',
		                        :old.NAME,
		                        :new.NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('XSOLUTION_ID',
		                        :old.XSOLUTION_ID,
		                        :new.XSOLUTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('NUM_OPEN_WORKSPACES',
		                        :old.NUM_OPEN_WORKSPACES,
		                        :new.NUM_OPEN_WORKSPACES
		                       );
		DATA_INTG_PKG.CHECK_VAL('ENABLE_NOTIFICATIONS',
		                        :old.ENABLE_NOTIFICATIONS,
		                        :new.ENABLE_NOTIFICATIONS
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('DEPLOYMENT_ID',
		                        :old.DEPLOYMENT_ID,
		                        :new.DEPLOYMENT_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('NUM_ACTIVE_WINDOWS',
		                        :old.NUM_ACTIVE_WINDOWS,
		                        :new.NUM_ACTIVE_WINDOWS
		                       );
		DATA_INTG_PKG.CHECK_VAL('HOST_NAME',
		                        :old.HOST_NAME,
		                        :new.HOST_NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('PORT_NUMBER',
		                        :old.PORT_NUMBER,
		                        :new.PORT_NUMBER
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('ENABLE_SEARCH',
		                        :old.ENABLE_SEARCH,
		                        :new.ENABLE_SEARCH
		                       );
		DATA_INTG_PKG.CHECK_VAL('NUM_ACTIVE_WORKSPACES',
		                        :old.NUM_ACTIVE_WORKSPACES,
		                        :new.NUM_ACTIVE_WORKSPACES
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('NUM_OPEN_WINDOWS',
		                        :old.NUM_OPEN_WINDOWS,
		                        :new.NUM_OPEN_WINDOWS
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('USE_HTTPS',
		                        :old.USE_HTTPS,
		                        :new.USE_HTTPS
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XSOLUTION_ID',
		                        		:old.XSOLUTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XSOLUTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XSOLUTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XSOLUTION_RESTRICTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XSOLUTION_RESTRICTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('XRESTRICTION_ID',
		                        :old.XRESTRICTION_ID,
		                        :new.XRESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('XSOLUTION_ID',
		                        :old.XSOLUTION_ID,
		                        :new.XSOLUTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('XSOLUTION_RESTRICTION_ID',
		                        :old.XSOLUTION_RESTRICTION_ID,
		                        :new.XSOLUTION_RESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XSOLUTION_RESTRICTION_ID',
		                        		:old.XSOLUTION_RESTRICTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XSOLUTION_RESTRICTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XSOLUTION_RESTRICTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XSOLUTION_USER_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XSOLUTION_USER
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('XSOLUTION_ID',
		                        :old.XSOLUTION_ID,
		                        :new.XSOLUTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('UCL_USER_ID',
		                        :old.UCL_USER_ID,
		                        :new.UCL_USER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XSOLUTION_ID',
		                        		:old.XSOLUTION_ID
		                       			);
		DATA_INTG_PKG.GET_PK_DATA_STRING('UCL_USER_ID',
		                        		:old.UCL_USER_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XSOLUTION_USER'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XSOLUTION_USER_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XSOLUTION_XREF_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XSOLUTION_XREF
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('XREF_SOLUTION_ID',
		                        :old.XREF_SOLUTION_ID,
		                        :new.XREF_SOLUTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('XSOLUTION_ID',
		                        :old.XSOLUTION_ID,
		                        :new.XSOLUTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('XSOLUTION_XREF_ID',
		                        :old.XSOLUTION_XREF_ID,
		                        :new.XSOLUTION_XREF_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XSOLUTION_XREF_ID',
		                        		:old.XSOLUTION_XREF_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XSOLUTION_XREF'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XSOLUTION_XREF_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XUSER_GROUP_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XUSER_GROUP
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('NAME',
		                        :old.NAME,
		                        :new.NAME
		                       );
		DATA_INTG_PKG.CHECK_VAL('DESCRIPTION',
		                        :old.DESCRIPTION,
		                        :new.DESCRIPTION
		                       );
		DATA_INTG_PKG.CHECK_VAL('COMPANY_ID',
		                        :old.COMPANY_ID,
		                        :new.COMPANY_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('XUSER_GROUP_ID',
		                        :old.XUSER_GROUP_ID,
		                        :new.XUSER_GROUP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XUSER_GROUP_ID',
		                        		:old.XUSER_GROUP_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XUSER_GROUP'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XUSER_GROUP_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XUSER_GROUP_RESTRICTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XUSER_GROUP_RESTRICTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('XRESTRICTION_ID',
		                        :old.XRESTRICTION_ID,
		                        :new.XRESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('XUSER_GROUP_RESTRICTION_ID',
		                        :old.XUSER_GROUP_RESTRICTION_ID,
		                        :new.XUSER_GROUP_RESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('XUSER_GROUP_ID',
		                        :old.XUSER_GROUP_ID,
		                        :new.XUSER_GROUP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XUSER_GROUP_RESTRICTION_ID',
		                        		:old.XUSER_GROUP_RESTRICTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XUSER_GROUP_RESTRICTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XUSER_GROUP_RESTRICTION_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XUSER_GROUP_USER_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XUSER_GROUP_USER
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('UCL_USER_ID',
		                        :old.UCL_USER_ID,
		                        :new.UCL_USER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('XUSER_GROUP_ID',
		                        :old.XUSER_GROUP_ID,
		                        :new.XUSER_GROUP_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('XUSER_GROUP_USER_ID',
		                        :old.XUSER_GROUP_USER_ID,
		                        :new.XUSER_GROUP_USER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XUSER_GROUP_USER_ID',
		                        		:old.XUSER_GROUP_USER_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XUSER_GROUP_USER'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XUSER_GROUP_USER_DD_TRG ENABLE;

CREATE OR REPLACE TRIGGER XUSER_RESTRICTION_DD_TRG
BEFORE INSERT OR DELETE OR UPDATE ON XUSER_RESTRICTION
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
dd_data              VARCHAR2 (4000);
append_comma         CHAR(1);
v_action             varchar2(20);
v_pk_id  number;
BEGIN
	DATA_INTG_PKG.CLEAR_VAL;
	IF INSERTING THEN
		v_action := 'INSERT';
	ELSIF UPDATING THEN
		v_action := 'UPDATE';
	ELSIF DELETING THEN
		v_action := 'DELETE';
	END IF;
	IF (INSERTING OR UPDATING) THEN
		DATA_INTG_PKG.CHECK_VAL('XUSER_RESTRICTION_ID',
		                        :old.XUSER_RESTRICTION_ID,
		                        :new.XUSER_RESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('XRESTRICTION_ID',
		                        :old.XRESTRICTION_ID,
		                        :new.XRESTRICTION_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE_TYPE',
		                        :old.CREATED_SOURCE_TYPE,
		                        :new.CREATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('UCL_USER_ID',
		                        :old.UCL_USER_ID,
		                        :new.UCL_USER_ID
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE',
		                        :old.LAST_UPDATED_SOURCE,
		                        :new.LAST_UPDATED_SOURCE
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_SOURCE_TYPE',
		                        :old.LAST_UPDATED_SOURCE_TYPE,
		                        :new.LAST_UPDATED_SOURCE_TYPE
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_DTTM',
		                        :old.CREATED_DTTM,
		                        :new.CREATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('LAST_UPDATED_DTTM',
		                        :old.LAST_UPDATED_DTTM,
		                        :new.LAST_UPDATED_DTTM
		                       );
		DATA_INTG_PKG.CHECK_VAL('CREATED_SOURCE',
		                        :old.CREATED_SOURCE,
		                        :new.CREATED_SOURCE
		                       );
	END IF;
	IF DELETING OR DATA_INTG_PKG.IS_DATA_CHANGED = 'Y' THEN
		DATA_INTG_PKG.GET_PK_DATA_STRING('XUSER_RESTRICTION_ID',
		                        		:old.XUSER_RESTRICTION_ID
		                       			);
		dd_data := DATA_INTG_PKG.GET_VAL;
		DATA_INTG_PKG.ADD_TO_OUTBOUND_STAGING(
	                                          v_action
	                                          , 'XUSER_RESTRICTION'
	                                          );
	END IF;
END;
/

ALTER TRIGGER XUSER_RESTRICTION_DD_TRG ENABLE;

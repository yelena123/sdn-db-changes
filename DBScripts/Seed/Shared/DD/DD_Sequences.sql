SET DEFINE OFF;
SET ECHO ON;
create sequence DATA_MAPPER_ID_SEQ start with 1 maxvalue 9999999999999999999999999999 increment by 1 nocycle cache 20 noorder;
create sequence DATA_SYNC_SUBSCRIBER_ID_SEQ start with 1 maxvalue 9999999999999999999999999999 increment by 1 nocycle cache 20 noorder;
create sequence DD_SUBSCRIBER_DETAIL_ID_SEQ start with 21 maxvalue 9999999999999999999999999999 increment by 1 nocycle cache 20 noorder;
create sequence DD_SYNC_RULE_ID_SEQ start with 1 maxvalue 9999999999999999999999999999 increment by 1 nocycle cache 20 noorder;
create sequence SEQ_DD_ERROR_LOG start with 1 maxvalue 9999999999999999999999999999 increment by 1 nocycle cache 20 noorder;
create sequence SEQ_DD_ERROR_LOG_DETAIL start with 1 maxvalue 9999999999999999999999999999 increment by 1 nocycle cache 20 noorder;
create sequence SEQ_DD_STAGING_DATA start with 1 maxvalue 9999999999999999999999999999 increment by 1 nocycle cache 20 noorder;

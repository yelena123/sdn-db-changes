set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DD_SUBSCRIBER

INSERT INTO DD_SUBSCRIBER ( SUBSCRIBER_ID,SUBSCRIBER_NAME,IS_ENABLE,SUBSCRIBER_SCHEMA_NAME) 
VALUES  ( 1,'DM(WM-LM).ma-india-L237R:ORA237:EXTRACTLMDA',1,'ma-india-L237R:ORA237:EXTRACTLMDA');

Commit;





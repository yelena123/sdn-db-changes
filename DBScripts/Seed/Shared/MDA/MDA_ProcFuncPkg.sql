SET DEFINE OFF;
SET ECHO ON;

CREATE OR REPLACE TYPE T_ENDPOINT_NAME_SEQ AS TABLE OF VARCHAR2 (40);
/

CREATE OR REPLACE FUNCTION CL_GET_PRIORITY (P_ENDPOINTID   IN CL_ENDPOINT.ENDPOINT_ID%TYPE,
                      P_EVENTID      IN CL_EVENT_MASTER.EVENT_ID%TYPE,
                      P_PRIORITY     IN CL_ENDPOINT_QUEUE.PRTY%TYPE)
 RETURN CL_ENDPOINT_QUEUE.PRTY%TYPE
IS
 NEW_PRIORITY                CL_ENDPOINT_QUEUE.PRTY%TYPE;
 DEFAULT_PRIORITY   CONSTANT CL_ENDPOINT_QUEUE.PRTY%TYPE := 0;
BEGIN
 -- Do we have a priority?
 IF P_PRIORITY IS NOT NULL
 THEN
  RETURN P_PRIORITY;
 END IF;
 -- Does the event have a default priority?
 SELECT DFLT_MSG_PRTY
 INTO NEW_PRIORITY
 FROM CL_EVENT_MASTER
WHERE EVENT_ID = P_EVENTID;
 IF NEW_PRIORITY IS NOT NULL
 THEN
  RETURN NEW_PRIORITY;
 END IF;
 -- Does the endpoint have a default priority?
 SELECT DFLT_MSG_PRTY
 INTO NEW_PRIORITY
 FROM CL_ENDPOINT
WHERE ENDPOINT_ID = P_ENDPOINTID;
 IF NEW_PRIORITY IS NOT NULL
 THEN
  RETURN NEW_PRIORITY;
 END IF;
 -- Use the (hardwired) default priority
 RETURN DEFAULT_PRIORITY;
EXCEPTION
 WHEN OTHERS
 THEN
  RAISE_APPLICATION_ERROR (-20010, SQLERRM);
END;
/

CREATE OR REPLACE FUNCTION CL_PARSE_ENDPOINT_NAME_CSV (P_ENDPOINT_NAME_CSV IN VARCHAR2)
 RETURN T_ENDPOINT_NAME_SEQ
 PIPELINED
AS
 V_CURRENT_ENDPOINT      VARCHAR2 (50);
 V_REMAINING_STR         VARCHAR2 (2048) := P_ENDPOINT_NAME_CSV;
 V_INPUT_DELIMITER_POS   NUMBER (5) := 0;
BEGIN
 LOOP
  V_INPUT_DELIMITER_POS :=
     COALESCE (INSTR (V_REMAINING_STR,
                      ',',
                      1,
                      1),
               0);
  -- check: need to trim? assumes no spaces
  IF (V_INPUT_DELIMITER_POS = 0)
  THEN
     V_CURRENT_ENDPOINT := V_REMAINING_STR;
     V_REMAINING_STR := '';
  ELSE
     V_CURRENT_ENDPOINT :=
        SUBSTR (V_REMAINING_STR, 1, V_INPUT_DELIMITER_POS - 1);
     V_REMAINING_STR :=
        SUBSTR (V_REMAINING_STR, V_INPUT_DELIMITER_POS + 1);
  END IF;
  PIPE ROW (V_CURRENT_ENDPOINT);
  IF (V_INPUT_DELIMITER_POS = 0)
  THEN
     EXIT;
  END IF;
 END LOOP;
 RETURN;
END;
/

CREATE OR REPLACE FUNCTION FN_GET_MSGLNTXT (P_TRAN_LOG_ID IN NUMBER)
 RETURN CLOB
IS
 v_msg_list   CLOB;
BEGIN
 SELECT DBMS_XMLGEN.CONVERT (
           EXTRACT (
              XMLAGG (XMLELEMENT (y, MSG_LINE_TEXT)
                      ORDER BY msg_line_number),
              '//text()').getclobval (),
           1)
           VALUE
   INTO v_msg_list
   FROM TRAN_LOG_MESSAGE
  WHERE tran_log_id = P_TRAN_LOG_ID
 GROUP BY TRAN_LOG_ID;
 RETURN v_msg_list;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (
     SQLCODE || 'FN_GET_MSGLNTXT:Exception => ' || SQLERRM);
  ROLLBACK;
END FN_GET_MSGLNTXT;
/

CREATE OR REPLACE FUNCTION FN_GET_RMSGLNTXT (P_MESSAGE_ID IN NUMBER)
 RETURN CLOB
IS
 v_msg_list   CLOB;
BEGIN
 SELECT DBMS_XMLGEN.CONVERT (
           EXTRACT (
              XMLAGG (XMLELEMENT (y, MSG_LINE_TEXT)
                      ORDER BY msg_line_number),
              '//text()').getclobval (),
           1)
           VALUE
   INTO v_msg_list
   FROM TRAN_LOG_RESPONSE_MESSAGE
  WHERE message_id = P_MESSAGE_ID
 GROUP BY MESSAGE_ID;
 RETURN v_msg_list;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (
     SQLCODE || 'FN_GET_RMSGLNTXT:Exception => ' || SQLERRM);
  ROLLBACK;
END FN_GET_RMSGLNTXT;
/

CREATE OR REPLACE FUNCTION GETDATE
 RETURN DATE
AS
BEGIN
 RETURN SYSDATE;
END Getdate;
/

CREATE OR REPLACE FUNCTION GET_COL_LIST (p_table_name IN VARCHAR2)
 RETURN CLOB
IS
 v_col_list   CLOB;
 v_obj_type   VARCHAR2 (20);
 CURSOR cur_get_col_list
 IS
  (SELECT column_name, column_id
     FROM user_tab_columns
    WHERE table_name = UPPER (p_table_name))
  ORDER BY column_id;
BEGIN
 FOR currec IN cur_get_col_list
 LOOP
  IF (currec.column_id = 1)
  THEN
     v_col_list := currec.column_name;
  ELSE
     v_col_list := v_col_list || ',' || currec.column_name;
  END IF;
 END LOOP;
 RETURN v_col_list;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.put_line (
     'ORA- Error occured while getting column list for table => '
     || p_table_name);
  ROLLBACK;
END get_col_list;
/

CREATE OR REPLACE FUNCTION TODATE (P_VAR1 VARCHAR2, P_VAR2 VARCHAR2)
 RETURN DATE
AS
BEGIN
 RETURN TO_DATE (P_VAR1, P_VAR2);
END Todate;
/

CREATE OR REPLACE PROCEDURE ACQUIRE_LOCK (P_TC_OBJECT_ID     IN     VARCHAR2,
                    P_TABLE_NAME       IN     VARCHAR2,
                    P_TC_COMPANY_ID    IN     NUMBER,
                    P_LOCK_OWNER       IN     NUMBER,
                    P_REF_COUNT        IN     NUMBER,
                    P_PROCESS          IN     VARCHAR2,
                    P_COMMENTS         IN     VARCHAR2,
                    P_SOURCE_TYPE      IN     NUMBER,
                    P_SOURCE           IN     VARCHAR2,
                    P_SEQUENCE         IN     VARCHAR2,
                    P_GROUP_ID         IN     NUMBER,
                    P_TOTAL_REFCOUNT          NUMBER,
                    P_LOCK_ACQUIRED       OUT NUMBER,
                    P_REF_COUNT_OUT       OUT NUMBER)
AS
 V_IS_LOCKED   NUMBER (10);
BEGIN
 P_LOCK_ACQUIRED := 0;
 P_REF_COUNT_OUT := 0;
 IS_LOCKED (P_TC_OBJECT_ID,
          P_TABLE_NAME,
          P_TC_COMPANY_ID,
          P_LOCK_OWNER,
          V_IS_LOCKED);
 IF (V_IS_LOCKED = 0)
 THEN
  INCREMENT_REF_COUNT (P_TC_OBJECT_ID,
                       P_TABLE_NAME,
                       P_TC_COMPANY_ID,
                       P_LOCK_OWNER,
                       P_REF_COUNT,
                       P_PROCESS,
                       P_COMMENTS,
                       P_SOURCE_TYPE,
                       P_SOURCE,
                       P_SEQUENCE,
                       P_GROUP_ID,
                       P_TOTAL_REFCOUNT,
                       P_REF_COUNT_OUT);
  P_LOCK_ACQUIRED := 1;
 ELSE
  P_LOCK_ACQUIRED := 0;
 END IF;
END;
/

CREATE OR REPLACE PROCEDURE CL_CHECK_FAILOVER_STATUS (
 p_serverconfigid   IN     CL_SERVER_CONFIG.SERVER_CONFIG_ID%TYPE,
 p_clusterid        IN     CL_CLUSTER.CLUSTER_ID%TYPE,
 p_runstatus           OUT INT,
 p_status              OUT INT)
AS
 v_machineID                     NUMBER;
 -- v_clusterID number;
 v_srv_act_config_id             NUMBER;
 v_max_server_count              NUMBER;
 v_current_active_server_count   NUMBER;
 CURSOR T1Cursor (
  machineId NUMBER)
 IS
  SELECT *
    FROM CL_FAILOVER_TRACKING
   WHERE CLUSTER_ID = p_clusterid
         AND CL_FAILOVER_TRACKING.DRIVER_ID IN
                (SELECT SERVER_CONFIG_ID
                   FROM CL_SERVER_CONFIG
                  WHERE CL_SERVER_CONFIG.MACHINE_ID = machineId);
 T1Cursor_rec                    T1Cursor%ROWTYPE;
 CURSOR T2Cursor
 IS
  SELECT A.DRIVER_ID
    FROM CL_FAILOVER_TRACKING A, CL_CLUSTER B
   WHERE     A.CLUSTER_ID = B.CLUSTER_ID
         AND A.CLUSTER_ID = p_clusterid
         AND A.run_status = 3
         AND CURRENT_TIMESTAMP - B.FAILOVER_CHECK_TIMEOUT / 86400000 >
                A.LAST_CHECK_TIME
  FOR UPDATE;
 T2Cursor_rec                    T2Cursor%ROWTYPE;
 CURSOR T3Cursor (
  machineId NUMBER)
 IS
  SELECT A.DRIVER_ID, A.RUN_STATUS
    FROM CL_FAILOVER_TRACKING A, CL_CLUSTER B
   WHERE     A.CLUSTER_ID = B.CLUSTER_ID
         AND A.CLUSTER_ID = p_clusterid
         AND A.DRIVER_ID IN (SELECT SERVER_CONFIG_ID
                               FROM CL_SERVER_CONFIG
                              WHERE MACHINE_ID = machineId)
         AND CURRENT_TIMESTAMP
             - 1.5 * B.FAILOVER_CHECK_TIMEOUT / 86400000 >
                A.LAST_CHECK_TIME
  FOR UPDATE;
 T3Cursor_rec                    T3Cursor%ROWTYPE;
 CURSOR T4Cursor
 IS
  SELECT A.DRIVER_ID
    FROM CL_FAILOVER_TRACKING A, CL_CLUSTER B
   WHERE     A.CLUSTER_ID = B.CLUSTER_ID
         AND A.CLUSTER_ID = p_clusterid
         AND A.run_status = 3;
 T4Cursor_rec                    T4Cursor%ROWTYPE;
BEGIN
 p_status := 1;
 SELECT MACHINE_ID
 INTO v_machineID
 FROM CL_SERVER_CONFIG
WHERE SERVER_CONFIG_ID = p_ServerConfigId;
 OPEN T1Cursor (v_machineID);
 FETCH T1Cursor INTO t1cursor_rec;
 IF (T1Cursor%NOTFOUND)
 THEN
  SELECT maximum_active
    INTO v_max_server_count
    FROM CL_CLUSTER
   WHERE CLUSTER_ID = p_clusterid;
  SELECT COUNT (*)
    INTO v_current_active_server_count
    FROM CL_FAILOVER_TRACKING
   WHERE CLUSTER_ID = p_clusterid AND run_status = 3;
  IF (v_current_active_server_count < v_max_server_count)
  THEN
     --Max active engines available, so current node becomes active
     p_runstatus := 3;
     INSERT INTO CL_FAILOVER_TRACKING (VERSION_ID,
                                       CLUSTER_ID,
                                       driver_id,
                                       run_status,
                                       last_check_time,
                                       yield)
          VALUES (0,
                  p_clusterid,
                  p_serverconfigid,
                  3,
                  CURRENT_TIMESTAMP,
                  0);
  ELSE
     --Max active engines not available, so current node becomes passive
     p_runstatus := 2;
     INSERT INTO CL_FAILOVER_TRACKING (VERSION_ID,
                                       CLUSTER_ID,
                                       driver_id,
                                       run_status,
                                       last_check_time,
                                       yield)
          VALUES (0,
                  p_clusterid,
                  p_serverconfigid,
                  2,
                  CURRENT_TIMESTAMP,
                  0);
  END IF;
 ELSE
  IF (t1cursor_rec.driver_id = p_serverconfigid)
  THEN
     IF (t1cursor_rec.run_status = 3)
     THEN
        UPDATE CL_FAILOVER_TRACKING
           SET last_check_time = CURRENT_TIMESTAMP
         WHERE driver_id = p_serverconfigid AND CLUSTER_ID = p_clusterid;
        p_runstatus := 3;
     ELSE
        IF (t1cursor_rec.run_status = 2)
        THEN
           OPEN T4Cursor;
           FETCH T4Cursor INTO T4cursor_rec;
           IF T4Cursor%FOUND
           THEN
              OPEN T2Cursor;
              FETCH T2Cursor INTO T2cursor_rec;
              IF (T2Cursor%FOUND)
              THEN
                 --Active server has not updated the last check time, so Change Passive server to active
                 p_runstatus := 3;
                 UPDATE CL_FAILOVER_TRACKING
                    SET run_status = 2,
                        last_check_time = CURRENT_TIMESTAMP
                  WHERE driver_id = T2cursor_rec.driver_id
                        AND CLUSTER_ID = p_clusterid;
                 UPDATE CL_FAILOVER_TRACKING
                    SET run_status = 3,
                        last_check_time = CURRENT_TIMESTAMP
                  WHERE driver_id = p_serverconfigid
                        AND CLUSTER_ID = p_clusterid;
              ELSE
                 --Remain Passive
                 p_runstatus := 2;
                 UPDATE CL_FAILOVER_TRACKING
                    SET last_check_time = CURRENT_TIMESTAMP
                  WHERE driver_id = p_serverconfigid
                        AND CLUSTER_ID = p_clusterid;
                 p_status := 0;
              END IF;
           ELSE
              --No active server, so Change Passive server to active
              p_runstatus := 3;
              UPDATE CL_FAILOVER_TRACKING
                 SET run_status = 3, last_check_time = CURRENT_TIMESTAMP
               WHERE driver_id = p_serverconfigid
                     AND CLUSTER_ID = p_clusterid;
           END IF;
        ELSE
           IF (t1cursor_rec.run_status = 1)
           THEN
              --Endpoint was in stopped status
              SELECT maximum_active
                INTO v_max_server_count
                FROM CL_CLUSTER
               WHERE CLUSTER_ID = p_clusterid;
              SELECT COUNT (*)
                INTO v_current_active_server_count
                FROM CL_FAILOVER_TRACKING
               WHERE CLUSTER_ID = p_clusterid AND run_status = 3;
              IF (v_current_active_server_count < v_max_server_count)
              THEN
                 --Max active engines available, so current node becomes active
                 p_runstatus := 3;
                 UPDATE CL_FAILOVER_TRACKING
                    SET run_status = 3,
                        last_check_time = CURRENT_TIMESTAMP
                  WHERE driver_id = p_serverconfigid
                        AND CLUSTER_ID = p_clusterid;
              ELSE
                 --Max active engines not available, so current node becomes passive
                 p_runstatus := 2;
                 UPDATE CL_FAILOVER_TRACKING
                    SET run_status = 2,
                        last_check_time = CURRENT_TIMESTAMP
                  WHERE driver_id = p_serverconfigid
                        AND CLUSTER_ID = p_clusterid;
              END IF;
           END IF;
        END IF;
     END IF;
  ELSE
     OPEN T3Cursor (v_machineID);
     LOOP
        FETCH T3Cursor INTO T3Cursor_rec;
        IF T3Cursor%FOUND
        THEN
           p_runstatus := T3Cursor_rec.RUN_STATUS;
           UPDATE CL_FAILOVER_TRACKING
              SET DRIVER_ID = p_ServerConfigId,
                  last_check_time = CURRENT_TIMESTAMP
            WHERE DRIVER_ID = T3Cursor_rec.DRIVER_ID
                  AND CLUSTER_ID = p_clusterid;
        ELSE
           p_runstatus := 2;
           p_status := 0;
        END IF;
        EXIT WHEN T3Cursor%NOTFOUND;
     END LOOP;
  END IF;
 END IF;
END;
/

CREATE OR REPLACE PROCEDURE CL_GET_SEQUENCE (
 P_SEQUENCENAME   IN     CL_SEQUENCE.SEQUENCE_NAME%TYPE,
 P_SEQUENCENEXT      OUT CL_SEQUENCE.SEQUENCE_LAST%TYPE)
IS
BEGIN
 P_SEQUENCENEXT := -1;
  UPDATE CL_SEQUENCE
     SET SEQUENCE_LAST = SEQUENCE_LAST + 1
   WHERE SEQUENCE_NAME = P_SEQUENCENAME
 RETURNING SEQUENCE_LAST
    INTO P_SEQUENCENEXT;
EXCEPTION
 WHEN OTHERS
 THEN
  RAISE_APPLICATION_ERROR (-20010, SQLERRM);
END;
/

CREATE OR REPLACE PROCEDURE CL_MSG_RECEIVE_BEGIN (
 p_EndpointId        IN     CL_ENDPOINT_QUEUE.ENDPOINT_ID%TYPE,
 p_EndpointQueueId      OUT CL_ENDPOINT_QUEUE.ENDPOINT_QUEUE_ID%TYPE,
 p_MessageId            OUT CL_MESSAGE.MSG_ID%TYPE,
 p_ErrorCost            OUT CL_ENDPOINT_QUEUE.ERROR_COST%TYPE,
 p_ErrorCount           OUT CL_ENDPOINT_QUEUE.ERROR_COUNT%TYPE,
 p_Encoding             OUT CL_MESSAGE.ENCODING%TYPE,
 p_SourceId             OUT CL_MESSAGE.SOURCE_ID%TYPE,
 p_SourceURI            OUT CL_MESSAGE.SOURCE_URI%TYPE,
 p_EventId              OUT CL_MESSAGE.EVENT_ID%TYPE,
 p_RC                   OUT INT)
IS
BEGIN
 p_RC := 0;
 -- Lock the record for update
 SELECT ENDPOINT_QUEUE_ID,
      MSG_ID,
      ERROR_COUNT,
      ERROR_COST
 INTO p_EndpointQueueId,
      p_MessageId,
      p_ErrorCount,
      p_ErrorCost
 FROM CL_ENDPOINT_QUEUE
WHERE     ENDPOINT_ID = p_EndpointId
      AND STATUS = 2
      AND ENDPOINT_QUEUE_ID = (SELECT ENDPOINT_QUEUE_ID
                                 FROM (  SELECT ENDPOINT_QUEUE_ID
                                           FROM CL_ENDPOINT_QUEUE
                                          WHERE ENDPOINT_ID = p_EndpointId
                                                AND STATUS = 2
                                       ORDER BY PRTY DESC,
                                                WHEN_QUEUED ASC,
                                                ENDPOINT_QUEUE_ID ASC)
                                WHERE ROWNUM < 2)
 FOR UPDATE;
 -- Update the record to set STATUS = 10
 UPDATE CL_ENDPOINT_QUEUE
  SET STATUS = 10, WHEN_STATUS_CHANGED = SYSDATE
WHERE CL_ENDPOINT_QUEUE.ENDPOINT_QUEUE_ID = p_EndpointQueueId;
 -- Get information from Cl_Message table for the Msg_Id retrieved
 SELECT ENCODING,
      SOURCE_ID,
      SOURCE_URI,
      EVENT_ID
 INTO p_Encoding,
      p_SourceId,
      p_SourceURI,
      p_EventId
 FROM CL_MESSAGE
WHERE MSG_ID = p_MessageId;
EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
  p_EndpointQueueId := -1;
  RETURN;
 WHEN OTHERS
 THEN
  p_RC := SQLCODE;
  RAISE_APPLICATION_ERROR (-20010, SQLERRM);
END;
/

CREATE OR REPLACE PROCEDURE CL_MSG_RECEIVE_BEGIN_BY_NAME (
 P_ENDPOINT_NAME_CSV   IN     VARCHAR2,
 P_ENDPOINT_QUEUE_ID      OUT CL_ENDPOINT_QUEUE.ENDPOINT_QUEUE_ID%TYPE,
 P_MESSAGE_ID             OUT CL_MESSAGE.MSG_ID%TYPE,
 P_ERROR_COUNT            OUT CL_ENDPOINT_QUEUE.ERROR_COUNT%TYPE,
 P_ERROR_COST             OUT CL_ENDPOINT_QUEUE.ERROR_COST%TYPE,
 P_ENCODING               OUT CL_MESSAGE.ENCODING%TYPE,
 P_SOURCE_ID              OUT CL_MESSAGE.SOURCE_ID%TYPE,
 P_SOURCE_URI             OUT CL_MESSAGE.SOURCE_URI%TYPE,
 P_EVENT_ID               OUT CL_MESSAGE.EVENT_ID%TYPE,
 P_DATA                   OUT CL_MESSAGE.DATA%TYPE,
 P_RC                     OUT NUMBER)
AS
BEGIN
 P_RC := 0;
   -- get the first message
   SELECT CEQ.ENDPOINT_QUEUE_ID,
          CEQ.MSG_ID,
          CEQ.ERROR_COUNT,
          CEQ.ERROR_COST
     INTO P_ENDPOINT_QUEUE_ID,
          P_MESSAGE_ID,
          P_ERROR_COUNT,
          P_ERROR_COST
     FROM CL_ENDPOINT_QUEUE CEQ
    WHERE CEQ.ENDPOINT_QUEUE_ID =
             (SELECT MIN (
                        CEQ.ENDPOINT_QUEUE_ID)
                     KEEP (DENSE_RANK FIRST ORDER BY
                                               CEQ.PRTY DESC,
                                               CEQ.WHEN_QUEUED ASC,
                                               CEQ.ENDPOINT_QUEUE_ID ASC)
                        FIRST_ENDPOINT_QUEUE_ID
                FROM CL_ENDPOINT_QUEUE CEQ
                     JOIN CL_ENDPOINT CE
                        ON CE.ENDPOINT_ID = CEQ.ENDPOINT_ID
                     JOIN TABLE (
                             CL_PARSE_ENDPOINT_NAME_CSV (P_ENDPOINT_NAME_CSV)) T
                        ON T.COLUMN_VALUE = CE.NAME
               WHERE CEQ.STATUS = 2)
 FOR UPDATE WAIT 15;
 -- check: when is this lock released? when do we commit?
 UPDATE CL_ENDPOINT_QUEUE CEQ
  SET CEQ.STATUS = 10, CEQ.WHEN_STATUS_CHANGED = SYSDATE
WHERE CEQ.ENDPOINT_QUEUE_ID = P_ENDPOINT_QUEUE_ID;
 COMMIT;
 SELECT CM.ENCODING,
      CM.SOURCE_ID,
      CM.SOURCE_URI,
      CM.EVENT_ID,
      CM.DATA
 INTO P_ENCODING,
      P_SOURCE_ID,
      P_SOURCE_URI,
      P_EVENT_ID,
      P_DATA
 FROM CL_MESSAGE CM
WHERE CM.MSG_ID = P_MESSAGE_ID;
EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
  -- check: what of the lock obtained above in this situation? rollback?
  P_ENDPOINT_QUEUE_ID := -1;
  ROLLBACK;
  RETURN;
 -- check: dont we need a resource busy handler for the update lock?
 WHEN OTHERS
 THEN
  -- todo: we should not need this; and what of retval? cant we do away with it?
  P_RC := SQLCODE;
  ROLLBACK;
  RAISE;
END;
/

CREATE OR REPLACE PROCEDURE CL_MSG_RECEIVE_FINISH (
 P_ENDPOINTQID   IN     CL_ENDPOINT_QUEUE.ENDPOINT_QUEUE_ID%TYPE,
 P_STATCODE      IN     CL_ENDPOINT_QUEUE.STATUS%TYPE,
 P_ERRORCOST     IN     CL_ENDPOINT_QUEUE.ERROR_COST%TYPE,
 P_DISPOSITION   IN     CL_ENDPOINT_QUEUE.DISPOSITION%TYPE,
 P_TARGETID      IN     CL_ENDPOINT_QUEUE.TARGET_ID%TYPE,
 P_TARGETURI     IN     CL_ENDPOINT_QUEUE.TARGET_URI%TYPE,
 P_RC               OUT INT)
IS
 V_COUNT       INT;
 V_ERRORFLAG   INT;
BEGIN
 P_RC := 0;
 IF (P_STATCODE = 5)
 THEN
  V_ERRORFLAG := 0;
 ELSE
  V_ERRORFLAG := 1;
 END IF;
   SELECT 1
     INTO V_COUNT
     FROM CL_ENDPOINT_QUEUE
    WHERE ENDPOINT_QUEUE_ID = P_ENDPOINTQID
 FOR UPDATE WAIT 3;
 UPDATE CL_ENDPOINT_QUEUE
  SET STATUS = P_STATCODE,
      ERROR_COST = P_ERRORCOST,
      DISPOSITION = P_DISPOSITION,
      TARGET_ID = P_TARGETID,
      TARGET_URI = P_TARGETURI,
      ERROR_COUNT = ERROR_COUNT + V_ERRORFLAG,
      WHEN_STATUS_CHANGED = SYSDATE
WHERE ENDPOINT_QUEUE_ID = P_ENDPOINTQID;
EXCEPTION
 WHEN OTHERS
 THEN
  P_RC := SQLCODE;
  RAISE_APPLICATION_ERROR (-20010, SQLERRM);
END;
/

CREATE OR REPLACE PROCEDURE CL_MSG_SEND_ROUTE (P_EVENTID     IN     CL_MESSAGE.EVENT_ID%TYPE,
                         P_PRIORITY    IN     CL_MESSAGE.PRTY%TYPE,
                         P_MESSAGEID   IN     CL_MESSAGE.MSG_ID%TYPE,
                         P_RC             OUT INT)
IS
 V_ENABLED       INT;
 V_SUBSCRIBERS   INT;
BEGIN
 SELECT COUNT (1)
 INTO V_ENABLED
 FROM CL_EVENT_MASTER
WHERE EVENT_ID = P_EVENTID AND ENABLED != 0;
 IF (V_ENABLED < 1)
 THEN
  P_RC := -1;
 ELSE
  SELECT COUNT (1)
    INTO V_SUBSCRIBERS
    FROM CL_EVENT_SUBSCRIPTION
   WHERE EVENT_ID = P_EVENTID;
  IF (V_SUBSCRIBERS < 1)
  THEN
     P_RC := -2;
  ELSE
     INSERT INTO CL_ENDPOINT_QUEUE (ENDPOINT_ID,
                                    MSG_ID,
                                    STATUS,
                                    PRTY,
                                    ERROR_COUNT,
                                    ERROR_COST,
                                    DISPOSITION)
        SELECT ENDPOINT_ID,
               P_MESSAGEID,
               2,
               Cl_Get_Priority (ENDPOINT_ID, P_EVENTID, P_PRIORITY),
               0,
               0,
               0
          FROM CL_EVENT_SUBSCRIPTION
         WHERE EVENT_ID = P_EVENTID;
     P_RC := 0;
  END IF;
 END IF;
EXCEPTION
 WHEN OTHERS
 THEN
  P_RC := SQLCODE;
  RAISE_APPLICATION_ERROR (-20010, SQLERRM);
END;
/

CREATE OR REPLACE PROCEDURE CONCURRENT_USER_SAMPLING (pi_from_date TIMESTAMP,pi_interval NUMBER)
IS
lv_int_count NUMBER;
lv_login_count NUMBER;
lv_logout_count NUMBER;
lv_usr_count NUMBER;
lv_sample_number NUMBER := pi_interval ;
lv_time_interval TIMESTAMP := pi_from_date;
BEGIN
--filtering into  USER_AUDIT_DETAIL
INSERT INTO USER_AUDIT_DETAIL_GTK (USER_NAME,SESSION_ID,HOST_IP_ADDRESS,DEVICE_TYPE,LOGIN_TIME,LOGOUT_TIME)
SELECT   USER_NAME,SESSION_ID,MAX(HOST_IP_ADDRESS)HOST_IP_ADDRESS,MAX(DEVICE_TYPE)DEVICE_TYPE,MAX(LOGIN_TIME)LOGIN_TIME,MAX(LOGOUT_TIME)LOGOUT_TIME FROM USER_AUDIT_EVENT_GTK GROUP BY USER_NAME,SESSION_ID;
UPDATE user_audit_detail_GTK SET logout_time=SYSDATE WHERE logout_time IS NULL;
UPDATE user_audit_detail_GTK SET login_time=pi_from_date WHERE login_time IS NULL;
--Sampling
SELECT COUNT(*) INTO lv_int_count
FROM USER_AUDIT_DETAIL_GTK WHERE login_time <= pi_from_date AND  LOGOUT_TIME > PI_FROM_DATE;
INSERT INTO HOUR_SAMPLING_GTK(HOUR_SAMPLING_ID,SAMPLING_DATE,SAMPLING_INTERVAL,USER_COUNT)VALUES(seq_hour_sampling_idx.NEXTVAL,LV_TIME_INTERVAL,0,lv_int_count);
WHILE(LV_TIME_INTERVAL <= SYSTIMESTAMP)
LOOP
SELECT COUNT(*) INTO lv_login_count
FROM user_audit_detail_GTK WHERE login_time > lv_time_interval AND login_time<= lv_time_interval + pi_interval/1440;
SELECT COUNT(*) INTO lv_logout_count
FROM user_audit_detail_GTK WHERE logout_time > lv_time_interval AND logout_time <= lv_time_interval + pi_interval/1440;
lv_usr_count := lv_int_count + lv_login_count - lv_logout_count;
lv_int_count := lv_usr_count;
IF ((lv_time_interval+lv_sample_number/1440) > systimestamp )
THEN
lv_sample_number := MOD(TO_NUMBER(SUBSTR(TO_CHAR(SYSTIMESTAMP, 'yyyy-mm-dd hh24:mi:ss'),15,2)),pi_interval);
END IF;
lv_time_interval := lv_time_interval + pi_interval/1440;
INSERT INTO HOUR_SAMPLING_GTK(HOUR_SAMPLING_ID,SAMPLING_DATE,SAMPLING_INTERVAL,USER_COUNT)VALUES(SEQ_HOUR_SAMPLING_IDX.NEXTVAL,LV_TIME_INTERVAL,lv_sample_number,LV_USR_COUNT);
END LOOP;
END;
/

CREATE OR REPLACE PROCEDURE DD_PURGE_SUBSCRIBER_SYNC_LOG (V_NO_OF_DAYS   IN NUMBER,
                                    V_STATUS       IN VARCHAR2)
AS
 TYPE CURSOR_TYPE IS REF CURSOR;
 st_cursor       CURSOR_TYPE;
 v_row_count     PLS_INTEGER := 0;
 V_SQL           VARCHAR2 (2000);
 TYPE TRAN_ID_T IS TABLE OF NUMBER
                    INDEX BY BINARY_INTEGER;
 V_TRAN_ID_ARR   TRAN_ID_T;
 l_start         NUMBER;
 l_end           NUMBER;
BEGIN
 l_start := DBMS_UTILITY.get_time;
 DBMS_OUTPUT.PUT_LINE ('Purge Data Sync start time:' || SYSTIMESTAMP);
 V_SQL :=
  'SELECT TRANSACTION_ID
                     FROM  SUBSCRIBER_SYNC_LOG  WHERE LAST_UPDATED_DTTM < (SYSDATE - '
  || V_NO_OF_DAYS
  || ')';
 IF V_STATUS IS NOT NULL
 THEN
  V_SQL := V_SQL || ' AND STATUS IN (''' || V_STATUS || ''')';
 END IF;
 OPEN st_cursor FOR V_SQL;
 LOOP
  FETCH st_cursor
  BULK COLLECT INTO V_TRAN_ID_ARR
  LIMIT 20000;
  EXIT WHEN v_row_count = st_cursor%ROWCOUNT;
  v_row_count := st_cursor%ROWCOUNT;
  FORALL i IN 1 .. V_TRAN_ID_ARR.COUNT
     INSERT INTO DD_SUBS_TRAN_GTT
          VALUES (V_TRAN_ID_ARR (i));
  COMMIT;
 END LOOP;
 DELETE FROM SUBSCRIBER_SYNC_LOG
     WHERE (TRANSACTION_ID) IN (SELECT TRANID FROM DD_SUBS_TRAN_GTT);
 COMMIT;
 l_end := DBMS_UTILITY.get_time;
 DBMS_OUTPUT.PUT_LINE ('Purge Data Sync end time:' || SYSTIMESTAMP);
 DBMS_OUTPUT.put_line (
  'Elapsed Time: ' || ROUND ( (l_end - l_start) / 100, 2) || ' secs');
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (SQLERRM);
END;
/

CREATE OR REPLACE PROCEDURE DECREMENT_REF_COUNT (P_TC_OBJECT_ID     IN     VARCHAR2,
                           P_TABLE_NAME       IN     VARCHAR2,
                           P_TC_COMPANY_ID    IN     DECIMAL,
                           P_REF_COUNT        IN     NUMBER,
                           P_PROCESS          IN     VARCHAR2,
                           P_LOCK_OWNER       IN     NUMBER,
                           P_COMMENTS         IN     VARCHAR2,
                           P_SOURCE_TYPE      IN     NUMBER,
                           P_SOURCE           IN     VARCHAR2,
                           P_GROUP_ID         IN     NUMBER,
                           P_IS_DECREMENTED      OUT NUMBER,
                           P_IS_DELETED          OUT NUMBER)
AS
 V_SELECT_SQL         VARCHAR2 (1000);
 V_UPDATE_SQL         VARCHAR2 (1000);
 V_DELETE_SQL         VARCHAR2 (1000);
 V_DEP_GROUP_ID_REC   NUMBER (10);
 V_LOCK_GROUP_ID      NUMBER (10);
BEGIN
 P_IS_DECREMENTED := 0;
 P_IS_DELETED := 0;
 V_SELECT_SQL :=
     'SELECT LAST_GROUP_ID FROM  '
  || P_TABLE_NAME
  || ' WHERE TC_OBJECT_ID = :VAR1
                  AND TC_COMPANY_ID = :VAR2 ';
 V_UPDATE_SQL :=
     'UPDATE '
  || P_TABLE_NAME
  || ' SET REF_COUNT = REF_COUNT - :VAR1 ,
                    PROCESS = :VAR2 ,
                    COMMENTS =  :VAR3,
                    LAST_UPDATED_SOURCE_TYPE =  :VAR4,
                    LAST_UPDATED_SOURCE = :VAR5,
                    LAST_UPDATED_DTTM = SYSDATE
                    WHERE TC_OBJECT_ID =  :VAR6
                    AND TC_COMPANY_ID = :VAR7
                    AND LOCK_OWNER <> :VAR8
                    AND CREATED_GROUP_ID <= :VAR9
                    AND LAST_GROUP_ID >= :VAR10 ';
 EXECUTE IMMEDIATE V_UPDATE_SQL
  USING P_REF_COUNT,
        P_PROCESS,
        P_COMMENTS,
        P_SOURCE_TYPE,
        P_SOURCE,
        P_TC_OBJECT_ID,
        P_TC_COMPANY_ID,
        P_LOCK_OWNER,
        P_GROUP_ID,
        P_GROUP_ID;
 IF (SQL%ROWCOUNT <> 0)
 THEN
  P_IS_DECREMENTED := 1;
  EXECUTE IMMEDIATE V_SELECT_SQL
     INTO V_LOCK_GROUP_ID
     USING P_TC_OBJECT_ID, P_TC_COMPANY_ID;
  SELECT CASE
            WHEN EXISTS
                    (SELECT 1
                       FROM DO_LOCK DLK
                      WHERE     V_LOCK_GROUP_ID >= DLK.CREATED_GROUP_ID
                            AND V_LOCK_GROUP_ID <= DLK.LAST_GROUP_ID
                            AND REF_COUNT > 0
                            AND ROWNUM <= 1
                     UNION
                     SELECT 1
                       FROM SHIPMENT_LOCK SHLK
                      WHERE     V_LOCK_GROUP_ID >= SHLK.CREATED_GROUP_ID
                            AND V_LOCK_GROUP_ID <= SHLK.LAST_GROUP_ID
                            AND REF_COUNT > 0
                            AND ROWNUM <= 1)
            THEN
               1
            ELSE
               0
         END
    INTO V_DEP_GROUP_ID_REC
    FROM DUAL;
  IF (V_DEP_GROUP_ID_REC = 0)
  THEN
     DELETE FROM DO_LOCK DLK
           WHERE     V_LOCK_GROUP_ID >= DLK.CREATED_GROUP_ID
                 AND V_LOCK_GROUP_ID <= DLK.LAST_GROUP_ID
                 AND DLK.REF_COUNT <= 0;
     IF (SQL%ROWCOUNT <> 0)
     THEN
        P_IS_DELETED := 1;
     END IF;
     DELETE FROM SHIPMENT_LOCK SHLK
           WHERE     V_LOCK_GROUP_ID >= SHLK.CREATED_GROUP_ID
                 AND V_LOCK_GROUP_ID <= SHLK.LAST_GROUP_ID
                 AND SHLK.REF_COUNT <= 0;
     IF (SQL%ROWCOUNT <> 0)
     THEN
        P_IS_DELETED := 1;
     END IF;
  END IF;
 END IF;
END;
/

CREATE OR REPLACE PROCEDURE DELETE_LOCK (P_TC_OBJECT_ID           IN     VARCHAR2,
                   P_TC_COMPANY_ID          IN     NUMBER,
                   P_TABLE_NAME             IN     VARCHAR2,
                   P_DEPENDENT_TABLE_NAME   IN     VARCHAR2,
                   P_IS_DELETED                OUT NUMBER)
AS
 V_SELECT_SQL             VARCHAR2 (1000);
 V_DELETE_SQL             VARCHAR2 (1000);
 V_DELETE_DEPENDENT_SQL   VARCHAR2 (1000);
 V_NO_OF_ROWS             NUMBER (10);
 V_LOCK_GROUP_ID          NUMBER (10);
BEGIN
 V_SELECT_SQL :=
     'SELECT LAST_GROUP_ID FROM  '
  || P_TABLE_NAME
  || ' WHERE TC_OBJECT_ID = :VAR1
                    AND TC_COMPANY_ID = :VAR2 ';
 V_DELETE_SQL :=
  'DELETE FROM ' || P_TABLE_NAME || ' WHERE LAST_GROUP_ID = :var1';
 V_DELETE_DEPENDENT_SQL :=
     'DELETE FROM '
  || P_DEPENDENT_TABLE_NAME
  || ' WHERE LAST_GROUP_ID = :VAR1';
 BEGIN
  EXECUTE IMMEDIATE V_SELECT_SQL
     INTO V_LOCK_GROUP_ID
     USING P_TC_OBJECT_ID, P_TC_COMPANY_ID;
  IF (SQL%ROWCOUNT > 0)
  THEN
     EXECUTE IMMEDIATE V_DELETE_SQL USING V_LOCK_GROUP_ID;
     V_NO_OF_ROWS := SQL%ROWCOUNT;
     EXECUTE IMMEDIATE V_DELETE_DEPENDENT_SQL USING V_LOCK_GROUP_ID;
     V_NO_OF_ROWS := SQL%ROWCOUNT;
  END IF;
  IF (V_NO_OF_ROWS = 0)
  THEN
     P_IS_DELETED := 0;
  ELSE
     P_IS_DELETED := 1;
  END IF;
 EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
     P_IS_DELETED := 1;
 END;
END;
/

CREATE OR REPLACE PROCEDURE INCREMENT_REF_COUNT (P_TC_OBJECT_ID     IN     VARCHAR2,
                           P_TABLE_NAME       IN     VARCHAR2,
                           P_TC_COMPANY_ID    IN     NUMBER,
                           P_LOCK_OWNER       IN     NUMBER,
                           P_REF_COUNT        IN     NUMBER,
                           P_PROCESS          IN     VARCHAR2,
                           P_COMMENTS         IN     VARCHAR2,
                           P_SOURCE_TYPE      IN     NUMBER,
                           P_SOURCE           IN     VARCHAR2,
                           P_SEQUENCE         IN     VARCHAR2,
                           P_GROUP_ID         IN     NUMBER,
                           P_TOTAL_REFCOUNT   IN     NUMBER,
                           P_REF_COUNT_OUT       OUT NUMBER)
AS
 V_UPDATE_SQL   VARCHAR2 (1000);
 V_INSERT_SQL   VARCHAR2 (1000);
 V_NO_OF_ROWS   NUMBER (10);
 V_SELECT_SQL   VARCHAR2 (1000);
BEGIN
 V_SELECT_SQL :=
     'SELECT REF_COUNT FROM '
  || P_TABLE_NAME
  || ' WHERE TC_OBJECT_ID = :VAR1
                     AND TC_COMPANY_ID = :VAR2
                     AND LOCK_OWNER = :VAR3 ';
 V_UPDATE_SQL :=
     'UPDATE '
  || P_TABLE_NAME
  || ' SET REF_COUNT = REF_COUNT + :VAR1 ,
                    PROCESS = :VAR2 ,
                    COMMENTS =  :VAR3,
                    LAST_UPDATED_SOURCE_TYPE =  :VAR4,
                    LAST_UPDATED_SOURCE = :VAR5,
                    LAST_UPDATED_DTTM = SYSDATE,
                    LAST_GROUP_ID = :VAR6
                    WHERE TC_OBJECT_ID =  :VAR7
                    AND TC_COMPANY_ID = :VAR8
                    AND LOCK_OWNER = :VAR9 ';
 V_INSERT_SQL :=
  'INSERT INTO ' || P_TABLE_NAME || '  VALUES(' || P_SEQUENCE
  || '.NEXTVAL ,:VAR1,:VAR2,
                    :VAR3,:VAR4,:VAR5,:VAR6,:VAR7,:VAR8,SYSDATE,:VAR9,:VAR10,SYSDATE,
                    :VAR11,:VAR12)';
 EXECUTE IMMEDIATE V_UPDATE_SQL
  USING P_REF_COUNT,
        P_PROCESS,
        P_COMMENTS,
        P_SOURCE_TYPE,
        P_SOURCE,
        P_GROUP_ID,
        P_TC_OBJECT_ID,
        P_TC_COMPANY_ID,
        P_LOCK_OWNER;
 IF (SQL%ROWCOUNT = 0)
 THEN
  EXECUTE IMMEDIATE V_INSERT_SQL
     USING P_TC_OBJECT_ID,
           P_TC_COMPANY_ID,
           P_LOCK_OWNER,
           P_REF_COUNT,
           P_PROCESS,
           P_COMMENTS,
           P_SOURCE_TYPE,
           P_SOURCE,
           P_SOURCE_TYPE,
           P_SOURCE,
           P_GROUP_ID,
           P_GROUP_ID;
 END IF;
 IF (P_TOTAL_REFCOUNT = 1)
 THEN
  EXECUTE IMMEDIATE V_SELECT_SQL
     INTO P_REF_COUNT_OUT
     USING P_TC_OBJECT_ID, P_TC_COMPANY_ID, P_LOCK_OWNER;
 ELSE
  P_REF_COUNT_OUT := P_REF_COUNT;
 END IF;
END;
/

CREATE OR REPLACE PROCEDURE INS_CARRIER_PARAMETER_EVENT (
 vSetId             IN CARRIER_PARAMETER_EVENT.ACCESSORIAL_PARAM_SET_ID%TYPE,
 vRateId            IN CARRIER_PARAMETER_EVENT.CM_RATE_DISCOUNT_ID%TYPE,
 vStopChargeId      IN CARRIER_PARAMETER_EVENT.STOP_OFF_CHARGE_ID%TYPE,
 vAccRateId         IN CARRIER_PARAMETER_EVENT.ACCESSORIAL_RATE_ID%TYPE,
 vFieldName         IN CARRIER_PARAMETER_EVENT.FIELD_NAME%TYPE,
 vOldValue          IN CARRIER_PARAMETER_EVENT.OLD_VALUE%TYPE,
 vNewValue          IN CARRIER_PARAMETER_EVENT.NEW_VALUE%TYPE,
 vLastupdateduser   IN CARRIER_PARAMETER_EVENT.LAST_UPDATED_SRC%TYPE)
AS
 PRAGMA AUTONOMOUS_TRANSACTION;
 vEventSeq   NUMBER (4, 0);
BEGIN
 SELECT NVL (MAX (EVENT_SEQ), 0) + 1
 INTO vEventSeq
 FROM CARRIER_PARAMETER_EVENT
WHERE ACCESSORIAL_PARAM_SET_ID = vSetId;
 INSERT INTO CARRIER_PARAMETER_EVENT (ACCESSORIAL_PARAM_SET_ID,
                                    EVENT_SEQ,
                                    CM_RATE_DISCOUNT_ID,
                                    STOP_OFF_CHARGE_ID,
                                    ACCESSORIAL_RATE_ID,
                                    FIELD_NAME,
                                    OLD_VALUE,
                                    NEW_VALUE,
                                    LAST_UPDATED_SRC,
                                    LAST_UPDATED_DTTM)
    VALUES (vSetId,
            vEventSeq,
            vRateId,
            vStopChargeId,
            vAccRateId,
            vFieldName,
            vOldValue,
            vNewValue,
            vLastupdateduser,
            SYSDATE);
 COMMIT;
END;
/

CREATE OR REPLACE PROCEDURE IS_DATASYNC_ENABLED (P_RC OUT NUMBER)
AS
 TAB_COUNT   NUMBER;
BEGIN
 P_RC := 0;
 SELECT COUNT (table_name)
 INTO TAB_COUNT
 FROM user_tables
WHERE table_name = 'DD_REPLICATED_TABLES';
 IF (TAB_COUNT = 0)
 THEN
  P_RC := -1;
 END IF;
END;
/

CREATE OR REPLACE PROCEDURE IS_LOCKED (P_TC_OBJECT_ID    IN     VARCHAR2,
                 P_TABLE_NAME      IN     VARCHAR2,
                 P_TC_COMPANY_ID   IN     INT,
                 P_LOCK_OWNER      IN     INT,
                 P_IS_LOCKED          OUT INT)
AS
 V_LOCK_COUNT   NUMBER (10);
 V_SELECT_SQL   VARCHAR2 (1000);
BEGIN
 V_SELECT_SQL :=
     'SELECT COUNT(1) FROM '
  || P_TABLE_NAME
  || ' WHERE TC_OBJECT_ID = :VAR1
                  AND TC_COMPANY_ID = :VAR2
                  AND LOCK_OWNER <> :VAR3';
 EXECUTE IMMEDIATE V_SELECT_SQL
  INTO V_LOCK_COUNT
  USING P_TC_OBJECT_ID, P_TC_COMPANY_ID, P_LOCK_OWNER;
 IF (V_LOCK_COUNT > 0)
 THEN
  P_IS_LOCKED := 1;
 ELSE
  P_IS_LOCKED := 0;
 END IF;
END;
/

CREATE OR REPLACE PROCEDURE MANH_CREATE_SEQUENCE (VONAME VARCHAR2)
IS
 V_OBJNAME   USER_OBJECTS.OBJECT_NAME%TYPE;
 V_SQL       VARCHAR2 (1000);
BEGIN
 BEGIN
  SELECT SEQUENCE_NAME
    INTO V_OBJNAME
    FROM USER_SEQUENCES
   WHERE SEQUENCE_NAME = UPPER (VONAME);
 EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
     V_OBJNAME := NULL;
 END;
 IF (V_OBJNAME IS NULL)
 THEN
  V_OBJNAME := VONAME;
  V_SQL := 'CREATE SEQUENCE ' || V_OBJNAME || ' MINVALUE 1 NOMAXVALUE';
  DBMS_OUTPUT.PUT_LINE (V_SQL);
  EXECUTE IMMEDIATE V_SQL;
  DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
 ELSE
  DBMS_OUTPUT.PUT_LINE ('[WARNING] - Sequence already exists');
 END IF;
END MANH_CREATE_SEQUENCE;
/

CREATE OR REPLACE PROCEDURE MANH_CREATE_SYNONYM (VONAME VARCHAR2)
IS
 V_SQL   VARCHAR2 (1000);
 V_MSF   USER_USERS.USERNAME%TYPE;
 V_CNT   NUMBER;
BEGIN
 BEGIN
SELECT DISTINCT GRANTOR INTO V_MSF
  FROM USER_TAB_PRIVS UTP
 WHERE UTP.TABLE_NAME = VONAME
       AND EXISTS
              (SELECT 1
                 FROM USER_SYNONYMS US
                WHERE US.TABLE_NAME = 'COMPANY'
                      AND US.TABLE_OWNER = UTP.GRANTOR);
EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
     V_MSF := NULL;
 END;
    SELECT SUM (CNT) INTO V_CNT
      FROM ( (SELECT COUNT (1) CNT
                FROM USER_SYNONYMS US
               WHERE US.SYNONYM_NAME = VONAME)
            UNION ALL
            (SELECT COUNT (1)
               FROM USER_TABLES UT
              WHERE UT.TABLE_NAME = VONAME));
 IF (V_MSF IS NOT NULL )
 THEN
  V_SQL :=
        'CREATE OR REPLACE SYNONYM '
     || VONAME
     || ' FOR '
     || V_MSF
     || '.'
     || VONAME;
  DBMS_OUTPUT.PUT_LINE (V_SQL);
  EXECUTE IMMEDIATE (V_SQL);
  DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
 ELSIF (V_MSF IS NULL)
 THEN
  DBMS_OUTPUT.PUT_LINE (
     'ORA-xxxxx: Required privileges were not granted from MSF');
 END IF;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (SQLERRM);
END MANH_CREATE_SYNONYM;
/

CREATE OR REPLACE PROCEDURE MANH_DROP_COLUMN (V_TABNAME VARCHAR2,V_COLNAME VARCHAR2)
IS
 V_CNT   NUMBER;
 V_SQL   VARCHAR2 (1000);
BEGIN
 SELECT COUNT(*) INTO V_CNT
						from USER_TAB_COLUMNS
						where TABLE_NAME=upper(V_TABNAME)
						and COLUMN_NAME=upper(V_COLNAME) ;
 IF (V_CNT > 0)
 THEN
  V_SQL := 'ALTER TABLE '|| V_TABNAME ||' DROP COLUMN '|| V_COLNAME ;
	  DBMS_OUTPUT.PUT_LINE (V_SQL);
  EXECUTE IMMEDIATE V_SQL;
  DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
 ELSE
  DBMS_OUTPUT.PUT_LINE ('[WARNING] - Table/Column does not exist in the Schema');
 END IF;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END MANH_DROP_COLUMN;
/

CREATE OR REPLACE PROCEDURE MANH_DROP_REF (V_TABNAME VARCHAR2)
IS
BEGIN
 FOR i
  IN (SELECT CONSTRAINT_NAME
        FROM USER_CONSTRAINTS
       WHERE     CONSTRAINT_TYPE = 'R'
             AND OWNER = USER
             AND TABLE_NAME = V_TABNAME)
 LOOP
  EXECUTE IMMEDIATE
        'ALTER TABLE '
     || V_TABNAME
     || ' DROP CONSTRAINT '
     || I.CONSTRAINT_NAME;
  DBMS_OUTPUT.PUT_LINE ('Dropped Foreign Key - ' || I.CONSTRAINT_NAME);
 END LOOP;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (SQLERRM);
END;
/

-- CREATE OR REPLACE PROCEDURE MANH_GRANT_BASE_PRIV_PROD (PRDSCHEMA VARCHAR2)
-- IS
--  V_OBJNAME   MANH_BASE_GRANT_LIST.OBJECT_NAME%TYPE;
--  V_OBJTYPE   MANH_BASE_GRANT_LIST.OBJECT_TYPE%TYPE;
--  V_OBJECT_PERMISSION   MANH_BASE_GRANT_LIST.PERMISSION_NAME%TYPE;
--  V_SQL       VARCHAR2 (1000);
-- BEGIN
--   FOR CUR_PSCHEMA IN ( SELECT d.OBJECT_NAME, d.PERMISSION_NAME
--                                   FROM MANH_BASE_GRANT_LIST d, USER_OBJECTS o
--                                   WHERE UPPER(d.OBJECT_NAME) = UPPER (o.object_name)
--                                   AND UPPER(d.OBJECT_TYPE) = UPPER (o.object_type))
--        LOOP
--           V_SQL :=
--                 'GRANT ' || CUR_PSCHEMA.PERMISSION_NAME || ' ON '  || CUR_PSCHEMA.OBJECT_NAME || ' TO ' || PRDSCHEMA;
--           DBMS_OUTPUT.PUT_LINE (V_SQL);
--           EXECUTE IMMEDIATE V_SQL;
--           DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
--        END LOOP;
-- EXCEPTION
--  WHEN OTHERS
--  THEN
--     DBMS_OUTPUT.PUT_LINE (SQLERRM);
-- END MANH_GRANT_BASE_PRIV_PROD;
-- /
--

CREATE OR REPLACE PROCEDURE MANH_GRANT_PRIV_PROD (VONAME VARCHAR2)
IS
 V_OBJNAME   USER_OBJECTS.OBJECT_NAME%TYPE;
 V_OBJTYPE   USER_OBJECTS.OBJECT_TYPE%TYPE;
 V_SQL       VARCHAR2 (1000);
BEGIN
 BEGIN
  SELECT OBJECT_NAME, OBJECT_TYPE
    INTO V_OBJNAME, V_OBJTYPE
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = UPPER (VONAME)
         AND OBJECT_TYPE IN
                ('PROCEDURE',
                 'FUNCTION',
                 'TABLE',
                 'VIEW',
                 'SYNONYM',
                 'PACKAGE',
                 'SEQUENCE');
 EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
     V_OBJNAME := NULL;
     V_OBJTYPE := NULL;
 END;
 IF (V_OBJNAME IS NOT NULL OR V_OBJTYPE IS NOT NULL)
 THEN
  IF (V_OBJTYPE = 'TABLE' OR V_OBJTYPE = 'VIEW' OR V_OBJTYPE = 'SYNONYM')
  THEN
     FOR CUR_PSCHEMA IN (SELECT GRANTEE
                           FROM USER_TAB_PRIVS
                          WHERE TABLE_NAME = 'COMPANY'
                         UNION
                         SELECT OWNER
                           FROM ALL_SYNONYMS
                          WHERE TABLE_OWNER = USER AND OWNER <> USER)
     LOOP
        V_SQL :=
              'GRANT SELECT, INSERT, UPDATE, DELETE,REFERENCES ON '
           || V_OBJNAME
           || ' TO '
           || CUR_PSCHEMA.GRANTEE;
        DBMS_OUTPUT.PUT_LINE (V_SQL);
        EXECUTE IMMEDIATE V_SQL;
        DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
     END LOOP;
  ELSIF (   V_OBJTYPE = 'PROCEDURE'
         OR V_OBJTYPE = 'FUNCTION'
         OR V_OBJTYPE = 'PACKAGE')
  THEN
     FOR CUR_PSCHEMA IN (SELECT GRANTEE
                           FROM USER_TAB_PRIVS
                          WHERE TABLE_NAME = 'COMPANY'
                         UNION
                         SELECT OWNER
                           FROM ALL_SYNONYMS
                          WHERE TABLE_OWNER = USER AND OWNER <> USER)
     LOOP
        V_SQL :=
              'GRANT EXECUTE, DEBUG ON '
           || V_OBJNAME
           || ' TO '
           || CUR_PSCHEMA.GRANTEE;
        DBMS_OUTPUT.PUT_LINE (V_SQL);
        EXECUTE IMMEDIATE V_SQL;
        DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
     END LOOP;
  ELSIF (V_OBJTYPE = 'SEQUENCE')
  THEN
     FOR CUR_PSCHEMA IN (SELECT GRANTEE
                           FROM USER_TAB_PRIVS
                          WHERE TABLE_NAME = 'COMPANY'
                         UNION
                         SELECT OWNER
                           FROM ALL_SYNONYMS
                          WHERE TABLE_OWNER = USER AND OWNER <> USER)
     LOOP
        V_SQL :=
              'GRANT SELECT, ALTER ON '
           || V_OBJNAME
           || ' TO '
           || CUR_PSCHEMA.GRANTEE;
        DBMS_OUTPUT.PUT_LINE (V_SQL);
        EXECUTE IMMEDIATE V_SQL;
        DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
     END LOOP;
  END IF;
 ELSE
  DBMS_OUTPUT.PUT_LINE (
        '[WARNING] - Object '
     || V_OBJNAME
     || ' either does not exists or type not supported');
 END IF;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (SQLERRM);
END MANH_GRANT_PRIV_PROD;
/

CREATE OR REPLACE PROCEDURE MANH_MOVE_TABLE_INDEX (VTNAME     VARCHAR2,
                             VDTTBS     VARCHAR2,
                             VIDXTBS    VARCHAR2 DEFAULT NULL)
IS
 V_TABNAME   USER_TABLES.TABLE_NAME%TYPE;
 V_TBS_CNT   PLS_INTEGER;
 V_TBL_CNT   PLS_INTEGER;
 V_TB_SQL    VARCHAR2 (1000);
 V_IDX_SQL   VARCHAR2 (1000);
 V_DTBS      USER_TABLESPACES.TABLESPACE_NAME%TYPE;
 V_ITBS      USER_TABLESPACES.TABLESPACE_NAME%TYPE;
BEGIN
 BEGIN
    SELECT TABLESPACE_NAME, COUNT (*)
      INTO V_DTBS, V_TBL_CNT
      FROM USER_TABLES
     WHERE TABLE_NAME = UPPER (VTNAME)
  GROUP BY TABLESPACE_NAME;
  IF (V_DTBS IS NULL)
  THEN
     -- IOT
     SELECT TABLESPACE_NAME
       INTO V_DTBS
       FROM USER_INDEXES
      WHERE TABLE_NAME = UPPER (VTNAME) AND INDEX_TYPE = 'IOT - TOP';
  END IF;
 EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
     V_TBL_CNT := 0;
 END;
 IF (VIDXTBS IS NULL)
 THEN
  V_ITBS := UPPER (V_DTBS);
 ELSE
  V_ITBS := UPPER (VIDXTBS);
 END IF;
 SELECT COUNT (*)
 INTO V_TBS_CNT
 FROM USER_TABLESPACES
WHERE TABLESPACE_NAME IN (V_DTBS, V_ITBS);
 IF ( (V_TBS_CNT = 1 AND V_TBL_CNT = 1 AND V_DTBS = V_ITBS)
   OR (V_TBS_CNT = 2 AND V_TBL_CNT = 1 AND V_DTBS <> V_ITBS))
 THEN
  IF (UPPER (V_DTBS) <> UPPER (VDTTBS))
  THEN
     V_DTBS := VDTTBS;
     V_TB_SQL := 'ALTER TABLE ' || VTNAME || ' MOVE TABLESPACE ' || V_DTBS;
     DBMS_OUTPUT.PUT_LINE (V_TB_SQL);
     EXECUTE IMMEDIATE V_TB_SQL;
     DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully' || CHR (13));
  ELSE
     DBMS_OUTPUT.PUT_LINE (
           '[INFO] - Table -'
        || VTNAME
        || ' already in '
        || V_DTBS
        || CHR (13));
  END IF;
  FOR CUR_IDX
     IN (SELECT INDEX_NAME
           FROM USER_INDEXES
          WHERE TABLE_NAME = VTNAME
                AND INDEX_TYPE IN
                       ('NORMAL',
                        'NORMAL/REV',
                        'BITMAP',
                        'FUNCTION-BASED NORMAL',
                        'FUNCTION-BASED NORMAL/REV',
                        'FUNCTION-BASED BITMAP')
                AND (TABLESPACE_NAME <> V_ITBS OR STATUS <> 'VALID'))
  LOOP
     V_IDX_SQL :=
           'ALTER INDEX '
        || CUR_IDX.INDEX_NAME
        || ' REBUILD TABLESPACE '
        || V_ITBS;
     DBMS_OUTPUT.PUT_LINE (V_IDX_SQL);
     EXECUTE IMMEDIATE V_IDX_SQL;
     DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully' || CHR (13));
  END LOOP;
 ELSE
  DBMS_OUTPUT.PUT_LINE (
     '[ERROR] - Incorrect table / tablepace name provided' || CHR (13));
 END IF;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (SQLERRM);
END MANH_MOVE_TABLE_INDEX;
/

CREATE OR REPLACE PROCEDURE MANH_REBUILD_INDEX (INDNAME VARCHAR2, TBSPNAME VARCHAR2)
IS
 V_SQL       VARCHAR2 (1000);
 V_IDX_CNT   NUMBER;
BEGIN
 SELECT COUNT (*)
 INTO V_IDX_CNT
 FROM USER_INDEXES
WHERE INDEX_NAME = UPPER (INDNAME);
 IF (V_IDX_CNT > 0)
 THEN
  V_SQL := 'ALTER INDEX ' || INDNAME || ' REBUILD TABLESPACE ' || TBSPNAME;
  EXECUTE IMMEDIATE (V_SQL);
 END IF;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (SQLERRM);
END MANH_REBUILD_INDEX;
/

CREATE OR REPLACE PROCEDURE MANH_REVOKE_PRIV_PROD (VONAME VARCHAR2)
IS
 V_OBJNAME   USER_OBJECTS.OBJECT_NAME%TYPE;
 V_OBJTYPE   USER_OBJECTS.OBJECT_TYPE%TYPE;
 V_SQL       VARCHAR2 (1000);
BEGIN
 BEGIN
  SELECT OBJECT_NAME, OBJECT_TYPE
    INTO V_OBJNAME, V_OBJTYPE
    FROM USER_OBJECTS
   WHERE OBJECT_NAME = UPPER (VONAME)
         AND OBJECT_TYPE IN
                ('PROCEDURE',
                 'FUNCTION',
                 'TABLE',
                 'VIEW',
                 'SYNONYM',
                 'PACKAGE',
                 'SEQUENCE');
 EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
     V_OBJNAME := NULL;
     V_OBJTYPE := NULL;
 END;
 IF (V_OBJNAME IS NOT NULL OR V_OBJTYPE IS NOT NULL)
 THEN
  IF (V_OBJTYPE = 'TABLE' OR V_OBJTYPE = 'VIEW' OR V_OBJTYPE = 'SYNONYM')
  THEN
     FOR CUR_PSCHEMA IN (SELECT GRANTEE
                           FROM USER_TAB_PRIVS
                          WHERE TABLE_NAME = 'COMPANY'
                         UNION
                         SELECT OWNER
                           FROM ALL_SYNONYMS
                          WHERE TABLE_OWNER = USER AND OWNER <> USER)
     LOOP
        V_SQL :=
              'REVOKE INSERT, UPDATE, DELETE on '
           || V_OBJNAME
           || ' FROM '
           || CUR_PSCHEMA.GRANTEE;
        EXECUTE IMMEDIATE V_SQL;
     END LOOP;
  ELSIF (   V_OBJTYPE = 'PROCEDURE'
         OR V_OBJTYPE = 'FUNCTION'
         OR V_OBJTYPE = 'PACKAGE')
  THEN
     FOR CUR_PSCHEMA IN (SELECT GRANTEE
                           FROM USER_TAB_PRIVS
                          WHERE TABLE_NAME = 'COMPANY'
                         UNION
                         SELECT OWNER
                           FROM ALL_SYNONYMS
                          WHERE TABLE_OWNER = USER AND OWNER <> USER)
     LOOP
        V_SQL :=
              'REVOKE EXECUTE, DEBUG ON '
           || V_OBJNAME
           || ' FROM '
           || CUR_PSCHEMA.GRANTEE;
        EXECUTE IMMEDIATE V_SQL;
     END LOOP;
  ELSIF (V_OBJTYPE = 'SEQUENCE')
  THEN
     FOR CUR_PSCHEMA IN (SELECT GRANTEE
                           FROM USER_TAB_PRIVS
                          WHERE TABLE_NAME = 'COMPANY'
                         UNION
                         SELECT OWNER
                           FROM ALL_SYNONYMS
                          WHERE TABLE_OWNER = USER AND OWNER <> USER)
     LOOP
        V_SQL :=
              'REVOKE SELECT, ALTER ON '
           || V_OBJNAME
           || ' FROM '
           || CUR_PSCHEMA.GRANTEE;
        EXECUTE IMMEDIATE V_SQL;
     END LOOP;
  END IF;
 ELSE
  DBMS_OUTPUT.PUT_LINE (
        '[WARNING] - Object '
     || V_OBJNAME
     || ' either does not exists or type not supported');
 END IF;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (SQLERRM);
END MANH_REVOKE_PRIV_PROD;
/

CREATE OR REPLACE PROCEDURE MANH_SEQUENCE_DROP (SEQNAME VARCHAR2)
IS
 V_SQL       VARCHAR2 (1000);
 V_SEQ_CNT   NUMBER;
BEGIN
 BEGIN
  SELECT COUNT (*)
    INTO V_SEQ_CNT
    FROM USER_SEQUENCES
   WHERE SEQUENCE_NAME = UPPER (SEQNAME);
 EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
     V_SEQ_CNT := 0;
 END;
 IF (V_SEQ_CNT > 0)
 THEN
  V_SQL := 'DROP SEQUENCE ' || SEQNAME;
  EXECUTE IMMEDIATE (V_SQL);
  DBMS_OUTPUT.PUT_LINE ('SUCCESSFULLY DROPPED SEQUENCE');
 END IF;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (SQLERRM);
END MANH_SEQUENCE_DROP;
/

CREATE OR REPLACE PROCEDURE MSGSTORE_RECEIVE_BEGIN (
 P_MSGTYPE           IN     MESSAGE_STORE_INBOUND.MSG_TYPE%TYPE,
 P_STATCODE          IN     MESSAGE_STORE_INBOUND.STATUS%TYPE,
 P_BUSY_STATCODE     IN     MESSAGE_STORE_INBOUND.STATUS%TYPE,
 P_LAST_UPD_SOURCE   IN     MESSAGE_STORE_INBOUND.LAST_UPDATED_SOURCE%TYPE,
 P_MSGID                OUT INT,
 P_MSGCONTENT           OUT CLOB,
 P_RC                   OUT INT)
IS
BEGIN
 P_RC := 0;
 -- Lock the record for update
 SELECT MESSAGE_ID, MSG_CONTENT
 INTO P_MSGID, P_MSGCONTENT
 FROM MESSAGE_STORE_INBOUND
WHERE MESSAGE_ID = (SELECT MIN (MESSAGE_ID)
                      FROM MESSAGE_STORE_INBOUND
                     WHERE MSG_TYPE = P_MSGTYPE AND STATUS = P_STATCODE)
 FOR UPDATE;
 -- Update the record to set STATUS = 2
 UPDATE MESSAGE_STORE_INBOUND
  SET STATUS = P_BUSY_STATCODE,
      LAST_UPDATED_SOURCE = P_LAST_UPD_SOURCE,
      LAST_UPDATED_DTTM = SYSDATE
WHERE MESSAGE_ID = P_MSGID;
 COMMIT;
EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
  P_MSGID := -1;
  RETURN;
 WHEN OTHERS
 THEN
  P_RC := SQLCODE;
  RAISE_APPLICATION_ERROR (-20010, SQLERRM);
END;
/

CREATE OR REPLACE PROCEDURE MSGSTORE_RECEIVE_FINISH (
 P_MSGID             IN     MESSAGE_STORE_INBOUND.MESSAGE_ID%TYPE,
 P_STATCODE          IN     MESSAGE_STORE_INBOUND.STATUS%TYPE,
 P_LAST_UPD_SOURCE   IN     MESSAGE_STORE_INBOUND.LAST_UPDATED_SOURCE%TYPE,
 P_RESPONSE          IN     MESSAGE_STORE_INBOUND.RESPONSE%TYPE,
 P_RC                   OUT INT)
IS
 V_COUNT   INT;
BEGIN
 P_RC := 0;
   SELECT 1
     INTO V_COUNT
     FROM MESSAGE_STORE_INBOUND
    WHERE MESSAGE_ID = P_MSGID
 FOR UPDATE WAIT 3;
 UPDATE MESSAGE_STORE_INBOUND
  SET STATUS = P_STATCODE,
      RESPONSE = P_RESPONSE,
      LAST_UPDATED_SOURCE = P_LAST_UPD_SOURCE,
      LAST_UPDATED_DTTM = SYSDATE
WHERE MESSAGE_ID = P_MSGID;
 COMMIT;
EXCEPTION
 WHEN OTHERS
 THEN
  P_RC := SQLCODE;
  RAISE_APPLICATION_ERROR (-20010, SQLERRM);
END;
/

CREATE OR REPLACE PROCEDURE POPULATE_LRF_SERVER_CONFIG (
 P_DEFAULT_DEST_QUEUE   IN LRF_SERVER_CONFIG.DEST_QUEUE_NAME%TYPE,
 P_DEFAULT_FREQUENCY    IN LRF_SERVER_CONFIG.FETCH_FREQ%TYPE)
IS
 CURSOR PRT_QUEUE_QUERY
 IS
  SELECT PRT_Q_ID FROM LRF_PRT_QUEUE;
 cnt   NUMBER;
BEGIN
 FOR PRT_QUEUE_QUERY_REC IN PRT_QUEUE_QUERY
 LOOP
  SELECT COUNT (*)
    INTO cnt
    FROM LRF_SERVER_CONFIG
   WHERE GROUP_ID = PRT_QUEUE_QUERY_REC.PRT_Q_ID;
  IF (cnt = 0)
  THEN
     INSERT INTO LRF_SERVER_CONFIG (LRF_SERVER_CONFIG_ID,
                                    GROUP_ID,
                                    FETCH_FREQ,
                                    DEST_QUEUE_NAME,
                                    LAST_UPDATED_DTTM,
                                    LAST_UPDATED_SOURCE)
          VALUES (LRF_SERVER_CONFIG_SEQ.NEXTVAL,
                  PRT_QUEUE_QUERY_REC.PRT_Q_ID,
                  P_DEFAULT_FREQUENCY,
                  P_DEFAULT_DEST_QUEUE,
                  SYSDATE,
                  'PROCEDURE');
  END IF;
 END LOOP;
END;
/

CREATE OR REPLACE PROCEDURE PURGE_DWM_WORK_REQUEST (V_NO_OF_DAYS IN NUMBER,
                                          V_STATUS   IN  VARCHAR2
                                          )
IS
V_SQL			VARCHAR2(2000);
V_WHR_SQL		VARCHAR2(2000) DEFAULT ' ';
V_SQL_AND		NUMBER(4);
vStatus			VARCHAR2(50);
BEGIN
 V_SQL := 'SELECT WORK_REQUEST_ID
                      FROM DWM_WORK_REQUEST' ;
V_SQL_AND := 0;
IF V_STATUS IS NOT NULL
 THEN
  SELECT REPLACE(REPLACE(V_STATUS,',','-,-'),'-,-',''',''') INTO vStatus FROM DUAL;
  V_WHR_SQL := ' WHERE SUBMITTED_DTTM < (SYSDATE - ' || V_NO_OF_DAYS || ' )  AND STATUS IN (''' ||  vStatus || ''')';
	 V_SQL_AND := 1;
	ELSE
  V_WHR_SQL :=  ' WHERE SUBMITTED_DTTM < (SYSDATE - ' || V_NO_OF_DAYS || ' ) ';
  V_SQL_AND := 1;
END IF ;
IF (V_SQL_AND = 1 )
 THEN
 V_SQL := V_SQL || V_WHR_SQL;
 END IF ;
EXECUTE IMMEDIATE 'DELETE FROM DWM_WORK_EVENT_LOG WHERE WORK_REQUEST_ID IN ('|| V_SQL ||')' ;
EXECUTE IMMEDIATE 'DELETE FROM DWM_WORK_REQUEST_ERROR WHERE WORK_REQUEST_ID IN ('|| V_SQL ||')' ;
EXECUTE IMMEDIATE 'DELETE FROM DWM_WORK_REQUEST '|| V_WHR_SQL ;
END;
/

CREATE OR REPLACE PROCEDURE PURGE_MESSAGE_STORE_INBOUND (NUM_DAYS IN NUMBER)
AS
 TYPE ROWID_TAB IS TABLE OF ROWID
                    INDEX BY BINARY_INTEGER;
 V_ROWID_TAB   ROWID_TAB;
 V_ROW_COUNT   NUMBER := 0;
 CURSOR CUR1
 IS
  SELECT ROWID AS ROW_ID
    FROM MESSAGE_STORE_INBOUND
   WHERE LAST_UPDATED_DTTM < (TRUNC (SYSDATE) - NUM_DAYS);
BEGIN
 OPEN CUR1;
 LOOP
  FETCH CUR1
  BULK COLLECT INTO V_ROWID_TAB
  LIMIT 20000;
  EXIT WHEN V_ROW_COUNT = CUR1%ROWCOUNT;
  V_ROW_COUNT := CUR1%ROWCOUNT;
  FORALL I IN 1 .. V_ROWID_TAB.COUNT
     DELETE FROM MESSAGE_STORE_INBOUND
           WHERE ROWID = V_ROWID_TAB (I);
 END LOOP;
 DBMS_OUTPUT.PUT_LINE (
     TO_CHAR (SYSDATE, 'yyyymmdd hh24:mi:ss')
  || ' - Number of Records deleted : '
  || CUR1%ROWCOUNT);
 CLOSE CUR1;
EXCEPTION
 WHEN OTHERS
 THEN
  -- Print out any Oracle error
  DBMS_OUTPUT.PUT_LINE (SQLCODE || SQLERRM (SQLCODE));
  -- Close cursor if still open
  IF (CUR1%ISOPEN)
  THEN
     CLOSE CUR1;
  END IF;
  RETURN;
END;
/

CREATE OR REPLACE PROCEDURE PURGE_MESSAGE_STORE_OUTBOUND (NUM_DAYS IN NUMBER)
AS
 TYPE ROWID_TAB IS TABLE OF ROWID
                    INDEX BY BINARY_INTEGER;
 V_ROWID_TAB   ROWID_TAB;
 V_ROW_COUNT   NUMBER := 0;
 CURSOR CUR1
 IS
  SELECT ROWID AS ROW_ID
    FROM MESSAGE_STORE_OUTBOUND
   WHERE LAST_UPDATED_DTTM < (TRUNC (SYSDATE) - NUM_DAYS);
BEGIN
 OPEN CUR1;
 LOOP
  FETCH CUR1
  BULK COLLECT INTO V_ROWID_TAB
  LIMIT 20000;
  EXIT WHEN V_ROW_COUNT = CUR1%ROWCOUNT;
  V_ROW_COUNT := CUR1%ROWCOUNT;
  FORALL I IN 1 .. V_ROWID_TAB.COUNT
     DELETE FROM MESSAGE_STORE_OUTBOUND
           WHERE ROWID = V_ROWID_TAB (I);
 END LOOP;
 DBMS_OUTPUT.PUT_LINE (
     TO_CHAR (SYSDATE, 'yyyymmdd hh24:mi:ss')
  || ' - Number of Records deleted : '
  || CUR1%ROWCOUNT);
 CLOSE CUR1;
EXCEPTION
 WHEN OTHERS
 THEN
  -- Print out any Oracle error
  DBMS_OUTPUT.PUT_LINE (SQLCODE || SQLERRM (SQLCODE));
  -- Close cursor if still open
  IF (CUR1%ISOPEN)
  THEN
     CLOSE CUR1;
  END IF;
  RETURN;
END;
/

CREATE OR REPLACE PROCEDURE PURGE_SEESION_AUDIT (P_DATE in date)
IS
V_MONTHS_COUNT Number;
begin
Select MONTHS_BETWEEN
 (TO_CHAR(TO_DATE(SYSDATE,'DD-MM-YYYY')), TO_CHAR(TO_DATE(P_DATE,'DD-MM-YYYY')) ) into V_MONTHS_COUNT from dual;
if V_MONTHS_COUNT > 24
then
delete from SESSION_AUDIT where TO_DATE(TO_CHAR(CREATED_DTTM, 'DD-MM-YYYY'),'DD-MM-YYYY') < TO_DATE(TO_CHAR(P_DATE,'DD-MM-YYYY'),'DD-MM-YYYY');
ELSE NULL;
commit;
end if;
end;
/

CREATE OR REPLACE PROCEDURE PURGE_STATS_SNAPSHOT (  V_NO_OF_DAYS IN NUMBER )
IS
V_SQL			VARCHAR2(2000);
V_WHR_SQL		VARCHAR2(2000) DEFAULT ' ';
BEGIN
V_SQL := 'SELECT SNAPSHOT_ID
                      FROM STATS_SNAPSHOT ' ;
V_WHR_SQL :=  ' WHERE SNAPSHOT_START_TIME < (SYSDATE - ' || V_NO_OF_DAYS || ' ) ';
V_SQL := V_SQL || V_WHR_SQL;
EXECUTE IMMEDIATE 'DELETE FROM STATS_SNAPSHOT_EVENT_DATA WHERE SNAPSHOT_ID IN ('|| V_SQL ||')' ;
EXECUTE IMMEDIATE 'DELETE FROM STATS_SNAPSHOT_EVENT WHERE SNAPSHOT_ID IN ('|| V_SQL ||')' ;
EXECUTE IMMEDIATE 'DELETE FROM STATS_SNAPSHOT '|| V_WHR_SQL ;
END;
/

CREATE OR REPLACE PROCEDURE PURGE_TRAN_LOG (
 V_MESSAGE_TYPE     IN     VARCHAR2,
 V_FROM_DATE        IN     TIMESTAMP,
 V_TO_DATE          IN     TIMESTAMP,
 V_NO_OF_DAYS       IN     NUMBER,
 V_DELETE_SUCCESS   IN     NUMBER,
 V_DELETE_MESSAGE   IN     NUMBER,
 V_TRANID              OUT CLOB)
IS
 TYPE TranLogRecTyp IS RECORD
 (
  TRAN_LOG_ID   TRAN_LOG.TRAN_LOG_ID%TYPE,
  MESSAGE_ID    TRAN_LOG.MESSAGE_ID%TYPE
 );
 TYPE TRAN_T IS TABLE OF TranLogRecTyp
                 INDEX BY BINARY_INTEGER;
 V_TRAN_ARR           TRAN_T;
 V_SQL                VARCHAR2 (2000);
 V_WHR_SQL            VARCHAR2 (2000);
 V_WHERE_STR          VARCHAR2 (6) := 'WHERE ';
 V_NO_OF_DAYS_LOCAL   NUMBER := 0;
 V_PURGE_COMPLETE     PLS_INTEGER := 0;
 V_SQL_AND            NUMBER;
 --vTranId   CLOB;
 TYPE CURSOR_TYPE IS REF CURSOR;
 st_cursor            CURSOR_TYPE;
 v_row_count          PLS_INTEGER := 0;
BEGIN
 V_SQL := 'SELECT TRAN_LOG_ID, MESSAGE_ID
                     FROM TRAN_LOG ';
 V_SQL_AND := 0;
 IF V_NO_OF_DAYS IS NOT NULL
 THEN
  V_NO_OF_DAYS_LOCAL := V_NO_OF_DAYS;
 END IF;
 IF     V_MESSAGE_TYPE IS NULL
  AND V_FROM_DATE IS NULL
  AND V_TO_DATE IS NULL
  AND V_NO_OF_DAYS_LOCAL = 0
  AND V_DELETE_SUCCESS = 0
  AND V_DELETE_MESSAGE = 1
  AND V_PURGE_COMPLETE = 0
 THEN
  UPDATE TRAN_LOG
     SET IS_MSG_STORED = 0
   WHERE IS_MSG_STORED <> 0;
  SELECT XMLAGG (XMLELEMENT (e, tran_log_id || ',')).EXTRACT ('//text()').getclobval ()
            tran_log_id
    INTO V_TRANID
    FROM TRAN_LOG_MESSAGE;
  EXECUTE IMMEDIATE 'TRUNCATE TABLE TRAN_LOG_MESSAGE';
  V_PURGE_COMPLETE := 1;
 END IF;
 IF V_MESSAGE_TYPE IS NOT NULL AND V_PURGE_COMPLETE = 0
 THEN
  V_WHR_SQL := 'MSG_TYPE =' || '''' || V_MESSAGE_TYPE || '''';
  V_SQL_AND := 1;
 END IF;
 IF     V_FROM_DATE IS NOT NULL
  AND V_TO_DATE IS NOT NULL
  AND V_PURGE_COMPLETE = 0
 THEN
  IF V_SQL_AND = 1
  THEN
     V_WHR_SQL :=
           V_WHR_SQL
        || ' AND LAST_UPDATED_DTTM BETWEEN '
        || ''''
        || V_FROM_DATE
        || ''''
        || 'AND '
        || ''''
        || V_TO_DATE
        || ''''
        || '';
  ELSE
     V_WHR_SQL :=
           V_WHR_SQL
        || ' LAST_UPDATED_DTTM BETWEEN '
        || ''''
        || V_FROM_DATE
        || ''''
        || ' AND '
        || ''''
        || V_TO_DATE
        || ''''
        || '';
     V_SQL_AND := 1;
  END IF;
 END IF;
 IF V_DELETE_SUCCESS = 1 AND V_PURGE_COMPLETE = 0
 THEN
  IF V_SQL_AND = 1
  THEN
     V_WHR_SQL := V_WHR_SQL || ' AND HAS_ERRORS = 0';
  ELSE
     V_WHR_SQL := V_WHR_SQL || ' HAS_ERRORS = 0';
     V_SQL_AND := 1;
  END IF;
 END IF;
 IF     (V_FROM_DATE IS NULL OR V_TO_DATE IS NULL)
  AND (V_NO_OF_DAYS_LOCAL > 0)
  AND V_PURGE_COMPLETE = 0
 THEN
  IF V_SQL_AND = 1
  THEN
     V_WHR_SQL :=
           V_WHR_SQL
        || ' AND LAST_UPDATED_DTTM < (SYSDATE - '
        || V_NO_OF_DAYS_LOCAL
        || ')';
  ELSE
     V_WHR_SQL :=
           V_WHR_SQL
        || ' LAST_UPDATED_DTTM < (SYSDATE - '
        || V_NO_OF_DAYS_LOCAL
        || ')';
     V_SQL_AND := 1;
  END IF;
 END IF;
 IF (V_WHR_SQL IS NOT NULL)
 THEN
  V_SQL := V_SQL || V_WHERE_STR || V_WHR_SQL;
 END IF;
 DBMS_OUTPUT.put_line (' ' || V_SQL);
 OPEN st_cursor FOR V_SQL;
 IF V_DELETE_SUCCESS = 1 AND V_PURGE_COMPLETE = 0
 THEN
  LOOP
     FETCH st_cursor
     BULK COLLECT INTO V_TRAN_ARR
     LIMIT 10000;
     EXIT WHEN v_row_count = st_cursor%ROWCOUNT;
     v_row_count := st_cursor%ROWCOUNT;
     FORALL i IN 1 .. V_TRAN_ARR.COUNT
        DELETE FROM TRAN_LOG_RESPONSE_MESSAGE
              WHERE MESSAGE_ID = V_TRAN_ARR (i).MESSAGE_ID;
     FORALL i IN 1 .. V_TRAN_ARR.COUNT
        DELETE FROM TRAN_LOG_MESSAGE TL1
              WHERE TL1.TRAN_LOG_ID = V_TRAN_ARR (i).TRAN_LOG_ID
                    AND NOT EXISTS
                               (SELECT 1
                                  FROM TRAN_LOG TL2
                                 WHERE TL1.TRAN_LOG_ID = TL2.TRAN_LOG_ID
                                       AND TL2.HAS_ERRORS != 0);
     FORALL i IN 1 .. V_TRAN_ARR.COUNT
        INSERT INTO DD_TRAN_LOG_ID_GTT
             VALUES (V_TRAN_ARR (i).TRAN_LOG_ID);
     FORALL i IN 1 .. V_TRAN_ARR.COUNT
        DELETE FROM TRAN_LOG
              WHERE TRAN_LOG_ID = V_TRAN_ARR (i).TRAN_LOG_ID;
     COMMIT;
  END LOOP;
 ELSE
  IF V_PURGE_COMPLETE = 0
  THEN
     LOOP
        FETCH st_cursor
        BULK COLLECT INTO V_TRAN_ARR
        LIMIT 10000;
        EXIT WHEN v_row_count = st_cursor%ROWCOUNT;
        v_row_count := st_cursor%ROWCOUNT;
        FORALL i IN 1 .. V_TRAN_ARR.COUNT
           DELETE FROM TRAN_LOG_RESPONSE_MESSAGE
                 WHERE MESSAGE_ID = V_TRAN_ARR (i).MESSAGE_ID;
        FORALL i IN 1 .. V_TRAN_ARR.COUNT
           DELETE FROM TRAN_LOG
                 WHERE TRAN_LOG_ID = V_TRAN_ARR (i).TRAN_LOG_ID;
        FORALL i IN 1 .. V_TRAN_ARR.COUNT
           INSERT INTO DD_TRAN_LOG_ID_GTT
                VALUES (V_TRAN_ARR (i).TRAN_LOG_ID);
        FORALL i IN 1 .. V_TRAN_ARR.COUNT
           DELETE FROM TRAN_LOG_MESSAGE
                 WHERE TRAN_LOG_ID = V_TRAN_ARR (i).TRAN_LOG_ID;
        COMMIT;
     END LOOP;
  END IF;
 END IF;
 SELECT XMLAGG (XMLELEMENT (e, tranid || ',')).EXTRACT ('//text()').getclobval ()
         tran_log_id
 INTO V_TRANID
 FROM DD_TRAN_LOG_ID_GTT;
 EXECUTE IMMEDIATE 'TRUNCATE  TABLE DD_TRAN_LOG_ID_GTT';
 COMMIT;
EXCEPTION
 WHEN OTHERS
 THEN
  DBMS_OUTPUT.PUT_LINE (SQLERRM);
END;
/

CREATE OR REPLACE PROCEDURE QUIET_DROP (v_object_type   IN VARCHAR2,
                                    v_object_name   IN VARCHAR2)
AS
v_statement           VARCHAR2 (100);
missing_idx_object    EXCEPTION;
PRAGMA EXCEPTION_INIT (missing_idx_object, -01418);
missing_sequ_object   EXCEPTION;
PRAGMA EXCEPTION_INIT (missing_sequ_object, -02289);
missing_trig_object   EXCEPTION;
PRAGMA EXCEPTION_INIT (missing_trig_object, -04080);
missing_view_object   EXCEPTION;
PRAGMA EXCEPTION_INIT (missing_view_object, -00942);
missing_synm_object   EXCEPTION;
PRAGMA EXCEPTION_INIT (missing_synm_object, -01434);
missing_othr_object   EXCEPTION;
PRAGMA EXCEPTION_INIT (missing_othr_object, -04043);
missing_cstr_object   EXCEPTION;
PRAGMA EXCEPTION_INIT (missing_cstr_object, -02443);
BEGIN
IF (UPPER (v_object_type) = 'TABLE')
THEN
  v_statement :=
        'DROP '
     || v_object_type
     || ' '
     || v_object_name
     || ' cascade constraints';
ELSE
  v_statement := 'DROP ' || v_object_type || ' ' || v_object_name;
END IF;
EXECUTE IMMEDIATE v_statement;
EXCEPTION
WHEN missing_idx_object
THEN
  DBMS_OUTPUT.Put_Line (v_statement);
  COMMIT;
WHEN missing_sequ_object
THEN
  DBMS_OUTPUT.Put_Line (v_statement);
  COMMIT;
WHEN missing_trig_object
THEN
  DBMS_OUTPUT.Put_Line (v_statement);
  COMMIT;
WHEN missing_view_object
THEN
  DBMS_OUTPUT.Put_Line (v_statement);
  COMMIT;
WHEN missing_synm_object
THEN
  DBMS_OUTPUT.Put_Line (v_statement);
  COMMIT;
WHEN missing_othr_object
THEN
  DBMS_OUTPUT.Put_Line (v_statement);
  COMMIT;
WHEN missing_cstr_object
THEN
  DBMS_OUTPUT.Put_Line (v_statement);
  COMMIT;
WHEN NO_DATA_FOUND
THEN
  DBMS_OUTPUT.Put_Line (v_statement);
  COMMIT;
END;
/

CREATE OR REPLACE PROCEDURE RELEASE_LOCK (P_TC_OBJECT_ID    IN     VARCHAR2,
                    P_TABLE_NAME      IN     VARCHAR2,
                    P_TC_COMPANY_ID   IN     NUMBER,
                    P_REF_COUNT       IN     NUMBER,
                    P_PROCESS         IN     VARCHAR2,
                    P_LOCK_OWNER      IN     NUMBER,
                    P_COMMENTS        IN     VARCHAR2,
                    P_SOURCE_TYPE     IN     NUMBER,
                    P_SOURCE          IN     VARCHAR2,
                    CHECKDEPENDENTS   IN     NUMBER,
                    P_RELEASED           OUT NUMBER)
AS
 V_DELETE_SQL          VARCHAR2 (1000);
 V_SELECT_SQL          VARCHAR2 (1000);
 V_DEP_GROUP_ID_REC    NUMBER (10);
 V_SELECT_RCOUNT_SQL   VARCHAR2 (1000);
 V_UPDATE_SQL          VARCHAR2 (1000);
 V_NO_OF_ROWS          NUMBER (10);
 V_LOCK_GROUP_ID       NUMBER (10);
 V_REFCOUNT            NUMBER (10);
BEGIN
 P_RELEASED := 0;
 V_SELECT_SQL :=
     'SELECT LAST_GROUP_ID FROM  '
  || P_TABLE_NAME
  || ' WHERE TC_OBJECT_ID = :VAR1
                       AND TC_COMPANY_ID = :VAR2 ';
 V_SELECT_RCOUNT_SQL :=
     'SELECT REF_COUNT FROM  '
  || P_TABLE_NAME
  || ' WHERE TC_OBJECT_ID = :VAR1
                       AND TC_COMPANY_ID = :VAR2 ';
 V_DELETE_SQL :=
     'DELETE FROM '
  || P_TABLE_NAME
  || ' WHERE TC_OBJECT_ID = :VAR1
                       AND TC_COMPANY_ID = :VAR2 ';
 V_UPDATE_SQL :=
     'UPDATE '
  || P_TABLE_NAME
  || ' SET REF_COUNT = REF_COUNT - :VAR1 ,
                       PROCESS = :VAR2 ,
                       COMMENTS =  :VAR3,
                       LAST_UPDATED_SOURCE_TYPE =  :VAR4,
                       LAST_UPDATED_SOURCE = :VAR5,
                       LAST_UPDATED_DTTM = SYSDATE
                       WHERE TC_OBJECT_ID =  :VAR6
                       AND TC_COMPANY_ID = :VAR7
                       AND LOCK_OWNER = :VAR8
                       AND CREATED_GROUP_ID <= :VAR9
                       AND LAST_GROUP_ID >= :VAR10 ';
 BEGIN
  EXECUTE IMMEDIATE V_SELECT_SQL
     INTO V_LOCK_GROUP_ID
     USING P_TC_OBJECT_ID, P_TC_COMPANY_ID;
  EXECUTE IMMEDIATE V_UPDATE_SQL
     USING P_REF_COUNT,
           P_PROCESS,
           P_COMMENTS,
           P_SOURCE_TYPE,
           P_SOURCE,
           P_TC_OBJECT_ID,
           P_TC_COMPANY_ID,
           P_LOCK_OWNER,
           V_LOCK_GROUP_ID,
           V_LOCK_GROUP_ID;
  IF (SQL%ROWCOUNT <> 0)
  THEN
     P_RELEASED := 1;
     IF (CHECKDEPENDENTS = 1)
     THEN
        SELECT CASE
                  WHEN EXISTS
                          (SELECT 1
                             FROM DO_LOCK DLK
                            WHERE V_LOCK_GROUP_ID >= DLK.CREATED_GROUP_ID
                                  AND V_LOCK_GROUP_ID <=
                                         DLK.LAST_GROUP_ID
                                  AND REF_COUNT > 0
                                  AND ROWNUM <= 1
                           UNION
                           SELECT 1
                             FROM SHIPMENT_LOCK SHLK
                            WHERE V_LOCK_GROUP_ID >=
                                     SHLK.CREATED_GROUP_ID
                                  AND V_LOCK_GROUP_ID <=
                                         SHLK.LAST_GROUP_ID
                                  AND REF_COUNT > 0
                                  AND ROWNUM <= 1)
                  THEN
                     1
                  ELSE
                     0
               END
          INTO V_DEP_GROUP_ID_REC
          FROM DUAL;
        IF (V_DEP_GROUP_ID_REC = 0)
        THEN
           DELETE FROM DO_LOCK DLK
                 WHERE     V_LOCK_GROUP_ID >= DLK.CREATED_GROUP_ID
                       AND V_LOCK_GROUP_ID <= DLK.LAST_GROUP_ID
                       AND DLK.REF_COUNT <= 0;
           IF (SQL%ROWCOUNT <> 0)
           THEN
              P_RELEASED := 1;
           END IF;
           DELETE FROM SHIPMENT_LOCK SHLK
                 WHERE     V_LOCK_GROUP_ID >= SHLK.CREATED_GROUP_ID
                       AND V_LOCK_GROUP_ID <= SHLK.LAST_GROUP_ID
                       AND SHLK.REF_COUNT <= 0;
           IF (SQL%ROWCOUNT <> 0)
           THEN
              P_RELEASED := 1;
           END IF;
        END IF;
     ELSE
        EXECUTE IMMEDIATE V_SELECT_RCOUNT_SQL
           INTO V_REFCOUNT
           USING P_TC_OBJECT_ID, P_TC_COMPANY_ID;
        IF (V_REFCOUNT = 0)
        THEN
           EXECUTE IMMEDIATE V_DELETE_SQL
              USING P_TC_OBJECT_ID, P_TC_COMPANY_ID;
        END IF;
     END IF;
  END IF;
 EXCEPTION
  WHEN NO_DATA_FOUND
  THEN
     P_RELEASED := 0;
 END;
END;
/

CREATE OR REPLACE PROCEDURE SEQNEXTVAL (
P_SEQNAME    IN           VARCHAR2,
P_NEXTVAL    OUT       NUMBER)
IS
	v_dynstmt VARCHAR2(100);
v_prepstmt VARCHAR2(250);
c_nextval SYS_REFCURSOR;
BEGIN
v_dynstmt := 'SELECT '||p_seqname||'.NEXTVAL FROM DUAL' ;
EXECUTE IMMEDIATE (V_DYNSTMT) INTO P_NEXTVAL;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			DBMS_OUTPUT.PUT_LINE('Mentioned sequence does not exist');
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE(sqlerrm);
END;
/

CREATE OR REPLACE PROCEDURE SEQUPDT (T_NAME VARCHAR2, C_NAME VARCHAR2, S_NAME VARCHAR2)
AS
 S_VAL   NUMBER;
 L_STR   VARCHAR2 (200);
 C_VAL   NUMBER;
 T_VAL   NUMBER;
BEGIN
 L_STR := 'select max(' || NVL (C_NAME, 0) || ') from ' || T_NAME;
 EXECUTE IMMEDIATE L_STR INTO S_VAL;
 L_STR := 'select ' || S_NAME || '.nextval from dual';
 EXECUTE IMMEDIATE L_STR INTO C_VAL;
 IF (S_VAL > C_VAL)
 THEN
  S_VAL := S_VAL - C_VAL;
  L_STR := 'alter sequence ' || S_NAME || ' increment by ' || S_VAL;
  EXECUTE IMMEDIATE L_STR;
  L_STR := 'select ' || S_NAME || '.nextval from dual';
  EXECUTE IMMEDIATE L_STR INTO T_VAL;
  L_STR := 'alter sequence ' || S_NAME || ' increment by 1';
  EXECUTE IMMEDIATE L_STR;
 END IF;
END Sequpdt;
/

CREATE OR REPLACE PROCEDURE TRAN_LOG_READ_BEGIN (P_MESSAGE_ID             IN     NUMBER,
                           P_IN_TRASIT_RESULTCODE   IN     NUMBER,
                           P_TRAN_LOG_ID               OUT NUMBER,
                           P_ORIGIN_FORMAT             OUT VARCHAR2,
                           P_ROUTER_ID                 OUT NUMBER,
                           P_DIRECTION                 OUT VARCHAR2,
                           P_RC                        OUT NUMBER)
IS
BEGIN
 P_RC := 0;
 SELECT TRAN_LOG_ID,
      ORIGIN_FORMAT,
      ROUTER_ID,
      DIRECTION
 INTO P_TRAN_LOG_ID,
      P_ORIGIN_FORMAT,
      P_ROUTER_ID,
      P_DIRECTION
 FROM TRAN_LOG
WHERE MESSAGE_ID = P_MESSAGE_ID
 FOR UPDATE;
 -- Update the record to set RESULT_CODE = IN_TRANSIT
 UPDATE TRAN_LOG
  SET RESULT_CODE = P_IN_TRASIT_RESULTCODE, LAST_UPDATED_DTTM = SYSDATE
WHERE TRAN_LOG_ID = P_TRAN_LOG_ID AND MESSAGE_ID = P_MESSAGE_ID;
 COMMIT;
END;
/

CREATE OR REPLACE PROCEDURE TRAN_LOG_READ_FINISH (P_TRAN_LOG_ID     IN     NUMBER,
                            P_MESSAGE_ID      IN     NUMBER,
                            P_RESULTCODE      IN     NUMBER,
                            P_ERROR_DETAILS   IN     VARCHAR2,
                            P_HAS_ERRORS      IN     NUMBER,
                            P_PROCESSED_AT    IN     VARCHAR2,
                            P_RC                 OUT NUMBER)
IS
BEGIN
 P_RC := 0;
 UPDATE TRAN_LOG
  SET RESULT_CODE = P_RESULTCODE,
      ERROR_DETAILS = P_ERROR_DETAILS,
      HAS_ERRORS = P_HAS_ERRORS,
      PROCESSED_AT = P_PROCESSED_AT,
      LAST_UPDATED_DTTM = SYSDATE
WHERE TRAN_LOG_ID = P_TRAN_LOG_ID AND MESSAGE_ID = P_MESSAGE_ID;
 COMMIT;
END;
/

CREATE OR REPLACE PACKAGE LANE_LOCATION_PKG
AS
 -- if this value changes all tables containing
 -- these strings will need to be refreshed
 cvDelimiter   CONSTANT VARCHAR2 (1) := '~';
 FUNCTION fnGetLocationString (aLocType       IN VARCHAR2,
                             aFacilityId    IN NUMBER,
                             aCity          IN VARCHAR2,
                             aCounty        IN VARCHAR2,
                             aStateProv     IN VARCHAR2,
                             aPostalCode    IN VARCHAR2,
                             aCountryCode   IN VARCHAR2,
                             aZoneId        IN VARCHAR2)
  RETURN VARCHAR2;
 FUNCTION fnGetFacilityCSVFromAlias3PL (aCompanyId   IN VARCHAR2,
                                      aList        IN VARCHAR2)
  RETURN VARCHAR2;
 FUNCTION fnGetLocationCSVFromCSV (aPrefix   IN VARCHAR2,
                                 aList     IN VARCHAR2,
                                 aSuffix   IN VARCHAR2 DEFAULT NULL)
  RETURN VARCHAR2;
 FUNCTION fnGetFacilityCSVFromAliasCSV (aCompanyId   IN NUMBER,
                                      aList        IN VARCHAR2)
  RETURN VARCHAR2;
 FUNCTION fnGetImportLocationString (vBusinessUnit       IN VARCHAR2,
                                   vOFacilityAliasId   IN VARCHAR2,
                                   vOCity              IN VARCHAR2,
                                   vOStateProv         IN VARCHAR2,
                                   vOCounty            IN VARCHAR2,
                                   vOPostal            IN VARCHAR2,
                                   vOCountry           IN VARCHAR2,
                                   vOZone              IN VARCHAR2,
                                   vDFacilityAliasId   IN VARCHAR2,
                                   vDCity              IN VARCHAR2,
                                   vDStateProv         IN VARCHAR2,
                                   vDCounty            IN VARCHAR2,
                                   vDPostal            IN VARCHAR2,
                                   vDCountry           IN VARCHAR2,
                                   vDZone              IN VARCHAR2)
  RETURN VARCHAR2;
END;
/

CREATE OR REPLACE PACKAGE BODY LANE_LOCATION_PKG
AS
 /*************************************
 *
 *  Returns the concatenated location string
 *  of the location type then the relevent
 *  data for the particular location type
 *
 *************************************/
 FUNCTION fnGetLocationString (aLocType       IN VARCHAR2,
                             aFacilityId    IN NUMBER,
                             aCity          IN VARCHAR2,
                             aCounty        IN VARCHAR2,
                             aStateProv     IN VARCHAR2,
                             aPostalCode    IN VARCHAR2,
                             aCountryCode   IN VARCHAR2,
                             aZoneId        IN VARCHAR2)
  RETURN VARCHAR2
 IS
  vLocationString   VARCHAR2 (50);
 BEGIN
  SELECT DECODE (
            aLocType,
            'CO', aLocType || cvDelimiter || aCountryCode,
            'FA', aLocType || cvDelimiter || TO_CHAR (aFacilityId),
            'CS',    aLocType
                  || cvDelimiter
                  || aCity
                  || cvDelimiter
                  || aStateProv
                  || cvDelimiter
                  || aCountryCode,
            'ST',    aLocType
                  || cvDelimiter
                  || aStateProv
                  || cvDelimiter
                  || aCountryCode,
            'P2',    aLocType
                  || cvDelimiter
                  || SUBSTR (aPostalCode, 1, 2)
                  || cvDelimiter
                  || aCountryCode,
            'P3',    aLocType
                  || cvDelimiter
                  || SUBSTR (aPostalCode, 1, 3)
                  || cvDelimiter
                  || aCountryCode,
            'P4',    aLocType
                  || cvDelimiter
                  || SUBSTR (aPostalCode, 1, 4)
                  || cvDelimiter
                  || aCountryCode,
            'P5',    aLocType
                  || cvDelimiter
                  || SUBSTR (aPostalCode, 1, 5)
                  || cvDelimiter
                  || aCountryCode,
            'P6',    aLocType
                  || cvDelimiter
                  || SUBSTR (aPostalCode, 1, 6)
                  || cvDelimiter
                  || aCountryCode,
            'ZN', aLocType || cvDelimiter || aZoneId,
            NULL)
    INTO vLocationString
    FROM DUAL;
  RETURN vLocationString;
 END;
 /*************************************
 *
 *  Returns a CSV String with Location Type inserted in
 *  front of each value in the input string
 *
 *  Output can be used in in queries against
 *  search locations in rating_lane and rg_lane
 *
 *  For example:
 *      aLocType = 'FA'     aList = '''test'',''test2'''
 *      returns '''FA~test'',''FA~test2'''
 *************************************/
 FUNCTION fnGetLocationCSVFromCSV (aPrefix   IN VARCHAR2,
                                 aList     IN VARCHAR2,
                                 aSuffix   IN VARCHAR2 DEFAULT NULL)
  RETURN VARCHAR2
 IS
  vList                 VARCHAR2 (1000) := NULL;
  vPrefix               VARCHAR2 (30) := aPrefix || cvDelimiter;
  vSuffix               VARCHAR2 (30) := NULL;
  -- These can be changed if simular data is in the string
  cvValStart   CONSTANT VARCHAR2 (5) := '<<<<';
  cvValEnd     CONSTANT VARCHAR2 (5) := '>>>>';
 BEGIN
  IF aList IS NOT NULL
  THEN
     IF aSuffix IS NOT NULL
     THEN
        vSuffix := cvDelimiter || aSuffix;
     END IF;
     vList := cvValStart || LTRIM (RTRIM (alist, ''''), '''') || cvValEnd;
     vList := REPLACE (vlist, ''''',''''', cvValStart || cvValEnd);
     vList := REPLACE (vlist, ''',''', cvValEnd || cvValStart);
     vList := REPLACE (vList, cvValStart || cvValEnd, ''''',''''');
     vList := REPLACE (vList, cvValStart, '''' || vPrefix);
     vList := RTRIM (REPLACE (vList, cvValEnd, vSuffix || ''','), ',');
  END IF;
  RETURN vList;
 END;
 /*************************************
 *
 *  Returns a Search location string of facility id's
 *  For the input company and facility alias csv list
 *
 *************************************/
 FUNCTION fnGetFacilityCSVFromAlias3PL (aCompanyId   IN VARCHAR2,
                                      aList        IN VARCHAR2)
  RETURN VARCHAR2
 IS
  vSQL              VARCHAR2 (2000);
  vFacilityString   VARCHAR2 (2000) := NULL;
  TYPE tRefCur IS REF CURSOR;
  cFacilityId       tRefCur;
  nFacilityId       FACILITY_ALIAS.FACILITY_ID%TYPE;
  nCount            NUMBER := 0;
 BEGIN
  vSQL :=
        'SELECT FACILITY_ID FROM FACILITY_ALIAS '
     || ' WHERE TC_COMPANY_ID in ( '
     || aCompanyId
     || ' ) and facility_alias_id in ('
     || aList
     || ')';
  --    '(select * from THE (SELECT CAST(IN_LIST(:FacilityAliasList) AS IN_LIST_TAB) FROM DUAL))';
  --    open cFacilityId for vSql using aCompanyId, replace(aList,'''','');
  OPEN cFacilityId FOR vSql;
  LOOP
     FETCH cFacilityId INTO nFacilityId;
     EXIT WHEN cFacilityId%NOTFOUND;
     nCount := nCount + 1;
     vFacilityString :=
           vFacilityString
        || ',''FA'
        || cvDelimiter
        || TO_CHAR (nFacilityId)
        || '''';
  END LOOP;
  IF nCount = 0
  THEN
     vFacilityString := '''DUMMY''';
  END IF;
  vFacilityString := LTRIM (vFacilityString, ',');
  RETURN vFacilityString;
  NULL;
 END;
 /****************************************
 ****************************************/
 FUNCTION fnGetFacilityCSVFromAliasCSV (aCompanyId   IN NUMBER,
                                      aList        IN VARCHAR2)
  RETURN VARCHAR2
 IS
 BEGIN
  RETURN fnGetFacilityCSVFromAlias3PL (aCompanyId, aList);
 END;
 /****************************************
 ****************************************/
 FUNCTION fnGetImportLocationString (vBusinessUnit       IN VARCHAR2,
                                   vOFacilityAliasId   IN VARCHAR2,
                                   vOCity              IN VARCHAR2,
                                   vOStateProv         IN VARCHAR2,
                                   vOCounty            IN VARCHAR2,
                                   vOPostal            IN VARCHAR2,
                                   vOCountry           IN VARCHAR2,
                                   vOZone              IN VARCHAR2,
                                   vDFacilityAliasId   IN VARCHAR2,
                                   vDCity              IN VARCHAR2,
                                   vDStateProv         IN VARCHAR2,
                                   vDCounty            IN VARCHAR2,
                                   vDPostal            IN VARCHAR2,
                                   vDCountry           IN VARCHAR2,
                                   vDZone              IN VARCHAR2)
  RETURN VARCHAR2
 IS
  vLocationString   VARCHAR2 (500);
 BEGIN
  vLocationString :=
        TRIM (vBusinessUnit)
     || cvDelimiter
     || TRIM (vOFacilityAliasId)
     || cvDelimiter
     || TRIM (vOCity)
     || cvDelimiter
     || TRIM (vOStateProv)
     || cvDelimiter
     || TRIM (vOCounty)
     || cvDelimiter
     || TRIM (vOPostal)
     || cvDelimiter
     || TRIM (vOCountry)
     || cvDelimiter
     || TRIM (vOZone)
     || cvDelimiter
     || TRIM (vDFacilityAliasId)
     || cvDelimiter
     || TRIM (vDCity)
     || cvDelimiter
     || TRIM (vDStateProv)
     || cvDelimiter
     || TRIM (vDCounty)
     || cvDelimiter
     || TRIM (vDPostal)
     || cvDelimiter
     || TRIM (vDCountry)
     || cvDelimiter
     || TRIM (vDZone)
     || cvDelimiter;
  RETURN vLocationString;
 END;
END;
/

CREATE OR REPLACE TRIGGER ACCESSORIAL_RATE_A_IU_TR_1
 AFTER INSERT OR UPDATE
 ON ACCESSORIAL_RATE
 FOR EACH ROW
DECLARE
 l_old_value         CARRIER_PARAMETER_EVENT.OLD_VALUE%TYPE;
 l_new_value         CARRIER_PARAMETER_EVENT.NEW_VALUE%TYPE;
 l_chg_flag          NUMBER (1) := 0;
 l_time_now          DATE := SYSDATE;
 last_updated_user   CARRIER_PARAMETER_EVENT.LAST_UPDATED_SRC%TYPE;
 OldIncotermName     INCOTERM.INCOTERM_NAME%TYPE;
 NewIncotermName     INCOTERM.INCOTERM_NAME%TYPE;
BEGIN
 -- ACCESSORAIL_ID
 IF ( (:old.ACCESSORIAL_ID <> :new.ACCESSORIAL_ID)
   OR ( (:old.ACCESSORIAL_ID IS NULL)
       AND (:new.ACCESSORIAL_ID IS NOT NULL))
   OR ( (:new.ACCESSORIAL_ID IS NULL)
       AND (:old.ACCESSORIAL_ID IS NOT NULL)))
 THEN
  -- fix CR24776
  IF (:old.ACCESSORIAL_ID IS NULL)
  THEN
     l_old_value := NULL;
  ELSE
     SELECT accessorial_code
       INTO l_old_value
       FROM ACCESSORIAL_CODE
      WHERE accessorial_id = :old.ACCESSORIAL_ID;
  END IF;
  IF (:new.ACCESSORIAL_ID IS NULL)
  THEN
     l_new_value := NULL;
  ELSE
     SELECT accessorial_code
       INTO l_new_value
       FROM ACCESSORIAL_CODE
      WHERE accessorial_id = :new.ACCESSORIAL_ID;
  END IF;
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'ACCESSORIAL CODE',
                               l_old_value,        -- :old.ACCESSORIAL_ID,
                               l_new_value,         -- :new.ACCESSORIAL_ID
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- RATE
 IF (   (:old.RATE <> :new.RATE)
   OR ( (:old.RATE IS NULL) AND (:new.RATE IS NOT NULL))
   OR ( (:new.RATE IS NULL) AND (:old.RATE IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'RATE',
                               :old.RATE,
                               :new.RATE,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- PAYEE_CARRIER_ID
 IF ( (:old.PAYEE_CARRIER_ID <> :new.PAYEE_CARRIER_ID)
   OR ( (:old.PAYEE_CARRIER_ID IS NULL)
       AND (:new.PAYEE_CARRIER_ID IS NOT NULL))
   OR ( (:new.PAYEE_CARRIER_ID IS NULL)
       AND (:old.PAYEE_CARRIER_ID IS NOT NULL)))
 THEN
  --Fix 57617
  IF (:old.PAYEE_CARRIER_ID IS NULL)
  THEN
     l_old_value := NULL;
  ELSE
     SELECT carrier_code
       INTO l_old_value
       FROM CARRIER_CODE
      WHERE carrier_id = :old.PAYEE_CARRIER_ID;
  END IF;
  IF (:new.PAYEE_CARRIER_ID IS NULL)
  THEN
     l_new_value := NULL;
  ELSE
     SELECT carrier_code
       INTO l_new_value
       FROM CARRIER_CODE
      WHERE carrier_id = :new.PAYEE_CARRIER_ID;
  END IF;
  --Fix end 57617
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'PAYEE',
                               l_old_value,
                               l_new_value,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- MOT_ID
 IF (   (:old.MOT_ID <> :new.MOT_ID)
   OR ( (:old.MOT_ID IS NULL) AND (:new.MOT_ID IS NOT NULL))
   OR ( (:new.MOT_ID IS NULL) AND (:old.MOT_ID IS NOT NULL)))
 THEN
  -- fix CR24776
  IF (:old.MOT_ID IS NULL)
  THEN
     l_old_value := NULL;
  ELSE
     SELECT mot
       INTO l_old_value
       FROM MOT
      WHERE mot_id = :old.MOT_ID;
  END IF;
  IF (:new.MOT_ID IS NULL)
  THEN
     l_new_value := NULL;
  ELSE
     SELECT mot
       INTO l_new_value
       FROM MOT
      WHERE mot_id = :new.MOT_ID;
  END IF;
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'MODE',
                               l_old_value,                 --:old.MOT_ID,
                               l_new_value,                  --:new.MOT_ID
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- PROTECTION_LEVEL_ID
 IF ( (:old.PROTECTION_LEVEL_ID <> :new.PROTECTION_LEVEL_ID)
   OR ( (:old.PROTECTION_LEVEL_ID IS NULL)
       AND (:new.PROTECTION_LEVEL_ID IS NOT NULL))
   OR ( (:new.PROTECTION_LEVEL_ID IS NULL)
       AND (:old.PROTECTION_LEVEL_ID IS NOT NULL)))
 THEN
  -- fix CR24776
  IF (:old.PROTECTION_LEVEL_ID IS NULL)
  THEN
     l_old_value := NULL;
  ELSE
     SELECT protection_level
       INTO l_old_value
       FROM PROTECTION_LEVEL
      WHERE protection_level_id = :old.PROTECTION_LEVEL_ID;
  END IF;
  IF (:new.PROTECTION_LEVEL_ID IS NULL)
  THEN
     l_new_value := NULL;
  ELSE
     SELECT protection_level
       INTO l_new_value
       FROM PROTECTION_LEVEL
      WHERE protection_level_id = :new.PROTECTION_LEVEL_ID;
  END IF;
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'PROTECTION LEVEL',
                               l_old_value,   -- :old.PROTECTION_LEVEL_ID,
                               l_new_value,    -- :new.PROTECTION_LEVEL_ID
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- EQUIPMENT_ID
 IF (   (:old.EQUIPMENT_ID <> :new.EQUIPMENT_ID)
   OR ( (:old.EQUIPMENT_ID IS NULL) AND (:new.EQUIPMENT_ID IS NOT NULL))
   OR ( (:new.EQUIPMENT_ID IS NULL) AND (:old.EQUIPMENT_ID IS NOT NULL)))
 THEN
  -- fix CR24776
  IF (:old.EQUIPMENT_ID IS NULL)
  THEN
     l_old_value := NULL;
  ELSE
     SELECT equipment_code
       INTO l_old_value
       FROM EQUIPMENT
      WHERE equipment_id = :old.EQUIPMENT_ID;
  END IF;
  IF (:new.EQUIPMENT_ID IS NULL)
  THEN
     l_new_value := NULL;
  ELSE
     SELECT equipment_code
       INTO l_new_value
       FROM EQUIPMENT
      WHERE equipment_id = :new.EQUIPMENT_ID;
  END IF;
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'EQUIPMENT CODE',
                               l_old_value,          -- :old.EQUIPMENT_ID,
                               l_new_value,           -- :new.EQUIPMENT_ID
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- SERVICE_LEVEL_ID
 IF ( (:old.SERVICE_LEVEL_ID <> :new.SERVICE_LEVEL_ID)
   OR ( (:old.SERVICE_LEVEL_ID IS NULL)
       AND (:new.SERVICE_LEVEL_ID IS NOT NULL))
   OR ( (:new.SERVICE_LEVEL_ID IS NULL)
       AND (:old.SERVICE_LEVEL_ID IS NOT NULL)))
 THEN
  -- fix CR24776
  IF (:old.SERVICE_LEVEL_ID IS NULL)
  THEN
     l_old_value := NULL;
  ELSE
     SELECT service_level
       INTO l_old_value
       FROM SERVICE_LEVEL
      WHERE service_level_id = :old.SERVICE_LEVEL_ID;
  END IF;
  IF (:new.SERVICE_LEVEL_ID IS NULL)
  THEN
     l_new_value := NULL;
  ELSE
     SELECT service_level
       INTO l_new_value
       FROM SERVICE_LEVEL
      WHERE service_level_id = :new.SERVICE_LEVEL_ID;
  END IF;
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'SERVICE LEVEL',
                               l_old_value,      -- :old.SERVICE_LEVEL_ID,
                               l_new_value,       -- :new.SERVICE_LEVEL_ID
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- MINIMUM_SIZE
 IF (   (:old.MINIMUM_SIZE <> :new.MINIMUM_SIZE)
   OR ( (:old.MINIMUM_SIZE IS NULL) AND (:new.MINIMUM_SIZE IS NOT NULL))
   OR ( (:new.MINIMUM_SIZE IS NULL) AND (:old.MINIMUM_SIZE IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'MINIMUM QUANTITY',
                               :old.MINIMUM_SIZE,
                               :new.MINIMUM_SIZE,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- MAXIMUM_SIZE
 IF (   (:old.MAXIMUM_SIZE <> :new.MAXIMUM_SIZE)
   OR ( (:old.MAXIMUM_SIZE IS NULL) AND (:new.MAXIMUM_SIZE IS NOT NULL))
   OR ( (:new.MAXIMUM_SIZE IS NULL) AND (:old.MAXIMUM_SIZE IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'MAXIMUM QUANTITY',
                               :old.MAXIMUM_SIZE,
                               :new.MAXIMUM_SIZE,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- EFFECTIVE_DT
 IF (   (:old.EFFECTIVE_DT <> :new.EFFECTIVE_DT)
   OR ( (:old.EFFECTIVE_DT IS NULL) AND (:new.EFFECTIVE_DT IS NOT NULL))
   OR ( (:new.EFFECTIVE_DT IS NULL) AND (:old.EFFECTIVE_DT IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'EFFECTIVE DATE',
                               to_char(trunc(:old.EFFECTIVE_DT), 'MM/DD/YYYY'),
                               to_char(trunc(:new.EFFECTIVE_DT), 'MM/DD/YYYY'),
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- EXPIRATION_DT
 IF ( (:old.EXPIRATION_DT <> :new.EXPIRATION_DT)
   OR ( (:old.EXPIRATION_DT IS NULL) AND (:new.EXPIRATION_DT IS NOT NULL))
   OR ( (:new.EXPIRATION_DT IS NULL) AND (:old.EXPIRATION_DT IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'EXPIRATION DATE',
                               to_char(trunc(:old.EXPIRATION_DT), 'MM/DD/YYYY'),
                               to_char(trunc(:new.EXPIRATION_DT), 'MM/DD/YYYY'),
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- ZONE_ID
 IF (   (:old.ZONE_ID <> :new.ZONE_ID)
   OR ( (:old.ZONE_ID IS NULL) AND (:new.ZONE_ID IS NOT NULL))
   OR ( (:new.ZONE_ID IS NULL) AND (:old.ZONE_ID IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'ZONE',
                               :old.ZONE_ID,
                               :new.ZONE_ID,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- IS_AUTO_APPROVE
 IF ( (:old.IS_AUTO_APPROVE <> :new.IS_AUTO_APPROVE)
   OR ( (:old.IS_AUTO_APPROVE IS NULL)
       AND (:new.IS_AUTO_APPROVE IS NOT NULL))
   OR ( (:new.IS_AUTO_APPROVE IS NULL)
       AND (:old.IS_AUTO_APPROVE IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'DEFAULT APPROVED',
                               :old.IS_AUTO_APPROVE,
                               :new.IS_AUTO_APPROVE,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- MINIMUM_RATE
 IF (   (:old.MINIMUM_RATE <> :new.MINIMUM_RATE)
   OR ( (:old.MINIMUM_RATE IS NULL) AND (:new.MINIMUM_RATE IS NOT NULL))
   OR ( (:new.MINIMUM_RATE IS NULL) AND (:old.MINIMUM_RATE IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'MINIMUM RATE',
                               :old.MINIMUM_RATE,
                               :new.MINIMUM_RATE,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 --INCOTERM ID
 IF (:old.INCOTERM_ID IS NOT NULL)
 THEN
  SELECT INCOTERM_NAME
    INTO OldIncotermName
    FROM INCOTERM
   WHERE INCOTERM_ID = :old.INCOTERM_ID;
 END IF;
 IF (:new.INCOTERM_ID IS NOT NULL)
 THEN
  SELECT INCOTERM_NAME
    INTO NewIncotermName
    FROM INCOTERM
   WHERE INCOTERM_ID = :new.INCOTERM_ID;
 END IF;
 IF (   (:old.INCOTERM_ID <> :new.INCOTERM_ID)
   OR ( (:old.INCOTERM_ID IS NULL) AND (:new.INCOTERM_ID IS NOT NULL))
   OR ( (:new.INCOTERM_ID IS NULL) AND (:old.INCOTERM_ID IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'INCOTERM',
                               OldIncotermName,        --old INCOTERM_NAME
                               NewIncotermName,        --new INCOTERM_NAME
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- CURRENCY_CODE
 IF ( (:old.CURRENCY_CODE <> :new.CURRENCY_CODE)
   OR ( (:old.CURRENCY_CODE IS NULL) AND (:new.CURRENCY_CODE IS NOT NULL))
   OR ( (:new.CURRENCY_CODE IS NULL) AND (:old.CURRENCY_CODE IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               NULL,
                               :new.ACCESSORIAL_RATE_ID,
                               'CURRENCY CODE',
                               :old.CURRENCY_CODE,
                               :new.CURRENCY_CODE,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
END ACCESSORIAL_RATE_A_IU_TR_1;
/

ALTER TRIGGER ACCESSORIAL_RATE_A_IU_TR_1 ENABLE;

CREATE OR REPLACE TRIGGER ACCESSORIAL_RATE_EDED_BIU_TR
 BEFORE INSERT OR UPDATE OF EFFECTIVE_DT, EXPIRATION_DT
 ON ACCESSORIAL_RATE
 FOR EACH ROW
BEGIN
 IF :NEW.EFFECTIVE_DT > :NEW.EXPIRATION_DT
 THEN
  :NEW.EFFECTIVE_SEQ := :NEW.ACCESSORIAL_RATE_ID;
 ELSE
  :NEW.EFFECTIVE_SEQ := 0;
 END IF;
END;
/

ALTER TRIGGER ACCESSORIAL_RATE_EDED_BIU_TR ENABLE;

CREATE OR REPLACE TRIGGER ACCESSORI_AU_TR1
 before update
 on accessorial_option_group
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER ACCESSORI_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER ACCESS_CONTROL_AID_1
AFTER INSERT OR DELETE
ON ACCESS_CONTROL
REFERENCING
  OLD AS OLD
  NEW AS NEW
FOR EACH ROW
DECLARE
VOLDVALUE VARCHAR2(500);
VNEWVALUE VARCHAR2(500);
ACTION VARCHAR2(10);
CNT NUMBER(4);
BEGIN
	  SELECT COUNT(PARAMETER_VALUE)  INTO CNT
	  FROM COMPANY_PARAM
	  WHERE PARAMETER_NAME='AUDITING_ENABLED' AND COMPANY_ID = -1 AND PARAMETER_VALUE = '1';
	IF (CNT = 1) THEN
	  IF INSERTING THEN
		  ACTION:='INSERT';
			INSERT INTO ACCESS_CONTROL_AUDIT (
			ACCESS_CONTROL_AUDIT_ID,
			  AUDIT_ACTION,
			  USER_NAME,
			  ROLE_NAME,
			  COMPANY_NAME,
			  ACCESS_CONTROL_ID,
			  UCL_USER_ID,
			  ROLE_ID,
			  GEO_REGION_ID,
			  PARTNER_COMPANY_ID,
			  BUSINESS_UNIT_ID,
			  USER_GROUP_ID,
			  IS_INTERNAL_CONTROL,
			  COMPANY_ID,
			  CREATED_DTTM,
			  LAST_UPDATED_DTTM)
			VALUES (
			SEQ_ACCESS_CONTROL_AUDIT_ID.nextval,
			  ACTION,
			  'NOT_USED',
			  'NOT_USED',
			  'NOT_USED',
			  :NEW.ACCESS_CONTROL_ID,
			  :NEW.UCL_USER_ID,
			  :NEW.ROLE_ID,
			  :NEW.GEO_REGION_ID,
			  :NEW.PARTNER_COMPANY_ID,
			  :NEW.BUSINESS_UNIT_ID,
			  :NEW.USER_GROUP_ID,
			  :NEW.IS_INTERNAL_CONTROL,
			  :NEW.COMPANY_ID,
			  :NEW.CREATED_DTTM,
			  :NEW.LAST_UPDATED_DTTM
);
	  ELSIF DELETING  THEN
		   ACTION:='DELETE';
			Insert into ACCESS_CONTROL_AUDIT (
			ACCESS_CONTROL_AUDIT_ID,
			  AUDIT_ACTION,
			  USER_NAME,
			  ROLE_NAME,
			  COMPANY_NAME,
			  ACCESS_CONTROL_ID,
			  UCL_USER_ID,
			  ROLE_ID,
			  GEO_REGION_ID,
			  PARTNER_COMPANY_ID,
			  BUSINESS_UNIT_ID,
			  USER_GROUP_ID,
			  IS_INTERNAL_CONTROL,
			  COMPANY_ID,
			  CREATED_DTTM,
			  LAST_UPDATED_DTTM)
			values (
			SEQ_ACCESS_CONTROL_AUDIT_ID.nextval,
			  ACTION,
			  'NOT_USED',
			  'NOT_USED',
			  'NOT_USED',
			  :OLD.ACCESS_CONTROL_ID,
			  :OLD.UCL_USER_ID,
			  :OLD.ROLE_ID,
			  :OLD.GEO_REGION_ID,
			  :OLD.PARTNER_COMPANY_ID,
			  :OLD.BUSINESS_UNIT_ID,
			  :OLD.USER_GROUP_ID,
			  :OLD.IS_INTERNAL_CONTROL,
			  :OLD.COMPANY_ID,
			  :OLD.CREATED_DTTM,
			  SYSTIMESTAMP);
	  End If;
	END If;
END;
/

ALTER TRIGGER ACCESS_CONTROL_AID_1 ENABLE;

CREATE OR REPLACE TRIGGER ACCESS_GP_LUD_TRG BEFORE
UPDATE ON ACCESSORIAL_GROUP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER ACCESS_GP_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ACCESS_PAR_LUD_TRG BEFORE
UPDATE ON ACCESSORIAL_PARAM_SET FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER ACCESS_PAR_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ACCESS_RAT_LUD_TRG BEFORE
UPDATE ON ACCESSORIAL_RATE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER ACCESS_RAT_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ACCESS_TYP_LUD_TRG BEFORE
UPDATE ON ACCESSORIAL_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER ACCESS_TYP_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ACC_CTRL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON ACCESS_CONTROL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER ACC_CTRL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER ADT_OVR_DT_LUD_TRG BEFORE
UPDATE ON DT_OVERRIDE_DETAIL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER ADT_OVR_DT_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ALTERSESS_TRIG
 AFTER LOGON ON SCHEMA
BEGIN
 EXECUTE IMMEDIATE 'alter session set NLS_TIMESTAMP_FORMAT = ''YY-MM-DD HH24:MI:SS.FF''';
END ALTERSESS_TRIG;
/

ALTER TRIGGER ALTERSESS_TRIG ENABLE;

CREATE OR REPLACE TRIGGER APP_INST_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON APP_INSTANCE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER APP_INST_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON APP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER APP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_MODULE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON APP_MODULE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER APP_MODULE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER APP_MOD_PE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON APP_MOD_PERM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER APP_MOD_PE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER ATTRIBUTE_AU_TR1
 before update
 on attribute_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER ATTRIBUTE_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER AUTO_ACCE_AU_TR1
 before update
 on auto_accept_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER AUTO_ACCE_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER BILLING_M_AU_TR1
 before update
 on billing_method
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER BILLING_M_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER BIND_TYPE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON BINDING_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER BIND_TYPE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER BULK_R_LUD_TRG BEFORE
UPDATE ON BULK_RATING_RULES FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER BULK_R_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER BUND_METDAT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON BUNDLE_META_DATA FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER BUND_METDAT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER BUSINESS__AU_TR1
 before update
 on business_partner_contact
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER BUSINESS__AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER BUSIN_UT_AU_TR1
 before update
 on business_unit
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER BUSIN_UT_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER BUS_PARTTYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON BUSINESS_PARTNER_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER BUS_PARTTYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CARRIER_I_AU_TR1
 before update
 on carrier_ins_dtl
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CARRIER_I_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CARRIE_FAC_LUD_TRG BEFORE
UPDATE ON CARRIER_FACILITY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER CARRIE_FAC_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER CARRIE_LAB_LUD_TRG BEFORE
UPDATE ON CARRIER_LABEL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER CARRIE_LAB_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER CARRIE_ROU_LUD_TRG BEFORE
UPDATE ON CARRIER_ROUTING_CODE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER CARRIE_ROU_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER CARR_CODE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CARRIER_CODE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CARR_CODE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CARR_P_LUD_TRG BEFORE
UPDATE ON CARR_PARAM_ACC_EXCLUDED_LIST FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER CARR_P_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER CAR_CD_CN_AU_TR1
 before update
 on carrier_code_contact
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_CD_CN_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAR_CD_MT_AU_TR1
 before update
 on carrier_code_mot
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_CD_MT_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAR_CD_ST_AU_TR1
 before update
 on carrier_code_status
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_CD_ST_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAR_CT_TP_AU_TR1
 before update
 on carrier_contact_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_CT_TP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAR_MP_CP_AU_TR1
 before update
 on carrier_code_mot_capcomm
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_MP_CP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAR_MT_FT_AU_TR1
 before update
 on carrier_code_mot_facility
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_MT_FT_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAR_RG_DY_AU_TR1
 before update
 on carrier_schdl_reg_day_dtl
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_RG_DY_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAR_SL_DY_AU_TR1
 before update
 on carrier_schdl_reg_day
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_SL_DY_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAR_SL_HO_AU_TR1
 before update
 on carrier_schdl_holiday
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_SL_HO_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAR_TYP_AU_TR1
 before update
 on carrier_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CAR_TYP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CAVWCRTCTRL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CA_VIEWCRITCTRL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CAVWCRTCTRL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CA_DAT_TYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CA_DATA_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CA_DAT_TYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CA_ENT_APL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CA_ENTITY_APPL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CA_ENT_APL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CA_ENT_DEF_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CA_ENTITY_DEF FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CA_ENT_DEF_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CA_ETDEFKY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CA_ENTITY_DEF_KEY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CA_ETDEFKY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CA_PROP_DEF_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CA_PROPERTY_DEF FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CA_PROP_DEF_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CA_PRP_APPL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CA_PROP_APPLICABLE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CA_PRP_APPL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CA_VWMEDA_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CA_VIEWMETADATA FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CA_VWMEDA_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CD_SYSCD_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CD_SYS_CODE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CD_SYSCD_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CHAN_TYPUCL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CHANNEL_TYPE_UCL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CHAN_TYPUCL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CLCRNGRP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_CRON_GROUP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CLCRNGRP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ALERT_TRG
 BEFORE INSERT
 ON CL_ALERT
 FOR EACH ROW
BEGIN
 IF (:NEW.ALERT_ID IS NULL)
 THEN
  SELECT CL_ALERT_SEQ.NEXTVAL INTO :NEW.ALERT_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_ALERT_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_CLUSTER_TRG
 BEFORE INSERT
 ON CL_CLUSTER
 FOR EACH ROW
BEGIN
 IF (:NEW.CLUSTER_ID IS NULL)
 THEN
  SELECT CL_CLUSTER_SEQ.NEXTVAL INTO :NEW.CLUSTER_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_CLUSTER_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_CLUS_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_CLUSTER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_CLUS_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_CONVERSATION_TRG
 BEFORE INSERT
 ON CL_CONVERSATION
 FOR EACH ROW
BEGIN
 IF (:NEW.CONVERSATION_ID IS NULL)
 THEN
  SELECT CL_CONVERSATION_SEQ.NEXTVAL INTO :NEW.CONVERSATION_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_CONVERSATION_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_CONV_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_CONVERSATION FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_CONV_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_CRON_GROUP_TRG
 BEFORE INSERT
 ON CL_CRON_GROUP
 FOR EACH ROW
BEGIN
 IF (:NEW.CRON_GROUP_ID IS NULL)
 THEN
  SELECT CL_CRON_GROUP_SEQ.NEXTVAL INTO :NEW.CRON_GROUP_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_CRON_GROUP_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_CRON_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_CRON FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_CRON_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_CRON_TRG
 BEFORE INSERT
 ON CL_CRON
 FOR EACH ROW
BEGIN
 IF (:NEW.CRON_ID IS NULL)
 THEN
  SELECT CL_CRON_SEQ.NEXTVAL INTO :NEW.CRON_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_CRON_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_DRIVER_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_DRIVER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_DRIVER_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_DRIVER_TRG
 BEFORE INSERT
 ON CL_DRIVER
 FOR EACH ROW
BEGIN
 IF (:NEW.DRIVER_ID IS NULL)
 THEN
  SELECT CL_DRIVER_SEQ.NEXTVAL INTO :NEW.DRIVER_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_DRIVER_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_DVR_INT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_DRIVER_INTERCEPTOR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_DVR_INT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_EDPT_EXT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_ENDPOINT_EXT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_EDPT_EXT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ENDPOINT_QUEUE_TRG BEFORE INSERT ON CL_ENDPOINT_QUEUE FOR EACH ROW
BEGIN
IF (:NEW.ENDPOINT_QUEUE_ID IS NULL) THEN
	SELECT CL_ENDPOINT_QUEUE_SEQ.NEXTVAL INTO :NEW.ENDPOINT_QUEUE_ID
		FROM DUAL;
END IF;
END;
/

ALTER TRIGGER CL_ENDPOINT_QUEUE_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ENDPOINT_TRG
 BEFORE INSERT
 ON CL_ENDPOINT
 FOR EACH ROW
BEGIN
 IF (:NEW.ENDPOINT_ID IS NULL)
 THEN
  SELECT CL_ENDPOINT_SEQ.NEXTVAL INTO :NEW.ENDPOINT_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_ENDPOINT_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ENDPT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_ENDPOINT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_ENDPT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ENGINE_GROUP_TRG
 BEFORE INSERT
 ON CL_ENGINE_GROUP
 FOR EACH ROW
BEGIN
 IF (:NEW.ENGINE_GROUP_ID IS NULL)
 THEN
  SELECT CL_ENGINE_GROUP_SEQ.NEXTVAL INTO :NEW.ENGINE_GROUP_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_ENGINE_GROUP_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ENGINE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_ENGINE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_ENGINE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ENGINE_TRG
 BEFORE INSERT
 ON CL_ENGINE
 FOR EACH ROW
BEGIN
 IF (:NEW.ENGINE_ID IS NULL)
 THEN
  SELECT CL_ENGINE_SEQ.NEXTVAL INTO :NEW.ENGINE_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_ENGINE_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ENGRPXRE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_ENGINE_GROUP_XREF FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_ENGRPXRE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ENG_CONF_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_ENGINE_CONFIG FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_ENG_CONF_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_ENG_GRP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_ENGINE_GROUP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_ENG_GRP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_EVNTMAST_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_EVENT_MASTER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_EVNTMAST_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_EVNTSUBS_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_EVENT_SUBSCRIPTION FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_EVNTSUBS_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_EVNT_PUB_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_EVENT_PUBLICATION FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_EVNT_PUB_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_EXTDCONF_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_EXTENDED_CONFIGURATION FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_EXTDCONF_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_EXTENDED_CONFIGURATION_TRG
 BEFORE INSERT
 ON CL_EXTENDED_CONFIGURATION
 FOR EACH ROW
BEGIN
 IF (:NEW.CONFIG_ID IS NULL)
 THEN
  SELECT CL_EXTENDED_CONFIGURATION_SEQ.NEXTVAL
    INTO :NEW.CONFIG_ID
    FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_EXTENDED_CONFIGURATION_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_FLOVRTRK_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_FAILOVER_TRACKING FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_FLOVRTRK_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_HTTP_GROUP_TRG
 BEFORE INSERT
 ON CL_HTTP_GROUP
 FOR EACH ROW
BEGIN
 IF (:NEW.HTTP_GROUP_ID IS NULL)
 THEN
  SELECT CL_HTTP_GROUP_SEQ.NEXTVAL INTO :NEW.HTTP_GROUP_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_HTTP_GROUP_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_HTTP_GRP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_HTTP_GROUP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_HTTP_GRP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_HTTP_HDR_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_HTTP_HEADER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_HTTP_HDR_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_HTTP_HEADER_TRG
 BEFORE INSERT
 ON CL_HTTP_HEADER
 FOR EACH ROW
BEGIN
 IF (:NEW.HTTP_HEADER_ID IS NULL)
 THEN
  SELECT CL_HTTP_HEADER_SEQ.NEXTVAL INTO :NEW.HTTP_HEADER_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_HTTP_HEADER_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_INTERCEPTOR_TRG
 BEFORE INSERT
 ON CL_INTERCEPTOR
 FOR EACH ROW
BEGIN
 IF (:NEW.INTERCEPTOR_ID IS NULL)
 THEN
  SELECT CL_INTERCEPTOR_SEQ.NEXTVAL INTO :NEW.INTERCEPTOR_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_INTERCEPTOR_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_INTER_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_INTERCEPTOR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_INTER_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_MACHINE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_MACHINE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_MACHINE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_MACHINE_TRG
 BEFORE INSERT
 ON CL_MACHINE
 FOR EACH ROW
BEGIN
 IF (:NEW.MACHINE_ID IS NULL)
 THEN
  SELECT CL_MACHINE_SEQ.NEXTVAL INTO :NEW.MACHINE_ID FROM DUAL;
 END IF;
END;
/

ALTER TRIGGER CL_MACHINE_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_MACHXREF_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_MACHINE_XREF FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_MACHXREF_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_MESG_KEY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_MESSAGE_KEYS FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_MESG_KEY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_MESSAGE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_MESSAGE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_MESSAGE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_PROTTCPX_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_PROTOCOL_TCPMUX FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_PROTTCPX_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_PRTCL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_PROTOCOL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_PRTCL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_PRTFILE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_PROTOCOL_FILE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_PRTFILE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_PRT_FTP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_PROTOCOL_FTP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_PRT_FTP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_PRT_HTTP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_PROTOCOL_HTTP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_PRT_HTTP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_PRT_MQS_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_PROTOCOL_MQS FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_PRT_MQS_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_PRT_RFID_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_PROTOCOL_RFID1 FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_PRT_RFID_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_PRT_TCP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_PROTOCOL_TCP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_PRT_TCP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_PRT_UDP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_PROTOCOL_UDP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_PRT_UDP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_SEQ_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_SEQUENCE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_SEQ_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_SERVCONF_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_SERVER_CONFIG FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_SERVCONF_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CL_SERVTYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CL_SERVER_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CL_SERVTYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CM_RAT_LUD_TRG BEFORE
UPDATE ON CM_RATE_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER CM_RAT_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER CM_RT_DSC_A_U_TR_1
 AFTER INSERT OR UPDATE
 ON CM_RATE_DISCOUNT
 FOR EACH ROW
DECLARE
 l_old_value   CARRIER_PARAMETER_EVENT.OLD_VALUE%TYPE;
 l_new_value   CARRIER_PARAMETER_EVENT.NEW_VALUE%TYPE;
 l_chg_flag    NUMBER (1) := 0;
 l_time_now    DATE := SYSDATE;
BEGIN
 -- START_NUMBER
 IF (   (:old.START_NUMBER <> :new.START_NUMBER)
   OR ( (:old.START_NUMBER IS NULL) AND (:new.START_NUMBER IS NOT NULL))
   OR ( (:new.START_NUMBER IS NULL) AND (:old.START_NUMBER IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               :new.CM_RATE_DISCOUNT_ID,
                               NULL,
                               NULL,
                               'START DISTANCE',
                               :old.START_NUMBER,
                               :new.START_NUMBER,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- END_NUMBER
 IF (   (:old.END_NUMBER <> :new.END_NUMBER)
   OR ( (:old.END_NUMBER IS NULL) AND (:new.END_NUMBER IS NOT NULL))
   OR ( (:new.END_NUMBER IS NULL) AND (:old.END_NUMBER IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               :new.CM_RATE_DISCOUNT_ID,
                               NULL,
                               NULL,
                               'END DISTANCE',
                               :old.END_NUMBER,
                               :new.END_NUMBER,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- VALUE
 IF (   (:old.VALUE <> :new.VALUE)
   OR ( (:old.VALUE IS NULL) AND (:new.VALUE IS NOT NULL))
   OR ( (:new.VALUE IS NULL) AND (:old.VALUE IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               :new.CM_RATE_DISCOUNT_ID,
                               NULL,
                               NULL,
                               'RATE',
                               :old.VALUE,
                               :new.VALUE,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
END CM_RT_DSC_A_U_TR_1;
/

ALTER TRIGGER CM_RT_DSC_A_U_TR_1 ENABLE;

CREATE OR REPLACE TRIGGER COMMODITY_AU_TR1
 before update
 on commodity_class
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER COMMODITY_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER COMMOD_LUD_TRG BEFORE
UPDATE ON COMMODITY_CLASS_TIER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER COMMOD_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMM_METH_AU_TR1
 before update
 on comm_method
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER COMM_METH_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER COMM_METUCL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COMM_METHOD_UCL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COMM_METUCL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_AI_1
 AFTER INSERT
 ON COMPANY
 REFERENCING NEW AS NEW
 FOR EACH ROW
DECLARE
BEGIN
 IF (:NEW.PARENT_COMPANY_ID = -1 AND :NEW.COMPANY_ID NOT IN (0, -1, -2))
 THEN
  FOR REC IN (SELECT ROLE_ID
                FROM ROLE
               WHERE COMPANY_ID = -1)
  LOOP
     INSERT INTO ROLE (ROLE_ID,
                       COMPANY_ID,
                       ROLE_NAME,
                       IS_ROLE_PRIVATE,
                       IS_ACTIVE,
                       CREATED_SOURCE_TYPE_ID,
                       CREATED_SOURCE,
                       CREATED_DTTM,
                       LAST_UPDATED_SOURCE_TYPE_ID,
                       LAST_UPDATED_SOURCE,
                       LAST_UPDATED_DTTM,
                       HIBERNATE_VERSION,
                       APPLY_TO_BUSINESS_PARTNERS,
                       ROLE_TYPE_ID)
        (SELECT SEQ_ROLE_ID.NEXTVAL,
                :NEW.COMPANY_ID,
                ROLE_NAME,
                IS_ROLE_PRIVATE,
                IS_ACTIVE,
                CREATED_SOURCE_TYPE_ID,
                CREATED_SOURCE,
                SYSDATE,
                LAST_UPDATED_SOURCE_TYPE_ID,
                LAST_UPDATED_SOURCE,
                SYSDATE,
                HIBERNATE_VERSION,
                APPLY_TO_BUSINESS_PARTNERS,
                ROLE_TYPE_ID
           FROM ROLE
          WHERE COMPANY_ID = -1 AND ROLE_ID = REC.ROLE_ID);
     INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                                    APP_ID,
                                    MODULE_ID,
                                    PERMISSION_ID,
                                    ROW_UID)
        (SELECT SEQ_ROLE_ID.CURRVAL,
                RAMP.APP_ID,
                RAMP.MODULE_ID,
                RAMP.PERMISSION_ID,
                SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
           FROM ROLE_APP_MOD_PERM RAMP, ROLE
          WHERE     ROLE.ROLE_ID = RAMP.ROLE_ID
                AND ROLE.COMPANY_ID = -1
                AND RAMP.ROLE_ID = REC.ROLE_ID);
  END LOOP;
 END IF;
END;
/

ALTER TRIGGER COMPANY_AI_1 ENABLE;

CREATE OR REPLACE TRIGGER COMPANY_AI_2
 AFTER INSERT
 ON COMPANY
 REFERENCING NEW AS NEW
 FOR EACH ROW
DECLARE
BEGIN
 INSERT INTO DFLT_FACILITY (COMPANY_ID)
    VALUES (:NEW.COMPANY_ID);
END;
/

ALTER TRIGGER COMPANY_AI_2 ENABLE;

CREATE OR REPLACE TRIGGER COMP_APPMOD_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COMPANY_APP_MODULE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COMP_APPMOD_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMP_APP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COMPANY_APP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COMP_APP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMP_DTL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COMPANY_DETAIL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COMP_DTL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMP_MAP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COMPANY_MAPPING FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COMP_MAP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMP_PARAM_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COMPANY_PARAMETER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COMP_PARAM_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMP_PARA_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COMPANY_PARAM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COMP_PARA_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMP_TYPPER_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COMPANY_TYPE_PERM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COMP_TYPPER_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER COMP_TYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COMPANY_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COMP_TYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER COM_CD_CP_AU_TR1
 before update
 on comm_code_chapter
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER COM_CD_CP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER COM_CD_SC_AU_TR1
 before update
 on comm_code_section
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER COM_CD_SC_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER COUNTRY_C_AU_TR1
 before update
 on country_credit_limit
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER COUNTRY_C_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER COUNTRY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON COUNTRY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER COUNTRY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CUST_ST_AU_TR1
 before update
 on customer_status
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER CUST_ST_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER CUST_VWTYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CUSTOMIZE_VIEW_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CUST_VWTYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER CUST_VWUIAR_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON CUSTOM_VIEW_UIATTR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER CUST_VWUIAR_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER DATA_TYPE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON DATA_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER DATA_TYPE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER DAYOFWEEK_AU_TR1
 before update
 on dayofweek
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DAYOFWEEK_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DCK_HRS_CAPUSD_TRG
 BEFORE INSERT
 ON DOCK_HOURS_CAP_USED
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
WHEN (
NEW.DOCK_HOURS_CAP_USED_ID IS NULL
  ) BEGIN
 SELECT SEQ_DOCK_HOURS_CAP_USED_ID.NEXTVAL
 INTO :NEW.DOCK_HOURS_CAP_USED_ID
 FROM DUAL;
END;
/

ALTER TRIGGER DCK_HRS_CAPUSD_TRG ENABLE;

CREATE OR REPLACE TRIGGER DCK_HR_AU_TR1
 before update
 on dock_hours
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DCK_HR_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DCK_HR_CP_AU_TR1
 before update
 on dock_hours_cap_used
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DCK_HR_CP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DFLT_DOCK_AU_TR1
 before update
 on dflt_dock_hours
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DFLT_DOCK_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DISTANCE__AU_TR1
 before update
 on distance_uom
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DISTANCE__AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DOCK_ACTI_AU_TR1
 before update
 on dock_action_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DOCK_ACTI_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DOCK_DISP_AU_TR1
 before update
 on dock_dispatch_capacity
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DOCK_DISP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DOCK_RTE_AU_TR1
 before update
 on dock_rate
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DOCK_RTE_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DOCK_SCHD_AU_TR1
 before update
 on dock_schdl_cap
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DOCK_SCHD_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DOCK_SCHE_AU_TR1
 before update
 on dock_schedule_permission
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DOCK_SCHE_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DOCK_SLCP_AU_TR1
 before update
 on dock_schdl_cap_dtl
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER DOCK_SLCP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER DSHBRDPORT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON DSHBRD_PORTLET FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER DSHBRDPORT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER DS_DSRCT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON DS_DATA_SOURCE_TYPE FOR EACH ROW BEGIN :NEW.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER DS_DSRCT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER DS_DSRC_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON DS_DATA_SOURCE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER DS_DSRC_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER DT_OVERRIDE_BIU_TR
 BEFORE INSERT OR UPDATE
 ON DT_OVERRIDE
 FOR EACH ROW
BEGIN
 :NEW.ORIG := UPPER (:NEW.ORIG);
 :NEW.DEST := UPPER (:NEW.DEST);
END;
/

ALTER TRIGGER DT_OVERRIDE_BIU_TR ENABLE;

CREATE OR REPLACE TRIGGER DT_OVERRIDE_B_I_TR_1
 BEFORE INSERT
 ON DT_OVERRIDE
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
 SELECT NVL (MAX (OVERRIDE_SEQ), 0) + 1
 INTO :NEW.OVERRIDE_SEQ
 FROM DT_OVERRIDE
WHERE COMPANY_ID = :NEW.COMPANY_ID;
END;
/

ALTER TRIGGER DT_OVERRIDE_B_I_TR_1 ENABLE;

CREATE OR REPLACE TRIGGER DT_OVRIDE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON DT_OVERRIDE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER DT_OVRIDE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER DT_OVRIDTL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON DT_OVERRIDE_DETAIL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER DT_OVRIDTL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER DT_OVRIDWIN_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON DT_OVERRIDE_WINDOW FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER DT_OVRIDWIN_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER DWM_WORK_REQUEST_BU_TRG
BEFORE UPDATE
 ON DWM_WORK_REQUEST
REFERENCING NEW AS NEW
 FOR EACH ROW
BEGIN
	if (:NEW.STATUS = 'In Progress')
	THEN
		:NEW.EXECUTION_START_DTTM :=SYSTIMESTAMP;
	ELSE
		:NEW.EXECUTION_END_DTTM := SYSTIMESTAMP;
	END IF;
END;
/

ALTER TRIGGER DWM_WORK_REQUEST_BU_TRG ENABLE;

CREATE OR REPLACE TRIGGER EQP_AVAIL_AU_TR1
 before update
 on equipment_availability
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER EQP_AVAIL_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER EQP_CONFG_AU_TR1
 before update
 on equipment_config
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER EQP_CONFG_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER EQP_COST_AU_TR1
 before update
 on equipment_cost
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER EQP_COST_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER EQP_DOLE_AU_TR1
 before update
 on equipment_domicile
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER EQP_DOLE_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER EQP_SIZE_AU_TR1
 before update
 on equipment_size
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER EQP_SIZE_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER EQP_TYPE_AU_TR1
 before update
 on equipment_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER EQP_TYPE_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER EQUIP_DOO_AU_TR1
 before update
 on equip_door_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER EQUIP_DOO_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FACILIT_YRD_LST_UPD_DTTM_TRG
 BEFORE UPDATE
 ON FACILITY_YARD
 FOR EACH ROW
BEGIN
 :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

ALTER TRIGGER FACILIT_YRD_LST_UPD_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FACILIT_YRD_ZONE_LST_UPD_D_TRG
 BEFORE UPDATE
 ON FACILITY_YARD_ZONE
 FOR EACH ROW
BEGIN
 :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

ALTER TRIGGER FACILIT_YRD_ZONE_LST_UPD_D_TRG ENABLE;

CREATE OR REPLACE TRIGGER FACTY_AV_AU_TR1
 before update
 on facility_availability
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FACTY_AV_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_CLEXC_AU_TR1
 before update
 on fac_shp_cal_schdl_exc
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_CLEXC_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_CLSDL_AU_TR1
 before update
 on fac_shp_cal_schdl
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_CLSDL_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_CNTYP_AU_TR1
 before update
 on facility_contact_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_CNTYP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_LDATB_AU_TR1
 before update
 on facility_load_unload_attrib
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_LDATB_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_PGCST_AU_TR1
 before update
 on facility_package_handling_cost
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_PGCST_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_PPHLG_AU_TR1
 before update
 on facility_pp_handling
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_PPHLG_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_REGIO_AU_TR1
 before update
 on fac_region
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_REGIO_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_RGDY_AU_TR1
 before update
 on fac_schdl_reg_day_dtl
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_RGDY_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_RSAEA_AU_TR1
 before update
 on facility_rs_area
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_RSAEA_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_SDLRG_AU_TR1
 before update
 on fac_schdl_reg_day
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_SDLRG_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_SPCAL_AU_TR1
 before update
 on facility_shipping_calendar
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_SPCAL_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_STOP__AU_TR1
 before update
 on fac_stop_bit
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_STOP__AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_TO_FA_AU_TR1
 before update
 on fac_to_fac_schedule
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_TO_FA_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_XDHG_AU_TR1
 before update
 on facility_xdock_handling
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_XDHG_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_XDSRT_AU_TR1
 before update
 on facility_xdock_sort
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_XDSRT_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FAC_YRDZN_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FACILITY_YARD_ZONE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FAC_YRDZN_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FAC_YRD_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FACILITY_YARD FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FAC_YRD_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FAC_ZONE_AU_TR1
 before update
 on facility_zone
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FAC_ZONE_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FEASIBILIT_DTL_LST_UPD_D_TRG BEFORE
UPDATE ON FEASIBILITY_DTL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER FEASIBILIT_DTL_LST_UPD_D_TRG ENABLE;

CREATE OR REPLACE TRIGGER FEASIBILIT_LST_UPD_DTTM_TRG BEFORE
UPDATE ON FEASIBILITY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER FEASIBILIT_LST_UPD_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FEAS_DTL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FEASIBILITY_DTL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FEAS_DTL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FEAS_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FEASIBILITY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FEAS_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FILORDLYT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER_ORDERBY_LAYOUT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FILORDLYT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FILTER_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FILTER_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FIL_DTL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER_DETAIL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FIL_DTL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FIL_GRBYDTL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER_GROUPBY_DETAIL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FIL_GRBYDTL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FIL_GRBYLY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER_GROUPBY_LAYOUT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FIL_GRBYLY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FIL_GRPDEF_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER_GROUP_DEFINITION FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FIL_GRPDEF_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FIL_LAY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER_LAYOUT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FIL_LAY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FIL_OBJ_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER_OBJECT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FIL_OBJ_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FIL_ORDDET_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER_ORDERBY_DETAIL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FIL_ORDDET_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FIL_SCRTY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON FILTER_SCREEN_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER FIL_SCRTY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER FREQUENCY_AU_TR1
 before update
 on frequency
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER FREQUENCY_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER FUEL_I_LUD_TRG BEFORE
UPDATE ON FUEL_INDEX FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER FUEL_I_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER FUEL_R_LUD_TRG BEFORE
UPDATE ON FUEL_RATE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER FUEL_R_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER FUEL_S_LUD_TRG BEFORE
UPDATE ON FUEL_SURCHARGE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER FUEL_S_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER HANDLER_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON HANDLER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER HANDLER_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER ILM_DYNAM_AU_TR1
 before update
 on ilm_dynamic_group
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER ILM_DYNAM_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER ILM_OBJEC_AU_TR1
 before update
 on ilm_object_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER ILM_OBJEC_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER ILM_STATU_AU_TR1
 before update
 on ilm_status
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER ILM_STATU_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER ILS_OBJTY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON ILS_OBJECT_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER ILS_OBJTY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER ILS_PRTY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON ILS_PRODUCT_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER ILS_PRTY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER IMPORT_PATH_SET_BUI_TR before
INSERT OR
UPDATE ON IMPORT_PATH_SET FOR EACH ROW
BEGIN :NEW.O_SEARCH_LOCATION := LANE_LOCATION_PKG.FnGetLocationString ( :NEW.O_LOC_TYPE, --aLocType
:NEW.O_FACILITY_ID,                                                                                                   --aFacilityId
:NEW.O_CITY        ,                                                                                                  --aCity
:NEW.O_COUNTY       ,                                                                                                 --aCounty
:NEW.O_STATE_PROV    ,                                                                                                --aStateProv
:NEW.O_POSTAL_CODE    ,                                                                                               --aPostalCode
:NEW.O_COUNTRY_CODE    ,                                                                                              --aCountryCode
:NEW.O_ZONE_ID                                                                                                       --aZoneId
);
:NEW.D_SEARCH_LOCATION := LANE_LOCATION_PKG.FnGetLocationString ( :NEW.D_LOC_TYPE, --aLocType
:NEW.D_FACILITY_ID,                                                               --aFacilityId
:NEW.D_CITY        ,                                                              --aCity
:NEW.D_COUNTY       ,                                                             --aCounty
:NEW.D_STATE_PROV    ,                                                            --aStateProv
:NEW.D_POSTAL_CODE    ,                                                           --aPostalCode
:NEW.D_COUNTRY_CODE    ,                                                          --aCountryCode
:NEW.D_ZONE_ID                                                                   --aZoneId
);
END ;
/

ALTER TRIGGER IMPORT_PATH_SET_BUI_TR ENABLE;

CREATE OR REPLACE TRIGGER INCOTERM_AU_TR1
 before update
 on incoterm
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER INCOTERM_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER INCOTERM__AU_TR1
 before update
 on incoterm_map
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER INCOTERM__AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER INC_ACCS__AU_TR1
 before update
 on incoterm_accessorial
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER INC_ACCS__AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER INFEASIBI_AU_TR1
 before update
 on infeasibility
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER INFEASIBI_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER INFEA_TYP_AU_TR1
 before update
 on infeasibility_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER INFEA_TYP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER LABEL_AI_TR_1
AFTER INSERT OR UPDATE ON LABEL
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
 v_chk number(1);
 v_screen_type_id NUMBER(4);
BEGIN
	SELECT DEF_SCREEN_TYPE_ID INTO v_screen_type_id FROM BUNDLE_META_DATA WHERE BUNDLE_NAME = :NEW.BUNDLE_NAME;
	SELECT COUNT(1) INTO v_chk FROM LABEL_DISPLAY WHERE DISP_KEY = :NEW.VALUE AND TO_LANG = 'en' AND SCREEN_TYPE_ID = v_screen_type_id;
	IF (v_chk = 0) THEN
		INSERT INTO LABEL_DISPLAY (LABEL_DISPLAY_ID, DISP_KEY, DISP_VALUE, TO_LANG, SCREEN_TYPE_ID, CREATED_SOURCE, LAST_UPDATED_SOURCE,BU_ID)
		VALUES (SEQ_LABEL_DISPLAY_ID.NEXTVAL, :NEW.VALUE, :NEW.VALUE, 'en', v_screen_type_id, :NEW.BUNDLE_NAME, :NEW.BUNDLE_NAME,-1);
	ELSE
		UPDATE LABEL_DISPLAY SET DISP_VALUE = :NEW.VALUE, LAST_UPDATED_DTTM = SYSTIMESTAMP, LAST_UPDATED_SOURCE = :NEW.BUNDLE_NAME
		WHERE DISP_KEY = :NEW.VALUE AND TO_LANG = 'en' AND SCREEN_TYPE_ID = v_screen_type_id;
	END IF;
END;
/

ALTER TRIGGER LABEL_AI_TR_1 ENABLE;

CREATE OR REPLACE TRIGGER LABEL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LABEL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LABEL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LANE_A_LUD_TRG BEFORE
UPDATE ON LANE_ACC_EXCLUDED_LIST FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER LANE_A_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER LANE_S_LUD_TRG BEFORE
UPDATE ON LANE_STATUS FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER LANE_S_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER LANG_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LANGUAGE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LANG_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LEMA_DOMGRP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LEMA_DOMAINGROUP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LEMA_DOMGRP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LEMA_RCRAV_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LEMA_RULE_CONF_RES_ATTR_VAL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LEMA_RCRAV_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LEMA_RCRA_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LEMA_RULE_CONF_RES_ATTR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LEMA_RCRA_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LEMA_RLSET_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LEMA_RULESET FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LEMA_RLSET_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LEMA_RTEM_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LEMA_RULE_TEMPLATE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LEMA_RTEM_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LEMA_RULE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LEMA_RULE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LEMA_RULE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LOCALE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LOCALE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LOCALE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LOCATION_AU_TR1
 before update
 on location
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER LOCATION_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER LOCN_HDR_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LOCN_HDR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LOCN_HDR_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_EVENT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_EVENT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_EVENT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_PRTQDES_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_PRT_QUEUE_DEST FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_PRTQDES_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_PRTQSER_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_PRT_QUEUE_SERVICE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_PRTQSER_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_PRTQTYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_PRT_Q_TYPES FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_PRTQTYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_PRTQ_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_PRT_QUEUE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_PRTQ_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_PRTREQ_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_PRT_REQUESTOR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_PRTREQ_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_RDLD_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_REPORT_DEF_LAYOUT_DTL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_RDLD_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_REPDEF_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_REPORT_DEF FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_REPDEF_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_REPDELY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_REPORT_DEF_LAYOUT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_REPDELY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_REP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_REPORT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_REP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_RPVAL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_REPORT_PARAM_VALUE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_RPVAL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_RSP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_REP_SCHD_PARAM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_RSP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_SCEVR_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_SCHED_EVENT_REPORT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_SCEVR_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER LRF_SERVER_AU_T
 AFTER UPDATE
 ON LRF_SERVER
 FOR EACH ROW
BEGIN
 IF ( (:OLD.SERVER_STATUS <> :NEW.SERVER_STATUS)
   OR ( (:OLD.SERVER_STATUS IS NULL) AND (:NEW.SERVER_STATUS IS NOT NULL))
   OR ( (:OLD.SERVER_STATUS IS NOT NULL) AND (:NEW.SERVER_STATUS IS NULL)))
 THEN
  INSERT INTO LRF_SERVER_AUDIT (LRF_SERVER_AUDIT_ID,
                                LRF_SERVER_NAME,
                                FIELD_NAME,
                                LAST_UPDATED_SOURCE,
                                OLD_VALUE,
                                NEW_VALUE,
                                LAST_UPDATED_DTTM)
       VALUES (LRF_SERVER_AUDIT_SEQ.NEXTVAL,
               :NEW.APP_INST_SHORT_NAME,
               'SERVER_STATUS',
               :NEW.LAST_UPDATED_SOURCE,
               :OLD.SERVER_STATUS,
               :NEW.SERVER_STATUS,
               :NEW.LAST_UPDATED_DTTM);
 END IF;
END;
/

ALTER TRIGGER LRF_SERVER_AU_T ENABLE;

CREATE OR REPLACE TRIGGER LRF_SERVER_CONFIG_AU_T
 AFTER UPDATE
 ON LRF_SERVER_CONFIG
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
 IF (:OLD.GROUP_ID <> :NEW.GROUP_ID)
 THEN
  INSERT INTO LRF_SERVER_CONFIG_AUDIT (LRF_SERVER_CONFIG_AUDIT_ID,
                                       LRF_SERVER_CONFIG_ID,
                                       FIELD_NAME,
                                       LAST_UPDATED_SOURCE,
                                       OLD_VALUE,
                                       NEW_VALUE,
                                       LAST_UPDATED_DTTM)
       VALUES (LRF_SERVER_CONFIG_AUDIT_SEQ.NEXTVAL,
               :NEW.LRF_SERVER_CONFIG_ID,
               'GROUP_ID',
               :NEW.LAST_UPDATED_SOURCE,
               TO_CHAR (:OLD.GROUP_ID),
               TO_CHAR (:NEW.GROUP_ID),
               :NEW.LAST_UPDATED_DTTM);
 END IF;
 IF (:OLD.FETCH_FREQ <> :NEW.FETCH_FREQ)
 THEN
  INSERT INTO LRF_SERVER_CONFIG_AUDIT (LRF_SERVER_CONFIG_AUDIT_ID,
                                       LRF_SERVER_CONFIG_ID,
                                       FIELD_NAME,
                                       LAST_UPDATED_SOURCE,
                                       OLD_VALUE,
                                       NEW_VALUE,
                                       LAST_UPDATED_DTTM)
       VALUES (LRF_SERVER_CONFIG_AUDIT_SEQ.NEXTVAL,
               :NEW.LRF_SERVER_CONFIG_ID,
               'FETCH_FREQ',
               :NEW.LAST_UPDATED_SOURCE,
               TO_CHAR (:OLD.FETCH_FREQ),
               TO_CHAR (:NEW.FETCH_FREQ),
               :NEW.LAST_UPDATED_DTTM);
 END IF;
 IF (:OLD.DEST_QUEUE_NAME <> :NEW.DEST_QUEUE_NAME)
 THEN
  INSERT INTO LRF_SERVER_CONFIG_AUDIT (LRF_SERVER_CONFIG_AUDIT_ID,
                                       LRF_SERVER_CONFIG_ID,
                                       FIELD_NAME,
                                       LAST_UPDATED_SOURCE,
                                       OLD_VALUE,
                                       NEW_VALUE,
                                       LAST_UPDATED_DTTM)
       VALUES (LRF_SERVER_CONFIG_AUDIT_SEQ.NEXTVAL,
               :NEW.LRF_SERVER_CONFIG_ID,
               'DEST_QUEUE_NAME',
               :NEW.LAST_UPDATED_SOURCE,
               :OLD.DEST_QUEUE_NAME,
               :NEW.DEST_QUEUE_NAME,
               :NEW.LAST_UPDATED_DTTM);
 END IF;
 IF (   (:OLD.ORIG_SERVER <> :NEW.ORIG_SERVER)
   OR ( (:OLD.ORIG_SERVER IS NULL) AND (:NEW.ORIG_SERVER IS NOT NULL))
   OR ( (:OLD.ORIG_SERVER IS NOT NULL) AND (:NEW.ORIG_SERVER IS NULL)))
 THEN
  INSERT INTO LRF_SERVER_CONFIG_AUDIT (LRF_SERVER_CONFIG_AUDIT_ID,
                                       LRF_SERVER_CONFIG_ID,
                                       FIELD_NAME,
                                       LAST_UPDATED_SOURCE,
                                       OLD_VALUE,
                                       NEW_VALUE,
                                       LAST_UPDATED_DTTM)
       VALUES (LRF_SERVER_CONFIG_AUDIT_SEQ.NEXTVAL,
               :NEW.LRF_SERVER_CONFIG_ID,
               'ORIG_SERVER',
               :NEW.LAST_UPDATED_SOURCE,
               :OLD.ORIG_SERVER,
               :NEW.ORIG_SERVER,
               :NEW.LAST_UPDATED_DTTM);
 END IF;
 IF ( (:OLD.CURRENT_SERVER <> :NEW.CURRENT_SERVER)
   OR ( (:OLD.CURRENT_SERVER IS NULL)
       AND (:NEW.CURRENT_SERVER IS NOT NULL))
   OR ( (:OLD.CURRENT_SERVER IS NOT NULL)
       AND (:NEW.CURRENT_SERVER IS NULL)))
 THEN
  INSERT INTO LRF_SERVER_CONFIG_AUDIT (LRF_SERVER_CONFIG_AUDIT_ID,
                                       LRF_SERVER_CONFIG_ID,
                                       FIELD_NAME,
                                       LAST_UPDATED_SOURCE,
                                       OLD_VALUE,
                                       NEW_VALUE,
                                       LAST_UPDATED_DTTM)
       VALUES (LRF_SERVER_CONFIG_AUDIT_SEQ.NEXTVAL,
               :NEW.LRF_SERVER_CONFIG_ID,
               'CURRENT_SERVER',
               :NEW.LAST_UPDATED_SOURCE,
               :OLD.CURRENT_SERVER,
               :NEW.CURRENT_SERVER,
               :NEW.LAST_UPDATED_DTTM);
 END IF;
END;
/

ALTER TRIGGER LRF_SERVER_CONFIG_AU_T ENABLE;

CREATE OR REPLACE TRIGGER LRF_USRPRO_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON LRF_USER_PRO FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER LRF_USRPRO_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER MERCHANDI_AU_TR1
 before update
 on merchandizing_department
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER MERCHANDI_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER MESG_MAS_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON MESSAGE_MASTER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER MESG_MAS_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER MESSAGE_MASTER_AI_TR_1
AFTER INSERT OR UPDATE ON MESSAGE_MASTER
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
 v_chk number(1);
 v_screen_type_id NUMBER(4);
BEGIN
SELECT DEF_SCREEN_TYPE_ID INTO v_screen_type_id FROM BUNDLE_META_DATA WHERE BUNDLE_NAME = :NEW.BUNDLE_NAME;
SELECT COUNT(1) INTO v_chk FROM MESSAGE_DISPLAY WHERE DISP_KEY = :NEW.KEY AND TO_LANG = 'en' AND SCREEN_TYPE_ID = v_screen_type_id;
IF (v_chk = 0) THEN
    INSERT INTO MESSAGE_DISPLAY (MESSAGE_DISPLAY_ID, DISP_KEY, DISP_VALUE, TO_LANG, SCREEN_TYPE_ID, CREATED_SOURCE, LAST_UPDATED_SOURCE)
    VALUES (SEQ_MESSAGE_DISPLAY_ID.NEXTVAL, :NEW.KEY, :NEW.MSG, 'en', v_screen_type_id, :NEW.BUNDLE_NAME, :NEW.BUNDLE_NAME);
ELSIF (:OLD.MSG <> :NEW.MSG) THEN
    UPDATE MESSAGE_DISPLAY SET DISP_VALUE = :NEW.MSG, LAST_UPDATED_DTTM = SYSTIMESTAMP, LAST_UPDATED_SOURCE = :NEW.BUNDLE_NAME
    WHERE DISP_KEY = :NEW.KEY AND TO_LANG = 'en' AND SCREEN_TYPE_ID = v_screen_type_id;
END IF;
END;
/

ALTER TRIGGER MESSAGE_MASTER_AI_TR_1 ENABLE;

CREATE OR REPLACE TRIGGER MIN_DENSITY_PARAM_B_I_TR_1
 BEFORE INSERT
 ON MIN_DENSITY_PARAM
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE
BEGIN
 SELECT SEQ_MIN_DENSITY_PARAM_ID.NEXTVAL
 INTO :NEW.MIN_DENSITY_PARAM_ID
 FROM DUAL;
END;
/

ALTER TRIGGER MIN_DENSITY_PARAM_B_I_TR_1 ENABLE;

CREATE OR REPLACE TRIGGER MIN_DE_LUD_TRG BEFORE
UPDATE ON MIN_DENSITY_PARAM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER MIN_DE_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER MOD_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON MODULE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER MOD_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER MOT_AU_TR1
 before update
 on mot
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER MOT_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER NAV_AMP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON NAVIGATION_APP_MOD_PERM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER NAV_AMP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER NAV_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON NAVIGATION FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER NAV_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER NAV_PAR_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON NAVIGATION_PARAMETER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER NAV_PAR_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER OBJ_TYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON OBJECT_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER OBJ_TYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER OBJ_TYTAB_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON OBJECT_TYPE_TABLE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER OBJ_TYTAB_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER OPTION_LUD_TRG BEFORE
UPDATE ON OPTION_GROUP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER OPTION_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER OWNERSHIP_AU_TR1
 before update
 on ownership_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER OWNERSHIP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER PARCEL_OR_AU_TR1
 before update
 on parcel_origin_attr
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER PARCEL_OR_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER PATH_LUD_TRG BEFORE
UPDATE ON PATH FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER PATH_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER PATH_P_LUD_TRG BEFORE
UPDATE ON PATH_PRODUCT_CLASS FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER PATH_P_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER PATH_SET_BUI_TR
 BEFORE INSERT OR UPDATE
 ON PATH_SET
 FOR EACH ROW
BEGIN
 :NEW.O_SEARCH_LOCATION :=
  LANE_LOCATION_PKG.FnGetLocationString (:NEW.O_LOC_TYPE        --aLocType
                                                        ,
                                         :NEW.O_FACILITY_ID  --aFacilityId
                                                           ,
                                         :NEW.O_CITY               --aCity
                                                    ,
                                         :NEW.O_COUNTY           --aCounty
                                                      ,
                                         :NEW.O_STATE_PROV    --aStateProv
                                                          ,
                                         :NEW.O_POSTAL_CODE  --aPostalCode
                                                           ,
                                         :NEW.O_COUNTRY_CODE --aCountryCode
                                                            ,
                                         :NEW.O_ZONE_ID          --aZoneId
                                                       );
 :NEW.D_SEARCH_LOCATION :=
  LANE_LOCATION_PKG.FnGetLocationString (:NEW.D_LOC_TYPE        --aLocType
                                                        ,
                                         :NEW.D_FACILITY_ID  --aFacilityId
                                                           ,
                                         :NEW.D_CITY               --aCity
                                                    ,
                                         :NEW.D_COUNTY           --aCounty
                                                      ,
                                         :NEW.D_STATE_PROV    --aStateProv
                                                          ,
                                         :NEW.D_POSTAL_CODE  --aPostalCode
                                                           ,
                                         :NEW.D_COUNTRY_CODE --aCountryCode
                                                            ,
                                         :NEW.D_ZONE_ID          --aZoneId
                                                       );
END;
/

ALTER TRIGGER PATH_SET_BUI_TR ENABLE;

CREATE OR REPLACE TRIGGER PATH_W_LUD_TRG BEFORE
UPDATE ON PATH_WAYPOINT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER PATH_W_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER PERM_INHER_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON PERMISSION_INHERITANCE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER PERM_INHER_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER PRODUCT_C_AU_TR1
 before update
 on product_class
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER PRODUCT_C_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER PROTECTIO_AU_TR1
 before update
 on protection_level
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER PROTECTIO_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER RATE_C_LUD_TRG BEFORE
UPDATE ON RATE_CALC_METHOD FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER RATE_C_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER REAN_TYP_AU_TR1
 before update
 on reason_code_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER REAN_TYP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER REASON_CO_AU_TR1
 before update
 on reason_code
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER REASON_CO_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER RELTNSHIP_TR_AI_1
 AFTER INSERT OR UPDATE
 ON RELATIONSHIP
 REFERENCING NEW AS N OLD AS O
 FOR EACH ROW
DECLARE
 VCOMPANY_ID       NUMBER (9);
 VRELNAME          VARCHAR2 (100);
 VRELDESC          VARCHAR2 (100);
 VBUSSNUM          VARCHAR2 (35);
 VCREATESRC        VARCHAR2 (50);
 VRELTYPEID        NUMBER (18);
 VCOMPANYRELATED   NUMBER (9);
 VSTATUS           NUMBER (4);
 VCREATEDTTM       TIMESTAMP;
 VUPDATEDTTM       TIMESTAMP;
 VADDRESS1 VARCHAR2(75);
 VADDRESS2   VARCHAR2(75);
 VADDRESS3  VARCHAR2(75);
 VCITY VARCHAR2(50);
 VSTATE_PROV VARCHAR2(3);
 VPOSTAL_CODE VARCHAR2(10);
 VCOUNTRY_CODE VARCHAR2(4);
 VTELEPHONE_NUMBER VARCHAR2(32);
BEGIN
 VCOMPANY_ID := :N.COMPANY_ID;
 VRELNAME := :N.RELATIONSHIP_NAME;
 VRELDESC := :N.RELATIONSHIP_DESCRIPTION;
 VBUSSNUM := :N.BUSINESS_NUMBER;
 VCREATESRC := :N.CREATED_SOURCE;
 VRELTYPEID := :N.RELATIONSHIP_TYPE_ID;
 VCOMPANYRELATED := :N.COMPANY_ID_RELATED;
 VCREATEDTTM := :N.CREATED_DTTM;
 VUPDATEDTTM := :N.LAST_UPDATED_DTTM;
 SELECT ADDRESS_1,ADDRESS_2,ADDRESS_3,CITY,STATE_PROV,POSTAL_CODE,COUNTRY_CODE,TELEPHONE_NUMBER INTO VADDRESS1,VADDRESS2,VADDRESS3,VCITY,VSTATE_PROV,VPOSTAL_CODE,VCOUNTRY_CODE,VTELEPHONE_NUMBER from COMPANY where company_id = :N.COMPANY_ID_RELATED ;
 IF (:N.IS_ACTIVE = 1)
 THEN
    VSTATUS := 0;
 ELSE
    VSTATUS := 1;
 END IF;
 IF INSERTING
 THEN
    IF (:N.RELATIONSHIP_TYPE_ID = 4)
    THEN
       INSERT INTO BUSINESS_PARTNER (TC_COMPANY_ID,
                                     BUSINESS_PARTNER_ID,
                                     DESCRIPTION,
                                     MARK_FOR_DELETION,
                                     BP_COMPANY_ID,
                                     BUSINESS_NUMBER,
                                     BP_ID,
                                     CREATED_SOURCE,
                                     CREATED_DTTM,ADDRESS_1,ADDRESS_2,ADDRESS_3,CITY,STATE_PROV,POSTAL_CODE,COUNTRY_CODE,TEL_NBR)
            VALUES (:N.COMPANY_ID,
                    SUBSTR (VRELNAME, 1, 10),
                    SUBSTR (VRELDESC, 1, 40),
                    VSTATUS,
                    :N.COMPANY_ID_RELATED,
                    VBUSSNUM,
                    BUSINESS_PARTNER_ID_SEQ.NEXTVAL,
                    VCREATESRC,
                    VCREATEDTTM,VADDRESS1,VADDRESS2,VADDRESS3,VCITY,VSTATE_PROV,VPOSTAL_CODE,VCOUNTRY_CODE ,VTELEPHONE_NUMBER);
    END IF;
    IF (:N.RELATIONSHIP_TYPE_ID = 12)
    THEN
       INSERT INTO BUSINESS_UNIT (TC_COMPANY_ID,
                                  BUSINESS_UNIT,
                                  DESCRIPTION,
                                  MARK_FOR_DELETION,
                                  UNLOAD_ALLOWANCE_TIME,
                                  BUSINESS_NUMBER,
                                  BU_ID)
            VALUES (:N.COMPANY_ID,
                    SUBSTR (VRELNAME, 1, 8),
                    SUBSTR (VRELDESC, 1, 50),
                    VSTATUS,
                    0,
                    VBUSSNUM,
                    :N.COMPANY_ID_RELATED);
    END IF;
 END IF;
 IF UPDATING
 THEN
    IF (:N.RELATIONSHIP_TYPE_ID = 4)
    THEN
       UPDATE BUSINESS_PARTNER
          SET DESCRIPTION = VRELDESC,
              BUSINESS_NUMBER = VBUSSNUM,
              MARK_FOR_DELETION = VSTATUS,
              BP_COMPANY_ID = VCOMPANYRELATED,
              LAST_UPDATED_DTTM = VUPDATEDTTM
        WHERE BUSINESS_PARTNER_ID = :O.RELATIONSHIP_NAME
              AND TC_COMPANY_ID = VCOMPANY_ID;
    END IF;
    IF (:N.RELATIONSHIP_TYPE_ID = 12)
    THEN
       UPDATE BUSINESS_UNIT
          SET DESCRIPTION = VRELDESC,
              BUSINESS_UNIT = SUBSTR (vRELNAME, 1, 8),
              BUSINESS_NUMBER = VBUSSNUM,
              MARK_FOR_DELETION = VSTATUS,
              BU_ID = VCOMPANYRELATED
        WHERE BUSINESS_UNIT = :O.RELATIONSHIP_NAME;
    END IF;
 END IF;
END;
/

ALTER TRIGGER RELTNSHIP_TR_AI_1 ENABLE;

CREATE OR REPLACE TRIGGER RELTP_AMP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON RELTP_APP_MOD_PERM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER RELTP_AMP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER REL_AMP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON REL_APP_MOD_PERM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER REL_AMP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER REL_TYPE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON RELATIONSHIP_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER REL_TYPE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER RG_SHI_LUD_TRG BEFORE
UPDATE ON RG_SHIPPING_PARAM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER RG_SHI_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER ROLE_AIUD_1
AFTER INSERT OR UPDATE OR DELETE
ON ROLE
REFERENCING
  NEW AS NEW_REC
	OLD AS OLD_REC
FOR EACH ROW
	DECLARE
	ACTION VARCHAR2(10);
	CNT NUMBER(4);
	BEGIN
	  SELECT COUNT(PARAMETER_VALUE)  INTO CNT
	  FROM COMPANY_PARAM
	  WHERE PARAMETER_NAME='AUDITING_ENABLED' AND COMPANY_ID = -1 AND PARAMETER_VALUE = '1';
	IF INSERTING THEN
		ACTION:='INSERT';
	ELSIF UPDATING  THEN
		ACTION:='UPDATE';
	ELSIF DELETING  THEN
		ACTION:='DELETE';
	END IF;
	IF (CNT = 1) THEN
		IF (INSERTING OR UPDATING) THEN
			INSERT INTO ROLE_AUDIT (
			ROLE_AUDIT_ID,
				 AUDIT_ACTION
				,ROLE_ID
				,COMPANY_ID
				,ROLE_NAME
				,IS_ROLE_PRIVATE
				,IS_ACTIVE
				,CREATED_SOURCE_TYPE_ID
				,CREATED_SOURCE
				,CREATED_DTTM
				,LAST_UPDATED_SOURCE_TYPE_ID
				,LAST_UPDATED_SOURCE
				,LAST_UPDATED_DTTM
				,HIBERNATE_VERSION
				,APPLY_TO_BUSINESS_PARTNERS
				,ROLE_TYPE_ID
				,DESCRIPTION
				)
			VALUES (
			seq_ROLE_AUDIT_ID.nextval,
				ACTION
				,:NEW_REC.ROLE_ID
				,:NEW_REC.COMPANY_ID
				,:NEW_REC.ROLE_NAME
				,:NEW_REC.IS_ROLE_PRIVATE
				,:NEW_REC.IS_ACTIVE
				,:NEW_REC.CREATED_SOURCE_TYPE_ID
				,:NEW_REC.CREATED_SOURCE
				,:NEW_REC.CREATED_DTTM
				,:NEW_REC.LAST_UPDATED_SOURCE_TYPE_ID
				,:NEW_REC.LAST_UPDATED_SOURCE
				,:NEW_REC.LAST_UPDATED_DTTM
				,:NEW_REC.HIBERNATE_VERSION
				,:NEW_REC.APPLY_TO_BUSINESS_PARTNERS
				,:NEW_REC.ROLE_TYPE_ID
				,:NEW_REC.DESCRIPTION
				);
		ELSIF DELETING THEN
			INSERT INTO ROLE_AUDIT (
			ROLE_AUDIT_ID,
				 AUDIT_ACTION
				,ROLE_ID
				,COMPANY_ID
				,ROLE_NAME
				,IS_ROLE_PRIVATE
				,IS_ACTIVE
				,CREATED_SOURCE_TYPE_ID
				,CREATED_SOURCE
				,CREATED_DTTM
				,LAST_UPDATED_SOURCE_TYPE_ID
				,LAST_UPDATED_SOURCE
				,LAST_UPDATED_DTTM
				,HIBERNATE_VERSION
				,APPLY_TO_BUSINESS_PARTNERS
				,ROLE_TYPE_ID
				,DESCRIPTION
				)
			VALUES (
			seq_ROLE_AUDIT_ID.nextval,
				ACTION
				,:OLD_REC.ROLE_ID
				,:OLD_REC.COMPANY_ID
				,:OLD_REC.ROLE_NAME
				,:OLD_REC.IS_ROLE_PRIVATE
				,:OLD_REC.IS_ACTIVE
				,:OLD_REC.CREATED_SOURCE_TYPE_ID
				,:OLD_REC.CREATED_SOURCE
				,:OLD_REC.CREATED_DTTM
				,:OLD_REC.LAST_UPDATED_SOURCE_TYPE_ID
				,:OLD_REC.LAST_UPDATED_SOURCE
				,SYSTIMESTAMP
				,:OLD_REC.HIBERNATE_VERSION
				,:OLD_REC.APPLY_TO_BUSINESS_PARTNERS
				,:OLD_REC.ROLE_TYPE_ID
				,:OLD_REC.DESCRIPTION
				);
		END IF;
	END IF;
END;
/

ALTER TRIGGER ROLE_AIUD_1 ENABLE;

CREATE OR REPLACE TRIGGER ROLE_AMP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON ROLE_APP_MOD_PERM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER ROLE_AMP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER ROLE_APP_MOD_PERM_AIUD_1
AFTER INSERT OR UPDATE OR DELETE
ON ROLE_APP_MOD_PERM
REFERENCING
  NEW AS NEW_REC
	OLD AS OLD_REC
FOR EACH ROW
	DECLARE
	ACTION VARCHAR2(10);
	CNT NUMBER(4);
BEGIN
	  SELECT COUNT(PARAMETER_VALUE) INTO CNT
	  FROM COMPANY_PARAM
	  WHERE PARAMETER_NAME='AUDITING_ENABLED' AND COMPANY_ID = -1 AND PARAMETER_VALUE = '1';
	IF (CNT = 1) THEN
		IF INSERTING THEN
			ACTION:='INSERT';
			INSERT INTO ROLE_APP_MOD_PERM_AUDIT (
			ROLE_APP_MOD_PERM_AUDIT_ID,
				 AUDIT_ACTION
				,ROLE_ID
				,APP_ID
				,APP_NAME
				,MODULE_ID
				,MODULE_NAME
				,PERMISSION_ID
				,PERMISSION_NAME
				,ROW_UID
				,CREATED_DTTM
				,LAST_UPDATED_DTTM
				)
			VALUES (
			seq_ROLE_APP_MOD_PERM_AUDIT_ID.nextval,
				 ACTION
				,:NEW_REC.ROLE_ID
				,:NEW_REC.APP_ID
				,'NOT_USED'
				,:NEW_REC.MODULE_ID
				,'NOT_USED'
				,:NEW_REC.PERMISSION_ID
				,'NOT_USED'
				,:NEW_REC.ROW_UID
				,:NEW_REC.CREATED_DTTM
				,:NEW_REC.LAST_UPDATED_DTTM
				);
		ELSIF DELETING THEN
			ACTION:='DELETE';
			INSERT INTO ROLE_APP_MOD_PERM_AUDIT (
			ROLE_APP_MOD_PERM_AUDIT_ID,
				 AUDIT_ACTION
				,ROLE_ID
				,APP_ID
				,APP_NAME
				,MODULE_ID
				,MODULE_NAME
				,PERMISSION_ID
				,PERMISSION_NAME
				,ROW_UID
				,CREATED_DTTM
				,LAST_UPDATED_DTTM
				)
			VALUES (
			seq_ROLE_APP_MOD_PERM_AUDIT_ID.nextval,
				ACTION
				,:OLD_REC.ROLE_ID
				,:OLD_REC.APP_ID
				,'NOT_USED'
				,:OLD_REC.MODULE_ID
				,'NOT_USED'
				,:OLD_REC.PERMISSION_ID
				,'NOT_USED'
				,:OLD_REC.ROW_UID
				,:OLD_REC.CREATED_DTTM
				,SYSTIMESTAMP
				);
		 END IF;
	END IF;
END;
/

ALTER TRIGGER ROLE_APP_MOD_PERM_AIUD_1 ENABLE;

CREATE OR REPLACE TRIGGER ROLE_TEMPLATE_REFERENCE_AIUD_1
AFTER INSERT OR UPDATE OR DELETE
ON ROLE_TEMPLATE_REFERENCE
REFERENCING
  NEW AS NEW_REC
	OLD AS OLD_REC
FOR EACH ROW
	DECLARE
	ACTION VARCHAR(10);
  CNT SMALLINT;
BEGIN
	  SELECT COUNT(PARAMETER_VALUE) INTO CNT
	  FROM COMPANY_PARAM
	  WHERE PARAMETER_NAME='AUDITING_ENABLED' AND COMPANY_ID = -1 AND PARAMETER_VALUE = '1';
	IF (CNT = 1) THEN
		IF INSERTING THEN
			ACTION:='INSERT';
			INSERT INTO ROLE_TEMPLATE_REFERENCE_AUDIT (
			ROLE_TEMPLATE_REF_AUDIT_ID,
				 AUDIT_ACTION
				,ROLE_ID
				,REFERENCED_ROLE_TEMPLATE_ID
				,CREATED_OR_DELETED_DTTM
				)
			VALUES (
			seq_ROLE_TEMPLATE_REF_AUDIT_ID.nextval,
				 ACTION
				,:NEW_REC.ROLE_ID
				,:NEW_REC.REFERENCED_ROLE_TEMPLATE_ID
				,SYSTIMESTAMP
				);
		ELSIF DELETING THEN
			ACTION:='DELETE';
			INSERT INTO ROLE_TEMPLATE_REFERENCE_AUDIT (
			ROLE_TEMPLATE_REF_AUDIT_ID,
				 AUDIT_ACTION
				,ROLE_ID
				,REFERENCED_ROLE_TEMPLATE_ID
				,CREATED_OR_DELETED_DTTM
				)
			VALUES (
			seq_ROLE_TEMPLATE_REF_AUDIT_ID.nextval,
				 ACTION
				,:OLD_REC.ROLE_ID
				,:OLD_REC.REFERENCED_ROLE_TEMPLATE_ID
				,SYSTIMESTAMP
				);
		END IF;
	END IF;
END;
/

ALTER TRIGGER ROLE_TEMPLATE_REFERENCE_AIUD_1 ENABLE;

CREATE OR REPLACE TRIGGER ROLE_TYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON ROLE_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER ROLE_TYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER SAIL_SH_DT_LUD_TRG BEFORE
UPDATE ON SAILING_SCHDL_BY_DATE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SAIL_SH_DT_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER SAIL_SH_WD_LUD_TRG BEFORE
UPDATE ON SAILING_SCHDL_BY_WEEK_DAY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SAIL_SH_WD_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER SAIL_SH_WK_LUD_TRG BEFORE
UPDATE ON SAILING_SCHDL_BY_WEEK FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SAIL_SH_WK_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER SCREEN_TYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON SCREEN_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER SCREEN_TYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER SHAPE_TYP_AU_TR1
 before update
 on shape_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER SHAPE_TYP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER SHIPMENT__AU_TR1
 before update
 on shipment_leg_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER SHIPMENT__AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER SHIP_V_ACC_LUD_TRG BEFORE
UPDATE ON SHIP_VIA_INCL_ACCESSORIAL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SHIP_V_ACC_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER SHIP_V_LUD_TRG BEFORE
UPDATE ON SHIP_VIA FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SHIP_V_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER SIZE_UOM_LST_UPDT_DTTM_TRG BEFORE
UPDATE ON SIZE_UOM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER SIZE_UOM_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER SLOTS_GRP_LST_UPD_DTTM_TRG BEFORE
UPDATE ON SLOTS_GROUP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SLOTS_GRP_LST_UPD_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER SLOTS_LST_UPD_DTTM_TRG BEFORE
UPDATE ON SLOTS FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SLOTS_LST_UPD_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER SLOT_SCCPDT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON SLOT_SCHEDULE_CAPACITY_DTL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER SLOT_SCCPDT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER SLT_SCHED_CAPACT_LST_UPD_D_TRG BEFORE
UPDATE ON SLOT_SCHEDULE_CAPACITY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SLT_SCHED_CAPACT_LST_UPD_D_TRG ENABLE;

CREATE OR REPLACE TRIGGER SLT_SCHE_CPCT_DTL_LT_UPD_D_TRG BEFORE
UPDATE ON SLOT_SCHEDULE_CAPACITY_DTL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SLT_SCHE_CPCT_DTL_LT_UPD_D_TRG ENABLE;

CREATE OR REPLACE TRIGGER STANDARD__AU_TR1
 before update
 on standard_mot
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER STANDARD__AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER STAND_UOM_AU_TR1
 before update
 on standard_uom
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER STAND_UOM_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER STATE_PROV_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON STATE_PROV FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER STATE_PROV_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER STATIC_CAL_LUD_TRG BEFORE
UPDATE ON STATIC_CALENDAR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER STATIC_CAL_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER STATIC_COM_LUD_TRG BEFORE
UPDATE ON STATIC_COMPANY_RULE_ATTRIBUTE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER STATIC_COM_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER STAT_RO_CA_LUD_TRG BEFORE
UPDATE ON STATIC_ROUTE_CALENDAR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER STAT_RO_CA_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER STAT_RO_ST_LUD_TRG BEFORE
UPDATE ON STATIC_ROUTE_STOP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER STAT_RO_ST_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER STA_RL_ATT_LUD_TRG BEFORE
UPDATE ON STATIC_RULE_ATTRIBUTE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER STA_RL_ATT_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER STA_RL_RO_LUD_TRG BEFORE
UPDATE ON STATIC_RULE_ROUTE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER STA_RL_RO_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER STD_UMTYP_AU_TR1
 before update
 on standard_uom_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER STD_UMTYP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER STOP_OFF_CHARGE_A_IU_TR_1
 AFTER INSERT OR UPDATE
 ON STOP_OFF_CHARGE
 FOR EACH ROW
DECLARE
 l_old_value   CARRIER_PARAMETER_EVENT.OLD_VALUE%TYPE;
 l_new_value   CARRIER_PARAMETER_EVENT.NEW_VALUE%TYPE;
 l_chg_flag    NUMBER (1) := 0;
 l_time_now    DATE := SYSDATE;
BEGIN
 -- START_NUMBER
 IF (   (:old.START_NUMBER <> :new.START_NUMBER)
   OR ( (:old.START_NUMBER IS NULL) AND (:new.START_NUMBER IS NOT NULL))
   OR ( (:new.START_NUMBER IS NULL) AND (:old.START_NUMBER IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               :new.STOP_OFF_CHARGE_ID,
                               NULL,
                               'INITIAL STOP-OFF',
                               :old.START_NUMBER,
                               :new.START_NUMBER,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- END_NUMBER
 IF (   (:old.END_NUMBER <> :new.END_NUMBER)
   OR ( (:old.END_NUMBER IS NULL) AND (:new.END_NUMBER IS NOT NULL))
   OR ( (:new.END_NUMBER IS NULL) AND (:old.END_NUMBER IS NOT NULL)))
 THEN
  -- Fix CR 57625
  IF (:old.END_NUMBER IS NULL)
  THEN
     l_old_value := 'and over...';
  ELSE
     l_old_value := :old.END_NUMBER;
  END IF;
  IF (:new.END_NUMBER IS NULL)
  THEN
     l_new_value := 'and over...';
  ELSE
     l_new_value := :new.END_NUMBER;
  END IF;
  -- End fix CR 57625
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               :new.STOP_OFF_CHARGE_ID,
                               NULL,
                               'FINAL STOP-OFF',
                               l_old_value,
                               l_new_value,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
 -- VALUE
 IF (   (:old.VALUE <> :new.VALUE)
   OR ( (:old.VALUE IS NULL) AND (:new.VALUE IS NOT NULL))
   OR ( (:new.VALUE IS NULL) AND (:old.VALUE IS NOT NULL)))
 THEN
  INS_CARRIER_PARAMETER_EVENT (:new.ACCESSORIAL_PARAM_SET_ID,
                               NULL,
                               :new.STOP_OFF_CHARGE_ID,
                               NULL,
                               'CHARGE',
                               :old.VALUE,
                               :new.VALUE,
                               :new.LAST_UPDATED_USER);
  l_chg_flag := 1;
 END IF;
END STOP_OFF_CHARGE_A_IU_TR_1;
/

ALTER TRIGGER STOP_OFF_CHARGE_A_IU_TR_1 ENABLE;

CREATE OR REPLACE TRIGGER SURGE_CAP_LUD_TRG BEFORE
UPDATE ON SURGE_CAPACITY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER SURGE_CAP_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER SYS_CDPM_AU_TR1
 before update
 on sys_code_parm
 for each row
begin
 :new.MOD_DATE_TIME := sysdate;
end;
/

ALTER TRIGGER SYS_CDPM_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER SYS_CDPM_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON SYS_CODE_PARM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER SYS_CDPM_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER SYS_CDTP_AU_TR1
 before update
 on sys_code_type
 for each row
begin
 :new.MOD_DATE_TIME := sysdate;
end;
/

ALTER TRIGGER SYS_CDTP_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER SYS_CDTYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON SYS_CODE_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER SYS_CDTYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER SYS_CODE_AU_TR1
 before update
 on sys_code
 for each row
begin
 :new.MOD_DATE_TIME := sysdate;
end;
/

ALTER TRIGGER SYS_CODE_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER SYS_CODE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON SYS_CODE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER SYS_CODE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER TARIFF_CD_LUD_TRG BEFORE
UPDATE ON TARIFF_CODE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TARIFF_CD_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER TARIFF_LAN_LUD_TRG BEFORE
UPDATE ON TARIFF_LANE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TARIFF_LAN_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER TARIFF_LUD_TRG BEFORE
UPDATE ON TARIFF FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TARIFF_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER TARIFF_RT_LUD_TRG BEFORE
UPDATE ON TARIFF_RATE_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TARIFF_RT_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER TARIFF_TRA_LUD_TRG BEFORE
UPDATE ON TARIFF_TRANSIT_TIME FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TARIFF_TRA_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER TARIFF_ZN_LUD_TRG BEFORE
UPDATE ON TARIFF_ZONE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TARIFF_ZN_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER TARIFF_ZOR_LUD_TRG BEFORE
UPDATE ON TARIFF_ZONE_RATE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TARIFF_ZOR_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER TARIFF_ZR_LUD_TRG BEFORE
UPDATE ON TARIFF_ZONE_RATE_TIER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TARIFF_ZR_LUD_TRG ENABLE;

CREATE OR REPLACE TRIGGER TIB_LOCN_HDR
 BEFORE INSERT
 ON LOCN_HDR
 REFERENCING NEW AS NEW
 FOR EACH ROW
BEGIN
 SELECT REVERSE (:NEW.LOCN_BRCD)
 INTO :NEW.VOCO_INTRNL_REVERSE_BRCD
 FROM DUAL;
END;
/

ALTER TRIGGER TIB_LOCN_HDR ENABLE;

CREATE OR REPLACE TRIGGER TIER_AU_TR1
 before update
 on tier
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER TIER_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER TIME_CALC_AU_TR1
 before update
 on time_calc_method
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER TIME_CALC_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER TIME_ZN_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON TIME_ZONE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER TIME_ZN_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER TOTAL_CAPACT_DTL_LST_UPD_D_TRG BEFORE
UPDATE ON TOTAL_CAPACITY_DTL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TOTAL_CAPACT_DTL_LST_UPD_D_TRG ENABLE;

CREATE OR REPLACE TRIGGER TOTAL_CAPACT_LST_UPD_DTTM_TRG BEFORE
UPDATE ON TOTAL_CAPACITY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/

ALTER TRIGGER TOTAL_CAPACT_LST_UPD_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER TOTAL_CAP_LST_UPDT_DTTM_TRG BEFORE
UPDATE ON TOTAL_CAPACITY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER TOTAL_CAP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER TOT_CAP_DTL_LST_UPDT_DTTM_TRG BEFORE
UPDATE ON TOTAL_CAPACITY_DTL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER TOT_CAP_DTL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER TP_COMPAN_AU_TR1
 before update
 on tp_company_service_level
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER TP_COMPAN_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER TP_COMPEQUI_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON TP_COMPANY_EQUIPMENT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER TP_COMPEQUI_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER TRAILER_T_AU_TR1
 before update
 on trailer_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER TRAILER_T_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER TRAN_LOG_BU_1
 BEFORE UPDATE
 ON TRAN_LOG
 REFERENCING NEW AS NEW
 FOR EACH ROW
BEGIN
 :NEW.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

ALTER TRIGGER TRAN_LOG_BU_1 ENABLE;

CREATE OR REPLACE TRIGGER UCL_GRPTYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON UCL_GROUP_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER UCL_GRPTYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER UCL_UGT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON UCL_USER_GROUP_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER UCL_UGT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER UCL_USER_AIU_1
AFTER INSERT OR UPDATE
ON UCL_USER
REFERENCING
  OLD AS OLD
  NEW AS NEW
FOR EACH ROW
DECLARE
VOLDVALUE VARCHAR2(500);
VNEWVALUE VARCHAR2(500);
ACTION VARCHAR2(10);
CNT NUMBER(4);
BEGIN
	  SELECT COUNT(PARAMETER_VALUE) INTO CNT
	  FROM COMPANY_PARAM
	  WHERE PARAMETER_NAME='AUDITING_ENABLED' AND COMPANY_ID = -1 AND PARAMETER_VALUE = '1';
	  If INSERTING Then
		  ACTION:='INSERT';
	  ElsIf UPDATING  Then
		If :OLD.IS_ACTIVE <> :NEW.IS_ACTIVE AND :NEW.IS_ACTIVE = -1 Then
			ACTION:='DELETE';
		ELSE
			ACTION:='UPDATE';
		End If;
	  End If;
	IF (CNT = 1) AND (:NEW.NUMBER_OF_INVALID_LOGINS IS NULL) AND (:OLD.LAST_LOGIN_DTTM = :NEW.LAST_LOGIN_DTTM) THEN
		Insert into UCL_USER_AUDIT (
		UCL_USER_AUDIT_ID,
		AUDIT_ACTION,
		UCL_USER_ID,
		COMPANY_ID,
		USER_NAME,
		USER_PASSWORD,
		IS_ACTIVE,
		CREATED_SOURCE_TYPE_ID,
		CREATED_SOURCE,
		CREATED_DTTM,
	    LAST_UPDATED_SOURCE_TYPE_ID,
	    LAST_UPDATED_SOURCE,
	    LAST_UPDATED_DTTM,
		USER_TYPE_ID,
		LOCALE_ID,
		LOCATION_ID,
		USER_FIRST_NAME,
		USER_MIDDLE_NAME,
		USER_LAST_NAME,
		USER_PREFIX,
		USER_TITLE,
		TELEPHONE_NUMBER,
		FAX_NUMBER,ADDRESS_1,
		ADDRESS_2,
		CITY,
		STATE_PROV_CODE,
		POSTAL_CODE,
		COUNTRY_CODE,
		USER_EMAIL_1,
		USER_EMAIL_2,
		COMM_METHOD_ID_DURING_BH_1,
		COMM_METHOD_ID_DURING_BH_2,
		COMM_METHOD_ID_AFTER_BH_1,
		COMM_METHOD_ID_AFTER_BH_2,
		HIBERNATE_VERSION,
		COMMON_NAME,
		LAST_PASSWORD_CHANGE_DTTM,
		NUMBER_OF_INVALID_LOGINS,
		LOGGED_IN,
		LAST_LOGIN_DTTM,
		DEFAULT_BUSINESS_UNIT_ID,
		DEFAULT_WHSE_REGION_ID,
		CHANNEL_ID,
		TAX_ID_NBR,
		EMP_START_DATE,
		BIRTH_DATE,
		GENDER_ID,
		PASSWORD_RESET_DATE_TIME,
		PASSWORD_TOKEN,
		ISPASSWORDMANAGEDINTERNALLY)
		values (
		seq_UCL_USER_AUDIT_ID.nextval,
		ACTION,
		:NEW.UCL_USER_ID,
		:NEW.COMPANY_ID,
		'NOT_USED',
		:NEW.USER_PASSWORD,
		:NEW.IS_ACTIVE,
		:NEW.CREATED_SOURCE_TYPE_ID,
		:NEW.CREATED_SOURCE,
		:NEW.CREATED_DTTM,
	    :NEW.LAST_UPDATED_SOURCE_TYPE_ID,
	    :NEW.LAST_UPDATED_SOURCE,
	    :NEW.LAST_UPDATED_DTTM,
		:NEW.USER_TYPE_ID,
		:NEW.LOCALE_ID,
		:NEW.LOCATION_ID,
		:NEW.USER_FIRST_NAME,
		:NEW.USER_MIDDLE_NAME,
		:NEW.USER_LAST_NAME,
		:NEW.USER_PREFIX,
		:NEW.USER_TITLE,
		:NEW.TELEPHONE_NUMBER,
		:NEW.FAX_NUMBER,
		:NEW.ADDRESS_1,
		:NEW.ADDRESS_2,
		:NEW.CITY,
		:NEW.STATE_PROV_CODE,
		:NEW.POSTAL_CODE,
		:NEW.COUNTRY_CODE,
		:NEW.USER_EMAIL_1,
		:NEW.USER_EMAIL_2,
		:NEW.COMM_METHOD_ID_DURING_BH_1,
		:NEW.COMM_METHOD_ID_DURING_BH_2,
		:NEW.COMM_METHOD_ID_AFTER_BH_1,
		:NEW.COMM_METHOD_ID_AFTER_BH_2,
		:NEW.HIBERNATE_VERSION,
		:NEW.COMMON_NAME,
		:NEW.LAST_PASSWORD_CHANGE_DTTM,
		:NEW.NUMBER_OF_INVALID_LOGINS,
		:NEW.LOGGED_IN,
		:NEW.LAST_LOGIN_DTTM,
		:NEW.DEFAULT_BUSINESS_UNIT_ID,
		:NEW.DEFAULT_WHSE_REGION_ID,
		:NEW.CHANNEL_ID,
		:NEW.TAX_ID_NBR,
		:NEW.EMP_START_DATE,
		:NEW.BIRTH_DATE,
		:NEW.GENDER_ID,
		:NEW.PASSWORD_RESET_DATE_TIME,
		:NEW.PASSWORD_TOKEN,
		:NEW.ISPASSWORDMANAGEDINTERNALLY);
	END If;
END;
/

ALTER TRIGGER UCL_USER_AIU_1 ENABLE;

CREATE OR REPLACE TRIGGER UCL_USER_FUNCTION_AID_1
AFTER INSERT OR DELETE
ON UCL_USER_FUNCTION
REFERENCING
  OLD AS OLD
  NEW AS NEW
FOR EACH ROW
DECLARE
VOLDVALUE VARCHAR2(500);
VNEWVALUE VARCHAR2(500);
ACTION VARCHAR2(10);
CNT NUMBER(4);
BEGIN
	  SELECT COUNT(PARAMETER_VALUE)  INTO CNT
	  FROM COMPANY_PARAM
	  WHERE PARAMETER_NAME='AUDITING_ENABLED' AND COMPANY_ID = -1 AND PARAMETER_VALUE = '1';
	IF (CNT = 1) THEN
	  If INSERTING Then
		  ACTION:='INSERT';
		Insert into UCL_USER_FUNCTION_AUDIT (
		UCL_USER_FUNCTION_AUDIT_ID,
		  AUDIT_ACTION,
		  USER_NAME,
		  UCL_USER_ID,
		  USER_FUNCTION_ID,
		  CREATED_OR_DELETED_DTTM)
		values (
		seq_UCL_USER_FUNCTION_AUDIT_ID.nextval,
		  ACTION,
		  'NOT_USED',
		  :NEW.UCL_USER_ID,
		  :NEW.USER_FUNCTION_ID,
		  SYSTIMESTAMP);
	  ELSIF DELETING  Then
		  ACTION:='DELETE';
		Insert into UCL_USER_FUNCTION_AUDIT (
		UCL_USER_FUNCTION_AUDIT_ID,
		  AUDIT_ACTION,
		  USER_NAME,
		  UCL_USER_ID,
		  USER_FUNCTION_ID,
		  CREATED_OR_DELETED_DTTM)
		values (
		seq_UCL_USER_FUNCTION_AUDIT_ID.nextval,
		  ACTION,
		  'NOT_USED',
		  :OLD.UCL_USER_ID,
		  :OLD.USER_FUNCTION_ID,
		  SYSTIMESTAMP);
	  End If;
	END If;
END;
/

ALTER TRIGGER UCL_USER_FUNCTION_AID_1 ENABLE;

CREATE OR REPLACE TRIGGER UI_CTRLTYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON UI_CONTROL_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER UI_CTRLTYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER UI_CUDATYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON UI_CU_DATA_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER UI_CUDATYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER UI_TYPE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON UI_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER UI_TYPE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_DEFAULT_AIU_1
AFTER INSERT OR UPDATE
ON USER_DEFAULT
REFERENCING
  OLD AS OLD
  NEW AS NEW
FOR EACH ROW
DECLARE
VOLDVALUE VARCHAR2(500);
VNEWVALUE VARCHAR2(500);
ACTION VARCHAR2(10);
CNT NUMBER(4);
BEGIN
	  SELECT COUNT(PARAMETER_VALUE)  INTO CNT
	  FROM COMPANY_PARAM
	  WHERE PARAMETER_NAME='AUDITING_ENABLED' AND COMPANY_ID = -1 AND PARAMETER_VALUE = '1';
	IF (CNT = 1) THEN
	  If INSERTING Then
		  ACTION:='INSERT';
	  ELSIF UPDATING  Then
		  ACTION:='UPDATE';
	  End If;
		Insert into USER_DEFAULT_AUDIT (
		USER_DEFAULT_AUDIT_ID,
		  AUDIT_ACTION,
		  USER_DEFAULT_ID,
		  USER_NAME,
		  UCL_USER_ID,
		  PARAMETER_NAME,
		  PARAMETER_VALUE,
		  CREATED_DTTM,
		  LAST_UPDATED_DTTM)
		values (
		seq_USER_DEFAULT_AUDIT_ID.nextval,
		  ACTION,
		  :NEW.USER_DEFAULT_ID,
		  'NOT_USED',
		  :NEW.UCL_USER_ID,
		  :NEW.PARAMETER_NAME,
		  :NEW.PARAMETER_VALUE,
		  :NEW.CREATED_DTTM,
		  SYSTIMESTAMP);
	END If;
END;
/

ALTER TRIGGER USER_DEFAULT_AIU_1 ENABLE;

CREATE OR REPLACE TRIGGER USER_DEF_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON USER_DEFAULT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER USER_DEF_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_FUN_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON USER_FUNCTION FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER USER_FUN_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER USER_TY_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON USER_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER USER_TY_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER VENDOR_RLAS_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON VENDOR_ROLE_ASSIGNMENT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER VENDOR_RLAS_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER WAYPOINT_AU_TR1
 before update
 on waypoint_type
 for each row
begin
 :new.last_updated_dttm := systimestamp;
end;
/

ALTER TRIGGER WAYPOINT_AU_TR1 ENABLE;

CREATE OR REPLACE TRIGGER WHSE_SYSCD_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON WHSE_SYS_CODE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER WHSE_SYSCD_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER WSDL_BIND_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON WSDL_BINDINGS FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER WSDL_BIND_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER WSDL_MESG_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON WSDL_MESSAGES FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER WSDL_MESG_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER WSDL_PALIT_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON WSDL_PARTNER_LINK_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER WSDL_PALIT_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER WSDL_PRTTYP_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON WSDL_PORT_TYPES FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER WSDL_PRTTYP_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER WSDL_PTOPER_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON WSDL_PT_OPERATIONS FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER WSDL_PTOPER_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER XSOLUTION_USER_AID_1
AFTER INSERT OR DELETE
ON XSOLUTION_USER
REFERENCING
  OLD AS OLD
  NEW AS NEW
FOR EACH ROW
DECLARE
VOLDVALUE VARCHAR2(500);
VNEWVALUE VARCHAR2(500);
ACTION VARCHAR2(10);
CNT NUMBER(4);
BEGIN
	  SELECT COUNT(PARAMETER_VALUE)  INTO CNT
	  FROM COMPANY_PARAM
	  WHERE PARAMETER_NAME='AUDITING_ENABLED' AND COMPANY_ID = -1 AND PARAMETER_VALUE = '1';
	  IF (CNT = 1) THEN
	  If INSERTING Then
		  ACTION:='INSERT';
		Insert into XSOLUTION_USER_AUDIT (
		XSOLUTION_USER_AUDIT_ID,
		  AUDIT_ACTION,
		  USER_NAME,
		  XSOLUTION_ID,
		  UCL_USER_ID,
		  CREATED_SOURCE,
		  CREATED_SOURCE_TYPE,
		  CREATED_DTTM,
		  LAST_UPDATED_DTTM)
		values (
		seq_XSOLUTION_USER_AUDIT_ID.nextval,
		  ACTION,
		  'NOT_USED',
		  :NEW.XSOLUTION_ID,
		  :NEW.UCL_USER_ID,
		  :NEW.CREATED_SOURCE,
		  :NEW.CREATED_SOURCE_TYPE,
		  :NEW.CREATED_DTTM,
		  :NEW.LAST_UPDATED_DTTM);
	  ELSIF DELETING  Then
		  ACTION:='DELETE';
		Insert into XSOLUTION_USER_AUDIT (
		XSOLUTION_USER_AUDIT_ID,
		  AUDIT_ACTION,
		  USER_NAME,
		  XSOLUTION_ID,
		  UCL_USER_ID,
		  CREATED_SOURCE,
		  CREATED_SOURCE_TYPE,
		  CREATED_DTTM,
		  LAST_UPDATED_DTTM)
		values (
		seq_XSOLUTION_USER_AUDIT_ID.nextval,
		  ACTION,
		  'NOT_USED',
		  :OLD.XSOLUTION_ID,
		  :OLD.UCL_USER_ID,
		  :OLD.CREATED_SOURCE,
		  :OLD.CREATED_SOURCE_TYPE,
		  :OLD.CREATED_DTTM,
		  SYSTIMESTAMP);
	  End If;
	END If;
END;
/

ALTER TRIGGER XSOLUTION_USER_AID_1 ENABLE;

CREATE OR REPLACE TRIGGER YARD_TPCOEQ_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON YARD_TP_COMPANY_EQUIPMENT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER YARD_TPCOEQ_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER YARD_ZNSL_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON YARD_ZONE_SLOT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER YARD_ZNSL_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER YARD_ZONE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON YARD_ZONE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER YARD_ZONE_LST_UPDT_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER YARD_ZONE_LST_UPD_DTTM_TRG
 BEFORE UPDATE
 ON YARD_ZONE
 FOR EACH ROW
BEGIN
 :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

ALTER TRIGGER YARD_ZONE_LST_UPD_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER YRD_LST_UPD_DTTM_TRG
 BEFORE UPDATE
 ON YARD
 FOR EACH ROW
BEGIN
 :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

ALTER TRIGGER YRD_LST_UPD_DTTM_TRG ENABLE;

CREATE OR REPLACE TRIGGER YRD_TP_CO_EQUPMNT_LT_UP_D_TRG
 BEFORE UPDATE
 ON YARD_TP_COMPANY_EQUIPMENT
 FOR EACH ROW
BEGIN
 :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

ALTER TRIGGER YRD_TP_CO_EQUPMNT_LT_UP_D_TRG ENABLE;

CREATE OR REPLACE TRIGGER ZONE_LST_UPDT_DTTM_TRG BEFORE
 UPDATE ON ZONE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
 END;
/

ALTER TRIGGER ZONE_LST_UPDT_DTTM_TRG ENABLE;

set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LICENSE_PARAMS

INSERT INTO LICENSE_PARAMS ( LICENSE_PARAMS_ID,APP_ID,LICENSE_PARAMS_NAME,LICENSE_PARAMS_DESC) 
VALUES  ( 4,1000,'Number of Users','Total number of active users');

INSERT INTO LICENSE_PARAMS ( LICENSE_PARAMS_ID,APP_ID,LICENSE_PARAMS_NAME,LICENSE_PARAMS_DESC) 
VALUES  ( 8,1000,'Number of Business Partners','Total number of active business partners');

INSERT INTO LICENSE_PARAMS ( LICENSE_PARAMS_ID,APP_ID,LICENSE_PARAMS_NAME,LICENSE_PARAMS_DESC) 
VALUES  ( 12,1000,'Number of Companies','Total number of active companies');

INSERT INTO LICENSE_PARAMS ( LICENSE_PARAMS_ID,APP_ID,LICENSE_PARAMS_NAME,LICENSE_PARAMS_DESC) 
VALUES  ( 16,24,'Number of Loads','Number of loads');

Commit;





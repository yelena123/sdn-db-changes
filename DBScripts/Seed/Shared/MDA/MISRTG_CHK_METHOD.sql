set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for MISRTG_CHK_METHOD

INSERT INTO MISRTG_CHK_METHOD ( MISRTG_CHK_METHOD_ID,METHOD_DESCRIPTION) 
VALUES  ( 1,'Ranked carrier on any routing lane');

INSERT INTO MISRTG_CHK_METHOD ( MISRTG_CHK_METHOD_ID,METHOD_DESCRIPTION) 
VALUES  ( 2,'Any routing lane carrier');

Commit;





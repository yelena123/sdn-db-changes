set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for UIB_QUERY_TYPE

INSERT INTO UIB_QUERY_TYPE ( UIB_QUERY_TYPE_ID,QUERY_TYPE) 
VALUES  ( 1,'SQL');

INSERT INTO UIB_QUERY_TYPE ( UIB_QUERY_TYPE_ID,QUERY_TYPE) 
VALUES  ( 2,'Stored Procedure');

INSERT INTO UIB_QUERY_TYPE ( UIB_QUERY_TYPE_ID,QUERY_TYPE) 
VALUES  ( 3,'Text');

Commit;





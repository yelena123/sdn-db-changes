set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for COMPANY_PARAM

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,1,'CHANGE_PASSWORD_ON_FIRST_LOGGIN','N','CHECKBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 3,1,'EXPIRE_PASSWORD','N','CHECKBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 4,1,'MAX_INACTIVE_DAYS','0','TEXTBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 5,1,'MAX_PWD_EXP_DAYS','0','TEXTBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 6,1,'MINIMUM_PASSWORD_LENGTH','1','TEXTBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 8,1,'PWD_CANNOT_BE_SAME_AS_ANY_PREVIOUS_X_PWDS','0','TEXTBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 9,1,'SESSION_IDLE_TIMEOUT','60','TEXTBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 21,1,'PWD_CREATION_REST_LINK_EXPIRY','2','TEXTBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 22,1,'PWD_CANNOT_BE_IDENTICAL_TO_USER_ID','N','CHECKBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 23,1,'PWD_NO_THREE_CONSECUTIVE_IDENTICAL_CHARACTERS','N','CHECKBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 24,1,'PWD_MUST_CONTAIN_ATLEAST_ONE_UPPERCASE','N','CHECKBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 25,1,'PWD_MUST_CONTAIN_AT_LEAST_ONE_LOWERCASE','N','CHECKBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 26,1,'PWD_MUST_CONTAIN_AT_LEAST_ONE_NUMBER','N','CHECKBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 27,1,'PWD_MUST_CONTAIN_AT_LEAST_ONE_SPECIAL_CHARACTER','N','CHECKBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 62,1,'DATA_ACCESS_CONFIGURATION','BU_First','DROPDOWN',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 64,1,'STOP_COMPANY_IMPORT_UPDATE','Y','CHECKBOX',SYSDATE,systimestamp);

INSERT INTO COMPANY_PARAM ( PARAMETER_ID,COMPANY_ID,PARAMETER_NAME,PARAMETER_VALUE,UI_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 65,-1,'AUDITING_ENABLED','1',null,SYSDATE,systimestamp);

Commit;





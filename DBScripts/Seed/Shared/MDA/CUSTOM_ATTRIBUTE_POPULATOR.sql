set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CUSTOM_ATTRIBUTE_POPULATOR

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 1,'com.manh.cbo.transactional.persistence.customattribute.InheritDOCustomAttributeFromFirstPO','Inherit from first PO on Order','ORDR');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 2,'com.manh.cbo.transactional.persistence.customattribute.InheritShipmentCustomAttributeFromFirstPO','Inherit from first PO on Shipment','SHIP');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 3,'com.manh.cbo.transactional.persistence.customattribute.InheritShipmentCustomAttributeFromFirstDO','Inherit from first Order on Shipment','SHIP');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 4,'com.manh.cbo.transactional.persistence.customattribute.InheritDOLineCustomAttributeFromPOLine','Inherit from PO Line on DO Line','ORLI');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 5,'com.manh.cbo.transactional.persistence.customattribute.RankBasedInheritDOCustomAttributeFromPO','Rank based inherit PO on Order','ORDR');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 6,'com.manh.cbo.transactional.persistence.customattribute.RankBasedInheritShipmentCustomAttributeFromPO','Rank based inherit PO on Shipment','SHIP');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 7,'com.manh.cbo.transactional.persistence.customattribute.RankBasedInheritShipmentCustomAttributeFromDO','Rank based inherit Order on Shipment','SHIP');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 8,'com.manh.cbo.transactional.persistence.customattribute.RollupDOCustomAttributeFromPO','Rollup all Custom attributes from POs on Distribution Order','ORDR');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 9,'com.manh.cbo.transactional.persistence.customattribute.RollupShipmentCustomAttributeFromPO','Rollup all Custom attributes from POs on Shipment','SHIP');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 15,'com.manh.appointment.ui.core.InheritAppointmentCustomAttributeFromShipmentPO','Roll up the custom attributes from shipment/po into appointment','APPT');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 10,'com.manh.tpe.custompopulators.generic.CalculateBasedOnRoundRobinAlgorithm','Set attribute value based on Ratio(Ratio, Value)','SHIP');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 11,'com.manh.tpe.custompopulators.clientspecific.RSAreaPopulator','Set true if O and D RS areas are different','SHIP');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 12,'com.manh.tpe.custompopulators.clientspecific.CalculateBasedOnRatioPopulatorParameters','Set attribute value based on Random Ratio (Ratio,Value)','SHIP');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 13,'com.manh.olm.fulfillment.model.customattribute.InheritDOCustomAttributeFromSO','Inherit from Sales Order on Distribution Order','ORDR');

INSERT INTO CUSTOM_ATTRIBUTE_POPULATOR ( POPULATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 14,'com.manh.olm.fulfillment.model.customattribute.InheritDOCustomAttributeFromSOLine','Inherit from Sales Order Line on Distribution Order Line','ORLI');

Commit;





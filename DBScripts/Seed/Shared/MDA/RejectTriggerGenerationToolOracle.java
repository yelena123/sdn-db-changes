import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.MessageFormat;

/**
 *
 * Tool to generate the Trigger to reject INSERT , UPDATE & DELETE on the table.
 * This tool should be used on the common schema.  
 * The input arguments are: dbType dbHost db2Schema dbPort schemaName schemaPwd tableName pkColumns triggerGenDir
 *
 */
 
public class RejectTriggerGenerationToolOracle {
	/*
	CREATE OR REPLACE TRIGGER COMPANY_DD_REJ_BIUD_TR1
	BEFORE INSERT OR UPDATE OR DELETE
	ON COMPANY
	REFERENCING OLD AS O NEW AS N
	FOR EACH ROW
	DECLARE v_pname VARCHAR2(10);
  
	---'MADataSync'
	BEGIN
	SELECT SUBSTR(PROGRAM,1,10) INTO v_pname FROM GV$SESSION WHERE sid = sys_context('USERENV','SID');
   
	IF v_pname != 'MADataSync'
	THEN
        raise_application_error(-20001,'MANH Error - Data Insert,Update,delete not allowed on read-only table');
	END IF;
        
	END;
	*/
	private static final String TRIGGER_BODY =  " CREATE OR REPLACE TRIGGER {0} \n" + 
												" BEFORE INSERT OR UPDATE OR DELETE \n" +
												" ON {1} \n" + 
												" REFERENCING OLD AS O NEW AS N \n" + 
												" FOR EACH ROW \n" +
												" DECLARE v_pname VARCHAR2(10); \n" +
												" BEGIN \n" +
												" SELECT SUBSTR(PROGRAM,1,10) INTO v_pname FROM GV$SESSION WHERE sid = sys_context(''USERENV'',''SID''); \n" +
												" IF v_pname != ''MADataSync'' \n" + 
												" THEN \n" +
												" raise_application_error(-20001,''MANH Error - Data Insert,Update,delete not allowed on read-only table''); \n" +
												" END IF; \n" + 
												" END; \n" +
												" /";
	
	
	 /* Method to generate the reject trigger body for the reject Insert Operation.
	 *
	 * @param   tablename   the table for which reject trigger needs to be created.
	 * @return  String      the reject trigger body for the Insert sql is returned as String.
	 */
	 
	public static String generateRejectTriggerInsertBody(String tableName, String triggerName) {
		
		Object[] args = {triggerName, tableName};
		String triggerBody = MessageFormat.format(TRIGGER_BODY, args);
		return triggerBody;
		
	}	
	
	
	/**
	 * Method to generate the reject trigger body for the Drop sql.
	 *
	 * @param   tablename  the table for which reject trigger needs to be created.
	 * @return  String     the reject trigger body for the Drop sql is returned as String.
	 */
	 
	public static String generateRejectTriggerDropBody(String triggerName) {
		StringBuilder dropTriggerBody = new StringBuilder ();
		
		dropTriggerBody.append ("DROP TRIGGER ").append(triggerName).append("! \n");
		return dropTriggerBody.toString();
	}
	
	/**
	 * Parses the argument list.
	 * Connect to the Database and create the .sql and the drop.sql files.
	 *
	 * @param  args        the argument list is : dbHost db2Schema dbPort schemaName schemaPwd tableName pkColumns triggerGenDir
	 * @throws Exception    
	 */
	 
	 public static void main(String[] args) throws Exception  {
		
		if(args.length != 8) {
			System.out.println("*** Tool Usage ***\n");
			System.out.println("java -jar TriggerRejectTool.ora.1.0.0.jar dbHost db2Schema dbPort schemaName schemaPwd tableName triggername triggerGenDir\n");
			System.out.println("now exiting...");
			System.exit(0);
		}
		
		/*** Oracle specific ***/
		final String DRIVER = "oracle.jdbc.OracleDriver";
		final String URL = "jdbc:oracle:thin:@" +args[0]+ ":" +args[2]+ ":" +args[1];
		final String USERNAME = args[3];
		final String PASSWORD = args[4];
		final String tableName = args[5];
		final String triggerName = args[6];
		final String triggerGenDir = args[7];
		
		Connection connection = null;
		FileWriter out = null;
		FileWriter outDrop = null;
		try {
			Class.forName(DRIVER);
			
			System.out.println("now connecting to database using URL: '" +URL+ "' with credentials '" +USERNAME+ "/" +PASSWORD+ "");
			connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			System.out.println("  successfully connected to the database");
			
			System.out.println("now generating reject trigger code for table: '" +tableName+ "'");
			StringBuilder triggerBody = new StringBuilder("");
			
			triggerBody.append(generateRejectTriggerInsertBody(tableName,triggerName));
			String droptriggerBody = generateRejectTriggerDropBody(triggerName);
			
			System.out.println("  successfully generated reject trigger code");
			
			System.out.println("now creating file for storing reject trigger body: '" +triggerGenDir+ "/" +tableName+ ".sql'");
			File file = new File(triggerGenDir, tableName+".sql");
			File fileDrop = new File(triggerGenDir, tableName+".drop.sql");
			out = new FileWriter(file);
			System.out.println("triggerBody " + triggerBody);
			outDrop = new FileWriter(fileDrop);
			out.append(triggerBody);
			outDrop.append(droptriggerBody);
			System.out.println("  successfully wrote reject trigger body to file");
			System.out.println("now exiting the tool..");
					
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(connection != null)	connection.close();
			if(out != null)			out.close();
			if(outDrop != null)		outDrop.close();
		}

	}

}

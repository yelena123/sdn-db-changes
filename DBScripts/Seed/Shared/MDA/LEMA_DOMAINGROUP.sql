set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LEMA_DOMAINGROUP

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 32,'TE','Static Route Rules','Rules for matching static Routes for LPNs',0,sysdate,null,sysdate,null,SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 200,'DOM','Sourcing Rule Set','Sourcing Rule',1,sysdate,'op',sysdate,'op',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 201,'DOM','ASN Rule Set','ASN',1,sysdate,'op',sysdate,'op',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 202,'DOM','PO Rule Set','PO',1,sysdate,'op',sysdate,'op',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 203,'DOM','Sales Order','Sales Order',1,sysdate,'op',sysdate,'op',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 204,'DOM','On Success Allocation','Allocation',1,sysdate,'BPE',sysdate,'BPE',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 205,'DOM','On Success Prioritization','Prioritization',1,sysdate,'BPE',sysdate,'BPE',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 206,'DOM','On Success Sourcing','Sourcing',1,sysdate,'BPE',sysdate,'BPE',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 500,'SCEM','EventManagement','EventManagement',1,sysdate,'BPE',sysdate,'BPE',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 1,'TP','TPGroup','TPGroup',1,sysdate,'rgarret',sysdate,'rgarret',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 600,'RLM','Return Order','Return Order',1,sysdate,'RLM',sysdate,'RLM',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 700,'TCS','Estimation Formula','TCS Estimation Formula Rule Set',1,sysdate,'TCS',sysdate,'TCS',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 800,'EEM','EEM Workflow','EEM Workflow',1,sysdate,'eem',sysdate,'eem',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 4000,'TPE','INVOICE','Carrier Invoiceing Rules and Validations',1,sysdate,'MANH',sysdate,'MANH',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 5040,'TPE','INVOICE_ACCOUNT','Invoice Account Coding Rules',1,sysdate,'MANH',sysdate,'MANH',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 5,'WMSCPP','Common','Common',1,sysdate,'system',sysdate,'system',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 6,'WMSCPP','LPND','LPNDisposition',1,sysdate,'seeddata',sysdate,'seeddata',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 5000,'WMSCPP','WAVE','Domain Group For Wave',1,sysdate,'system',sysdate,'system',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 5101,'WMSCPP','PUTAWAY','Putaway',1,sysdate,'SEED',sysdate,'SEED',SYSDATE,
systimestamp);

INSERT INTO LEMA_DOMAINGROUP ( IDENTITY,DOMAIN,GROUPNAME,DESCRIPTION,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,MODIFIEDBY,CREATED_DTTM,
LAST_UPDATED_DTTM) 
VALUES  ( 5102,'TPE','RS_RULES','RS_RULES',1,sysdate,'BPE',sysdate,'BPE',SYSDATE,
systimestamp);

Commit;





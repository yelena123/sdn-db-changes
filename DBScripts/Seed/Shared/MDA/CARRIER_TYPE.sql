set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CARRIER_TYPE

INSERT INTO CARRIER_TYPE ( CARRIER_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'UPS',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO CARRIER_TYPE ( CARRIER_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'FedEx Ground',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO CARRIER_TYPE ( CARRIER_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 3,'FedEx Express',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO CARRIER_TYPE ( CARRIER_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 4,'USPS',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO CARRIER_TYPE ( CARRIER_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 6,'DHL',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO CARRIER_TYPE ( CARRIER_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 23,'FedEx Smartpost',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO CARRIER_TYPE ( CARRIER_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 24,'External',SYSTIMESTAMP,SYSTIMESTAMP);

Commit;





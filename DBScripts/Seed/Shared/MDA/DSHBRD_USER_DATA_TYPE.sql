set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DSHBRD_USER_DATA_TYPE

INSERT INTO DSHBRD_USER_DATA_TYPE ( DSHBRD_USER_DATA_TYPE_ID,DESCRIPTION) 
VALUES  ( 1,'NORMAL');

INSERT INTO DSHBRD_USER_DATA_TYPE ( DSHBRD_USER_DATA_TYPE_ID,DESCRIPTION) 
VALUES  ( 2,'FLEXIBLE');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for PERFORMANCE_FACTOR

INSERT INTO PERFORMANCE_FACTOR ( PERFORMANCE_FACTOR_ID,TC_COMPANY_ID,PERFORMANCE_FACTOR_NAME,PF_TYPE,DESCRIPTION,DEFAULT_VALUE,MIN_REC_COUNT,NUM_WEEKS_USED,EARLY_TOLERANCE_POINT,LATE_TOLERANCE_POINT,
EARLY_TOLERANCE_WINDOW,LATE_TOLERANCE_WINDOW,LEVEL_APPLIED,IS_USE_FLAG,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
MISRTG_CHK_METHOD_ID,USE_RG_LOOKUP,NOT_ASSIGN_ON_VIOLATION) 
VALUES  ( 61,-1,'ON TIME',3,'On Time',0,null,null,null,null,
null,null,null,0,2,'LDAP',SYSTIMESTAMP,2,'LDAP',SYSTIMESTAMP,
null,null,null);

INSERT INTO PERFORMANCE_FACTOR ( PERFORMANCE_FACTOR_ID,TC_COMPANY_ID,PERFORMANCE_FACTOR_NAME,PF_TYPE,DESCRIPTION,DEFAULT_VALUE,MIN_REC_COUNT,NUM_WEEKS_USED,EARLY_TOLERANCE_POINT,LATE_TOLERANCE_POINT,
EARLY_TOLERANCE_WINDOW,LATE_TOLERANCE_WINDOW,LEVEL_APPLIED,IS_USE_FLAG,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
MISRTG_CHK_METHOD_ID,USE_RG_LOOKUP,NOT_ASSIGN_ON_VIOLATION) 
VALUES  ( 62,-1,'ACCEPT RATIO',2,'Accept Rat',0,null,null,null,null,
null,null,null,0,2,'LDAP',SYSTIMESTAMP,2,'LDAP',SYSTIMESTAMP,
null,null,null);

INSERT INTO PERFORMANCE_FACTOR ( PERFORMANCE_FACTOR_ID,TC_COMPANY_ID,PERFORMANCE_FACTOR_NAME,PF_TYPE,DESCRIPTION,DEFAULT_VALUE,MIN_REC_COUNT,NUM_WEEKS_USED,EARLY_TOLERANCE_POINT,LATE_TOLERANCE_POINT,
EARLY_TOLERANCE_WINDOW,LATE_TOLERANCE_WINDOW,LEVEL_APPLIED,IS_USE_FLAG,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
MISRTG_CHK_METHOD_ID,USE_RG_LOOKUP,NOT_ASSIGN_ON_VIOLATION) 
VALUES  ( 63,0,'ON TIME',3,'On Time',0,null,null,null,null,
null,null,null,0,2,'LDAP',SYSTIMESTAMP,2,'LDAP',SYSTIMESTAMP,
null,null,null);

INSERT INTO PERFORMANCE_FACTOR ( PERFORMANCE_FACTOR_ID,TC_COMPANY_ID,PERFORMANCE_FACTOR_NAME,PF_TYPE,DESCRIPTION,DEFAULT_VALUE,MIN_REC_COUNT,NUM_WEEKS_USED,EARLY_TOLERANCE_POINT,LATE_TOLERANCE_POINT,
EARLY_TOLERANCE_WINDOW,LATE_TOLERANCE_WINDOW,LEVEL_APPLIED,IS_USE_FLAG,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
MISRTG_CHK_METHOD_ID,USE_RG_LOOKUP,NOT_ASSIGN_ON_VIOLATION) 
VALUES  ( 64,0,'ACCEPT RATIO',2,'Accept Rat',0,null,null,null,null,
null,null,null,0,2,'LDAP',SYSTIMESTAMP,2,'LDAP',SYSTIMESTAMP,
null,null,null);

INSERT INTO PERFORMANCE_FACTOR ( PERFORMANCE_FACTOR_ID,TC_COMPANY_ID,PERFORMANCE_FACTOR_NAME,PF_TYPE,DESCRIPTION,DEFAULT_VALUE,MIN_REC_COUNT,NUM_WEEKS_USED,EARLY_TOLERANCE_POINT,LATE_TOLERANCE_POINT,
EARLY_TOLERANCE_WINDOW,LATE_TOLERANCE_WINDOW,LEVEL_APPLIED,IS_USE_FLAG,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
MISRTG_CHK_METHOD_ID,USE_RG_LOOKUP,NOT_ASSIGN_ON_VIOLATION) 
VALUES  ( 41,-2,'ON TIME',3,'On Time',0,null,null,null,null,
null,null,null,0,2,'LDAP',SYSTIMESTAMP,2,'LDAP',SYSTIMESTAMP,
null,null,null);

INSERT INTO PERFORMANCE_FACTOR ( PERFORMANCE_FACTOR_ID,TC_COMPANY_ID,PERFORMANCE_FACTOR_NAME,PF_TYPE,DESCRIPTION,DEFAULT_VALUE,MIN_REC_COUNT,NUM_WEEKS_USED,EARLY_TOLERANCE_POINT,LATE_TOLERANCE_POINT,
EARLY_TOLERANCE_WINDOW,LATE_TOLERANCE_WINDOW,LEVEL_APPLIED,IS_USE_FLAG,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
MISRTG_CHK_METHOD_ID,USE_RG_LOOKUP,NOT_ASSIGN_ON_VIOLATION) 
VALUES  ( 42,-2,'ACCEPT RATIO',2,'Accept Rat',0,null,null,null,null,
null,null,null,0,2,'LDAP',SYSTIMESTAMP,2,'LDAP',SYSTIMESTAMP,
null,null,null);

INSERT INTO PERFORMANCE_FACTOR ( PERFORMANCE_FACTOR_ID,TC_COMPANY_ID,PERFORMANCE_FACTOR_NAME,PF_TYPE,DESCRIPTION,DEFAULT_VALUE,MIN_REC_COUNT,NUM_WEEKS_USED,EARLY_TOLERANCE_POINT,LATE_TOLERANCE_POINT,
EARLY_TOLERANCE_WINDOW,LATE_TOLERANCE_WINDOW,LEVEL_APPLIED,IS_USE_FLAG,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
MISRTG_CHK_METHOD_ID,USE_RG_LOOKUP,NOT_ASSIGN_ON_VIOLATION) 
VALUES  ( 43,1,'ACCEPT RATIO',2,'Accept Rat',0,null,null,null,null,
null,null,null,0,2,'LDAP',SYSTIMESTAMP,2,'LDAP',SYSTIMESTAMP,
null,null,null);

INSERT INTO PERFORMANCE_FACTOR ( PERFORMANCE_FACTOR_ID,TC_COMPANY_ID,PERFORMANCE_FACTOR_NAME,PF_TYPE,DESCRIPTION,DEFAULT_VALUE,MIN_REC_COUNT,NUM_WEEKS_USED,EARLY_TOLERANCE_POINT,LATE_TOLERANCE_POINT,
EARLY_TOLERANCE_WINDOW,LATE_TOLERANCE_WINDOW,LEVEL_APPLIED,IS_USE_FLAG,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,
MISRTG_CHK_METHOD_ID,USE_RG_LOOKUP,NOT_ASSIGN_ON_VIOLATION) 
VALUES  ( 44,1,'ON TIME',3,'On Time',0,null,null,null,null,
null,null,null,0,2,'LDAP',SYSTIMESTAMP,2,'LDAP',SYSTIMESTAMP,
null,null,null);

Commit;





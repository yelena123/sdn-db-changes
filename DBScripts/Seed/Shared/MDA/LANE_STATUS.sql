set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LANE_STATUS

INSERT INTO LANE_STATUS ( LANE_STATUS,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 0,'Active',SYSTIMESTAMP,systimestamp);

INSERT INTO LANE_STATUS ( LANE_STATUS,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'Draft',SYSTIMESTAMP,systimestamp);

INSERT INTO LANE_STATUS ( LANE_STATUS,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'Deleted',SYSTIMESTAMP,systimestamp);

INSERT INTO LANE_STATUS ( LANE_STATUS,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 3,'Imported',SYSTIMESTAMP,systimestamp);

INSERT INTO LANE_STATUS ( LANE_STATUS,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 4,'Invalid',SYSTIMESTAMP,systimestamp);

Commit;





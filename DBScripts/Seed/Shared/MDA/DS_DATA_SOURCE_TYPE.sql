set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DS_DATA_SOURCE_TYPE

INSERT INTO DS_DATA_SOURCE_TYPE ( DATA_SOURCE_TYPE_ID,DATA_SOURCE_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'Deployment','Datasources belonging to configured deployments',SYSDATE,systimestamp);

INSERT INTO DS_DATA_SOURCE_TYPE ( DATA_SOURCE_TYPE_ID,DATA_SOURCE_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'External Datasource','External datasource',SYSDATE,systimestamp);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LEMA_RULE_CONF_RES_ATTR

INSERT INTO LEMA_RULE_CONF_RES_ATTR ( IDENTITY,NAME,DESCRIPTION,STATUS,HANDLER,SORT_ORDER,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,
MODIFIEDBY,RANK,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'ifNodeWt','This attribute will hold the rule if template weights.',1,'com.manh.bpe.rule.conflict.handler.attributes.TemplateWeightAttributeHandler','DESC',1,sysdate,null,sysdate,
null,1,SYSDATE,systimestamp);

INSERT INTO LEMA_RULE_CONF_RES_ATTR ( IDENTITY,NAME,DESCRIPTION,STATUS,HANDLER,SORT_ORDER,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,
MODIFIEDBY,RANK,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'ifNodeCnt','This attribute will hold the number of if templates used in the rule.',1,'com.manh.bpe.rule.conflict.handler.attributes.NoOfTemplatesAttributeHandler','DESC',1,sysdate,null,sysdate,
null,2,SYSDATE,systimestamp);

INSERT INTO LEMA_RULE_CONF_RES_ATTR ( IDENTITY,NAME,DESCRIPTION,STATUS,HANDLER,SORT_ORDER,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,
MODIFIEDBY,RANK,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 3,'operatorCnt','This attribute will calculate the number of operators in the rule',1,'com.manh.bpe.rule.conflict.handler.attributes.OperatorCountAttributeHandler','DESC',1,sysdate,null,sysdate,
null,3,SYSDATE,systimestamp);

INSERT INTO LEMA_RULE_CONF_RES_ATTR ( IDENTITY,NAME,DESCRIPTION,STATUS,HANDLER,SORT_ORDER,VERSION,CREATEDDATE,CREATEDBY,MODIFIEDDATE,
MODIFIEDBY,RANK,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 4,'operatorPrecWt','This attribute will calculate the operator precedence weight of the rule',1,'com.manh.bpe.rule.conflict.handler.attributes.OperatorPrecedenceWtAttributeHandler','ASC',1,sysdate,null,sysdate,
null,4,SYSDATE,systimestamp);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CM_RATE_TYPE

INSERT INTO CM_RATE_TYPE ( CM_RATE_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DB','Distance Bands',SYSTIMESTAMP,systimestamp);

INSERT INTO CM_RATE_TYPE ( CM_RATE_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'FD','Flat Discount',SYSTIMESTAMP,systimestamp);

INSERT INTO CM_RATE_TYPE ( CM_RATE_TYPE,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'PD','Percentage Discount',SYSTIMESTAMP,systimestamp);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CUSTOM_ATTRIBUTE_VALIDATOR

INSERT INTO CUSTOM_ATTRIBUTE_VALIDATOR ( VALIDATOR_ID,CLASS_NAME,DESCRIPTION,OBJECT_TYPE) 
VALUES  ( 1,'com.manh.tpe.customvalidators.clientspecific.THDGLCodeValidator','GLCode Validator','ACCD');

Commit;





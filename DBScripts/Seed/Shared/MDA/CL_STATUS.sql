set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_STATUS

INSERT INTO CL_STATUS ( STATUS_ID,STATUS_NAME) 
VALUES  ( 0,'Disabled');

INSERT INTO CL_STATUS ( STATUS_ID,STATUS_NAME) 
VALUES  ( 1,'Enabled');

Commit;





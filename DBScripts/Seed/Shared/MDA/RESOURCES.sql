set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for RESOURCES

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 493,'/services/dwmService/*','DWM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 491,'/services/rest/lps/PhoneTopService/*','SCPP',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 492,'/services/rest/dwm/*','DWM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 494,'/ifse/tranlog/TranlogList.xhtml','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 495,'/ifse/resources/ViewResourceContents.jsp','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 496,'/dif/clmessage/CLMessageDetails.jsflps','DIF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 497,'/services/rest/integration/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 498,'/reports/ui/ReportDefnList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 499,'/reports/ui/ConfigureReportParam.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 500,'/reports/ui/reportScheduled.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 501,'/reports/ui/reportScheduledGrid.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 502,'/reports/ui/reportSaved.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 503,'/reports/ui/reportSavedGrid.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 504,'/reports/ui/PrintReqUserAsgnList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 505,'/reports/ui/PrintQueueMonitor.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 506,'/reports/ui/PrintReqSerAsgnList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 507,'/reports/ui/PrintRequestorList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 508,'/reports/ui/PrintQueueSyncList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 509,'/reports/ui/LRFListenerConfigList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 510,'/reports/ui/PrinterFacLookup.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 511,'/reports/ui/PrintQueueSerAsgnList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 512,'/reports/ui/runReport.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 513,'/reports/ui/reportSubmit.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 514,'/reports/ui/printReport.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 515,'/reports/ui/PrintServiceTypesReviewList.jsflps','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 516,'/reports/ui/ReportsUpdateSequence.xhtml','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 517,'/ifse/dataloader/DataloaderexportDialog.jsp','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 518,'/ucl/admin/lps/CompanyParameters.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 520,'/jsp.report.serv/*','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 521,'/servlet/InstantReport.serv','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 522,'/lrf.report.serv/*','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 523,'/print.report.serv','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 524,'/ifse/dataintegration/SubscriberDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 525,'/ifse/dataintegration/SubscriberEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 526,'/reports/ui/JSPInstantReport.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 527,'/jsp.report.serv','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 528,'/lrf.report.serv','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 529,'/bpe/*','BPE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 530,'/services/rest/ifse/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 531,'/services/rest/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 532,'/manh/tools/rest/*','SCPP',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 533,'/dwm/*','DWM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 534,'/services/rest/cbotransactional/DistributionOrderActionService/distributionOrderActions/onClickEditDatesAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 535,'/services/rest/cbotransactional/DistributionOrderActionService/distributionOrderActions/cancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 536,'/services/rest/cbotransactional/DistributionOrderActionService/distributionOrderActions/unCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 537,'/services/rest/cbotransactional/DistributionOrderActionService/distributionOrderActions/breakSyncLock','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 538,'/services/rest/cbotransactional/ASNActionService/asnActions/enableDisableEditASNHeaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 539,'/services/rest/cbotransactional/ASNActionService/asnActions/enableDisableShipASNAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 540,'/services/rest/cbotransactional/ASNActionService/asnActions/shipASNAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 541,'/services/rest/cbotransactional/ASNActionService/asnActions/enableDisableCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 542,'/services/rest/cbotransactional/ASNActionService/asnActions/cancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 543,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableEditAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 544,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableAcceptAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 545,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableDeclineAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 546,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableReadyAcceptanceAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 547,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableRTSAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 548,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableCreateRTSAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 549,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableAddToDOAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 550,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableCreateDOAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 551,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disablePOListCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 552,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disablePOListUnCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 553,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableLockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 554,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/disableUnLockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 555,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/cancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 556,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/unCancelAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 557,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/validateCreateDOAction','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 558,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/readyToShipAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 559,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/validateViewRTSAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 560,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/lockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 561,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/unLockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 562,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/acceptAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 563,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/declineAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 564,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/readyForAcceptanceAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 565,'/services/rest/cbotransactional/ShipmentListActionService/shipmentListActions/breakSyncLockAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 566,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/carrierExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 567,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/calendarExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 568,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/facilityExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 569,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/equipInstExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 570,'/services/rest/cbobase/CBOBaseExportActionService/cboBaseExportActionService/driverExportToDataLoaderAction','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 571,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/lookupList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 572,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/buList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 573,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/shipmentStatusList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 574,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/poStatusList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 575,'/services/rest/cbobase/CBOFiniteValueService/cbofv/sourceType','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 576,'/services/rest/cbobase/CBOFiniteValueService/cbofv/region','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 577,'/services/rest/cbobase/CBOFiniteValueService/cbofv/carrierbuList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 578,'/services/rest/cbobase/CBOFiniteValueService/cbofv/lookupList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 579,'/services/rest/cbobase/CBOFiniteValueService/cbofv/statProv','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 580,'/basedata/customerserv/view/CustomerDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 581,'/basedata/customerserv/view/CustomerEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 582,'/basedata/payee/PayeeEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 583,'/docManagementFileServlet.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 584,'/report.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 585,'/basedataEquipmentCreateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 586,'/basedataEquipmentUpdateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 587,'/basedataEquipmentListServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 588,'/basedataFacilityDockCreateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 589,'/basedataFacilityDockUpdateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 590,'/basedataFacilityDockHoursCreateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 591,'/basedataFacilityDockHoursUpdateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 592,'/basedataFacilityDockListServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 593,'/basedataAdvanceFilterDispatcherServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 594,'/basedataRegionCreateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 595,'/basedataRegionUpdateServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 596,'/basedataRegionListServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 597,'/ilsFilterDispatcherServices.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 598,'/basedata/ajax.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 599,'/oblcontrollerServlet.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 600,'/basedataAdvanceFilterLoader.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 601,'/basedata/payee/PayeeDetail.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 602,'/basedata/payee/PayeeEditContact.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 603,'/basedata/payee/PayeeContact.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 604,'/cbo/rules/CBORuleColmList.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 606,'/cbo/alert/view/ApAlertDefView.jsflps','BMD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 607,'/basedata/util/ProcessVendorLogin.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 608,'/cbo/alert/view/ApAlertDefEdit.jsflps','BMD',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 609,'/basedataFilterDispatcherServices.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 610,'/cbo/CreateShipment.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 611,'/cbo/EditShipmentDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 612,'/cbo/POList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 613,'/cbo/processStatusInquiry.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 614,'/cbo/PurchaseOrderCreate.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 615,'/cbo/PurchaseOrderEdit.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 616,'/cbo/SubmitCreatePO.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 617,'/cbo/SubmitCreateShipment.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 618,'/cbo/SubmitEditPO.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 619,'/cbo/SubmitShipmentDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 620,'/cbo/transactional/shipment/view/ViewDependentShipmentList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 621,'/cbo/VcpPurchaseOrderCreate.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 622,'/cbo/VcpPurchaseOrderEdit.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 623,'/cbo/ViewDependentShipmentList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 624,'/cbo/ViewPODetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 625,'/cbo/ViewShipmentDetail.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 626,'/cbo/ViewShipmentList.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 627,'/distributionorder/distributionOrderDetails.do','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 646,'/services/rest/cbobase/ItemLookupService/itemlookup/mask','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 647,'/cbo/classificationcode/ClassificationCodeLookupPopup.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 648,'/basedata/unnumber/view/UnNumberEdit.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 649,'/basedata/unnumber/view/UnNumberDetail.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 650,'/cbo/rules/CBORuleColmRef.xhtml','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 651,'/lcom/index.jsp','CBO',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 652,'/cbotransactional/drillDown.serv/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 653,'/cbo/massupdate/view.jsp','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 654,'/cbo/shipperselection/ShipperSelection.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 655,'/cbo/transactional/alerts/view/ApAlertList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 656,'/cbo/transactional/lookup/idLookup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 657,'/docMgtFileDownload.serv','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 658,'/services/rest/cbotransactional/CBOTransFiniteValueService/cboTransfv/activeCurrencyTypeList','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 659,'/cbo/ZipCode/*','BDM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 661,'/cbo/shipperselection/ShipperSelectionDefault.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 663,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 664,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 665,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 666,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 667,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 668,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/incotermList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 669,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/billingMethodList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 670,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/countryListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 671,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/originStateProvMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 672,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/stateProvMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 673,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/zoneMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 674,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/frequencyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 675,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/rGQualifier','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 676,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/exportLane','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 677,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/exportAllLane','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 678,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/validateLanes','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 679,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/validationErrorDetails','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 680,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 681,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 682,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 683,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 684,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 685,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 686,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 687,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 688,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/weekDaysList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 689,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 690,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 691,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 692,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 693,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/routingTierList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 694,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/carrierRejectionPeriodList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 695,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/cubingFactorList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 696,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/sailingFrequencyTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 697,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/fixedTransitTimeUOMList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 362,'/lps/cache/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 363,'/ucl/admin/lps/UserGroupList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 364,'/uclDisableUserServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 365,'/uclEditCompanyParameterServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 366,'/basedata/facility/jsp/crossDockWindowsList.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 367,'/basedata/facility/jsp/DropHookParameterPopup.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 368,'/cbo/transactional/infeasibility/equipmentEquipment/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 369,'/cbo/transactional/infeasibility/facilityEquipment/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 370,'/cbo/transactional/infeasibility/protectionLevelFacility/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 371,'/cbo/transactional/location/view/LocationLookup.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 372,'/cbo/transactional/asn/CreateASNFromPO.jsflps?isShipping=true','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 373,'/lcom/admin/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 374,'/ucl/admin/jsp/AdministerApplications.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 375,'/ucl/admin/jsp/ViewInternalUserAccessControl.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 376,'/ucl/admin/jsp/ViewRolePermissions.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 377,'/ucl/common/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 378,'/uclEnableUserServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 379,'/cbo/transactional/infeasibility/modeProtectionLevel/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 380,'/cbo/transactional/infeasibility/productClassEquipment/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 381,'/cbo/classificationcode/ClassificationCodeList.xhtml','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 382,'/ucl/admin/lps/CreateApplicationInstance.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 383,'/ucl/admin/lps/ListApplicationInstance.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 384,'/ucl/admin/lps/RoleList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 385,'/uclEditUserServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 386,'/uclRelationshipFilterServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 387,'/ucl/admin/jsp/SuccessPage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 388,'/cbo/transactional/infeasibility/UnNumberUnNumber/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 389,'/cbo/transactional/infeasibility/productClassCarrier/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 390,'/ucl/admin/jsp/ErrorPage.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 391,'/manh/tools/ca/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 392,'/ucl/admin/jsp/ViewExternalUserAccessControl.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 393,'/uclEditCompanyServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 394,'/lps/customization/customize.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 395,'/cbo/transactional/infeasibility/facilityTP/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 396,'/cbo/transactional/infeasibility/productClassProductClass/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 397,'/ucl/admin/lps/RegionList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 398,'/uclAppInstanceServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 399,'/uclControllerServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 400,'/uclCreateUserServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 401,'/cbo/transactional/infeasibility/protectionLevelFacilityPair/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 402,'/cbo/transactional/dataloader/CBOImportDataLoader.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 403,'/basedata/currencylist/view/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 404,'/lps/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 405,'/ucl/admin/lps/ChannelList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 406,'/ucl/admin/lps/PermissionList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 407,'/uclCreateLocationServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 408,'/ucl/admin/lps/ViewUserInternalAccessControl.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 409,'/cbo/transactional/infeasibility/protectionEquipmentByFacility/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 410,'/wm/systemcontrol/ui/ProgramParmList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 411,'/dashboard/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 412,'/lcom/common/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 413,'/ucl/admin/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 414,'/ucl/admin/lps/LocationList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 415,'/ucl/admin/lps/RelationshipList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 416,'/uclEditLocationServlet.serv','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 417,'/ucl/admin/lps/ViewUserExternalAccessControl.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 418,'/cbo/transactional/infeasibility/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 419,'/cbo/transactional/infeasibility/protectionLevelProtectionLevel/jsp/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 420,'/wm/systemcontrol/ui/TranMaintenanceList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 421,'/manh/tools/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 422,'/manh/tools/sa/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 423,'/ucl/admin/lps/CompanyCreate.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 424,'/ucl/admin/lps/UserList.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 425,'/ucl/toolpages/*','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 426,'/ucl/admin/lps/ViewRolePermissions.jsflps','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 427,'/services/rest/lps/*','SCPP',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 428,'/services/rest/dashboard/*','SCPP',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 429,'/services/rest/see/*','SCPP',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 430,'/services/rest/oum/CommonService/commonservice/countrylist','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 431,'/services/rest/oum/CommonService/commonservice/statelist','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 432,'/services/rest/oum/CommonService/commonservice/status','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 433,'/services/rest/oum/CompanyService/companyservice/listcompanytypes','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 434,'/services/rest/oum/CompanyService/companyservice/companydetailbyid','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 435,'/services/rest/oum/CompanyService/companyservice/updatecompany','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 436,'/services/rest/oum/CompanyService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 437,'/services/rest/oum/ModuleService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 438,'/services/rest/oum/LocationService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 439,'/services/rest/oum/RegionService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 440,'/services/rest/oum/UserService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 441,'/services/rest/oum/ChannelService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 442,'/services/rest/oum/PermissionService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 443,'/services/rest/oum/CommonService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 444,'/services/rest/Integration/Company/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 445,'/services/rest/Integration/User/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 446,'/services/rest/Integration/Role/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 447,'/services/rest/Integration/Relationship/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 448,'/services/rest/Integration/UserGroup/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 449,'/services/rest/integration/oum/region/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 450,'/services/rest/integration/oum/location/import','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 451,'/ifse/crossref/CrossRefCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 452,'/ifse/crossref/CrossRefEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 453,'/ifse/crossref/CrossRefDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 454,'/ifse/alerts/AlertCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 455,'/ifse/alerts/AlertEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 456,'/ifse/alerts/AlertDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 457,'/ifse/lookup/CreateLookup.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 458,'/ifse/lookup/EditLookup.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 459,'/ifse/lookup/LookupDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 460,'/ifse/pixcrossref/PixCrossRefCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 461,'/ifse/pixcrossref/PixCrossRefEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 462,'/ifse/pixcrossref/PixCrossRefDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 463,'/ifse/router/RouterCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 464,'/ifse/router/RouterEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 465,'/ifse/router/RouterDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 466,'/ifse/listener/ListenerCreate.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 467,'/ifse/listener/ListenerEdit.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 468,'/ifse/listener/ListenerDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 469,'/ifse/listener/ftp/AddFTPListener.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 470,'/ifse/tranlog/TranlogDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 471,'/ifse/resources/UploadResource.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 472,'/dif/endPoint/EndPointEdit.jsflps','DIF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 473,'/dif/endPoint/EndPointDetail.jsflps','DIF',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 474,'/services/rest/oum/UserService/userservice/myprofile','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 475,'/services/rest/oum/UserService/userservice/myprofile','ACM',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 476,'/services/rest/oum/CommonService/commonservice/locales','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 477,'/services/rest/oum/CommonService/commonservice/communicationmethods','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 478,'/services/rest/oum/CompanyService/companyservice/passwordcriteriaoncompanyid','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 479,'/services/rest/oum/LocationService/locationservice/locationSelectionList','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 480,'/services/rest/oum/ChannelService/channelservice/channelSelectionList','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 481,'/services/rest/oum/UserCredentialService/usercredservice/sendpasswordresetrequestbyemail','ACM',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 482,'/services/rest/lps/PhoneTopService/*','SCPP',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 483,'/services/datasync/mda/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 484,'/services/datasync/subscriber/*','IFSE',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 485,'/services/rest/integration/device/*','DIF',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 486,'/services/rest/oum/UserService/userservice/appmodpermtree','ACM',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 487,'/ifse/dataintegration/DataSyncLogDetail.jsflps','IFSE',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 699,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/capacityCommitmentSizeUOMMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 700,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 701,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 702,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 703,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 704,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/tierList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 705,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/reasonCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 706,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/tariffList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 707,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 708,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/minRateTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 709,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/distanceUOMList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 710,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMWeightList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 711,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMAllList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 712,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMDistanceList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 713,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/auditTrailList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 714,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/pendingApprovalChangesList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 715,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 716,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 717,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 718,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 719,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/auditTrailList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 720,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/pendingApprovalChangesList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 721,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/accessorialExclusionList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 722,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 723,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 724,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 725,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 726,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 727,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 728,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 729,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 730,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/lpnTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 731,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 488,'/manh/oum/audit/audittest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 698,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/adjustLaneDetails','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 732,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 733,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 734,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 735,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/carrierUtilizationUsageByWeek','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 736,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 737,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 738,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 739,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 740,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 741,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 742,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 743,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 744,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 745,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 746,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 747,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 748,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/accessorialCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 749,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/surchargeTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 750,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/zoneList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 751,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/serviceLevelList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 752,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/modeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 753,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/currencyList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 754,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 755,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 756,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 757,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 758,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 759,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 760,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 761,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 762,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 763,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 764,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 765,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 766,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/stopOffCurrencyList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 767,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/distanceUOMList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 768,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/rateTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 769,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/businessUnitList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 770,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/weightList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 771,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/equipmentCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 772,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/cmCurrencyList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 773,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/fakCommodityCodeClassList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 774,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/carrierCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 775,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/auditTrailList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 776,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/getStopoffCurrencies','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 777,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/getCMCurrencies','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 778,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 779,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 780,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 781,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 782,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/accessorialCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 783,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/incotermList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 784,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/currencyList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 785,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/sizeTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 786,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/rateTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 787,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/modeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 788,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/equipmentCodeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 789,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/serviceLevelList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 790,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/billingMethodList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 791,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/protectionLevelList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 792,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/uomList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 793,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/accessorialExclusionList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 794,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/payeeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 795,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/fuelAccessorialList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 796,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/ORMList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 797,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/updateExcludedAccessorialList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 798,'/services/rest/contractmanagement/AccessorialService/accessorialservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 799,'/services/rest/contractmanagement/AccessorialService/accessorialservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 800,'/services/rest/contractmanagement/AccessorialService/accessorialservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 801,'/services/rest/contractmanagement/AccessorialService/accessorialservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 802,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialLabelTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 803,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 804,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialOptionGroupMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 805,'/services/rest/contractmanagement/AccessorialService/accessorialservice/distanceUOMMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 806,'/services/rest/contractmanagement/AccessorialService/accessorialservice/acessorialOptionGroupDesc','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 807,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialLabelTypeDesc','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 808,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialCodesDependentForTC','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 809,'/services/rest/contractmanagement/AccessorialService/accessorialservice/validAccessorialCodesMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 810,'/services/rest/contractmanagement/AccessorialService/accessorialservice/fuelIndexMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 811,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialDataById','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 812,'/services/rest/contractmanagement/AccessorialService/accessorialservice/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 813,'/services/rest/contractmanagement/AccessorialService/accessorialservice/dataSourceMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 814,'/services/rest/contractmanagement/AccessorialService/accessorialservice/exportFuelSurcharge','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 815,'/services/rest/contractmanagement/AccessorialService/accessorialservice/sellSideRatesFlag','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 816,'/services/rest/contractmanagement/ShipViaService/shipviaservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 817,'/services/rest/contractmanagement/ShipViaService/shipviaservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 818,'/services/rest/contractmanagement/ShipViaService/shipviaservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 819,'/services/rest/contractmanagement/ShipViaService/shipviaservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 820,'/services/rest/contractmanagement/ShipViaService/shipviaservice/modeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 821,'/services/rest/contractmanagement/ShipViaService/shipviaservice/serviceLevelMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 822,'/services/rest/contractmanagement/ShipViaService/shipviaservice/shipViaAccessorialMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 489,'/audit/audittest.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 885,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 886,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 887,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getBusinessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 888,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getModeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 889,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getProtectionLevelMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 890,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getServiceLevelMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 891,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getTariffCodeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 892,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getServiceLevelMapForFilter','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 893,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getProtectionLevelMapForFilter','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 894,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getModeMapForFilter','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 895,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getBuMapForFilter','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 896,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 897,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 898,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 899,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 900,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getCurrencyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 901,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getPackageTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 902,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getCommodityClassMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 903,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getRateCalcMethodMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 904,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getSizeUOMMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 905,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 906,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 907,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 908,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 909,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/getZoneMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 910,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/getTimeUOMMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 911,'/services/rest/contractmanagement/MassExpirationService/massexpirationservices/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 912,'/services/rest/contractmanagement/MassExpirationService/massexpirationservices/updateRecord','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 913,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 914,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 915,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 916,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 917,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/getRateZonesList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 918,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/getZoneMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 919,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 920,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 921,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 922,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 923,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/carrCompanyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 924,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/shipCompanyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 925,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/companyTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 926,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 927,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 928,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 929,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 930,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 931,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 932,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 933,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 934,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 935,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 936,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 937,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 938,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 939,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 940,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 941,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 942,'/services/rest/contractmanagement/RailRoutService/railservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 943,'/services/rest/contractmanagement/RailRoutService/railservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 944,'/services/rest/contractmanagement/RailRoutService/railservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 945,'/services/rest/contractmanagement/RailRoutService/railservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 946,'/services/rest/contractmanagement/RailRoutService/railservice/railDetail','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 947,'/services/rest/contractmanagement/RailRoutService/railservice/railPath','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 961,'/reports/admin/jsp/DevTools.jsp','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 962,'/reports/admin/jsp/MRFMonitoring.jsp','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 963,'/manh/tools/sa/logging/LoggerOutput.xhtml','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 964,'/manh/tools/sa/logging/LoggerMgmt.xhtml','REPORTS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 965,'/services/rest/oum/RoleService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 966,'/services/rest/oum/AccessControlService/*','ACM',2,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 968,'/basedata/season/jsp/ProcessSeason.jsp','ACM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 969,'/basedata/util/PDFViewer.jsp','TRANS',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 970,'/services/rest/cbotransactional/PurchaseOrderActionService/purchaseOrderActions/commonEnableDisablePOButtons','CBO',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 971,'/services/rest/cbotransactional/CustomParamsService/customParams','CBO',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 972,'/docMgmtImportFileServlet.serv','APPT',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 973,'/doms/dom/selling/co/customerorder/EditCustomerOrder.jsflps','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 974,'/doms/dom/selling/co/customerorder/EditCustomerOrder.xhtml','DOM',1,null);

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 948,'/services/rest/contractmanagement/RailRoutService/railservice/railSplc','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 823,'/services/rest/contractmanagement/ShipViaService/shipviaservice/currencyListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 824,'/services/rest/contractmanagement/ShipViaService/shipviaservice/shipViaCustomAttributeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 825,'/services/rest/contractmanagement/ShipViaService/shipviaservice/insuranceCoverTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 826,'/services/rest/contractmanagement/ShipViaService/shipviaservice/labelTypeList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 827,'/services/rest/contractmanagement/ShipViaService/shipviaservice/parcelServiceIconList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 828,'/services/rest/contractmanagement/ShipViaService/shipviaservice/serviceLevelIndicatorList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 829,'/services/rest/contractmanagement/ShipViaService/shipviaservice/executionlevelList','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 830,'/services/rest/contractmanagement/ShipViaService/shipviaservice/carrierLookUpMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 831,'/services/rest/contractmanagement/ShipViaService/shipviaservice/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 832,'/services/rest/contractmanagement/ShipViaService/shipviaservice/billShipViaMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 833,'/services/rest/contractmanagement/ShipViaService/shipviaservice/normalizationCurrency','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 834,'/services/rest/contractmanagement/ShipViaService/shipviaservice/trackingNumberRequiredMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 835,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 836,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 837,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 838,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 839,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/FuelTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 840,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/countryListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 841,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/RegionListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 842,'/services/rest/contractmanagement/FuelRateService/fuelRateService/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 843,'/services/rest/contractmanagement/FuelRateService/fuelRateService/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 844,'/services/rest/contractmanagement/FuelRateService/fuelRateService/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 845,'/services/rest/contractmanagement/FuelRateService/fuelRateService/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 846,'/services/rest/contractmanagement/FuelRateService/fuelRateService/CurrencyListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 847,'/services/rest/contractmanagement/FuelRateService/fuelRateService/NormalizationCurrencyMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 848,'/services/rest/contractmanagement/FuelRateService/fuelRateService/RateUOMListMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 849,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 850,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/billShipviaLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 851,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 852,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/customerlookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 853,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/stateProvMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 854,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/countryMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 855,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 856,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/searchTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 857,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/errorTypeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 858,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/modeMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 859,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierCompanyLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 860,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/shipperCompanyLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 861,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/railcarrierLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 862,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/payeeLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 863,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/businessUnitMapForLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 864,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/shipThroughLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 865,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/commodityLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 866,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 867,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 868,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 869,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 870,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 871,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 872,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 873,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/{recordMarker}','CNTRMGT',2,'PUT');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 874,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/','CNTRMGT',2,'POST');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 875,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/','CNTRMGT',2,'DELETE');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 876,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/modes','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 877,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/serviceLevels','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 878,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/sizeUOMs','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 879,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/zones','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 880,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/comparisionMethods','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 881,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/baseUOMs','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 882,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/businessUnitMap','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 883,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/','CNTRMGT',2,'GET');

INSERT INTO RESOURCES ( RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD) 
VALUES  ( 884,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/{recordMarker}','CNTRMGT',2,'PUT');

Commit;





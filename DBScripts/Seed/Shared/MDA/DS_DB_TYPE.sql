set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DS_DB_TYPE

INSERT INTO DS_DB_TYPE ( DB_TYPE_ID,DATABASE_TYPE) 
VALUES  ( 1,'ORACLE');

INSERT INTO DS_DB_TYPE ( DB_TYPE_ID,DATABASE_TYPE) 
VALUES  ( 2,'DB2');

Commit;





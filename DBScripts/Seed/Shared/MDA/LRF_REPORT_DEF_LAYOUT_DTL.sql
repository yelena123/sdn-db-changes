set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LRF_REPORT_DEF_LAYOUT_DTL

INSERT INTO LRF_REPORT_DEF_LAYOUT_DTL ( LRF_REPORT_DEF_LAYOUT_DTL_ID,JS_EVENT,DEPEND_IMPL_CLASS,REPORT_DEFN_LAYOUT_ID,CHILD_DEFN_LAYOUT_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 21,'onchange','#{jobFunctionBackingBean.dependentAction}',1265,1272,SYSDATE,systimestamp);

Commit;





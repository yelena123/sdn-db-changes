set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_PRIORITY

INSERT INTO CL_PRIORITY ( PRIORITY_ID,PRIORITY_NAME) 
VALUES  ( 9999,'Default');

INSERT INTO CL_PRIORITY ( PRIORITY_ID,PRIORITY_NAME) 
VALUES  ( 1,'Low');

INSERT INTO CL_PRIORITY ( PRIORITY_ID,PRIORITY_NAME) 
VALUES  ( 0,'Medium');

INSERT INTO CL_PRIORITY ( PRIORITY_ID,PRIORITY_NAME) 
VALUES  ( 100,'High');

Commit;





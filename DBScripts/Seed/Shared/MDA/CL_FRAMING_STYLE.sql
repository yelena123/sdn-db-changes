set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_FRAMING_STYLE

INSERT INTO CL_FRAMING_STYLE ( FRAMING_STYLE_ID,FRAMING_STYLE_NAME) 
VALUES  ( 1,'Delimited');

INSERT INTO CL_FRAMING_STYLE ( FRAMING_STYLE_ID,FRAMING_STYLE_NAME) 
VALUES  ( 2,'Sized');

INSERT INTO CL_FRAMING_STYLE ( FRAMING_STYLE_ID,FRAMING_STYLE_NAME) 
VALUES  ( 3,'Fixed');

Commit;





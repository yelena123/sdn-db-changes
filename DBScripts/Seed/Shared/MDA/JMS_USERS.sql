set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for JMS_USERS

INSERT INTO JMS_USERS ( USERID,PASSWD,CLIENTID) 
VALUES  ( 'dynsub','dynsub',null);

INSERT INTO JMS_USERS ( USERID,PASSWD,CLIENTID) 
VALUES  ( 'nobody','nobody',null);

INSERT INTO JMS_USERS ( USERID,PASSWD,CLIENTID) 
VALUES  ( 'john','needle','DurableSubscriberExample');

INSERT INTO JMS_USERS ( USERID,PASSWD,CLIENTID) 
VALUES  ( 'j2ee','j2ee',null);

INSERT INTO JMS_USERS ( USERID,PASSWD,CLIENTID) 
VALUES  ( 'guest','guest',null);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for UIB_SP_DETAILS

INSERT INTO UIB_SP_DETAILS ( UIB_SP_DETAILS_ID,UIB_QUERY_ID,SP_NAME,SP_PARAM_LIST,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 45,1527,'PORTLET_FAC_PERF_PKG.PORTLET_FAC_PERF_PROC',':session.wt_loggedInUserId.isInteger,7',0,1,'LMADMIN',SYSTIMESTAMP,1,'LMADMIN',
SYSTIMESTAMP);

INSERT INTO UIB_SP_DETAILS ( UIB_SP_DETAILS_ID,UIB_QUERY_ID,SP_NAME,SP_PARAM_LIST,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM) 
VALUES  ( 46,1528,'PORTLET_JF_PERF_PKG.PORTLET_JF_PERF_PROC',':session.wt_loggedInUserId.isInteger,7',0,1,'LMADMIN',SYSTIMESTAMP,1,'LMADMIN',
SYSTIMESTAMP);

Commit;





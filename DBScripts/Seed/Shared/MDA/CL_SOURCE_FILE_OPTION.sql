set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_SOURCE_FILE_OPTION

INSERT INTO CL_SOURCE_FILE_OPTION ( SOURCE_FILE_OPTION_ID,SOURCE_FILE_OPTION_NAME) 
VALUES  ( 1,'Delete');

INSERT INTO CL_SOURCE_FILE_OPTION ( SOURCE_FILE_OPTION_ID,SOURCE_FILE_OPTION_NAME) 
VALUES  ( 2,'Rename');

INSERT INTO CL_SOURCE_FILE_OPTION ( SOURCE_FILE_OPTION_ID,SOURCE_FILE_OPTION_NAME) 
VALUES  ( 3,'Move');

Commit;





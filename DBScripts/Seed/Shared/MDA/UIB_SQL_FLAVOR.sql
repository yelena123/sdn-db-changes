set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for UIB_SQL_FLAVOR

INSERT INTO UIB_SQL_FLAVOR ( UIB_SQL_FLAVOR_ID,SQL_FLAVOR) 
VALUES  ( 1,'ANSI');

INSERT INTO UIB_SQL_FLAVOR ( UIB_SQL_FLAVOR_ID,SQL_FLAVOR) 
VALUES  ( 3,'DB2 SQL PL');

INSERT INTO UIB_SQL_FLAVOR ( UIB_SQL_FLAVOR_ID,SQL_FLAVOR) 
VALUES  ( 2,'ORACLE PL/SQL');

Commit;





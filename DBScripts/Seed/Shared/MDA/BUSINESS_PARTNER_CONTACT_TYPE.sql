set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for BUSINESS_PARTNER_CONTACT_TYPE

INSERT INTO BUSINESS_PARTNER_CONTACT_TYPE ( CONTACT_TYPE,DESCRIPTION) 
VALUES  ( 1,'Direct Delivery');

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_SIZE_ENCODING

INSERT INTO CL_SIZE_ENCODING ( SIZE_ENCODING_ID,SIZE_ENCODING_NAME) 
VALUES  ( 1,'ASCII');

INSERT INTO CL_SIZE_ENCODING ( SIZE_ENCODING_ID,SIZE_ENCODING_NAME) 
VALUES  ( 2,'Binary');

Commit;





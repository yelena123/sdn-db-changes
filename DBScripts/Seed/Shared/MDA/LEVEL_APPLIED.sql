set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for LEVEL_APPLIED

INSERT INTO LEVEL_APPLIED ( LEVEL_APPLIED,DESCRIPTION) 
VALUES  ( 1,'Global');

INSERT INTO LEVEL_APPLIED ( LEVEL_APPLIED,DESCRIPTION) 
VALUES  ( 2,'Facility/Direction');

INSERT INTO LEVEL_APPLIED ( LEVEL_APPLIED,DESCRIPTION) 
VALUES  ( 3,'Routing Lane');

INSERT INTO LEVEL_APPLIED ( LEVEL_APPLIED,DESCRIPTION) 
VALUES  ( 4,'Facility');

Commit;





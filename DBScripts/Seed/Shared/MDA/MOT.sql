set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for MOT

INSERT INTO MOT ( TC_COMPANY_ID,MOT,DESCRIPTION,TRACK_COMMITMENT,ALLOW_CM,ALLOW_CFMF,IS_CONS_DEFAULT,IS_MULTI_STOP,TIME_CALC_METHOD,MARK_FOR_DELETION,
IS_PARCEL,IS_POOLPOINT_OPERATOR_MODE,STANDARD_MOT,MERCHANDISE_AMOUNT,CARRIER_ID,REP_RATE_CARRIER_ID,MOT_ID,REQUIRES_BOOKING,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'PCL','Parcel',0,0,0,0,0,'TF',0,
1,0,28,null,null,null,1,0,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO MOT ( TC_COMPANY_ID,MOT,DESCRIPTION,TRACK_COMMITMENT,ALLOW_CM,ALLOW_CFMF,IS_CONS_DEFAULT,IS_MULTI_STOP,TIME_CALC_METHOD,MARK_FOR_DELETION,
IS_PARCEL,IS_POOLPOINT_OPERATOR_MODE,STANDARD_MOT,MERCHANDISE_AMOUNT,CARRIER_ID,REP_RATE_CARRIER_ID,MOT_ID,REQUIRES_BOOKING,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'CWT','Hundredweight',0,0,0,0,0,'TF',0,
1,0,32,null,null,null,2,0,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO MOT ( TC_COMPANY_ID,MOT,DESCRIPTION,TRACK_COMMITMENT,ALLOW_CM,ALLOW_CFMF,IS_CONS_DEFAULT,IS_MULTI_STOP,TIME_CALC_METHOD,MARK_FOR_DELETION,
IS_PARCEL,IS_POOLPOINT_OPERATOR_MODE,STANDARD_MOT,MERCHANDISE_AMOUNT,CARRIER_ID,REP_RATE_CARRIER_ID,MOT_ID,REQUIRES_BOOKING,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'LTL','Less than Truck Load',0,0,0,0,0,'SS',0,
0,0,4,null,null,null,3,0,SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO MOT ( TC_COMPANY_ID,MOT,DESCRIPTION,TRACK_COMMITMENT,ALLOW_CM,ALLOW_CFMF,IS_CONS_DEFAULT,IS_MULTI_STOP,TIME_CALC_METHOD,MARK_FOR_DELETION,
IS_PARCEL,IS_POOLPOINT_OPERATOR_MODE,STANDARD_MOT,MERCHANDISE_AMOUNT,CARRIER_ID,REP_RATE_CARRIER_ID,MOT_ID,REQUIRES_BOOKING,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'TL','Truck load',0,0,0,0,0,'SS',0,
0,0,8,null,null,null,4,0,SYSTIMESTAMP,SYSTIMESTAMP);

Commit;





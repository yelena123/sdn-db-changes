set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_DIRECTION

INSERT INTO CL_DIRECTION ( DIRECTION_ID,DIRECTION_NAME) 
VALUES  ( 1,'Receiver');

INSERT INTO CL_DIRECTION ( DIRECTION_ID,DIRECTION_NAME) 
VALUES  ( 2,'Sender');

Commit;





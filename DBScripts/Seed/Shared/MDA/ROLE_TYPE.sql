set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for ROLE_TYPE

INSERT INTO ROLE_TYPE ( ROLE_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 4,'ROLE',SYSDATE,systimestamp);

INSERT INTO ROLE_TYPE ( ROLE_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 8,'TEMPLATE',SYSDATE,systimestamp);

Commit;





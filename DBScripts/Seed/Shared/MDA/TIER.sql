set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for TIER

INSERT INTO TIER ( TIER_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '1','Tier 1',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO TIER ( TIER_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '2','Tier 2',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO TIER ( TIER_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '3','Tier 3',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO TIER ( TIER_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '4','Tier 4',SYSTIMESTAMP,SYSTIMESTAMP);

INSERT INTO TIER ( TIER_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( '5','Tier 5',SYSTIMESTAMP,SYSTIMESTAMP);

Commit;





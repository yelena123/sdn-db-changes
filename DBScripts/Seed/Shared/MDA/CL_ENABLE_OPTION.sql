set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_ENABLE_OPTION

INSERT INTO CL_ENABLE_OPTION ( ENABLE_OPTION_ID,ENABLE_OPTION_NAME) 
VALUES  ( 1,'Disable');

INSERT INTO CL_ENABLE_OPTION ( ENABLE_OPTION_ID,ENABLE_OPTION_NAME) 
VALUES  ( 2,'Enable');

Commit;





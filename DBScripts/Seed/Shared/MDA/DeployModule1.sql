-- &1 => product schema name
set define on echo on
@@MDA_Tables_PKs.sql
@@MDA_Tables_Column_Comments.sql
@@MDA_Sequences.sql
@@MDA_Synonyms.sql
@@MDA_Views.sql
@@MDA_StaticData.sql

exit

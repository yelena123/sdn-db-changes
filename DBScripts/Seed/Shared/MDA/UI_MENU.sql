set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for UI_MENU

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 7,'56637',0,'Customize','/lps/resources/menu/ribbon/images/customize.gif','/lps/customization/customize.jsflps',null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 8,'56637',0,'Report','/lps/resources/menu/images/report.gif',null,'instantReport1()');

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 10,'56637',0,'Print','/lps/resources/menu/ribbon/images/print.gif',null,'printPage()');

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 1,'56637',0,'Actions','/lps/resources/menu/ribbon/images/action1d.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 2,'56637',0,'Links','/lps/resources/menu/ribbon/images/linkd.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 3,'56637',0,'ConfigTool','/lps/resources/menu/ribbon/images/tools.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 4,'56637',0,'Tools','/lps/resources/menu/ribbon/images/tools.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 5,'56637',0,'Detail','/lps/resources/menu/ribbon/images/linkd.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 6,'56637',0,'Delete','/lps/resources/menu/ribbon/images/action1d.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 11,'56637',0,'Help','/lps/resources/menu/ribbon/images/help.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 13,'56637',0,'Test','/lps/resources/menu/ribbon/images/action1d.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 14,'56637',0,'Tools','/lps/resources/menu/ribbon/images/tools.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 15,'56637',0,'System','/lps/resources/menu/ribbon/images/customize.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 16,'56638',0,'Customize','/lps/resources/menu/ribbon/images/customize.gif','/lps/customization/customize.jsflps',null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 17,'56638',0,'Actions','/lps/resources/menu/ribbon/images/action1d.gif',null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 18,'56637',0,'RFMenu',null,null,null);

INSERT INTO UI_MENU ( MENU_ID,USER_ID,IS_DISPLAY,DISPLAY_TEXT,IMAGE_URL,BUTTON_HREF,BUTTON_JS) 
VALUES  ( 3069,null,0,'Change Print Requestor','/lps/resources/menu/images/printReq.png',null,null);

Commit;





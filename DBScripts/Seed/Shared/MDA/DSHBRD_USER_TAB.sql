set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DSHBRD_USER_TAB

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 61,43,'LM Supervisor Dashboard',3,2,1,'LMADMIN',SYSTIMESTAMP,1,'LMADMIN',
SYSTIMESTAMP,1);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 41,21,'Transportation Lifecycle Management',1,3,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 42,22,'Receiving-Summary',1,1,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 43,22,'Inbound-Summary',2,1,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 44,22,'Orders-Summary',3,1,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 45,22,'Replenishment-Summary',4,1,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 46,22,'Restocking-Summary',5,1,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 47,22,'Picking-Summary',6,1,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 48,22,'Shipping-Summary',7,1,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 49,22,'Wave-Summary',8,1,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 50,22,'QA-Summary',9,1,1,null,SYSTIMESTAMP,1,null,
SYSTIMESTAMP,0);

INSERT INTO DSHBRD_USER_TAB ( DSHBRD_USER_TAB_ID,DASHBOARD_ID,TAB_NAME,TAB_SEQ,LAYOUT_TYPE_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,
LAST_UPDATED_DTTM,IS_MOBILE) 
VALUES  ( 51,43,'LM Dashboard',1,2,1,'LMADMIN',SYSTIMESTAMP,1,'LMADMIN',
SYSTIMESTAMP,0);

Commit;





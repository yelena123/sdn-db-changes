set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for BINDING_TYPE

INSERT INTO BINDING_TYPE ( BINDING_TYPE_ID,TYPE_NAME,BINDING_HANDLER,NAMESPACE_URI,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'pojo','com.manh.bpe.xml.parser.wsdl.POJOBindingParser','http://www.manh.com/Resources/Schemas/wsdl/binding/pojo',SYSDATE,systimestamp);

INSERT INTO BINDING_TYPE ( BINDING_TYPE_ID,TYPE_NAME,BINDING_HANDLER,NAMESPACE_URI,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'ejb','com.manh.bpe.xml.parser.wsdl.EJBBindingParser','http://www.manh.com/Resources/Schemas/wsdl/binding/ejb',SYSDATE,systimestamp);

INSERT INTO BINDING_TYPE ( BINDING_TYPE_ID,TYPE_NAME,BINDING_HANDLER,NAMESPACE_URI,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 3,'soap','com.manh.bpe.xml.parser.wsdl.SOAPBindingParser','http://schemas.xmlsoap.org/wsdl/soap/',SYSDATE,systimestamp);

Commit;





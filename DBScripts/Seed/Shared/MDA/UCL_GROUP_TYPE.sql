set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for UCL_GROUP_TYPE

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 130,'Report Management',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 128,'Store Commerce Activation',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 104,'Advanced Planning',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 108,'Carrier Management',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 112,'Test Director',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 29,'Dispatch',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 40,'Extended Enterprise Management',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 32,'RLMUsers',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 36,'Warehouse Management',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'UCLUsers',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'SAUsers',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 4,'OptiBid',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 8,'Network Dashboard',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 12,'Freight Selector',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 24,'OptiManage',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 28,'Distributed Order Management',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 44,'Common Business Object',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 60,'Labor Management',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 64,'Replenishment',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 68,'Forecasting',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 84,'Yard Management',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 92,'Total Cost to Serve',SYSDATE,systimestamp);

INSERT INTO UCL_GROUP_TYPE ( UCL_GROUP_TYPE_ID,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 96,'Dashboard',SYSDATE,systimestamp);

Commit;





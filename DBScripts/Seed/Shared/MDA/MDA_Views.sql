SET DEFINE OFF;
SET ECHO ON;

CREATE OR REPLACE FORCE VIEW "VIEW_LRF_EVENTS" ("EVENT_ID", "LRF_EVENT_ID", "LRF_EVENT_NAME", "GROUP_ID", "EVENT_START_DTTM", "EVENT_EXP_DATE", "OM_CATEGORY", "EVENT_TIMESTAMP", "EVENT_FREQ_IN_DAYS", "EVENT_FREQ_PER_DAY", "EXECUTED_DTTM", "IS_EXECUTED", "EVENT_OBJECTS", "EVENT_TYPE", "EVENT_CNT", "EVENT_FREQ_IN_DAY_OF_MONTH", "FREQ_TYPE", "FREQ_VAL_1", "FREQ_VAL_2", "SCHEDULED_DTTM", "SCHEDULED_TIME", "CREATED_DTTM", "MODIFIED_DTTM", "CREATED_BY", "MODIFIED_BY", "PREVIOUS_SCHEDULED_DTTM", "EXCLUDE_WEEKEND") AS
(select om.event_id,
       lrf_event_id,
       lrf_event_name,
       group_id,
       event_start_dttm,
       event_exp_date,
       om_category,
       event_timestamp,
       event_freq_in_days,
       event_freq_per_day,
       executed_dttm,
       is_executed,
       event_objects,
       event_type,
       event_cnt,
       event_freq_in_day_of_month,
       freq_type,
       freq_val_1,
       freq_val_2,
       scheduled_dttm,
       scheduled_time,
       lrf.created_dttm,
       modified_dttm,
       created_by,
       modified_by,
       previous_scheduled_dttm,
       exclude_weekend
  from lrf_event lrf, lrf_sched_event_report om
 where lrf.event_id = om.event_id);

CREATE OR REPLACE FORCE VIEW "VW_DB_BUILD_MAX_VERSION" ("VERSION_LABEL", "LAYER", "MAJOR", "MINOR", "REV1", "REV2", "REV3", "START_DTTM") AS
SELECT version_label,
        REGEXP_SUBSTR (version_label,
                       '[^_]+',
                       1,
                       1)
           layer,
        TO_NUMBER (REGEXP_SUBSTR (fullver,
                                  '[^.]+',
                                  1,
                                  1))
           major,
        TO_NUMBER (REGEXP_SUBSTR (fullver,
                                  '[^.]+',
                                  1,
                                  2))
           minor,
        TO_NUMBER (REGEXP_REPLACE (REGEXP_SUBSTR (fullver,
                                                  '[^.]+',
                                                  1,
                                                  3),
                                   '[A-Za-z]*',
                                   '')
                   * 1)
           rev1,
        TO_NUMBER (COALESCE (REGEXP_SUBSTR (fullver,
                                            '[^.]+',
                                            1,
                                            4),
                             '0'))
           rev2,
        TO_NUMBER (COALESCE (REGEXP_SUBSTR (fullver,
                                            '[^.]+',
                                            1,
                                            5),
                             '0'))
           rev3,
        start_dttm
   FROM (SELECT version_label,
                REGEXP_SUBSTR (version_label,
                               '[^_]+',
                               1,
                               2)
                   fullver,
                start_dttm
           FROM db_build_history
          WHERE    version_label LIKE 'SCPP%'
                OR version_label LIKE 'MDA%'
                OR version_label LIKE 'CBO%'
                OR version_label LIKE 'LMARCH%'
                OR (version_label LIKE 'WM%'))
 ORDER BY 2 DESC,
        3 DESC,
        4 DESC,
        5 DESC NULLS LAST,
        6 DESC NULLS LAST,
        7 DESC NULLS LAST,
        1 DESC;

CREATE OR REPLACE FORCE VIEW "NAVIGATION_VIEW" ("PARENT_TITLE", "NAVIGATION_ID", "PARENT_ID", "LEVEL", "NAV_TITLE", "URL", "TYPE", "SIDE_NAV", "UDEF_SEQ") AS
select p.title parent_title, t."NAVIGATION_ID",t."PARENT_ID",t."LEVEL",t."NAV_TITLE",t."URL",t."TYPE",t."SIDE_NAV",t."UDEF_SEQ"
from navigation p right outer join (
              select navigation_id, parent_id, level, title nav_title, url,type,side_nav,udef_seq
              from navigation
              start with parent_id is null
              connect by prior navigation_id = parent_id
              ) t
on (p.navigation_id = t.parent_id);

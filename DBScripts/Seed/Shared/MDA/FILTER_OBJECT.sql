set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for FILTER_OBJECT

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'RF_BIND_KEYS_FILTER',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'TRANLOG_IFSE',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LISTENER',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'ROUTER',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'TEMPLATE_UCL',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LPRF_REPORTS_PRT_REVIEW',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'USER_GROUP',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'UCL_REGION',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'FL_FILTER',4,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LOCK',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DESTINATION_MAPPING',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'UCL_RELATIONSHIP',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'MSG_TYPES',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'MSG_PROCESS_CONFIG',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'MESSAGESTORE_INBOUND',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'MESSAGESTORE_OUTBOUND',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'MOCK_SHIPMENT_DEMO',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'RLM_CHANNEL',4,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'ITEM_MASTER',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COMPANY',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'USER',null,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'ROLE',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'RELATIONSHIP',null,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LOCATION_UCL',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LPRF_REPORT_DEF',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LPRF_REPORTS_SCHD',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LPRF_REPORTS_SAVED',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LPRF_REPORTS_PRT_QUEUE',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LPRF_PRT_QUEUE_MONITOR',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'LPRF_PRT_REQUESTOR',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'UCL_USER',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DASHBOARD',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DSHBRD_PORTLET',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'SCOPE_LABEL',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'SCOPE_LITERAL',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'SCOPE_MESSAGE_MASTER',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'SERVICE_REPOSITORY',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COMPOSITE_SERVICE',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'SCRIPT_OBJECT',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DIF_ENDPOINT',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DIF_EVENT_MASTER',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DIF_ENGINE',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DIF_CL_MESSAGE',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DIF_CL_MACHINE',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'SYSTEM_ADMIN',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'COMPANY_PARAM',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'BD_SERVICE_LEVEL',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'BD_MODE',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'EXP_SCHD_EVENT',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CLASSIFICATION CODE',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CBO_RCL_FILTER',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CBO_RCR_FILTER',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CURRENCY_EXCHANGE_RATES',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CURRENCY_HISTORY_RATES',8,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'REGION LIST',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'ERROR_LOG',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DD_TRANSACTION',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'CURRENCY',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'IMPORT_DATA_LOADER',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'MPS_FILTER_LAYOUT',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'POSTAL CODE',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'SCREEN_TYPE',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'WM_TRAN_MAINTENANCE',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'WM_PROGRAM_PARAMETERS',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DD_DETAIL_LOG',12,SYSDATE,systimestamp);

INSERT INTO FILTER_OBJECT ( FILTER_OBJECT,SCREEN_TYPE,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 'DD_LOG',12,SYSDATE,systimestamp);

Commit;





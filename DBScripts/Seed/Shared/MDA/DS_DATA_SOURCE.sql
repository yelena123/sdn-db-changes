set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for DS_DATA_SOURCE

INSERT INTO DS_DATA_SOURCE ( DATA_SOURCE_ID,APP_INST_SHORT_NAME,DATA_SOURCE_NAME,DATA_SOURCE_TYPE_ID,CONNECTION_URL,DRIVER_CLASS,USER_NAME,PASSWORD,MAX_POOL_SIZE,CONNECTION_PROPERTIES,
CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 41,'lm',null,1,'jdbc:oracle:thin:@<host>:<port>:<sid>','oracle.jdbc.driver.OracleDriver','dbusername','C250AC3763654CA727672D4E622FEB5E',50,null,
SYSDATE,systimestamp);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for SUFFIX

INSERT INTO SUFFIX ( SUFFIX_DESC) 
VALUES  ( 'CAPS');

INSERT INTO SUFFIX ( SUFFIX_DESC) 
VALUES  ( 'CASE');

INSERT INTO SUFFIX ( SUFFIX_DESC) 
VALUES  ( 'SHORT');

Commit;





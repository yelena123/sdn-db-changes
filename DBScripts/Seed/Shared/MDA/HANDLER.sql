set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for HANDLER

INSERT INTO HANDLER ( HANDLER,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 0,'Carrier',SYSDATE,systimestamp);

INSERT INTO HANDLER ( HANDLER,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'Lumper',SYSDATE,systimestamp);

INSERT INTO HANDLER ( HANDLER,DESCRIPTION,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'Stop Owner',SYSDATE,systimestamp);

Commit;





set define off;
set echo on;
set sqlblanklines on;
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';

-- Seed Data Inserts for CL_SERVER_TYPE

INSERT INTO CL_SERVER_TYPE ( SERVER_TYPE_ID,SERVER_TYPE_NAME,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 1,'AutoDetect',SYSDATE,systimestamp);

INSERT INTO CL_SERVER_TYPE ( SERVER_TYPE_ID,SERVER_TYPE_NAME,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES  ( 2,'Custom',SYSDATE,systimestamp);

Commit;





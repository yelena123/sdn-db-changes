SET SCAN OFF;
SET DEFINE OFF;
SET ECHO ON;

-- DBTicket DD-SYNC

CREATE OR REPLACE FUNCTION get_col_list (p_table_name IN VARCHAR2)
      RETURN CLOB
   IS
      v_col_list   CLOB;
      v_obj_type   VARCHAR2 (20);

	      CURSOR cur_get_col_list
	      IS
			 (SELECT column_name, column_id
		   FROM user_tab_columns
		  WHERE table_name = UPPER (p_table_name)
			)
		ORDER BY column_id;
BEGIN
      FOR currec IN cur_get_col_list
      LOOP
         IF (currec.column_id = 1)
         THEN
            v_col_list := currec.column_name;
         ELSE
            v_col_list := v_col_list || ',' || currec.column_name;
         END IF;
      END LOOP;

      RETURN v_col_list;
EXCEPTION
      WHEN OTHERS
      THEN
	 dbms_output.put_line('ORA- Error occured while getting column list for table => ' || p_table_name);
         ROLLBACK;
END get_col_list;
/

CREATE OR REPLACE PROCEDURE SYNC_DD_TABLES(p_copy_table_name IN VARCHAR2) IS
        v_dblink_name 		VARCHAR2(30) := '@MDA' ;
        t_col_list 		CLOB;
BEGIN
          t_col_list := get_col_list (upper(p_copy_table_name));
         
	           execute immediate 'DELETE FROM '||p_copy_table_name;
                   execute immediate 'INSERT INTO '
 	                   || p_copy_table_name 
 	                   || ' ( '
 	                   || t_col_list
 	                   || ') SELECT '
 	                   || t_col_list
 	                   || ' FROM '
 	                   ||  p_copy_table_name
			   || v_dblink_name;
         
	 COMMIT;
 
EXCEPTION WHEN OTHERS
 	 THEN
	 DBMS_OUTPUT.PUT_LINE(SQLERRM);
  	   ROLLBACK;            
END SYNC_DD_TABLES; 
/

CREATE OR REPLACE PROCEDURE DD_REPLICATED_TABLES_SYNC
IS
BEGIN
   FOR i IN (  SELECT TABLE_NAME
                 FROM DD_REPLICATED_TABLES@MDA
             ORDER BY 1)
   LOOP
      SYNC_DD_TABLES (i.TABLE_NAME);
   END LOOP;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (SQLERRM);
END;
/



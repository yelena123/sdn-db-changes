-- DBTicket DBA-VERSCTRL REFL

alter table ACCESS_CONTROL add (version_from number default -1 not null, version_to number default -1 not null);
alter table APP add (version_from number default -1 not null, version_to number default -1 not null);
alter table APP_INSTANCE add (version_from number default -1 not null, version_to number default -1 not null);  
alter table APP_MODULE add (version_from number default -1 not null, version_to number default -1 not null);    
alter table APP_MOD_PERM add (version_from number default -1 not null, version_to number default -1 not null);  
alter table BUSINESS_PARTNER_TYPE add (version_from number default -1 not null, version_to number default -1 not null); 
alter table CHANNEL add (version_from number default -1 not null, version_to number default -1 not null);   
alter table CHANNEL_TYPE_UCL add (version_from number default -1 not null, version_to number default -1 not null); 
alter table COMM_METHOD_UCL add (version_from number default -1 not null, version_to number default -1 not null);
alter table COMPANY add (version_from number default -1 not null, version_to number default -1 not null); 
alter table COMPANY_APP add (version_from number default -1 not null, version_to number default -1 not null);   
alter table COMPANY_APP_MODULE add (version_from number default -1 not null, version_to number default -1 not null);  
alter table COMPANY_PARAM add (version_from number default -1 not null, version_to number default -1 not null);  
alter table COMPANY_TYPE add (version_from number default -1 not null, version_to number default -1 not null);  
alter table COMPANY_TYPE_PERM add (version_from number default -1 not null, version_to number default -1 not null);
alter table COUNTRY add (version_from number default -1 not null, version_to number default -1 not null);  
alter table DSHBRD_COGNOS_PTLT_DTL add (version_from number default -1 not null, version_to number default -1 not null);   
alter table DSHBRD_PORTLET add (version_from number default -1 not null, version_to number default -1 not null);
alter table DSHBRD_PORTLET_PARAM_DEF add (version_from number default -1 not null, version_to number default -1 not null); 
alter table DSHBRD_PORTLET_PERMISSION add (version_from number default -1 not null, version_to number default -1 not null);
alter table DSHBRD_PORTLET_TAGS add (version_from number default -1 not null, version_to number default -1 not null); 
alter table DSHBRD_UIB_PTLT_DTL add (version_from number default -1 not null, version_to number default -1 not null); 
alter table DS_DATA_SOURCE add (version_from number default -1 not null, version_to number default -1 not null);
alter table LANGUAGE add (version_from number default -1 not null, version_to number default -1 not null); 
alter table LOCALE add (version_from number default -1 not null, version_to number default -1 not null);   
alter table LOCATION_SCHEDULE add (version_from number default -1 not null, version_to number default -1 not null);   
alter table LOCATION_UCL add (version_from number default -1 not null, version_to number default -1 not null);  
alter table MODULE add (version_from number default -1 not null, version_to number default -1 not null);   
alter table PERMISSION add (version_from number default -1 not null, version_to number default -1 not null);    
alter table PERMISSION_INHERITANCE add (version_from number default -1 not null, version_to number default -1 not null);   
alter table REGION add (version_from number default -1 not null, version_to number default -1 not null);   
alter table RELATIONSHIP add (version_from number default -1 not null, version_to number default -1 not null);  
alter table RELATIONSHIP_TYPE add (version_from number default -1 not null, version_to number default -1 not null);   
alter table RELTP_APP_MOD_PERM add (version_from number default -1 not null, version_to number default -1 not null);  
alter table REL_APP_MOD_PERM add (version_from number default -1 not null, version_to number default -1 not null);    
alter table REL_GEO_REGION add (version_from number default -1 not null, version_to number default -1 not null);
alter table ROLE add (version_from number default -1 not null, version_to number default -1 not null);
alter table ROLE_APP_MOD_PERM add (version_from number default -1 not null, version_to number default -1 not null);   
alter table ROLE_TEMPLATE_REFERENCE add (version_from number default -1 not null, version_to number default -1 not null);  
alter table ROLE_TYPE add (version_from number default -1 not null, version_to number default -1 not null);
alter table SOURCE_TYPE add (version_from number default -1 not null, version_to number default -1 not null);   
alter table STATE_PROV add (version_from number default -1 not null, version_to number default -1 not null);    
alter table TIME_ZONE add (version_from number default -1 not null, version_to number default -1 not null);
alter table UCL_GROUP_TYPE add (version_from number default -1 not null, version_to number default -1 not null);
alter table UCL_PARAM_DEF add (version_from number default -1 not null, version_to number default -1 not null); 
alter table UCL_USER add (version_from number default -1 not null, version_to number default -1 not null); 
alter table UCL_USER_FUNCTION add (version_from number default -1 not null, version_to number default -1 not null);   
alter table UIB_QUERY add (version_from number default -1 not null, version_to number default -1 not null);
alter table UIB_QUERY_DETAILS add (version_from number default -1 not null, version_to number default -1 not null);   
alter table UIB_SMRY_VIEW_DTL add (version_from number default -1 not null, version_to number default -1 not null);   
alter table UIB_SP_DETAILS add (version_from number default -1 not null, version_to number default -1 not null);
alter table UIB_VIEW_PROPERTY_SETS add (version_from number default -1 not null, version_to number default -1 not null);   
alter table UIB_VIEW_PROP_SET_DTL add (version_from number default -1 not null, version_to number default -1 not null);    
alter table USER_DEFAULT add (version_from number default -1 not null, version_to number default -1 not null);  
alter table USER_FUNCTION add (version_from number default -1 not null, version_to number default -1 not null); 
alter table USER_GROUP add (version_from number default -1 not null, version_to number default -1 not null);    
alter table USER_TYPE add (version_from number default -1 not null, version_to number default -1 not null);
alter table USER_TYPE_FUNCTION add (version_from number default -1 not null, version_to number default -1 not null);  
alter table VENDOR_ROLE_ASSIGNMENT add (version_from number default -1 not null, version_to number default -1 not null);   
alter table XCOMPANY_GROUP add (version_from number default -1 not null, version_to number default -1 not null);
alter table XCOMPANY_GROUP_COMPANY add (version_from number default -1 not null, version_to number default -1 not null);   
alter table XCOMPANY_GROUP_RESTRICTION add (version_from number default -1 not null, version_to number default -1 not null);  
alter table postal_code add (version_from number default -1 not null, version_to number default -1 not null);  
alter table XCOMPANY_RESTRICTION add (version_from number default -1 not null, version_to number default -1 not null);
--alter table XFIELD_RESTRICTION add (version_from number default -1 not null, version_to number default -1 not null);  
alter table XRESTRICTION add (version_from number default -1 not null, version_to number default -1 not null);  
alter table XSOLUTION add (version_from number default -1 not null, version_to number default -1 not null);
alter table XSOLUTION_APP add (version_from number default -1 not null, version_to number default -1 not null);
alter table XSOLUTION_RESTRICTION add (version_from number default -1 not null, version_to number default -1 not null);  
alter table XSOLUTION_USER add (version_from number default -1 not null, version_to number default -1 not null);   
alter table XSOLUTION_XREF add (version_from number default -1 not null, version_to number default -1 not null);   
alter table XUSER_GROUP add (version_from number default -1 not null, version_to number default -1 not null); 
alter table XUSER_GROUP_RESTRICTION add (version_from number default -1 not null, version_to number default -1 not null);
alter table XUSER_GROUP_USER add (version_from number default -1 not null, version_to number default -1 not null); 
alter table XUSER_RESTRICTION add (version_from number default -1 not null, version_to number default -1 not null); 

---- Adding Column Comments
BEGIN
   FOR i
      IN (SELECT ucc.table_name, ucc.column_name
            FROM user_tables ut, user_col_comments ucc
           WHERE     ut.table_name = ucc.table_name
                 AND ucc.comments IS NULL
                 AND ut.temporary = 'N'
                 AND ucc.column_name IN ('VERSION_FROM'))
   LOOP
      EXECUTE IMMEDIATE
            'COMMENT ON COLUMN '
         || i.table_name
         || '.'
         || i.column_name
         || '  IS '
         || '''Record Valid from Release Number''';
   END LOOP;

   FOR i
      IN (SELECT ucc.table_name, ucc.column_name
            FROM user_tables ut, user_col_comments ucc
           WHERE     ut.table_name = ucc.table_name
                 AND ucc.comments IS NULL
                 AND ut.temporary = 'N'
                 AND ucc.column_name IN ('VERSION_TO'))
   LOOP
      EXECUTE IMMEDIATE
            'COMMENT ON COLUMN '
         || i.table_name
         || '.'
         || i.column_name
         || '  IS '
         || '''Record Valid till Release Number''';
   END LOOP;
END;
/



-- DBTicket DB-2097 REFL

alter table SECURITY_POLICY_GROUP add (version_from number default -1 not null, version_to number default -1 not null);

COMMENT ON COLUMN SECURITY_POLICY_GROUP.VERSION_FROM is 'Record Valid from Release Number';
COMMENT ON COLUMN SECURITY_POLICY_GROUP.VERSION_TO is 'Record Valid till Release Number';

alter table SECURITY_PARAM add (version_from number default -1 not null, version_to number default -1 not null);

COMMENT ON COLUMN SECURITY_PARAM.VERSION_FROM is 'Record Valid from Release Number';
COMMENT ON COLUMN SECURITY_PARAM.VERSION_TO is 'Record Valid till Release Number';

COMMIT;

-- DBTicket DB-3897 REFL

update app set version_from=2016 where app_id=136 and app_name='Slotting';

commit;

-- DBTicket DB-6472 REFL

UPDATE ACCESS_CONTROL SET VERSION_TO=2015 where role_id = (SELECT ROLE_ID FROM ROLE WHERE IS_DEFAULT=1 AND COMPANY_ID=0) AND COMPANY_ID=0;
UPDATE ACCESS_CONTROL SET VERSION_TO=2015 WHERE ROLE_ID IN( SELECT  ROLE_ID FROM ROLE WHERE COMPANY_ID=0 AND ROLE_NAME IN ('Manage SA User','Manage Location','Manage Geo Region','Admin Company','Manage Company','SYSADMNDEFAULPERM','DFLTSYSADMINROLE','Default System Administrator Role'));

UPDATE ACCESS_CONTROL
set VERSION_FROM=2016
where (ROLE_ID,UCL_USER_ID) in
(SELECT 
    DATA2.ROLE_ID,U.UCL_USER_ID
  FROM
    (SELECT DATA1.UCL_USER_ID,
      DATA1.ROLE_ID,
      DATA1.ROLE_NAME,
      DATA1.USER_FUNCTION_ID
    FROM
      (SELECT P.UCL_USER_ID,
        P.USER_FUNCTION_ID,
        R.ROLE_NAME,
        R.ROLE_ID
      FROM
        (SELECT USER_FUNCTION.USER_FUNCTION_ID,
          UF.UCL_USER_ID ,
          DESCRIPTION AS PATTERN
        FROM USER_FUNCTION,
          UCL_USER_FUNCTION UF
        WHERE UF.USER_FUNCTION_ID=USER_FUNCTION.USER_FUNCTION_ID
        ) P,
        ROLE R
      WHERE R.ROLE_NAME    = P.PATTERN
      AND R.COMPANY_ID     = 0
      AND R.ROLE_NAME NOT IN ('Facilitate', 'SA View Company', 'CA Admin Own Company')
      ) DATA1
    ) DATA2,
    UCL_USER U
  WHERE COMPANY_ID     = 0
  AND U.UCL_USER_ID    = DATA2.UCL_USER_ID
  AND DATA2.ROLE_NAME IS NOT NULL
  );
  
UPDATE ACCESS_CONTROL
SET VERSION_TO=2015
WHERE UCL_USER_ID=
  (SELECT UCL_USER_ID
  FROM UCL_USER U
  WHERE USER_NAME  ='system'
  AND USER_TYPE_ID =12
  AND u.COMPANY_ID =0
  );
  
UPDATE ACCESS_CONTROL
SET VERSION_FROM=2016
where (ROLE_ID,UCL_USER_ID,COMPANY_ID) in
(SELECT R.ROLE_ID,U.UCL_USER_ID,R.COMPANY_ID
  FROM Role R,
    UCL_USER U
  WHERE USER_NAME  ='system'
  AND USER_TYPE_ID =12
  AND R.COMPANY_ID =0
  AND R.IS_DEFAULT = 1
  AND R.COMPANY_ID =U.COMPANY_ID
 );
    
UPDATE ACCESS_CONTROL
SET VERSION_FROM=2016
where (ROLE_ID,UCL_USER_ID,company_id) IN
(
  SELECT R.ROLE_ID,
	U.UCL_USER_ID,
    data.company_id    
  FROM Role R,
    UCL_USER U,
    (SELECT C.COMPANY_ID ,
      (SELECT COMPANY_ID
      FROM COMPANY
      WHERE PARENT_COMPANY_ID =-1
        START WITH COMPANY_ID = C.COMPANY_ID
        CONNECT BY COMPANY_ID = PRIOR PARENT_COMPANY_ID
      AND PARENT_COMPANY_ID  <> PRIOR COMPANY_ID
      ) AS ROOT_ID
    FROM COMPANY C
    WHERE C.company_id<>0
    ) DATA
  WHERE DATA.ROOT_ID = R.COMPANY_ID
  AND U.COMPANY_ID   = DATA.COMPANY_ID
  AND USER_TYPE_ID   =4
  AND R.IS_DEFAULT   = 1
  ) ;
  
UPDATE ACCESS_CONTROL
SET VERSION_FROM=2016
where (UCL_USER_ID,ROLE_ID,BUSINESS_UNIT_ID,IS_INTERNAL_CONTROL,COMPANY_ID) IN
 (SELECT UCL_USER_ID,
      (SELECT ROLE_ID
      FROM ROLE
      WHERE ROLE_NAME='Miscellaneous Administration'
      AND COMPANY_ID = 0
      ) ROLE_ID,
      -1 BUSINESS_UNIT_ID,
      1 IS_INTERNAL_CONTROL,
      0 COMPANY_ID
    FROM UCL_USER
    WHERE COMPANY_ID = 0);
	
UPDATE ACCESS_CONTROL
SET VERSION_FROM=2016
where (UCL_USER_ID,ROLE_ID,BUSINESS_UNIT_ID,IS_INTERNAL_CONTROL,COMPANY_ID) IN
(SELECT UCL_USER_ID,
      (SELECT ROLE_ID FROM ROLE WHERE ROLE_NAME='View Company' AND COMPANY_ID = 0
      ) ROLE_ID,
      -1 BUSINESS_UNIT_ID,
      1 IS_INTERNAL_CONTROL,
      0 COMPANY_ID
    FROM UCL_USER
    WHERE COMPANY_ID = 0
    );	
	
COMMIT;

-- DBTicket DB-6995 REFL

EXEC QUIET_DROP ('INDEX','UCL_USER_USRNM_FNIDX');
CREATE INDEX UCL_USER_USRNM_FNIDX ON UCL_USER
(LOWER(USER_NAME),VERSION_FROM)
LOGGING TABLESPACE LEMA_TXN_IDX_TBS;

-- DBTicket DB-7056 REFL

EXEC QUIET_DROP ('INDEX','ROLEAMP_ROLE_VF_VT_IDX');
CREATE INDEX ROLEAMP_ROLE_VF_VT_IDX ON ROLE_APP_MOD_PERM
(VERSION_FROM,VERSION_TO)
LOGGING
TABLESPACE LEMA_TXN_IDX_TBS;

-- DBTicket DB-7182 REFL

CREATE INDEX TIME_ZONE_VF_IDX ON TIME_ZONE
(VERSION_FROM)
LOGGING
TABLESPACE LEMA_STAT_DT_TBS;

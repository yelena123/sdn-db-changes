
-- DBTicket TCS-253



exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');

MERGE INTO PERMISSION L
     USING (SELECT 'VCDIE' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) 
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,'View CDV Import Errors',1,-1,'VCDIE',3,'Manhattan Associates',systimestamp,3,'Manhattan Associates',systimestamp,null);
               
               
               
               MERGE INTO PERMISSION L
	            USING (SELECT 'ACDIE' PERMISSION_CODE FROM DUAL) B
	               ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
	       WHEN NOT MATCHED
	       THEN
	          INSERT     (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) 
	              VALUES (SEQ_PERMISSION_ID.NEXTVAL,'Admin CDV Import Errors',1,-1,'ACDIE',3,'Manhattan Associates',systimestamp,3,'Manhattan Associates',systimestamp,null);



delete from permission_inheritance where PARENT_PERMISSION_ID = (SELECT permission_id
                         FROM permission
                        WHERE permission_code = 'TIFE') and CHILD_PERMISSION_ID =
                          (SELECT permission_id
                             FROM permission
                            WHERE permission_code = 'VCDIE');
                            
delete from permission_inheritance where  PARENT_PERMISSION_ID =
                      (SELECT permission_id
                         FROM permission
                        WHERE permission_code = 'TIFE')
                   AND CHILD_PERMISSION_ID =
                          (SELECT permission_id
                             FROM permission
                            WHERE permission_code = 'ACDIE'); 





   INSERT into permission_inheritance    (PARENT_PERMISSION_ID, CHILD_PERMISSION_ID)
       VALUES ( (SELECT permission_id
                         FROM permission
                        WHERE permission_code = 'TIFE'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'VCDIE'));


   INSERT into permission_inheritance     (PARENT_PERMISSION_ID, CHILD_PERMISSION_ID)
       VALUES ( (SELECT permission_id
                         FROM permission
                        WHERE permission_code = 'TIFE'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ACDIE'));



Insert into permission (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) values (SEQ_PERMISSION_ID.NEXTVAL,'Assign Cost Drievrs',1,-1,'VACD',3,'Manhattan Associates',systimestamp,3,'Manhattan Associates',systimestamp,null);
Insert into permission_inheritance(parent_permission_id,child_permission_id) values((select permission_id from permission where PERMISSION_CODE='AUDC'),(select permission_id from permission where permission_code like 'VACD'));

Insert into permission (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) 
values (SEQ_PERMISSION_ID.NEXTVAL,'Administer Cost Drivers',1,-1,'ACD',3,'Manhattan Associates',systimestamp,3,'Manhattan Associates',systimestamp,null);

Insert into permission (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) 
values (SEQ_PERMISSION_ID.NEXTVAL,'View Cost Drivers',1,-1,'VCD',3,'Manhattan Associates',systimestamp,3,'Manhattan Associates',systimestamp,null);

Insert into permission_inheritance(parent_permission_id,child_permission_id) 
values((select permission_id from permission where PERMISSION_CODE='ACD'),(select permission_id from permission where permission_code like 'VCD'));

-- DBTicket TCS-2866

Insert into permission (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) values 
(SEQ_PERMISSION_ID.NEXTVAL,'View Cost Group',1,-1,'VCGS',3,'Manhattan Associates',SYSTIMESTAMP,3,'Manhattan Associates',SYSTIMESTAMP,null);

Insert into permission (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) values 
(SEQ_PERMISSION_ID.NEXTVAL,'Admin Cost Group',1,-1,'ACGS',3,'Manhattan Associates',SYSTIMESTAMP,3,'Manhattan Associates',SYSTIMESTAMP,null);

Insert into permission_inheritance(parent_permission_id,child_permission_id) values((select permission_id from permission where permission_code like 'ACGS'),(select permission_id from permission where permission_code like 'VCGS'));

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

insert into app_mod_perm(app_id,module_id,permission_id,row_uid) values((select app_id from app where app_name='Total Cost to Serve'),(select module_id from module where module_name='TCS'),(select permission_id from permission where permission_code='VCGS'),(SEQ_APP_MOD_PERM.nextval));

insert into app_mod_perm(app_id,module_id,permission_id,row_uid) values((select app_id from app where app_name='Total Cost to Serve'),(select module_id from module where module_name='TCS'),(select permission_id from permission where permission_code='ACGS'),(SEQ_APP_MOD_PERM.nextval));

-- DBTicket TCS-3083

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

 MERGE INTO APP_MOD_PERM AMP
  USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                PERMISSION.PERMISSION_CODE
           FROM APP, MODULE, PERMISSION
          WHERE APP.APP_NAME = 'Total Cost to Serve'
            AND MODULE.MODULE_CODE = 'TCS'
            AND PERMISSION.PERMISSION_CODE = 'ACD') APP1
  ON (    AMP.APP_ID = APP1.APP_ID
      AND AMP.MODULE_ID = APP1.MODULE_ID
      AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
  WHEN NOT MATCHED THEN
     INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
     VALUES ((select app_id from app where app_name='Total Cost to Serve'),
(select module_id from module where module_code='TCS'),
(select permission_id from permission where permission_code='ACD'),
(SEQ_APP_MOD_PERM.nextval));

 MERGE INTO APP_MOD_PERM AMP
  USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                PERMISSION.PERMISSION_CODE
           FROM APP, MODULE, PERMISSION
          WHERE APP.APP_NAME = 'Total Cost to Serve'
            AND MODULE.MODULE_CODE = 'TCS'
            AND PERMISSION.PERMISSION_CODE = 'VCD') APP1
  ON (    AMP.APP_ID = APP1.APP_ID
      AND AMP.MODULE_ID = APP1.MODULE_ID
      AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
  WHEN NOT MATCHED THEN
     INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
     VALUES ((select app_id from app where app_name='Total Cost to Serve'),
(select module_id from module where module_code='TCS'),
(select permission_id from permission where permission_code='VCD'),
(SEQ_APP_MOD_PERM.nextval));

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='ACD'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='VCD'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Total Cost to Serve'),
(select module_id from module where module_code='TCS'),(select permission_id from permission where permission_code='ACD'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Total Cost to Serve'),
(select module_id from module where module_code='TCS'),(select permission_id from permission where permission_code='VCD'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

--For Cost Groups
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='ACGS'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='VCGS'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Total Cost to Serve'),
(select module_id from module where module_code='TCS'),(select permission_id from permission where permission_code='ACGS'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Total Cost to Serve'),
(select module_id from module where module_code='TCS'),(select permission_id from permission where permission_code='VCGS'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);

-- DBTicket TCS-3091

Insert into permission (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) values 
(SEQ_PERMISSION_ID.NEXTVAL,'View Scheduler Configuration',1,-1,'VSCS',3,'Manhattan Associates',sysdate,3,'Manhattan Associates',sysdate,null);

Insert into permission (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) values 
(SEQ_PERMISSION_ID.NEXTVAL,'Admin Scheduler Configuration',1,-1,'ASCS',3,'Manhattan Associates',sysdate,3,'Manhattan Associates',sysdate,null);

Insert into permission_inheritance(parent_permission_id,child_permission_id) values((select permission_id from permission where permission_code like 'ASCS'),(select permission_id from permission where permission_code like 'VSCS'));

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

insert into app_mod_perm(app_id,module_id,permission_id,row_uid) values((select app_id from app where app_name='Total Cost to Serve'),(select module_id from module where module_name='TCS'),(select permission_id from permission where permission_code='VSCS'),(SEQ_APP_MOD_PERM.nextval));

insert into app_mod_perm(app_id,module_id,permission_id,row_uid) values((select app_id from app where app_name='Total Cost to Serve'),(select module_id from module where module_name='TCS'),(select permission_id from permission where permission_code='ASCS'),(SEQ_APP_MOD_PERM.nextval));

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='ASCS'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
VALUES ((select COMPANY_TYPE_ID from company_type 
where lower(DESCRIPTION) = lower('Shipper')), (select permission_id from permission where permission_code='VSCS'),
(select max(row_uid) from COMPANY_TYPE_PERM)+4);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where app_name='Total Cost to Serve'),
(select module_id from module where module_code='TCS'),(select permission_id from permission where permission_code='ASCS'),
(select max(row_uid) from RELTP_APP_MOD_PERM)+4,0);




-- DBTicket TCS-3095

MERGE INTO LABEL L
     USING (SELECT  'UCLPermissions_Administer Cost Drivers' KEY,'UCL' BUNDLE_NAME FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UCLPermissions_Administer Cost Drivers',
               'Administer Cost Drivers',
               'UCL');
	
MERGE INTO LABEL L
     USING (SELECT  'UCLPermissions_View Cost Drivers' KEY,'UCL' BUNDLE_NAME FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,'UCLPermissions_View Cost Drivers', 'View Cost Drivers', 'UCL');	
	   
MERGE INTO LABEL L
     USING (SELECT  'UCLPermissions_Admin Cost Group' KEY,'UCL' BUNDLE_NAME FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,'UCLPermissions_Admin Cost Group', 'Admin Cost Group', 'UCL');	

MERGE INTO LABEL L
     USING (SELECT 'UCLPermissions_View Cost Group' KEY,'UCL' BUNDLE_NAME FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,'UCLPermissions_View Cost Group', 'View Cost Group', 'UCL');


MERGE INTO LABEL L
     USING (SELECT 'UCLPermissions_Admin Scheduler Configuration' KEY,'UCL' BUNDLE_NAME FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,'UCLPermissions_Admin Scheduler Configuration', 'Admin Scheduler Configuration', 'UCL');
	   
MERGE INTO LABEL L
     USING (SELECT 'UCLPermissions_View Scheduler Configuration' KEY,'UCL' BUNDLE_NAME FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,'UCLPermissions_View Scheduler Configuration', 'View Scheduler Configuration', 'UCL');
	   
	   
   
-- DBTicket TCS-3104
MERGE INTO LABEL L
     USING (SELECT 'UCLPermissions_Admin CDV Import Errors' KEY,'UCL' BUNDLE_NAME FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,'UCLPermissions_Admin CDV Import Errors', 'Admin CDV Import Errors', 'UCL');
	   
MERGE INTO LABEL L
     USING (SELECT 'UCLPermissions_View CDV Import Errors' KEY,'UCL' BUNDLE_NAME FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES  (SEQ_LABEL_ID.NEXTVAL,'UCLPermissions_View CDV Import Errors', 'View CDV Import Errors', 'UCL'); 

	   
COMMIT;	   

-- DBTicket TCS-3103

MERGE INTO COMPANY_TYPE_PERM ctp
USING (SELECT (select COMPANY_TYPE_ID from company_type 
				where lower(DESCRIPTION) = lower('Shipper')) company_type_id, 
				(select permission_id from permission where permission_code='ACDIE') permission_id
              FROM DUAL) ctp1
ON (ctp.company_type_id = ctp1.company_type_id
    AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED THEN
   INSERT (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
   VALUES (ctp1.company_type_id,ctp1.permission_id, (SELECT MAX (row_uid) + 4
                                  FROM company_type_perm));

MERGE INTO COMPANY_TYPE_PERM ctp
USING (SELECT (select COMPANY_TYPE_ID from company_type 
				where lower(DESCRIPTION) = lower('Shipper')) company_type_id, 
				(select permission_id from permission where permission_code='VCDIE') permission_id
              FROM DUAL) ctp1
ON (ctp.company_type_id = ctp1.company_type_id
    AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED THEN
   INSERT (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
   VALUES (ctp1.company_type_id,ctp1.permission_id, (SELECT MAX (row_uid) + 4
                                  FROM company_type_perm));	
                                  
                                  
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Total Cost to Serve'
              AND MODULE.MODULE_CODE = 'TCS'
              AND PERMISSION.PERMISSION_CODE = 'ACDIE') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );   
               

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Total Cost to Serve'
              AND MODULE.MODULE_CODE = 'TCS'
              AND PERMISSION.PERMISSION_CODE = 'VCDIE') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );                 

MERGE INTO RELTP_APP_MOD_PERM RAMP
  USING (SELECT  RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
       APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                PERMISSION.PERMISSION_CODE
           FROM RELATIONSHIP_TYPE,APP, MODULE, PERMISSION
          WHERE RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID=4
            AND APP.APP_NAME = 'Total Cost to Serve'
            AND MODULE.MODULE_CODE = 'TCS'
            AND PERMISSION.PERMISSION_CODE = 'ACDIE') APP1
  ON (RAMP.RELATIONSHIP_TYPE_ID=APP1.RELATIONSHIP_TYPE_ID
      AND RAMP.APP_ID = APP1.APP_ID
      AND RAMP.MODULE_ID = APP1.MODULE_ID
      AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
  WHEN NOT MATCHED THEN
     INSERT (RAMP.RELATIONSHIP_TYPE_ID,RAMP.APP_ID, RAMP.MODULE_ID, RAMP.PERMISSION_ID, RAMP.ROW_UID,RAMP.IS_PERM_IMPLIED)
     VALUES (4,APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
             (SELECT NVL (MAX (ROW_UID), 0) + 4
                FROM RELTP_APP_MOD_PERM),0);
				
MERGE INTO RELTP_APP_MOD_PERM RAMP
  USING (SELECT  RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
       APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                PERMISSION.PERMISSION_CODE
           FROM RELATIONSHIP_TYPE,APP, MODULE, PERMISSION
          WHERE RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID=4
            AND APP.APP_NAME = 'Total Cost to Serve'
            AND MODULE.MODULE_CODE = 'TCS'
            AND PERMISSION.PERMISSION_CODE = 'VCDIE') APP1
  ON (RAMP.RELATIONSHIP_TYPE_ID=APP1.RELATIONSHIP_TYPE_ID
      AND RAMP.APP_ID = APP1.APP_ID
      AND RAMP.MODULE_ID = APP1.MODULE_ID
      AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
  WHEN NOT MATCHED THEN
     INSERT (RAMP.RELATIONSHIP_TYPE_ID,RAMP.APP_ID, RAMP.MODULE_ID, RAMP.PERMISSION_ID, RAMP.ROW_UID,RAMP.IS_PERM_IMPLIED)
     VALUES (4,APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
             (SELECT NVL (MAX (ROW_UID), 0) + 4
                FROM RELTP_APP_MOD_PERM),0);				
								  
COMMIT;				

-- DBTicket TCS-3082

update permission_inheritance set PARENT_PERMISSION_ID=(select permission_id from permission where permission_name='Administer Cost Events' and permission_code='AUDC')
where PARENT_PERMISSION_ID=(select permission_id from permission where permission_name='Administer Cost Drivers' and permission_code='ACD') 
and CHILD_PERMISSION_ID=(select permission_id from permission where permission_name='View Cost Drivers' and permission_code='VCD');

COMMIT;

				  
-- DBTicket SLOT-6221

merge into company c1
     using (select '01' company_name from dual) c2
        on (c1.company_name = c2.company_name)
when not matched
then
insert (company_id, company_name, company_type_id, is_active, is_multiple_logon_restricted, 
    created_source_type_id, created_source, created_dttm, last_updated_source_type_id, last_updated_source, 
    last_updated_dttm, company_description, company_url, telephone_number, fax_number, 
    address_1, address_2, city, state_prov, postal_code, 
    country_code, billing_address_1, billing_address_2, billing_city, billing_state_prov, 
    billing_postal_code, billing_country_code, contact_title, contact_name, contact_telephone_number, 
    contact_fax_number, contact_email, hibernate_version, has_logo, business_number, 
    duns_number, company_code, invoice_compare_method, address_3, auto_create_batch_flag, 
    batch_ctrl_flag, batch_role_id, billing_address_3, case_lock_code_exp_rec, case_lock_code_held, 
    color_mask, color_offset, color_septr, color_sfx_mask, color_sfx_offset, 
    color_sfx_septr, dflt_batch_stat_code, dsp_item_desc_flag, lock_code_invalid, pick_lock_code_exp_rec, 
    pick_lock_code_held, proc_whse_xfer, qual_mask, qual_offset, qual_septr, 
    recv_batch, season_mask, season_offset, season_septr, season_yr_mask, 
    season_yr_offset, season_yr_septr, sec_dim_mask, sec_dim_offset, sec_dim_septr, 
    size_desc_mask, size_desc_offset, size_desc_septr, sku_mask, sku_offset_mask, 
    style_mask, style_offset, style_septr, style_sfx_mask, style_sfx_offset, 
    style_sfx_septr, ucc_ean_co_pfx, parent_company_id, is_initialized)
 values
   (seq_company_id.nextval, '01', 4, 1, 0, 
    3, 'Manhattan Associates', sysdate, 3, 'Manhattan Associates', 
    sysdate, 'Default Company', null, '(770) 955-7070', null, 
    '2300 Windy Ridge Parkway', null, 'Atlanta', 'GA', '30339', 
    'US', '2300 Windy Ridge Parkway', null, 'Atlanta', 'GA', 
    '30339', 'US', null, null, null, 
    null, null, 0, 0, null, 
    null, '01', null, null, null, 
    null, null, null, null, null, 
    null, null, null, null, null, 
    null, null, null, null, null, 
    null, null, null, null, null, 
    null, null, null, null, null, 
    null, null, null, null, null, 
    null, null, null, null, null, 
    null, null, null, null, null, 
    null, null, -1, 0);

merge into location_ucl u1 
 using (select company_id, 'Atlanta' location_name from company where company_name = '01') u2
    on (u1.company_id = u2.company_id and u1.location_name = u2.location_name)
when not matched
then
insert (location_id, company_id, location_name, is_active, created_source_type_id, 
    created_source, created_dttm, last_updated_source_type_id, last_updated_source, last_updated_dttm, 
    time_zone_id, is_dst_observed, telephone_number, fax_number, address_1, 
    address_2, city, state_prov_code, postal_code, country_code, 
    contact_title, contact_name, contact_telephone_number, contact_fax_number, contact_email, 
    hibernate_version)
 values
   (seq_location_id.nextval, u2.company_id, 'Atlanta', 1, 3, 
    'WMADMIN', sysdate, 3, 'WMADMIN', sysdate, 
    3, 1, null, null, 'Main Street', 
    null, 'Atlanta', 'GA', '30339', 'US', 
    null, null, null, null, null, 
    null);

Insert into ROLE
   (ROLE_ID, COMPANY_ID, ROLE_NAME, IS_ROLE_PRIVATE, IS_ACTIVE, 
    CREATED_SOURCE_TYPE_ID, CREATED_SOURCE, CREATED_DTTM, LAST_UPDATED_SOURCE_TYPE_ID, LAST_UPDATED_SOURCE, 
    LAST_UPDATED_DTTM, HIBERNATE_VERSION, APPLY_TO_BUSINESS_PARTNERS, ROLE_TYPE_ID, IS_DEFAULT)
 Values
   (seq_role_id.nextval, (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01'), 'SLOTROLE', 0, 1, 
    1, 'SLOTADMIN', sysdate, 1, 'SLOTADMIN', 
    sysdate, 0, 0, 4, 0);
    
INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AAL'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ABD'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASDG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASEQ'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASFAC'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASPRI'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASRG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'CAL'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VAL'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VBD'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VDO'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSDG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSEQ'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSFAC'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSPRI'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSRG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ABR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AITEM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AUOM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VBR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VITEM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VUOM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMVMSGLOG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAMSGLOG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMVPRGSYSTBL'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAPRGSYSTBL'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ACU'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCD'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSYSCD'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ANXTUPNBR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VNXTUPNBR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VDOCMGT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADOCMGT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_AREPDEF'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_AREPSCH'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_AREPSAV'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTQ'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTQASS'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTREQASS'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTQMON'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VREPDEF'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VREPSCH'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VREPSAV'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTQ'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTQASS'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTREQASS'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTQMON'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VRTSA'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VPKLOC'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'APKLOC'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTREQUSR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTREQUSR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTREQSTR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTREQSTR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADA'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMASUPER'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VASSITM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VLOCUNLOC'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VPLITM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AIMPEXL'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VIMPEXL'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AITEMPRF'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VITEMPRF'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AITEMFPRF'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VITEMFPRF'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'MRFARPTDFN'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AMIF'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VMIF'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMIN_FILTER'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDEVT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAAPPSUPP'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMVAPPSUPP'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAUIFLAYOUT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMVUIFLAYOUT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'CRT_PUB_FILTER'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'CRT_PVT_FILTER'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSBP'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASBP'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSCNTRY'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASCNTRY'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'IDL'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VITEMEXP'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AITEMEXP'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDSTYPE'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDOTYPE'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDSPARM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDOPARM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDS'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'DLU'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'DLP'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AMENUMAINT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VMENUMAINT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINMENU'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINBASEMENU'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINSCREEN'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMIN_THEME'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINRES'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINPER'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMLM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMMM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'MYPROFILE'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'SECPOLICY'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LOGMGMT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMLBLMGMT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMLTLMGMT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMMSGS'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMSCRNTYPE'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITUSG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDUSG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VUSG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITCOM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VCOM'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ACCCNTRL'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITBU'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDBU'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VBU'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITREG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDREG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VREG'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITLOC'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDLOC'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VLOC'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITUSR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDUSR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'DELETEUSR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'RESETUSR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VUSR'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDRLE'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'DELETERLE'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VRLE'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDPER'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITPER'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VPER'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMCPP'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITRLT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDRLT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO app_mod_perm (app_id,
                          module_id,
                          permission_id,
                          row_uid,
                          created_dttm,
                          last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VRLT'),
             SEQ_APP_MOD_PERM.NEXTVAL,
             SYSDATE,
             SYSDATE);


INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AAL'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ABD'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASDG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASEQ'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASFAC'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASPRI'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASRG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'CAL'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VAL'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VBD'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VDO'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSDG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSEQ'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSFAC'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSPRI'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSRG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ABR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AITEM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AUOM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VBR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VITEM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VUOM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMVMSGLOG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAMSGLOG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMVPRGSYSTBL'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAPRGSYSTBL'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ACU'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCD'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSYSCD'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ANXTUPNBR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VNXTUPNBR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VDOCMGT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADOCMGT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_AREPDEF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_AREPSCH'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_AREPSAV'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTQ'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTQASS'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTREQASS'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTQMON'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VREPDEF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VREPSCH'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VREPSAV'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTQ'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTQASS'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTREQASS'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTQMON'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VRTSA'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VPKLOC'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'APKLOC'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTREQUSR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTREQUSR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_APRTREQSTR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LRF_VPRTREQSTR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADA'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMASUPER'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VASSITM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VLOCUNLOC'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VPLITM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AIMPEXL'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VIMPEXL'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AITEMPRF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VITEMPRF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AITEMFPRF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VITEMFPRF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'MRFARPTDFN'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AMIF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VMIF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMIN_FILTER'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDEVT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAAPPSUPP'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMVAPPSUPP'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAUIFLAYOUT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMVUIFLAYOUT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'CRT_PUB_FILTER'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'CRT_PVT_FILTER'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSBP'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASBP'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VSCNTRY'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASCNTRY'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'IDL'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VITEMEXP'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AITEMEXP'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDSTYPE'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDOTYPE'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDSPARM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDOPARM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ASYSCDS'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'DLU'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'DLP'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'AMENUMAINT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VMENUMAINT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINMENU'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINBASEMENU'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINSCREEN'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMIN_THEME'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINRES'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMINPER'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMLM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMMM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'MYPROFILE'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'SECPOLICY'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LOGMGMT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMLBLMGMT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMLTLMGMT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMMSGS'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMSCRNTYPE'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITUSG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDUSG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VUSG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITCOM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VCOM'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ACCCNTRL'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITBU'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDBU'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VBU'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITREG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDREG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VREG'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITLOC'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDLOC'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VLOC'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITUSR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDUSR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'DELETEUSR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'RESETUSR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VUSR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDRLE'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'DELETERLE'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VRLE'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDPER'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITPER'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VPER'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADMCPP'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EDITRLT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'ADDRLT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

INSERT INTO role_app_mod_perm (role_id,
                               app_id,
                               module_id,
                               permission_id,
                               row_uid,
                               created_dttm,
                               last_updated_dttm)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Slotting'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Slotting'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'VRLT'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             SYSDATE,
             SYSDATE);

--select * from  ucl_user where ucl_user_id = 5
merge into security_policy_group s1
using (select 'Migrated Security Policy' security_policy_group, company_id from company where company_name = '01') s2
on (s1.security_policy_group = s2.security_policy_group and s1.company_id = s2.company_id)
when not matched then
 insert  (security_policy_group_id, company_id, security_policy_group, hibernate_version, is_deleted, 
    created_source_type_id, created_source, created_dttm, last_updated_source_type_id, last_updated_source, 
    last_updated_dttm, is_default)
 values
   (seq_security_policy_group_id.nextval, s2.company_id, 'Migrated Security Policy', 0, 0, 
    3, 'Manhattan Associates', sysdate, 3, 'Manhattan Associates', 
    sysdate, 1);

Insert into UCL_USER
   (UCL_USER_ID, COMPANY_ID, USER_NAME, USER_PASSWORD, IS_ACTIVE, 
    CREATED_SOURCE_TYPE_ID, CREATED_SOURCE, CREATED_DTTM, LAST_UPDATED_SOURCE_TYPE_ID, LAST_UPDATED_SOURCE, 
    LAST_UPDATED_DTTM, USER_TYPE_ID, LOCALE_ID, LOCATION_ID, USER_FIRST_NAME, 
    USER_LAST_NAME, TELEPHONE_NUMBER, ADDRESS_1, CITY, STATE_PROV_CODE, 
    POSTAL_CODE, COUNTRY_CODE, USER_EMAIL_1, COMM_METHOD_ID_DURING_BH_1, COMM_METHOD_ID_DURING_BH_2, 
    COMM_METHOD_ID_AFTER_BH_1, COMM_METHOD_ID_AFTER_BH_2, COMMON_NAME, LAST_PASSWORD_CHANGE_DTTM, LOGGED_IN, 
    LAST_LOGIN_DTTM, DEFAULT_BUSINESS_UNIT_ID, DEFAULT_WHSE_REGION_ID, HIBERNATE_VERSION, NUMBER_OF_INVALID_LOGINS, 
    ISPASSWORDMANAGEDINTERNALLY, SECURITY_POLICY_GROUP_ID)
 Values
   (seq_ucl_user_id.nextval, (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01'), 'SLOTADMIN', 'B109F3BBBC244EB82441917ED06D618B9008DD09B3BEFD1B5E07394C706A8BB980B1D7785E5976EC049B46DF5F1326AF5A2EA6D103FD07C95385FFAB0CACBC86', 1, 
    3, 'Manhattan Associates', SYSDATE, 3, 'Manhattan Associates', 
    SYSDATE, 4, 16, (select location_id from LOCATION_UCL where location_name = 'Atlanta' and company_id = (select COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')), 'SLOTADMIN', 
    'SLOTADMIN', '(770) 955-7070', '2300 Windy Ridge Pkwy', 'Atlanta', 'GA', 
    '30339', 'US', 'seeduser@manh.com', 12, 12, 
    12, 12, 'SLOTADMIN SLOTADMIN', SYSDATE, 1, 
    SYSDATE, 1, 1010, 0, 0, 
    1, (select SECURITY_POLICY_GROUP_ID from SECURITY_POLICY_GROUP where security_policy_group = 'Migrated Security Policy' and company_id in (select company_id from company where company_name = '01')));

INSERT INTO ACCESS_CONTROL (ACCESS_CONTROL_ID,
                            UCL_USER_ID,
                            ROLE_ID,
                            GEO_REGION_ID,
                            PARTNER_COMPANY_ID,
                            BUSINESS_UNIT_ID,
                            IS_INTERNAL_CONTROL,
                            COMPANY_ID,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (seq_access_control_id.nextval,
             (SELECT UCL_USER_ID
                FROM UCL_USER
               WHERE USER_NAME = 'SLOTADMIN'),
             (SELECT ROLE_ID
                FROM ROLE
               WHERE ROLE_NAME = 'SLOTROLE' and company_id in(SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT REGION_ID
                FROM REGION
               WHERE     REGION_NAME = '01'
                     AND TC_COMPANY_ID = (SELECT COMPANY_ID
                                            FROM COMPANY
                                           WHERE COMPANY_NAME = '01')),
             -1,
             1,
             1,
             (SELECT COMPANY_ID
                FROM COMPANY
               WHERE COMPANY_NAME = '01'),
             SYSDATE,
             SYSDATE);

INSERT INTO UCL_USER_FUNCTION (UCL_USER_ID, USER_FUNCTION_ID)
     VALUES ( (SELECT UCL_USER_ID
                 FROM UCL_USER
                WHERE USER_NAME = 'SLOTADMIN'),
             (SELECT USER_FUNCTION_ID
                FROM USER_FUNCTION
               WHERE DESCRIPTION = 'Manage Location'));

INSERT INTO UCL_USER_FUNCTION (UCL_USER_ID, USER_FUNCTION_ID)
     VALUES ( (SELECT UCL_USER_ID
                 FROM UCL_USER
                WHERE USER_NAME = 'SLOTADMIN'),
             (SELECT USER_FUNCTION_ID
                FROM USER_FUNCTION
               WHERE DESCRIPTION = 'Manage User'));

INSERT INTO UCL_USER_FUNCTION (UCL_USER_ID, USER_FUNCTION_ID)
     VALUES ( (SELECT UCL_USER_ID
                 FROM UCL_USER
                WHERE USER_NAME = 'SLOTADMIN'),
             (SELECT USER_FUNCTION_ID
                FROM USER_FUNCTION
               WHERE DESCRIPTION = 'CA Admin Own Company'));

INSERT INTO UCL_USER_GROUP_TYPE (UCL_USER_ID,
                                 UCL_GROUP_TYPE_ID,
                                 USER_NAME,
                                 GRPTP_DESCRIPTION,
                                 CREATED_DTTM,
                                 LAST_UPDATED_DTTM)
     VALUES ( (SELECT UCL_USER_ID
                   FROM UCL_USER
                  WHERE USER_NAME = 'SLOTADMIN'),
               (SELECT UCL_GROUP_TYPE_ID
                  FROM UCL_GROUP_TYPE
                 WHERE DESCRIPTION = 'UCLUsers'),
               'SLOTADMIN',
               'UCLUsers',
               SYSDATE,
               SYSDATE);

INSERT INTO UCL_USER_GROUP_TYPE (UCL_USER_ID,
                                 UCL_GROUP_TYPE_ID,
                                 USER_NAME,
                                 GRPTP_DESCRIPTION,
                                 CREATED_DTTM,
                                 LAST_UPDATED_DTTM)
     VALUES ( (SELECT UCL_USER_ID
                 FROM UCL_USER
                WHERE USER_NAME = 'SLOTADMIN'),
             (SELECT UCL_GROUP_TYPE_ID
                FROM UCL_GROUP_TYPE
               WHERE DESCRIPTION = 'Common Business Object'),
             'SLOTADMIN',
             'Common Business Object',
             SYSDATE,
             SYSDATE);

INSERT INTO USER_DEFAULT (USER_DEFAULT_ID,
                          UCL_USER_ID,
                          PARAMETER_NAME,
                          PARAMETER_VALUE)
     VALUES (seq_user_default_id.nextval,
             (SELECT UCL_USER_ID
                FROM UCL_USER
               WHERE USER_NAME = 'SLOTADMIN'),
             'USER_DEFAULT_APPLICATION_ID',
             136);

INSERT INTO USER_DEFAULT (USER_DEFAULT_ID,
                          UCL_USER_ID,
                          PARAMETER_NAME,
                          PARAMETER_VALUE)
     VALUES (seq_user_default_id.nextval,
             (SELECT UCL_USER_ID
                FROM UCL_USER
               WHERE USER_NAME = 'SLOTADMIN'),
             'USER_DEFAULT_BU_ID',
             1);

INSERT INTO USER_DEFAULT (USER_DEFAULT_ID,
                          UCL_USER_ID,
                          PARAMETER_NAME,
                          PARAMETER_VALUE)
     VALUES (seq_user_default_id.nextval,
             (SELECT UCL_USER_ID
                FROM UCL_USER
               WHERE USER_NAME = 'SLOTADMIN'),
             'USER_DEFAULT_REGION_ID',
             1010);
			 
COMMIT;

-- DBTicket DB-4715
exec sequpdt('ROLE','ROLE_ID','SEQ_ROLE_ID');
exec sequpdt('UCL_USER','UCL_USER_ID','SEQ_UCL_USER_ID');
exec sequpdt('ACCESS_CONTROL','ACCESS_CONTROL_ID','SEQ_ACCESS_CONTROL_ID');
exec sequpdt('USER_DEFAULT','USER_DEFAULT_ID','SEQ_USER_DEFAULT_ID');
exec sequpdt('COMPANY_APP_MODULE','ROW_UID','SEQ_COAPPMOD_ROWUID');

-- DBTicket DB-4875 REFL
------------------------------------------------------------ Slotting Admin User ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin User',
             1,
             -1,
             'SLOTAUSER',
             'Allow user to administer User Master');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAUSER'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAUSER'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAUSER'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAUSER'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Kitting Groups ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Kitting Groups',
             1,
             -1,
             'SLOTAKITTINGGPS',
             'Allow user to administer Kitting Groups');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAKITTINGGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAKITTINGGPS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAKITTINGGPS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAKITTINGGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Seasonal ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Seasonal',
             1,
             -1,
             'SLOTASEASONAL',
             'Allow user to administer Seasonal');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASEASONAL'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASEASONAL'),
            SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASEASONAL'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASEASONAL'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Bins ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Bins',
             1,
             -1,
             'SLOTABINS',
             'Allow user to administer Bins');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTABINS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTABINS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in ( SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTABINS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTABINS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Pallets ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Pallets',
             1,
             -1,
             'SLOTAPLTS',
             'Allow user to administer Pallets');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPLTS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPLTS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in ( SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPLTS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPLTS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Container Type ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Container Type',
             1,
             -1,
             'SLOTACNTRTYPE',
             'Allow user to administer Container Type');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACNTRTYPE'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACNTRTYPE'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in ( SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACNTRTYPE'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACNTRTYPE'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Pick Zone ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Pick Zone',
             1,
             -1,
             'SLOTAPICKZONE',
             'Allow user to administer Pick Zone');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPICKZONE'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPICKZONE'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPICKZONE'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPICKZONE'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Pickline Groups ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Pickline Groups',
             1,
             -1,
             'SLOTAPICKLINEGPS',
             'Allow user to administer Pickline Groups');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPICKLINEGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPICKLINEGPS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in ( SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPICKLINEGPS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPICKLINEGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Process Manager ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Process Manager',
             1,
             -1,
             'SLOTAPROCMGR',
             'Allow user to administer Process Manager');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPROCMGR'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPROCMGR'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in ( SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPROCMGR'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPROCMGR'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Multiple Location Zones ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Multiple Location Zones',
             1,
             -1,
             'SLOTAMLOCNZONES',
             'Allow user to administer Multiple Location Zones');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMLOCNZONES'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMLOCNZONES'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in ( SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMLOCNZONES'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMLOCNZONES'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Range Groups ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Range Groups',
             1,
             -1,
             'SLOTARANGEGPS',
             'Allow user to administer Range Groups');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARANGEGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARANGEGPS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARANGEGPS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
           current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARANGEGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Slot System Codes ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Slot System Codes',
             1,
             -1,
             'SLOTASYSCODES',
             'Allow user to administer System Codes');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASYSCODES'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASYSCODES'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASYSCODES'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASYSCODES'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Set Slot Width ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Set Slot Width',
             1,
             -1,
             'SLOTASETSLOTWID',
             'Allow user to administer Set Slot Width');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETSLOTWID'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETSLOTWID'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in ( SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETSLOTWID'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
           current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETSLOTWID'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Set Left/Right Slot Pointers ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Set Left/Right Slot Pointers',
             1,
             -1,
             'SLOTASETLRPTRS',
             'Allow user to administer Set Left/Right Slot Pointers');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETLRPTRS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETLRPTRS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in ( SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETLRPTRS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETLRPTRS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Recalculate Forecast ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Recalculate Forecast',
             1,
             -1,
             'SLOTARECALCFRCST',
             'Allow user to administer Recalculate Forecast');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARECALCFRCST'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARECALCFRCST'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARECALCFRCST'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
           current_timestamp,
         current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARECALCFRCST'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Batch Reslot ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Batch Reslot',
             1,
             -1,
             'SLOTABATCHRESLOT',
             'Allow user to administer Batch Reslot');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTABATCHRESLOT'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTABATCHRESLOT'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTABATCHRESLOT'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTABATCHRESLOT'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Slot Numbering Schemas ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Slot Numbering Schemas',
             1,
             -1,
             'SLOTASNS',
             'Allow user to administer Slot Numbering Schemes');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASNS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASNS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASNS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
           current_timestamp,
         current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASNS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Rank Items ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Rank Items',
             1,
             -1,
             'SLOTARANKITEMS',
             'Allow user to administer Rank Items');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARANKITEMS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARANKITEMS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARANKITEMS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARANKITEMS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Consecutive Reslot ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Consecutive Reslot',
             1,
             -1,
             'SLOTACNSCTRESOLT',
             'Allow user to administer Consecutive Reslot');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACNSCTRESOLT'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACNSCTRESOLT'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACNSCTRESOLT'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
           current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACNSCTRESOLT'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Recalculate Slot Layout ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Recalculate Slot Layout',
             1,
             -1,
             'SLOTARCLCSLOTLYT',
             'Allow user to administer Recalculate Slot Layout');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARCLCSLOTLYT'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARCLCSLOTLYT'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARCLCSLOTLYT'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
           current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARCLCSLOTLYT'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Purge History ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Purge History',
             1,
             -1,
             'SLOTAPURGEHIST',
             'Allow user to administer Purge History');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPURGEHIST'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPURGEHIST'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPURGEHIST'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
         current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPURGEHIST'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Clear Needed Rack Types ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Clear Needed Rack Types',
             1,
             -1,
             'SLOTACLRRACKTP',
             'Allow user to administer Clear Needed Rack Types');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLRRACKTP'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLRRACKTP'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLRRACKTP'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLRRACKTP'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Reslot Items ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Reslot Items',
             1,
             -1,
             'SLOTARESLOTITEMS',
             'Allow user to administer Reslot Items');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARESLOTITEMS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARESLOTITEMS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARESLOTITEMS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARESLOTITEMS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Export/Clear Moves List ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Export/Clear Moves List',
             1,
             -1,
             'SLOTAEXPCLRMOVES',
             'Allow user to administer Export/Clear Moves List');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAEXPCLRMOVES'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAEXPCLRMOVES'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAEXPCLRMOVES'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
           current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAEXPCLRMOVES'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Purge Printed/Exported Moves ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Purge Printed/Exported Moves',
             1,
             -1,
             'SLOTAPURGEMOVES',
             'Allow user to administer Purge Printed/Exported Moves');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPURGEMOVES'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPURGEMOVES'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPURGEMOVES'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAPURGEMOVES'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Left Justify ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Left Justify',
             1,
             -1,
             'SLOTALJUSTIFY',
             'Allow user to administer Left Justify');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTALJUSTIFY'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTALJUSTIFY'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTALJUSTIFY'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTALJUSTIFY'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Clear Moves List ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Clear Moves List',
             1,
             -1,
             'SLOTACLRMOVES',
             'Allow user to administer Clear Moves List');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLRMOVES'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLRMOVES'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLRMOVES'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
           current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLRMOVES'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Clear Exception List ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Clear Exception List',
             1,
             -1,
             'SLOTACLREXCPT',
             'Allow user to administer Clear Exception List');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLREXCPT'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLREXCPT'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLREXCPT'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACLREXCPT'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Max Moves Reslot ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Max Moves Reslot',
             1,
             -1,
             'SLOTAMXMVRESLOT',
             'Allow user to administer Max Moves Reslot');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMXMVRESLOT'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMXMVRESLOT'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMXMVRESLOT'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
          current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMXMVRESLOT'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Categories ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Categories',
             1,
             -1,
             'SLOTACATEGORIES',
             'Allow user to administer Categories');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACATEGORIES'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACATEGORIES'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACATEGORIES'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
           current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTACATEGORIES'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Message Master ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Message Master',
             1,
             -1,
             'SLOTAMSGMASTER',
             'Allow user to administer Message Master');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMSGMASTER'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMSGMASTER'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMSGMASTER'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMSGMASTER'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Set XYZ Coordinates ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Set XYZ Coordinates',
             1,
             -1,
             'SLOTASETXYZ',
             'Allow user to administer Set XYZ Coordinates');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETXYZ'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETXYZ'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETXYZ'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
           current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASETXYZ'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Items ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Items',
             1,
             -1,
             'SLOTAITEMS',
             'Allow user to administer Items');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
           current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Slots ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Slots',
             1,
             -1,
             'SLOTASLOTS',
             'Allow user to administer Slots');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASLOTS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASLOTS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASLOTS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
           current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASLOTS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Orders ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Orders',
             1,
             -1,
             'SLOTAORDERS',
             'Allow user to administer Orders');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAORDERS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAORDERS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAORDERS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAORDERS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Work Assignments ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Work Assignments',
             1,
             -1,
             'SLOTAWKASSG',
             'Allow user to administer Work Assignments');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAWKASSG'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAWKASSG'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAWKASSG'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAWKASSG'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Moves List ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Moves List',
             1,
             -1,
             'SLOTAMOVES',
             'Allow user to administer Moves List');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMOVES'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMOVES'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMOVES'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
           current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAMOVES'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Family Groups ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Family Groups',
             1,
             -1,
             'SLOTAFAMILYGPS',
             'Allow user to administer Family Groups');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAFAMILYGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAFAMILYGPS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAFAMILYGPS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAFAMILYGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Like-Item Separation Groups ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Like-Item Separation Groups',
             1,
             -1,
             'SLOTASPRTNGPS',
             'Allow user to administer Like-Item Separation Groups');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASPRTNGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASPRTNGPS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASPRTNGPS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
           current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASPRTNGPS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Rack Types ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Rack Types',
             1,
             -1,
             'SLOTARACKTP',
             'Allow user to administer Rack Types');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARACKTP'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARACKTP'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARACKTP'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
            current_timestamp,
            current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARACKTP'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Main Import Dialog ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Main Import Dialog',
             1,
             -1,
             'SLOTAIMPTDIALOG',
             'Allow user to administer Main Import Dialog');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAIMPTDIALOG'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAIMPTDIALOG'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAIMPTDIALOG'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAIMPTDIALOG'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Slot Preferences ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Slot Preferences',
             1,
             -1,
             'SLOTASLOTPREFS',
             'Allow user to administer Slot Preferences');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASLOTPREFS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASLOTPREFS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASLOTPREFS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTASLOTPREFS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Forecast Preferences ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Forecast Preferences',
             1,
             -1,
             'SLOTAFRCSTPREFS',
             'Allow user to administer Forecast Preferences');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAFRCSTPREFS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAFRCSTPREFS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAFRCSTPREFS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAFRCSTPREFS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Import Preferences ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Import Preferences',
             1,
             -1,
             'SLOTAIMPTPREFS',
             'Allow user to administer Import Preferences');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAIMPTPREFS'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAIMPTPREFS'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAIMPTPREFS'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAIMPTPREFS'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Item CBO ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Item CBO',
             1,
             -1,
             'SLOTAITEMLIST',
             'Allow user to administer Item CBO');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMLIST'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMLIST'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMLIST'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMLIST'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Item Facility ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Item Facility',
             1,
             -1,
             'SLOTALOCNLIST',
             'Allow user to administer Item Facility');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTALOCNLIST'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTALOCNLIST'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTALOCNLIST'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTALOCNLIST'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Item Export From CBO ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Item Export From CBO',
             1,
             -1,
             'SLOTAITEMEXP',
             'Allow user to administer Item Export from CBO');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMEXP'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMEXP'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMEXP'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTAITEMEXP'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);



------------------------------------------------------------ Slotting Admin Rule Column Assignments ------------------------------------------------------------

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Slotting Admin Rule Column Assignments',
             1,
             -1,
             'SLOTARLCLM',
             'Allow user to administer Rule Column Assignments');

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES (4,
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARLCLM'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARLCLM'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE.ROLE_ID
                 FROM ROLE
                WHERE ROLE_NAME = 'SLOTROLE' and company_id in  (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARLCLM'),
             (SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL),
             current_timestamp,
             current_timestamp);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Slotting'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'Slotting'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'SLOTARLCLM'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);
COMMIT;

-- DBTicket DB-5953 REFL
MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Slotting'
                   AND MODULE.module_name = 'Slotting'
                   AND PERMISSION.PERMISSION_CODE = 'POSTMSG') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.nextval));

MERGE INTO role_app_mod_perm ramp
     USING (SELECT DISTINCT role_id,
                            app_id,
                            module_id,
                            p.permission_id
              FROM ROLE_APP_MOD_PERM ramp, permission p
             WHERE     ramp.PERMISSION_ID = (SELECT PERMISSION_ID
                                               FROM PERMISSION
                                              WHERE PERMISSION_CODE = 'AMIF')
                   AND Ramp.App_Id IN (SELECT App_Id
                                         FROM App
                                        WHERE App_Name = 'Slotting')
                   AND Ramp.Module_Id IN (SELECT Module_Id
                                            FROM Module
                                           WHERE Module_Name = 'Slotting')
                   AND P.permission_code IN ('POSTMSG')) APP1
        ON (    ramp.role_id = app1.role_id
            AND ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       VALUES (app1.role_id,
               app1.app_id,
               app1.module_id,
               app1.permission_id,
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
COMMIT;			   
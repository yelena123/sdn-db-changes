-- DBTicket WM-22984
-- Waiting for CBO to release the ticket CBO-15285
MERGE INTO SYS_CODE_TYPE A
     USING (SELECT 'S' REC_TYPE, '580' CODE_TYPE FROM DUAL) B
        ON (A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_DESC,
               SHORT_DESC,
               MAX_CODE_ID_LEN,
               ALLOW_ADD,
               ALLOW_DEL,
               CHG_MISC_FLAG,
               CHG_DTL_DESC,
               WHSE_DEPNDT,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_TYPE_ID,
               ORDER_BY)
       VALUES ('S',
               '580',
               'SPrty 60 Replen',
               'Pty60Repln',
               1,
               'Y',
               'Y',
               'Y',
               'Y',
               '0',
               SYSDATE,
               SYSDATE,
               'WMADMIN',
               1,
               SYS_CODE_TYPE_ID_SEQ.NEXTVAL,
               NULL);

MERGE INTO SYS_CODE A
     USING (SELECT 'S' REC_TYPE, '580' CODE_TYPE, '0' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (A.rec_type,
               A.code_type,
               A.code_id,
               A.code_desc,
               A.short_desc,
               A.misc_flags,
               A.create_date_time,
               A.mod_date_time,
               A.user_id,
               A.wm_version_id,
               A.sys_code_id,
               A.sys_code_type_id)
       VALUES ('S',
               '580',
               '0',
               'Suppress priority 60 replenishments',
               'SPPR',
               '',
               SYSDATE,
               SYSDATE,
               'F99USR',
               1,
               sys_code_id_seq.NEXTVAL,
               (SELECT sct.sys_code_type_id
                  FROM sys_code_type sct
                 WHERE sct.rec_type = 'S' AND sct.code_type = '580'));

MERGE INTO SYS_CODE A
     USING (SELECT 'S' REC_TYPE, '580' CODE_TYPE, '1' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (A.rec_type,
               A.code_type,
               A.code_id,
               A.code_desc,
               A.short_desc,
               A.misc_flags,
               A.create_date_time,
               A.mod_date_time,
               A.user_id,
               A.wm_version_id,
               A.sys_code_id,
               A.sys_code_type_id)
       VALUES ('S',
               '580',
               '1',
               'Anytime replenishment',
               'SPPR',
               '',
               SYSDATE,
               SYSDATE,
               'F99USR',
               1,
               sys_code_id_seq.NEXTVAL,
               (SELECT sct.sys_code_type_id
                  FROM sys_code_type sct
                 WHERE sct.rec_type = 'S' AND sct.code_type = '580'));

MERGE INTO SYS_CODE A
     USING (SELECT 'S' REC_TYPE, '580' CODE_TYPE, '2' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (A.rec_type,
               A.code_type,
               A.code_id,
               A.code_desc,
               A.short_desc,
               A.misc_flags,
               A.create_date_time,
               A.mod_date_time,
               A.user_id,
               A.wm_version_id,
               A.sys_code_id,
               A.sys_code_type_id)
       VALUES ('S',
               '580',
               '2',
               'Top-up replenishment',
               'SPPR',
               '',
               SYSDATE,
               SYSDATE,
               'F99USR',
               1,
               sys_code_id_seq.NEXTVAL,
               (SELECT sct.sys_code_type_id
                  FROM sys_code_type sct
                 WHERE sct.rec_type = 'S' AND sct.code_type = '580'));

COMMIT;

-- DBTicket WM-25831
update sys_code set code_id = 'SEL' where code_id = '003' and rec_type = 'S' and code_type ='520';
update sys_code set code_id = 'MRG' where code_id = '005' and rec_type = 'S' and code_type ='520';
commit;

-- DBTicket WM-26824
delete from reltp_app_mod_perm where permission_id in ( select permission_id from permission where permission_code = 'VAUTRL') and app_id = '36';
-- delete from company_type_perm where permission_id in ( select permission_id from permission where permission_code = 'VAUTRL');
delete from REL_APP_MOD_PERM where permission_id in ( select permission_id from permission where permission_code = 'VAUTRL') and app_id = '36';
delete from role_app_mod_perm where permission_id in ( select permission_id from permission where permission_code = 'VAUTRL') and app_id = '36';
delete from app_mod_perm where permission_id in ( select permission_id from permission where permission_code = 'VAUTRL') and app_id = '36';
delete from role_app_mod_perm where permission_id in ( select permission_id from permission where permission_code = 'VAUTRL') and app_id = '36';
commit;

-- DBTicket WM-35425
CALL SEQUPDT('PERMISSION', 'PERMISSION_ID', 'SEQ_PERMISSION_ID');
INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (seq_PERMISSION_ID.NEXTVAL,
             'Create WMiLPN',
             1,
             -1,
             'WVLPN',
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             NULL);

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (seq_PERMISSION_ID.NEXTVAL,
             'LPNs - Administer WM LPNs',
             1,
             -1,
             'WALPN',
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             NULL);

commit;		

-- DBTicket WM-34884

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.module_name = 'WM System Control'
                   AND PERMISSION.PERMISSION_CODE = 'WMVSSD') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.nextval));			 
			 
MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.module_name = 'WM System Control'
                   AND PERMISSION.PERMISSION_CODE = 'WMASSD') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.nextval));	
			   
MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_NAME = 'WM System Control'
                   AND PERMISSION.PERMISSION_CODE = 'WMVSSD') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));			 

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_NAME = 'WM System Control'
                   AND PERMISSION.PERMISSION_CODE = 'WMASSD') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));			   
			   
MERGE INTO company_type_perm ctp
     USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'WMVSSD') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES (
                 4,
                 ctp1.permission_id,
                 (SELECT MAX (row_uid) + 4 FROM company_type_perm));			 
			 
MERGE INTO company_type_perm ctp
     USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'WMASSD') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES (
                 4,
                 ctp1.permission_id,
                 (SELECT MAX (row_uid) + 4 FROM company_type_perm));

commit;
			 
-- DBTicket WM-35754
call SEQUPDT('role_app_mod_perm','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');
INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'RCVNG'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WVLPN'),
             (SELECT MAX (ROW_UID) + 4 FROM APP_MOD_PERM));

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'RCVNG'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WVLPN'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/									   

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'RCVNG'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WALPN'),
             (SELECT MAX (ROW_UID) + 4 FROM APP_MOD_PERM));

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'RCVNG'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WALPN'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/									   

commit;		

-- DBTicket WM-36406
call SEQUPDT('role_app_mod_perm','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');
INSERT INTO permission (permission_id,
                        permission_name,
                        is_active,
                        company_id,
                        permission_code)
     VALUES (seq_permission_id.NEXTVAL,
             'Admin - Global Lot Recall',
             1,
             -1,
             'WMAGBRCL');

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'INVNMGMT'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMAGBRCL'),
             (SELECT MAX (row_uid) + 4 FROM app_mod_perm));

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'INVNMGMT'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMAGBRCL'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/	
	
commit;	

-- DBTicket WM-36557
CALL SEQUPDT('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
MERGE INTO PERMISSION L
     USING (SELECT 'WMVCLFT' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_NAME,
               IS_ACTIVE,
               COMPANY_ID,
               PERMISSION_CODE,
               CREATED_SOURCE_TYPE_ID,
               CREATED_SOURCE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE_TYPE_ID,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_DTTM,
               DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'InventoryMgmt - CaseComment',
               1,
               -1,
               'WMVCLFT',
               3,
               'Manhattan Associates',
               SYSDATE,
               3,
               'Manhattan Associates',
               SYSDATE,
               NULL);

MERGE INTO PERMISSION L
     USING (SELECT 'WMVWOCT' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_NAME,
               IS_ACTIVE,
               COMPANY_ID,
               PERMISSION_CODE,
               CREATED_SOURCE_TYPE_ID,
               CREATED_SOURCE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE_TYPE_ID,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_DTTM,
               DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Receiving',
               1,
               -1,
               'WMVWOCT',
               3,
               'Manhattan Associates',
               SYSDATE,
               3,
               'Manhattan Associates',
               SYSDATE,
               NULL);

MERGE INTO PERMISSION L
     USING (SELECT 'AMENMAINT' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_NAME,
               IS_ACTIVE,
               COMPANY_ID,
               PERMISSION_CODE,
               CREATED_SOURCE_TYPE_ID,
               CREATED_SOURCE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE_TYPE_ID,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_DTTM,
               DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Common',
               1,
               -1,
               'AMENMAINT',
               3,
               'Manhattan Associates',
               SYSDATE,
               3,
               'Manhattan Associates',
               SYSDATE,
               NULL);

COMMIT;

-- DBTicket WM-36597
update permission set permission_name = 'Admin - Global Lot Recall' where permission_code='WMAGBRCL';
commit;

-- DBTicket WM-36612
INSERT INTO COMPANY_TYPE_PERM (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WALPN'),
             (SELECT MAX (row_uid) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO COMPANY_TYPE_PERM (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WVLPN'),
             (SELECT MAX (row_uid) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'RCVNG'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WALPN'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'RCVNG'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WVLPN'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);
			 
commit;			 

-- DBTicket WM-36285
DELETE FROM reltp_app_mod_perm
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'ASO')
            AND app_id = (select app_id from app where app_name='Warehouse Management');

--DELETE FROM company_type_perm
--      WHERE permission_id IN (SELECT permission_id
--                                FROM permission
--                               WHERE permission_code = 'ASO');

DELETE FROM REL_APP_MOD_PERM
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'ASO')
            AND app_id = (select app_id from app where app_name='Warehouse Management');

DELETE FROM role_app_mod_perm
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'ASO')
            AND app_id = (select app_id from app where app_name='Warehouse Management');

DELETE FROM reltp_app_mod_perm
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'VSO')
            AND app_id = (select app_id from app where app_name='Warehouse Management');

--DELETE FROM company_type_perm
--      WHERE permission_id IN (SELECT permission_id
--                                FROM permission
--                               WHERE permission_code = 'VSO');

DELETE FROM REL_APP_MOD_PERM
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'VSO')
            AND app_id = (select app_id from app where app_name='Warehouse Management');

DELETE FROM role_app_mod_perm
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'VSO')
            AND app_id = (select app_id from app where app_name='Warehouse Management');
			
commit;

-- DBTicket WM-36614


MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.module_name = 'WM Inventory Management'
                   AND PERMISSION.PERMISSION_CODE = 'WMAGBRCL') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.nextval));			 

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_NAME = 'WM Inventory Management'
                   AND PERMISSION.PERMISSION_CODE = 'WMAGBRCL') APP1
        ON (RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT (RAMP.RELATIONSHIP_TYPE_ID,
		   RAMP.APP_ID,
		   RAMP.MODULE_ID,
		   RAMP.PERMISSION_ID,
		   RAMP.ROW_UID,
		   IS_PERM_IMPLIED)
   VALUES (4,
		   APP1.APP_ID,
		   APP1.MODULE_ID,
		   APP1.PERMISSION_ID,
		   (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM),
		   0);
			   
MERGE INTO company_type_perm ctp
     USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'WMAGBRCL') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
INSERT (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
VALUES (4, ctp1.permission_id, (SELECT MAX (row_uid) + 4 FROM company_type_perm));	
		
commit;

-- DBTicket WM-36656
CALL SEQUPDT('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
MERGE INTO PERMISSION L
     USING (SELECT 'WMACNCLSHORT' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
INSERT (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE) 
VALUES (SEQ_PERMISSION_ID.NEXTVAL, 'Show Cancel Shorts button', 1, 'WMACNCLSHORT');

MERGE INTO PERMISSION L
     USING (SELECT 'WMAROLLSHORT' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
INSERT (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE) 
VALUES (SEQ_PERMISSION_ID.NEXTVAL, 'Show Roll-Over Shorts button', 1, 'WMAROLLSHORT');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE APP.APP_NAME = 'Warehouse Management'
			   AND MODULE.MODULE_NAME = 'WM Outbound'
			   AND PERMISSION.PERMISSION_CODE = 'WMACNCLSHORT') APP1
        ON (AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
	INSERT (AMP.APP_ID,
		   AMP.MODULE_ID,
		   AMP.PERMISSION_ID,
		   AMP.ROW_UID)
	VALUES (APP1.APP_ID,
		   APP1.MODULE_ID,
		   APP1.PERMISSION_ID,
		   (SEQ_APP_MOD_PERM.nextval));		

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE APP.APP_NAME = 'Warehouse Management'
			   AND MODULE.MODULE_NAME = 'WM Outbound'
			   AND PERMISSION.PERMISSION_CODE = 'WMAROLLSHORT') APP1
        ON (AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
	INSERT (AMP.APP_ID,
		   AMP.MODULE_ID,
		   AMP.PERMISSION_ID,
		   AMP.ROW_UID)
	VALUES (APP1.APP_ID,
		   APP1.MODULE_ID,
		   APP1.PERMISSION_ID,
		   (SEQ_APP_MOD_PERM.nextval));		

MERGE INTO company_type_perm ctp
     USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'WMACNCLSHORT') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
INSERT (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
VALUES (4, ctp1.permission_id, (SELECT MAX(row_uid)+ 4 FROM company_type_perm));	

MERGE INTO company_type_perm ctp
     USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'WMAROLLSHORT') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
INSERT (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
VALUES (4, ctp1.permission_id, (SELECT MAX(row_uid)+ 4 FROM company_type_perm));	

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_NAME = 'WM Outbound'
                   AND PERMISSION.PERMISSION_CODE = 'WMACNCLSHORT') APP1
        ON (RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
	INSERT (RAMP.RELATIONSHIP_TYPE_ID,
		   RAMP.APP_ID,
		   RAMP.MODULE_ID,
		   RAMP.PERMISSION_ID,
		   RAMP.ROW_UID)
	VALUES (4,
		   APP1.APP_ID,
		   APP1.MODULE_ID,
		   APP1.PERMISSION_ID,
		   (SELECT NVL(MAX (ROW_UID), 0)+ 4 FROM RELTP_APP_MOD_PERM));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_NAME = 'WM Outbound'
                   AND PERMISSION.PERMISSION_CODE = 'WMAROLLSHORT') APP1
        ON (RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
	INSERT (RAMP.RELATIONSHIP_TYPE_ID,
		   RAMP.APP_ID,
		   RAMP.MODULE_ID,
		   RAMP.PERMISSION_ID,
		   RAMP.ROW_UID)
	VALUES (4,
		   APP1.APP_ID,
		   APP1.MODULE_ID,
		   APP1.PERMISSION_ID,
		   (SELECT NVL(MAX (ROW_UID), 0)+ 4 FROM RELTP_APP_MOD_PERM));

commit;

-- DBTicket WM-37996
UPDATE lrf_report_def_layout
   SET is_required = 1
 WHERE report_def_id IN (SELECT report_def_id
                           FROM lrf_report_def
                          WHERE report_file_name = 'LockedLPNReport')
       AND field_label = 'Lock Code';

COMMIT;

-- DBTicket WM-37574
exec quiet_drop ('SEQUENCE', 'SEQ_RELTP_APP_MOD_PERM');
create sequence seq_reltp_app_mod_perm;

exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');
exec sequpdt('ROLE_APP_MOD_PERM','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
exec sequpdt('REL_APP_MOD_PERM ','ROW_UID','SEQ_RELAPPMODPERM_ROWUID');

merge into app_mod_perm amp
     using (select distinct ramp.app_id, ramp.module_id, p.permission_id
              from app_mod_perm ramp, permission p
             where ramp.permission_id in (select permission_id
                                            from permission
                                           where permission_code = 'ASN')
                   and p.permission_code in
                          ('WVLPN')) app1
        on (     amp.app_id = app1.app_id
            and amp.module_id = app1.module_id
            and amp.permission_id = app1.permission_id)
when not matched
then
   insert     (amp.app_id,
               amp.module_id,
               amp.permission_id,
               amp.row_uid)
       values (app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_app_mod_perm.nextval);

merge into role_app_mod_perm ramp
     using (select distinct role_id,
                            app_id,
                            module_id,
                            p.permission_id
              from role_app_mod_perm ramp, permission p
             where ramp.permission_id = (select permission_id
                                           from permission
                                          where permission_code = 'ASN')
                   and p.permission_code in
                          ('WVLPN')) app1
        on (    ramp.role_id = app1.role_id
            and ramp.app_id = app1.app_id
            and ramp.module_id = app1.module_id
            and ramp.permission_id = app1.permission_id)
when not matched
then
   insert     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       values (app1.role_id,
               app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_roleappmodperm_rowuid.nextval);
               
merge into reltp_app_mod_perm ramp
     using (select distinct relationship_type_id,
                            app_id,
                            module_id,
                            p.permission_id
              from reltp_app_mod_perm ramp, permission p
             where ramp.permission_id = (select permission_id
                                           from permission
                                          where permission_code = 'ASN')
                   and p.permission_code in
                          ('WVLPN')) app1
        on (    ramp.relationship_type_id = app1.relationship_type_id
            and ramp.app_id = app1.app_id
            and ramp.module_id = app1.module_id
            and ramp.permission_id = app1.permission_id)
when not matched
then
   insert     (ramp.relationship_type_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       values (app1.relationship_type_id,
               app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_reltp_app_mod_perm.nextval);
                              
merge into rel_app_mod_perm ramp
     using (select distinct relationship_id,
                            app_id,
                            module_id,
                            p.permission_id
              from rel_app_mod_perm ramp, permission p
             where ramp.permission_id = (select permission_id
                                           from permission
                                          where permission_code = 'ASN')
                   and p.permission_code in
                          ('WVLPN')) app1
        on (    ramp.relationship_id = app1.relationship_id
            and ramp.app_id = app1.app_id
            and ramp.module_id = app1.module_id
            and ramp.permission_id = app1.permission_id)
when not matched
then
   insert     (ramp.relationship_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       values (app1.relationship_id,
               app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_relappmodperm_rowuid.nextval);

merge into app_mod_perm amp
     using (select distinct ramp.app_id, ramp.module_id, p.permission_id
              from app_mod_perm ramp, permission p
             where ramp.permission_id in (select permission_id
                                            from permission
                                           where permission_code = 'ASN')
                   and p.permission_code in
                          ('WALPN')) app1
        on (     amp.app_id = app1.app_id
            and amp.module_id = app1.module_id
            and amp.permission_id = app1.permission_id)
when not matched
then
   insert     (amp.app_id,
               amp.module_id,
               amp.permission_id,
               amp.row_uid)
       values (app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_app_mod_perm.nextval);

merge into role_app_mod_perm ramp
     using (select distinct role_id,
                            app_id,
                            module_id,
                            p.permission_id
              from role_app_mod_perm ramp, permission p
             where ramp.permission_id = (select permission_id
                                           from permission
                                          where permission_code = 'ASN')
                   and p.permission_code in
                          ('WALPN')) app1
        on (    ramp.role_id = app1.role_id
            and ramp.app_id = app1.app_id
            and ramp.module_id = app1.module_id
            and ramp.permission_id = app1.permission_id)
when not matched
then
   insert     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       values (app1.role_id,
               app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_roleappmodperm_rowuid.nextval);
               
merge into reltp_app_mod_perm ramp
     using (select distinct relationship_type_id,
                            app_id,
                            module_id,
                            p.permission_id
              from reltp_app_mod_perm ramp, permission p
             where ramp.permission_id = (select permission_id
                                           from permission
                                          where permission_code = 'ASN')
                   and p.permission_code in
                          ('WALPN')) app1
        on (    ramp.relationship_type_id = app1.relationship_type_id
            and ramp.app_id = app1.app_id
            and ramp.module_id = app1.module_id
            and ramp.permission_id = app1.permission_id)
when not matched
then
   insert     (ramp.relationship_type_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       values (app1.relationship_type_id,
               app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_reltp_app_mod_perm.nextval);
                              
merge into rel_app_mod_perm ramp
     using (select distinct relationship_id,
                            app_id,
                            module_id,
                            p.permission_id
              from rel_app_mod_perm ramp, permission p
             where ramp.permission_id = (select permission_id
                                           from permission
                                          where permission_code = 'ASN')
                   and p.permission_code in
                          ('WALPN')) app1
        on (    ramp.relationship_id = app1.relationship_id
            and ramp.app_id = app1.app_id
            and ramp.module_id = app1.module_id
            and ramp.permission_id = app1.permission_id)
when not matched
then
   insert     (ramp.relationship_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       values (app1.relationship_id,
               app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_relappmodperm_rowuid.nextval);

drop sequence seq_reltp_app_mod_perm;

-- DBTicket WM-38455

EXEC SEQUPDT('ROLE_APP_MOD_PERM','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'BDM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'ADMINSCREEN'),
             (SEQ_APP_MOD_PERM.nextval));
			 
DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'BDM'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'ADMINSCREEN'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/				 
			 
INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'BDM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'ADMINRES'),
             (SEQ_APP_MOD_PERM.nextval));		
			 
DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'BDM'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'ADMINRES'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/				 

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'BDM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'ADMINUCG'),
             (SEQ_APP_MOD_PERM.nextval));		
			 
DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'BDM'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'ADMINUCG'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/			 

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'BDM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'ADMINPER'),
             (SEQ_APP_MOD_PERM.nextval));	

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'BDM'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'ADMINPER'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/		

commit;	

-- DBTicket WM-43603
DELETE FROM lrf_report_def_layout
      WHERE report_def_id IN (SELECT report_def_id
                                FROM lrf_report_def
                               WHERE report_name = 'BOLReport_Retail');
                               
delete from lrf_report_def_layout
      where report_def_id in (select report_def_id
                                from lrf_report_def
                               where report_name = 'BOLReport_Retail');
                               
delete from lrf_report_param_value_dtl
      where (rep_param_id, report_id, company_id) in
               (select rep_param_id, report_id, company_id
                  from lrf_report_param_value
                 where (report_id, company_id) in
                          (select report_id, company_id
                             from lrf_report
                            where report_def_id in
                                     (select report_def_id
                                        from lrf_report_def
                                       where report_name = 'BOLReport_Retail')));

delete from lrf_report_param_value
       where (report_id, company_id) in
               (select report_id, company_id
                  from lrf_report
                 where report_def_id in
                          (select report_def_id
                             from lrf_report_def
                            where report_name = 'BOLReport_Retail'));

delete from lrf_prt_queue_service
      where report_def_id in (select report_def_id
                                from lrf_report_def
                               where report_name = 'BOLReport_Retail');                               

delete from lrf_report
      where report_def_id in (select report_def_id
                                from lrf_report_def
                               where report_name = 'BOLReport_Retail');


commit;    
         
         
         
-- DBTicket WM-45117


BEGIN
   FOR i
      IN (SELECT ucc.table_name, ucc.column_name
            FROM user_tables ut, user_col_comments ucc
           WHERE     ut.table_name = ucc.table_name
                 AND ucc.comments IS NULL
                 AND ut.temporary = 'N'
                 AND ucc.column_name IN ('CREATED_DTTM','CREATED_DATE_TIME','CREATE_DATE_TIME'))
   LOOP
      EXECUTE IMMEDIATE
            'COMMENT ON COLUMN '
         || i.table_name
         || '.'
         || i.column_name
         || '  IS '
         || '''Created on Date and Time''';
   END LOOP;

   FOR i
      IN (SELECT ucc.table_name, ucc.column_name
            FROM user_tables ut, user_col_comments ucc
           WHERE     ut.table_name = ucc.table_name
                 AND ucc.comments IS NULL
                 AND ut.temporary = 'N'
                 AND ucc.column_name IN ('LAST_UPDATED_DTTM','MOD_DATE_TIME'))
   LOOP
      EXECUTE IMMEDIATE
            'COMMENT ON COLUMN '
         || i.table_name
         || '.'
         || i.column_name
         || '  IS '
         || '''Last Updated on Date and Time''';
   END LOOP;
END;
/

SHOW ERRORS;

COMMIT;

-- DBTicket WM-42328 REFL
DELETE FROM role_app_mod_perm 
      WHERE permission_id in 
                (SELECT permission_id 
			       FROM permission 
                  WHERE permission_code ='UCLSA') 
	        AND app_id = 36;  
COMMIT;
         
-- DBTicket WM-48669 REFL
MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   MODULE.MODULE_ID,
                   PERMISSION.PERMISSION_ID
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'OTBND'
                   AND PERMISSION.PERMISSION_CODE = 'RSA') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID) WHEN NOT MATCHED THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'OTBND'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'RSA'),
               SEQ_APP_MOD_PERM.NEXTVAL);

COMMIT;            

-- DBTicket WM-49976 REFL
exec sequpdt('ROLE_APP_MOD_PERM','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM)
            SELECT ROLE.ROLE_ID,
          (SELECT APP_ID
             FROM APP
            WHERE APP_NAME = 'Warehouse Management'),
          (SELECT MODULE_ID
             FROM MODULE
            WHERE MODULE_CODE = 'OTBND'),
          (SELECT PERMISSION_ID
             FROM PERMISSION
            WHERE PERMISSION_CODE = 'RSA'),
          SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
          CURRENT_TIMESTAMP,
          CURRENT_TIMESTAMP
     FROM ROLE
    WHERE ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');
       END IF;
END;
/

-- DBTicket WM-49898 REFL

EXEC SEQUPDT ('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');

EXEC SEQUPDT ('ROLE_APP_MOD_PERM','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.nextval,
             'Admin - Pickup Task Rules',
             1,
             -1,
             'WMAPKUPTSKRUL');

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'TASKMGMT'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMAPKUPTSKRUL'),
             SEQ_APP_MOD_PERM.nextval);

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM(ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'TASKMGMT'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMAPKUPTSKRUL'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'TASKMGMT'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMAPKUPTSKRUL'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);


INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAPKUPTSKRUL'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));


--For View Permission - WMVPKUPTSKRUL

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE)
     VALUES (SEQ_PERMISSION_ID.nextval,
             'View - Pickup Task Rules',
             1,
             -1,
             'WMVPKUPTSKRUL');

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'TASKMGMT'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMVPKUPTSKRUL'),
             SEQ_APP_MOD_PERM.nextval);

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM(ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'TASKMGMT'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMVPKUPTSKRUL'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'TASKMGMT'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMVPKUPTSKRUL'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);


INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMVPKUPTSKRUL'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));
             
COMMIT;

-- DBTicket WM-24050 REFL
DELETE FROM ROLE_APP_MOD_PERM
      WHERE PERMISSION_ID IN (SELECT PERMISSION_ID
                                FROM PERMISSION
                               WHERE PERMISSION_CODE = 'VAUTRL');

COMMIT;

-- DBTicket WM-51597 REFL
delete from reltp_app_mod_perm where permission_id in (SELECT Permission_Id
                  FROM Permission
                 WHERE Permission_Code IN ('WMASTMAPRL', 'WMVSTMAPRL'));               
                 
delete from REL_APP_MOD_PERM where permission_id in (SELECT Permission_Id
                  FROM Permission
                 WHERE Permission_Code IN ('WMASTMAPRL', 'WMVSTMAPRL'));   

DELETE FROM Role_App_Mod_Perm
      WHERE Permission_Id IN
               (SELECT Permission_Id
                  FROM Permission
                 WHERE Permission_Code IN ('WMASTMAPRL', 'WMVSTMAPRL'));

DELETE FROM App_Mod_Perm
      WHERE Permission_Id IN
               (SELECT Permission_Id
                  FROM Permission
                 WHERE Permission_Code IN ('WMASTMAPRL', 'WMVSTMAPRL'));
                 
COMMIT;

-- DBTicket WM-54315 REFL
DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'SYSCTRL'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMVCOMSCRN'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/               
               
COMMIT;               

-- DBTicket WM-55596
UPDATE COMPANY_PARAM
   SET PARAMETER_VALUE = 'Y'
 WHERE PARAMETER_NAME = 'STOP_COMPANY_IMPORT_UPDATE' AND COMPANY_ID = 1;
 
COMMIT;

-- DBTicket WM-57267
INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'CARTRLPOOL'),
             SEQ_APP_MOD_PERM.NEXTVAL);

COMMIT;

-- DBTicket WM-62502
MERGE INTO lrf_report_def_layout L1
     USING (SELECT L2.REPORT_DEF_ID,
                   L2.category,
                   L2.subcategory,
                   L2.report_name
              FROM lrf_report_def L2
             WHERE     category = 'WM'
                   AND subcategory = 'RECEIVING'
                   AND report_name = 'ASNInquiryReport') L3
        ON (    L1.REPORT_DEF_ID = L3.REPORT_DEF_ID
            AND L1.FIELD_NAME = 'ic.item_name'
            AND L1.FIELD_LABEL = 'Item')
WHEN NOT MATCHED
THEN
   INSERT     (report_def_layout_id,
               field_position,
               is_required,
               field_name,
               field_label,
               field_operators,
               field_type,
               operator_type,
               is_localizable,
               send_parameter,
               hibernate_field_name,
               report_def_id,
               created_dttm,
               last_updated_dttm)
       VALUES (
                 LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL,
                 0,
                 1,
                 'ASN.TC_ASN_ID',
                 'ASN Number',
                 '=',
                 'STRING',
                 'REPORT_TEXT_ITEM',
                 0,
                 0,
                 'tcAsnId',
                 (SELECT report_def_id
                    FROM lrf_report_def
                   WHERE     subcategory = 'RECEIVING'
                         AND category = 'WM'
                         AND report_name = 'ASNInquiryReport'),
                 CURRENT_TIMESTAMP,
                 CURRENT_TIMESTAMP);

UPDATE lrf_report_def_layout
   SET field_label = 'ASN Id', field_position = 2, is_required = 0
 WHERE report_def_id =
          (SELECT report_def_id
             FROM lrf_report_def
            WHERE     subcategory = 'RECEIVING'
                  AND category = 'WM'
                  AND report_name = 'ASNInquiryReport')
       AND hibernate_field_name = 'ASNId'
       AND field_name = 'ASN.ASN_ID';

MERGE INTO lrf_report_def_layout L1
     USING (SELECT L2.REPORT_DEF_ID,
                   L2.category,
                   L2.subcategory,
                   L2.report_name
              FROM lrf_report_def L2
             WHERE     category = 'WM'
                   AND subcategory = 'RECEIVING'
                   AND report_name = 'ASNVarianceReport') L3
        ON (    L1.REPORT_DEF_ID = L3.REPORT_DEF_ID
            AND L1.FIELD_NAME = 'ic.item_name'
            AND L1.FIELD_LABEL = 'Item')
WHEN NOT MATCHED
THEN
   INSERT     (report_def_layout_id,
               field_position,
               is_required,
               field_name,
               field_label,
               field_operators,
               field_type,
               operator_type,
               is_localizable,
               send_parameter,
               hibernate_field_name,
               report_def_id,
               created_dttm,
               last_updated_dttm)
       VALUES (
                 LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL,
                 0,
                 1,
                 'ASN.TC_ASN_ID',
                 'ASN Number',
                 '=',
                 'STRING',
                 'REPORT_TEXT_ITEM',
                 0,
                 0,
                 'tcAsnId',
                 (SELECT report_def_id
                    FROM lrf_report_def
                   WHERE     subcategory = 'RECEIVING'
                         AND category = 'WM'
                         AND report_name = 'ASNVarianceReport'),
                 CURRENT_TIMESTAMP,
                 CURRENT_TIMESTAMP);

UPDATE lrf_report_def_layout
   SET field_label = 'ASN Id', field_position = 2, is_required = 0
 WHERE report_def_id =
          (SELECT report_def_id
             FROM lrf_report_def
            WHERE     subcategory = 'RECEIVING'
                  AND category = 'WM'
                  AND report_name = 'ASNVarianceReport')
       AND field_name = 'ASN.ASN_ID';
       
COMMIT;       

-- DBTicket WM-62548
update accessorial_option_group set Description = 'Address Service Requested' where accessorial_group_code = 'ASR';
update accessorial_option_group set Description = 'Change Service Requested' where accessorial_group_code = 'CSR';
update accessorial_option_group set Description = 'Forwarding Service Requested' where accessorial_group_code = 'FSR';
update accessorial_option_group set Description = 'Carrier Leave if No Response' where accessorial_group_code = 'LVE';
update accessorial_option_group set Description = 'Return Service Requested' where accessorial_group_code = 'RSR';
commit;

-- DBTicket WM-61725
UPDATE lrf_report_def_layout
   SET selection_page_url =
          '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=ItemName'
          || '&'
          || 'permission_code=WMVRARPT'
 WHERE report_def_id = (SELECT report_def_id
                          FROM lrf_report_def
                         WHERE report_name = 'ReceivingActivityReport')
       AND selection_page_url =
              '/lps/resources/editControl/lookup/idLookup.jsflps?lookupType=ItemName';
              
COMMIT;              

-- DBTicket WM-65182
DELETE FROM reltp_app_mod_perm
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'RLPNASN')
            AND app_id = 36;

DELETE FROM REL_APP_MOD_PERM
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'RLPNASN')
            AND app_id = 36;

DELETE FROM role_app_mod_perm
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'RLPNASN')
            AND app_id = 36;

DELETE FROM role_app_mod_perm
      WHERE permission_id IN (SELECT permission_id
                                FROM permission
                               WHERE permission_code = 'RLPNASN')
            AND app_id = 36;

COMMIT;

-- DBTicket WM-65997
alter sequence LOCN_HDR_ID_SEQ cache 100;

-- DBTicket WM-65854
INSERT INTO lrf_report_def_layout (report_def_layout_id,
                                   field_position,
                                   is_required,
                                   field_name,
                                   field_label,
                                   field_operators,
                                   field_type,
                                   operator_type,
                                   is_localizable,
                                   send_parameter,
                                   hibernate_field_name,
                                   report_def_id,
                                   created_dttm,
                                   last_updated_dttm)
     VALUES (
               LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL,
               0,
               1,
               'ASN.TC_ASN_ID',
               'ASN Number',
               '=',
               'STRING',
               'REPORT_TEXT_ITEM',
               0,
               0,
               'tcAsnId',
               (SELECT report_def_id
                  FROM lrf_report_def
                 WHERE     subcategory = 'RECEIVING'
                       AND category = 'WM'
                       AND report_name = 'ASNInquiryReport'),
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

UPDATE lrf_report_def_layout
   SET field_label = 'ASN Id', field_position = 2, is_required = 0
 WHERE report_def_id =
          (SELECT report_def_id
             FROM lrf_report_def
            WHERE     subcategory = 'RECEIVING'
                  AND category = 'WM'
                  AND report_name = 'ASNInquiryReport')
       AND hibernate_field_name = 'ASNId'
       AND field_name = 'ASN.ASN_ID';

INSERT INTO lrf_report_def_layout (report_def_layout_id,
                                   field_position,
                                   is_required,
                                   field_name,
                                   field_label,
                                   field_operators,
                                   field_type,
                                   operator_type,
                                   is_localizable,
                                   send_parameter,
                                   hibernate_field_name,
                                   report_def_id,
                                   created_dttm,
                                   last_updated_dttm)
     VALUES (
               LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL,
               0,
               1,
               'ASN.TC_ASN_ID',
               'ASN Number',
               '=',
               'STRING',
               'REPORT_TEXT_ITEM',
               0,
               0,
               'tcAsnId',
               (SELECT report_def_id
                  FROM lrf_report_def
                 WHERE     subcategory = 'RECEIVING'
                       AND category = 'WM'
                       AND report_name = 'ASNVarianceReport'),
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

UPDATE lrf_report_def_layout
   SET field_label = 'ASN Id', field_position = 2, is_required = 0
 WHERE report_def_id =
          (SELECT report_def_id
             FROM lrf_report_def
            WHERE     subcategory = 'RECEIVING'
                  AND category = 'WM'
                  AND report_name = 'ASNVarianceReport')
       AND field_name = 'ASN.ASN_ID';
       
COMMIT;       

-- DBTicket WM-63182
delete from company_type_perm
      where permission_id in (select permission_id
                                from permission
                               where permission_code in ('UCLSA', 'UCLCA'));

delete from navigation_app_mod_perm
      where app_id in (select app_id
                         from app
                        where app_name = 'Warehouse Management')
            and permission_id in (select permission_id
                                    from permission
                                   where permission_code in ('UCLSA', 'UCLCA'));

delete from role_app_mod_perm
      where app_id in (select app_id
                         from app
                        where app_name = 'Warehouse Management')
            and permission_id in (select permission_id
                                    from permission
                                   where permission_code in ('UCLSA', 'UCLCA'));

delete from rel_app_mod_perm
      where app_id in (select app_id
                         from app
                        where app_name = 'Warehouse Management')
            and permission_id in (select permission_id
                                    from permission
                                   where permission_code in ('UCLSA', 'UCLCA'));

delete from reltp_app_mod_perm
      where app_id in (select app_id
                         from app
                        where app_name = 'Warehouse Management')
            and permission_id in (select permission_id
                                    from permission
                                   where permission_code in ('UCLSA', 'UCLCA'));

delete from app_mod_perm
      where app_id in (select app_id
                         from app
                        where app_name = 'Warehouse Management')
            and permission_id in (select permission_id
                                    from permission
                                   where permission_code in ('UCLSA', 'UCLCA'));

commit;

-- DBTicket WM-50133
exec sequpdt('ROLE_APP_MOD_PERM','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID') ;

merge into role_app_mod_perm ramp
using
(
    select distinct role_id, app_id, module_id, p.permission_id
	from role_app_mod_perm, permission p
	where app_id=(select app_id from app where app_name='Warehouse Management')
	    and module_id = (select module_id from module where module_code='MNF') 
	    and role_id in  (select r.role_id from role r 
		    where r.role_name in ('WMOBMGRROLE', 'WMOBSHIPMGRROLE', 'WMROLE'))
	    and p.permission_id in (select permission_id from permission where permission_code in ('MNFLPN', 'WGHLPN')) 
) app1
on (ramp.role_id = app1.role_id and ramp.app_id = app1.app_id
    and ramp.module_id = app1.module_id and ramp.permission_id = app1.permission_id)
when not matched
then
   insert     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       values (app1.role_id,
               app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_roleappmodperm_rowuid.nextval) ;

commit ;

-- DBTicket DB-506

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'MNF'
                   AND PERMISSION.PERMISSION_CODE = 'DMNFO') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID,
               AMP.CREATED_DTTM,
               AMP.LAST_UPDATED_DTTM)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               SEQ_APP_MOD_PERM.NEXTVAL,
               CURRENT_TIMESTAMP,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT r.role_id,
                   a.app_id,
                   m.module_id,
                   p.permission_id
              FROM role r,
                   app a,
                   module m,
                   permission p
             WHERE     a.app_name = 'Warehouse Management'
                   AND m.module_code = 'MNF'
                   AND r.role_name = 'WMROLE'
                   AND p.permission_code = 'DMNFO') app1
        ON (    ramp.role_id = app1.role_id
            AND ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.CREATED_DTTM,
               ramp.LAST_UPDATED_DTTM)
       VALUES (app1.role_id,
               app1.app_id,
               app1.module_id,
               app1.permission_id,
               seq_roleappmodperm_rowuid.NEXTVAL,
               CURRENT_TIMESTAMP,
               NULL);

MERGE INTO company_type_perm ctp
     USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'DMNFO') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES (
                 4,
                 ctp1.permission_id,
                 (SELECT MAX (row_uid) + 4 FROM company_type_perm));

COMMIT;

-- DBTicket DB-581
CALL SEQUPDT('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
INSERT INTO PERMISSION 
(
    PERMISSION_ID,PERMISSION_NAME, IS_ACTIVE, COMPANY_ID,
	PERMISSION_CODE, CREATED_SOURCE_TYPE_ID, CREATED_SOURCE,
	CREATED_DTTM, LAST_UPDATED_SOURCE_TYPE_ID, LAST_UPDATED_SOURCE,
    LAST_UPDATED_DTTM, DESCRIPTION
)
VALUES 
(
    seq_permission_id.nextval, 'Pack Station UI', 1, - 1, 
	'WMPACKSTN', 3, 'Manhattan Associates', SYSDATE, 3, 
	'Manhattan Associates', SYSDATE, 'Pack Station UI'
);

INSERT INTO PERMISSION_TAG 
(
    TAG_ID, DESCRIPTION, CREATED_SOURCE_TYPE_ID, CREATED_SOURCE, 
	CREATED_DTTM, LAST_UPDATED_SOURCE_TYPE_ID, LAST_UPDATED_SOURCE,
	LAST_UPDATED_DTTM
)
VALUES 
(
    seq_tag_id.nextval, 'Pack Station', 1, 'Seed', SYSDATE,
	1, 'Seed', SYSDATE
);

INSERT INTO PERMISSION_TAG_MAPPING 
VALUES 
(
    (select permission_id from permission where PERMISSION_CODE='WMPACKSTN'), 
	(SELECT TAG_ID FROM PERMISSION_TAG where DESCRIPTION='Pack Station')
);

INSERT INTO APP_MOD_PERM 
(
APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, CREATED_DTTM, LAST_UPDATED_DTTM
)
VALUES 
(
(SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'), 
(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'), 
(select permission_id from permission where PERMISSION_CODE='WMPACKSTN'),
(SELECT max(ROW_UID)+1 FROM APP_MOD_PERM), SYSDATE, NULL
);

INSERT INTO COMPANY_TYPE_PERM 
(
    COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID, CREATED_DTTM, LAST_UPDATED_DTTM
)
VALUES 
(
    4,(select permission_id from permission where PERMISSION_CODE='WMPACKSTN'),
    (SELECT max(ROW_UID)+1 FROM COMPANY_TYPE_PERM), SYSDATE, NULL
);

commit ;

-- DBTicket DB-763 REFL

EXEC SEQUPDT('COMPANY','COMPANY_ID','SEQ_COMPANY_ID');

Insert into COMPANY
(
    COMPANY_ID, COMPANY_NAME, COMPANY_TYPE_ID, IS_ACTIVE, 
                IS_MULTIPLE_LOGON_RESTRICTED, CREATED_SOURCE_TYPE_ID, 
                CREATED_SOURCE, CREATED_DTTM, LAST_UPDATED_SOURCE_TYPE_ID, 
                LAST_UPDATED_SOURCE, LAST_UPDATED_DTTM, COMPANY_DESCRIPTION, 
                TELEPHONE_NUMBER, FAX_NUMBER, ADDRESS_1, ADDRESS_2, CITY, 
                STATE_PROV, POSTAL_CODE, COUNTRY_CODE, BILLING_ADDRESS_1, 
                BILLING_ADDRESS_2, BILLING_CITY, BILLING_STATE_PROV, 
                BILLING_POSTAL_CODE, BILLING_COUNTRY_CODE, HIBERNATE_VERSION, 
                HAS_LOGO, COMPANY_CODE, PARENT_COMPANY_ID, IS_INITIALIZED
)
Values
(
    SEQ_COMPANY_ID.nextval, 'External Parcel Carrier', 8, 1, 0, 
    1, 'System Administrator',sysdate, 1, 'System Administrator', 
    sysdate, 'Default Carrier Company for External Parcel Integration', '+1 770.955.7070', '+1 770.955.7070', 
    '2300 Windy Ridge Pkwy', null, 'Atlanta', 'GA', '30339', 
    'US', '2300 Windy Ridge Pkwy', null, 'Atlanta', 'GA', 
    '30339', 'US', 0, 0, 'EPI', -1, 0
);

insert into company_app
(
    COMPANY_ID, APP_ID, ROW_UID, CREATED_DTTM, LAST_UPDATED_DTTM
)
values
(
    (select company_id from company where COMPANY_NAME = 'External Parcel Carrier'),
                (SELECT app_id FROM app WHERE app_name = 'Dashboard'),
               SEQ_COAPP_ROWUID.nextval, 
                sysdate, systimestamp
);

insert into company_app_module
(
    COMPANY_ID, APP_ID, MODULE_ID, ROW_UID, CREATED_DTTM, LAST_UPDATED_DTTM
)
values
(
    (select company_id from company where COMPANY_NAME = 'External Parcel Carrier'),
                (SELECT app_id FROM app WHERE app_name = 'Dashboard'),
                (SELECT module_id FROM module WHERE module_name = 'Supply Chain Process Platform'),
                SEQ_COAPPMOD_ROWUID.NEXTVAL, sysdate, systimestamp
);

commit;

-- DBTicket DB-1623 REFL
MERGE INTO app_mod_perm amp
     USING (SELECT DISTINCT ramp.app_id, ramp.module_id, p.permission_id
              FROM app_mod_perm ramp, permission p
             WHERE ramp.permission_id IN (SELECT permission_id
                                            FROM permission
                                           WHERE permission_code = 'WMASHIPWV')
                   AND p.PERMISSION_CODE IN
                          ('WMSHIPCONF','WMWAVECHCLSEDO')) app1
        ON (     amp.app_id = app1.app_id
            AND amp.module_id = app1.module_id
            AND amp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (amp.app_id,
               amp.module_id,
               amp.permission_id,
               amp.row_uid)
       VALUES (app1.app_id,
               APP1.MODULE_ID,
               app1.permission_id,
               SEQ_APP_MOD_PERM.NEXTVAL);     

MERGE INTO role_app_mod_perm ramp
     USING (SELECT DISTINCT role_id,
                            app_id,
                            module_id,
                            p.permission_id
              FROM ROLE_APP_MOD_PERM ramp, permission p
             WHERE ramp.PERMISSION_ID = (SELECT PERMISSION_ID
                                           FROM PERMISSION
                                          WHERE PERMISSION_CODE = 'WMASHIPWV')
                   AND P.permission_code IN
                          ('WMSHIPCONF','WMWAVECHCLSEDO')) APP1
        ON (    ramp.role_id = app1.role_id
            AND ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       VALUES (app1.role_id,
               app1.app_id,
               APP1.MODULE_ID,
               app1.permission_id,
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
               
               
MERGE INTO app_mod_perm amp
     USING (SELECT DISTINCT ramp.app_id, ramp.module_id, p.permission_id
              FROM app_mod_perm ramp, permission p
             WHERE ramp.permission_id IN (SELECT permission_id
                                            FROM permission
                                           WHERE permission_code = 'AOR')
                   AND p.PERMISSION_CODE IN
                          ('WMADDLPNDO','WMWAVECHCLSEDO','CNCLDO')) app1
        ON (     amp.app_id = app1.app_id
            AND amp.module_id = app1.module_id
            AND amp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (amp.app_id,
               amp.module_id,
               amp.permission_id,
               amp.row_uid)
       VALUES (app1.app_id,
               APP1.MODULE_ID,
               app1.permission_id,
               SEQ_APP_MOD_PERM.NEXTVAL);               
               
MERGE INTO role_app_mod_perm ramp
     USING (SELECT DISTINCT role_id,
                            app_id,
                            module_id,
                            p.permission_id
              FROM ROLE_APP_MOD_PERM ramp, permission p
             WHERE ramp.PERMISSION_ID = (SELECT PERMISSION_ID
                                           FROM PERMISSION
                                          WHERE PERMISSION_CODE = 'AOR')
                   AND P.permission_code IN
                          ('WMADDLPNDO','WMWAVECHCLSEDO','CNCLDO')) APP1
        ON (    ramp.role_id = app1.role_id
            AND ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       VALUES (app1.role_id,
               app1.app_id,
               APP1.MODULE_ID,
               app1.permission_id,
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
               
               
MERGE INTO app_mod_perm amp
     USING (SELECT DISTINCT ramp.app_id, ramp.module_id, p.permission_id
              FROM app_mod_perm ramp, permission p
             WHERE ramp.permission_id IN (SELECT permission_id
                                            FROM permission
                                           WHERE permission_code = 'VOR')
                   AND p.PERMISSION_CODE IN
                          ('WMDOPCKRPT')) app1
        ON (     amp.app_id = app1.app_id
            AND amp.module_id = app1.module_id
            AND amp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (amp.app_id,
               amp.module_id,
               amp.permission_id,
               amp.row_uid)
       VALUES (app1.app_id,
               APP1.MODULE_ID,
               app1.permission_id,
               SEQ_APP_MOD_PERM.NEXTVAL);                  

MERGE INTO role_app_mod_perm ramp
     USING (SELECT DISTINCT role_id,
                            app_id,
                            module_id,
                            p.permission_id
              FROM ROLE_APP_MOD_PERM ramp, permission p
             WHERE ramp.PERMISSION_ID = (SELECT PERMISSION_ID
                                           FROM PERMISSION
                                          WHERE PERMISSION_CODE = 'VOR')
                   AND P.permission_code IN
                          ('WMDOPCKRPT')) APP1
        ON (    ramp.role_id = app1.role_id
            AND ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid)
       VALUES (app1.role_id,
               app1.app_id,
               APP1.MODULE_ID,
               app1.permission_id,
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
COMMIT;			   

-- DBTicket DB-2029 REFL
DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID = 1;

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'OTBND'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMPACKSTN'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID = 1;
       END IF;
END;
/	
               
COMMIT;

-- DBTicket DB-2179 REFL
INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (seq_permission_id.NEXTVAL,
             'RF Print oLPN Cart labels',
             1,
             -1,
             'WMRFPRNTOLPNLAB',
             3,
             'Manhattan Associates',
             current_timestamp,
             3,
             'Manhattan Associates',
             current_timestamp,
             'RF Print oLPN Labels');

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID,
                          CREATED_DTTM,
                          LAST_UPDATED_DTTM)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),
             (SELECT permission_id
                FROM permission
               WHERE PERMISSION_CODE = 'WMRFPRNTOLPNLAB'),
             (SELECT MAX (ROW_UID) + 1 FROM APP_MOD_PERM),
             current_timestamp,
             NULL);

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM company_type
                WHERE description = 'Shipper'),
             (SELECT permission_id
                FROM permission
               WHERE PERMISSION_CODE = 'WMRFPRNTOLPNLAB'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM),
             current_timestamp,
             NULL);

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'OTBND'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMRFPRNTOLPNLAB'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/
               
COMMIT;               

-- DBTicket DB-2221
INSERT INTO PERMISSION_TAG (TAG_ID,
                            DESCRIPTION,
                            CREATED_SOURCE_TYPE_ID,
                            CREATED_SOURCE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'Print Cart oLPN',
             1,
             'Seed',
             current_timestamp,
             1,
             'Seed',
             current_timestamp);
             
INSERT INTO PERMISSION_TAG_MAPPING
     VALUES ( (SELECT permission_id
                 FROM permission
                WHERE PERMISSION_CODE = 'WMRFPRNTOLPNLAB'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'Print Cart oLPN'));
               
COMMIT;

-- DBTicket DB-2263 REFL
INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (seq_permission_id.NEXTVAL,
             'Administer Resend Pack Wave Download',
             1,
             -1,
             'WMARESENDPCKWAVE',
             3,
             'Manhattan Associates',
             current_timestamp,
             3,
             'Manhattan Associates',
             current_timestamp,
             'Allows the user to resend pack wave download');

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID,
                          CREATED_DTTM,
                          LAST_UPDATED_DTTM)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),
             (SELECT permission_id
                FROM permission
               WHERE PERMISSION_CODE = 'WMARESENDPCKWAVE'),
             (SELECT MAX (ROW_UID) + 1 FROM APP_MOD_PERM),
             current_timestamp,
             NULL);

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM company_type
                WHERE description = 'Shipper'),
             (SELECT permission_id
                FROM permission
               WHERE PERMISSION_CODE = 'WMARESENDPCKWAVE'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM),
             current_timestamp,
             NULL);

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'OTBND'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMARESENDPCKWAVE'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'OTBND'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMARESENDPCKWAVE'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/               
               
DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'OTBND'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMARESENDPCKWAVE'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/                

COMMIT;

-- DBTicket DB-2277
INSERT INTO PERMISSION_TAG (TAG_ID,
                            DESCRIPTION,
                            CREATED_SOURCE_TYPE_ID,
                            CREATED_SOURCE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'PackWave',
             1,
             'Seed',
             current_timestamp,
             1,
             'Seed',
             current_timestamp);
             
INSERT INTO PERMISSION_TAG_MAPPING
     VALUES ( (SELECT permission_id
                 FROM permission
                WHERE PERMISSION_CODE = 'WMARESENDPCKWAVE'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'PackWave'));
               
COMMIT; 

-- DBTicket DB-2407 REFL
MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMATGELG') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMATGELG'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMVTGELG') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVTGELG'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Warehouse Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMATGELG')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT role_id
                   FROM role
                  WHERE role_name = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMATGELG'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Warehouse Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMVTGELG')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT ROLE_ID
                   FROM role
                  WHERE ROLE_NAME = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVTGELG'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMATSPHDEF') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMATSPHDEF'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMVTSPHDEF') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVTSPHDEF'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Warehouse Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMATSPHDEF')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT role_id
                   FROM role
                  WHERE role_name = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMATSPHDEF'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Warehouse Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMVTSPHDEF')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT role_id
                   FROM role
                  WHERE role_name = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMVTSPHDEF'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMAYUP') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMAYUP'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMVYUP') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVYUP'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Warehouse Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMAYUP')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT ROLE_ID
                   FROM role
                  WHERE ROLE_NAME = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMAYUP'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);


MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Warehouse Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMVYUP')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT ROLE_ID
                   FROM role
                  WHERE ROLE_NAME = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Warehouse Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVYUP'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);
COMMIT;



-- DBTicket DB-1848 REFL
INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM PERMISSION
               WHERE permission_code = 'YMATGELG'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));
            
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMATGELG'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM PERMISSION
               WHERE permission_code = 'YMVTGELG'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));


INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMVTGELG'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);
			 
INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM PERMISSION
               WHERE permission_code = 'YMATSPHDEF'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMATSPHDEF'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);


INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM PERMISSION
               WHERE permission_code = 'YMVTSPHDEF'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMVTSPHDEF'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);	

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'YMAYUP'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMAYUP'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'YMVYUP'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMVYUP'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);
COMMIT;	

-- DBTicket DB-2618 REFL
DECLARE
   v_seq_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_seq_count
     FROM user_sequences
    WHERE sequence_name = 'SEQ_RELTP_APP_MOD_PERM';

   IF v_seq_count = 0
   THEN
      EXECUTE IMMEDIATE 'CREATE SEQUENCE SEQ_RELTP_APP_MOD_PERM';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

DECLARE
   v_seq_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_seq_count
     FROM user_sequences
    WHERE sequence_name = 'SEQ_APP_MOD_PERM';

   IF v_seq_count = 0
   THEN
      EXECUTE IMMEDIATE 'CREATE SEQUENCE SEQ_APP_MOD_PERM';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

CALL sequpdt ('RELTP_APP_MOD_PERM', 'ROW_UID', 'SEQ_RELTP_APP_MOD_PERM');
CALL sequpdt ('ROLE_APP_MOD_PERM', 'ROW_UID', 'SEQ_ROLEAPPMODPERM_ROWUID');
CALL sequpdt ('APP_MOD_PERM', 'ROW_UID', 'SEQ_APP_MOD_PERM');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'OTBND'
                   AND PERMISSION.PERMISSION_CODE = 'ADMIN_THEME') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               SEQ_APP_MOD_PERM.NEXTVAL);

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'OTBND'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'ADMIN_THEME'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'OTBND'
                   AND PERMISSION.PERMISSION_CODE = 'ADMIN_THEME') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               SEQ_RELTP_APP_MOD_PERM.NEXTVAL);
               
COMMIT;
call quiet_drop('SEQUENCE','SEQ_RELTP_APP_MOD_PERM');

-- DBTicket DB-2743 REFL
INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (
               SEQ_PERMISSION_ID.NEXTVAL,
               'Administer YMS Rule Priority ',
               1,
               -1,
               'YMAR',
               3,
               'Manhattan Associates',
               CURRENT_TIMESTAMP,
               3,
               'Manhattan Associates',
               CURRENT_TIMESTAMP,
               'Allow users to execute edit, add, delete and copy operations on YMS Rule priority UI');

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'View YMS Rule Priority ',
             1,
             -1,
             'YMVR',
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             'Allow users to View data in YMS Rule priority UI');

INSERT INTO app_mod_perm (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID,
                          CREATED_DTTM,
                          LAST_UPDATED_DTTM)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'YMAR'),
             (SEQ_APP_MOD_PERM.NEXTVAL),
             CURRENT_TIMESTAMP,
             NULL);

INSERT INTO app_mod_perm (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID,
                          CREATED_DTTM,
                          LAST_UPDATED_DTTM)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMVR'),
             (SEQ_APP_MOD_PERM.NEXTVAL),
             CURRENT_TIMESTAMP,
             NULL);

INSERT INTO role_app_mod_perm (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE role_name = 'YMMGRROLE'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'YMAR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             CURRENT_TIMESTAMP,
             NULL);
             
INSERT INTO role_app_mod_perm (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM)
     VALUES ( (SELECT ROLE_ID
                 FROM role
                WHERE ROLE_NAME = 'YMMGRROLE'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMVR'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
             CURRENT_TIMESTAMP,
             NULL);

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'YARD'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'YMAR'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/
             
DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'YARD'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'YMVR'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/             


INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'YMAR'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO reltp_app_mod_perm (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMAR'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);


INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'YMVR'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO reltp_app_mod_perm (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'YMVR'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);
COMMIT;

-- DBTicket DB-2744
INSERT INTO permission_tag (Tag_id,
                            description,
                            created_source_type_id,
                            created_source,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'PriorityRule',
             1,
             'Seed',
             1,
             'Seed');

INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'YMAR'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'PriorityRule'));

INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'YMVR'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'PriorityRule'));
COMMIT;

-- DBTicket DB-2739 REFL
DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                  FROM app_mod_perm
                 WHERE permission_id IN
                          (SELECT permission_id
                             FROM permission
                            WHERE permission_code = 'WMAESLCKCNT')),
               (SELECT MODULE_ID
                  FROM app_mod_perm
                 WHERE permission_id IN
                          (SELECT permission_id
                             FROM permission
                            WHERE permission_code = 'WMAESLCKCNT')),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'WMAESLCKCNT'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/

-- DBTicket DB-2941 REFL 
INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'View Bulk Update Case',
             1,
             -1,
             'WMBULKUPDVAL',
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             NULL);
COMMIT;

-- -- DBTicket DB-2680 REFL
-- INSERT INTO APP_MOD_PERM (APP_ID,
--                           MODULE_ID,
--                           PERMISSION_ID,
--                           ROW_UID)
--      VALUES ( (SELECT APP_ID
--                  FROM APP
--                 WHERE APP_NAME = 'Warehouse Management'),
--              (SELECT MODULE_ID
--                 FROM MODULE
--                WHERE MODULE_CODE = 'TRANS'),
--              (SELECT PERMISSION_ID
--                 FROM PERMISSION
--                WHERE PERMISSION_CODE = 'PRTEPIDOC'),
--              (SELECT MAX (ROW_UID) + 4 FROM APP_MOD_PERM));
-- 
-- MERGE INTO RELTP_APP_MOD_PERM RAMP
--      USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
--                    APP.APP_ID,
--                    APP.APP_NAME,
--                    MODULE.MODULE_ID,
--                    MODULE.MODULE_CODE,
--                    PERMISSION.PERMISSION_ID,
--                    PERMISSION.PERMISSION_CODE
--               FROM RELATIONSHIP_TYPE,
--                    APP,
--                    MODULE,
--                    PERMISSION
--              WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
--                    AND APP.APP_NAME = 'Warehouse Management'
--                    AND MODULE.MODULE_CODE = 'TRANS'
--                    AND PERMISSION.PERMISSION_CODE = 'PRTEPIDOC') APP1
--         ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
--             AND RAMP.APP_ID = APP1.APP_ID
--             AND RAMP.MODULE_ID = APP1.MODULE_ID
--             AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
--                RAMP.APP_ID,
--                RAMP.MODULE_ID,
--                RAMP.PERMISSION_ID,
--                RAMP.ROW_UID)
--        VALUES (4,
--                APP1.APP_ID,
--                APP1.MODULE_ID,
--                APP1.PERMISSION_ID,
--                (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));
-- 
-- 
-- DECLARE
--    V_COUNT   INTEGER;
-- BEGIN
--    SELECT COUNT (1)
--      INTO V_COUNT
--      FROM ROLE
--     WHERE ROLE_NAME = 'WMROLE'
--           AND COMPANY_ID IN (SELECT COMPANY_ID
--                                FROM COMPANY
--                               WHERE COMPANY_NAME = '01');
-- 
--    IF V_COUNT = 1
--    THEN
--       INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
--                                      APP_ID,
--                                      MODULE_ID,
--                                      PERMISSION_ID,
--                                      ROW_UID)
--          SELECT ROLE.ROLE_ID,
--                 (SELECT APP_ID
--                    FROM APP
--                   WHERE APP_NAME = 'Warehouse Management'),
--                 (SELECT MODULE_ID
--                    FROM MODULE
--                   WHERE MODULE_CODE = 'TRANS'),
--                 (SELECT PERMISSION_ID
--                    FROM PERMISSION
--                   WHERE PERMISSION_CODE = 'PRTEPIDOC'),
--                 SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
--            FROM ROLE
--           WHERE ROLE_NAME = 'WMROLE'
--                 AND COMPANY_ID IN (SELECT COMPANY_ID
--                                      FROM COMPANY
--                                     WHERE COMPANY_NAME = '01');
--    END IF;
-- END;
-- /
-- COMMIT;

-- DBTicket DB-3198 REFL
MERGE INTO permission P
     USING (SELECT 'View Lot Configuration' PERMISSION_NAME,
                   'WMVLOTCONFIG' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME
            AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE,
               P.DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               B.PERMISSION_NAME,
               1,
               -1,
               B.PERMISSION_CODE,
               'Allow users to view lot configuration');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'INVNMGMT'
                   AND PERMISSION.PERMISSION_CODE = 'WMVLOTCONFIG') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               SEQ_APP_MOD_PERM.NEXTVAL);

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
    MERGE INTO role_app_mod_perm ramp
         USING (SELECT amp.app_id app_id,
                       amp.module_id module_id,
                       p.permission_id permission_id
                  FROM app_mod_perm amp, PERMISSION p
                 WHERE     app_id = (SELECT app_id
                                       FROM app
                                      WHERE app_name = 'Warehouse Management')
                       AND p.permission_id = amp.permission_id
                       AND module_id = (SELECT module_id
                                          FROM module
                                         WHERE module_code = 'INVNMGMT')
                       AND p.permission_id =
                              (SELECT PERMISSION_ID
                                 FROM PERMISSION
                                WHERE PERMISSION_CODE = 'WMVLOTCONFIG')) APP1
            ON (    ramp.app_id = app1.app_id
                AND ramp.module_id = app1.module_id
                AND ramp.permission_id = app1.permission_id)
    WHEN NOT MATCHED
    THEN
       INSERT     (ramp.role_id,
                   ramp.app_id,
                   ramp.module_id,
                   ramp.permission_id,
                   ramp.row_uid)
           VALUES ( (SELECT role_id
                       FROM role
                      WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                   app1.app_id,
                   APP1.MODULE_ID,
                   app1.permission_id,
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

MERGE INTO company_type_perm ctp
     USING (SELECT company_type.company_type_id,
                   company_type.description,
                   permission.permission_id,
                   permission.permission_code
              FROM company_type, permission
             WHERE LOWER (company_type.description) = LOWER ('Shipper')
                   AND permission.permission_code = 'WMVLOTCONFIG') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES ( (SELECT company_type_id
                   FROM company_type
                  WHERE LOWER (description) = LOWER ('Shipper')),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'WMVLOTCONFIG'),
               (SELECT MAX (row_uid) + 4 FROM company_type_perm));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'INVNMGMT'
                   AND PERMISSION.PERMISSION_CODE = 'WMVLOTCONFIG') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));

MERGE INTO permission P
     USING (SELECT 'Administer Lot Configuration' PERMISSION_NAME,
                   'WMALOTCONFIG' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME
            AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE,
               P.DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               B.PERMISSION_NAME,
               1,
               -1,
               B.PERMISSION_CODE,
               'Allow users to administer lot configuration');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'INVNMGMT'
                   AND PERMISSION.PERMISSION_CODE = 'WMALOTCONFIG') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               SEQ_APP_MOD_PERM.NEXTVAL);

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
    MERGE INTO role_app_mod_perm ramp
         USING (SELECT amp.app_id app_id,
                       amp.module_id module_id,
                       p.permission_id permission_id
                  FROM app_mod_perm amp, PERMISSION p
                 WHERE     app_id = (SELECT app_id
                                       FROM app
                                      WHERE app_name = 'Warehouse Management')
                       AND p.permission_id = amp.permission_id
                       AND module_id = (SELECT module_id
                                          FROM module
                                         WHERE module_code = 'INVNMGMT')
                       AND p.permission_id =
                              (SELECT PERMISSION_ID
                                 FROM PERMISSION
                                WHERE PERMISSION_CODE = 'WMALOTCONFIG')) APP1
            ON (    ramp.app_id = app1.app_id
                AND ramp.module_id = app1.module_id
                AND ramp.permission_id = app1.permission_id)
    WHEN NOT MATCHED
    THEN
       INSERT     (ramp.role_id,
                   ramp.app_id,
                   ramp.module_id,
                   ramp.permission_id,
                   ramp.row_uid)
           VALUES ( (SELECT role_id
                       FROM role
                      WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                   app1.app_id,
                   APP1.MODULE_ID,
                   app1.permission_id,
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

MERGE INTO company_type_perm ctp
     USING (SELECT company_type.company_type_id,
                   company_type.description,
                   permission.permission_id,
                   permission.permission_code
              FROM company_type, permission
             WHERE LOWER (company_type.description) = LOWER ('Shipper')
                   AND permission.permission_code = 'WMALOTCONFIG') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES ( (SELECT company_type_id
                   FROM company_type
                  WHERE LOWER (description) = LOWER ('Shipper')),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'WMALOTCONFIG'),
               (SELECT MAX (row_uid) + 4 FROM company_type_perm));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'INVNMGMT'
                   AND PERMISSION.PERMISSION_CODE = 'WMALOTCONFIG') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));
			   
MERGE INTO permission P
     USING (SELECT 'View User Profile Configuration' PERMISSION_NAME,
                   'WMVUSERPROF' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME
            AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE,
               P.DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               B.PERMISSION_NAME,
               1,
               -1,
               B.PERMISSION_CODE,
               'Allow users to view user profile configurations');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMVUSERPROF') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               SEQ_APP_MOD_PERM.NEXTVAL);

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
    MERGE INTO role_app_mod_perm ramp
         USING (SELECT amp.app_id app_id,
                       amp.module_id module_id,
                       p.permission_id permission_id
                  FROM app_mod_perm amp, PERMISSION p
                 WHERE     app_id = (SELECT app_id
                                       FROM app
                                      WHERE app_name = 'Warehouse Management')
                       AND p.permission_id = amp.permission_id
                       AND module_id = (SELECT module_id
                                          FROM module
                                         WHERE module_code = 'SYSCTRL')
                       AND p.permission_id =
                              (SELECT PERMISSION_ID
                                 FROM PERMISSION
                                WHERE PERMISSION_CODE = 'WMVUSERPROF')) APP1
            ON (    ramp.app_id = app1.app_id
                AND ramp.module_id = app1.module_id
                AND ramp.permission_id = app1.permission_id)
    WHEN NOT MATCHED
    THEN
       INSERT     (ramp.role_id,
                   ramp.app_id,
                   ramp.module_id,
                   ramp.permission_id,
                   ramp.row_uid)
           VALUES ( (SELECT role_id
                       FROM role
                      WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                   app1.app_id,
                   APP1.MODULE_ID,
                   app1.permission_id,
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

MERGE INTO company_type_perm ctp
     USING (SELECT company_type.company_type_id,
                   company_type.description,
                   permission.permission_id,
                   permission.permission_code
              FROM company_type, permission
             WHERE LOWER (company_type.description) = LOWER ('Shipper')
                   AND permission.permission_code = 'WMVUSERPROF') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES ( (SELECT company_type_id
                   FROM company_type
                  WHERE LOWER (description) = LOWER ('Shipper')),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'WMVUSERPROF'),
               (SELECT MAX (row_uid) + 4 FROM company_type_perm));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMVUSERPROF') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));

MERGE INTO permission P
     USING (SELECT 'Administer User Profile Configuration' PERMISSION_NAME,
                   'WMAUSERPROF' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME
            AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE,
               P.DESCRIPTION)
       VALUES (
                 SEQ_PERMISSION_ID.NEXTVAL,
                 B.PERMISSION_NAME,
                 1,
                 -1,
                 B.PERMISSION_CODE,
                 'Allow users to add, delete and copy user profile configurations');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMAUSERPROF') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               SEQ_APP_MOD_PERM.NEXTVAL);

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
    MERGE INTO role_app_mod_perm ramp
         USING (SELECT amp.app_id app_id,
                       amp.module_id module_id,
                       p.permission_id permission_id
                  FROM app_mod_perm amp, PERMISSION p
                 WHERE     app_id = (SELECT app_id
                                       FROM app
                                      WHERE app_name = 'Warehouse Management')
                       AND p.permission_id = amp.permission_id
                       AND module_id = (SELECT module_id
                                          FROM module
                                         WHERE module_code = 'SYSCTRL')
                       AND p.permission_id =
                              (SELECT PERMISSION_ID
                                 FROM PERMISSION
                                WHERE PERMISSION_CODE = 'WMAUSERPROF')) APP1
            ON (    ramp.app_id = app1.app_id
                AND ramp.module_id = app1.module_id
                AND ramp.permission_id = app1.permission_id)
    WHEN NOT MATCHED
    THEN
       INSERT     (ramp.role_id,
                   ramp.app_id,
                   ramp.module_id,
                   ramp.permission_id,
                   ramp.row_uid)
           VALUES ( (SELECT role_id
                       FROM role
                      WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                   app1.app_id,
                   APP1.MODULE_ID,
                   app1.permission_id,
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

MERGE INTO company_type_perm ctp
     USING (SELECT company_type.company_type_id,
                   company_type.description,
                   permission.permission_id,
                   permission.permission_code
              FROM company_type, permission
             WHERE LOWER (company_type.description) = LOWER ('Shipper')
                   AND permission.permission_code = 'WMAUSERPROF') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES ( (SELECT company_type_id
                   FROM company_type
                  WHERE LOWER (description) = LOWER ('Shipper')),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'WMAUSERPROF'),
               (SELECT MAX (row_uid) + 4 FROM company_type_perm));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMAUSERPROF') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));			   
COMMIT;			   

-- DBTicket DB-3259
INSERT INTO PERMISSION_TAG (TAG_ID,
                            DESCRIPTION,
                            CREATED_SOURCE_TYPE_ID,
                            CREATED_SOURCE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'Lot Configuration',
             1,
             'Seed',
             CURRENT_TIMESTAMP,
             1,
             'Seed',
             CURRENT_TIMESTAMP);

INSERT INTO PERMISSION_TAG_MAPPING
     VALUES ( (SELECT permission_id
                 FROM permission
                WHERE PERMISSION_CODE = 'WMVLOTCONFIG'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'Lot Configuration'));

INSERT INTO PERMISSION_TAG_MAPPING
     VALUES ( (SELECT permission_id
                 FROM permission
                WHERE PERMISSION_CODE = 'WMALOTCONFIG'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'Lot Configuration'));
			   
INSERT INTO PERMISSION_TAG (TAG_ID,
                            DESCRIPTION,
                            CREATED_SOURCE_TYPE_ID,
                            CREATED_SOURCE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'User Profile Configuration',
             1,
             'Seed',
             CURRENT_TIMESTAMP,
             1,
             'Seed',
             CURRENT_TIMESTAMP);

INSERT INTO PERMISSION_TAG_MAPPING
     VALUES ( (SELECT permission_id
                 FROM permission
                WHERE PERMISSION_CODE = 'WMVUSERPROF'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'User Profile Configuration'));

INSERT INTO PERMISSION_TAG_MAPPING
     VALUES ( (SELECT permission_id
                 FROM permission
                WHERE PERMISSION_CODE = 'WMAUSERPROF'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'User Profile Configuration'));			   
COMMIT;

-- DBTicket DB-3202 REFL
BEGIN
    FOR cur
      IN (SELECT role_id
            FROM role_app_mod_perm
           WHERE permission_id IN (SELECT permission_id
                                     FROM permission
                                    WHERE permission_code IN ('WMAACTTRKG'))
                 AND role_id NOT IN
                        (SELECT role_id
                           FROM role_app_mod_perm
                          WHERE permission_id IN
                                   (SELECT permission_id
                                      FROM permission
                                     WHERE permission_code IN
                                              ('WMVUSERPROF',
                                               'WMAUSERPROF',
                                               'WMVLOTCONFIG',
                                               'WMALOTCONFIG'))))
    LOOP

      INSERT INTO role_app_mod_perm (role_id,
                                     app_id,
                                     module_id,
                                     permission_id,
                                     row_uid)
           VALUES (cur.role_id,
                   (SELECT app_id
                      FROM app
                     WHERE app_name = 'Warehouse Management'),
                   (SELECT module_id
                      FROM module
                     WHERE module_code = 'SYSCTRL'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'WMAUSERPROF'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

      INSERT INTO role_app_mod_perm (role_id,
                                     app_id,
                                     module_id,
                                     permission_id,
                                     row_uid)
           VALUES (cur.role_id,
                   (SELECT app_id
                      FROM app
                     WHERE app_name = 'Warehouse Management'),
                   (SELECT module_id
                      FROM module
                     WHERE module_code = 'INVNMGMT'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'WMALOTCONFIG'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

      INSERT INTO role_app_mod_perm (role_id,
                                     app_id,
                                     module_id,
                                     permission_id,
                                     row_uid)
           VALUES (cur.role_id,
                   (SELECT app_id
                      FROM app
                     WHERE app_name = 'Warehouse Management'),
                   (SELECT module_id
                      FROM module
                     WHERE module_code = 'SYSCTRL'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'WMVUSERPROF'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

      INSERT INTO role_app_mod_perm (role_id,
                                     app_id,
                                     module_id,
                                     permission_id,
                                     row_uid)
           VALUES (cur.role_id,
                   (SELECT app_id
                      FROM app
                     WHERE app_name = 'Warehouse Management'),
                   (SELECT module_id
                      FROM module
                     WHERE module_code = 'INVNMGMT'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'WMVLOTCONFIG'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END LOOP;
   
end;   
/
begin
   FOR cur
      IN (SELECT role_id
            FROM role_app_mod_perm
           WHERE permission_id IN (SELECT permission_id
                                     FROM permission
                                    WHERE permission_code IN ('WMVACTTRKG'))
                 AND role_id NOT IN
                        (SELECT role_id
                           FROM role_app_mod_perm
                          WHERE permission_id IN
                                   (SELECT permission_id
                                      FROM permission
                                     WHERE permission_code IN ('WMVUSERPROF','WMVLOTCONFIG','WMAUSERPROF', 'WMALOTCONFIG' ,'WMAACTTRKG'))))
   LOOP
      INSERT INTO role_app_mod_perm (role_id,
                                     app_id,
                                     module_id,
                                     permission_id,
                                     row_uid)
           VALUES (cur.role_id,
                   (SELECT app_id
                      FROM app
                     WHERE app_name = 'Warehouse Management'),
                   (SELECT module_id
                      FROM module
                     WHERE module_code = 'SYSCTRL'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'WMVUSERPROF'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
                                
      INSERT INTO role_app_mod_perm (role_id,
                                     app_id,
                                     module_id,
                                     permission_id,
                                     row_uid)
           VALUES (cur.role_id,
                   (SELECT app_id
                      FROM app
                     WHERE app_name = 'Warehouse Management'),
                   (SELECT module_id
                      FROM module
                     WHERE module_code = 'INVNMGMT'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'WMVLOTCONFIG'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);                             
                                                                   
   END LOOP;
END;
/
Commit;

-- DBTicket DB-2680 REFL
MERGE INTO app_module amp
     USING (SELECT app.app_id,
                   app.app_name,
                   module.module_id,
                   module.module_code
              FROM app, module
             WHERE app.app_short_name = 'WMS' AND module.module_code = 'EPI') app1
        ON (amp.app_id = app1.app_id AND amp.module_id = app1.module_id)
WHEN NOT MATCHED
THEN
   INSERT     (app_id,
               module_id,
               is_active,
               row_uid)
       VALUES ( (SELECT app_id
                   FROM app
                  WHERE app_short_name = 'WMS'),
               (SELECT module_id
                  FROM module
                 WHERE module_code = 'EPI'),
               1,
               (SELECT MAX (row_uid) + 4 FROM app_module));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'EPI'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'PRTEPIDOC'),
             (SELECT MAX (ROW_UID) + 4 FROM APP_MOD_PERM));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'EPI'
                   AND PERMISSION.PERMISSION_CODE = 'PRTEPIDOC') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));


DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID,
                                     ROW_UID)
         SELECT ROLE.ROLE_ID,
                (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Warehouse Management'),
                (SELECT MODULE_ID
                   FROM MODULE
                  WHERE MODULE_CODE = 'EPI'),
                (SELECT PERMISSION_ID
                   FROM PERMISSION
                  WHERE PERMISSION_CODE = 'PRTEPIDOC'),
                SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
           FROM ROLE
          WHERE ROLE_NAME = 'WMROLE'
                AND COMPANY_ID IN (SELECT COMPANY_ID
                                     FROM COMPANY
                                    WHERE COMPANY_NAME = '01');
   END IF;
END;
/

COMMIT;

-- DBTicket DB-3430 REFL
INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'WMRFOTB'),
             (SELECT permission_id
                FROM permission
               WHERE PERMISSION_CODE = 'WMRFPRNTOLPNLAB'),
             SEQ_APP_MOD_PERM.NEXTVAL);

COMMIT;

-- DBTicket DB-3535 REFL
MERGE INTO PERMISSION p
     USING (SELECT 'Administer Mobile Workflows' PERMISSION_NAME,
                   'AMOBILEWORKFLOWS' PERMISSION_CODE
              FROM DUAL) B
        ON (p.PERMISSION_NAME = B.PERMISSION_NAME
            AND p.PERMISSION_CODE = b.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_NAME,
               IS_ACTIVE,
               COMPANY_ID,
               PERMISSION_CODE)
       VALUES (seq_permission_id.NEXTVAL,
               B.PERMISSION_NAME,
               1,
               -1,
               B.PERMISSION_CODE);

MERGE INTO company_type_perm ctp
     USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'AMOBILEWORKFLOWS') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES (
                 4,
                 ctp1.permission_id,
                 (SELECT MAX (row_uid) + 4 FROM company_type_perm));

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'AMOBILEWORKFLOWS') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.NEXTVAL));

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                                     APP_ID,
                                     MODULE_ID,
                                     PERMISSION_ID,
                                     ROW_UID)
         SELECT ROLE.ROLE_ID,
                (SELECT APP_ID
                   FROM app_mod_perm
                  WHERE permission_id IN
                           (SELECT permission_id
                              FROM permission
                             WHERE permission_code = 'AMOBILEWORKFLOWS')),
                (SELECT MODULE_ID
                   FROM app_mod_perm
                  WHERE permission_id IN
                           (SELECT permission_id
                              FROM permission
                             WHERE permission_code = 'AMOBILEWORKFLOWS')),
                (SELECT permission_id
                   FROM permission
                  WHERE permission_code = 'AMOBILEWORKFLOWS'),
                SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
           FROM ROLE
          WHERE ROLE_NAME = 'WMROLE'
                AND COMPANY_ID IN (SELECT COMPANY_ID
                                     FROM COMPANY
                                    WHERE COMPANY_NAME = '01');
   END IF;
END;
/

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'AMOBILEWORKFLOWS') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID,
               RAMP.IS_PERM_IMPLIED)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM),
               0);
			   
COMMIT;

-- DBTicket DB-3673 REFL
UPDATE permission
   SET company_id = -1, description = 'Allows the user to view EOD UI'
 WHERE permission_code = 'EPIEOD';

COMMIT;

-- DBTicket DB-3746 REFL

MERGE INTO PERMISSION p
     USING (SELECT 'Administer Hierarchy' PERMISSION_NAME,
                   'WMAHIER' PERMISSION_CODE
              FROM DUAL) B
        ON (p.PERMISSION_NAME = B.PERMISSION_NAME
            AND p.PERMISSION_CODE = b.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_NAME,
               IS_ACTIVE,
               COMPANY_ID,
               PERMISSION_CODE)
       VALUES (seq_permission_id.NEXTVAL,
               B.PERMISSION_NAME,
               1,
               -1,
               B.PERMISSION_CODE);
               

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMAHIER') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.NEXTVAL));
               
DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id
                            FROM role
                           WHERE     role_name = 'WMROLE'
                                 AND COMPANY_ID IN
                                        (SELECT COMPANY_ID
                                           FROM COMPANY
                                          WHERE COMPANY_NAME = '01'))
                            role_id,
                         (SELECT MODULE_ID
                            FROM app_mod_perm
                           WHERE permission_id IN
                                    (SELECT permission_id
                                       FROM permission
                                      WHERE permission_code = 'WMAHIER'))
                            module_id,
                         (SELECT permission_id
                            FROM permission
                           WHERE permission_code = 'WMAHIER')
                            permission_id,
                         (SELECT APP_ID
                            FROM app_mod_perm
                           WHERE permission_id IN
                                    (SELECT permission_id
                                       FROM permission
                                      WHERE permission_code = 'WMAHIER'))
                            app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id
                  AND rp.role_id = b.role_id
                  AND rp.permission_id = b.permission_id
                  AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID
                          FROM role
                         WHERE     role_name = 'WMROLE'
                               AND COMPANY_ID IN (SELECT COMPANY_ID
                                                    FROM COMPANY
                                                   WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID
                          FROM app_mod_perm
                         WHERE permission_id IN
                                  (SELECT permission_id
                                     FROM permission
                                    WHERE permission_code = 'WMAHIER')),
                       (SELECT MODULE_ID
                          FROM app_mod_perm
                         WHERE permission_id IN
                                  (SELECT permission_id
                                     FROM permission
                                    WHERE permission_code = 'WMAHIER')),
                       (SELECT permission_id
                          FROM permission
                         WHERE permission_code = 'WMAHIER'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/


MERGE INTO PERMISSION p
     USING (SELECT 'View Hierarchy' PERMISSION_NAME,
                   'WMVHIER' PERMISSION_CODE
              FROM DUAL) B
        ON (p.PERMISSION_NAME = B.PERMISSION_NAME
            AND p.PERMISSION_CODE = b.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_NAME,
               IS_ACTIVE,
               COMPANY_ID,
               PERMISSION_CODE)
       VALUES (seq_permission_id.NEXTVAL,
               B.PERMISSION_NAME,
               1,
               -1,
               B.PERMISSION_CODE);
               

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMVHIER') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.NEXTVAL));
               
DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id
                            FROM role
                           WHERE     role_name = 'WMROLE'
                                 AND COMPANY_ID IN
                                        (SELECT COMPANY_ID
                                           FROM COMPANY
                                          WHERE COMPANY_NAME = '01'))
                            role_id,
                         (SELECT MODULE_ID
                            FROM app_mod_perm
                           WHERE permission_id IN
                                    (SELECT permission_id
                                       FROM permission
                                      WHERE permission_code = 'WMVHIER'))
                            module_id,
                         (SELECT permission_id
                            FROM permission
                           WHERE permission_code = 'WMVHIER')
                            permission_id,
                         (SELECT APP_ID
                            FROM app_mod_perm
                           WHERE permission_id IN
                                    (SELECT permission_id
                                       FROM permission
                                      WHERE permission_code = 'WMVHIER'))
                            app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id
                  AND rp.role_id = b.role_id
                  AND rp.permission_id = b.permission_id
                  AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID
                          FROM role
                         WHERE     role_name = 'WMROLE'
                               AND COMPANY_ID IN (SELECT COMPANY_ID
                                                    FROM COMPANY
                                                   WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID
                          FROM app_mod_perm
                         WHERE permission_id IN
                                  (SELECT permission_id
                                     FROM permission
                                    WHERE permission_code = 'WMVHIER')),
                       (SELECT MODULE_ID
                          FROM app_mod_perm
                         WHERE permission_id IN
                                  (SELECT permission_id
                                     FROM permission
                                    WHERE permission_code = 'WMVHIER')),
                       (SELECT permission_id
                          FROM permission
                         WHERE permission_code = 'WMVHIER'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

commit;

-- DBTicket DB-3788 REFL

MERGE INTO company_type_perm ctp
     USING (SELECT company_type.company_type_id,
                   company_type.description,
                   permission.permission_id,
                   permission.permission_code
              FROM company_type, permission
             WHERE LOWER (company_type.description) = LOWER ('Shipper')
                   AND permission.permission_code = 'WMAHIER') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES ( (SELECT company_type_id
                   FROM company_type
                  WHERE LOWER (description) = LOWER ('Shipper')),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'WMAHIER'),
               (SELECT MAX (row_uid) + 4 FROM company_type_perm));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMAHIER') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));



MERGE INTO company_type_perm ctp
     USING (SELECT company_type.company_type_id,
                   company_type.description,
                   permission.permission_id,
                   permission.permission_code
              FROM company_type, permission
             WHERE LOWER (company_type.description) = LOWER ('Shipper')
                   AND permission.permission_code = 'WMVHIER') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES ( (SELECT company_type_id
                   FROM company_type
                  WHERE LOWER (description) = LOWER ('Shipper')),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'WMVHIER'),
               (SELECT MAX (row_uid) + 4 FROM company_type_perm));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMVHIER') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));
			   
commit;

-- DBTicket DB-1553 REFL
INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Ship Confirm for DO',
             1,
             -1,
             'WMSHIPCONF',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             'Gives access to Ship Confirm button on the DO list page');
             


INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMSHIPCONF'),
             (SELECT MAX (row_uid) + 1 FROM company_type_perm));



INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),
       (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'WMSHIPCONF'), 
       (SELECT MAX(row_uid) + 1 FROM app_mod_perm));


INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES ( 4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),(select PERMISSION_ID from PERMISSION where PERMISSION_CODE='WMSHIPCONF'),
               (select max(ROW_UID)+ 1 from RELTP_APP_MOD_PERM),
               0);


merge into app_mod_perm amp using (select (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id,
(SELECT permission_id FROM permission WHERE permission_code = 'WMAHIER') permission_id,
			  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id from dual) B
on (amp.app_id = b.app_id and amp.permission_id=b.permission_id and amp.module_id = b.module_id)
when not matched
then
insert (APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID)
values (
(SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
(SELECT permission_id FROM permission WHERE permission_code = 'WMAHIER'),
(select MAX(row_uid) + 1 FROM app_mod_perm));

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMAHIER') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMAHIER'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

merge into app_mod_perm amp using (select (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id,
(SELECT permission_id FROM permission WHERE permission_code = 'WMSHIPCONF') permission_id,
			  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id from dual) B
on (amp.app_id = b.app_id and amp.permission_id=b.permission_id and amp.module_id = b.module_id)
when not matched
then
insert (APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID)
values (
(SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
(SELECT permission_id FROM permission WHERE permission_code = 'WMSHIPCONF'),
(select MAX(row_uid) + 1 FROM app_mod_perm));

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMOBMGRROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMSHIPCONF') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMSHIPCONF'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMWAVEMGRROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMSHIPCONF') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMSHIPCONF'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

INSERT INTO PERMISSION (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (
               SEQ_PERMISSION_ID.NEXTVAL,
               'WAVE / UNDO Wave, CREATE Chase Task AND CLOSE DO',
               1,
               -1,
               'WMWAVECHCLSEDO',
               3,
               'Manhattan Associates',
               SYSDATE,
               3,
               'Manhattan Associates',
               SYSDATE,
               'Gives access to "Wave", "Undo Wave", "Create Chase Task" and "Close" buttons on the DO list page.');
               
INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMWAVECHCLSEDO'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMWAVECHCLSEDO'),
             (SELECT MAX (row_uid) + 4 FROM app_mod_perm));
      

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMWAVECHCLSEDO'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMWAVECHCLSEDO') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMWAVECHCLSEDO'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMOBMGRROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMWAVECHCLSEDO') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMWAVECHCLSEDO'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMWAVEMGRROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMWAVECHCLSEDO') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMWAVECHCLSEDO'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Add LPN to DO',
             1,
             -1,
             'WMADDLPNDO',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             'Gives access to "Add LPN" button on the DO list page.');
     


                 

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMADDLPNDO'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));


INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMADDLPNDO'),
             (SELECT MAX (row_uid) + 4 FROM app_mod_perm));


INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMADDLPNDO'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);  

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMADDLPNDO') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMADDLPNDO'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMOBMGRROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMADDLPNDO') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMADDLPNDO'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMWAVEMGRROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMADDLPNDO') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMADDLPNDO'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (
               SEQ_PERMISSION_ID.NEXTVAL,
               'Print/View Order report and Packing Slip Report',
               1,
               -1,
               'WMDOPCKRPT',
               3,
               'Manhattan Associates',
               SYSDATE,
               3,
               'Manhattan Associates',
               SYSDATE,
               'Gives access to "View Packing Slip Report", "Print Packing Slip Report", "View Report" and "Print Report" buttons on the DO list page.');
                             

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMDOPCKRPT'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));
           

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMDOPCKRPT'),
             (SELECT MAX (row_uid) + 4 FROM app_mod_perm));
             
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Warehouse Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'OTBND'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'WMDOPCKRPT'),
             (SELECT MAX (ROW_UID) + 4 FROM RELTP_APP_MOD_PERM),
             0);             
   
DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMOBMGRROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMOBMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'WMWAVEMGRROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT ROLE.ROLE_ID FROM role WHERE role_name = 'WMWAVEMGRROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'OTBND'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );
   END IF;
END;
/

COMMIT;	

-- DBTicket DB-3760 REFL
exec sequpdt('ROLE','ROLE_ID','SEQ_ROLE_ID');
MERGE INTO role A
     USING (SELECT '-1' COMPANY_ID, 'wmsadmin' ROLE_NAME FROM DUAL) B
        ON (A.COMPANY_ID = B.COMPANY_ID AND A.ROLE_NAME = B.ROLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (ROLE_ID,
               COMPANY_ID,
               ROLE_NAME,
               IS_ROLE_PRIVATE,
               IS_ACTIVE,
               CREATED_SOURCE_TYPE_ID,
               CREATED_SOURCE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE_TYPE_ID,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_DTTM,
               HIBERNATE_VERSION,
               APPLY_TO_BUSINESS_PARTNERS,
               ROLE_TYPE_ID,
               DESCRIPTION,
               IS_DEFAULT)
       VALUES (SEQ_ROLE_ID.NEXTVAL,
               -1,
               'wmsadmin',
               0,
               1,
               1,
               'system',
               CURRENT_TIMESTAMP,
               1,
               'system',
               CURRENT_TIMESTAMP,
               0,
               0,
               4,
               NULL,
               0);

MERGE INTO role A
     USING (SELECT '-1' COMPANY_ID, 'wmsdefault' ROLE_NAME FROM DUAL) B
        ON (A.COMPANY_ID = B.COMPANY_ID AND A.ROLE_NAME = B.ROLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (ROLE_ID,
               COMPANY_ID,
               ROLE_NAME,
               IS_ROLE_PRIVATE,
               IS_ACTIVE,
               CREATED_SOURCE_TYPE_ID,
               CREATED_SOURCE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE_TYPE_ID,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_DTTM,
               HIBERNATE_VERSION,
               APPLY_TO_BUSINESS_PARTNERS,
               ROLE_TYPE_ID,
               DESCRIPTION,
               IS_DEFAULT)
       VALUES (SEQ_ROLE_ID.NEXTVAL,
               -1,
               'wmsdefault',
               0,
               1,
               1,
               'system',
               CURRENT_TIMESTAMP,
               1,
               'system',
               CURRENT_TIMESTAMP,
               0,
               0,
               4,
               NULL,
               0);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'TRANS'
                   AND PERMISSION.PERMISSION_CODE = 'WMSHIPCONF') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.NEXTVAL));

INSERT INTO role_app_mod_perm (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE role_name = 'wmsdefault' AND company_id = -1),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Warehouse Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'TRANS'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMSHIPCONF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

INSERT INTO role_app_mod_perm (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE role_name = 'wmsadmin' AND company_id = -1),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Warehouse Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'TRANS'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMSHIPCONF'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'TRANS'
                   AND PERMISSION.PERMISSION_CODE = 'WMWAVECHCLSEDO') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.NEXTVAL));

INSERT INTO role_app_mod_perm (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE role_name = 'wmsdefault' AND company_id = -1),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Warehouse Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'TRANS'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMWAVECHCLSEDO'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

INSERT INTO role_app_mod_perm (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE role_name = 'wmsadmin' AND company_id = -1),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Warehouse Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'TRANS'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMWAVECHCLSEDO'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'TRANS'
                   AND PERMISSION.PERMISSION_CODE = 'WMADDLPNDO') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.NEXTVAL));

INSERT INTO role_app_mod_perm (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE role_name = 'wmsdefault' AND company_id = -1),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Warehouse Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'TRANS'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMADDLPNDO'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

INSERT INTO role_app_mod_perm (ROLE_ID,
                               APP_ID,
                               MODULE_ID,
                               PERMISSION_ID,
                               ROW_UID)
     VALUES ( (SELECT role_id
                 FROM role
                WHERE role_name = 'wmsadmin' AND company_id = -1),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Warehouse Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'TRANS'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMADDLPNDO'),
             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'TRANS'
                   AND PERMISSION.PERMISSION_CODE = 'WMDOPCKRPT') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.NEXTVAL));

MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'wmsdefault' AND company_id = -1) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'TRANS') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT role_id FROM role WHERE role_name = 'wmsdefault' AND company_id = -1),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'TRANS'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );

MERGE INTO role_app_mod_perm rp
           USING (SELECT (SELECT role_id FROM role WHERE role_name = 'wmsadmin' AND company_id = -1) role_id,
                         (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'TRANS') module_id,
                         (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT') permission_id,
                         (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management') app_id
                    FROM DUAL) B
              ON (    rp.app_id = b.app_id AND rp.role_id = b.role_id AND rp.permission_id = b.permission_id AND rp.module_id = b.module_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID)
             VALUES (
                       (SELECT role_id FROM role WHERE role_name = 'wmsadmin' AND company_id = -1),
                       (SELECT APP_ID FROM APP WHERE APP_NAME = 'Warehouse Management'),
                       (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'TRANS'),
                       (SELECT permission_id FROM permission WHERE permission_code = 'WMDOPCKRPT'),
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
					   );					   
             
COMMIT;

-- DBTicket DB-3773 REFL

MERGE INTO PERMISSION L
     USING (SELECT 'WMAMOBPKPIKCRT' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_NAME,
               IS_ACTIVE,
               COMPANY_ID,
               PERMISSION_CODE,
               CREATED_SOURCE_TYPE_ID,
               CREATED_SOURCE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE_TYPE_ID,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_DTTM,
               DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Administer Mobile Pack Pick Cart',
             1,
             -1,
             'WMAMOBPKPIKCRT',
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             'Gives access to perform pack pick cart on the mobile');

MERGE INTO PERMISSION L
     USING (SELECT 'WMAMOBMAKPICRT' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_NAME,
               IS_ACTIVE,
               COMPANY_ID,
               PERMISSION_CODE,
               CREATED_SOURCE_TYPE_ID,
               CREATED_SOURCE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE_TYPE_ID,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_DTTM,
               DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'Administer Mobile Make Pick Cart',
             1,
             -1,
             'WMAMOBMAKPICRT',
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             3,
             'Manhattan Associates',
             CURRENT_TIMESTAMP,
             'Gives access to perform make pick cart on the mobile');
INSERT INTO MODULE (MODULE_ID,
                    MODULE_NAME,
                    IS_ACTIVE,
                    MODULE_CODE,
                    CREATED_DTTM,
                    LAST_UPDATED_DTTM)
     VALUES ( (SELECT MAX (module_id) + 1 FROM MODULE) ,
             'WM Mobile',
             1,
             'WMMOBILE',
             CURRENT_TIMESTAMP,
             CURRENT_TIMESTAMP);


INSERT INTO APP_MODULE (app_id,
                        module_id,
                        is_active,
                        row_uid,
                        created_dttm,
                        last_updated_dttm)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE APP_NAME = 'Warehouse Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'WMMOBILE'),
             1,
             (SELECT MAX (ROW_UID) FROM APP_MODULE) + 4,
             CURRENT_TIMESTAMP,
             CURRENT_TIMESTAMP);		 

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'WMMOBILE'
                   AND PERMISSION.PERMISSION_CODE = 'WMAMOBPKPIKCRT') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               SEQ_APP_MOD_PERM.NEXTVAL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'WMMOBILE'
                   AND PERMISSION.PERMISSION_CODE = 'WMAMOBMAKPICRT') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               SEQ_APP_MOD_PERM.NEXTVAL);

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
        MERGE INTO role_app_mod_perm ramp
             USING (SELECT amp.app_id app_id,
                           amp.module_id module_id,
                           p.permission_id permission_id
                      FROM app_mod_perm amp, PERMISSION p
                     WHERE     app_id = (SELECT app_id
                                           FROM app
                                          WHERE app_name = 'Warehouse Management')
                           AND p.permission_id = amp.permission_id
                           AND module_id = (SELECT module_id
                                              FROM module
                                             WHERE module_code = 'WMMOBILE')
                           AND p.permission_id =
                                  (SELECT PERMISSION_ID
                                     FROM PERMISSION
                                    WHERE PERMISSION_CODE = 'WMAMOBMAKPICRT')) APP1
                ON (    ramp.app_id = app1.app_id
                    AND ramp.module_id = app1.module_id
                    AND ramp.permission_id = app1.permission_id)
        WHEN NOT MATCHED
        THEN
           INSERT     (ramp.role_id,
                       ramp.app_id,
                       ramp.module_id,
                       ramp.permission_id,
                       ramp.row_uid)
               VALUES ( (SELECT role_id
                           FROM role
                          WHERE role_name = 'WMROLE'AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       app1.app_id,
                       APP1.MODULE_ID,
                       app1.permission_id,
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
       END IF;
END;
/

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
        MERGE INTO role_app_mod_perm ramp
             USING (SELECT amp.app_id app_id,
                           amp.module_id module_id,
                           p.permission_id permission_id
                      FROM app_mod_perm amp, PERMISSION p
                     WHERE     app_id = (SELECT app_id
                                           FROM app
                                          WHERE app_name = 'Warehouse Management')
                           AND p.permission_id = amp.permission_id
                           AND module_id = (SELECT module_id
                                              FROM module
                                             WHERE module_code = 'WMMOBILE')
                           AND p.permission_id =
                                  (SELECT PERMISSION_ID
                                     FROM PERMISSION
                                    WHERE PERMISSION_CODE = 'WMAMOBPKPIKCRT')) APP1
                ON (    ramp.app_id = app1.app_id
                    AND ramp.module_id = app1.module_id
                    AND ramp.permission_id = app1.permission_id)
        WHEN NOT MATCHED
        THEN
           INSERT     (ramp.role_id,
                       ramp.app_id,
                       ramp.module_id,
                       ramp.permission_id,
                       ramp.row_uid)
               VALUES ( (SELECT role_id
                           FROM role
                          WHERE role_name = 'WMROLE'AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                       app1.app_id,
                       APP1.MODULE_ID,
                       app1.permission_id,
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
       END IF;
END;
/

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'WMMOBILE'
                   AND PERMISSION.PERMISSION_CODE = 'WMAMOBPKPIKCRT') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'WMMOBILE'
                   AND PERMISSION.PERMISSION_CODE = 'WMAMOBMAKPICRT') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM));

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAMOBPKPIKCRT'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMAMOBMAKPICRT'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));
COMMIT;			 

-- DBTicket DB-3872
INSERT INTO permission_tag (Tag_id,
                            description,
                            created_source_type_id,
                            created_source,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'Mobile Pack Pick Cart',
             1,
             'Seed',
             1,
             'Seed');

INSERT INTO permission_tag (Tag_id,
                            description,
                            created_source_type_id,
                            created_source,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'Mobile Make Pick Cart',
             1,
             'Seed',
             1,
             'Seed');

MERGE INTO PERMISSION P
     USING (SELECT 'Administer Mobile Make Pick Cart' PERMISSION_NAME, 'WMAMOBMAKPICRT' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,
             'Administer Mobile Make Pick Cart',
             1,
             -1,
             'WMAMOBMAKPICRT',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             'Gives access to perform make pick cart on the mobile');
                                             

MERGE INTO PERMISSION P
     USING (SELECT 'Administer Mobile Pack Pick Cart' PERMISSION_NAME, 'WMAMOBPKPIKCRT' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,
             'Administer Mobile Pack Pick Cart',
             1,
             -1,
             'WMAMOBPKPIKCRT',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             'Gives access to perform pack pick cart on the mobile');			 
			 
INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'WMAMOBMAKPICRT'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'Mobile Make Pick Cart'));

INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'WMAMOBPKPIKCRT'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'Mobile Pack Pick Cart'));
COMMIT;			   

-- -- DBTicket DB-6021
-- 
-- CALL sequpdt ('SYS_CODE', 'SYS_CODE_ID', 'SYS_CODE_ID_SEQ');
-- 
-- DECLARE
--    v_cnt   NUMBER;
-- BEGIN
--    SELECT COUNT (SYS_CODE_TYPE_ID)
--      INTO v_cnt
--      FROM SYS_CODE_TYPE
--     WHERE REC_TYPE = 'B' AND CODE_TYPE = '349';
-- 
--    IF v_cnt = 1
--    THEN
--       MERGE INTO SYS_CODE A
--            USING (SELECT 'B' REC_TYPE, '349' CODE_TYPE, 'CP' CODE_ID
--                     FROM DUAL) B
--               ON (    A.REC_TYPE = B.REC_TYPE
--                   AND A.CODE_TYPE = B.CODE_TYPE
--                   AND A.CODE_ID = B.CODE_ID)
--       WHEN NOT MATCHED
--       THEN
--          INSERT     (A.rec_type,
--                      A.code_type,
--                      A.code_id,
--                      A.code_desc,
--                      A.short_desc,
--                      A.misc_flags,
--                      A.create_date_time,
--                      A.mod_date_time,
--                      A.user_id,
--                      A.wm_version_id,
--                      A.sys_code_id,
--                      A.sys_code_type_id)
--              VALUES ('B',
--                      '349',
--                      'CP',
--                      'Customer pickup comment',
--                      'CUSTPICKUP',
--                      '0',
--                      SYSDATE,
--                      SYSDATE,
--                      'SEED',
--                      1,
--                      SYS_CODE_ID_SEQ.NEXTVAL,
--                      (SELECT SYS_CODE_TYPE_ID
--                         FROM SYS_CODE_TYPE
--                        WHERE REC_TYPE = 'B' AND CODE_TYPE = '349'));
--    END IF;
-- EXCEPTION
--    WHEN OTHERS
--    THEN
--       NULL;
-- END;
-- /
-- 
-- 
-- DECLARE
--    v_cnt   NUMBER;
-- BEGIN
--    SELECT COUNT (SYS_CODE_TYPE_ID)
--      INTO v_cnt
--      FROM SYS_CODE_TYPE
--     WHERE REC_TYPE = 'B' AND CODE_TYPE = '349';
-- 
--    IF v_cnt = 1
--    THEN
--       MERGE INTO SYS_CODE A
--            USING (SELECT 'B' REC_TYPE, '349' CODE_TYPE, 'SR' CODE_ID
--                     FROM DUAL) B
--               ON (    A.REC_TYPE = B.REC_TYPE
--                   AND A.CODE_TYPE = B.CODE_TYPE
--                   AND A.CODE_ID = B.CODE_ID)
--       WHEN NOT MATCHED
--       THEN
--          INSERT     (A.rec_type,
--                      A.code_type,
--                      A.code_id,
--                      A.code_desc,
--                      A.short_desc,
--                      A.misc_flags,
--                      A.create_date_time,
--                      A.mod_date_time,
--                      A.user_id,
--                      A.wm_version_id,
--                      A.sys_code_id,
--                      A.sys_code_type_id)
--              VALUES ('B',
--                      '349',
--                      'SR',
--                      'Short reason comment',
--                      'SHR',
--                      '0',
--                      SYSDATE,
--                      SYSDATE,
--                      'SEED',
--                      1,
--                      SYS_CODE_ID_SEQ.NEXTVAL,
--                      (SELECT SYS_CODE_TYPE_ID
--                         FROM SYS_CODE_TYPE
--                        WHERE REC_TYPE = 'B' AND CODE_TYPE = '349'));
--    END IF;
-- EXCEPTION
--    WHEN OTHERS
--    THEN
--       NULL;
-- END;
-- /
-- 
-- COMMIT;

-- DBTicket DB-6448

MERGE INTO SYS_CODE A
     USING (SELECT 'S' REC_TYPE, '577' CODE_TYPE, '500 017' CODE_ID FROM DUAL)
           B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (A.rec_type,
               A.code_type,
               A.code_id,
               A.code_desc,
               A.short_desc,
               A.misc_flags,
               A.create_date_time,
               A.mod_date_time,
               A.user_id,
               A.wm_version_id,
               A.sys_code_id,
               A.sys_code_type_id)
       VALUES ('S',
               '577',
               '500 017',
               'Order instruction acknowledgement',
               'Order inst',
               '500',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               1,
               SYS_CODE_ID_SEQ.NEXTVAL,
               (SELECT SYS_CODE_TYPE_ID
                  FROM SYS_CODE_TYPE
                 WHERE REC_TYPE = 'S' AND CODE_TYPE = '577'));

MERGE INTO LABEL L
     USING (SELECT 'SysCode' BUNDLE_NAME,
                   'Order instruction acknowledgement' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Order instruction acknowledgement',
               'Order instruction acknowledgement',
               'SysCode');

MERGE INTO SYS_CODE A
     USING (SELECT 'S' REC_TYPE, '577' CODE_TYPE, '500 018' CODE_ID FROM DUAL)
           B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (A.rec_type,
               A.code_type,
               A.code_id,
               A.code_desc,
               A.short_desc,
               A.misc_flags,
               A.create_date_time,
               A.mod_date_time,
               A.user_id,
               A.wm_version_id,
               A.sys_code_id,
               A.sys_code_type_id)
       VALUES ('S',
               '577',
               '500 018',
               'Item instruction acknowledgement',
               'Item inst',
               '500',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               1,
               SYS_CODE_ID_SEQ.NEXTVAL,
               (SELECT SYS_CODE_TYPE_ID
                  FROM SYS_CODE_TYPE
                 WHERE REC_TYPE = 'S' AND CODE_TYPE = '577'));

MERGE INTO LABEL L
     USING (SELECT 'SysCode' BUNDLE_NAME,
                   'Item instruction acknowledgement' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Item instruction acknowledgement',
               'Item instruction acknowledgement',
               'SysCode');
               
COMMIT;

-- DBTicket DB-6521

MERGE INTO SYS_CODE A
     USING (SELECT 'S' REC_TYPE, '009' CODE_TYPE, '102' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (A.rec_type,
               A.code_type,
               A.code_id,
               A.code_desc,
               A.short_desc,
               A.misc_flags,
               A.create_date_time,
               A.mod_date_time,
               A.user_id,
               A.wm_version_id,
               A.sys_code_id,
               A.sys_code_type_id)
       VALUES ('S',
               '009',
               '102',
               'Instruction Type',
               'Inst Type',
               'YNNN',
               SYSDATE,
               SYSDATE,
               'SEED',
               1,
               SYS_CODE_ID_SEQ.NEXTVAL,
               (SELECT SYS_CODE_TYPE_ID
                  FROM SYS_CODE_TYPE
                 WHERE REC_TYPE = 'S' AND CODE_TYPE = '009'));

MERGE INTO LABEL L
     USING (SELECT 'SysCode' BUNDLE_NAME, 'Instruction Type' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Instruction Type',
               'Instruction Type',
               'SysCode');
               
COMMIT;

-- DBTicket DB-6711
CALL SEQUPDT('SYS_CODE', 'SYS_CODE_ID', 'SYS_CODE_ID_SEQ');
MERGE INTO SYS_CODE A
     USING (SELECT 'S' REC_TYPE, '009' CODE_TYPE, '103' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (A.rec_type,
               A.code_type,
               A.code_id,
               A.code_desc,
               A.short_desc,
               A.misc_flags,
               A.create_date_time,
               A.mod_date_time,
               A.user_id,
               A.wm_version_id,
               A.sys_code_id,
               A.sys_code_type_id)
       VALUES ('S',
               '009',
               '103',
               'Special Instruction',
               'Spl Inst',
               'YNNN',
               SYSDATE,
               SYSDATE,
               'SEED',
               1,
               SYS_CODE_ID_SEQ.NEXTVAL,
               (SELECT SYS_CODE_TYPE_ID
                  FROM SYS_CODE_TYPE
                 WHERE REC_TYPE = 'S' AND CODE_TYPE = '009'));

MERGE INTO LABEL L
     USING (SELECT 'SysCode' BUNDLE_NAME, 'Special Instruction' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Special Instruction',
               'Special Instruction',
               'SysCode');
               
COMMIT;

-- DBTicket DB-9008 REFL
DECLARE
   v_cnt   NUMBER;
BEGIN
   SELECT COUNT (1)
     INTO v_cnt
     FROM permission
    WHERE permission_code = 'ALPNASN';

   IF v_cnt = 1
   THEN
      DELETE FROM role_app_mod_perm
            WHERE permission_id IN (SELECT permission_id
                                      FROM permission
                                     WHERE permission_code = 'ALPNASN');
   END IF;
END;
/

COMMIT;
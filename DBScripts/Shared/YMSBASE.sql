------ DBTicket YM-2260
----MERGE INTO XSCREEN L
----     USING (SELECT 'Check-In' NAME, '360004' XSCREEN_ID FROM DUAL) B
----        ON (L.NAME = B.NAME AND L.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XSCREEN_ID,
----               NAME,
----               DESCRIPTION,
----               SCREEN_TYPE,
----               SCREEN_VERSION,
----               SCREEN_MODE,
----               IS_RESIZABLE,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               CREATED_DTTM,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               LAST_UPDATED_DTTM,
----               DEFAULT_X,
----               DEFAULT_Y,
----               DEFAULT_WIDTH,
----               DEFAULT_HEIGHT)
----       VALUES (360004,
----               'Check-In',
----               ' Check-In',
----               '1',
----               '2',
----               '0',
----               '1',
----               NULL,
----               '1',
----               CURRENT_TIMESTAMP,
----               NULL,
----               '1',
----               CURRENT_TIMESTAMP,
----               125,
----               125,
----               600,
----               400);
----
----commit;

-- DBTicket YM-2262

update lrf_report_def_layout set hibernate_field_name = 'TRAILER' where field_name = 'TRAILER' and report_def_id = (select report_def_id from lrf_report_def where report_name like 'CheckInReport' and type like 'Jasper' and category = 'WM');
update lrf_report_def_layout set hibernate_field_name = 'COMPANY_ID' where field_name = 'COMPANY_ID' and report_def_id = (select report_def_id from lrf_report_def where report_name like 'CheckInReport' and type like 'Jasper' and category = 'WM');
commit;

-- DBTicket YM-1924

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'APPT'
                   AND PERMISSION.PERMISSION_CODE = 'LMVC') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.nextval));

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'APPT'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'LMVC'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/

commit;

-- DBTicket YM-3680

alter table YARD_ZONE_SLOT add LOCN_ID VARCHAR2(10);

MERGE INTO APP_MODULE AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE
              FROM APP, MODULE
             WHERE APP.APP_NAME = 'Yard Management'
                   AND MODULE.MODULE_CODE = 'YARD') APP1
        ON (AMP.APP_ID = APP1.APP_ID AND AMP.MODULE_ID = APP1.MODULE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               IS_ACTIVE,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Yard Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               1,
               (SELECT MAX (ROW_UID) + 4 FROM APP_MODULE));
               
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');               

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Yard Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'ASYRD'),
             (SEQ_APP_MOD_PERM.nextval));

INSERT INTO RELTP_APP_MOD_PERM
            (RELATIONSHIP_TYPE_ID, APP_ID,
             MODULE_ID,
             PERMISSION_ID,
             ROW_UID, IS_PERM_IMPLIED
            )
     VALUES (4,
			 (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Yard Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'ASYRD'),
             (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
			 0);   

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Yard Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'VSYRD'),
             (SEQ_APP_MOD_PERM.nextval));
			   
INSERT INTO RELTP_APP_MOD_PERM
            (RELATIONSHIP_TYPE_ID, APP_ID,
             MODULE_ID,
             PERMISSION_ID,
             ROW_UID, IS_PERM_IMPLIED
            )
     VALUES (4,
			 (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Yard Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'YARD'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'VSYRD'),
             (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
			 0);  
commit;

-- DBTicket YM-3712
BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE FACILITY_YARD ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE FACILITY_YARD_ZONE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE YARD_TP_COMPANY_EQUIPMENT ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/

BEGIN
   EXECUTE IMMEDIATE
      'ALTER TABLE YARD_ZONE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)';
EXCEPTION
WHEN OTHERS THEN
    NULL;
END;
/


CREATE OR REPLACE TRIGGER FACILIT_YRD_LST_UPD_DTTM_TRG
   BEFORE UPDATE
   ON FACILITY_YARD
   FOR EACH ROW
BEGIN
   :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER FACILIT_YRD_ZONE_LST_UPD_D_TRG
   BEFORE UPDATE
   ON FACILITY_YARD_ZONE
   FOR EACH ROW
BEGIN
   :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER YRD_LST_UPD_DTTM_TRG
   BEFORE UPDATE
   ON YARD
   FOR EACH ROW
BEGIN
   :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER YRD_TP_CO_EQUPMNT_LT_UP_D_TRG
   BEFORE UPDATE
   ON YARD_TP_COMPANY_EQUIPMENT
   FOR EACH ROW
BEGIN
   :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

CREATE OR REPLACE TRIGGER YARD_ZONE_LST_UPD_DTTM_TRG
   BEFORE UPDATE
   ON YARD_ZONE
   FOR EACH ROW
BEGIN
   :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
END;
/

-- CREATE OR REPLACE TRIGGER YARD_ZONE_SLOT_LST_UPD_D_TRG
--    BEFORE UPDATE
--    ON YARD_ZONE_SLOT
--    FOR EACH ROW
-- BEGIN
--    :new.LAST_UPDATED_DTTM := SYSTIMESTAMP;
-- END;
-- /

-- DBTicket YM-3731
CALL SEQUPDT ('LRF_REPORT_DEF', 'REPORT_DEF_ID', 'SEQ_LRF_REPORT_DEF_ID');
CALL SEQUPDT ('LRF_REPORT_DEF_LAYOUT', 'REPORT_DEF_LAYOUT_ID', 'LRF_REPORT_DEF_LAYOUT_ID_SEQ');

merge into lrf_report_def L1
using (select 'CheckInDriverReport' REPORT_NAME, 'CheckInDriverReport' DESCRIPTION, 'Facility' SUBCATEGORY from dual) B
on(L1.REPORT_NAME=B.REPORT_NAME and L1.DESCRIPTION=B.DESCRIPTION and L1.SUBCATEGORY=B.SUBCATEGORY)
when not matched
then 
insert(description, TYPE,viewPerm, editperm, CATEGORY, subcategory, report_def_id, report_name, report_file_name, priority, reset_count, reset_interval, retry_count, retry_interval)
values('CheckInDriverReport', 'Jasper', 'WMAGCICO', 'WMAGCICO', 'WM', 'Facility', seq_lrf_report_def_id.nextval, 'CheckInDriverReport', 'CheckInDriverReport', 10, 0,0,0,0);

MERGE INTO lrf_report_def_layout L1
     USING (SELECT L2.REPORT_DEF_ID,
                   L2.category,
                   L2.subcategory,
                   L2.report_name
              FROM lrf_report_def L2
             WHERE category = 'WM'
                   AND subcategory = 'Facility'
                   AND report_name = 'CheckInDriverReport') L3
        ON (L1.REPORT_DEF_ID = L3.REPORT_DEF_ID
            AND L1.FIELD_NAME = 'CARRIER_DRIVER_ID'
            AND L1.FIELD_LABEL = 'CARRIER_DRIVER_ID')
WHEN NOT MATCHED
THEN
	INSERT(report_def_layout_id, field_position, is_required, field_name, field_label, field_operators, field_type, operator_type, is_localizable, send_parameter, hibernate_field_name, report_def_id)
	VALUES(lrf_report_def_layout_id_seq.nextval, 0, 1, 'CARRIER_DRIVER_ID', 'CARRIER_DRIVER_ID', '=', 'STRING', 'REPORT_TEXT_ITEM', 0, 0, 'CARRIER_DRIVER_ID', (select report_def_id from lrf_report_def where report_name='CheckInDriverReport'));

MERGE INTO lrf_report_def_layout L1
     USING (SELECT L2.REPORT_DEF_ID,
                   L2.category,
                   L2.subcategory,
                   L2.report_name
              FROM lrf_report_def L2
             WHERE category = 'WM'
                   AND subcategory = 'Facility'
                   AND report_name = 'CheckInDriverReport') L3
        ON (L1.REPORT_DEF_ID = L3.REPORT_DEF_ID
            AND L1.FIELD_NAME = 'COMPANY_ID'
            AND L1.FIELD_LABEL = 'COMPANY_ID')
WHEN NOT MATCHED
THEN
	INSERT(report_def_layout_id, field_position, is_required, field_name, field_label, field_operators, field_type, operator_type, is_localizable, send_parameter, hibernate_field_name, report_def_id)
	VALUES(lrf_report_def_layout_id_seq.nextval, 0, 1, 'COMPANY_ID', 'COMPANY_ID', '=', 'STRING', 'REPORT_TEXT_ITEM', 0, 0, 'COMPANY_ID', (select report_def_id from lrf_report_def where report_name='CheckInDriverReport'));

commit;

-- DBTicket YM-3720
INSERT INTO LABOR_ACTIVITY (LABOR_ACTIVITY_ID,
                            NAME,
                            DESCRIPTION,
                            AIL_ACT,
                            ACT_TYPE,
                            CREATED_SOURCE_TYPE,
                            CREATED_SOURCE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_DTTM,
                            HIBERNATE_VERSION,
                            PROMPT_LOCN,
                            DISPL_EPP,
                            INCLD_TRVL,
                            CRIT_RULE_TYPE,
                            PERMISSION_ID,
                            COMPANY_ID)
     VALUES (LABOR_ACTIVITY_ID_SEQ.NEXTVAL,
             'YMMOVE',
             'Move Task',
             0,
             '10',
             1,
             NULL,
             CURRENT_TIMESTAMP,
             1,
             NULL,
             CURRENT_TIMESTAMP,
             0,
             0,
             0,
             1,
             NULL,
             NULL,
             1);

INSERT INTO LABOR_ACTIVITY (LABOR_ACTIVITY_ID,
                            NAME,
                            DESCRIPTION,
                            AIL_ACT,
                            ACT_TYPE,
                            CREATED_SOURCE_TYPE,
                            CREATED_SOURCE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_DTTM,
                            HIBERNATE_VERSION,
                            PROMPT_LOCN,
                            DISPL_EPP,
                            INCLD_TRVL,
                            CRIT_RULE_TYPE,
                            PERMISSION_ID,
                            COMPANY_ID)
     VALUES (LABOR_ACTIVITY_ID_SEQ.NEXTVAL,
             'YMAUDIT',
             'Audit Task',
             0,
             '10',
             1,
             NULL,
             CURRENT_TIMESTAMP,
             1,
             NULL,
             CURRENT_TIMESTAMP,
             0,
             0,
             0,
             1,
             NULL,
             NULL,
             1);

INSERT INTO LABOR_ACTIVITY (LABOR_ACTIVITY_ID,
                            NAME,
                            DESCRIPTION,
                            AIL_ACT,
                            ACT_TYPE,
                            CREATED_SOURCE_TYPE,
                            CREATED_SOURCE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_DTTM,
                            HIBERNATE_VERSION,
                            PROMPT_LOCN,
                            DISPL_EPP,
                            INCLD_TRVL,
                            CRIT_RULE_TYPE,
                            PERMISSION_ID,
                            COMPANY_ID)
     VALUES (LABOR_ACTIVITY_ID_SEQ.NEXTVAL,
             'YMCHECKIN',
             'Yard Check In',
             0,
             '10',
             1,
             NULL,
             CURRENT_TIMESTAMP,
             1,
             NULL,
             CURRENT_TIMESTAMP,
             0,
             0,
             0,
             1,
             NULL,
             NULL,
             1);

INSERT INTO LABOR_ACTIVITY (LABOR_ACTIVITY_ID,
                            NAME,
                            DESCRIPTION,
                            AIL_ACT,
                            ACT_TYPE,
                            CREATED_SOURCE_TYPE,
                            CREATED_SOURCE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_DTTM,
                            HIBERNATE_VERSION,
                            PROMPT_LOCN,
                            DISPL_EPP,
                            INCLD_TRVL,
                            CRIT_RULE_TYPE,
                            PERMISSION_ID,
                            COMPANY_ID)
     VALUES (LABOR_ACTIVITY_ID_SEQ.NEXTVAL,
             'YMCHECKOUT',
             'Yard Check out',
             0,
             '10',
             1,
             NULL,
             CURRENT_TIMESTAMP,
             1,
             NULL,
             CURRENT_TIMESTAMP,
             0,
             0,
             0,
             1,
             NULL,
             NULL,
             1);
			 
commit;

-- DBTicket YM-4248
comment on table LOCATION_PROXIMITY is 'Table stores proximity between yard zones and docks.';
comment on table FACILITY_YARD is 'Describes facility yard relationship.';
comment on table FACILITY_YARD_ZONE is 'Describes facility yard zone relationship.';
comment on column LOCATION_PROXIMITY.ORIGINAL_LOCATION_ID is 'Location Id of from Yard Zones/Docks.';
comment on column LOCATION_PROXIMITY.PROXIMITY_LOCATION_ID is 'Location Id of To Yard Zones/Docks.';
comment on column YARD_ZONE_SLOT.LOCN_ID is 'Location Id of related record in Location Header table.';

-- DBTicket YM-4362
MERGE INTO lrf_report_def_layout L1
     USING (SELECT L2.REPORT_DEF_ID,
                   L2.category,
                   L2.subcategory,
                   L2.report_name
              FROM lrf_report_def L2
             WHERE report_name = 'CheckInDriverReport') L3
        ON (    L1.REPORT_DEF_ID = L3.REPORT_DEF_ID
            AND L1.FIELD_NAME = 'FIRST_NAME'
            AND L1.FIELD_LABEL = 'FIRST_NAME')
WHEN NOT MATCHED
THEN
   INSERT     (report_def_layout_id,
               field_position,
               is_required,
               field_name,
               field_label,
               field_operators,
               field_type,
               operator_type,
               is_localizable,
               send_parameter,
               hibernate_field_name,
               report_def_id)
       VALUES (LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL,
               0,
               1,
               'FIRST_NAME',
               'FIRST_NAME',
               '=',
               'STRING',
               'REPORT_TEXT_ITEM',
               0,
               0,
               'FIRST_NAME',
               (SELECT report_def_id
                  FROM lrf_report_def
                 WHERE report_name = 'CheckInDriverReport'));

MERGE INTO lrf_report_def_layout L1
     USING (SELECT L2.REPORT_DEF_ID,
                   L2.category,
                   L2.subcategory,
                   L2.report_name
              FROM lrf_report_def L2
             WHERE report_name = 'CheckInDriverReport') L3
        ON (    L1.REPORT_DEF_ID = L3.REPORT_DEF_ID
            AND L1.FIELD_NAME = 'SURNAME'
            AND L1.FIELD_LABEL = 'SURNAME')
WHEN NOT MATCHED
THEN
   INSERT     (report_def_layout_id,
               field_position,
               is_required,
               field_name,
               field_label,
               field_operators,
               field_type,
               operator_type,
               is_localizable,
               send_parameter,
               hibernate_field_name,
               report_def_id)
       VALUES (LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL,
               0,
               1,
               'SURNAME',
               'SURNAME',
               '=',
               'STRING',
               'REPORT_TEXT_ITEM',
               0,
               0,
               'SURNAME',
               (SELECT report_def_id
                  FROM lrf_report_def
                 WHERE report_name = 'CheckInDriverReport'));

COMMIT;

-- DBTicket YM-4507
ALTER TABLE YARD_ZONE_SLOT
RENAME COLUMN CURRENT_CAPACITY TO USED_CAPACITY;
-- DBTicket YM-4538

UPDATE YARD_ZONE_SLOT
   SET MAX_CAPACITY = 1
 WHERE MAX_CAPACITY = 0;

COMMIT;

-- DBTicket YM-4643 REFL
INSERT INTO permission (permission_id,
                        permission_name,
                        is_active,
                        company_id,
                        permission_code)
     VALUES (seq_permission_id.NEXTVAL,
             'Carrier Trailer Pool',
             1,
             -1,
             'CARTRLPOOL');  
             
COMMIT;

-- DBTicket YM-4714 REFL
MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT 8 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'CARTRLPOOL') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (CTP.COMPANY_TYPE_ID, CTP.PERMISSION_ID, CTP.ROW_UID)
       VALUES (
                 8,
                 CTP1.PERMISSION_ID,
                 (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
                 
COMMIT;                 

-- DBTicket YM-4727

ALTER TABLE yard MODIFY STATE_PROV VARCHAR2(3);

-- DBTicket YM-5141
CALL SEQUPDT ('COMPANY_APP_MODULE', 'ROW_UID', 'SEQ_COAPPMOD_ROWUID');
merge into app_module amp
     using (select app.app_id,
                   app.app_name,
                   module.module_id,
                   module.module_code
              from app, module
             where app.app_short_name = 'LG'
                   and module.module_code = 'YARD') app1
        on (amp.app_id = app1.app_id and amp.module_id = app1.module_id)
when not matched
then
   insert     (app_id,
               module_id,
               is_active,
               row_uid)
       values ( (select app_id
                   from app
                  where app_short_name = 'LG'),
               (select module_id
                  from module
                 where module_code = 'YARD'),
               1,
               (select max (row_uid) + 4 from app_module));

insert into company_app_module (company_id,
                                app_id,
                                module_id,
                                row_uid,
                                created_dttm,
                                last_updated_dttm)
   select company_id,
          (select app_id
             from app
            where app_short_name = 'LG'),
          (select module_id
             from module
            where module_code = 'YARD'),
          seq_coappmod_rowuid.nextval,
          sysdate,
          null
     from (select distinct company_id
             from company_app_module cam
            where app_id = (select app_id
                              from app
                             where app_short_name = 'LG')
                  and not exists
                             (select 1
                                from company_app_module cam2
                               where cam.company_id = cam2.company_id
                                     and app_id =
                                            (select app_id
                                               from app
                                              where app_short_name = 'LG')
                                     and module_id =
                                            (select module_id
                                               from module
                                              where module_code = 'YARD')));

COMMIT;

-- DBTicket YM-5347
insert into ilm_dynamic_group (ilm_dynamic_group,
                               description,
                               is_for_feasibility,
                               is_for_priority,
                               is_for_total_capacity,
                               capacity_measure_desc,
                               is_for_ntd,
                               is_for_load_unload_rate,
                               created_dttm,
                               last_updated_dttm)
     values (70,
             'SUPPLIER_CATEGORY',
             1,
             1,
             0,
             null,
             1,
             0,
             sysdate,
             sysdate);

commit;

-- DBTicket YM-5372
alter table LOCATION_PROXIMITY add(DOCK_ID VARCHAR2(8), YARD_ZONE_ID number(15));

-- alter table LOCATION_PROXIMITY add constraint LOCN_PROX_DOCK_ID_FK foreign key(DOCK_ID) references DOCK(DOCK_ID);
-- CREATE INDEX LOCN_PROX_DOCK_ID_FK_IDX ON LOCATION_PROXIMITY(DOCK_ID) TABLESPACE YMS_BST_IDX_TBS;

-- alter table LOCATION_PROXIMITY add constraint LOCN_PROX_YD_ZN_ID_FK foreign key(YARD_ZONE_ID) references YARD_ZONE(YARD_ZONE_ID);
-- CREATE INDEX LOCN_PROX_YD_ZN_ID_FK_IDX ON LOCATION_PROXIMITY(YARD_ZONE_ID) TABLESPACE YMS_BST_IDX_TBS;

comment on column LOCATION_PROXIMITY.DOCK_ID is 'Represents dock id Foreign key to DOCK.DOCK_ID';
comment on column LOCATION_PROXIMITY.YARD_ZONE_ID is 'Represents yard zone Foreign key to YARD_ZONE.YARD_ZONE_ID';

-- DBTicket DB-88
INSERT INTO OBJECT_TYPE (OBJECT_TYPE,
                         DESCRIPTION,
                         LOCK_TABLE_NAME,
                         LOCK_SEQ_NAME,
                         CREATED_DTTM,
                         LAST_UPDATED_DTTM)
     VALUES ('VSIT',
             'Visit Type',
             NULL,
             NULL,
             CURRENT_TIMESTAMP,
             NULL);
             
COMMIT;             

-- DBTicket DB-91
CREATE SEQUENCE SEQ_LOCATION_PROXIMITY_ID START WITH 1 INCREMENT BY 1;

ALTER TABLE LOCATION_PROXIMITY DROP PRIMARY KEY DROP INDEX;
ALTER TABLE LOCATION_PROXIMITY ADD(LOCATION_PROXIMITY_ID NUMBER(9));
--ALTER TABLE LOCATION_PROXIMITY ADD CONSTRAINT PK_LOCATION_PROXIMITY_ID PRIMARY KEY(LOCATION_PROXIMITY_ID);
COMMENT ON COLUMN LOCATION_PROXIMITY.LOCATION_PROXIMITY_ID IS 'It is the system generated sequence number which represents the visit data of the trailer.';

-- DBTicket DB-105
BEGIN
   FOR cur
      IN (SELECT SEQ_LOCATION_PROXIMITY_ID.NEXTVAL seq,
                 l.LOCATION_ID,
                 d.DOCK_ID
            FROM    location l
                 JOIN
                    dock d
                 ON     d.FACILITY_ID = l.LOCATION_OBJID_PK1
                    AND d.DOCK_ID = l.LOCATION_OBJID_PK2
                    AND l.ILM_OBJECT_TYPE = 4)
   LOOP
      UPDATE LOCATION_PROXIMITY LP
         SET LP.DOCK_ID = cur.DOCK_ID, lp.LOCATION_PROXIMITY_ID = cur.seq
       WHERE cur.LOCATION_ID = LP.ORIGINAL_LOCATION_ID;
   END LOOP;
END;
/

BEGIN
   FOR cur
      IN (SELECT SEQ_LOCATION_PROXIMITY_ID.NEXTVAL seq,
                 l.LOCATION_ID,
                 yz.YARD_ZONE_ID
            FROM    location l
                 JOIN
                    YARD_ZONE yz
                 ON     yz.YARD_ID = l.LOCATION_OBJID_PK1
                    AND yz.YARD_ZONE_ID = l.LOCATION_OBJID_PK2
                    AND l.ILM_OBJECT_TYPE = 28)
   LOOP
      UPDATE LOCATION_PROXIMITY LP
         SET LP.YARD_ZONE_ID = cur.YARD_ZONE_ID,
             lp.LOCATION_PROXIMITY_ID = cur.seq
       WHERE cur.LOCATION_ID = LP.ORIGINAL_LOCATION_ID;
   END LOOP;
END;
/

BEGIN
   FOR cur
      IN (SELECT SEQ_LOCATION_PROXIMITY_ID.NEXTVAL seq,
                 l.LOCATION_ID,
                 d.DOCK_ID
            FROM    location l
                 JOIN
                    dock d
                 ON     d.FACILITY_ID = l.LOCATION_OBJID_PK1
                    AND d.DOCK_ID = l.LOCATION_OBJID_PK2
                    AND l.ILM_OBJECT_TYPE = 4)
   LOOP
      UPDATE LOCATION_PROXIMITY LP
         SET LP.DOCK_ID = cur.DOCK_ID, lp.LOCATION_PROXIMITY_ID = cur.seq
       WHERE cur.LOCATION_ID = LP.PROXIMITY_LOCATION_ID;
   END LOOP;
END;
/

BEGIN
   FOR cur
      IN (SELECT SEQ_LOCATION_PROXIMITY_ID.NEXTVAL seq,
                 l.LOCATION_ID,
                 yz.YARD_ZONE_ID
            FROM    location l
                 JOIN
                    YARD_ZONE yz
                 ON     yz.YARD_ID = l.LOCATION_OBJID_PK1
                    AND yz.YARD_ZONE_ID = l.LOCATION_OBJID_PK2
                    AND l.ILM_OBJECT_TYPE = 28)
   LOOP
      UPDATE LOCATION_PROXIMITY LP
         SET LP.YARD_ZONE_ID = cur.YARD_ZONE_ID,
             lp.LOCATION_PROXIMITY_ID = cur.seq
       WHERE cur.LOCATION_ID = LP.PROXIMITY_LOCATION_ID;
   END LOOP;
END;
/

DECLARE
   v_cons_count   NUMBER (1);
BEGIN
   SELECT COUNT (*)
     INTO v_cons_count
     FROM user_constraints
    WHERE CONSTRAINT_NAME = 'PK_LOCATION_PROXIMITY_ID';

   IF v_cons_count = 0
   THEN
      EXECUTE IMMEDIATE
         'ALTER TABLE LOCATION_PROXIMITY ADD CONSTRAINT PK_LOCATION_PROXIMITY_ID PRIMARY KEY(LOCATION_PROXIMITY_ID)';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/

-- DBTicket DB-232
UPDATE yard_zone_slot
   SET yard_zone_slot_status = '204'
 WHERE yard_zone_slot_status = '304';

UPDATE yard_zone_slot
   SET yard_zone_slot_status = '208'
 WHERE yard_zone_slot_status = '308';

UPDATE yard_zone_slot
   SET yard_zone_slot_status = '212'
 WHERE yard_zone_slot_status = '312';

UPDATE yard_zone_slot
   SET yard_zone_slot_status = '220'
 WHERE yard_zone_slot_status = '320';
 
COMMIT;

-- DBTicket DB-721
insert into ilm_dynamic_group (ilm_dynamic_group,
                               description,
                               is_for_feasibility,
                               is_for_priority,
                               is_for_total_capacity,
                               capacity_measure_desc,
                               is_for_ntd,
                               is_for_load_unload_rate,
                               created_dttm,
                               last_updated_dttm)
     values (74,
             'APPOINTMENT SCHEDULED TIME',
             1,
             1,
             0,
             null,
             1,
             0,
             sysdate,
             sysdate);

commit;

-- DBTicket DB-921
MERGE INTO ILM_DYNAMIC_GROUP L
     USING (SELECT '84' ILM_DYNAMIC_GROUP FROM DUAL) B
        ON (L.ILM_DYNAMIC_GROUP = B.ILM_DYNAMIC_GROUP)
WHEN NOT MATCHED
THEN
   INSERT     (ILM_DYNAMIC_GROUP,
               DESCRIPTION,
               IS_FOR_FEASIBILITY,
               IS_FOR_PRIORITY,
               IS_FOR_TOTAL_CAPACITY,
               IS_FOR_NTD,
               IS_FOR_LOAD_UNLOAD_RATE,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (84,
               'YARD_ZONE',
               1,
               0,
               0,
               0,
               0,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

COMMIT;

-- DBTicket DB-941
CALL QUIET_DROP('TRIGGER','YARD_ZNSL_LST_UPDT_DTTM_TRG');

-- DBTicket DB-1108
MERGE INTO ILM_DYNAMIC_GROUP L
     USING (SELECT '88' ILM_DYNAMIC_GROUP FROM DUAL) B
        ON (L.ILM_DYNAMIC_GROUP = B.ILM_DYNAMIC_GROUP)
WHEN NOT MATCHED
THEN
   INSERT     (Ilm_Dynamic_Group,
               Description,
               Is_For_Feasibility,
               Is_For_Priority,
               Is_For_Total_Capacity,
               Capacity_Measure_Desc,
               Is_For_Ntd,
               Is_For_Load_Unload_Rate,
               Created_Dttm,
               Last_Updated_Dttm)
       VALUES (88,
               'PRE_PLANNED_DOCK_DOOR',
               0,
               0,
               0,
               NULL,
               1,
               0,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);
               
COMMIT;               

-- DBTicket DB-1524
merge into ILM_DYNAMIC_GROUP L
USING (SELECT '92' ILM_DYNAMIC_GROUP FROM DUAL) B
ON (L.ILM_DYNAMIC_GROUP = B.ILM_DYNAMIC_GROUP)
WHEN NOT MATCHED
THEN
INSERT (Ilm_Dynamic_Group,
Description,
Is_For_Feasibility,
Is_For_Priority,
Is_For_Total_Capacity,
Capacity_Measure_Desc,
Is_For_Ntd,
Is_For_Load_Unload_Rate,
Created_Dttm,
LAST_UPDATED_DTTM)
values (92,
'VISIT TYPE',
0,
1,
0,
NULL,
1,
0,
current_timestamp,
current_timestamp) ;

-- DBTicket DB-1537
update YARD set IS_TASKING_ALLOWED  = 1
where IS_TASKING_ALLOWED is null;

Alter table YARD
modify (IS_TASKING_ALLOWED number(1) not null);

update YARD_ZONE_SLOT set USED_CAPACITY = 0
where USED_CAPACITY is null;

update YARD_ZONE_SLOT set MAX_CAPACITY  = 1
where MAX_CAPACITY is null;

Alter table YARD_ZONE_SLOT
modify (USED_CAPACITY number(8) not null);

Alter table YARD_ZONE_SLOT
modify (MAX_CAPACITY number(8) not null);

-- DBTicket DB-1803 REFL
INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (
               SEQ_PERMISSION_ID.NEXTVAL,
               'Administer YMS Task Group Eligibility',
               1,
               -1,
               'YMATGELG',
               3,
               'Manhattan Associates',
               SYSDATE,
               3,
               'Manhattan Associates',
               SYSDATE,
               'Allow users to execute edit, add, delete and copy operations on YMS task group eligibility UI');

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'View YMS Task Group Eligibility',
             1,
             -1,
             'YMVTGELG',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             'Allow users to View data in YMS Task group eligibility UI');

----INSERT INTO app_mod_perm (APP_ID,
----                          MODULE_ID,
----                          PERMISSION_ID,
----                          ROW_UID,
----                          CREATED_DTTM,
----                          LAST_UPDATED_DTTM)
----     VALUES ((SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMATGELG'),
----             (SEQ_APP_MOD_PERM.nextval),
----             SYSDATE,
----             NULL);
----
----INSERT INTO APP_MOD_PERM (APP_ID,
----                          MODULE_ID,
----                          PERMISSION_ID,
----                          ROW_UID,
----                          CREATED_DTTM,
----                          LAST_UPDATED_DTTM)
----     VALUES ((SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT PERMISSION_ID
----                FROM PERMISSION
----               WHERE PERMISSION_CODE = 'YMVTGELG'),
----             (SEQ_APP_MOD_PERM.nextval),
----             SYSDATE,
----             NULL);
----
----INSERT INTO role_app_mod_perm (ROLE_ID,
----                               APP_ID,
----                               MODULE_ID,
----                               PERMISSION_ID,
----                               ROW_UID,
----                               CREATED_DTTM,
----                               LAST_UPDATED_DTTM)
----     VALUES ( (SELECT role_id
----                 FROM role
----                WHERE role_name = 'YMMGRROLE'),
----             (SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMATGELG'),
----             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
----             SYSDATE,
----             NULL);
----
----INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
----                               APP_ID,
----                               MODULE_ID,
----                               PERMISSION_ID,
----                               ROW_UID,
----                               CREATED_DTTM,
----                               LAST_UPDATED_DTTM)
----     VALUES ( (SELECT ROLE_ID
----                 FROM role
----                WHERE ROLE_NAME = 'YMMGRROLE'),
----             (SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT PERMISSION_ID
----                FROM PERMISSION
----               WHERE PERMISSION_CODE = 'YMVTGELG'),
----             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
----             SYSDATE,
----             NULL);


INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (
               SEQ_PERMISSION_ID.NEXTVAL,
               'Administer YMS Task Path Definition',
               1,
               -1,
               'YMATSPHDEF',
               3,
               'Manhattan Associates',
               SYSDATE,
               3,
               'Manhattan Associates',
               SYSDATE,
               'Allow users to execute edit, add, delete and copy operations on YM task path definition UI');

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'View YMS Task Path Definition',
             1,
             -1,
             'YMVTSPHDEF',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             'Allow users to View data in YMS task path definition UI');

----INSERT INTO app_mod_perm (APP_ID,
----                          MODULE_ID,
----                          PERMISSION_ID,
----                          ROW_UID,
----                          CREATED_DTTM,
----                          LAST_UPDATED_DTTM)
----     VALUES ((SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMATSPHDEF'),
----             (SEQ_APP_MOD_PERM.nextval),
----             SYSDATE,
----             NULL);
----
----INSERT INTO APP_MOD_PERM (APP_ID,
----                          MODULE_ID,
----                          PERMISSION_ID,
----                          ROW_UID,
----                          CREATED_DTTM,
----                          LAST_UPDATED_DTTM)
----     VALUES ((SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT PERMISSION_ID
----                FROM PERMISSION
----               WHERE PERMISSION_CODE = 'YMVTSPHDEF'),
----             (SEQ_APP_MOD_PERM.nextval),
----             SYSDATE,
----             NULL);
----
----INSERT INTO role_app_mod_perm (ROLE_ID,
----                               APP_ID,
----                               MODULE_ID,
----                               PERMISSION_ID,
----                               ROW_UID,
----                               CREATED_DTTM,
----                               LAST_UPDATED_DTTM)
----     VALUES ( (SELECT role_id
----                 FROM role
----                WHERE role_name = 'YMMGRROLE'),
----             (SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMATSPHDEF'),
----             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
----             SYSDATE,
----             NULL);
----
----INSERT INTO role_app_mod_perm (ROLE_ID,
----                               APP_ID,
----                               MODULE_ID,
----                               PERMISSION_ID,
----                               ROW_UID,
----                               CREATED_DTTM,
----                               LAST_UPDATED_DTTM)
----     VALUES ( (SELECT role_id
----                 FROM role
----                WHERE role_name = 'YMMGRROLE'),
----             (SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMVTSPHDEF'),
----             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
----             SYSDATE,
----             NULL);
			 

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (
               SEQ_PERMISSION_ID.NEXTVAL,
               'Administer YMS User Profile',
               1,
               -1,
               'YMAYUP',
               3,
               'Manhattan Associates',
               SYSDATE,
               3,
               'Manhattan Associates',
               SYSDATE,
               'Allow users to execute edit, add, delete and copy operations on YMS user profile UI');

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'View YMS User Profile',
             1,
             -1,
             'YMVYUP',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             'Allow users view data on YMS user profile UI');

----INSERT INTO app_mod_perm (APP_ID,
----                          MODULE_ID,
----                          PERMISSION_ID,
----                          ROW_UID,
----                          CREATED_DTTM,
----                          LAST_UPDATED_DTTM)
----     VALUES ((SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMAYUP'),
----             (SEQ_APP_MOD_PERM.nextval),
----             SYSDATE,
----             NULL);
----
----INSERT INTO APP_MOD_PERM (APP_ID,
----                          MODULE_ID,
----                          PERMISSION_ID,
----                          ROW_UID,
----                          CREATED_DTTM,
----                          LAST_UPDATED_DTTM)
----     VALUES ((SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT PERMISSION_ID
----                FROM PERMISSION
----               WHERE PERMISSION_CODE = 'YMVYUP'),
----             (SEQ_APP_MOD_PERM.nextval),
----             SYSDATE,
----             NULL);
----
----INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
----                               APP_ID,
----                               MODULE_ID,
----                               PERMISSION_ID,
----                               ROW_UID,
----                               CREATED_DTTM,
----                               LAST_UPDATED_DTTM)
----     VALUES ( (SELECT ROLE_ID
----                 FROM role
----                WHERE ROLE_NAME = 'YMMGRROLE'),
----             (SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT PERMISSION_ID
----                FROM PERMISSION
----               WHERE PERMISSION_CODE = 'YMAYUP'),
----             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
----             SYSDATE,
----             NULL);
----
----INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
----                               APP_ID,
----                               MODULE_ID,
----                               PERMISSION_ID,
----                               ROW_UID,
----                               CREATED_DTTM,
----                               LAST_UPDATED_DTTM)
----     VALUES ( (SELECT ROLE_ID
----                 FROM role
----                WHERE ROLE_NAME = 'YMMGRROLE'),
----             (SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management'),
----             (SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD'),
----             (SELECT PERMISSION_ID
----                FROM PERMISSION
----               WHERE PERMISSION_CODE = 'YMVYUP'),
----             SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
----             SYSDATE,
----             NULL);	
COMMIT;	

-- DBTicket DB-1839
INSERT INTO permission_tag (Tag_id,
                            description,
                            created_source_type_id,
                            created_source,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'TaskGrpElgblty',
             1,
             'Seed',
             1,
             'Seed');

INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'YMATGELG'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'TaskGrpElgblty'));
               
INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'YMVTGELG'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'TaskGrpElgblty')); 

INSERT INTO permission_tag (Tag_id,
                            description,
                            created_source_type_id,
                            created_source,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'TaskPathDefn',
             1,
             'Seed',
             1,
             'Seed');

INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'YMATSPHDEF'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'TaskPathDefn'));
               
INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'YMVTSPHDEF'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'TaskPathDefn'));


INSERT INTO permission_tag (Tag_id,
                            description,
                            created_source_type_id,
                            created_source,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'YardUserProfile',
             1,
             'Seed',
             1,
             'Seed');

INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'YMAYUP'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'YardUserProfile'));
			   
INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'YMVYUP'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'YardUserProfile'));	
COMMIT;			   			   

-- DBTicket DB-2152 REFL
INSERT
  INTO PERMISSION_INHERITANCE (PARENT_PERMISSION_ID,
                               CHILD_PERMISSION_ID,
                               CREATED_DTTM)
VALUES ( (SELECT PERMISSION_ID
            FROM PERMISSION
           WHERE PERMISSION_CODE = 'YMATSPHDEF'),
        (SELECT PERMISSION_ID
           FROM PERMISSION
          WHERE PERMISSION_CODE = 'YMVTSPHDEF'),
        SYSDATE);

INSERT
  INTO PERMISSION_INHERITANCE (PARENT_PERMISSION_ID,
                               CHILD_PERMISSION_ID,
                               CREATED_DTTM)
VALUES ( (SELECT PERMISSION_ID
            FROM PERMISSION
           WHERE PERMISSION_CODE = 'YMATGELG'),
        (SELECT PERMISSION_ID
           FROM PERMISSION
          WHERE PERMISSION_CODE = 'YMVTGELG'),
        SYSDATE);

INSERT
  INTO PERMISSION_INHERITANCE (PARENT_PERMISSION_ID,
                               CHILD_PERMISSION_ID,
                               CREATED_DTTM)
VALUES ( (SELECT PERMISSION_ID
            FROM PERMISSION
           WHERE PERMISSION_CODE = 'YMAYUP'),
        (SELECT PERMISSION_ID
           FROM PERMISSION
          WHERE PERMISSION_CODE = 'YMVYUP'),
        SYSDATE);
COMMIT;	 

-- DBTicket DB-2218
alter table facility_yard_zone modify (yard_zone_id number(15));

-- DBTicket DB-2283
ALTER TABLE ILM_DYNAMIC_GROUP ADD(FOR_DOCK NUMBER(1) DEFAULT 0 NOT NULL);
ALTER TABLE ILM_DYNAMIC_GROUP ADD(FOR_ZONE NUMBER(1) DEFAULT 0 NOT NULL);
ALTER TABLE ILM_DYNAMIC_GROUP ADD(FOR_SLOT NUMBER(1) DEFAULT 0 NOT NULL);

COMMENT ON COLUMN ILM_DYNAMIC_GROUP.FOR_DOCK IS  'Flag to check if a particular attribute is to determine the next trailer determination';
COMMENT ON COLUMN ILM_DYNAMIC_GROUP.FOR_ZONE IS  'Flag to check if a particular attribute is to determine the putaway location';
COMMENT ON COLUMN ILM_DYNAMIC_GROUP.FOR_SLOT IS  'Flag to check if a particular attribute is to determine appointment slots';

------ DBTicket DB-2406 REFL
----
----DELETE from app_mod_perm where APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID =(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') AND PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMATGELG');
----
----DELETE from app_mod_perm where APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID =(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') AND PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMVTGELG');
----
----DELETE FROM role_app_mod_perm where ROLE_ID =(SELECT role_id
----                 FROM role
----                WHERE role_name = 'YMMGRROLE') AND APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID=(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') and PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMATGELG');
----
----DELETE FROM role_app_mod_perm where ROLE_ID =(SELECT role_id
----                 FROM role
----                WHERE role_name = 'YMMGRROLE') AND APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID=(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') and PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMVTGELG');			   
----
----
----DELETE from app_mod_perm where APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID =(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') AND PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMATSPHDEF');
----			   
----DELETE from app_mod_perm where APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID =(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') AND PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMVTSPHDEF');
----
----DELETE FROM role_app_mod_perm where ROLE_ID =(SELECT role_id
----                 FROM role
----                WHERE role_name = 'YMMGRROLE') AND APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID=(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') and PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMATSPHDEF');
----
----DELETE FROM role_app_mod_perm where ROLE_ID =(SELECT role_id
----                 FROM role
----                WHERE role_name = 'YMMGRROLE') AND APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID=(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') and PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMVTSPHDEF');
----			   
----
----DELETE from app_mod_perm where APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID =(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') AND PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMAYUP');
----			   
----DELETE from app_mod_perm where APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID =(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') AND PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMVYUP');			   
----
----DELETE FROM role_app_mod_perm where ROLE_ID =(SELECT role_id
----                 FROM role
----                WHERE role_name = 'YMMGRROLE') AND APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID=(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') and PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMAYUP');			   
----
----DELETE FROM role_app_mod_perm where ROLE_ID =(SELECT role_id
----                 FROM role
----                WHERE role_name = 'YMMGRROLE') AND APP_ID=(SELECT APP_ID
----                 FROM APP
----                WHERE APP_NAME = 'Warehouse Management') AND MODULE_ID=(SELECT MODULE_ID
----                FROM MODULE
----               WHERE MODULE_CODE = 'YARD') and PERMISSION_ID=(SELECT permission_id
----                FROM permission
----               WHERE permission_code = 'YMVYUP');
----
----
----COMMIT;

-- DBTicket DB-2567
INSERT INTO ILM_DYNAMIC_GROUP (ILM_DYNAMIC_GROUP,
                               DESCRIPTION,
                               IS_FOR_FEASIBILITY,
                               IS_FOR_PRIORITY,
                               IS_FOR_TOTAL_CAPACITY,
                               CAPACITY_MEASURE_DESC,
                               IS_FOR_NTD,
                               IS_FOR_LOAD_UNLOAD_RATE,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM,
                               IS_FOR_TASK)
     VALUES (
               98,
               'DOCK DOOR CODE',
               0,
               1,
               0,
               NULL,
               0,
               0,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               1);

INSERT INTO ILM_DYNAMIC_GROUP (ILM_DYNAMIC_GROUP,
                               DESCRIPTION,
                               IS_FOR_FEASIBILITY,
                               IS_FOR_PRIORITY,
                               IS_FOR_TOTAL_CAPACITY,
                               CAPACITY_MEASURE_DESC,
                               IS_FOR_NTD,
                               IS_FOR_LOAD_UNLOAD_RATE,
                               CREATED_DTTM,
                               LAST_UPDATED_DTTM,
                               IS_FOR_TASK)
     VALUES (
               97,
               'SLOT',
               0,
               1,
               0,
               NULL,
               0,
               0,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               1);

UPDATE ilm_dynamic_group
   SET TYPE = 'INPUTL', VALUE_GENERATOR = 'DOCK', is_for_task = 1
 WHERE ilm_dynamic_group = '72';

UPDATE ilm_dynamic_group
   SET TYPE = 'INPUTL', VALUE_GENERATOR = 'DOOR'
 WHERE ilm_dynamic_group = '98';

UPDATE ilm_dynamic_group
   SET TYPE = 'INPUTL', VALUE_GENERATOR = 'SLOT'
 WHERE ilm_dynamic_group = '97';
 
COMMIT;

-- DBTicket DB-2852
UPDATE ILM_DYNAMIC_GROUP
   SET IS_FOR_TASK = 0
 WHERE ILM_DYNAMIC_GROUP = '97';

UPDATE ilm_dynamic_group
   SET TYPE = 'SELECT'
 WHERE ilm_dynamic_group = '56';

UPDATE ilm_dynamic_group
   SET TYPE = 'SELECT'
 WHERE ilm_dynamic_group = '60';

UPDATE ilm_dynamic_group
   SET TYPE = 'SELECT'
 WHERE ilm_dynamic_group = '32';

UPDATE ilm_dynamic_group
   SET TYPE = 'SELECT',
       VALUE_GENERATOR =
          'com.manh.appointment.ui.finitevalue.AppointmentPriorityFiniteValue'
 WHERE ilm_dynamic_group = '20';

UPDATE ilm_dynamic_group
   SET TYPE = 'SELECT', VALUE_GENERATOR = 'com.manh.common.fv.Direction'
 WHERE ilm_dynamic_group = '28';

UPDATE ilm_dynamic_group
   SET TYPE = 'SELECT'
 WHERE ilm_dynamic_group = '32';

UPDATE ilm_dynamic_group
   SET TYPE = 'SELECT',
       VALUE_GENERATOR = 'com.manh.yms.finitevalue.DockPlanningType'
 WHERE ilm_dynamic_group = '88';

UPDATE ilm_dynamic_group
   SET TYPE = 'SELECT',
       VALUE_GENERATOR = 'com.manh.yms.finitevalue.VisitType'
 WHERE ilm_dynamic_group = '92';

COMMIT;

-- DBTicket DB-3427
alter TABLE ILM_DYNAMIC_GROUP move TABLESPACE YMS_BST_DT_TBS ;

begin
for i in (select index_name from user_indexes where table_name = 'ILM_DYNAMIC_GROUP')
loop
    execute immediate 'alter index ' || i.index_name || ' rebuild TABLESPACE YMS_BST_IDX_TBS';
end loop;
end;
/ 

-- DBTicket DB-4754 REFL
MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'VYA') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM APP_MOD_PERM));

DECLARE
   V_COUNT   NUMBER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE ROLE_NAME = 'YMMGRROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm ramp
           USING (SELECT amp.app_id app_id,
                         amp.module_id module_id,
                         p.permission_id permission_id
                    FROM app_mod_perm amp, PERMISSION p
                   WHERE app_id = (SELECT app_id
                                     FROM app
                                    WHERE app_name = 'Warehouse Management')
                         AND p.permission_id = amp.permission_id
                         AND module_id = (SELECT module_id
                                            FROM module
                                           WHERE module_code = 'YARD')
                         AND p.permission_id =
                                (SELECT PERMISSION_ID
                                   FROM PERMISSION
                                  WHERE PERMISSION_CODE = 'VYA')) APP1
              ON (    ramp.app_id = app1.app_id
                  AND ramp.module_id = app1.module_id
                  AND ramp.permission_id = app1.permission_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ramp.role_id,
                     ramp.app_id,
                     ramp.module_id,
                     ramp.permission_id,
                     ramp.row_uid)
             VALUES (
                       (SELECT role_id
                          FROM role
                         WHERE role_name = 'YMMGRROLE'
                               AND company_id IN (SELECT company_id
                                                    FROM company
                                                   WHERE company_name = '01')),
                       app1.app_id,
                       APP1.MODULE_ID,
                       app1.permission_id,
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/
			   
COMMIT;

-- DBTicket DB-4851
comment on table ILM_DYNAMIC_GROUP is 'Contains the valid attributes that can be defined for Prioritization rules (NTD/PutAway and Task priority)';
-- DBTicket CBO-23417

UPDATE SYS_CODE SET CODE_DESC = 'Info', SHORT_DESC = 'Info', MISC_FLAGS = 'OTHERS         Information    Y' WHERE REC_TYPE = 'S' AND CODE_TYPE = 'I01' AND CODE_ID = 'Info';

COMMIT;

-- DBTicket CBO-23484

UPDATE SYS_CODE SET CODE_DESC = 'Burlington Coat Factory Label', SHORT_DESC = 'BrlCFLbl' WHERE REC_TYPE = 'B' AND CODE_TYPE = '504' AND CODE_ID = '041';

MERGE INTO LABEL L USING (SELECT 'SysCode' BUNDLE_NAME, 'Burlington Coat Factory Label' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Burlington Coat Factory Label', 'Burlington Coat Factory Label', 'SysCode');

COMMIT;


-- DBTicket CBO-23485

MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, '582' CODE_TYPE, '620 0501' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', '582', '620 0501', 'Order shortage during hard allocation', 'ordshrtha','',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = '582'));

MERGE INTO LABEL L USING (SELECT 'SysCode' BUNDLE_NAME, 'Order shortage during hard allocation' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Order shortage during hard allocation', 'Order shortage during hard allocation', 'SysCode');

MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, '582' CODE_TYPE, '620 0601' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', '582', '620 0601', 'Order shortage during soft allocation', 'ordshrtsa','',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = '582'));

MERGE INTO LABEL L USING (SELECT 'SysCode' BUNDLE_NAME, 'Order shortage during soft allocation' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Order shortage during soft allocation', 'Order shortage during soft allocation', 'SysCode');

MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, '582' CODE_TYPE, '620 0701' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', '582', '620 0701', 'Order increase from order rounding', 'ordincor ','',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = '582'));

MERGE INTO LABEL L USING (SELECT 'SysCode' BUNDLE_NAME, 'Order increase from order rounding' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Order increase from order rounding', 'Order increase from order rounding', 'SysCode');

MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, '582' CODE_TYPE, '602 14' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', '582', '602 14', 'Change work order quantity', 'ChgWOQty','',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = '582'));

MERGE INTO LABEL L USING (SELECT 'SysCode' BUNDLE_NAME, 'Change work order quantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Change work order quantity', 'Change work order quantity', 'SysCode');

MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, '582' CODE_TYPE, '601 02' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', '582', '601 02', 'RSDO status change', 'RSDOStatcg','',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = '582'));

MERGE INTO LABEL L USING (SELECT 'SysCode' BUNDLE_NAME, 'RSDO status change' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'RSDO status change', 'RSDO status change', 'SysCode');

COMMIT;

-- DBTicket CBO-23489

delete From Label Where Key='Delete Shipment Templates' and bundle_name='OM';
delete From Message_master where KEY='1130012' and bundle_name='ErrorMessage';

MERGE INTO LABEL L USING
(SELECT 'OM' BUNDLE_NAME, 'Delete Shipment Templates' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'Delete Shipment Templates',
      'Delete Shipment Profiles',
      'OM'
    );
	
	
MERGE INTO MESSAGE_MASTER M USING
  (SELECT '1130012' KEY, 'ErrorMessage' bundle_name FROM DUAL
  )
  d ON (m.key = d.key AND m.bundle_name = d.bundle_name)
WHEN NOT MATCHED THEN
  INSERT
    (
      MESSAGE_MASTER_ID,
      MSG_ID,
      KEY,
      ILS_MODULE,
      MSG_MODULE,
      MSG,
      BUNDLE_NAME,
      MSG_CLASS,
      MSG_TYPE,
      OVRIDE_ROLE,
      LOG_FLAG
    )
    VALUES
    (
      SEQ_MESSAGE_MASTER_ID.NEXTVAL,
      '1130012',
      '1130012',
      'cbo',
      NULL,
      'Shipment Profile {0} successfully created.',
      'ErrorMessage',
      NULL,
      NULL,
      NULL,
      NULL);

COMMIT;



-- DBTicket CBO-23552

Update Label Set Value='Pool Point Shipment' Where Key='PPShipment' and bundle_name='CBOTrans';

COMMIT;

-- DBTicket CBO-23580

MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, '582' CODE_TYPE, '606 0421' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', '582', '606 0421', 'Consume LPN to Pick before ASN verification', 'CnLPkb4vrf','',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = '582'));

MERGE INTO LABEL L USING (SELECT 'SysCode' BUNDLE_NAME, 'Consume LPN to Pick before ASN verification' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Consume LPN to Pick before ASN verification', 'Consume LPN to Pick before ASN verification', 'SysCode');

MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, '582' CODE_TYPE, '300 0421' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', '582', '300 0421', 'Consume LPN to Pick before ASN verification', 'CnLPkb4vrf','',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = '582'));

MERGE INTO LABEL L USING (SELECT 'SysCode' BUNDLE_NAME, 'Consume LPN to Pick before ASN verification' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Consume LPN to Pick before ASN verification', 'Consume LPN to Pick before ASN verification', 'SysCode');

COMMIT;


-- DBTicket CBO-23631

MERGE INTO RESOURCES A USING
(SELECT '/basedata/season/jsp/ProcessSeason.jsp' AS URI,
  1                                              AS URI_TYPE_ID,
  NULL AS HTTP_METHOD
FROM DUAL
) B ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI AND A.HTTP_METHOD = B.HTTP_METHOD)
WHEN NOT MATCHED THEN
  INSERT
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/basedata/season/jsp/ProcessSeason.jsp',
      'ACM',
      1,
      NULL
    );
    
MERGE INTO RESOURCE_PERMISSION RP USING
  (SELECT RESOURCES.RESOURCE_ID,
      'VBD' AS PERMISSION_CODE
    FROM RESOURCES
    WHERE RESOURCES.URI = '/basedata/season/jsp/ProcessSeason.jsp'
  )
  RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED THEN
  INSERT
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI        = '/basedata/season/jsp/ProcessSeason.jsp'
        AND URI_TYPE_ID  = 1
        AND HTTP_METHOD IS NULL
      )
      ,
      'VBD'
    );
	
COMMIT;


-- DBTicket CBO-23644

UPDATE RESOURCE_PERMISSION
SET PERMISSION_CODE   = 'UCLU'
WHERE PERMISSION_CODE = 'VBD'
AND RESOURCE_ID       =
  (SELECT RESOURCE_ID
  FROM RESOURCES
  WHERE URI        = '/basedata/popup/*'
  AND URI_TYPE_ID  = 1
  AND HTTP_METHOD IS NULL
  );
  
MERGE INTO RESOURCE_PERMISSION RP USING
(SELECT RESOURCES.RESOURCE_ID,
  'UCLCA' AS PERMISSION_CODE
FROM RESOURCES
WHERE RESOURCES.URI      = '/basedata/popup/*'
) RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED THEN
  INSERT
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI        = '/basedata/popup/*'
        AND URI_TYPE_ID  = 1
        AND HTTP_METHOD IS NULL
      )
      ,
      'UCLCA'
    );
	
COMMIT;

-- DBTicket CBO-23710

DELETE
FROM SYS_CODE_PARM
WHERE REC_TYPE = 'B'
AND CODE_TYPE  = '869'
AND FROM_POSN  = '3';

DELETE
FROM SYS_CODE_PARM
WHERE REC_TYPE = 'B'
AND CODE_TYPE  = '869'
AND FROM_POSN  = '13';

MERGE INTO SYS_CODE_PARM SCP USING
(SELECT 'B' REC_TYPE, '869' CODE_TYPE , '3' FROM_POSN FROM DUAL
) D ON ( SCP.REC_TYPE = D.REC_TYPE AND SCP.CODE_TYPE = D.CODE_TYPE AND SCP.FROM_POSN = D.FROM_POSN)
WHEN NOT MATCHED THEN
  INSERT
    (
      REC_TYPE,
      CODE_TYPE,
      FROM_POSN,
      TO_POSN,
      MISC_FLAG_DESC,
      LITRL,
      MANDT_FLAG,
      VALID_CODE,
      VALID_SYS_CODE_REC_TYPE,
      VALID_SYS_CODE_TYPE,
      VALID_FROM,
      VALID_TO,
      VALID_VALUES,
      EDIT_STYLE,
      CREATE_DATE_TIME,
      MOD_DATE_TIME,
      USER_ID,
      WM_VERSION_ID,
      SYS_CODE_PARM_ID,
      SYS_CODE_TYPE_ID,
      VALID_SYS_CODE_TYPE_ID
    )
    VALUES
    (
      'B',
      '869',
      '3',
      '17',
      'Preferred Location (Display Location)',
      'Preferred Location',
      'N',
      'B',
      '',
      '',
      '',
      '',
      NULL,
      'EDIT',
      SYSDATE,
      SYSDATE,
      'SEED',
      1,
      SYS_CODE_PARM_ID_SEQ.NEXTVAL,
      (SELECT SYS_CODE_TYPE_ID
      FROM SYS_CODE_TYPE
      WHERE REC_TYPE = 'B'
      AND CODE_TYPE  = '869'
      ),
      NULL
    );

MERGE INTO LABEL L USING
  (SELECT 'SysCode' BUNDLE_NAME,
      'Preferred Location (Display Location)' KEY
    FROM DUAL
  )
  B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'Preferred Location (Display Location)',
      'Preferred Location (Display Location)',
      'SysCode'
    );

MERGE INTO SYS_CODE_PARM SCP USING
  (SELECT 'B' REC_TYPE, '869' CODE_TYPE , '18' FROM_POSN FROM DUAL
  )
  D ON ( SCP.REC_TYPE = D.REC_TYPE AND SCP.CODE_TYPE = D.CODE_TYPE AND SCP.FROM_POSN = D.FROM_POSN)
WHEN NOT MATCHED THEN
  INSERT
    (
      REC_TYPE,
      CODE_TYPE,
      FROM_POSN,
      TO_POSN,
      MISC_FLAG_DESC,
      LITRL,
      MANDT_FLAG,
      VALID_CODE,
      VALID_SYS_CODE_REC_TYPE,
      VALID_SYS_CODE_TYPE,
      VALID_FROM,
      VALID_TO,
      VALID_VALUES,
      EDIT_STYLE,
      CREATE_DATE_TIME,
      MOD_DATE_TIME,
      USER_ID,
      WM_VERSION_ID,
      SYS_CODE_PARM_ID,
      SYS_CODE_TYPE_ID,
      VALID_SYS_CODE_TYPE_ID
    )
    VALUES
    (
      'B',
      '869',
      '18',
      '27',
      'Not used',
      'Not used',
      'N',
      'B',
      '',
      '',
      '',
      '',
      NULL,
      'EDIT',
      SYSDATE,
      SYSDATE,
      'SEED',
      1,
      SYS_CODE_PARM_ID_SEQ.NEXTVAL,
      (SELECT SYS_CODE_TYPE_ID
      FROM SYS_CODE_TYPE
      WHERE REC_TYPE = 'B'
      AND CODE_TYPE  = '869'
      ),
      NULL
    );

MERGE INTO LABEL L USING
  (SELECT 'SysCode' BUNDLE_NAME, 'Not used' KEY FROM DUAL
  )
  B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'Not used',
      'Not used',
      'SysCode'
    );
	
COMMIT;

-- DBTicket CBO-23711

MERGE INTO SYS_CODE A USING
(SELECT 'S' REC_TYPE, '525' CODE_TYPE, '9' CODE_ID FROM DUAL
) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      A.rec_type,
      A.code_type,
      A.code_id,
      A.code_desc,
      A.short_desc,
      A.misc_flags,
      A.create_date_time,
      A.mod_date_time,
      A.user_id,
      A.wm_version_id,
      A.sys_code_id,
      A.sys_code_type_id
    )
    VALUES
    (
      'S',
      '525',
      '9',
      'Java Class',
      'Java class',
      '',
      SYSDATE,
      SYSDATE,
      'SEED',
      1,
      SYS_CODE_ID_SEQ.NEXTVAL,
      (SELECT SYS_CODE_TYPE_ID
      FROM SYS_CODE_TYPE
      WHERE REC_TYPE = 'S'
      AND CODE_TYPE  = '525'
      )
    );
	
MERGE INTO LABEL L USING
  (SELECT 'SysCode' BUNDLE_NAME, 'Java Class' KEY FROM DUAL
  )
  B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'Java Class',
      'Java Class',
      'SysCode'
    );


COMMIT;

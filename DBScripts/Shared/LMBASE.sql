
-- DBTicket LM-9180

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
MERGE INTO PERMISSION L
     USING (SELECT 'LMAOBSINTCNFG' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Observation Management Interaction Type - Admin ',
               1,
               -1,
               'LMAOBSINTCNFG');

MERGE INTO PERMISSION L
     USING (SELECT 'LMVOBSINTCNFG' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Observation Management Interaction Type - View ',
               1,
               -1,
               'LMVOBSINTCNFG');	




INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMAOBSINTCNFG'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMVOBSINTCNFG'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));


exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

INSERT INTO app_mod_perm (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMAOBSINTCNFG'),
             (SEQ_APP_MOD_PERM.nextval));

INSERT INTO app_mod_perm (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMVOBSINTCNFG'),
             (SEQ_APP_MOD_PERM.nextval));

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
        INSERT INTO role_app_mod_perm (ROLE_ID,
                                       APP_ID,
                                       MODULE_ID,
                                       PERMISSION_ID,
                                       ROW_UID)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Labor Management'),
                     (SELECT module_id
                        FROM module
                       WHERE module_code = 'LMOBSMGMT'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_code = 'LMAOBSINTCNFG'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
        INSERT INTO role_app_mod_perm (ROLE_ID,
                                       APP_ID,
                                       MODULE_ID,
                                       PERMISSION_ID,
                                       ROW_UID)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Labor Management'),
                     (SELECT module_id
                        FROM module
                       WHERE module_code = 'LMOBSMGMT'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_code = 'LMVOBSINTCNFG'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
       END IF;
END;
/

--CALL SEQUPDT('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELAPPMODPERM_ROWUID');

INSERT INTO reltp_app_mod_perm (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES ( (SELECT relationship_type_id
                 FROM relationship_type
                WHERE description = 'Business Partner'),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMAOBSINTCNFG'),
             (SELECT MAX (ROW_UID)
                FROM RELTP_APP_MOD_PERM) + 4,
             0);

INSERT INTO reltp_app_mod_perm (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES ( (SELECT relationship_type_id
                 FROM relationship_type
                WHERE description = 'Business Partner'),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMVOBSINTCNFG'),
             (SELECT MAX (ROW_UID)
                FROM RELTP_APP_MOD_PERM) + 4,
             0);

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE)
     VALUES (seq_permission_id.NEXTVAL,
             'Observation Management Coaching Type - Admin',
             1,
             -1,
             'LMAOBSCOACNFG');

INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE)
     VALUES (seq_permission_id.NEXTVAL,
             'Observation Management Coaching Type - View',
             1,
             -1,
             'LMVOBSCOACNFG');

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMAOBSCOACNFG'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMVOBSCOACNFG'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));


INSERT INTO app_mod_perm (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMAOBSCOACNFG'),
             (SEQ_APP_MOD_PERM.nextval));

INSERT INTO app_mod_perm (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMVOBSCOACNFG'),
             (SEQ_APP_MOD_PERM.nextval));

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
        INSERT INTO role_app_mod_perm (ROLE_ID,
                                       APP_ID,
                                       MODULE_ID,
                                       PERMISSION_ID,
                                       ROW_UID)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Labor Management'),
                     (SELECT module_id
                        FROM module
                       WHERE module_code = 'LMOBSMGMT'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_code = 'LMAOBSCOACNFG'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

        INSERT INTO role_app_mod_perm (ROLE_ID,
                                       APP_ID,
                                       MODULE_ID,
                                       PERMISSION_ID,
                                       ROW_UID)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Labor Management'),
                     (SELECT module_id
                        FROM module
                       WHERE module_code = 'LMOBSMGMT'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_code = 'LMVOBSCOACNFG'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
       END IF;
END;
/

INSERT INTO reltp_app_mod_perm (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES ( (SELECT relationship_type_id
                 FROM relationship_type
                WHERE description = 'Business Partner'),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMAOBSCOACNFG'),
             (SELECT MAX (ROW_UID)
                FROM RELTP_APP_MOD_PERM) + 4,
             0);

INSERT INTO reltp_app_mod_perm (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES ( (SELECT relationship_type_id
                 FROM relationship_type
                WHERE description = 'Business Partner'),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMVOBSCOACNFG'),
             (SELECT MAX (ROW_UID)
                FROM RELTP_APP_MOD_PERM) + 4,
             0);

commit;


-- DBTicket LM-9273

INSERT INTO app_module
            (app_id,
             module_id, is_active, row_uid
            )
     VALUES ((SELECT app_id
                FROM app
               WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'SYSCTRL'), 1, (SELECT MAX (row_uid) + 1
                                                 FROM app_module)
            );
INSERT INTO app_mod_perm (APP_ID,
                          MODULE_ID,
                          permission_id,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM app
                WHERE app_short_name = 'LM'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'SYSCTRL'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'UCLCA'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO app_mod_perm (APP_ID,
                          MODULE_ID,
                          permission_id,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM app
                WHERE app_short_name = 'LM'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'SYSCTRL'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'UCLSA'),
             SEQ_APP_MOD_PERM.NEXTVAL);


DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
        INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                                       APP_ID,
                                       MODULE_ID,
                                       PERMISSION_ID,
                                       ROW_UID)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Labor Management'),
                     (SELECT module_id
                        FROM module
                       WHERE module_code = 'SYSCTRL'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_code = 'UCLCA'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
                     
        INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,
                                       APP_ID,
                                       MODULE_ID,
                                       PERMISSION_ID,
                                       ROW_UID)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Labor Management'),
                     (SELECT module_id
                        FROM module
                       WHERE module_code = 'SYSCTRL'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_code = 'UCLSA'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
       END IF;
END;
/
			 
commit;

-- DBTicket LM-9286
   
INSERT INTO SYS_CODE_TYPE (REC_TYPE,
                           CODE_TYPE,
                           CODE_DESC,
                           SHORT_DESC,
                           MAX_CODE_ID_LEN,
                           ALLOW_ADD,
                           ALLOW_DEL,
                           CHG_MISC_FLAG,
                           CHG_DTL_DESC,
                           WHSE_DEPNDT,
                           CREATE_DATE_TIME,
                           MOD_DATE_TIME,
                           USER_ID,
                           SYS_CODE_TYPE_ID)
     VALUES ('L',
             '162',
             'Invalid',
             'Inval',
             10,
             'Y',
             'Y',
             'Y',
             'Y',
             '0',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             SYS_CODE_TYPE_ID_SEQ.NEXTVAL);

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '162',
             'U1',
             'Unplanned Congestion',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '162',
             'U2',
             'Unfamiliar with SOP',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '162',
             'U3',
             'Phone Distraction',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '162',
             'U4',
             'Unclear Instruction',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '162',
             'U5',
             'Other Miscellaneous',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '162',
             'U6',
             '+ New Invalid Delay',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));



INSERT INTO SYS_CODE_TYPE (REC_TYPE,
                           CODE_TYPE,
                           CODE_DESC,
                           SHORT_DESC,
                           MAX_CODE_ID_LEN,
                           ALLOW_ADD,
                           ALLOW_DEL,
                           CHG_MISC_FLAG,
                           CHG_DTL_DESC,
                           WHSE_DEPNDT,
                           CREATE_DATE_TIME,
                           MOD_DATE_TIME,
                           USER_ID,
                           SYS_CODE_TYPE_ID)
     VALUES ('L',
             '161',
             'Valid',
             'Val',
             10,
             'Y',
             'Y',
             'Y',
             'Y',
             '0',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             SYS_CODE_TYPE_ID_SEQ.NEXTVAL);

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '161',
             'W',
             'Waiting to scan',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '161',
             'W1',
             'Waiting for system instruction',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '161',
             'W2',
             'Waiting for task allocation',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '161',
             'W3',
             'Waiting for system response',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '161',
             'W4',
             'System delay',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '161',
             'W5',
             '+ New Valid Delay',
             'Warehouse',
             'Warehouse',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '160'));

			   
commit;


-- DBTicket LM-9461

MERGE INTO sys_code A
     USING (SELECT 'L' AS REC_TYPE, '142' AS CODE_TYPE, '2014' AS CODE_ID
              FROM DUAL) B
        ON (A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_ID,
               CODE_DESC,
               SHORT_DESC,
               MISC_FLAGS,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('L',
               '142',
               '2014',
               '2014',
               'Ver 14',
               'Version 2014',
               SYSDATE,
               SYSDATE,
               'LMADMIN',
               1,
               sys_code_id_seq.NEXTVAL,
               (SELECT sys_code_type_id
                  FROM sys_code_type
                 WHERE rec_type = 'L' AND code_type = '142'));
				 
commit;

-- DBTicket LM-9499

MERGE INTO sys_code A
     USING (SELECT 'L' AS REC_TYPE, '067' AS CODE_TYPE, '2013' AS CODE_ID
              FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_ID,
               CODE_DESC,
               SHORT_DESC,
               MISC_FLAGS,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('L',
               '067',
               '2013',
               'Version 2013',
               '2013',
               '13',
               SYSDATE,
               SYSDATE,
               'LMADMIN',
               1,
               sys_code_id_seq.NEXTVAL,
               (SELECT sys_code_type_id
                  FROM sys_code_type
                 WHERE rec_type = 'L' AND code_type = '067'));


MERGE INTO sys_code A
     USING (SELECT 'L' AS REC_TYPE, '067' AS CODE_TYPE, '2014' AS CODE_ID
              FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_ID,
               CODE_DESC,
               SHORT_DESC,
               MISC_FLAGS,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('L',
               '067',
               '2014',
               'Version 2014',
               '2014',
               '14',
               SYSDATE,
               SYSDATE,
               'LMADMIN',
               1,
               sys_code_id_seq.NEXTVAL,
               (SELECT sys_code_type_id
                  FROM sys_code_type
                 WHERE rec_type = 'L' AND code_type = '067'));

COMMIT;

-- DBTicket LM-9654

DELETE FROM sys_code s1
      WHERE     s1.code_type = '161'
            AND s1.rec_type = 'L'
            AND s1.code_desc = '+ New Valid Delay';

DELETE FROM sys_code
      WHERE     code_type = '162'
            AND rec_type = 'L'
            AND code_desc = '+ New Invalid Delay';

COMMIT;



-- DBTicket LM-9658

DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT (1)
     INTO v_count
     FROM role
    WHERE role_name = 'LMROLE'
          AND company_id IN (SELECT company_id
                               FROM company
                              WHERE company_name = '01');

   IF v_count = 1
   THEN
      MERGE INTO role_app_mod_perm A
           USING (SELECT (SELECT role_id
                            FROM role
                           WHERE role_name = 'LMROLE')
                            AS role_id,
                         (SELECT app_id
                            FROM app
                           WHERE app_name = 'Dashboard')
                            AS app_id,
                         (SELECT module_id
                            FROM module
                           WHERE module_name = 'Dashboard')
                            AS module_id,
                         (SELECT permission_id
                            FROM permission
                           WHERE permission_name = 'Administer Portlets')
                            AS permission_id
                    FROM DUAL) B
              ON (    A.role_id = B.role_id
                  AND A.app_id = B.app_id
                  AND A.module_id = B.module_id
                  AND A.permission_id = B.permission_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (role_id,
                     app_id,
                     module_id,
                     permission_id,
                     row_uid)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE'),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Dashboard'),
                     (SELECT module_id
                        FROM module
                       WHERE module_name = 'Dashboard'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_name = 'Administer Portlets'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');


commit;


-- DBTicket LM-9807

exec sequpdt('ROLE_APP_MOD_PERM','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');

DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT (1)
     INTO v_count
     FROM role
    WHERE role_name = 'LMROLE'
          AND company_id IN (SELECT company_id
                               FROM company
                              WHERE company_name = '01');

   IF v_count = 1
   THEN
      MERGE INTO role_app_mod_perm A
           USING (SELECT (SELECT role_id
                            FROM role
                           WHERE role_name = 'LMROLE')
                            AS role_id,
                         (SELECT app_id
                            FROM app
                           WHERE app_name = 'Dashboard')
                            AS app_id,
                         (SELECT module_id
                            FROM module
                           WHERE module_name = 'Dashboard')
                            AS module_id,
                         (SELECT permission_id
                            FROM permission
                           WHERE permission_name = 'Administer Dashboard')
                            AS permission_id
                    FROM DUAL) B
              ON (    A.role_id = B.role_id
                  AND A.app_id = B.app_id
                  AND A.module_id = B.module_id
                  AND A.permission_id = B.permission_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (role_id,
                     app_id,
                     module_id,
                     permission_id,
                     row_uid)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE'),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Dashboard'),
                     (SELECT module_id
                        FROM module
                       WHERE module_name = 'Dashboard'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_name = 'Administer Dashboard'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
					 
	MERGE INTO role_app_mod_perm A
           USING (SELECT (SELECT role_id
                            FROM role
                           WHERE role_name = 'LMROLE')
                            AS role_id,
                         (SELECT app_id
                            FROM app
                           WHERE app_name = 'Dashboard')
                            AS app_id,
                         (SELECT module_id
                            FROM module
                           WHERE module_name = 'Dashboard')
                            AS module_id,
                         (SELECT permission_id
                            FROM permission
                           WHERE permission_name = 'View Dashboard')
                            AS permission_id
                    FROM DUAL) B
              ON (    A.role_id = B.role_id
                  AND A.app_id = B.app_id
                  AND A.module_id = B.module_id
                  AND A.permission_id = B.permission_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (role_id,
                     app_id,
                     module_id,
                     permission_id,
                     row_uid)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE'),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Dashboard'),
                     (SELECT module_id
                        FROM module
                       WHERE module_name = 'Dashboard'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_name = 'View Dashboard'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);				 
					 
					 
   END IF;
END;
/
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
commit;


-- DBTicket LM-9835

---UIB_QUERY

EXEC SEQUPDT('UIB_QUERY','UIB_QUERY_ID','SEQ_UIB_QUERY_ID');
exec sequpdt('UIB_QUERY_DETAILS','UIB_QUERY_DETAILS_ID','SEQ_UIB_QUERY_DETAILS_ID');
exec sequpdt('DSHBRD_PORTLET','DSHBRD_PORTLET_ID','SEQ_DSHBRD_PORTLET_ID');
exec sequpdt('DSHBRD_PORTLET_TAGS','DSHBRD_PORTLET_TAGS_ID','SEQ_DSHBRD_PORTLET_TAGS_ID');
exec sequpdt('DSHBRD_UIB_PTLT_DTL','DSHBRD_UIB_PTLT_DTL_ID','SEQ_DSHBRD_UIB_PTLT_DTL_ID');
CALL SEQUPDT('DS_DATA_SOURCE','DATA_SOURCE_ID','SEQ_DATA_SOURCE_ID');

DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT(1) INTO v_count
   FROM company
   WHERE company_name = '01';

   IF v_count > 0
   THEN
   
INSERT INTO UIB_QUERY (UIB_QUERY_ID,
                       QUERY_NAME,
                       QUERY_TYPE_ID,
                       QUERY_DESCRIPTION,
                       HIBERNATE_VERSION,
                       CREATED_SOURCE_TYPE,
                       CREATED_SOURCE,
                       CREATED_DTTM,
                       LAST_UPDATED_SOURCE_TYPE,
                       LAST_UPDATED_SOURCE,
                       LAST_UPDATED_DTTM,
                       CACHE_TIMEOUT_ID)
     VALUES (SEQ_UIB_QUERY_ID.NEXTVAL,
             'Performance_By_Employee',
             (SELECT UIB_QUERY_TYPE_ID
                FROM UIB_QUERY_TYPE
               WHERE QUERY_TYPE = 'SQL'),
             'Performance_By_Employee',
             0,
             1,
             'LMADMIN',
             SYSDATE,
             1,
             'LMADMIN',
             SYSDATE,
             1);

INSERT INTO UIB_QUERY (UIB_QUERY_ID,
                       QUERY_NAME,
                       QUERY_TYPE_ID,
                       QUERY_DESCRIPTION,
                       HIBERNATE_VERSION,
                       CREATED_SOURCE_TYPE,
                       CREATED_SOURCE,
                       CREATED_DTTM,
                       LAST_UPDATED_SOURCE_TYPE,
                       LAST_UPDATED_SOURCE,
                       LAST_UPDATED_DTTM,
                       CACHE_TIMEOUT_ID)
     VALUES (SEQ_UIB_QUERY_ID.NEXTVAL,
             'Utilization_By_Employee',
             (SELECT UIB_QUERY_TYPE_ID
                FROM UIB_QUERY_TYPE
               WHERE QUERY_TYPE = 'SQL'),
             'Utilization_By_Employee',
             0,
             1,
             'LMADMIN',
             SYSDATE,
             1,
             'LMADMIN',
             SYSDATE,
             1);


INSERT INTO UIB_QUERY (UIB_QUERY_ID,
                       QUERY_NAME,
                       QUERY_TYPE_ID,
                       QUERY_DESCRIPTION,
                       HIBERNATE_VERSION,
                       CREATED_SOURCE_TYPE,
                       CREATED_SOURCE,
                       CREATED_DTTM,
                       LAST_UPDATED_SOURCE_TYPE,
                       LAST_UPDATED_SOURCE,
                       LAST_UPDATED_DTTM,
                       CACHE_TIMEOUT_ID)
     VALUES (SEQ_UIB_QUERY_ID.NEXTVAL,
             'Quality_By_Employee',
             (SELECT UIB_QUERY_TYPE_ID
                FROM UIB_QUERY_TYPE
               WHERE QUERY_TYPE = 'SQL'),
             'Quality_By_Employee',
             0,
             1,
             'LMADMIN',
             SYSDATE,
             1,
             'LMADMIN',
             SYSDATE,
             1);


INSERT INTO UIB_QUERY (UIB_QUERY_ID,
                       QUERY_NAME,
                       QUERY_TYPE_ID,
                       QUERY_DESCRIPTION,
                       HIBERNATE_VERSION,
                       CREATED_SOURCE_TYPE,
                       CREATED_SOURCE,
                       CREATED_DTTM,
                       LAST_UPDATED_SOURCE_TYPE,
                       LAST_UPDATED_SOURCE,
                       LAST_UPDATED_DTTM,
                       CACHE_TIMEOUT_ID)
     VALUES (SEQ_UIB_QUERY_ID.NEXTVAL,
             'Unapproved_Indirect_Labor',
             (SELECT UIB_QUERY_TYPE_ID
                FROM UIB_QUERY_TYPE
               WHERE QUERY_TYPE = 'SQL'),
             'Unapproved_Indirect_Labor',
             0,
             1,
             'LMADMIN',
             SYSDATE,
             1,
             'LMADMIN',
             SYSDATE,
             1);

INSERT INTO UIB_QUERY (UIB_QUERY_ID,
                       QUERY_NAME,
                       QUERY_TYPE_ID,
                       QUERY_DESCRIPTION,
                       HIBERNATE_VERSION,
                       CREATED_SOURCE_TYPE,
                       CREATED_SOURCE,
                       CREATED_DTTM,
                       LAST_UPDATED_SOURCE_TYPE,
                       LAST_UPDATED_SOURCE,
                       LAST_UPDATED_DTTM,
                       CACHE_TIMEOUT_ID)
     VALUES (SEQ_UIB_QUERY_ID.NEXTVAL,
             'Interactions_Pending_For_The_Week',
             (SELECT UIB_QUERY_TYPE_ID
                FROM UIB_QUERY_TYPE
               WHERE QUERY_TYPE = 'SQL'),
             'Interactions_Pending_For_The_Week',
             0,
             1,
             'LMADMIN',
             SYSDATE,
             1,
             'LMADMIN',
             SYSDATE,
             1);


INSERT INTO UIB_QUERY (UIB_QUERY_ID,
                       QUERY_NAME,
                       QUERY_TYPE_ID,
                       QUERY_DESCRIPTION,
                       HIBERNATE_VERSION,
                       CREATED_SOURCE_TYPE,
                       CREATED_SOURCE,
                       CREATED_DTTM,
                       LAST_UPDATED_SOURCE_TYPE,
                       LAST_UPDATED_SOURCE,
                       LAST_UPDATED_DTTM,
                       CACHE_TIMEOUT_ID)
     VALUES (SEQ_UIB_QUERY_ID.NEXTVAL,
             'LM_Top_5_Performances_Under_Supervisor',
             (SELECT UIB_QUERY_TYPE_ID
                FROM UIB_QUERY_TYPE
               WHERE QUERY_TYPE = 'SQL'),
             'LM_Top_5_Performances_Under_Supervisor',
             0,
             1,
             'LMADMIN',
             SYSDATE,
             1,
             'LMADMIN',
             SYSDATE,
             1);

INSERT INTO UIB_QUERY (UIB_QUERY_ID,
                       QUERY_NAME,
                       QUERY_TYPE_ID,
                       QUERY_DESCRIPTION,
                       HIBERNATE_VERSION,
                       CREATED_SOURCE_TYPE,
                       CREATED_SOURCE,
                       CREATED_DTTM,
                       LAST_UPDATED_SOURCE_TYPE,
                       LAST_UPDATED_SOURCE,
                       LAST_UPDATED_DTTM,
                       CACHE_TIMEOUT_ID)
     VALUES (SEQ_UIB_QUERY_ID.NEXTVAL,
             'LM_Bottom_5_Performances_Under_Supervisor',
             (SELECT UIB_QUERY_TYPE_ID
                FROM UIB_QUERY_TYPE
               WHERE QUERY_TYPE = 'SQL'),
             'LM_Bottom_5_Performances_Under_Supervisor',
             0,
             1,
             'LMADMIN',
             SYSDATE,
             1,
             'LMADMIN',
             SYSDATE,
             1);



---UIB_QUERY_DETAILS

INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Performance_By_Employee'),
               NULL,
               'to_char( clock_in_date), case when total_pam =0.0 then 0.0 when total_nonstdlabor_time =0.0 then 0.0
else cast(ROUND(100.0*(total_pam/total_nonstdlabor_time), 2 ) as FLOAT) end as "EP"',
               'e_emp_perf_smry',
               'EMP_ID in 
(select emp_id from latest_emp_dtl where spvsr_emp_id = (select ucl_user_id from ucl_user where user_name =LMUSER)) and whse =LM
 and CLOCK_IN_DATE >= FN_DATEADD (DAY, -7, sysdate, E_EMP_PERF_SMRY.WHSE) order by CLOCK_IN_DATE',
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE);


INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Utilization_By_Employee'),
               NULL,
               'to_char( clock_in_date),case when UTIL_NUM =0.0 then 0.0 when UTIL_DENOM =0.0 then 0.0
else cast(ROUND(100.0*(UTIL_NUM/UTIL_DENOM), 2 ) as FLOAT) end as "UTIL"',
               'e_emp_perf_smry',
               'emp_id in 
(select emp_id from latest_emp_dtl where spvsr_emp_id = (select ucl_user_id from ucl_user where user_name =LMUSER)) and whse =LM 
 and CLOCK_IN_DATE >= FN_DATEADD (DAY, -7, sysdate, E_EMP_PERF_SMRY.WHSE) order by CLOCK_IN_DATE',
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE);


INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Quality_By_Employee'),
               NULL,
               'to_char( qa_totals_date),case when ((QA_TOTAL_QTY - QA_TOTAL_FAIL_QTY) / (QA_TOTAL_QTY)) =0.0 then 100 
else cast(ROUND(100.0*((QA_TOTAL_QTY - QA_TOTAL_FAIL_QTY) / (QA_TOTAL_QTY)), 2 ) as FLOAT) end as QA',
               'e_qa_smry',
               'emp_id in (select emp_id from latest_emp_dtl where spvsr_emp_id = (select ucl_user_id from ucl_user where user_name =LMUSER)) and whse =LM and  qa_totals_date >= FN_DATEADD (DAY, -7, sysdate, E_QA_SMRY.WHSE) order by qa_totals_date',
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE);


INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Unapproved_Indirect_Labor'),
               NULL,
               'to_char( create_date_time),count(1) as "UIL"',
               'e_evnt_smry_hdr',
               'evnt_stat_code in (5,25) and whse=LM and sched_start_date >= FN_DATEADD (DAY, -7, sysdate, e_evnt_smry_hdr.WHSE)group by create_date_time order by sched_start_date',
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE);


INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Interactions_Pending_For_The_Week'),
               NULL,
               'to_char( create_date_time),count(1) as "Interactions" ',
               'E_OBS_MGMT',
               'obs_made=N and E_OBS_MGMT.create_date_time >= FN_DATEADD(DAY, -7, sysdate,E_OBS_MGMT.whse) group by create_date_time',
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE);


INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'LM_Top_5_Performances'),
               NULL,
               'EMPLOYEE as "Employee", EPP as "EP %"',
               '(select EMP.EMP_ID, EPUSER.LAST_NAME||'', ''|| EPUSER.FIRST_NAME as EMPLOYEE, case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP 
FROM E_EMP_PERF_SMRY EPS
inner join VIEW_EMP_LATEST_INFO EMP on EPS.WHSE = EMP.WHSE
inner join VIEW_EMP_LATEST_INFO EPUSER on EPUSER.EMP_ID = EPS.EMP_ID and EMP.WHSE = EPUSER.WHSE where 
EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EMP.WHSE)
group by EPUSER.LAST_NAME, EPUSER.FIRST_NAME, EMP.EMP_ID
order by EPP desc, EMPLOYEE asc
) A ',
               'EMP_ID= :request.dshbrdUserId.isInteger and EPP in (
select EPP2 from (
select distinct EPP2 from (
select EPS2.LOGIN_USER_ID,  case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP2  from E_EMP_PERF_SMRY EPS2 
where EPS2.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS2.WHSE)
group by EPS2.LOGIN_USER_ID order by EPP2 desc 
) where EPP2 >=90  order by EPP2 desc 
) SQ2 where rownum <=5
)',
               0,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP);


INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'LM_Bottom_5_Performances'),
               NULL,
               'EMPLOYEE as "Employee", EPP as "EP %"',
               '(select EMP.EMP_ID, EPUSER.LAST_NAME||'', ''|| EPUSER.FIRST_NAME as EMPLOYEE, case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0
when SUM(TOTAL_PAM) =0.0 then 0.0 else ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP 
FROM E_EMP_PERF_SMRY EPS inner join VIEW_EMP_LATEST_INFO EMP on EPS.WHSE = EMP.WHSE
inner join VIEW_EMP_LATEST_INFO EPUSER on EPUSER.EMP_ID = EPS.EMP_ID and EMP.WHSE = EPUSER.WHSE 
where EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EMP.WHSE)
group by EPUSER.LAST_NAME, EPUSER.FIRST_NAME, EMP.EMP_ID
order by EPP , EMPLOYEE asc
) A ',
               'EMP_ID= :request.dshbrdUserId.isInteger and EPP in (
select EPP2 from (
select distinct EPP2 from (
select EPS2.LOGIN_USER_ID,  case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP2  from E_EMP_PERF_SMRY EPS2 
where EPS2.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS2.WHSE)
group by EPS2.LOGIN_USER_ID order by EPP2 asc 
) where EPP2 <=50  order by EPP2 asc 
) SQ2 where rownum <=5
)',
               0,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP);



--DASHBOARD

INSERT INTO DASHBOARD (DASHBOARD_ID,
                       name,
                       OWNER_ID,
                       OWNER_TYPE_ID,
                       DESCRIPTION,
                       IS_DEFAULT)
     VALUES (SEQ_DASHBOARD_ID.NEXTVAL,
             'LM_SPVSR_Dashboard',
             (SELECT MIN(COMPANY_ID)
                FROM COMPANY
               WHERE COMPANY_NAME = '01'),
             (SELECT DSHBRD_OWNER_TYPE_ID
                FROM DSHBRD_OWNER_TYPE
               WHERE OWNER_TYPE = 'COMPANY'),
             'Default LM Supervisor Seed Dashboard',
             1);


-- DSHBRD_PORTLET

INSERT INTO DSHBRD_PORTLET (DSHBRD_PORTLET_ID,
                            PORTLET_NAME,
                            APP_INSTANCE_ID,
                            URL,
                            PORTLET_HEIGHT_TYPE_ID,
                            PORTLET_WIDTH_TYPE_ID,
                            DESCRIPTION,
                            IMAGE_PATH,
                            IS_RSS_FEED,
                            URL_TYPE,
                            CONFIG_URL,
                            DSHBRD_PORTLET_TYPE_ID,
                            SUPPORT_MOBILE)
     VALUES (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,
             'Performance for the week (last 7 days) by date',
             NULL,
             NULL,
             10,
             10,
             NULL,
             NULL,
             NULL,
             'NONE',
             'default url',
             (SELECT DSHBRD_PORTLET_TYPE_ID
                FROM DSHBRD_PORTLET_TYPE
               WHERE DSHBRD_PORTLET_TYPE = 'Query'),
             NULL);


INSERT INTO DSHBRD_PORTLET (DSHBRD_PORTLET_ID,
                            PORTLET_NAME,
                            APP_INSTANCE_ID,
                            URL,
                            PORTLET_HEIGHT_TYPE_ID,
                            PORTLET_WIDTH_TYPE_ID,
                            DESCRIPTION,
                            IMAGE_PATH,
                            IS_RSS_FEED,
                            URL_TYPE,
                            CONFIG_URL,
                            DSHBRD_PORTLET_TYPE_ID,
                            SUPPORT_MOBILE)
     VALUES (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,
             'Utilization for the week (last 7 days) by date',
             NULL,
             NULL,
             10,
             10,
             NULL,
             NULL,
             NULL,
             'NONE',
             'default url',
             (SELECT DSHBRD_PORTLET_TYPE_ID
                FROM DSHBRD_PORTLET_TYPE
               WHERE DSHBRD_PORTLET_TYPE = 'Query'),
             NULL);

INSERT INTO DSHBRD_PORTLET (DSHBRD_PORTLET_ID,
                            PORTLET_NAME,
                            APP_INSTANCE_ID,
                            URL,
                            PORTLET_HEIGHT_TYPE_ID,
                            PORTLET_WIDTH_TYPE_ID,
                            DESCRIPTION,
                            IMAGE_PATH,
                            IS_RSS_FEED,
                            URL_TYPE,
                            CONFIG_URL,
                            DSHBRD_PORTLET_TYPE_ID,
                            SUPPORT_MOBILE)
     VALUES (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,
             'Quality for the week (last 7 days) by date',
             NULL,
             NULL,
             10,
             10,
             NULL,
             NULL,
             NULL,
             'NONE',
             'default url',
             (SELECT DSHBRD_PORTLET_TYPE_ID
                FROM DSHBRD_PORTLET_TYPE
               WHERE DSHBRD_PORTLET_TYPE = 'Query'),
             NULL);


INSERT INTO DSHBRD_PORTLET (DSHBRD_PORTLET_ID,
                            PORTLET_NAME,
                            APP_INSTANCE_ID,
                            URL,
                            PORTLET_HEIGHT_TYPE_ID,
                            PORTLET_WIDTH_TYPE_ID,
                            DESCRIPTION,
                            IMAGE_PATH,
                            IS_RSS_FEED,
                            URL_TYPE,
                            CONFIG_URL,
                            DSHBRD_PORTLET_TYPE_ID,
                            SUPPORT_MOBILE)
     VALUES (
               SEQ_DSHBRD_PORTLET_ID.NEXTVAL,
               'Number of Unapproved Indirect labor for the week (last 7 days) by date',
               NULL,
               NULL,
               10,
               10,
               NULL,
               NULL,
               NULL,
               'NONE',
               'default url',
               (SELECT DSHBRD_PORTLET_TYPE_ID
                  FROM DSHBRD_PORTLET_TYPE
                 WHERE DSHBRD_PORTLET_TYPE = 'Query'),
               NULL);


INSERT INTO DSHBRD_PORTLET (DSHBRD_PORTLET_ID,
                            PORTLET_NAME,
                            APP_INSTANCE_ID,
                            URL,
                            PORTLET_HEIGHT_TYPE_ID,
                            PORTLET_WIDTH_TYPE_ID,
                            DESCRIPTION,
                            IMAGE_PATH,
                            IS_RSS_FEED,
                            URL_TYPE,
                            CONFIG_URL,
                            DSHBRD_PORTLET_TYPE_ID,
                            SUPPORT_MOBILE)
     VALUES (
               SEQ_DSHBRD_PORTLET_ID.NEXTVAL,
               'Number of Interactions pending for the week (last 7 days) by date',
               NULL,
               NULL,
               10,
               10,
               NULL,
               NULL,
               NULL,
               'NONE',
               'default url',
               (SELECT DSHBRD_PORTLET_TYPE_ID
                  FROM DSHBRD_PORTLET_TYPE
                 WHERE DSHBRD_PORTLET_TYPE = 'Query'),
               NULL);


INSERT INTO DSHBRD_PORTLET (DSHBRD_PORTLET_ID,
                            PORTLET_NAME,
                            APP_INSTANCE_ID,
                            URL,
                            PORTLET_HEIGHT_TYPE_ID,
                            PORTLET_WIDTH_TYPE_ID,
                            DESCRIPTION,
                            IMAGE_PATH,
                            IS_RSS_FEED,
                            URL_TYPE,
                            CONFIG_URL,
                            DSHBRD_PORTLET_TYPE_ID,
                            SUPPORT_MOBILE)
     VALUES (
               SEQ_DSHBRD_PORTLET_ID.NEXTVAL,
               'Top 5 performers for the week (last 7 days) under the Supervisor',
               NULL,
               NULL,
               10,
               10,
               NULL,
               NULL,
               NULL,
               'NONE',
               'default url',
               (SELECT DSHBRD_PORTLET_TYPE_ID
                  FROM DSHBRD_PORTLET_TYPE
                 WHERE DSHBRD_PORTLET_TYPE = 'Query'),
               NULL);


INSERT INTO DSHBRD_PORTLET (DSHBRD_PORTLET_ID,
                            PORTLET_NAME,
                            APP_INSTANCE_ID,
                            URL,
                            PORTLET_HEIGHT_TYPE_ID,
                            PORTLET_WIDTH_TYPE_ID,
                            DESCRIPTION,
                            IMAGE_PATH,
                            IS_RSS_FEED,
                            URL_TYPE,
                            CONFIG_URL,
                            DSHBRD_PORTLET_TYPE_ID,
                            SUPPORT_MOBILE)
     VALUES (
               SEQ_DSHBRD_PORTLET_ID.NEXTVAL,
               'Bottom 5 performers for the week (last 7 days) under the Supervisor',
               NULL,
               NULL,
               10,
               10,
               NULL,
               NULL,
               NULL,
               'NONE',
               'default url',
               (SELECT DSHBRD_PORTLET_TYPE_ID
                  FROM DSHBRD_PORTLET_TYPE
                 WHERE DSHBRD_PORTLET_TYPE = 'Query'),
               NULL);

-- DSHBRD_PORTLET_TAGS

INSERT
  INTO DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID, DSHBRD_PORTLET_ID, TAGS)
VALUES (
          SEQ_DSHBRD_PORTLET_TAGS_ID.NEXTVAL,
          (SELECT DSHBRD_PORTLET_ID
             FROM DSHBRD_PORTLET
            WHERE PORTLET_NAME =
                     'Performance for the week (last 7 days) by date'),
          'Performance for the week (last 7 days) by date');

INSERT
  INTO DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID, DSHBRD_PORTLET_ID, TAGS)
VALUES (SEQ_DSHBRD_PORTLET_TAGS_ID.NEXTVAL,
        (SELECT DSHBRD_PORTLET_ID
           FROM DSHBRD_PORTLET
          WHERE PORTLET_NAME = 'Quality for the week (last 7 days) by date'),
        'Quality for the week (last 7 days) by date');

INSERT
  INTO DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID, DSHBRD_PORTLET_ID, TAGS)
VALUES (
          SEQ_DSHBRD_PORTLET_TAGS_ID.NEXTVAL,
          (SELECT DSHBRD_PORTLET_ID
             FROM DSHBRD_PORTLET
            WHERE PORTLET_NAME =
                     'Utilization for the week (last 7 days) by date'),
          'Utilization for the week (last 7 days) by date');

INSERT
  INTO DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID, DSHBRD_PORTLET_ID, TAGS)
VALUES (
          SEQ_DSHBRD_PORTLET_TAGS_ID.NEXTVAL,
          (SELECT DSHBRD_PORTLET_ID
             FROM DSHBRD_PORTLET
            WHERE PORTLET_NAME =
                     'Number of Unapproved Indirect labor for the week (last 7 days) by date'),
          'Number of Unapproved Indirect labor for the week (last 7 days) by date');

INSERT
  INTO DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID, DSHBRD_PORTLET_ID, TAGS)
VALUES (
          SEQ_DSHBRD_PORTLET_TAGS_ID.NEXTVAL,
          (SELECT DSHBRD_PORTLET_ID
             FROM DSHBRD_PORTLET
            WHERE PORTLET_NAME =
                     'Number of Interactions pending for the week (last 7 days) by date'),
          'Number of Interactions pending for the week (last 7 days) by date');

INSERT
  INTO DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID, DSHBRD_PORTLET_ID, TAGS)
VALUES (
          SEQ_DSHBRD_PORTLET_TAGS_ID.NEXTVAL,
          (SELECT DSHBRD_PORTLET_ID
             FROM DSHBRD_PORTLET
            WHERE PORTLET_NAME =
                     'Number of Interactions pending for the week (last 7 days) by date'),
          'Top 5 performers for the week (last 7 days) under the Supervisor');

INSERT
  INTO DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID, DSHBRD_PORTLET_ID, TAGS)
VALUES (
          SEQ_DSHBRD_PORTLET_TAGS_ID.NEXTVAL,
          (SELECT DSHBRD_PORTLET_ID
             FROM DSHBRD_PORTLET
            WHERE PORTLET_NAME =
                     'Number of Interactions pending for the week (last 7 days) by date'),
          'Bottom 5 performers for the week (last 7 days) under the Supervisor');


-- DS_DATA_SOURCE table data


MERGE INTO DS_DATA_SOURCE L
     USING (SELECT 'lm' APP_INST_SHORT_NAME FROM DUAL) B
        ON (L.APP_INST_SHORT_NAME = B.APP_INST_SHORT_NAME)
WHEN MATCHED
THEN
   UPDATE SET L.DATA_SOURCE_TYPE_ID = 1,
              L.CONNECTION_URL = 'jdbc:oracle:thin:@<host>:<port>:<sid>',
              L.DRIVER_CLASS = 'oracle.jdbc.driver.OracleDriver',
              L.USER_NAME = 'dbusername',
              L.PASSWORD = 'dbuserpassword',
              L.MAX_POOL_SIZE = 50
WHEN NOT MATCHED
THEN
   INSERT     (L.DATA_SOURCE_ID,
               L.APP_INST_SHORT_NAME,
               L.DATA_SOURCE_TYPE_ID,
               L.CONNECTION_URL,
               L.DRIVER_CLASS,
               L.USER_NAME,
               L.PASSWORD,
               L.MAX_POOL_SIZE)
       VALUES (SEQ_DATA_SOURCE_ID.NEXTVAL,
               'lm',
               1,
               'jdbc:oracle:thin:@<host>:<port>:<sid>',
               'oracle.jdbc.driver.OracleDriver',
               'dbusername',
               'dbuserpassword',
               50);



-- DSHBRD_UIB_PTLT_DTL

INSERT INTO DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,
                                 DSHBRD_PORTLET_ID,
                                 UIB_QUERY_ID,
                                 UIB_VIEW_TYPE_ID,
                                 UIB_VIEW_PROPERTY_SET_ID,
                                 HIBERNATE_VERSION,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 TITLE,
                                 DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Performance for the week (last 7 days) by date'),
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Performance_By_Employee'),
               (SELECT UIB_VIEW_TYPE_ID
                  FROM UIB_VIEW_TYPE
                 WHERE VIEW_TYPE = 'Bar'),
               NULL,
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE,
               'Performance for the week (last 7 days) by date',
               (SELECT DATA_SOURCE_ID
                  FROM DS_DATA_SOURCE
                 WHERE app_inst_short_name = 'lm'));


INSERT INTO DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,
                                 DSHBRD_PORTLET_ID,
                                 UIB_QUERY_ID,
                                 UIB_VIEW_TYPE_ID,
                                 UIB_VIEW_PROPERTY_SET_ID,
                                 HIBERNATE_VERSION,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 TITLE,
                                 DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Quality for the week (last 7 days) by date'),
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Quality_By_Employee'),
               (SELECT UIB_VIEW_TYPE_ID
                  FROM UIB_VIEW_TYPE
                 WHERE VIEW_TYPE = 'Bar'),
               NULL,
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE,
               'Quality for the week (last 7 days) by date',
               (SELECT DATA_SOURCE_ID
                  FROM DS_DATA_SOURCE
                 WHERE app_inst_short_name = 'lm'));


INSERT INTO DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,
                                 DSHBRD_PORTLET_ID,
                                 UIB_QUERY_ID,
                                 UIB_VIEW_TYPE_ID,
                                 UIB_VIEW_PROPERTY_SET_ID,
                                 HIBERNATE_VERSION,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 TITLE,
                                 DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Quality for the week (last 7 days) by date'),
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Quality_By_Employee'),
               (SELECT UIB_VIEW_TYPE_ID
                  FROM UIB_VIEW_TYPE
                 WHERE VIEW_TYPE = 'Bar'),
               NULL,
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE,
               'Quality for the week (last 7 days) by date',
               (SELECT DATA_SOURCE_ID
                  FROM DS_DATA_SOURCE
                 WHERE app_inst_short_name = 'lm'));


INSERT INTO DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,
                                 DSHBRD_PORTLET_ID,
                                 UIB_QUERY_ID,
                                 UIB_VIEW_TYPE_ID,
                                 UIB_VIEW_PROPERTY_SET_ID,
                                 HIBERNATE_VERSION,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 TITLE,
                                 DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Number of Unapproved Indirect labor for the week (last 7 days) by date'),
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Unapproved_Indirect_Labor'),
               (SELECT UIB_VIEW_TYPE_ID
                  FROM UIB_VIEW_TYPE
                 WHERE VIEW_TYPE = 'Bar'),
               NULL,
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE,
               'Number of Unapproved Indirect labor for the week (last 7 days) by date',
               (SELECT DATA_SOURCE_ID
                  FROM DS_DATA_SOURCE
                 WHERE app_inst_short_name = 'lm'));


INSERT INTO DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,
                                 DSHBRD_PORTLET_ID,
                                 UIB_QUERY_ID,
                                 UIB_VIEW_TYPE_ID,
                                 UIB_VIEW_PROPERTY_SET_ID,
                                 HIBERNATE_VERSION,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 TITLE,
                                 DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Number of Interactions pending for the week (last 7 days) by date'),
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Interactions_Pending_For_The_Week'),
               (SELECT UIB_VIEW_TYPE_ID
                  FROM UIB_VIEW_TYPE
                 WHERE VIEW_TYPE = 'Bar'),
               NULL,
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE,
               'Number of Interactions pending for the week (last 7 days) by date',
               (SELECT DATA_SOURCE_ID
                  FROM DS_DATA_SOURCE
                 WHERE app_inst_short_name = 'lm'));


INSERT
  INTO DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID, DSHBRD_PORTLET_ID, TAGS)
VALUES (
          SEQ_DSHBRD_PORTLET_TAGS_ID.NEXTVAL,
          (SELECT DSHBRD_PORTLET_ID
             FROM DSHBRD_PORTLET
            WHERE PORTLET_NAME =
                     'Top 5 performers for the week (last 7 days) under the Supervisor'),
          'Top 5 performers for the week (last 7 days) under the Supervisor');


INSERT INTO DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,
                                 DSHBRD_PORTLET_ID,
                                 UIB_QUERY_ID,
                                 UIB_VIEW_TYPE_ID,
                                 UIB_VIEW_PROPERTY_SET_ID,
                                 HIBERNATE_VERSION,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 TITLE,
                                 DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Top 5 performers for the week (last 7 days) under the Supervisor'),
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'LM_Top_5_Performances_Under_Supervisor'),
               (SELECT UIB_VIEW_TYPE_ID
                  FROM UIB_VIEW_TYPE
                 WHERE VIEW_TYPE = 'Bar'),
               NULL,
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE,
               'Top 5 performers for the week (last 7 days) under the Supervisor',
               (SELECT DATA_SOURCE_ID
                  FROM DS_DATA_SOURCE
                 WHERE app_inst_short_name = 'lm'));


INSERT INTO DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,
                                 DSHBRD_PORTLET_ID,
                                 UIB_QUERY_ID,
                                 UIB_VIEW_TYPE_ID,
                                 UIB_VIEW_PROPERTY_SET_ID,
                                 HIBERNATE_VERSION,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 TITLE,
                                 DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Bottom 5 performers for the week (last 7 days) under the Supervisor'),
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME =
                          'LM_Bottom_5_Performances_Under_Supervisor'),
               (SELECT UIB_VIEW_TYPE_ID
                  FROM UIB_VIEW_TYPE
                 WHERE VIEW_TYPE = 'Bar'),
               NULL,
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE,
               'Bottom 5 performers for the week (last 7 days) under the Supervisor',
               (SELECT DATA_SOURCE_ID
                  FROM DS_DATA_SOURCE
                 WHERE app_inst_short_name = 'lm'));


-- DSHBRD_USER_TAB -- change tab name and tab seq

INSERT INTO DSHBRD_USER_TAB (DSHBRD_USER_TAB_ID,
                             DASHBOARD_ID,
                             TAB_NAME,
                             TAB_SEQ,
                             LAYOUT_TYPE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_DSHBRD_USER_TAB_ID.NEXTVAL,
             (SELECT DASHBOARD_ID
                FROM DASHBOARD
               WHERE name = 'LM_SPVSR_Dashboard'),
             'LM Supervisor Dashboard',
             1,
             (SELECT DSHBRD_LAYOUT_ID
                FROM DSHBRD_LAYOUT
               WHERE LAYOUT_NAME = '2Column 50:50'),
             1,
             'LMADMIN',
             CURRENT_TIMESTAMP,
             1,
             'LMADMIN',
             CURRENT_TIMESTAMP);


-- DSHBRD_USER_DATA --change tab name

INSERT INTO DSHBRD_USER_DATA (DSHBRD_USER_DATA_ID,
                              TAB_ID,
                              PORTLET_ID,
                              COLUMN_INDEX,
                              ROW_INDEX,
                              USER_DATA_NAME,
                              LEFT_POSITION,
                              TOP_POSITION,
                              WIDTH,
                              HEIGHT,
                              ZINDEX,
                              DSHBRD_USER_DATA_STATUS_ID,
                              DSHBRD_USER_DATA_TYPE_ID,
                              DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_USER_DATA_ID.NEXTVAL,
               (SELECT DSHBRD_USER_TAB_ID
                  FROM DSHBRD_USER_TAB
                 WHERE TAB_NAME = 'LM Supervisor Dashboard'),
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Performance for the week (last 7 days) by date'),
               2,
               2,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               1,
               1,
               NULL);

INSERT INTO DSHBRD_USER_DATA (DSHBRD_USER_DATA_ID,
                              TAB_ID,
                              PORTLET_ID,
                              COLUMN_INDEX,
                              ROW_INDEX,
                              USER_DATA_NAME,
                              LEFT_POSITION,
                              TOP_POSITION,
                              WIDTH,
                              HEIGHT,
                              ZINDEX,
                              DSHBRD_USER_DATA_STATUS_ID,
                              DSHBRD_USER_DATA_TYPE_ID,
                              DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_USER_DATA_ID.NEXTVAL,
               (SELECT DSHBRD_USER_TAB_ID
                  FROM DSHBRD_USER_TAB
                 WHERE TAB_NAME = 'LM Supervisor Dashboard'),
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Quality for the week (last 7 days) by date'),
               2,
               2,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               1,
               1,
               NULL);

INSERT INTO DSHBRD_USER_DATA (DSHBRD_USER_DATA_ID,
                              TAB_ID,
                              PORTLET_ID,
                              COLUMN_INDEX,
                              ROW_INDEX,
                              USER_DATA_NAME,
                              LEFT_POSITION,
                              TOP_POSITION,
                              WIDTH,
                              HEIGHT,
                              ZINDEX,
                              DSHBRD_USER_DATA_STATUS_ID,
                              DSHBRD_USER_DATA_TYPE_ID,
                              DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_USER_DATA_ID.NEXTVAL,
               (SELECT DSHBRD_USER_TAB_ID
                  FROM DSHBRD_USER_TAB
                 WHERE TAB_NAME = 'LM Supervisor Dashboard'),
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Utilization for the week (last 7 days) by date'),
               2,
               2,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               1,
               1,
               NULL);

INSERT INTO DSHBRD_USER_DATA (DSHBRD_USER_DATA_ID,
                              TAB_ID,
                              PORTLET_ID,
                              COLUMN_INDEX,
                              ROW_INDEX,
                              USER_DATA_NAME,
                              LEFT_POSITION,
                              TOP_POSITION,
                              WIDTH,
                              HEIGHT,
                              ZINDEX,
                              DSHBRD_USER_DATA_STATUS_ID,
                              DSHBRD_USER_DATA_TYPE_ID,
                              DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_USER_DATA_ID.NEXTVAL,
               (SELECT DSHBRD_USER_TAB_ID
                  FROM DSHBRD_USER_TAB
                 WHERE TAB_NAME = 'LM Supervisor Dashboard'),
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Number of Unapproved Indirect labor for the week (last 7 days) by date'),
               2,
               2,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               1,
               1,
               NULL);

INSERT INTO DSHBRD_USER_DATA (DSHBRD_USER_DATA_ID,
                              TAB_ID,
                              PORTLET_ID,
                              COLUMN_INDEX,
                              ROW_INDEX,
                              USER_DATA_NAME,
                              LEFT_POSITION,
                              TOP_POSITION,
                              WIDTH,
                              HEIGHT,
                              ZINDEX,
                              DSHBRD_USER_DATA_STATUS_ID,
                              DSHBRD_USER_DATA_TYPE_ID,
                              DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_USER_DATA_ID.NEXTVAL,
               (SELECT DSHBRD_USER_TAB_ID
                  FROM DSHBRD_USER_TAB
                 WHERE TAB_NAME = 'LM Supervisor Dashboard'),
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Number of Interactions pending for the week (last 7 days) by date'),
               2,
               2,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               1,
               1,
               NULL);


INSERT INTO DSHBRD_USER_DATA (DSHBRD_USER_DATA_ID,
                              TAB_ID,
                              PORTLET_ID,
                              COLUMN_INDEX,
                              ROW_INDEX,
                              USER_DATA_NAME,
                              LEFT_POSITION,
                              TOP_POSITION,
                              WIDTH,
                              HEIGHT,
                              ZINDEX,
                              DSHBRD_USER_DATA_STATUS_ID,
                              DSHBRD_USER_DATA_TYPE_ID,
                              DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_USER_DATA_ID.NEXTVAL,
               (SELECT DSHBRD_USER_TAB_ID
                  FROM DSHBRD_USER_TAB
                 WHERE TAB_NAME = 'LM Supervisor Dashboard'),
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Top 5 performers for the week (last 7 days) under the Supervisor'),
               2,
               2,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               1,
               1,
               NULL);


INSERT INTO DSHBRD_USER_DATA (DSHBRD_USER_DATA_ID,
                              TAB_ID,
                              PORTLET_ID,
                              COLUMN_INDEX,
                              ROW_INDEX,
                              USER_DATA_NAME,
                              LEFT_POSITION,
                              TOP_POSITION,
                              WIDTH,
                              HEIGHT,
                              ZINDEX,
                              DSHBRD_USER_DATA_STATUS_ID,
                              DSHBRD_USER_DATA_TYPE_ID,
                              DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_USER_DATA_ID.NEXTVAL,
               (SELECT DSHBRD_USER_TAB_ID
                  FROM DSHBRD_USER_TAB
                 WHERE TAB_NAME = 'LM Supervisor Dashboard'),
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Bottom 5 performers for the week (last 7 days) under the Supervisor'),
               2,
               2,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               1,
               1,
               NULL);

   END IF;
END;
/

COMMIT;

-- DBTicket LM-9920

DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT(1) INTO v_count
   FROM company
   WHERE company_name = '01';

   IF v_count > 0
   THEN

DELETE FROM DSHBRD_UIB_PTLT_DTL
      WHERE DSHBRD_PORTLET_ID IN
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Quality for the week (last 7 days) by date');

INSERT INTO DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,
                                 DSHBRD_PORTLET_ID,
                                 UIB_QUERY_ID,
                                 UIB_VIEW_TYPE_ID,
                                 UIB_VIEW_PROPERTY_SET_ID,
                                 HIBERNATE_VERSION,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 TITLE,
                                 DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Utilization for the week (last 7 days) by date'),
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Utilization_By_Employee'),
               (SELECT UIB_VIEW_TYPE_ID
                  FROM UIB_VIEW_TYPE
                 WHERE VIEW_TYPE = 'Bar'),
               NULL,
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE,
               'Utilization for the week (last 7 days) by date',
               (SELECT DATA_SOURCE_ID
                  FROM DS_DATA_SOURCE
                 WHERE app_inst_short_name = 'lm'));


INSERT INTO DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,
                                 DSHBRD_PORTLET_ID,
                                 UIB_QUERY_ID,
                                 UIB_VIEW_TYPE_ID,
                                 UIB_VIEW_PROPERTY_SET_ID,
                                 HIBERNATE_VERSION,
                                 CREATED_SOURCE_TYPE,
                                 CREATED_SOURCE,
                                 CREATED_DTTM,
                                 LAST_UPDATED_SOURCE_TYPE,
                                 LAST_UPDATED_SOURCE,
                                 LAST_UPDATED_DTTM,
                                 TITLE,
                                 DATA_SOURCE_ID)
     VALUES (
               SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,
               (SELECT DSHBRD_PORTLET_ID
                  FROM DSHBRD_PORTLET
                 WHERE PORTLET_NAME =
                          'Quality for the week (last 7 days) by date'),
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Quality_By_Employee'),
               (SELECT UIB_VIEW_TYPE_ID
                  FROM UIB_VIEW_TYPE
                 WHERE VIEW_TYPE = 'Bar'),
               NULL,
               0,
               1,
               'LMADMIN',
               SYSDATE,
               1,
               'LMADMIN',
               SYSDATE,
               'Quality for the week (last 7 days) by date',
               (SELECT DATA_SOURCE_ID
                  FROM DS_DATA_SOURCE
                 WHERE app_inst_short_name = 'lm'));

				    END IF;
END;
/
commit;	


-- DBTicket LM-9942

EXEC SEQUPDT('UIB_QUERY_DETAILS','UIB_QUERY_DETAILS_ID','SEQ_UIB_QUERY_DETAILS_ID');

DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT(1) INTO v_count
   FROM company
   WHERE company_name = '01';

   IF v_count > 0
   THEN
   
DELETE FROM UIB_QUERY_DETAILS
      WHERE UIB_QUERY_ID IN (SELECT UIB_QUERY_ID
                               FROM UIB_QUERY
                              WHERE QUERY_NAME = 'LM_Top_5_Performances');

DELETE FROM UIB_QUERY_DETAILS
      WHERE UIB_QUERY_ID IN (SELECT UIB_QUERY_ID
                               FROM UIB_QUERY
                              WHERE QUERY_NAME = 'LM_Bottom_5_Performances');
                              
                              


INSERT INTO UIB_QUERY_DETAILS
                               (
                               UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'LM_Top_5_Performances'),
               NULL,
               'EMPLOYEE as "Employee", EPP as "EP %"',
               '(select EMP.EMP_ID, EPUSER.LAST_NAME||'', ''|| EPUSER.FIRST_NAME as EMPLOYEE, case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP 
FROM E_EMP_PERF_SMRY EPS
inner join VIEW_EMP_LATEST_INFO EMP on EPS.WHSE = EMP.WHSE
inner join VIEW_EMP_LATEST_INFO EPUSER on EPUSER.EMP_ID = EPS.EMP_ID and EMP.WHSE = EPUSER.WHSE where 
EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EMP.WHSE)
group by EPUSER.LAST_NAME, EPUSER.FIRST_NAME, EMP.EMP_ID
order by EPP desc, EMPLOYEE asc
) A ',
               'EMP_ID= :session.wt_loggedInUserId.isInteger and EPP in (
select EPP2 from (
select distinct EPP2 from (
select EPS2.LOGIN_USER_ID,  case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP2  from E_EMP_PERF_SMRY EPS2 
where EPS2.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS2.WHSE)
group by EPS2.LOGIN_USER_ID order by EPP2 desc 
) where EPP2 >=90  order by EPP2 desc 
) SQ2 where rownum <=5
)',
               0,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP);


INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'LM_Bottom_5_Performances'),
               NULL,
               'EMPLOYEE as "Employee", EPP as "EP %"',
               '(select EMP.EMP_ID, EPUSER.LAST_NAME||'', ''|| EPUSER.FIRST_NAME as EMPLOYEE, case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0
when SUM(TOTAL_PAM) =0.0 then 0.0 else ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP 
FROM E_EMP_PERF_SMRY EPS inner join VIEW_EMP_LATEST_INFO EMP on EPS.WHSE = EMP.WHSE
inner join VIEW_EMP_LATEST_INFO EPUSER on EPUSER.EMP_ID = EPS.EMP_ID and EMP.WHSE = EPUSER.WHSE 
where EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EMP.WHSE)
group by EPUSER.LAST_NAME, EPUSER.FIRST_NAME, EMP.EMP_ID
order by EPP , EMPLOYEE asc
) A ',
               'EMP_ID= :session.wt_loggedInUserId.isInteger and EPP in (
select EPP2 from (
select distinct EPP2 from (
select EPS2.LOGIN_USER_ID,  case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP2  from E_EMP_PERF_SMRY EPS2 
where EPS2.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS2.WHSE)
group by EPS2.LOGIN_USER_ID order by EPP2 asc 
) where EPP2 <=50  order by EPP2 asc 
) SQ2 where rownum <=5
)',
               0,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP);



INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'LM_Top_5_Performances_Under_Supervisor'),
               NULL,
               'EMPLOYEE as "Employee", EPP as "EP %"',
               '(select EMP.EMP_ID, EPUSER.LAST_NAME||'', ''|| EPUSER.FIRST_NAME as EMPLOYEE, case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP 
FROM E_EMP_PERF_SMRY EPS
inner join VIEW_EMP_LATEST_INFO EMP on EPS.WHSE = EMP.WHSE
inner join VIEW_EMP_LATEST_INFO EPUSER on EPUSER.EMP_ID = EPS.EMP_ID and EMP.WHSE = EPUSER.WHSE where 
EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EMP.WHSE)
group by EPUSER.LAST_NAME, EPUSER.FIRST_NAME, EMP.EMP_ID
order by EPP desc, EMPLOYEE asc
) A ',
               'EMP_ID= :request.loggedInUserId.isInteger and EPP in (
select EPP2 from (
select distinct EPP2 from (
select EPS2.LOGIN_USER_ID,  case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP2  from E_EMP_PERF_SMRY EPS2 
where EPS2.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS2.WHSE)
group by EPS2.LOGIN_USER_ID order by EPP2 desc 
) where EPP2 >=90  order by EPP2 desc 
) SQ2 where rownum <=5
)',
               0,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP);


INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME =
                          'LM_Bottom_5_Performances_Under_Supervisor'),
               NULL,
               'EMPLOYEE as "Employee", EPP as "EP %"',
               '(select EMP.EMP_ID, EPUSER.LAST_NAME||'', ''|| EPUSER.FIRST_NAME as EMPLOYEE, case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0
when SUM(TOTAL_PAM) =0.0 then 0.0 else ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP 
FROM E_EMP_PERF_SMRY EPS inner join VIEW_EMP_LATEST_INFO EMP on EPS.WHSE = EMP.WHSE
inner join VIEW_EMP_LATEST_INFO EPUSER on EPUSER.EMP_ID = EPS.EMP_ID and EMP.WHSE = EPUSER.WHSE 
where EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EMP.WHSE)
group by EPUSER.LAST_NAME, EPUSER.FIRST_NAME, EMP.EMP_ID
order by EPP , EMPLOYEE asc
) A ',
               'EMP_ID= :request.loggedInUserId.isInteger and EPP in (
select EPP2 from (
select distinct EPP2 from (
select EPS2.LOGIN_USER_ID,  case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP2  from E_EMP_PERF_SMRY EPS2 
where EPS2.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS2.WHSE)
group by EPS2.LOGIN_USER_ID order by EPP2 asc 
) where EPP2 <=50  order by EPP2 asc 
) SQ2 where rownum <=5
)',
               0,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP,
               1,
               'LMADMIN',
               CURRENT_TIMESTAMP);

   END IF;
END;
/
			   
commit;		


-- DBTicket LM-9949

CALL SEQUPDT ('ROLE_APP_MOD_PERM', 'ROW_UID', 'SEQ_ROLEAPPMODPERM_ROWUID');

-- New permission codes

CALL SEQUPDT ('PERMISSION', 'PERMISSION_ID', 'SEQ_PERMISSION_ID');

INSERT INTO permission (permission_id,
                        permission_name,
                        is_active,
                        company_id,
                        permission_code)
     VALUES (seq_permission_id.NEXTVAL,
             'Labor Mobility-Admin',
             1,
             -1,
             'LMAMOBILITY');

INSERT INTO MODULE (module_id,
                    module_name,
                    is_active,
                    module_code)
     VALUES ( (SELECT MAX (module_id) + 1 FROM module),
             'Labor Mobility',
             1,
             'LMLABMOB');

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMAMOBILITY'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO app_module (app_id,
                        module_id,
                        is_active,
                        row_uid)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Labor Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'LMLABMOB'),
             1,
             (SELECT MAX (ROW_UID) + 1 FROM APP_MODULE));

--APP_MOD_PERM

CALL SEQUPDT ('APP_MOD_PERM', 'ROW_UID', 'SEQ_APP_MOD_PERM');

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_NAME = 'Labor Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'LMLABMOB'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'LMAMOBILITY'),
             SEQ_APP_MOD_PERM.NEXTVAL);
			 
CALL SEQUPDT ('ROLE_APP_MOD_PERM', 'ROW_UID', 'SEQ_ROLEAPPMODPERM_ROWUID');			 

DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT (1)
     INTO v_count
     FROM role
    WHERE role_name = 'LMROLE'
          AND company_id IN (SELECT company_id
                               FROM company
                              WHERE company_name = '01');

   IF v_count > 0
   THEN
      INSERT INTO role_app_mod_perm (role_id,
                                     app_id,
                                     module_id,
                                     permission_id,
                                     row_uid)
           VALUES ( (SELECT role_id
                       FROM role
                      WHERE role_name = 'LMROLE'),
                   (SELECT app_id
                      FROM app
                     WHERE app_name = 'Labor Management'),
                   (SELECT module_id
                      FROM module
                     WHERE module_code = 'LMLABMOB'),
                   (SELECT permission_id
                      FROM permission
                     WHERE permission_code = 'LMAMOBILITY'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

--CALL SEQUPDT ('RELTP_APP_MOD_PERM', 'ROW_UID', 'SEQ_RELAPPMODPERM_ROWUID');			 

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Labor Management'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'LMLABMOB'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'LMAMOBILITY'),
             (SELECT MAX (ROW_UID)
                FROM RELTP_APP_MOD_PERM) + 4,
             0);

CALL SEQUPDT ('COMPANY_APP_MODULE', 'ROW_UID', 'SEQ_COAPPMOD_ROWUID');			 
			 
DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT(1) INTO v_count
   FROM company
   WHERE company_name = '01';

   IF v_count > 0
   THEN
   INSERT INTO company_app_module (company_id,
                                app_id,
                                module_id,
                                row_uid)
     VALUES ( (SELECT min(company_id)
                 FROM company
                WHERE company_name = '01'),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_name = 'Labor Mobility'),
             SEQ_COAPPMOD_ROWUID.NEXTVAL);

   END IF;
END;
/
commit;	 


-- DBTicket LM-10014

DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT(1) INTO v_count
   FROM company
   WHERE company_name = '01';

   IF v_count > 0
   THEN

-------Query for Performance_By_Employee-------
UPDATE UIB_QUERY_DETAILS 
set QUERY_WHERE_CLAUSE='EMP_ID in (select emp_id from latest_emp_dtl 
where spvsr_emp_id = (select ucl_user_id from ucl_user where user_name = :request.loggedInUserId.isString ))  
and CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, E_EMP_PERF_SMRY.WHSE) order by CLOCK_IN_DATE'
where
UIB_QUERY_ID =(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Performance_By_Employee');

				 
--------Query for Utilization_By_Employee------			 
UPDATE UIB_QUERY_DETAILS 
set QUERY_WHERE_CLAUSE='emp_id in (select emp_id from latest_emp_dtl 
where spvsr_emp_id = (select ucl_user_id from ucl_user where user_name = :request.loggedInUserId.isString)) 
and CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, E_EMP_PERF_SMRY.WHSE) order by CLOCK_IN_DATE'
where
UIB_QUERY_ID =(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Utilization_By_Employee');
				 
				 
-------Query for Quality_By_Employee--------
UPDATE UIB_QUERY_DETAILS 
set QUERY_WHERE_CLAUSE='emp_id in (select emp_id from latest_emp_dtl 
where spvsr_emp_id = (select ucl_user_id from ucl_user where user_name = :request.loggedInUserId.isString)) 
and  qa_totals_date <= FN_DATEADD (''DAY'', -7, current_timestamp, E_QA_SMRY.WHSE) order by qa_totals_date'
where
UIB_QUERY_ID =(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Quality_By_Employee');
				 
				 
-------Query for Interactions_Pending_For_The_Week------				 
UPDATE UIB_QUERY_DETAILS 
set QUERY_WHERE_CLAUSE='obs_made=''N'' and E_OBS_MGMT.create_date_time >= FN_DATEADD(''DAY'', -7, current_timestamp,E_OBS_MGMT.whse) group by create_date_time'
where
UIB_QUERY_ID =(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Interactions_Pending_For_The_Week');
				 
--------Query for Unapproved_Indirect_Labor--------			 
UPDATE UIB_QUERY_DETAILS 
set QUERY_WHERE_CLAUSE='evnt_stat_code in (5,25)  and create_date_time>= FN_DATEADD (''DAY'', -7, current_timestamp, e_evnt_smry_hdr.WHSE)
group by create_date_time order by create_date_time'
where
UIB_QUERY_ID =(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Unapproved_Indirect_Labor');

   END IF;
END;
/
				 
				 
COMMIT;


-- DBTicket LM-10009

DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT(1) INTO v_count
   FROM company
   WHERE company_name = '01';

   IF v_count > 0
   THEN
   
DELETE  from DSHBRD_USER_DATA where tab_id in(select DSHBRD_USER_TAB_ID from DSHBRD_USER_TAB where TAB_NAME = 'LM Reports') and portlet_id in(select DSHBRD_PORTLET_ID from DSHBRD_PORTLET where PORTLET_NAME = 'LM Reports');
delete from DSHBRD_USER_DATA where TAB_ID in (select DSHBRD_USER_TAB_ID from DSHBRD_USER_TAB where tab_name='LM Reports');
delete from DSHBRD_USER_DATA where PORTLET_ID in (select DSHBRD_PORTLET_ID from DSHBRD_PORTLET where portlet_name='LM Reports');
DELETE  from DSHBRD_USER_TAB where tab_name='LM Reports';
DELETE  from DSHBRD_PORTLET_TAGS where DSHBRD_PORTLET_ID in (select DSHBRD_PORTLET_ID from DSHBRD_PORTLET where PORTLET_NAME = 'LM Reports');
DELETE  from DSHBRD_PORTLET where portlet_name='LM Reports';

   END IF;
END;
/


COMMIT;

-- DBTicket LM-10057
CALL SEQUPDT('DASHBOARD','DASHBOARD_ID','SEQ_DASHBOARD_ID');

DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT(1) INTO v_count
   FROM company
   WHERE company_name = '01';

   IF v_count > 0
   THEN
   MERGE INTO DASHBOARD A
     USING (SELECT 'LM_Dashboard' AS name, 1 AS OWNER_ID, 1 AS OWNER_TYPE_ID
              FROM DUAL) B
        ON (    A.name = B.name
            AND A.OWNER_ID = B.OWNER_ID
            AND a.OWNER_TYPE_ID = b.OWNER_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (DASHBOARD_ID,
               name,
               OWNER_ID,
               OWNER_TYPE_ID,
               DESCRIPTION,
               IS_DEFAULT)
       VALUES (SEQ_DASHBOARD_ID.NEXTVAL,
               b.name,
               b.OWNER_ID,
               b.OWNER_TYPE_ID,
               'Default LM Seed Dashboard',
               1);  

UPDATE DSHBRD_USER_TAB set DASHBOARD_ID =(select DASHBOARD_ID from DASHBOARD where name = 'LM_Dashboard'),tab_seq=3 where tab_name='LM Supervisor Dashboard';			   

   END IF;
END;
/


DELETE FROM DSHBRD_PORTLET_PARAMS
      WHERE USER_DATA_ID IN
               (SELECT DSHBRD_USER_DATA_ID
                  FROM DSHBRD_USER_DATA
                 WHERE TAB_ID IN
                          (SELECT DSHBRD_USER_TAB_ID
                             FROM DSHBRD_USER_TAB
                            WHERE DASHBOARD_ID IN
                                     (SELECT DASHBOARD_ID
                                        FROM DASHBOARD
                                       WHERE NAME = 'LM_SPVSR_Dashboard')));

DELETE FROM DSHBRD_USER_DATA
      WHERE TAB_ID IN
               (SELECT DSHBRD_USER_TAB_ID
                  FROM DSHBRD_USER_TAB
                 WHERE DASHBOARD_ID IN (SELECT DASHBOARD_ID
                                          FROM DASHBOARD
                                         WHERE NAME = 'LM_SPVSR_Dashboard'));

DELETE FROM DSHBRD_USER_TAB
      WHERE DASHBOARD_ID IN (SELECT DASHBOARD_ID
                               FROM DASHBOARD
                              WHERE NAME = 'LM_SPVSR_Dashboard');

DELETE from DASHBOARD where name='LM_SPVSR_Dashboard';
commit;

-- DBTicket LM-10058

UPDATE DSHBRD_PORTLET
   SET PORTLET_NAME = 'Performance % for the Week'
 WHERE portlet_name = 'Performance for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET
   SET PORTLET_NAME = 'Quality Audit % for the Week'
 WHERE portlet_name = 'Quality for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET
   SET PORTLET_NAME = 'Utilization % for the Week'
 WHERE portlet_name = 'Utilization for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET
   SET PORTLET_NAME = 'Number of Unapproved Indirect Labor for the Week'
 WHERE portlet_name =
          'Number of Unapproved Indirect labor for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET
   SET PORTLET_NAME = 'Number of Interactions pending for the Week'
 WHERE portlet_name =
          'Number of Interactions pending for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET
   SET PORTLET_NAME = 'Top 5 Performers of the Week'
 WHERE portlet_name =
          'Top 5 performers for the week (last 7 days) under the Supervisor';

UPDATE DSHBRD_PORTLET
   SET PORTLET_NAME = 'Bottom 5 Performers of the Week'
 WHERE portlet_name =
          'Bottom 5 performers for the week (last 7 days) under the Supervisor';


UPDATE DSHBRD_PORTLET_TAGS
   SET TAGS = 'Performance % for the Week'
 WHERE tags = 'Performance for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET_TAGS
   SET TAGS = 'Quality Audit % for the Week'
 WHERE tags = 'Quality for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET_TAGS
   SET TAGS = 'Utilization % for the Week'
 WHERE tags = 'Utilization for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET_TAGS
   SET TAGS = 'Number of Unapproved Indirect Labor for the Week'
 WHERE tags =
          'Number of Unapproved Indirect labor for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET_TAGS
   SET TAGS = 'Number of Interactions pending for the Week'
 WHERE tags =
          'Number of Interactions pending for the week (last 7 days) by date';

UPDATE DSHBRD_PORTLET_TAGS
   SET TAGS = 'Top 5 Performers of the Week'
 WHERE tags =
          'Top 5 performers for the week (last 7 days) under the Supervisor';

UPDATE DSHBRD_PORTLET_TAGS
   SET TAGS = 'Bottom 5 Performers of the Week'
 WHERE tags =
          'Bottom 5 performers for the week (last 7 days) under the Supervisor';


UPDATE DSHBRD_UIB_PTLT_DTL
   SET TITLE = 'Performance % for the Week'
 WHERE TITLE = 'Performance for the week (last 7 days) by date';

UPDATE DSHBRD_UIB_PTLT_DTL
   SET TITLE = 'Quality Audit % for the Week'
 WHERE TITLE = 'Quality for the week (last 7 days) by date';

UPDATE DSHBRD_UIB_PTLT_DTL
   SET TITLE = 'Utilization % for the Week'
 WHERE TITLE = 'Quality for the week (last 7 days) by date';

UPDATE DSHBRD_UIB_PTLT_DTL
   SET TITLE = 'Number of Unapproved Indirect Labor for the Week'
 WHERE TITLE =
          'Number of Unapproved Indirect labor for the week (last 7 days) by date';

UPDATE DSHBRD_UIB_PTLT_DTL
   SET TITLE = 'Number of Interactions pending for the Week'
 WHERE TITLE =
          'Number of Interactions pending for the week (last 7 days) by date';

UPDATE DSHBRD_UIB_PTLT_DTL
   SET TITLE = 'Top 5 Performers of the Week'
 WHERE TITLE =
          'Top 5 performers for the week (last 7 days) under the Supervisor';

UPDATE DSHBRD_UIB_PTLT_DTL
   SET TITLE = 'Bottom 5 Performers of the Week'
 WHERE TITLE =
          'Bottom 5 performers for the week (last 7 days) under the Supervisor';
		  
COMMIT;


-- DBTicket LM-10111

-------Query for Performance_By_Employee-------

UPDATE UIB_QUERY_DETAILS
   SET QUERY_WHERE_CLAUSE =
          'EMP_ID in (select emp_id from latest_emp_dtl 
where spvsr_emp_id = :session.wt_loggedInUserId.isInteger)  
and CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, E_EMP_PERF_SMRY.WHSE) order by CLOCK_IN_DATE'
 WHERE UIB_QUERY_ID IN (SELECT UIB_QUERY_ID
                          FROM UIB_QUERY
                         WHERE QUERY_NAME = 'Performance_By_Employee');


--------Query for Utilization_By_Employee------

UPDATE UIB_QUERY_DETAILS
   SET QUERY_WHERE_CLAUSE =
          'emp_id in (select emp_id from latest_emp_dtl 
where spvsr_emp_id = :session.wt_loggedInUserId.isInteger) 
and CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, E_EMP_PERF_SMRY.WHSE) order by CLOCK_IN_DATE'
 WHERE UIB_QUERY_ID IN (SELECT UIB_QUERY_ID
                          FROM UIB_QUERY
                         WHERE QUERY_NAME = 'Utilization_By_Employee');


-------Query for Quality_By_Employee--------

UPDATE UIB_QUERY_DETAILS
   SET QUERY_WHERE_CLAUSE =
          'emp_id in (select emp_id from latest_emp_dtl 
where spvsr_emp_id = :session.wt_loggedInUserId.isInteger) 
and  qa_totals_date <= FN_DATEADD (''DAY'', -7, current_timestamp, E_QA_SMRY.WHSE) order by qa_totals_date'
 WHERE UIB_QUERY_ID IN (SELECT UIB_QUERY_ID
                          FROM UIB_QUERY
                         WHERE QUERY_NAME = 'Quality_By_Employee');


-------Query for Interactions_Pending_For_The_Week------

UPDATE UIB_QUERY_DETAILS
   SET QUERY_WHERE_CLAUSE =
          'obs_made=''N'' and E_OBS_MGMT.create_date_time >= FN_DATEADD(''DAY'', -7, current_timestamp,E_OBS_MGMT.whse) group by create_date_time'
 WHERE UIB_QUERY_ID IN
          (SELECT UIB_QUERY_ID
             FROM UIB_QUERY
            WHERE QUERY_NAME = 'Interactions_Pending_For_The_Week');

--------Query for Unapproved_Indirect_Labor--------

UPDATE UIB_QUERY_DETAILS
   SET QUERY_WHERE_CLAUSE =
          'evnt_stat_code in (5,25)  and create_date_time>= FN_DATEADD (''DAY'', -7, current_timestamp, e_evnt_smry_hdr.WHSE)
group by create_date_time order by create_date_time'
 WHERE UIB_QUERY_ID IN (SELECT UIB_QUERY_ID
                          FROM UIB_QUERY
                         WHERE QUERY_NAME = 'Unapproved_Indirect_Labor');


------Top 5 Performance-------

UPDATE UIB_QUERY_DETAILS
   SET QUERY_WHERE_CLAUSE =
          'EMP_ID= :session.wt_loggedInUserId.isInteger and EPP in (
select EPP2 from (
select distinct EPP2 from (
select EPS2.LOGIN_USER_ID,  case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP2  from E_EMP_PERF_SMRY EPS2 
where EPS2.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current timestamp, EPS2.WHSE)
group by EPS2.LOGIN_USER_ID order by EPP2 asc 
) where EPP2 <=50  order by EPP2 asc 
) SQ2 where rownum <=5
)'
 WHERE UIB_QUERY_ID IN
          (SELECT UIB_QUERY_ID
             FROM UIB_QUERY
            WHERE QUERY_NAME = 'LM_Top_5_Performances_Under_Supervisor');


-------Bottom 5 Performance-----

UPDATE UIB_QUERY_DETAILS
   SET QUERY_WHERE_CLAUSE =
          'EMP_ID= :session.wt_loggedInUserId.isInteger and EPP in (
select EPP2 from (
select distinct EPP2 from (
select EPS2.LOGIN_USER_ID,  case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP2  from E_EMP_PERF_SMRY EPS2 
where EPS2.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current timestamp, EPS2.WHSE)
group by EPS2.LOGIN_USER_ID order by EPP2 asc 
) where EPP2 <=50  order by EPP2 asc 
) SQ2 where rownum <=5
)'
 WHERE UIB_QUERY_ID IN
          (SELECT UIB_QUERY_ID
             FROM UIB_QUERY
            WHERE QUERY_NAME = 'LM_Bottom_5_Performances_Under_Supervisor');


COMMIT;

-- DBTicket LM-10119

----Employee Performance------
UPDATE UIB_QUERY_DETAILS 
set 
QUERY_COLUMNS='cd, case when sum(total_nonstdlabor_time) != 0.0 then cast(ROUND(100.0*(sum(total_pam)/sum(total_nonstdlabor_time)), 2 ) as FLOAT) else 0.0 end as EP',
QUERY_TABLES='(select EMP_ID,  CLOCK_IN_DATE, whse, FN_DATESTRING_DATEPART(clock_in_date,whse) cd,  total_pam, total_nonstdlabor_time 
      from e_emp_perf_smry) EPS',
QUERY_WHERE_CLAUSE='EMP_ID in (select emp_id from latest_emp_dtl
      where spvsr_emp_id = :session.wt_loggedInUserId.isInteger )
      and CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE) 
Group by cd
order by cd'
where
UIB_QUERY_ID in(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Performance_By_Employee');
				 
				 
--------Query for Utilization_By_Employee------
UPDATE UIB_QUERY_DETAILS 
set 
QUERY_COLUMNS='cd, case when sum(UTIL_DENOM) != 0.0 then cast(ROUND(100.0*(sum(UTIL_NUM)/sum(UTIL_DENOM)), 2 ) as FLOAT) else 0.0 end as UTIL',
QUERY_TABLES='(select EMP_ID,  CLOCK_IN_DATE, whse, FN_DATESTRING_DATEPART(clock_in_date,whse) cd,  UTIL_NUM, UTIL_DENOM 
      from e_emp_perf_smry) EPS',
QUERY_WHERE_CLAUSE='EMP_ID in (select emp_id from latest_emp_dtl
      where spvsr_emp_id = :session.wt_loggedInUserId.isInteger )
      and CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE) 
Group by cd
order by cd'
where
UIB_QUERY_ID in(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Utilization_By_Employee');
				 
				 
-------Query for Quality_By_Employee--------
UPDATE UIB_QUERY_DETAILS 
set 
QUERY_COLUMNS='cd, case when sum(QA_TOTAL_QTY) != 0.0 then cast(ROUND(100.0*(sum(QA_TOTAL_QTY - QA_TOTAL_FAIL_QTY)/sum(QA_TOTAL_QTY)), 2 ) as FLOAT) else 100.0 end as QA',
QUERY_TABLES='(select EMP_ID, qa_totals_date,whse,to_char(qa_totals_date) cd,QA_TOTAL_QTY,QA_TOTAL_FAIL_QTY
	from e_qa_smry) EQS',
QUERY_WHERE_CLAUSE='emp_id in (select emp_id from latest_emp_dtl
where spvsr_emp_id = :session.wt_loggedInUserId.isInteger )
and  qa_totals_date >= FN_DATEADD (''DAY'', -7, current_timestamp, EQS.WHSE) 
Group by cd
order by cd'
where
UIB_QUERY_ID in(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Quality_By_Employee');
				 
				 
-------Query for Interactions_Pending_For_The_Week------
UPDATE UIB_QUERY_DETAILS 
set 
QUERY_COLUMNS='cd, count(1) as Interactions',
QUERY_TABLES='(select spvsr_id,OBS_REQ_DATE_TIME, whse, FN_DATESTRING_DATEPART(OBS_REQ_DATE_TIME,whse) cd, obs_made
      from E_OBS_MGMT) OBS',
QUERY_WHERE_CLAUSE='( OBS.spvsr_id = :session.wt_loggedInUserId.isInteger  ) and
obs_made=''N'' and OBS.OBS_REQ_DATE_TIME >= FN_DATEADD(''DAY'', -7, current_timestamp,OBS.whse) 
Group by cd
order by cd'
where
UIB_QUERY_ID in(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Interactions_Pending_For_The_Week');
				 
				 
--------Query for Unapproved_Indirect_Labor--------
UPDATE UIB_QUERY_DETAILS 
set 
QUERY_COLUMNS='cd, count(evnt_stat_code) as UIL',
QUERY_TABLES='(select spvsr_login_user_id, sched_start_date , whse, FN_DATESTRING_DATEPART(sched_start_date,whse) cd, evnt_stat_code 
      from e_evnt_smry_hdr) EVS',
QUERY_WHERE_CLAUSE='EVS.spvsr_login_user_id in (select user_name from ucl_user where ucl_user_id = :session.wt_loggedInUserId.isInteger )  and  
evnt_stat_code in (5,25)  and sched_start_date >= FN_DATEADD (''DAY'', -7, current_timestamp, WHSE)
Group by cd
order by cd'
where
UIB_QUERY_ID in(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'Unapproved_Indirect_Labor');
				 
				 
----Top 5 performances-----
UPDATE UIB_QUERY_DETAILS 
set 
QUERY_COLUMNS='"Employee", "EP %"
from
(
select USER_LAST_NAME||'', ''|| USER_FIRST_NAME as "Employee", EPP as "EP %"',
QUERY_TABLES='(select led.EMP_ID, uu.USER_LAST_NAME, uu.USER_FIRST_NAME, case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP
FROM E_EMP_PERF_SMRY EPS inner join latest_emp_dtl led on EPS.emp_id = led.emp_id and EPS.whse = led.whse inner join
ucl_user uu on uu.ucl_user_id = led.emp_id',
QUERY_WHERE_CLAUSE='led.spvsr_emp_id = :session.wt_loggedInUserId.isInteger and EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE)
group by led.EMP_ID, uu.USER_LAST_NAME, uu.USER_FIRST_NAME
) A 
order by EPP desc)
where rownum <=5'
where
UIB_QUERY_ID in(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'LM_Top_5_Performances_Under_Supervisor');
				 
				 
-------Bottom 5 Performance-----
UPDATE UIB_QUERY_DETAILS 
set 
QUERY_COLUMNS='"Employee", "EP %"
from 
(
select USER_LAST_NAME||'', ''|| USER_FIRST_NAME as "Employee", EPP as "EP %"
from (select led.EMP_ID, uu.USER_LAST_NAME, uu.USER_FIRST_NAME, case when SUM(TOTAL_NONSTDLABOR_TIME) =0.0 then 0.0 when SUM(TOTAL_PAM) =0.0 then 0.0 else
ROUND(((100.00000*(SUM(TOTAL_PAM)))/SUM(TOTAL_NONSTDLABOR_TIME)),2)end  as EPP',
QUERY_TABLES='E_EMP_PERF_SMRY EPS inner join latest_emp_dtl led on EPS.emp_id = led.emp_id and EPS.whse = led.whse inner join
ucl_user uu on uu.ucl_user_id = led.emp_id',
QUERY_WHERE_CLAUSE='led.spvsr_emp_id = :session.wt_loggedInUserId.isInteger and EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE)
group by led.EMP_ID, uu.USER_LAST_NAME, uu.USER_FIRST_NAME
) A 
order by EPP asc)
where 
rownum <=5'
where
UIB_QUERY_ID in(SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'LM_Bottom_5_Performances_Under_Supervisor');
				 
COMMIT;

-- DBTicket LM-10124
MERGE INTO DS_DATA_SOURCE L
     USING (SELECT 'lm' APP_INST_SHORT_NAME FROM DUAL) B
        ON (L.APP_INST_SHORT_NAME = B.APP_INST_SHORT_NAME)
WHEN MATCHED
THEN
   UPDATE SET L.password = 'C250AC3763654CA727672D4E622FEB5E';                
commit;

-- DBTicket LM-10127
DECLARE
   v_count   NUMBER;
BEGIN
   SELECT COUNT(1) INTO v_count
   FROM company
   WHERE company_name = '01';

   IF v_count > 0
   THEN

UPDATE DSHBRD_UIB_PTLT_DTL
   SET uib_view_type_id =
          (SELECT UIB_VIEW_TYPE_ID
             FROM UIB_VIEW_TYPE
            WHERE VIEW_TYPE = 'Grid')
 WHERE DSHBRD_PORTLET_ID IN
          (SELECT DSHBRD_PORTLET_ID
             FROM DSHBRD_PORTLET
            WHERE PORTLET_NAME = 'Top 5 Performers of the Week');


UPDATE DSHBRD_UIB_PTLT_DTL
   SET uib_view_type_id =
          (SELECT UIB_VIEW_TYPE_ID
             FROM UIB_VIEW_TYPE
            WHERE VIEW_TYPE = 'Grid')
 WHERE DSHBRD_PORTLET_ID IN
          (SELECT DSHBRD_PORTLET_ID
             FROM DSHBRD_PORTLET
            WHERE PORTLET_NAME = 'Bottom 5 Performers of the Week');

   END IF;
END;
/
			
COMMIT;			

-- DBTicket LM-10157

UPDATE UIB_QUERY_DETAILS
   SET QUERY_TABLES =
          '(select EMP_ID, qa_totals_date,whse,FN_DATESTRING_DATEPART(qa_totals_date,whse) cd,QA_TOTAL_QTY,QA_TOTAL_FAIL_QTY
    from e_qa_smry) EQS'
WHERE UIB_QUERY_ID in (SELECT UIB_QUERY_ID
                         FROM UIB_QUERY
                        WHERE QUERY_NAME = 'Quality_By_Employee');

COMMIT;

-- DBTicket LM-10159

UPDATE UIB_QUERY_DETAILS
   SET QUERY_COLUMNS =
          'cd, ROUND(case when sum(total_nonstdlabor_time) != 0.0 then cast(sum(total_pam) as FLOAT)/cast(sum(total_nonstdlabor_time)as FLOAT)else 0.0 end,2)  as EP',
       QUERY_TABLES =
          '(select EMP_ID,  CLOCK_IN_DATE, whse, FN_DATESTRING_DATEPART(clock_in_date,whse) cd,  total_pam, total_nonstdlabor_time from e_emp_perf_smry) EPS',
       QUERY_WHERE_CLAUSE =
          'EMP_ID in (select emp_id from latest_emp_dtl where spvsr_emp_id = :session.wt_loggedInUserId.isInteger ) and CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE) Group by cd order by cd'
WHERE UIB_QUERY_ID in (SELECT UIB_QUERY_ID
                         FROM UIB_QUERY
                        WHERE QUERY_NAME = 'Performance_By_Employee');

UPDATE UIB_QUERY_DETAILS
   SET QUERY_COLUMNS =
          'cd,  case when sum(QA_TOTAL_QTY) != 0.0 then cast(ROUND(100.0*(sum( cast(QA_TOTAL_QTY - QA_TOTAL_FAIL_QTY as FLOAT))/sum(cast(QA_TOTAL_QTY as FLOAT))), 2 ) as FLOAT) else 100.0 end as QA',
       QUERY_TABLES =
          '(select EMP_ID, qa_totals_date,whse,FN_DATESTRING_DATEPART(qa_totals_date,whse) cd,QA_TOTAL_QTY,QA_TOTAL_FAIL_QTY from e_qa_smry) EQS',
       QUERY_WHERE_CLAUSE =
          'emp_id in (select emp_id from latest_emp_dtl where spvsr_emp_id = :session.wt_loggedInUserId.isInteger ) and  qa_totals_date >= FN_DATEADD (''DAY'', -7, current_timestamp, EQS.WHSE) Group by cd order by cd'
WHERE UIB_QUERY_ID in (SELECT UIB_QUERY_ID
                         FROM UIB_QUERY
                        WHERE QUERY_NAME = 'Quality_By_Employee');
						
commit;

-- DBTicket LM-10161

UPDATE UIB_QUERY_DETAILS
   SET QUERY_COLUMNS =
          'cd, case when sum(UTIL_DENOM) != 0.0 then cast(ROUND(100.0*(sum( cast(UTIL_NUM as FLOAT))/sum(cast(UTIL_DENOM as FLOAT))), 2 ) as FLOAT) else 0.0 end  as UTIL',
       QUERY_TABLES =
          '(select EMP_ID,  CLOCK_IN_DATE, whse, FN_DATESTRING_DATEPART(clock_in_date,whse) cd,  UTIL_NUM, UTIL_DENOM from e_emp_perf_smry) EPS',
       QUERY_WHERE_CLAUSE =
          'EMP_ID in (select emp_id from latest_emp_dtl where spvsr_emp_id = :session.wt_loggedInUserId.isInteger ) and CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE) Group by cd order by cd'
 WHERE UIB_QUERY_ID in (SELECT UIB_QUERY_ID
                         FROM UIB_QUERY
                        WHERE QUERY_NAME = 'Utilization_By_Employee');

COMMIT;

-- DBTicket LM-10234

UPDATE UIB_QUERY_DETAILS
   SET QUERY_COLUMNS =
          'cd, ROUND(100.0* case when sum(total_nonstdlabor_time) != 0.0 then cast(sum(total_pam) as FLOAT)/cast(sum(total_nonstdlabor_time)as FLOAT)else 0.0 end,2)  as EP'
 WHERE UIB_QUERY_ID IN (SELECT UIB_QUERY_ID
                          FROM UIB_QUERY
                         WHERE QUERY_NAME = 'Performance_By_Employee');

COMMIT;


-- DBTicket LM-10353

DECLARE
   v_count   NUMBER;
BEGIN
   select count(1) INTO v_count  from company where company_name = '01';

   IF v_count = 1
   THEN
      UPDATE UIB_QUERY_DETAILS
         SET QUERY_COLUMNS =
                'cd, case when sum(UTIL_DENOM) != 0.0 then CAST(ROUND(100.0*(SUM( CAST(UTIL_NUM AS FLOAT))/SUM(CAST(UTIL_DENOM AS FLOAT))),2) AS DECIMAL(6,2)) else 0.0 end  as UTIL'
       WHERE UIB_QUERY_ID IN (SELECT uib_query_id
                                FROM UIB_QUERY
                               WHERE query_name = 'Utilization_By_Employee');

      UPDATE UIB_QUERY_DETAILS
         SET QUERY_COLUMNS =
                'cd,  case when sum(QA_TOTAL_QTY) != 0.0 then cast(ROUND(100.0*(sum( cast(QA_TOTAL_QTY - QA_TOTAL_FAIL_QTY as FLOAT))/sum(cast(QA_TOTAL_QTY as FLOAT))), 2 ) as DECIMAL(6,2)) else 100.0 end as QA'
       WHERE UIB_QUERY_ID IN (SELECT uib_query_id
                                FROM UIB_QUERY
                               WHERE query_name = 'Quality_By_Employee');
   END IF;
END;
/
COMMIT;


-- DBTicket LM-10357

DECLARE
   v_count   NUMBER;
BEGIN
   select count(1) INTO v_count  from company where company_name = '01';

   IF v_count = 1
   THEN
      UPDATE UIB_QUERY_DETAILS
         SET QUERY_COLUMNS =
                'cd, CAST(ROUND(100.0* case when sum(total_nonstdlabor_time) != 0.0 then cast(sum(total_pam) as FLOAT)/cast(sum(total_nonstdlabor_time)as FLOAT)else 0.0 end,2) as DECIMAL(6,2))  as EP'
       WHERE UIB_QUERY_ID IN (SELECT uib_query_id
                                FROM UIB_QUERY
                               WHERE query_name = 'Performance_By_Employee');
   END IF;
END;
/
COMMIT;

-- DBTicket LM-10399

UPDATE
  UIB_QUERY_DETAILS
SET
  QUERY_WHERE_CLAUSE = 'led.spvsr_emp_id = :session.wt_loggedInUserId.isInteger AND EPS.CLOCK_OUT_DATE IS NOT NULL and EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE) group by led.EMP_ID, uu.USER_LAST_NAME, uu.USER_FIRST_NAME) A order by EPP desc) where rownum <=5'
WHERE
  UIB_QUERY_ID IN
  (
    SELECT
      uib_query_id
    FROM
      UIB_QUERY
    WHERE
      query_name = 'LM_Top_5_Performances_Under_Supervisor'
  );
  
UPDATE
  UIB_QUERY_DETAILS
SET
  QUERY_WHERE_CLAUSE = 'led.spvsr_emp_id = :session.wt_loggedInUserId.isInteger AND EPS.CLOCK_OUT_DATE IS NOT NULL and EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE) group by led.EMP_ID, uu.USER_LAST_NAME, uu.USER_FIRST_NAME) A order by EPP asc) where rownum <=5'
WHERE
  UIB_QUERY_ID IN
  (
    SELECT
      uib_query_id
    FROM
      UIB_QUERY
    WHERE
      query_name = 'LM_Bottom_5_Performances_Under_Supervisor'
  ); 
  
commit;

-- DBTicket LM-10700 REFL

UPDATE uib_query_details
   SET query_where_clause =
          'led.spvsr_emp_id = :session.wt_loggedInUserId.isInteger AND led.emp_stat_id in(1) and EPS.CLOCK_OUT_DATE IS NOT NULL and EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE) group by led.EMP_ID, uu.USER_LAST_NAME, uu.USER_FIRST_NAME) A order by EPP desc) where rownum <=5'
 WHERE uib_query_id IN
          (SELECT uib_query_id
             FROM uib_query
            WHERE query_name = 'LM_Top_5_Performances_Under_Supervisor');

UPDATE uib_query_details
   SET query_where_clause =
          'led.spvsr_emp_id = :session.wt_loggedInUserId.isInteger AND led.emp_stat_id in(1) and EPS.CLOCK_OUT_DATE IS NOT NULL and EPS.CLOCK_IN_DATE >= FN_DATEADD (''DAY'', -7, current_timestamp, EPS.WHSE) group by led.EMP_ID, uu.USER_LAST_NAME, uu.USER_FIRST_NAME) A order by EPP asc) where rownum <=5'
 WHERE uib_query_id IN
          (SELECT uib_query_id
             FROM uib_query
            WHERE query_name = 'LM_Bottom_5_Performances_Under_Supervisor');

			
COMMIT;

-- DBTicket LM-10964
MERGE INTO sys_code A
     USING (SELECT 'L' AS REC_TYPE, '142' AS CODE_TYPE, '2015' AS CODE_ID
              FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
CODE_TYPE,
CODE_ID,
CODE_DESC,
SHORT_DESC,
MISC_FLAGS,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
SYS_CODE_ID,
SYS_CODE_TYPE_ID) values
('L','142','2015','2015','Ver 15','Version 2015',sysdate,sysdate,'LMADMIN',1,sys_code_id_seq.nextval,(select sys_code_type_id from sys_code_type where rec_type='L' and code_type='142'));

				 
MERGE INTO sys_code A
     USING (SELECT 'L' AS REC_TYPE, '067' AS CODE_TYPE, '2015' AS CODE_ID
              FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
CODE_TYPE,
CODE_ID,
CODE_DESC,
SHORT_DESC,
MISC_FLAGS,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
SYS_CODE_ID,
SYS_CODE_TYPE_ID) 
values
('L','067','2015','Version 2015','2015','15',sysdate,sysdate,'LMADMIN',1,sys_code_id_seq.nextval,(select sys_code_type_id from sys_code_type where rec_type='L' and code_type='067'));		 

commit;

-- DBTicket LM-10991

UPDATE SYS_CODE_TYPE SET CODE_DESC = 'Monitoring Ref code mapping' WHERE REC_TYPE = 'L' AND CODE_TYPE = '122';
MERGE INTO SYS_CODE A USING (SELECT 'L' REC_TYPE, '122' CODE_TYPE, '07' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('L', '122', '07', 'CARTON_NBR', 'CARTON_NBR','10',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'L' AND CODE_TYPE = '122'));
MERGE INTO SYS_CODE A USING (SELECT 'L' REC_TYPE, '122' CODE_TYPE, '08' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('L', '122', '08', 'CASE_NBR', 'CASE_NBR','42',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'L' AND CODE_TYPE = '122'));
UPDATE SYS_CODE SET MISC_FLAGS = '32' WHERE REC_TYPE = 'L' AND CODE_TYPE = '122' AND CODE_ID = '04';
UPDATE SYS_CODE SET MISC_FLAGS = '34' WHERE REC_TYPE = 'L' AND CODE_TYPE = '122' AND CODE_ID = '01';
UPDATE SYS_CODE SET MISC_FLAGS = '02' WHERE REC_TYPE = 'L' AND CODE_TYPE = '122' AND CODE_ID = '02';
MERGE INTO SYS_CODE_PARM SCP USING (SELECT 'L' REC_TYPE, '122' CODE_TYPE , '1' FROM_POSN FROM DUAL) D ON ( SCP.REC_TYPE = D.REC_TYPE AND SCP.CODE_TYPE = D.CODE_TYPE  AND SCP.FROM_POSN = D.FROM_POSN) WHEN NOT MATCHED THEN INSERT (REC_TYPE, CODE_TYPE, FROM_POSN, TO_POSN, MISC_FLAG_DESC, LITRL, MANDT_FLAG, VALID_CODE, VALID_SYS_CODE_REC_TYPE, VALID_SYS_CODE_TYPE, VALID_FROM, VALID_TO, VALID_VALUES, EDIT_STYLE, CREATE_DATE_TIME, MOD_DATE_TIME, USER_ID, WM_VERSION_ID,SYS_CODE_PARM_ID, SYS_CODE_TYPE_ID, VALID_SYS_CODE_TYPE_ID)VALUES ('L', '122', '1', '2', 'WM Ref Code', '', 'N', 'A', '', '','','',null,'EDIT', SYSDATE, SYSDATE, 'SEED', 1, SYS_CODE_PARM_ID_SEQ.NEXTVAL, (SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'L' AND CODE_TYPE = '122'),null);
commit;

-- DBTicket LM-11161
MERGE INTO sys_code A
     USING (SELECT 'L' AS REC_TYPE, '084' AS CODE_TYPE, '20' AS CODE_ID
              FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_ID,
               CODE_DESC,
               SHORT_DESC,
               MISC_FLAGS,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('L',
               '084',
               '20',
               'In Queue',
               'In Queue',
               '',
               SYSDATE,
               SYSDATE,
               'LMADMIN',
               1,
               sys_code_id_seq.NEXTVAL,
               (SELECT sys_code_type_id
                  FROM sys_code_type
                 WHERE rec_type = 'L' AND code_type = '084'));
			 
commit; 

-- DBTicket LM-11342
UPDATE sys_code
   SET code_desc = 'Pallet Id', short_desc = 'Pallet Id'
 WHERE rec_type = 'L' AND code_type = '122' AND code_id = 06;

UPDATE sys_code
   SET code_desc = 'Pickticket', short_desc = 'Pickticket'
 WHERE rec_type = 'L' AND code_type = '122' AND code_id = 02;

commit;

-- DBTicket LM-11322


INSERT INTO SYS_CODE_TYPE (REC_TYPE,
                           CODE_TYPE,
                           CODE_DESC,
                           SHORT_DESC,
                           MAX_CODE_ID_LEN,
                           ALLOW_ADD,
                           ALLOW_DEL,
                           CHG_MISC_FLAG,
                           CHG_DTL_DESC,
                           WHSE_DEPNDT,
                           CREATE_DATE_TIME,
                           MOD_DATE_TIME,
                           USER_ID,
                           SYS_CODE_TYPE_ID)
     VALUES ('L',
             '163',
             'Matching fields from Monitoring',
             'MatchFlds',
             50,
             'Y',
             'Y',
             'Y',
             'Y',
             '0',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             SYS_CODE_TYPE_ID_SEQ.NEXTVAL);

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '163',
             'taskNbr',
             'TASK_NBR',
             'task Nbr',
             'Y',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '163'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '163',
             'actName',
             'ACT_NAME',
             'task Nbr',
             'Y',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '163'));
               
INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '163',
             'caseNbr',
             'CASE_NBR',
             'case Nbr',
             'N',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '163'));  
			   

INSERT
INTO SYS_CODE_PARM
  (
    REC_TYPE,    CODE_TYPE,    FROM_POSN,    TO_POSN,    MISC_FLAG_DESC,    LITRL,    MANDT_FLAG,    VALID_CODE,    VALID_SYS_CODE_REC_TYPE,    VALID_SYS_CODE_TYPE,    VALID_FROM,
    VALID_TO,    VALID_VALUES,    EDIT_STYLE,    CREATE_DATE_TIME,    MOD_DATE_TIME,    USER_ID,    WM_VERSION_ID,    SYS_CODE_PARM_ID,    SYS_CODE_TYPE_ID,    VALID_SYS_CODE_TYPE_ID
      )
  VALUES
  (
    'L','163', 1,1,'Matched',null,'Y', 'A', null, null, null, null, null,  'CKBX', sysdate, sysdate, 'LMUSER',1 , SYS_CODE_PARM_ID_SEQ.NEXTVAL, (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '163'), null );
			   

COMMIT ;

-- DBTicket LM-11391

UPDATE DSHBRD_PORTLET
   SET SUPPORT_MOBILE = 1
 WHERE PORTLET_NAME IN
          ('Performance % for the Week',
           'Utilization % for the Week',
           'Quality Audit % for the Week',
           'Number of Unapproved Indirect Labor for the Week',
           'Number of Interactions pending for the Week',
           'Top 5 Performers of the Week',
           'Bottom 5 Performers of the Week');

UPDATE DSHBRD_USER_TAB
   SET IS_MOBILE = 1
 WHERE TAB_NAME LIKE 'LM Supervisor Dashboard';
 
commit;

-- DBTicket LM-12109
update sys_code_type
   set max_code_id_len = 15
 where rec_type = 'L' and code_type = 123;

merge into sys_code l
     using (select 'L' rec_type, '123' code_type, 'STATUS' code_id from dual) b
        on (l.rec_type = b.rec_type and l.code_type = b.code_type and l.code_id = b.code_id)
when not matched
then
   insert     (rec_type,
               code_type,
               code_id,
               code_desc,
               short_desc,
               misc_flags,
               create_date_time,
               mod_date_time,
               user_id,
               wm_version_id,
               sys_code_id,
               sys_code_type_id,
               created_dttm,
               last_updated_dttm)
       values ('L',
               '123',
               'STATUS',
               'STATUS',
               'STATUS',
               'null',
               sysdate,
               sysdate,
               'LMWEB',
               1,
               sys_code_id_seq.nextval,
               (select (sys_code_type_id)
                  from sys_code_type
                 where rec_type = 'L' and code_type = '123'),
               sysdate,
               sysdate);
commit;

-- DBTicket DB-1348
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ((SELECT COMPANY_TYPE_ID
                FROM COMPANY_TYPE
               WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'LMAOBSMGMTQA'),
             (SELECT MAX (ROW_UID) + 4
                FROM COMPANY_TYPE_PERM)
            );
commit;	

-- DBTicket DB-1278
MERGE INTO company_type_perm ctp
     USING (SELECT company_type.company_type_id,
                   company_type.description,
                   permission.permission_id,
                   permission.permission_code
              FROM company_type, permission
             WHERE LOWER (company_type.description) = LOWER ('Shipper')
                   AND permission.permission_code = 'LMVOBSMGMTQA') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES ( (SELECT company_type_id
                   FROM company_type
                  WHERE LOWER (description) = LOWER ('Shipper')),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'LMVOBSMGMTQA'),
               (SELECT MAX (row_uid) + 4 FROM company_type_perm));
COMMIT;

-- DBTicket DB-2123
INSERT INTO SYS_CODE (REC_TYPE,CODE_TYPE,CODE_ID,CODE_DESC,SHORT_DESC,MISC_FLAGS,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_ID,SYS_CODE_TYPE_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
VALUES('L','078','3','Evaluation EP% Period','EvalEP','3',sysdate,sysdate,'LMADMIN',1,sys_code_id_seq.nextval,(select sys_code_type_id from sys_code_type where rec_type='L' and code_type='078'), sysdate, sysdate); 

commit;

-- DBTicket DB-2473
INSERT INTO SYS_CODE_TYPE (REC_TYPE,
                           CODE_TYPE,
                           CODE_DESC,
                           SHORT_DESC,
                           MAX_CODE_ID_LEN,
                           ALLOW_ADD,
                           ALLOW_DEL,
                           CHG_MISC_FLAG,
                           CHG_DTL_DESC,
                           WHSE_DEPNDT,
                           CREATE_DATE_TIME,
                           MOD_DATE_TIME,
                           USER_ID,
                           SYS_CODE_TYPE_ID)
     VALUES ('L',
             '167',
             'Interaction Management Status Filter',
             'Status',
             1,
             'Y',
             'Y',
             'Y',
             'Y',
             '0',
             current_timestamp,
             current_timestamp,
             'LMWEB',
             SYS_CODE_TYPE_ID_SEQ.NEXTVAL);


INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '167',
             'N',
             'Pending',
             'Pending',
             '',
             current_timestamp,
             current_timestamp,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '167'));


INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '167',
             'Y',
             'Completed',
             'Complete',
             '',
             current_timestamp,
             current_timestamp,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '167'));
               
COMMIT;               

-- DBTicket DB-2474 REFL
INSERT INTO permission (permission_id,
                        permission_name,
                        is_active,
                        company_id,
                        permission_code)
     VALUES (seq_permission_id.NEXTVAL,
             'Interaction Management - Delete',
             1,
             -1,
             'LMDOBSMGMTTRAN');
			 
COMMIT;

-- DBTicket DB-2613

MERGE INTO SYS_CODE A USING (SELECT 'L' REC_TYPE, '029' CODE_TYPE, 'M' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) 
WHEN NOT MATCHED THEN INSERT (A.REC_TYPE, A.CODE_TYPE, A.CODE_ID, A.CODE_DESC, A.SHORT_DESC, A.MISC_FLAGS, A.CREATE_DATE_TIME, A.MOD_DATE_TIME, A.USER_ID, A.WM_VERSION_ID, A.SYS_CODE_ID, A.SYS_CODE_TYPE_ID) 
values ('L', '029', 'M', 'Miscellaneous', 'Misc','',SYSDATE,SYSDATE,'LMWEB',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'L' AND CODE_TYPE = '029'));

commit;

-- DBTicket DB-2689 REFL
INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMDOBSMGMTTRAN'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

COMMIT;

-- DBTicket DB-2637
MERGE INTO SYS_CODE_TYPE A
     USING (SELECT 'L' REC_TYPE, '169' CODE_TYPE FROM DUAL) B
        ON (A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_DESC,
               SHORT_DESC,
               MAX_CODE_ID_LEN,
               ALLOW_ADD,
               ALLOW_DEL,
               CHG_MISC_FLAG,
               CHG_DTL_DESC,
               WHSE_DEPNDT,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('L',
               '169',
               'Questions Order',
               'QSOrder',
               1,
               'Y',
               'Y',
               'Y',
               'Y',
               '0',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'LMWEB',
               SYS_CODE_TYPE_ID_SEQ.NEXTVAL);

MERGE INTO SYS_CODE A
     USING (SELECT 'L' REC_TYPE, '169' CODE_TYPE, '1' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_ID,
               CODE_DESC,
               SHORT_DESC,
               MISC_FLAGS,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('L',
               '169',
               '1',
               'By Interaction Type',
               'IntType',
               NULL,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'LMWEB',
               1,
               SYS_CODE_ID_SEQ.NEXTVAL,
               (SELECT (SYS_CODE_TYPE_ID)
                  FROM SYS_CODE_TYPE
                 WHERE REC_TYPE = 'L' AND CODE_TYPE = '169'));

MERGE INTO SYS_CODE A
     USING (SELECT 'L' REC_TYPE, '169' CODE_TYPE, '2' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_ID,
               CODE_DESC,
               SHORT_DESC,
               MISC_FLAGS,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('L',
               '169',
               '2',
               'By Functional Area',
               'FuncArea',
               NULL,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'LMWEB',
               1,
               SYS_CODE_ID_SEQ.NEXTVAL,
               (SELECT (SYS_CODE_TYPE_ID)
                  FROM SYS_CODE_TYPE
                 WHERE REC_TYPE = 'L' AND CODE_TYPE = '169'));

COMMIT;

-- DBTicket DB-3101 REFL
INSERT INTO permission (PERMISSION_ID,
                        PERMISSION_NAME,
                        IS_ACTIVE,
                        COMPANY_ID,
                        PERMISSION_CODE,
                        CREATED_SOURCE_TYPE_ID,
                        CREATED_SOURCE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE_TYPE_ID,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_DTTM,
                        DESCRIPTION)
     VALUES (SEQ_PERMISSION_ID.NEXTVAL,
             'RF Change Password',
             1,
             -1,
             'WMARFCHGPWD',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             'Allows the user to change the password');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMARFCHGPWD') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4
                  FROM APP_MOD_PERM));

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                         FROM APP
                        WHERE APP_NAME = 'Warehouse Management'),
                      (SELECT MODULE_ID
                         FROM MODULE
                        WHERE MODULE_CODE = 'SYSCTRL'),
                      (SELECT PERMISSION_ID
                         FROM PERMISSION
                        WHERE PERMISSION_CODE = 'WMARFCHGPWD'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'SYSCTRL'
                   AND PERMISSION.PERMISSION_CODE = 'WMARFCHGPWD') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4
                  FROM RELTP_APP_MOD_PERM));
				  
INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES (4,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'WMARFCHGPWD'),
             (SELECT MAX (row_uid) + 4
                FROM company_type_perm));
                
COMMIT;                
				
-- DBTicket DB-3107
INSERT INTO permission_tag (Tag_id,
                            description,
                            created_source_type_id,
                            created_source,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'ChangePassword',
             1,
             'Seed',
             1,
             'Seed');

INSERT INTO PERMISSION_TAG_MAPPING (PERMISSION_ID, TAG_ID)
     VALUES ( (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'WMARFCHGPWD'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'ChangePassword'));
               
COMMIT;               

-- DBTicket DB-3354 REFL
UPDATE ucl_user
   SET user_password =
          '013140CD0729275A5FDCED8C18E8F3CD8C9A11947C26097999DEE34071EA7B2881E43C7B054B13D2D84455B0C12D4F7C5BC8480327ED314E157BD2433530E098BC6F7917F2A5FF4F520AAB6AD74B6A3734600B66BB7D0B1CE99CDBC0701BF77311'
 WHERE user_name = 'LMADMIN';

COMMIT;

-- DBTicket DB-3353
INSERT INTO SYS_CODE_TYPE (REC_TYPE,
                           CODE_TYPE,
                           CODE_DESC,
                           SHORT_DESC,
                           MAX_CODE_ID_LEN,
                           ALLOW_ADD,
                           ALLOW_DEL,
                           CHG_MISC_FLAG,
                           CHG_DTL_DESC,
                           WHSE_DEPNDT,
                           CREATE_DATE_TIME,
                           MOD_DATE_TIME,
                           USER_ID,
                           SYS_CODE_TYPE_ID) 
      VALUES ('L',
             '168',
             'Sign off Status Filter',
             'Sign',
             1,
             'Y',
             'Y',
             'Y',
             'Y',
             '0',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             SYS_CODE_TYPE_ID_SEQ.NEXTVAL);
			 
			 
			 
INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '168',
             'Y',
             'Accept',
             'Accept',
             '',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '168'));
             
             
INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '168',
             'N',
             'Reject',
             'Reject',
             '',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '168'));

Insert into LRF_REPORT_DEF (REPORT_DEF_ID_O,DESCRIPTION,DBNAME,TYPE,VIEWPERM,EDITPERM,CATEGORY,SUBCATEGORY,REPORT_DEF_ID,REPORT_NAME,REPORT_FILE_NAME,PRIORITY,RESET_COUNT,RESET_INTERVAL,RETRY_COUNT,RETRY_INTERVAL) values (null,'Interaction Detail by Employee',null,'Jasper','LMVCONFRPT','LMACONFRPT','LM','Configuration',SEQ_LRF_REPORT_DEF_ID.nextVal,'Interaction Detail by Employee','InteractionDetailbyEmployee',-1,0,0,0,0);

Insert into LRF_REPORT_DEF_LAYOUT
   (REPORT_DEF_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, SEND_PARAMETER, REPORT_DEF_ID)
 Values
   (LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL, 0, 1, 'WHSE', 'Warehouse', '=', 'STRING', 'REPORT_WHSE', 0, 0, (SELECT REPORT_DEF_ID FROM LRF_REPORT_DEF WHERE REPORT_FILE_NAME='InteractionDetailbyEmployee'));
Insert into LRF_REPORT_DEF_LAYOUT
   (REPORT_DEF_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, OPERATOR_TYPE, IS_LOCALIZABLE, SEND_PARAMETER, REPORT_DEF_ID)
 Values
   (LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL, 1, 1, 'DATE_RANGE', 'Date/Time Range', 'BT', 'DATE', 'REPORT_DATE_BETWEEN', 0, 0, (SELECT REPORT_DEF_ID FROM LRF_REPORT_DEF WHERE REPORT_FILE_NAME='InteractionDetailbyEmployee'));
   
Insert into LRF_REPORT_DEF_LAYOUT
   (REPORT_DEF_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, IS_LOCALIZABLE, SEND_PARAMETER, REPORT_DEF_ID, CUST_VALID_CLASS)
 Values
   (LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL, 2, 1, 'EMP_LIST', 'Employee', 'IN', 'STRING', 'com.manh.els.systemctrl.finitevalue.EmployeeFV', 'REPORT_DUAL_LIST_ITEM', 0, 0, (SELECT REPORT_DEF_ID FROM LRF_REPORT_DEF WHERE REPORT_FILE_NAME='InteractionDetailbyEmployee'), 'com.manh.ils.reports.LMReportsDualListValidator');
   
Insert into LRF_REPORT_DEF_LAYOUT
   (REPORT_DEF_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, IS_LOCALIZABLE, SEND_PARAMETER, REPORT_DEF_ID)
 Values
   (LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL, 3, 1, 'OBSERVER', 'Observer', 'BT', 'STRING', 'com.manh.els.systemctrl.finitevalue.SupervisorUserIdFV', 'REPORT_SELECT_ITEM', 0, 0, (SELECT REPORT_DEF_ID FROM LRF_REPORT_DEF WHERE REPORT_FILE_NAME='InteractionDetailbyEmployee'));

Insert into LRF_REPORT_DEF_LAYOUT
   (REPORT_DEF_LAYOUT_ID, FIELD_POSITION, IS_REQUIRED, FIELD_NAME, FIELD_LABEL, FIELD_OPERATORS, FIELD_TYPE, VALUE_GENERATOR_CLASS, OPERATOR_TYPE, IS_LOCALIZABLE, SEND_PARAMETER, REPORT_DEF_ID)
 Values
   (LRF_REPORT_DEF_LAYOUT_ID_SEQ.NEXTVAL, 4, 0, 'SIGN_OF_STATUS', 'Sign Off Status', 'BT', 'STRING', 'com.manh.cbo.syscode.finitevalue.SignoffStatusFV', 'REPORT_SELECT_ITEM', 0, 0, (SELECT REPORT_DEF_ID FROM LRF_REPORT_DEF WHERE REPORT_FILE_NAME='InteractionDetailbyEmployee'));

commit;

-- DBTicket DB-3426 REFL
CALL sequpdt ('APP_MOD_PERM', 'ROW_UID', 'SEQ_APP_MOD_PERM');

INSERT INTO app_mod_perm (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT app_id
                 FROM app
                WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMDOBSMGMTTRAN'),
             SEQ_APP_MOD_PERM.NEXTVAL);

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'LMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
        INSERT INTO role_app_mod_perm (ROLE_ID,
                                       APP_ID,
                                       MODULE_ID,
                                       PERMISSION_ID,
                                       ROW_UID)
             VALUES ( (SELECT role_id
                         FROM role
                        WHERE role_name = 'LMROLE' and COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01')),
                     (SELECT app_id
                        FROM app
                       WHERE app_name = 'Labor Management'),
                     (SELECT module_id
                        FROM module
                       WHERE module_code = 'LMOBSMGMT'),
                     (SELECT permission_id
                        FROM permission
                       WHERE permission_code = 'LMDOBSMGMTTRAN'),
                     SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
       END IF;
END;
/

INSERT INTO reltp_app_mod_perm (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES ( (SELECT relationship_type_id
                 FROM relationship_type
                WHERE description = 'Business Partner'),
             (SELECT app_id
                FROM app
               WHERE app_name = 'Labor Management'),
             (SELECT module_id
                FROM module
               WHERE module_code = 'LMOBSMGMT'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'LMDOBSMGMTTRAN'),
             (SELECT NVL (MAX (ROW_UID), 0) + 4
                FROM RELTP_APP_MOD_PERM),
             0);

COMMIT;

-- DBTicket DB-3533 REFL
INSERT INTO permission (permission_id,
                        permission_name,
                        is_active,
                        company_id,
                        permission_code)
     VALUES (seq_permission_id.NEXTVAL,
             'View WMi Item Inventory',
             1,
             -1,
             'EXTWMVITEMINVN');

INSERT INTO permission (permission_id,
                        permission_name,
                        is_active,
                        company_id,
                        permission_code)
     VALUES (seq_permission_id.NEXTVAL,
             'View WMi Pick Locations',
             1,
             -1,
             'VEXTWMPKLOC');

INSERT INTO permission (permission_id,
                        permission_name,
                        is_active,
                        company_id,
                        permission_code)
     VALUES (seq_permission_id.NEXTVAL,
             'View WMi Reserve Locations',
             1,
             -1,
             'VEXTWMRSLOC');

INSERT INTO company_type_perm (company_type_id, permission_id, row_uid)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'EXTWMVITEMINVN'),
             (SELECT MAX (row_uid) + 4 FROM company_type_perm));

INSERT INTO APP_MODULE (APP_ID,
                        MODULE_ID,
                        IS_ACTIVE,
                        ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_SHORT_NAME = 'LM'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'INVNMGMT'),
             1,
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO APP_MOD_PERM (APP_ID,
                          MODULE_ID,
                          PERMISSION_ID,
                          ROW_UID)
     VALUES ( (SELECT APP_ID
                 FROM APP
                WHERE APP_SHORT_NAME = 'LM'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'INVNMGMT'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'EXTWMVITEMINVN'),
             SEQ_APP_MOD_PERM.NEXTVAL);
COMMIT;

-- DBTicket DB-3827
MERGE INTO SYS_CODE A
     USING (SELECT 'L' REC_TYPE, '165' CODE_TYPE, '3' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN MATCHED
THEN
   UPDATE SET
      a.code_desc =
         'At the later of shift end or event end plus clean-up time';

MERGE INTO SYS_CODE A
     USING (SELECT 'L' REC_TYPE, '166' CODE_TYPE, '062' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN MATCHED
THEN
   UPDATE SET
      a.code_desc = 'Missed CO; Auto-CO at (last event end + clean-up)';

COMMIT;

-- DBTicket DB-3915

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '166',
             '111',
             'Events Overlap (Different Start). Earlier Event auto-ended',
             'EvntOvrlap',
             'EvntOvrlap',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '166'));
			   
			   
			   
INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '166',
             '112',
             'Events Overlap (Same Start). Shorter Event Rejected',
             'EvntOvrlap',
             'EvntOvrlap',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '166'));
			   
			   
			   
INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '166',
             '113',
             'Events Overlap (Same Start/End). Later Created Event Rejected',
             'EvntOvrlap',
             'EvntOvrlap',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '166'));
			   
commit;

-- DBTicket DB-3923 REFL

update permission set permission_name='Interaction Management Email Template  - Admin' where permission_code='LMAOBSEMAILCNFG';
update permission set permission_name='Interaction Management Email Template  - View' where permission_code='LMVOBSEMAILCNFG';
update permission set permission_name='Interaction Management  - Admin' where permission_code='LMAOBSMGMTTRAN';
update permission set permission_name='Interaction Management  - View' where permission_code='LMVOBSMGMTTRAN';
update permission set permission_name='Interaction Management Question Answer - Admin' where permission_code='LMAOBSMGMTQA';
update permission set permission_name='Interaction Management Question Answer - View ' where permission_code='LMVOBSMGMTQA';
update permission set permission_name='Interaction Management Interaction Type - Admin ' where permission_code='LMAOBSINTCNFG';
update permission set permission_name='Interaction Management Interaction Type - View ' where permission_code='LMVOBSINTCNFG';
update permission set permission_name='Interaction Management Coaching Type - Admin' where permission_code='LMAOBSCOACNFG';
update permission set permission_name='Interaction Management Coaching Type - View' where permission_code='LMVOBSCOACNFG';

commit;

-- DBTicket DB-3986

update LRF_REPORT_DEF_LAYOUT set IS_REQUIRED=0 where field_name='OBSERVER' and report_def_id = (SELECT REPORT_DEF_ID FROM LRF_REPORT_DEF WHERE REPORT_FILE_NAME='InteractionDetailbyEmployee');

commit;


-- DBTicket LM-14135
MERGE INTO sys_code a
     USING (SELECT 'L' rec_type, '166' code_type, '065' code_id FROM DUAL) b
        ON (    a.rec_type = b.rec_type
            AND a.code_type = b.code_type
            AND a.code_id = b.code_id)
WHEN NOT MATCHED
THEN
   INSERT  (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '166',
             '065',
             'Missed CO; Auto CO at Auto CICO Threshold',
             'MissedCO',
             '',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '166')) ;
               
commit ;

-- DBTicket DB-4633
INSERT INTO SYS_CODE_TYPE (REC_TYPE,
                           CODE_TYPE,
                           CODE_DESC,
                           SHORT_DESC,
                           MAX_CODE_ID_LEN,
                           ALLOW_ADD,
                           ALLOW_DEL,
                           CHG_MISC_FLAG,
                           CHG_DTL_DESC,
                           WHSE_DEPNDT,
                           CREATE_DATE_TIME,
                           MOD_DATE_TIME,
                           USER_ID,
                           SYS_CODE_TYPE_ID)
     VALUES ('L',
             '170',
             'Data Management Auto CI Options',
             'CI-Opt',
             1,
             'Y',
             'Y',
             'Y',
             'Y',
             '0',
             current_timestamp,
             current_timestamp,
             'LMWEB',
             SYS_CODE_TYPE_ID_SEQ.NEXTVAL);

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '170',
             '0',
             'At event start',
             'evStart',
             '',
             current_timestamp,
             current_timestamp,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '170'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '170',
             '1',
             'At event start minus start-up time',
             'evSt-str',
             '',
             current_timestamp,
             current_timestamp,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '170'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '170',
             '2',
             'At the earlier of shift start or event start',
             'ev-sh-St',
             '',
             current_timestamp,
             current_timestamp,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '170'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
        VALUES (
                  'L',
                  '170',
                  '3',
                  'At the earlier of shift start or event start minus start-up time',
                  'sh-evSt-St',
                  '',
                  current_timestamp,
                  current_timestamp,
                  'LMWEB',
                  1,
                  SYS_CODE_ID_SEQ.NEXTVAL,
                  (SELECT (SYS_CODE_TYPE_ID)
                     FROM SYS_CODE_TYPE
                    WHERE REC_TYPE = 'L' AND CODE_TYPE = '170'));

DELETE FROM sys_code
      WHERE sys_code_type_id IN
               (SELECT (SYS_CODE_TYPE_ID)
                  FROM SYS_CODE_TYPE
                 WHERE     REC_TYPE = 'L'
                       AND CODE_TYPE = '164'
                       AND SHORT_DESC = 'CI-Opt');

DELETE FROM sys_code_type
      WHERE rec_type = 'L' AND code_type = 164 AND SHORT_DESC = 'CI-Opt';
      
COMMIT;

-- DBTicket DB-4657
INSERT INTO SYS_CODE_TYPE (REC_TYPE,
                           CODE_TYPE,
                           CODE_DESC,
                           SHORT_DESC,
                           MAX_CODE_ID_LEN,
                           ALLOW_ADD,
                           ALLOW_DEL,
                           CHG_MISC_FLAG,
                           CHG_DTL_DESC,
                           WHSE_DEPNDT,
                           CREATE_DATE_TIME,
                           MOD_DATE_TIME,
                           USER_ID,
                           SYS_CODE_TYPE_ID)
     VALUES ('L',
             '171',
             'Event Authorization Filter Options',
             'fltrOptns',
             50,
             'Y',
             'Y',
             'Y',
             'Y',
             '0',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             SYS_CODE_TYPE_ID_SEQ.NEXTVAL);      


INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '171',
             '4',
             'Select All',
             'Select All',
             'Select All',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '171'));	



INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '171',
             '1',
             'Pending Approval',
             'P. App',
             'P. App',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '171'));

INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '171',
             '2',
             'Approved',
             'App',
             'App',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '171'));
			   
			   
INSERT INTO SYS_CODE (REC_TYPE,
                      CODE_TYPE,
                      CODE_ID,
                      CODE_DESC,
                      SHORT_DESC,
                      MISC_FLAGS,
                      CREATE_DATE_TIME,
                      MOD_DATE_TIME,
                      USER_ID,
                      WM_VERSION_ID,
                      SYS_CODE_ID,
                      SYS_CODE_TYPE_ID)
     VALUES ('L',
             '171',
             '3',
             'Rejected',
             'Rej',
             'Rej',
             SYSDATE,
             SYSDATE,
             'LMWEB',
             1,
             SYS_CODE_ID_SEQ.NEXTVAL,
             (SELECT (SYS_CODE_TYPE_ID)
                FROM SYS_CODE_TYPE
               WHERE REC_TYPE = 'L' AND CODE_TYPE = '171'));
               
COMMIT;

-- DBTicket DB-4470 REFL
UPDATE module
   SET module_name = 'Interaction Management'
 WHERE module_code = 'LMOBSMGMT' AND module_name = 'Observation Management';

COMMIT;

-- DBTicket DB-5504
MERGE INTO SYS_CODE A
     USING (SELECT 'L' REC_TYPE, '166' CODE_TYPE, '114' CODE_ID FROM DUAL) B
        ON (    A.REC_TYPE = B.REC_TYPE
            AND A.CODE_TYPE = B.CODE_TYPE
            AND A.CODE_ID = B.CODE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (A.rec_type,
               A.code_type,
               A.code_id,
               A.code_desc,
               A.short_desc,
               A.misc_flags,
               A.create_date_time,
               A.mod_date_time,
               A.user_id,
               A.wm_version_id,
               A.sys_code_id,
               A.sys_code_type_id)
       VALUES (
                 'L',
                 '166',
                 '114',
                 'Events Overlap (Event completely overlapping). Earlier Event auto-ended',
                 'EvntOvrlap',
                 'EvntOvrlap',
                 CURRENT_TIMESTAMP,
                 CURRENT_TIMESTAMP,
                 'LMWEB',
                 1,
                 SYS_CODE_ID_SEQ.NEXTVAL,
                 (SELECT SYS_CODE_TYPE_ID
                    FROM SYS_CODE_TYPE
                   WHERE REC_TYPE = 'L' AND CODE_TYPE = '166'));

COMMIT;

-- DBTicket DB-5591

CALL SEQUPDT('SYS_CODE','sys_code_id','SYS_CODE_ID_SEQ');

MERGE INTO SYS_CODE A USING (SELECT 'L' REC_TYPE, '166' CODE_TYPE, '104' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('L', '166', '104', 'Missed Event End; Event auto-ended due to next Event start', 'MisEvntEnd','',SYSDATE,SYSDATE,'LMWEB',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'L' AND CODE_TYPE = '166'));

COMMIT;

-- DBTicket DB-5800 REFL
MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Labor Management'
                   AND MODULE.MODULE_CODE = 'TRANS'
                   AND PERMISSION.PERMISSION_CODE = 'ASCERB') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Labor Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'TRANS'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ASCERB'),
               SEQ_APP_MOD_PERM.NEXTVAL);


MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Labor Management'
                   AND MODULE.MODULE_CODE = 'TRANS'
                   AND PERMISSION.PERMISSION_CODE = 'ANXTUPNBRB') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Labor Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'TRANS'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ANXTUPNBRB'),
               SEQ_APP_MOD_PERM.NEXTVAL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Labor Management'
                   AND MODULE.MODULE_CODE = 'TRANS'
                   AND PERMISSION.PERMISSION_CODE = 'POSTMSG') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Labor Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'MIF'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'POSTMSG'),
               SEQ_APP_MOD_PERM.NEXTVAL);

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'LMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm ramp
           USING (SELECT amp.app_id app_id,
                         amp.module_id module_id,
                         p.permission_id permission_id
                    FROM app_mod_perm amp, PERMISSION p
                   WHERE     app_id = (SELECT app_id
                                         FROM app
                                        WHERE app_name = 'Labor Management')
                         AND p.permission_id = amp.permission_id
                         AND module_id = (SELECT module_id
                                            FROM module
                                           WHERE module_code = 'TRANS')
                         AND p.permission_id =
                                (SELECT PERMISSION_ID
                                   FROM PERMISSION
                                  WHERE PERMISSION_CODE = 'ASCERB')) APP1
              ON (    ramp.app_id = app1.app_id
                  AND ramp.module_id = app1.module_id
                  AND ramp.permission_id = app1.permission_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ramp.role_id,
                     ramp.app_id,
                     ramp.module_id,
                     ramp.permission_id,
                     ramp.row_uid)
             VALUES (
                       (SELECT role_id
                          FROM role
                         WHERE     role_name = 'LMROLE'
                               AND COMPANY_ID IN (SELECT COMPANY_ID
                                                    FROM COMPANY
                                                   WHERE COMPANY_NAME = '01')),
                       app1.app_id,
                       APP1.MODULE_ID,
                       app1.permission_id,
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'LMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm ramp
           USING (SELECT amp.app_id app_id,
                         amp.module_id module_id,
                         p.permission_id permission_id
                    FROM app_mod_perm amp, PERMISSION p
                   WHERE     app_id = (SELECT app_id
                                         FROM app
                                        WHERE app_name = 'Labor Management')
                         AND p.permission_id = amp.permission_id
                         AND module_id = (SELECT module_id
                                            FROM module
                                           WHERE module_code = 'TRANS')
                         AND p.permission_id =
                                (SELECT PERMISSION_ID
                                   FROM PERMISSION
                                  WHERE PERMISSION_CODE = 'ANXTUPNBRB')) APP1
              ON (    ramp.app_id = app1.app_id
                  AND ramp.module_id = app1.module_id
                  AND ramp.permission_id = app1.permission_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ramp.role_id,
                     ramp.app_id,
                     ramp.module_id,
                     ramp.permission_id,
                     ramp.row_uid)
             VALUES (
                       (SELECT role_id
                          FROM role
                         WHERE     role_name = 'LMROLE'
                               AND COMPANY_ID IN (SELECT COMPANY_ID
                                                    FROM COMPANY
                                                   WHERE COMPANY_NAME = '01')),
                       app1.app_id,
                       APP1.MODULE_ID,
                       app1.permission_id,
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

DECLARE
   V_COUNT   INTEGER;
BEGIN
   SELECT COUNT (1)
     INTO V_COUNT
     FROM ROLE
    WHERE     ROLE_NAME = 'LMROLE'
          AND COMPANY_ID IN (SELECT COMPANY_ID
                               FROM COMPANY
                              WHERE COMPANY_NAME = '01');

   IF V_COUNT = 1
   THEN
      MERGE INTO role_app_mod_perm ramp
           USING (SELECT amp.app_id app_id,
                         amp.module_id module_id,
                         p.permission_id permission_id
                    FROM app_mod_perm amp, PERMISSION p
                   WHERE     app_id = (SELECT app_id
                                         FROM app
                                        WHERE app_name = 'Labor Management')
                         AND p.permission_id = amp.permission_id
                         AND module_id = (SELECT module_id
                                            FROM module
                                           WHERE module_code = 'MIF')
                         AND p.permission_id =
                                (SELECT PERMISSION_ID
                                   FROM PERMISSION
                                  WHERE PERMISSION_CODE = 'POSTMSG')) APP1
              ON (    ramp.app_id = app1.app_id
                  AND ramp.module_id = app1.module_id
                  AND ramp.permission_id = app1.permission_id)
      WHEN NOT MATCHED
      THEN
         INSERT     (ramp.role_id,
                     ramp.app_id,
                     ramp.module_id,
                     ramp.permission_id,
                     ramp.row_uid)
             VALUES (
                       (SELECT role_id
                          FROM role
                         WHERE     role_name = 'LMROLE'
                               AND COMPANY_ID IN (SELECT COMPANY_ID
                                                    FROM COMPANY
                                                   WHERE COMPANY_NAME = '01')),
                       app1.app_id,
                       APP1.MODULE_ID,
                       app1.permission_id,
                       SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END IF;
END;
/

COMMIT;

-- DBTicket DB-8018 REFL
MERGE INTO ROLE_APP_MOD_PERM A
     USING (SELECT (SELECT APP_ID
                      FROM APP
                     WHERE app_name = 'Common Business Object')
                      AS APP_ID,
                   (SELECT MODULE_ID
                      FROM MODULE
                     WHERE MODULE_CODE = 'BDM')
                      MODULE_ID,
                   (SELECT ROLE_ID
                      FROM role
                     WHERE ROLE_NAME = 'LMROLE' and CREATED_SOURCE='system')
                      ROLE_ID,
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'AWKAREA')
                      PERMISSION_ID
              FROM DUAL) B
        ON (    A.APP_ID = B.APP_ID
            AND A.MODULE_ID = B.MODULE_ID
            AND A.PERMISSION_ID = B.PERMISSION_ID
            AND A.ROLE_ID = B.ROLE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (ROLE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT role_id
                   FROM role
                  WHERE role_name = 'LMROLE' and CREATED_SOURCE='system'),
               (SELECT APP_ID
                  FROM app
                 WHERE app_name = 'Common Business Object'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'BDM'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'AWKAREA'),
               NULL,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);
               
MERGE INTO ROLE_APP_MOD_PERM A
     USING (SELECT (SELECT APP_ID
                      FROM APP
                     WHERE app_name = 'Common Business Object')
                      AS APP_ID,
                   (SELECT MODULE_ID
                      FROM MODULE
                     WHERE MODULE_CODE = 'TRANS')
                      MODULE_ID,
                   (SELECT ROLE_ID
                      FROM role
                     WHERE ROLE_NAME = 'LMROLE' and CREATED_SOURCE='system')
                      ROLE_ID,
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'VWKAREA')
                      PERMISSION_ID
              FROM DUAL) B
        ON (    A.APP_ID = B.APP_ID
            AND A.MODULE_ID = B.MODULE_ID
            AND A.PERMISSION_ID = B.PERMISSION_ID
            AND A.ROLE_ID = B.ROLE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (ROLE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT role_id
                   FROM role
                  WHERE role_name = 'LMROLE' and CREATED_SOURCE='system'),
               (SELECT APP_ID
                  FROM app
                 WHERE app_name = 'Common Business Object'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'TRANS'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'VWKAREA'),
               NULL,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);               
              
COMMIT;
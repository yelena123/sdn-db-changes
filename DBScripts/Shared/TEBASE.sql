-- DBTicket TE-343
merge into LRF_REPORT_DEF L1
using (select 'ShipmentControlForm' REPORT_NAME, 'Shipment Control Form' DESCRIPTION, 'OUTBOUND' SUBCATEGORY from dual) B
on(L1.REPORT_NAME=B.REPORT_NAME and L1.DESCRIPTION=B.DESCRIPTION and L1.SUBCATEGORY=B.SUBCATEGORY)
when not matched
then 
insert(REPORT_DEF_ID, REPORT_NAME, REPORT_FILE_NAME, DESCRIPTION, DBNAME, TYPE, VIEWPERM, EDITPERM, CATEGORY, SUBCATEGORY)
values(SEQ_LRF_REPORT_DEF_ID.nextval, 'ShipmentControlForm', 'ShipmentControlForm', 'Shipment Control Form', 'DB2DB', 'Jasper', 'AAPA', 'AAPA', 'TE', 'OUTBOUND');

MERGE INTO lrf_report_def_layout L1
     USING (SELECT L2.REPORT_DEF_ID,
                   L2.category,
                   L2.subcategory,
                   L2.report_name
              FROM lrf_report_def L2
             WHERE     category = 'TE'
                   AND subcategory = 'OUTBOUND'
                   AND report_name = 'ShipmentControlForm') L3
        ON (    L1.REPORT_DEF_ID = L3.REPORT_DEF_ID
            AND L1.FIELD_NAME = 'MANIFEST_HDR.TC_MANIFEST_ID'
            AND L1.FIELD_LABEL = 'Manifest ID')
WHEN NOT MATCHED
THEN
   INSERT(report_def_layout_id, report_def_id, field_position, is_required, field_name, field_label, field_operators, field_type, value_generator_class, selection_page_url, operator_type, sub_object_type, table_name, default_value, is_localizable, send_parameter, hibernate_field_name)
   values(LRF_REPORT_DEF_LAYOUT_ID_SEQ.nextval, (select report_def_id from lrf_report_def  where report_file_name = 'ShipmentControlForm'), 0, 1, 'MANIFEST_HDR.TC_MANIFEST_ID', 'Manifest ID', '=', 'STRING', null, null, 'REPORT_TEXT_ITEM', null, null, null, 0, 0, 'tcManifestId');

MERGE INTO lrf_report_def_layout L1
     USING (SELECT L2.REPORT_DEF_ID,
                   L2.category,
                   L2.subcategory,
                   L2.report_name
              FROM lrf_report_def L2
             WHERE     category = 'TE'
                   AND subcategory = 'OUTBOUND'
                   AND report_name = 'ShipmentControlForm') L3
        ON (    L1.REPORT_DEF_ID = L3.REPORT_DEF_ID
            AND L1.FIELD_NAME = 'MANIFEST_HDR.TC_COMPANY_ID'
            AND L1.FIELD_LABEL = 'Company ID')
WHEN NOT MATCHED
THEN
   INSERT(report_def_layout_id, report_def_id, field_position, is_required, field_name, field_label, field_operators, field_type, value_generator_class, selection_page_url, operator_type, sub_object_type, table_name, default_value, is_localizable, send_parameter, hibernate_field_name)
   values(LRF_REPORT_DEF_LAYOUT_ID_SEQ.nextval, (select report_def_id from lrf_report_def  where report_file_name = 'ShipmentControlForm'), 1, 1, 'MANIFEST_HDR.TC_COMPANY_ID', 'Company ID', '=', 'STRING', null, null, 'REPORT_TEXT_ITEM', null, null, null, 0, 0, 'tcCompanyId');
MERGE INTO ACCESSORIAL_GROUP A
USING (SELECT  'TSM' ACCESSORIAL_GROUP,
                'Time Sensitive Material' DESCRIPTION,
                '113' OPTION_GROUP FROM DUAL) B
     on ( A.ACCESSORIAL_GROUP=B.ACCESSORIAL_GROUP and  A.DESCRIPTION=B.DESCRIPTION AND A.OPTION_GROUP=B.OPTION_GROUP )
     WHEN NOT  MATCHED THEN
     INSERT(ACCESSORIAL_GROUP,DESCRIPTION,OPTION_GROUP) 
VALUES('TSM','Time Sensitive Material','113');

MERGE INTO ACCESSORIAL_OPTION_GROUP A
USING (SELECT  'TSM' ACCESSORIAL_GROUP_CODE,
                'Time Sensitive Material' DESCRIPTION,
                '113' OPTION_GROUP FROM DUAL) B
     on ( A.ACCESSORIAL_GROUP_CODE=B.ACCESSORIAL_GROUP_CODE and  A.DESCRIPTION=B.DESCRIPTION AND A.OPTION_GROUP=B.OPTION_GROUP )
     WHEN NOT  MATCHED THEN
     INSERT(ACCESSORIAL_GROUP_CODE,DESCRIPTION,OPTION_GROUP)
VALUES('TSM','Time Sensitive Material','113');

commit;

-- DBTicket MACR00858179
--PERMISSION
MERGE INTO PERMISSION P
     USING (SELECT 'View - Parcel Transit Time' PERMISSION_NAME, 'VPCLTRANSITTIME' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);

MERGE INTO permission P
     USING (SELECT 'Admin - Parcel Transit Time' PERMISSION_NAME, 'APCLTRANSITTIME' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);


--company_type_perm
MERGE INTO COMPANY_TYPE_PERM ctp
USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'VPCLTRANSITTIME') ctp1
ON (ctp.company_type_id = ctp1.company_type_id
    AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED THEN
   INSERT (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
   VALUES (4,ctp1.permission_id, (SELECT MAX (row_uid) + 4
                                  FROM company_type_perm));

MERGE INTO company_type_perm ctp
USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'APCLTRANSITTIME') ctp1
ON (ctp.company_type_id = ctp1.company_type_id
    AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED THEN
   INSERT (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
   VALUES (4,ctp1.permission_id, (SELECT MAX (row_uid) + 4
                                  FROM company_type_perm));
                                  
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');                                  

MERGE INTO APP_MOD_PERM AMP
   USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                 MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                 PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
           WHERE APP.APP_NAME = 'Warehouse Management'
             AND MODULE.MODULE_CODE = 'CNTRMGT'
             AND PERMISSION.PERMISSION_CODE = 'VPCLTRANSITTIME') APP1
   ON (    AMP.APP_ID = APP1.APP_ID
       AND AMP.MODULE_ID = APP1.MODULE_ID
       AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
   WHEN NOT MATCHED THEN
      INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
      VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
              (SEQ_APP_MOD_PERM.nextval));	   

MERGE INTO APP_MOD_PERM AMP
   USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                 MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                 PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
           WHERE APP.APP_NAME = 'Warehouse Management'
             AND MODULE.MODULE_CODE = 'CNTRMGT'
             AND PERMISSION.PERMISSION_CODE = 'APCLTRANSITTIME') APP1
   ON (    AMP.APP_ID = APP1.APP_ID
       AND AMP.MODULE_ID = APP1.MODULE_ID
       AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
   WHEN NOT MATCHED THEN
      INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
      VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
              (SEQ_APP_MOD_PERM.nextval));   
   
MERGE INTO ROLE_APP_MOD_PERM ramp
  USING (select amp.app_id app_id, amp.module_id module_id, p.permission_id permission_id from app_mod_perm amp,PERMISSION p
         where app_id=(select app_id from app where app_name='Warehouse Management')
         and p.permission_id=amp.permission_id
         and module_id= (select module_id from module where module_code='CNTRMGT')
         and p.permission_id=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VPCLTRANSITTIME')) APP1
     ON (ramp.app_id = app1.app_id
         AND ramp.module_id = app1.module_id
         AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
INSERT     (ramp.role_id,
            ramp.app_id,
            ramp.module_id,
            ramp.permission_id,
            ramp.row_uid)
    VALUES ((SELECT role_id
                FROM role
               WHERE role_name = 'WMROLE'),
            app1.app_id,
            APP1.MODULE_ID,
            app1.permission_id,
            SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);	
	
MERGE INTO role_app_mod_perm ramp
  USING (select amp.app_id app_id, amp.module_id module_id, p.permission_id permission_id from app_mod_perm amp,PERMISSION p
         where app_id=(select app_id from app where app_name='Warehouse Management')
         and p.permission_id=amp.permission_id
         and module_id= (select module_id from module where module_code='CNTRMGT')
         and p.permission_id=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'APCLTRANSITTIME')) APP1
     ON (ramp.app_id = app1.app_id
         AND ramp.module_id = app1.module_id
         AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
INSERT     (ramp.role_id,
            ramp.app_id,
            ramp.module_id,
            ramp.permission_id,
            ramp.row_uid)
    VALUES ((SELECT role_id
                FROM role
               WHERE role_name = 'WMROLE'),
            app1.app_id,
            APP1.MODULE_ID,
            app1.permission_id,
            SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
	
MERGE INTO role_app_mod_perm ramp
  USING (select amp.app_id app_id, amp.module_id module_id, p.permission_id permission_id from app_mod_perm amp,PERMISSION p
         where app_id=(select app_id from app where app_name='Warehouse Management')
         and p.permission_id=amp.permission_id
         and module_id= (select module_id from module where module_code='CNTRMGT')
         and p.permission_id=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VPCLTRANSITTIME')) APP1
     ON (ramp.app_id = app1.app_id
         AND ramp.module_id = app1.module_id
         AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
INSERT     (ramp.role_id,
            ramp.app_id,
            ramp.module_id,
            ramp.permission_id,
            ramp.row_uid)
    VALUES ((SELECT role_id
                FROM role
               WHERE role_name = 'WMOBMGRROLE'),
            app1.app_id,
            APP1.MODULE_ID,
            app1.permission_id,
            SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);	
	
MERGE INTO role_app_mod_perm ramp
  USING (select amp.app_id app_id, amp.module_id module_id, p.permission_id permission_id from app_mod_perm amp,PERMISSION p
         where app_id=(select app_id from app where app_name='Warehouse Management')
         and p.permission_id=amp.permission_id
         and module_id= (select module_id from module where module_code='CNTRMGT')
         and p.permission_id=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'APCLTRANSITTIME')) APP1
     ON (ramp.app_id = app1.app_id
         AND ramp.module_id = app1.module_id
         AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
INSERT     (ramp.role_id,
            ramp.app_id,
            ramp.module_id,
            ramp.permission_id,
            ramp.row_uid)
    VALUES ((SELECT role_id
                FROM role
               WHERE role_name = 'WMOBMGRROLE'),
            app1.app_id,
            APP1.MODULE_ID,
            app1.permission_id,
            SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);	
    
    
---RELTP_APP_MOD_PERM:
MERGE INTO RELTP_APP_MOD_PERM RAMP
  USING (SELECT  RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
       APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                PERMISSION.PERMISSION_CODE
           FROM RELATIONSHIP_TYPE,APP, MODULE, PERMISSION
          WHERE RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID=4
            AND APP.APP_NAME = 'Warehouse Management'
            AND MODULE.MODULE_CODE = 'CNTRMGT'
            AND PERMISSION.PERMISSION_CODE = 'VPCLTRANSITTIME') APP1
  ON (RAMP.RELATIONSHIP_TYPE_ID=APP1.RELATIONSHIP_TYPE_ID
      AND RAMP.APP_ID = APP1.APP_ID
      AND RAMP.MODULE_ID = APP1.MODULE_ID
      AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
  WHEN NOT MATCHED THEN
     INSERT (RAMP.RELATIONSHIP_TYPE_ID,RAMP.APP_ID, RAMP.MODULE_ID, RAMP.PERMISSION_ID, RAMP.ROW_UID)
     VALUES (4,APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
             (SELECT NVL (MAX (ROW_UID), 0) + 4
                FROM RELTP_APP_MOD_PERM));


MERGE INTO RELTP_APP_MOD_PERM RAMP
  USING (SELECT  RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
       APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                PERMISSION.PERMISSION_CODE
           FROM RELATIONSHIP_TYPE,APP, MODULE, PERMISSION
          WHERE RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID=4
            AND APP.APP_NAME = 'Warehouse Management'
            AND MODULE.MODULE_CODE = 'CNTRMGT'
            AND PERMISSION.PERMISSION_CODE = 'APCLTRANSITTIME') APP1
  ON (RAMP.RELATIONSHIP_TYPE_ID=APP1.RELATIONSHIP_TYPE_ID
      AND RAMP.APP_ID = APP1.APP_ID
      AND RAMP.MODULE_ID = APP1.MODULE_ID
      AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
  WHEN NOT MATCHED THEN
     INSERT (RAMP.RELATIONSHIP_TYPE_ID,RAMP.APP_ID, RAMP.MODULE_ID, RAMP.PERMISSION_ID, RAMP.ROW_UID)
     VALUES (4,APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
             (SELECT NVL (MAX (ROW_UID), 0) + 4
                FROM RELTP_APP_MOD_PERM));
				
--PERMISSION
MERGE INTO permission P
     USING (SELECT 'View - Parcel Zone Rates' PERMISSION_NAME, 'VPCLZONERATE' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(seq_permission_id.nextval, 'View - Parcel Zone Rates', 1, -1, 'VPCLZONERATE');

MERGE INTO permission P
     USING (SELECT 'Admin - Parcel Zone Rates' PERMISSION_NAME, 'APCLZONERATE' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(seq_permission_id.nextval, 'Admin - Parcel Zone Rates', 1, -1, 'APCLZONERATE');

--COMPANY_TYPE_PERM:
MERGE INTO company_type_perm ctp
USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'VPCLZONERATE') ctp1
ON (ctp.company_type_id = ctp1.company_type_id
    AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED THEN
   INSERT (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
   VALUES (4,ctp1.permission_id, (SELECT MAX (row_uid) + 4
                                  FROM company_type_perm));

MERGE INTO company_type_perm ctp
USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'APCLZONERATE') ctp1
ON (ctp.company_type_id = ctp1.company_type_id
    AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED THEN
   INSERT (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
   VALUES (4,ctp1.permission_id, (SELECT MAX (row_uid) + 4
                                  FROM company_type_perm));

--APP_MOD_PERM:
MERGE INTO APP_MOD_PERM AMP
   USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                 MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                 PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
           WHERE APP.APP_NAME = 'Warehouse Management'
             AND MODULE.MODULE_CODE = 'CNTRMGT'
             AND PERMISSION.PERMISSION_CODE = 'VPCLZONERATE') APP1
   ON (    AMP.APP_ID = APP1.APP_ID
       AND AMP.MODULE_ID = APP1.MODULE_ID
       AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
   WHEN NOT MATCHED THEN
      INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
      VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
              (SEQ_APP_MOD_PERM.nextval));	   

MERGE INTO APP_MOD_PERM AMP
   USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                 MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                 PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
           WHERE APP.APP_NAME = 'Warehouse Management'
             AND MODULE.MODULE_CODE = 'CNTRMGT'
             AND PERMISSION.PERMISSION_CODE = 'APCLZONERATE') APP1
   ON (    AMP.APP_ID = APP1.APP_ID
       AND AMP.MODULE_ID = APP1.MODULE_ID
       AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
   WHEN NOT MATCHED THEN
      INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
      VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
              (SEQ_APP_MOD_PERM.nextval));
				 
--ROLE_APP_MOD_PERM:
MERGE INTO role_app_mod_perm ramp
  USING (select amp.app_id app_id, amp.module_id module_id, p.permission_id permission_id from app_mod_perm amp,PERMISSION p
         where app_id=(select app_id from app where app_name='Warehouse Management')
         and p.permission_id=amp.permission_id
         and module_id= (select module_id from module where module_code='CNTRMGT')
         and p.permission_id=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VPCLZONERATE')) APP1
     ON (ramp.app_id = app1.app_id
         AND ramp.module_id = app1.module_id
         AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
INSERT     (ramp.role_id,
            ramp.app_id,
            ramp.module_id,
            ramp.permission_id,
            ramp.row_uid)
    VALUES ((SELECT role_id
                FROM role
               WHERE role_name = 'WMROLE'),
            app1.app_id,
            APP1.MODULE_ID,
            app1.permission_id,
            SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);	

MERGE INTO role_app_mod_perm ramp
  USING (select amp.app_id app_id, amp.module_id module_id, p.permission_id permission_id from app_mod_perm amp,PERMISSION p
         where app_id=(select app_id from app where app_name='Warehouse Management')
         and p.permission_id=amp.permission_id
         and module_id= (select module_id from module where module_code='CNTRMGT')
         and p.permission_id=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'APCLZONERATE')) APP1
     ON (ramp.app_id = app1.app_id
         AND ramp.module_id = app1.module_id
         AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
INSERT     (ramp.role_id,
            ramp.app_id,
            ramp.module_id,
            ramp.permission_id,
            ramp.row_uid)
    VALUES ((SELECT role_id
                FROM role
               WHERE role_name = 'WMROLE'),
            app1.app_id,
            APP1.MODULE_ID,
            app1.permission_id,
            SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);	
	
MERGE INTO role_app_mod_perm ramp
  USING (select amp.app_id app_id, amp.module_id module_id, p.permission_id permission_id from app_mod_perm amp,PERMISSION p
         where app_id=(select app_id from app where app_name='Warehouse Management')
         and p.permission_id=amp.permission_id
         and module_id= (select module_id from module where module_code='CNTRMGT')
         and p.permission_id=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VPCLZONERATE')) APP1
     ON (ramp.app_id = app1.app_id
         AND ramp.module_id = app1.module_id
         AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
INSERT     (ramp.role_id,
            ramp.app_id,
            ramp.module_id,
            ramp.permission_id,
            ramp.row_uid)
    VALUES ((SELECT role_id
                FROM role
               WHERE role_name = 'WMOBMGRROLE'),
            app1.app_id,
            APP1.MODULE_ID,
            app1.permission_id,
            SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);		
	
MERGE INTO role_app_mod_perm ramp
  USING (select amp.app_id app_id, amp.module_id module_id, p.permission_id permission_id from app_mod_perm amp,PERMISSION p
         where app_id=(select app_id from app where app_name='Warehouse Management')
         and p.permission_id=amp.permission_id
         and module_id= (select module_id from module where module_code='CNTRMGT')
         and p.permission_id=(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'APCLZONERATE')) APP1
     ON (ramp.app_id = app1.app_id
         AND ramp.module_id = app1.module_id
         AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
INSERT     (ramp.role_id,
            ramp.app_id,
            ramp.module_id,
            ramp.permission_id,
            ramp.row_uid)
    VALUES ((SELECT role_id
                FROM role
               WHERE role_name = 'WMOBMGRROLE'),
            app1.app_id,
            APP1.MODULE_ID,
            app1.permission_id,
            SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);	
	
	
--RELTP_APP_MOD_PERM:
MERGE INTO RELTP_APP_MOD_PERM RAMP
  USING (SELECT  RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
       APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                PERMISSION.PERMISSION_CODE
           FROM RELATIONSHIP_TYPE,APP, MODULE, PERMISSION
          WHERE RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID=4
            AND APP.APP_NAME = 'Warehouse Management'
            AND MODULE.MODULE_CODE = 'CNTRMGT'
            AND PERMISSION.PERMISSION_CODE = 'VPCLZONERATE') APP1
  ON (RAMP.RELATIONSHIP_TYPE_ID=APP1.RELATIONSHIP_TYPE_ID
      AND RAMP.APP_ID = APP1.APP_ID
      AND RAMP.MODULE_ID = APP1.MODULE_ID
      AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
  WHEN NOT MATCHED THEN
     INSERT (RAMP.RELATIONSHIP_TYPE_ID,RAMP.APP_ID, RAMP.MODULE_ID, RAMP.PERMISSION_ID, RAMP.ROW_UID)
     VALUES (4,APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
             (SELECT NVL (MAX (ROW_UID), 0) + 4
                FROM RELTP_APP_MOD_PERM));

MERGE INTO RELTP_APP_MOD_PERM RAMP
  USING (SELECT  RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
       APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                PERMISSION.PERMISSION_CODE
           FROM RELATIONSHIP_TYPE,APP, MODULE, PERMISSION
          WHERE RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID=4
            AND APP.APP_NAME = 'Warehouse Management'
            AND MODULE.MODULE_CODE = 'CNTRMGT'
            AND PERMISSION.PERMISSION_CODE = 'APCLZONERATE') APP1
  ON (RAMP.RELATIONSHIP_TYPE_ID=APP1.RELATIONSHIP_TYPE_ID
      AND RAMP.APP_ID = APP1.APP_ID
      AND RAMP.MODULE_ID = APP1.MODULE_ID
      AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
  WHEN NOT MATCHED THEN
     INSERT (RAMP.RELATIONSHIP_TYPE_ID,RAMP.APP_ID, RAMP.MODULE_ID, RAMP.PERMISSION_ID, RAMP.ROW_UID)
     VALUES (4,APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID,
             (SELECT NVL (MAX (ROW_UID), 0) + 4
                FROM RELTP_APP_MOD_PERM));

COMMIT;

-- DBTicket TE-2581
CALL SEQUPDT('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');

-- INSERT INTO permission(PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) 
-- VALUES(SEQ_PERMISSION_ID.nextval,  'Transportation - Manifest',  1,-1,'WMVWKAREA',3,'Manhattan Associates',sysdate,3,'Manhattan Associates',sysdate,null);
MERGE INTO PERMISSION L
     USING (SELECT 'WMVWKAREA' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
INSERT (PERMISSION_ID,PERMISSION_NAME,IS_ACTIVE,COMPANY_ID,PERMISSION_CODE,CREATED_SOURCE_TYPE_ID,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE_ID,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,DESCRIPTION) 
VALUES(SEQ_PERMISSION_ID.nextval,  'Transportation - Manifest',  1,-1,'WMVWKAREA',3,'Manhattan Associates',sysdate,3,'Manhattan Associates',sysdate,null);

commit;

-- DBTicket TE-3071
UPDATE lrf_report_def_layout
   SET field_name = 'LPN.TC_LPN_ID',
       field_label = 'LPN ID',
       hibernate_field_name = 'tcLpnId'
 WHERE report_def_id = (SELECT report_def_id
                          FROM lrf_report_def
                         WHERE report_file_name = 'FedExGroundShipping')
       AND field_position = 0;

UPDATE lrf_report_def_layout
   SET field_name = 'LPN.TC_COMPANY_ID'
 WHERE report_def_id = (SELECT report_def_id
                          FROM lrf_report_def
                         WHERE report_file_name = 'FedExGroundShipping')
       AND field_position = 1;

UPDATE lrf_report_def_layout
   SET field_name = 'LPN.TC_LPN_ID',
       field_label = 'LPN ID',
       hibernate_field_name = 'tcLpnId'
 WHERE report_def_id = (SELECT report_def_id
                          FROM lrf_report_def
                         WHERE report_file_name = 'UpsGroundShipping')
       AND field_position = 0;

UPDATE lrf_report_def_layout
   SET field_name = 'LPN.TC_COMPANY_ID'
 WHERE report_def_id = (SELECT report_def_id
                          FROM lrf_report_def
                         WHERE report_file_name = 'UpsGroundShipping')
       AND field_position = 1;
       
commit;       

-- DBTicket TE-4186
MERGE INTO ACCESSORIAL_OPTION_GROUP A
     USING (SELECT 'COI' ACCESSORIAL_GROUP_CODE,
                   'Indirect Signature' DESCRIPTION,
                   '103' OPTION_GROUP
              FROM DUAL) B
        ON (    A.ACCESSORIAL_GROUP_CODE = B.ACCESSORIAL_GROUP_CODE
            AND A.DESCRIPTION = B.DESCRIPTION
            AND A.OPTION_GROUP = B.OPTION_GROUP)
WHEN NOT MATCHED
THEN
   INSERT     (ACCESSORIAL_GROUP_CODE, DESCRIPTION, OPTION_GROUP)
       VALUES ('COI', 'Indirect Signature', '103');             
       
COMMIT;

-- DBTicket TE-4296 REFL
merge into permission p
     using (select 'De-Manifest oLPN UI' permission_name, 'DMNFO' permission_code from dual) b
        on (p.permission_name = b.permission_name and p.permission_code = b.permission_code)
when not matched
then
   insert     (permission_id,
               permission_name,
               is_active,
               company_id,
               permission_code,
               created_source_type_id,
               created_source,
               created_dttm,
               last_updated_source_type_id,
               last_updated_source,
               last_updated_dttm,
               description)
       values (seq_permission_id.nextval,
               'De-Manifest oLPN UI',
               1,
               -1,
               'DMNFO',
               3,
               'Manhattan Associates',
               sysdate,
               3,
               'Manhattan Associates',
               sysdate,
               null);
commit;

-- DBTicket TE-4322
MERGE INTO ACCESSORIAL_OPTION_GROUP A
     USING (SELECT 'HAZ' ACCESSORIAL_GROUP_CODE,
                   'Hazardous Material' DESCRIPTION,
                   '4' OPTION_GROUP
              FROM DUAL) B
        ON (    A.ACCESSORIAL_GROUP_CODE = B.ACCESSORIAL_GROUP_CODE
            AND A.DESCRIPTION = B.DESCRIPTION
            AND A.OPTION_GROUP = B.OPTION_GROUP)
WHEN NOT MATCHED
THEN
   INSERT     (ACCESSORIAL_GROUP_CODE, DESCRIPTION, OPTION_GROUP)
       VALUES ('HAZ', 'Hazardous Material', '4');

COMMIT;

-- DBTicket DB-2360
UPDATE service_level
   SET IS_RATE_LPN_AT_MANIFEST = 0
 WHERE service_level = 'UPSWXP25' AND tc_company_id = 1;

UPDATE service_level
   SET IS_RATE_LPN_AT_MANIFEST = 0
 WHERE service_level = 'UPSMIBPM' AND tc_company_id = 1;
 
COMMIT;

-- DBTicket DB-3042
UPDATE country
   SET DFLT_CURRENCY_CODE = 'USD'
 WHERE country_code IN ('AO',
                        'BI',
                        'CM',
                        'CD',
                        'DJ',
                        'ET',
                        'CI',
                        'JO',
                        'MW',
                        'MZ',
                        'RW',
                        'TZ',
                        'TN',
                        'ZM',
                        'ZW');

UPDATE country
   SET DFLT_CURRENCY_CODE = 'EUR'
 WHERE country_code = 'LT';
 
COMMIT;

-- DBTicket DB-3149 REFL

MERGE INTO permission p
     USING (SELECT 'EPI End of Day' permission_name, 'EPIEOD' permission_code
              FROM DUAL) b
        ON (p.permission_name = b.permission_name
            AND p.permission_code = b.permission_code)
WHEN NOT MATCHED
THEN
   INSERT     (permission_id,
               permission_name,
               is_active,
               company_id,
               permission_code)
       VALUES (seq_permission_id.NEXTVAL,
               'EPI End of Day',
               1,
               -1,
               'EPIEOD');

MERGE INTO company_type_perm ctp
     USING (SELECT 4 company_type_id, permission_id
              FROM permission
             WHERE permission_code = 'EPIEOD') ctp1
        ON (ctp.company_type_id = ctp1.company_type_id
            AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ctp.company_type_id, ctp.permission_id, ctp.row_uid)
       VALUES (
                 4,
                 ctp1.permission_id,
                 (SELECT MAX (row_uid) + 4 FROM company_type_perm));

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'ASHP'
                   AND PERMISSION.PERMISSION_CODE = 'EPIEOD') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES (APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SEQ_APP_MOD_PERM.NEXTVAL));

DECLARE
      V_COUNT integer;
BEGIN

       SELECT COUNT(1)
       INTO V_COUNT
       FROM ROLE
       WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');

       IF V_COUNT = 1
       THEN
            INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
               SELECT ROLE.ROLE_ID,
                      (SELECT APP_ID
                  FROM app_mod_perm
                 WHERE permission_id IN
                          (SELECT permission_id
                             FROM permission
                            WHERE permission_code = 'EPIEOD')),
               (SELECT MODULE_ID
                  FROM app_mod_perm
                 WHERE permission_id IN
                          (SELECT permission_id
                             FROM permission
                            WHERE permission_code = 'EPIEOD')),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'EPIEOD'),
                      SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL
                 FROM ROLE
                WHERE ROLE_NAME = 'WMROLE' AND COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE COMPANY_NAME = '01');
       END IF;
END;
/               

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_NAME = 'Warehouse Management'
                   AND MODULE.MODULE_CODE = 'ASHP'
                   AND PERMISSION.PERMISSION_CODE = 'EPIEOD') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RAMP.RELATIONSHIP_TYPE_ID,
               RAMP.APP_ID,
               RAMP.MODULE_ID,
               RAMP.PERMISSION_ID,
               RAMP.ROW_UID,
               RAMP.IS_PERM_IMPLIED)
       VALUES (4,
               APP1.APP_ID,
               APP1.MODULE_ID,
               APP1.PERMISSION_ID,
               (SELECT NVL (MAX (ROW_UID), 0) + 4 FROM RELTP_APP_MOD_PERM),
               0);

COMMIT;
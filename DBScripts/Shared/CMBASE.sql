-- DBTicket TPE-3483

alter table TARIFF_ZONE_RATE_TIER modify MAXIMUM_SIZE NUMBER(17,6);
alter table TARIFF_ZONE_RATE_TIER modify MINIMUM_SIZE NUMBER(17,6);


-- DBTicket CMGT-158

alter table IMPORT_SAILING_LANE modify (O_ZONE_NAME varchar2(30));
alter table IMPORT_SAILING_LANE modify (D_ZONE_NAME varchar2(30));


-- DBTicket TPE-28469

ALTER TABLE ACCESSORIAL_CODE ADD IS_RAIL_RULE_11 NUMBER(1) DEFAULT 0 NOT NULL;
COMMENT ON COLUMN ACCESSORIAL_CODE.IS_RAIL_RULE_11 IS 'Rail rule 11 rating support for rating';

-- DBTicket CMGT-386

ALTER TABLE TARIFF_ZONE_RATE ADD MINIMUM_RATE NUMBER(14,3); 
COMMENT ON COLUMN TARIFF_ZONE_RATE.MINIMUM_RATE IS 'Tariff to include Minimum Rate at Rate Zone level';


-- DBTicket CMGT-491

MERGE INTO LABEL L USING
(
   SELECT
   'UCL' BUNDLE_NAME, 'UCLPermissions_View Carrier Forecast Page' KEY
   FROM DUAL
)
B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN
INSERT
(LABEL_ID,KEY,VALUE,BUNDLE_NAME)
VALUES
  (
    SEQ_LABEL_ID.NEXTVAL,
    'UCLPermissions_View Carrier Forecast Page',
    'Contract Mgmt - View Carrier Forecast Page',
    'UCL'
  );

COMMIT;



-- DBTicket CMGT-537
 
 
 exec sequpdt('RESOURCES','RESOURCE_ID','SEQ_RESOURCE_ID');
 
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/businessUnitMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/businessUnitMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/incotermList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/incotermList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/billingMethodList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/billingMethodList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/countryListMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/countryListMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/originStateProvMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/originStateProvMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/stateProvMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/stateProvMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/zoneMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/zoneMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/frequencyMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/frequencyMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/rGQualifier'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/rGQualifier' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/exportLane'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/exportLane' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/exportAllLane'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/exportAllLane' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/validateLanes'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/validateLanes' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneUIServices/laneUIServices/validationErrorDetails'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneUIServices/laneUIServices/validationErrorDetails' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SailingScheduleService/sailingScheduleService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/weekDaysList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SailingScheduleWeeklyService/sailingScheduleWeeklyService/weekDaysList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/routingTierList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/routingTierList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/carrierRejectionPeriodList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/carrierRejectionPeriodList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/cubingFactorList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/cubingFactorList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/sailingFrequencyTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/sailingFrequencyTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/fixedTransitTimeUOMList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/fixedTransitTimeUOMList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/adjustLaneDetails'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/adjustLaneDetails' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/capacityCommitmentSizeUOMMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/LaneCarrierGridServices/laneCarrierGridServices/capacityCommitmentSizeUOMMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/tierList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/tierList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/reasonCodeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/reasonCodeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/tariffList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/tariffList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/minRateTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/minRateTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/distanceUOMList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/distanceUOMList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMWeightList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMWeightList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMAllList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMAllList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMDistanceList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/rateUOMDistanceList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/auditTrailList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/auditTrailList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/pendingApprovalChangesList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierRatesService/carrierratesservice/pendingApprovalChangesList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/auditTrailList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/auditTrailList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/pendingApprovalChangesList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/pendingApprovalChangesList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/accessorialExclusionList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RatingAccessorialService/ratingaccessorialservice/accessorialExclusionList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/SurgeCapacityService/surgecapacityservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/lpnTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShippingParameterService/shippingparameterservice/lpnTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/carrierUtilizationUsageByWeek'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierUtilizationService/carrierutilizationservice/carrierUtilizationUsageByWeek' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierModeCapacityService/carriermodecapacityservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CommodityClassTierService/commodityclasstierservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/accessorialCodeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/accessorialCodeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/surchargeTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/surchargeTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/zoneList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/zoneList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/serviceLevelList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/serviceLevelList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/modeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/modeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/currencyList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/DeliverySurchargeService/deliverysurchargeservice/currencyList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/StopoffChargeService/stopoffchargeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContinuousMoveDiscountService/continuousmovediscountservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/stopOffCurrencyList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/stopOffCurrencyList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/distanceUOMList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/distanceUOMList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/rateTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/rateTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/businessUnitList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/businessUnitList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/weightList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/weightList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/equipmentCodeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/equipmentCodeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/cmCurrencyList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/cmCurrencyList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/fakCommodityCodeClassList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/fakCommodityCodeClassList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/carrierCodeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/carrierCodeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/auditTrailList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/auditTrailList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/getStopoffCurrencies'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/getStopoffCurrencies' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/getCMCurrencies'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierParametersService/carrierparamsservice/getCMCurrencies' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/accessorialCodeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/accessorialCodeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/incotermList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/incotermList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/currencyList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/currencyList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/sizeTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/sizeTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/rateTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/rateTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/modeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/modeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/equipmentCodeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/equipmentCodeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/serviceLevelList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/serviceLevelList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/billingMethodList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/billingMethodList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/protectionLevelList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/protectionLevelList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/uomList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/uomList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/accessorialExclusionList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/accessorialExclusionList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/payeeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/payeeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/fuelAccessorialList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/fuelAccessorialList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/ORMList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/ORMList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/updateExcludedAccessorialList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialDataService/accessorialdataservice/updateExcludedAccessorialList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialLabelTypeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialLabelTypeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialTypeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialTypeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialOptionGroupMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialOptionGroupMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/distanceUOMMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/distanceUOMMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/acessorialOptionGroupDesc'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/acessorialOptionGroupDesc' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialLabelTypeDesc'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialLabelTypeDesc' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialCodesDependentForTC'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialCodesDependentForTC' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/validAccessorialCodesMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/validAccessorialCodesMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/fuelIndexMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/fuelIndexMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialDataById'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/accessorialDataById' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/businessUnitMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/businessUnitMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/dataSourceMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/dataSourceMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/exportFuelSurcharge'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/exportFuelSurcharge' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/AccessorialService/accessorialservice/sellSideRatesFlag'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/AccessorialService/accessorialservice/sellSideRatesFlag' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/modeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/modeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/serviceLevelMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/serviceLevelMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/shipViaAccessorialMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/shipViaAccessorialMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/currencyListMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/currencyListMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/shipViaCustomAttributeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/shipViaCustomAttributeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/insuranceCoverTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/insuranceCoverTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/labelTypeList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/labelTypeList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/parcelServiceIconList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/parcelServiceIconList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/serviceLevelIndicatorList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/serviceLevelIndicatorList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/executionlevelList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/executionlevelList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/carrierLookUpMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/carrierLookUpMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/businessUnitMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/businessUnitMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/billShipViaMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/billShipViaMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/normalizationCurrency'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/normalizationCurrency' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipViaService/shipviaservice/trackingNumberRequiredMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipViaService/shipviaservice/trackingNumberRequiredMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelIndexService/fuelIndexService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelIndexService/fuelIndexService/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelIndexService/fuelIndexService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelIndexService/fuelIndexService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/FuelTypeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelIndexService/fuelIndexService/FuelTypeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/countryListMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelIndexService/fuelIndexService/countryListMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelIndexService/fuelIndexService/RegionListMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelIndexService/fuelIndexService/RegionListMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelRateService/fuelRateService/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelRateService/fuelRateService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelRateService/fuelRateService/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelRateService/fuelRateService/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelRateService/fuelRateService/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelRateService/fuelRateService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelRateService/fuelRateService/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelRateService/fuelRateService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelRateService/fuelRateService/CurrencyListMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelRateService/fuelRateService/CurrencyListMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelRateService/fuelRateService/NormalizationCurrencyMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelRateService/fuelRateService/NormalizationCurrencyMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/FuelRateService/fuelRateService/RateUOMListMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/FuelRateService/fuelRateService/RateUOMListMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/billShipviaLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/billShipviaLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/customerlookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/customerlookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/stateProvMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/stateProvMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/countryMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/countryMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/searchTypeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/searchTypeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/errorTypeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/errorTypeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/modeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/modeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierCompanyLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierCompanyLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/shipperCompanyLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/shipperCompanyLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/railcarrierLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/railcarrierLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/payeeLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/payeeLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/businessUnitMapForLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/businessUnitMapForLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/shipThroughLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/shipThroughLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/commodityLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/commodityLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/businessUnitMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffCodeService/tariffcodeservice/businessUnitMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/modes'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/modes' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/serviceLevels'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/serviceLevels' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/sizeUOMs'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/sizeUOMs' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/zones'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/zones' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/comparisionMethods'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/comparisionMethods' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/baseUOMs'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/baseUOMs' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/businessUnitMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ChargeableSizeService/chargeableSizeService/businessUnitMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getBusinessUnitMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getBusinessUnitMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getModeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getModeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getProtectionLevelMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getProtectionLevelMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getServiceLevelMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getServiceLevelMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getTariffCodeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getTariffCodeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getServiceLevelMapForFilter'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getServiceLevelMapForFilter' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getProtectionLevelMapForFilter'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getProtectionLevelMapForFilter' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getModeMapForFilter'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getModeMapForFilter' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getBuMapForFilter'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneService/tariffzoneservices/getBuMapForFilter' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getCurrencyMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getCurrencyMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getPackageTypeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getPackageTypeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getCommodityClassMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getCommodityClassMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getRateCalcMethodMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getRateCalcMethodMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getSizeUOMMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateService/tariffzonerateservice/getSizeUOMMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/getZoneMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/getZoneMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/getTimeUOMMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffTransitTimeService/tarifftransittimeservice/getTimeUOMMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/MassExpirationService/massexpirationservices/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/MassExpirationService/massexpirationservices/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/MassExpirationService/massexpirationservices/updateRecord'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/MassExpirationService/massexpirationservices/updateRecord' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/getRateZonesList'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/getRateZonesList' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/getZoneMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffLaneService/tarifflaneservice/getZoneMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/carrCompanyMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/carrCompanyMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/shipCompanyMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/shipCompanyMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/companyTypeMap'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneForecastDataService/carrierLaneForecastDataService/companyTypeMap' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/CarrierLaneDetailCapacityDataService/carrierLaneDetailCapacityDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TransportationForecastDataService/transportationForecastDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/TariffZoneRateTierService/tariffzoneratetierservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/ShipperViewForecastDataService/shipperViewForecastDataService/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'VCFP');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RailRoutService/railservice/'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RailRoutService/railservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RailRoutService/railservice/{recordMarker}'  ,'CNTRMGT'  ,2 ,'PUT'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RailRoutService/railservice/{recordMarker}' AND MODULE='CNTRMGT' AND HTTP_METHOD='PUT' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RailRoutService/railservice/'  ,'CNTRMGT'  ,2 ,'POST'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RailRoutService/railservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='POST' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RailRoutService/railservice/'  ,'CNTRMGT'  ,2 ,'DELETE'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RailRoutService/railservice/' AND MODULE='CNTRMGT' AND HTTP_METHOD='DELETE' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RailRoutService/railservice/railDetail'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RailRoutService/railservice/railDetail' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RailRoutService/railservice/railPath'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RailRoutService/railservice/railPath' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'ACR');
 
  INSERT INTO RESOURCES(RESOURCE_ID,URI,MODULE,URI_TYPE_ID,HTTP_METHOD  ) VALUES  ( SEQ_RESOURCE_ID.nextval ,'/services/rest/contractmanagement/RailRoutService/railservice/railSplc'  ,'CNTRMGT'  ,2 ,'GET'  ); 
 
 INSERT INTO RESOURCE_PERMISSION(RESOURCE_ID,PERMISSION_CODE) VALUES  (( SELECT RESOURCE_ID FROM RESOURCES WHERE URI='/services/rest/contractmanagement/RailRoutService/railservice/railSplc' AND MODULE='CNTRMGT' AND HTTP_METHOD='GET' ),'ACR');
 
 
COMMIT;



-- DBTicket CMGT-550

ALTER TABLE lane_accessorial ADD (commodity_code_id NUMBER(12,0) , max_range_commodity_code_id NUMBER(12,0));
ALTER TABLE accessorial_rate ADD (commodity_code_id NUMBER(12,0) , max_range_commodity_code_id NUMBER(12,0));

COMMENT ON COLUMN lane_accessorial.commodity_code_id IS 'Commodity Code Id';
COMMENT ON COLUMN lane_accessorial.max_range_commodity_code_id IS 'Max Range Commodity Code Id';
COMMENT ON COLUMN accessorial_rate.commodity_code_id IS 'Commodity Code Id';
COMMENT ON COLUMN accessorial_rate.max_range_commodity_code_id IS 'Max Range Commodity Code Id';


-- DBTicket CMGT-542

-- UPDATE xfield
-- SET xpicks       = NULL,
--   field_config   = '{buUrl : ''/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/businessUnitMapForLookup''}'
-- WHERE xscreen_id = 240004
-- AND name         = 'carrierCode';

-- COMMIT;


-- DBTicket CMGT-569

MERGE INTO OBJECT_TYPE L USING
(SELECT 'RR' OBJECT_TYPE, 'Rail route' DESCRIPTION FROM DUAL
) B ON (L.OBJECT_TYPE = B.OBJECT_TYPE AND L.DESCRIPTION = B.DESCRIPTION)
WHEN NOT MATCHED THEN
  INSERT (OBJECT_TYPE,DESCRIPTION
    ) VALUES
    ('RR','Rail route'
    );
    
COMMIT;    

 
 
-- DBTicket CMGT-632
 
-- UPDATE xfield
 --SET is_default   = 1 ,
 --  is_sortable    = 1
-- WHERE xscreen_id = 240002
-- AND name         = 'tariffCode';
 
-- COMMIT;

 
 
-- DBTicket CMGT-642

--MERGE INTO XSCREEN L
--     USING (SELECT 'Rail' NAME, '240014' XSCREEN_ID FROM DUAL) B
--        ON (L.NAME = B.NAME AND L.XSCREEN_ID = B.XSCREEN_ID)
--WHEN NOT MATCHED
--THEN
--   insert 
--       (XSCREEN_ID,
--        NAME,
--                DESCRIPTION,
--                SCREEN_TYPE,
--                SCREEN_VERSION,
--                SCREEN_MODE,
--                IS_RESIZABLE,
--                CREATED_SOURCE_TYPE,
--                CREATED_DTTM,
--                LAST_UPDATED_SOURCE_TYPE,
--                LAST_UPDATED_DTTM,
--                DEFAULT_X,
--                DEFAULT_Y,
--                DEFAULT_WIDTH,
--                DEFAULT_HEIGHT)
--                values
--                (240014,
--                'Rail',
--                'Rail Rout',
--                1,
--                2,
--                0,
--                1,
--                1,
--                systimestamp,
--                1,
--                systimestamp,
--                125,
--                125,
--                600,
--                400);

COMMIT;



--MERGE INTO XSCREEN L
--     USING (SELECT 'Rail' NAME FROM DUAL) B
--        ON (L.NAME = B.NAME)
--WHEN NOT MATCHED
--THEN
--   insert 
--       (XSCREEN_ID,
--        NAME,
--                DESCRIPTION,
--                SCREEN_TYPE,
--                SCREEN_VERSION,
--                SCREEN_MODE,
--                IS_RESIZABLE,
--                CREATED_SOURCE_TYPE,
--                CREATED_DTTM,
--                LAST_UPDATED_SOURCE_TYPE,
--                LAST_UPDATED_DTTM,
--                DEFAULT_X,
--                DEFAULT_Y,
--                DEFAULT_WIDTH,
--                DEFAULT_HEIGHT)
--                values
--                (240014,
--                'Rail',
--                'Rail Rout',
--                1,
--                2,
--                0,
--                1,
--                1,
--                systimestamp,
--                1,
--                systimestamp,
--                125,
--                125,
--                600,
--                400);

----MERGE INTO XFIELD A USING
----(SELECT 'rule11' NAME, 240014 XSCREEN_ID FROM DUAL
----) B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----  INSERT
----    (
----      XFIELD_ID,
----      NAME,
----      DESCRIPTION,
----      XSCREEN_ID,
----      IS_DISPLAYABLE,
----      IS_FILTERABLE,
----      IS_MOVABLE,
----      IS_HEADER,
----      DATATYPE,
----      XTYPE,
----      XURL,
----      XPICKS,
----      CREATED_SOURCE,
----      CREATED_SOURCE_TYPE,
----      CREATED_DTTM,
----      LAST_UPDATED_SOURCE,
----      LAST_UPDATED_SOURCE_TYPE,
----      LAST_UPDATED_DTTM,
----      BASE_PART_NAME,
----      BASE_SECTION_NAME,
----      IS_REQUIRED
----    )
----    VALUES
----    (
----      seq_xfield_id.NEXTVAL,
----      'rule11',
----      'Rule 11',
----      240014,
----      1,
----      1,
----      1,
----      1,
----      0,
----      1,
----      NULL,
----      'true,false',
----      NULL,
----      1,
----      systimestamp,
----      NULL,
----      1,
----      systimestamp,
----      'GENERAL',
----      'GENERAL',
----      0
----    );
----
----  MERGE INTO XFIELD A USING
----  (SELECT 'Rail_Route_Id' NAME, 240014 XSCREEN_ID FROM DUAL
----  )
----  B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----  INSERT
----    (
----      XFIELD_ID,
----      NAME,
----      DESCRIPTION,
----      XSCREEN_ID,
----      IS_DISPLAYABLE,
----      IS_FILTERABLE,
----      IS_MOVABLE,
----      IS_HEADER,
----      DATATYPE,
----      XTYPE,
----      XURL,
----      XPICKS,
----      CREATED_SOURCE,
----      CREATED_SOURCE_TYPE,
----      CREATED_DTTM,
----      LAST_UPDATED_SOURCE,
----      LAST_UPDATED_SOURCE_TYPE,
----      LAST_UPDATED_DTTM,
----      BASE_PART_NAME,
----      BASE_SECTION_NAME,
----      IS_REQUIRED
----    )
----    VALUES
----    (
----      seq_xfield_id.NEXTVAL,
----      'Rail_Route_Id',
----      'Rail Route Id',
----      240014,
----      1,
----      1,
----      1,
----      1,
----      0,
----      0,
----      NULL,
----      NULL,
----      NULL,
----      1,
----      systimestamp,
----      NULL,
----      1,
----      systimestamp,
----      'GENERAL',
----      'GENERAL',
----      0
----    );
----  
----  COMMIT;
----  
  
  
   
-- DBTicket CMGT-645

--MERGE INTO XSCREEN L USING
--(SELECT 'Shipment List Grid' NAME, '440060' XSCREEN_ID FROM DUAL
--) B ON (L.NAME = B.NAME AND L.XSCREEN_ID = B.XSCREEN_ID)
--WHEN NOT MATCHED THEN
--  Insert (XSCREEN_ID,NAME,DESCRIPTION,SCREEN_TYPE,SCREEN_VERSION,SCREEN_MODE,IS_RESIZABLE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,DEFAULT_X,DEFAULT_Y,DEFAULT_WIDTH,DEFAULT_HEIGHT,CUSTOM_ATTRIB_OBJ_TYPE) values (440060,'Shipment List Grid','Shipment List Grid Config',1,7,0,1,null,1,systimestamp,null,1, systimestamp ,125,125,600,400,null);
--
--
--MERGE INTO XSCREEN L
--     USING (SELECT 'Distribution Order List Grid' NAME, '440020' XSCREEN_ID FROM DUAL) B
--        ON (L.NAME = B.NAME AND L.XSCREEN_ID = B.XSCREEN_ID)
--WHEN NOT MATCHED
--THEN
--   Insert (XSCREEN_ID,NAME,DESCRIPTION,SCREEN_TYPE,SCREEN_VERSION,SCREEN_MODE,IS_RESIZABLE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,DEFAULT_X,DEFAULT_Y,DEFAULT_WIDTH,DEFAULT_HEIGHT,CUSTOM_ATTRIB_OBJ_TYPE) values (440020,'Distribution Order List Grid','Distribution Order List Grid Config','1','7','0','1',null,'1',systimestamp,null,'1',systimestamp,125,125,600,400,'ORDR');
--


COMMIT;	



-- DBTicket CMGT-669
 
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE TARIFF  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE COMMODITY_CLASS_TIER  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE TARIFF_ZONE_RATE_TIER ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE CM_RATE_TYPE  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE ACCESSORIAL_GROUP ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE ACCESSORIAL_PARAM_SET ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE ACCESSORIAL_RATE  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE ACCESSORIAL_TYPE  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE BULK_RATING_RULES ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE CARRIER_FACILITY  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE CARRIER_LABEL ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE CARRIER_ROUTING_CODE  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE CARR_PARAM_ACC_EXCLUDED_LIST  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE FUEL_INDEX  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE FUEL_RATE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE FUEL_SURCHARGE  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE LANE_ACC_EXCLUDED_LIST  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE LANE_STATUS ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE MIN_DENSITY_PARAM ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE OPTION_GROUP  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE TARIFF_RATE_TYPE  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE PATH_PRODUCT_CLASS  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE TARIFF_TRANSIT_TIME ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE TARIFF_ZONE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE RATE_CALC_METHOD  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE TARIFF_CODE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE RG_SHIPPING_PARAM ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE SAILING_SCHDL_BY_DATE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE SAILING_SCHDL_BY_WEEK ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE SAILING_SCHDL_BY_WEEK_DAY ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE SHIP_VIA  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE SHIP_VIA_INCL_ACCESSORIAL ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE STATIC_CALENDAR ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE STATIC_COMPANY_RULE_ATTRIBUTE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE STATIC_ROUTE_CALENDAR ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE STATIC_ROUTE_STOP ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE STATIC_RULE_ATTRIBUTE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE STATIC_RULE_ROUTE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE SURGE_CAPACITY  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE PATH_WAYPOINT ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE PATH  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE TARIFF_ZONE_RATE  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
BEGIN EXECUTE IMMEDIATE 'ALTER TABLE TARIFF_LANE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
/
 
 
 
----DT_OVERRIDE_DETAIL  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
----DT_OVERRIDE_WINDOW  ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
----DT_OVERRIDE ADD (CREATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL , LAST_UPDATED_DTTM TIMESTAMP)'; EXCEPTION WHEN OTHERS THEN  NULL; END;
 
 
 
CREATE OR REPLACE TRIGGER ADT_OVR_DT_LUD_TRG BEFORE
  UPDATE ON DT_OVERRIDE_DETAIL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER ACCESS_GP_LUD_TRG BEFORE
  UPDATE ON ACCESSORIAL_GROUP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER ACCESS_PAR_LUD_TRG BEFORE
  UPDATE ON ACCESSORIAL_PARAM_SET FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER ACCESS_RAT_LUD_TRG BEFORE
  UPDATE ON ACCESSORIAL_RATE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER ACCESS_TYP_LUD_TRG BEFORE
  UPDATE ON ACCESSORIAL_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER BULK_R_LUD_TRG BEFORE
  UPDATE ON BULK_RATING_RULES FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER CARRIE_FAC_LUD_TRG BEFORE
  UPDATE ON CARRIER_FACILITY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER CARRIE_LAB_LUD_TRG BEFORE
  UPDATE ON CARRIER_LABEL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER CARRIE_ROU_LUD_TRG BEFORE
  UPDATE ON CARRIER_ROUTING_CODE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER CARR_P_LUD_TRG BEFORE
  UPDATE ON CARR_PARAM_ACC_EXCLUDED_LIST FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER CM_RAT_LUD_TRG BEFORE
  UPDATE ON CM_RATE_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER COMMOD_LUD_TRG BEFORE
  UPDATE ON COMMODITY_CLASS_TIER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER FUEL_I_LUD_TRG BEFORE
  UPDATE ON FUEL_INDEX FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER FUEL_R_LUD_TRG BEFORE
  UPDATE ON FUEL_RATE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER FUEL_S_LUD_TRG BEFORE
  UPDATE ON FUEL_SURCHARGE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER LANE_A_LUD_TRG BEFORE
  UPDATE ON LANE_ACC_EXCLUDED_LIST FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER LANE_S_LUD_TRG BEFORE
  UPDATE ON LANE_STATUS FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER MIN_DE_LUD_TRG BEFORE
  UPDATE ON MIN_DENSITY_PARAM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER OPTION_LUD_TRG BEFORE
  UPDATE ON OPTION_GROUP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER PATH_P_LUD_TRG BEFORE
  UPDATE ON PATH_PRODUCT_CLASS FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER RATE_C_LUD_TRG BEFORE
  UPDATE ON RATE_CALC_METHOD FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER RG_SHI_LUD_TRG BEFORE
  UPDATE ON RG_SHIPPING_PARAM FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER SAIL_SH_DT_LUD_TRG BEFORE
  UPDATE ON SAILING_SCHDL_BY_DATE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER SAIL_SH_WK_LUD_TRG BEFORE
  UPDATE ON SAILING_SCHDL_BY_WEEK FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER SAIL_SH_WD_LUD_TRG BEFORE
  UPDATE ON SAILING_SCHDL_BY_WEEK_DAY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER SHIP_V_LUD_TRG BEFORE
  UPDATE ON SHIP_VIA FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER SHIP_V_ACC_LUD_TRG BEFORE
  UPDATE ON SHIP_VIA_INCL_ACCESSORIAL FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER STATIC_CAL_LUD_TRG BEFORE
  UPDATE ON STATIC_CALENDAR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER STATIC_COM_LUD_TRG BEFORE
  UPDATE ON STATIC_COMPANY_RULE_ATTRIBUTE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER STAT_RO_CA_LUD_TRG BEFORE
  UPDATE ON STATIC_ROUTE_CALENDAR FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER STAT_RO_ST_LUD_TRG BEFORE
  UPDATE ON STATIC_ROUTE_STOP FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER STA_RL_ATT_LUD_TRG BEFORE
  UPDATE ON STATIC_RULE_ATTRIBUTE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER STA_RL_RO_LUD_TRG BEFORE
  UPDATE ON STATIC_RULE_ROUTE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER SURGE_CAP_LUD_TRG BEFORE
  UPDATE ON SURGE_CAPACITY FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER TARIFF_LUD_TRG BEFORE
  UPDATE ON TARIFF FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER TARIFF_CD_LUD_TRG BEFORE
  UPDATE ON TARIFF_CODE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER TARIFF_RT_LUD_TRG BEFORE
  UPDATE ON TARIFF_RATE_TYPE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER TARIFF_TRA_LUD_TRG BEFORE
  UPDATE ON TARIFF_TRANSIT_TIME FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER TARIFF_ZN_LUD_TRG BEFORE
  UPDATE ON TARIFF_ZONE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER TARIFF_ZR_LUD_TRG BEFORE
  UPDATE ON TARIFF_ZONE_RATE_TIER FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER PATH_LUD_TRG BEFORE
  UPDATE ON PATH FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER PATH_W_LUD_TRG BEFORE
  UPDATE ON PATH_WAYPOINT FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER TARIFF_LAN_LUD_TRG BEFORE
  UPDATE ON TARIFF_LANE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/
CREATE OR REPLACE TRIGGER TARIFF_ZOR_LUD_TRG BEFORE
  UPDATE ON TARIFF_ZONE_RATE FOR EACH ROW BEGIN :new.LAST_UPDATED_DTTM := SYSTIMESTAMP ;
END;
/




-- DBTicket CMGT-694

ALTER TABLE IMPORT_LANE_ACCESSORIAL ADD (commodity_code_id NUMBER(12,0) , max_range_commodity_code_id NUMBER(12,0));
COMMENT ON COLUMN IMPORT_LANE_ACCESSORIAL.commodity_code_id IS 'Commodity Code Id';
COMMENT ON COLUMN IMPORT_LANE_ACCESSORIAL.max_range_commodity_code_id IS 'Max Range Commodity Code Id';
   
-- DBTicket CMGT-697


----update xfield set is_displayable=0 where xscreen_id=240004 and name='labelTypeCode';
----
----update xfield set is_displayable=0 where xscreen_id=240004 and name='executionLevelCode';
----update xfield set is_displayable=0 where xscreen_id=240004 and name='billShipViaCode';
----
----update xfield set is_displayable=0 where xscreen_id=240004 and name='trackingNbrRequired';
----
----update xfield set is_displayable=0 where xscreen_id=240004 and name='serviceLevelIconCode' ;
----
----commit;   



-- DBTicket CMGT-610

----exec sequpdt('XFIELD','XFIELD_ID','SEQ_XFIELD_ID');
----exec sequpdt('LABEL','LABEL_ID','SEQ_LABEL_ID');
----exec sequpdt('XSCREEN_LABEL','XSCREEN_LABEL_ID','SEQUENCE_XSCREEN_LABEL_ID');
----
----MERGE INTO XFIELD A USING
----(SELECT 'origin' NAME, 24001 XSCREEN_ID FROM DUAL
----) B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----INSERT
----(
----xfield_id,
----name,
----description,
----xscreen_id,
----is_displayable,
----is_filterable,
----is_movable,
----is_header,
----datatype,
----xtype,
----xurl,
----xpicks,
----created_source,
----created_source_type,
----created_dttm,
----last_updated_source,
----last_updated_source_type,
----last_updated_dttm,
----BASE_PART_NAME,       
----BASE_SECTION_NAME,    
----IS_REQUIRED,    
----IS_ARCHIVE_FILTERABLE,
----IS_KEY,             
----RELATED_SCREEN_ID,    
----XPAGE_ID,             
----FIELD_CONFIG,         
----IS_DEFAULT,           
----IS_ARCHIVE,           
----DEFAULT_POSITION,     
----IS_EXPORTABLE,        
----MODULE,               
----LABEL_OVERRIDE_KEY,   
----IS_SORTABLE,          
----IS_ALWAYS_SELECTED   
----)
----VALUES
----(seq_xfield_id.NEXTVAL,'origin','Origin Facility',24001,1,1,1,1,0,3,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup','ctmgt.facilityLookUp',null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,1,0,0,0,null,null,0,1);
----MERGE INTO XFIELD A USING
----(SELECT 'destination' NAME, 24001 XSCREEN_ID FROM DUAL
----) B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----INSERT
----(
----xfield_id,
----name,
----description,
----xscreen_id,
----is_displayable,
----is_filterable,
----is_movable,
----is_header,
----datatype,
----xtype,
----xurl,
----xpicks,
----created_source,
----created_source_type,
----created_dttm,
----last_updated_source,
----last_updated_source_type,
----last_updated_dttm,
----BASE_PART_NAME,       
----BASE_SECTION_NAME,    
----IS_REQUIRED,    
----IS_ARCHIVE_FILTERABLE,
----IS_KEY,             
----RELATED_SCREEN_ID,    
----XPAGE_ID,             
----FIELD_CONFIG,         
----IS_DEFAULT,           
----IS_ARCHIVE,           
----DEFAULT_POSITION,     
----IS_EXPORTABLE,        
----MODULE,               
----LABEL_OVERRIDE_KEY,   
----IS_SORTABLE,          
----IS_ALWAYS_SELECTED   
----)
----VALUES
----(seq_xfield_id.NEXTVAL,'destination','Destination Facility',24001,1,1,1,1,0,3,'/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup','ctmgt.facilityLookUp',null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,1,0,0,0,null,null,0,1);
----MERGE INTO XFIELD A USING
----(SELECT 'deviation' NAME, 24001 XSCREEN_ID FROM DUAL
----) B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----INSERT
----(
----xfield_id,
----name,
----description,
----xscreen_id,
----is_displayable,
----is_filterable,
----is_movable,
----is_header,
----datatype,
----xtype,
----xurl,
----xpicks,
----created_source,
----created_source_type,
----created_dttm,
----last_updated_source,
----last_updated_source_type,
----last_updated_dttm,
----BASE_PART_NAME,       
----BASE_SECTION_NAME,    
----IS_REQUIRED,    
----IS_ARCHIVE_FILTERABLE,
----IS_KEY,             
----RELATED_SCREEN_ID,    
----XPAGE_ID,             
----FIELD_CONFIG,         
----IS_DEFAULT,           
----IS_ARCHIVE,           
----DEFAULT_POSITION,     
----IS_EXPORTABLE,        
----MODULE,               
----LABEL_OVERRIDE_KEY,   
----IS_SORTABLE,          
----IS_ALWAYS_SELECTED   
----)
----VALUES
----(seq_xfield_id.NEXTVAL,'deviation','Deviation:Absolute',24001,1,1,1,1,2,0,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,1,0,0,0,null,null,0,1);
----MERGE INTO XFIELD A USING
----(SELECT 'mode' NAME, 24001 XSCREEN_ID FROM DUAL
----) B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----INSERT
----(
----xfield_id,
----name,
----description,
----xscreen_id,
----is_displayable,
----is_filterable,
----is_movable,
----is_header,
----datatype,
----xtype,
----xurl,
----xpicks,
----created_source,
----created_source_type,
----created_dttm,
----last_updated_source,
----last_updated_source_type,
----last_updated_dttm,
----BASE_PART_NAME,       
----BASE_SECTION_NAME,    
----IS_REQUIRED,    
----IS_ARCHIVE_FILTERABLE,
----IS_KEY,             
----RELATED_SCREEN_ID,    
----XPAGE_ID,             
----FIELD_CONFIG,         
----IS_DEFAULT,           
----IS_ARCHIVE,           
----DEFAULT_POSITION,     
----IS_EXPORTABLE,        
----MODULE,               
----LABEL_OVERRIDE_KEY,   
----IS_SORTABLE,          
----IS_ALWAYS_SELECTED   
----)
----VALUES
----(seq_xfield_id.NEXTVAL,'mode','Mode',24001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,1,0,0,0,null,null,0,0);
----MERGE INTO XFIELD A USING
----(SELECT 'forecastVolume' NAME, 24001 XSCREEN_ID FROM DUAL
----) B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----INSERT
----(
----xfield_id,
----name,
----description,
----xscreen_id,
----is_displayable,
----is_filterable,
----is_movable,
----is_header,
----datatype,
----xtype,
----xurl,
----xpicks,
----created_source,
----created_source_type,
----created_dttm,
----last_updated_source,
----last_updated_source_type,
----last_updated_dttm,
----BASE_PART_NAME,       
----BASE_SECTION_NAME,    
----IS_REQUIRED,    
----IS_ARCHIVE_FILTERABLE,
----IS_KEY,             
----RELATED_SCREEN_ID,    
----XPAGE_ID,             
----FIELD_CONFIG,         
----IS_DEFAULT,           
----IS_ARCHIVE,           
----DEFAULT_POSITION,     
----IS_EXPORTABLE,        
----MODULE,               
----LABEL_OVERRIDE_KEY,   
----IS_SORTABLE,          
----IS_ALWAYS_SELECTED   
----)
----VALUES
----(seq_xfield_id.NEXTVAL,'forecastVolume','Forecast Volume',24001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,1,0,0,0,null,null,0,0);
----MERGE INTO XFIELD A USING
----(SELECT 'normalVolume' NAME, 24001 XSCREEN_ID FROM DUAL
----) B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----INSERT
----(
----xfield_id,
----name,
----description,
----xscreen_id,
----is_displayable,
----is_filterable,
----is_movable,
----is_header,
----datatype,
----xtype,
----xurl,
----xpicks,
----created_source,
----created_source_type,
----created_dttm,
----last_updated_source,
----last_updated_source_type,
----last_updated_dttm,
----BASE_PART_NAME,       
----BASE_SECTION_NAME,    
----IS_REQUIRED,    
----IS_ARCHIVE_FILTERABLE,
----IS_KEY,             
----RELATED_SCREEN_ID,    
----XPAGE_ID,             
----FIELD_CONFIG,         
----IS_DEFAULT,           
----IS_ARCHIVE,           
----DEFAULT_POSITION,     
----IS_EXPORTABLE,        
----MODULE,               
----LABEL_OVERRIDE_KEY,   
----IS_SORTABLE,          
----IS_ALWAYS_SELECTED   
----)
----VALUES
----(seq_xfield_id.NEXTVAL,'normalVolume','Normal Volume',24001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,1,0,0,0,null,null,0,0);
----COMMIT;

-- DBTicket CMGT-704

----delete from xfield_filter_xref where xfield_id in (
----select x.xfield_id from xfield x where x.xscreen_id=24002);
----
----delete from xfield_custom_attrib where xfield_id in (
----select x.xfield_id from xfield x where x.xscreen_id=24002);
----
----delete from xfield_process_info where xfield_id in (
----select x.xfield_id from xfield x where x.xscreen_id=24002);
----
----delete from xfield_restriction where xfield_id in (
----select x.xfield_id from xfield x where x.xscreen_id=24002);
----
----delete from xfield_xcolumn_xref where xfield_id in (
----select x.xfield_id from xfield x where x.xscreen_id=24002);
----
----delete from xlayout_field where xfield_id in (
----select x.xfield_id from xfield x where x.xscreen_id=24002);
----
----delete  from xfield where xscreen_id=24002;
----
----
----MERGE INTO XFIELD A
----     USING (SELECT 'carrierCompany' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'carrierCompany',
----                 'Carrier Company',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 2,
----                 3,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierCompanyLookup',
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 1,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'forecastDate' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'forecastDate',
----                 'Forecast Date',
----                 24002,
----                 1,
----                 1,
----                 1,
----                 1,
----                 5,
----                 4,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'deviation' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'deviation',
----                 'Deviation:Absolute',
----                 24002,
----                 1,
----                 1,
----                 0,
----                 1,
----                 2,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'deviationPercentage' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'deviationPercentage',
----                 'Deviation:Percentage',
----                 24002,
----                 1,
----                 1,
----                 0,
----                 1,
----                 4,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'searchType' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'searchType',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 1,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/searchTypeMap',
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'laneId' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'laneId',
----                 'Lane Id',
----                 24002,
----                 1,
----                 1,
----                 1,
----                 1,
----                 2,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'origin' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'origin',
----                 'Origin Facility',
----                 24002,
----                 1,
----                 1,
----                 1,
----                 1,
----                 0,
----                 3,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup',
----                 'ctmgt.facilityLookUp',
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'originCity' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'originCity',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'originState' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'originState',
----                 'Desc',
----                 24002,
----                 0,
----                1,
----                 1,
----                 1,
----                 0,
----                 1,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/stateProvMap',
----                 NULL,
----                NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'originPostalCode' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'originPostalCode',
----                 'Origin Postal Code',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'originCounty' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'originCounty',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'originCountry' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'originCountry',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 1,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/countryMap',
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'originZone' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'originZone',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 2,
----                 1,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneMap',
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'destination' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'destination',
----                 'Destination Facility',
----                 24002,
----                 1,
----                 1,
----                 1,
----                 1,
----                 0,
----                 3,
----                '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup',
----                 'ctmgt.facilityLookUp',
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'destinationCity' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'destinationCity',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'destinationState' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'destinationState',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 1,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/stateProvMap',
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'destinationPostalCode' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----              XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'destinationPostalCode',
----                 'Destination Postal Code',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'destinationCounty' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'destinationCounty',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'destinationCountry' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'destinationCountry',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 0,
----                 1,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/countryMap',
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'destinationZone' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'destinationZone',
----                 'Desc',
----                 24002,
----                 0,
----                 1,
----                 1,
----                 1,
----                 2,
----                 1,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneMap',
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'mode' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'mode',
----                 'Desc',
----                 24002,
----                 1,
----                 1,
----                 1,
----                 1,
----                 2,
----                 1,
----                 '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/modeMap',
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 0,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 1,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'contractCapacity' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'contractCapacity',
----                 'Contract Capacity',
----                 24002,
----                 1,
----                 0,
----                 0,
----                 1,
----                 0,
----                 1,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 0,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'normalVolume' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'normalVolume',
----                 'Normal Volume',
----                 24002,
----                 1,
----                 0,
----                 0,
----                 1,
----                 0,
----                 1,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 0,
----                 0);
----
----MERGE INTO XFIELD A
----     USING (SELECT 'forecastVolume' NAME, 24002 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   INSERT     (XFIELD_ID,
----               NAME,
----               DESCRIPTION,
----               XSCREEN_ID,
----               IS_DISPLAYABLE,
----               IS_FILTERABLE,
----               IS_MOVABLE,
----               IS_HEADER,
----               DATATYPE,
----               XTYPE,
----               XURL,
----               XPICKS,
----               CREATED_SOURCE,
----               CREATED_SOURCE_TYPE,
----               LAST_UPDATED_SOURCE,
----               LAST_UPDATED_SOURCE_TYPE,
----               BASE_PART_NAME,
----               BASE_SECTION_NAME,
----               IS_REQUIRED,
----               IS_ARCHIVE_FILTERABLE,
----               IS_KEY,
----               RELATED_SCREEN_ID,
----               XPAGE_ID,
----               FIELD_CONFIG,
----               IS_DEFAULT,
----               IS_ARCHIVE,
----               DEFAULT_POSITION,
----               IS_EXPORTABLE,
----               MODULE,
----               LABEL_OVERRIDE_KEY,
----               IS_SORTABLE,
----               IS_ALWAYS_SELECTED)
----       VALUES (
----                 seq_xfield_id.NEXTVAL,
----                 'forecastVolume',
----                 'Forecast Volume',
----                 24002,
----                 1,
----                 0,
----                 0,
----                 1,
----                 0,
----                 1,
----                NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 NULL,
----                 1,
----                 'GENERAL',
----                 'GENERAL',
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 NULL,
----                 1,
----                 0,
----                 0,
----                 0,
----                 NULL,
----                 NULL,
----                 0,
----                 0);
----                 
----COMMIT;                 
----
----
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='mode'),'LDF.MOT_ID','LANE_DTL_FORECAST LDF, COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='destination'),'LANE.D_FACILITY_ID','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='destinationCity'),'LANE.D_CITY','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='destinationCountry'),'LANE.D_COUNTRY_CODE','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='destinationCounty'),'LANE.D_COUNTY','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='destinationPostalCode'),'LANE.D_POSTAL_CODE','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='carrierCompany'),'LDF.TP_COMPANY_ID','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='origin'),'LANE.O_FACILITY_ID','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='originCity'),'LANE.O_CITY','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='originCountry'),'LANE.O_COUNTRY_CODE','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='originCounty'),'LANE.O_COUNTY','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='originPostalCode'),'LANE.O_POSTAL_CODE','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='originState'),'LANE.O_STATE_PROV','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='originZone'),'LANE.O_ZONE_ID','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='destinationState'),'LANE.D_STATE_PROV','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='destinationZone'),'LANE.D_ZONE_ID','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='deviation'),'FORECAST_VOLUME-NORMAL_VOLUME','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='deviationPercentage'),'NORMAL_VOLUME <> 0 AND( CAST ((FORECAST_VOLUME - NORMAL_VOLUME) AS FLOAT )/ NORMAL_VOLUME) * 100','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='forecastDate'),'LDF.FORECAST_DATE','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----Insert into xfield_filter_xref (XFIELD_ID,COLUMN_ALIAS,JOINED_TABLE,JOIN_CLAUSE,CREATED_SOURCE,CREATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE) values ((select xfield_id from xfield  where xscreen_id=24002 and name='laneId'),'LDF.LANE_ID','LANE_DTL_FORECAST LDF , COMB_LANE LANE','LDF.LANE_ID = LANE.LANE_ID',null,1,null,1);
----


COMMIT;




-- DBTicket CMGT-755


----update xfield  set is_displayable=0 where xscreen_id=240001 and name='isValid';
----update xfield  set name='global' where xscreen_id=240001 and name='Is Global';
----
----
----MERGE INTO XFIELD A
----     USING (SELECT 'useFakCommodity' NAME, 240001 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   Insert (XFIELD_ID,NAME,DESCRIPTION,XSCREEN_ID,IS_DISPLAYABLE,IS_FILTERABLE,IS_MOVABLE,IS_HEADER,DATATYPE,XTYPE,XURL,XPICKS,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,BASE_PART_NAME,BASE_SECTION_NAME,IS_REQUIRED,IS_ARCHIVE_FILTERABLE,IS_KEY,RELATED_SCREEN_ID,XPAGE_ID,FIELD_CONFIG,IS_DEFAULT,IS_ARCHIVE,DEFAULT_POSITION,IS_EXPORTABLE,MODULE,LABEL_OVERRIDE_KEY,IS_SORTABLE,IS_ALWAYS_SELECTED) values (seq_xfield_id.NEXTVAL,'useFakCommodity','useFakCommodity',240001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,0,0,0,0,null,null,0,0);
----   
----   MERGE INTO XFIELD A
----     USING (SELECT 'commodityClassCode' NAME, 240001 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   Insert (XFIELD_ID,NAME,DESCRIPTION,XSCREEN_ID,IS_DISPLAYABLE,IS_FILTERABLE,IS_MOVABLE,IS_HEADER,DATATYPE,XTYPE,XURL,XPICKS,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,BASE_PART_NAME,BASE_SECTION_NAME,IS_REQUIRED,IS_ARCHIVE_FILTERABLE,IS_KEY,RELATED_SCREEN_ID,XPAGE_ID,FIELD_CONFIG,IS_DEFAULT,IS_ARCHIVE,DEFAULT_POSITION,IS_EXPORTABLE,MODULE,LABEL_OVERRIDE_KEY,IS_SORTABLE,IS_ALWAYS_SELECTED) values (seq_xfield_id.NEXTVAL,'commodityClassCode','commodityClassCode',240001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,0,0,0,0,null,null,0,0);
----   
----   MERGE INTO XFIELD A
----     USING (SELECT 'stopOffCurrency' NAME, 240001 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   Insert (XFIELD_ID,NAME,DESCRIPTION,XSCREEN_ID,IS_DISPLAYABLE,IS_FILTERABLE,IS_MOVABLE,IS_HEADER,DATATYPE,XTYPE,XURL,XPICKS,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,BASE_PART_NAME,BASE_SECTION_NAME,IS_REQUIRED,IS_ARCHIVE_FILTERABLE,IS_KEY,RELATED_SCREEN_ID,XPAGE_ID,FIELD_CONFIG,IS_DEFAULT,IS_ARCHIVE,DEFAULT_POSITION,IS_EXPORTABLE,MODULE,LABEL_OVERRIDE_KEY,IS_SORTABLE,IS_ALWAYS_SELECTED) values (seq_xfield_id.NEXTVAL,'stopOffCurrency','stopOffCurrency',240001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,0,0,0,0,null,null,0,0);
----   
----   MERGE INTO XFIELD A
----     USING (SELECT 'cmCurrency' NAME, 240001 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   Insert (XFIELD_ID,NAME,DESCRIPTION,XSCREEN_ID,IS_DISPLAYABLE,IS_FILTERABLE,IS_MOVABLE,IS_HEADER,DATATYPE,XTYPE,XURL,XPICKS,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,BASE_PART_NAME,BASE_SECTION_NAME,IS_REQUIRED,IS_ARCHIVE_FILTERABLE,IS_KEY,RELATED_SCREEN_ID,XPAGE_ID,FIELD_CONFIG,IS_DEFAULT,IS_ARCHIVE,DEFAULT_POSITION,IS_EXPORTABLE,MODULE,LABEL_OVERRIDE_KEY,IS_SORTABLE,IS_ALWAYS_SELECTED) values (seq_xfield_id.NEXTVAL,'cmCurrency','cmCurrency',240001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,0,0,0,0,null,null,0,0);
----   
----   
----      MERGE INTO XFIELD A
----     USING (SELECT 'distanceUOM' NAME, 240001 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   Insert (XFIELD_ID,NAME,DESCRIPTION,XSCREEN_ID,IS_DISPLAYABLE,IS_FILTERABLE,IS_MOVABLE,IS_HEADER,DATATYPE,XTYPE,XURL,XPICKS,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,BASE_PART_NAME,BASE_SECTION_NAME,IS_REQUIRED,IS_ARCHIVE_FILTERABLE,IS_KEY,RELATED_SCREEN_ID,XPAGE_ID,FIELD_CONFIG,IS_DEFAULT,IS_ARCHIVE,DEFAULT_POSITION,IS_EXPORTABLE,MODULE,LABEL_OVERRIDE_KEY,IS_SORTABLE,IS_ALWAYS_SELECTED) values (seq_xfield_id.NEXTVAL,'distanceUOM','distanceUOM',240001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,0,0,0,0,null,null,0,0);
----   
----   
----   
----         MERGE INTO XFIELD A
----     USING (SELECT 'rateType' NAME, 240001 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   Insert (XFIELD_ID,NAME,DESCRIPTION,XSCREEN_ID,IS_DISPLAYABLE,IS_FILTERABLE,IS_MOVABLE,IS_HEADER,DATATYPE,XTYPE,XURL,XPICKS,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,BASE_PART_NAME,BASE_SECTION_NAME,IS_REQUIRED,IS_ARCHIVE_FILTERABLE,IS_KEY,RELATED_SCREEN_ID,XPAGE_ID,FIELD_CONFIG,IS_DEFAULT,IS_ARCHIVE,DEFAULT_POSITION,IS_EXPORTABLE,MODULE,LABEL_OVERRIDE_KEY,IS_SORTABLE,IS_ALWAYS_SELECTED) values (seq_xfield_id.NEXTVAL,'rateType','rateType',240001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,0,0,0,0,null,null,0,0);
----   
----   
----   MERGE INTO XFIELD A
----     USING (SELECT 'sizeUomId' NAME, 240001 XSCREEN_ID FROM DUAL) B
----        ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED
----THEN
----   Insert (XFIELD_ID,NAME,DESCRIPTION,XSCREEN_ID,IS_DISPLAYABLE,IS_FILTERABLE,IS_MOVABLE,IS_HEADER,DATATYPE,XTYPE,XURL,XPICKS,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM,BASE_PART_NAME,BASE_SECTION_NAME,IS_REQUIRED,IS_ARCHIVE_FILTERABLE,IS_KEY,RELATED_SCREEN_ID,XPAGE_ID,FIELD_CONFIG,IS_DEFAULT,IS_ARCHIVE,DEFAULT_POSITION,IS_EXPORTABLE,MODULE,LABEL_OVERRIDE_KEY,IS_SORTABLE,IS_ALWAYS_SELECTED) values (315864,'sizeUomId','sizeUomId',240001,1,0,0,1,0,1,null,null,null,1,systimestamp,null,1,systimestamp,'GENERAL','GENERAL',0,0,0,null,null,null,0,0,0,0,null,null,0,0);
----
----update xfield  set name='equipmentId' where xscreen_id=240001 and name='equipmentCode';
----
----
----update xfield set is_default=1 where 
----name in ('isValid','carrierCode','equipmentId','businessUnit','effectiveDate','expirationDate','global')  
----and xscreen_id=240001;
			

COMMIT;

---- DBTicket CMGT-778

----DELETE FROM permission
----      WHERE permission_code IN ('ICR', 'IRG', 'ITL');
	  
----COMMIT;

-- DBTicket CMGT-779

DELETE FROM billing_method
      WHERE description = 'MGE';

COMMIT;

-- DBTicket CMGT-977

----update xfield set xtype=7,IS_HEADER=1 where xscreen_id=240001 and name in ('global');

COMMIT;

-- DBTicket CMGT-985

----UPDATE xfield
----SET xurl         = '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/zoneLookup',
----  xpicks         = 'ctmgt.zoneLookup',
----  xtype          = 3
----WHERE xscreen_id =240003
----AND name        IN ('origZoneName','destZoneName');
----
----COMMIT;

-- DBTicket CMGT-559
---- Changes has been addressed in Oracle as part of CMGT-694


-- DBTicket CMGT-1230

COMMENT ON TABLE DT_OVERRIDE IS 'Stores Distance Time Overrides for a company';
COMMENT ON TABLE DT_OVERRIDE_WINDOW IS 'Stores Date Windows which is used by DT_OVERRIDE_DETAIL';
COMMENT ON TABLE DT_OVERRIDE_DETAIL IS 'Stores Distance Time Overrides specific to some time windows in a week';


COMMENT ON COLUMN DT_OVERRIDE_DETAIL.DEST_DAY_OF_WEEK IS 'Day of week at destination where this override is applicable';
COMMENT ON COLUMN DT_OVERRIDE_DETAIL.OVERRIDE_WINDOW_ID IS 'Maps the entry in DT_OVERRIDE_WINDOW to identify origin start and end times';
COMMENT ON COLUMN DT_OVERRIDE_DETAIL.DURATION IS 'Duration Time in Minutes';

COMMENT ON COLUMN DT_OVERRIDE.DURATION IS 'Duration Time in Minutes';

COMMENT ON COLUMN DT_OVERRIDE_WINDOW.WINDOW_VAL IS 'Indicates the Window Timings as a string';


COMMENT ON COLUMN ACCESSORIAL_RATE.INCOTERM_ID IS 'Surrogate key for the table (internal DB reference only)';
COMMENT ON COLUMN CARR_PRM_ACC_FEASIBLE_INCOTERM.INCOTERM_ID IS 'Surrogate key for the table (internal DB reference only)';
COMMENT ON COLUMN COMB_LANE.INCOTERM_ID IS 'Surrogate key for the table (internal DB reference only)';
COMMENT ON COLUMN IMPORT_RATING_LANE.INCOTERM_ID IS 'Surrogate key for the table (internal DB reference only)';

COMMENT ON COLUMN LANE_ACCESSORIAL.INCOTERM_ID IS 'Surrogate key for the table (internal DB reference only)';
COMMENT ON COLUMN PATH_SET.INCOTERM_ID IS 'Surrogate key for the table (internal DB reference only)';

COMMENT ON COLUMN TRANS_FORECAST.D_FAC_ALIAS_ID IS 'Destination Facility Alias ID';
COMMENT ON COLUMN TRANS_FORECAST.D_FAC_BU IS 'Destination Facility Business Unit';
COMMENT ON COLUMN TRANS_FORECAST.O_FAC_ALIAS_ID IS 'Origin Facility Alias ID';
COMMENT ON COLUMN TRANS_FORECAST.O_FAC_BU IS 'Origin Facility Business Unit';



-- DBTicket CMGT-1306

INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ACCESSORIAL_RATE_A_IU_TR_1','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ACCESSORIAL_RATE_EDED_BIU_TR','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ACCESSORI_AU_TR1','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ACCESS_CONTROL_AI_1','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ACCESS_GP_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ACCESS_PAR_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ACCESS_RAT_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ACCESS_TYP_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('ADT_OVR_DT_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('BULK_R_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('CARRIER_I_AU_TR1','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('CARRIE_FAC_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('CARRIE_LAB_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('CARRIE_ROU_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('CARR_P_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('CM_RAT_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('CM_RT_DSC_A_U_TR_1','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('DT_OVERRIDE_BIU_TR','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('DT_OVERRIDE_B_I_TR_1','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('DT_OVRIDE_LST_UPDT_DTTM_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('DT_OVRIDTL_LST_UPDT_DTTM_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('DT_OVRIDWIN_LST_UPDT_DTTM_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('FUEL_I_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('FUEL_R_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('FUEL_S_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('IMPORT_PATH_SET_BUI_TR','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('LANE_A_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('LANE_S_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('MIN_DENSITY_PARAM_B_I_TR_1','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('MIN_DE_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('OPTION_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('PATH_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('PATH_P_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('PATH_SET_BUI_TR','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('PATH_W_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('RATE_C_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('RG_SHI_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('SAIL_SH_DT_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('SAIL_SH_WD_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('SAIL_SH_WK_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('SHIP_V_ACC_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('SHIP_V_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('STATIC_CAL_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('STATIC_COM_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('STAT_RO_CA_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('STAT_RO_ST_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('STA_RL_ATT_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('STA_RL_RO_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('SURGE_CAP_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('TARIFF_CD_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('TARIFF_LAN_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('TARIFF_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('TARIFF_RT_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('TARIFF_TRA_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('TARIFF_ZN_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('TARIFF_ZOR_LUD_TRG','TRIGGER','CM','2014');
INSERT INTO  MANH_DATABASE_OBJECT_LIST (OBJECT_NAME,OBJECT_TYPE,MODULE,RELEASE_NUMBER) VALUES  ('TARIFF_ZR_LUD_TRG','TRIGGER','CM','2014');
COMMIT;



-- DBTicket CMGT-1365
----Only DB2 related changes

-- DBTicket CMGT-2185

EXEC SEQUPDT('STATIC_RULE_ATTRIBUTE','ATTRIB_ID','SEQ_ATTRIB_ID');

Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ITEM_FACILITY_MAPPING_ID','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'LPN_PER_TIER','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'TIER_PER_PLT','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CASE_SIZE_TYPE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PICK_RATE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'WAGE_VALUE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PACK_RATE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_PROC_RATE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUTO_SUB_CASE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ASSIGN_DYNAMIC_ACTV_PICK_SITE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ASSIGN_DYNAMIC_CASE_PICK_SITE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PICK_LOCN_ASSIGN_TYPE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PUTWY_TYPE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'DFLT_WAVE_PROC_TYPE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'XCESS_WAVE_NEED_PROC_TYPE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ALLOC_TYPE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VIOLATE_FIFO_ALLOC_QTY_MATCH','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'QV_ITEM_GRP','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'QUAL_INSPCT_ITEM_GRP','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MAX_UNITS_IN_DYNAMIC_ACTV','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MAX_CASES_IN_DYNAMIC_ACTV','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MAX_UNITS_IN_DYNAMIC_CASE_PICK','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MAX_CASES_IN_DYNAMIC_CASE_PICK','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CASE_CNT_DATE_TIME','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ACTV_CNT_DATE_TIME','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CASE_PICK_CNT_DATE_TIME','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VENDOR_CARTON_PER_TIER','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VENDOR_TIER_PER_PLT','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ORD_CARTON_PER_TIER','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ORD_TIER_PER_PLT','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'DFLT_CATCH_WT_METHOD','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'DFLT_DATE_MASK','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CARTON_BREAK_ATTR','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_SHORT_ALPHA_1','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_SHORT_ALPHA_2','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_ALPHA_1','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_ALPHA_2','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_ALPHA_3','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_NUMERIC_1','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_NUMERIC_2','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_NUMERIC_3','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_DATE_1','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MISC_DATE_2','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CHUTE_ASSIGN_TYPE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ACTV_REPL_ORGN','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'UIN_NBR','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'FIFO_RANGE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PRTL_CASE_ALLOC_THRESH_UNITS','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PRTL_CASE_PUTWY_THRESH_UNITS','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VENDOR_TAGGED_EPC_FLAG','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ITEM_AVG_WT','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'DFLT_MIN_FROM_PREV_LOCN_FLAG','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_MISC_1','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_MISC_2','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_MISC_3','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_MISC_4','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_MISC_5','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_MISC_6','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_ROTATE_EACHES_FLAG','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_ROTATE_INNERS_FLAG','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_ROTATE_BINS_FLAG','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_ROTATE_CASES_FLAG','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_3D_SLOTTING_FLAG','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_NEST_EACHES_FLAG','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_INCR_HT','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_INCR_LEN','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOT_INCR_WIDTH','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'NBR_OF_DYN_ACTV_PICK_PER_ITEM','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'NBR_OF_DYN_CASE_PICK_PER_ITEM','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_CREATED_SOURCE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_CREATED_SOURCE_TYPE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_CREATED_DTTM','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_LAST_UPDATED_SOURCE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_LAST_UPDATED_SOURCE_TYPE','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_LAST_UPDATED_DTTM','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MARK_FOR_DELETION','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ITEM_ID','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'FACILITY_ID','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'BUSINESS_PARTNER_ID','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AVERAGE_MOVEMENT','ITEM_FACILITY_MAPPING_WMS',32,'=,!=,IN','String','a');


Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ITEM_ID','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SIZE_RANGE_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SIZE_REL_POSN_IN_TABLE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VOLTY_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PKG_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PROD_SUB_GRP','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PROD_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PROD_LINE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SALE_GRP','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'COORD_1','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'COORD_2','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CARTON_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'UNIT_PRICE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'RETAIL_PRICE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'OPER_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MAX_CASE_QTY','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CUBE_MULT_QTY','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'NEST_VOL','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'NEST_CNT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'UNITS_PER_PICK_ACTIVE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'HNDL_ATTR_ACTIVE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'UNITS_PER_PICK_CASE_PICK','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'HNDL_ATTR_CASE_PICK','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'UNITS_PER_PICK_RESV','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'HNDL_ATTR_RESV','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PROD_LIFE_IN_DAY','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MAX_RECV_TO_XPIRE_DAYS','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AVG_DLY_DMND','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'WT_TOL_PCNT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CONS_PRTY_DATE_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CONS_PRTY_DATE_WINDOW','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CONS_PRTY_DATE_WINDOW_INCR','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ACTVTN_DATE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ALLOW_RCPT_OLDER_ITEM','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CRITCL_DIM_1','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CRITCL_DIM_2','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CRITCL_DIM_3','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MFG_DATE_REQD','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'XPIRE_DATE_REQD','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SHIP_BY_DATE_REQD','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ITEM_ATTR_REQD','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'BATCH_REQD','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PROD_STAT_REQD','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CNTRY_OF_ORGN_REQD','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VENDOR_ITEM_NBR','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PICK_WT_TOL_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PICK_WT_TOL_AMNT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MHE_WT_TOL_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MHE_WT_TOL_AMNT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'LOAD_ATTR','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'TEMP_ZONE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'TRLR_TEMP_ZONE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PKT_CONSOL_ATTR','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'BUYER_DISP_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CRUSH_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CONVEY_FLAG','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'STORE_DEPT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MERCH_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MERCH_GROUP','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_1','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_2','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_3','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_4','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_5','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_6','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_7','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_8','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_9','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_CODE_10','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_1','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SPL_INSTR_2','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PROMPT_FOR_VENDOR_ITEM_NBR','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PROMPT_PACK_QTY','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ECCN_NBR','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'EXP_LICN_NBR','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'EXP_LICN_XP_DATE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'EXP_LICN_SYMBOL','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ORGN_CERT_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'ITAR_EXEMPT_NBR','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'NMFC_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'FRT_CLASS','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'DFLT_BATCH_STAT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'DFLT_INCUB_LOCK','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'BASE_INCUB_FLAG','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'INCUB_DAYS','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'INCUB_HOURS','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SRL_NBR_BRCD_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MINOR_SRL_NBR_REQ','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'DUP_SRL_NBR_FLAG','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MAX_RCPT_QTY','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VOCOLLECT_BASE_WT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VOCOLLECT_BASE_QTY','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VOCOLLECT_BASE_ITEM','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PICK_WT_TOL_AMNT_ERROR','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PRICE_TKT_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MONETARY_VALUE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MV_CURRENCY_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CODE_DATE_PROMPT_METHOD_FLAG','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MIN_RECV_TO_XPIRE_DAYS','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MIN_PCNT_FOR_LPN_SPLIT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MIN_LPN_QTY_FOR_SPLIT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PROD_CATGRY','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_CREATED_SOURCE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_CREATED_SOURCE_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_CREATED_DTTM','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_LAST_UPDATED_SOURCE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_LAST_UPDATED_SOURCE_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'AUDIT_LAST_UPDATED_DTTM','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MARK_FOR_DELETION','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'TOP_SHELF_ELIGIBLE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VOCO_ABS_PICK_TOL_AMT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CARTON_CNT_DATE_TIME','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'TRANS_INVN_CNT_DATE_TIME','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'WORK_ORD_CNT_DATE_TIME','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SHELF_DAYS','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'VENDOR_MASTER_ID','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'NBR_OF_DYN_ACTV_PICK_PER_SKU','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'NBR_OF_DYN_CASE_PICK_PER_SKU','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'LET_UP_PRTY','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PREF_CRITERIA_FLAG','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PRODUCER_FLAG','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'NET_COST_FLAG','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MARKS_NBRS','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SLOTTING_OPT_STAT_CODE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'PRICE_TIX_AVAIL','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'MV_SIZE_UOM','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'UNITS_PER_GRAB_PLT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'HNDL_ATTR_PLT','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'HNDL_ATTR_ACT_UOM_ID','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'HNDL_ATTR_CASE_PICK_UOM_ID','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'SRL_NBR_REQD','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CC_UNIT_TOLER_VALUE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CC_WGT_TOLER_VALUE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CC_DLR_TOLER_VALUE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'CC_PCNT_TOLER_VALUE','ITEM_WMS',32,'=,!=,IN','String','a');
Insert into static_rule_attribute (ATTRIB_ID,ATTRIB_NAME,ENTITY_NAME,DOMAINGROUP_ID,OPERATOR,ATTRIBUTE_TYPE,BOUND_VARIABLE) values(SEQ_ATTRIB_ID.NEXTVAL,'DISPOSITION_TYPE','ITEM_WMS',32,'=,!=,IN','String','a');

COMMIT;


-- DBTicket CMGT-2218

DECLARE
   v_exists   NUMBER;
BEGIN
   SELECT COUNT (INDEX_NAME)
     INTO v_exists
     FROM USER_INDEXES
    WHERE TABLE_NAME = 'COMB_LANE' AND INDEX_NAME = 'COMB_LANE_IS_R_TC_IDX';

   IF v_exists = 0
   THEN
      EXECUTE IMMEDIATE
         'CREATE INDEX COMB_LANE_IS_R_TC_IDX ON COMB_LANE (IS_RATING,TC_COMPANY_ID) TABLESPACE CM_BST4K_IDX_TBS';
        END IF;
END;
/


  
  
-- DBTicket CMGT-2372
----DB2 Specific Change

-- DBTicket CMGT-2394

begin
  execute immediate('alter index IMPORT_RATING_LANE rename to IMPORT_RATING_LANE_UX1');
  exception
    when others then
      null;
end;
/


-- DBTicket CMGT-2443 REFL

EXEC sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
EXEC sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
INSERT
INTO permission
  (
    permission_id,
    permission_name ,
    is_active,
    permission_code
  )
  VALUES
  (
    SEQ_PERMISSION_ID.NEXTVAL,
    'Admin Override Capacity',
    1,
    'AOC'
  );
INSERT
INTO app_mod_perm
  (
    app_id,
    module_id,
    permission_id,
    row_uid
  )
  VALUES
  (
    (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG'
    )
    ,
    (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'
    ),
    (SELECT PERMISSION_ID
    FROM PERMISSION
    WHERE PERMISSION_NAME = 'Admin Override Capacity'
    ),
    SEQ_APP_MOD_PERM.nextval
  );

COMMIT;

-- DBTicket CMGT-2438

ALTER TABLE surge_capacity ADD IS_SURGE NUMBER(1) default 0 not null;
COMMENT ON COLUMN surge_capacity.IS_SURGE IS 'IS_SURGE';

-- DBTicket CMGT-2452 REFL

INSERT
INTO company_type_perm
  (
    company_type_id,
    permission_id,
    row_uid
  )
  VALUES
  (
    (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('carrier')),
    ( SELECT permission_id FROM permission WHERE permission_code ='AOC'
    ),
    (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    );
  
COMMIT;

-- DBTicket CMGT-2480

ALTER TABLE surge_capacity modify is_surge  NUMBER(1)  DEFAULT 1;   

-- DBTicket CMGT-2611
alter table COMB_LANE modify O_STATE_PROV varchar2(3 CHAR);
alter table IMPORT_PATH_SET modify O_STATE_PROV varchar2(3 CHAR);
alter table IMPORT_RATING_LANE modify O_STATE_PROV varchar2(3 CHAR);
alter table IMPORT_RG_LANE modify O_STATE_PROV varchar2(3 CHAR);
alter table IMPORT_SAILING_LANE modify O_STATE_PROV varchar2(3 CHAR);
alter table PATH_SET modify O_STATE_PROV varchar2(3 CHAR);


-- DBTicket DB-384

Alter table ACCESSORIAL_RATE modify ACCESSORIAL_PARAM_SET_ID number(15);

ALTER TABLE TARIFF_ZONE_RATE ADD COMMODITY_CLASS_TMP NUMBER(8);
UPDATE TARIFF_ZONE_RATE SET COMMODITY_CLASS_TMP=COMMODITY_CLASS;
update TARIFF_ZONE_RATE set COMMODITY_CLASS=NULL;
alter table TARIFF_ZONE_RATE modify COMMODITY_CLASS number (8);
update TARIFF_ZONE_RATE set COMMODITY_CLASS=COMMODITY_CLASS_TMP;
alter table TARIFF_ZONE_RATE drop column COMMODITY_CLASS_TMP;

alter table ACCESSORIAL_RATE  modify  EQUIPMENT_ID number (12);
alter table ACCESSORIAL_RATE  modify MOT_ID number (12);
alter table ACCESSORIAL_RATE  modify SERVICE_LEVEL_ID number (12);

ALTER TABLE SHIP_VIA modify BILL_SHIP_VIA_ID NUMBER(9);

ALTER TABLE TARIFF_ZONE_RATE ADD TARIFF_RATE_TYPE_TMP NUMBER(2);
UPDATE TARIFF_ZONE_RATE SET TARIFF_RATE_TYPE_TMP=TARIFF_RATE_TYPE;
update TARIFF_ZONE_RATE set TARIFF_RATE_TYPE=NULL;
alter table TARIFF_ZONE_RATE modify TARIFF_RATE_TYPE number (2);
update TARIFF_ZONE_RATE set TARIFF_RATE_TYPE=TARIFF_RATE_TYPE_TMP;
alter table TARIFF_ZONE_RATE drop column TARIFF_RATE_TYPE_TMP;

ALTER TABLE STATIC_RULE_ROUTE MODIFY RULE_ID NUMBER(10);

Commit;

-- DBTicket DB-589

ALTER table IMPORT_RG_LANE ADD (USE_EPI NUMBER(1) DEFAULT 0 NOT NULL,EPI_SERVICE_GROUP VARCHAR2(50));

COMMENT ON COLUMN IMPORT_RG_LANE.USE_EPI IS 'Use External Parcel Integration';
COMMENT ON COLUMN IMPORT_RG_LANE.EPI_SERVICE_GROUP IS 'EPI Service Group';

ALTER table COMB_LANE ADD (USE_EPI NUMBER(1) DEFAULT 0 NOT NULL,EPI_SERVICE_GROUP VARCHAR2(50));

COMMENT ON COLUMN COMB_LANE.USE_EPI IS 'Use External Parcel Integration';
COMMENT ON COLUMN COMB_LANE.EPI_SERVICE_GROUP IS 'EPI Service Group';

COMMIT;

-- DBTicket DB-777

ALTER TABLE static_route ADD (PRIORITY NUMBER(12),
                             DELIVERY_TYPE VARCHAR2(20) ,
                             EQUIPMENT_ID NUMBER(12),
                             DRIVER_TYPE_ID NUMBER(12),
                             DRIVER_CALENDAR TIMESTAMP,
                             DESG_CARRIER_CODE_ID  NUMBER(12),
                             DESG_SERVICE_LEVEL_ID NUMBER(12),
                             DESG_MOT_ID  NUMBER(12),
                             REFERENCE_FIELD_1 VARCHAR2(75),
                             REFERENCE_FIELD_2 VARCHAR2(75),
                             REFERENCE_FIELD_3 VARCHAR2(75),
                             REFERENCE_FIELD_4 VARCHAR2(75),
                             REFERENCE_FIELD_5 VARCHAR2(75));

							 
comment on column static_route.PRIORITY is 'Route Priority';
comment on column static_route.DELIVERY_TYPE  is 'Delivery Type';
comment on column static_route.EQUIPMENT_ID  is 'Designated Equipment Id';
comment on column static_route.DRIVER_TYPE_ID  is 'Driver Type';
comment on column static_route.DRIVER_CALENDAR is 'Driver Calendar';
comment on column static_route.DESG_CARRIER_CODE_ID   is 'Designated Carrier Code';
comment on column static_route.DESG_SERVICE_LEVEL_ID is 'Designated Service level';
comment on column static_route.DESG_MOT_ID  is 'Designated Mode';
comment on column static_route.REFERENCE_FIELD_1 is 'Reference field 1';
comment on column static_route.REFERENCE_FIELD_2  is 'Reference field 2';
comment on column static_route.REFERENCE_FIELD_3  is 'Reference field 3';
comment on column static_route.REFERENCE_FIELD_4 is 'Reference field 4';
comment on column static_route.REFERENCE_FIELD_5  is 'Reference field 5'; 

ALTER TABLE static_route_stop ADD (STOP_PRIORITY  NUMBER(10),
								   ZIP_CODE  VARCHAR2(16),
								   STATE  VARCHAR2(16),
								   COUNTY  VARCHAR2(16),
								   COUNTY_CODE  VARCHAR2(2),
								   REFERENCE_FIELD_1  VARCHAR2(75),
								   REFERENCE_FIELD_2  VARCHAR2(75),
								   NOTES  VARCHAR2(150));
								   
comment on column static_route_stop.STOP_PRIORITY is 'Stop Priority';
comment on column static_route_stop.ZIP_CODE is 'ZipCode of Facility';
comment on column static_route_stop.STATE is 'State';
comment on column static_route_stop.COUNTY is 'County';
comment on column static_route_stop.COUNTY_CODE is 'County Code';
comment on column static_route_stop.REFERENCE_FIELD_1 is 'Reference field 1';
comment on column static_route_stop.REFERENCE_FIELD_2 is 'Reference field 2';
comment on column static_route_stop.NOTES is 'Notes Section';

Commit;

-- DBTicket DB-1225

ALTER TABLE static_route_stop ADD (FACILITY_NAME VARCHAR2(32));

comment on column static_route_stop.FACILITY_NAME is 'Facility Alias Full Name';

Commit;

-- DBTicket DB-1405

ALTER TABLE static_route MODIFY DRIVER_CALENDAR NUMBER(8);

COMMIT;

-- DBTicket DB-1632
----Db2 specific

-- DBTicket DB-2132

alter table IMPORT_SAILING_SCHDL_WK_DAY add ARRIVAL_DAYOFWEEK_TMP number(1);
update IMPORT_SAILING_SCHDL_WK_DAY set ARRIVAL_DAYOFWEEK_TMP=ARRIVAL_DAYOFWEEK ;
UPDATE IMPORT_SAILING_SCHDL_WK_DAY set ARRIVAL_DAYOFWEEK=NULL;
alter table IMPORT_SAILING_SCHDL_WK_DAY modify ARRIVAL_DAYOFWEEK number(1);
update IMPORT_SAILING_SCHDL_WK_DAY set ARRIVAL_DAYOFWEEK=ARRIVAL_DAYOFWEEK_TMP;
ALTER TABLE IMPORT_SAILING_SCHDL_WK_DAY DROP COLUMN ARRIVAL_DAYOFWEEK_TMP;

alter table IMPORT_SAILING_SCHDL_WK_DAY add CUTOFF_DAYOFWEEK_TMP number(1);
update IMPORT_SAILING_SCHDL_WK_DAY set CUTOFF_DAYOFWEEK_TMP=CUTOFF_DAYOFWEEK ;
UPDATE IMPORT_SAILING_SCHDL_WK_DAY set CUTOFF_DAYOFWEEK=NULL;
alter table IMPORT_SAILING_SCHDL_WK_DAY modify CUTOFF_DAYOFWEEK number(1);
update IMPORT_SAILING_SCHDL_WK_DAY set CUTOFF_DAYOFWEEK=CUTOFF_DAYOFWEEK_TMP;
alter table IMPORT_SAILING_SCHDL_WK_DAY drop column CUTOFF_DAYOFWEEK_TMP;


alter table IMPORT_SAILING_SCHDL_WK_DAY add DEPARTURE_DAYOFWEEK_TMP number(1);
update IMPORT_SAILING_SCHDL_WK_DAY set DEPARTURE_DAYOFWEEK_TMP=DEPARTURE_DAYOFWEEK ;
UPDATE IMPORT_SAILING_SCHDL_WK_DAY set DEPARTURE_DAYOFWEEK=NULL;
alter table IMPORT_SAILING_SCHDL_WK_DAY modify DEPARTURE_DAYOFWEEK number(1);
update IMPORT_SAILING_SCHDL_WK_DAY set DEPARTURE_DAYOFWEEK=DEPARTURE_DAYOFWEEK_TMP;
alter table IMPORT_SAILING_SCHDL_WK_DAY drop column DEPARTURE_DAYOFWEEK_TMP;

alter table IMPORT_SAILING_SCHDL_WK_DAY add PICKUP_DAYOFWEEK_TMP number(1);
update IMPORT_SAILING_SCHDL_WK_DAY set PICKUP_DAYOFWEEK_TMP=PICKUP_DAYOFWEEK ;
UPDATE IMPORT_SAILING_SCHDL_WK_DAY set PICKUP_DAYOFWEEK=NULL;
alter table IMPORT_SAILING_SCHDL_WK_DAY modify PICKUP_DAYOFWEEK number(1);
update IMPORT_SAILING_SCHDL_WK_DAY set PICKUP_DAYOFWEEK=PICKUP_DAYOFWEEK_TMP;
alter table IMPORT_SAILING_SCHDL_WK_DAY drop column PICKUP_DAYOFWEEK_TMP;

alter table IMPORT_RG_LANE modify BILLING_METHOD_CODE varchar2(35);

alter table IMPORT_SAILING_LANE_DTL add FIXED_TRANSIT_VALUE_TMP number(14,3);
update IMPORT_SAILING_LANE_DTL set FIXED_TRANSIT_VALUE_TMP=FIXED_TRANSIT_VALUE;
update IMPORT_SAILING_LANE_DTL set FIXED_TRANSIT_VALUE=null;
alter table IMPORT_SAILING_LANE_DTL modify FIXED_TRANSIT_VALUE number(14,3);
update IMPORT_SAILING_LANE_DTL set FIXED_TRANSIT_VALUE=FIXED_TRANSIT_VALUE_TMP;
alter table IMPORT_SAILING_LANE_DTL drop column FIXED_TRANSIT_VALUE_TMP;

ALTER TRIGGER RG_SHI_LUD_TRG DISABLE;

alter table RG_SHIPPING_PARAM add IS_FASTEST_TMP number(1);
alter table RG_SHIPPING_PARAM modify IS_FASTEST null;
update RG_SHIPPING_PARAM set IS_FASTEST_TMP=IS_FASTEST;
update RG_SHIPPING_PARAM set IS_FASTEST=null;
alter table RG_SHIPPING_PARAM modify IS_FASTEST number(1);
update RG_SHIPPING_PARAM set IS_FASTEST=IS_FASTEST_TMP;
alter table RG_SHIPPING_PARAM modify IS_FASTEST NOT NULL;
ALTER TABLE RG_SHIPPING_PARAM DROP COLUMN IS_FASTEST_TMP;

alter table RG_SHIPPING_PARAM add IS_USE_PREFERENCE_BONUS_TMP number(1);
alter table RG_SHIPPING_PARAM modify IS_USE_PREFERENCE_BONUS null;
update RG_SHIPPING_PARAM set IS_USE_PREFERENCE_BONUS_TMP=IS_USE_PREFERENCE_BONUS;
update RG_SHIPPING_PARAM set IS_USE_PREFERENCE_BONUS=null;
alter table RG_SHIPPING_PARAM modify IS_USE_PREFERENCE_BONUS number(1);
update RG_SHIPPING_PARAM set IS_USE_PREFERENCE_BONUS=IS_USE_PREFERENCE_BONUS_TMP;
alter table RG_SHIPPING_PARAM modify IS_USE_PREFERENCE_BONUS NOT NULL;
ALTER TABLE RG_SHIPPING_PARAM DROP COLUMN IS_USE_PREFERENCE_BONUS_TMP;

alter trigger RG_SHI_LUD_TRG ENABLE;


alter table ACCESSORIAL_CODE add IS_OVERSIZE_TMP number(1);
alter table ACCESSORIAL_CODE modify IS_OVERSIZE null;
update ACCESSORIAL_CODE set IS_OVERSIZE_TMP=IS_OVERSIZE;
update ACCESSORIAL_CODE set IS_OVERSIZE=null;
alter table ACCESSORIAL_CODE modify IS_OVERSIZE number(1);
update ACCESSORIAL_CODE set IS_OVERSIZE=IS_OVERSIZE_TMP;
alter table ACCESSORIAL_CODE modify IS_OVERSIZE NOT NULL;
alter table ACCESSORIAL_CODE drop column IS_OVERSIZE_TMP;

ALTER TABLE DT_OVERRIDE MODIFY DEST VARCHAR2(255); 

ALTER TABLE RG_SHIPPING_PARAM MODIFY NO_RATING NUMBER(4);

ALTER TABLE DT_OVERRIDE_DETAIL MODIFY OVERRIDE_SEQ NUMBER(10);


ALTER TRIGGER IMPORT_PATH_SET_BUI_TR	DISABLE;

alter table IMPORT_PATH_SET add PATH_SET_HIERARCHY_TMP number(5);
update IMPORT_PATH_SET set PATH_SET_HIERARCHY_TMP=PATH_SET_HIERARCHY ;
UPDATE IMPORT_PATH_SET set PATH_SET_HIERARCHY=NULL;
alter table IMPORT_PATH_SET modify PATH_SET_HIERARCHY number(5);
update IMPORT_PATH_SET set PATH_SET_HIERARCHY=PATH_SET_HIERARCHY_TMP;
alter table IMPORT_PATH_SET drop column PATH_SET_HIERARCHY_TMP;

ALTER TRIGGER IMPORT_PATH_SET_BUI_TR	ENABLE;

alter table IMPORT_RG_LANE_DTL add REP_TP_FLAG_TMP number(1);
alter table IMPORT_RG_LANE_DTL modify REP_TP_FLAG null;
update IMPORT_RG_LANE_DTL set REP_TP_FLAG_TMP=REP_TP_FLAG;
update IMPORT_RG_LANE_DTL set REP_TP_FLAG=null;
alter table IMPORT_RG_LANE_DTL modify REP_TP_FLAG number(1);
update IMPORT_RG_LANE_DTL set REP_TP_FLAG=REP_TP_FLAG_TMP;
alter table IMPORT_RG_LANE_DTL modify REP_TP_FLAG NOT NULL;
ALTER TABLE IMPORT_RG_LANE_DTL DROP COLUMN REP_TP_FLAG_TMP;

alter table IMPORT_PATH_PRODUCT_CLASS modify SHIPPING_PARAM_ID number(10);

ALTER TABLE TARIFF_RATE_TYPE MODIFY TARIFF_RATE_TYPE NUMBER(8);

ALTER TABLE SAILING_SCHDL_BY_DATE MODIFY TRANSIT_TIME_VALUE NUMBER(17,4);
ALTER TABLE SAILING_SCHDL_BY_WEEK_DAY MODIFY TRANSIT_TIME_VALUE NUMBER(17,4);

COMMIT;

-- DBTicket DB-2367

ALTER TABLE static_route_stop ADD (CITY VARCHAR2(16));

comment on column static_route_stop.CITY is 'City';

COMMIT;

-- DBTicket DB-3469

ALTER TABLE static_route_stop MODIFY REFERENCE_FIELD_1 VARCHAR2(120);
ALTER TABLE static_route_stop MODIFY REFERENCE_FIELD_2 VARCHAR2(120);
COMMIT;

-- DBTicket DB-4203

ALTER TABLE IMPORT_LANE_ACCESSORIAL MODIFY RATE NUMBER(15,4);

-- DBTicket DB-4246

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Parcel Base Data Import' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Parcel Base Data Import', 'Parcel Base Data Import', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Parcel Zones Import' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Parcel Zones Import', 'Parcel Zones Import', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Parcel Rates Import' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Parcel Rates Import', 'Parcel Rates Import', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Parcel Origin Attributes Bulk Update' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Parcel Origin Attributes Bulk Update', 'Parcel Origin Attributes Bulk Update', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Parcel Zones and Rates' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Parcel Zones and Rates', 'Parcel Zones and Rates', 'Navigation');
COMMIT;

-- DBTicket DB-5154 REFL

MERGE into PERMISSION X
               using (select
                              'ICR' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'Contract Mgmt - Import Contract Rate' PERMISSION_NAME,
                              'Authorizes the user to import contract rates in Contract Management' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
               insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
               values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);
			   
MERGE into PERMISSION X
               using (select
                              'ACL' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'Administer Contract Lanes' PERMISSION_NAME,
                              'Allows Users to do Lane Administration' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
               insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
               values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);
			   
MERGE into PERMISSION X
               using (select
                              'VCL' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'View Contract Lanes' PERMISSION_NAME,
                              'Allows Users to View Lanes' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
               insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
               values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

			   
MERGE into PERMISSION X
               using (select
                              'ACRD' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'Administer Contract Routing Details' PERMISSION_NAME,
                              'Allows Users to Administer Routing Guide Lanes' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
               insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
               values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);
			   
MERGE into PERMISSION X
               using (select
                              'VCRD' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'View Contract Routing Details' PERMISSION_NAME,
                              'Allows Users to View Routing Guide Lanes' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
               insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
               values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);
			   
MERGE into PERMISSION X
               using (select
                              'VCTT' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'View Contract Transit Times' PERMISSION_NAME,
                              'Allows Users to View Transit Lanes' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
               insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
               values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);
			   
MERGE into PERMISSION X
               using (select
                              'ACTT' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'Administer Contract Transit Times' PERMISSION_NAME,
                              'Allows Users to Administer Transit Lanes' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
               insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
               values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);
			   

MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACL') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACL'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Carrier')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACL') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Carrier')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACL'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));

	  
	  
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCL') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCL'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Carrier')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCL') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Carrier')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCL'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	  
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACRD') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACRD'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Carrier')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACRD') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Carrier')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACRD'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));

	  
	  
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCRD') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCRD'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Carrier')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCRD') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Carrier')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCRD'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	  
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCTT') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCTT'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Carrier')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCTT') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Carrier')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCTT'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));

	  
	  
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACTT') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACTT'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	  
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Carrier')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACTT') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Carrier')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACTT'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TP'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'DOM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'WMS'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'eem'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'CBO'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'LM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'tcs'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'SIF'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);

	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TP'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'DOM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'WMS'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'eem'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'CBO'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'LM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'tcs'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'SIF'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);

	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TP'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   

	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'DOM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'WMS'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'eem'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'CBO'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'LM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'tcs'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'SIF'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);

	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TP'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   

	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'DOM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'WMS'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'eem'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'CBO'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'LM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'tcs'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'SIF'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TP'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   

	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'DOM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'WMS'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'eem'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'CBO'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'LM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'tcs'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'SIF'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TP'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	      
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'DOM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'WMS'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'eem'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'CBO'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'LM'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'tcs'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'SIF'
              AND MODULE.MODULE_CODE = 'CNTRMGT'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 

	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );

	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
	
	

	
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );

	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Lanes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );

	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );

	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Routing Details'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
		
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );

	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'View Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	

	
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TP'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );

	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'DOM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'eem'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'CBO'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LM'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'tcs'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'SIF'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'CNTRMGT'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Contract Transit Times'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );	

MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCL') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCL')
    );
	
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACR') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACR')
    );
	
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCR') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCR')
    );
	
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACTT') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACTT')
    );
	
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCTT') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCTT')
    );
	
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCRD') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCRD')
    );
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACRD') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACRD')
    );
	
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ICR') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ICR')
    );
	
	
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCR') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCR')
    );
	
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCTT') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCTT')
    );

	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCL') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCRD') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCL'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCRD')
    );

MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACRD') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCRD') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACRD'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCRD')
    );
	
	
MERGE INTO PERMISSION_INHERITANCE PI USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACTT') AS PARENT_PERMISSION_ID,
  (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCTT') AS CHILD_PERMISSION_ID
FROM DUAL
) C ON (PI.PARENT_PERMISSION_ID = C.PARENT_PERMISSION_ID AND PI.CHILD_PERMISSION_ID = C.CHILD_PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    ((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'ACTT'), 
     (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'VCTT')
    );


COMMIT;

---- Declare
---- BEGIN
----  FOR c_cur
----  in (Select ROLE_ID,APP_ID,MODULE_ID from ROLE_APP_MOD_PERM where PERMISSION_ID IN (Select PERMISSION_ID  from PERMISSION where PERMISSION_CODE IN ('VCR')) 
---- 		and APP_ID NOT IN (Select APP_ID FROM APP where APP.APP_SHORT_NAME = 'LG'))
----   LOOP
----     INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM)  VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('VCL')),(Select max(ROW_UID) from ROLE_APP_MOD_PERM),sysdate,null);
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('VCRD')),(Select max(ROW_UID) from ROLE_APP_MOD_PERM),sysdate,null);
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('VCTT')),(Select max(ROW_UID) from ROLE_APP_MOD_PERM),sysdate,null);
----  end LOOP;
----   EXCEPTION
----  WHEN OTHERS THEN NULL;
----  commit;
---- END;
---- /
---- 
---- Declare
---- BEGIN
----  FOR c_cur
----  in (Select ROLE_ID,APP_ID,MODULE_ID from ROLE_APP_MOD_PERM where PERMISSION_ID IN (Select PERMISSION_ID  from PERMISSION where PERMISSION_CODE IN ('ACR')) 
---- 		and APP_ID NOT IN (Select APP_ID FROM APP where APP.APP_SHORT_NAME = 'LG'))
----   
---- LOOP
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('ACL')),(Select max(ROW_UID) from ROLE_APP_MOD_PERM),sysdate,null);
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('ACRD')),(Select max(ROW_UID) from ROLE_APP_MOD_PERM),sysdate,null);
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('ACTT')),(Select max(ROW_UID) from ROLE_APP_MOD_PERM),sysdate,null);
----  end LOOP;
----  EXCEPTION
----  WHEN OTHERS THEN NULL;
----  commit;
---- END;
---- /

COMMIT;

---- -- DBTicket DB-5262 REFL
---- 
---- Declare
---- BEGIN
----  FOR c_cur
----  in (select ROLE_ID,APP_ID,MODULE_ID from ROLE_APP_MOD_PERM where PERMISSION_ID in (select PERMISSION_ID  from PERMISSION where PERMISSION_CODE in ('VCR')) 
---- 		and APP_ID  IN (Select APP_ID FROM APP where APP.APP_SHORT_NAME in('TP','TPE','DOM','WMS','eem','CBO','LM','tcs','SIF')))
----   LOOP
----     INSERT INTO ROLE_APP_MOD_PERM (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM)  VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('VCL')),(Select max(ROW_UID)+4 from ROLE_APP_MOD_PERM),sysdate,null);
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('VCRD')),(Select max(ROW_UID)+4 from ROLE_APP_MOD_PERM),sysdate,null);
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('VCTT')),(Select max(ROW_UID)+4 from ROLE_APP_MOD_PERM),sysdate,null);
----  end LOOP;
----   EXCEPTION
----  WHEN OTHERS THEN NULL;
----  commit;
---- END;
---- /
---- 
---- Declare
---- BEGIN
----  FOR c_cur
----  in (Select ROLE_ID,APP_ID,MODULE_ID from ROLE_APP_MOD_PERM where PERMISSION_ID IN (Select PERMISSION_ID  from PERMISSION where PERMISSION_CODE IN ('ACR')) 
---- 		and APP_ID IN (Select APP_ID FROM APP where APP.APP_SHORT_NAME in('TP','TPE','DOM','WMS','eem','CBO','LM','tcs','SIF')))
----   
---- LOOP
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('ACL')),(Select max(ROW_UID) from ROLE_APP_MOD_PERM),sysdate,null);
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('ACRD')),(Select max(ROW_UID) from ROLE_APP_MOD_PERM),sysdate,null);
----     INSERT INTO ROLE_APP_MOD_PERM  (ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) VALUES (c_cur.ROLE_ID,c_cur.APP_ID,c_cur.MODULE_ID,(Select PERMISSION_ID from PERMISSION where PERMISSION_CODE IN ('ACTT')),(Select max(ROW_UID) from ROLE_APP_MOD_PERM),sysdate,null);
----  end LOOP;
----  EXCEPTION
----  WHEN OTHERS THEN NULL;
----  commit;
---- END;
---- /

--above commented and fixed as  DB-5338

-- DBTicket DB-5338 REFL

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'BDM'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
       
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'CBO'
              AND MODULE.MODULE_CODE = 'BDM'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
       
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'UCL'
              AND MODULE.MODULE_CODE = 'BDM'
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                 MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'BDM'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
       
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'CBO'
              AND MODULE.MODULE_CODE = 'BDM'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
       
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'UCL'
              AND MODULE.MODULE_CODE = 'BDM'
              AND PERMISSION.PERMISSION_CODE = 'ACL') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);    
                   
                   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'BDM'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
       
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'CBO'
              AND MODULE.MODULE_CODE = 'BDM'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
       
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'UCL'
              AND MODULE.MODULE_CODE = 'BDM'
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
commit;

DECLARE
BEGIN
   FOR c_cur
      IN (SELECT ROLE_APP_MOD_PERM.ROLE_ID,
                 ROLE_APP_MOD_PERM.APP_ID,
                 ROLE_APP_MOD_PERM.MODULE_ID
            FROM ROLE_APP_MOD_PERM, ROLE
           WHERE ROLE_APP_MOD_PERM.ROLE_ID IN
                    (SELECT role_id
                       FROM role
                      WHERE role_id NOT IN
                               (SELECT ROLE_ID
                                  FROM ROLE_APP_MOD_PERM
                                 WHERE PERMISSION_ID IN
                                          (SELECT PERMISSION_ID
                                             FROM PERMISSION
                                            WHERE PERMISSION_CODE IN ('VCR'))
                                       AND APP_ID IN
                                              (SELECT APP_ID
                                                 FROM APP
                                                WHERE APP.APP_SHORT_NAME IN
                                                         ('TP',
                                                          'TPE',
                                                          'DOM',
                                                          'WMS',
                                                          'eem',
                                                          'CBO',
                                                          'LM',
                                                          'tcs',
                                                          'SIF'))))
                 AND ROLE_APP_MOD_PERM.PERMISSION_ID IN
                        (SELECT PERMISSION_ID
                           FROM PERMISSION
                          WHERE PERMISSION_CODE IN ('VCR'))
                 AND ROLE_APP_MOD_PERM.APP_ID IN
                        (SELECT APP_ID
                           FROM APP
                          WHERE APP.APP_SHORT_NAME IN
                                   ('TP',
                                    'TPE',
                                    'DOM',
                                    'WMS',
                                    'eem',
                                    'CBO',
                                    'LM',
                                    'tcs',
                                    'SIF')))
   LOOP
      MERGE INTO ROLE_APP_MOD_PERM RAMP
           USING (SELECT c_cur.ROLE_ID AS ROLE_ID,
                         c_cur.APP_ID AS APP_ID,
                         c_cur.MODULE_ID AS MODULE_ID,
                         (SELECT PERMISSION_ID
                            FROM PERMISSION
                           WHERE PERMISSION_CODE IN ('VCL'))
                            AS PERMISSION_ID
                    FROM DUAL) APP1
              ON (    RAMP.APP_ID = APP1.APP_ID
                  AND RAMP.MODULE_ID = APP1.MODULE_ID
                  AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID
                  AND RAMP.ROLE_ID = APP1.ROLE_ID)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID,
                     CREATED_DTTM)
             VALUES (c_cur.ROLE_ID,
                     c_cur.APP_ID,
                     c_cur.MODULE_ID,
                     APP1.PERMISSION_ID,
                     (SELECT MAX (ROW_UID) + 4 FROM ROLE_APP_MOD_PERM),
                     SYSDATE);

      MERGE INTO ROLE_APP_MOD_PERM RAMP
           USING (SELECT c_cur.ROLE_ID AS ROLE_ID,
                         c_cur.APP_ID AS APP_ID,
                         c_cur.MODULE_ID AS MODULE_ID,
                         (SELECT PERMISSION_ID
                            FROM PERMISSION
                           WHERE PERMISSION_CODE IN ('VCRD'))
                            AS PERMISSION_ID
                    FROM DUAL) APP1
              ON (    RAMP.APP_ID = APP1.APP_ID
                  AND RAMP.MODULE_ID = APP1.MODULE_ID
                  AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID
                  AND RAMP.ROLE_ID = APP1.ROLE_ID)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID,
                     CREATED_DTTM)
             VALUES (c_cur.ROLE_ID,
                     c_cur.APP_ID,
                     c_cur.MODULE_ID,
                     APP1.PERMISSION_ID,
                     (SELECT MAX (ROW_UID) + 4 FROM ROLE_APP_MOD_PERM),
                     SYSDATE);

      MERGE INTO ROLE_APP_MOD_PERM RAMP
           USING (SELECT c_cur.ROLE_ID AS ROLE_ID,
                         c_cur.APP_ID AS APP_ID,
                         c_cur.MODULE_ID AS MODULE_ID,
                         (SELECT PERMISSION_ID
                            FROM PERMISSION
                           WHERE PERMISSION_CODE IN ('VCTT'))
                            AS PERMISSION_ID
                    FROM DUAL) APP1
              ON (    RAMP.APP_ID = APP1.APP_ID
                  AND RAMP.MODULE_ID = APP1.MODULE_ID
                  AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID
                  AND RAMP.ROLE_ID = APP1.ROLE_ID)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID,
                     CREATED_DTTM)
             VALUES (c_cur.ROLE_ID,
                     c_cur.APP_ID,
                     c_cur.MODULE_ID,
                     APP1.PERMISSION_ID,
                     (SELECT MAX (ROW_UID) + 4 FROM ROLE_APP_MOD_PERM),
                     SYSDATE);
   END LOOP;
END;
/

DECLARE
BEGIN
   FOR c_cur
      IN (SELECT ROLE_APP_MOD_PERM.ROLE_ID,
                 ROLE_APP_MOD_PERM.APP_ID,
                 ROLE_APP_MOD_PERM.MODULE_ID
            FROM ROLE_APP_MOD_PERM, ROLE
           WHERE ROLE_APP_MOD_PERM.ROLE_ID IN
                    (SELECT role_id
                       FROM role
                      WHERE role_id NOT IN
                               (SELECT ROLE_ID
                                  FROM ROLE_APP_MOD_PERM
                                 WHERE PERMISSION_ID IN
                                          (SELECT PERMISSION_ID
                                             FROM PERMISSION
                                            WHERE PERMISSION_CODE IN ('ACR'))
                                       AND APP_ID IN
                                              (SELECT APP_ID
                                                 FROM APP
                                                WHERE APP.APP_SHORT_NAME IN
                                                         ('TP',
                                                          'TPE',
                                                          'DOM',
                                                          'WMS',
                                                          'eem',
                                                          'CBO',
                                                          'LM',
                                                          'tcs',
                                                          'SIF'))))
                 AND ROLE_APP_MOD_PERM.PERMISSION_ID IN
                        (SELECT PERMISSION_ID
                           FROM PERMISSION
                          WHERE PERMISSION_CODE IN ('ACR'))
                 AND ROLE_APP_MOD_PERM.APP_ID IN
                        (SELECT APP_ID
                           FROM APP
                          WHERE APP.APP_SHORT_NAME IN
                                   ('TP',
                                    'TPE',
                                    'DOM',
                                    'WMS',
                                    'eem',
                                    'CBO',
                                    'LM',
                                    'tcs',
                                    'SIF')))
   LOOP
      MERGE INTO ROLE_APP_MOD_PERM RAMP
           USING (SELECT c_cur.ROLE_ID AS ROLE_ID,
                         c_cur.APP_ID AS APP_ID,
                         c_cur.MODULE_ID AS MODULE_ID,
                         (SELECT PERMISSION_ID
                            FROM PERMISSION
                           WHERE PERMISSION_CODE IN ('ACL'))
                            AS PERMISSION_ID
                    FROM DUAL) APP1
              ON (    RAMP.APP_ID = APP1.APP_ID
                  AND RAMP.MODULE_ID = APP1.MODULE_ID
                  AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID
                  AND RAMP.ROLE_ID = APP1.ROLE_ID)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID,
                     CREATED_DTTM)
             VALUES (c_cur.ROLE_ID,
                     c_cur.APP_ID,
                     c_cur.MODULE_ID,
                     APP1.PERMISSION_ID,
                     (SELECT MAX (ROW_UID) + 4 FROM ROLE_APP_MOD_PERM),
                     SYSDATE);

      MERGE INTO ROLE_APP_MOD_PERM RAMP
           USING (SELECT c_cur.ROLE_ID AS ROLE_ID,
                         c_cur.APP_ID AS APP_ID,
                         c_cur.MODULE_ID AS MODULE_ID,
                         (SELECT PERMISSION_ID
                            FROM PERMISSION
                           WHERE PERMISSION_CODE IN ('ACRD'))
                            AS PERMISSION_ID
                    FROM DUAL) APP1
              ON (    RAMP.APP_ID = APP1.APP_ID
                  AND RAMP.MODULE_ID = APP1.MODULE_ID
                  AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID
                  AND RAMP.ROLE_ID = APP1.ROLE_ID)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID,
                     CREATED_DTTM)
             VALUES (c_cur.ROLE_ID,
                     c_cur.APP_ID,
                     c_cur.MODULE_ID,
                     APP1.PERMISSION_ID,
                     (SELECT MAX (ROW_UID) + 4 FROM ROLE_APP_MOD_PERM),
                     SYSDATE);

      MERGE INTO ROLE_APP_MOD_PERM RAMP
           USING (SELECT c_cur.ROLE_ID AS ROLE_ID,
                         c_cur.APP_ID AS APP_ID,
                         c_cur.MODULE_ID AS MODULE_ID,
                         (SELECT PERMISSION_ID
                            FROM PERMISSION
                           WHERE PERMISSION_CODE IN ('ACTT'))
                            AS PERMISSION_ID
                    FROM DUAL) APP1
              ON (    RAMP.APP_ID = APP1.APP_ID
                  AND RAMP.MODULE_ID = APP1.MODULE_ID
                  AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID
                  AND RAMP.ROLE_ID = APP1.ROLE_ID)
      WHEN NOT MATCHED
      THEN
         INSERT     (ROLE_ID,
                     APP_ID,
                     MODULE_ID,
                     PERMISSION_ID,
                     ROW_UID,
                     CREATED_DTTM)
             VALUES (c_cur.ROLE_ID,
                     c_cur.APP_ID,
                     c_cur.MODULE_ID,
                     APP1.PERMISSION_ID,
                     (SELECT MAX (ROW_UID) + 4 FROM ROLE_APP_MOD_PERM),
                     SYSDATE);
   END LOOP;
END;
/

COMMIT;


-- DBTicket DB-5477

comment on table IMPORT_PATH IS  'Imported Path information of Path Set';
comment on table IMPORT_PATH_SET IS  'Imported Path Set information';
comment on table IMPORT_PATH_WAYPOINT IS  'Imported Path Waypoints information of Path Set';
comment on table IMPORT_PATH_PRODUCT_CLASS IS  'Imported Product Class information of Path';
comment on table IMPORT_PATH_SET_SHIPPING_PARAM IS  'Imported Shipping Parameters information of Path';
comment on table PATH_SET_ERRORS IS  'Errors in Path Set';
comment on table CARR_PRM_ACC_FEASIBLE_INCOTERM IS  'Feasible Incoterms for the Carrier Parameter Accessorial';
comment on table IMP_CARR_PARAM_ACC_INCOTERM IS  'Imported Feasible Incoterms information for the Carrier Parameter Accessorial';
comment on table LANE_ACC_FEASIBLE_INCOTERM IS  'Feasible Incoterms for the Rating Lane Carrier Accessorial';
comment on table IMP_LANE_ACC_FEASIBLE_INCOTERM IS  'Imported Feasible Incoterms information for the Rating Lane Carrier Accessorial';

-- DBTicket DB-5702 REFL

Declare
BEGIN
FOR c_cur
in (select APP_ID,MODULE_ID from APP_MOD_PERM where PERMISSION_ID in (select PERMISSION_ID  from PERMISSION where PERMISSION_CODE in ('AMRE')) )
  LOOP
    
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_ID = c_cur.APP_ID
              AND MODULE.MODULE_ID = c_cur.MODULE_ID
              AND PERMISSION.PERMISSION_CODE = 'VCR') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (c_cur.APP_ID, c_cur.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_ID = c_cur.APP_ID
              AND MODULE.MODULE_ID = c_cur.MODULE_ID
              AND PERMISSION.PERMISSION_CODE = 'ACR') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (c_cur.APP_ID, c_cur.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_ID = c_cur.APP_ID
              AND MODULE.MODULE_ID = c_cur.MODULE_ID
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (c_cur.APP_ID, c_cur.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_ID = c_cur.APP_ID
              AND MODULE.MODULE_ID = c_cur.MODULE_ID
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (c_cur.APP_ID, c_cur.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 

	   
end LOOP;
  EXCEPTION
WHEN OTHERS THEN 
 DBMS_OUTPUT.PUT_LINE('Error Occcured');
 

END;
/


Declare
BEGIN
FOR c_cur
in (select APP_ID,MODULE_ID from APP_MOD_PERM where PERMISSION_ID in (select PERMISSION_ID  from PERMISSION where PERMISSION_CODE in ('AMRGE')) )
  LOOP
    
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_ID = c_cur.APP_ID
              AND MODULE.MODULE_ID = c_cur.MODULE_ID
              AND PERMISSION.PERMISSION_CODE = 'VCRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (c_cur.APP_ID, c_cur.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_ID = c_cur.APP_ID
              AND MODULE.MODULE_ID = c_cur.MODULE_ID
              AND PERMISSION.PERMISSION_CODE = 'ACRD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (c_cur.APP_ID, c_cur.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 
	   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_ID = c_cur.APP_ID
              AND MODULE.MODULE_ID = c_cur.MODULE_ID
              AND PERMISSION.PERMISSION_CODE = 'VCTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (c_cur.APP_ID, c_cur.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_ID = c_cur.APP_ID
              AND MODULE.MODULE_ID = c_cur.MODULE_ID
              AND PERMISSION.PERMISSION_CODE = 'ACTT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (c_cur.APP_ID, c_cur.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 

	   
end LOOP;
  EXCEPTION
WHEN OTHERS THEN 
 DBMS_OUTPUT.PUT_LINE('Error Occcured');
 

END;
/

Declare
BEGIN
FOR c_cur
in (select ROLE_ID,APP_ID,MODULE_ID from ROLE_APP_MOD_PERM where PERMISSION_ID in (select PERMISSION_ID  from PERMISSION where PERMISSION_CODE in ('AMRE')) 
                                and APP_ID  IN (Select APP_ID FROM APP_MOD_PERM where PERMISSION_ID in (select PERMISSION_ID  from PERMISSION where PERMISSION_CODE in ('AMRE'))))
  LOOP
    
MERGE INTO ROLE_APP_MOD_PERM RAMP USING
(SELECT c_cur.ROLE_ID as ROLE_ID, c_cur.APP_ID as APP_ID , c_cur.MODULE_ID as MODULE_ID,  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE IN ('VCR') ) AS PERMISSION_ID from DUAL) APP1 
      ON ( RAMP.APP_ID   = APP1.APP_ID AND RAMP.MODULE_ID = APP1.MODULE_ID AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID AND RAMP.ROLE_ID = APP1.ROLE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
       ROLE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      CREATED_DTTM
    )
    VALUES
    (
     c_cur.ROLE_ID,
      c_cur.APP_ID,
      c_cur.MODULE_ID,
      APP1.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM ROLE_APP_MOD_PERM ),
      sysdate
    );

MERGE INTO ROLE_APP_MOD_PERM RAMP USING
(SELECT c_cur.ROLE_ID as ROLE_ID, c_cur.APP_ID as APP_ID , c_cur.MODULE_ID as MODULE_ID,  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE IN ('ACR') ) AS PERMISSION_ID from DUAL) APP1 
      ON ( RAMP.APP_ID   = APP1.APP_ID AND RAMP.MODULE_ID = APP1.MODULE_ID AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID AND RAMP.ROLE_ID = APP1.ROLE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
       ROLE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      CREATED_DTTM
    )
    VALUES
    (
      c_cur.ROLE_ID,
      c_cur.APP_ID,
      c_cur.MODULE_ID,
      APP1.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM ROLE_APP_MOD_PERM ),
      sysdate
    );

MERGE INTO ROLE_APP_MOD_PERM RAMP USING
(SELECT c_cur.ROLE_ID as ROLE_ID, c_cur.APP_ID as APP_ID , c_cur.MODULE_ID as MODULE_ID,  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE IN ('VCTT') ) AS PERMISSION_ID from DUAL) APP1 
      ON ( RAMP.APP_ID   = APP1.APP_ID AND RAMP.MODULE_ID = APP1.MODULE_ID AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID AND RAMP.ROLE_ID = APP1.ROLE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
       ROLE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      CREATED_DTTM
    )
    VALUES
    (
      c_cur.ROLE_ID,
      c_cur.APP_ID,
      c_cur.MODULE_ID,
      APP1.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM ROLE_APP_MOD_PERM ),
      sysdate
    );    
	
	MERGE INTO ROLE_APP_MOD_PERM RAMP USING
(SELECT c_cur.ROLE_ID as ROLE_ID, c_cur.APP_ID as APP_ID , c_cur.MODULE_ID as MODULE_ID,  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE IN ('ACTT') ) AS PERMISSION_ID from DUAL) APP1 
      ON ( RAMP.APP_ID   = APP1.APP_ID AND RAMP.MODULE_ID = APP1.MODULE_ID AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID AND RAMP.ROLE_ID = APP1.ROLE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
       ROLE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      CREATED_DTTM
    )
    VALUES
    (
      c_cur.ROLE_ID,
      c_cur.APP_ID,
      c_cur.MODULE_ID,
      APP1.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM ROLE_APP_MOD_PERM ),
      sysdate
    ); 
end LOOP;
  EXCEPTION
WHEN OTHERS THEN 
 DBMS_OUTPUT.PUT_LINE('Error Occcured');

END;
/

Declare
BEGIN
FOR c_cur
in (select ROLE_ID,APP_ID,MODULE_ID from ROLE_APP_MOD_PERM where PERMISSION_ID in (select PERMISSION_ID  from PERMISSION where PERMISSION_CODE in ('AMRGE')) 
                                and APP_ID  IN (Select APP_ID FROM APP_MOD_PERM where PERMISSION_ID in (select PERMISSION_ID  from PERMISSION where PERMISSION_CODE in ('AMRGE'))))
  LOOP
    
MERGE INTO ROLE_APP_MOD_PERM RAMP USING
(SELECT c_cur.ROLE_ID as ROLE_ID, c_cur.APP_ID as APP_ID , c_cur.MODULE_ID as MODULE_ID,  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE IN ('VCRD') ) AS PERMISSION_ID from DUAL) APP1 
      ON ( RAMP.APP_ID   = APP1.APP_ID AND RAMP.MODULE_ID = APP1.MODULE_ID AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID AND RAMP.ROLE_ID = APP1.ROLE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
       ROLE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      CREATED_DTTM
    )
    VALUES
    (
     c_cur.ROLE_ID,
      c_cur.APP_ID,
      c_cur.MODULE_ID,
      APP1.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM ROLE_APP_MOD_PERM ),
      sysdate
    );

MERGE INTO ROLE_APP_MOD_PERM RAMP USING
(SELECT c_cur.ROLE_ID as ROLE_ID, c_cur.APP_ID as APP_ID , c_cur.MODULE_ID as MODULE_ID,  (SELECT PERMISSION_ID FROM 
PERMISSION WHERE PERMISSION_CODE IN ('ACRD') ) AS PERMISSION_ID from DUAL) APP1 
      ON ( RAMP.APP_ID   = APP1.APP_ID AND RAMP.MODULE_ID = APP1.MODULE_ID AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID AND RAMP.ROLE_ID = APP1.ROLE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
       ROLE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      CREATED_DTTM
    )
    VALUES
    (
      c_cur.ROLE_ID,
      c_cur.APP_ID,
      c_cur.MODULE_ID,
      APP1.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM ROLE_APP_MOD_PERM ),
      sysdate
    );

MERGE INTO ROLE_APP_MOD_PERM RAMP USING
(SELECT c_cur.ROLE_ID as ROLE_ID, c_cur.APP_ID as APP_ID , c_cur.MODULE_ID as MODULE_ID,  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE IN ('VCTT') ) AS PERMISSION_ID from DUAL) APP1 
      ON ( RAMP.APP_ID   = APP1.APP_ID AND RAMP.MODULE_ID = APP1.MODULE_ID AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID AND RAMP.ROLE_ID = APP1.ROLE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
       ROLE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      CREATED_DTTM
    )
    VALUES
    (
      c_cur.ROLE_ID,
      c_cur.APP_ID,
      c_cur.MODULE_ID,
      APP1.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM ROLE_APP_MOD_PERM ),
      sysdate
    );    
	
	MERGE INTO ROLE_APP_MOD_PERM RAMP USING
(SELECT c_cur.ROLE_ID as ROLE_ID, c_cur.APP_ID as APP_ID , c_cur.MODULE_ID as MODULE_ID,  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE IN ('ACTT') ) AS PERMISSION_ID from DUAL) APP1 
      ON ( RAMP.APP_ID   = APP1.APP_ID AND RAMP.MODULE_ID = APP1.MODULE_ID AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID AND RAMP.ROLE_ID = APP1.ROLE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
       ROLE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      CREATED_DTTM
    )
    VALUES
    (
      c_cur.ROLE_ID,
      c_cur.APP_ID,
      c_cur.MODULE_ID,
      APP1.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM ROLE_APP_MOD_PERM ),
      sysdate
    ); 
end LOOP;
  EXCEPTION
WHEN OTHERS THEN 
 DBMS_OUTPUT.PUT_LINE('Error Occcured');

END;
/

delete from ROLE_APP_MOD_PERM where PERMISSION_ID in (select PERMISSION_ID  from PERMISSION where PERMISSION_CODE in ('AMRE','AMRGE'));

COMMIT;

-- DBTicket DB-6971

CREATE INDEX COMB_LANE_LNNAME_TC_IDX ON COMB_LANE
(TC_COMPANY_ID,LANE_NAME) 
LOGGING
TABLESPACE CM_BST4K_IDX_TBS
PCTFREE    10
INITRANS   10;

COMMIT;

-- DBTicket DB-6982

ALTER TABLE static_route MODIFY (
REFERENCE_FIELD_1 VARCHAR2(120),
REFERENCE_FIELD_2 VARCHAR2(120),
REFERENCE_FIELD_3 VARCHAR2(120),
REFERENCE_FIELD_4 VARCHAR2(120),
REFERENCE_FIELD_5 VARCHAR2(120));


-- DBTicket DB-7878

ALTER TABLE CURRENCY_CONVERSION MODIFY CREATED_SOURCE VARCHAR2(50);
ALTER TABLE CURRENCY_CONVERSION MODIFY LAST_UPDATED_SOURCE VARCHAR2(50);
ALTER TABLE IMPORT_PATH_SET MODIFY CREATED_SOURCE VARCHAR2(50);
ALTER TABLE IMPORT_RG_LANE MODIFY CREATED_SOURCE VARCHAR2(50);
ALTER TABLE IMPORT_SAILING_LANE MODIFY CREATED_SOURCE VARCHAR2(50);
ALTER TABLE IMPORT_SAILING_LANE_DTL MODIFY LAST_UPDATED_SOURCE VARCHAR2(50);
ALTER TABLE PATH_SET MODIFY CREATED_SOURCE VARCHAR2(50);
ALTER TABLE STATIC_ROUTE MODIFY CREATED_SOURCE VARCHAR2(50);
ALTER TABLE STATIC_ROUTE MODIFY LAST_UPDATED_SOURCE VARCHAR2(50);

-- DBTicket DB-7937

alter table STATIC_ROUTE_STOP modify CITY VARCHAR2(40);
alter table STATIC_ROUTE_STOP modify FACILITY_NAME VARCHAR2(50);

COMMIT;

-- DBTicket DB-9314

DELETE FROM STATIC_COMPANY_RULE_ATTRIBUTE where ATTRIB_ID in (Select ATTRIB_ID from STATIC_RULE_ATTRIBUTE WHERE ENTITY_NAME = 'LPN' AND ATTRIB_NAME IN ('PRE_RECEIPT_ALLOCATION_FLAG','TRAILER_NBR'));
DELETE FROM STATIC_RULE_ATTRIBUTE WHERE ENTITY_NAME = 'LPN' AND ATTRIB_NAME IN ('PRE_RECEIPT_ALLOCATION_FLAG','TRAILER_NBR');

COMMIT;
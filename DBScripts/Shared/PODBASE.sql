-- DBTicket POD-67

EXEC sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO MODULE M USING
(SELECT 372 MODULE_ID,
  'Proof of Delivery Mobile' MODULE_NAME,
  1 IS_ACTIVE,
  'PODM' MODULE_CODE
FROM DUAL
) B ON (M.MODULE_ID = B.MODULE_ID)
WHEN NOT matched THEN
  INSERT
    (
      MODULE_ID,
      MODULE_NAME,
      IS_ACTIVE,
      MODULE_CODE
    )
    VALUES
    (
      B.MODULE_ID,
      B.MODULE_NAME,
      B.IS_ACTIVE,
      B.MODULE_CODE
    );
	
MERGE INTO APP_MODULE X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      1 IS_ACTIVE,
      (
      (SELECT MAX(ROW_UID) FROM APP_MODULE
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.MODULE_ID = B.MODULE_ID AND X.APP_ID = B.APP_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      IS_ACTIVE,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.IS_ACTIVE,
      B.ROW_UID
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'PODMOBILE' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Proof of Delivery Mobile Application' PERMISSION_NAME,
      'Allows user to access PoD Mobile App' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'ADLVVLDBAR' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Deliver - Valid Barcode' PERMISSION_NAME,
      'Administer Deliver - Valid Barcode' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'AEDTLSVLDBAR' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Edit Details - Valid Barcode' PERMISSION_NAME,
      'Administer Edit Details - Valid Barcode' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'AASKVLDBAR' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Ask Me - Valid Barcode' PERMISSION_NAME,
      'Administer Ask Me - Valid Barcode' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'AIGNUNKBAR' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Ignore - Unknown Barcode' PERMISSION_NAME,
      'Administer Ignore - Unknown Barcode' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'AEDTLSUKNBAR' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Edit Details - Unknown Barcode' PERMISSION_NAME,
      'Administer Edit Details - Unknown Barcode' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'AASKUKNBAR' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Ask Me - Unknown Barcode' PERMISSION_NAME,
      'Administer Ask Me - Unknown Barcode' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );

MERGE INTO PERMISSION X USING
  (SELECT 'AIGNWRGSTP' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Ignore - Wrong Stop' PERMISSION_NAME,
      'Administer Ignore - Wrong Stop' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'AEDTLSWRGSTP' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Edit Details - Wrong Stop' PERMISSION_NAME,
      'Administer Edit Details - Wrong Stop' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'AASKWRGSTP' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Ask Me - Wrong Stop' PERMISSION_NAME,
      'Administer Ask Me - Wrong Stop' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO PERMISSION X USING
  (SELECT 'ADLVWRGSTP' PERMISSION_CODE,
      1 IS_ACTIVE,
      'Administer Deliver - Wrong Stop' PERMISSION_NAME,
      'Administer Deliver - Wrong Stop' DESCRIPTION
    FROM DUAL
  )
  B ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT matched THEN
  INSERT
    (
      PERMISSION_ID,
      PERMISSION_CODE,
      IS_ACTIVE,
      PERMISSION_NAME,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      B.PERMISSION_CODE,
      B.IS_ACTIVE,
      B.PERMISSION_NAME,
      B.DESCRIPTION
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'PODMOBILE'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'ADLVVLDBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'AEDTLSVLDBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'AASKVLDBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'AIGNUNKBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'AEDTLSUKNBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'AASKUKNBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'AIGNWRGSTP'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'AEDTLSWRGSTP'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'AASKWRGSTP'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO APP_MOD_PERM X USING
  (SELECT 132 APP_ID,
      372 MODULE_ID,
      (SELECT MAX(PERMISSION_ID)
      FROM PERMISSION
      WHERE PERMISSION_CODE = 'ADLVWRGSTP'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON (X.APP_ID = B.APP_ID AND X.MODULE_ID = B.MODULE_ID AND X.PERMISSION_ID = B.PERMISSION_ID)
WHEN NOT matched THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.APP_ID,
      B.MODULE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='PODMOBILE'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='ADLVVLDBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='AEDTLSVLDBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='AASKVLDBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='AIGNUNKBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='AEDTLSUKNBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='AASKUKNBAR'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='AIGNWRGSTP'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='AEDTLSWRGSTP'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='AASKWRGSTP'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );
	
MERGE INTO COMPANY_TYPE_PERM C USING
  (SELECT 4 COMPANY_TYPE_ID,
      (SELECT PERMISSION_ID
      FROM PERMISSION
      WHERE PERMISSION_CODE='ADLVWRGSTP'
      ) PERMISSION_ID,
      (
      (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
      )+4) ROW_UID
    FROM DUAL
  )
  B ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      B.COMPANY_TYPE_ID,
      B.PERMISSION_ID,
      B.ROW_UID
    );

COMMIT;


-- DBTicket POD-70

EXEC sequpdt('LABEL','LABEL_ID','SEQ_LABEL_ID');

MERGE INTO LABEL L USING
(SELECT 'UCLAppModule_Proof of Delivery Mobile' KEY,
  'Proof of Delivery Mobile' VALUE,
  'UCL' BUNDLE_NAME
FROM DUAL
) B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Proof of Delivery Mobile Application' KEY,
      'Proof of Delivery Mobile Application' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Deliver - Valid Barcode' KEY,
      'Administer Deliver - Valid Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Edit Details - Valid Barcode' KEY,
      'Administer Edit Details - Valid Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ask Me - Valid Barcode' KEY,
      'Administer Ask Me - Valid Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ignore - Unknown Barcode' KEY,
      'Administer Ignore - Unknown Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Edit Details - Unknown Barcode' KEY,
      'Administer Edit Details - Unknown Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ask Me - Unknown Barcode' KEY,
      'Administer Ask Me - Unknown Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ignore - Wrong Stop' KEY,
      'Administer Ignore - Wrong Stop' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Edit Details - Wrong Stop' KEY,
      'Administer Edit Details - Wrong Stop' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ask Me - Wrong Stop' KEY,
      'Administer Ask Me - Wrong Stop' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
MERGE INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Deliver - Wrong Stop' KEY,
      'Administer Deliver - Wrong Stop' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    );
	
COMMIT;	

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');


-- DBTicket POD-128

delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE ='Proof of Delivery Mobile';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE = 'Proof of Delivery Mobile Application';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'Administer Deliver%Valid Barcode';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'Administer Edit Details%Valid Barcode';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'Administer Ask Me%Valid Barcode';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'Administer Ignore%Unknown Barcode';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'Administer Edit Details%Unknown Barcode';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'Administer Ask Me%Unknown Barcode';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'Administer Ignore%Wrong Stop';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'Administer Edit Details%Wrong Stop';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'UCLPermissions_Administer Ask Me%Wrong Stop';
delete from LABEL_DISPLAY where CREATED_SOURCE='UCL' and DISP_VALUE like  'UCLPermissions_Administer Deliver%Wrong Stop';


delete from LABEL where BUNDLE_NAME='UCL' and VALUE ='Proof of Delivery Mobile';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE = 'Proof of Delivery Mobile Application';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'Administer Deliver%Valid Barcode';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'Administer Edit Details%Valid Barcode';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'Administer Ask Me%Valid Barcode';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'Administer Ignore%Unknown Barcode';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'Administer Edit Details%Unknown Barcode';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'Administer Ask Me%Unknown Barcode';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'Administer Ignore%Wrong Stop';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'Administer Edit Details%Wrong Stop';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'UCLPermissions_Administer Ask Me%Wrong Stop';
delete from LABEL where BUNDLE_NAME='UCL' and VALUE like  'UCLPermissions_Administer Deliver%Wrong Stop';

EXEC sequpdt('LABEL','LABEL_ID','SEQ_LABEL_ID');

merge INTO LABEL L USING
(SELECT 'UCLAppModule_Proof of Delivery Mobile' KEY,
  'Proof of Delivery Mobile' VALUE,
  'UCL' BUNDLE_NAME
FROM DUAL
) B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Proof of Delivery Mobile Application' KEY,
      'Proof of Delivery Mobile Application' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Deliver - Valid Barcode' KEY,
      'Administer Deliver - Valid Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Edit Details - Valid Barcode' KEY,
      'Administer Edit Details - Valid Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ask Me - Valid Barcode' KEY,
      'Administer Ask Me - Valid Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ignore - Unknown Barcode' KEY,
      'Administer Ignore - Unknown Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Edit Details - Unknown Barcode' KEY,
      'Administer Edit Details - Unknown Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ask Me - Unknown Barcode' KEY,
      'Administer Ask Me - Unknown Barcode' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ignore - Wrong Stop' KEY,
      'Administer Ignore - Wrong Stop' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Edit Details - Wrong Stop' KEY,
      'Administer Edit Details - Wrong Stop' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Ask Me - Wrong Stop' KEY,
      'Administer Ask Me - Wrong Stop' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;
  merge INTO LABEL L USING
  (SELECT 'UCLPermissions_Administer Deliver - Wrong Stop' KEY,
      'Administer Deliver - Wrong Stop' VALUE,
      'UCL' BUNDLE_NAME
    FROM DUAL
  )
  B ON (L.KEY=B.KEY)
WHEN NOT MATCHED THEN
  INSERT
    (
      LABEL_ID,
      KEY,
      VALUE,
      BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      B.KEY,
      B.VALUE,
      B.BUNDLE_NAME
    ) ;

COMMIT;	


-- DBTicket POD-158 REFL

update permission set PERMISSION_NAME= 'Administer Deliver - Valid Barcode',DESCRIPTION = 'Administer Deliver - Valid Barcode' where PERMISSION_CODE = 'ADLVVLDBAR';
update permission set PERMISSION_NAME= 'Administer Edit Details - Valid Barcode',DESCRIPTION = 'Administer Edit Details - Valid Barcode' where PERMISSION_CODE = 'AEDTLSVLDBAR';
update permission set PERMISSION_NAME= 'Administer Ask Me - Valid Barcode',DESCRIPTION = 'Administer Ask Me - Valid Barcode' where PERMISSION_CODE = 'AASKVLDBAR';
update permission set PERMISSION_NAME= 'Administer Ignore - Unknown Barcode',DESCRIPTION = 'Administer Ignore - Unknown Barcode' where PERMISSION_CODE = 'AIGNUNKBAR';
update permission set PERMISSION_NAME= 'Administer Edit Details - Unknown Barcode',DESCRIPTION = 'Administer Edit Details - Unknown Barcode' where PERMISSION_CODE = 'AEDTLSUKNBAR';
update permission set PERMISSION_NAME= 'Administer Ask Me - Unknown Barcode',DESCRIPTION = 'Administer Ask Me - Unknown Barcode' where PERMISSION_CODE = 'AASKUKNBAR';
update permission set PERMISSION_NAME= 'Administer Ignore - Wrong Stop',DESCRIPTION = 'Administer Ignore - Wrong Stop' where PERMISSION_CODE = 'AIGNWRGSTP';
update permission set PERMISSION_NAME= 'Administer Edit Details - Wrong Stop',DESCRIPTION = 'Administer Edit Details - Wrong Stop' where PERMISSION_CODE = 'AEDTLSWRGSTP';
update permission set PERMISSION_NAME= 'Administer Ask Me - Wrong Stop',DESCRIPTION = 'Administer Ask Me - Wrong Stop' where PERMISSION_CODE = 'AASKWRGSTP';
update permission set PERMISSION_NAME= 'Administer Deliver - Wrong Stop',DESCRIPTION = 'Administer Deliver - Wrong Stop' where PERMISSION_CODE = 'ADLVWRGSTP';

COMMIT;

-- DBTicket POD-385 REFL
EXEC SEQUPDT ('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');

MERGE INTO PERMISSION X
     USING (SELECT 'PODQUESTVIEW' PERMISSION_CODE,
                   1 IS_ACTIVE,
                   'PoD Questionnaire View' PERMISSION_NAME,
                   'Allows user to view questionnaires' DESCRIPTION
              FROM DUAL) B
        ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_CODE,
               IS_ACTIVE,
               PERMISSION_NAME,
               DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               B.PERMISSION_CODE,
               B.IS_ACTIVE,
               B.PERMISSION_NAME,
               B.DESCRIPTION);
			   
COMMIT;		

-- DBTicket POD-476 REFL

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'POD'
              AND MODULE.MODULE_CODE = 'PODM'
              AND PERMISSION.PERMISSION_CODE = 'PODQUESTVIEW') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );
			   
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'PODQUESTVIEW') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'PODQUESTVIEW'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    );
commit;
	   
	   
-- DBTicket POD-687 REFL

DELETE
FROM app_mod_perm
WHERE permission_id IN
  (SELECT permission_id
  FROM permission
  WHERE permission_code IN ('ADLVVLDBAR','AEDTLSVLDBAR','AASKVLDBAR','AIGNUNKBAR','AEDTLSUKNBAR','AASKUKNBAR','AIGNWRGSTP','AEDTLSWRGSTP','AASKWRGSTP','ADLVWRGSTP')
  );
  
DELETE
FROM COMPANY_TYPE_PERM
WHERE permission_id IN
  (SELECT permission_id
  FROM permission
  WHERE permission_code IN ('ADLVVLDBAR','AEDTLSVLDBAR','AASKVLDBAR','AIGNUNKBAR','AEDTLSUKNBAR','AASKUKNBAR','AIGNWRGSTP','AEDTLSWRGSTP','AASKWRGSTP','ADLVWRGSTP')
  );
  
DELETE
FROM permission
WHERE permission_code IN ('ADLVVLDBAR','AEDTLSVLDBAR','AASKVLDBAR','AIGNUNKBAR','AEDTLSUKNBAR','AASKUKNBAR','AIGNWRGSTP','AEDTLSWRGSTP','AASKWRGSTP','ADLVWRGSTP');

COMMIT;	   
-- DBTicket SCPP-14295

update LABEL set value = 'Client Identity' where bundle_name = 'mps' and key = 'deviceId';
update LABEL set value = 'Are you sure you would like to Logout?' where bundle_name = 'mps' and key = 'logoutMessage';
COMMIT;

---- DBTicket SCPP-14277
---- Changes are present in 2014 maint branch hence commenting this in Feature script.
--MERGE INTO LABEL L
--     USING (SELECT 'mps' BUNDLE_NAME, 'device' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'device',
--               'Device',
--               'mps');
--
--MERGE INTO XSCREEN_LABEL X
--		USING (SELECT '1000' XSCREEN_ID, 'mps' LABEL_BUNDLE_NAME, 'device' LABEL_KEY FROM DUAL) B
--			ON (X.XSCREEN_ID = B.XSCREEN_ID AND X.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND X.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED 
--THEN
--	INSERT (X.XSCREEN_LABEL_ID,
--			X.XSCREEN_ID,
--			X.LABEL_BUNDLE_NAME,
--			X.LABEL_KEY)
--	VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL, 
--			1000, 
--			'mps', 
--			'device');
--
--MERGE INTO LABEL L
--     USING (SELECT 'mps' BUNDLE_NAME, 'appInfo' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'appInfo',
--               'Application',
--               'mps');
--
--MERGE INTO XSCREEN_LABEL X
--		USING (SELECT '1000' XSCREEN_ID, 'mps' LABEL_BUNDLE_NAME, 'appInfo' LABEL_KEY FROM DUAL) B
--			ON (X.XSCREEN_ID = B.XSCREEN_ID AND X.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND X.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED 
--THEN
--	INSERT (X.XSCREEN_LABEL_ID,
--			X.XSCREEN_ID,
--			X.LABEL_BUNDLE_NAME,
--			X.LABEL_KEY)
--	VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL, 
--			1000, 
--			'mps', 
--			'appInfo');
--
--MERGE INTO LABEL L
--     USING (SELECT 'mps' BUNDLE_NAME, 'appInfomation' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'appInfomation',
--               'Application Infomation',
--               'mps');
--
--MERGE INTO XSCREEN_LABEL X
--		USING (SELECT '1000' XSCREEN_ID, 'mps' LABEL_BUNDLE_NAME, 'appInfomation' LABEL_KEY FROM DUAL) B
--			ON (X.XSCREEN_ID = B.XSCREEN_ID AND X.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND X.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED 
--THEN
--	INSERT (X.XSCREEN_LABEL_ID,
--			X.XSCREEN_ID,
--			X.LABEL_BUNDLE_NAME,
--			X.LABEL_KEY)
--	VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL, 
--			1000, 
--			'mps', 
--			'appInfomation');
--
--MERGE INTO LABEL L
--     USING (SELECT 'mps' BUNDLE_NAME, 'settings' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'settings',
--               'Settings',
--               'mps');
--
--MERGE INTO XSCREEN_LABEL X
--		USING (SELECT '1000' XSCREEN_ID, 'mps' LABEL_BUNDLE_NAME, 'settings' LABEL_KEY FROM DUAL) B
--			ON (X.XSCREEN_ID = B.XSCREEN_ID AND X.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND X.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED 
--THEN
--	INSERT (X.XSCREEN_LABEL_ID,
--			X.XSCREEN_ID,
--			X.LABEL_BUNDLE_NAME,
--			X.LABEL_KEY)
--	VALUES (SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL, 
--			1000, 
--			'mps', 
--			'settings');
--
--COMMIT;
--

-- DBTicket SCPP-14285

MERGE INTO LABEL L USING
(SELECT 'Displaying' KEY, 'lps' BUNDLE_NAME FROM DUAL
) B ON ( L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'Displaying',
      'Displaying',
      'lps'
    );
  
COMMIT;


-- DBTicket SCPP-14315

MERGE INTO LABEL L USING
(SELECT 'mps' BUNDLE_NAME, 'clearAllSelected' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'clearAllSelected',
      'Clear all selected records',
      'mps'
    );

MERGE INTO XSCREEN_LABEL SL USING
  (SELECT 'mps' LABEL_BUNDLE_NAME,
      'clearAllSelected' LABEL_KEY ,
      2 XSCREEN_ID
    FROM DUAL
  )
  B ON (SL.LABEL_KEY = B.LABEL_KEY AND SL.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND SL.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      SL.XSCREEN_LABEL_ID,
      SL.XSCREEN_ID,
      SL.LABEL_BUNDLE_NAME,
      SL.LABEL_KEY
    )
    VALUES
    (
      SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
      2,
      'mps',
      'clearAllSelected'
    );
  
COMMIT;



-- DBTicket SCPP-14405

MERGE INTO LABEL L USING
(SELECT 'mps' BUNDLE_NAME, 'pagingDisplayMsg' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'pagingDisplayMsg',
      'Displaying {0} - {1} of {2}',
      'mps'
    );
	
MERGE INTO XSCREEN_LABEL SL USING
  (SELECT 'mps' LABEL_BUNDLE_NAME,
      'pagingDisplayMsg' LABEL_KEY ,
      2 XSCREEN_ID
    FROM DUAL
  )
  B ON (SL.LABEL_KEY = B.LABEL_KEY AND SL.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND SL.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      SL.XSCREEN_LABEL_ID,
      SL.XSCREEN_ID,
      SL.LABEL_BUNDLE_NAME,
      SL.LABEL_KEY
    )
    VALUES
    (
      SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
      2,
      'mps',
      'pagingDisplayMsg'
    );
  MERGE INTO LABEL L USING
  (SELECT 'mps' BUNDLE_NAME, 'nextText' KEY FROM DUAL
  )
  B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'nextText',
      'Next',
	  'mps'
    );
  MERGE INTO XSCREEN_LABEL SL USING
  (SELECT 'mps' LABEL_BUNDLE_NAME, 'nextText' LABEL_KEY, 2 XSCREEN_ID FROM DUAL
  )
  B ON (SL.LABEL_KEY = B.LABEL_KEY AND SL.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND SL.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      SL.XSCREEN_LABEL_ID,
      SL.XSCREEN_ID,
      SL.LABEL_BUNDLE_NAME,
      SL.LABEL_KEY
    )
    VALUES
    (
      SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
      2,
      'mps',
      'nextText'
    );
  MERGE INTO LABEL L USING
  (SELECT 'mps' BUNDLE_NAME, 'lastText' KEY FROM DUAL
  )
  B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'lastText',
      'Last',
	  'mps'
    );
  MERGE INTO XSCREEN_LABEL SL USING
  (SELECT 'mps' LABEL_BUNDLE_NAME, 'lastText' LABEL_KEY, 2 XSCREEN_ID FROM DUAL
  )
  B ON (SL.LABEL_KEY = B.LABEL_KEY AND SL.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND SL.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      SL.XSCREEN_LABEL_ID,
      SL.XSCREEN_ID,
      SL.LABEL_BUNDLE_NAME,
      SL.LABEL_KEY
    )
    VALUES
    (
      SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
      2,
      'mps',
      'lastText'
    );
  MERGE INTO LABEL L USING
  (SELECT 'mps' BUNDLE_NAME, 'prevText' KEY FROM DUAL
  )
  B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'prevText',
      'Previous',
	  'mps'
    );
  MERGE INTO XSCREEN_LABEL SL USING
  (SELECT 'mps' LABEL_BUNDLE_NAME,
      'prevText' LABEL_KEY ,
      2 XSCREEN_ID
    FROM DUAL
  )
  B ON (SL.LABEL_KEY = B.LABEL_KEY AND SL.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND SL.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      SL.XSCREEN_LABEL_ID,
      SL.XSCREEN_ID,
      SL.LABEL_BUNDLE_NAME,
      SL.LABEL_KEY
    )
    VALUES
    (
      SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
      2,
      'mps',
      'prevText'
    );
  MERGE INTO LABEL L USING
  (SELECT 'mps' BUNDLE_NAME, 'firstText' KEY FROM DUAL
  )
  B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'firstText',
      'First',
	  'mps'
    );
  MERGE INTO XSCREEN_LABEL SL USING
  (SELECT 'mps' LABEL_BUNDLE_NAME,
      'firstText' LABEL_KEY ,
      2 XSCREEN_ID
    FROM DUAL
  )
  B ON (SL.LABEL_KEY = B.LABEL_KEY AND SL.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND SL.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      SL.XSCREEN_LABEL_ID,
      SL.XSCREEN_ID,
      SL.LABEL_BUNDLE_NAME,
      SL.LABEL_KEY
    )
    VALUES
    (
      SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
      2,
      'mps',
      'firstText'
    );
  MERGE INTO LABEL L USING
  (SELECT 'mps' BUNDLE_NAME, 'genericSelectedRecordsText' KEY FROM DUAL
  )
  B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'genericSelectedRecordsText',
      '{0} record(s) selected',
	  'mps'
    );
  MERGE INTO XSCREEN_LABEL SL USING
  (SELECT 'mps' LABEL_BUNDLE_NAME,
      'genericSelectedRecordsText' LABEL_KEY,
      2 XSCREEN_ID
    FROM DUAL
  )
  B ON (SL.LABEL_KEY = B.LABEL_KEY AND SL.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND SL.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      SL.XSCREEN_LABEL_ID,
      SL.XSCREEN_ID,
      SL.LABEL_BUNDLE_NAME,
      SL.LABEL_KEY
    )
    VALUES
    (
      SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
      2,
      'mps',
      'genericSelectedRecordsText'
    );
  MERGE INTO LABEL L USING
  (SELECT 'mps' BUNDLE_NAME, 'loadedRecordsText' KEY FROM DUAL
  )
  B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'loadedRecordsText',
      '{0} record(s) loaded',
	  'mps'
    );
  MERGE INTO XSCREEN_LABEL SL USING
  (SELECT 'mps' LABEL_BUNDLE_NAME,
      'loadedRecordsText' LABEL_KEY ,
      2 XSCREEN_ID
    FROM DUAL
  )
  B ON (SL.LABEL_KEY = B.LABEL_KEY AND SL.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME AND SL.XSCREEN_ID = B.XSCREEN_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      SL.XSCREEN_LABEL_ID,
      SL.XSCREEN_ID,
      SL.LABEL_BUNDLE_NAME,
      SL.LABEL_KEY
    )
    VALUES
    (
      SEQUENCE_XSCREEN_LABEL_ID.NEXTVAL,
      2,
      'mps',
      'loadedRecordsText'
    );
  COMMIT;
  
-- DBTicket SCPP-14480

MERGE INTO MESSAGE_MASTER A USING
(SELECT 'ALL_USERS_NOT_DISABLED_MSG' MSG_ID,
  'ALL_USERS_NOT_DISABLED_MSG' KEY,
  'ucl' ILS_MODULE,
  'ErrorMessage' BUNDLE_NAME
FROM DUAL
) B ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      MESSAGE_MASTER_ID,
      BUNDLE_NAME,
      KEY,
      MSG,
      ILS_MODULE,
      MSG_MODULE,
      MSG_ID,
      MSG_CLASS,
      MSG_TYPE,
      OVRIDE_ROLE,
      LOG_FLAG
    )
    VALUES
    (
      SEQ_MESSAGE_MASTER_ID.NEXTVAL,
      'ErrorMessage',
      'ALL_USERS_NOT_DISABLED_MSG',
      'The selected users could not be disabled because they are either disabled or have stale data.',
      'ucl',
      NULL,
      'ALL_USERS_NOT_DISABLED_MSG',
      NULL,
      NULL,
      NULL,
      NULL
    );

  MERGE INTO MESSAGE_MASTER A USING
  (SELECT 'ALL_USERS_NOT_DELETED_MSG' MSG_ID,
      'ALL_USERS_NOT_DELETED_MSG' KEY,
      'ucl' ILS_MODULE,
      'ErrorMessage' BUNDLE_NAME
    FROM DUAL
  )
  B ON (A.KEY = B.KEY AND A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      MESSAGE_MASTER_ID,
      BUNDLE_NAME,
      KEY,
      MSG,
      ILS_MODULE,
      MSG_MODULE,
      MSG_ID,
      MSG_CLASS,
      MSG_TYPE,
      OVRIDE_ROLE,
      LOG_FLAG
    )
    VALUES
    (
      SEQ_MESSAGE_MASTER_ID.NEXTVAL,
      'ErrorMessage',
      'ALL_USERS_NOT_DELETED_MSG',
      'The selected users could not be deleted because they are either deleted or have stale data.',
      'ucl',
      NULL,
      'ALL_USERS_NOT_DELETED_MSG',
      NULL,
      NULL,
      NULL,
      NULL
    );

  UPDATE message_master
  SET MSG          = 'Some of the selected users could not be disabled/deleted since they are group leaders of active user groups. All other selected users have been disabled/deleted.'
  WHERE BUNDLE_NAME='ErrorMessage'
  AND KEY          = 'GROUP_LEADER_USERS_NOT_DISABLED_MSG'
  AND ILS_MODULE   = 'ucl';

COMMIT;
  
  
  
-- DBTicket SCPP-14474
-- Rest of the LABEL entries were already present as part of SCPP-13763

MERGE INTO BUNDLE_META_DATA B
     USING (SELECT 'RelativeDateLabels' AS BUNDLE_NAME, 10 AS DEF_SCREEN_TYPE_ID
                 FROM DUAL) BD
        ON (B.BUNDLE_NAME = BD.BUNDLE_NAME AND B.DEF_SCREEN_TYPE_ID = BD.DEF_SCREEN_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (B.BUNDLE_META_DATA_ID,
               B.BUNDLE_NAME,
               B.DESCRIPTION,
               B.DEF_SCREEN_TYPE_ID,
               B.IS_ACTIVE,
               IS_LABEL_BUNDLE,
               DEF_BU_ID)
       VALUES (SEQ_BUNDLE_META_DATA_ID.NEXTVAL,
               'RelativeDateLabels',
               'RelativeDateLabels',
               10,
               1,
               1,
               -1);
			   
MERGE INTO LABEL L
     USING (SELECT 'Last One Hour' AS KEY, 'Last One Hour' AS VALUE, 'RelativeDateLabels' AS BUNDLE_NAME
                 FROM DUAL) B
        ON (L.KEY = B.KEY AND L.VALUE = B.VALUE AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Last One Hour',
               'Last One Hour',
               'RelativeDateLabels');


MERGE INTO LABEL L
     USING (SELECT 'Last Three Hours' AS KEY, 'Last Three Hours' AS VALUE, 'RelativeDateLabels' AS BUNDLE_NAME
                 FROM DUAL) B
        ON (L.KEY = B.KEY AND L.VALUE = B.VALUE AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Last Three Hours',
               'Last Three Hours',
               'RelativeDateLabels');
			   
MERGE INTO LABEL L
     USING (SELECT 'Last Six Hours' AS KEY, 'Last Six Hours' AS VALUE, 'RelativeDateLabels' AS BUNDLE_NAME
                 FROM DUAL) B
        ON (L.KEY = B.KEY AND L.VALUE = B.VALUE AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Last Six Hours',
               'Last Six Hours',
               'RelativeDateLabels');

MERGE INTO LABEL L
     USING (SELECT 'Last Twelve Hours' AS KEY, 'Last Twelve Hours' AS VALUE, 'RelativeDateLabels' AS BUNDLE_NAME
                 FROM DUAL) B
        ON (L.KEY = B.KEY AND L.VALUE = B.VALUE AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Last Twelve Hours',
               'Last Twelve Hours',
               'RelativeDateLabels');
			   
MERGE INTO LABEL L
     USING (SELECT 'Last Twenty-Four Hours' AS KEY, 'Last Twenty-Four Hours' AS VALUE, 'RelativeDateLabels' AS BUNDLE_NAME
                 FROM DUAL) B
        ON (L.KEY = B.KEY AND L.VALUE = B.VALUE AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Last Twenty-Four Hours',
               'Last Twenty-Four Hours',
               'RelativeDateLabels');
			   
MERGE INTO LABEL L
     USING (SELECT 'Now' AS KEY, 'Now' AS VALUE, 'RelativeDateLabels' AS BUNDLE_NAME
                 FROM DUAL) B
        ON (L.KEY = B.KEY AND L.VALUE = B.VALUE AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Now',
               'Now',
               'RelativeDateLabels');
			   

COMMIT;


-- DBTicket SCPP-14459

MERGE INTO message_master m
     USING (SELECT 'BusinessUnit_LISTED_SUCCESS_MSG' KEY, 'ErrorMessage' bundle_name FROM DUAL) d
        ON (m.key = d.key AND m.bundle_name = d.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               MSG,
               BUNDLE_NAME)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 'BusinessUnit_LISTED_SUCCESS_MSG',
                 'BusinessUnit_LISTED_SUCCESS_MSG',
                 'ucl',
                 'Business Unit list generated successfully.',
                 'ErrorMessage');

MERGE INTO message_master m
     USING (SELECT 'BusinessUnit_CREATED_SUCCESS_MSG' KEY, 'ErrorMessage' bundle_name FROM DUAL) d
        ON (m.key = d.key AND m.bundle_name = d.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               MSG,
               BUNDLE_NAME)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 'BusinessUnit_CREATED_SUCCESS_MSG',
                 'BusinessUnit_CREATED_SUCCESS_MSG',
                 'ucl',
                 'Business Unit {0} created successfully.',
                 'ErrorMessage');

MERGE INTO message_master m
     USING (SELECT 'BusinessUnit_UPDATED_SUCCESS_MSG' KEY, 'ErrorMessage' bundle_name FROM DUAL) d
        ON (m.key = d.key AND m.bundle_name = d.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               MSG,
               BUNDLE_NAME)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 'BusinessUnit_UPDATED_SUCCESS_MSG',
                 'BusinessUnit_UPDATED_SUCCESS_MSG',
                 'ucl',
                 'Business Unit {0} updated successfully.',
                 'ErrorMessage');

MERGE INTO message_master m
     USING (SELECT 'BusinessUnit_ENABLED_ALL_SUCCESS_MSG' KEY, 'ErrorMessage' bundle_name FROM DUAL) d
        ON (m.key = d.key AND m.bundle_name = d.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               MSG,
               BUNDLE_NAME)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 'BusinessUnit_ENABLED_ALL_SUCCESS_MSG',
                 'BusinessUnit_ENABLED_ALL_SUCCESS_MSG',
                 'ucl',
                 'The selected Business Units have been enabled.',
                 'ErrorMessage');

MERGE INTO message_master m
     USING (SELECT 'BusinessUnit_DISABLED_ALL_SUCCESS_MSG' KEY, 'ErrorMessage' bundle_name FROM DUAL) d
        ON (m.key = d.key AND m.bundle_name = d.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               MSG,
               BUNDLE_NAME)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 'BusinessUnit_DISABLED_ALL_SUCCESS_MSG',
                 'BusinessUnit_DISABLED_ALL_SUCCESS_MSG',
                 'ucl',
                 'The selected Business Units have been disabled.',
                 'ErrorMessage');
				 
MERGE INTO message_master m
     USING (SELECT 'BusinessUnit_ENABLED_NOTALL_SUCCESS_MSG' KEY, 'ErrorMessage' bundle_name FROM DUAL) d
        ON (m.key = d.key AND m.bundle_name = d.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               MSG,
               BUNDLE_NAME)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 'BusinessUnit_ENABLED_NOTALL_SUCCESS_MSG',
                 'BusinessUnit_ENABLED_NOTALL_SUCCESS_MSG',
                 'ucl',
                 'Some of the selected Business Units could not be enabled because they are already enabled. All other selected Business UnitS have been enabled.',
                 'ErrorMessage');
				 
				 
MERGE INTO message_master m
     USING (SELECT 'BusinessUnit_DISABLED_NOTALL_SUCCESS_MSG' KEY, 'ErrorMessage' bundle_name FROM DUAL) d
        ON (m.key = d.key AND m.bundle_name = d.bundle_name)
WHEN NOT MATCHED
THEN
   INSERT     (message_master_id,
               msg_id,
               key,
               ils_module,
               MSG,
               BUNDLE_NAME)
       VALUES (
                 seq_message_master_id.NEXTVAL,
                 'BusinessUnit_DISABLED_NOTALL_SUCCESS_MSG',
                 'BusinessUnit_DISABLED_NOTALL_SUCCESS_MSG',
                 'ucl',
                 'Some of the selected Business Units could not be disabled because they are already disabled. All other selected Business UnitS have been disabled.',
                 'ErrorMessage');

COMMIT;

-- DBTicket SCPP-14499

update label set value = 'Screen Lockout(minutes)' where key = 'lockouttime' and bundle_name = 'mps';

MERGE INTO LABEL L
     USING (SELECT 'mps' BUNDLE_NAME, 'server' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'server',
               'Server',
               'mps');
			   
COMMIT;

-- DBTicket SCPP-14546

update label set value = 'Copyright 2014' where key like 'CopyRight_Message';

COMMIT;

-- DBTicket SCPP-14573

MERGE INTO XBASE_MENU_ITEM X USING
(SELECT 100810096 XMENU_ITEM_ID, 59 XBASE_MENU_PART_ID FROM DUAL
) B ON ( X.XBASE_MENU_PART_ID = B.XBASE_MENU_PART_ID AND X.XMENU_ITEM_ID= B.XMENU_ITEM_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      X.XBASE_MENU_ITEM_ID,
      X.XBASE_MENU_PART_ID,
      X.XMENU_ITEM_ID,
      X.SEQUENCE
    )
    VALUES
    (
      234,
      59,
      100810096,
      6
    );

MERGE INTO XMENU_ITEM_APP X USING
  (SELECT 100810096 XMENU_ITEM_ID, 1008 APP_ID FROM DUAL
  )
  B ON (X.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND X.APP_ID = B.APP_ID)
WHEN NOT MATCHED THEN
  INSERT
    (X.XMENU_ITEM_ID, X.APP_ID
    ) VALUES
    (100810096, 1008
    );

COMMIT;


-- DBTicket SCPP-14588

MERGE INTO MESSAGE_MASTER M
	USING (SELECT '210102205' KEY, '210102205' MSG_ID, 'ErrorMessage' BUNDLE_NAME, 'reports' ILS_MODULE FROM DUAL) D
		ON (M.KEY = D.KEY AND M.MSG_ID = D.MSG_ID AND M.BUNDLE_NAME = D.BUNDLE_NAME AND M.ILS_MODULE = D.ILS_MODULE)
	WHEN NOT MATCHED
	THEN
		INSERT (MESSAGE_MASTER_ID,
			MSG_ID,
			KEY,
			ILS_MODULE,
			MSG,
			BUNDLE_NAME,
			MSG_CLASS,
			MSG_TYPE,
			OVRIDE_ROLE,
			LOG_FLAG)
		VALUES (
			SEQ_MESSAGE_MASTER_ID.NEXTVAL,
			'210102205',
			'210102205',
			'reports',
			'Report {0} exceeded the limit of maximum pages',
			'ErrorMessage',
			NULL,
			NULL,
			NULL,
			NULL);

			
MERGE INTO MESSAGE_MASTER M
	USING (SELECT '210102206' KEY, '210102206' MSG_ID, 'ErrorMessage' BUNDLE_NAME, 'reports' ILS_MODULE FROM DUAL) D
		ON (M.KEY = D.KEY AND M.MSG_ID = D.MSG_ID AND M.BUNDLE_NAME = D.BUNDLE_NAME AND M.ILS_MODULE = D.ILS_MODULE)
	WHEN NOT MATCHED
	THEN
		INSERT (MESSAGE_MASTER_ID,
			MSG_ID,
			KEY,
			ILS_MODULE,
			MSG,
			BUNDLE_NAME,
			MSG_CLASS,
			MSG_TYPE,
			OVRIDE_ROLE,
			LOG_FLAG)
		VALUES (
			SEQ_MESSAGE_MASTER_ID.NEXTVAL,
			'210102206',
			'210102206',
			'reports',
			'Report {0} exceeded the timeout limit',
			'ErrorMessage',
			NULL,
			NULL,
			NULL,
			NULL);
			
COMMIT;
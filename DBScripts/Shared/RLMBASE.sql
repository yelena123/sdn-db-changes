-- DBTicket RLM-2471

EXEC SEQUPDT ('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
EXEC SEQUPDT ('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');

MERGE INTO MODULE A
     USING (SELECT 'RLM' MODULE_CODE FROM DUAL) B
        ON (A.MODULE_CODE = B.MODULE_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (MODULE_ID,
               MODULE_NAME,
               IS_ACTIVE,
               MODULE_CODE)
       VALUES ( (SELECT MAX (MODULE_ID) + 1 FROM MODULE),
               'Reverse Logistics Management',
               1,
               'RLM');
			   
MERGE INTO APP_MODULE AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE
              FROM APP, MODULE
             WHERE APP.APP_NAME = 'Reverse Logistics Management'
                   AND MODULE.MODULE_CODE = 'RLM') APP1
        ON (AMP.APP_ID = APP1.APP_ID AND AMP.MODULE_ID = APP1.MODULE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               IS_ACTIVE,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Reverse Logistics Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'RLM'),
               1,
               (SELECT MAX (ROW_UID) + 4 FROM APP_MODULE));
			   
MERGE INTO PERMISSION L
     USING (SELECT 'RLM_VBR' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Company Parameter - RLM',
               1,
               -1,
               'RLM_VBR');

               
-- 'Administer Company Parameter - RLM' Permission
INSERT INTO PERMISSION(PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, COMPANY_ID, PERMISSION_CODE) 
VALUES(SEQ_PERMISSION_ID.NEXTVAL, 'Administer Company Parameter - RLM', 1, -1, 'RLM_ABR');

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID) 
VALUES((select app_id from app where app_name ='Reverse Logistics Management'),
(select module_id from module where MODULE_CODE ='RLM' ),
(select PERMISSION_ID from PERMISSION where PERMISSION_CODE='RLM_ABR'),
(SEQ_APP_MOD_PERM.nextval));


INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID) 
    VALUES((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
           (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'RLM_ABR'),
           (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));


insert into RELTP_APP_MOD_PERM(RELATIONSHIP_TYPE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,IS_PERM_IMPLIED) 
    VALUES ((SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Business Partner') 
                AND COMPANY_TYPE_ID = (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper'))),
           (SELECT APP_ID FROM APP WHERE APP_NAME='Reverse Logistics Management'),
           (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'RLM'),
           (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='RLM_ABR'),
           (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM), 0);
		   
INSERT INTO PERMISSION_INHERITANCE(PARENT_PERMISSION_ID, CHILD_PERMISSION_ID)  
  VALUES((SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'RLM_ABR'), (SELECT PERMISSION_ID FROM PERMISSION where PERMISSION_CODE = 'RLM_VBR'));

COMMIT;


-- DBTicket RLM-2628

BEGIN
   FOR i
      IN (SELECT ucc.table_name, ucc.column_name
            FROM user_tables ut, user_col_comments ucc
           WHERE     ut.table_name = ucc.table_name
                 AND ucc.comments IS NULL
                 AND ut.temporary = 'N'
                 AND ucc.column_name IN ('CREATED_DTTM','CREATED_DATE_TIME','CREATE_DATE_TIME'))
   LOOP
      EXECUTE IMMEDIATE
            'COMMENT ON COLUMN '
         || i.table_name
         || '.'
         || i.column_name
         || '  IS '
         || '''Created on Date and Time''';
   END LOOP;

   FOR i
      IN (SELECT ucc.table_name, ucc.column_name
            FROM user_tables ut, user_col_comments ucc
           WHERE     ut.table_name = ucc.table_name
                 AND ucc.comments IS NULL
                 AND ut.temporary = 'N'
                 AND ucc.column_name IN ('LAST_UPDATED_DTTM','MOD_DATE_TIME'))
   LOOP
      EXECUTE IMMEDIATE
            'COMMENT ON COLUMN '
         || i.table_name
         || '.'
         || i.column_name
         || '  IS '
         || '''Last Updated on Date and Time''';
   END LOOP;
END;
/

SHOW ERRORS;

COMMIT;


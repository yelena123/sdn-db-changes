
-- DBTicket TPE-3983


INSERT
INTO permission
  (
    permission_id,
    permission_name,
    is_active,
    company_id,
    permission_code,
    created_source_type_id,
    created_source,
    created_dttm,
    last_updated_source_type_id,
    last_updated_source,
    last_updated_dttm
  )
  VALUES
  (
    (SELECT MAX (PERMISSION_ID)+1 FROM PERMISSION
    )
    ,
    'open rejected adjustment invoices' ,
    1,
    -1,
    'IRI',
    3,
    'Manhattan Associates',
    sysdate,
    3,
    'Manhattan Associates',
    sysdate
  );

INSERT
INTO app_mod_perm
  (
    app_id ,
    module_id,
    permission_id,
    row_uid
  )
  VALUES
  (
    (SELECT APP_ID
      FROM APP
      WHERE APP_NAME = 'Transportation LifeCycle Management'
    )
    ,
    (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'FAP'
    ),
    (SELECT permission_id FROM permission WHERE permission_code ='IRI'
    ),
    (SELECT MAX(ROW_UID) FROM APP_MOD_PERM
    )+1
 );

INSERT
INTO company_type_perm
  (
    company_type_id,
    permission_id,
    row_uid
  )
  VALUES
  (
    (SELECT COMPANY_TYPE_ID
      FROM COMPANY_TYPE
      WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')
    )
    ,
    (SELECT permission_id FROM permission WHERE permission_code='IRI'
    ),
    (SELECT MAX(ROW_UID) FROM COMPANY_TYPE_PERM
    )+1
);

INSERT
INTO label
  (
    label_id,
    KEY,
    value,
    bundle_name
  )
  VALUES
  (
    seq_label_id.nextval ,
    'UCLPermissions_open rejected adjustment invoices',
    'open rejected adjustment invoices',
    'UCL'
  );
  
 
COMMIT;  

---- DBTicket TPE-4112
----Commenting as the enhancement is moved to CM layer
----insert into xscreen
----       (XSCREEN_ID,
----        NAME,
----                DESCRIPTION,
----                SCREEN_TYPE,
----                SCREEN_VERSION,
----                SCREEN_MODE,
----                IS_RESIZABLE,
----                CREATED_SOURCE_TYPE,
----                CREATED_DTTM,
----                LAST_UPDATED_SOURCE_TYPE,
----                LAST_UPDATED_DTTM,
----                DEFAULT_X,
----                DEFAULT_Y,
----                DEFAULT_WIDTH,
----                DEFAULT_HEIGHT)
----                values
----                (240014,
----                'Rail',
----                'Rail Rout',
----                1,
----                2,
----                0,
----                1,
----                1,
----                systimestamp,
----                1,
----                systimestamp,
----                125,
----                125,
----                600,
----                400);
----
----
----COMMIT;




-- DBTicket TPE-15912

----INSERT
----INTO XFIELD
----  (
----    XFIELD_ID,
----    NAME,
----    DESCRIPTION,
----    XSCREEN_ID,
----    IS_DISPLAYABLE,
----    IS_FILTERABLE,
----    IS_MOVABLE,
----    IS_HEADER,
----    DATATYPE,
----    XTYPE,
----    XURL,
----    XPICKS,
----    CREATED_SOURCE,
----    CREATED_SOURCE_TYPE,
----    CREATED_DTTM,
----    LAST_UPDATED_SOURCE,
----    LAST_UPDATED_SOURCE_TYPE,
----    LAST_UPDATED_DTTM,
----    BASE_PART_NAME,
----    BASE_SECTION_NAME,
----    IS_REQUIRED
----  )
----  VALUES
----  (
----    seq_xfield_id.NEXTVAL,
----    'ocarrier',
----    'Origin Carrier Code',
----    240014,
----    1,
----    1,
----    1,
----    1,
----    0,
----    3,
----    '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierLookup',
----    NULL,
----    NULL,
----    1,
----    systimestamp,
----    NULL,
----    1,
----    systimestamp,
----    'GENERAL',
----    'GENERAL',
----    0
----  );
----
----INSERT
----INTO XFIELD
----  (
----    XFIELD_ID,
----    NAME,
----    DESCRIPTION,
----    XSCREEN_ID,
----    IS_DISPLAYABLE,
----    IS_FILTERABLE,
----    IS_MOVABLE,
----    IS_HEADER,
----    DATATYPE,
----    XTYPE,
----    XURL,
----    XPICKS,
----    CREATED_SOURCE,
----    CREATED_SOURCE_TYPE,
----    CREATED_DTTM,
----    LAST_UPDATED_SOURCE,
----    LAST_UPDATED_SOURCE_TYPE,
----    LAST_UPDATED_DTTM,
----    BASE_PART_NAME,
----    BASE_SECTION_NAME,
----    IS_REQUIRED
----  )
----  VALUES
----  (
----    seq_xfield_id.NEXTVAL,
----    'dcarrier',
----    'Destination Carrier Code',
----    240014,
----    1,
----    1,
----    1,
----    1,
----    0,
----    3,
----    '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/carrierLookup',
----    NULL,
----    NULL,
----    1,
----    systimestamp,
----    NULL,
----    1,
----    systimestamp,
----    'GENERAL',
----    'GENERAL',
----    0
----  );
----  
----INSERT
----INTO XFIELD
----  (
----    XFIELD_ID,
----    NAME,
----    DESCRIPTION,
----    XSCREEN_ID,
----    IS_DISPLAYABLE,
----    IS_FILTERABLE,
----    IS_MOVABLE,
----    IS_HEADER,
----    DATATYPE,
----    XTYPE,
----    XURL,
----    XPICKS,
----    CREATED_SOURCE,
----    CREATED_SOURCE_TYPE,
----    CREATED_DTTM,
----    LAST_UPDATED_SOURCE,
----    LAST_UPDATED_SOURCE_TYPE,
----    LAST_UPDATED_DTTM,
----    BASE_PART_NAME,
----    BASE_SECTION_NAME,
----    IS_REQUIRED
----  )
----  VALUES
----  (
----    seq_xfield_id.NEXTVAL,
----    'ofacility',
----    'Origin Facility',
----    240014,
----    1,
----    1,
----    1,
----    1,
----    0,
----    3,
----    '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup',
----    'ctmgt.facilityLookUp',
----    NULL,
----    1,
----    systimestamp,
----    NULL,
----    1,
----    systimestamp,
----    'GENERAL',
----    'GENERAL',
----    0
----  );
----  
----INSERT
----INTO XFIELD
----  (
----    XFIELD_ID,
----    NAME,
----    DESCRIPTION,
----    XSCREEN_ID,
----    IS_DISPLAYABLE,
----    IS_FILTERABLE,
----    IS_MOVABLE,
----    IS_HEADER,
----    DATATYPE,
----    XTYPE,
----    XURL,
----    XPICKS,
----    CREATED_SOURCE,
----    CREATED_SOURCE_TYPE,
----    CREATED_DTTM,
----    LAST_UPDATED_SOURCE,
----    LAST_UPDATED_SOURCE_TYPE,
----    LAST_UPDATED_DTTM,
----    BASE_PART_NAME,
----    BASE_SECTION_NAME,
----    IS_REQUIRED
----  )
----  VALUES
----  (
----    seq_xfield_id.NEXTVAL,
----    'dfacility',
----    'Destination Facility',
----    240014,
----    1,
----    1,
----    1,
----    1,
----    0,
----    3,
----    '/services/rest/contractmanagement/ContractManagementLookupServices/contractManagementLookupServices/facilityLookup',
----    'ctmgt.facilityLookUp',
----    NULL,
----    1,
----    systimestamp,
----    NULL,
----    1,
----    systimestamp,
----    'GENERAL',
----    'GENERAL',
----    0
----  );
----  
----INSERT
----INTO XFIELD
----  (
----    XFIELD_ID,
----    NAME,
----    DESCRIPTION,
----    XSCREEN_ID,
----    IS_DISPLAYABLE,
----    IS_FILTERABLE,
----    IS_MOVABLE,
----    IS_HEADER,
----    DATATYPE,
----    XTYPE,
----    XURL,
----    XPICKS,
----    CREATED_SOURCE,
----    CREATED_SOURCE_TYPE,
----    CREATED_DTTM,
----    LAST_UPDATED_SOURCE,
----    LAST_UPDATED_SOURCE_TYPE,
----    LAST_UPDATED_DTTM,
----    BASE_PART_NAME,
----    BASE_SECTION_NAME,
----    IS_REQUIRED
----  )
----  VALUES
----  (
----    seq_xfield_id.NEXTVAL,
----    'vnumber',
----    'Voyage Number',
----    240014,
----    1,
----    1,
----    1,
----    1,
----    0,
----    0,
----    NULL,
----    NULL,
----    NULL,
----    1,
----    systimestamp,
----    NULL,
----    1,
----    systimestamp,
----    'GENERAL',
----    'GENERAL',
----    0
----  );
----  
----INSERT
----INTO XFIELD
----  (
----    XFIELD_ID,
----    NAME,
----    DESCRIPTION,
----    XSCREEN_ID,
----    IS_DISPLAYABLE,
----    IS_FILTERABLE,
----    IS_MOVABLE,
----    IS_HEADER,
----    DATATYPE,
----    XTYPE,
----    XURL,
----    XPICKS,
----    CREATED_SOURCE,
----    CREATED_SOURCE_TYPE,
----    CREATED_DTTM,
----    LAST_UPDATED_SOURCE,
----    LAST_UPDATED_SOURCE_TYPE,
----    LAST_UPDATED_DTTM,
----    BASE_PART_NAME,
----    BASE_SECTION_NAME,
----    IS_REQUIRED
----  )
----  VALUES
----  (
----    seq_xfield_id.NEXTVAL,
----    'railRoutname',
----    'Rail Route Name',
----    240014,
----    1,
----    1,
----    1,
----    1,
----    0,
----    0,
----    NULL,
----    NULL,
----    NULL,
----    1,
----    systimestamp,
----    NULL,
----    1,
----    systimestamp,
----    'GENERAL',
----    'GENERAL',
----    0
----  );
----  
----UPDATE xscreen SET DESCRIPTION ='Rail Route' WHERE XSCREEN_ID=240014;
----
----COMMIT;


---- DBTicket TPE-34535
----Commenting as the enhancement is moved to CM layer

----MERGE INTO XSCREEN L
----     USING (SELECT 'Rail' NAME FROM DUAL) B
----        ON (L.NAME = B.NAME)
----WHEN NOT MATCHED
----THEN
----   insert 
----       (XSCREEN_ID,
----        NAME,
----                DESCRIPTION,
----                SCREEN_TYPE,
----                SCREEN_VERSION,
----                SCREEN_MODE,
----                IS_RESIZABLE,
----                CREATED_SOURCE_TYPE,
----                CREATED_DTTM,
----                LAST_UPDATED_SOURCE_TYPE,
----                LAST_UPDATED_DTTM,
----                DEFAULT_X,
----                DEFAULT_Y,
----                DEFAULT_WIDTH,
----                DEFAULT_HEIGHT)
----                values
----                (240014,
----                'Rail',
----                'Rail Rout',
----                1,
----                2,
----                0,
----                1,
----                1,
----                systimestamp,
----                1,
----                systimestamp,
----                125,
----                125,
----                600,
----                400);
----
----MERGE INTO XFIELD A USING
----(SELECT 'rule11' NAME, 240014 XSCREEN_ID FROM DUAL
----) B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----  INSERT
----    (
----      XFIELD_ID,
----      NAME,
----      DESCRIPTION,
----      XSCREEN_ID,
----      IS_DISPLAYABLE,
----      IS_FILTERABLE,
----      IS_MOVABLE,
----      IS_HEADER,
----      DATATYPE,
----      XTYPE,
----      XURL,
----      XPICKS,
----      CREATED_SOURCE,
----      CREATED_SOURCE_TYPE,
----      CREATED_DTTM,
----      LAST_UPDATED_SOURCE,
----      LAST_UPDATED_SOURCE_TYPE,
----      LAST_UPDATED_DTTM,
----      BASE_PART_NAME,
----      BASE_SECTION_NAME,
----      IS_REQUIRED
----    )
----    VALUES
----    (
----      seq_xfield_id.NEXTVAL,
----      'rule11',
----      'Rule 11',
----      240014,
----      1,
----      1,
----      1,
----      1,
----      0,
----      1,
----      NULL,
----      'true,false',
----      NULL,
----      1,
----      systimestamp,
----      NULL,
----      1,
----      systimestamp,
----      'GENERAL',
----      'GENERAL',
----      0
----    );
----
----  MERGE INTO XFIELD A USING
----  (SELECT 'Rail_Route_Id' NAME, 240014 XSCREEN_ID FROM DUAL
----  )
----  B ON (A.NAME = B.NAME AND A.XSCREEN_ID = B.XSCREEN_ID)
----WHEN NOT MATCHED THEN
----  INSERT
----    (
----      XFIELD_ID,
----      NAME,
----      DESCRIPTION,
----      XSCREEN_ID,
----      IS_DISPLAYABLE,
----      IS_FILTERABLE,
----      IS_MOVABLE,
----      IS_HEADER,
----      DATATYPE,
----      XTYPE,
----      XURL,
----      XPICKS,
----      CREATED_SOURCE,
----      CREATED_SOURCE_TYPE,
----      CREATED_DTTM,
----      LAST_UPDATED_SOURCE,
----      LAST_UPDATED_SOURCE_TYPE,
----      LAST_UPDATED_DTTM,
----      BASE_PART_NAME,
----      BASE_SECTION_NAME,
----      IS_REQUIRED
----    )
----    VALUES
----    (
----      seq_xfield_id.NEXTVAL,
----      'Rail_Route_Id',
----      'Rail Route Id',
----      240014,
----      1,
----      1,
----      1,
----      1,
----      0,
----      0,
----      NULL,
----      NULL,
----      NULL,
----      1,
----      systimestamp,
----      NULL,
----      1,
----      systimestamp,
----      'GENERAL',
----      'GENERAL',
----      0
----    );
----  
----  COMMIT;




-- DBTicket CMGT-787

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
 
INSERT INTO APP_MOD_PERM
    (APP_ID,
     MODULE_ID,
     PERMISSION_ID,
     ROW_UID
    )
VALUES ((SELECT APP_ID
	FROM APP
       WHERE APP_NAME = 'Transportation LifeCycle Management'),
     (SELECT MODULE_ID
	FROM MODULE
       WHERE MODULE_NAME = 'Common Business Object - Base'),
     (SELECT PERMISSION_ID
	FROM PERMISSION
       WHERE PERMISSION_CODE = 'VSLAOGINF'),
     SEQ_APP_MOD_PERM.nextval
    );

INSERT INTO APP_MOD_PERM
    (APP_ID,
     MODULE_ID,
     PERMISSION_ID,
     ROW_UID
    )
VALUES ((SELECT APP_ID
	FROM APP
       WHERE APP_NAME = 'Transportation LifeCycle Management'),
     (SELECT MODULE_ID
	FROM MODULE
       WHERE MODULE_NAME = 'Common Business Object - Base'),
     (SELECT PERMISSION_ID
	FROM PERMISSION
       WHERE PERMISSION_CODE = 'ADSLAOGINF'),
     SEQ_APP_MOD_PERM.nextval
    );

	
COMMIT;



-- DBTicket TPE-35185

MERGE INTO COMPANY_TYPE_PERM ctp USING
(SELECT
  (SELECT COMPANY_TYPE_ID
  FROM company_type
  WHERE lower(DESCRIPTION) = lower('Shipper')
  ) company_type_id,
  (SELECT permission_id FROM permission WHERE permission_code='VAUTRL'
  ) permission_id
FROM DUAL
) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id)
WHEN NOT MATCHED THEN
  INSERT
    (
      ctp.company_type_id,
      ctp.permission_id,
      ctp.row_uid
    )
    VALUES
    (
      ctp1.company_type_id,
      ctp1.permission_id,
      (SELECT MAX (row_uid) + 4 FROM company_type_perm
      )
    );
	
COMMIT;	


-- DBTicket TPE-35342

exec sequpdt('FACILITY','FACILITY_ID','SEQ_FACILITY_ID');
exec sequpdt('SIZE_UOM','SIZE_UOM_ID','UOM_ID_SEQ');
exec sequpdt('MOT','MOT_ID','MOT_ID_SEQ');
 
-- DBTicket TPE-35527

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_NAME,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_NAME = 'Supply Chain Process Platform'
                   AND PERMISSION.PERMISSION_CODE = 'ADMINSCREEN') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_NAME = 'Supply Chain Process Platform'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADMINSCREEN'),
               (SEQ_APP_MOD_PERM.nextval));

			   
MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_NAME,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_NAME = 'Supply Chain Process Platform'
                   AND PERMISSION.PERMISSION_CODE = 'ADMINRES') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_NAME = 'Supply Chain Process Platform'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADMINRES'),
               (SEQ_APP_MOD_PERM.nextval));
			   
			   
MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_NAME,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_NAME = 'Supply Chain Process Platform'
                   AND PERMISSION.PERMISSION_CODE = 'ADMINUCG') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_NAME = 'Supply Chain Process Platform'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADMINUCG'),
               (SEQ_APP_MOD_PERM.nextval));


MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_NAME,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_NAME = 'Supply Chain Process Platform'
                   AND PERMISSION.PERMISSION_CODE = 'ADMINPER') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_NAME = 'Supply Chain Process Platform'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADMINPER'),
               (SEQ_APP_MOD_PERM.nextval));
			   


MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT COMPANY_TYPE.COMPANY_TYPE_ID,
                   COMPANY_TYPE.DESCRIPTION,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM COMPANY_TYPE, PERMISSION
             WHERE COMPANY_TYPE.COMPANY_TYPE_ID = 4
                   AND PERMISSION.PERMISSION_CODE = 'ADMINSCREEN') CTP1
        ON (CTP.COMPANY_TYPE_ID = CTP1.COMPANY_TYPE_ID
            AND CTP.PERMISSION_ID = CTP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
       VALUES (4,
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADMINSCREEN'),
               (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));


MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT COMPANY_TYPE.COMPANY_TYPE_ID,
                   COMPANY_TYPE.DESCRIPTION,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM COMPANY_TYPE, PERMISSION
             WHERE COMPANY_TYPE.COMPANY_TYPE_ID = 4
                   AND PERMISSION.PERMISSION_CODE = 'ADMINRES') CTP1
        ON (CTP.COMPANY_TYPE_ID = CTP1.COMPANY_TYPE_ID
            AND CTP.PERMISSION_ID = CTP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
       VALUES (4,
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADMINRES'),
               (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
			   
			   
MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT COMPANY_TYPE.COMPANY_TYPE_ID,
                   COMPANY_TYPE.DESCRIPTION,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM COMPANY_TYPE, PERMISSION
             WHERE COMPANY_TYPE.COMPANY_TYPE_ID = 4
                   AND PERMISSION.PERMISSION_CODE = 'ADMINUCG') CTP1
        ON (CTP.COMPANY_TYPE_ID = CTP1.COMPANY_TYPE_ID
            AND CTP.PERMISSION_ID = CTP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
       VALUES (4,
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADMINUCG'),
               (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));		


MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT COMPANY_TYPE.COMPANY_TYPE_ID,
                   COMPANY_TYPE.DESCRIPTION,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM COMPANY_TYPE, PERMISSION
             WHERE COMPANY_TYPE.COMPANY_TYPE_ID = 4
                   AND PERMISSION.PERMISSION_CODE = 'ADMINPER') CTP1
        ON (CTP.COMPANY_TYPE_ID = CTP1.COMPANY_TYPE_ID
            AND CTP.PERMISSION_ID = CTP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
       VALUES (4,
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADMINPER'),
               (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));		

COMMIT;


-- DBTicket TPE-35680

DELETE FROM ROLE_APP_MOD_PERM RAMP
WHERE RAMP.APP_ID IN (SELECT APP_ID FROM APP  WHERE APP_SHORT_NAME='TPE') 
AND RAMP.MODULE_ID IN (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'TRANS') 
AND RAMP.PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'BDOLPN'); 

DELETE FROM APP_MOD_PERM AMP
WHERE AMP.APP_ID IN (SELECT APP_ID FROM APP  WHERE APP_SHORT_NAME='TPE') 
AND AMP.MODULE_ID IN (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'TRANS') 
AND AMP.PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'BDOLPN');

COMMIT;

-- DBTicket TPE-35786

exec sequpdt('ROLE_APP_MOD_PERM','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');
exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

INSERT INTO PERMISSION(
    permission_id,
    permission_name,
    is_active,
    company_id,
    permission_code,
    created_source_type_id,
    created_source,
    created_dttm,
    last_updated_source_type_id,
    last_updated_source,
    last_updated_dttm,
	DESCRIPTION
  )
     VALUES ( SEQ_PERMISSION_ID.nextval,
             'Tab - Transportation Lifecycle Management',
             1,
             '-1',
             'TTLM',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             NULL);

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE DESCRIPTION = 'Shipper'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code IN ('TTLM')),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO LABEL (LABEL_ID, KEY,VALUE,BUNDLE_NAME)
     VALUES ( SEQ_LABEL_ID.NEXTVAL,
             'UCLPermissions_Tab - Transportation Lifecycle Management',
             'Tab - Transportation Lifecycle Management',
             'UCL');

INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID,PERMISSION_ID,ROW_UID)
     VALUES ( (SELECT App_Id
                 FROM App
                WHERE App_Short_Name = 'TPE'),
             (SELECT Module_Id
                FROM Module
               WHERE Module_Code = 'TRANS'),
             (SELECT Permission_Id
                FROM Permission
               WHERE Permission_Code = 'TTLM'),
             SEQ_APP_MOD_PERM.NEXTVAL);


INSERT INTO PERMISSION(
    permission_id,
    permission_name,
    is_active,
    company_id,
    permission_code,
    created_source_type_id,
    created_source,
    created_dttm,
    last_updated_source_type_id,
    last_updated_source,
    last_updated_dttm,
	DESCRIPTION
  )
     VALUES ( SEQ_PERMISSION_ID.nextval,
             'Tab - Contract Management',
             1,
             '-1',
             'TCMGT',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             NULL);

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE DESCRIPTION = 'Shipper'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code IN ('TCMGT')),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO LABEL (LABEL_ID, KEY,VALUE,BUNDLE_NAME)
     VALUES ( SEQ_LABEL_ID.NEXTVAL,
             'UCLPermissions_Tab - Contract Management',
             'Tab - Contract Management',
             'UCL');

INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID,PERMISSION_ID,ROW_UID)
     VALUES ( (SELECT App_Id
                 FROM App
                WHERE App_Short_Name = 'TPE'),
             (SELECT Module_Id
                FROM Module
               WHERE Module_Code = 'CNTRMGT'),
             (SELECT Permission_Id
                FROM Permission
               WHERE Permission_Code = 'TCMGT'),
             SEQ_APP_MOD_PERM.NEXTVAL);



INSERT INTO PERMISSION(
    permission_id,
    permission_name,
    is_active,
    company_id,
    permission_code,
    created_source_type_id,
    created_source,
    created_dttm,
    last_updated_source_type_id,
    last_updated_source,
    last_updated_dttm,
	DESCRIPTION
  )
     VALUES ( SEQ_PERMISSION_ID.nextval,
             'Tab - Yard Management',
             1,
             '-1',
             'TYARD',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             NULL);

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE DESCRIPTION = 'Shipper'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code IN ('TYARD')),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO LABEL (LABEL_ID, KEY,VALUE,BUNDLE_NAME)
     VALUES ( SEQ_LABEL_ID.NEXTVAL,
             'UCLPermissions_Tab - Yard Management',
             'Tab - Yard Management',
             'UCL');

INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID,PERMISSION_ID,ROW_UID)
     VALUES ( (SELECT App_Id
                 FROM App
                WHERE App_Short_Name = 'TPE'),
             (SELECT Module_Id
                FROM Module
               WHERE Module_Code = 'YARD'),
             (SELECT Permission_Id
                FROM Permission
               WHERE Permission_Code = 'TYARD'),
             SEQ_APP_MOD_PERM.NEXTVAL);


INSERT INTO PERMISSION(
    permission_id,
    permission_name,
    is_active,
    company_id,
    permission_code,
    created_source_type_id,
    created_source,
    created_dttm,
    last_updated_source_type_id,
    last_updated_source,
    last_updated_dttm,
	DESCRIPTION
  )
     VALUES ( SEQ_PERMISSION_ID.nextval,
             'Tab - Logistics Gateway',
             1,
             '-1',
             'TLG',
             3,
             'Manhattan Associates',
             SYSDATE,
             3,
             'Manhattan Associates',
             SYSDATE,
             NULL);

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE DESCRIPTION = 'Carrier'),
             (SELECT permission_id
                FROM permission
               WHERE permission_code IN ('TLG')),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

INSERT INTO LABEL (LABEL_ID, KEY,VALUE,BUNDLE_NAME)
     VALUES ( SEQ_LABEL_ID.NEXTVAL,
             'UCLPermissions_Tab - Logistics Gateway',
             'Tab - Logistics Gateway',
             'UCL');

INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID,PERMISSION_ID,ROW_UID)
     VALUES ( (SELECT App_Id
                 FROM App
                WHERE App_Short_Name = 'LG'),
             (SELECT Module_Id
                FROM Module
               WHERE Module_Code = 'RAM'),
             (SELECT Permission_Id
                FROM Permission
               WHERE Permission_Code = 'TLG'),
             SEQ_APP_MOD_PERM.NEXTVAL);
			 
COMMIT;


CREATE OR REPLACE PROCEDURE UPDATE_TAB_PERMISSION
IS
BEGIN
   FOR SHIPPER_PERMISSION
      IN (SELECT DISTINCT (AC.ROLE_ID)
            FROM ACCESS_CONTROL AC, COMPANY_TYPE CT, COMPANY COMP
           WHERE     AC.COMPANY_ID = COMP.COMPANY_ID
                 AND COMP.COMPANY_TYPE_ID = CT.COMPANY_TYPE_ID
                 AND CT.DESCRIPTION = 'Shipper')
   LOOP
      INSERT INTO ROLE_APP_MOD_PERM(ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID)
           VALUES (SHIPPER_PERMISSION.ROLE_ID,
                   (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'TPE'),
                   (SELECT MODULE_ID
                      FROM MODULE
                     WHERE MODULE_CODE = 'TRANS'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'TTLM'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

      INSERT INTO ROLE_APP_MOD_PERM(ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID)
           VALUES (SHIPPER_PERMISSION.ROLE_ID,
                   (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'TPE'),
                   (SELECT MODULE_ID
                      FROM MODULE
                     WHERE MODULE_CODE = 'CNTRMGT'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'TCMGT'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

      INSERT INTO ROLE_APP_MOD_PERM(ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID)
           VALUES (SHIPPER_PERMISSION.ROLE_ID,
                   (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'TPE'),
                   (SELECT MODULE_ID
                      FROM MODULE
                     WHERE MODULE_CODE = 'YARD'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'TYARD'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END LOOP;


   FOR CARRIER_PERMISSION
      IN (SELECT DISTINCT (AC.ROLE_ID)
            FROM ACCESS_CONTROL AC, COMPANY_TYPE CT, COMPANY COMP
           WHERE     AC.COMPANY_ID = COMP.COMPANY_ID
                 AND COMP.COMPANY_TYPE_ID = CT.COMPANY_TYPE_ID
                 AND CT.DESCRIPTION = 'Carrier')
   LOOP
      INSERT INTO ROLE_APP_MOD_PERM(ROLE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID)
           VALUES (CARRIER_PERMISSION.ROLE_ID,
                   (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'LG'),
                   (SELECT MODULE_ID
                      FROM MODULE
                     WHERE MODULE_CODE = 'RAM'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'TLG'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);
   END LOOP;
   
   COMMIT;
END UPDATE_TAB_PERMISSION;
/

SHOW ERRORS;

EXEC UPDATE_TAB_PERMISSION;

DROP PROCEDURE UPDATE_TAB_PERMISSION;

-- DBTicket TPE-35807

DELETE FROM  ROLE_APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MSA') AND APP_ID = 24;
DELETE FROM  REL_APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MSA') AND APP_ID = 24;
DELETE FROM  RELTP_APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MSA') AND APP_ID = 24;
DELETE FROM  NAVIGATION_APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MSA') AND APP_ID = 24;
DELETE FROM  APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MSA') AND APP_ID = 24;

COMMIT;

-- DBTicket TPE-36017

DELETE FROM  ROLE_APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CLSHP') AND APP_ID = 24;
DELETE FROM  REL_APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CLSHP') AND APP_ID = 24;
DELETE FROM  RELTP_APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CLSHP') AND APP_ID = 24;
DELETE FROM  NAVIGATION_APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CLSHP') AND APP_ID = 24;
DELETE FROM  APP_MOD_PERM WHERE PERMISSION_ID IN (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CLSHP') AND APP_ID = 24;

COMMIT;


---- DBTicket TPE-36055

----delete from xsolution_app where app_id in (28,40,32,128);

----COMMIT;

-- DBTicket TPE-36166

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO APP_MOD_PERM AMP USING
(SELECT
  (SELECT app_id FROM app WHERE app_name ='Transportation LifeCycle Management') AS APP_ID,
  (SELECT module_id FROM module WHERE MODULE_CODE ='OSE') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='CLSHP') AS PERMISSION_ID
FROM DUAL
) C ON (AMP.APP_ID = C.APP_ID AND AMP.MODULE_ID = C.MODULE_ID AND AMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT app_id FROM app WHERE app_name ='Transportation LifeCycle Management'),
      (SELECT module_id FROM module WHERE MODULE_CODE ='OSE'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='CLSHP'),
      (SEQ_APP_MOD_PERM.nextval)
    );

COMMIT;

-- DBTicket TPE-36892
-- Linked CR - TPE-35786

EXEC SEQUPDT('REL_APP_MOD_PERM','ROW_UID','SEQ_RELAPPMODPERM_ROWUID');

INSERT INTO RELTP_APP_MOD_PERM
            (RELATIONSHIP_TYPE_ID, APP_ID,
             MODULE_ID,
             PERMISSION_ID,
             ROW_UID, IS_PERM_IMPLIED
            )
     VALUES (4, (SELECT App_Id
                 FROM App
                WHERE App_Short_Name = 'TPE'),
             (SELECT Module_Id
                FROM Module
               WHERE Module_Code = 'TRANS'),
             (SELECT Permission_Id
                FROM Permission
               WHERE Permission_Code = 'TTLM'),
             (SELECT MAX (ROW_UID)
                FROM RELTP_APP_MOD_PERM) + 4, 0
            );
			
COMMIT;

-- DBTicket TP-3277

exec  sequpdt ('BINDING_TYPE','BINDING_TYPE_ID','SEQ_BINDING_TYPE_ID');
exec  sequpdt ('CUSTOM_ATTRIBUTE','CUSTOM_ATTRIBUTE_ID','SEQ_CUSTOM_ATTRIBUTE_ID');
exec  sequpdt ('DASHBOARD','DASHBOARD_ID','SEQ_DASHBOARD_ID');
exec  sequpdt ('FAC_SCHDL_REG_DAY_DTL','FAC_REG_DAY_DTL_ID','SEQ_FAC_REG_DAY_DTL_ID');
exec  sequpdt ('INCOTERM','INCOTERM_ID','SEQ_INCOTERM_ID');
exec  sequpdt ('LABOR_ACTIVITY','LABOR_ACTIVITY_ID','LABOR_ACTIVITY_ID_SEQ');
exec  sequpdt ('LEMA_RULE_CONF_RES_ATTR','IDENTITY','LEMA_RULE_CONF_RES_ATTR_S');
exec  sequpdt ('LEMA_RULE_CONF_RES_ATTR_VAL','IDENTITY','LEMA_RULE_CONF_RES_ATTR_VAL_S');
exec  sequpdt ('MOT','MOT_ID','MOT_ID_SEQ');
exec  sequpdt ('NAVIGATION','NAVIGATION_ID','NAVIGATION_ID_SEQ');
exec  sequpdt ('UI_MENU_ITEM_PERMISSION','UI_MENU_ITEM_PERMISSION_ID','UI_MENU_ITEM_PERM_SEQ');
exec  sequpdt ('USER_DEFAULT','USER_DEFAULT_ID','SEQ_USER_DEFAULT_ID');
exec  sequpdt ('XBASE_MENU_PART','XBASE_MENU_PART_ID','SEQ_XBASE_MENU_PART_ID');
exec  sequpdt('ACCESS_CONTROL','ACCESS_CONTROL_ID','SEQ_ACCESS_CONTROL_ID');
exec  sequpdt('APP_INSTANCE','APP_INSTANCE_ID','SEQ_APP_INSTANCE_ID');
exec  sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
exec  sequpdt('CHANNEL','CHANNEL_ID','SEQ_CHANNEL_ID');
exec  sequpdt('COMPANY','COMPANY_ID','SEQ_COMPANY_ID');
exec  sequpdt('COMPANY_APP','ROW_UID','SEQ_COAPP_ROWUID');
exec  sequpdt('COMPANY_APP_MODULE','ROW_UID','SEQ_COAPPMOD_ROWUID');
exec  sequpdt('COMPANY_PARAM','PARAMETER_ID','SEQ_PARAMETER_ID');
exec  sequpdt('DSHBRD_COGNOS_PTLT_DTL','DSHBRD_COGNOS_PTLT_DTL_ID','SEQ_DSHBRD_COGNOS_PTLT_DTL_ID');
exec  sequpdt('DSHBRD_PORTLET','DSHBRD_PORTLET_ID','SEQ_DSHBRD_PORTLET_ID');
exec  sequpdt('DSHBRD_PORTLET_PARAM_DEF','DSHBRD_PTLT_PARAM_DEF_ID','SEQ_DSHBRD_PTLT_PARAM_DEF_ID');
exec  sequpdt('DSHBRD_PORTLET_TAGS','DSHBRD_PORTLET_TAGS_ID','SEQ_DSHBRD_PORTLET_TAGS_ID');
exec  sequpdt('DSHBRD_UIB_PTLT_DTL','DSHBRD_UIB_PTLT_DTL_ID','SEQ_DSHBRD_UIB_PTLT_DTL_ID');
exec  sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec  sequpdt('REGION','REGION_ID','REGION_ID_SEQ');
exec  sequpdt('REL_APP_MOD_PERM','ROW_UID','SEQ_RELAPPMODPERM_ROWUID');
exec  sequpdt('REL_GEO_REGION','ROW_UID','SEQ_RELGEOREGION_ROWUID');
exec  sequpdt('RELATIONSHIP','RELATIONSHIP_ID','SEQ_RELATIONSHIP_ID');
exec  sequpdt('ROLE','ROLE_ID','SEQ_ROLE_ID');
exec  sequpdt('ROLE_APP_MOD_PERM','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');
exec  sequpdt('UCL_USER','UCL_USER_ID','SEQ_UCL_USER_ID');
exec  sequpdt('UCL_USER_LOGIN_HISTORY','LOGIN_LOGOUT_ID','SEQ_LOGIN_LOGOUT_ID');
exec  sequpdt('UIB_QUERY','UIB_QUERY_ID','SEQ_UIB_QUERY_ID');
exec  sequpdt('UIB_QUERY_DETAILS','UIB_QUERY_DETAILS_ID','SEQ_UIB_QUERY_DETAILS_ID');
exec  sequpdt('UIB_SMRY_VIEW_DTL','UIB_SMRY_VIEW_DTL_ID','SEQ_UIB_SMRY_VIEW_DTL_ID');
exec  sequpdt('UIB_SP_DETAILS','UIB_SP_DETAILS_ID','SEQ_UIB_SP_DETAILS_ID');
exec  sequpdt('UIB_VIEW_PROP_SET_DTL','UIB_VIEW_PROP_SET_DTL_ID','SEQ_UIB_VIEW_PROP_SET_DTL_ID');
exec  sequpdt('USER_DEFAULT','USER_DEFAULT_ID','SEQ_USER_DEFAULT_ID');
exec  sequpdt('USER_GROUP','USER_GROUP_ID','SEQ_USER_GROUP_ID');
exec  sequpdt('XSOLUTION','XSOLUTION_ID','SEQ_XSOLUTION_ID');
exec  sequpdt('PERFORMANCE_FACTOR','PERFORMANCE_FACTOR_ID','SEQ_PERFORMANCE_FACTOR_ID');

-- DBTicket TP-3309 REFL

INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID,PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT App_Id
                 FROM App
                WHERE App_Short_Name = 'TP'),
             (SELECT Module_Id
                FROM Module
               WHERE Module_Code = 'CNTRMGT'),
             (SELECT Permission_Id
                FROM Permission
               WHERE Permission_Code = 'TCMGT'),
             SEQ_APP_MOD_PERM.NEXTVAL);
             
COMMIT;


create or replace
PROCEDURE UPDATE_TAB_PERMISSION
IS
BEGIN
   FOR SHIPPER_PERMISSION
      IN (SELECT  DISTINCT (AC.ROLE_ID)
        FROM ACCESS_CONTROL AC, COMPANY_TYPE CT, COMPANY COMP,ROLE_APP_MOD_PERM RAMP
        WHERE  AC.COMPANY_ID = COMP.COMPANY_ID
        AND COMP.COMPANY_TYPE_ID = CT.COMPANY_TYPE_ID
         AND CT.DESCRIPTION = 'Shipper'
         AND AC.ROLE_ID = RAMP.ROLE_ID
         AND RAMP.APP_ID = 4)

   LOOP
      INSERT INTO ROLE_APP_MOD_PERM(role_id,app_id,module_id,permission_id,row_uid,created_dttm,last_updated_dttm)
           VALUES (SHIPPER_PERMISSION.ROLE_ID,
                   (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'TP'),
                   (SELECT MODULE_ID
                      FROM MODULE
                     WHERE MODULE_CODE = 'CNTRMGT'),
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'TCMGT'),
                   SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
                   GETDATE(),
                   GETDATE()
                   );


   END LOOP;
   
   COMMIT;
END UPDATE_TAB_PERMISSION;
/

EXEC UPDATE_TAB_PERMISSION;

DROP PROCEDURE UPDATE_TAB_PERMISSION;


-- DBTicket TPE-37272

BEGIN
   FOR i
      IN (SELECT ucc.table_name, ucc.column_name
            FROM user_tables ut, user_col_comments ucc
           WHERE     ut.table_name = ucc.table_name
                 AND ucc.comments IS NULL
                 AND ut.temporary = 'N'
                 AND ucc.column_name IN ('CREATED_DTTM','CREATED_DATE_TIME'))
   LOOP
      EXECUTE IMMEDIATE
            'COMMENT ON COLUMN '
         || i.table_name
         || '.'
         || i.column_name
         || '  IS '
         || '''Created on Date and Time''';
   END LOOP;

   FOR i
      IN (SELECT ucc.table_name, ucc.column_name
            FROM user_tables ut, user_col_comments ucc
           WHERE     ut.table_name = ucc.table_name
                 AND ucc.comments IS NULL
                 AND ut.temporary = 'N'
                 AND ucc.column_name IN ('LAST_UPDATED_DTTM','MOD_DATE_TIME'))
   LOOP
      EXECUTE IMMEDIATE
            'COMMENT ON COLUMN '
         || i.table_name
         || '.'
         || i.column_name
         || '  IS '
         || '''Last Updated on Date and Time''';
   END LOOP;
END;
/

SHOW ERRORS;

COMMIT;



-- DBTicket TPE-38098 REFL


exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');


MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Base Data - Administer Master Codes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Base Data - Administer Master Codes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );

MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Base Data - Carrier Administer Equipments') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Base Data - Carrier Administer Equipments'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );


MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Base Data - Carrier View Equipments') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Base Data - Carrier View Equipments'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );


MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Base Data - Carrier View Reason Codes') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Base Data - Carrier View Reason Codes'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_NAME,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_SHORT_NAME = 'LG'
                   AND MODULE.MODULE_CODE = 'BDM'
                   AND PERMISSION.PERMISSION_NAME = 'Administer Filters') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_SHORT_NAME = 'LG'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'BDM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_NAME = 'Administer Filters'),
               (SEQ_APP_MOD_PERM.nextval));
			   

	
	
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Filters') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Carrier'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'LG'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'BDM'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Filters'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );
	
	
COMMIT;	

-- DBTicket TPE-37554 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');


MERGE INTO APP_MODULE AMP USING
(SELECT
  (SELECT app_id FROM app WHERE app_name ='Transportation LifeCycle Management') AS APP_ID,
  (SELECT module_id FROM module WHERE MODULE_CODE ='RSM') AS MODULE_ID
FROM DUAL
) C ON (AMP.APP_ID = C.APP_ID AND AMP.MODULE_ID = C.MODULE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      IS_ACTIVE,
      ROW_UID
    )
    VALUES
    (
      (SELECT app_id FROM app WHERE app_name ='Transportation LifeCycle Management'),
      (SELECT module_id FROM module WHERE MODULE_CODE ='RSM'),
      1,
      (SELECT MAX(ROW_UID)+4 FROM APP_MODULE)
    );
	
MERGE INTO PERMISSION P
     USING (SELECT 'View Continuous Moves . Archive' PERMISSION_NAME, 'VCMA' PERMISSION_CODE
              FROM DUAL) B
        ON (P.PERMISSION_NAME = B.PERMISSION_NAME AND P.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (P.PERMISSION_ID,
               P.PERMISSION_NAME,
               P.IS_ACTIVE,
               P.COMPANY_ID,
               P.PERMISSION_CODE)
       VALUES(SEQ_PERMISSION_ID.NEXTVAL,B.PERMISSION_NAME,1,-1,B.PERMISSION_CODE);	

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.app_name ='Transportation LifeCycle Management'
                   AND MODULE.MODULE_CODE ='RSM'
                   AND PERMISSION.permission_code ='VCMA') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE app_name ='Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE ='RSM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE permission_code ='VCMA'),
               SEQ_APP_MOD_PERM.nextval);

			   

MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCMA') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCMA'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    );

COMMIT;	

-- DBTicket TPE-38919
 
MERGE INTO LABEL L
     USING (SELECT 'UCL' BUNDLE_NAME, 'UCLPermissions_View_Continuous_Moves_Archive' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UCLPermissions_View_Continuous_Moves_Archive',
               'View Continuous Moves . Archive',
               'UCL');  

UPDATE xmenu_item_permission
SET permission_code='VCMA'
WHERE xmenu_item_id=
  (SELECT xmenu_item_id
  FROM xmenu_item
  WHERE short_name LIKE '%Continuous Moves - Archive%'
  );
  
  
COMMIT;  


-- DBTicket TPE-40187

INSERT
INTO lema_domaingroup
  (
    IDENTITY,
    DOMAIN,
    GROUPNAME,
    DESCRIPTION,
    VERSION,
    CREATEDDATE,
    CREATEDBY,
    MODIFIEDDATE,
    MODIFIEDBY,
    CREATED_DTTM,
    LAST_UPDATED_DTTM
  )
  VALUES
  (
   (select max(IDENTITY)+1 from lema_domaingroup),
    'TPE',
    'RS_RULES',
    'RS_RULES',
    1,
    sysdate,
    'BPE',
    sysdate,
    'BPE',
    sysdate,
    NULL
  );
COMMIT;

-- DBTicket TPE-40257 REFL

EXEC sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO APP_MODULE AMP USING
(SELECT
  (SELECT app_id FROM app WHERE app_name ='Logistics Gateway') AS APP_ID,
  (SELECT module_id FROM module WHERE MODULE_CODE ='YARD') AS MODULE_ID
FROM DUAL
) C ON (AMP.APP_ID = C.APP_ID AND AMP.MODULE_ID = C.MODULE_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      APP_ID,
      MODULE_ID,
      IS_ACTIVE,
      ROW_UID
    )
    VALUES
    (
      (SELECT app_id FROM app WHERE app_name ='Logistics Gateway'),
      (SELECT module_id FROM module WHERE MODULE_CODE ='YARD'),
      1,
      (SELECT MAX(ROW_UID)+4 FROM APP_MODULE)
    );
    
INSERT
INTO APP_MOD_PERM
  (
    APP_ID,
    MODULE_ID,
    PERMISSION_ID,
    ROW_UID
  )
  VALUES
  (
    (SELECT APP_ID FROM APP WHERE APP_NAME = 'Logistics Gateway'
    )
    ,
    (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'YARD'
    ),
    (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CARTRLPOOL'
    ),
    SEQ_APP_MOD_PERM.NEXTVAL
  );
  
COMMIT;  

-- DBTicket TPE-44528 REFL

update permission set description = 'Allows a user to view appointment alerts' where permission_code = 'VAPA';
update permission set description = 'Allows a user to view claim alerts' where permission_code = 'VCA';
update permission set description = 'Allows users to view alerts corresponding to the invoices' where permission_code = 'VINA';
update permission set description = 'Allows users to create or edit appointment alerts' where permission_code = 'AAPA';
update permission set description = 'Allows a user to view booking alerts' where permission_code = 'VBA';
update permission set description = 'Allows a user to view dock alerts' where permission_code = 'VDA';
update permission set description = 'Allows a user to view yard alerts' where permission_code = 'VYA';
update permission set description = 'Authorizes vendor user to create and edit appointments' where permission_code = 'VAA';
update permission set description = 'Authorizes user to view appointments' where permission_code = 'VAPT';
update permission set description = 'Authorizes user to view the appointment calendar' where permission_code = 'VCLD';
update permission set description = 'Authorizes user to create, edit and cancel appointments' where permission_code = 'AAPT';
update permission set description = 'Authorizes user to view schedules' where permission_code = 'VSCH';
update permission set description = 'Authorizes vendor user to view appointments' where permission_code = 'VVA';
update permission set description = 'Authorizes user to view control number' where permission_code = 'LMVC';
update permission set description = 'Enables the user to Create/ edit Run Templates for Consolidation Engines' where permission_code = 'CMA';
update permission set description = 'Enables the user to Run the Existing Run Templates and See the Run Summaries' where permission_code = 'COP';
update permission set description = 'Ability to add an Order to an existing shipment' where permission_code = 'VES';
update permission set description = 'Works as a Blanket View permission for the entire TLM >> Setup folder' where permission_code = 'VBD';
update permission set description = 'Provides viewing abilities for the TLM>>General>>Business Rules and the corresponding links detail pages' where permission_code = 'VBR';
update permission set description = 'Allows user to view the TLM >> Setup >> Dock Schedule >> Dock Schedule page' where permission_code = 'VDS';
update permission set description = 'Provides viewing abilities for the TLM >> Setup >> Inventory Segment' where permission_code = 'VIS';
update permission set description = 'Allow users to view path sets and static routes' where permission_code = 'VNMG';
update permission set description = 'Authorizes the user to perform Create, Update and Delete actions for Base Data objects of all types' where permission_code = 'ABD';
update permission set description = 'Authorizes users to perform Create, Update and Delete actions for contract rates (rating lane, accessorials, tariffs, ship via, transit time lanes, etc)' where permission_code = 'ACR';
update permission set description = 'Allows a user to modify the configuration for Inventory Segments' where permission_code = 'AIS';
update permission set description = 'Allow users to perform create,edit and delete path set and static routes' where permission_code = 'ANMG';
update permission set description = 'Allows a user to modify the configuration for TE Reports' where permission_code = 'ARM';
update permission set description = 'Allows a user to modify the configuration for Account Codes' where permission_code = 'ASACC';
update permission set description = 'Allows a user to add, edit, or delete Transportation Carriers' where permission_code = 'ASCRR';
update permission set description = 'Allows a user to add, edit, or delete Seasons' where permission_code = 'ASON';
update permission set description = 'Administer the others tab in Base Data' where permission_code = 'ASOT';
update permission set description = 'Allows a user to add, edit, or delete Yards' where permission_code = 'ASYRD';
update permission set description = 'Allow users to print/email documents such as (BOL, Letter of instruction etc.)' where permission_code = 'DDO';
update permission set description = 'Allows a user to view TE Reports' where permission_code = 'VRM';
update permission set description = 'Allows a user to view Account Codes' where permission_code = 'VSACC';
update permission set description = 'Allows a user to view Seasons' where permission_code = 'VSON';
update permission set description = 'Allows a user to view Others page' where permission_code = 'VSOT';
update permission set description = 'Allows a user to view Rates' where permission_code = 'VSR';
update permission set description = 'Allows a user to view Yards' where permission_code = 'VSYRD';
update permission set description = 'Allows a user to view System Codes page' where permission_code = 'VSYSCD';
update permission set description = 'Allows a user to view Archived Alerts' where permission_code = 'VARCALRT';
update permission set description = 'Allows a user to view availability of Inyard Equipment' where permission_code = 'VSIYEA';
update permission set description = 'Allows a user to add, edit, or delete availability of Inyard Equipment' where permission_code = 'ASIYEA';
update permission set description = 'Allows a user to view Size Description' where permission_code = 'VSSD';
update permission set description = 'Allows a user to add, edit, or delete Size Description' where permission_code = 'ASSD';
update permission set description = 'Allows a user to view Size Description' where permission_code = 'WMVSSD';
update permission set description = 'Allows a user to add, edit, or delete Size Description' where permission_code = 'WMASSD';
update permission set description = 'Authorizes user to create/file /adjust Cargo Claims' where permission_code = 'ACCLM';
update permission set description = 'Authorizes users to approve both Cargo and Transportation Claims' where permission_code = 'MCLM';
update permission set description = 'Authorizes user to create/file/adjust Transportation Claims' where permission_code = 'ATCLM';
update permission set description = 'Authorizes users to create, edit and delete both cargo and transportation claims' where permission_code = 'ACLM';
update permission set description = 'Authorizes user to modify Cargo Claims filed in the system. Will not allow the user to create/file new claims' where permission_code = 'ECCLM';
update permission set description = 'Authorizes user to view all Claims filed in the system. Will not allow the user to create/file new claims or modify them' where permission_code = 'VCLM';
update permission set description = 'Authorizes user to modify Transportation Claims filed in the system. Will not allow the user to create/file new claims' where permission_code = 'ETCLM';
update permission set description = 'Authorizes user to modify both Cargo and Transportation Claims filed in the system. Will not allow the user to create/file new claims' where permission_code = 'ECLM';
update permission set description = 'Allows users to create a supplier cargo claim' where permission_code = 'CSCC';
update permission set description = 'Authorizes the user to perform quick rate lookup' where permission_code = 'QRL';
update permission set description = 'Authorizes the carrier user to view: Accessorials, Fuel Rates, Bulk Rating Rules, Carrier Parameters, Dimensional Weights, Lanes, Pending Approval List, Ship Vias, Tariffs, Zone Rating, Zone Mapping, Transit Time Zones' 
where permission_code = 'VCR';
update permission set description = 'Authorizes the carrier user to: Import Rates, Add and edit Lanes, Accessorials, Tariffs, Add, edit & delete Carrier Parameters, Dimensional Weights, Ship Vias, Tariff Codes' 
where permission_code = 'ACR';
update permission set description = 'Authorizes the user to expire Rating Lanes in bulk' where permission_code = 'AMRE';
update permission set description = 'Authorizes the user to expire Rating Routes in bulk' where permission_code = 'AMRGE';
update permission set description = 'Authorizes the user to import parcel rating data' where permission_code = 'APCDIT';
update permission set description = 'Authorizes the user to add & update Routing Lanes in Contract Management' where permission_code = 'ARG';
update permission set description = 'Authorizes the user to administer rates in Contract Management' where permission_code = 'ASR';
update permission set description = 'Authorizes the user to Approve/Reject changes to Lane Rates & Lane Accessorials when the Company parameter - Rating change authorizations required - is enabled ' where permission_code = 'AURC';
update permission set description = 'Authorizes the user to import contract rates in Contract Management' where permission_code = 'ICR';
update permission set description = 'Authorizes the user to import Routing Lanes into Contract Management' where permission_code = 'IRG';
update permission set description = 'Authorizes the user to import Transit Time Lanes into Contract Management' where permission_code = 'ITL';
update permission set description = 'Authorizes the user to view Routing details in Contract Management' where permission_code = 'VRGD';
update permission set description = 'Enables viewing of retrieved rates and their details on transactional objects like shipments and D.O. ' where permission_code = 'VSR';
update permission set description = 'Authorizes the user to view Carrier Forecast' where permission_code = 'VCFP';
update permission set description = 'The permission associated with the Contract Management tab that allows the user to access the tab in the full menu' where permission_code = 'TCMGT';
update permission set description = 'Authorizes the user to view detention list and detention details' where permission_code = 'VDTN';
update permission set description = 'Authorizes the user to create and modify detentions supplier notifications' where permission_code = 'ADTN';
update permission set description = 'Authorizes the user to modify detentions' where permission_code = 'EDTN';
update permission set description = 'Authorizes the user to approve detentions' where permission_code = 'APDTN';
update permission set description = 'Allows a user to upload documents and edit or delete document revisions for objects which they have permission to access' where permission_code = 'DDO';
update permission set description = 'Displays link for Driver issues tracking system on debrief page' where permission_code = 'DITS';
update permission set description = 'View alerts raised for trips on the Dispatch-> Trip List screen' where permission_code = 'VTRA';
update permission set description = 'Provides view access to all dispatch screens under the following links: Dispatch Workspace, New Dispatch Movement, Fleet Dashboard and Bid List. This permission is applicable for dispatch screens in the shipper portal only.' 
where permission_code = 'DPTVIEW';
update permission set description = 'Allows users specific dispatch execution permissions' where permission_code = 'DPTUPD';
update permission set description = 'View Driver and Load Assignment Ratings' where permission_code = 'VAQR';
update permission set description = 'View alerts raised for Equipments' where permission_code = 'VEQA';
update permission set description = 'View alerts raised for drivers' where permission_code = 'VDRA';
update permission set description = 'Allows the user to add/edit a user created activity in the past for the driver' where permission_code = 'DAHADM';
update permission set description = 'Allows User to override HOS calculations for the Driver' where permission_code = 'AHCR';
update permission set description = 'Enables the user to edit trips in Completed or Delivered status (add/remove stops, create relay, add/remove shipments etc.)' where permission_code = 'ADCTRIPS';
update permission set description = 'Allows the user to create a new bid, add resources to an existing bid an also to view the bid list and bid details' where permission_code = 'BIDVIEW';
update permission set description = 'Allows the user to execute trips in Dispatch' where permission_code = 'DPTADM';
update permission set description = 'User can access Fleet Dashboard' where permission_code = 'DPTFLTDBRD';
update permission set description = 'Use dispatch query to determine ratings for drivers, along with driver NAT/NAL options' where permission_code = 'ANAQ';
update permission set description = 'Allows the user to create a new bid, add resources to an existing bid an also to view the bid list and bid details' where permission_code = 'BIDADMN';
update permission set description = 'Displays driver feedback on debriefing the trip' where permission_code = 'VDF';
update permission set description = 'View the OBC Message List and Details screens under Workspace->On Board Computer' where permission_code = 'OBCVIEW';
update permission set description = 'Permits the user to send and view OBC messages' where permission_code = 'OBCADM';
update permission set description = 'User can only view tandem trips' where permission_code = 'TANVIEW';
update permission set description = 'Allows you to modify tandem trip' where permission_code = 'TANADM';
update permission set description = 'Allows the user to set up account codes' where permission_code = 'ASACC';
update permission set description = 'Authorizes the user to create & update Customer Invoices and Adjustment Customer Invoices' where permission_code = 'ACIN';
update permission set description = 'Authorizes users to retrieve eligible shipments / bookings from the invoice details page' where permission_code = 'RIN';
update permission set description = 'Authorizes the user to create & update all Invoices and Adjustment Invoices' where permission_code = 'AIN';
update permission set description = 'Authorizes the user to view Invoices of any type' where permission_code = 'VIN';
update permission set description = 'Authorizes the user to view Customer Invoices' where permission_code = 'VCIN';
update permission set description = 'Authorizes the user to do mass approval of any invoice type' where permission_code = 'MAI';
update permission set description = 'Authorizes the user to do mass reject of any invoice type' where permission_code = 'MRI';
update permission set description = 'Authorizes the user to re-open rejected Adjustment Invoices' where permission_code = 'IRI';
update permission set description = 'Allows the user to view carrier issues' where permission_code = 'CIVIEW';
update permission set description = 'Allows the user to administer carrier issues' where permission_code = 'CIADMIN';
update permission set description = 'Controls the TLM >> Parcel Processing >> Label Substitutions page' where permission_code = 'LSUB';
update permission set description = 'Controls the TLM >> Parcel Processing >> Manifest Type page. Create, Update and Delete manifest types using this permission.' where permission_code = 'MMT';
update permission set description = 'Controls all the four intermediate screens pertaining to TLM >> Parcel Processing >> Weigh/Manifest oLPNs.' where permission_code = 'MNF';
update permission set description = 'Controls the TLM>>Parcel Processing>>EDI Cross Reference page' where permission_code = 'MXREF';
update permission set description = 'Controls the Close Manifest button on the TLM >> Parcel Processing >> Manifests page. Close out manifests with this permission.' where permission_code = 'CMNF';
update permission set description = 'Controls the De-Manifest oLPNs button on the TLM >> Parcel Processing >> Manifests >> Manifested oLPNs page. De-manifest or remove oLPNs from the selected manifest.' where permission_code = 'DMNF';
update permission set description = 'Controls the viewing abilities on the TLM >> Parcel Processing >> Fedex Transactions page.' where permission_code = 'FETR';
update permission set description = 'Allow users to manifest LPN though UI' where permission_code = 'MNFLPN';
update permission set description = 'Allow users to weigh and manifest though UI' where permission_code = 'WGHLPN';
update permission set description = 'Allows the user to Create shipments and manually plan them into shipments' where permission_code = 'MCO';
update permission set description = 'Allow users to set bing map license' where permission_code = 'AFVL';
update permission set description = 'Allow user to view path sets and static routes' where permission_code = 'VNMG';
update permission set description = 'Authorizes the user to create, update and delete bookings' where permission_code = 'ABK';
update permission set description = 'Authorizes the user to view all bookings' where permission_code = 'VBK';
update permission set description = 'Controls Label Printing (for LPNs) functionality' where permission_code = 'LPR';
update permission set description = 'Authorizes users to create static/retail route' where permission_code = 'AST';
update permission set description = 'Controls Close shipment button present on TLM>>Shipments>>Shipment Details page for the shipment in Accepted Status' where permission_code = 'CLSHP';
update permission set description = 'Authorizes users to view static/retail route' where permission_code = 'VST';
update permission set description = 'Authorizes the user to view all Carrier and Vendor Performance Factors' where permission_code = 'VVENDPF';
update permission set description = 'Allows the user to view performance factors' where permission_code = 'VPF';
update permission set description = 'Allows the user to change the spot charge offered by the carrier' where permission_code = 'ASPC';
update permission set description = 'Allows users to promote or demote eligible shipments' where permission_code = 'PDS';
update permission set description = 'Allows users to override the warning message - Carrier has insufficient Cargo insurance - Other users will not be able to override these warnings and can not assign a shipment to carrier with insufficient insurance.' 
where permission_code = 'ACIW';
update permission set description = 'Tender details link will be visible on the shipment details page' where permission_code = 'RSE';
update permission set description = 'View the resource selection (Tender details) page without the action buttons' where permission_code = 'VRSE';
update permission set description = 'User will be allowed to view and add new RS configurations and RS cycles' where permission_code = 'ARC';
update permission set description = 'Allows users to manually assign a resource to a shipment' where permission_code = 'MARS';
update permission set description = 'Allows the user to View Schedules' where permission_code = 'VSCH';
update permission set description = 'Allows the user to View Schedule Template Groups' where permission_code = 'VSCHTG';
update permission set description = 'Allows the user to View Event Notifications' where permission_code = 'VENOT';
update permission set description = 'Allows the user to View Schedule Template Group Rules' where permission_code = 'VSTR';
update permission set description = 'Allows the user to View Schedule Templates' where permission_code = 'VSCHTEMPL';
update permission set description = 'Allows the user to View,Add and Edit the Event Definitions' where permission_code = 'AEDEF';
update permission set description = 'Allows the user to View Schedule Groups' where permission_code = 'VSCHG';
update permission set description = 'Allows the user to View,Add,Delete,Enable and Disable Schedule Templates' where permission_code = 'ASCHTEMPL';
update permission set description = 'Allows the user to View,Add,Delete,Enable and Disable Schedule Template Group Rules' where permission_code = 'ASTR';
update permission set description = 'Allows the user to View Event Occurrences' where permission_code = 'VEVTO';
update permission set description = 'Allows the user to View Event Definitions' where permission_code = 'VEDEF';
update permission set description = 'Allows the user to View,Add,Edit and Delete Schedules' where permission_code = 'ASCH';
update permission set description = 'Allows the user to View,Add,Edit and Delete Schedule Template Groups' where permission_code = 'ASCHTG';
update permission set description = 'Allows the user to View,Add,Edit and Delete Events' where permission_code = 'AEVT';
update permission set description = 'Allows the user to Acknowledge the Event Notifications created for the Event Definitions' where permission_code = 'ACKEN';
update permission set description = 'Allows the user to View Events' where permission_code = 'VEVT';
update permission set description = 'Use this permission to Import Shipments into the system using tXMLs' where permission_code = 'MIS';
update permission set description = 'Allows users to create,edit,view and delete an RTS PO Object' where permission_code = 'RTS';
update permission set description = 'Allows a user to view appointment alerts for objects which they have permission to access' where permission_code = 'VAPA';
update permission set description = 'Allows a user to view appointments for objects which they have permission to access' where permission_code = 'VAPT';
update permission set description = 'Allows the user to view fulfillment orders' where permission_code = 'VFO';
update permission set description = 'Allows a user to configure base data' where permission_code = 'ABD';
update permission set description = 'Allows a user to administer fulfillment orders' where permission_code = 'AFO';
update permission set description = 'Allows users to view a simple view of various system data objects such as PO,DO, and shipment' where permission_code = 'AQS';
update permission set description = 'Allows a user to add, edit, or delete receive shipments' where permission_code = 'ARSH';
update permission set description = 'Allows a user to view Sales Orders' where permission_code = 'VSO';
update permission set description = 'Allows a user to view trip alerts' where permission_code = 'VTRA';
update permission set description = 'Allows a user to view detention' where permission_code = 'VDTN';
update permission set description = 'Allows a user to view dispatch driver alerts' where permission_code = 'VDRA';
update permission set description = 'Allows a user to view booking alerts' where permission_code = 'VBA';
update permission set description = 'Allows a user to view RA' where permission_code = 'VRA';
update permission set description = 'Allows a user to view CM reporting' where permission_code = 'VCM';
update permission set description = 'Allows a user to view claims' where permission_code = 'VCLM';
update permission set description = 'Allows a user to view yard alerts' where permission_code = 'VYA';
update permission set description = 'Allows a user to perform bulk updates for Purchase Orders' where permission_code = 'APBU';
update permission set description = 'The permission associated with the Transportation Lifecycle Management tab that allows the user to access the tab in the full menu. (Tab = Section)' where permission_code = 'TTLM';
update permission set description = 'User can View and create tracking messages from shipper side' where permission_code = 'ATM';
update permission set description = 'User can view already created tracking messages' where permission_code = 'VTM';
update permission set description = 'Allow vendor user to View detentions. (Vendor Portal -> Detentions)' where permission_code = 'VVD';
update permission set description = 'Controls the Cancel button on the Alerts page for Vendors. Allow vendor user to cancel alerts' where permission_code = 'VVCA';
update permission set description = 'Allow vendor users to add, edit, or cancel claims' where permission_code = 'VAC';
update permission set description = 'Allow vendor users to view alerts on the Alerts List screen.' where permission_code = 'VVVA';
update permission set description = 'Allow vendor user to View Claims. (Vendor Portal -> Claims)' where permission_code = 'VVC';
update permission set description = 'Allow vendor user to view Performance Factors' where permission_code = 'VVPF';
update permission set description = 'Allows vendor user to acknowledge alerts' where permission_code = 'VVAA';
update permission set description = 'Allow shipper user to View Graphical Yard' where permission_code = 'VGY';
update permission set description = 'Allow shipper user to create Appointment related Setup' where permission_code = 'AAST';
update permission set description = 'Allow shipper user to create,view & delete Dock Doors' where permission_code = 'ADCK';
update permission set description = 'Allow shipper user to create trailer & check the trailer activities' where permission_code = 'ATRL';
update permission set description = 'Allow shipper user to create tasks' where permission_code = 'ATSK';
update permission set description = 'Allow shipper user to create,edit and delete Yard ' where permission_code = 'AYARD';
update permission set description = 'Allow shipper user to create, view and delete Yard Slots' where permission_code = 'AYRD';
update permission set description = 'Allow shipper user to perform Yard Audit for the locations' where permission_code = 'AYRDAU';
update permission set description = 'Allow shipper user to CheckIn/CheckOut the Appointment' where permission_code = 'CHIO';
update permission set description = 'Allow shipper user to Checkin Inbound appointment' where permission_code = 'LMCI';
update permission set description = 'Allow shipper user to view the existing created trailers' where permission_code = 'VTRL';
update permission set description = 'Allow shipper user to view existing created tasks' where permission_code = 'VTSK';
update permission set description = 'Allow shipper user to View Graphical Yard' where permission_code = 'VGYARD';
update permission set description = 'Allows a user to view base data' where permission_code = 'VBD';
update permission set description = 'Allows a user to configure base data' where permission_code = 'ABD';
update permission set description = 'Allows a user to add, edit, or delete Carriers' where permission_code = 'ASCRR';
update permission set description = 'Allows a user to add, edit, or delete Seasons' where permission_code = 'ASON';
update permission set description = 'Allows a user to view Seasons' where permission_code = 'VSON';
update permission set description = 'Allows a user to view Inyard Equipment availability' where permission_code = 'VSIYEA';
update permission set description = 'Allows a user to add or edit Inyard Equipment availability' where permission_code = 'ASIYEA';
update permission set description = 'Allows a user to view Size Description' where permission_code = 'VSSD';
update permission set description = 'Allows a user to add or edit Size Description' where permission_code = 'ASSD';
update permission set description = 'Allows a user to view Size Description' where permission_code = 'WMVSSD';
update permission set description = 'Allows a user to add or edit Size Description' where permission_code = 'WMASSD';
update permission set description = 'Allows a user to view Contract Rates' where permission_code = 'VCR';
update permission set description = 'Allows a user to add or edit Contract Rates' where permission_code = 'ACR';
update permission set description = 'Allows a user to add or edit Mass Rate Expiration' where permission_code = 'AMRE';
update permission set description = 'Allows a user to add or edit Mass Rate Expiration' where permission_code = 'AMRGE';
update permission set description = 'Allows a user to add or edit Routing Guide' where permission_code = 'ARG';
update permission set description = 'Allows a user to add or edit Rates Changes' where permission_code = 'AURC';
update permission set description = 'Allows a user to import Routing Guide' where permission_code = 'IRG';
update permission set description = 'Allows a user to import Transit Time Lanes' where permission_code = 'ITL';
update permission set description = 'Allows a user to view Rating Details' where permission_code = 'VRD';
update permission set description = 'Allows a user to view Routing Details' where permission_code = 'VRGD';
update permission set description = 'Allows a user to add or edit Lane Bid' where permission_code = 'ALB';
update permission set description = 'Allows a user to add or edit RPF Response' where permission_code = 'ARR';
update permission set description = 'Allows a user to add or edit Carrier Bid' where permission_code = 'ACB';
update permission set description = 'Allows a user to view Lane' where permission_code = 'VLN';
update permission set description = 'Allows a user to add or edit Lane' where permission_code = 'ALN';
update permission set description = 'Allows a user to cancel Lane' where permission_code = 'CLN';
update permission set description = 'Allows a user to add or edit Carrier Group' where permission_code = 'ACG';
update permission set description = 'Allows a user to upload or download Lanes' where permission_code = 'UDL';
update permission set description = 'Allows a user to add or edit Lane Award' where permission_code = 'ALA';
update permission set description = 'Allows a user to view Carrier Group' where permission_code = 'VCG';
update permission set description = 'Allows a user to post RPF Response' where permission_code = 'PRR';
update permission set description = 'Allows a user to add or edit RPF Response' where permission_code = 'ARR';
update permission set description = 'Allows a user to add or edit RPF' where permission_code = 'ARF';
update permission set description = 'Allows a user to view RPF' where permission_code = 'VRF';
update permission set description = 'Allows a user to close RPF' where permission_code = 'CLR';
update permission set description = 'Allows a user to post RPF' where permission_code = 'PRF';
update permission set description = 'Allows a user to post RPF Award' where permission_code = 'PRA';
update permission set description = 'Allows a user to cancel RPF' where permission_code = 'CRF';
update permission set description = 'Allows a user to add or edit RPF Network Changes' where permission_code = 'ANC';
update permission set description = 'Allows a user to add, edit, or delete TE Reports' where permission_code = 'ARM';
update permission set description = 'Allows a user to view TE Reports' where permission_code = 'VRM';
update permission set description = 'Allows a user to add, edit, or delete CM Reporting' where permission_code = 'ACM';
update permission set description = 'Allows a user to view CM Reporting' where permission_code = 'VCM';

COMMIT;

-- DBTicket DB-308

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VES') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DAHADM') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Driver') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'AAPA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Appointment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'AAPA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Alert') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VAPA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Appointment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VAPA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCLD') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CLSHP') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VRL') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VRD') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VRGD') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CRP') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Report') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'WVLPN') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'LPN') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'WALPN') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'LPN') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MNFLPN') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'LPN') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'FETR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'WGHLPN') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'LPN') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MIS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWASUOTNS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'DO') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWASUOTNS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWASULPNTNS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'LPN') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWASULPNTNS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWAUOTES') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'DO') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWAUOTES') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWAULPNTES') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'LPN') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWAULPNTES') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWCMUOTNS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'DO') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWCMUOTNS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWCS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWCMULPNTNS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'LPN') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWCMULPNTNS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWMO') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'DO') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWSS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MPWUO') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'DO') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'PDS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'RTS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'PurchaseOrder') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'RTS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'ReadytoShip') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'COP') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Shipment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'WMVSSD') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVAIRWAYBILL') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVBOLREPORT') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVCCINVOICE') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVCERTOFORG') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVCOMRCLINVC') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVDHLMANIFST') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVDOCKRCPT') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVEXPPACKLST') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVCTDPSTRNBR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVDCNBRSTNBR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVODNBRSTNBR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVPONBRSTNBR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVSHPCTSTNBR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVSTSTRNBR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVSTNBRPONBR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVCUSTPO') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVSTNBRODNBR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVNAFTACERT') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVOCEANBOL') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVSHPMNTINQ') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVSHPEXPDECL') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVSHPLTRINST') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVUPSCNTLLOG') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVPRCLMANFST') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVFSTCLPSTGF') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVFSTCLPSTGL') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVFSTCLPSTGP') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVITMZDMANFT') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVPRCLPSTG') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TEVPRMAILPSTG') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VST') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVAA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Alert') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VAA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Appointment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVCA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Alert') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVVA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Alert') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVVA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Appointment') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVC') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVD') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVPF') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VARCALRT') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VARCALRT') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Alert') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VARCALRT') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Archive') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VARCH') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VARCH') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Archive') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VAQR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'BIDVIEW') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VBA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VBK') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCFP') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CIVIEW') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCLM') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCM') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCMA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'LMVC') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCIN') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VDTN') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DPTVIEW') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VDRA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VDRA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Alert') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VDA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VDF') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DITS') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VEQA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VEDEF') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VEVTO') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VEVT') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DPTFLTDBRD') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VFO') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VGYARD') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIN') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VNMG') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'OBCVIEW') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSOT') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VPF') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VRA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VRSE') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSCH') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSCHG') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSTR') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSCHTG') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSCHTEMPL') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSON') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'TANVIEW') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VTM') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VTRA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VTRA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'Alert') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VVENDPF') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VYA') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VTSK') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VGY') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VTK') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VTRL') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VLN') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VCG') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

MERGE INTO PERMISSION_TAG_MAPPING L USING (SELECT(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VRF') PERMISSION_ID,(SELECT TAG_ID FROM PERMISSION_TAG WHERE DESCRIPTION = 'View') TAG_ID FROM DUAL) B ON(L.PERMISSION_ID=B.PERMISSION_ID AND L.TAG_ID = B.TAG_ID) WHEN NOT MATCHED THEN INSERT (L.PERMISSION_ID,L.TAG_ID) VALUES(B.PERMISSION_ID,B.TAG_ID);

COMMIT;

-- DBTicket DB-1814

EXEC SEQUPDT('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

INSERT INTO APP_MOD_PERM
            (APP_ID,
             MODULE_ID,
             PERMISSION_ID,
             ROW_UID)
     VALUES ((SELECT APP_ID
                FROM APP
               WHERE APP_NAME = 'Transportation Procurement'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'SCPP'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'ADMIN_THEME'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO RELTP_APP_MOD_PERM
            (RELATIONSHIP_TYPE_ID, APP_ID,
             MODULE_ID,
             PERMISSION_ID,
             ROW_UID, IS_PERM_IMPLIED)
     VALUES (4, (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation Procurement'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'SCPP'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'ADMIN_THEME'),
             (SELECT MAX (ROW_UID)
                FROM RELTP_APP_MOD_PERM) + 4, 0);
COMMIT;

-- DBTicket DB-2153 REFL

MERGE into PERMISSION X
	using (select
		'MASSINVPAY' PERMISSION_CODE,
		1 IS_ACTIVE,
		'Mass invoice payment' PERMISSION_NAME,
		'Allows user to pay multiple invoices from invoice list page' DESCRIPTION
		from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'FAP'
              AND PERMISSION.PERMISSION_CODE = 'MASSINVPAY') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);



MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MASSINVPAY') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MASSINVPAY'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	  
COMMIT;

-- DBTicket DB-4597 REFL

EXEC SEQUPDT ('ROLE','ROLE_ID','SEQ_ROLE_ID');

MERGE INTO ROLE R USING
  (SELECT 'YMMGRROLE' AS ROLE_NAME FROM DUAL
  )
  B ON (R.ROLE_NAME = B.ROLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      ROLE_ID,
      ROLE_NAME,
      COMPANY_ID,
      IS_ACTIVE,
      CREATED_SOURCE,
      CREATED_SOURCE_TYPE_ID,
      LAST_UPDATED_SOURCE,
      LAST_UPDATED_SOURCE_TYPE_ID,
      ROLE_TYPE_ID,
      DESCRIPTION,
      IS_DEFAULT
    )
    VALUES
    (
      SEQ_ROLE_ID.nextVal,
      'YMMGRROLE',
      1,1,
      'seed',
      1,
      'seed',
      1,4,
      null,
      1
    );

Commit;


-- DBTicket DB-2355 REFL

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMATGELG') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMATGELG'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMVTGELG') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVTGELG'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Transportation LifeCycle Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMATGELG')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT role_id
                   FROM role
                  WHERE role_name = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMATGELG'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Transportation LifeCycle Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMVTGELG')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT ROLE_ID
                   FROM role
                  WHERE ROLE_NAME = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVTGELG'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMATSPHDEF') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMATSPHDEF'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMVTSPHDEF') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVTSPHDEF'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Transportation LifeCycle Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMATSPHDEF')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT role_id
                   FROM role
                  WHERE role_name = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMATSPHDEF'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Transportation LifeCycle Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMVTSPHDEF')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT role_id
                   FROM role
                  WHERE role_name = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMVTSPHDEF'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMAYUP') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT permission_id
                  FROM permission
                 WHERE permission_code = 'YMAYUP'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
                   AND MODULE.MODULE_CODE = 'YARD'
                   AND PERMISSION.PERMISSION_CODE = 'YMVYUP') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVYUP'),
               (SEQ_APP_MOD_PERM.NEXTVAL),
               SYSDATE,
               NULL);

MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Transportation LifeCycle Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMAYUP')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT ROLE_ID
                   FROM role
                  WHERE ROLE_NAME = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMAYUP'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);


MERGE INTO role_app_mod_perm ramp
     USING (SELECT amp.app_id app_id,
                   amp.module_id module_id,
                   p.permission_id permission_id
              FROM app_mod_perm amp, PERMISSION p
             WHERE     app_id = (SELECT app_id
                                   FROM app
                                  WHERE app_name = 'Transportation LifeCycle Management')
                   AND p.permission_id = amp.permission_id
                   AND module_id = (SELECT module_id
                                      FROM module
                                     WHERE module_code = 'YARD')
                   AND p.permission_id = (SELECT PERMISSION_ID
                                            FROM PERMISSION
                                           WHERE PERMISSION_CODE = 'YMVYUP')) APP1
        ON (    ramp.app_id = app1.app_id
            AND ramp.module_id = app1.module_id
            AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED
THEN
   INSERT     (ramp.role_id,
               ramp.app_id,
               ramp.module_id,
               ramp.permission_id,
               ramp.row_uid,
               ramp.created_dttm,
               ramp.last_updated_dttm)
       VALUES ( (SELECT ROLE_ID
                   FROM role
                  WHERE ROLE_NAME = 'YMMGRROLE'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_NAME = 'Transportation LifeCycle Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'YARD'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'YMVYUP'),
               SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
               SYSDATE,
               NULL);
COMMIT;

-- DBTicket DB-2356 REFL

INSERT INTO RELTP_APP_MOD_PERM(RELATIONSHIP_TYPE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,IS_PERM_IMPLIED) 
VALUES(4,(SELECT APP_ID FROM APP WHERE APP_NAME='Transportation LifeCycle Management'),(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE='YARD'),(select PERMISSION_ID from PERMISSION where PERMISSION_CODE= 'YMATGELG'),(select max(ROW_UID)+4 from RELTP_APP_MOD_PERM),0) ;

INSERT INTO RELTP_APP_MOD_PERM(RELATIONSHIP_TYPE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,IS_PERM_IMPLIED) 
VALUES(4,(SELECT APP_ID FROM APP WHERE APP_NAME='Transportation LifeCycle Management'),(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE='YARD'),(select PERMISSION_ID from PERMISSION where PERMISSION_CODE= 'YMVTGELG'),(select max(ROW_UID)+4 from RELTP_APP_MOD_PERM),0) ;

INSERT INTO RELTP_APP_MOD_PERM(RELATIONSHIP_TYPE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,IS_PERM_IMPLIED) 
VALUES(4,(SELECT APP_ID FROM APP WHERE APP_NAME='Transportation LifeCycle Management'),(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE='YARD'),(select PERMISSION_ID from PERMISSION where PERMISSION_CODE= 'YMATSPHDEF'),(select max(ROW_UID)+4 from RELTP_APP_MOD_PERM),0) ;

INSERT INTO RELTP_APP_MOD_PERM(RELATIONSHIP_TYPE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,IS_PERM_IMPLIED) 
VALUES(4,(SELECT APP_ID FROM APP WHERE APP_NAME='Transportation LifeCycle Management'),(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE='YARD'),(select PERMISSION_ID from PERMISSION where PERMISSION_CODE= 'YMVTSPHDEF'),(select max(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM(RELATIONSHIP_TYPE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,IS_PERM_IMPLIED) 
VALUES(4,(SELECT APP_ID FROM APP WHERE APP_NAME='Transportation LifeCycle Management'),(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE='YARD'),(select PERMISSION_ID from PERMISSION where PERMISSION_CODE= 'YMAYUP'),(select max(ROW_UID)+4 from RELTP_APP_MOD_PERM),0); 

INSERT INTO RELTP_APP_MOD_PERM(RELATIONSHIP_TYPE_ID,APP_ID,MODULE_ID,PERMISSION_ID,ROW_UID,IS_PERM_IMPLIED) 
VALUES(4,(SELECT APP_ID FROM APP WHERE APP_NAME='Transportation LifeCycle Management'),(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE='YARD'),(select PERMISSION_ID from PERMISSION where PERMISSION_CODE= 'YMVYUP'),(select max(ROW_UID)+4 from RELTP_APP_MOD_PERM),0) ;

COMMIT;

-- DBTicket DB-3539 REFL

EXEC SEQUPDT('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO APP_MOD_PERM AMP
   USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                 MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                 PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
           WHERE APP.APP_ID = (select app_id from app where APP_NAME = 'Transportation LifeCycle Management')
             AND MODULE.MODULE_CODE = 'YARD'
             AND PERMISSION.PERMISSION_CODE = 'YMAR') APP1
   ON (    AMP.APP_ID = APP1.APP_ID
       AND AMP.MODULE_ID = APP1.MODULE_ID
       AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
   WHEN NOT MATCHED THEN
      INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
      VALUES (APP1.APP_ID, (SELECT MODULE_ID
                       FROM MODULE
                      WHERE MODULE_CODE = 'YARD'),
              (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'YMAR'), SEQ_APP_MOD_PERM.nextval);

MERGE INTO APP_MOD_PERM AMP
USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
PERMISSION.PERMISSION_CODE
FROM APP, MODULE, PERMISSION
WHERE APP.APP_ID = (select app_id from app where APP_NAME = 'Transportation LifeCycle Management')
AND MODULE.MODULE_CODE = 'YARD'
AND PERMISSION.PERMISSION_CODE = 'YMVR') APP1
ON ( AMP.APP_ID = APP1.APP_ID
AND AMP.MODULE_ID = APP1.MODULE_ID
AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
VALUES (APP1.APP_ID, (SELECT MODULE_ID
FROM MODULE
WHERE MODULE_CODE = 'YARD'),
(SELECT PERMISSION_ID
FROM PERMISSION
WHERE PERMISSION_CODE = 'YMVR'), SEQ_APP_MOD_PERM.nextval);

COMMIT;


-- DBTicket DB-3574 REFL

MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITCOM' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDCOM' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVCOM' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSACCCNTRL' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITBU' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDBU' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVBU' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITCH' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDCH' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVCH' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADMCPP' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITLOC' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDLOC' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVLOC' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITPER' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDPER' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVPER' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITREG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDREG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVREG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITRLT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDRLT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVRLT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITRLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSDELETERLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDRLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVRLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITUSG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDUSG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVUSG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSEDITUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSDELETEUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADDUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSRESETUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='MYPROFILE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADMSUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVSUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSSECPOLICY' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SASECPOLICY' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSAPPS' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADMDASOURCES' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADMSOLT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADMSARLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSVSARLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SYSADMINCCG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VPSTLCODE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMPSTLCODE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='LOGMGMT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='LOGS' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMLBLMGMT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMLTLMGMT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMMSGS' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMSCRNTYPE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMINBASEMENU' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMINMENU' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMINRES' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMINSCREEN' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='EDEVT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='DLP' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='DLU' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='AMDS' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='AMIF' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VMIF' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMIN_FILTER' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='CRT_PUB_FILTER' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='CRT_PVT_FILTER' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMIN_THEME' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMLM' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMMM' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='EDITCOM' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VCOM' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ACCCNTRL' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='EDITBU' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADDBU' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VBU' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='OUM_ECH' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='OUM_ACH' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='OUM_VCH' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='EDITREG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADDREG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VREG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='EDITLOC' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADDLOC' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VLOC' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='EDITUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADDUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='DELETEUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='RESETUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VUSR' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='EDITUSG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADDUSG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VUSG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='EDITRLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADDRLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='DELETERLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VRLE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADDPER' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VPER' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMCPP' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='EDITRLT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADDRLT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VRLT' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='LOGS' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMMSGS' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='SECPOLICY' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='VPSTLCODE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMPSTLCODE' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMINRES' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));
MERGE INTO COMPANY_TYPE_PERM ctp USING (SELECT   (SELECT COMPANY_TYPE_ID FROM company_type WHERE lower(DESCRIPTION) = lower('Carrier')) company_type_id,   (SELECT permission_id FROM permission WHERE permission_code='ADMINUCG' ) permission_id FROM DUAL ) ctp1 ON (ctp.company_type_id = ctp1.company_type_id AND ctp.permission_id = ctp1.permission_id) WHEN NOT MATCHED THEN INSERT (ctp.company_type_id,ctp.permission_id,ctp.row_uid) VALUES (ctp1.company_type_id,ctp1.permission_id,(SELECT MAX (row_uid) + 4 FROM company_type_perm));

COMMIT;

-- DBTicket DB-3557 REFL

exec  sequpdt('DSHBRD_PORTLET','DSHBRD_PORTLET_ID','SEQ_DSHBRD_PORTLET_ID');
exec  sequpdt('DSHBRD_PORTLET_TAGS','DSHBRD_PORTLET_TAGS_ID','SEQ_DSHBRD_PORTLET_TAGS_ID');

EXEC SEQUPDT('UIB_QUERY','UIB_QUERY_ID','SEQ_UIB_QUERY_ID');
exec sequpdt('UIB_QUERY_DETAILS','UIB_QUERY_DETAILS_ID','SEQ_UIB_QUERY_DETAILS_ID');
exec sequpdt('DSHBRD_UIB_PTLT_DTL','DSHBRD_UIB_PTLT_DTL_ID','SEQ_DSHBRD_UIB_PTLT_DTL_ID');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'Invoices by Status',null,null,null,null,'Group invoices by status',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'Shipments by Status',null,null,null,null,'Group shipments by status',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'RFP Carrier Response Summary',null,null,null,null,'Carrier Response Summary',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'Trips By Status',null,null,null,null,'Group trips by status',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'RFP Carrier Award Volume',null,null,null,null,'Carrier award volume',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'Distribution Orders by Status',null,null,null,null,'Group orders by status',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'ASNs by Status',null,null,null,null,'Group ASNs by status',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'Invoices Pending Payment by Carrier',null,null,null,null,'Invoices pending payment by carrier',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'RFP Carrier Award Spend',null,null,null,null,'Carrier award spend',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');

INSERT into DSHBRD_PORTLET (DSHBRD_PORTLET_ID,PORTLET_NAME,APP_INSTANCE_ID,URL,PORTLET_HEIGHT_TYPE_ID,PORTLET_WIDTH_TYPE_ID,DESCRIPTION,IMAGE_PATH,IS_RSS_FEED,URL_TYPE,CONFIG_URL,DSHBRD_PORTLET_TYPE_ID,SUPPORT_MOBILE,CREATED_DTTM,LAST_UPDATED_DTTM,
DELETED) values (SEQ_DSHBRD_PORTLET_ID.NEXTVAL,'RFP Invited Carriers',null,null,null,null,'Monitor Invited Carrier Responses',null,null,'NONE','default url','3','0',sysdate,sysdate,'0');



---- INSERTING into DSHBRD_PORTLET_TAGS 
INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='Shipments by Status'),'Shipment');

INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='Invoices by Status'),'Invoice');

INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='RFP Invited Carriers'),'Bid');

INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='Invoices Pending Payment by Carrier'),'Pending Payment');

INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='Distribution Orders by Status'),'Distribution Order');

INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='Trips By Status'),'Dispatch');

INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='RFP Carrier Response Summary'),'RFP');

INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='ASNs by Status'),'ASN');

INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='RFP Carrier Award Spend'),'Award');

INSERT into DSHBRD_PORTLET_TAGS (DSHBRD_PORTLET_TAGS_ID,DSHBRD_PORTLET_ID,TAGS)
values (SEQ_DSHBRD_PORTLET_TAGS_ID.nextval,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME ='RFP Carrier Award Volume'),'Award');


---- INSERTING into UIB_QUERY 
INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'Trips By Status','1','Group trip by status','0','1',sysdate,'1',sysdate,'1');

INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'RFP Invited Carriers','1','Invited carriers','0','1',sysdate,'1',sysdate,'1');

INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'RFP Carrier Response Summary','1','Carrier response summary','0','1',sysdate,'1',sysdate,'1');

INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'RFP Carrier Award Volume','1','Carrier award volume','0','1',sysdate,'1',sysdate,'1');

INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'Shipments by Status','1','Group shipments by status','0','1',sysdate,'1',sysdate,'1');

INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'RFP Carrier Award Spend','1','Carrier award spend','0','1',sysdate,'1',sysdate,'1');

INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'Invoices Pending Payment by Carrier','1','Pending payment by carrier','0','1',sysdate,'1',sysdate,'1');

INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'Distribution Orders Status','1','Group orders by status','0','1',sysdate,'1',sysdate,'1');

INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'ASNs by Status','1','Group ASNs by status','0','1',sysdate,'1',sysdate,'1');

INSERT into UIB_QUERY (UIB_QUERY_ID,QUERY_NAME,QUERY_TYPE_ID,QUERY_DESCRIPTION,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,
LAST_UPDATED_DTTM,CACHE_TIMEOUT_ID) values (SEQ_UIB_QUERY_ID.NEXTVAL,'Invoices By Status','1','Group invoices by status','0','1',sysdate,'1',sysdate,'1');



---- INSERTING into UIB_QUERY_DETAILS 
INSERT into UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,UIB_QUERY_ID,UIB_SQL_FLAVOR_ID,QUERY_COLUMNS,QUERY_TABLES,QUERY_WHERE_CLAUSE,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values (SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Distribution Orders Status'),null,'ORDER_STATUS.DESCRIPTION as Status, COUNT(*) AS Total_Orders','ORDERS , ORDER_STATUS','ORDERS.ORDER_STATUS = ORDER_STATUS.ORDER_STATUS   group by ORDER_STATUS.DESCRIPTION','0','1','seed',sysdate,'1','seed',sysdate);

INSERT into UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,UIB_QUERY_ID,UIB_SQL_FLAVOR_ID,QUERY_COLUMNS,QUERY_TABLES,QUERY_WHERE_CLAUSE,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values (SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Invoices Pending Payment by Carrier'),null,
'carrier_code.CARRIER_CODE ""Carrier"", nvl(SUM(ACTUAL_PAYMENT_AMOUNT), 0 ) as ""Approved Pending Payment""','invoice, carrier_code','invoice.CARRIER_ID = carrier_code.carrier_id
and invoice_status in (20, 30) group by carrier_code.CARRIER_CODE order by  carrier_code.CARRIER_CODE
','1','1','seed',sysdate,'1','seed',sysdate);

INSERT into UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,UIB_QUERY_ID,UIB_SQL_FLAVOR_ID,QUERY_COLUMNS,QUERY_TABLES,QUERY_WHERE_CLAUSE,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values (SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Shipments by Status'),null,
'ss.description as STATUS, count (*) as TOTAL','shipment s, shipment_status ss','s.shipment_status=ss.shipment_status group by ss.description',
'0','1','seed',sysdate,'1','seed',sysdate);

INSERT into UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,UIB_QUERY_ID,UIB_SQL_FLAVOR_ID,QUERY_COLUMNS,QUERY_TABLES,QUERY_WHERE_CLAUSE,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values (SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'RFP Carrier Award Volume'),null,'CO.COMPANY_NAME,
 SUM((NVL(SA.MANUALAWARDAMOUNT,0)+NVL(SA.AUTOAWARDAMOUNT,0)+NVL(PACKAGEAWARDAMOUNT,0))) AS AWARD_AMOUNT','SCENARIOADJUSTMENT SA,
    OBCARRIERCODE OB,
    COMPANY CO,
    CAPACITYBIDRFP CBR,
    SCENARIO SCEN ','SA.SCENARIOID in (0) AND SCEN.SCENARIOID = SA.SCENARIOID AND SA.RFPID = SCEN.RFPID
    AND OB.OBCARRIERCODEID = SA.OBCARRIERCODEID AND OB.TPCOMPANYID = CO.COMPANY_ID
    AND SA.RFPID = CBR.RFPID AND OB.OBCARRIERCODEID = CBR.OBCARRIERCODEID
GROUP BY CO.COMPANY_NAME','1','1','seed',sysdate,'1','seed',sysdate);

INSERT into UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,UIB_QUERY_ID,UIB_SQL_FLAVOR_ID,QUERY_COLUMNS,QUERY_TABLES,QUERY_WHERE_CLAUSE,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values (SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Trips By Status'),null,'B.description, count(*) ','trip A, trip_status B','A.trip_status=B.trip_status
group by B.description','0','1','seed',sysdate,'1','seed',sysdate);

INSERT
INTO UIB_QUERY_DETAILS
  (
    UIB_QUERY_DETAILS_ID,
    UIB_QUERY_ID,
    UIB_SQL_FLAVOR_ID,
    QUERY_COLUMNS,
    QUERY_TABLES,
    QUERY_WHERE_CLAUSE,
    HIBERNATE_VERSION,
    CREATED_SOURCE_TYPE,
    CREATED_SOURCE,
    CREATED_DTTM,
    LAST_UPDATED_SOURCE_TYPE,
    LAST_UPDATED_SOURCE,
    LAST_UPDATED_DTTM
  )
  VALUES
  (
    SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
    (SELECT UIB_QUERY_ID
    FROM UIB_QUERY
    WHERE QUERY_NAME = 'RFP Carrier Response Summary'
    ),
    NULL,
    'CASE WHEN (TIC.RFPRESPONSESTATUS) = 1 THEN ''Yes'' ELSE ''No'' END AS RESPONSE||'','' || COUNT(TIC.RFPRESPONSESTATUS) AS COUNT_POSTED_or_BID_ON_LANES',
    'TEMP_INVITED_CARRIERS TIC, OBCARRIERCODE OB',
    'TIC.OBCARRIERCODEID = OB.OBCARRIERCODEID GROUP BY CASE WHEN (TIC.RFPRESPONSESTATUS) = 1 THEN ''Yes'' ELSE ''No'' END',
    '0',
    '1',
    'seed',
    sysdate,
    '1',
    'seed',
    sysdate
  );

INSERT into UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,UIB_QUERY_ID,UIB_SQL_FLAVOR_ID,QUERY_COLUMNS,QUERY_TABLES,QUERY_WHERE_CLAUSE,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values (SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Invoices By Status'),null,'INVOICE_STATUS.DESCRIPTION as ""Invoice Status"", count(*) as ""Number of Invoices""','invoice, invoice_status','invoice.INVOICE_STATUS = INVOICE_STATUS.INVOICE_STATUS group by INVOICE_STATUS.DESCRIPTION','0','1','seed',sysdate,'1','seed',sysdate);

INSERT into UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,UIB_QUERY_ID,UIB_SQL_FLAVOR_ID,QUERY_COLUMNS,QUERY_TABLES,QUERY_WHERE_CLAUSE,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values (SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'ASNs by Status'),null,'ASN_STATUS.DESCRIPTION AS STATUS,COUNT(ASN.ASN_ID) AS COUNT','ASN,ASN_STATUS','ASN_STATUS.ASN_STATUS = ASN.ASN_STATUS
GROUP BY ASN_STATUS.DESCRIPTION
ORDER BY ASN_STATUS.DESCRIPTION','0','1','seed',sysdate,'1','seed',sysdate);

INSERT into UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,UIB_QUERY_ID,UIB_SQL_FLAVOR_ID,QUERY_COLUMNS,QUERY_TABLES,QUERY_WHERE_CLAUSE,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values (SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'RFP Carrier Award Spend'),null,'SCEN.NAME,CO.COMPANY_NAME,ROUND((SUM(((NVL(SA.MANUALAWARDAMOUNT,0)+NVL(SA.AUTOAWARDAMOUNT,0)+NVL(SA.PACKAGEAWARDAMOUNT,0)))*SA.COSTPERLOAD)/7*365),2) AS ANNUAL_SPEND','SCENARIOADJUSTMENT SA,
    OBCARRIERCODE OB,
    COMPANY CO,
    CAPACITYBIDRFP CBR,
    SCENARIO SCEN','SCEN.SCENARIOID = SA.SCENARIOID AND SA.RFPID = SCEN.RFPID
    AND OB.OBCARRIERCODEID = SA.OBCARRIERCODEID AND OB.TPCOMPANYID = CO.COMPANY_ID
    AND SA.RFPID = CBR.RFPID AND OB.OBCARRIERCODEID = CBR.OBCARRIERCODEID
GROUP BY CO.COMPANY_NAME,SCEN.NAME
ORDER BY SCEN.NAME,CO.COMPANY_NAME','0','1','seed',sysdate,'1','seed',sysdate);

INSERT INTO UIB_QUERY_DETAILS (UIB_QUERY_DETAILS_ID,
                               UIB_QUERY_ID,
                               UIB_SQL_FLAVOR_ID,
                               QUERY_COLUMNS,
                               QUERY_TABLES,
                               QUERY_WHERE_CLAUSE,
                               HIBERNATE_VERSION,
                               CREATED_SOURCE_TYPE,
                               CREATED_SOURCE,
                               CREATED_DTTM,
                               LAST_UPDATED_SOURCE_TYPE,
                               LAST_UPDATED_SOURCE,
                               LAST_UPDATED_DTTM)
     VALUES (
               SEQ_UIB_QUERY_DETAILS_ID.NEXTVAL,
               (SELECT UIB_QUERY_ID
                  FROM UIB_QUERY
                 WHERE QUERY_NAME = 'RFP Invited Carriers'),
               NULL,
               ' TIC.COMPANY_NAME || '','' || CASE WHEN (TIC.RFPRESPONSESTATUS) = 1 THEN ''Yes'' ELSE ''No'' END AS POSTED||'','' || CASE WHEN (TIC.ISSPECREQSAVE) = 1 THEN ''Yes'' ELSE ''No'' END AS SPEQREQ_SAVED||'',''
             ||CASE WHEN (TIC.ISPKGINFOSAVE) = 1 THEN ''Yes'' ELSE ''No'' END AS PKGINFO_SAVED||'','' || CASE WHEN (TIC.ISFACINFOSAVE) = 1 THEN ''Yes'' ELSE ''No'' END AS FACINFO_SAVED||'',''
             ||MAX(TIC.CURRENT_ROUND_LANE_SAVED) AS LANES_BIDON||'',''||MAX(TIC.CURRENT_ROUND_LANE_DECLINED) AS LANES_DECLINED||'',''
              ||ROUND(AVG(TIC.SUM_UNREAD_STATUS),0) AS UNREAD_MSGS||'',''||ROUND(AVG(TIC.SUM_COUNT_DOWNLOADED),0) AS DOWNLOADED_MSGS',
               'TEMP_INVITED_CARRIERS TIC||'',''|| OBCARRIERCODE OB',
               'TIC.OBCARRIERCODEID = OB.OBCARRIERCODEID AND TIC.ROUND_NUM = 1
             ||GROUP BY TIC.COMPANY_NAME, CASE WHEN (TIC.RFPRESPONSESTATUS) = 1 THEN ''Yes'' ELSE ''No'' END, CASE WHEN (TIC.ISSPECREQSAVE) = 1 THEN ''Yes'' ELSE ''No'' END, CASE WHEN (TIC.ISPKGINFOSAVE) = 1 THEN ''Yes'' ELSE ''No'' END, 
             CASE WHEN (TIC.ISFACINFOSAVE) = 1 THEN ''Yes'' ELSE ''No'' END, CASE WHEN (TIC.RFPRESPONSESTATUS) = 1 THEN ''Yes'' ELSE ''No'' END ORDER BY TIC.COMPANY_NAME',
               '0',
               '1',
               'seed',
               SYSDATE,
               '1',
               'seed',
               SYSDATE);


---- INSERTING into DSHBRD_UIB_PTLT_DTL  
INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
values (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Invited Carriers'),(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'RFP Invited Carriers'),'5',null,0,1,'seed',sysdate, '1','seed',sysdate,'RFP Invited Carriers Responses',(select data_source_id from DS_DATA_SOURCE where app_inst_short_name = 'tlm'));

INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
values (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Response Summary'),(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'RFP Carrier Response Summary'),'1',null,0,1,'seed',sysdate, '1','seed',sysdate,'RFP Carrier Response Summary',(select data_source_id from DS_DATA_SOURCE where app_inst_short_name = 'tlm'));

INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
values (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'Shipments by Status'),(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Shipments by Status'),'3',null,0,1,'seed',sysdate, '1','seed',sysdate,'Shipments by Status',(select data_source_id from DS_DATA_SOURCE where app_inst_short_name = 'tlm'));

INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
values (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Spend'),(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'RFP Carrier Award Spend'),'6',null,0,1,'seed',sysdate, '1','seed',sysdate,'Awarded Spend by Carrier by Scenario',(select data_source_id from DS_DATA_SOURCE where app_inst_short_name = 'tlm'));

INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
values (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'Distribution Orders by Status'),(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Distribution Orders Status'),'3',null,0,1,'seed',sysdate, '1','seed',sysdate,'Distribution Orders by Status',(select data_source_id from DS_DATA_SOURCE where app_inst_short_name = 'tlm'));

INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
values (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'Invoices Pending Payment by Carrier'),(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Invoices Pending Payment by Carrier'),'5',null,0,1,'seed',sysdate, '1','seed',sysdate,'Invoices Pending Payment by Carrier',(select data_source_id from DS_DATA_SOURCE where app_inst_short_name = 'tlm'));

INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
values (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Volume'),(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'RFP Carrier Award Volume'),'1',null,0,1,'seed',sysdate, '1','seed',sysdate,'Awarded Volume by Carrier by Scenario',(select data_source_id from DS_DATA_SOURCE where app_inst_short_name = 'tlm'));

INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
values (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'Invoices by Status'),(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Invoices By Status'),'5',null,0,1,'seed',sysdate, '1','seed',sysdate,'Invoices by Status',(select data_source_id from DS_DATA_SOURCE where app_inst_short_name = 'tlm'));

INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
values (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'ASNs by Status'),(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'ASNs by Status'),'1',null,0,1,'seed',sysdate, '1','seed',sysdate,'ASNs by Status',(select data_source_id from DS_DATA_SOURCE where app_inst_short_name = 'tlm'));

INSERT into DSHBRD_UIB_PTLT_DTL (DSHBRD_UIB_PTLT_DTL_ID,DSHBRD_PORTLET_ID,UIB_QUERY_ID,UIB_VIEW_TYPE_ID,UIB_VIEW_PROPERTY_SET_ID,HIBERNATE_VERSION,CREATED_SOURCE_TYPE,
CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM,TITLE,DATA_SOURCE_ID) 
VALUES (SEQ_DSHBRD_UIB_PTLT_DTL_ID.NEXTVAL,(SELECT DSHBRD_PORTLET_ID FROM  DSHBRD_PORTLET WHERE PORTLET_NAME = 'Trips By Status'),(SELECT UIB_QUERY_ID FROM UIB_QUERY WHERE QUERY_NAME = 'Trips By Status'),'1',NULL,0,1,'seed',SYSDATE, '1','seed',SYSDATE,'Trips By Status',(SELECT DATA_SOURCE_ID FROM DS_DATA_SOURCE WHERE APP_INST_SHORT_NAME = 'tlm'));

COMMIT;

-- DBTicket DB-3708 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');

MERGE INTO PERMISSION X
     USING (SELECT 'TLMPORTLETS' PERMISSION_CODE,
                   1 IS_ACTIVE,
                   'TLM Seed Portlets' PERMISSION_NAME,
                   'Allows a user to view the TLM seed portlets.' DESCRIPTION
              FROM DUAL) B
        ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (PERMISSION_ID,
               PERMISSION_CODE,
               IS_ACTIVE,
               PERMISSION_NAME,
               DESCRIPTION)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               B.PERMISSION_CODE,
               B.IS_ACTIVE,
               B.PERMISSION_NAME,
               B.DESCRIPTION);

MERGE INTO APP_MODULE M    
    using (select (select APP_ID from APP where APP_SHORT_NAME = 'TPE') APP_ID, 
                  (SELECT module_id  FROM module WHERE module_code = 'DSHBD') MODULE_ID FROM DUAL) D
    ON (M.MODULE_ID=D.MODULE_ID AND M.APP_ID=D.APP_ID) 
    WHEN NOT MATCHED THEN
    insert (APP_ID,MODULE_ID,IS_ACTIVE,ROW_UID,CREATED_DTTM,LAST_UPDATED_DTTM) 
    VALUES (D.APP_ID,D.MODULE_ID,1,(SELECT MAX(row_uid)+4 FROM app_module),SYSDATE,NULL); 
			   
MERGE INTO app_mod_perm B
USING (select (SELECT APP_ID
                 FROM app
                WHERE app_short_name = 'TPE') APP_ID,
             (SELECT module_id
                FROM module
               WHERE module_code = 'DSHBD') module_id,
             (SELECT permission_id
                FROM permission
               WHERE permission_code = 'TLMPORTLETS') permission_id from dual) C
ON (B.APP_ID=C.APP_ID AND B.module_id=C.module_id and b.permission_id=c.permission_id)
WHEN NOT MATCHED THEN
INSERT (
b.APP_ID,
b.MODULE_ID,
b.permission_id,
b.ROW_UID)
VALUES(C.APP_ID,c.module_id,c.permission_id,SEQ_APP_MOD_PERM.NEXTVAL);	

MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT COMPANY_TYPE.COMPANY_TYPE_ID,
                   COMPANY_TYPE.DESCRIPTION,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM COMPANY_TYPE, PERMISSION
             WHERE COMPANY_TYPE.COMPANY_TYPE_ID = 4
                   AND PERMISSION.PERMISSION_CODE = 'TLMPORTLETS') CTP1
        ON (CTP.COMPANY_TYPE_ID = CTP1.COMPANY_TYPE_ID
            AND CTP.PERMISSION_ID = CTP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
       VALUES (4,
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'TLMPORTLETS'),
               (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM)); 
COMMIT;

-- DBTicket DB-3758

exec sequpdt('PERMISSION_TAG','TAG_ID','SEQ_TAG_ID');
			   
INSERT INTO PERMISSION_TAG (TAG_ID,
                            DESCRIPTION,
                            CREATED_SOURCE_TYPE_ID,
                            CREATED_SOURCE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE_TYPE_ID,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_TAG_ID.NEXTVAL,
             'Dashboard, Portlets',
             1,
             'Seed',
             SYSDATE,
             1,
             'Seed',
             SYSDATE);

INSERT INTO PERMISSION_TAG_MAPPING
     VALUES ( (SELECT permission_id
                 FROM permission
                WHERE PERMISSION_CODE = 'TLMPORTLETS'),
             (SELECT TAG_ID
                FROM PERMISSION_TAG
               WHERE DESCRIPTION = 'Dashboard, Portlets'));
	
COMMIT;

-- DBTicket DB-3876 REFL

EXEC sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_CODE = 'ASHP'
              AND PERMISSION.PERMISSION_CODE = 'EPIEOD') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       values (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.NEXTVAL); 
       
commit;

-- DBTicket DB-3960 REFL

MERGE INTO APP_MOD_PERM AMP
USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
PERMISSION.PERMISSION_CODE
FROM APP, MODULE, PERMISSION
WHERE APP.app_short_name = 'TPE'
AND MODULE.MODULE_CODE = 'CNTRMGT'
AND PERMISSION.PERMISSION_CODE = 'APCLTRANSITTIME') APP1
ON ( AMP.APP_ID = APP1.APP_ID
AND AMP.MODULE_ID = APP1.MODULE_ID
AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);


MERGE INTO APP_MOD_PERM AMP
USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
PERMISSION.PERMISSION_CODE
FROM APP, MODULE, PERMISSION
WHERE APP.app_short_name = 'TPE'
AND MODULE.MODULE_CODE = 'CNTRMGT'
AND PERMISSION.PERMISSION_CODE = 'VPCLZONERATE') APP1
ON ( AMP.APP_ID = APP1.APP_ID
AND AMP.MODULE_ID = APP1.MODULE_ID
AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);

MERGE INTO APP_MOD_PERM AMP
USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
PERMISSION.PERMISSION_CODE
FROM APP, MODULE, PERMISSION
WHERE APP.app_short_name = 'TPE'
AND MODULE.MODULE_CODE = 'CNTRMGT'
AND PERMISSION.PERMISSION_CODE = 'APCLZONERATE') APP1
ON ( AMP.APP_ID = APP1.APP_ID
AND AMP.MODULE_ID = APP1.MODULE_ID
AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);

COMMIT;

-- DBTicket DB-4065 REFL
update UIB_QUERY_DETAILS
set QUERY_COLUMNS='carrier_code.CARRIER_CODE "Carrier", nvl(SUM(ACTUAL_PAYMENT_AMOUNT), 0 ) as "Approved Pending Payment"'
where UIB_QUERY_ID=(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'Invoices Pending Payment by Carrier');

update UIB_QUERY_DETAILS
set QUERY_COLUMNS='CASE WHEN (TIC.RFPRESPONSESTATUS) = 1 THEN ''Yes'' ELSE ''No'' END AS RESPONSE, COUNT(TIC.RFPRESPONSESTATUS) AS COUNT_POSTED_or_BID_ON_LANES'
where UIB_QUERY_ID=(select UIB_QUERY_ID from UIB_QUERY where QUERY_NAME = 'RFP Carrier Response Summary');

commit;


-- DBTicket  DB-4223

MERGE INTO RESOURCE_PERMISSION RP USING
(SELECT RESOURCES.RESOURCE_ID,
  'RTT' AS PERMISSION_CODE
FROM RESOURCES
WHERE RESOURCES.URI      ='/ofr/ra/jsp/NoteList.jsp'
AND RESOURCES.MODULE     ='ACM'
) RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED THEN
  INSERT
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT resource_id
        FROM resources
        WHERE URI ='/ofr/ra/jsp/NoteList.jsp'
        AND MODULE='ACM'
      )
      ,
      'RTT'
    );

MERGE INTO RESOURCE_PERMISSION RP USING
  (SELECT RESOURCES.RESOURCE_ID,
      'RTT' AS PERMISSION_CODE
    FROM RESOURCES
    WHERE RESOURCES.URI  ='/ofr/ra/jsp/TrackingComment.jsp'
    AND RESOURCES.MODULE ='ACM'
  )
  RP1 ON (RP.RESOURCE_ID = RP1.RESOURCE_ID AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
WHEN NOT MATCHED THEN
  INSERT
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT resource_id
        FROM resources
        WHERE URI ='/ofr/ra/jsp/TrackingComment.jsp'
        AND MODULE='ACM'
      )
      ,
      'RTT'
    );
    
COMMIT;

-- DBTicket DB-4247

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Customer Questionnaire' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Customer Questionnaire', 'Customer Questionnaire', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'EPI End of Day' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'EPI End of Day', 'EPI End of Day', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Work Group Proximity' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Work Group Proximity', 'Work Group Proximity', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'YARD User Profile' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'YARD User Profile', 'YARD User Profile', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'YARD Task group Eligibility' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'YARD Task group Eligibility', 'YARD Task group Eligibility', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'YARD Task Path Definition' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'YARD Task Path Definition', 'YARD Task Path Definition', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'YARD Prioritization Rule' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'YARD Prioritization Rule', 'YARD Prioritization Rule', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Dock - Equipment' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Dock - Equipment', 'Dock - Equipment', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Dock - Product Class' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Dock - Product Class', 'Dock - Product Class', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Dock - Protection Level' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Dock - Protection Level', 'Dock - Protection Level', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Dock - Appointment Type' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Dock - Appointment Type', 'Dock - Appointment Type', 'Navigation');

MERGE INTO LABEL L
USING (SELECT 'Navigation' BUNDLE_NAME, 'Service Level - Accessorial Option Group' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME)
values(SEQ_LABEL_ID.NEXTVAL, 'Service Level - Accessorial Option Group', 'Service Level - Accessorial Option Group', 'Navigation');
COMMIT;



-- DBTicket DB-4267 REFL

MERGE INTO APP_MOD_PERM AMP USING
(SELECT APP.APP_ID,
  APP.APP_NAME,
  MODULE.MODULE_ID,
  MODULE.MODULE_CODE,
  PERMISSION.PERMISSION_ID,
  PERMISSION.PERMISSION_CODE
FROM APP,
  MODULE,
  PERMISSION
WHERE APP.app_short_name       = 'TPE'
AND MODULE.MODULE_CODE         = 'ASHP'
AND PERMISSION.PERMISSION_CODE = 'PRTEPIDOC'
) APP1 ON ( AMP.APP_ID         = APP1.APP_ID AND AMP.MODULE_ID = APP1.MODULE_ID AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      AMP.APP_ID,
      AMP.MODULE_ID,
      AMP.PERMISSION_ID,
      AMP.ROW_UID
    )
    VALUES
    (
      APP1.APP_ID,
      APP1.MODULE_ID,
      APP1.PERMISSION_ID,
      SEQ_APP_MOD_PERM.nextval
    );
    
COMMIT;


------ DBTicket DB-4288
----
----MERGE INTO RESOURCES A
----     USING (SELECT '/oblImportRebidLanes.serv' AS URI,
----                   1 AS URI_TYPE_ID
----              FROM DUAL) B
----        ON (A.URI_TYPE_ID = B.URI_TYPE_ID AND A.URI = B.URI)
----WHEN NOT MATCHED
----THEN
----   INSERT     (RESOURCE_ID,
----               URI,
----               MODULE,
----               URI_TYPE_ID,
----               HTTP_METHOD)
----       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
----               '/oblImportRebidLanes.serv',
----               'RFPM',
----               1,
----               NULL);
----			   
----			   
----MERGE INTO RESOURCE_PERMISSION RP
----     USING (SELECT RESOURCES.RESOURCE_ID, 'VRF' AS PERMISSION_CODE
----              FROM RESOURCES
----             WHERE RESOURCES.URI='/oblImportRebidLanes.serv' and RESOURCES.MODULE='RFPM') RP1
----        ON (RP.RESOURCE_ID = RP1.RESOURCE_ID
----            AND RP.PERMISSION_CODE = RP1.PERMISSION_CODE)
----WHEN NOT MATCHED
----THEN
----   INSERT     (RESOURCE_ID, PERMISSION_CODE)
----       VALUES ( (SELECT resource_id
----                  FROM resources
----                 WHERE URI='/oblImportRebidLanes.serv' and MODULE='RFPM'),
----               'VRF');
----			   
----COMMIT;

-- DBTicket DB-4331 REFL

MERGE INTO APP_MOD_PERM AMP
   USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                 MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                 PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
           WHERE APP.APP_ID = (select app_id from app where APP_NAME = 'Transportation LifeCycle Management')
             AND MODULE.MODULE_CODE = 'SCEM'
             AND PERMISSION.PERMISSION_CODE = 'DSCHG') APP1
   ON (    AMP.APP_ID = APP1.APP_ID
       AND AMP.MODULE_ID = APP1.MODULE_ID
       AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
   WHEN NOT MATCHED THEN
      INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
      VALUES (APP1.APP_ID, (SELECT MODULE_ID
                       FROM MODULE
                      WHERE MODULE_CODE = 'SCEM'),
              (SELECT PERMISSION_ID
                 FROM PERMISSION
                WHERE PERMISSION_CODE = 'DSCHG'), SEQ_APP_MOD_PERM.nextval);

COMMIT;

-- DBTicket DB-4362 REFL

INSERT INTO DSHBRD_PORTLET_PERMISSION (DSHBRD_PORTLET_PERMISSION_ID, DSHBRD_PORTLET_ID, PERMISSION_CODE)
values (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'Invoices by Status'),'TLMPORTLETS');

Insert into Dshbrd_portlet_permission (dshbrd_portlet_permission_id, dshbrd_portlet_id, permission_code)
values (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'Shipments by Status'),'TLMPORTLETS');

Insert into Dshbrd_portlet_permission (dshbrd_portlet_permission_id, dshbrd_portlet_id, permission_code)
values (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Response Summary'),'TLMPORTLETS');

Insert into Dshbrd_portlet_permission (dshbrd_portlet_permission_id, dshbrd_portlet_id, permission_code)
values (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'Trips By Status'),'TLMPORTLETS');

Insert into Dshbrd_portlet_permission (dshbrd_portlet_permission_id, dshbrd_portlet_id, permission_code)
values (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Volume'),'TLMPORTLETS');

Insert into Dshbrd_portlet_permission (dshbrd_portlet_permission_id, dshbrd_portlet_id, permission_code)
values (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'Distribution Orders by Status'),'TLMPORTLETS');

Insert into Dshbrd_portlet_permission (dshbrd_portlet_permission_id, dshbrd_portlet_id, permission_code)
values (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'ASNs by Status'),'TLMPORTLETS');

Insert into Dshbrd_portlet_permission (dshbrd_portlet_permission_id, dshbrd_portlet_id, permission_code)
values (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'Invoices Pending Payment by Carrier'),'TLMPORTLETS');

Insert into Dshbrd_portlet_permission (dshbrd_portlet_permission_id, dshbrd_portlet_id, permission_code)
values (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Spend'),'TLMPORTLETS');

Insert into Dshbrd_portlet_permission (dshbrd_portlet_permission_id, dshbrd_portlet_id, permission_code)
VALUES (SEQ_DSHBRD_PORTLET_PERM_ID.NEXTVAL,(SELECT DSHBRD_PORTLET_ID FROM  DSHBRD_PORTLET WHERE PORTLET_NAME = 'RFP Invited Carriers'),'TLMPORTLETS');

COMMIT;

-- DBTicket DB-4762 REFL

MERGE INTO dshbrd_portlet_param_def d USING (select (select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Spend') PORTLET_ID from dual ) b
on (d.PORTLET_ID = b.PORTLET_ID)
when not matched then
INSERT  (DSHBRD_PTLT_PARAM_DEF_ID,PORTLET_ID,PORTLET_PARAM_DEF_NAME,PARAM_LABEL)
values (SEQ_DSHBRD_PTLT_PARAM_DEF_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Spend'),'RFPID','RFP Id');

MERGE INTO dshbrd_portlet_param_def d USING (select (select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Volume') PORTLET_ID from dual ) b
on (d.PORTLET_ID = b.PORTLET_ID)
when not matched then
INSERT  (DSHBRD_PTLT_PARAM_DEF_ID,PORTLET_ID,PORTLET_PARAM_DEF_NAME,PARAM_LABEL)
values (SEQ_DSHBRD_PTLT_PARAM_DEF_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Volume'),'RFPID','RFP Id');

UPDATE UIB_QUERY_DETAILS SET 
QUERY_WHERE_CLAUSE ='SA.RFPID = :request.RFPID.isString AND SA.SCENARIOID in (0) AND SCEN.SCENARIOID = SA.SCENARIOID AND SA.RFPID = SCEN.RFPID
    AND OB.OBCARRIERCODEID = SA.OBCARRIERCODEID AND OB.TPCOMPANYID = CO.COMPANY_ID
    AND SA.RFPID = CBR.RFPID AND OB.OBCARRIERCODEID = CBR.OBCARRIERCODEID
GROUP BY CO.COMPANY_NAME' 
Where UIB_QUERY_ID=(SELECT UIB_QUERY_ID FROM UIB_QUERY WHERE QUERY_NAME = 'RFP Carrier Award Volume');

UPDATE UIB_QUERY_DETAILS SET 
QUERY_WHERE_CLAUSE = 'SA.RFPID = :request.RFPID.isString AND SCEN.SCENARIOID = SA.SCENARIOID AND SA.RFPID = SCEN.RFPID
    AND OB.OBCARRIERCODEID = SA.OBCARRIERCODEID AND OB.TPCOMPANYID = CO.COMPANY_ID
    AND SA.RFPID = CBR.RFPID AND OB.OBCARRIERCODEID = CBR.OBCARRIERCODEID
GROUP BY CO.COMPANY_NAME,SCEN.NAME
ORDER BY SCEN.NAME,CO.COMPANY_NAME'
Where UIB_QUERY_ID=(SELECT UIB_QUERY_ID FROM UIB_QUERY WHERE QUERY_NAME = 'RFP Carrier Award Spend');

COMMIT;

-- DBTicket DB-5156 REFL

MERGE INTO PERMISSION P USING
(SELECT 'IMAR' AS PERMISSION_CODE FROM DUAL
) B ON (P.PERMISSION_CODE = B.PERMISSION_CODE )
WHEN NOT MATCHED THEN
  INSERT
    (
      PERMISSION_ID,
      COMPANY_ID,
      PERMISSION_NAME,
      PERMISSION_CODE,
      IS_ACTIVE,
      CREATED_SOURCE,
      CREATED_SOURCE_TYPE_ID,
      CREATED_DTTM,
      LAST_UPDATED_SOURCE,
      LAST_UPDATED_DTTM,
      LAST_UPDATED_SOURCE_TYPE_ID,
      DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      -1,
      'Manager Approval Required for Invoice Approval',
      'IMAR',
      1,
      'Manhattan Associates',
      3,
      sysdate,
      'Manhattan Associates',
      sysdate,
      3,
      'Manager Approval Required for Invoice Approval'
    );
			 
MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID  FROM PERMISSION WHERE PERMISSION_CODE = 'IMAR') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'IMAR'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
			 
                                              
MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_SHORT_NAME = 'TPE'
                   AND MODULE.MODULE_CODE = 'FAP'
                   AND PERMISSION.PERMISSION_CODE = 'IMAR') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (AMP.APP_ID,
               AMP.MODULE_ID,
               AMP.PERMISSION_ID,
               AMP.ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_SHORT_NAME = 'TPE'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'FAP'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'IMAR'),
               SEQ_APP_MOD_PERM.NEXTVAL);

COMMIT;

----- -- DBTicket DB-5194 REFL
-----                                            
----- MERGE INTO APP_MODULE AM
-----      USING (SELECT (SELECT APP_ID
-----                       FROM APP
-----                      WHERE APP_SHORT_NAME = 'TPE')
-----                       APP_ID,
-----                    (SELECT MODULE_ID
-----                       FROM MODULE
-----                      WHERE MODULE_NAME = 'Manhattan System Management')
-----                       MODULE_ID
-----               FROM DUAL) B
-----         ON (AM.APP_ID = B.APP_ID AND AM.MODULE_ID = B.MODULE_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AM.APP_ID,
-----                AM.MODULE_ID,
-----                AM.IS_ACTIVE,
-----                AM.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_SHORT_NAME = 'TPE'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_NAME = 'Manhattan System Management'),
-----                1,
-----                (SELECT MAX (ROW_UID) + 4
-----                   FROM APP_MODULE));
-----                                                               
-----                                          
----- MERGE INTO APP_MODULE AM
-----      USING (SELECT (SELECT APP_ID
-----                       FROM APP
-----                      WHERE APP_SHORT_NAME = 'mda')
-----                       APP_ID,
-----                    (SELECT MODULE_ID
-----                       FROM MODULE
-----                      WHERE MODULE_NAME = 'Manhattan System Management')
-----                       MODULE_ID
-----               FROM DUAL) B
-----         ON (AM.APP_ID = B.APP_ID AND AM.MODULE_ID = B.MODULE_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AM.APP_ID,
-----                AM.MODULE_ID,
-----                AM.IS_ACTIVE,
-----                AM.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_SHORT_NAME = 'mda'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_NAME = 'Manhattan System Management'),
-----                1,
-----                (SELECT MAX (ROW_UID) + 4
-----                   FROM APP_MODULE));
----- 				  		 
----- 
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ASYSCDSTYPE') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ASYSCDSTYPE'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ASYSCDSPARM') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ASYSCDSPARM'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ASYSCDOTYPE') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ASYSCDOTYPE'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ASYSCDOPARM') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ASYSCDOPARM'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ASYSCDS') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ASYSCDS'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ASYSCD') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ASYSCD'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ASCUR') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ASCUR'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ASCERB') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ASCERB'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ANXTUPNBRB') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ANXTUPNBRB'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'EDEVT') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'EDEVT'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'VMIF') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'VMIF'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'AMIF') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'AMIF'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'VLS') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'VLS'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Transportation LifeCycle Management'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ALS') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Transportation LifeCycle Management'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ALS'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 
----- 
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ADMLBLMGMT') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ADMLBLMGMT'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ADMLM') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ADMLM'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ADMLTLMGMT') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ADMLTLMGMT'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ADMMSGS') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ADMMSGS'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ADMMM') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ADMMM'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ADMSCRNTYPE') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ADMSCRNTYPE'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ADMINBASEMENU') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ADMINBASEMENU'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'ADMINMENU') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'ADMINMENU'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'EDEVT') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'EDEVT'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'AMDS') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'AMDS'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'VMIF') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'VMIF'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 
----- MERGE INTO APP_MOD_PERM AMP
-----      USING (SELECT APP.APP_ID,
-----                    APP.APP_NAME,
-----                    MODULE.MODULE_ID,
-----                    MODULE.MODULE_CODE,
-----                    PERMISSION.PERMISSION_ID,
-----                    PERMISSION.PERMISSION_CODE
-----               FROM APP, MODULE, PERMISSION
-----              WHERE     APP.APP_NAME = 'Master Data Administration'
-----                    AND MODULE.MODULE_CODE = 'MSM'
-----                    AND PERMISSION.PERMISSION_CODE = 'AMIF') APP1
-----         ON (    AMP.APP_ID = APP1.APP_ID
-----             AND AMP.MODULE_ID = APP1.MODULE_ID
-----             AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
----- WHEN NOT MATCHED
----- THEN
-----    INSERT     (AMP.APP_ID,
-----                AMP.MODULE_ID,
-----                AMP.PERMISSION_ID,
-----                AMP.ROW_UID)
-----        VALUES ( (SELECT APP_ID
-----                    FROM APP
-----                   WHERE APP_NAME = 'Master Data Administration'),
-----                (SELECT MODULE_ID
-----                   FROM MODULE
-----                  WHERE MODULE_CODE = 'MSM'),
-----                (SELECT PERMISSION_ID
-----                   FROM PERMISSION
-----                  WHERE PERMISSION_CODE = 'AMIF'),
-----                SEQ_APP_MOD_PERM.NEXTVAL);
----- 			   
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='ASCUR') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMLBLMGMT') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='ADMLM') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='ADMLTLMGMT') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='ADMMSGS') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='ADMMM') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='ADMSCRNTYPE') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='ADMINBASEMENU') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMINMENU') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='EDEVT') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='AMDS') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='VMIF') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='AMIF') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- delete from app_mod_perm where permission_id in (select permission_id from permission where permission_code='ALS') and module_id not in (select MODULE_ID from MODULE  where MODULE_CODE = 'MSM');
----- 
----- EXEC SEQUPDT('role_app_mod_perm','ROW_UID','SEQ_ROLEAPPMODPERM_ROWUID');
----- 
----- 
----- UPDATE ROLE_APP_MOD_PERM SET MODULE_ID = (select M.MODULE_ID from MODULE M where M.MODULE_CODE='MSM'),PERMISSION_ID =(select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ASCERB') WHERE PERMISSION_ID in (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ASCER') AND APP_ID IN (SELECT APP_ID FROM APP WHERE APP_NAME='Transportation LifeCycle Management')
----- AND MODULE_ID NOT IN (select M.MODULE_ID from MODULE M where M.MODULE_CODE='MSM');
----- 
----- UPDATE ROLE_APP_MOD_PERM SET MODULE_ID = (select M.MODULE_ID from MODULE M where M.MODULE_CODE='MSM'),PERMISSION_ID =(select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ANXTUPNBRB') WHERE PERMISSION_ID in (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ANXTUPNBR') AND APP_ID IN (SELECT APP_ID FROM APP WHERE APP_NAME='Transportation LifeCycle Management')
----- AND MODULE_ID NOT IN (select M.MODULE_ID from MODULE M where M.MODULE_CODE='MSM');
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ASCUR') and app_id in (4,12,24);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code='ASCUR') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (4,12,24);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ADMLBLMGMT') and app_id in (1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMLBLMGMT') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (1008) ;
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ADMLM') and app_id in (1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMLM') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (1008);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ADMLTLMGMT') and app_id in (1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMLTLMGMT') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (1008);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ADMMSGS') and app_id in (1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMMSGS') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (1008);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ADMMM') and app_id in (1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMMM') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (1008);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ADMSCRNTYPE') and app_id in (1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMSCRNTYPE') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (1008);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ADMINBASEMENU') and app_id in (1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMINBASEMENU') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (1008);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ADMINMENU') and app_id in (1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ADMINMENU') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (1008);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='EDEVT') and app_id in (24,1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code='EDEVT') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (4,12,24,1008);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='AMDS') and app_id in (1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'AMDS') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (1008);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='ALS') and app_id in (24);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'ALS') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (24);
----- 
----- Update role_app_mod_perm set module_id =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where permission_id in (select permission_id from permission where permission_code='AMIF') and app_id in (24,1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'AMIF') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (24,1008);
----- 
----- update ROLE_APP_MOD_PERM set MODULE_ID =(select MODULE_ID from MODULE  where MODULE_CODE = 'MSM') where PERMISSION_ID in (select PERMISSION_ID from PERMISSION where PERMISSION_CODE='VMIF') and APP_ID in (24,1008);
----- delete from role_app_mod_perm where permission_id in (select permission_id from permission where permission_code = 'VMIF') and module_id not in ((select MODULE_ID from MODULE  where MODULE_CODE = 'MSM')) and app_id in (4,12,24,1008);
----- 
----- commit;

------ DBTicket DB-4762 REFL
----
----MERGE INTO dshbrd_portlet_param_def d USING (select (select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Spend') PORTLET_ID from dual ) b
----on (d.PORTLET_ID = b.PORTLET_ID)
----when not matched then
----INSERT  (DSHBRD_PTLT_PARAM_DEF_ID,PORTLET_ID,PORTLET_PARAM_DEF_NAME,PARAM_LABEL)
----values (SEQ_DSHBRD_PTLT_PARAM_DEF_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Spend'),'RFPID','RFP Id');
----
----MERGE INTO dshbrd_portlet_param_def d USING (select (select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Volume') PORTLET_ID from dual ) b
----on (d.PORTLET_ID = b.PORTLET_ID)
----when not matched then
----INSERT  (DSHBRD_PTLT_PARAM_DEF_ID,PORTLET_ID,PORTLET_PARAM_DEF_NAME,PARAM_LABEL)
----values (SEQ_DSHBRD_PTLT_PARAM_DEF_ID.NEXTVAL,(select DSHBRD_PORTLET_ID from  DSHBRD_PORTLET where PORTLET_NAME = 'RFP Carrier Award Volume'),'RFPID','RFP Id');
----
----UPDATE UIB_QUERY_DETAILS SET 
----QUERY_WHERE_CLAUSE ='SA.RFPID = :request.RFPID.isString AND SA.SCENARIOID in (0) AND SCEN.SCENARIOID = SA.SCENARIOID AND SA.RFPID = SCEN.RFPID
----    AND OB.OBCARRIERCODEID = SA.OBCARRIERCODEID AND OB.TPCOMPANYID = CO.COMPANY_ID
----    AND SA.RFPID = CBR.RFPID AND OB.OBCARRIERCODEID = CBR.OBCARRIERCODEID
----GROUP BY CO.COMPANY_NAME' 
----Where UIB_QUERY_ID=(SELECT UIB_QUERY_ID FROM UIB_QUERY WHERE QUERY_NAME = 'RFP Carrier Award Volume');
----
----UPDATE UIB_QUERY_DETAILS SET 
----QUERY_WHERE_CLAUSE = 'SA.RFPID = :request.RFPID.isString AND SCEN.SCENARIOID = SA.SCENARIOID AND SA.RFPID = SCEN.RFPID
----    AND OB.OBCARRIERCODEID = SA.OBCARRIERCODEID AND OB.TPCOMPANYID = CO.COMPANY_ID
----    AND SA.RFPID = CBR.RFPID AND OB.OBCARRIERCODEID = CBR.OBCARRIERCODEID
----GROUP BY CO.COMPANY_NAME,SCEN.NAME
----ORDER BY SCEN.NAME,CO.COMPANY_NAME'
----Where UIB_QUERY_ID=(SELECT UIB_QUERY_ID FROM UIB_QUERY WHERE QUERY_NAME = 'RFP Carrier Award Spend');
----
----COMMIT;

-- DBTicket DB-5456 REFL

update dshbrd_portlet set url_type ='PARAM' where portlet_name in('RFP Carrier Award Volume','RFP Carrier Award Spend');

COMMIT;

-- DBTicket DB-5585 REFL

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_NAME = 'Common Business Object - Base'
              AND PERMISSION.PERMISSION_CODE = 'AMENUMAINT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, (select max(ROW_UID)+4 from app_mod_perm));
  

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'TPE'
              AND MODULE.MODULE_NAME = 'Common Business Object - Base'
              AND PERMISSION.PERMISSION_CODE = 'VMENUMAINT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, (select max(ROW_UID)+4 from app_mod_perm) );

COMMIT;

-- DBTicket DB-5841 REFL

MERGE INTO app_mod_perm ramp USING
(SELECT DISTINCT app_id, module_id, p.permission_id 
FROM ROLE_APP_MOD_PERM ramp, permission p
WHERE ramp.PERMISSION_ID = (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'AMIF')
AND Ramp.App_Id IN (SELECT App_Id FROM App WHERE App_SHORT_Name ='TPE' )
AND Ramp.Module_Id IN (SELECT Module_Id FROM Module WHERE MODULE_CODE='MSM')
AND P.permission_code IN ('POSTMSG')
)APP1 ON (ramp.app_id = app1.app_id AND ramp.module_id = app1.module_id AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED THEN
INSERT (RAMP.APP_ID,RAMP.MODULE_ID,RAMP.PERMISSION_ID,RAMP.ROW_UID)
values (APP1.APP_ID,APP1.MODULE_ID,APP1.PERMISSION_ID,(select max(ROW_UID)+4 from app_mod_perm));



MERGE INTO role_app_mod_perm ramp USING
(SELECT DISTINCT role_id, app_id, module_id, p.permission_id 
FROM ROLE_APP_MOD_PERM ramp, permission p
WHERE ramp.PERMISSION_ID = (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'AMIF')
AND Ramp.App_Id IN (SELECT App_Id FROM App WHERE App_SHORT_Name ='TPE' )
AND Ramp.Module_Id IN (SELECT Module_Id FROM Module WHERE MODULE_CODE='MSM')
AND P.permission_code IN ('POSTMSG')
)APP1 ON (ramp.role_id = app1.role_id AND ramp.app_id = app1.app_id AND ramp.module_id = app1.module_id AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED THEN
INSERT (RAMP.ROLE_ID,RAMP.APP_ID,RAMP.MODULE_ID,RAMP.PERMISSION_ID,RAMP.ROW_UID)
VALUES (app1.role_id,app1.app_id,app1.module_id,app1.permission_id,SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

COMMIT;

-- DBTicket DB-5965 REFL

DELETE FROM APP_MOD_PERM WHERE MODULE_ID = (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE='MSM') 
 AND PERMISSION_ID =(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='POSTMSG') AND APP_ID IN ( SELECT APP_ID FROM APP WHERE APP_SHORT_NAME IN ('TPE'));
 
MERGE INTO ROLE_APP_MOD_PERM RAMP USING
(SELECT DISTINCT ROLE_ID, APP_ID, M.MODULE_ID, P.PERMISSION_ID 
FROM ROLE_APP_MOD_PERM ramp, permission p, module m 
WHERE ramp.PERMISSION_ID = (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'AMIF')
AND RAMP.APP_ID IN (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME ='TPE' )
AND RAMP.MODULE_ID IN (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE='MSM') 
AND M.module_code='MIF' 
AND P.permission_code IN ('POSTMSG')
)APP1 ON (ramp.role_id = app1.role_id AND ramp.app_id = app1.app_id AND ramp.module_id = app1.module_id AND ramp.permission_id = app1.permission_id)
WHEN NOT MATCHED THEN
INSERT (RAMP.ROLE_ID,RAMP.APP_ID,RAMP.MODULE_ID,RAMP.PERMISSION_ID,RAMP.ROW_UID)
VALUES (APP1.ROLE_ID,APP1.APP_ID,APP1.MODULE_ID,APP1.PERMISSION_ID,SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL);

COMMIT;

-- DBTicket DB-6096

MERGE INTO SYS_CODE_PARM SCP
     USING (SELECT 'S' REC_TYPE, '489' CODE_TYPE, 13 FROM_POSN FROM DUAL) D
        ON (    SCP.REC_TYPE = D.REC_TYPE
            AND SCP.CODE_TYPE = D.CODE_TYPE
            AND SCP.FROM_POSN = D.FROM_POSN)
WHEN NOT MATCHED
THEN
   INSERT (
    REC_TYPE,
    CODE_TYPE,
    FROM_POSN,
    TO_POSN,
    MISC_FLAG_DESC,
    MANDT_FLAG,
    VALID_CODE,
    EDIT_STYLE,
    WM_VERSION_ID,
    SYS_CODE_PARM_ID,
    SYS_CODE_TYPE_ID
  )
  VALUES
  (
    'S',
    '489',
    '13',
    '13',
    'Invoice',
    'Y',
    'F',
    'CKBX',
    '1',
    SYS_CODE_PARM_ID_SEQ.nextval,
    (select sys_code_type_id from sys_code_type where REC_TYPE = 'S' and CODE_TYPE = '489')
  );

update SYS_CODE set MISC_FLAGS='NYNNNNNNNYNNY' where CODE_DESC='Proof of Delivery' AND REC_TYPE='S' AND CODE_TYPE='489';

update SYS_CODE set MISC_FLAGS='NYNNNNNNNYNNY' where CODE_DESC='Parcel Shipping Document' AND REC_TYPE='S' AND CODE_TYPE='489';

update SYS_CODE set MISC_FLAGS='YNNYYNNYNYNNY' where CODE_DESC='Packing List' AND REC_TYPE='S' AND CODE_TYPE='489';

update SYS_CODE set MISC_FLAGS='YNNYYNNNNNNNY' where CODE_DESC='Bill of Lading' AND REC_TYPE='S' AND CODE_TYPE='489';

update SYS_CODE set MISC_FLAGS='NNNNNNYYYYNYY' where CODE_DESC='Air Waybill' AND REC_TYPE='S' AND CODE_TYPE='489';

update SYS_CODE set MISC_FLAGS='NNNYYNNNNYNNY' where CODE_DESC='Custom Invoice' AND REC_TYPE='S' AND CODE_TYPE='489';

update SYS_CODE set MISC_FLAGS='NNNYYNNYNYNNY' where CODE_DESC='Ocean Bill of Lading' AND REC_TYPE='S' AND CODE_TYPE='489';

update SYS_CODE set MISC_FLAGS='NNNNNNNNYYNNY' where CODE_DESC='COGI Documents' AND REC_TYPE='S' AND CODE_TYPE='489';

update SYS_CODE set MISC_FLAGS='NNNNNNNNNYNNY' where CODE_DESC='Freight Invoice' AND REC_TYPE='S' AND CODE_TYPE='489';

update SYS_CODE set MISC_FLAGS='YYNYYNNNNYYNY' where CODE_DESC='Others' AND REC_TYPE='S' AND CODE_TYPE='489';

commit;

-- DBTicket DB-6125 REFL

MERGE into PERMISSION X
               using (select
                              'AVT' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'Administer Virtual Trips' PERMISSION_NAME,
                              'Allows Users to create Virtual Trips' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
               insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
               values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);


MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'AVT') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
   )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'AVT'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));


MERGE INTO APP_MOD_PERM AMP
USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
PERMISSION.PERMISSION_CODE
FROM APP, MODULE, PERMISSION
WHERE APP.app_short_name = 'TPE'
AND MODULE.MODULE_CODE = 'MSHP'
AND PERMISSION.PERMISSION_CODE = 'AVT') APP1
ON ( AMP.APP_ID = APP1.APP_ID
AND AMP.MODULE_ID = APP1.MODULE_ID
AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);



MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'MSHP') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Virtual Trips') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (
      (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner'
      ),
      (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'TPE'),
      (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'MSHP'),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_NAME = 'Administer Virtual Trips'),
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
      0
    );

COMMIT;

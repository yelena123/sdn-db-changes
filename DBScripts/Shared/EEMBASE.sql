-- DBTicket SIF-728

INSERT INTO lrf_report_def (report_def_id_o,
                            description,
                            dbname,
                            TYPE,
                            viewperm,
                            editperm,
                            category,
                            subcategory,
                            report_def_id,
                            report_name,
                            report_file_name,
                            priority)
     VALUES ('PICKLIST',
             'Pick List',
             'OracleDB',
             'Jasper',
             'LRF_VREPDEF',
             'LRF_VREPDEF',
             'EEM',
             'ASN',
             seq_lrf_report_def_id.NEXTVAL,
             'PrintPickList',
             'PrintPickList',
             -1);



INSERT INTO LRF_REPORT_DEF_LAYOUT (REPORT_DEF_ID,
                                   FIELD_POSITION,
                                   IS_REQUIRED,
                                   FIELD_NAME,
                                   FIELD_LABEL,
                                   FIELD_OPERATORS,
                                   FIELD_TYPE,
                                   VALUE_GENERATOR_CLASS,
                                   SELECTION_PAGE_URL,
                                   OPERATOR_TYPE,
                                   SUB_OBJECT_TYPE,
                                   TABLE_NAME,
                                   DEFAULT_VALUE,
                                   IS_LOCALIZABLE,
                                   SEND_PARAMETER,
                                   HIBERNATE_FIELD_NAME,
                                   LOOKUP_ATTRIBUTE,
                                   MAX_DATE_DIFF,
                                   CUST_VALID_CLASS,
                                   HIDDEN_PARAM_CLASS,
                                   DATA_CONFIG_QUERY,
                                   LOOKUP_CONFIG_QUERY,
                                   REPORT_DEF_LAYOUT_ID)
     VALUES (
               (SELECT REPORT_DEF_ID
                  FROM LRF_REPORT_DEF
                 WHERE     REPORT_NAME = 'PrintPickList'
                       AND CATEGORY = 'EEM'
                       AND SUBCATEGORY = 'ASN'),
               1,
               0,
               'ord.order_id',
               'Order Ids',
               'in',
               'STRING',
               NULL,
               NULL,
               'REPORT_TEXT_ITEM',
               NULL,
               NULL,
               NULL,
               0,
               0,
               'ORDER_ID',
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               (lrf_report_def_layout_id_seq.NEXTVAL));


INSERT INTO LRF_REPORT_DEF_LAYOUT (REPORT_DEF_ID,
                                   FIELD_POSITION,
                                   IS_REQUIRED,
                                   FIELD_NAME,
                                   FIELD_LABEL,
                                   FIELD_OPERATORS,
                                   FIELD_TYPE,
                                   VALUE_GENERATOR_CLASS,
                                   SELECTION_PAGE_URL,
                                   OPERATOR_TYPE,
                                   SUB_OBJECT_TYPE,
                                   TABLE_NAME,
                                   DEFAULT_VALUE,
                                   IS_LOCALIZABLE,
                                   SEND_PARAMETER,
                                   HIBERNATE_FIELD_NAME,
                                   LOOKUP_ATTRIBUTE,
                                   MAX_DATE_DIFF,
                                   CUST_VALID_CLASS,
                                   HIDDEN_PARAM_CLASS,
                                   DATA_CONFIG_QUERY,
                                   LOOKUP_CONFIG_QUERY,
                                   REPORT_DEF_LAYOUT_ID)
     VALUES (
               (SELECT REPORT_DEF_ID
	                         FROM LRF_REPORT_DEF
	                        WHERE     REPORT_NAME = 'PrintPickList'
	                              AND CATEGORY = 'EEM'
                       AND SUBCATEGORY = 'ASN'),
              2,
               0,
               'ord.tc_company_id',
               'Company Id',
               '=',
               'STRING',
               NULL,
               NULL,
               'REPORT_TEXT_ITEM',
               NULL,
               NULL,
               NULL,
               0,
               0,
               'TC_COMPANY_ID',
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               (lrf_report_def_layout_id_seq.NEXTVAL));

COMMIT;


-- DBTicket SIF-855

UPDATE LRF_REPORT_DEF
   SET CATEGORY = 'SIF'
 WHERE SUBCATEGORY = 'CYCL' AND CATEGORY = 'EEM';

COMMIT;

-- DBTicket SIF-912

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO PERMISSION L
     USING (SELECT 'CREATETRANSFER' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Create Transfer Order',
               1,
               -1,
               'CREATETRANSFER');
MERGE INTO APP_MODULE AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE
              FROM APP, MODULE
             WHERE APP.APP_NAME = 'Store Inventory & Fulfillment'
                   AND MODULE.MODULE_CODE = 'EEM') APP1
        ON (AMP.APP_ID = APP1.APP_ID AND AMP.MODULE_ID = APP1.MODULE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               IS_ACTIVE,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Store Inventory & Fulfillment'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               1,
               (SELECT MAX (ROW_UID) + 4 FROM APP_MODULE));
			   

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Store Inventory & Fulfillment'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'CREATETRANSFER') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );


           
  INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID
                 FROM COMPANY_TYPE
                WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'CREATETRANSFER'),
             (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));        

--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

  INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID,
                                APP_ID,
                                MODULE_ID,
                                PERMISSION_ID,
                                ROW_UID,
                                IS_PERM_IMPLIED)
     VALUES ( (SELECT RELATIONSHIP_TYPE_ID
                 FROM RELATIONSHIP_TYPE
                WHERE LOWER (DESCRIPTION) = LOWER ('Business Partner')),
             (SELECT app_id
                FROM app
               WHERE app_name LIKE 'Store Inventory & Fulfillment'),
             (SELECT MODULE_ID
                FROM MODULE
               WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID
                FROM PERMISSION
               WHERE PERMISSION_CODE = 'CREATETRANSFER'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0); 

COMMIT;			 

-- DBTicket EEM-5396
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
 
 MERGE INTO APP_MOD_PERM AMP
         USING (SELECT APP.APP_ID,
                       APP.APP_NAME,
                       MODULE.MODULE_ID,
                       MODULE.MODULE_CODE,
                       PERMISSION.PERMISSION_ID,
                       PERMISSION.PERMISSION_CODE
                  FROM APP, MODULE, PERMISSION
                 WHERE     APP.APP_SHORT_NAME = 'eem'
                      AND MODULE.MODULE_CODE = 'EEM'
                      AND PERMISSION.PERMISSION_CODE = 'VIEWSTOREORDER') APP1
           ON (    AMP.APP_ID = APP1.APP_ID
               AND AMP.MODULE_ID = APP1.MODULE_ID
               AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
   WHEN NOT MATCHED
   THEN
      INSERT     (APP_ID,
                  MODULE_ID,
                  PERMISSION_ID,
                  ROW_UID)
          VALUES ( (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'eem'),
                  (SELECT MODULE_ID
                     FROM MODULE
                    WHERE MODULE_CODE = 'EEM'),
                  (SELECT PERMISSION_ID
                     FROM PERMISSION
                    WHERE PERMISSION_CODE = 'VIEWSTOREORDER'),
                  SEQ_APP_MOD_PERM.NEXTVAL);

 MERGE INTO APP_MOD_PERM AMP
         USING (SELECT APP.APP_ID,
                       APP.APP_NAME,
                       MODULE.MODULE_ID,
                       MODULE.MODULE_CODE,
                       PERMISSION.PERMISSION_ID,
                       PERMISSION.PERMISSION_CODE
                  FROM APP, MODULE, PERMISSION
                 WHERE     APP.APP_SHORT_NAME = 'eem'
                      AND MODULE.MODULE_CODE = 'EEM'
                      AND PERMISSION.PERMISSION_CODE = 'VIEWCYCLCNT') APP1
           ON (    AMP.APP_ID = APP1.APP_ID
               AND AMP.MODULE_ID = APP1.MODULE_ID
               AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
   WHEN NOT MATCHED
   THEN
      INSERT     (APP_ID,
                  MODULE_ID,
                  PERMISSION_ID,
                  ROW_UID)
          VALUES ( (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'eem'),
                  (SELECT MODULE_ID
                     FROM MODULE
                    WHERE MODULE_CODE = 'EEM'),
                  (SELECT PERMISSION_ID
                     FROM PERMISSION
                    WHERE PERMISSION_CODE = 'VIEWCYCLCNT'),
                  SEQ_APP_MOD_PERM.NEXTVAL);	

COMMIT;				  			 

-- DBTicket SIF-1165

INSERT INTO lrf_report_def (report_def_id_o,
                            description,
                            dbname,
                            TYPE,
                            viewperm,
                            editperm,
                            category,
                            subcategory,
                            report_def_id,
                            report_name,
                            report_file_name,
                            priority)
     VALUES ('PACKSLIP',
             'Packing Slip Report',
             'OracleDB',
             'Jasper',
             'LRF_VREPDEF',
             'LRF_VREPDEF',
             'EEM',
             'LPN',
             seq_lrf_report_def_id.NEXTVAL,
             'LPNIDPackingSlip',
             'LPNIDPackingSlip',
             -1);

INSERT INTO LRF_REPORT_DEF_LAYOUT (REPORT_DEF_ID,
                                   FIELD_POSITION,
                                   IS_REQUIRED,
                                   FIELD_NAME,
                                   FIELD_LABEL,
                                   FIELD_OPERATORS,
                                   FIELD_TYPE,
                                   VALUE_GENERATOR_CLASS,
                                   SELECTION_PAGE_URL,
                                   OPERATOR_TYPE,
                                   SUB_OBJECT_TYPE,
                                   TABLE_NAME,
                                   DEFAULT_VALUE,
                                   IS_LOCALIZABLE,
                                   SEND_PARAMETER,
                                   HIBERNATE_FIELD_NAME,
                                   LOOKUP_ATTRIBUTE,
                                   MAX_DATE_DIFF,
                                   CUST_VALID_CLASS,
                                   HIDDEN_PARAM_CLASS,
                                   DATA_CONFIG_QUERY,
                                   LOOKUP_CONFIG_QUERY,
                                   REPORT_DEF_LAYOUT_ID)
     VALUES (
               (SELECT REPORT_DEF_ID
                  FROM LRF_REPORT_DEF
                 WHERE     REPORT_NAME = 'LPNIDPackingSlip'
                       AND CATEGORY = 'EEM'
                       AND SUBCATEGORY = 'LPN'),
               1,
               0,
               'LPN_LPN_ID',
               'LPN Id',
               '=',
               'INTIGER',
               NULL,
               NULL,
               'REPORT_TEXT_ITEM',
               NULL,
               NULL,
               NULL,
               0,
               0,
               'LPN_ID',
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               (lrf_report_def_layout_id_seq.NEXTVAL));

COMMIT;

-- DBTicket SIF-1180 

Insert into LRF_REPORT_DEF (REPORT_DEF_ID_O,DESCRIPTION,DBNAME,TYPE,VIEWPERM,EDITPERM,CATEGORY,SUBCATEGORY,REPORT_DEF_ID,REPORT_NAME,REPORT_FILE_NAME,PRIORITY,RESET_COUNT,RESET_INTERVAL,RETRY_COUNT,RETRY_INTERVAL,DATASOURCE) values 
('ORDLST','Order List','OracleDB','Jasper','LRF_VREPDEF','LRF_AREPDEF','SIF','CYCL',seq_lrf_report_def_id.nextval,'OrderListReport','OrderListReport',-1,null,null,null,null,null);

Insert into LRF_REPORT_DEF_LAYOUT (REPORT_DEF_LAYOUT_ID,FIELD_POSITION,IS_REQUIRED,FIELD_NAME,FIELD_LABEL,FIELD_OPERATORS,FIELD_TYPE,VALUE_GENERATOR_CLASS,SELECTION_PAGE_URL,OPERATOR_TYPE,SUB_OBJECT_TYPE,TABLE_NAME,DEFAULT_VALUE,IS_LOCALIZABLE,SEND_PARAMETER,HIBERNATE_FIELD_NAME,LOOKUP_ATTRIBUTE,MAX_DATE_DIFF,REPORT_DEF_ID,CUST_VALID_CLASS,HIDDEN_PARAM_CLASS,DATA_CONFIG_QUERY,LOOKUP_CONFIG_QUERY,RENDERED) values
(( lrf_report_def_layout_id_seq.nextval),3,0,'tc_order_id','Order Id','in','STRING',null,null,'REPORT_TEXT_ITEM',null,null,null,0,0, 'orderIds_ID',null,null,(select REPORT_DEF_ID from LRF_REPORT_DEF where REPORT_NAME= 'OrderListReport' 
and CATEGORY = 'SIF' and SUBCATEGORY = 'CYCL'),null,null,null,null,null);

Insert into LRF_REPORT_DEF_LAYOUT (REPORT_DEF_LAYOUT_ID,FIELD_POSITION,IS_REQUIRED,FIELD_NAME,FIELD_LABEL,FIELD_OPERATORS,FIELD_TYPE,VALUE_GENERATOR_CLASS,SELECTION_PAGE_URL,OPERATOR_TYPE,SUB_OBJECT_TYPE,TABLE_NAME,DEFAULT_VALUE,IS_LOCALIZABLE,SEND_PARAMETER,HIBERNATE_FIELD_NAME,LOOKUP_ATTRIBUTE,MAX_DATE_DIFF,REPORT_DEF_ID,CUST_VALID_CLASS,HIDDEN_PARAM_CLASS,DATA_CONFIG_QUERY,LOOKUP_CONFIG_QUERY,RENDERED) values
(( lrf_report_def_layout_id_seq.nextval),1,0,'ord.tc_company_id','Shipper Id','=','STRING',null,null,'REPORT_TEXT_ITEM',null,null,null,0,0,'tcCompanyId',null,null,(select REPORT_DEF_ID from LRF_REPORT_DEF where REPORT_NAME= 'OrderListReport' 
and CATEGORY = 'SIF' and SUBCATEGORY = 'CYCL'),null,null,null,null,null);

commit;

-- DBTicket SIF-1200

MERGE INTO sys_code a
     USING (SELECT 'S' rec_type, '994' code_type, '122' code_id FROM DUAL) b
        ON (    a.rec_type = b.rec_type
            AND a.code_type = b.code_type
            AND a.code_id = b.code_id)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE, CODE_TYPE, CODE_ID,  CODE_DESC, SHORT_DESC,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_ID,SYS_CODE_TYPE_ID)
       VALUES ('S',  '994', '122','UPS SurePost� Less than 1 lb',  'UPSSPLT1',SYSDATE, SYSDATE,'manh', '1',SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE CODE_TYPE='994'  AND REC_TYPE='S')); 
       
MERGE INTO sys_code a
     USING (SELECT 'S' rec_type, '994' code_type, '123' code_id FROM DUAL) b
        ON (    a.rec_type = b.rec_type
            AND a.code_type = b.code_type
            AND a.code_id = b.code_id)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE, CODE_TYPE, CODE_ID,  CODE_DESC, SHORT_DESC,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_ID,SYS_CODE_TYPE_ID)
       VALUES ('S',  '994', '123','UPS SurePost� 1 lb or Greater',  'UPSSP1ORGT',SYSDATE, SYSDATE,'manh', '1',SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE CODE_TYPE='994'  AND REC_TYPE='S')); 
       
MERGE INTO sys_code a
     USING (SELECT 'S' rec_type, '994' code_type, '124' code_id FROM DUAL) b
        ON (    a.rec_type = b.rec_type
            AND a.code_type = b.code_type
            AND a.code_id = b.code_id)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE, CODE_TYPE, CODE_ID,  CODE_DESC, SHORT_DESC,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_ID,SYS_CODE_TYPE_ID)
       VALUES ('S',  '994', '124','UPS SurePost� BPM',  'UPSSPBPM',SYSDATE, SYSDATE,'manh', '1',SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE CODE_TYPE='994'  AND REC_TYPE='S'));        
       
MERGE INTO sys_code a
     USING (SELECT 'S' rec_type, '994' code_type, '125' code_id FROM DUAL) b
        ON (    a.rec_type = b.rec_type
            AND a.code_type = b.code_type
            AND a.code_id = b.code_id)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE, CODE_TYPE, CODE_ID,  CODE_DESC, SHORT_DESC,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_ID,SYS_CODE_TYPE_ID)
       VALUES ('S',  '994', '125','UPS SurePost� Media',  'UPSSPMED',SYSDATE, SYSDATE,'manh', '1',SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE CODE_TYPE='994'  AND REC_TYPE='S')); 

COMMIT;       
       
-- DBTicket EEM-5563

delete from reltp_app_mod_perm where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('REGMGR') );

delete from role_app_mod_perm where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('REGMGR') );

delete from navigation_app_mod_perm where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('REGMGR') );

delete from REL_APP_MOD_PERM where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('REGMGR'));

delete from app_mod_perm where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('REGMGR') );


delete from reltp_app_mod_perm where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('PRTORD') );

delete from role_app_mod_perm where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('PRTORD') );

delete from navigation_app_mod_perm where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('PRTORD') );

delete from REL_APP_MOD_PERM where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('PRTORD'));

delete from app_mod_perm where app_id = ( select app_id from app where app_name = 'Extended Enterprise Management' )
and permission_id = ( select permission_id from permission where permission_code in  ('PRTORD') );

COMMIT;       

-- DBTicket SIF-1314


MERGE INTO PERMISSION L
      USING (SELECT 'RESTOCK' PERMISSION_CODE FROM DUAL) B
         ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
 WHEN NOT MATCHED
  THEN
     INSERT     (L.PERMISSION_ID,
                 L.PERMISSION_NAME,
                 L.IS_ACTIVE,
                 L.COMPANY_ID,
                 L.PERMISSION_CODE)
         VALUES (SEQ_PERMISSION_ID.NEXTVAL,
                 'Restock',
                 1,
                 -1,
                 'RESTOCK');

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_SHORT_NAME = 'SIF'
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'RESTOCK') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_SHORT_NAME = 'SIF'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'RESTOCK'),
               SEQ_APP_MOD_PERM.NEXTVAL);


MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_SHORT_NAME = 'SIF'
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'RESTOCK') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RELATIONSHIP_TYPE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               IS_PERM_IMPLIED)
       VALUES (4,
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'SIF'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'RESTOCK'),
               (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
               0);

COMMIT;



-- DBTicket SIF-1424

UPDATE lrf_report_def 
SET report_def_id_o='LPNCOLLATE',DESCRIPTION='LPN Collate Report',report_name='LPNCollate',report_file_name='LPNCollate' 
where report_name='PackingSlip' AND category='EEM' AND subcategory='LPN';

UPDATE LRF_REPORT_DEF_LAYOUT SET   FIELD_NAME='LPN.LPN_ID',FIELD_TYPE='NUMBER' WHERE HIBERNATE_FIELD_NAME='LPN_ID'
AND  REPORT_DEF_ID=( select REPORT_DEF_ID from LRF_REPORT_DEF where REPORT_NAME= 'LPNCollate' and CATEGORY = 'EEM' and SUBCATEGORY = 'LPN');

COMMIT;

-- DBTicket SIF-1535

MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'RESTOCK') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'RESTOCK'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    );

COMMIT;


-- DBTicket EEM-5681

UPDATE CUSTOM_VIEW_UIATTR
   SET custom_view_id =
          '/manh/olm/inventory/inventorymanagement/xhtml/InventoryDataList.xhtml'
 WHERE object_type_id = (SELECT OBJECT_TYPE_ID
                           FROM OBJECT_TYPE_TABLE
                          WHERE OBJECT_TYPE_NAME = 'InventoryBySource');

UPDATE CUSTOM_VIEW_UIATTR
   SET custom_view_id =
          '/manh/olm/inventory/inventorymanagement/xhtml/GlobalItemInventory.xhtml'
 WHERE object_type_id = (SELECT OBJECT_TYPE_ID
                           FROM OBJECT_TYPE_TABLE
                          WHERE OBJECT_TYPE_NAME = 'GlobalItemInventory');

COMMIT;

-- DBTicket SIF-1959

UPDATE lrf_report_def 
SET report_def_id_o='LPNCOLLATE',DESCRIPTION='LPN Collate Report',report_name='LPNCollate',report_file_name='LPNCollate' 
where report_name='LPNIDPackingSlip' AND category='EEM' AND subcategory='LPN';

UPDATE LRF_REPORT_DEF_LAYOUT SET FIELD_NAME='LPN.LPN_ID',FIELD_TYPE='NUMBER' WHERE HIBERNATE_FIELD_NAME='LPN_ID'
AND REPORT_DEF_ID=( select REPORT_DEF_ID from LRF_REPORT_DEF where REPORT_NAME= 'LPNCollate' 
and CATEGORY = 'EEM' and SUBCATEGORY = 'LPN');

commit;

-- DBTicket SIF-2010 REFL

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'SCV'
              AND PERMISSION.PERMISSION_CODE = 'I_VIE') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );
               
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'SCV'
              AND PERMISSION.PERMISSION_CODE = 'DOM_VISYT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );               


MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'SCV'
              AND PERMISSION.PERMISSION_CODE = 'VBR') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );               


MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Store Inventory & Fulfillment'
              AND MODULE.MODULE_CODE = 'SCV'
              AND PERMISSION.PERMISSION_CODE = 'I_VPI') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               ); 
               
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Store Inventory & Fulfillment'
              AND MODULE.MODULE_CODE = 'SCV'
              AND PERMISSION.PERMISSION_CODE = 'I_AEC') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );                


MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Store Inventory & Fulfillment'
              AND MODULE.MODULE_CODE = 'SCV'
              AND PERMISSION.PERMISSION_CODE = 'I_VOA') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               ); 
               
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Store Inventory & Fulfillment'
              AND MODULE.MODULE_CODE = 'SCV'
              AND PERMISSION.PERMISSION_CODE = 'I_VIE') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );  
               
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Store Inventory & Fulfillment'
              AND MODULE.MODULE_CODE = 'SCV'
              AND PERMISSION.PERMISSION_CODE = 'DOM_VISYT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );  

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Store Inventory & Fulfillment'
              AND MODULE.MODULE_CODE = 'SCV'
              AND PERMISSION.PERMISSION_CODE = 'VBR') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );                           
               
commit;

-- DBTicket SIF-2232

MERGE INTO LABEL L
     USING (SELECT 'UCL' BUNDLE_NAME,
                   'UCLPermissions_Create Transfer Order' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (Seq_Label_Id.Nextval, 'UCLPermissions_Create Transfer Order', 'Create Transfer Order', 'UCL');
       
commit; 

-- DBTicket EEM-5842

UPDATE LRF_REPORT_DEF_LAYOUT
   SET OPERATOR_TYPE = 'REPORT_TEXT_ITEM'
 WHERE REPORT_DEF_ID = (SELECT REPORT_DEF_ID
                          FROM LRF_REPORT_DEF
                         WHERE REPORT_FILE_NAME LIKE 'ContentLabelPrint1');

COMMIT;

-- DBTicket SIF-2702

MERGE INTO sys_code a
     USING (SELECT 'S' rec_type, '994' code_type, '104' code_id FROM DUAL) b
        ON (    a.rec_type = b.rec_type
            AND a.code_type = b.code_type
            AND a.code_id = b.code_id)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_ID,
               CODE_DESC,
               SHORT_DESC,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('S',
               '994',
               '104',
               'FedEx SP Returns',
               'FedExSPRET',
               SYSDATE,
               SYSDATE,
               'manh',
               '1',
               SYS_CODE_ID_SEQ.NEXTVAL,
               (SELECT SYS_CODE_TYPE_ID
                  FROM SYS_CODE_TYPE
                 WHERE CODE_TYPE = '994' AND REC_TYPE = 'S'));

COMMIT;

-- DBTicket SIF-2674 REFL

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

 
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name = 'SIF'
              AND MODULE.MODULE_CODE = 'SCPP'
              AND PERMISSION.PERMISSION_CODE = 'UCLSA') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );

MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'UCLSA') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'UCLSA'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    );
    
MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_SHORT_NAME = 'SIF'
                   AND MODULE.MODULE_CODE = 'SCPP'
                   AND PERMISSION.PERMISSION_CODE = 'UCLSA') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RELATIONSHIP_TYPE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               IS_PERM_IMPLIED)
       VALUES (4,
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'SIF'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'SCPP'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'UCLSA'),
               (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
               0);


COMMIT;    

-- DBTicket SIF-3104

INSERT INTO lrf_report_def_layout (REPORT_DEF_LAYOUT_ID,
                                   IS_REQUIRED,
                                   FIELD_POSITION,
                                   FIELD_NAME,
                                   FIELD_LABEL,
                                   FIELD_OPERATORS,
                                   FIELD_TYPE,
                                   OPERATOR_TYPE,
                                   HIBERNATE_FIELD_NAME,
                                   REPORT_DEF_ID)
     VALUES (lrf_report_def_layout_id_seq.NEXTVAL,
             0,
             2,
             'AWBD.ACTIVITY_ID',
             'Activity_Id',
             'in',
             'STRING',
             'REPORT_TEXT_ITEM',
             'activity_Id',
             (SELECT report_def_id
                FROM lrf_report_def
               WHERE report_def_id_o = 'ORDLST'));

COMMIT;

-- DBTicket SIF-3194

UPDATE LRF_REPORT_DEF_LAYOUT
   SET FIELD_NAME = 'sua.activity_id',
       HIBERNATE_FIELD_NAME = 'ACTIVITY_ID',
       FIELD_LABEL = 'Activity Id'
 WHERE field_name = 'ord.order_id'
       AND REPORT_DEF_ID = (SELECT REPORT_DEF_ID
                              FROM LRF_REPORT_DEF
                             WHERE REPORT_NAME LIKE 'PrintPickList');

UPDATE LRF_REPORT_DEF_LAYOUT
   SET FIELD_NAME = 'sua.company_id'
 WHERE FIELD_NAME = 'ord.tc_company_id'
       AND REPORT_DEF_ID = (SELECT REPORT_DEF_ID
                              FROM LRF_REPORT_DEF
                             WHERE REPORT_NAME LIKE 'PrintPickList');

COMMIT;

-- DBTicket EEM-6350 REFL

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

MERGE INTO APP_MODULE AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE
              FROM APP, MODULE
             WHERE APP.APP_NAME = 'Extended Enterprise Management'
                   AND MODULE.MODULE_CODE = 'CMD') APP1
        ON (AMP.APP_ID = APP1.APP_ID AND AMP.MODULE_ID = APP1.MODULE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               IS_ACTIVE,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_NAME = 'Extended Enterprise Management'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'CMD'),
               1,
               (SELECT MAX (ROW_UID) + 4 FROM APP_MODULE));
               
               
INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_MODEL_CREATE'),SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_MODEL_READ'),SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_MODEL_MODIFY'),SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_MODEL_DELETE'),SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_CS_CREATE'),SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_CS_READ'),SEQ_APP_MOD_PERM.NEXTVAL); 
 
INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_CS_MODIFY'),SEQ_APP_MOD_PERM.NEXTVAL);  
 
INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_CS_DELETE'),SEQ_APP_MOD_PERM.NEXTVAL); 

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_DEPL_CREATE'),SEQ_APP_MOD_PERM.NEXTVAL); 

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_DEPL_READ'),SEQ_APP_MOD_PERM.NEXTVAL); 

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_DEPL_MODIFY'),SEQ_APP_MOD_PERM.NEXTVAL); 

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_DEPL_DELETE'),SEQ_APP_MOD_PERM.NEXTVAL);  

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_EPNT_CREATE'),SEQ_APP_MOD_PERM.NEXTVAL);  


INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_EPNT_READ'),SEQ_APP_MOD_PERM.NEXTVAL); 

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_EPNT_MODIFY'),SEQ_APP_MOD_PERM.NEXTVAL);  

INSERT INTO APP_MOD_PERM(APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
 VALUES ((select app_id from app where APP_ID=40),(select module_id from module where module_code='CMD'),
 (select permission_id from permission where permission_code='CMD_EPNT_DELETE'),SEQ_APP_MOD_PERM.NEXTVAL); 


INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_MODEL_CREATE'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_MODEL_READ'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_MODEL_MODIFY'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_MODEL_DELETE'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_CS_CREATE'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_CS_READ'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_CS_MODIFY'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_CS_DELETE'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_DEPL_CREATE'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_DEPL_READ'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_DEPL_MODIFY'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_DEPL_DELETE'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_EPNT_CREATE'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_EPNT_READ'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_EPNT_MODIFY'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED) 
VALUES (4,(select app_id from app where APP_ID=40),
(select module_id from module where module_code='CMD'),
(select permission_id from permission where permission_code='CMD_EPNT_DELETE'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),0);

commit;


-- DBTicket EEM-6421 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

-----------------------------------------------------------------------------------------------------
-- Permissions for Message Board
-----------------------------------------------------------------------------------------------------
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Add Messages', 1, 'ADDMSG');
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)    
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'View Messages', 1, 'VIEWMSG');
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Delete Messages', 1, 'DELETEMSG');
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Edit Messages', 1, 'EDITMSG');
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'View External Recipients', 1, 'VIEWEXTRECIPS');    
     
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'ADDMSG'),
             SEQ_APP_MOD_PERM.nextval);
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'VIEWMSG'),
             SEQ_APP_MOD_PERM.nextval);
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'DELETEMSG'),
             SEQ_APP_MOD_PERM.nextval);
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'EDITMSG'),
             SEQ_APP_MOD_PERM.nextval);
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'VIEWEXTRECIPS'),
             SEQ_APP_MOD_PERM.nextval);
             
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADDMSG'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWMSG'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DELETEMSG'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EDITMSG'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWEXTRECIPS'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADDMSG'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWMSG'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DELETEMSG'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EDITMSG'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWEXTRECIPS'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);

commit;

-- DBTicket SIF-3417 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

-- View Store Tags  
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'View Store Tags', 1, 'VIEWSTAG');
     
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 128),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'VIEWSTAG'),
             SEQ_APP_MOD_PERM.nextval);
             
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWSTAG'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 128),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWSTAG'),
              (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
-- Admin Store Tags             
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Admin Store Tags', 1, 'ADMSTAG');
     
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 128),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'ADMSTAG'),
             SEQ_APP_MOD_PERM.nextval);
             
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADMSTAG'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 128),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADMSTAG'),
              (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
             
commit;             

-- DBTicket SIF-3467

MERGE INTO sys_code a
     USING (SELECT 'S' rec_type, '537' code_type, 'SAN' code_id FROM DUAL) b
        ON (    a.rec_type = b.rec_type
            AND a.code_type = b.code_type
            AND a.code_id = b.code_id)
WHEN NOT MATCHED
THEN
   INSERT    
(REC_TYPE, CODE_TYPE, CODE_ID,  CODE_DESC, SHORT_DESC,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_ID,SYS_CODE_TYPE_ID)
 VALUES ('S',  '537', 'SAN','Store Activity Number',  'ActivityId',SYSDATE, SYSDATE,'SCA', '1',SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE CODE_TYPE='537'  AND REC_TYPE='S')); 
 
commit;               
       
-- DBTicket SIF-3488

INSERT INTO PERMISSION_INHERITANCE (PARENT_PERMISSION_ID, CHILD_PERMISSION_ID) VALUES(
(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='ADMSTAG'),
(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VIEWSTAG'));

commit;

-- DBTicket SIF-3486 REFL


exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');
 
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES (  SEQ_PERMISSION_ID.NEXTVAL, 'Cancel Activity', 1, 'CANCELACTIVITY');
     
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 128),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'CANCELACTIVITY'),
             SEQ_APP_MOD_PERM.nextval);
             
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CANCELACTIVITY'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 128),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'CANCELACTIVITY'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);

commit;

-- DBTicket SIF-3500 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');
 
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Skip Order', 1, 'SKIPORDER');

INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 128),
(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
(SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'SKIPORDER'),
SEQ_APP_MOD_PERM.nextval);

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'SKIPORDER'),
(SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
VALUES (4,
(SELECT APP_ID FROM APP WHERE APP_ID = 128),
(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'SKIPORDER'),
(SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
0);

commit;

-- DBTicket SIF-3505 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Reprocess Orders', 1, 'REPROCESSORDERS');
     
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 128),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'REPROCESSORDERS'),
            SEQ_APP_MOD_PERM.nextval);
             
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'REPROCESSORDERS'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 128),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'REPROCESSORDERS'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);

commit;

-- DBTicket EEM-6495 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Add Document', 1, 'ADDDOC');
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)	
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'View Document', 1, 'VIEWDOC');
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Delete Document', 1, 'DELETEDOC');
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Edit Document', 1, 'EDITDOC');
INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'View External Recipients Reference Library', 1, 'VIEWEXTRECIPSREF');	
	 
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'ADDDOC'),
             SEQ_APP_MOD_PERM.nextval);
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'VIEWDOC'),
             SEQ_APP_MOD_PERM.nextval);
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'DELETEDOC'),
             SEQ_APP_MOD_PERM.nextval);
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'EDITDOC'),
             SEQ_APP_MOD_PERM.nextval);
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'VIEWEXTRECIPSREF'),
             SEQ_APP_MOD_PERM.nextval);
			 
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADDDOC'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWDOC'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DELETEDOC'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EDITDOC'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWEXTRECIPSREF'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADDDOC'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWDOC'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DELETEDOC'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EDITDOC'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VIEWEXTRECIPSREF'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);

commit;

-- DBTicket EEM-6540

MERGE INTO sys_code a
     USING (SELECT 'B' rec_type, '093' code_type, 'PL' code_id FROM DUAL) b
        ON (    a.rec_type = b.rec_type
            AND a.code_type = b.code_type
            AND a.code_id = b.code_id)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_ID,
               CODE_DESC,
               SHORT_DESC,
               MISC_FLAGS,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('B',
               '093',
               'PL',
               'Policies',
               'Policies',
               NULL,
               SYSDATE,
               SYSDATE,
               'AU2',
               1,
               SYS_CODE_ID_SEQ.NEXTVAL,
               (SELECT SYS_CODE_TYPE_ID
                  FROM SYS_CODE_TYPE
                 WHERE rec_type = 'B' AND code_type = '093'));
                 
MERGE INTO sys_code a
     USING (SELECT 'B' rec_type, '093' code_type, 'PL' code_id FROM DUAL) b
        ON (    a.rec_type = b.rec_type
            AND a.code_type = b.code_type
            AND a.code_id = b.code_id)
WHEN NOT MATCHED
THEN
   INSERT     (REC_TYPE,
               CODE_TYPE,
               CODE_ID,
               CODE_DESC,
               SHORT_DESC,
               MISC_FLAGS,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               SYS_CODE_ID,
               SYS_CODE_TYPE_ID)
       VALUES ('B',
               '093',
               'PL',
               'Procedure',
               'Procedure',
               NULL,
               SYSDATE,
               SYSDATE,
               'AU2',
               1,
               SYS_CODE_ID_SEQ.NEXTVAL,
               (SELECT SYS_CODE_TYPE_ID
                  FROM SYS_CODE_TYPE
                 WHERE rec_type = 'B' AND code_type = '093'));                 


commit;

-- DBTicket SIF-3651 REFL

update permission set permission_name='Administer Store Tags' where permission_code='ADMSTAG';
commit;

-- DBTicket SIF-4293 REFL

update permission set permission_name = 'Administer Win Mobile Configuration' where permission_code = 'AFSCONFIG';
update permission set permission_name = 'View Win Mobile PO Line tab' where permission_code = 'VPOLINETAB';
update permission set permission_name = 'Search Incorrect LPN Win Mobile' where permission_code = 'LPNSERVERSEARCH';
update permission set permission_name = 'Win Mobile Item Level Receipt' where permission_code = 'RCVITEMPROMPT';
update permission set permission_name = 'Win Mobile Inventory Adjustment Subtraction' where permission_code = 'INVADJSUB';
update permission set permission_name = 'View Defect Masters' where permission_code = 'VDM';
update permission set permission_name = 'Win Mobile Outbound Sync' where permission_code = 'SYNCOUTBOUND';
update permission set permission_name = 'Administer Defect Masters' where permission_code = 'ADM';
update permission set permission_name = 'Win Mobile LPN Misc Instructions' where permission_code = 'LPNMISCPROMPT';
update permission set permission_name = 'Win Mobile LPN Destination Mismatch Notification' where permission_code = 'LPNDESTMISMATCH';
update permission set permission_name = 'Win Mobile Inventory Adjustment Addition' where permission_code = 'INVADJADD';
update permission set permission_name = 'Win Mobile Inbound/Outbound Sync' where permission_code = 'SYNCINOUT';
update permission set permission_name = 'Win Mobile Inbound Sync' where permission_code = 'SYNCINBOUND';
update permission set permission_name = 'Administer Rate Shop' where permission_code = 'ARATESHOP';
update permission set permission_name = 'Pack Customer Order' where permission_code = 'DOCUSTORDER';
update permission set permission_name = 'Pack Transfer Order' where permission_code = 'DOTRANSFER';
update permission set permission_name = 'Pack Return Order' where permission_code = 'DORETURNS';
update permission set permission_name = 'Create Transfer Orders' where permission_code = 'CREATETRANSFER';
update permission set permission_name = 'Skip Packing Order' where permission_code = 'SKIPORDER';
update permission set permission_name = 'Administer Inventory Error Code' where permission_code = 'I_AEC';


----update LABEL set  KEY = 'UCLPermissions_Administer Win Mobile Configuration', VALUE = 'Administer Win Mobile Configuration' where KEY = 'UCLPermissions_FieldScout - Admin Configuration' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = ' Win Mobile PO Line tab', VALUE = 'View Win Mobile PO Line tab' where KEY = 'UCLPermissions_FieldScout - View PO Line Tab' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Search Incorrect LPN Win Mobile', VALUE = 'Search Incorrect LPN Win Mobile' where KEY = 'UCLPermissions_FieldScout - Search Incorrect LPN' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Win Mobile Item Level Receipt', VALUE = 'Win Mobile Item Level Receipt' where KEY = 'UCLPermissions_FieldScout - Item Level Receipt Prompt' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Win Mobile Inventory Adjustment Subtraction', VALUE = 'Win Mobile Inventory Adjustment Subtraction' where KEY = 'UCLPermissions_FieldScout - Inventory Adjustment Subtraction' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_View Defect Masters', VALUE = 'View Defect Masters' where KEY = 'UCLPermissions_View Defect Master' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Win Mobile Outbound Sync', VALUE = 'Win Mobile Outbound Sync' where KEY = 'UCLPermissions_FieldScout - Outbound Sync' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Administer Defect Masters', VALUE = 'Administer Defect Masters' where KEY = 'UCLPermissions_Administer Defect Master' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Win Mobile LPN Misc Instructions', VALUE = 'Win Mobile LPN Misc Instructions' where KEY = 'UCLPermissions_FieldScout - LPN Misc Instructions Prompt' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Win Mobile LPN Destination Mismatch Notification', VALUE = 'Win Mobile LPN Destination Mismatch Notification' where KEY = 'UCLPermissions_FieldScout - LPN Destination Mismatch Notification' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Win Mobile Inventory Adjustment Addition', VALUE = 'Win Mobile Inventory Adjustment Addition' where KEY = 'UCLPermissions_FieldScout - Inventory Adjustment Addition' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Win Mobile Inbound/Outbound Sync', VALUE = 'Win Mobile Inbound/Outbound Sync' where KEY = 'UCLPermissions_FieldScout - Inbound/Outbound Sync' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Win Mobile Inbound Sync', VALUE = 'Win Mobile Inbound Sync' where KEY = 'UCLPermissions_FieldScout - Inbound Sync' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Administer Rate Shop', VALUE = 'Administer Rate Shop' where KEY = 'UCLPermissions_Admin Rate Shop' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Pack Customer Order', VALUE = 'Pack Customer Order' where KEY = 'UCLPermissions_FieldScout - DO Customer Order' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Pack Transfer Order', VALUE = 'Pack Transfer Order' where KEY = 'UCLPermissions_FieldScout - DO Transfer' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Pack Return Order', VALUE = 'Pack Return Order' where KEY = 'UCLPermissions_FieldScout - DO Returns' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Create Transfer Orders', VALUE = 'Create Transfer Orders' where KEY = 'UCLPermissions_Create Transfer Order' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Skip Packing Order', VALUE = 'Skip Packing Order' where KEY = 'UCLPermissions_Skip Order' AND BUNDLE_NAME = 'UCL' ;
----update LABEL set  KEY = 'UCLPermissions_Administer Inventory Error Code', VALUE = 'Administer Inventory Error Code' where KEY = 'UCLPermissions_Admin Error Code' AND BUNDLE_NAME = 'UCL' ;


-- Permission Description changes
update permission set description = 'Allows a user to adjust received quantity while crossdocking' where permission_code = 'ADJRCVQTY';
update permission set description = 'Allows a user to modify the windows mobile configuration' where permission_code = 'AFSCONFIG';
update permission set description = 'Allows a user to view the PO line tab in windows mobile' where permission_code = 'VPOLINETAB';
update permission set description = 'Allows a user to receive and verify the ASN in mobile and web UI' where permission_code = 'RCVASNVFY';
update permission set description = 'Allows a user to search an incorrect LPN in windows mobile' where permission_code = 'LPNSERVERSEARCH';
update permission set description = 'Allows a user to receive an ASN to inventory in mobile and web UI' where permission_code = 'RCVASN';
update permission set description = 'Allows a user to add, edit, pass and fail inspections' where permission_code = 'AINSP';
update permission set description = 'Allows a user to view the item level receipt prompt' where permission_code = 'RCVITEMPROMPT';
update permission set description = 'Allows a user to view reconciliation summary page that maintains mobile transaction results' where permission_code = 'VRECS';
update permission set description = 'Allows a user to perform negative inventory adjustment in windows mobile' where permission_code = 'INVADJSUB';
update permission set description = 'Allows a user to ship store orders in web UI' where permission_code = 'ASSHIP';
update permission set description = 'Allows a user to view fieldvision page' where permission_code = 'VFV';
update permission set description = 'Allows a user to view defect masters' where permission_code = 'VDM';
update permission set description = 'Allows a user to perform outbound sync in windows mobile' where permission_code = 'SYNCOUTBOUND';
update permission set description = 'Allows user to add, edit and delete defect masters' where permission_code = 'ADM';
update permission set description = 'Allows a user to cross dock inbound LPNs to outbound ASN in win-mobile and web UI' where permission_code = 'RCVLPNOBASN';
update permission set description = 'Allows a user to print price ticket for items in web UI' where permission_code = 'PPTKT';
update permission set description = 'Allows a user to cross dock inbound ASN to outbound in win-mobile and web UI' where permission_code = 'RCVASNOBASN';
update permission set description = 'Allows a user to receive an LPN to inventory in mobile and web UI' where permission_code = 'RCVLPN';
update permission set description = 'Allows a user to view LPN Misc Instructions prompt in windows mobile' where permission_code = 'LPNMISCPROMPT';
update permission set description = 'Allows a user to view LPN Destination Mismatch Notification in windows mobile' where permission_code = 'LPNDESTMISMATCH';
update permission set description = 'Allows a user to perform positive inventory adjustment in windows mobile' where permission_code = 'INVADJADD';
update permission set description = 'Allows a user to receive blindly without ASN or LPN information in ios/android mobile' where permission_code = 'BLNDRCV';
update permission set description = 'Allows the user to pack items by scanning them' where permission_code = 'ASCANPK';
update permission set description = 'Allows a user to acknowledge or reprocess mobile transactions from web UI' where permission_code = 'ARECS';
update permission set description = 'Allows user to perform sync for both inbound and outbound in win-mobile' where permission_code = 'SYNCINOUT';
update permission set description = 'Allows a user to add a license to be able to view FieldVision' where permission_code = 'AFVL';
update permission set description = 'Allows a user to perform inbound sync in win-mobile' where permission_code = 'SYNCINBOUND';
update permission set description = 'Allows a user to view inspection details in web UI' where permission_code = 'VINSP';
update permission set description = 'Allows a user to delete an inspection' where permission_code = 'DINSP';
update permission set description = 'Allows a user to change the default carrier service level combination in rate packages page in web UI. Controls the change of shipping options while packing store order' where permission_code = 'ARATESHOP';
update permission set description = 'Identifies the user as a store manager' where permission_code = 'CCRSM';
update permission set description = 'Identifies the user as a store associate' where permission_code = 'CCRSA';
update permission set description = 'Allows a user to re-print pick list in web UI' where permission_code = 'PRTPKL';
update permission set description = 'Allows a user to Print the store order in web UI' where permission_code = 'PRTORD';
update permission set description = 'Allows a user to submit the pick in mobile and web UI' where permission_code = 'SSPICK';
update permission set description = 'Allows a user to create cycle count request in web UI' where permission_code = 'CREATECYCLCNT';
update permission set description = 'Allows a user to see the cycle count created in mobile and web UI.' where permission_code = 'VIEWCYCLCNT';
update permission set description = 'Allows a user to see the cycle count list page in web UI' where permission_code = 'SEARCHCYCLCNT';
update permission set description = 'Allows a user to edit a cycle count request in web UI' where permission_code = 'EDITCYCLCNT';
update permission set description = 'Allows a user to cancel a cycle count request in web UI' where permission_code = 'CANCELCYCLCNT';
update permission set description = 'Allows a user to cancel cycle count line in web UI' where permission_code = 'CANCLCYCLCNTLINE';
update permission set description = 'Allows a user to assign cycle count request to other associates' where permission_code = 'ASSIGNCYCLCNT';
update permission set description = 'Allows a user to initiate a cycle count request from web UI' where permission_code = 'INITIATECYCLCNT';
update permission set description = 'Allows a user to enter count for a  cycle count request in web UI' where permission_code = 'ENTERCYCLCNT';
update permission set description = 'Allows a user to review the counts submitted for a cycle count request in web UI' where permission_code = 'REVIEWCYCLCNT';
update permission set description = 'Allows a user to see the store menu in web UI' where permission_code = 'STORECOMMTAB';
update permission set description = 'Allows a user to pack customer order in mobile' where permission_code = 'DOCUSTORDER';
update permission set description = 'Allows a user to pack transfer order in mobile' where permission_code = 'DOTRANSFER';
update permission set description = 'Allows a user to pack Return order in win-mobile' where permission_code = 'DORETURNS';
update permission set description = 'Allows a user to see the store orders page in web UI' where permission_code = 'VIEWSTOREORDER';
update permission set description = 'Allows a user to accept a store order in web UI' where permission_code = 'ACCEPTSTOREORDER';
update permission set description = 'Allows a user to confirm pickup a store order from mobile and web UI' where permission_code = 'CONFRMSTOREPICK';
update permission set description = 'Allows a user to cancel store orders from web UI. Governs the display of cancel button under more.' where permission_code = 'CANCELSTOREORDER';
update permission set description = 'Allows a user to Audit packages before shipping in the web UI' where permission_code = 'AUDITSTOREORDER';
update permission set description = 'Allows a user to pack store orders in Web UI' where permission_code = 'PACKSTOREORDER';
update permission set description = 'Identifies a user as regional manager' where permission_code = 'REGMGR';
update permission set description = 'Allows a user to override the overshipping warning while packing' where permission_code = 'OVRSHPOVRIDE';
update permission set description = 'Allows a user to override the overshipping warning while packing' where permission_code = 'OVRPCKOVRIDE';
update permission set description = 'Allows a user to build residual LPN for prepack items' where permission_code = 'RESIDULPN';
update permission set description = 'Allows a user to build shipment by scanning packages in mobile' where permission_code = 'BUILDSHIPMENT';
update permission set description = 'Allows a user to ship the shipment by created in mobile' where permission_code = 'CLOSESHIPMENT';
update permission set description = 'Allows a user to print cycle count lines which are assigned to him' where permission_code = 'PRINTASSGNDCNT';
update permission set description = 'Allows a user to print all the cycle count lines irrespective of assignment' where permission_code = 'PRINTALLCOUNT';
update permission set description = 'Allows a user to create transfer orders from web UI in store orders page' where permission_code = 'CREATETRANSFER';
update permission set description = 'Allows a user to perform restocking in ios/andriod mobile' where permission_code = 'RESTOCK';
update permission set description = 'Allows a user to see the configured store tags. Controls the menu link manage pickqueue' where permission_code = 'VIEWSTAG';
update permission set description = 'Allows a user to add, edit and delete tag and its rules in web UI.' where permission_code = 'ADMSTAG';
update permission set description = 'Allows a user to cancel activity from store orders page in web UI' where permission_code = 'CANCELACTIVITY';
update permission set description = 'Allows a user to skip packing for the current order in web UI' where permission_code = 'SKIPORDER';
update permission set description = 'Allows a user to reprocess open orders from store orders page in web UI' where permission_code = 'REPROCESSORDERS';
update permission set description = 'Allows a user to rate and ship using a parcel carrier in web UI' where permission_code = 'APARC';
update permission set description = 'Allows a user to close a manifest in web UI' where permission_code = 'AMFST';
update permission set description = 'Allows a user to see trace and tracel parcel page in web UI' where permission_code = 'VTATP';
update permission set description = 'Allows a user to cancel parcel shipment from shipment pages in web UI' where permission_code = 'CPCLSHP';
update permission set description = 'Allows a user to see the manifest reports in web UI' where permission_code = 'VMFSTRPT';
update permission set description = 'Allows a user to view the manifest list page in web UI' where permission_code = 'VMFST';
update permission set description = 'Allows a user to view the COD reports in web UI' where permission_code = 'VCODRPT';
update permission set description = 'Allows a user to view the hazmat reports in web UI' where permission_code = 'VHAZRPT';
update permission set description = 'Allows a user to view schedules created in event management ' where permission_code = 'VSCH';
update permission set description = 'Allows a user to view schedule template group created in event management ' where permission_code = 'VSCHTG';
update permission set description = 'Allows a user to view the event notifications' where permission_code = 'VENOT';
update permission set description = 'Allows a user to view the schedule template group rules' where permission_code = 'VSTR';
update permission set description = 'Allows a user to view the schedule templates' where permission_code = 'VSCHTEMPL';
update permission set description = 'Allows a user to add and edit event definitions' where permission_code = 'AEDEF';
update permission set description = 'Allows a user to view schedule groups' where permission_code = 'VSCHG';
update permission set description = 'Allows a user to add and edit schedule templates' where permission_code = 'ASCHTEMPL';
update permission set description = 'Allows a user to add and edit schedule template group rules' where permission_code = 'ASTR';
update permission set description = 'Allows a user to view the events occurrence' where permission_code = 'VEVTO';
update permission set description = 'Allows a user to view the event definitions' where permission_code = 'VEDEF';
update permission set description = 'Allows a user to add and edit event schedules ' where permission_code = 'ASCH';
update permission set description = 'Allows a user to add and edit schedule template groups' where permission_code = 'ASCHTG';
update permission set description = 'Allows a user to add, edit and delete event occurences' where permission_code = 'AEVT';
update permission set description = 'Allows a user to delete schedule template group' where permission_code = 'DSCHTG';
update permission set description = 'Allows a user to delete schedule groups' where permission_code = 'DSCHG';
update permission set description = 'Allows a user to add and edit schedule groups' where permission_code = 'ASCHG';
update permission set description = 'Allows a user to acknowledge the event notifications' where permission_code = 'ACKEN';
update permission set description = 'Allows a user to view the events. Governs the display of Events link in menu.' where permission_code = 'VEVT';
update permission set description = 'Allows a user to adjust inventory in ios/android mobile ' where permission_code = 'EINV';
update permission set description = 'Allows a user to view inventory page in mobile' where permission_code = 'VINV';
update permission set description = 'Allows a user to view inventory sync template' where permission_code = 'DOM_VISYT'; 
update permission set description = 'Allows a user to adjust and create inventory in Win-mobile and web UI' where permission_code = 'CINV';
update permission set description = 'Allows a user to access the Inventory by Facility UI. From the Inventory by Facility UI, the user can navigate to PO/ASN details'where permission_code = 'I_VPI';
update permission set description = 'Allows a user to apply or remove an error code on the Inventory by Facility UI' where permission_code = 'I_AEC';
update permission set description = 'Allows a user to access the On Hand Events UI, to observe the inventory trace' where permission_code = 'I_VOA';
update permission set description = 'Allows a user to access the Inventory Error UI, and search among the existing inventory errors' where permission_code = 'I_VIE';

commit;


-- DBTicket SCEM-1333 REFL

INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VSCH'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VSCHTG'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VENOT'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VSTR'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VSCHTEMPL'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='AEDEF'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VSCHG'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='ASCHTEMPL'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='ASTR'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VEVTO'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VEDEF'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='ASCH'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='ASCHTG'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='AEVT'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='DSCHTG'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='DSCHG'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='ASCHG'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='ACKEN'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));
INSERT INTO COMPANY_TYPE_PERM(COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID) values((SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Carrier')),(SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE ='VEVT'),(SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

COMMIT;

-- DBTicket EEM-6903 REFL

DELETE FROM APP_MOD_PERM WHERE APP_ID IN  ( SELECT APP_ID FROM APP WHERE APP_SHORT_NAME='SIF' ) AND 
PERMISSION_ID IN  ( SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='UCLSA');
    
DELETE FROM COMPANY_TYPE_PERM WHERE  PERMISSION_ID IN  ( SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='UCLSA');
    
DELETE  FROM RELTP_APP_MOD_PERM WHERE  APP_ID IN  ( SELECT APP_ID FROM APP WHERE APP_SHORT_NAME='SIF' ) AND
PERMISSION_ID IN  ( SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='UCLSA');

commit;

-- DBTicket EEM-7066 REFL
--exec sequpdt('RELTP_APP_MOD_PERM', 'ROW_UID', 'SEQ_RELTP_APP_MOD_PERM');

MERGE INTO PERMISSION L
     USING (SELECT 'SPDOPACKING' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'ScanPack DO Packing',
               1,
               -1,
               'SPDOPACKING');


MERGE INTO APP_MOD_PERM AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM APP, MODULE, PERMISSION
             WHERE     APP.APP_ID = 40
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'SPDOPACKING') APP1
        ON (    AMP.APP_ID = APP1.APP_ID
            AND AMP.MODULE_ID = APP1.MODULE_ID
            AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE APP_ID = 40),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE Permission_Code = 'SPDOPACKING'),
               SEQ_APP_MOD_PERM.NEXTVAL);

MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT (SELECT COMPANY_TYPE_ID
                      FROM COMPANY_TYPE
                     WHERE LOWER (DESCRIPTION) = LOWER ('Shipper'))
                      AS COMPANY_TYPE_ID,
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'SPDOPACKING')
                      PERMISSION_ID
              FROM DUAL) C
        ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID
            AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
       VALUES ( (SELECT COMPANY_TYPE_ID
                   FROM COMPANY_TYPE
                  WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'SPDOPACKING'),
               (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM));

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_ID = 40
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'SPDOPACKING') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RELATIONSHIP_TYPE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               IS_PERM_IMPLIED)
       VALUES (4,
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_ID = 40),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'SPDOPACKING'),
               (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
               0);

COMMIT;

-- DBTicket DB-1734 REFL
exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Acknowledge Document', 1, 'ACKNOWLEDGEDOC');

INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Acknowledge Message', 1, 'ACKNOWLEDGEMSG');
	 
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'ACKNOWLEDGEDOC'),
             SEQ_APP_MOD_PERM.nextval);
			 
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'ACKNOWLEDGEMSG'),
             SEQ_APP_MOD_PERM.nextval);

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACKNOWLEDGEDOC'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
			 
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACKNOWLEDGEMSG'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACKNOWLEDGEDOC'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ACKNOWLEDGEMSG'),
            (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);

commit;             

-- DBTicket DB-1901 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Document Acknowledgement Configuration', 1, 'DOCACKCONFIG');

INSERT INTO PERMISSION (PERMISSION_ID, PERMISSION_NAME, IS_ACTIVE, PERMISSION_CODE)
     VALUES ( SEQ_PERMISSION_ID.NEXTVAL, 'Message Acknowledgement Configuration', 1, 'MSGACKCONFIG');
	 
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'DOCACKCONFIG'),
             SEQ_APP_MOD_PERM.NEXTVAL);
			 
INSERT INTO APP_MOD_PERM (APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE Permission_Code = 'MSGACKCONFIG'),
             SEQ_APP_MOD_PERM.NEXTVAL);

INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DOCACKCONFIG'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));
			 
INSERT INTO COMPANY_TYPE_PERM (COMPANY_TYPE_ID, PERMISSION_ID, ROW_UID)
     VALUES ( (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MSGACKCONFIG'),
             (SELECT MAX (ROW_UID) + 1 FROM COMPANY_TYPE_PERM));

INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DOCACKCONFIG'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
INSERT INTO RELTP_APP_MOD_PERM (RELATIONSHIP_TYPE_ID, APP_ID, MODULE_ID, PERMISSION_ID, ROW_UID, IS_PERM_IMPLIED)
     VALUES (4,
             (SELECT APP_ID FROM APP WHERE APP_ID = 40),
             (SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM'),
             (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'MSGACKCONFIG'),
             (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
             0);
             
commit;

-- DBTicket DB-2405 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

------HMRECV------
merge into PERMISSION X
	using (select 'HMRECV' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Receive' PERMISSION_NAME,'Hub Mobile Receive' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION)
;


MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'HMRECV') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );
               

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMRECV') PERMISSION_ID,((select max(ROW_UID) from COMPANY_TYPE_PERM)+4) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);

------HMTRANSFERASN------
merge into PERMISSION X
	using (select 'HMTRANSFERASN' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Transfer ASN' PERMISSION_NAME,'Hub Mobile Transfer ASN' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION)
;

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'HMTRANSFERASN') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );
               


merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMTRANSFERASN') PERMISSION_ID,((select max(ROW_UID) from COMPANY_TYPE_PERM)+4) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);

------HMTRANSFERLPN------
merge into PERMISSION X
	using (select 'HMTRANSFERLPN' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Transfer LPN' PERMISSION_NAME,'Hub Mobile Transfer LPN' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION)
;

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'HMTRANSFERLPN') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMTRANSFERLPN') PERMISSION_ID,((select max(ROW_UID) from COMPANY_TYPE_PERM)+4) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);			 

------HMREMOVELPN------
merge into PERMISSION X
	using (select 'HMREMOVELPN' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Remove LPN' PERMISSION_NAME,'Hub Mobile Remove LPN' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION)
;

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'HMREMOVELPN') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );
               

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMREMOVELPN') PERMISSION_ID,((select max(ROW_UID) from COMPANY_TYPE_PERM)+4) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);

------HMSTOPLPNASSGN------
merge into PERMISSION X
	using (select 'HMSTOPLPNASSGN' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Stop LPN Assignment' PERMISSION_NAME,'Hub Mobile Stop LPN Assignment' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION)
;

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'HMSTOPLPNASSGN') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );
               

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMSTOPLPNASSGN') PERMISSION_ID,((select max(ROW_UID) from COMPANY_TYPE_PERM)+4) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);

------HMSHIPASN------
merge into PERMISSION X
	using (select 'HMSHIPASN' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Ship LPN' PERMISSION_NAME,'Hub Mobile Ship LPN' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION)
;

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'HMSHIPASN') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );
               
merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMSHIPASN') PERMISSION_ID,((select max(ROW_UID) from COMPANY_TYPE_PERM)+4) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);	


commit;


-- DBTicket DB-2878 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

------  HMLPNINQ  ------------
merge into PERMISSION X
	using (select 'HMLPNINQ' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile LPN Inquiry' PERMISSION_NAME,'Hub Mobile LPN Inquiry' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);


MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'HMLPNINQ') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );


merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMLPNINQ') PERMISSION_ID,((select max(ROW_UID) from COMPANY_TYPE_PERM)+4) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);

commit;

-- DBTicket DB-3082 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');
--exec sequpdt('RELTP_APP_MOD_PERM','ROW_UID','SEQ_RELTP_APP_MOD_PERM');

MERGE INTO PERMISSION L
     USING (SELECT 'EDITVENDORDOC' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Edit Vendor Document',
               1,
               -1,
               'EDITVENDORDOC');
               
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'EDITVENDORDOC') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );


MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EDITVENDORDOC') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EDITVENDORDOC'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    ); 
 
 

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_SHORT_NAME = 'eem'
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'EDITVENDORDOC') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RELATIONSHIP_TYPE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               IS_PERM_IMPLIED)
       VALUES (4,
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'eem'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'EDITVENDORDOC'),
               (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
               0);  
 
 

MERGE INTO PERMISSION L
     USING (SELECT 'ADDVENDORDOC' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Add Vendor Document',
               1,
               -1,
               'ADDVENDORDOC');
               
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'ADDVENDORDOC') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );


MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADDVENDORDOC') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADDVENDORDOC'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    ); 
 
 

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_SHORT_NAME = 'eem'
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'ADDVENDORDOC') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RELATIONSHIP_TYPE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               IS_PERM_IMPLIED)
       VALUES (4,
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'eem'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADDVENDORDOC'),
               (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
               0);  
 
  
 
 
MERGE INTO PERMISSION L
     USING (SELECT 'DELETEVENDORDOC' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Delete Vendor Document',
               1,
               -1,
               'DELETEVENDORDOC');
               
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'DELETEVENDORDOC') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );


MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DELETEVENDORDOC') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DELETEVENDORDOC'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    ); 
 
 

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_SHORT_NAME = 'eem'
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'DELETEVENDORDOC') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RELATIONSHIP_TYPE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               IS_PERM_IMPLIED)
       VALUES (4,
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'eem'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'DELETEVENDORDOC'),
               (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
               0);  
 
  
 commit;
 
-- DBTicket DB-3106 REFL

exec sequpdt('PERMISSION','PERMISSION_ID','SEQ_PERMISSION_ID');
exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

merge into PERMISSION X
	using (select 'HMVERIFYASN' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Verify ASN' PERMISSION_NAME,'Hub Mobile Verify ASN' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION)
;

merge into APP_MOD_PERM X
	using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select max(PERMISSION_ID) from PERMISSION where PERMISSION_CODE = 'HMVERIFYASN') PERMISSION_ID,((select max(ROW_UID) from APP_MOD_PERM)+4) ROW_UID from DUAL) B
	on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
	insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
	values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID, B.ROW_UID)
;

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMVERIFYASN') PERMISSION_ID,((select max(ROW_UID) from COMPANY_TYPE_PERM)+4) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);

commit; 
 
 
-- DBTicket DB-3139 REFL

MERGE INTO PERMISSION L
     USING (SELECT 'EDITVENDORMSG' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Edit Vendor Message',
               1,
               -1,
               'EDITVENDORMSG');
			   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'EDITVENDORMSG') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );


MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EDITVENDORMSG') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EDITVENDORMSG'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    ); 
 
 

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_SHORT_NAME = 'eem'
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'EDITVENDORMSG') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RELATIONSHIP_TYPE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               IS_PERM_IMPLIED)
       VALUES (4,
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'eem'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'EDITVENDORMSG'),
               (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
               0);  
 
 

MERGE INTO PERMISSION L
     USING (SELECT 'ADDVENDORMSG' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Add Vendor Message',
               1,
               -1,
               'ADDVENDORMSG');
			   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'ADDVENDORMSG') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );


MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADDVENDORMSG') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ADDVENDORMSG'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    ); 
 
 

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_SHORT_NAME = 'eem'
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'ADDVENDORMSG') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RELATIONSHIP_TYPE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               IS_PERM_IMPLIED)
       VALUES (4,
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'eem'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'ADDVENDORMSG'),
               (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
               0);  
 
MERGE INTO PERMISSION L
     USING (SELECT 'DELETEVENDORMSG' PERMISSION_CODE FROM DUAL) B
        ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (L.PERMISSION_ID,
               L.PERMISSION_NAME,
               L.IS_ACTIVE,
               L.COMPANY_ID,
               L.PERMISSION_CODE)
       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
               'Delete Vendor Message',
               1,
               -1,
               'DELETEVENDORMSG');
			   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'DELETEVENDORMSG') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );


MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DELETEVENDORMSG') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'DELETEVENDORMSG'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM)
    ); 
 
 

MERGE INTO RELTP_APP_MOD_PERM RAMP
     USING (SELECT RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID,
                   APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE,
                   PERMISSION.PERMISSION_ID,
                   PERMISSION.PERMISSION_CODE
              FROM RELATIONSHIP_TYPE,
                   APP,
                   MODULE,
                   PERMISSION
             WHERE     RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_ID = 4
                   AND APP.APP_SHORT_NAME = 'eem'
                   AND MODULE.MODULE_CODE = 'EEM'
                   AND PERMISSION.PERMISSION_CODE = 'DELETEVENDORMSG') APP1
        ON (    RAMP.RELATIONSHIP_TYPE_ID = APP1.RELATIONSHIP_TYPE_ID
            AND RAMP.APP_ID = APP1.APP_ID
            AND RAMP.MODULE_ID = APP1.MODULE_ID
            AND RAMP.PERMISSION_ID = APP1.PERMISSION_ID)
WHEN NOT MATCHED
THEN
   INSERT     (RELATIONSHIP_TYPE_ID,
               APP_ID,
               MODULE_ID,
               PERMISSION_ID,
               ROW_UID,
               IS_PERM_IMPLIED)
       VALUES (4,
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'eem'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE MODULE_CODE = 'EEM'),
               (SELECT PERMISSION_ID
                  FROM PERMISSION
                 WHERE PERMISSION_CODE = 'DELETEVENDORMSG'),
               (SELECT MAX(ROW_UID)+4 from RELTP_APP_MOD_PERM),
               0);  
 
commit;   
 
-- DBTicket DB-4796 REFL

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE INTO APP_MOD_PERM AMP
         USING (SELECT APP.APP_ID,
                       APP.APP_NAME,
                       MODULE.MODULE_ID,
                       MODULE.MODULE_CODE,
                       PERMISSION.PERMISSION_ID,
                       PERMISSION.PERMISSION_CODE
                  FROM APP, MODULE, PERMISSION
                 WHERE     APP.APP_SHORT_NAME = 'eem'
                      AND MODULE.module_name='Common Business Object - Transaction'
                      AND PERMISSION.permission_name='Extended Enterprise TAB') APP1
           ON (    AMP.APP_ID = APP1.APP_ID
               AND AMP.MODULE_ID = APP1.MODULE_ID
               AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
   WHEN NOT MATCHED
   THEN
      INSERT     (APP_ID,
                  MODULE_ID,
                  PERMISSION_ID,
                  ROW_UID)
          VALUES ( (select app_id from app where app_name='Extended Enterprise Management'),
                  (select module_id from module where module_name='Common Business Object - Transaction'),
                  (select permission_id from permission where permission_name='Extended Enterprise TAB'),
                  SEQ_APP_MOD_PERM.NEXTVAL);

COMMIT;

-- DBTicket DB-4948 REFL

exec sequpdt('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

merge into PERMISSION X
	using (select 'HMLOADTRL' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Load Trailer' PERMISSION_NAME,'Hub Mobile Load Trailer' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

merge into APP_MOD_PERM X
	using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select PERMISSION_ID from PERMISSION where PERMISSION_CODE = 'HMLOADTRL') PERMISSION_ID from DUAL) B
	on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
	insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
	values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID,SEQ_APP_MOD_PERM.NEXTVAL);

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMLOADTRL') PERMISSION_ID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,(select max(ROW_UID)+4 from COMPANY_TYPE_PERM));

commit;

-- DBTicket DB-5119

MERGE INTO SYS_CODE_TYPE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE FROM DUAL) B ON (A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE) WHEN NOT MATCHED THEN INSERT(REC_TYPE, CODE_TYPE, CODE_DESC, SHORT_DESC, MAX_CODE_ID_LEN, ALLOW_ADD, ALLOW_DEL, CHG_MISC_FLAG, CHG_DTL_DESC, WHSE_DEPNDT, CREATE_DATE_TIME, MOD_DATE_TIME, USER_ID, WM_VERSION_ID, SYS_CODE_TYPE_ID, ORDER_BY) VALUES ('S', 'E04', 'Hub Ship Attributes', 'HubShip', '15', 'Y', 'Y','Y', 'Y', '0', SYSDATE, SYSDATE, 'SEED', 1, SYS_CODE_TYPE_ID_SEQ.NEXTVAL, null);
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'BOL' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'BOL', 'Bill of lading', 'BOL','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'PRO' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'PRO', 'PRO number', 'PRO','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'SEAL' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'SEAL', 'Seal number', 'SEAL','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'ESTDELIVERYDTTM' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'ESTDELIVERYDTTM', 'Estimated delivery date', 'ESTDELDTTM','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD1' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD1', 'Reference field 1', 'REF1','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD2' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD2', 'Reference field 2', 'REF2','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD3' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD3', 'Reference field 3', 'REF3','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD4' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD4', 'Reference field 4', 'REF4','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD5' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD5', 'Reference field 5', 'REF5','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD6' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD6', 'Reference field 6', 'REF6','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD7' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD7', 'Reference field 7', 'REF7','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD8' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD8', 'Reference field 8', 'REF8','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD9' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD9', 'Reference field 9', 'REF9','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFFIELD10' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFFIELD10', 'Reference field 10', 'REF10','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFNUMFIELD1' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFNUMFIELD1', 'Reference number field 1', 'REFN1','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFNUMFIELD2' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFNUMFIELD2', 'Reference number field 2', 'REFN2','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFNUMFIELD3' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFNUMFIELD3', 'Reference number field 3', 'REFN3','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFNUMFIELD4' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFNUMFIELD4', 'Reference number field 4', 'REFN4','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE, 'REFNUMFIELD5' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E04', 'REFNUMFIELD5', 'Reference number field 5', 'REFN5','YN',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'));
MERGE INTO SYS_CODE_PARM SCP USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE , '1' FROM_POSN FROM DUAL) D ON ( SCP.REC_TYPE = D.REC_TYPE AND SCP.CODE_TYPE = D.CODE_TYPE  AND SCP.FROM_POSN = D.FROM_POSN) WHEN NOT MATCHED THEN INSERT (REC_TYPE, CODE_TYPE, FROM_POSN, TO_POSN, MISC_FLAG_DESC, LITRL, MANDT_FLAG, VALID_CODE, VALID_SYS_CODE_REC_TYPE, VALID_SYS_CODE_TYPE, VALID_FROM, VALID_TO, VALID_VALUES, EDIT_STYLE, CREATE_DATE_TIME, MOD_DATE_TIME, USER_ID, WM_VERSION_ID,SYS_CODE_PARM_ID, SYS_CODE_TYPE_ID, VALID_SYS_CODE_TYPE_ID)VALUES ('S', 'E04', '1', '1', 'Enable attribute on Hub Mobile Ship screen', 'Enable attribute', 'N', 'F', '', '','','',null,'CKBX', SYSDATE, SYSDATE, 'SEED', 1, SYS_CODE_PARM_ID_SEQ.NEXTVAL, (SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'),null);
MERGE INTO SYS_CODE_PARM SCP USING (SELECT 'S' REC_TYPE, 'E04' CODE_TYPE , '2' FROM_POSN FROM DUAL) D ON ( SCP.REC_TYPE = D.REC_TYPE AND SCP.CODE_TYPE = D.CODE_TYPE  AND SCP.FROM_POSN = D.FROM_POSN) WHEN NOT MATCHED THEN INSERT (REC_TYPE, CODE_TYPE, FROM_POSN, TO_POSN, MISC_FLAG_DESC, LITRL, MANDT_FLAG, VALID_CODE, VALID_SYS_CODE_REC_TYPE, VALID_SYS_CODE_TYPE, VALID_FROM, VALID_TO, VALID_VALUES, EDIT_STYLE, CREATE_DATE_TIME, MOD_DATE_TIME, USER_ID, WM_VERSION_ID,SYS_CODE_PARM_ID, SYS_CODE_TYPE_ID, VALID_SYS_CODE_TYPE_ID)VALUES ('S', 'E04', '2', '2', 'Required field', 'Required field', 'N', 'F', '', '','','',null,'CKBX', SYSDATE, SYSDATE, 'SEED', 1, SYS_CODE_PARM_ID_SEQ.NEXTVAL, (SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E04'),null);

commit;

-- DBTicket DB-5219 REFL

merge into PERMISSION X
	using (select 'HMSHIPTRL' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Ship Trailer' PERMISSION_NAME,'Hub Mobile Ship Trailer' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

merge into APP_MOD_PERM X
	using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select max(PERMISSION_ID) from PERMISSION where PERMISSION_CODE = 'HMSHIPTRL') PERMISSION_ID,((select max(ROW_UID) from APP_MOD_PERM)+4) ROW_UID from DUAL) B
	on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
	insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
	values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID, B.ROW_UID);

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMSHIPTRL') PERMISSION_ID,((select max(ROW_UID) from COMPANY_TYPE_PERM)+4) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);

COMMIT;

-- DBTicket DB-5361 REFL

MERGE into PERMISSION X
               using (select 'VSTGLOC' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'View Staging Location' PERMISSION_NAME,
                              'This allows a user to view the Staging Location on EEM' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
    insert (PERMISSION_ID,
   PERMISSION_NAME,
   IS_ACTIVE,
   COMPANY_ID,
   PERMISSION_CODE,
   CREATED_SOURCE_TYPE_ID,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE_ID,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   DESCRIPTION 
)
VALUES
(SEQ_PERMISSION_ID.NEXTVAL,
   'View Staging Location',
   1,
   -1,
   'VSTGLOC',
   3,
   'Manhattan Associates',
   SYSDATE,
   3,
   'Manhattan Associates',
   SYSDATE,
   'This allows a user to view the Staging Location on EEM');  
			   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_name='Extended Enterprise Management'
              AND MODULE.module_name='EEM'
              AND PERMISSION.PERMISSION_CODE = 'VSTGLOC') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);   

			   
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_NAME = 'Extended Enterprise Management') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE module_name='EEM') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSTGLOC') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (C.RELATIONSHIP_TYPE_ID,C.APP_ID,C.MODULE_ID,C.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
    0);


MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSTGLOC') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'VSTGLOC'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	 
	 
MERGE into PERMISSION X
               using (select 'ASTGLOC' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'Administer Staging Location' PERMISSION_NAME,
                              'This allows a user to administer the Staging Location on EEM' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
    insert (PERMISSION_ID,
   PERMISSION_NAME,
   IS_ACTIVE,
   COMPANY_ID,
   PERMISSION_CODE,
   CREATED_SOURCE_TYPE_ID,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE_ID,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   DESCRIPTION 
)
VALUES
(SEQ_PERMISSION_ID.NEXTVAL,
   'Administer Staging Location',
   1,
   -1,
   'ASTGLOC',
   3,
   'Manhattan Associates',
   SYSDATE,
   3,
   'Manhattan Associates',
   SYSDATE,
   'This allows a user to administer the Staging Location on EEM'
);


MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_name='Extended Enterprise Management'
              AND MODULE.module_name='EEM'
              AND PERMISSION.PERMISSION_CODE = 'ASTGLOC') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);  



MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_NAME ='Extended Enterprise Management') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE module_name='EEM') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ASTGLOC') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (C.RELATIONSHIP_TYPE_ID,C.APP_ID,C.MODULE_ID,C.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
    0);

MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ASTGLOC') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'ASTGLOC'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));

commit;

-- DBTicket DB-5559 REFL

MERGE into PERMISSION X
               using (select 'EEMVROUTE' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'EEM View Routes' PERMISSION_NAME,
                              'This allows a user to view routes on EEM' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
    insert (PERMISSION_ID,
   PERMISSION_NAME,
   IS_ACTIVE,
   COMPANY_ID,
   PERMISSION_CODE,
   CREATED_SOURCE_TYPE_ID,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE_ID,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   DESCRIPTION 
)
VALUES
(SEQ_PERMISSION_ID.NEXTVAL,
   'EEM View Routes',
   1,
   -1,
   'EEMVROUTE',
   3,
   'Manhattan Associates',
   SYSDATE,
   3,
   'Manhattan Associates',
   SYSDATE,
   'This allows a user to view routes on EEM');
			   

MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_name='Extended Enterprise Management'
              AND MODULE.module_name='EEM'
              AND PERMISSION.PERMISSION_CODE = 'EEMVROUTE') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval); 

			   
MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_NAME = 'Extended Enterprise Management') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE module_name='EEM') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EEMVROUTE') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (C.RELATIONSHIP_TYPE_ID,C.APP_ID,C.MODULE_ID,C.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
    0);


MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EEMVROUTE') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EEMVROUTE'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));
	 

MERGE into PERMISSION X
               using (select 'EEMAROUTE' PERMISSION_CODE,
                              1 IS_ACTIVE,
                              'EEM Administer Routes' PERMISSION_NAME,
                              'This allows a user to administer Routes on EEM' DESCRIPTION
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
    insert (PERMISSION_ID,
   PERMISSION_NAME,
   IS_ACTIVE,
   COMPANY_ID,
   PERMISSION_CODE,
   CREATED_SOURCE_TYPE_ID,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE_ID,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   DESCRIPTION 
)
VALUES
(SEQ_PERMISSION_ID.NEXTVAL,
   'EEM Administer Routes',
   1,
   -1,
   'EEMAROUTE',
   3,
   'Manhattan Associates',
   SYSDATE,
   3,
   'Manhattan Associates',
   SYSDATE,
   'This allows a user to administer Routes on EEM'
);


MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_name='Extended Enterprise Management'
              AND MODULE.module_name='EEM'
              AND PERMISSION.PERMISSION_CODE = 'EEMAROUTE') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);  



MERGE INTO RELTP_APP_MOD_PERM RAMP USING
(SELECT
  (SELECT RELATIONSHIP_TYPE_ID FROM RELATIONSHIP_TYPE WHERE DESCRIPTION = 'Business Partner') AS RELATIONSHIP_TYPE_ID,
  (SELECT APP_ID FROM APP WHERE APP_NAME ='Extended Enterprise Management') AS APP_ID,
  (SELECT MODULE_ID FROM MODULE WHERE module_name='EEM') AS MODULE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EEMAROUTE') AS PERMISSION_ID
FROM DUAL
) C ON (RAMP.RELATIONSHIP_TYPE_ID = C.RELATIONSHIP_TYPE_ID AND RAMP.APP_ID = C.APP_ID AND RAMP.MODULE_ID = C.MODULE_ID AND RAMP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      RELATIONSHIP_TYPE_ID,
      APP_ID,
      MODULE_ID,
      PERMISSION_ID,
      ROW_UID,
      IS_PERM_IMPLIED
    )
    VALUES
    (C.RELATIONSHIP_TYPE_ID,C.APP_ID,C.MODULE_ID,C.PERMISSION_ID,
      (SELECT MAX(ROW_UID)+4 FROM RELTP_APP_MOD_PERM),
    0);

MERGE INTO COMPANY_TYPE_PERM CTP USING
(SELECT
  (SELECT COMPANY_TYPE_ID FROM COMPANY_TYPE WHERE LOWER(DESCRIPTION) = LOWER('Shipper')) AS COMPANY_TYPE_ID,
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EEMAROUTE') PERMISSION_ID
FROM DUAL
) C ON (CTP.COMPANY_TYPE_ID = C.COMPANY_TYPE_ID AND CTP.PERMISSION_ID = C.PERMISSION_ID)
WHEN NOT MATCHED THEN
  INSERT
    (
      COMPANY_TYPE_ID,
      PERMISSION_ID,
      ROW_UID
    )
    VALUES
    (
      (SELECT COMPANY_TYPE_ID
        FROM COMPANY_TYPE
        WHERE LOWER(DESCRIPTION) = LOWER('Shipper')),
      (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EEMAROUTE'),
      (SELECT MAX(ROW_UID)+4 FROM COMPANY_TYPE_PERM));


MERGE INTO PERMISSION_INHERITANCE P1 USING
(SELECT
  (SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EEMAROUTE'
  ) AS PARENT_PERMISSION_ID,
  ( SELECT PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE = 'EEMVROUTE'
  ) AS CHILD_PERMISSION_ID
FROM dual
) P2 ON ( P1.PARENT_PERMISSION_ID = P2.PARENT_PERMISSION_ID AND P1.CHILD_PERMISSION_ID = P2.CHILD_PERMISSION_ID )
WHEN NOT matched THEN
  INSERT
    (
      PARENT_PERMISSION_ID,
      CHILD_PERMISSION_ID
    )
    VALUES
    (
      P2.PARENT_PERMISSION_ID ,
      P2.CHILD_PERMISSION_ID
    );
	  
COMMIT;

-- DBTicket DB-5731 REFL

delete from COMPANY_TYPE_PERM where permission_id  in (select permission_id from PERMISSION where PERMISSION_CODE IN ('HMSTOPLPNASSGN','HMTRANSFERASN','HMTRANSFERLPN','HMREMOVELPN','HMSHIPASN'));

delete from APP_MOD_PERM where permission_id  in (select permission_id from PERMISSION where PERMISSION_CODE IN ('HMSTOPLPNASSGN','HMTRANSFERASN','HMTRANSFERLPN','HMREMOVELPN','HMSHIPASN'));

---- delete from PERMISSION where PERMISSION_CODE IN ('HMSTOPLPNASSGN','HMTRANSFERASN','HMTRANSFERLPN','HMREMOVELPN','HMSHIPASN');

COMMIT;

-- DBTicket DB-5773 REFL

MERGE INTO APP_MODULE AMP
     USING (SELECT APP.APP_ID,
                   APP.APP_NAME,
                   MODULE.MODULE_ID,
                   MODULE.MODULE_CODE
              FROM APP, MODULE
             WHERE APP.app_short_name='eem'
                   AND MODULE.module_name='Manhattan System Management') APP1
        ON (AMP.APP_ID = APP1.APP_ID AND AMP.MODULE_ID = APP1.MODULE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (APP_ID,
               MODULE_ID,
               IS_ACTIVE,
               ROW_UID)
       VALUES ( (SELECT APP_ID
                   FROM APP
                  WHERE app_short_name='eem'),
               (SELECT MODULE_ID
                  FROM MODULE
                 WHERE module_name='Manhattan System Management'),
               1,
               (SELECT MAX (ROW_UID) + 4 FROM APP_MODULE));


MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.app_short_name='eem'
              AND MODULE.module_name='Manhattan System Management'
              AND PERMISSION.PERMISSION_CODE = 'POSTMSG') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );
COMMIT;

-- DBTicket DB-6026 REFL

EXEC SEQUPDT('APP_MOD_PERM','ROW_UID','SEQ_APP_MOD_PERM');

MERGE into PERMISSION X
               using (select 'RASNFSRPT' PERMISSION_CODE,    
                              'First Sale ASN Report' PERMISSION_NAME
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
    insert (PERMISSION_ID,
   PERMISSION_NAME,
   IS_ACTIVE,
   COMPANY_ID,
   PERMISSION_CODE,
   CREATED_SOURCE_TYPE_ID,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE_ID,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   DESCRIPTION 
)
VALUES
(SEQ_PERMISSION_ID.NEXTVAL,
   'First Sale ASN Report',
   1,
   -1,
   'RASNFSRPT',
   3,
   'Manhattan Associates',
   SYSDATE,
   3,
   'Manhattan Associates',
   SYSDATE,
   'Allows a user to view and run the First Sale ASN Report');
   
   
   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
            WHERE APP.app_name='Extended Enterprise Management'
              AND MODULE.module_name='Common Business Object - Transaction'
              AND PERMISSION.PERMISSION_CODE = 'RASNFSRPT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);


MERGE into PERMISSION X
               using (select 'REEBOLRPT' PERMISSION_CODE,    
                              'EEM BOL Report' PERMISSION_NAME
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
    insert (PERMISSION_ID,
   PERMISSION_NAME,
   IS_ACTIVE,
   COMPANY_ID,
   PERMISSION_CODE,
   CREATED_SOURCE_TYPE_ID,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE_ID,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   DESCRIPTION 
)
VALUES
(SEQ_PERMISSION_ID.NEXTVAL,
   'EEM BOL Report',
   1,
   -1,
   'REEBOLRPT',
   3,
   'Manhattan Associates',
   SYSDATE,
   3,
   'Manhattan Associates',
   SYSDATE,
   'Allows a user to view and run the EEM BOL Report');
   
   
   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
            WHERE APP.app_name='Extended Enterprise Management'
              AND MODULE.module_name='Common Business Object - Transaction'
              AND PERMISSION.PERMISSION_CODE = 'REEBOLRPT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);
 
MERGE into PERMISSION X
               using (select 'RSHIPMANRPT' PERMISSION_CODE,    
                              'Shipping Manifest Report' PERMISSION_NAME
                              from DUAL) B
               on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
    insert (PERMISSION_ID,
   PERMISSION_NAME,
   IS_ACTIVE,
   COMPANY_ID,
   PERMISSION_CODE,
   CREATED_SOURCE_TYPE_ID,
   CREATED_SOURCE,
   CREATED_DTTM,
   LAST_UPDATED_SOURCE_TYPE_ID,
   LAST_UPDATED_SOURCE,
   LAST_UPDATED_DTTM,
   DESCRIPTION 
)
VALUES
(SEQ_PERMISSION_ID.NEXTVAL,
   'Shipping Manifest Report',
   1,
   -1,
   'RSHIPMANRPT',
   3,
   'Manhattan Associates',
   SYSDATE,
   3,
   'Manhattan Associates',
   SYSDATE,
   'Allows a user to view and run the Shipping Manifest Report');
   
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
            FROM APP, MODULE, PERMISSION
            WHERE APP.app_name='Extended Enterprise Management'
              AND MODULE.module_name='Common Business Object - Transaction'
              AND PERMISSION.PERMISSION_CODE = 'RSHIPMANRPT') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval);

COMMIT;

-- DBTicket DB-6027 REFL

MERGE INTO APP_MOD_PERM amp USING
(SELECT (select APP_ID from APP where App_Name ='Extended Enterprise Management') APP_ID ,
  (select MODULE_ID from MODULE  where Module_Name = 'Supply Chain Process Platform') MODULE_ID ,
  (select PERMISSION_ID from PERMISSION where PERMISSION_CODE='POSTMSG') PERMISSION_ID
FROM DUAL
) B ON (amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.APP_ID,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_APP_MOD_PERM.NEXTVAL,
    SYSDATE,
    SYSDATE
  );


Declare
VCOUNT number(9);
begin
select count(*) into VCOUNT
FROM ROLE_APP_MOD_PERM ramp,
  permission perm
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('EDEVT','VMIF')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Supply Chain Process Platform'
  )
AND perm.PERMISSION_ID = (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='POSTMSG');

if VCOUNT>0
then
FOR i in (select 
DISTINCT role_id,
  app_id,
  MODULE_ID
FROM ROLE_APP_MOD_PERM ramp
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  from PERMISSION
  WHERE PERMISSION_CODE IN ('EDEVT','VMIF'))
and RAMP.APP_ID in
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management')
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Supply Chain Process Platform'
  ))
LOOP
merge into ROLE_APP_MOD_PERM AMP using
(SELECT i.role_id role_id,(select APP_ID from APP where App_Name ='Extended Enterprise Management') APP_ID ,
  (select MODULE_ID from MODULE  where Module_Name = 'Supply Chain Process Platform') MODULE_ID ,
  (select PERMISSION_ID from PERMISSION where PERMISSION_CODE='POSTMSG') PERMISSION_ID
FROM DUAL
) B ON (amp.role_id = B.role_id AND amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.ROLE_ID,
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.ROLE_ID,
    B.APP_ID,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
END LOOP;
end if;
end;
/

commit;

-- DBTicket DB-6028 REFL

MERGE INTO APP_MOD_PERM amp USING
(SELECT (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management') APP_ID ,
 (SELECT Module_Id  FROM Module WHERE Module_Name = ('Common Business Object - Base')) MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ANXTUPNBRB') PERMISSION_ID
FROM DUAL
) B ON (amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.APP_ID,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_APP_MOD_PERM.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
  
  
MERGE INTO APP_MOD_PERM amp USING
(SELECT (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management') APP_ID ,
 (SELECT Module_Id  FROM Module WHERE Module_Name = ('Common Business Object - Transaction')) MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ANXTUPNBRB') PERMISSION_ID
FROM DUAL
) B ON (amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.APP_ID,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_APP_MOD_PERM.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
  

declare
VCOUNT number(9);
begin
select count(*) into VCOUNT
FROM ROLE_APP_MOD_PERM ramp,
  permission perm
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('ANXTUPNBR')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name in ('Common Business Object - Base','Common Business Object - Transaction')
  )
AND perm.PERMISSION_ID = (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ANXTUPNBRB');

if VCOUNT>0
then
FOR i in (select 
DISTINCT role_id,
  app_id,
  MODULE_ID
FROM ROLE_APP_MOD_PERM ramp
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('ANXTUPNBR')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name in ('Common Business Object - Base','Common Business Object - Transaction')
  ))
LOOP
MERGE INTO ROLE_APP_MOD_PERM amp USING
(SELECT 
  i.role_id role_id,
  i.App_id APP_ID ,
  i.module_id MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ANXTUPNBRB') PERMISSION_ID
FROM DUAL
) B ON (amp.role_id = B.role_id AND amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.ROLE_ID,
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.ROLE_ID,
    B.APP_ID,
    B.Module_id,
    B.PERMISSION_ID,
    SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
END LOOP;
else
null;
end if;
end;
/


MERGE INTO APP_MOD_PERM amp USING
(SELECT (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management') APP_ID ,
  (SELECT Module_Id  FROM Module WHERE Module_Name = ('Common Business Object - Base')) MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ASCERB') PERMISSION_ID
FROM DUAL
) B ON (amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.APP_ID,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_APP_MOD_PERM.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
  
  
MERGE INTO APP_MOD_PERM amp USING
(SELECT (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management') APP_ID ,
  (SELECT Module_Id  FROM Module WHERE Module_Name = ('Common Business Object - Transaction')) MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ASCERB') PERMISSION_ID
FROM DUAL
) B ON (amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.APP_ID,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_APP_MOD_PERM.NEXTVAL,
    SYSDATE,
    SYSDATE
  );


declare
VCOUNT number(9);
begin
select count(*) into VCOUNT
FROM ROLE_APP_MOD_PERM ramp,
  permission perm
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('ASCER')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name in ('Common Business Object - Base','Common Business Object - Transaction')
  )
AND perm.PERMISSION_ID = (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ASCERB');

if VCOUNT>0
then
FOR i in (select 
DISTINCT role_id,
  app_id,
  MODULE_ID
FROM ROLE_APP_MOD_PERM ramp
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('ASCER')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name in ('Common Business Object - Base','Common Business Object - Transaction')
  ))
LOOP
MERGE INTO ROLE_APP_MOD_PERM amp USING
(SELECT 
  i.role_id role_id,
  i.App_id APP_ID ,
  i.module_id MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='ASCERB') PERMISSION_ID
FROM DUAL
) B ON (amp.role_id = B.role_id AND amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.ROLE_ID,
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.ROLE_ID,
    B.APP_ID,
    B.Module_id,
    B.PERMISSION_ID,
    SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
END LOOP;
else
null;
end if;
end;
/

commit;

-- DBTicket DB-6331

MERGE INTO SYS_CODE_TYPE A USING (SELECT 'S' REC_TYPE, 'E05' CODE_TYPE FROM DUAL) B ON (A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE) WHEN NOT MATCHED THEN INSERT(REC_TYPE, CODE_TYPE, CODE_DESC, SHORT_DESC, MAX_CODE_ID_LEN, ALLOW_ADD, ALLOW_DEL, CHG_MISC_FLAG, CHG_DTL_DESC, WHSE_DEPNDT, CREATE_DATE_TIME, MOD_DATE_TIME, USER_ID, WM_VERSION_ID, SYS_CODE_TYPE_ID, ORDER_BY) VALUES ('S', 'E05', 'Reconciliation Summary Log Level', 'LogLevel', '15', 'N', 'N','N', 'N', '0', SYSDATE, SYSDATE, 'SEED', 1, SYS_CODE_TYPE_ID_SEQ.NEXTVAL, null);

MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E05' CODE_TYPE, '0' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E05', '0', 'Error', 'Error','',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E05'));

MERGE INTO SYS_CODE A USING (SELECT 'S' REC_TYPE, 'E05' CODE_TYPE, '1' CODE_ID FROM DUAL) B ON ( A.REC_TYPE = B.REC_TYPE AND A.CODE_TYPE = B.CODE_TYPE AND A.CODE_ID = B.CODE_ID) WHEN NOT MATCHED THEN INSERT (A.rec_type, A.code_type, A.code_id, A.code_desc, A.short_desc, A.misc_flags, A.create_date_time, A.mod_date_time, A.user_id, A.wm_version_id, A.sys_code_id, A.sys_code_type_id) values ('S', 'E05', '1', 'Warning', 'Warning','',SYSDATE,SYSDATE,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(SELECT SYS_CODE_TYPE_ID FROM SYS_CODE_TYPE WHERE REC_TYPE = 'S' AND CODE_TYPE = 'E05'));

COMMIT;

-- DBTicket DB-6498 REFL

----This script will automatically add the Post Message permission to any role having the EDEVT or VMIF Permission .  Simply replace the applications and modules with the desired values.


declare
VCOUNT number(9);
begin
select count(*) into VCOUNT
FROM ROLE_APP_MOD_PERM ramp,
  permission perm
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('VSN')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Common Business Object - Transaction'
  )
AND perm.PERMISSION_ID = (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='RASNFSRPT');

if VCOUNT>0
then
FOR i in (select 
DISTINCT role_id,
  app_id,
  MODULE_ID
FROM ROLE_APP_MOD_PERM ramp
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('VSN')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Common Business Object - Transaction'
  ))
LOOP
MERGE INTO ROLE_APP_MOD_PERM amp USING
(SELECT 
  i.role_id role_id,
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management') APP_ID ,
  (SELECT Module_Id FROM Module WHERE Module_Name = 'Common Business Object - Transaction') MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='RASNFSRPT') PERMISSION_ID
FROM DUAL
) B ON (amp.role_id = B.role_id AND amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.ROLE_ID,
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.ROLE_ID,
    B.App_Id,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
END LOOP;
else
null;
end if;
end;
/

-----This script will automatically add the Post Message permission to any role having the EDEVT or VMIF Permission .  Simply replace the applications and modules with the desired values.

declare
VCOUNT number(9);
begin
select count(*) into VCOUNT
FROM ROLE_APP_MOD_PERM ramp,
  permission perm
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('VSN')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Common Business Object - Transaction'
  )
AND perm.PERMISSION_ID = (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='REEBOLRPT');

if VCOUNT>0
then
FOR i in (select 
DISTINCT role_id,
  app_id,
  MODULE_ID
FROM ROLE_APP_MOD_PERM ramp
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('VSN')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Common Business Object - Transaction'
  ))
LOOP
MERGE INTO ROLE_APP_MOD_PERM amp USING
(SELECT 
  i.role_id role_id,
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management') APP_ID ,
  (SELECT Module_Id FROM Module WHERE Module_Name = 'Common Business Object - Transaction') MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='REEBOLRPT') PERMISSION_ID
FROM DUAL
) B ON (amp.role_id = B.role_id AND amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.ROLE_ID,
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.ROLE_ID,
    B.App_Id,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
END LOOP;
else
null;
end if;
end;
/

-----This script will automatically add the Post Message permission to any role having the EDEVT or VMIF Permission .  Simply replace the applications and modules with the desired values.

declare
VCOUNT number(9);
begin
select count(*) into VCOUNT
FROM ROLE_APP_MOD_PERM ramp,
  permission perm
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('VSN')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Common Business Object - Transaction'
  )
AND perm.PERMISSION_ID = (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='RSHIPMANRPT');

if VCOUNT>0
then
FOR i in (select 
DISTINCT role_id,
  app_id,
  MODULE_ID
FROM ROLE_APP_MOD_PERM ramp
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('VSN')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Common Business Object - Transaction'
  ))
LOOP
MERGE INTO ROLE_APP_MOD_PERM amp USING
(SELECT 
  i.role_id role_id,
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management') APP_ID ,
  (SELECT Module_Id FROM Module WHERE Module_Name = 'Common Business Object - Transaction') MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='RSHIPMANRPT') PERMISSION_ID
FROM DUAL
) B ON (amp.role_id = B.role_id AND amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.ROLE_ID,
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.ROLE_ID,
    B.App_Id,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
END LOOP;
else
null;
end if;
end;
/


commit;


-- DBTicket DB-6503 REFL

declare
VCOUNT number(9);
begin
select count(*) into VCOUNT
FROM ROLE_APP_MOD_PERM ramp,
  permission perm
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('EDEVT','VMIF')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Supply Chain Process Platform'
  )
AND perm.PERMISSION_ID = (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='POSTMSG');

if VCOUNT>0
then
FOR i in (select 
DISTINCT role_id,
  app_id,
  MODULE_ID
FROM ROLE_APP_MOD_PERM ramp
WHERE ramp.PERMISSION_ID IN
  (SELECT PERMISSION_ID
  FROM PERMISSION
  WHERE PERMISSION_CODE IN ('EDEVT','VMIF')
  )
AND Ramp.App_Id IN
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management'
  )
AND Ramp.Module_Id IN
  (SELECT Module_Id
  FROM Module
  WHERE Module_Name = 'Supply Chain Process Platform'
  ))
LOOP
MERGE INTO ROLE_APP_MOD_PERM amp USING
(SELECT 
  i.role_id role_id,
  (SELECT App_Id FROM App WHERE App_Name ='Extended Enterprise Management') APP_ID ,
  (SELECT Module_Id FROM Module WHERE Module_Name = 'Supply Chain Process Platform') MODULE_ID ,
  (select P.PERMISSION_ID from PERMISSION P where P.PERMISSION_CODE='POSTMSG') PERMISSION_ID
FROM DUAL
) B ON (amp.role_id = B.role_id AND amp.APP_ID = B.APP_ID AND amp.MODULE_ID = B.MODULE_ID AND amp.permission_id = B.permission_id)
WHEN NOT MATCHED THEN
INSERT
  (
    amp.ROLE_ID,
    amp.APP_ID,
    amp.MODULE_ID,
    amp.PERMISSION_ID,
    amp.ROW_UID,
    amp.CREATED_DTTM,
    amp.LAST_UPDATED_DTTM
  )
  VALUES
  (
    B.ROLE_ID,
    B.APP_ID,
    B.MODULE_ID,
    B.PERMISSION_ID,
    SEQ_ROLEAPPMODPERM_ROWUID.NEXTVAL,
    SYSDATE,
    SYSDATE
  );
END LOOP;
else
null;
end if;
end;
/

commit;

-- DBTicket DB-6616 REFL

MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT (SELECT COMPANY_TYPE_ID
                      FROM COMPANY_TYPE
                     WHERE LOWER (DESCRIPTION) = LOWER ('Shipper'))
                      COMPANY_TYPE_ID,
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'RASNFSRPT')
                      PERMISSION_ID
              FROM DUAL) B
        ON (CTP.PERMISSION_ID = B.PERMISSION_ID
            AND CTP.COMPANY_TYPE_ID = B.COMPANY_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (CTP.COMPANY_TYPE_ID,
               CTP.PERMISSION_ID,
               CTP.ROW_UID,
               CTP.CREATED_DTTM,
               CTP.LAST_UPDATED_DTTM)
       VALUES (B.COMPANY_TYPE_ID,
               B.PERMISSION_ID,
               (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM),
               SYSDATE,
               SYSDATE);

MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT (SELECT COMPANY_TYPE_ID
                      FROM COMPANY_TYPE
                     WHERE LOWER (DESCRIPTION) = LOWER ('Shipper'))
                      COMPANY_TYPE_ID,
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'REEBOLRPT')
                      PERMISSION_ID
              FROM DUAL) B
        ON (CTP.PERMISSION_ID = B.PERMISSION_ID
            AND CTP.COMPANY_TYPE_ID = B.COMPANY_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (CTP.COMPANY_TYPE_ID,
               CTP.PERMISSION_ID,
               CTP.ROW_UID,
               CTP.CREATED_DTTM,
               CTP.LAST_UPDATED_DTTM)
       VALUES (B.COMPANY_TYPE_ID,
               B.PERMISSION_ID,
               (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM),
               SYSDATE,
               SYSDATE);

MERGE INTO COMPANY_TYPE_PERM CTP
     USING (SELECT (SELECT COMPANY_TYPE_ID
                      FROM COMPANY_TYPE
                     WHERE LOWER (DESCRIPTION) = LOWER ('Shipper'))
                      COMPANY_TYPE_ID,
                   (SELECT PERMISSION_ID
                      FROM PERMISSION
                     WHERE PERMISSION_CODE = 'RSHIPMANRPT')
                      PERMISSION_ID
              FROM DUAL) B
        ON (CTP.PERMISSION_ID = B.PERMISSION_ID
            AND CTP.COMPANY_TYPE_ID = B.COMPANY_TYPE_ID)
WHEN NOT MATCHED
THEN
   INSERT     (CTP.COMPANY_TYPE_ID,
               CTP.PERMISSION_ID,
               CTP.ROW_UID,
               CTP.CREATED_DTTM,
               CTP.LAST_UPDATED_DTTM)
       VALUES (B.COMPANY_TYPE_ID,
               B.PERMISSION_ID,
               (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM),
               SYSDATE,
               SYSDATE);

COMMIT;

-- DBTicket DB-6784 REFL

merge into PERMISSION X
	using (select 'HMLOCATE' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Locate LPN' PERMISSION_NAME,'Hub Mobile Locate LPN' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

merge into APP_MOD_PERM X
	using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select PERMISSION_ID from PERMISSION where PERMISSION_CODE = 'HMLOCATE') PERMISSION_ID,((select max(ROW_UID) from APP_MOD_PERM)+4) ROW_UID from DUAL) B
	on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
	insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
	values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID, B.ROW_UID);

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMLOCATE') PERMISSION_ID,((select max(ROW_UID)+4 from COMPANY_TYPE_PERM)) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);
COMMIT;

-- DBTicket DB-7069 REFL

merge into PERMISSION X
	using (select 'HMLOCINQ' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Location Inquiry' PERMISSION_NAME,'Hub Mobile Location Inquiry' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

merge into APP_MOD_PERM X
	using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select PERMISSION_ID from PERMISSION where PERMISSION_CODE = 'HMLOCINQ') PERMISSION_ID,((select max(ROW_UID) from APP_MOD_PERM)+4) ROW_UID from DUAL) B
	on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
	insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
	values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID, B.ROW_UID);

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMLOCINQ') PERMISSION_ID,((select max(ROW_UID)+4 from COMPANY_TYPE_PERM)) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);

-- DBTicket DB-7696 REFL

merge into PERMISSION X
	using (select 'HOLDRELEASE' PERMISSION_CODE,1 IS_ACTIVE, 'Hold and Release LPN' PERMISSION_NAME,'This allows a hub user to hold and release an LPN from hub mobile application or from the UI' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

merge into APP_MOD_PERM X
	using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select PERMISSION_ID from PERMISSION where PERMISSION_CODE = 'HOLDRELEASE') PERMISSION_ID,((select max(ROW_UID) from APP_MOD_PERM)+4) ROW_UID from DUAL) B
	on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
	insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
	values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID, B.ROW_UID);

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HOLDRELEASE') PERMISSION_ID,((select max(ROW_UID)+4 from COMPANY_TYPE_PERM)) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);

merge into PERMISSION X
	using (select 'HOLDRELEASELC' PERMISSION_CODE,1 IS_ACTIVE, 'Hold and Release Location' PERMISSION_NAME,'This allows a hub user to hold and release an LPN from hub mobile application or from the UI' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

merge into APP_MOD_PERM X
	using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select PERMISSION_ID from PERMISSION where PERMISSION_CODE = 'HOLDRELEASELC') PERMISSION_ID,((select max(ROW_UID) from APP_MOD_PERM)+4) ROW_UID from DUAL) B
	on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
	insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
	values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID, B.ROW_UID);

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HOLDRELEASELC') PERMISSION_ID,((select max(ROW_UID)+4 from COMPANY_TYPE_PERM)) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);
COMMIT;

-- DBTicket DB-7779 REFL

merge into PERMISSION X
	using (select 'HMASNINQ' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile ASN Inquiry' PERMISSION_NAME,'Hub Mobile ASN Inquiry' DESCRIPTION from DUAL) B
	on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
	insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
	values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

merge into APP_MOD_PERM X
	using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select PERMISSION_ID from PERMISSION where PERMISSION_CODE = 'HMASNINQ') PERMISSION_ID,((select max(ROW_UID) from APP_MOD_PERM)+4) ROW_UID from DUAL) B
	on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
	insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
	values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID, B.ROW_UID);

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMASNINQ') PERMISSION_ID,((select max(ROW_UID)+4 from COMPANY_TYPE_PERM)) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);
COMMIT;


-- DBTicket DB-7865 REFL

MERGE INTO PERMISSION L USING
(SELECT 'HMPALLETIZE' PERMISSION_CODE FROM DUAL
) B ON (L.PERMISSION_CODE = B.PERMISSION_CODE)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.PERMISSION_ID,
      L.PERMISSION_NAME,
      L.IS_ACTIVE,
      L.COMPANY_ID,
      L.PERMISSION_CODE,
      L.DESCRIPTION
    )
    VALUES
    (
      SEQ_PERMISSION_ID.NEXTVAL,
      'Hub Mobile Palletize LPN''s',
      1,
      -1,
      'HMPALLETIZE',
      ' This allows a hub user to palletize existing LPN''s'
    );
	
MERGE INTO APP_MOD_PERM AMP
    USING (SELECT APP.APP_ID, APP.APP_NAME, MODULE.MODULE_ID,
                  MODULE.MODULE_CODE, PERMISSION.PERMISSION_ID,
                  PERMISSION.PERMISSION_CODE
             FROM APP, MODULE, PERMISSION
            WHERE APP.APP_NAME = 'Extended Enterprise Management'
              AND MODULE.MODULE_CODE = 'EEM'
              AND PERMISSION.PERMISSION_CODE = 'HMPALLETIZE') APP1
    ON (    AMP.APP_ID = APP1.APP_ID
        AND AMP.MODULE_ID = APP1.MODULE_ID
        AND AMP.PERMISSION_ID = APP1.PERMISSION_ID)
    WHEN NOT MATCHED THEN
       INSERT (AMP.APP_ID, AMP.MODULE_ID, AMP.PERMISSION_ID, AMP.ROW_UID)
       VALUES (APP1.APP_ID, APP1.MODULE_ID,APP1.PERMISSION_ID, SEQ_APP_MOD_PERM.nextval
               );
			   
INSERT
INTO COMPANY_TYPE_PERM
  (
    COMPANY_TYPE_ID,
    PERMISSION_ID,
    ROW_UID
  )
  VALUES
  (
    (SELECT COMPANY_TYPE_ID
      FROM COMPANY_TYPE
      WHERE LOWER (DESCRIPTION) = LOWER ('Shipper')
    )
    ,
    (SELECT PERMISSION_ID
    FROM PERMISSION
    WHERE PERMISSION_CODE = 'HMPALLETIZE'
    ),
    (SELECT MAX (ROW_UID) + 4 FROM COMPANY_TYPE_PERM
    )
  );
  
COMMIT;


-- DBTicket DB-7862 REFL

update PERMISSION set PERMISSION_NAME = 'Hub Mobile Hold and Release LPN' where PERMISSION_CODE = 'HOLDRELEASE' ;
update PERMISSION set PERMISSION_NAME = 'Hub Mobile Hold and Release Location' where PERMISSION_CODE = 'HOLDRELEASELC' ;

COMMIT;

-- DBTicket DB-8131 REFL

merge into PERMISSION X
 using (select 'HMINTEGRITYSCAN' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Integrity Scan' PERMISSION_NAME,'Hub Mobile Integrity Scan' DESCRIPTION from DUAL) B
 on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
 insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
 values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

merge into APP_MOD_PERM X
 using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select PERMISSION_ID from PERMISSION where PERMISSION_CODE = 'HMINTEGRITYSCAN') PERMISSION_ID,((select max(ROW_UID) from APP_MOD_PERM)+4) ROW_UID from DUAL) B
 on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
 insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
 values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID, B.ROW_UID);

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMINTEGRITYSCAN') PERMISSION_ID,((select max(ROW_UID)+4 from COMPANY_TYPE_PERM)) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);
COMMIT;

-- DBTicket DB-8257 REFL

merge into PERMISSION X
 using (select 'HMCAPTURECOND' PERMISSION_CODE,1 IS_ACTIVE, 'Hub Mobile Capture Condition' PERMISSION_NAME,'Hub Mobile Capture Condition' DESCRIPTION from DUAL) B
 on (X.PERMISSION_CODE = B.PERMISSION_CODE)
when not matched then
 insert (  PERMISSION_ID,   PERMISSION_CODE,   IS_ACTIVE,   PERMISSION_NAME,   DESCRIPTION)
 values (SEQ_PERMISSION_ID.NEXTVAL, B.PERMISSION_CODE, B.IS_ACTIVE, B.PERMISSION_NAME, B.DESCRIPTION);

merge into APP_MOD_PERM X
 using (select 40 APP_ID,(SELECT MODULE_ID FROM MODULE WHERE MODULE_CODE = 'EEM') MODULE_ID,(select PERMISSION_ID from PERMISSION where PERMISSION_CODE = 'HMCAPTURECOND') PERMISSION_ID,((select max(ROW_UID) from APP_MOD_PERM)+4) ROW_UID from DUAL) B
 on (X.APP_ID = B.APP_ID and X.MODULE_ID = B.MODULE_ID and X.PERMISSION_ID = B.PERMISSION_ID)
when not matched then
 insert (  APP_ID,   MODULE_ID,   PERMISSION_ID,   ROW_UID)
 values (B.APP_ID, B.MODULE_ID, B.PERMISSION_ID, B.ROW_UID);

merge into COMPANY_TYPE_PERM C
using (select 4 COMPANY_TYPE_ID,(select PERMISSION_ID FROM PERMISSION WHERE PERMISSION_CODE='HMCAPTURECOND') PERMISSION_ID,((select max(ROW_UID)+4 from COMPANY_TYPE_PERM)) ROW_UID from DUAL) B
ON ( C.PERMISSION_ID=B.PERMISSION_ID)
WHEN NOT MATCHED THEN
INSERT (COMPANY_TYPE_ID,PERMISSION_ID,ROW_UID)
VALUES (B.COMPANY_TYPE_ID,B.PERMISSION_ID,B.ROW_UID);
COMMIT;
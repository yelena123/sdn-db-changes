#!/bin/ksh

vSetupSchema=$1
vSetupSchemaPswd=$2
vSetupdbName=$3
LOG_DIR=$4
postDir=$5
vParentSchemaName=$6
vParentSchemaPswd=$7
vParentDbName=$8


vrslttrunc=`echo "
TRUNCATE TABLE WF_RULE_DEFAULT;
TRUNCATE TABLE WF_BUSINESS_PROCESS_DEFAULT;
TRUNCATE TABLE WF_VARIABLE_DEFAULT;
TRUNCATE TABLE WF_MODEL_DEFAULT;
COMMIT;
exit;" |sqlplus -s -L ${vSetupSchema}/${vSetupSchemaPswd}@${vSetupdbName}`

imp ${vSetupSchema}/${vSetupSchemaPswd}@${vSetupdbName} file=${postDir}/OLM_SCPP_WFBP_MASTER.dmp \
            fromuser=CA \
            touser=${vSetupSchema} \
            log=${LOG_DIR}/${vSetupSchema}_OLM_SCPP_WFBP_MASTER.log \
            ignore=y \
            grants=n \
            constraints=n \
            silent=y 2>/dev/null
			
sqlplus -L ${vSetupSchema}/${vSetupSchemaPswd}@${vSetupdbName} @${postDir}/OLM_WFBP_PATCH.sql >> ${LOG_DIR}/${vSetupSchema}_OLM_WFBP_PATCH.log			




-- DBTicket POD-67

CREATE TABLE DRIVER_SHIPMENT
  (
    ROW_UID           NUMBER(10,0) NOT NULL,
    DRIVER_ID         NUMBER (12,0) NOT NULL,
    SHIPMENT_ID       NUMBER (10,0) NOT NULL,
    CARRIER_ID        NUMBER (10,0) NOT NULL,
    LAST_UPDATED_DTTM TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
  ) TABLESPACE APPT_TXN_DT_TBS;

COMMENT ON COLUMN DRIVER_SHIPMENT.ROW_UID IS '* Unique Identifier for the record';
COMMENT ON COLUMN DRIVER_SHIPMENT.DRIVER_ID IS '* Driver the shipment is assigned to. ';
COMMENT ON COLUMN DRIVER_SHIPMENT.SHIPMENT_ID IS '* Shipment ID for Proof of Delivery';
COMMENT ON COLUMN DRIVER_SHIPMENT.CARRIER_ID IS '* Carrier assigned to the shipment';
COMMENT ON COLUMN DRIVER_SHIPMENT.LAST_UPDATED_DTTM IS '* last modified time for the record';
  
ALTER TABLE DRIVER_SHIPMENT ADD CONSTRAINT PK_ROW_UID PRIMARY KEY (ROW_UID) USING INDEX TABLESPACE APPT_TXN_IDX_TBS;
ALTER TABLE DRIVER_SHIPMENT ADD CONSTRAINT FK_DRIVER_ID FOREIGN KEY (CARRIER_ID, DRIVER_ID) REFERENCES DRIVER (CARRIER_ID, DRIVER_ID);
ALTER TABLE DRIVER_SHIPMENT ADD CONSTRAINT FK_SHIPMENT_ID FOREIGN KEY (SHIPMENT_ID) REFERENCES SHIPMENT (SHIPMENT_ID);
  
CREATE sequence DRIVER_SHIPMENT_SEQUENCE minvalue 1 maxvalue 999999999 increment BY 1 start with 1 cache 20 noorder cycle;

CREATE TABLE POD_COMMANDS
    (
      COMMAND_ID    NUMBER(8) NOT NULL,
      SEQ           NUMBER(8) NOT NULL,
      SHIPMENT_ID   NUMBER(10) NOT NULL,
      STOP_SEQ      NUMBER(2) NOT NULL,
      COMMAND       VARCHAR2(50) NOT NULL,
      COMMAND_VALUE VARCHAR2(512) NOT NULL,
      IS_EXECUTED   NUMBER(1) DEFAULT 0
    ) TABLESPACE APPT_TXN_DT_TBS;
    

COMMENT ON COLUMN POD_COMMANDS.COMMAND_ID IS '* Unique Identifier for the record';
COMMENT ON COLUMN POD_COMMANDS.SEQ IS '* Unique Sequence for the shipment';
COMMENT ON COLUMN POD_COMMANDS.SHIPMENT_ID IS '* Shipment ID for Proof of Delivery';
COMMENT ON COLUMN POD_COMMANDS.STOP_SEQ IS '* Data received Stop';
COMMENT ON COLUMN POD_COMMANDS.COMMAND IS '* Data Category';
COMMENT ON COLUMN POD_COMMANDS.COMMAND_VALUE IS '* Data Value';
COMMENT ON COLUMN POD_COMMANDS.IS_EXECUTED IS '* Is executed by server?';
	
ALTER TABLE POD_COMMANDS ADD CONSTRAINT POD_COMMANDS_BUS_KEY UNIQUE (SHIPMENT_ID, STOP_SEQ, COMMAND)  USING INDEX TABLESPACE APPT_TXN_IDX_TBS;
ALTER TABLE POD_COMMANDS ADD CONSTRAINT PK_POD_COMMANDS PRIMARY KEY(COMMAND_ID)  USING INDEX TABLESPACE APPT_TXN_IDX_TBS;
	
CREATE SEQUENCE POD_COMMANDS_SEQ INCREMENT BY 1 MAXVALUE 999999999 MINVALUE 1 CYCLE CACHE 20;


CREATE TABLE POD_REASON_CODE
    (
      REASON_CODE_ID NUMBER(10,0) NOT NULL ,
      DESCRIPTION    VARCHAR2(50) NOT NULL ,
      COMPANY_ID     NUMBER(10,0) NOT NULL 
    ) TABLESPACE APPT_TXN_DT_TBS;
    
	
COMMENT ON COLUMN POD_REASON_CODE.REASON_CODE_ID IS '* Reason Code to show in POD app';
COMMENT ON COLUMN POD_REASON_CODE.DESCRIPTION IS '* Reason Code description';
COMMENT ON COLUMN POD_REASON_CODE.COMPANY_ID IS '* Reason Code applicable to Company';
	
ALTER TABLE POD_REASON_CODE ADD CONSTRAINT PK_POD_REASON_CODE_ID PRIMARY KEY (REASON_CODE_ID)  USING INDEX TABLESPACE APPT_TXN_IDX_TBS;
    
	
MERGE INTO POD_REASON_CODE X USING
  (SELECT 0 REASON_CODE_ID, 'OK' DESCRIPTION, -1 COMPANY_ID FROM DUAL
  )
  B ON (X.REASON_CODE_ID = B.REASON_CODE_ID)
WHEN NOT matched THEN
INSERT 
    (
      REASON_CODE_ID,
      DESCRIPTION,
      COMPANY_ID
    )
    VALUES
    (
      B.REASON_CODE_ID,
      B.DESCRIPTION,
      B.COMPANY_ID
    );
	
MERGE INTO POD_REASON_CODE X USING
  (SELECT 1 REASON_CODE_ID, 'SHORT' DESCRIPTION, -1 COMPANY_ID FROM DUAL
  )
  B ON (X.REASON_CODE_ID = B.REASON_CODE_ID)
WHEN NOT matched THEN
INSERT 
    (
      REASON_CODE_ID,
      DESCRIPTION,
      COMPANY_ID
    )
    VALUES
    (
      B.REASON_CODE_ID,
      B.DESCRIPTION,
      B.COMPANY_ID
    );
	
MERGE INTO POD_REASON_CODE X USING
  (SELECT 2 REASON_CODE_ID, 'DAMAGED' DESCRIPTION, -1 COMPANY_ID FROM DUAL
  )
  B ON (X.REASON_CODE_ID = B.REASON_CODE_ID)
WHEN NOT matched THEN
INSERT 
    (
      REASON_CODE_ID,
      DESCRIPTION,
      COMPANY_ID
    )
    VALUES
    (
      B.REASON_CODE_ID,
      B.DESCRIPTION,
      B.COMPANY_ID
    );
	
MERGE INTO POD_REASON_CODE X USING
  (SELECT 3 REASON_CODE_ID, 'OVERAGE' DESCRIPTION, -1 COMPANY_ID FROM DUAL
  )
  B ON (X.REASON_CODE_ID = B.REASON_CODE_ID)
WHEN NOT matched THEN
INSERT 
    (
      REASON_CODE_ID,
      DESCRIPTION,
      COMPANY_ID
    )
    VALUES
    (
      B.REASON_CODE_ID,
      B.DESCRIPTION,
      B.COMPANY_ID
    );
	
CREATE TABLE POD_UNIT_OF_DELIVERY
    (
      UOD_ID             NUMBER (10) NOT NULL,
      BAR_CODE           VARCHAR2 (50) DEFAULT NULL,
      DESCRIPTION        VARCHAR2 (50) DEFAULT NULL,
      DONE               NUMBER (10) DEFAULT 0 NOT NULL,
      NOTES              VARCHAR2 (256) DEFAULT NULL,
      PROT_LEVEL         VARCHAR2 (50) DEFAULT NULL,
      QUANTITY_DELIVERED NUMBER (19,4) DEFAULT 0 NOT NULL,
      QUANTITY_SHIPPED   NUMBER (19,4) DEFAULT 0 NOT NULL,
      REASON_CODE        NUMBER (10) DEFAULT 0 NOT NULL,
      RECORD_ID          NUMBER (10) DEFAULT 0 NOT NULL,
      SHIPMENT_ID        NUMBER (10) DEFAULT 0 NOT NULL,
      STOP_SEQ_DELIVERED NUMBER (10) DEFAULT 0 NOT NULL,
      STOP_SEQ_ORIGINAL  NUMBER (10) DEFAULT 0 NOT NULL,
      UOD_TYPE           VARCHAR2 (10) DEFAULT NULL,
      UOM                VARCHAR2 (10) DEFAULT NULL,
      LAST_UPDATED_DDTM  TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
    ) TABLESPACE APPT_TXN_DT_TBS;
	
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.UOD_ID IS '* Unique identifier of the record';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.BAR_CODE IS '* Bar code of the item';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.DESCRIPTION IS '* Description of the item';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.DONE IS '* flag if the item is delivered';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.NOTES IS '* Notes for the item';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.PROT_LEVEL IS '* Protection level of the item';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.QUANTITY_DELIVERED IS '* Quantity delivered';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.QUANTITY_SHIPPED IS '* Quantity shipped';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.REASON_CODE IS '* Reason Code entered by driver';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.SHIPMENT_ID IS '* Shipment ID for Proof of Delivery';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.STOP_SEQ_DELIVERED IS '* Stop Sequence item was delivered';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.STOP_SEQ_ORIGINAL IS '* Stop sequence item supposed to be delivered';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.UOD_TYPE IS '* Unit of Delivery type';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.UOM IS '* Unit of Measurement';
COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.LAST_UPDATED_DDTM IS '* last modified timestamp of the record';
	
ALTER TABLE POD_UNIT_OF_DELIVERY ADD CONSTRAINT PK_POD_UNIT_OF_DELIVERY PRIMARY KEY(UOD_ID)   USING INDEX TABLESPACE APPT_TXN_IDX_TBS;
	
	
CREATE sequence POD_UNIT_OF_DELIVERY_SEQ minvalue 1 maxvalue 999999999 increment BY 1 start with 1 cache 20 noorder cycle;

CREATE INDEX POD_UNIT_OF_DELIVERY_INDEX ON POD_UNIT_OF_DELIVERY
    (SHIPMENT_ID
    ) TABLESPACE APPT_TXN_IDX_TBS;
	
INSERT
  INTO Resources
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/pod/*',
      'POD',
      2,
      'PUT'
    );
	
INSERT
  INTO Resource_permission
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI ='/services/rest/pod/*'
        AND HTTP_METHOD='PUT'
      )
      ,
      'PODMOBILE'
    );
	
INSERT
  INTO Resources
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/pod/*',
      'POD',
      2,
      'POST'
    );
	
INSERT
  INTO Resource_permission
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI      ='/services/rest/pod/*'
        AND HTTP_METHOD='POST'
      )
      ,
      'PODMOBILE'
    );
	
INSERT
  INTO Resources
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/pod/UODService/*',
      'POD',
      2,
      'GET'
    );
	
INSERT
  INTO Resource_permission
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI      ='/services/rest/pod/UODService/*'
        AND HTTP_METHOD='GET'
      )
      ,
      'PODMOBILE'
    );
	
INSERT
  INTO Resources
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/pod/UODService/*',
      'POD',
      2,
      'PUT'
    );
	
INSERT
  INTO Resource_permission
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI = '/services/rest/pod/UODService/*'
        AND HTTP_METHOD='PUT'
      )
      ,
      'PODMOBILE'
    );
	
INSERT
  INTO Resources
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/pod/PODCommandService/*',
      'POD',
      2,
      'GET'
    );
	
INSERT
  INTO Resource_permission
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI ='/services/rest/pod/PODCommandService/*'
        AND HTTP_METHOD='GET'
      )
      ,
      'PODMOBILE'
    );
	
INSERT
  INTO Resources
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/pod/PODCommandService/*',
      'POD',
      2,
      'PUT'
    );
	
INSERT
  INTO Resource_permission
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI ='/services/rest/pod/PODCommandService/*'
        AND HTTP_METHOD='PUT'
      )
      ,
      'PODMOBILE'
    );
	
INSERT
  INTO Resources
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/pod/PODCommandService/*',
      'POD',
      2,
      'POST'
    );
	
INSERT
  INTO Resource_permission
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI ='/services/rest/pod/PODCommandService/*'
        AND HTTP_METHOD='POST'
      )
      ,
      'PODMOBILE'
    );
	
INSERT
  INTO Resources
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/pod/UODService/*',
      'POD',
      2,
      'POST'
    );
	
INSERT
  INTO Resource_permission
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI  = '/services/rest/pod/UODService/*'
        AND HTTP_METHOD='POST'
      ),
      'PODMOBILE'
    );
	
INSERT
  INTO Resources
    (
      RESOURCE_ID,
      URI,
      MODULE,
      URI_TYPE_ID,
      HTTP_METHOD
    )
    VALUES
    (
      SEQ_RESOURCE_ID.NEXTVAL,
      '/services/rest/pod/*',
      'POD',
      2,
      'GET'
    );
	
INSERT
  INTO Resource_permission
    (
      RESOURCE_ID,
      PERMISSION_CODE
    )
    VALUES
    (
      (SELECT RESOURCE_ID
        FROM RESOURCES
        WHERE URI      ='/services/rest/pod/*'
        AND HTTP_METHOD='GET'
      ),
      'PODMOBILE'
    );
	
COMMIT;


-- DBTicket TPE-35366
-- XMENU Entries POD

INSERT INTO XMENU_ITEM (
	XMENU_ITEM_ID
	,NAME
	,SHORT_NAME
	,NAVIGATION_KEY
	,TILE_KEY
	,ICON
	,BGCOLOR
	,SCREEN_MODE
	,SCREEN_VERSION
	,BASE_SECTION_NAME
	,IS_DEFAULT
	)
VALUES (
	13200001
	,'Proof Of Delivery'
	,'POD'
	,'Pod.screen.Screen'
	,''
	,'truck'
	,'#002E5F'
	,1
	,4
	,'1'
	,1
	);

INSERT INTO XMENU_ITEM_APP (
	XMENU_ITEM_ID
	,APP_ID
	)
VALUES (
	4400163
	,132
	);

INSERT INTO XMENU_ITEM_APP (
	XMENU_ITEM_ID
	,APP_ID
	)
VALUES (
	13200001
	,132
	);

INSERT INTO XMENU_ITEM_PERMISSION (
	XMENU_ITEM_ID
	,PERMISSION_CODE
	)
VALUES (
	13200001
	,'PODMOBILE'
	);

INSERT INTO XBASE_MENU_SECTION (
	XBASE_MENU_SECTION_ID
	,NAME
	,SEQUENCE
	,APP_ID
	)
VALUES (
	132001
	,'Proof Of Delivery'
	,1
	,132
	);

CALL SEQUPDT('XBASE_MENU_PART','XBASE_MENU_PART_ID','SEQ_XBASE_MENU_PART_ID');
CALL SEQUPDT('XMENU_ITEM','XMENU_ITEM_ID','SEQ_XMENU_ITEM_ID');

INSERT INTO XBASE_MENU_PART (
	XBASE_MENU_PART_ID
	,XBASE_MENU_SECTION_ID
	,NAME
	,SEQUENCE
	)
VALUES (
	132001
	,132001
	,'Shipments'
	,1
	);

INSERT INTO XBASE_MENU_ITEM (
	XBASE_MENU_ITEM_ID
	,XBASE_MENU_PART_ID
	,XMENU_ITEM_ID
	,SEQUENCE
	)
VALUES (
	132001
	,132001
	,13200001
	,1
	);

INSERT INTO XBASE_MENU_ITEM (
	XBASE_MENU_ITEM_ID
	,XBASE_MENU_PART_ID
	,XMENU_ITEM_ID
	,SEQUENCE
	)
VALUES (
	132002
	,132001
	,4400163
	,2
	);
	
COMMIT;

-- DBTicket POD-71

ALTER TABLE POD_COMMANDS
ADD(
	LATITUDE  NUMBER(12,6) DEFAULT -999.0 NOT NULL,
	LONGITUDE NUMBER(12,6) DEFAULT -999.0 NOT NULL);
	
COMMENT ON COLUMN POD_COMMANDS.LATITUDE is 'Last calculated latitude on mobile device';
COMMENT ON COLUMN POD_COMMANDS.LONGITUDE is 'Last calculated longitude on mobile device';


-- DBTicket TPE-35781
-- XMENU Entries POD

UPDATE XBASE_MENU_PART
   SET NAME = 'Proof Of Delivery'
 WHERE XBASE_MENU_PART_ID = 132001;
 
COMMIT; 


-- DBTicket POD-78

ALTER TABLE POD_COMMANDS 
ADD ( DATA1 VARCHAR2(100), 
      DATA2 VARCHAR2(100) );

COMMENT ON COLUMN POD_COMMANDS.DATA1 IS '* Sub Data value 1';
COMMENT ON COLUMN POD_COMMANDS.DATA2 IS '* Sub Data value 2';

-- DBTicket POD-162
-- Linked CR TPE-35366

INSERT INTO XBASE_MENU_SECTION_PERMISSION (XBASE_MENU_SECTION_ID,PERMISSION_CODE) 
	VALUES ((SELECT XBASE_MENU_SECTION_ID FROM XBASE_MENU_SECTION WHERE NAME = 'Proof Of Delivery'),'PODMOBILE');

COMMIT;

-- DBTicket POD-159

DECLARE
V_CNT NUMBER;
V_SQL VARCHAR2(100);
BEGIN
   SELECT COUNT(*) INTO V_CNT 
                        from user_constraints
                        where constraint_name='FK_DRIVER_ID';
   IF (V_CNT > 0)
   THEN
      V_SQL := 'ALTER TABLE DRIVER_SHIPMENT DROP CONSTRAINT FK_DRIVER_ID';
      EXECUTE IMMEDIATE V_SQL;
      DBMS_OUTPUT.PUT_LINE ('[INFO] - Completed successfully');
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (TO_CHAR(SQLCODE)|| ' ' || SQLERRM);
END;
/  



-- DBTicket POD-176

COMMENT ON TABLE POD_UNIT_OF_DELIVERY
IS
  'Records the status of each delivered pallet, tote, trolley, etc. as it was delivered at the stop.At each stop, there is a record written for each LPN, etc.';
  COMMENT ON TABLE POD_REASON_CODE
IS
  'POD stores reason codes in POD_REASON_CODE table';
  COMMENT ON TABLE POD_COMMANDS
IS
  'Stores a record of activities at each stop.Roughly five records are written to this table at each visit to a stop';
  COMMENT ON TABLE DRIVER_SHIPMENT
IS
  'An additional table that is used by PoD when it receives an XML message assigning a driver to a shipment';
  COMMENT ON COLUMN POD_UNIT_OF_DELIVERY.RECORD_ID
IS
  'Stores Record ID';
  COMMENT ON column ILM_ACTIVITY_TYPES.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPOINTMENTS.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPOINTMENT_CREATION_TYPE.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPOINTMENT_OBJECTS.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPOINTMENT_STATUS.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPOINTMENT_UTILIZATION.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPT_CHARGE_BACK_CODES.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPT_CUSTOM_ATTRIBUTE.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPT_LOAD_CONFIG.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPT_SIZES.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPT_SLOT_UTILIZATION.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_APPT_UTIL_NEW.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_REQUESTOR_TYPE.CREATED_DTTM
IS
  'created dttm column for auditing';
  COMMENT ON column ILM_ACTIVITY_TYPES.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPOINTMENT_CREATION_TYPE.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPOINTMENT_OBJECTS.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPOINTMENT_STATUS.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPOINTMENT_UTILIZATION.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPT_CHARGE_BACK_CODES.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPT_CUSTOM_ATTRIBUTE.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPT_LOAD_CONFIG.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPT_SIZES.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPT_SLOT_UTILIZATION.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_APPT_UTIL_NEW.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';
  COMMENT ON column ILM_REQUESTOR_TYPE.LAST_UPDATED_DTTM
IS
  'last updated dttm column for auditing';


  
-- DBTicket POD-204

MERGE INTO POD_REASON_CODE X USING
  (SELECT 4 REASON_CODE_ID, 'REJECT' DESCRIPTION, -1 COMPANY_ID FROM DUAL
  )
  B ON (X.REASON_CODE_ID = B.REASON_CODE_ID)
WHEN NOT matched THEN
INSERT
    (
      REASON_CODE_ID,
      DESCRIPTION,
      COMPANY_ID
    )
    VALUES
    (
      B.REASON_CODE_ID,
      B.DESCRIPTION,
      B.COMPANY_ID
    );
    
COMMIT;

-- DBTicket POD-383

CREATE TABLE POD_INTERACTION (
    interaction_id            number (10) not null,
    uuid                      varchar2(40) not null,
    shipment_id               number (10) not null,
    stop_sequence             number  (10) not null,
    questionnaire_template    varchar2(100),
    questionnaire_thanks      varchar2(100),
    to_email                  varchar2(256),
    from_email                varchar2(256),
    subject_email             varchar2(100),
    email_template            varchar2(100),
    questionnaire_url         varchar2(200),
    questionnaire_title       varchar2(100),
    lifecycle                 varchar2(16),
    tc_shipment_id            varchar2(50),
    stop_action               varchar2(2),
    facility_alias_id         varchar2(16),
    facility_address          varchar2(255),
    poc_name                  varchar2(100),
    poc_phone                 varchar2(32),
    poc_email                 varchar2(256),
    planned_arrival           varchar2(32),
    planned_departure         varchar2(32),
    stop_notes                varchar2(500),
    refer_to_crm              varchar2(8),
    crm_case                  varchar2(50),
    crm_url                   varchar2(100),
    crm_fail                  varchar2(8),
    driver_arrival            varchar2(32),
    driver_departure          varchar2(32),
    driver_notes              varchar2(100),
    responder_name            varchar2(100),
    responder_phone           varchar2(32),
    responder_email           varchar2(256),
    record_update_dttm        TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
    ) TABLESPACE APPT_TXN_DT_TBS;
	
CREATE TABLE POD_INTERACTION_PARAMETER (
    interaction_parameter_id      number(10) not null,
    interaction_id                number(10) not null,
    attribute_name                varchar2(50) not null,
    attribute_value               varchar2(255)
    ) TABLESPACE APPT_TXN_DT_TBS;
    

ALTER TABLE POD_INTERACTION
ADD CONSTRAINT POD_INTERACTION_PK 
PRIMARY KEY(interaction_id) USING INDEX TABLESPACE APPT_TXN_IDX_TBS;

ALTER TABLE POD_INTERACTION_PARAMETER
ADD CONSTRAINT POD_INTERACTION_PARAMETER_PK 
PRIMARY KEY(interaction_parameter_id) USING INDEX TABLESPACE APPT_TXN_IDX_TBS;


ALTER TABLE POD_INTERACTION_PARAMETER
ADD CONSTRAINT POD_FK_INTERACTION_ID 
FOREIGN KEY (interaction_id) REFERENCES POD_INTERACTION(interaction_id);

CREATE SEQUENCE POD_INTERACTION_SEQ MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER CYCLE;

CREATE INDEX POD_INTERACTION_INDEX ON POD_INTERACTION (SHIPMENT_ID, STOP_SEQUENCE) TABLESPACE APPT_TXN_IDX_TBS;

CREATE INDEX POD_INTERACTION_UUID_INDEX ON POD_INTERACTION (UUID) TABLESPACE APPT_TXN_IDX_TBS;

CREATE SEQUENCE POD_INTERACTION_PARAMETER_SEQ MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER CYCLE;

COMMENT ON TABLE  pod_interaction is 'represents lifecycle and results of interaction with store manager via PoD questionnaire function';
COMMENT ON COLUMN pod_interaction.interaction_id is    'primary key - generated by sequence';
COMMENT ON COLUMN pod_interaction.uuid is            'UUID - business key';
COMMENT ON COLUMN pod_interaction.shipment_id is    'shipment id and stop sequence are a business key';
COMMENT ON COLUMN pod_interaction.stop_sequence is     'stop number for this ';
COMMENT ON COLUMN pod_interaction.questionnaire_template is 'filename (no path) for the questionnaire';
COMMENT ON COLUMN pod_interaction.questionnaire_thanks is 'filename (no path) for the response to the questionnaire';
COMMENT ON COLUMN pod_interaction.to_email is         'single email address to which the email is sent by email service';
COMMENT ON COLUMN pod_interaction.from_email is        'senders email address - must be set for each company';
COMMENT ON COLUMN pod_interaction.subject_email is    'subject of email sent to store manager with link to questionnaire URL';
COMMENT ON COLUMN pod_interaction.email_template is 'template for email sent to store manager';
COMMENT ON COLUMN pod_interaction.questionnaire_url is 'URL to questionnaire - set by Java code';
COMMENT ON COLUMN pod_interaction.questionnaire_title is 'title for above URL';
COMMENT ON COLUMN pod_interaction.lifecycle is        'state of this interaction - see Constants.java for range of values';
COMMENT ON COLUMN pod_interaction.tc_shipment_id is 'the tc_shipment_id';
COMMENT ON COLUMN pod_interaction.stop_action is    'DL or PU';
COMMENT ON COLUMN pod_interaction.facility_alias_id is 'the facility alias id of the stop';
COMMENT ON COLUMN pod_interaction.facility_address is 'the address - parentheses on both ends';
COMMENT ON COLUMN pod_interaction.poc_name is        'point of contact name from the stop';
COMMENT ON COLUMN pod_interaction.poc_phone is        'point of contact phone number from the stop';
COMMENT ON COLUMN pod_interaction.poc_email is        'point of contact email from the stop';
COMMENT ON COLUMN pod_interaction.planned_arrival is 'planned arrival at the stop';
COMMENT ON COLUMN pod_interaction.planned_departure is 'planned departure from the stop';
COMMENT ON COLUMN pod_interaction.stop_notes is        'planner notes about the stop';
COMMENT ON COLUMN pod_interaction.refer_to_crm is    ' "true" means the business rules think this should be referred to customer support';
COMMENT ON COLUMN pod_interaction.crm_case is        'if referred to customer support, the case number';
COMMENT ON COLUMN pod_interaction.crm_url is        'if referred to customer support, a URL to the ticket';
COMMENT ON COLUMN pod_interaction.crm_fail is        'if the referral to the CRM failed, "true"';
COMMENT ON COLUMN pod_interaction.driver_arrival is 'driver claimed to arrive at this time on the PoD Mobile app';
COMMENT ON COLUMN pod_interaction.driver_departure is 'driver claimed to depart at this time on the PoD Mobile app';
COMMENT ON COLUMN pod_interaction.driver_notes is    'stop notes entered by the driver, if any';
COMMENT ON COLUMN pod_interaction.responder_name is 'name filled in on the questionnaire, if any';
COMMENT ON COLUMN pod_interaction.responder_phone is 'phone filled in on the questionnaire, if any';
COMMENT ON COLUMN pod_interaction.responder_email is 'email filled in on the questionnaire, if any';
COMMENT ON COLUMN pod_interaction.record_update_dttm is 'every time the record is updated, this value is set to the current time';

COMMENT ON TABLE pod_interaction_parameter is 'map of names and values that are associated with pod_interaction table';

COMMENT ON COLUMN POD_INTERACTION_PARAMETER.ATTRIBUTE_NAME is 'ATTRIBUTE NAME';
COMMENT ON COLUMN POD_INTERACTION_PARAMETER.ATTRIBUTE_VALUE is 'ATTRIBUTE VALUE';
COMMENT ON COLUMN POD_INTERACTION_PARAMETER.INTERACTION_ID is 'reference to pod_interaction table INTERACTION_ID column';
COMMENT ON COLUMN POD_INTERACTION_PARAMETER.INTERACTION_PARAMETER_ID is 'primary key - generated by sequence';

-- DBTicket POD-384

EXEC SEQUPDT('bundle_meta_data','BUNDLE_META_DATA_ID','SEQ_BUNDLE_META_DATA_ID');

DELETE FROM LABEL
      WHERE BUNDLE_NAME = 'PoDInteraction';

INSERT INTO BUNDLE_META_DATA (BUNDLE_META_DATA_ID,
                              BUNDLE_NAME,
                              DESCRIPTION,
                              DEF_SCREEN_TYPE_ID,
                              DEF_APP_ID,
                              HINT,
                              IS_ACTIVE,
                              IS_LABEL_BUNDLE,
                              DEF_BU_ID)
     VALUES ( (SELECT 1 + MAX (BUNDLE_META_DATA_ID) FROM BUNDLE_META_DATA),
             'PoDInteraction',
             'PoDInteraction-Questionnaire',
             10,
             NULL,
             NULL,
             1,
             1,
             -1);

MERGE INTO LABEL L
     USING (SELECT 'interactionId' KEY,
                   'Id' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'uuid' KEY, 'UUID' VALUE, 'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'shipmentId' KEY,
                   'Shipment Id' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'stopSequence' KEY,
                   'Stop' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'questionnaireTemplate' KEY,
                   'Questionnaire' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'questionnaireThanks' KEY,
                   'Thanks' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'toEmail' KEY, 'To' VALUE, 'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'fromEmail' KEY,
                   'From' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'subjectEmail' KEY,
                   'Subject' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'emailTemplate' KEY,
                   'Email File' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'questionnaireUrl' KEY,
                   'URL' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'questionnaireTitle' KEY,
                   'URL Link' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'lifecycle' KEY,
                   'Lifecycle' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'tcShipmentId' KEY,
                   'Shipment' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'stopAction' KEY,
                   'Stop Action' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'facilityAliasId' KEY,
                   'Facility' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'facilityAddress' KEY,
                   'Facility Addr' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'pocName' KEY,
                   'Contact' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'pocPhone' KEY,
                   'Phone' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'pocEmail' KEY,
                   'Email' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'plannedArrival' KEY,
                   'Planned Arrival' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'plannedDeparture' KEY,
                   'Planned Departure' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'stopNotes' KEY,
                   'Notes' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'referToCrm' KEY,
                   'To CRM' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'crmUrl' KEY,
                   'CRM Link' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'crmFail' KEY,
                   'CRM Fail' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'driverArrival' KEY,
                   'Driver Arrival' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'driverDeparture' KEY,
                   'Driver Depart' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'driverNotes' KEY,
                   'Driver Notes' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'responderName' KEY,
                   'Responder Name' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'responderPhone' KEY,
                   'Responder Phone' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'responderEmail' KEY,
                   'Responder Email' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;

MERGE INTO LABEL L
     USING (SELECT 'recordUpdateDttm' KEY,
                   'Last Update' VALUE,
                   'PoDInteraction' BUNDLE_NAME
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES ( Seq_Label_Id.Nextval,
               B.KEY,
               B.VALUE,
               B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET VALUE = B.VALUE;
   
COMMIT;

-- DBTicket POD-385

----MERGE INTO PERMISSION X
----     USING (SELECT 'PODQUESTVIEW' PERMISSION_CODE,
----                   1 IS_ACTIVE,
----                   'PoD Questionnaire View' PERMISSION_NAME,
----                   'Allows user to view questionnaires' DESCRIPTION
----              FROM DUAL) B
----        ON (X.PERMISSION_CODE = B.PERMISSION_CODE)
----WHEN NOT MATCHED
----THEN
----   INSERT     (PERMISSION_ID,
----               PERMISSION_CODE,
----               IS_ACTIVE,
----               PERMISSION_NAME,
----               DESCRIPTION)
----       VALUES (SEQ_PERMISSION_ID.NEXTVAL,
----               B.PERMISSION_CODE,
----               B.IS_ACTIVE,
----               B.PERMISSION_NAME,
----               B.DESCRIPTION);


INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        IS_DEFAULT)
     VALUES (13200002,
             'Customer Questionnaires',
             'Customer Questionnaires',
             'maui://Pod.screen.interaction.PodInteraction',
             '',
             'document_plain',
             '#002E5F',
             5,
             2,
             'Transportation Lifecycle Management',
             1);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID)
     VALUES (13200002, 132);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE)
     VALUES (132003,
             132001,
             13200002,
             10);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (13200002, 'PODQUESTVIEW');
	 
COMMIT;


-- DBTicket POD-405

ALTER TABLE DRIVER_SHIPMENT ADD( POD_SHIPMENT_STATUS NUMBER, DRIVER_NAME VARCHAR2(100) );

COMMENT ON COLUMN DRIVER_SHIPMENT.POD_SHIPMENT_STATUS
IS
  '* Shipment Status (accepted/available) based on driver action';

COMMENT ON COLUMN DRIVER_SHIPMENT.DRIVER_NAME
IS
  '* Driver Screen name provided on the device';
  

-- DBTicket POD-454

MERGE INTO BUNDLE_META_DATA A
     USING (SELECT 'POD' AS BUNDLE_NAME FROM DUAL) B
        ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (BUNDLE_META_DATA_ID,
               BUNDLE_NAME,
               DESCRIPTION,
               DEF_SCREEN_TYPE_ID,
               IS_ACTIVE,
               IS_LABEL_BUNDLE)
       VALUES (SEQ_BUNDLE_META_DATA_ID.NextVal,'POD','POD',10,1,1);

--Delete from XCOMPONENT_LABEL where LABEL_BUNDLE_NAME = 'POD';
--Delete from label where bundle_name = 'POD';
--
--MERGE INTO XCOMPONENT L USING (SELECT 'POD' name FROM DUAL) B 
--ON (L.name = B.name) 
--WHEN NOT MATCHED THEN 
--INSERT (L.XCOMPONENT_ID,L.NAME,L.XLEGACY_ID) VALUES (SEQ_XCOMPONENT_ID.NEXTVAL,B.name,1320001);
--
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'selectAll');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'noImageAvailable');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'lpnType');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'gallery');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'reason');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'lpn');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'description');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'pickupDetails');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'deliveryDetails');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'shipmentListTitle');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'saveData');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'confirm');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'pickupsOnStopActivity');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'departureOnStopActivity');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'notesTitle');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'arrival');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'deliveries');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'pickups');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'noNotesFoundMessage');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'searching');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'loading');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'pickupShort');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'deliveryShort');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'stopListTitle');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'driver');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', ':');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'accept');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'noShipmentAssignedMessage');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'noMatchingShipmentFoundMessage');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'shipmentSearchPlaceHolder');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'logout');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'search');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'shipmentSearchMessage');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'proNumberTag');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'bolNumberTag');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'sealTag');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'enterScreenName');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'cancel');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'save');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'loadingImages');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'arrivalListTitle');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'date');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'time');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'currentTime');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'deliveriesListTitle');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'scan');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'pickupAll');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'deliverAll');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'pickupsListTitle');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'remove');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'departureListTitle');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'signatureListTitle');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'clear');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'signedBy');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'shippedUnits');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'pickedUpUnits');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'IMAGES');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'signature');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'deliveredUnits');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'reasonCode');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'stopImages');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'images');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'scannerConfiguration');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'validBarCode');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'unknownBarCode');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'wrongStop');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'startScanning');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'addSeal');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'sealNumberToAdd');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'removeSeal');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'sealNumberToRemove');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'mustProvideAReasonCode');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'tripTag');
--INSERT INTO XCOMPONENT_LABEL (XCOMPONENT_LABEL_ID,XCOMPONENT_ID,LABEL_BUNDLE_NAME,LABEL_KEY) VALUES(SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,(SELECT XCOMPONENT_ID FROM XCOMPONENT WHERE XLEGACY_ID=1320001), 'POD', 'notes');


MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'selectAll' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'selectAll','Select All','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'noImageAvailable' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'noImageAvailable','No Image Available','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'lpnType' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'lpnType','LPN Type','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'gallery' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'gallery','Gallery','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'reason' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'reason','Reason','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'lpn' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'lpn','LPN','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'description' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'description','Description','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'pickupDetails' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'pickupDetails','PICKUP DETAILS','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'deliveryDetails' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'deliveryDetails','DELIVERY DETAILS','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'shipmentListTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'shipmentListTitle','SHIPMENTS','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'saveData' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'saveData','Save Data','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'confirm' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'confirm','Confirm','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'pickupsOnStopActivity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'pickupsOnStopActivity','Pickups','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'departureOnStopActivity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'departureOnStopActivity','Departure','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'notesTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'notesTitle','NOTES','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'arrival' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'arrival','Arrival','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'deliveries' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'deliveries','Deliveries','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'pickups' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'pickups','PickUps','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'noNotesFoundMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'noNotesFoundMessage','No Notes Found','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'searching' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'searching','Searching..','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'loading' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'loading','Loading..','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'pickupShort' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'pickupShort','PU','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'deliveryShort' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'deliveryShort','DL','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'stopListTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'stopListTitle','STOPS','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'driver' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'driver','Driver','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,':' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,':',':','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'accept' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'accept','Accept','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'noShipmentAssignedMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'noShipmentAssignedMessage','No Shipments Assigned','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'noMatchingShipmentFoundMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'noMatchingShipmentFoundMessage','No Matching Shipments Found','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'shipmentSearchPlaceHolder' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'shipmentSearchPlaceHolder','Enter Ship, PRO, BOL, PO or Seal #','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'logout' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'logout','Logout','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'search' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'search','Search','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'shipmentSearchMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'shipmentSearchMessage','Your search returned more than 25 shipments in the results. Refine your search or click on one of the shipments to proceed','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'proNumberTag' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'proNumberTag','PRO#','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'bolNumberTag' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'bolNumberTag','BOL#','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'sealTag' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'sealTag','Seal#','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'enterScreenName' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'enterScreenName','Enter screen name','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'cancel' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'cancel','Cancel','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'save' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'save','Save','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'loadingImages' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'loadingImages','Loading Images..','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'arrivalListTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'arrivalListTitle','ARRIVAL','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'date' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'date','Date','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'time' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'time','Time','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'currentTime' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'currentTime','Current Time','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'deliveriesListTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'deliveriesListTitle','DELIVERIES','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'scan' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'scan','Scan','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'pickupAll' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'pickupAll','Pickup All','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'deliverAll' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'deliverAll','Deliver All','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'pickupsListTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'pickupsListTitle','PICKUPS','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'remove' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'remove','Remove','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'departureListTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'departureListTitle','DEPARTURE','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'signatureListTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'signatureListTitle','SIGNATURE','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'clear' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'clear','Clear','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'signedBy' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'signedBy','Signed By','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'shippedUnits' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'shippedUnits','Shipped unit(s)','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'pickedUpUnits' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'pickedUpUnits','Picked up unit(s)','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'IMAGES' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'IMAGES','IMAGES','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'signature' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'signature','Signature','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'deliveredUnits' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'deliveredUnits','Delivered unit(s)','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'reasonCode' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'reasonCode','Reason Code','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'stopImages' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'stopImages','STOP IMAGES','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'images' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'images','Image(s)','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'scannerConfiguration' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'scannerConfiguration','Scanner Configuration','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'validBarCode' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'validBarCode','Valid Bar Code','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'unknownBarCode' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'unknownBarCode','Unknown Bar Code','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'wrongStop' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'wrongStop','Wrong Stop','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'startScanning' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'startScanning','Start Scanning','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'addSeal' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'addSeal','Add Seal','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'sealNumberToAdd' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'sealNumberToAdd','SEAL NUMBER TO ADD','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'removeSeal' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'removeSeal','Remove Seal','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'sealNumberToRemove' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'sealNumberToRemove','SEAL NUMBER TO REMOVE','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'mustProvideAReasonCode' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'mustProvideAReasonCode','Must provide a Reason Code','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'tripTag' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'tripTag','Trip#','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'notes' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'notes','Notes','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'NoPodDetailsPresent' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'NoPodDetailsPresent','No Pod Details Present','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'PODDetails' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'PODDetails','POD Details','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'ArrivalDTTM' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ArrivalDTTM','Arrival DTTM','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'DepartureDTTM' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'DepartureDTTM','Departure DTTM','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'LPNPickedUpQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'LPNPickedUpQuantity','LPN Picked Up Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'DOLinePickedUpQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'DOLinePickedUpQuantity','DO Line Picked Up Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'LPNShippedQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'LPNShippedQuantity','LPN Shipped Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'DOLineShippedQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'DOLineShippedQuantity','DO Line Shipped Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'LPNOverQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'LPNOverQuantity','LPN Over Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'DOLineOverQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'DOLineOverQuantity','DO Line Over Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'LPNShortQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'LPNShortQuantity','LPN Short Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'DOLineShortQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'DOLineShortQuantity','DO Line Short Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'LPNDeliveredQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'LPNDeliveredQuantity','LPN Delivered Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'DOLineDeliveredQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'DOLineDeliveredQuantity','DO Line Delivered Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'LPNRejectedQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'LPNRejectedQuantity','LPN Rejected Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'DOLineRejectedQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'DOLineRejectedQuantity','DO Line Rejected Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'LPNNotDeliveredQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'LPNNotDeliveredQuantity','LPN Not Delivered Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'LPNNotPickedUpQuantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'LPNNotPickedUpQuantity','LPN Not Picked Up Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'NoExceptionsFound' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'NoExceptionsFound','No Exceptions Found','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'ID' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ID','ID','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'UOM' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'UOM','UOM','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'QuantityShipped' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'QuantityShipped','Quantity Shipped','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'Quantity' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Quantity','Quantity','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'Delivered' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Delivered','Delivered','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'Picked' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'Picked','Picked','POD');

commit;

-- DBTicket POD-493

--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'STOPACTIVITIES' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
              
MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME,
                   'STOPACTIVITIES' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'STOPACTIVITIES',
                 'STOP ACTIVITIES',
                 'POD');
                 
commit;

-- DBTicket POD-488

--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'REMOVESEAL' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--               
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'ADD SEAL' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--               
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'submitBarCode' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);               
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'stopScanning' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);               
--               
--MERGE INTO LABEL L
--     USING (SELECT 'POD' BUNDLE_NAME, 'REMOVESEAL' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'REMOVESEAL',
--               'REMOVE SEAL',
--               'POD');
--
--MERGE INTO LABEL L
--     USING (SELECT 'POD' BUNDLE_NAME, 'ADDSEAL' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'ADDSEAL',
--               'ADD SEAL',
--               'POD');
--
--MERGE INTO LABEL L
--     USING (SELECT 'POD' BUNDLE_NAME, 'submitBarCode' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'submitBarCode',
--               'Submit Bar Code',
--               'POD');
--
--MERGE INTO LABEL L
--     USING (SELECT 'POD' BUNDLE_NAME, 'stopScanning' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'stopScanning',
--               'Stop Scanning',
--               'POD');
--
--commit;               
--
-- DBTicket POD-607

--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'photos' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);               


MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'photos' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'photos',
               'Photo(s)',
               'POD');


commit;

-- DBTicket POD-609

--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'snapshot' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);  
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'delete' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'unitOfDeliveryImages' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'unitOfPickupImages' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--
--MERGE INTO LABEL L
--     USING (SELECT 'POD' BUNDLE_NAME, 'snapshot' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'snapshot',
--               'Snapshot',
--               'POD');
--
--MERGE INTO LABEL L
--     USING (SELECT 'POD' BUNDLE_NAME, 'delete' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'delete',
--               'Delete',
--               'POD');
--
--MERGE INTO LABEL L
--     USING (SELECT 'POD' BUNDLE_NAME, 'unitOfDeliveryImages' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'unitOfDeliveryImages',
--               'UNIT OF DELIVERY IMAGES',
--               'POD');
--
--MERGE INTO LABEL L
--     USING (SELECT 'POD' BUNDLE_NAME, 'unitOfPickupImages' KEY FROM DUAL) B
--        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.LABEL_ID,
--               L.KEY,
--               L.VALUE,
--               L.BUNDLE_NAME)
--       VALUES (SEQ_LABEL_ID.NEXTVAL,
--               'unitOfPickupImages',
--               'UNIT OF PICKUP IMAGES',
--               'POD');
--               
--commit;               
--
---- DBTicket POD-611
--
--delete from label where bundle_name = 'POD' AND key = 'enterScreenName';
--
--
--DELETE FROM xcomponent_label WHERE label_bundle_name = 'POD' AND label_key = 'enterScreenName';
--
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'pickedUpQuantityKey' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--               
--
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'shippedQuantityKey' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'deliveredQuantityKey' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'photo' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'enterFullName' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
               

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'pickedUpQuantityKey' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'pickedUpQuantityKey',
               'Picked Up Quantity',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'shippedQuantityKey' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'shippedQuantityKey',
               'Shipped Quantity',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'deliveredQuantityKey' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'deliveredQuantityKey',
               'Delivered Quantity',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'photo' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'photo',
               'Photo',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'enterFullName' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'enterFullName',
               'Enter Full Name',
               'POD');

commit;

-- DBTicket POD-615


--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'lastScan' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--               
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'scanBarcode' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);               
--
--commit;
--
-- DBTicket POD-627

--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'UNFINISHEDPICKUPS' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'UNFINISHEDDELIVERIES' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--               
--               
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'unfinishedPickupMessage' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--               
--               
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'unfinishedDeliveryMessage' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--               
--               
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'continuePickups' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--               
--               
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'continueDeliveries' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);
--               
--
--MERGE INTO XCOMPONENT_LABEL L
--     USING (SELECT (SELECT XCOMPONENT_ID
--                      FROM XCOMPONENT
--                     WHERE XLEGACY_ID = 1320001)
--                      XCOMPONENT_ID,
--                   'POD' LABEL_BUNDLE_NAME,
--                   'shortRemainder' LABEL_KEY
--              FROM DUAL) B
--        ON (    L.XCOMPONENT_ID = B.XCOMPONENT_ID
--            AND L.LABEL_BUNDLE_NAME = B.LABEL_BUNDLE_NAME
--            AND L.LABEL_KEY = B.LABEL_KEY)
--WHEN NOT MATCHED
--THEN
--   INSERT     (L.XCOMPONENT_LABEL_ID,
--               L.XCOMPONENT_ID,
--               L.LABEL_BUNDLE_NAME,
--               L.LABEL_KEY)
--       VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--               B.XCOMPONENT_ID,
--               B.LABEL_BUNDLE_NAME,
--               B.LABEL_KEY);               
--


MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'UNFINISHEDPICKUPS' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UNFINISHEDPICKUPS',
               'UNFINISHED PICKUPS',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'UNFINISHEDDELIVERIES' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UNFINISHEDDELIVERIES',
               'UNFINISHED DELIVERIES',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'unfinishedPickupMessage' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'unfinishedPickupMessage',
               'The unit of pickup quantity is less than what was shipped.',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'unfinishedDeliveryMessage' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'unfinishedDeliveryMessage',
                 'The unit of delivery quantity is less than what was shipped.',
                 'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'continuePickups' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'continuePickups',
               'Continue Pickups',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'continueDeliveries' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'continueDeliveries',
               'Continue Deliveries',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'shortRemainder' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'shortRemainder',
               'Short Remainder',
               'POD');
commit;


MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'lastScan' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'lastScan',
               'Last Scan',
               'POD');

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'scanBarcode' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'scanBarcode',
               'Scan Barcode',
               'POD');  

commit;               


-- DBTicket POD-649

update LABEL set value = 'SCAN BARCODE' where KEY = 'scanBarcode' and bundle_name = 'POD';
commit;

-- DBTicket POD-645

update LABEL set value = 'Pickups' where KEY = 'pickups' and bundle_name = 'POD';
commit;

-- DBTicket POD-668

UPDATE LABEL SET VALUE = 'Enter Ship, PRO, BOL, PO, Trip or Seal#' where bundle_name = 'POD' and key = 'shipmentSearchPlaceHolder' ;
commit;


-- DBTicket POD-703

MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'PodMobilePermissionTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'PodMobilePermissionTitle','PODMOBILE Permission','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'PodMobilePermissionMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'PodMobilePermissionMessage','You do not have permission to use PoD','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'TakeShipmentTiltle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'TakeShipmentTiltle','Take Shipment?','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'TakeShipmentMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'TakeShipmentMessage','Are you sure you want to take this shipment from','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'QuestionMark' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'QuestionMark','?','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'PleaseAssignTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'PleaseAssignTitle','Please assign','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'PleaseAssignMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'PleaseAssignMessage','Please assign this shipment by swiping from the right edge of the screen.','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'ExitConfirmMessageWithoutSavingImages' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ExitConfirmMessageWithoutSavingImages','Are you sure you want to exit without saving images to the server?','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'DataSavedTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'DataSavedTitle','Data Saved','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'DataSavedMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'DataSavedMessage','All data has been saved.','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'NotAssignTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'NotAssignTitle','Not Assigned','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'AssignShipmentMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'AssignShipmentMessage','Please assign this shipment to yourself first.','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'SealHashTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'SealHashTitle','SEAL #','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'EnterSealMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'EnterSealMessage','Please enter a Seal #','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'IncorrectArrivalTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'IncorrectArrivalTitle','INCORRECT ARRIVAL','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'IncorrectArrivalMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'IncorrectArrivalMessage','Arrival later than Departure','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'IncorrectDepartureTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'IncorrectDepartureTitle','INCORRECT DEPARTURE','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'IncorrectDepartureMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'IncorrectDepartureMessage','Departure earlier than Arrival','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'MissingSignatureTitle' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'MissingSignatureTitle','MISSING SIGNATURE','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'EnterSignatureMessage' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'EnterSignatureMessage','Please provide a signature','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'ok' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'ok','OK','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'yes' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'yes','Yes','POD');
MERGE INTO LABEL L USING (SELECT 'POD' BUNDLE_NAME,'no' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME=B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL,'no','No','POD');

COMMIT;

-- DBTicket POD-707

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'unfinishedActivities' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'unfinishedActivities',
               'Unfinished Activities',
               'POD');  

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'unfinishedActivitiesMessage' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'unfinishedActivitiesMessage',
               'Do you want to stay here?',
               'POD'); 
			   
COMMIT;		

-- DBTicket DB-211

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Proof Of Delivery' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Proof Of Delivery',
               'Proof Of Delivery',
               'Navigation');

COMMIT;

-- DBTicket DB-312

MERGE INTO LABEL L
     USING (SELECT 'POD' BUNDLE_NAME, 'appointment' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'appointment',
               'Appointment',
               'POD');

COMMIT;

-- DBTicket DB-396

MERGE INTO LABEL L USING
(
SELECT
'POD' BUNDLE_NAME, 'Delivery Departure earlier than Pickup Departure. Do you want to continue?' KEY
FROM DUAL
)
B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN
INSERT
(LABEL_ID,KEY,VALUE,BUNDLE_NAME)
VALUES
(
SEQ_LABEL_ID.NEXTVAL,
'Delivery Departure earlier than Pickup Departure. Do you want to continue?',
'Delivery Departure earlier than Pickup Departure. Do you want to continue?',
'POD'
);


MERGE INTO LABEL L USING
(
SELECT
'POD' BUNDLE_NAME, 'Pickup Departure later than Delivery Arrival. Do you want to continue?' KEY
FROM DUAL
)
B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN
INSERT
(LABEL_ID,KEY,VALUE,BUNDLE_NAME)
VALUES
(
SEQ_LABEL_ID.NEXTVAL,
'Pickup Departure later than Delivery Arrival. Do you want to continue?',
'Pickup Departure later than Delivery Arrival. Do you want to continue?',
'POD'
);

MERGE INTO LABEL L USING
(
SELECT
'POD' BUNDLE_NAME, 'Delivery Arrival earlier than Pickup Departure. Do you want to continue?' KEY
FROM DUAL
)
B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN
INSERT
(LABEL_ID,KEY,VALUE,BUNDLE_NAME)
VALUES
(
SEQ_LABEL_ID.NEXTVAL,
'Delivery Arrival earlier than Pickup Departure. Do you want to continue?',
'Delivery Arrival earlier than Pickup Departure. Do you want to continue?',
'POD'
);


MERGE INTO LABEL L USING
(
SELECT
'POD' BUNDLE_NAME, 'Pickup Arrival later than Delivery Arrival. Do you want to continue?' KEY
FROM DUAL
)
B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN
INSERT
(LABEL_ID,KEY,VALUE,BUNDLE_NAME)
VALUES
(
SEQ_LABEL_ID.NEXTVAL,
'Pickup Arrival later than Delivery Arrival. Do you want to continue?',
'Pickup Arrival later than Delivery Arrival. Do you want to continue?',
'POD'
);

commit;

-- DBTicket DB-400

MERGE INTO LABEL L USING
(
SELECT
'POD' BUNDLE_NAME, 'AssignedDriver' KEY
FROM DUAL
)
B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN
INSERT
(LABEL_ID,KEY,VALUE,BUNDLE_NAME)
VALUES
(
SEQ_LABEL_ID.NEXTVAL,
'AssignedDriver',
'Assigned Driver',
'POD'
);

commit;

-- DBTicket DB-8041
alter sequence DRIVER_SHIPMENT_SEQUENCE maxvalue 9999999999;
alter sequence POD_COMMANDS_SEQ maxvalue 99999999;
alter sequence POD_INTERACTION_SEQ maxvalue 9999999999;
alter sequence POD_INTERACTION_PARAMETER_SEQ maxvalue 9999999999;
alter sequence POD_UNIT_OF_DELIVERY_SEQ maxvalue 9999999999;

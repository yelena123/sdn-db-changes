-- DBTicket DMM-47
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'totalTasks' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'totalTasks',
               'Total tasks',
               'lm_mobile_label');




MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'openTasks' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'openTasks',
               'Open tasks',
               'lm_mobile_label');




MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'remainingHrs' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'remainingHrs',
               'Remaining hrs',
               'lm_mobile_label');





MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'employees' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'employees',
               'Employees',
               'lm_mobile_label');





MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'summary' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'summary',
               'Summary',
               'lm_mobile_label');




MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'sort' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'sort',
               'Sort',
               'lm_mobile_label');




MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'apply' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'apply',
               'Apply',
               'lm_mobile_label');




MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'cancel' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'cancel',
               'Cancel',
               'lm_mobile_label');
commit;

-- DBTicket DMM-48

             

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ActualHrs' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ActualHrs',
               'Actual Hrs',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Current' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Current',
               'Current',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Plan' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Plan',
               'Plan',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'PlannedHrs' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PlannedHrs',
               'Planned Hrs',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnitsPerHr' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnitsPerHr',
               'u/hr',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Tasks' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Tasks',
               'Tasks',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Units' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Units',
               'Units',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Open' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Open',
               'Open',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Open' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'EP',
               'EP',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Open' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'OpenHrs',
               'Open Hrs',
               'lm_mobile_label');
commit;

-- DBTicket DMM-46
UPDATE XMENU_ITEM
   SET NAVIGATION_KEY = 'maui://Dm.screen.jobfunction.DmJFScreen'
 WHERE NAME = 'Job Function';

delete from xbase_menu_item
      where xmenu_item_id in (select xmenu_item_id
                                from xmenu_item
                               where name = 'MAUI JF View');

delete from xmenu_item_app
      where xmenu_item_id in (select xmenu_item_id
                                from xmenu_item
                               where name = 'MAUI JF View');

delete from xmenu_item_parameter
      where xmenu_item_id in (select xmenu_item_id
                                from xmenu_item
                               where name = 'MAUI JF View');

delete from xmenu_item_permission
      where xmenu_item_id in (select xmenu_item_id
                                from xmenu_item
                               where name = 'MAUI JF View');

delete from xquick_menu_item
      where xmenu_item_id in (select xmenu_item_id
                                from xmenu_item
                               where name = 'MAUI JF View');

delete from xmenu_item
      where name = 'MAUI JF View';

commit;

-- DBTicket DMM-49

CREATE SEQUENCE LABOR_MSG_MONITOR_ID_SEQ
   START WITH 1
   INCREMENT BY 1
   NOCACHE
   NOCYCLE;

CREATE TABLE LABOR_MSG_MONITOR 
  (
    LABOR_MSG_MONITOR_ID NUMBER(9), 
    LABOR_MSG_ID NUMBER(9), 
    MATCHED_LABOR_TRAN_ID NUMBER(9), 
    STATUS NUMBER(9) DEFAULT 10,
    PROCESSED_BY VARCHAR2(200 CHAR),
	CREATED_SOURCE_TYPE NUMBER(4) DEFAULT 1, 
	CREATED_SOURCE VARCHAR2(25 CHAR), 
    CREATED_DTTM TIMESTAMP (6) DEFAULT SYSTIMESTAMP, 
    LAST_UPDATED_SOURCE_TYPE NUMBER(4) DEFAULT 1, 
    LAST_UPDATED_SOURCE VARCHAR2(25 CHAR), 
    LAST_UPDATED_DTTM TIMESTAMP (6) DEFAULT SYSTIMESTAMP, 
    MISC_TXT_1 VARCHAR2(20 CHAR), 
    MISC_TXT_2 VARCHAR2(20 CHAR), 
    MISC_NUM_1 NUMBER(20,7), 
    MISC_NUM_2 NUMBER(20,7), 
    VERSION_ID NUMBER(10)
  ) TABLESPACE LM_TXN4K_TBS;
  
  
COMMENT ON COLUMN LABOR_MSG_MONITOR.LABOR_MSG_MONITOR_ID IS 'Unique ID for matched records';
COMMENT ON COLUMN LABOR_MSG_MONITOR.LABOR_MSG_ID IS 'foreign key to labor_msg.labor_msg_id';
COMMENT ON COLUMN LABOR_MSG_MONITOR.MATCHED_LABOR_TRAN_ID IS 'foreign key to labor_tran.labor_tran_id';
COMMENT ON COLUMN LABOR_MSG_MONITOR.STATUS IS 'status of the record';
COMMENT ON COLUMN LABOR_MSG_MONITOR.PROCESSED_BY IS 'processed by which node';
COMMENT ON COLUMN LABOR_MSG_MONITOR.CREATED_SOURCE_TYPE IS 'created source type';
COMMENT ON COLUMN LABOR_MSG_MONITOR.CREATED_SOURCE IS 'source created';
COMMENT ON COLUMN LABOR_MSG_MONITOR.CREATED_DTTM IS 'Created on Date and Time';
COMMENT ON COLUMN LABOR_MSG_MONITOR.LAST_UPDATED_SOURCE_TYPE IS 'Last Updated Source Type';
COMMENT ON COLUMN LABOR_MSG_MONITOR.LAST_UPDATED_SOURCE IS 'Last Updated Source';
COMMENT ON COLUMN LABOR_MSG_MONITOR.LAST_UPDATED_DTTM IS 'Last Updated on Date and Time';
COMMENT ON COLUMN LABOR_MSG_MONITOR.MISC_TXT_1 IS 'Misc Col 1';
COMMENT ON COLUMN LABOR_MSG_MONITOR.MISC_TXT_2 IS 'Misc Col 2';
COMMENT ON COLUMN LABOR_MSG_MONITOR.MISC_NUM_1 IS 'Misc Num 1';
COMMENT ON COLUMN LABOR_MSG_MONITOR.MISC_NUM_2 IS 'Misc Num 2';
COMMENT ON COLUMN LABOR_MSG_MONITOR.VERSION_ID IS 'Version of the record';
COMMENT ON TABLE LABOR_MSG_MONITOR  IS 'Labor Messages mapping to Matched Labor Tran';

ALTER TABLE LABOR_MSG_MONITOR ADD CONSTRAINT LABOR_MSG_MONITOR_PK PRIMARY KEY (LABOR_MSG_MONITOR_ID);

ALTER TABLE LABOR_MSG_MONITOR ADD CONSTRAINT FK_LAB_MSG_MON_TO_LAB_MSG FOREIGN KEY (LABOR_MSG_ID)
  REFERENCES LABOR_MSG (LABOR_MSG_ID) ON DELETE SET NULL ENABLE;
  
ALTER TABLE LABOR_MSG_MONITOR ADD CONSTRAINT FK_LAB_MSG_MON_TO_LAB_TRAN FOREIGN KEY (MATCHED_LABOR_TRAN_ID)
  REFERENCES LABOR_TRAN (LABOR_TRAN_ID) ON DELETE SET NULL ENABLE;	  
  
-- DBTicket DMM-54
  
ALTER TABLE E_CONSOL_PERF_SMRY ADD REF_NBR VARCHAR2(50 CHAR);  
  
COMMENT ON COLUMN E_CONSOL_PERF_SMRY.REF_NBR IS 'Nullable field, it contains Task Nbr came from WM';

-- DBTicket LM-11195
MERGE INTO XMENU_ITEM_APP L
     USING (SELECT (SELECT xmenu_item_id
                      FROM xmenu_item
                     WHERE name = 'Mobile Exceptions')
                      XMENU_ITEM_ID,
                   (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'LM')
                      APP_ID
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.APP_ID = B.APP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XMENU_ITEM_ID,
               APP_ID,
               CREATED_SOURCE,
               CREATED_SOURCE_TYPE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_SOURCE_TYPE,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT xmenu_item_id
                   FROM xmenu_item
                  WHERE name = 'Mobile Exceptions'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'LM'),
               NULL,
               1,
               CURRENT_TIMESTAMP,
               NULL,
               1,
               CURRENT_TIMESTAMP);

INSERT
  INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                              PERMISSION_CODE,
                              CREATED_SOURCE_TYPE)
VALUES ( (SELECT xmenu_item_id
            FROM xmenu_item
           WHERE name = 'Mobile Exceptions'),
        'LMAJFCNFG',
        1);

INSERT
  INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                              PERMISSION_CODE,
                              CREATED_SOURCE_TYPE)
VALUES ( (SELECT xmenu_item_id
            FROM xmenu_item
           WHERE name = 'Mobile Exceptions'),
        'LMVJFCNFG',
        1);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES ( (SELECT xmenu_item_id
                 FROM xmenu_item
                WHERE name = 'Rule Column Assignments'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

MERGE INTO XMENU_ITEM_APP L
     USING (SELECT (SELECT xmenu_item_id
                      FROM xmenu_item
                     WHERE name = 'Rule Column Assignments')
                      XMENU_ITEM_ID,
                   (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'LM')
                      APP_ID
              FROM DUAL) B
        ON (L.XMENU_ITEM_ID = B.XMENU_ITEM_ID AND L.APP_ID = B.APP_ID)
WHEN NOT MATCHED
THEN
   INSERT     (XMENU_ITEM_ID,
               APP_ID,
               CREATED_SOURCE,
               CREATED_SOURCE_TYPE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_SOURCE_TYPE,
               LAST_UPDATED_DTTM)
       VALUES ( (SELECT xmenu_item_id
                   FROM xmenu_item
                  WHERE name = 'Rule Column Assignments'),
               (SELECT APP_ID
                  FROM APP
                 WHERE APP_SHORT_NAME = 'LM'),
               NULL,
               1,
               CURRENT_TIMESTAMP,
               NULL,
               1,
               CURRENT_TIMESTAMP);

INSERT
  INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                              PERMISSION_CODE,
                              CREATED_SOURCE_TYPE)
VALUES ( (SELECT xmenu_item_id
            FROM xmenu_item
           WHERE name = 'Rule Column Assignments'),
        'LMAJFCNFG',
        1);

INSERT
  INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                              PERMISSION_CODE,
                              CREATED_SOURCE_TYPE)
VALUES ( (SELECT xmenu_item_id
            FROM xmenu_item
           WHERE name = 'Rule Column Assignments'),
        'LMVJFCNFG',
        1);
commit;

-- DBTicket DMM-100

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (SEQ_XMENU_ITEM_ID.NEXTVAL,
             'Wave View',
             'Wave View',
             'maui://Dm.screen.wave.DmWaveScreen',
             NULL,
             'Dashboard_EmpMgmt',
             NULL,
             0,
             4,
             '1',
             'GENERAL',
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE,
             0);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             SEQ_XMENU_ITEM_ID.CURRVAL,
             (SELECT MAX (SEQUENCE) + 1 FROM XBASE_MENU_ITEM),
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_XMENU_ITEM_ID.CURRVAL,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (SEQ_XMENU_ITEM_ID.CURRVAL,
             'LMAHATCNFG',
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (SEQ_XMENU_ITEM_ID.CURRVAL,
             'LMVHATCNFG',
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE);

COMMIT;

-- DBTicket DMM-66


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'jobFunctionName' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'jobFunctionName',
               'Job Function Name',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'allJobFunction' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'allJobFunction',
               'All Job functions',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'myEmployee' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'myEmployee',
               'My Employees',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'empScoreCard' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'empScoreCard',
               'Employee scorecard',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'employees' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'employees',
               'Employees',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'empPerf' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'empPerf',
               'EP%',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'location' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'location',
               'Location',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'status' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'status',
               'Status',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'sup' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'sup',
               'Sup',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'wa' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'wa',
               'WA',
               'lm_mobile_label');



MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'wg' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'wg',
               'WG',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'tg' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'tg',
               'TG',
               'lm_mobile_label');
			   
commit;

-- DBTicket DMM-107
MERGE INTO LABEL L
     USING (SELECT 'wm_label' BUNDLE_NAME, 'View Mobile Exceptions' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'View Mobile Exceptions',
               'View Mobile Exceptions',
               'wm_label');

MERGE INTO LABEL L
     USING (SELECT 'wm_label' BUNDLE_NAME, 'Admin Mobile Exceptions' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Admin Mobile Exceptions',
               'Admin Mobile Exceptions',
               'wm_label');

DELETE FROM UI_MENU_ITEM_PERMISSION
      WHERE UI_MENU_ITEM_ID IN
               (SELECT UI_MENU_ITEM_ID
                  FROM UI_MENU_ITEM
                 WHERE UI_MENU_SCREEN_ID IN (SELECT UI_MENU_SCREEN_ID
                                               FROM UI_MENU_SCREEN
                                              WHERE screen_id = '2000024')
                       AND DISPLAY_TEXT = 'Save');
					   
exec sequpdt('UI_MENU_ITEM_PERMISSION', 'UI_MENU_ITEM_PERMISSION_ID' ,'UI_MENU_ITEM_PERM_SEQ');

INSERT
  INTO UI_MENU_ITEM_PERMISSION (UI_MENU_ITEM_PERMISSION_ID,
                                UI_MENU_ITEM_ID,
                                PERMISSION)
VALUES (
          ui_menu_item_perm_seq.NEXTVAL,
          (SELECT UI_MENU_ITEM_ID
             FROM UI_MENU_ITEM
            WHERE UI_MENU_SCREEN_ID =
                     (SELECT ui_menu_screen_id
                        FROM ui_menu_screen
                       WHERE screen_id = '2000024' AND menu_id = 1)
                  AND DISPLAY_TEXT = 'Save'),
          'DMMAEXCPT');

DELETE FROM XMENU_ITEM_PERMISSION
      WHERE XMENU_ITEM_ID = (SELECT xmenu_item_id
                               FROM XMENU_ITEM
                              WHERE name = 'Mobile Exceptions');


MERGE INTO XMENU_ITEM_PERMISSION B
     USING (SELECT (SELECT xmenu_item_id
                      FROM xmenu_item
                     WHERE name = 'Mobile Exceptions')
                      xmenu_item_id,
                   'DMMVEXCPT' PERMISSION_CODE
              FROM DUAL) C
        ON (B.xmenu_item_id = C.xmenu_item_id
            AND B.PERMISSION_CODE = C.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (XMENU_ITEM_ID, PERMISSION_CODE, CREATED_SOURCE_TYPE)
       VALUES (C.xmenu_item_id, C.PERMISSION_CODE, 1);


MERGE INTO XMENU_ITEM_PERMISSION B
     USING (SELECT (SELECT xmenu_item_id
                      FROM xmenu_item
                     WHERE name = 'Mobile Exceptions')
                      xmenu_item_id,
                   'DMMAEXCPT' PERMISSION_CODE
              FROM DUAL) C
        ON (B.xmenu_item_id = C.xmenu_item_id
            AND B.PERMISSION_CODE = C.PERMISSION_CODE)
WHEN NOT MATCHED
THEN
   INSERT     (XMENU_ITEM_ID, PERMISSION_CODE, CREATED_SOURCE_TYPE)
       VALUES (C.xmenu_item_id, C.PERMISSION_CODE, 1);

COMMIT;

-- DBTicket DMM-108
MERGE INTO XMENU_ITEM_APP B
     USING (SELECT (SELECT xmenu_item_id
                      FROM xmenu_item
                     WHERE name = 'Mobile Exceptions')
                      xmenu_item_id,
                   (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'LM')
                      app_id
              FROM DUAL) c
        ON (B.XMENU_ITEM_ID = c.XMENU_ITEM_ID AND B.APP_ID = c.app_id)
WHEN NOT MATCHED
THEN
   INSERT     VALUES (c.XMENU_ITEM_ID,
                      c.app_id,
                      NULL,
                      1,
                      CURRENT_TIMESTAMP,
                      NULL,
                      1,
                      CURRENT_TIMESTAMP);

MERGE INTO XBASE_MENU_ITEM B
     USING (SELECT (SELECT XBASE_MENU_PART_ID
                      FROM XBASE_MENU_PART
                     WHERE NAME = 'System Administration'
                           AND xbase_menu_section_id IN
                                  (SELECT xbase_menu_section_id
                                     FROM xbase_menu_section
                                    WHERE name = 'Labor Management'))
                      XBASE_MENU_PART_ID,
                   (SELECT xmenu_item_id
                      FROM xmenu_item
                     WHERE name = 'Mobile Exceptions')
                      xmenu_item_id
              FROM DUAL) c
        ON (B.XBASE_MENU_PART_ID = c.XBASE_MENU_PART_ID
            AND B.xmenu_item_id = c.xmenu_item_id)
WHEN NOT MATCHED
THEN
   INSERT     (B.XBASE_MENU_ITEM_ID,
                             B.XBASE_MENU_PART_ID,
                             B.XMENU_ITEM_ID,
                             B.SEQUENCE,
                             B.CREATED_SOURCE_TYPE)
       VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
               C.XBASE_MENU_PART_ID,
               C.xmenu_item_id,
               seq_xbase_menu_item_id.NEXTVAL,
               1);					  

MERGE INTO XMENU_ITEM_PERMISSION B
     USING (SELECT xmenu_item_id
              FROM xmenu_item
             WHERE name = 'Mobile Exceptions') C
        ON (B.xmenu_item_id = C.xmenu_item_id)
WHEN NOT MATCHED
THEN
   INSERT     (B.XMENU_ITEM_ID, B.PERMISSION_CODE, B.CREATED_SOURCE_TYPE)
       VALUES (C.xmenu_item_id, 'DMMAEXCPT', 1);

MERGE INTO XMENU_ITEM_PERMISSION B
     USING (SELECT xmenu_item_id
              FROM xmenu_item
             WHERE name = 'Mobile Exceptions') C
        ON (B.xmenu_item_id = C.xmenu_item_id)
WHEN NOT MATCHED
THEN
   INSERT     (B.XMENU_ITEM_ID, B.PERMISSION_CODE, B.CREATED_SOURCE_TYPE)
       VALUES (C.xmenu_item_id, 'DMMVEXCPT', 1);

MERGE INTO XMENU_ITEM_APP B
     USING (SELECT (SELECT xmenu_item_id
                      FROM xmenu_item
                     WHERE name = 'Rule Column Assignments')
                      xmenu_item_id,
                   (SELECT APP_ID
                      FROM APP
                     WHERE APP_SHORT_NAME = 'LM')
                      APP_ID
              FROM DUAL) C
        ON (B.xmenu_item_id = C.xmenu_item_id AND B.APP_ID = C.APP_ID)
WHEN  NOT MATCHED
THEN
   INSERT     (b.XMENU_ITEM_ID,
               b.APP_ID,
               b.CREATED_SOURCE,
               b.CREATED_SOURCE_TYPE,
               b.CREATED_DTTM,
               b.LAST_UPDATED_SOURCE,
               b.LAST_UPDATED_SOURCE_TYPE,
               b.LAST_UPDATED_DTTM)
       VALUES (C.xmenu_item_id,
               C.APP_ID,
               NULL,
               1,
               CURRENT_TIMESTAMP,
               NULL,
               1,
               CURRENT_TIMESTAMP);		
			   
MERGE INTO XBASE_MENU_ITEM B
     USING (SELECT (SELECT XBASE_MENU_PART_ID
                      FROM XBASE_MENU_PART
                     WHERE NAME = 'System Administration'
                           AND xbase_menu_section_id IN
                                  (SELECT xbase_menu_section_id
                                     FROM xbase_menu_section
                                    WHERE name = 'Labor Management'))
                      XBASE_MENU_PART_ID,
                   (SELECT xmenu_item_id
                      FROM xmenu_item
                     WHERE name = 'Rule Column Assignments')
                      xmenu_item_id
              FROM DUAL) c
        ON (B.XBASE_MENU_PART_ID = c.XBASE_MENU_PART_ID
            AND B.xmenu_item_id = c.xmenu_item_id)
WHEN NOT MATCHED
THEN
   INSERT     (B.XBASE_MENU_ITEM_ID,
               B.XBASE_MENU_PART_ID,
               B.XMENU_ITEM_ID,
               B.SEQUENCE,
               B.CREATED_SOURCE_TYPE)
       VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
               C.XBASE_MENU_PART_ID,
               C.xmenu_item_id,
               seq_xbase_menu_item_id.NEXTVAL,
               1);

MERGE INTO XMENU_ITEM_PERMISSION B
     USING (SELECT xmenu_item_id
              FROM xmenu_item
             WHERE name = 'Rule Column Assignments') C
        ON (B.xmenu_item_id = C.xmenu_item_id)
WHEN NOT MATCHED
THEN
   INSERT     (B.XMENU_ITEM_ID, B.PERMISSION_CODE, B.CREATED_SOURCE_TYPE)
       VALUES (C.xmenu_item_id, 'ACBORULCOLLST', 1);

MERGE INTO XMENU_ITEM_PERMISSION B
     USING (SELECT xmenu_item_id
              FROM xmenu_item
             WHERE name = 'Rule Column Assignments') C
        ON (B.xmenu_item_id = C.xmenu_item_id)
WHEN NOT MATCHED
THEN
   INSERT     (B.XMENU_ITEM_ID, B.PERMISSION_CODE, B.CREATED_SOURCE_TYPE)
       VALUES (C.xmenu_item_id, 'VCBORULCOLLST', 1);
	   	
COMMIT;	


-- DBTicket DMM-113


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'exceptions' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'exceptions',
               'Exceptions',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'exceptionPanelText' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'exceptionPanelText',
               'To solve take actions on the Task list screen.',
               'lm_mobile_label');
			   
commit;

-- DBTicket DMM-123


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'jobFunctionName' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'jobFunctionName',
               'Job Function Name',
               'lm_mobile_label');



MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'allJobFunction' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'allJobFunction',
               'All Job functions',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'myEmployee' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'myEmployee',
               'My Employees',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'empScoreCard' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'empScoreCard',
               'Employee scorecard',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'employees' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'employees',
               'Employees',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'empPerf' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'empPerf',
               'EP%',
               'lm_mobile_label');





MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'location' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'location',
               'Location',
               'lm_mobile_label');



MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'status' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'status',
               'Status',
               'lm_mobile_label');



MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'sup' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'sup',
               'Sup',
               'lm_mobile_label');




MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'wa' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'wa',
               'WA',
               'lm_mobile_label');



MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'wg' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'wg',
               'WG',
               'lm_mobile_label');




MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'tg' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'tg',
               'TG',
               'lm_mobile_label');

commit;

-- DBTicket DMM-124


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'whatIfAnalysis' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'whatIfAnalysis',
               'What if analysis',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'currentMetrics' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'currentMetrics',
               'Current metrics',
               'lm_mobile_label');



MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'openUnits' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'openUnits',
               'Open units',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'currentEmpCount' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'currentEmpCount',
               'Current emp',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'currentEp' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'currentEp',
               'Current ep',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'openHrsPerEmp' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'openHrsPerEmp',
               'Open hrs/emp',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'timeLineHrs' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'timeLineHrs',
               'Timeline hours',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'enterValueCalculator' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'enterValueCalculator',
               'Enter values in the what if calculator',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'whatIfEp' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'whatIfEp',
               'What if ep%',
               'lm_mobile_label');



MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'startTime' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'startTime',
               'Start time',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'endTime' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'endTime',
               'End time',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'whatifEmp' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'whatifEmp',
               'What if emp',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'reset' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'reset',
               'Reset',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'calculate' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'calculate',
               'Calculate',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'timelineNote' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'timelineNote',
               'The changes made here will not impact the actual data',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'done' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'done',
               'Done',
               'lm_mobile_label');
			   
commit;

-- DBTicket DMM-166
DELETE FROM RESOURCE_PERMISSION
      WHERE RESOURCE_ID IN (SELECT resource_id
                              FROM RESOURCES
                             WHERE URI LIKE '/services/rest/LMCore/*');

DELETE FROM RESOURCES
      WHERE URI LIKE '/services/rest/LMCore/*';

EXEC SEQUPDT('RESOURCES', 'RESOURCE_ID', 'SEQ_RESOURCE_ID');

INSERT INTO RESOURCES (RESOURCE_ID,
                       URI,
                       MODULE,
                       URI_TYPE_ID,
                       HTTP_METHOD)
     VALUES (SEQ_RESOURCE_ID.NEXTVAL,
             '/services/rest/LMCore/*',
             'DMMOB',
             2,
             NULL);

INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'LMAMOBILITY',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE     URI = '/services/rest/LMCore/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));

INSERT INTO RESOURCES (RESOURCE_ID,
                       URI,
                       MODULE,
                       URI_TYPE_ID,
                       HTTP_METHOD)
     VALUES (SEQ_RESOURCE_ID.NEXTVAL,
             '/services/rest/dmmob/*',
             'DMMOB',
             2,
             NULL);

INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'LMMWRKMNTR',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE     URI = '/services/rest/dmmob/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));

INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'LMMEXTRNLWMV',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE     URI = '/services/rest/dmmob/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));

INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'WMVTSKLST',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE     URI = '/services/rest/dmmob/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));


--deleting existing 2014 labor Mobile menu items

DELETE FROM XMENU_ITEM_PERMISSION
      WHERE XMENU_ITEM_ID IN
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE navigation_key IN
                          ('lm.screen.EmpMgmt',
                           'lm.screen.SpvsrAuthorization',
                           'lm.screen.ObservationManagement',
                           'lm.screen.PerformanceMangament',
                           'lm.screen.TeamAssignment',
                           'lm.screen.ReflectiveAssignment',
                           'lm.screen.WorkStandards')
                       AND SCREEN_VERSION = 4);

DELETE FROM XMENU_ITEM_APP
      WHERE XMENU_ITEM_ID IN
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE navigation_key IN
                          ('lm.screen.EmpMgmt',
                           'lm.screen.SpvsrAuthorization',
                           'lm.screen.ObservationManagement',
                           'lm.screen.PerformanceMangament',
                           'lm.screen.TeamAssignment',
                           'lm.screen.ReflectiveAssignment',
                           'lm.screen.WorkStandards')
                       AND SCREEN_VERSION = 4);

DELETE FROM XBASE_MENU_ITEM
      WHERE XMENU_ITEM_ID IN
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE navigation_key IN
                          ('lm.screen.EmpMgmt',
                           'lm.screen.SpvsrAuthorization',
                           'lm.screen.ObservationManagement',
                           'lm.screen.PerformanceMangament',
                           'lm.screen.TeamAssignment',
                           'lm.screen.ReflectiveAssignment',
                           'lm.screen.WorkStandards')
                       AND SCREEN_VERSION = 4);

DELETE FROM XMENU_ITEM
      WHERE navigation_key IN
               ('lm.screen.EmpMgmt',
                'lm.screen.SpvsrAuthorization',
                'lm.screen.ObservationManagement',
                'lm.screen.PerformanceMangament',
                'lm.screen.TeamAssignment',
                'lm.screen.ReflectiveAssignment',
                'lm.screen.WorkStandards')
            AND SCREEN_VERSION = 4;


--delete existing 2015 mobile menu entries

DELETE FROM XMENU_ITEM_PERMISSION
      WHERE XMENU_ITEM_ID IN
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE NAVIGATION_KEY IN
                          ('maui://Dm.screen.jobfunction.DmJFScreen',
                           'maui://Dm.screen.taskmgmt.TaskHdrList',
                           'maui://Dm.screen.taskmgmt.FacilityViewScreen',
                           'Dm.screen.InventoryMgmt',
                           'maui://Dm.screen.wave.DmWaveScreen')
                       AND SCREEN_VERSION = 4);

DELETE FROM XMENU_ITEM_APP
      WHERE XMENU_ITEM_ID IN
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE NAVIGATION_KEY IN
                          ('maui://Dm.screen.jobfunction.DmJFScreen',
                           'maui://Dm.screen.taskmgmt.TaskHdrList',
                           'maui://Dm.screen.taskmgmt.FacilityViewScreen',
                           'Dm.screen.InventoryMgmt',
                           'maui://Dm.screen.wave.DmWaveScreen')
                       AND SCREEN_VERSION = 4);

DELETE FROM XBASE_MENU_ITEM
      WHERE XMENU_ITEM_ID IN
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE NAVIGATION_KEY IN
                          ('maui://Dm.screen.jobfunction.DmJFScreen',
                           'maui://Dm.screen.taskmgmt.TaskHdrList',
                           'maui://Dm.screen.taskmgmt.FacilityViewScreen',
                           'Dm.screen.InventoryMgmt',
                           'maui://Dm.screen.wave.DmWaveScreen')
                       AND SCREEN_VERSION = 4);

DELETE FROM XMENU_ITEM
      WHERE NAVIGATION_KEY IN
               ('maui://Dm.screen.jobfunction.DmJFScreen',
                'maui://Dm.screen.taskmgmt.TaskHdrList',
                'maui://Dm.screen.taskmgmt.FacilityViewScreen',
                'Dm.screen.InventoryMgmt',
                'maui://Dm.screen.wave.DmWaveScreen')
            AND SCREEN_VERSION = 4;

--menu entry for Task Grid

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001008,
             'Work Monitoring: Tasks',
             'Work Monitoring: Tasks',
             'maui://Dm.screen.taskmgmt.TaskHdrList',
             NULL,
             'Dashboard_wrkstd',
             '#E46C0A',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?
--??icon??

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001008,
             900,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001008,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001008,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'WMS'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001008,
             'WMVTSKLST',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001008,
             'LMMEXTRNLWMV',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

--- menu enrty for Facility

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001009,
             'Work Monitoring: Facility',
             'Work Monitoring: Facility',
             'maui://Dm.screen.taskmgmt.FacilityViewScreen',
             NULL,
             'Dashboard_wrkstd',
             '#E46C0A',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001009,
             700,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001009,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001009,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'WMS'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001009,
             'WMVTSKLST',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001009,
             'LMMEXTRNLWMV',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

--- menu entry for Waves

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001010,
             'Work Monitoring: Waves',
             'Work Monitoring: Waves',
             'maui://Dm.screen.wave.DmWaveScreen',
             NULL,
             'Dashboard_wrkstd',
             '#E46C0A',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001010,
             1000,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001010,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001010,
             'LMMWRKMNTR',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

--- menu entry for Job Function

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001011,
             'Work Monitoring: Job Functions',
             'Work Monitoring: Job Functions',
             'maui://Dm.screen.jobfunction.DmJFScreen',
             NULL,
             'Dashboard_wrkstd',
             '#E46C0A',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001011,
             800,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001011,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001011,
             'LMMWRKMNTR',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);



--re-insert existing 2014 menu items
--- menu entry for Employee Management

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001001,
             'Employee Management',
             'Employee Management',
             'lm.screen.EmpMgmt',
             NULL,
             'Dashboard_EmpMgmt',
             '#8E9300',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001001,
             200,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001001,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001001,
             'LMAMOBILITY',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


--- menu entry for Event Authorization

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001002,
             'Event Authorization',
             'Event Authorization',
             'lm.screen.SpvsrAuthorization',
             NULL,
             'Dashboard_EvntAuth',
             '#8E9300',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001002,
             300,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001002,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001002,
             'LMAMOBILITY',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


--- menu entry for Employee Interaction

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001003,
             'Employee Interactions',
             'Employee Interactions',
             'lm.screen.ObservationManagement',
             NULL,
             'Dashboard_ObsMgmt',
             '#8E9300',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001003,
             100,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001003,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001003,
             'LMAMOBILITY',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);



--- menu entry for Performance Management

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001004,
             'Performance Management',
             'Performance Management',
             'lm.screen.PerformanceMangament',
             NULL,
             'Dashboard_PerfMgmt',
             '#8E9300',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001004,
             400,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001004,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001004,
             'LMAMOBILITY',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

--- menu entry for Team Assignments

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001005,
             'Team Assignments',
             'Team Assignments',
             'lm.screen.TeamAssignment',
             NULL,
             'Dashboard_TeamAssg',
             '#8E9300',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001005,
             600,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001005,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001005,
             'LMAMOBILITY',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

--- menu entry for Reflective Assignments

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001006,
             'Reflective Assignments',
             'Reflective Assignments',
             'lm.screen.ReflectiveAssignment',
             NULL,
             'Dashboard_RefAssg',
             '#8E9300',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001006,
             500,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001006,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001006,
             'LMAMOBILITY',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


--- menu entry for Reflective Assignments

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6001007,
             'Work Standards',
             'Work Standards',
             'lm.screen.WorkStandards',
             NULL,
             'Dashboard_wrkstd',
             '#8E9300',
             0,
             4,
             '12',
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

--??Base_section_name and base_section_part? is this an ID or name?

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             701,
             6001007,
             1100,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6001007,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6001007,
             'LMAMOBILITY',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

COMMIT;

-- DBTicket DMM-194

merge into label l
     using (select 'lm_mobile_label' bundle_name, 'jobFunctionLevel' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (label_id,
               key,
               value,
               bundle_name)
       values (SEQ_LABEL_ID.NEXTVAL,
             'jobFunctionLevel',
             'Job Function / Levels',
             'lm_mobile_label');
			 
COMMIT;

-- DBTicket DMM-186

-- Timeline Title Label


MERGE INTO LABEL L USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'TimelineTitle' KEY FROM DUAL) B 
 ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) 
    WHEN NOT MATCHED THEN 
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'TimelineTitle','Timeline (Hrs)','lm_mobile_label');

-- Timeline Released Label


MERGE INTO LABEL L USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'TimelineReleased' KEY FROM DUAL) B 
 ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) 
    WHEN NOT MATCHED THEN 
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'TimelineReleased','Released','lm_mobile_label');

-- Timeline Ahead Label


MERGE INTO LABEL L USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Ahead' KEY FROM DUAL) B 
 ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) 
    WHEN NOT MATCHED THEN 
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Ahead','Ahead','lm_mobile_label');

-- Timeline Behind Label


MERGE INTO LABEL L USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Behind' KEY FROM DUAL) B 
 ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) 
    WHEN NOT MATCHED THEN 
INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) VALUES (SEQ_LABEL_ID.NEXTVAL, 'Behind','Behind','lm_mobile_label');

Commit ;

-- DBTicket DMM-202
exec quiet_drop('TABLE', 'NODE_INTVAL_GTT');
CREATE GLOBAL TEMPORARY TABLE NODE_INTVAL_GTT ( NODE VARCHAR2(20), INTVAL VARCHAR2(20) ) ON COMMIT PRESERVE ROWS;

-- DBTicket DMM-216

Insert into PARAM_GROUP
   (PARAM_GROUP_ID, PARAM_GROUP_NAME, CREATED_DTTM, LAST_UPDATED_DTTM)
 Values
   ('DM', 'DM Parameters', SYSTIMESTAMP, SYSTIMESTAMP);
  
Insert into PARAM_SUBGROUP
   (PARAM_GROUP_ID, PARAM_SUBGROUP_ID, PARAM_SUBGROUP_NAME, DISPLAY_ORDER, CREATED_DTTM, LAST_UPDATED_DTTM)
 Values
   ('DM', 'Mobile', 'DM Mobile Parameters', 4, SYSTIMESTAMP, SYSTIMESTAMP);  
  
insert into PARAM_DEF
 (PARAM_DEF_ID,PARAM_GROUP_ID,PARAM_SUBGROUP_ID,PARAM_NAME,DESCRIPTION,IS_DISABLED,IS_REQUIRED,DATA_TYPE,UI_TYPE,MIN_VALUE,IS_MIN_INC,MAX_VALUE,IS_MAX_INC,DFLT_VALUE,DISPLAY_ORDER,IS_LOCALIZABLE,PARAM_CATEGORY,CREATED_DTTM,LAST_UPDATED_DTTM,PARAM_LEVEL)
 VALUES
 ('default_monitor_hours','DM','Mobile','Default time (in hrs) to monitor in Mobile app','The default time applied in the filter criteria in Mobile UIs. The value should be less than 99.9',0,1,7,'TEXTBOX',0.5,1,99.9,1,24, 601000,0,'Work Monitoring',SYSTIMESTAMP,SYSTIMESTAMP,1);
 
Insert into PARAM_GROUPING (PARAM_GROUPING_ID, PARAM_GROUPING_NAME, PARAM_GROUP_ID, PARAM_SUBGROUP_ID)
 Values
  ((select max (param_grouping_id) from param_grouping) + 1, 'WhseCompanyParams', 'DM', 'Mobile');
  
Commit ; 

----DBTicket DMM-219

-- MERGE INTO LABEL L
     -- USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'allWaves' KEY FROM DUAL) B
        -- ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
-- WHEN NOT MATCHED
-- THEN
   -- INSERT     (L.LABEL_ID,
               -- L.KEY,
               -- L.VALUE,
               -- L.BUNDLE_NAME)
       -- VALUES (SEQ_LABEL_ID.NEXTVAL,
               -- 'allWaves',
               -- 'All Waves',
               -- 'lm_mobile_label');


-- MERGE INTO LABEL L
     -- USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Goal' KEY FROM DUAL) B
        -- ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
-- WHEN NOT MATCHED
-- THEN
   -- INSERT     (L.LABEL_ID,
               -- L.KEY,
               -- L.VALUE,
               -- L.BUNDLE_NAME)
       -- VALUES (SEQ_LABEL_ID.NEXTVAL,
               -- 'Goal',
               -- 'Goal',
               -- 'lm_mobile_label');


-- MERGE INTO LABEL L
     -- USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'JobFunction' KEY FROM DUAL) B
        -- ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
-- WHEN NOT MATCHED
-- THEN
   -- INSERT     (L.LABEL_ID,
               -- L.KEY,
               -- L.VALUE,
               -- L.BUNDLE_NAME)
       -- VALUES (SEQ_LABEL_ID.NEXTVAL,
               -- 'JobFunction',
               -- 'Job Function',
               -- 'lm_mobile_label');


-- MERGE INTO LABEL L
     -- USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Wave' KEY FROM DUAL) B
        -- ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
-- WHEN NOT MATCHED
-- THEN
   -- INSERT     (L.LABEL_ID,
               -- L.KEY,
               -- L.VALUE,
               -- L.BUNDLE_NAME)
       -- VALUES (SEQ_LABEL_ID.NEXTVAL,
               -- 'Wave',
               -- 'Wave',
               -- 'lm_mobile_label');


-- MERGE INTO LABEL L
     -- USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Activity' KEY FROM DUAL) B
        -- ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
-- WHEN NOT MATCHED
-- THEN
   -- INSERT     (L.LABEL_ID,
               -- L.KEY,
               -- L.VALUE,
               -- L.BUNDLE_NAME)
       -- VALUES (SEQ_LABEL_ID.NEXTVAL,
               -- 'Activity',
               -- 'Activity',
               -- 'lm_mobile_label');
-- commit;

-- DBTicket DMM-232


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'jobFunctionLevel' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'jobFunctionLevel',
               'Job Function / Levels',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Facility' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Facility',
               'Facility',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'WaveNbr' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'WaveNbr',
               'Wave Nbr',
               'lm_mobile_label');

COMMIT;

-- DBTicket DMM-244


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'exceptionDoneButton' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'exceptionDoneButton',
               'Done',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'exceptionsTitle' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'exceptionsTitle',
               'Exceptions',
               'lm_mobile_label');
commit;

-- DBTicket DMM-242


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'RecordsFound' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'RecordsFound',
               'Records found',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LastUpdated' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LastUpdated',
               'Last updated',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'TotalTask' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TotalTask',
               'Total Task',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'OpenTask' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'OpenTask',
               'Open Tasks',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'RemainingHrs' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'RemainingHrs',
               'Remaining Hrs',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Employees' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Employees',
               'Employees',
               'lm_mobile_label');
			   
commit;

-- DBTicket DMM-253

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'RecordsFound' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'RecordsFound',
               'Records found',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LastUpdated' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LastUpdated',
               'Last updated',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'TotalTask' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TotalTask',
               'Total Task',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'OpenTask' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'OpenTask',
               'Open Tasks',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'RemainingHrs' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'RemainingHrs',
               'Remaining Hrs',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Employees' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Employees',
               'Employees',
               'lm_mobile_label');
commit;

-- DBTicket DMM-255

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'waveView' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'waveView',
               'Work Monitoring: Waves',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'jobFunctionView' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'jobFunctionView',
               'Work Monitoring: Job Functions',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'activtyView' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'activtyView',
               'Work Monitoring: Activities',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'taskGridView' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'taskGridView',
               'Work Monitoring: Tasks',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Wave' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Wave',
               'Wave',
               'lm_mobile_label');
               
commit;

-- DBTicket DMM-235



MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'filterlist' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'filterlist',
               'Exceptions',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'select filterlist' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'select filterlist',
               'Select exceptions',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'taskAllocQty' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'taskAllocQty',
               'Alloc Qty',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'taskAge' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'taskAge',
               'Age',
               'lm_mobile_label');
			   
COMMIT;

-- DBTicket DMM-396


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'actName' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'actName',
               'Activity Name',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'waveNbr' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'waveNbr',
               'Wave Number',
               'lm_mobile_label');
commit;

-- DBTicket DMM-399
 
 MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'facilityView' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'facilityView',
               'Facility View',
               'lm_mobile_label');
               
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'workMonitoring' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'workMonitoring',
               'Work Monitoring',
               'lm_mobile_label');               

commit;

-- DBTicket DMM-410
CREATE OR REPLACE FUNCTION MATIMESTAMPDIFF(stamp1 TIMESTAMP, nhours number )
RETURN timestamp as
begin

 return ( stamp1 - nhours/24) ;

end ;
/

-- DBTicket DMM-460
set scan off
--menu entry for Warehouse parameters for Labor
INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (6000702,
             'Warehouse Parameters',
             'Warehouse Parameters',
             '/basedata/admin/view/CompanyParameters.jsflps ?PARAM_GROUPING_NAME=WhseCompanyParams&PARAM_LEVEL=1',
             NULL,
             'system',
             '#8E9300',
             0,
             1,
             '1',
             '1',
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE,
             0);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             620,
             6000702,
             25,
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (6000702,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'LM'),
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (6000702,
             'VBR',
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE);

			 
INSERT INTO RESOURCES (RESOURCE_ID,
                       URI,
                       MODULE,
                       URI_TYPE_ID,
                       HTTP_METHOD)
     VALUES (SEQ_RESOURCE_ID.NEXTVAL,
             '/services/rest/dm/*',
             'DMMOB',
             2,
             null);
			 
INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'LMMWRKMNTR',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE URI =
                          '/services/rest/dm/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));

INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'LMMEXTRNLWMV',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE URI =
                          '/services/rest/dm/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));
INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'WMVTSKLST',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE URI =
                          '/services/rest/dm/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));

COMMIT;
			 
-- DBTicket DMM-528

MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'age' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'age',
      'Age (hrs)',
      'lm_mobile_label'
    );
	
MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'whatifEmpCanNotZero' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'whatifEmpCanNotZero',
      'Employees can not be zero',
      'lm_mobile_label'
    );
	
MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'whatifEpCanNotZero' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'whatifEpCanNotZero',
      'EP% cannot be zero',
      'lm_mobile_label'
    );
	commit;	 

-- DBTicket DMM-543
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Hr' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Hr',
               'h',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Min' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Min',
               'm',
               'lm_mobile_label');
               
COMMIT;

-- DBTicket DMM-550
UPDATE LABEL SET VALUE = 'Last Updated' WHERE KEY = 'LastUpdated' AND BUNDLE_NAME = 'lm_mobile_label';
update label set value = 'Records Found' where key = 'RecordsFound' and bundle_name = 'lm_mobile_label';
commit;

-- DBTicket DMM-569
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Hours' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Hours',
               'Hours',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'of' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'of',
               'of',
               'lm_mobile_label');

COMMIT;

-- DBTicket DMM-574
begin
   execute immediate
      'ALTER TABLE HIERARCHY_NODES DROP CONSTRAINT HIERARCHY_NODES_UK1 DROP INDEX';
exception
   when others
   then
      null;
end;
/

call quiet_drop('INDEX', 'HIERARCHY_NODES_UK1');
CREATE UNIQUE INDEX HIERARCHY_NODES_IDX1 ON HIERARCHY_NODES(HIERARCHY_LEVEL_ID, NODE_CONTENT_DATA1_ID, NODE_CONTENT_DATA2_ID) TABLESPACE LLMINDX;

-- DBTicket DMM-573
        
INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'LMMWRKMNTR',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE URI =
                          '/services/rest/LMCore/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));
                       
                       
INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'WMVTSKLST',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE URI =
                          '/services/rest/LMCore/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));
                       
INSERT INTO RESOURCE_PERMISSION (PERMISSION_CODE, RESOURCE_ID)
     VALUES (
               'LMMEXTRNLWMV',
               (SELECT RESOURCE_ID
                  FROM RESOURCES
                 WHERE URI =
                          '/services/rest/LMCore/*'
                       AND MODULE = 'DMMOB'
                       AND URI_TYPE_ID = 2));                       

commit ;
					   
-- DBTicket DMM-587
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'hrs' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'hrs',
               'hrs',
               'lm_mobile_label');

COMMIT;


-- DBTicket DMM-592

update xmenu_item set name = 'Work Monitoring: Departments', short_name = 'Work Monitoring: Departments', icon='Dashboard_dept' where xmenu_item_id=6001009;
update xmenu_item set icon='Dashboard_tasks' where xmenu_item_id=6001008;
update xmenu_item set icon='Dashboard_waves' where xmenu_item_id=6001010;
update xmenu_item set icon='Dashboard_jobfn' where xmenu_item_id=6001011;

COMMIT;

-- DBTicket DMM-595
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'TaskTypeGroup' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TaskTypeGroup',
               'Task Type Group',
               'lm_mobile_label');

COMMIT;

-- DBTicket DMM-596
update label set value='Current EP%' where key='currentEp' and bundle_name='lm_mobile_label';
update label set value='EP%' where key='whatIfEp' and bundle_name='lm_mobile_label';
update label set value='Employees' where key='whatifEmp' and bundle_name='lm_mobile_label';
update label set value='Open hrs/employee' where key='openHrsPerEmp' and bundle_name='lm_mobile_label';
update label set value='Current employee' where key='currentEmpCount' and bundle_name='lm_mobile_label';

commit;

-- DBTicket DMM-608
CALL QUIET_DROP('INDEX','LABOR_MSG_REF_CODE_INDX');
CALL QUIET_DROP('INDEX','TASK_HDR_TASK_TYPE');
CALL QUIET_DROP('INDEX','ECONS_REFNUM_INDX');
CALL QUIET_DROP('INDEX','RUL_HDR_RUL_TYP_STAT_GRP_INDX');
CALL QUIET_DROP('INDEX','LBR_TRAN_PRC_NORMAL_INDX');

CREATE INDEX LABOR_MSG_REF_CODE_INDX
   ON LABOR_MSG (REF_NBR) TABLESPACE CBO_TXN_IDX_TBS;

CREATE INDEX TASK_HDR_TASK_TYPE
   ON TASK_HDR (TASK_TYPE) TABLESPACE LLMINDX;

CREATE INDEX ECONS_REFNUM_INDX
   ON E_CONSOL_PERF_SMRY (REF_NBR) TABLESPACE LM_IDX_TBS;

CREATE INDEX RUL_HDR_RUL_TYP_STAT_GRP_INDX
   ON RULE_HDR (RULE_TYPE, STAT_CODE, RULE_GRP) TABLESPACE CBO_BASE_IDX_TBS;

CREATE INDEX LBR_TRAN_PRC_NORMAL_INDX
   ON LABOR_TRAN (STATUS,
                  MSG_STAT_CODE,
                  WHSE,
                  LABOR_TRAN_ID,
                  WAVE_NBR,
                  LOGIN_USER_ID) TABLESPACE CBO_TXN_IDX_TBS;
                  
-- DBTicket DMM-618
update LABEL set  KEY = 'UCLPermissions_Administer Appointment Alert', VALUE = 'Administer Appointment Alert' where KEY = 'UCLPermissions_Appt - Administer Appointment Alert' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Contract Rate Profiles', VALUE = 'Administer Contract Rate Profiles' where KEY = 'UCLPermissions_Contract Mgmt - Administer Contract Rate Profiles' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Yard Dock Management', VALUE = 'Yard Dock Management' where KEY = 'UCLPermissions_Yard Management - Yard Dock Management' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Dev Tools', VALUE = 'Administer Dev Tools' where KEY = 'UCLPermissions_Admin Dev Tools' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Assign Dock Door', VALUE = 'Assign Dock Door' where KEY = 'UCLPermissions_Admin - Assign Dock Door' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Mass Route Expiration', VALUE = 'Administer Mass Route Expiration' where KEY = 'UCLPermissions_Contract Mgmt - Admin Mass Route Expiration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Navigation Permissions', VALUE = 'Administer Navigation Permissions' where KEY = 'UCLPermissions_Sys Ctrl - Administer Navigation permissions' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Parcel Data Toolkit', VALUE = 'Administer Parcel Data Toolkit' where KEY = 'UCLPermissions_Contract Mgmt - Administer Parcel Data Toolkit' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Parcel Transit Time', VALUE = 'Administer Parcel Transit Time' where KEY = 'UCLPermissions_Admin - Parcel Transit Time' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Parcel Zone Rates', VALUE = 'Administer Parcel Zone Rates' where KEY = 'UCLPermissions_Admin - Parcel Zone Rates' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Routing Guide (ARG)', VALUE = 'Administer Routing Guide (ARG)' where KEY = 'UCLPermissions_Contract Mgmt - Administer Routing Guide' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Shipment Assignment', VALUE = 'Administer Shipment Assignment' where KEY = 'UCLPermissions_Admin Shipment Assignment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Create Shipment Tasks', VALUE = 'Administer Create Shipment Tasks' where KEY = 'UCLPermissions_Outbound - Administer Create Shipment tasks' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Rates', VALUE = 'Administer Rates' where KEY = 'UCLPermissions_Contract Mgmt - Administer Rates' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Retail Route (AST)', VALUE = 'Administer Retail Route (AST)' where KEY = 'UCLPermissions_Transportation - Administer Retail Route' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Audit Task', VALUE = 'RF Audit Task' where KEY = 'UCLPermissions_Yard Management -RF  Audit Task' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Transaction Assignments', VALUE = 'Administer Transaction Assignments' where KEY = 'UCLPermissions_Sys Ctrl - Administer Transaction assignments' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Trailer', VALUE = 'Administer Trailer' where KEY = 'UCLPermissions_Yard Management - Administer Trailer' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Yard Tasks', VALUE = 'Administer Yard Tasks' where KEY = 'UCLPermissions_Yard Management - Administer Task' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Shipment', VALUE = 'Administer Shipment' where KEY = 'UCLPermissions_Outbound - Administer Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Yard', VALUE = 'Administer Yard' where KEY = 'UCLPermissions_Yard Management - Administer Yard' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Yard Slot Management', VALUE = 'Yard Slot Management' where KEY = 'UCLPermissions_Yard Management - Yard Slot Management' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Yard Audit', VALUE = 'Administer Yard Audit' where KEY = 'UCLPermissions_Yard Management - Administer Yard Audit' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Carrier Trailer Pool', VALUE = 'View Carrier Trailer Pool' where KEY = 'UCLPermissions_Carrier Trailer Pool' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Check-In/Out', VALUE = 'Check-In/Out' where KEY = 'UCLPermissions_Yard Management - Check In/Out' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Close Outbound Shipment', VALUE = 'Administer Close Outbound Shipment' where KEY = 'UCLPermissions_Admin Close Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Consolidation Management', VALUE = 'Administer Consolidation Management' where KEY = 'UCLPermissions_Shipment Plan - Administer Consolidation Mgmt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Create Change Set User', VALUE = 'Create Change Set User' where KEY = 'UCLPermissions_Config Director - Create Change Set User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Delete Change Set User', VALUE = 'Delete Change Set User' where KEY = 'UCLPermissions_Config Director - Delete Change Set User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Modify Change Set User', VALUE = 'Modify Change Set User' where KEY = 'UCLPermissions_Config Director - Modify Change Set User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Read Change Set User', VALUE = 'Read Change Set User' where KEY = 'UCLPermissions_Config Director - Read Change Set User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Create Deployment User', VALUE = 'Create Deployment User' where KEY = 'UCLPermissions_Config Director - Create Deployment User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Delete Deployment User', VALUE = 'Delete Deployment User' where KEY = 'UCLPermissions_Config Director - Delete Deployment User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Modify Deployment User', VALUE = 'Modify Deployment User' where KEY = 'UCLPermissions_Config Director - Modify Deployment User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Read Deployment User', VALUE = 'Read Deployment User' where KEY = 'UCLPermissions_Config Director - Read Deployment User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Create Endpoint User', VALUE = 'Create Endpoint User' where KEY = 'UCLPermissions_Config Director - Create Endpoint User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Delete Endpoint User', VALUE = 'Delete Endpoint User' where KEY = 'UCLPermissions_Config Director - Delete Endpoint User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Modify Endpoint User', VALUE = 'Modify Endpoint User' where KEY = 'UCLPermissions_Config Director - Modify Endpoint User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Read Endpoint User', VALUE = 'Read Endpoint User' where KEY = 'UCLPermissions_Config Director - Read Endpoint User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Create Meta Model User', VALUE = 'Create Meta Model User' where KEY = 'UCLPermissions_Config Director - Create Meta Model User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Delete Meta Model User', VALUE = 'Delete Meta Model User' where KEY = 'UCLPermissions_Config Director - Delete Meta Model User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Modify Meta Model User', VALUE = 'Modify Meta Model User' where KEY = 'UCLPermissions_Config Director - Modify Meta Model User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Read Meta Model User', VALUE = 'Read Meta Model User' where KEY = 'UCLPermissions_Config Director - Read Meta Model User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Close Manifest', VALUE = 'Administer Close Manifest' where KEY = 'UCLPermissions_Manifest - Administer Close Manifest' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Shipment Planning Optimization', VALUE = 'Administer Shipment Planning Optimization' where KEY = 'UCLPermissions_Shipment Plan - Administer Shipment Plng Optimzn' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Close Task', VALUE = 'RF Close Task' where KEY = 'UCLPermissions_Yard Management - RF Close Task' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Mobile Exceptions', VALUE = 'Administer Mobile Exceptions' where KEY = 'UCLPermissions_Admin Mobile Exceptions' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_De-Manifest In Manifest UI', VALUE = 'De-Manifest In Manifest UI' where KEY = 'UCLPermissions_Manifest - Administer Demanifesting' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View FedEx Transactions', VALUE = 'View FedEx Transactions' where KEY = 'UCLPermissions_Manifest - View FedEx Transactions' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Check-In Inbound', VALUE = 'Check-In Inbound' where KEY = 'UCLPermissions_Yard Management - Check-In Inbound' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Appointment Control Number', VALUE = 'View Appointment Control Number' where KEY = 'UCLPermissions_Appt - View Control Number' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Locate Trailer', VALUE = 'Administer Locate Trailer' where KEY = 'UCLPermissions_Admin Locate Trailer' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Label Substitutions', VALUE = 'Administer Label Substitutions' where KEY = 'UCLPermissions_Manifest - Administer Label Substitutions' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Manifest Type', VALUE = 'Administer Manifest Type' where KEY = 'UCLPermissions_Manifest - Administer Manifest Type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Manifesting', VALUE = 'Administer Manifesting' where KEY = 'UCLPermissions_Manifest - Administer Manifesting' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Edit DO Destination Address', VALUE = 'Edit DO Destination Address' where KEY = 'UCLPermissions_Manifest - Edit DO Destination Address' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Manifest LPN', VALUE = 'Manifest LPN' where KEY = 'UCLPermissions_Manifest - Manifest LPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_All Planning Actions', VALUE = 'All Planning Actions' where KEY = 'UCLPermissions_MSP All Planning Actions' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Assign Single Unassigned LPN To New Shipment', VALUE = 'Assign Single Unassigned LPN To New Shipment' where KEY = 'UCLPermissions_MSP Assign Single Unassigned LPN  to New Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Assign Single Unassigned DO To New Shipment', VALUE = 'Assign Single Unassigned DO To New Shipment' where KEY = 'UCLPermissions_MSP Assign Single Unassigned DO to New Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Assign Unassigned LPN To Existing Shipment', VALUE = 'Assign Unassigned LPN To Existing Shipment' where KEY = 'UCLPermissions_MSP Assign Unassigned LPN to Existing Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Assign Unassigned Order To Existing Shipment', VALUE = 'Assign Unassigned Order To Existing Shipment' where KEY = 'UCLPermissions_MSP Assign Unassigned DO to Existing Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Combine Unassigned LPNs To New Shipment', VALUE = 'Combine Unassigned LPNs To New Shipment' where KEY = 'UCLPermissions_MSP Combine Unassigned LPNs to New Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Combine Multiple Unassigned DO To New Shipment', VALUE = 'Combine Multiple Unassigned DO To New Shipment' where KEY = 'UCLPermissions_MSP Combine Multiple Unassigned DO to New Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Combine Shipments', VALUE = 'Combine Shipments' where KEY = 'UCLPermissions_MSP Combine Shipments' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Unassign LPN From Order', VALUE = 'Unassign LPN From Order' where KEY = 'UCLPermissions_MSP Unassign LPN FROM DO' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Move LPN', VALUE = 'Move LPN' where KEY = 'UCLPermissions_MSP Move LPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Move Distribution Order', VALUE = 'Move Distribution Order' where KEY = 'UCLPermissions_MSP Move DO' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Resequence Stops', VALUE = 'Resequence Stops' where KEY = 'UCLPermissions_MSP Resequence stops' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Split Order', VALUE = 'Split Order' where KEY = 'UCLPermissions_MSP Split Shipments' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Split Shipments For LPN', VALUE = 'Split Shipments For LPN' where KEY = 'UCLPermissions_MSP Split Shipments For LPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Unassign Distribution Order', VALUE = 'Unassign Distribution Order' where KEY = 'UCLPermissions_MSP Unassign DO' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Manual Shipment Assignment', VALUE = 'Administer Manual Shipment Assignment' where KEY = 'UCLPermissions_Transportation - Administer Manual Shpmt Assign' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Move Task', VALUE = 'RF Move Task' where KEY = 'UCLPermissions_Yard Management - RF Move Task' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Maintain EDI Cross Ref', VALUE = 'Administer Maintain EDI Cross Ref' where KEY = 'UCLPermissions_Manifest - Administer Maintain EDI Cross Ref' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Print Pre-Receipt Eligibility Report', VALUE = 'Print Pre-Receipt Eligibility Report' where KEY = 'UCLPermissions_Print Pre-receipt eligibility report' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Print Shipping Docs', VALUE = 'Administer Print Shipping Docs' where KEY = 'UCLPermissions_Outbound - Administer Print Shipping Docs' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Seal Task', VALUE = 'RF Seal Task' where KEY = 'UCLPermissions_Yard Management - RF Seal Task' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Airway Bill Report', VALUE = 'View Airway Bill Report' where KEY = 'UCLPermissions_TE Reports - View Airway Bill' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Bill Of Lading Report', VALUE = 'View Bill Of Lading Report' where KEY = 'UCLPermissions_TE Reports - View Bill of Lading' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Canadian Custom Invoice Report', VALUE = 'View Canadian Custom Invoice Report' where KEY = 'UCLPermissions_TE Reports - View Canadian Custom Invoice' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Certificate Of Origin Report', VALUE = 'View Certificate Of Origin Report' where KEY = 'UCLPermissions_TE Reports - View Certificate Of Origin' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Commercial Invoice Report', VALUE = 'View Commercial Invoice Report' where KEY = 'UCLPermissions_TE Reports - View Commercial Invoice' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL Manifest - Customer Dept/Store Nbr Report', VALUE = 'View LTL Manifest - Customer Dept/Store Nbr Report' where KEY = 'UCLPermissions_TE Reports - View LTL Manif - Cust Dept/Store Nbr' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL Manifest - Customer PO Report', VALUE = 'View LTL Manifest - Customer PO Report' where KEY = 'UCLPermissions_TE Reports - View LTL Manifest - Customer PO' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL Manifest - Dc Number/Store Number Report', VALUE = 'View LTL Manifest - Dc Number/Store Number Report' where KEY = 'UCLPermissions_TE Reports - View LTL Manif - DC Number/Store Nbr' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View DHL Manifest Report', VALUE = 'View DHL Manifest Report' where KEY = 'UCLPermissions_TE Reports - View DHL Manifest' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Dock Receipt Report', VALUE = 'View Dock Receipt Report' where KEY = 'UCLPermissions_TE Reports - View Dock Receipt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Export Packing List Report', VALUE = 'View Export Packing List Report' where KEY = 'UCLPermissions_TE Reports - View Export Packing List' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View USPS FC Postage Statement - Flats Report', VALUE = 'View USPS FC Postage Statement - Flats Report' where KEY = 'UCLPermissions_TE Reports - View USPS FC Postage Stmnt - Flats' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View USPS FC Postage Statement - Letters Report', VALUE = 'View USPS FC Postage Statement - Letters Report' where KEY = 'UCLPermissions_TE Reports - View USPS FC Postage Stmnt - Letters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View USPS FC Postage Statement - Parcels Report', VALUE = 'View USPS FC Postage Statement - Parcels Report' where KEY = 'UCLPermissions_TE Reports - View USPS FC Postage Stmnt - Parcels' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View USPS Itemized Manifest Report', VALUE = 'View USPS Itemized Manifest Report' where KEY = 'UCLPermissions_TE Reports - View USPS Itemized Manifest' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View NAFTA Certificate Of Origin Report', VALUE = 'View NAFTA Certificate Of Origin Report' where KEY = 'UCLPermissions_TE Reports - View NAFTA Certificate Of Origin' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Ocean Bill Of Lading Report', VALUE = 'View Ocean Bill Of Lading Report' where KEY = 'UCLPermissions_TE Reports - View Ocean Bill Of Lading' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL Manifest - Order/Store Number Report', VALUE = 'View LTL Manifest - Order/Store Number Report' where KEY = 'UCLPermissions_TE Reports - View LTL Manif - Order/Store Nbr' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL Manifest - PO/Store Number Report', VALUE = 'View LTL Manifest - PO/Store Number Report' where KEY = 'UCLPermissions_TE Reports - View LTL Manif - PO/Store Nbr' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View UPS Parcel Manifest Report', VALUE = 'View UPS Parcel Manifest Report' where KEY = 'UCLPermissions_TE Reports - View UPS Parcel Manifest' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View USPS Parcel Postage Statement Report', VALUE = 'View USPS Parcel Postage Statement Report' where KEY = 'UCLPermissions_TE Reports - View USPS Parcel Postage Stmnt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View USPS Priority Mail Postage Statement Report', VALUE = 'View USPS Priority Mail Postage Statement Report' where KEY = 'UCLPermissions_TE Reports - View USPS Prty Mail Postage Stmnt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL Manifest - Ship To City/Store Report', VALUE = 'View LTL Manifest - Ship To City/Store Report' where KEY = 'UCLPermissions_TE Reports - View LTL Manif - Ship To City/Store' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Shipper Export Declaration Report', VALUE = 'View Shipper Export Declaration Report' where KEY = 'UCLPermissions_TE Reports - View Shipper Export Declaration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Shipper Letter Of Instruction Report', VALUE = 'View Shipper Letter Of Instruction Report' where KEY = 'UCLPermissions_TE Reports - View Shipper Letter of Instruction' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Shipment Inquiry Report', VALUE = 'View Shipment Inquiry Report' where KEY = 'UCLPermissions_TE Reports - View Shipment Inquiry' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL Manifest - Store/Order Report', VALUE = 'View LTL Manifest - Store/Order Report' where KEY = 'UCLPermissions_TE Reports - View LTL Manifest - Store/Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL Manifest - Store/PO Report', VALUE = 'View LTL Manifest - Store/PO Report' where KEY = 'UCLPermissions_TE Reports - View LTL Manif - Store/Purchase Ord' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL Manifest - State/Store Number Report', VALUE = 'View LTL Manifest - State/Store Number Report' where KEY = 'UCLPermissions_TE Reports - View LTL Manif - State/Store Nbr' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View UPS Control Log Report', VALUE = 'View UPS Control Log Report' where KEY = 'UCLPermissions_TE Reports - View UPS Control Log' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View UPS EMT Response Report', VALUE = 'View UPS EMT Response Report' where KEY = 'UCLPermissions_TE Reports - View UPS EMT Response' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_UCL Corporate Administer', VALUE = 'UCL Corporate Administer' where KEY = 'UCLPermissions_UCL Corporate Admin' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_UCL System Administer', VALUE = 'UCL System Administer' where KEY = 'UCLPermissions_UCL System Admin' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Archive Invoices', VALUE = 'View Archive Invoices' where KEY = 'UCLPermissions_Archive Management - View Archive Invoices' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Validate Pre Receipt Allocation', VALUE = 'Validate Pre Receipt Allocation' where KEY = 'UCLPermissions_Validate Pre Receipt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Appointment Alert', VALUE = 'View Appointment Alert' where KEY = 'UCLPermissions_Appt - View Appointment Alert' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Appointment Report And Calendar', VALUE = 'View Appointment Report And Calendar' where KEY = 'UCLPermissions_Appt - View Calendar' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Graphical Yard', VALUE = 'View Graphical Yard' where KEY = 'UCLPermissions_Yard Management - View Graphical Yard' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Graphical Yard (VGYRD)', VALUE = 'View Graphical Yard (VGYRD)' where KEY = 'UCLPermissions_Yard Management - View Graphical Yard (VGYRD )' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Parcel Transit Time', VALUE = 'View Parcel Transit Time' where KEY = 'UCLPermissions_View - Parcel Transit Time' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Parcel Zone Rates', VALUE = 'View Parcel Zone Rates' where KEY = 'UCLPermissions_View - Parcel Zone Rates' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pre-Receipt Eligibility Report', VALUE = 'View Pre-Receipt Eligibility Report' where KEY = 'UCLPermissions_View Pre-receipt eligibility report' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Retail Route (VST)', VALUE = 'View Retail Route (VST)' where KEY = 'UCLPermissions_Transportation - View Retail Route' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Yard Tasks', VALUE = 'View Yard Tasks' where KEY = 'UCLPermissions_Yard Management - View Tasks' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Trailer', VALUE = 'View Trailer' where KEY = 'UCLPermissions_Yard Management - View Trailer' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF View Task', VALUE = 'RF View Task' where KEY = 'UCLPermissions_Yard Management - RF View Task' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Weigh LPN', VALUE = 'Weigh LPN' where KEY = 'UCLPermissions_Manifest - Weigh LPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Print 214 Attachments', VALUE = 'Administer Print 214 Attachments' where KEY = 'UCLPermissions_Admin Print 214 Attachments' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Assign Adjustment Reason Codes', VALUE = 'Administer Assign Adjustment Reason Codes' where KEY = 'UCLPermissions_Admin Assign Adjustment Reason Codes' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Activity Tracking', VALUE = 'Administer Activity Tracking' where KEY = 'UCLPermissions_Sys Ctrl - Administer Activity tracking' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Address', VALUE = 'Administer Address' where KEY = 'UCLPermissions_Admin Address' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Application Identifier Tran Config', VALUE = 'Administer Application Identifier Tran Config' where KEY = 'UCLPermissions_Sys Ctrl - Administer Appl Identifier Tran Conf' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Allocate Inventory', VALUE = 'Administer Allocate Inventory' where KEY = 'UCLPermissions_Allocation - Administer Allocate Inventory' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Allocate Inventory With BOM', VALUE = 'Administer Allocate Inventory With BOM' where KEY = 'UCLPermissions_Admin Allocate Inventory with BOM' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Allocate Inventory With BOM And Tasking', VALUE = 'Administer Allocate Inventory With BOM And Tasking' where KEY = 'UCLPermissions_Admin Allocation Inventory with BOM with Tasking' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Allocate Inventory With Tasking', VALUE = 'Administer Allocate Inventory With Tasking' where KEY = 'UCLPermissions_Admin Allocation Inventory with Tasking' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Allocation Parameters', VALUE = 'Administer Allocation Parameters' where KEY = 'UCLPermissions_Allocation - Administer Allocation Parms' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Allocation Rules', VALUE = 'Administer Allocation Rules' where KEY = 'UCLPermissions_Allocation - Administer Allocation Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Appointment Door Priorities', VALUE = 'Administer Appointment Door Priorities' where KEY = 'UCLPermissions_Admin Appointment Door Priorities' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Application Identifier', VALUE = 'Administer Application Identifier' where KEY = 'UCLPermissions_Sys Ctrl - Administer Application identifier' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Application Support', VALUE = 'Administer Application Support' where KEY = 'UCLPermissions_Admin Application Support' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Appointment', VALUE = 'Administer Appointment' where KEY = 'UCLPermissions_WM Admin Appointment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Appointment Schedule', VALUE = 'Administer Appointment Schedule' where KEY = 'UCLPermissions_Admin Appointment Schedule' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer AR Account', VALUE = 'Administer AR Account' where KEY = 'UCLPermissions_Sys Ctrl - Administer AR Account' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer ARN Events', VALUE = 'Administer ARN Events' where KEY = 'UCLPermissions_Smartlabels - Administer ARN Event' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer ARN Reports', VALUE = 'Administer ARN Reports' where KEY = 'UCLPermissions_Reports - Administer ARN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Appointment Schedule Parameters', VALUE = 'Administer Appointment Schedule Parameters' where KEY = 'UCLPermissions_Admin Appointment Schedule Parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer ASN Inquiry Reports', VALUE = 'Administer ASN Inquiry Reports' where KEY = 'UCLPermissions_Reports - Administer ASN Inquiry' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer ASN Variance Reports', VALUE = 'Administer ASN Variance Reports' where KEY = 'UCLPermissions_Reports - Administer ASN Variance' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Assign Item', VALUE = 'Administer Assign Item' where KEY = 'UCLPermissions_Admin - Assign Item' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Ship Confirm', VALUE = 'Administer Ship Confirm' where KEY = 'UCLPermissions_Outbound - Administer Ship Confirm' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Lot Master', VALUE = 'Administer Lot Master' where KEY = 'UCLPermissions_Inv Mgmt - Administer Lot Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer WM Business Partners', VALUE = 'Administer WM Business Partners' where KEY = 'UCLPermissions_MSF - Administer Business Partners' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Blocked Location Report', VALUE = 'Administer Blocked Location Report' where KEY = 'UCLPermissions_Reports - Administer Blocked Location' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Bill Of Materials', VALUE = 'Administer Bill Of Materials' where KEY = 'UCLPermissions_Admin BOM' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Barcode Type Master', VALUE = 'Administer Barcode Type Master' where KEY = 'UCLPermissions_Sys Ctrl - Administer Barocde Type master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Input Bridge Error', VALUE = 'Administer Input Bridge Error' where KEY = 'UCLPermissions_Admin Input Bridge Error' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Bridge Exclude Table', VALUE = 'Administer Bridge Exclude Table' where KEY = 'UCLPermissions_Admin Bridge exclude table' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Carrier Discount Inquiry', VALUE = 'Administer Carrier Discount Inquiry' where KEY = 'UCLPermissions_Admin Carrier discount inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Carrier', VALUE = 'Administer Carrier' where KEY = 'UCLPermissions_Admin Carrier' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Carrier Toolkit', VALUE = 'Administer Carrier Toolkit' where KEY = 'UCLPermissions_Admin Carrier Toolkit' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer oLPN Maintenance', VALUE = 'Administer oLPN Maintenance' where KEY = 'UCLPermissions_Admin Carton Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Carrier Exclusion Inquiry', VALUE = 'Administer Carrier Exclusion Inquiry' where KEY = 'UCLPermissions_Admin Carrier exclusion inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Create Cycle Count Task', VALUE = 'Administer Create Cycle Count Task' where KEY = 'UCLPermissions_Inv Mgmt Admin Create Cycle Count Task' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Cycle Count History', VALUE = 'Administer Cycle Count History' where KEY = 'UCLPermissions_Admin Cycle count history' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Cycle Count Trigger', VALUE = 'Administer Cycle Count Trigger' where KEY = 'UCLPermissions_Counting - Administer Cycle Count trigger' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Cycle Count Task Rules', VALUE = 'Administer Cycle Count Task Rules' where KEY = 'UCLPermissions_Counting - Administer Cycle Count Task Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Cycle Count Location Variance Report', VALUE = 'Administer Cycle Count Location Variance Report' where KEY = 'UCLPermissions_Reports - Administer Cyc Cnt Var (Location)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Cycle Count Item Variance Detail Report', VALUE = 'Administer Cycle Count Item Variance Detail Report' where KEY = 'UCLPermissions_Reports - Administer Cyc Cnt Var (Dtl By Item)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Cycle Count Item Variance Report', VALUE = 'Administer Cycle Count Item Variance Report' where KEY = 'UCLPermissions_Reports - Administer Cyc Cnt Var (Item)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer LPN Disposition Determination Rules', VALUE = 'Administer LPN Disposition Determination Rules' where KEY = 'UCLPermissions_Putaway - Administer LPN Disp Detrm Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Chute Priority Maintenance', VALUE = 'Administer Chute Priority Maintenance' where KEY = 'UCLPermissions_Waves - Administer Chute Priority Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Chute Master', VALUE = 'Administer Chute Master' where KEY = 'UCLPermissions_Waves - Administer Chute Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Close ASN/Shipment', VALUE = 'Administer Close ASN/Shipment' where KEY = 'UCLPermissions_Administer Close Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Show Cancel Shorts Button', VALUE = 'Show Cancel Shorts Button' where KEY = 'UCLPermissions_Show Cancel Shorts button' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Configuration Tool', VALUE = 'Administer Configuration Tool' where KEY = 'UCLPermissions_Config Tool - Administer Config Tool' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Country', VALUE = 'Administer Country' where KEY = 'UCLPermissions_Sys Ctrl - Administer Country' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Collate Reports', VALUE = 'Administer Collate Reports' where KEY = 'UCLPermissions_Reports - Administer Collate' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Companies', VALUE = 'Administer Companies' where KEY = 'UCLPermissions_MSF - Administer Companies' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Common Screens', VALUE = 'Administer Common Screens' where KEY = 'UCLPermissions_Admin Common Screens' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Configuration Tool Maintenance', VALUE = 'Administer Configuration Tool Maintenance' where KEY = 'UCLPermissions_Sys Ctrl - Administer Configuration Tool Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Create ASN From PO', VALUE = 'Administer Create ASN From PO' where KEY = 'UCLPermissions_Admin Create IBShipment fom PO' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer iLPN Size Type', VALUE = 'Administer iLPN Size Type' where KEY = 'UCLPermissions_WM Admin Case Size Type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Cubiscan Entry', VALUE = 'Administer Cubiscan Entry' where KEY = 'UCLPermissions_Inv Mgmt - Administer Cubiscan Entry' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Carton Weight Freight Manifest', VALUE = 'Administer Carton Weight Freight Manifest' where KEY = 'UCLPermissions_Admin Carton weight freight manifest' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Cycle Count', VALUE = 'Administer Cycle Count' where KEY = 'UCLPermissions_Counting - Administer Cycle Count' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Add Lot Master', VALUE = 'Add Lot Master' where KEY = 'UCLPermissions_Inv Mgmt - Add Lot Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Close ASN', VALUE = 'Administer Close ASN' where KEY = 'UCLPermissions_Admin - Close Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Dock/Door Availability', VALUE = 'Administer Dock/Door Availability' where KEY = 'UCLPermissions_Admin Dock/Door Availability' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Default HTS', VALUE = 'Administer Default HTS' where KEY = 'UCLPermissions_Admin Default HTS' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer MHE Divert History', VALUE = 'Administer MHE Divert History' where KEY = 'UCLPermissions_MHE - Administer Divert history' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer DO Destination Information', VALUE = 'Administer DO Destination Information' where KEY = 'UCLPermissions_Manifest - Admin DO Destination Information' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Dock/Door', VALUE = 'Administer Dock/Door' where KEY = 'UCLPermissions_Admin Dock/Door' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Permission For EIS Endpoint', VALUE = 'Administer Permission For EIS Endpoint' where KEY = 'UCLPermissions_Admin Permission for EIS Endpt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Permission For EIS Message Q', VALUE = 'Administer Permission For EIS Message Q' where KEY = 'UCLPermissions_Admin Permission for EIS Message Q' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Enterprise ID', VALUE = 'Administer Enterprise ID' where KEY = 'UCLPermissions_Admin Enterprise ID ' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Enterprise Master', VALUE = 'Administer Enterprise Master' where KEY = 'UCLPermissions_Admin Enterprise Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer EPC', VALUE = 'Administer EPC' where KEY = 'UCLPermissions_Inv Mgmt - Administer EPC' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Equipment Master', VALUE = 'Administer Equipment Master' where KEY = 'UCLPermissions_Admin Equipment Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer All', VALUE = 'Administer All' where KEY = 'UCLPermissions_E-Signature - Administer All' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer RF ASN Verification', VALUE = 'Administer RF ASN Verification' where KEY = 'UCLPermissions_E-Signature - Administer RF ASN Verification' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer UI ASN Verification', VALUE = 'Administer UI ASN Verification' where KEY = 'UCLPermissions_E-Signature - Administer UI ASN Verification' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Batch Inquiry', VALUE = 'Administer Batch Inquiry' where KEY = 'UCLPermissions_E-Signature - Administer Batch Inquiry' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer RF Cycle Count', VALUE = 'Administer RF Cycle Count' where KEY = 'UCLPermissions_E-Signature - Administer RF Cycle Count' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer UI Cycle Count', VALUE = 'Administer UI Cycle Count' where KEY = 'UCLPermissions_E-Signature - Administer UI Cycle Count' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Cancel oLPN', VALUE = 'Administer Cancel oLPN' where KEY = 'UCLPermissions_E-Signature - Administer Cancel oLPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Inventory Adjustment', VALUE = 'Administer Inventory Adjustment' where KEY = 'UCLPermissions_E-Signature - Administer Inventory Adjustment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Lock Unlock Container', VALUE = 'Administer Lock Unlock Container' where KEY = 'UCLPermissions_E-Signature - Administer Lock Unlock Container' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Modify iLPN', VALUE = 'Administer Modify iLPN' where KEY = 'UCLPermissions_E-Signature - Administer Modify iLPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer RF Physical Count', VALUE = 'Administer RF Physical Count' where KEY = 'UCLPermissions_E-Signature - Administer RF Physical Count' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer UI Physical Count', VALUE = 'Administer UI Physical Count' where KEY = 'UCLPermissions_E-Signature - Administer UI Physical Count' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Split Move iLPN', VALUE = 'Administer Split Move iLPN' where KEY = 'UCLPermissions_E-Signature - Administer Split Move iLPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Event Messages', VALUE = 'Administer Event Messages' where KEY = 'UCLPermissions_Interface - Administer Event Message' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Exp Date Warning Report (WMAEXPRT)', VALUE = 'Administer Exp Date Warning Report (WMAEXPRT)' where KEY = 'UCLPermissions_Admin Expiration date warning report (print)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Exp Date Warning Report (WMAEXRPT)', VALUE = 'Administer Exp Date Warning Report (WMAEXRPT)' where KEY = 'UCLPermissions_WMAEXRPT - Need to change the name' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer FedEx Transaction', VALUE = 'Administer FedEx Transaction' where KEY = 'UCLPermissions_Admin Fedex Tran' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Final Receiving Return Disposition', VALUE = 'Administer Final Receiving Return Disposition' where KEY = 'UCLPermissions_Receiving - Administer Final Return Disposition' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Final Receiving Return Disp Detrm', VALUE = 'Administer Final Receiving Return Disp Detrm' where KEY = 'UCLPermissions_Receiving - Administer Final Return Disp Detrm' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Frozen Location Detail Report', VALUE = 'Administer Frozen Location Detail Report' where KEY = 'UCLPermissions_Reports - Administer Frozen Locn Detail' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Receive ASN (FS)', VALUE = 'Administer Receive ASN (FS)' where KEY = 'UCLPermissions_Receiving - Administer Receive IBShipment (FS)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Fixed Station Receiving', VALUE = 'Administer Fixed Station Receiving' where KEY = 'UCLPermissions_Receiving - Administer FS Receiving' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer FTZ Batch Parameters', VALUE = 'Administer FTZ Batch Parameters' where KEY = 'UCLPermissions_Admin FTZ Batch parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer FTZ Confirm/Undo Layers', VALUE = 'Administer FTZ Confirm/Undo Layers' where KEY = 'UCLPermissions_Admin FTZ Confirm/undo layers' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer FTZ Control', VALUE = 'Administer FTZ Control' where KEY = 'UCLPermissions_Admin FTZ Control' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer FTZ Commercial Invoice', VALUE = 'Administer FTZ Commercial Invoice' where KEY = 'UCLPermissions_Admin FTZ Commercial invoice' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer FTZ Consolidate Layers', VALUE = 'Administer FTZ Consolidate Layers' where KEY = 'UCLPermissions_Admin FTZ Consolidate layers' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer FTZ Create Layers', VALUE = 'Administer FTZ Create Layers' where KEY = 'UCLPermissions_Admin FTZ Create Layers' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer FTZ Case Receipts', VALUE = 'Administer FTZ Case Receipts' where KEY = 'UCLPermissions_Admin FTZ Case receipts' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Global Lot Recall', VALUE = 'Administer Global Lot Recall' where KEY = 'UCLPermissions_Admin - Global Lot Recall' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Check In/Out', VALUE = 'Administer Check In/Out' where KEY = 'UCLPermissions_Admin Guard Check In/Out' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Generate 605 PIX', VALUE = 'Administer Generate 605 PIX' where KEY = 'UCLPermissions_Inv Mgmt - Administer Generate 605 PIX' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Hazardous Material Code', VALUE = 'Administer Hazardous Material Code' where KEY = 'UCLPermissions_Admin Hazardous Material Code' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Hazardous Material Ship Via Exclusions', VALUE = 'Administer Hazardous Material Ship Via Exclusions' where KEY = 'UCLPermissions_Admin-Hazardous Material Ship Via Exclusions' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer HTS Descriptions', VALUE = 'Administer HTS Descriptions' where KEY = 'UCLPermissions_Admin HTS Descriptions' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer HTS Execution', VALUE = 'Administer HTS Execution' where KEY = 'UCLPermissions_Admin HTS execution' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer HTS Table', VALUE = 'Administer HTS Table' where KEY = 'UCLPermissions_Admin HTS Table' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Hub Master', VALUE = 'Administer Hub Master' where KEY = 'UCLPermissions_Admin Hub Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Inventory Allocation Priority', VALUE = 'Administer Inventory Allocation Priority' where KEY = 'UCLPermissions_Allocation - Administer Invn Alloc Priorities' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Inbound Quality Audit', VALUE = 'Administer Inbound Quality Audit' where KEY = 'UCLPermissions_Pre-Recv - Administer Inbound Quality Audit' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer ASN', VALUE = 'Administer ASN' where KEY = 'UCLPermissions_Admin IBShipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Immediate Needs', VALUE = 'Administer Immediate Needs' where KEY = 'UCLPermissions_Pre-Recv - Administer Immediate Needs' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Inventory Adjustment Report', VALUE = 'Administer Inventory Adjustment Report' where KEY = 'UCLPermissions_Reports - Administer Inventory Adjustment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Insurance Type', VALUE = 'Administer Insurance Type' where KEY = 'UCLPermissions_Admin Insurance type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Inventory Need Type', VALUE = 'Administer Inventory Need Type' where KEY = 'UCLPermissions_Allocation - Administer Inventory Need Type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Interface Configurations', VALUE = 'Administer Interface Configurations' where KEY = 'UCLPermissions_Interface - Administer Interface Configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Inventory Reconciliation', VALUE = 'Administer Inventory Reconciliation' where KEY = 'UCLPermissions_Inv Mgmt - Administer Inventory Reconciliation' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Inventory Reconciliation Report', VALUE = 'Administer Inventory Reconciliation Report' where KEY = 'UCLPermissions_Reports - Administer Inventory Reconciliation' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer WM Items', VALUE = 'Administer WM Items' where KEY = 'UCLPermissions_Admin Item Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Inventory', VALUE = 'Administer Item Inventory' where KEY = 'UCLPermissions_Inv Mgmt - Administer Item Inventory' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Grouping', VALUE = 'Administer Item Grouping' where KEY = 'UCLPermissions_Admin Item Grouping' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Grouping Wizard', VALUE = 'Administer Item Grouping Wizard' where KEY = 'UCLPermissions_Admin Item Grouping Wizard' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Inventory By Category', VALUE = 'Administer Item Inventory By Category' where KEY = 'UCLPermissions_Inv Mgmt - Administer Item Inv By Category' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Inventory By Location', VALUE = 'Administer Item Inventory By Location' where KEY = 'UCLPermissions_Inv Mgmt - Administer Item Invn by Location' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Substitution', VALUE = 'Administer Item Substitution' where KEY = 'UCLPermissions_Inv Mgmt - Administer Item Substitution' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Master Profile', VALUE = 'Administer Item Master Profile' where KEY = 'UCLPermissions_Admin Item Master Profile' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_iLPN Exceeds Standard Quantity Report', VALUE = 'iLPN Exceeds Standard Quantity Report' where KEY = 'UCLPermissions_Reports - iLPN Exceeds standard quantity' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Journal Configuration', VALUE = 'Administer Journal Configuration' where KEY = 'UCLPermissions_Admin Journal Configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Layer Adjustment', VALUE = 'Administer Layer Adjustment' where KEY = 'UCLPermissions_Admin Layer adjustment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Layer Adjustment Process', VALUE = 'Administer Layer Adjustment Process' where KEY = 'UCLPermissions_Admin Layer adjustment process' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Layer', VALUE = 'Administer Layer' where KEY = 'UCLPermissions_Admin Layer' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Label Translations', VALUE = 'Administer Label Translations' where KEY = 'UCLPermissions_Admin Label Translations' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Labor Plan Inquiry', VALUE = 'Administer Labor Plan Inquiry' where KEY = 'UCLPermissions_Admin Labor Plan Inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Locked iLPN Report', VALUE = 'Administer Locked iLPN Report' where KEY = 'UCLPermissions_Reports - Administer Locked iLPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer LM Application Criteria Detrm Rules', VALUE = 'Administer LM Application Criteria Detrm Rules' where KEY = 'UCLPermissions_Labor - Administer LM Appl crit detrm rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer LM Monitor Defaults', VALUE = 'Administer LM Monitor Defaults' where KEY = 'UCLPermissions_Labor - Administer LM Monitor defaults' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer LM Handling Attribute', VALUE = 'Administer LM Handling Attribute' where KEY = 'UCLPermissions_Labor - Administer LM Handling attribute' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer LM Message', VALUE = 'Administer LM Message' where KEY = 'UCLPermissions_Labor - Administer LM Message' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer LM Performance', VALUE = 'Administer LM Performance' where KEY = 'UCLPermissions_Admin LM Performance' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Load', VALUE = 'Administer Load' where KEY = 'UCLPermissions_Admin Load' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Frozen Not Counted Location Report', VALUE = 'Administer Frozen Not Counted Location Report' where KEY = 'UCLPermissions_Reports - Administer Locn Frozn Not Cnted' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Location Wizard', VALUE = 'Administer Location Wizard' where KEY = 'UCLPermissions_Locations - Administer Location Wizard' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer iLPN', VALUE = 'Administer iLPN' where KEY = 'UCLPermissions_Admin Case' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer LTL oLPN', VALUE = 'Administer LTL oLPN' where KEY = 'UCLPermissions_Admin LTL Carton' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Literal Translations', VALUE = 'Administer Literal Translations' where KEY = 'UCLPermissions_Admin Literal Translations' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Lean Time Replenishment', VALUE = 'Administer Lean Time Replenishment' where KEY = 'UCLPermissions_Inv Mgmt - Administer Lean Time Replenishment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Lean Time Replenishment Rules', VALUE = 'Administer Lean Time Replenishment Rules' where KEY = 'UCLPermissions_LeanTimeRepl-Administer LeanTimeRepl Configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Layer Creation Report', VALUE = 'Administer Layer Creation Report' where KEY = 'UCLPermissions_Admin Layer creation report' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer WM Menu Maintenance', VALUE = 'Administer WM Menu Maintenance' where KEY = 'UCLPermissions_Sys Ctrl - Administer Menu Maintenance' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer MHE Message Format', VALUE = 'Administer MHE Message Format' where KEY = 'UCLPermissions_MHE - Administer MHE Message Format' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer WM MHE Rule Group Configuration', VALUE = 'Administer WM MHE Rule Group Configuration' where KEY = 'UCLPermissions_MHE - Administer WM MHE Rule Group Configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer WM MHE Rule Group', VALUE = 'Administer WM MHE Rule Group' where KEY = 'UCLPermissions_MHE - Administer WM MHE Rule Group' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Parcel And LTL Manifesting And Loading', VALUE = 'Administer Parcel And LTL Manifesting And Loading' where KEY = 'UCLPermissions_Admin Parcel and LTL manifesting and loading' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Modify oLPN Contents', VALUE = 'Administer Modify oLPN Contents' where KEY = 'UCLPermissions_Outbound - Administer Modify oLPN contents' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Message Log', VALUE = 'Administer Message Log' where KEY = 'UCLPermissions_Sys Ctrl - Administer Message Log' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Message Master', VALUE = 'Administer Message Master' where KEY = 'UCLPermissions_Sys Ctrl - Administer Message Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Send Message To RF User', VALUE = 'Administer Send Message To RF User' where KEY = 'UCLPermissions_Sys Ctrl - Administer Send Message To RF User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Mass Update Item Master', VALUE = 'Administer Mass Update Item Master' where KEY = 'UCLPermissions_Admin Mass update item master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Nextup Numbers', VALUE = 'Administer Nextup Numbers' where KEY = 'UCLPermissions_Sys Ctrl - Administer Nextup numbers' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Outbound Pallet Maintenance', VALUE = 'Administer Outbound Pallet Maintenance' where KEY = 'UCLPermissions_Admin Outbound pallet maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Outbound Quality Audit', VALUE = 'Administer Outbound Quality Audit' where KEY = 'UCLPermissions_Outbound - Administer OB Quality Audit' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Outbound QA Parameters', VALUE = 'Administer Outbound QA Parameters' where KEY = 'UCLPermissions_Sys Ctrl - Administer OBQA Parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Outbound Rule', VALUE = 'Administer Outbound Rule' where KEY = 'UCLPermissions_Admin Outbound Rule' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Outbound Shipment', VALUE = 'Administer Outbound Shipment' where KEY = 'UCLPermissions_Admin OB Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Order Report', VALUE = 'Administer Order Report' where KEY = 'UCLPermissions_Reports- Administer Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Order Rounding', VALUE = 'Administer Order Rounding' where KEY = 'UCLPermissions_Outbound - Administer Order rounding' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Outward Events', VALUE = 'Administer Outward Events' where KEY = 'UCLPermissions_Sys Ctrl - Administer Outward Events' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Outward Event Rules', VALUE = 'Administer Outward Event Rules' where KEY = 'UCLPermissions_Sys Ctrl - Administer Outward Event Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Outward Generated Events', VALUE = 'Administer Outward Generated Events' where KEY = 'UCLPermissions_Sys Ctrl - Administer Outward Generated Events' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Packing Slip Report', VALUE = 'Administer Packing Slip Report' where KEY = 'UCLPermissions_Reports - Administer Packing Slip' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Partial Shorted Order Report', VALUE = 'Administer Partial Shorted Order Report' where KEY = 'UCLPermissions_Reports - Administer Partial Shorted Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Order Consolidation Detrm Type Wizard', VALUE = 'Administer Order Consolidation Detrm Type Wizard' where KEY = 'UCLPermissions_Waves - Administer Ord Consol Detrm Type Wiz' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Order Consolidation Detrm Type Maint', VALUE = 'Administer Order Consolidation Detrm Type Maint' where KEY = 'UCLPermissions_Waves - Administer Ord Consol Detrm Type Mnt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Order Consolidation Location Maint', VALUE = 'Administer Order Consolidation Location Maint' where KEY = 'UCLPermissions_Waves- Administer Order Consol Locn Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Order Consolidation Parameters', VALUE = 'Administer Order Consolidation Parameters' where KEY = 'UCLPermissions_Waves - Administer Order Consol Parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Order Consolidation Profile', VALUE = 'Administer Order Consolidation Profile' where KEY = 'UCLPermissions_Waves - Administer Order Consol Profile' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Physical Count Reconciliation Report', VALUE = 'Administer Physical Count Reconciliation Report' where KEY = 'UCLPermissions_Reports - Administer Phy Count Recon (Srl Nbr)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pick Cart Contents', VALUE = 'Administer Pick Cart Contents' where KEY = 'UCLPermissions_Reports - Administer Pick Cart Contents' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Physical Count LPN Variance Report', VALUE = 'Administer Physical Count LPN Variance Report' where KEY = 'UCLPermissions_Reports - Administer Phy Count Var (LPN Booking)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Physical Count Non LPN Variance Report', VALUE = 'Administer Physical Count Non LPN Variance Report' where KEY = 'UCLPermissions_Reports - Administer Phy Cnt Var(Not tracking LPN)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pick Location Detail Properties', VALUE = 'Administer Pick Location Detail Properties' where KEY = 'UCLPermissions_Admin - Pick Location Detail Properties' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pick Location Header Properties', VALUE = 'Administer Pick Location Header Properties' where KEY = 'UCLPermissions_Admin - Pick Location Header Properties' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pack and Hold Parameters Template', VALUE = 'Administer Pack and Hold Parameters Template' where KEY = 'UCLPermissions_Waves - Administer Pack' ||'&'|| 'Hold parm template' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Physical Count', VALUE = 'Administer Physical Count' where KEY = 'UCLPermissions_Counting - Administer Physical Count' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer PIX Schedule Export', VALUE = 'Administer PIX Schedule Export' where KEY = 'UCLPermissions_Inv Mgmt - Administer PIX Schedule Export' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer PIX Export Configuration', VALUE = 'Administer PIX Export Configuration' where KEY = 'UCLPermissions_Admin Pix Export Config' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer PIX History Report', VALUE = 'Administer PIX History Report' where KEY = 'UCLPermissions_Reports - Administer PIX History' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer PIX Transaction Code', VALUE = 'Administer PIX Transaction Code' where KEY = 'UCLPermissions_Inv Mgmt - Administer PIX transaction Code' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pack oLPN And Verify', VALUE = 'Administer Pack oLPN And Verify' where KEY = 'UCLPermissions_Outbound - Administer Pack oLPN and verify' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pick Cart Plan ID', VALUE = 'Administer Pick Cart Plan ID' where KEY = 'UCLPermissions_Outbound - Administer Pick cart PlanID' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pack Efficiency', VALUE = 'Administer Pack Efficiency' where KEY = 'UCLPermissions_Waves - Administer Pack Efficiency' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Order Profile Maintenance', VALUE = 'Administer Order Profile Maintenance' where KEY = 'UCLPermissions_Admin Pkt Profile Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Picking Short Items', VALUE = 'Administer Picking Short Items' where KEY = 'UCLPermissions_Outbound - Administer Picking Short Items' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pack Station', VALUE = 'Administer Pack Station' where KEY = 'UCLPermissions_Waves - Administer Pack Station' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Order Inquiry', VALUE = 'Administer Order Inquiry' where KEY = 'UCLPermissions_Admin Pickticket inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pickup Task Rules', VALUE = 'Administer Pickup Task Rules' where KEY = 'UCLPermissions_Admin - Pickup Task Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pack Wave Parameters Template', VALUE = 'Administer Pack Wave Parameters Template' where KEY = 'UCLPermissions_Waves - Administer Pack Wave Parm Template' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pallet', VALUE = 'Administer Pallet' where KEY = 'UCLPermissions_Admin Pallet' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Putaway Method Zone Priority', VALUE = 'Administer Putaway Method Zone Priority' where KEY = 'UCLPermissions_Putaway - Administer PA Method - Zone Priority' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer PO', VALUE = 'Administer PO' where KEY = 'UCLPermissions_Admin PO' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer PO Consolidation Exclusion', VALUE = 'Administer PO Consolidation Exclusion' where KEY = 'UCLPermissions_Outbound - Administer PO Consol Exclusion' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Print Queue/Service Assignment', VALUE = 'Administer Print Queue/Service Assignment' where KEY = 'UCLPermissions_Admin Print queue/service assignment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Parcel Hazardous Materials', VALUE = 'Administer Parcel Hazardous Materials' where KEY = 'UCLPermissions_Admin-Parcel Hazardous Materials' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Parcel Manifests', VALUE = 'Administer Parcel Manifests' where KEY = 'UCLPermissions_Admin Parcel Manifestes' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Parcel Rate Import', VALUE = 'Administer Parcel Rate Import' where KEY = 'UCLPermissions_Admin Parcel rate import' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer PreWave', VALUE = 'Administer PreWave' where KEY = 'UCLPermissions_Waves - Administer PreWave' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Purge System Table', VALUE = 'Administer Purge System Table' where KEY = 'UCLPermissions_Sys Ctrl - Administer Purge system table' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Print Requestor/Service Assignment', VALUE = 'Administer Print Requestor/Service Assignment' where KEY = 'UCLPermissions_Admin Print requestor/service assignment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Print Blind iLPN Labels', VALUE = 'Administer Print Blind iLPN Labels' where KEY = 'UCLPermissions_Sys Ctrl - Administer Print blind labels - iLPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Location Label Printing', VALUE = 'Administer Location Label Printing' where KEY = 'UCLPermissions_Inv Mgmt - Administer Print Location Label' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Printer Location', VALUE = 'Administer Printer Location' where KEY = 'UCLPermissions_Admin Printer Locn' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Print Queue Master', VALUE = 'Administer Print Queue Master' where KEY = 'UCLPermissions_Admin Print queue master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pre-Wave', VALUE = 'Administer Pre-Wave' where KEY = 'UCLPermissions_Waves - Administer Pre-wave' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Putaway Configuration', VALUE = 'Administer Putaway Configuration' where KEY = 'UCLPermissions_Putaway - Administer Putaway Configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Putaway Override Report', VALUE = 'Administer Putaway Override Report' where KEY = 'UCLPermissions_Reports - Administer Putaway Override' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Putaway Report', VALUE = 'Administer Putaway Report' where KEY = 'UCLPermissions_Reports - Administer Putaway' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Putaway Override Report (WMAPUTREP)', VALUE = 'Administer Putaway Override Report (WMAPUTREP)' where KEY = 'UCLPermissions_WMAPUTREP - Need to change the name' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer PIX History', VALUE = 'Administer PIX History' where KEY = 'UCLPermissions_Inv Mgmt - Administer PIX History' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer PIX Transactions', VALUE = 'Administer PIX Transactions' where KEY = 'UCLPermissions_Inv Mgmt - Administer PIX Transaction' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pick Zone Assignment', VALUE = 'Administer Pick Zone Assignment' where KEY = 'UCLPermissions_Waves - Administer Pick Zone Assignment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Putaway Zone Utilization Report', VALUE = 'Administer Putaway Zone Utilization Report' where KEY = 'UCLPermissions_Reports- Administer PA Zone Utilization' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Question Determination Rules', VALUE = 'Administer Question Determination Rules' where KEY = 'UCLPermissions_Pre-Recv - Administer Question Detrm Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Question Answer History', VALUE = 'Administer Question Answer History' where KEY = 'UCLPermissions_Pre-Recv - Administer Question answer history' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Quality Inspection Rules', VALUE = 'Administer Quality Inspection Rules' where KEY = 'UCLPermissions_Receiving - Administer Quality Inspection Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Question Master', VALUE = 'Administer Question Master' where KEY = 'UCLPermissions_Pre-Recv - Administer Question Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Query Builder', VALUE = 'Administer Query Builder' where KEY = 'UCLPermissions_Admin Query builder' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Quality Verification Question Detrm', VALUE = 'Administer Quality Verification Question Detrm' where KEY = 'UCLPermissions_Receiving - Administer Quality Verf Quest Detrm' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Rebuild Wizard', VALUE = 'Administer Rebuild Wizard' where KEY = 'UCLPermissions_Sys Ctrl - Administer Rebuild Wizard' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Recall Inventory', VALUE = 'Administer Recall Inventory' where KEY = 'UCLPermissions_Inv Mgmt - Administer Recall Inventory' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Receiving Activity Report', VALUE = 'Administer Receiving Activity Report' where KEY = 'UCLPermissions_Reports - Administer Receiving Act Rpt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Recall Inventory Report', VALUE = 'Administer Recall Inventory Report' where KEY = 'UCLPermissions_Reports - Administer Recall Inventory' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Remove Pick Location Dedication Rules', VALUE = 'Administer Remove Pick Location Dedication Rules' where KEY = 'UCLPermissions_Admin - Remove Pick Location Dedication' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Add iLPN To Order', VALUE = 'RF Add iLPN To Order' where KEY = 'UCLPermissions_RF Add iLPN to Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer RFID Profile', VALUE = 'Administer RFID Profile' where KEY = 'UCLPermissions_WM Admin RFID profile' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF LM Non-Standard Labor', VALUE = 'RF LM Non-Standard Labor' where KEY = 'UCLPermissions_RF LM Non-Standarad Labor' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Lock/Unlock Outbound Container', VALUE = 'RF Lock/Unlock Outbound Container' where KEY = 'UCLPermissions_RF OB Lock/Unlock Container' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Locate Trailer', VALUE = 'RF Locate Trailer' where KEY = 'UCLPermissions_Locate Trailer' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Directed Putaway - iLPN', VALUE = 'RF Directed Putaway - iLPN' where KEY = 'UCLPermissions_RF Direct Putaway - iLPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Pack iLPN From TI', VALUE = 'RF Pack iLPN From TI' where KEY = 'UCLPermissions_RF Pack iLPN from TI' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Pack Cubed Directed', VALUE = 'RF Pack Cubed Directed' where KEY = 'UCLPermissions_RF Pack cubed directed' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Pack Cubed Non-Directed', VALUE = 'RF Pack Cubed Non-Directed' where KEY = 'UCLPermissions_RF Pack cubed non-directed' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Pack iLPN From Active', VALUE = 'RF Pack iLPN From Active' where KEY = 'UCLPermissions_RF Pack iLPN from Active' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Pack iLPN From Case Pick', VALUE = 'RF Pack iLPN From Case Pick' where KEY = 'UCLPermissions_RF Pack iLPN from Case Pick' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Pack Non-Cubed Directed', VALUE = 'RF Pack Non-Cubed Directed' where KEY = 'UCLPermissions_RF Pack non-cubed directed' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Pack Pick Cart', VALUE = 'RF Pack Pick Cart' where KEY = 'UCLPermissions_RF Pack pick cart' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Reprint iLPN', VALUE = 'RF Reprint iLPN' where KEY = 'UCLPermissions_Administer RF Reprint iLPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Sort iLPN Consolidation', VALUE = 'RF Sort iLPN Consolidation' where KEY = 'UCLPermissions_RF Sort iLPN - Consolidation' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Reserve Invn w/o Active Locn Report', VALUE = 'Administer Reserve Invn w/o Active Locn Report' where KEY = 'UCLPermissions_Reports - Administer Resv Invn w/o Act locn' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Show Roll-Over Shorts Button', VALUE = 'Show Roll-Over Shorts Button' where KEY = 'UCLPermissions_Show Roll-Over Shorts button' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Route Change Report', VALUE = 'Administer Route Change Report' where KEY = 'UCLPermissions_Reports - Administer Route Change' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Receiving Activity Report (WMARPRPT)', VALUE = 'Administer Receiving Activity Report (WMARPRPT)' where KEY = 'UCLPermissions_Reports - Administer Receiving Activity' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Retail Pre-Wave', VALUE = 'Administer Retail Pre-Wave' where KEY = 'UCLPermissions_Admin Retail pre-wave' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Report Translations', VALUE = 'Administer Report Translations' where KEY = 'UCLPermissions_Reports - Administer Report translations' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Dynamic Routing Requests', VALUE = 'Administer Dynamic Routing Requests' where KEY = 'UCLPermissions_Admin Dynamic Routing Requests' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Rate Shop Group Inquiry', VALUE = 'Administer Rate Shop Group Inquiry' where KEY = 'UCLPermissions_Admin Rate shop group inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Routing Guide (WMARTGUIDE)', VALUE = 'Administer Routing Guide (WMARTGUIDE)' where KEY = 'UCLPermissions_Admin Routeing guide' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Returnables Inbound History', VALUE = 'Administer Returnables Inbound History' where KEY = 'UCLPermissions_Outbound - Administer Returnables IB Hist' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Returnables Inbound Summary', VALUE = 'Administer Returnables Inbound Summary' where KEY = 'UCLPermissions_Outbound - Administer Returnables IB Summary' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Retail Route (WMARTLRT)', VALUE = 'Administer Retail Route (WMARTLRT)' where KEY = 'UCLPermissions_Admin Retail route' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Retail Route Check', VALUE = 'Administer Retail Route Check' where KEY = 'UCLPermissions_Admin Retail route check' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Retail Route Rule Definition', VALUE = 'Administer Retail Route Rule Definition' where KEY = 'UCLPermissions_Admin Retail route rule definition' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Retail Route Parameters', VALUE = 'Administer Retail Route Parameters' where KEY = 'UCLPermissions_Admin Retail route Parm' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Retail Route Time', VALUE = 'Administer Retail Route Time' where KEY = 'UCLPermissions_Admin Retail route time' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Returnables Outbound History', VALUE = 'Administer Returnables Outbound History' where KEY = 'UCLPermissions_Outbound - Administer Reurnables OB history' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Returnables Outbound Summary', VALUE = 'Administer Returnables Outbound Summary' where KEY = 'UCLPermissions_Outbound - Administer Returnables OB Summ' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Replenishment Task Priority Detrm Rules', VALUE = 'Administer Replenishment Task Priority Detrm Rules' where KEY = 'UCLPermissions_Tasking - Administer Repl Task Prty Detrm Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Routing Request-Response', VALUE = 'Administer Routing Request-Response' where KEY = 'UCLPermissions_Admin Routing request-response' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Rate Shipment Group', VALUE = 'Administer Rate Shipment Group' where KEY = 'UCLPermissions_Admin Rate shipt group' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Rate Shopping Transaction', VALUE = 'Administer Rate Shopping Transaction' where KEY = 'UCLPermissions_Admin Rate shopping transaction' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Route Wizard', VALUE = 'Administer Route Wizard' where KEY = 'UCLPermissions_Admin Route wizard' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Rule Column List', VALUE = 'Administer Rule Column List' where KEY = 'UCLPermissions_Sys Ctrl - Administer Rule column list' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Run Wave', VALUE = 'Administer Run Wave' where KEY = 'UCLPermissions_Waves - Administer Run wave' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Routing Wave Template', VALUE = 'Administer Routing Wave Template' where KEY = 'UCLPermissions_Admin Routing wave template' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Ship By Date Warning Report', VALUE = 'Administer Ship By Date Warning Report' where KEY = 'UCLPermissions_Reports - Administer Ship by date warning' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Ship Via', VALUE = 'Administer Ship Via' where KEY = 'UCLPermissions_Admin Ship via ' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pick Wave', VALUE = 'Administer Pick Wave' where KEY = 'UCLPermissions_Waves - Administer Pick Wave' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Shipping Calendar', VALUE = 'Administer Shipping Calendar' where KEY = 'UCLPermissions_Admin Shipping calendar' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Create Shipment And Load', VALUE = 'Administer Create Shipment And Load' where KEY = 'UCLPermissions_Admin Create shipment and load' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Shipping Planner', VALUE = 'Administer Shipping Planner' where KEY = 'UCLPermissions_Admin Shipping planner' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Supplemental Inbound Shipment', VALUE = 'Administer Supplemental Inbound Shipment' where KEY = 'UCLPermissions_Admin Supplemental IBShipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Ship Wave', VALUE = 'Administer Ship Wave' where KEY = 'UCLPermissions_Waves - Administer Ship wave' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Ship Wave Parameters', VALUE = 'Administer Ship Wave Parameters' where KEY = 'UCLPermissions_Waves - Administer Ship Wave Parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Smart Label Bulk Mail', VALUE = 'Administer Smart Label Bulk Mail' where KEY = 'UCLPermissions_Smartlabels - Administer Smartlabel bulk mail' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Smart Label Class Of Service', VALUE = 'Administer Smart Label Class Of Service' where KEY = 'UCLPermissions_Smartlabels - Administer Smartlabel Service Class' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Smart Label Fee', VALUE = 'Administer Smart Label Fee' where KEY = 'UCLPermissions_Smartlabels - Administer Smartlabel Fee' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Smart Label Rapid Returns', VALUE = 'Administer Smart Label Rapid Returns' where KEY = 'UCLPermissions_Smartlabels - Administer Smartlabel rapid returns' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Smart Label Rule Map', VALUE = 'Administer Smart Label Rule Map' where KEY = 'UCLPermissions_Smartlabels - Administer Smartlabel rule map' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer SmartLabel Item', VALUE = 'Administer SmartLabel Item' where KEY = 'UCLPermissions_Smartlabels - Administer Smarlabel SKU' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Smart Label Zip', VALUE = 'Administer Smart Label Zip' where KEY = 'UCLPermissions_Smartlabels - Administer Smartlabel zip' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer SmartLabel Parameters', VALUE = 'Administer SmartLabel Parameters' where KEY = 'UCLPermissions_Smartlabels - Administer Smartlabel Parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Smart Label', VALUE = 'Administer Smart Label' where KEY = 'UCLPermissions_Smartlabels - Administer Smartlabel' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Serial Number', VALUE = 'Administer Serial Number' where KEY = 'UCLPermissions_Inv Mgmt - Administer Serial Number' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Sorter Group', VALUE = 'Administer Sorter Group' where KEY = 'UCLPermissions_Waves - Administer Sorter Group' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Substitution (WMASSUB)', VALUE = 'Administer Item Substitution (WMASSUB)' where KEY = 'UCLPermissions_Admin Sku Substition for Warehouse Mgmt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Stabilization Codes', VALUE = 'Administer Stabilization Codes' where KEY = 'UCLPermissions_Waves - Administer Stabilization Codes' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Sortation Divert Action', VALUE = 'Administer Sortation Divert Action' where KEY = 'UCLPermissions_MHE - Administer Sortation Divert action' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Sortation Divert Rules', VALUE = 'Administer Sortation Divert Rules' where KEY = 'UCLPermissions_MHE - Administer Soration divert rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Store Distribution Inquiry', VALUE = 'Administer Store Distribution Inquiry' where KEY = 'UCLPermissions_Admin Store distro inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Store Master Inquiry', VALUE = 'Administer Store Master Inquiry' where KEY = 'UCLPermissions_Admin Store master inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Store Pack Location', VALUE = 'Administer Store Pack Location' where KEY = 'UCLPermissions_Outbound - Administer Store pack location' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Supervisor Override', VALUE = 'Supervisor Override' where KEY = 'UCLPermissions_Sys Ctrl - Supervisor Override' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer System Codes', VALUE = 'Administer System Codes' where KEY = 'UCLPermissions_WM Admin Sys Codes' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task List Report', VALUE = 'Administer Task List Report' where KEY = 'UCLPermissions_Reports - Administer Task List' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task Group Eligibility', VALUE = 'Administer Task Group Eligibility' where KEY = 'UCLPermissions_Tasking - Administer Task Group Eligibility' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task Grp Equip Eligibility', VALUE = 'Administer Task Grp Equip Eligibility' where KEY = 'UCLPermissions_Tasking - Administer Task Grp Equip Eligibility' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer TMS Interface Rules', VALUE = 'Administer TMS Interface Rules' where KEY = 'UCLPermissions_Admin TMS Interface Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task Priority Determination', VALUE = 'Administer Task Priority Determination' where KEY = 'UCLPermissions_Tasking - Administer Task Priority Detrm' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Travel Aisle Configuration', VALUE = 'Administer Travel Aisle Configuration' where KEY = 'UCLPermissions_Tasking - Administer Travel aisle configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Transitional Inventory (WMATRAN)', VALUE = 'Administer Transitional Inventory (WMATRAN)' where KEY = 'UCLPermissions_Admin Trans Inv' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Transitional Inventory (WMATRANINVN)', VALUE = 'Administer Transitional Inventory (WMATRANINVN)' where KEY = 'UCLPermissions_WM Admin Transitional Invn' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task Release Determination', VALUE = 'Administer Task Release Determination' where KEY = 'UCLPermissions_Tasking - Administer Task Release Determination' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Trailer Equipment Cross Reference', VALUE = 'Administer Trailer Equipment Cross Reference' where KEY = 'UCLPermissions_Admin Trailer equipment cross reference' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Trailer Equipment', VALUE = 'Administer Trailer Equipment' where KEY = 'UCLPermissions_Admin Trailer equipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Trailer Tran', VALUE = 'Administer Trailer Tran' where KEY = 'UCLPermissions_Admin Trailer Tran' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task Action', VALUE = 'Administer Task Action' where KEY = 'UCLPermissions_Tasking - Administer Task Action' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task Check In/Out', VALUE = 'Administer Task Check In/Out' where KEY = 'UCLPermissions_Tasking - Administer Task Check In/Out' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task Creation', VALUE = 'Administer Task Creation' where KEY = 'UCLPermissions_Tasking - Administer Task Creation' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task List', VALUE = 'Administer Task List' where KEY = 'UCLPermissions_Tasking - Administer Task List' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task Path Definition', VALUE = 'Administer Task Path Definition' where KEY = 'UCLPermissions_Tasking - Administer Task Path Definition' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Task Path Definition Wizard', VALUE = 'Administer Task Path Definition Wizard' where KEY = 'UCLPermissions_Tasking - Administer Task Path Defn Wizard' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Inbound Rule', VALUE = 'Administer Inbound Rule' where KEY = 'UCLPermissions_Admin Inbound Rule ' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Verification Rule', VALUE = 'Administer Item Verification Rule' where KEY = 'UCLPermissions_Admin Item Verification Rule ' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer UPS PLD Cross Reference', VALUE = 'Administer UPS PLD Cross Reference' where KEY = 'UCLPermissions_Admin UPS PLD Cross reference' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Users', VALUE = 'Administer Users' where KEY = 'UCLPermissions_MSF - Administer Users' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Vendor Master', VALUE = 'Administer Vendor Master' where KEY = 'UCLPermissions_Admin Vendor Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Vocollect Parameters', VALUE = 'Administer Vocollect Parameters' where KEY = 'UCLPermissions_Vocollect - Administer Vocollect Parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Vocollect Terminal', VALUE = 'Administer Vocollect Terminal' where KEY = 'UCLPermissions_Vocollect - Administer Vocollect Terminal' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Voice Active oLPN', VALUE = 'Administer Voice Active oLPN' where KEY = 'UCLPermissions_Vocollect - Administer Voice active carton' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pre-Receiving Vendor Performance Codes', VALUE = 'Administer Pre-Receiving Vendor Performance Codes' where KEY = 'UCLPermissions_Pre-Recv - Administer Vendor Performance Codes' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Pre-Receiving Vendor Performance Tran', VALUE = 'Administer Pre-Receiving Vendor Performance Tran' where KEY = 'UCLPermissions_Pre-Recv - Administer Vendor Perf Transaction' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Voice Work Type', VALUE = 'Administer Voice Work Type' where KEY = 'UCLPermissions_Tasking - Administer Voice Work Type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Warehouse Master', VALUE = 'Administer Warehouse Master' where KEY = 'UCLPermissions_WM Admin Warehouse Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Work Group Proximity', VALUE = 'Administer Work Group Proximity' where KEY = 'UCLPermissions_Tasking - Administer Work group proximity' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Workload Balancing', VALUE = 'Administer Workload Balancing' where KEY = 'UCLPermissions_Work Load Mgmt - Administer Workload Balancing' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Receiving Actions For ASN UI', VALUE = 'Administer Receiving Actions For ASN UI' where KEY = 'UCLPermissions_Receiving - Adminster Actions for ASN UI' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Work Orders', VALUE = 'Administer Work Orders' where KEY = 'UCLPermissions_Inv Mgmt - Administer Work Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Work Order Report', VALUE = 'Administer Work Order Report' where KEY = 'UCLPermissions_Reports - Administer Work Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Wave Process Type', VALUE = 'Administer Wave Process Type' where KEY = 'UCLPermissions_Waves - Administer Wave process type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Wave Queue Inquiry', VALUE = 'Administer Wave Queue Inquiry' where KEY = 'UCLPermissions_Admin Wave queue inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Wave Template', VALUE = 'Administer Wave Template' where KEY = 'UCLPermissions_Waves - Administer Wave template' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Item Cross Reference', VALUE = 'Administer Item Cross Reference' where KEY = 'UCLPermissions_Admin Item Cross Reference' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer Yard Locations', VALUE = 'Administer Yard Locations' where KEY = 'UCLPermissions_Admin Yard Location' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer iLPN Bulk Updates', VALUE = 'Administer iLPN Bulk Updates' where KEY = 'UCLPermissions_Bulk Updates iLPN - Lock/Unlock' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer WM MHE Message Format Details', VALUE = 'Administer WM MHE Message Format Details' where KEY = 'UCLPermissions_MHE - Administer WM MHE Message Format Details' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_Administer WM MHE Message Format', VALUE = 'Administer WM MHE Message Format' where KEY = 'UCLPermissions_MHE - Administer WM MHE Message Format' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View WM MHE Message Format Details', VALUE = 'View WM MHE Message Format Details' where KEY = 'UCLPermissions_MHE - View WM MHE Message Format Details' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View WM MHE Message Format', VALUE = 'View WM MHE Message Format' where KEY = 'UCLPermissions_MHE - View WM MHE Message Format' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF Audit Outbound Pallet', VALUE = 'RF Audit Outbound Pallet' where KEY = 'UCLPermissions_RF Audit OB Pallet' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_RF On Web', VALUE = 'RF On Web' where KEY = 'UCLPermissions_Sys Ctrl - RF on Web' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Activity Tracking', VALUE = 'View Activity Tracking' where KEY = 'UCLPermissions_Sys Ctrl - View Activity tracking' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Application Identifier Tran Configuration', VALUE = 'View Application Identifier Tran Configuration' where KEY = 'UCLPermissions_Sys Ctrl - View Appl Identifier Tran Conf' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Allocation Parameters', VALUE = 'View Allocation Parameters' where KEY = 'UCLPermissions_Allocation - View Allocation Parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Allocation Rules', VALUE = 'View Allocation Rules' where KEY = 'UCLPermissions_Allocation - View Allocation Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Application Identifier', VALUE = 'View Application Identifier' where KEY = 'UCLPermissions_Sys Ctrl - View Application identifier' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Appointment', VALUE = 'View Appointment' where KEY = 'UCLPermissions_WM View Appointment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View AR Account', VALUE = 'View AR Account' where KEY = 'UCLPermissions_Sys Ctrl - View AR Account' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View ARN Event', VALUE = 'View ARN Event' where KEY = 'UCLPermissions_Smartlabels - View ARN Event' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View ARN Reports', VALUE = 'View ARN Reports' where KEY = 'UCLPermissions_Reports - View ARN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View ASN Inquiry Reports', VALUE = 'View ASN Inquiry Reports' where KEY = 'UCLPermissions_Reports - View ASN Inquiry' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View ASN Variance Reports', VALUE = 'View ASN Variance Reports' where KEY = 'UCLPermissions_Reports - View ASN Variance' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Batch Invoicing', VALUE = 'View Batch Invoicing' where KEY = 'UCLPermissions_Outbound - View Batch Invoiving' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Lot Master', VALUE = 'View Lot Master' where KEY = 'UCLPermissions_Inv Mgmt - View Lot Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View WM Business Partners', VALUE = 'View WM Business Partners' where KEY = 'UCLPermissions_MSF - View Business Partners' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Blocked Location Report', VALUE = 'View Blocked Location Report' where KEY = 'UCLPermissions_Reports - View Blocked Location' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Bill Of Materials', VALUE = 'View Bill Of Materials' where KEY = 'UCLPermissions_View BOM' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Barcode Type Master', VALUE = 'View Barcode Type Master' where KEY = 'UCLPermissions_Sys Ctrl - View Barocde Type master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Bridge Exclude Table', VALUE = 'View Bridge Exclude Table' where KEY = 'UCLPermissions_View Bridge exclude table' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Carrier Discount Inquiry', VALUE = 'View Carrier Discount Inquiry' where KEY = 'UCLPermissions_View Carrier discount inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View oLPN Maintenance', VALUE = 'View oLPN Maintenance' where KEY = 'UCLPermissions_View Carton Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Carrier Exclusion Inquiry', VALUE = 'View Carrier Exclusion Inquiry' where KEY = 'UCLPermissions_View Carrier exclusion inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Cycle Count History', VALUE = 'View Cycle Count History' where KEY = 'UCLPermissions_View Cycle count history' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Cycle Count Trigger', VALUE = 'View Cycle Count Trigger' where KEY = 'UCLPermissions_Counting - View Cycle Count trigger' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Cycle Count Task Rules', VALUE = 'View Cycle Count Task Rules' where KEY = 'UCLPermissions_Counting - View Cycle Count Task Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Cycle Count Location Variance Report', VALUE = 'View Cycle Count Location Variance Report' where KEY = 'UCLPermissions_WM Inv Mgmt - View Cyc Cnt Var Rpt (Location)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Cycle Count Item Variance Detail Report', VALUE = 'View Cycle Count Item Variance Detail Report' where KEY = 'UCLPermissions_Reports - View Cyc Cnt Var (Dtl By Item)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Cycle Count Item Variance Summary Report', VALUE = 'View Cycle Count Item Variance Summary Report' where KEY = 'UCLPermissions_Reports - View Cycle Count Var (Item)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LPN Disposition Determination Rules', VALUE = 'View LPN Disposition Determination Rules' where KEY = 'UCLPermissions_Putaway - View LPN Disp Detrm Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Chute Priority Maintenance', VALUE = 'View Chute Priority Maintenance' where KEY = 'UCLPermissions_Waves - View Chute Priority Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Chute Master', VALUE = 'View Chute Master' where KEY = 'UCLPermissions_Waves - View Chute Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Configuration Tool', VALUE = 'View Configuration Tool' where KEY = 'UCLPermissions_Config Tool - View Config Tool' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Country', VALUE = 'View Country' where KEY = 'UCLPermissions_Sys Ctrl - View Country' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Collate Reports', VALUE = 'View Collate Reports' where KEY = 'UCLPermissions_Reports - View Collate' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Companies', VALUE = 'View Companies' where KEY = 'UCLPermissions_MSF - View Companies' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Create ASN From PO', VALUE = 'View Create ASN From PO' where KEY = 'UCLPermissions_View Create IBShipment fom PO' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View iLPN Size Type', VALUE = 'View iLPN Size Type' where KEY = 'UCLPermissions_WM View Case Size Type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Cycle Count Report', VALUE = 'View Cycle Count Report' where KEY = 'UCLPermissions_Counting - View Cycle Count' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View MHE Divert History', VALUE = 'View MHE Divert History' where KEY = 'UCLPermissions_MHE - View Divert history' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View EPC', VALUE = 'View EPC' where KEY = 'UCLPermissions_Inv Mgmt - View EPC' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Event Message', VALUE = 'View Event Message' where KEY = 'UCLPermissions_Interface - View Event Message' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Expiration Date Warning Report', VALUE = 'View Expiration Date Warning Report' where KEY = 'UCLPermissions_Reports - View Expiration date warning' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View FedEx Transaction', VALUE = 'View FedEx Transaction' where KEY = 'UCLPermissions_View Fedex Tran' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Final Receiving Return Disposition', VALUE = 'View Final Receiving Return Disposition' where KEY = 'UCLPermissions_Receiving - View Final Return Disposition' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Final Receiving Return Disposition Detrm', VALUE = 'View Final Receiving Return Disposition Detrm' where KEY = 'UCLPermissions_Receiving - View Final Return Disp Detrm' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Frozen Location Detail Report', VALUE = 'View Frozen Location Detail Report' where KEY = 'UCLPermissions_Reports - View Frozen Location Detail' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View FTZ Batch Parameters', VALUE = 'View FTZ Batch Parameters' where KEY = 'UCLPermissions_View FTZ Batch parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View FTZ Confirm/Undo Layers', VALUE = 'View FTZ Confirm/Undo Layers' where KEY = 'UCLPermissions_View FTZ Confirm/undo layers' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View FTZ Commercial Invoice', VALUE = 'View FTZ Commercial Invoice' where KEY = 'UCLPermissions_View FTZ Commercial invoice' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View FTZ Consolidate Layers', VALUE = 'View FTZ Consolidate Layers' where KEY = 'UCLPermissions_View FTZ Consolidate layers' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View FTZ Case Receipts', VALUE = 'View FTZ Case Receipts' where KEY = 'UCLPermissions_View FTZ Case receipts ' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Check In/Out', VALUE = 'View Check In/Out' where KEY = 'UCLPermissions_View Guard Check In/Out' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Hazardous Material Ship Via Exclusions', VALUE = 'View Hazardous Material Ship Via Exclusions' where KEY = 'UCLPermissions_View-Hazardous Material Ship Via Exclusions' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View HTS Execution', VALUE = 'View HTS Execution' where KEY = 'UCLPermissions_View HTS execution' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Inventory Allocation Priority', VALUE = 'View Inventory Allocation Priority' where KEY = 'UCLPermissions_Allocation - View Inventory Allocation Prty' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Inbound Quality Audit', VALUE = 'View Inbound Quality Audit' where KEY = 'UCLPermissions_Pre-Recv - View Inbound Quality Audit' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View ASN', VALUE = 'View ASN' where KEY = 'UCLPermissions_View IBShipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Immediate Needs', VALUE = 'View Immediate Needs' where KEY = 'UCLPermissions_Pre-Recv - View Immediate Needs' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Inventory Adjustment Report Transaction', VALUE = 'View Inventory Adjustment Report Transaction' where KEY = 'UCLPermissions_Reports - View Inventory Adjustment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Insurance Type', VALUE = 'View Insurance Type' where KEY = 'UCLPermissions_View Insurance type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Inventory Need Type', VALUE = 'View Inventory Need Type' where KEY = 'UCLPermissions_Allocation - View Inventory Need Type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Interface Configuration', VALUE = 'View Interface Configuration' where KEY = 'UCLPermissions_Interface - View Interface Configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Inventory Reconciliation', VALUE = 'View Inventory Reconciliation' where KEY = 'UCLPermissions_Inv Mgmt - View Inventory Reconciliation' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Inventory Reconciliation Report', VALUE = 'View Inventory Reconciliation Report' where KEY = 'UCLPermissions_Reports - View Inventory Reconciliation' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Inventory Value', VALUE = 'View Inventory Value' where KEY = 'UCLPermissions_Inv Mgmt - View Inventory Value' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View WM Items', VALUE = 'View WM Items' where KEY = 'UCLPermissions_View Item Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Item Inventory', VALUE = 'View Item Inventory' where KEY = 'UCLPermissions_Inv Mgmt - View Item Inventory' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Item Inventory By Category', VALUE = 'View Item Inventory By Category' where KEY = 'UCLPermissions_Inv Mgmt - View Item Inv By Category' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Item Inventory By Location', VALUE = 'View Item Inventory By Location' where KEY = 'UCLPermissions_Inv Mgmt - View Item Inventory by Location' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Item Substitution', VALUE = 'View Item Substitution' where KEY = 'UCLPermissions_Inv Mgmt - View Item Substitution' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View WM Item Profiles', VALUE = 'View WM Item Profiles' where KEY = 'UCLPermissions_View Item Master Profile' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Item Business Partner Configuration', VALUE = 'View Item Business Partner Configuration' where KEY = 'UCLPermissions_Reports - Item Vendor Attributes' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Layer Adjustment', VALUE = 'View Layer Adjustment' where KEY = 'UCLPermissions_View Layer adjustment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Labor Plan Inquiry', VALUE = 'View Labor Plan Inquiry' where KEY = 'UCLPermissions_View Labor Plan Inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Locked iLPN Report', VALUE = 'View Locked iLPN Report' where KEY = 'UCLPermissions_Reports - View Locked iLPN' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LM Application Criteria Determination Rules', VALUE = 'View LM Application Criteria Determination Rules' where KEY = 'UCLPermissions_Labor - View LM Appl criteria detrm rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LM Monitor Defaults', VALUE = 'View LM Monitor Defaults' where KEY = 'UCLPermissions_Labor - View LM Monitor defaults' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LM Handling Attribute', VALUE = 'View LM Handling Attribute' where KEY = 'UCLPermissions_Labor - View LM Handling attribute' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LM Message', VALUE = 'View LM Message' where KEY = 'UCLPermissions_Labor - View LM Message' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Location Frozen Not Counted Report', VALUE = 'View Location Frozen Not Counted Report' where KEY = 'UCLPermissions_Reports - View Locn Frozn Not Counted' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View WM iLPN', VALUE = 'View WM iLPN' where KEY = 'UCLPermissions_View Case' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View LTL oLPN', VALUE = 'View LTL oLPN' where KEY = 'UCLPermissions_View LTL Carton' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Lean Time Replenishments', VALUE = 'View Lean Time Replenishments' where KEY = 'UCLPermissions_Inv Mgmt - View Lean Time Replenishment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Lean Time Replenishment Rules', VALUE = 'View Lean Time Replenishment Rules' where KEY = 'UCLPermissions_LeanTimeRepl-View LeanTimeRepl Configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View WM Menu Maintenance', VALUE = 'View WM Menu Maintenance' where KEY = 'UCLPermissions_Sys Ctrl - View Menu Maintenance' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View MHE Message Format', VALUE = 'View MHE Message Format' where KEY = 'UCLPermissions_MHE - View MHE Message Format' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View WM MHE Rule Group Configuration', VALUE = 'View WM MHE Rule Group Configuration' where KEY = 'UCLPermissions_MHE - View WM MHE Rule Group Configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View WM MHE Rule Group', VALUE = 'View WM MHE Rule Group' where KEY = 'UCLPermissions_MHE - View WM MHE Rule Group' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Message Log', VALUE = 'View Message Log' where KEY = 'UCLPermissions_Sys Ctrl - View Message Log' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Message Master', VALUE = 'View Message Master' where KEY = 'UCLPermissions_Sys Ctrl - View Message Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Send Message To RF User', VALUE = 'View Send Message To RF User' where KEY = 'UCLPermissions_Sys Ctrl - View Send Message To RF User' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Nextup Numbers', VALUE = 'View Nextup Numbers' where KEY = 'UCLPermissions_Sys Ctrl - View Nextup numbers' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Outbound Pallet Maintenance', VALUE = 'View Outbound Pallet Maintenance' where KEY = 'UCLPermissions_View Outbound pallet maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Outbound Quality Audit', VALUE = 'View Outbound Quality Audit' where KEY = 'UCLPermissions_Outbound - View OB Quality Audit' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Outbound Shipment', VALUE = 'View Outbound Shipment' where KEY = 'UCLPermissions_View OB Shipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View On Hand Inventory Value', VALUE = 'View On Hand Inventory Value' where KEY = 'UCLPermissions_Inv Mgmt - View On-Hand Inventory Value' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Order Report', VALUE = 'View Order Report' where KEY = 'UCLPermissions_Reports - View Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Order Rounding', VALUE = 'View Order Rounding' where KEY = 'UCLPermissions_Outbound - View Order rounding' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Outward Events', VALUE = 'View Outward Events' where KEY = 'UCLPermissions_Sys Ctrl - View Outward Events' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Outward Event Rules', VALUE = 'View Outward Event Rules' where KEY = 'UCLPermissions_Sys Ctrl - View Outward Event Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Outward Generated Events', VALUE = 'View Outward Generated Events' where KEY = 'UCLPermissions_Sys Ctrl - View Outward Generated Events' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Packing Slip Report', VALUE = 'View Packing Slip Report' where KEY = 'UCLPermissions_Reports - View Packing Slip' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Partial Shorted Order Report', VALUE = 'View Partial Shorted Order Report' where KEY = 'UCLPermissions_Reports - View Partial Shorted Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Order Consolidation Detrm Type Maintenance', VALUE = 'View Order Consolidation Detrm Type Maintenance' where KEY = 'UCLPermissions_Waves - View Order Consol Detrm Type Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Order Consolidation Location Maintenance', VALUE = 'View Order Consolidation Location Maintenance' where KEY = 'UCLPermissions_Waves - View Order Consol Location Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Order Consolidation Parameters', VALUE = 'View Order Consolidation Parameters' where KEY = 'UCLPermissions_Waves- View Order Consol Parm' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Order Consolidation Profile', VALUE = 'View Order Consolidation Profile' where KEY = 'UCLPermissions_Waves - View Order Consol Profile' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Physical Count Reconciliation Report', VALUE = 'View Physical Count Reconciliation Report' where KEY = 'UCLPermissions_Reports - View Phy Cnt Recon (Srl Nbr)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pick Cart Contents', VALUE = 'View Pick Cart Contents' where KEY = 'UCLPermissions_Reports - View Pick Cart Contents' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Physical Count LPN Variance Report', VALUE = 'View Physical Count LPN Variance Report' where KEY = 'UCLPermissions_Reports - View Phy Cnt Var (LPN Booking)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Physical Count Non LPN Variance Report', VALUE = 'View Physical Count Non LPN Variance Report' where KEY = 'UCLPermissions_Reports - View Phy Cnt Var(Not tracking LPN)' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pack And Hold Parameters Template', VALUE = 'View Pack And Hold Parameters Template' where KEY = 'UCLPermissions_Waves - View Pack and Hold parm template' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Physical Count', VALUE = 'View Physical Count' where KEY = 'UCLPermissions_Counting - View Physical Count' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View PIX Schedule Export', VALUE = 'View PIX Schedule Export' where KEY = 'UCLPermissions_Inv Mgmt - View PIX Schedule Export' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View PIX Export Configuration', VALUE = 'View PIX Export Configuration' where KEY = 'UCLPermissions_View Pix Export Config' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View PIX History Report', VALUE = 'View PIX History Report' where KEY = 'UCLPermissions_Reports - View PIX History' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View PIX Transaction Code', VALUE = 'View PIX Transaction Code' where KEY = 'UCLPermissions_Inv Mgmt - View PIX transaction Code' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pack oLPN And Verify', VALUE = 'View Pack oLPN And Verify' where KEY = 'UCLPermissions_Outbound - View Pack oLPN and Verify' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pick Cart Plan ID', VALUE = 'View Pick Cart Plan ID' where KEY = 'UCLPermissions_Outbound - View Pick cart PlanID' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pack Efficiency', VALUE = 'View Pack Efficiency' where KEY = 'UCLPermissions_Waves - View Pack Efficiency' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Order Profile Maintenance', VALUE = 'View Order Profile Maintenance' where KEY = 'UCLPermissions_View Pkt Profile Maint' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Picking Short Items', VALUE = 'View Picking Short Items' where KEY = 'UCLPermissions_Outbound - View Picking Short Items' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pack Station', VALUE = 'View Pack Station' where KEY = 'UCLPermissions_Waves - View Pack Station' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Order Inquiry', VALUE = 'View Order Inquiry' where KEY = 'UCLPermissions_View Pickticket inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pickup Task Rules', VALUE = 'View Pickup Task Rules' where KEY = 'UCLPermissions_View - Pickup Task Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pack Wave Parameters Template', VALUE = 'View Pack Wave Parameters Template' where KEY = 'UCLPermissions_Waves - View Pack Wave Parm Template' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pallets', VALUE = 'View Pallets' where KEY = 'UCLPermissions_View Pallet' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Putaway Method Zone Priority', VALUE = 'View Putaway Method Zone Priority' where KEY = 'UCLPermissions_Putaway - View PA Method - Zone Priority' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View PO Consolidation Exclusion', VALUE = 'View PO Consolidation Exclusion' where KEY = 'UCLPermissions_Outbound - View PO Consol Exclusion' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Print Queue/Service Assignment', VALUE = 'View Print Queue/Service Assignment' where KEY = 'UCLPermissions_View Print queue/service assignment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Parcel Hazardous Materials', VALUE = 'View Parcel Hazardous Materials' where KEY = 'UCLPermissions_View-Parcel Hazardous Materials' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View PreWave', VALUE = 'View PreWave' where KEY = 'UCLPermissions_Waves - View PreWave' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Purge System Table', VALUE = 'View Purge System Table' where KEY = 'UCLPermissions_Sys Ctrl - View Purge system table' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Print Requestor/Service Assignment', VALUE = 'View Print Requestor/Service Assignment' where KEY = 'UCLPermissions_View Print requestor/service assignment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Location Label Printing', VALUE = 'View Location Label Printing' where KEY = 'UCLPermissions_Inv Mgmt - View Print Location Label' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Print Queue Master', VALUE = 'View Print Queue Master' where KEY = 'UCLPermissions_View Print queue master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Putaway Configuration', VALUE = 'View Putaway Configuration' where KEY = 'UCLPermissions_Putaway - View Putaway Configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Putaway Override Report', VALUE = 'View Putaway Override Report' where KEY = 'UCLPermissions_Reports - View Putaway Override' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Putaway Report', VALUE = 'View Putaway Report' where KEY = 'UCLPermissions_Reports - View Putaway Report' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View PIX History', VALUE = 'View PIX History' where KEY = 'UCLPermissions_Inv Mgmt - View PIX History' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View PIX Transaction', VALUE = 'View PIX Transaction' where KEY = 'UCLPermissions_Inv Mgmt - View PIX Transaction' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pick Zone Assignment', VALUE = 'View Pick Zone Assignment' where KEY = 'UCLPermissions_Waves - View Pick Zone Assignment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Putaway Zone Utilization Report', VALUE = 'View Putaway Zone Utilization Report' where KEY = 'UCLPermissions_Reports - View PA Zone Utilization' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Question Determination Rules', VALUE = 'View Question Determination Rules' where KEY = 'UCLPermissions_Pre-Recv - View Question Determination Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Question Answer History', VALUE = 'View Question Answer History' where KEY = 'UCLPermissions_Pre-Recv - View Question answer history' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Quality Inspection Rules', VALUE = 'View Quality Inspection Rules' where KEY = 'UCLPermissions_Receiving - View Quality Inspection Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Question Master', VALUE = 'View Question Master' where KEY = 'UCLPermissions_Pre-Recv - View Question Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Quality Verification Question Determination', VALUE = 'View Quality Verification Question Determination' where KEY = 'UCLPermissions_Receiving - View Quality Verf Quest Detrm' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Receiving Activity Report', VALUE = 'View Receiving Activity Report' where KEY = 'UCLPermissions_Receiving - View Receiving Activity Report' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Recall Inventory', VALUE = 'View Recall Inventory' where KEY = 'UCLPermissions_Inv Mgmt - View Recall Inventory' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Recall Inventory Report', VALUE = 'View Recall Inventory Report' where KEY = 'UCLPermissions_Reports - View Recall Inventory' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Remove Pick Location Dedication Rules', VALUE = 'View Remove Pick Location Dedication Rules' where KEY = 'UCLPermissions_View - Remove Pick Location Dedication' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_WM View RFID Profile', VALUE = 'WM View RFID Profile' where KEY = 'UCLPermissions_WM View RFID profile' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View RF Menu UI', VALUE = 'View RF Menu UI' where KEY = 'UCLPermissions_Inv Mgmt - View RF Menu in GUI' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Reserve Invn w/o Active Locn Report', VALUE = 'View Reserve Invn w/o Active Locn Report' where KEY = 'UCLPermissions_Reports - View Resv Invn w/o Act locn' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Route Change Report', VALUE = 'View Route Change Report' where KEY = 'UCLPermissions_Reports - View Route Change' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Report Translations', VALUE = 'View Report Translations' where KEY = 'UCLPermissions_Reports - View Report translations' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Rate Shop Group Inquiry', VALUE = 'View Rate Shop Group Inquiry' where KEY = 'UCLPermissions_View Rate shop group inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Routing Guide', VALUE = 'View Routing Guide' where KEY = 'UCLPermissions_View Routeing guide' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Returnables Inbound History', VALUE = 'View Returnables Inbound History' where KEY = 'UCLPermissions_Outbound - View Returnables IB history' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Returnables Inbound Summary', VALUE = 'View Returnables Inbound Summary' where KEY = 'UCLPermissions_Outbound - View Returnables IB Summary' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Retail Route (WMVRTLRT)', VALUE = 'View Retail Route (WMVRTLRT)' where KEY = 'UCLPermissions_View Retail route' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Retail Route Check', VALUE = 'View Retail Route Check' where KEY = 'UCLPermissions_View Retail route check' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Retail Route Rule Definition', VALUE = 'View Retail Route Rule Definition' where KEY = 'UCLPermissions_View Retail route rule definition' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Retail Route Parameters', VALUE = 'View Retail Route Parameters' where KEY = 'UCLPermissions_View Retail route Parm' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Retail Route Time', VALUE = 'View Retail Route Time' where KEY = 'UCLPermissions_View Retail route time' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Returnables Outbound History', VALUE = 'View Returnables Outbound History' where KEY = 'UCLPermissions_Outbound - View Reurnables OB history' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Returnables Outbound Summary', VALUE = 'View Returnables Outbound Summary' where KEY = 'UCLPermissions_Outbound - View Returnables OB summary' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Replenishment Task Priority Detrm Rules', VALUE = 'View Replenishment Task Priority Detrm Rules' where KEY = 'UCLPermissions_Tasking - View Repl Task Prty Detrm Rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Routing Request-Response', VALUE = 'View Routing Request-Response' where KEY = 'UCLPermissions_View Routing request-response' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Rate Shipment Group', VALUE = 'View Rate Shipment Group' where KEY = 'UCLPermissions_View Rate shipt group' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Rate Shopping Transaction', VALUE = 'View Rate Shopping Transaction' where KEY = 'UCLPermissions_View Rate shopping transaction' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Returnables Summary', VALUE = 'View Returnables Summary' where KEY = 'UCLPermissions_Outbound - View Returnables summary' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Rule Column List', VALUE = 'View Rule Column List' where KEY = 'UCLPermissions_Sys Ctrl - View Rule column list' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Routing Wave Template', VALUE = 'View Routing Wave Template' where KEY = 'UCLPermissions_View Routing wave template' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Ship By Date Warning Report', VALUE = 'View Ship By Date Warning Report' where KEY = 'UCLPermissions_Reports - View Ship by date warning' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Ship Via', VALUE = 'View Ship Via' where KEY = 'UCLPermissions_View Ship via' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Pick Wave', VALUE = 'View Pick Wave' where KEY = 'UCLPermissions_Waves - View Pick Wave' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Shipping Calendar', VALUE = 'View Shipping Calendar' where KEY = 'UCLPermissions_View Shipping calendar' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Ship Wave', VALUE = 'View Ship Wave' where KEY = 'UCLPermissions_Waves - View Ship wave' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Ship Wave Parameters', VALUE = 'View Ship Wave Parameters' where KEY = 'UCLPermissions_Waves - View Ship Wave Parameters' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Smart Label Bulk Mail', VALUE = 'View Smart Label Bulk Mail' where KEY = 'UCLPermissions_Smartlabels - View Smartlabel bulk mail' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Smart Label Class Of Service', VALUE = 'View Smart Label Class Of Service' where KEY = 'UCLPermissions_Smartlabels - View Smartlabel Class of service' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Smart Label Fee', VALUE = 'View Smart Label Fee' where KEY = 'UCLPermissions_Smartlabels - View Smartlabel Fee' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Smart Label Rapid Returns', VALUE = 'View Smart Label Rapid Returns' where KEY = 'UCLPermissions_Smartlabels - View Smartlabel rapid returns' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Smart Label Rule Map', VALUE = 'View Smart Label Rule Map' where KEY = 'UCLPermissions_Smartlabels - View Smartlabel rule map' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View SmartLabel Item', VALUE = 'View SmartLabel Item' where KEY = 'UCLPermissions_Smartlabels - View Smarlabel SKU' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Smart Label Zip', VALUE = 'View Smart Label Zip' where KEY = 'UCLPermissions_Smartlabels - View Smartlabel zip' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Smart Label', VALUE = 'View Smart Label' where KEY = 'UCLPermissions_Smartlabels - View Smartlabel' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Serial Numbers', VALUE = 'View Serial Numbers' where KEY = 'UCLPermissions_Inv Mgmt - View Serial Number' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Sorter Group', VALUE = 'View Sorter Group' where KEY = 'UCLPermissions_Waves - View Sorter Group' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Item Substitution (WMVSSUB)', VALUE = 'View Item Substitution (WMVSSUB)' where KEY = 'UCLPermissions_View Sku Substitution for Warehouse Mgmt' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Status', VALUE = 'View Status' where KEY = 'UCLPermissions_View Status Inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Stabilization Codes', VALUE = 'View Stabilization Codes' where KEY = 'UCLPermissions_Waves - View Stabilization Codes' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Sortation Divert Action', VALUE = 'View Sortation Divert Action' where KEY = 'UCLPermissions_MHE - View Sortation Divert action' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Sortation Divert Rules', VALUE = 'View Sortation Divert Rules' where KEY = 'UCLPermissions_MHE - View Soration divert rules' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Store Distribution Inquiry', VALUE = 'View Store Distribution Inquiry' where KEY = 'UCLPermissions_View Store distro inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Store Master Inquiry', VALUE = 'View Store Master Inquiry' where KEY = 'UCLPermissions_View Store master inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Store Pack Location', VALUE = 'View Store Pack Location' where KEY = 'UCLPermissions_Outbound - View Store pack location' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Supplemental Inbound Shipment', VALUE = 'View Supplemental Inbound Shipment' where KEY = 'UCLPermissions_View Supplemental IBShipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task List Report', VALUE = 'View Task List Report' where KEY = 'UCLPermissions_Reports - View Task List' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task Group Eligibility', VALUE = 'View Task Group Eligibility' where KEY = 'UCLPermissions_Tasking - View Task Group Eligibility' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task Group Equipment Eligibility', VALUE = 'View Task Group Equipment Eligibility' where KEY = 'UCLPermissions_Tasking - View Task Group Equipment Eligibility' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task Priority Determination', VALUE = 'View Task Priority Determination' where KEY = 'UCLPermissions_Tasking - View Task Priority Determination' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Travel Aisle Configuration', VALUE = 'View Travel Aisle Configuration' where KEY = 'UCLPermissions_Tasking - View Travel aisle configuration' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Transitional Inventory (WMVTRAN)', VALUE = 'View Transitional Inventory (WMVTRAN)' where KEY = 'UCLPermissions_View Trans Inv' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Transitional Inventory (WMVTRANINVN)', VALUE = 'View Transitional Inventory (WMVTRANINVN)' where KEY = 'UCLPermissions_WM View Transitional Invn' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task Release Determination', VALUE = 'View Task Release Determination' where KEY = 'UCLPermissions_Tasking - View Task Release Determination' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Trailer Equipment Cross Reference', VALUE = 'View Trailer Equipment Cross Reference' where KEY = 'UCLPermissions_View Trailer equipment cross reference' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Trailer Equipment', VALUE = 'View Trailer Equipment' where KEY = 'UCLPermissions_View Trailer equipment' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task Action', VALUE = 'View Task Action' where KEY = 'UCLPermissions_Tasking - View Task Action' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task Check In/Out', VALUE = 'View Task Check In/Out' where KEY = 'UCLPermissions_Tasking - View Task Check In/Out' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task Creation', VALUE = 'View Task Creation' where KEY = 'UCLPermissions_Tasking - View Task Creation' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task List', VALUE = 'View Task List' where KEY = 'UCLPermissions_Tasking - View Task List' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Task Path Definition', VALUE = 'View Task Path Definition' where KEY = 'UCLPermissions_Tasking - View Task Path Definition' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Inbound Rule', VALUE = 'View Inbound Rule' where KEY = 'UCLPermissions_View Inbound Rule ' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Item Verification Rule', VALUE = 'View Item Verification Rule' where KEY = 'UCLPermissions_View Item Verification Rule ' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View User Activity Inquiry', VALUE = 'View User Activity Inquiry' where KEY = 'UCLPermissions_View User Activity Inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Users', VALUE = 'View Users' where KEY = 'UCLPermissions_MSF - View Users' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Vocollect Terminal', VALUE = 'View Vocollect Terminal' where KEY = 'UCLPermissions_Vocollect - View Vocollect Terminal' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Voice Active oLPN', VALUE = 'View Voice Active oLPN' where KEY = 'UCLPermissions_Vocollect - View Voice active carton' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Vendor Performance Codes', VALUE = 'View Vendor Performance Codes' where KEY = 'UCLPermissions_Pre-Recv - View Vendor Performance Codes' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Vendor Performance Tran', VALUE = 'View Vendor Performance Tran' where KEY = 'UCLPermissions_Pre-Recv - View Vendor Performance Transaction' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Voice Work Type', VALUE = 'View Voice Work Type' where KEY = 'UCLPermissions_Tasking - View Voice Work Type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Warehouse Master', VALUE = 'View Warehouse Master' where KEY = 'UCLPermissions_WM View Warehouse Master' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Work Group Proximity', VALUE = 'View Work Group Proximity' where KEY = 'UCLPermissions_Tasking - View Work group proximity' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Workload Balancing', VALUE = 'View Workload Balancing' where KEY = 'UCLPermissions_Work Load Mgmt - View Workload Balancing' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Work Orders', VALUE = 'View Work Orders' where KEY = 'UCLPermissions_Inv Mgmt - View Work Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Work Order Reports', VALUE = 'View Work Order Reports' where KEY = 'UCLPermissions_Reports - View Work Order' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Wave Process Type', VALUE = 'View Wave Process Type' where KEY = 'UCLPermissions_Waves - View Wave process type' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Wave Queue Inquiry', VALUE = 'View Wave Queue Inquiry' where KEY = 'UCLPermissions_View Wave queue inq' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Wave Template', VALUE = 'View Wave Template' where KEY = 'UCLPermissions_Waves - View Wave template' AND BUNDLE_NAME = 'UCL' ;
update LABEL set  KEY = 'UCLPermissions_View Yard Locations', VALUE = 'View Yard Locations' where KEY = 'UCLPermissions_View Yard Location Maintenance' AND BUNDLE_NAME = 'UCL' ;
COMMIT;                  

-- DBTicket DMM-625
INSERT INTO HIERARCHY_DEFINITIONS (HIERARCHY_DEFINITION_ID,
                                   NAME,
                                   DESCRIPTION,
                                   HIERARCHY_TYPE,
                                   IS_ACTIVE,
                                   TC_COMPANY_ID,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_SOURCE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_DTTM,
                                   DOMAIN_ATTR_1,
                                   DOMAIN_ATTR_2)
     VALUES (HIERARCHY_DEFINITIONS_ID_SEQ.NEXTVAL,
             'Task Hierarchy',
             'Seed Task Hierarchy',
             'TASK',
             1,
             1,
             1,
             'Manhattan Associates',
             SYSDATE,
             1,
             'Manhattan Associates',
             SYSDATE,
             'TASK_TYPE',
             NULL);

INSERT INTO HIERARCHY_LEVELS (HIERARCHY_LEVEL_ID,
                              LEVEL_NUMBER,
                              HIERARCHY_DEFINITION_ID,
                              NAME,
                              PARENT_HIERARCHY_LEVEL_ID,
                              CREATED_SOURCE_TYPE,
                              CREATED_SOURCE,
                              CREATED_DTTM,
                              LAST_UPDATED_SOURCE_TYPE,
                              LAST_UPDATED_SOURCE,
                              LAST_UPDATED_DTTM,
                              NODE_CONTENT_TYPE1,
                              NODE_CONTENT_TYPE1_ID,
                              NODE_CONTENT_TYPE2,
                              NODE_CONTENT_TYPE2_ID)
     VALUES (HIERARCHY_LEVELS_ID_SEQ.NEXTVAL,
             1,
             (SELECT HIERARCHY_DEFINITION_ID
                FROM HIERARCHY_DEFINITIONS
               WHERE DESCRIPTION = 'Seed Task Hierarchy'),
             'Department',
             NULL,
             1,
             'Manhattan Associates',
             SYSDATE,
             1,
             'Manhattan Associates',
             SYSDATE,
             'SYS_CODE',
             (SELECT SYS_CODE_TYPE_ID
                FROM SYS_CODE_TYPE SCT
               WHERE SCT.REC_TYPE = 'B' AND SCT.CODE_TYPE = '054'),
             NULL,
             NULL);

INSERT INTO HIERARCHY_LEVELS (HIERARCHY_LEVEL_ID,
                              LEVEL_NUMBER,
                              HIERARCHY_DEFINITION_ID,
                              NAME,
                              PARENT_HIERARCHY_LEVEL_ID,
                              CREATED_SOURCE_TYPE,
                              CREATED_SOURCE,
                              CREATED_DTTM,
                              LAST_UPDATED_SOURCE_TYPE,
                              LAST_UPDATED_SOURCE,
                              LAST_UPDATED_DTTM,
                              NODE_CONTENT_TYPE1,
                              NODE_CONTENT_TYPE1_ID,
                              NODE_CONTENT_TYPE2,
                              NODE_CONTENT_TYPE2_ID)
     VALUES (HIERARCHY_LEVELS_ID_SEQ.NEXTVAL,
             2,
             (SELECT HIERARCHY_DEFINITION_ID
                FROM HIERARCHY_DEFINITIONS
               WHERE DESCRIPTION = 'Seed Task Hierarchy'),
             'Task Type Group',
             NULL,
             1,
             'Manhattan Associates',
             SYSDATE,
             1,
             'Manhattan Associates',
             SYSDATE,
             'SYS_CODE',
             (SELECT SYS_CODE_TYPE_ID
                FROM SYS_CODE_TYPE SCT
               WHERE SCT.REC_TYPE = 'B' AND SCT.CODE_TYPE = '053'),
             NULL,
             NULL);

INSERT INTO HIERARCHY_LEVELS (HIERARCHY_LEVEL_ID,
                              LEVEL_NUMBER,
                              HIERARCHY_DEFINITION_ID,
                              NAME,
                              PARENT_HIERARCHY_LEVEL_ID,
                              CREATED_SOURCE_TYPE,
                              CREATED_SOURCE,
                              CREATED_DTTM,
                              LAST_UPDATED_SOURCE_TYPE,
                              LAST_UPDATED_SOURCE,
                              LAST_UPDATED_DTTM,
                              NODE_CONTENT_TYPE1,
                              NODE_CONTENT_TYPE1_ID,
                              NODE_CONTENT_TYPE2,
                              NODE_CONTENT_TYPE2_ID)
     VALUES (HIERARCHY_LEVELS_ID_SEQ.NEXTVAL,
             3,
             (SELECT HIERARCHY_DEFINITION_ID
                FROM HIERARCHY_DEFINITIONS
               WHERE DESCRIPTION = 'Seed Task Hierarchy'),
             'Task Type',
             NULL,
             1,
             'Manhattan Associates',
             SYSDATE,
             1,
             'Manhattan Associates',
             SYSDATE,
             'SYS_CODE',
             (SELECT SYS_CODE_TYPE_ID
                FROM SYS_CODE_TYPE SCT
               WHERE SCT.REC_TYPE = 'S' AND SCT.CODE_TYPE = '590'),
             NULL,
             NULL);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.NAME = 'Department'),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'IB'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'IB'),
               NULL,
               NULL,
               NULL,
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.NAME = 'Department'),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'IC'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'IC'),
               NULL,
               NULL,
               NULL,
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.NAME = 'Department'),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'OB'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'OB'),
               NULL,
               NULL,
               NULL,
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.NAME = 'Department'),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'QA'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'QA'),
               NULL,
               NULL,
               NULL,
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.NAME = 'Department'),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'RT'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '054'
                       AND SC.CODE_ID = 'RT'),
               NULL,
               NULL,
               NULL,
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'PA'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'PA'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'IB')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'RC'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'RC'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'IB')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'CA'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'CA'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'IC')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'CR'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'CR'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'IC')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'SL'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'SL'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'IC')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'PI'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'PI'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'OB')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'PP'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'PP'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'OB')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'PR'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'PR'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'OB')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'QA'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'QA'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'QA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'SP'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'B'
                       AND SC.CODE_TYPE = '053'
                       AND SC.CODE_ID = 'SP'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 1
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '054'
                                      AND SC.CODE_ID = 'RT')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '04'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '04'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '05'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '05'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '17'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '17'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '50'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '50'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '12'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '12'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'RC')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '09'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '09'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'CA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '10'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '10'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'CA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '27'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '27'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'CA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '28'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '28'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'CA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '03'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '03'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'CR')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '30'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '30'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'CR')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '18'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '18'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'SL')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '19'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '19'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'SL')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '20'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '20'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'SL')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '06'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '06'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PI')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '07'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '07'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PI')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '16'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '16'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PI')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '13'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '13'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PP')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '14'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '14'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PP')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '24'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '24'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PP')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '26'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '26'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PP')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '01'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '01'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PR')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '02'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '02'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'PR')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '21'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '21'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'QA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '22'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '22'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'QA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '23'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '23'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'QA')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '15'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '15'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'SP')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);

INSERT INTO HIERARCHY_NODES (HIERARCHY_NODE_ID,
                             HIERARCHY_LEVEL_ID,
                             NODE_CONTENT_DATA1_ID,
                             NODE_CONTENT_DATA1_DESC,
                             NODE_CONTENT_DATA2_ID,
                             NODE_CONTENT_DATA2_DESC,
                             PARENT_HIERARCHY_NODE_ID,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (
               HIERARCHY_NODES_ID_SEQ.NEXTVAL,
               (SELECT HIERARCHY_LEVEL_ID
                  FROM HIERARCHY_LEVELS HL, HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 3),
               (SELECT SYS_CODE_ID
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '25'),
               (SELECT CODE_DESC
                  FROM SYS_CODE SC
                 WHERE     SC.REC_TYPE = 'S'
                       AND SC.CODE_TYPE = '590'
                       AND SC.CODE_ID = '25'),
               NULL,
               NULL,
               (SELECT HIERARCHY_NODE_ID
                  FROM HIERARCHY_NODES HN,
                       HIERARCHY_LEVELS HL,
                       HIERARCHY_DEFINITIONS HD
                 WHERE HL.HIERARCHY_DEFINITION_ID =
                          HD.HIERARCHY_DEFINITION_ID
                       AND HL.HIERARCHY_LEVEL_ID = HN.HIERARCHY_LEVEL_ID
                       AND HD.DESCRIPTION = 'Seed Task Hierarchy'
                       AND HL.LEVEL_NUMBER = 2
                       AND HN.NODE_CONTENT_DATA1_ID =
                              (SELECT SYS_CODE_ID
                                 FROM SYS_CODE SC
                                WHERE     SC.REC_TYPE = 'B'
                                      AND SC.CODE_TYPE = '053'
                                      AND SC.CODE_ID = 'SP')),
               1,
               'Manhattan Associates',
               SYSDATE,
               1,
               'Manhattan Associates',
               SYSDATE);
COMMIT;

-- DBTicket DMM-629

MERGE INTO LABEL L
 USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'allWaves' KEY FROM DUAL) B
 ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
 WHEN NOT MATCHED
 THEN
 INSERT (L.LABEL_ID,
 L.KEY,
 L.VALUE,
 L.BUNDLE_NAME)
 VALUES (SEQ_LABEL_ID.NEXTVAL,
 'allWaves',
 'All Waves',
 'lm_mobile_label');
 
-- MERGE INTO XCOMPONENT_LABEL CL
--      USING (SELECT 'lm_mobile_label' LABEL_BUNDLE_NAME, 'Goal' LABEL_KEY
--               FROM DUAL) CB
--         ON (CL.LABEL_KEY = CB.LABEL_KEY
--             AND CL.LABEL_BUNDLE_NAME = CB.LABEL_BUNDLE_NAME)
-- WHEN NOT MATCHED
-- THEN
--    INSERT     (CL.XCOMPONENT_LABEL_ID,
--                CL.XCOMPONENT_ID,
--                CL.LABEL_BUNDLE_NAME,
--                CL.LABEL_KEY)
--        VALUES (SEQ_XCOMPONENT_LABEL_ID.NEXTVAL,
--                (SELECT XCOMPONENT_ID
--                   FROM XCOMPONENT
--                  WHERE XLEGACY_ID = 600101),
--                'lm_mobile_label',
--                'Goal');
			   
   
--Menu parameters
UPDATE XMENU_ITEM
SET NAVIGATION_KEY  ='/basedata/admin/view/CompanyParameters.jsflps?PARAM_GROUPING_NAME=WhseCompanyParams' ||'&' || 'PARAM_LEVEL=1'
WHERE XMENU_ITEM_ID = 6000702;

COMMIT;

-- DBTicket DMM-631
-- delete from xcomponent_label
--       where label_key = 'Goal' and label_bundle_name = 'lm_mobile_label';

merge into label l
     using (select 'lm_mobile_label' bundle_name, 'Goal' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (l.label_id,
               l.key,
               l.value,
               l.bundle_name)
       values (seq_label_id.nextval,
               'Goal',
               'Goal',
               'lm_mobile_label');
commit;

-- DBTicket LM-12205

CREATE INDEX PE_WMLM_LABOR_MSG_IDX_1
   ON LABOR_MSG (WHSE,
                 REF_NBR,
                 REF_CODE,
                 LABOR_MSG_ID,
                 ACT_NAME,
                 MSG_STAT_CODE);

CREATE INDEX PE_WMLM_LABOR_MSG_MON_IDX_1
   ON LABOR_MSG_MONITOR (LABOR_MSG_ID);

CREATE INDEX PE_WMLM_LABOR_MSG_MON_IDX_2
   ON LABOR_MSG_MONITOR (MATCHED_LABOR_TRAN_ID);

CREATE INDEX PE_WMLM_LABOR_MSG_MON_IDX_3
   ON LABOR_MSG_MONITOR (LABOR_MSG_ID, STATUS, MATCHED_LABOR_TRAN_ID);

CREATE INDEX PE_WMLM_LABOR_MSG_IDX_2
   ON LABOR_MSG (WHSE,
                 STATUS,
                 SCHED_START_DATE,
                 MSG_STAT_CODE);

CREATE INDEX PE_WMLM_LABOR_TRAN_IDX_2
   ON LABOR_TRAN (TRAN_NBR,
                  WHSE,
                  MSG_STAT_CODE,
                  STATUS);

-- DBTicket LM-12231

CREATE INDEX PE_WMLM_LABOR_TRAN_IDX_3
   ON LABOR_TRAN (WHSE,
                  REF_NBR,
                  REF_CODE,
                  MSG_STAT_CODE,
                  STATUS);
				  
-- DBTicket DMM-658

CREATE OR REPLACE VIEW CUSTOMER_TASK_VIEW
AS
   SELECT TASK_HDR.task_id,
          TASK_HDR.STAT_CODE,
          TASK_HDR.OWNER_USER_ID,
          TASK_HDR.CREATE_DATE_TIME,
          TASK_HDR.MOD_DATE_TIME,
          TASK_HDR.ltst_cmpl_date_time,
          SUM (COALESCE (TASK_DTL.QTY_ALLOC, 0))
             OVER (PARTITION BY TASK_DTL.TASK_HDR_ID)
             AS ALLOCATED_QUANTITY,
          SUM (COALESCE (TASK_DTL.QTY_PULLD, 0))
             OVER (PARTITION BY TASK_DTL.TASK_HDR_ID)
             AS PULLED_QUANTITY,
          orders.TC_ORDER_ID,
          CUSTOMER.CUSTOMER_ID,
          CUSTOMER.CUSTOMER_CODE,
          CUSTOMER.CUSTOMER_NAME
     FROM TASK_HDR
          INNER JOIN TASK_DTL
             ON TASK_DTL.task_hdr_id = TASK_HDR.TASK_HDR_ID
          INNER JOIN orders
             ON orders.TC_ORDER_ID = TASK_DTL.TC_ORDER_ID
          INNER JOIN CUSTOMER
             ON CUSTOMER.CUSTOMER_ID = orders.CUSTOMER_ID;

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'customerView' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'customerView',
               'Customer View',
               'lm_mobile_label');

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (SEQ_XMENU_ITEM_ID.NEXTVAL,
             'Work Monitoring: Customers',
             'Work Monitoring: Customers',
             'maui://Dm.screen.taskmgmt.CustomerViewScreen',
             NULL,
             'menu-customerView',
             NULL,
             0,
             4,
             '1',
             'GENERAL',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             (SELECT XBASE_MENU_PART_ID
                FROM xbase_menu_part
               WHERE name = 'Mobile Supervisor Authorization'),
             (SELECT xmenu_item_id
                FROM xmenu_item
               WHERE name = 'Work Monitoring: Customers'),
             650,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES ( (SELECT xmenu_item_id
                 FROM xmenu_item
                WHERE name = 'Work Monitoring: Customers'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'WMS'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);
			 
COMMIT;

-- DBTicket DMM-762

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES ( (SELECT xmenu_item_id
                 FROM xmenu_item
                WHERE name = 'Work Monitoring: Customers'),
             'DMMVCUST',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

COMMIT;

-- DBTicket DMM-784
CREATE OR REPLACE VIEW CUSTOMER_TASK_VIEW
AS
   SELECT TASK_HDR.task_id,
          TASK_HDR.STAT_CODE,
          TASK_HDR.OWNER_USER_ID,
          TASK_HDR.CREATE_DATE_TIME,
          TASK_HDR.MOD_DATE_TIME,
          TASK_HDR.ltst_cmpl_date_time,
          TASK_HDR.WHSE,
          SUM (COALESCE (TASK_DTL.QTY_ALLOC, 0))
             OVER (PARTITION BY TASK_DTL.TASK_HDR_ID)
             AS ALLOCATED_QUANTITY,
          SUM (COALESCE (TASK_DTL.QTY_PULLD, 0))
             OVER (PARTITION BY TASK_DTL.TASK_HDR_ID)
             AS PULLED_QUANTITY,
          orders.TC_ORDER_ID,
          CUSTOMER.CUSTOMER_ID,
          CUSTOMER.CUSTOMER_CODE,
          CUSTOMER.CUSTOMER_NAME
     FROM TASK_HDR
          INNER JOIN TASK_DTL
             ON TASK_DTL.task_hdr_id = TASK_HDR.TASK_HDR_ID
          INNER JOIN orders
             ON orders.TC_ORDER_ID = TASK_DTL.TC_ORDER_ID
          INNER JOIN CUSTOMER
             ON CUSTOMER.CUSTOMER_ID = orders.CUSTOMER_ID;
             
-- DBTicket DB-238
UPDATE label
   SET key = 'No_records_found', VALUE = 'No records found'
 WHERE BUNDLE_NAME = 'lm_mobile_label' AND key = 'No records found';

DELETE FROM label_display
      WHERE DISP_KEY = 'No_records_found';
      
COMMIT;             

-- DBTicket DB-251
call quiet_drop('INDEX','LABOR_MSG_WAVE_NBR_INDX');

CREATE INDEX LABOR_MSG_WAVE_NBR_INDX
ON LABOR_MSG (WAVE_NBR) TABLESPACE CBO_TXN_IDX_TBS;

-- DBTicket DB-296
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'CustomerFilter' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (seq_label_id.NEXTVAL,
               'CustomerFilter',
               'Customer',
               'lm_mobile_label');

COMMIT;

-- DBTicket DB-403
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'allJobFunctionslevels' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'allJobFunctionslevels',
               'All Job Functions/Levels',
               'lm_mobile_label');
			   
COMMIT;

-- DBTicket DB-667

MERGE INTO Label A
     USING (SELECT 'ClientRequestFailedAppLog' Key FROM DUAL) B
        ON (A.Key = B.Key)
WHEN MATCHED
THEN
   UPDATE SET
      VALUE =
         'An error occurred while processing this request on the server. Please contact an administrator.';
         
MERGE INTO Label A
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'ClientRequestFailedAppLog' Key
              FROM DUAL) B
        ON (A.BUNDLE_NAME = B.BUNDLE_NAME AND A.Key = B.Key)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ClientRequestFailedAppLog',
                 'The mobile client has made an invalid request to the server. Please contact an administrator.',
                 'lm_mobile_label');
				 
COMMIT;

-- DBTicket DMM-932

MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'SelectUpTo2' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'SelectUpTo2',
      'Select upto 2',
      'lm_mobile_label'
    );
	
MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'Calculator' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'Calculator',
      'Calculator',
      'lm_mobile_label'
    );
	
MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'Output' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'Output',
      'Output',
      'lm_mobile_label'
    );
	
MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'whatIf' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'whatIf',
      'What if',
      'lm_mobile_label'
    );
	
MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'PlanVariance' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'PlanVariance',
      'Plan variance',
      'lm_mobile_label'
    );
	
MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'EPnote' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
  INSERT
    (
      L.LABEL_ID,
      L.KEY,
      L.VALUE,
      L.BUNDLE_NAME
    )
    VALUES
    (
      SEQ_LABEL_ID.NEXTVAL,
      'EPnote',
      'Only the changes in EP% will impact the timeline.',
      'lm_mobile_label'
    );

COMMIT;

-- DBTicket DB-334

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'TotalTask' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN MATCHED 
THEN 
   UPDATE SET L.VALUE = 'Total Tasks'
WHEN NOT MATCHED
THEN
   INSERT  (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TotalTask',
               'Total Tasks',
               'lm_mobile_label');
			   
COMMIT;

-- DBTicket DB-977

MERGE INTO Label A
     USING (SELECT 'ServerRequestFailedAppLog' Key,'lm_mobile_label' bundle_name FROM DUAL) B
        ON (A.Key = B.Key and A.bundle_name=B.bundle_name)
WHEN MATCHED
THEN
   UPDATE SET
      VALUE =
         'An error occurred while processing this request on the server. Please contact an administrator.';

COMMIT;

-- DBTicket DB-1200
INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (SEQ_XMENU_ITEM_ID.NEXTVAL,
             'Inventory Management',
             'Inventory Management',
             'Dm.screen.InventoryMgmt',
             NULL,
             'menu-inventoryMgmt',
             '#E46C0A',
             0,
             4,
             12,
             '701',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             (SELECT XBASE_MENU_PART_ID
                FROM xbase_menu_part
               WHERE name = 'Mobile Supervisor Authorization'),
             (SELECT xmenu_item_id
                FROM xmenu_item
               WHERE name = 'Inventory Management'),
             SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES ( (SELECT xmenu_item_id
                 FROM xmenu_item
                WHERE name = 'Inventory Management'),
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'WMS'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES ( (SELECT xmenu_item_id
                 FROM xmenu_item
                WHERE name = 'Inventory Management'),
             'VRSLOC',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES ( (SELECT xmenu_item_id
                 FROM xmenu_item
                WHERE name = 'Inventory Management'),
             'VPKLOC',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES ( (SELECT xmenu_item_id
                 FROM xmenu_item
                WHERE name = 'Inventory Management'),
             'WMVITEMINVN',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);
             
COMMIT;             

-- DBTicket DB-1345
INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES ( (SELECT xmenu_item_id
                 FROM xmenu_item
                WHERE name = 'Work Monitoring: Customers'),
             'LMMEXTRNLWMVCUST',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

COMMIT;

-- DBTicket DB-1539
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'InventoryManagement' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InventoryManagement',
               'Inventory Management',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Location' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Location',
               'Location',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LocationClass' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationClass',
               'Location Class',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'onHand' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'onHand',
               'OH',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Allocated' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Allocated',
               'A',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'unallocated' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'unallocated',
               'UA',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'items' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'items',
               'items',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'inventories' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'inventories',
               'inventories',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'total' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'total',
               'TOTAL',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'PLD' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PLD',
               'PLD',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LPN' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPN',
               'LPN',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Item' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Item',
               'Item',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Lock' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Lock',
               'Lock',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Unlock' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Unlock',
               'Unlock',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'cycleCount' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'cycleCount',
               'Cycle Count',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LocationMode' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationMode',
               'Location Mode',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'InventoryMode' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InventoryMode',
               'Inventory Mode',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LockInventoryLocation' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LockInventoryLocation',
               'Lock Inventory/Location',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'checkDetailBeforeProceeding' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'checkDetailBeforeProceeding',
               'Please check the details before proceeding',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Locations' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Locations',
               'Locations',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LpnsPlds' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LpnsPlds',
               'LPNs/PLDs',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'PutawayLock' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PutawayLock',
               'Putaway Lock',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'InventoryLock' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InventoryLock',
               'Inventory Lock',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'ApplyLocksToExistingInvn' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ApplyLocksToExistingInvn',
               'Apply Locks to existing inventory also',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Username' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Username',
               'Username',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Password' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Password',
               'Password',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Signature' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Signature',
               'Signature',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Back' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Back',
               'Back',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'UnlockLocationInventory' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnlockLocationInventory',
               'Unlock Location/Inventory',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Pallet' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Pallet',
               'Pallet',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'CycleCountLocation' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               key,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CycleCountLocation',
               'CycleCountLocation',
               'lm_mobile_label');
               
COMMIT;               

-- DBTicket DB-1802

MERGE INTO LABEL L USING
(SELECT 'lm_mobile_label' BUNDLE_NAME, 'ObservationStatus' KEY FROM DUAL
) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
INSERT
(
L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME
)
VALUES
(
SEQ_LABEL_ID.NEXTVAL,
'ObservationStatus',
'Observation Status',
'lm_mobile_label'
);

COMMIT;

-- DBTicket DB-1974
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'SelectedLocations' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectedLocations',
               'Selected Locations',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'SelectedLPNorPLD' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectedLPNorPLD',
               'Selected LPN/PLD',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'SelectAll' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectAll',
               'Select all',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LockLocation' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LockLocation',
               'LOCK LOCATION',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LockInventory' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LockInventory',
               'LOCK INVENTORY',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Items' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Items',
               'Items',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Mixed' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Mixed',
               'Mixed',
               'lm_mobile_label');
               
COMMIT;

-- DBTicket DB-2103
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LocationCount' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationCount',
               'Location Count',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnlockInventory' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnlockInventory',
               'UNLOCK INVENTORY',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnlockLocation' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnlockLocation',
               'UNLOCK LOCATION',
               'lm_mobile_label');
               
COMMIT;

-- DBTicket DB-2322
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Adjust' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Adjust',
               'Adjust',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'InventoryAdjustment' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InventoryAdjustment',
               'Inventory Adjustment',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'AddNew' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AddNew',
               'Add New',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Quantity' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Quantity',
               'Quantity',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UpdateQuantity' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UpdateQuantity',
               'Update Quantity',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ReasonCode' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ReasonCode',
               'Reason Code',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ItemAttributes' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ItemAttributes',
               'Item Attributes',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'InventoryType' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InventoryType',
               'Inventory Type',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ProductStatus' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ProductStatus',
               'Product Status',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'CountryOfOrigin' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CountryOfOrigin',
               'Country Of Origin',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Lot' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Lot',
               'Lot',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Attr1' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Attr1',
               'Attribute 1',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Attr2' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Attr2',
               'Attribute 2',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Attr3' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Attr3',
               'Attribute 3',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Attr4' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Attr4',
               'Attribute 4',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Attr5' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Attr5',
               'Attribute 5',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'BusinessUnit' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'BusinessUnit',
               'Business Unit',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'CurrentQuantity' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CurrentQuantity',
               'Current Quantity',
               'lm_mobile_label');
               
COMMIT;

-- DBTicket DB-2583
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Transfer' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Transfer',
               'Transfer',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'InventoryTransfer' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InventoryTransfer',
               'Inventory Transfer',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'TransitionalInventoryType' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TransitionalInventoryType',
               'Transitional Inventory Type',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'SelectTypeOfTransfer' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectTypeOfTransfer',
               'Select Type Of Transfer',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LocationTransfer' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationTransfer',
               'Location Transfer',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ItemTransfer' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ItemTransfer',
               'Item Transfer',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'SelectSource' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectSource',
               'Select Source',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'TransferQuantity' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TransferQuantity',
               'Transfer Quantity',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'FromiLPN' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'FromiLPN',
               'From iLPN',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Destination' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Destination',
               'Destination',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'InventoryCategory' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InventoryCategory',
               'Inventory Category',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ToLPN' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ToLPN',
               'To LPN',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Reference1' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Reference1',
               'Reference 1',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Reference2' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Reference2',
               'Reference 2',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnlocatedInbound' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnlocatedInbound',
               'Unlocated Inbound',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnlocatedOutbound' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnlocatedOutbound',
               'Unlocated Outbound',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'TransitionalInventory' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TransitionalInventory',
               'Transitional Inventory',
               'lm_mobile_label');
COMMIT;			   

-- DBTicket DB-2779
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Reslot' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Reslot',
               'Reslot',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ScoreDetail' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ScoreDetail',
               'Score Detail',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'TargetSlotFilter' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TargetSlotFilter',
               'Target Slot Filter',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'SelectOne' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectOne',
               'Select One',
               'lm_mobile_label');
COMMIT;	

-- DBTicket DB-2858
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'CntryOfOrgn' KEY FROM DUAL)
           B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CntryOfOrgn',
               'Country of Origin',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'SelectTransferTypeAndSource' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectTransferTypeAndSource',
               'Select Transfer Type / Source',
               'lm_mobile_label');

COMMIT;

-- DBTicket DB-2907
MERGE INTO BUNDLE_META_DATA A
     USING (SELECT 'dm_mobile_label' AS BUNDLE_NAME FROM DUAL) B
        ON (A.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (BUNDLE_META_DATA_ID,
               BUNDLE_NAME,
               DESCRIPTION,
               DEF_SCREEN_TYPE_ID,
               DEF_APP_ID,
               HINT,
               IS_ACTIVE,
               IS_LABEL_BUNDLE,
               DEF_BU_ID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES (SEQ_BUNDLE_META_DATA_ID.NEXTVAL,
               B.BUNDLE_NAME,
               'WM Mobile application labels',
               10,
               NULL,
               NULL,
               1,
               1,
               -1,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'reset' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Reset',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'done' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Done',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'lastLogin' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Last Login',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scanLocation' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Scan Location',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'validating' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Validating...',
               B.BUNDLE_NAME);
			   
COMMIT;

-- DBTicket DB-2967

-- Mobile App Title
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'makepickcart' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'makepickcart', 'Make Pick Cart', 'dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'makepickcartolpn' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'makepickcartolpn', 'Make Pick Cart - oLPN', 'dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'makepickcarttote' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'makepickcarttote', 'Make Pick Cart - Tote', 'dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'packpickcart' KEY FROM DUAL) B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'packpickcart', 'Pack Pick Cart', 'dm_mobile_label');

-- MPC Flow...
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'pickerId' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'pickerId','Scan [Picker]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'pickCartNbr' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'pickCartNbr','Scan [Cart]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'cartonNbr' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'cartonNbr','Scan [oLPN]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'slotNbr' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'slotNbr','Scan [Slot]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'toteNbr' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'toteNbr','Scan [Tote]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'taskIds' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'taskIds','Scan [Task]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'activity' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'activity','Scan [Activity]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'location' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'location','Scan [Location]','dm_mobile_label');

-- PPC FLOW...
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scannedItem' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'scannedItem','Scan [Item]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scanQty' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'scanQty','Scan [Quantity]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scannedLocn' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'scannedLocn','Scan [Location]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scanCase' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'scanCase','Scan [iLPN]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scannedTote' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'scannedTote','Scan [Tote]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scannedCarton' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'scannedCarton','Scan [oLPN]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scannedSlot' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'scannedSlot','Scan [Slot]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scannedPrintRequestor' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'scannedPrintRequestor','Scan [Print Requestor]','dm_mobile_label');

-- Buttons / Actions
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'CTRLF' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'CTRLF','Record Location','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'CTRLQ' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'CTRLQ','AIL Task','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'EndPickCart' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'EndPickCart','End Cart','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'EndTote' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'EndTote','End Tote','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ListTask' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'ListTask','Task Selection','dm_mobile_label');

-- PPC Buttons / Actions
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'EndOLPNorToteAction' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'EndOLPNorToteAction','End LPN','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'SkipAction' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'SkipAction','Skip Location','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'SkipReplAction' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'SkipReplAction','Skip And Replenish','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'RemoveOlpnOrToteAction' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'RemoveOlpnOrToteAction','Remove LPN','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'EndCartAction' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'EndCartAction','End Cart','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'PartialPickAction' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'PartialPickAction','Pick Partial','dm_mobile_label');

-- UI Label 'Title Case'
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Picker' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Picker','Picker','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Cart' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Cart','Cart','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'oLPN' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'oLPN','oLPN','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Slot' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Slot','Slot','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Tote' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Tote','Tote','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Task' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Task','Task','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Activity' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Activity','Activity','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Location' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Location','Location','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Item' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Item','Item','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Quantity' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Quantity','Quantity','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'iLPN' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'iLPN','iLPN','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Print Requestor' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Print Requestor','Print Requestor','dm_mobile_label');

-- UI Label 'Title Sentance Case'
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'picker' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'picker','picker','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'cart' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'cart','cart','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'slot' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'slot','slot','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'tote' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'tote','tote','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'task' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'task','task','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'activity' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'activity','activity','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'location' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'location','location','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'item' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'item','item','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'quantity' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'quantity','quantity','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'print requestor' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'print requestor','print requestor','dm_mobile_label');

-- LITERAL 'Title Case'
MERGE INTO LITERAL L USING (SELECT 'Picker' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Picker','Picker','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'Cart' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Cart','Cart','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'oLPN' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'oLPN','oLPN','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'Slot' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Slot','Slot','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'Tote' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Tote','Tote','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'Task' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Task','Task','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'Activity' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Activity','Activity','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'Location' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Location','Location','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'Item' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Item','Item','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'Quantity' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Quantity','Quantity','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'iLPN' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'iLPN','iLPN','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'Print Requestor' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'Print Requestor','Print Requestor','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

-- LITERAL 'Sentance Case'
MERGE INTO LITERAL L USING (SELECT 'picker' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'picker','picker','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'cart' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'cart','cart','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'slot' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'slot','slot','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'tote' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'tote','tote','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'task' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'task','task','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'activity' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'activity','activity','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'location' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'location','location','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'item' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'item','item','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'quantity' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'quantity','quantity','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

MERGE INTO LITERAL L USING (SELECT 'print requestor' FROM_LITERAL, 'en' TO_LANG  FROM DUAL) B ON (L.FROM_LITERAL = B.FROM_LITERAL AND L.TO_LANG = B.TO_LANG) 
WHEN NOT MATCHED THEN INSERT (L.LITERAL_ID,L.FROM_LITERAL,L.TO_LITERAL,TO_LANG,L.SCREEN_TYPE_ID,L.CREATED_SOURCE,L.CREATED_DTTM,L.LAST_UPDATED_SOURCE,L.LAST_UPDATED_DTTM,L.BU_ID) 
VALUES (SEQ_LITERAL_ID.nextval,'print requestor','print requestor','en',10,null,CURRENT_TIMESTAMP,null,CURRENT_TIMESTAMP,-1);

-- Mobile UI Related
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'changeLocation' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'changeLocation','Change Location','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'changeTaskGroup' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'changeTaskGroup','Change Task Group','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'changeEquipment' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'changeEquipment','Change Equipment','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'changeWarehouse' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'changeWarehouse','Change Warehouse','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'changeBU' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'changeBU','Change Business Unit','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'changePrinter' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'changePrinter','Change Printer','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'TaskList' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'TaskList','Task List','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Warning' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Warning','Warning','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Information' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Information','Information','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Error' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Error','Error','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Success' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Success','Success','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Cancel' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Cancel','Cancel','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Confirm' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Confirm','Confirm','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Ok' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Ok','Ok','dm_mobile_label');


commit;

-- DBTicket DB-2997
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'taskGroupInfoMessage' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 B.KEY,
                 'Choose a task group. You may also need to choose an equipment.',
                 B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'equipmentInfoMessage' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Choose an equipment (Optional).',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'locationInfoMessage' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Scan your current location (Optional).',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'warehouseInfoMessage' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 B.KEY,
                 'Choose a warehouse. You must choose business unit in order to continue.',
                 B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'buInfoMessage' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Choose a business unit (Required).',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'printerInfoMessage' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Choose a Printer (Optional).',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'defaultInfoMessage' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 B.KEY,
                 'Choose a parameter to review available values for each option',
                 B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'userInfoText' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Please choose an option to continue',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'userInfoTextReview' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Some values may be changed, please review',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'taskGroupNotAvailable' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Task Groups are not available',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'equipmentNotAvailable' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Equipments are not available',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'warehouseNotAvailable' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Warehouses are not available',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'buNotAvailable' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Business Units are not available',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'printerNotAvailable' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Printers are not available',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'User Profile' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'User Profile',
               B.BUNDLE_NAME);

UPDATE xmenu_item
   SET name = 'User Profile', short_name = 'User Profile'
 WHERE navigation_key = 'Mobilepicking.screen.parameter.Parameter';

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'User Profile' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'User Profile',
               B.BUNDLE_NAME);
               
COMMIT;

-- DBTicket DB-3019
UPDATE label
   SET VALUE = 'Business Unit'
 WHERE key = 'changeBU' AND bundle_name = 'dm_mobile_label';

UPDATE label
   SET VALUE = 'Equipment'
 WHERE key = 'changeEquipment' AND bundle_name = 'dm_mobile_label';

UPDATE label
   SET VALUE = 'Location'
 WHERE key = 'changeLocation' AND bundle_name = 'dm_mobile_label';

UPDATE label
   SET VALUE = 'Printer'
 WHERE key = 'changePrinter' AND bundle_name = 'dm_mobile_label';

UPDATE label
   SET VALUE = 'Task Group'
 WHERE key = 'changeTaskGroup' AND bundle_name = 'dm_mobile_label';

UPDATE label
   SET VALUE = 'Warehouse'
 WHERE key = 'changeWarehouse' AND bundle_name = 'dm_mobile_label';
 
COMMIT;

-- DBTicket DB-3098

-- Drop exisitng LABEL
delete from LABEL where BUNDLE_NAME = 'dm_mobile_label' and KEY = 'scanQty';

-- Add new LABEL
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scanQty' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'scanQty','Enter [Quantity]','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'EndOlpnOrToteAction' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'EndOlpnOrToteAction','End LPN','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ShortAction' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'ShortAction','Short','dm_mobile_label');

-- Add New Column to "TRAN_MASTER" Table.
-- Moved to CBO
--ALTER TABLE TRAN_MASTER ADD WORKFLOW_SCRIPT_NAME VARCHAR2(200); 
--COMMENT ON COLUMN TRAN_MASTER.WORKFLOW_SCRIPT_NAME IS 'Columns stores the DSL Or Workflow name';

-- New Syscode ID for Syscode - "S754"
-- Moved to CBOBASE.
--Insert into SYS_CODE (REC_TYPE,CODE_TYPE,CODE_ID,CODE_DESC,SHORT_DESC,MISC_FLAGS,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,WM_VERSION_ID,SYS_CODE_ID,SYS_CODE_TYPE_ID,CREATED_DTTM,LAST_UPDATED_DTTM) 
--values ('S','754','M','Mobile','M',null,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SEED',1,SYS_CODE_ID_SEQ.NEXTVAL,(select SYS_CODE_TYPE_ID from SYS_CODE_TYPE where REC_TYPE = 'S' and CODE_TYPE = --'754'),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

commit;

-- DBTicket DB-3078
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Details' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Details',
               'Details',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'AllocatedQty' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AllocatedQty',
               'Allocated Qty',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Barcode' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Barcode',
               'Barcode',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'SeasonYr' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SeasonYr',
               'Season Year',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Style' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Style',
               'Style',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Color' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Color',
               'Color',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Channel' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Channel',
               'Channel',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ProductType' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ProductType',
               'Product Type',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'HazmatCode' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'HazmatCode',
               'Hazmat Code',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'QtyUom' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'QtyUom',
               'Qty UOM',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'BaseUom' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'BaseUom',
               'Base UOM',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnitLength' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnitLength',
               'Unit Length',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnitWidth' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnitWidth',
               'Unit Width',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnitHeight' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnitHeight',
               'Unit Height',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnitVolume' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnitVolume',
               'Unit Volume',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'UnitWeight' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UnitWeight',
               'Unit Weight',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Class' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Class',
               'Class',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'ItemSrlNbrTrkedMobileActionNotAvbl' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ItemSrlNbrTrkedMobileActionNotAvbl',
               'Item is serial number tracked, mobile action not available',
               'lm_mobile_label');
COMMIT;

-- DBTicket DB-3071
INSERT INTO XBASE_MENU_PART (XBASE_MENU_PART_ID,
                             XBASE_MENU_SECTION_ID,
                             name,
                             sequence,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_PART_ID.NEXTVAL,
             (SELECT XBASE_MENU_SECTION_ID
                FROM XBASE_MENU_SECTION
               WHERE name = 'Labor Management Touch'),
             'Labor Management',
             4,
             1,
             CURRENT_TIMESTAMP,
             1,
             CURRENT_TIMESTAMP);
             
INSERT INTO XBASE_MENU_PART (XBASE_MENU_PART_ID,
                             XBASE_MENU_SECTION_ID,
                             name,
                             sequence,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_PART_ID.NEXTVAL,
             (SELECT XBASE_MENU_SECTION_ID
                FROM XBASE_MENU_SECTION
               WHERE NAME = 'Labor Management Touch'),
             'Work Monitoring',
             5,
             1,
             CURRENT_TIMESTAMP,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XBASE_MENU_PART (XBASE_MENU_PART_ID,
                             XBASE_MENU_SECTION_ID,
                             name,
                             sequence,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_PART_ID.NEXTVAL,
             (SELECT XBASE_MENU_SECTION_ID
                FROM XBASE_MENU_SECTION
               WHERE NAME = 'Labor Management Touch'),
             'Inventory Management',
             6,
             1,
             CURRENT_TIMESTAMP,
             1,
             CURRENT_TIMESTAMP);

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID = (SELECT XMENU_ITEM_ID
                          FROM XMENU_ITEM
                         WHERE NAVIGATION_KEY = 'lm.screen.EmpMgmt');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM XMENU_ITEM
            WHERE NAVIGATION_KEY = 'lm.screen.SpvsrAuthorization');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM XMENU_ITEM
            WHERE NAVIGATION_KEY = 'lm.screen.ObservationManagement');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM XMENU_ITEM
            WHERE NAVIGATION_KEY = 'lm.screen.PerformanceMangament');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID = (SELECT XMENU_ITEM_ID
                          FROM XMENU_ITEM
                         WHERE NAVIGATION_KEY = 'lm.screen.TeamAssignment');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM XMENU_ITEM
            WHERE NAVIGATION_KEY = 'lm.screen.ReflectiveAssignment');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID = (SELECT XMENU_ITEM_ID
                          FROM XMENU_ITEM
                         WHERE NAVIGATION_KEY = 'lm.screen.WorkStandards');

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE NAVIGATION_KEY = 'lm.screen.EmpMgmt';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE NAVIGATION_KEY = 'lm.screen.SpvsrAuthorization';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE NAVIGATION_KEY = 'lm.screen.ObservationManagement';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE NAVIGATION_KEY = 'lm.screen.PerformanceMangament';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE NAVIGATION_KEY = 'lm.screen.TeamAssignment';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE NAVIGATION_KEY = 'lm.screen.ReflectiveAssignment';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Labor Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE NAVIGATION_KEY = 'lm.screen.WorkStandards';

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM XMENU_ITEM
            WHERE NAVIGATION_KEY =
                     'maui://Dm.screen.taskmgmt.CustomerViewScreen');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM XMENU_ITEM
            WHERE NAVIGATION_KEY =
                     'maui://Dm.screen.taskmgmt.FacilityViewScreen');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM XMENU_ITEM
            WHERE NAVIGATION_KEY = 'maui://Dm.screen.jobfunction.DmJFScreen');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM XMENU_ITEM
            WHERE NAVIGATION_KEY = 'maui://Dm.screen.taskmgmt.TaskHdrList');

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID =
          (SELECT XMENU_ITEM_ID
             FROM XMENU_ITEM
            WHERE NAVIGATION_KEY = 'maui://Dm.screen.wave.DmWaveScreen');

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch')),
       name = 'Tasks',
       short_name = 'Tasks'
 WHERE NAVIGATION_KEY = 'maui://Dm.screen.taskmgmt.TaskHdrList';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch')),
       name = 'Departments',
       short_name = 'Departments'
 WHERE NAVIGATION_KEY = 'maui://Dm.screen.taskmgmt.FacilityViewScreen';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch')),
       name = 'Waves',
       short_name = 'Waves'
 WHERE NAVIGATION_KEY = 'maui://Dm.screen.wave.DmWaveScreen';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch')),
       name = 'Job Functions',
       short_name = 'Job Functions'
 WHERE NAVIGATION_KEY = 'maui://Dm.screen.jobfunction.DmJFScreen';

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Work Monitoring'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch')),
       BASE_SECTION_NAME =
          (SELECT XBASE_MENU_SECTION_ID
             FROM XBASE_MENU_SECTION
            WHERE name = 'Labor Management Touch'),
       name = 'Customers',
       short_name = 'Customers'
 WHERE NAVIGATION_KEY = 'maui://Dm.screen.taskmgmt.CustomerViewScreen';

UPDATE XBASE_MENU_ITEM
   SET XBASE_MENU_PART_ID =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Inventory Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE XMENU_ITEM_ID = (SELECT XMENU_ITEM_ID
                          FROM XMENU_ITEM
                         WHERE NAVIGATION_KEY = 'Dm.screen.InventoryMgmt');

UPDATE XMENU_ITEM
   SET BASE_PART_NAME =
          (SELECT XBASE_MENU_PART_ID
             FROM XBASE_MENU_PART
            WHERE     name = 'Inventory Management'
                  AND XBASE_MENU_SECTION_ID =
                         (SELECT XBASE_MENU_SECTION_ID
                            FROM XBASE_MENU_SECTION
                           WHERE name = 'Labor Management Touch'))
 WHERE NAVIGATION_KEY = 'Dm.screen.InventoryMgmt';
 
COMMIT; 

-- DBTicket DB-3185
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Reslot_SC' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Reslot_SC',
               'Reslot',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LocationSearch' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationSearch',
               'Location Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ItemSearch' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ItemSearch',
               'Item Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LPNSearch' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPNSearch',
               'LPN Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LPNs' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPNs',
               'LPNs',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'InInvNotPA' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InInvNotPA',
               'In Inv, not P/A',
               'lm_mobile_label');
COMMIT;

-- DBTicket DB-3028
CREATE TABLE HIERARCHY_TYPES
(
   HIERARCHY_TYPE_ID          NUMBER (8) NOT NULL,
   NAME                       VARCHAR2 (50) NOT NULL,
   DOMAIN_OBJECT              VARCHAR2 (50) NOT NULL,
   DOMAIN_OBJECT_ATTRIBUTE    VARCHAR2 (50) NOT NULL,
   CREATED_SOURCE_TYPE        NUMBER (4) NOT NULL,
   CREATED_SOURCE             VARCHAR2 (25),
   CREATED_DTTM               TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
   LAST_UPDATED_SOURCE_TYPE   NUMBER (4) NOT NULL,
   LAST_UPDATED_SOURCE        VARCHAR2 (25),
   LAST_UPDATED_DTTM          TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
)
TABLESPACE LLMDATA;

ALTER TABLE HIERARCHY_TYPES ADD CONSTRAINT PK_HIERARCHY_TYPES PRIMARY KEY (HIERARCHY_TYPE_ID);

COMMENT ON TABLE HIERARCHY_TYPES IS 'Contains hierarchy types';
COMMENT ON COLUMN HIERARCHY_TYPES.HIERARCHY_TYPE_ID IS 'Primary key';
COMMENT ON COLUMN HIERARCHY_TYPES.NAME IS 'Hierarchy name';
COMMENT ON COLUMN HIERARCHY_TYPES.DOMAIN_OBJECT IS 'User Selected - DB Table';
COMMENT ON COLUMN HIERARCHY_TYPES.DOMAIN_OBJECT_ATTRIBUTE IS 'User Selected - DB Column of Object Table';
COMMENT ON COLUMN HIERARCHY_TYPES.CREATED_SOURCE IS 'Create by source';
COMMENT ON COLUMN HIERARCHY_TYPES.CREATED_SOURCE_TYPE IS 'Created by source type';
COMMENT ON COLUMN HIERARCHY_TYPES.CREATED_DTTM IS 'Created Date Time - Audit Field';
COMMENT ON COLUMN HIERARCHY_TYPES.LAST_UPDATED_SOURCE IS 'Last Updated Source - Audit field';
COMMENT ON COLUMN HIERARCHY_TYPES.LAST_UPDATED_SOURCE_TYPE IS 'Last Updated Source Type - Audit field';
COMMENT ON COLUMN HIERARCHY_TYPES.LAST_UPDATED_DTTM IS 'Last Updated Date Time - Audit field';

CREATE SEQUENCE SEQ_HIERARCHY_TYPES_TYPE_ID START WITH 1 INCREMENT BY 1;
CALL SEQUPDT('HIERARCHY_TYPES','HIERARCHY_TYPE_ID','SEQ_HIERARCHY_TYPES_TYPE_ID');

INSERT INTO HIERARCHY_TYPES (HIERARCHY_TYPE_ID,
                             NAME,
                             DOMAIN_OBJECT,
                             DOMAIN_OBJECT_ATTRIBUTE,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_HIERARCHY_TYPES_TYPE_ID.NEXTVAL,
             'Task Type Hierarchy',
             'TASK_HDR',
             'TASK_TYPE',
             1,
             'Manhattan Associates',
             current_timestamp,
             1,
             'Manhattan Associates',
             current_timestamp);


INSERT INTO HIERARCHY_TYPES (HIERARCHY_TYPE_ID,
                             NAME,
                             DOMAIN_OBJECT,
                             DOMAIN_OBJECT_ATTRIBUTE,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_HIERARCHY_TYPES_TYPE_ID.NEXTVAL,
             'Inventory Need Type Hierarchy',
             'TASK_HDR',
             'INVN_NEED_TYPE',
             1,
             'Manhattan Associates',
             current_timestamp,
             1,
             'Manhattan Associates',
             current_timestamp);

INSERT INTO HIERARCHY_TYPES (HIERARCHY_TYPE_ID,
                             NAME,
                             DOMAIN_OBJECT,
                             DOMAIN_OBJECT_ATTRIBUTE,
                             CREATED_SOURCE_TYPE,
                             CREATED_SOURCE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_HIERARCHY_TYPES_TYPE_ID.NEXTVAL,
             'Customer Hierarchy',
             'CUSTOMER',
             'CUSTOMER_ID',
             1,
             'Manhattan Associates',
             current_timestamp,
             1,
             'Manhattan Associates',
             current_timestamp);
             
COMMIT;             

-- DBTicket DB-3225
INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        ICON,
                        BGCOLOR,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        Screen_version,
                        Screen_mode)
        VALUES (
                  SEQ_XMENU_ITEM_ID.NEXTVAL,
                  'Location Search',
                  'Location Search',
                  'Dm.screen.InventoryMgmt.LocationSearch',
                  'menu-location',
                  '#8E9300',
                  '12',
                  (SELECT XBASE_MENU_PART_ID
                     FROM XBASE_MENU_PART
                    WHERE     name = 'Inventory Management'
                          AND XBASE_MENU_SECTION_ID =
                                 (SELECT XBASE_MENU_SECTION_ID
                                    FROM XBASE_MENU_SECTION
                                   WHERE name = 'Labor Management Touch')),
                  '4',
                  '0');

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE)
        VALUES (
                  SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
                  (SELECT XBASE_MENU_PART_ID
                     FROM XBASE_MENU_PART
                    WHERE     name = 'Inventory Management'
                          AND XBASE_MENU_SECTION_ID =
                                 (SELECT XBASE_MENU_SECTION_ID
                                    FROM XBASE_MENU_SECTION
                                   WHERE name = 'Labor Management Touch')),
                  (SELECT Xmenu_Item_Id
                     FROM Xmenu_Item
                    WHERE     Name = 'Location Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LocationSearch'),
                  SEQ_XBASE_MENU_ITEM_ID.NEXTVAL);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     Name = 'Location Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LocationSearch'),
                  (SELECT APP_ID
                     FROM APP
                    WHERE app_short_name = 'WMS'));

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     name = 'Location Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LocationSearch'),
                  'VPKLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     name = 'Location Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LocationSearch'),
                  'VRSLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     name = 'Location Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LocationSearch'),
                  'WMVITEMINVN');

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        ICON,
                        BGCOLOR,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        Screen_version,
                        Screen_mode)
        VALUES (
                  SEQ_XMENU_ITEM_ID.NEXTVAL,
                  'LPN Search',
                  'LPN Search',
                  'Dm.screen.InventoryMgmt.LpnSearch',
                  'menu-lpn',
                  '#8E9300',
                  '12',
                  (SELECT XBASE_MENU_PART_ID
                     FROM XBASE_MENU_PART
                    WHERE     name = 'Inventory Management'
                          AND XBASE_MENU_SECTION_ID =
                                 (SELECT XBASE_MENU_SECTION_ID
                                    FROM XBASE_MENU_SECTION
                                   WHERE name = 'Labor Management Touch')),
                  '4',
                  '0');


INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE)
        VALUES (
                  SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
                  (SELECT XBASE_MENU_PART_ID
                     FROM XBASE_MENU_PART
                    WHERE     name = 'Inventory Management'
                          AND XBASE_MENU_SECTION_ID =
                                 (SELECT XBASE_MENU_SECTION_ID
                                    FROM XBASE_MENU_SECTION
                                   WHERE name = 'Labor Management Touch')),
                  (SELECT Xmenu_Item_Id
                     FROM Xmenu_Item
                    WHERE     Name = 'LPN Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LpnSearch'),
                  SEQ_XBASE_MENU_ITEM_ID.NEXTVAL);


INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     Name = 'LPN Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LpnSearch'),
                  (SELECT APP_ID
                     FROM APP
                    WHERE app_short_name = 'WMS'));

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     name = 'LPN Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LpnSearch'),
                  'VPKLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     name = 'LPN Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LpnSearch'),
                  'VRSLOC');


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     name = 'LPN Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.LpnSearch'),
                  'WMVITEMINVN');

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        ICON,
                        BGCOLOR,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        Screen_version,
                        Screen_mode)
        VALUES (
                  SEQ_XMENU_ITEM_ID.NEXTVAL,
                  'Item Search',
                  'Item Search',
                  'Dm.screen.InventoryMgmt.ItemSearch',
                  'menu-Item',
                  '#8E9300',
                  '12',
                  (SELECT XBASE_MENU_PART_ID
                     FROM XBASE_MENU_PART
                    WHERE     name = 'Inventory Management'
                          AND XBASE_MENU_SECTION_ID =
                                 (SELECT XBASE_MENU_SECTION_ID
                                    FROM XBASE_MENU_SECTION
                                   WHERE name = 'Labor Management Touch')),
                  '4',
                  '0');


INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE)
        VALUES (
                  SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
                  (SELECT XBASE_MENU_PART_ID
                     FROM XBASE_MENU_PART
                    WHERE     name = 'Inventory Management'
                          AND XBASE_MENU_SECTION_ID =
                                 (SELECT XBASE_MENU_SECTION_ID
                                    FROM XBASE_MENU_SECTION
                                   WHERE name = 'Labor Management Touch')),
                  (SELECT Xmenu_Item_Id
                     FROM Xmenu_Item
                    WHERE     Name = 'Item Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.ItemSearch'),
                  SEQ_XBASE_MENU_ITEM_ID.NEXTVAL);


INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     Name = 'Item Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.ItemSearch'),
                  (SELECT APP_ID
                     FROM APP
                    WHERE app_short_name = 'WMS'));

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     name = 'Item Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.ItemSearch'),
                  'VPKLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     name = 'Item Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.ItemSearch'),
                  'VRSLOC');


INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
        VALUES (
                  (SELECT XMENU_ITEM_ID
                     FROM XMENU_ITEM
                    WHERE     name = 'Item Search'
                          AND NAVIGATION_KEY =
                                 'Dm.screen.InventoryMgmt.ItemSearch'),
                  'WMVITEMINVN');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'Inventory Management: Location Search' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Inventory Management: Location Search',
               'Inventory Management: Location Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'Inventory Management: Item Search' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Inventory Management: Item Search',
               'Inventory Management: Item Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'Inventory Management: LPN Search' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Inventory Management: LPN Search',
               'Inventory Management: LPN Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Location Search' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Location Search',
               'Location Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LPN Search' KEY FROM DUAL)
           B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPN Search',
               'LPN Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Item Search' KEY FROM DUAL)
           B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Item Search',
               'Item Search',
               'lm_mobile_label');

DELETE FROM XMENU_ITEM_PERMISSION
      WHERE XMENU_ITEM_ID =
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     name = 'Inventory Management'
                       AND NAVIGATION_KEY = 'Dm.screen.InventoryMgmt');

DELETE FROM XMENU_ITEM_APP
      WHERE XMENU_ITEM_ID =
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     name = 'Inventory Management'
                       AND NAVIGATION_KEY = 'Dm.screen.InventoryMgmt');

DELETE FROM XBASE_MENU_ITEM
      WHERE XMENU_ITEM_ID =
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     name = 'Inventory Management'
                       AND NAVIGATION_KEY = 'Dm.screen.InventoryMgmt');

DELETE FROM xquick_menu_item
      WHERE XMENU_ITEM_ID =
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     name = 'Inventory Management'
                       AND NAVIGATION_KEY = 'Dm.screen.InventoryMgmt');

DELETE FROM xmenu_item_parameter
      WHERE XMENU_ITEM_ID =
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     name = 'Inventory Management'
                       AND NAVIGATION_KEY = 'Dm.screen.InventoryMgmt');


DELETE FROM XMENU_ITEM
      WHERE XMENU_ITEM_ID =
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE     name = 'Inventory Management'
                       AND NAVIGATION_KEY = 'Dm.screen.InventoryMgmt');
                       
COMMIT;

-- DBTicket DB-3266
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'CntryofOrigin' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Country of Origin',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'InventoryType' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Inventory Type',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ProdcutStatus' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Product Status',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Lot' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Lot',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Attr1' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Item Attr 1',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Attr2' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Item Attr 2',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Attr3' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Item Attr 3',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Attr4' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Item Attr 4',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Attr5' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Item Attr 5',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Gen Batch' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Gen Batch',
               B.BUNDLE_NAME);
               
COMMIT;

--DBTicket DB-3332
UPDATE LABEL
   SET VALUE = 'End Cart/Zone'
 WHERE KEY = 'EndCartAction' AND BUNDLE_NAME = 'dm_mobile_label';

COMMIT;

--DBTicket DB-3333
UPDATE LABEL
   SET VALUE = 'Skip'
 WHERE KEY = 'SkipAction' AND BUNDLE_NAME = 'dm_mobile_label';
 
COMMIT;

-- DBTicket DB-3369
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scannedTrackCase' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'scannedTrackCase',
               'Scan ILPN',
               'dm_mobile_label');
               
COMMIT;

-- DBTicket DB-3361
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'DisplayLocation' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'DisplayLocation',
               'Display Location',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LocationBarcode' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationBarcode',
               'Location Barcode',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'WorkGrpWorkArea' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'WorkGrpWorkArea',
               'Work Group/Work Area',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ItemDedicationType' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ItemDedicationType',
               'Item Dedication Type',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'LocationAdvancedSearch' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationAdvancedSearch',
               'Location Advanced Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LPNAdvancedSearch' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPNAdvancedSearch',
               'LPN Advanced Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ItemAdvancedSearch' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ItemAdvancedSearch',
               'Item Advanced Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Displaying' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Displaying',
               'Displaying',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'LocationsAsSearchResults' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationsAsSearchResults',
               'locations as search results',
               'lm_mobile_label'); 
COMMIT;

-- DBTicket DB-3397
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Displaying' KEY FROM DUAL)
           B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Displaying',
               'Displaying',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'LocationAdvancedSearch' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationAdvancedSearch',
               'Location Advanced Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LpnAdvancedSearch' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LpnAdvancedSearch',
               'LPN Advanced Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ItemAdvancedSearch' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ItemAdvancedSearch',
               'Item Advanced Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'locationsForSearchCriteria' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'locationsForSearchCriteria',
               'Locations for Search Criteria',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'DisplayLocation' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'DisplayLocation',
               'Display Location',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LocationBarcode' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LocationBarcode',
               'Location Barcode',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'WorkGrpWorkArea' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'WorkGrpWorkArea',
               'Work Group/Work Area',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ItemDedicationType' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ItemDedicationType',
               'Item Dedication Type',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'TypeMoreToRefineSearch' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TypeMoreToRefineSearch',
               'Type More to Refine Search',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Shipment' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Shipment',
               'Shipment',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ASN' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ASN',
               'ASN',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LpnType' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LpnType',
               'LPN Type',
               'lm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ItemBarcode' KEY FROM DUAL)
           B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ItemBarcode',
               'Item Barcode',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 's' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               's',
               's',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'selected' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'selected',
               'selected',
               'lm_mobile_label');

UPDATE LABEL
   SET VALUE = 'Re-Slot'
 WHERE key = 'Reslot_SC' AND BUNDLE_NAME = 'lm_mobile_label';

UPDATE LABEL
   SET VALUE = 'RE-SLOT'
 WHERE key = 'Reslot' AND BUNDLE_NAME = 'lm_mobile_label';

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'IBFacilityStatusFrom' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'IBFacilityStatusFrom',
               'I/B Facility Status From',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'IBFacilityStatusTo' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'IBFacilityStatusTo',
               'I/B Facility Status To',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'OBFacilityStatusTo' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'OBFacilityStatusTo',
               'O/B Facility Status To',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'OBFacilityStatusFrom' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'OBFacilityStatusFrom',
               'O/B Facility Status From',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'BusinessPartner' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'BusinessPartner',
               'Business Partner',
               'lm_mobile_label');
               
COMMIT;

-- DBTicket DB-3431
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'unitToWeigh' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'unitToWeigh',
               'unit to weigh',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'packToWeigh' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'packToWeigh',
               'Pack to weigh',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'enterWeight' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'enterWeight',
               'Enter weight',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'unitsToWeigh' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'unitsToWeigh',
               'units to weigh',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'packsToWeigh' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'packsToWeigh',
               'Packs to weigh',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scanWt' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'scanWt',
               'Enter weight',
               'dm_mobile_label');
               
COMMIT;

-- DBTicket DB-3420

ALTER TABLE HIERARCHY_TYPES ADD CONSTRAINT UNIQUE_HIERARCHY_TYPE UNIQUE (NAME) USING INDEX TABLESPACE MLMINDX;

ALTER TABLE HIERARCHY_TYPES ADD CONSTRAINT UNIQUE_HIERARCHY_OBJECT UNIQUE (DOMAIN_OBJECT, DOMAIN_OBJECT_ATTRIBUTE) USING INDEX TABLESPACE MLMINDX;

UPDATE HIERARCHY_TYPES SET NAME = 'TASK' WHERE NAME = 'Task Type Hierarchy';

UPDATE HIERARCHY_TYPES SET NAME = 'CUSTOMER',DOMAIN_OBJECT = 'CUSTOMER_TASK_VIEW' WHERE NAME = 'Customer Hierarchy';

ALTER TABLE HIERARCHY_DEFINITIONS ADD  HIERARCHY_TYPE_ID NUMBER(8)  DEFAULT 0 NOT NULL;

UPDATE HIERARCHY_DEFINITIONS SET HIERARCHY_TYPE_ID = (SELECT MIN(HIERARCHY_TYPE_ID) FROM HIERARCHY_TYPES);

UPDATE HIERARCHY_DEFINITIONS SET HIERARCHY_TYPE_ID = (SELECT HIERARCHY_TYPE_ID FROM HIERARCHY_TYPES WHERE NAME = 'TASK') WHERE HIERARCHY_TYPE = 'TASK';

UPDATE HIERARCHY_DEFINITIONS SET HIERARCHY_TYPE_ID = (SELECT HIERARCHY_TYPE_ID FROM HIERARCHY_TYPES WHERE NAME = 'CUSTOMER') WHERE HIERARCHY_TYPE = 'CUSTOMER';
  
ALTER TABLE HIERARCHY_DEFINITIONS  ADD CONSTRAINT FK_HIERARCHY_TYPE_ID  FOREIGN KEY (HIERARCHY_TYPE_ID) references HIERARCHY_TYPES (HIERARCHY_TYPE_ID);

commit;

-- DBTicket DB-3462
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Type' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Type','Type','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Size' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Size','Size','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'TypeSizeDescription' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'TypeSizeDescription','Type/Size Description','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Description' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Description','Description','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ItemDescription' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'ItemDescription','Item Description','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ItemBarcode' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'ItemBarcode','Item Barcode','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Barcode' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'Barcode','Barcode','dm_mobile_label');

commit ;

-- DBTicket DB-3503
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ReasonCode' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ReasonCode',
               'Reason Code',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ReferenceCode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ReferenceCode',
               'Reference Code',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Pack' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Pack',
               'Pack',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Unit' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Unit',
               'Unit',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'iLPN' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'iLPN',
               'iLPN',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'MULTITOTE' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'MULTITOTE',
               'MULTITOTE',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'MULTIOLPN' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'MULTIOLPN',
               'MULTIOLPN',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'reasonCode' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'reasonCode',
               'Enter Reason Code',
               'dm_mobile_label');
COMMIT;

-- DBTicket DB-3498
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ESignature' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ESignature',
               'Electronic Signature',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'CurrentUser' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CurrentUser',
               'Current User',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Date/Time' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Date/Time',
               'Date/Time',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ESignUserId' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ESignUserId',
               'E-Sign User ID',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LegalDisclaimer' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LegalDisclaimer',
               'Legal Disclaimer',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LegalDisclaimerText' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'LegalDisclaimerText',
                 'Your electronic signature is the legally binding equivalent of your handwritten signature. Confirm you understand to proceed with signoff.',
                 'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'SignOffOkCancel' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SignOffOkCancel',
               'Select OK to signoff this document or CANCEL to cancel.',
               'lm_mobile_label');
COMMIT;

-- DBTicket DB-3517
MERGE INTO BUNDLE_META_DATA BM
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME,
                   'Hierarchy UI Configuration' DESCRIPTION
              FROM DUAL) B
        ON (BM.DESCRIPTION = B.DESCRIPTION AND BM.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (BM.BUNDLE_META_DATA_ID,
               BM.BUNDLE_NAME,
               BM.DESCRIPTION,
               BM.DEF_SCREEN_TYPE_ID,
               BM.DEF_APP_ID,
               BM.HINT,
               BM.IS_ACTIVE,
               BM.IS_LABEL_BUNDLE,
               BM.DEF_BU_ID,
               BM.CREATED_DTTM,
               BM.LAST_UPDATED_DTTM)
       VALUES (SEQ_BUNDLE_META_DATA_ID.NEXTVAL,
               'WmsHierarchy',
               'Hierarchy UI Configuration',
               10,
               NULL,
               NULL,
               1,
               1,
               -1,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'name' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'name',
               'Name',
               'WmsHierarchy');


MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'description' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'description',
               'Description',
               'WmsHierarchy');


MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'hierarchyType' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'hierarchyType',
               'Type',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'isActive' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'isActive',
               'Active',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'company' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'company',
               'Company',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'domainAttr1' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'domainAttr1',
               'Attribute',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'domainAttr2' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'domainAttr2',
               'Secondary Attribute',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'levelNumber' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'levelNumber',
               'Level',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'nodeContentType1' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'nodeContentType1',
               'Content Type',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'nodeContentType1Id' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'nodeContentType1Id',
               'Content',
               'WmsHierarchy');


MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'nodeContentType2' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'nodeContentType2',
               'Secondary Content Type',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'nodeContentType2Id' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'nodeContentType2Id',
               'Secondary Content',
               'WmsHierarchy');


MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'parentHierarchyNodeId' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'parentHierarchyNodeId',
               'Parent',
               'WmsHierarchy');


MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'levelEdit' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'levelEdit',
               'Level',
               'WmsHierarchy');


MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'nodes' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'nodes',
               'Nodes',
               'WmsHierarchy');
			   
COMMIT;

-- DBTicket DB-3507
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scannedCheckDigit' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'scannedCheckDigit',
               'Scan Check Digit',
               'dm_mobile_label');
--Skip
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'SkipActionInAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SkipActionInAggrMode',
               'Skip',
               'dm_mobile_label');
--Short
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ShortActionInAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ShortActionInAggrMode',
               'Short',
               'dm_mobile_label');
--Skip And Replenish
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'SkipReplActionInAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SkipReplActionInAggrMode',
               'Skip And Replenish',
               'dm_mobile_label');                         
COMMIT;	

-- DBTicket DB-3532
MERGE INTO TRAN_PARM tpTrg
     USING (SELECT 'Mobile_Pack_Pick_Cart' pgm_id, 63 from_posn FROM DUAL)
           tpKey
        ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
   INSERT     (PGM_ID,
               FROM_POSN,
               TO_POSN,
               PARM_DESC,
               LITRL,
               MANDT_FLAG,
               VALID_CODE,
               VALID_SYS_CODE_REC_TYPE,
               VALID_SYS_CODE_TYPE,
               VALID_FROM,
               VALID_TO,
               VALID_VALUES,
               EDIT_STYLE,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               TRAN_PARM_ID,
               WM_VERSION_ID,
               PARM_ID,
               PARM_KEY,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('Mobile_Pack_Pick_Cart',
               63,
               63,
               'Pick Cart Entry Mode',
               'Pick Cart Entry Mode',
               'Y',
               'S',
               'S',
               'D48',
               NULL,
               NULL,
               NULL,
               'DDDW',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               TRAN_PARM_ID_SEQ.NEXTVAL,
               1,
               NULL,
               NULL,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

MERGE INTO TRAN_MASTER_PARM TMP
     USING (SELECT TRAN_ID, 63 FROM_POSN
              FROM TRAN_MASTER
             WHERE PGM_ID = 'Mobile_Pack_Pick_Cart') TM
        ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
   INSERT     (TRAN_ID,
               FROM_POSN,
               MENU_PARM,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               WHSE_MASTER_ID,
               CD_MASTER_ID,
               TRAN_MASTER_PARM_ID)
       VALUES (TM.TRAN_ID,
               63,
               1,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               1,
               NULL,
               NULL,
               TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);
               
COMMIT;

-- DBTicket DB-3599
-----related to DB-3533 Product entries
INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES ( (SELECT xmenu_item_id
                 FROM xmenu_item
                WHERE name = 'Item Inventory'),
             'EXTWMVITEMINVN',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE Name = 'Location Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.LocationSearch'),
               (SELECT APP_ID
                  FROM APP
                 WHERE app_short_name = 'LM'));

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE name = 'Location Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.LocationSearch'),
               'VEXTWMPKLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE name = 'Location Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.LocationSearch'),
               'VEXTWMRSLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE name = 'Location Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.LocationSearch'),
               'EXTWMVITEMINVN');

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE Name = 'LPN Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.LpnSearch'),
               (SELECT APP_ID
                  FROM APP
                 WHERE app_short_name = 'LM'));

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE name = 'LPN Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.LpnSearch'),
               'VEXTWMPKLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE name = 'LPN Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.LpnSearch'),
               'VEXTWMRSLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE name = 'LPN Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.LpnSearch'),
               'EXTWMVITEMINVN');

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE Name = 'Item Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.ItemSearch'),
               (SELECT APP_ID
                  FROM APP
                 WHERE app_short_name = 'LM'));

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE name = 'Item Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.ItemSearch'),
               'VEXTWMPKLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE name = 'Item Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.ItemSearch'),
               'VEXTWMRSLOC');

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE)
     VALUES (
               (SELECT XMENU_ITEM_ID
                  FROM XMENU_ITEM
                 WHERE name = 'Item Search'
                       AND NAVIGATION_KEY =
                              'Dm.screen.InventoryMgmt.ItemSearch'),
               'EXTWMVITEMINVN');
COMMIT;

-- DBTicket DB-3645
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'EndLpnActionInPartialPickAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'End LPN',
               B.BUNDLE_NAME);

---SkipActionInPartialPickAggrMode

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'SkipActionInPartialPickAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Skip',
               B.BUNDLE_NAME);

---ShortActionInPartialPickAggrMode

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'ShortActionInPartialPickAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Short',
               B.BUNDLE_NAME);
COMMIT;

-- DBTicket DB-3649
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'TaskId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Task Id',
               B.BUNDLE_NAME);
			   
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'WG/WA' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'WG/WA',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Priority' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Priority',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'SelectTaskToStartPicking' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Select Task To Start Picking',
               B.BUNDLE_NAME);

--labels for reason code

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'referenceCode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Enter Reference Code',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Description' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Description',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'CodeId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'CodeId',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'SelectReasonCode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Select Reason Code',
               B.BUNDLE_NAME);

--activity related lables

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'AcvitityId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Acvitity Id',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'SelectNon-StandardActivity' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Select Non-Standard Activity',
               B.BUNDLE_NAME);
			   
COMMIT;

-- DBTicket DB-3661
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Area' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Area',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Zone' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Zone',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Aisle' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Aisle',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Bay' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Bay',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Level' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Level',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Position' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Position',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'CheckDigit' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Check Digit',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'PickSequence' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Pick Sequence',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'PutawaySequence' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Putaway Sequence',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'ItemDedication' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Item Dedication',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'DedicatedItem' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Dedicated Item',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'WorkGroupWorkArea' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'WorkGroup/WorkArea',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LocationSizeType' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Location Size Type',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LastDateCounted' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Last Date Counted',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'QuantityUOM' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Quantity UOM',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'CurrentWeight' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Current Weight',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'CurrentVolume' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Current Volume',
               B.BUNDLE_NAME);
			   
COMMIT;

-- DBTicket DB-3621
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'AlternateLocationAction' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AlternateLocationAction',
               'Alternate Locn',
               'dm_mobile_label');
COMMIT;

-- DBTicket DB-3711

ALTER TABLE HIERARCHY_TYPES ADD  HIERARCHY_TYPE VARCHAR2(50) DEFAULT 'TASK' NOT NULL;

ALTER TABLE HIERARCHY_TYPES ADD  NODE_SOURCE VARCHAR2(50) DEFAULT 'SYS_CODE_TYPE' NOT NULL;
ALTER TABLE HIERARCHY_TYPES ADD  NODE_ID_DESCRIPTION VARCHAR2(50) DEFAULT 'CODE_DESC' NOT NULL;
ALTER TABLE HIERARCHY_TYPES ADD  NODE_ID VARCHAR2(50) DEFAULT 'SYS_CODE_TYPE_ID' NOT NULL;

ALTER TABLE HIERARCHY_TYPES ADD  LEAF_NODE_SOURCE VARCHAR2(50) DEFAULT 'SYS_CODE' NOT NULL;
ALTER TABLE HIERARCHY_TYPES ADD  LEAF_NODE_DESCRIPTION VARCHAR2(50) DEFAULT 'CODE_DESC' NOT NULL;
ALTER TABLE HIERARCHY_TYPES ADD  LEAF_NODE_ID VARCHAR2(50) DEFAULT 'SYS_CODE_ID' NOT NULL;

UPDATE HIERARCHY_TYPES SET HIERARCHY_TYPE = 'TASK' WHERE NAME = 'TASK';
UPDATE HIERARCHY_TYPES SET HIERARCHY_TYPE = 'CUSTOMER' WHERE NAME = 'CUSTOMER';
UPDATE HIERARCHY_TYPES SET HIERARCHY_TYPE = 'TASK' WHERE NAME = 'Inventory Need Type Hierarchy';

UPDATE HIERARCHY_TYPES SET LEAF_NODE_SOURCE = 'INT_MASTER',  LEAF_NODE_DESCRIPTION ='INT_DESC', LEAF_NODE_ID = 'INVN_NEED_TYPE' WHERE NAME = 'Inventory Need Type Hierarchy';
UPDATE HIERARCHY_TYPES SET LEAF_NODE_SOURCE = 'CUSTOMER',  LEAF_NODE_DESCRIPTION ='CUSTOMER_NAME', LEAF_NODE_ID = 'CUSTOMER_ID' WHERE NAME = 'CUSTOMER';

commit;

-- DBTicket DB-3717

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'SkipActionAtPutConfirmInAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Skip',
               B.BUNDLE_NAME);

--Short

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'ShortActionAtPutConfirmInAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Short',
               B.BUNDLE_NAME);

--Skip And Replenish

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'SkipReplActionAtPutConfirmInAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Skip And Replenish',
               B.BUNDLE_NAME);

--End LPN

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'EndLpnActionAtPutConfirmInAggrMode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'End LPN',
               B.BUNDLE_NAME);

--PartialPickActioN

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'PartialPickAction' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN MATCHED
THEN
   UPDATE SET L.VALUE = 'Put Partial Quantity'
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Put Partial Quantity',
               B.BUNDLE_NAME);
			   
COMMIT;

-- DBTicket DB-3716

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'RecordLocation' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Record Location',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'IndirectActivity' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Indirect Activity',
               B.BUNDLE_NAME);
               
COMMIT;

-- DBTicket DB-3755
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'LPNType' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPNType',
               'LPN Type',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'FacilityStatus' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'FacilityStatus',
               'Facility Status',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'PO' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PO',
               'PO',
               'lm_mobile_label');

UPDATE LABEL
   SET VALUE = 'Lock Inventory'
 WHERE key = 'LockInventory' AND bundle_name = 'lm_mobile_label';

UPDATE LABEL
   SET VALUE = 'Unlock Inventory'
 WHERE key = 'UnlockInventory' AND bundle_name = 'lm_mobile_label';

UPDATE LABEL
   SET VALUE = 'Business Partner/Vendor'
 WHERE key = 'BusinessPartner' AND bundle_name = 'lm_mobile_label';

COMMIT;

-- DBTicket DB-3756
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Season' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Season',
               B.BUNDLE_NAME);
               
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Seasonyear' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Season year',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Style' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Style',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Stylesuffix' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Style suffix',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Color' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Color',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Colorsuffix' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Color suffix',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Secondarydimension' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Secondary dimension',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Quality' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Quality',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Sizedescription' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Size description',
               B.BUNDLE_NAME);

COMMIT;

-- DBTicket DB-3763
MERGE INTO TRAN_PARM tpTrg
     USING (SELECT 'Mobile_Pack_Pick_Cart' pgm_id, 64 from_posn FROM DUAL) tpKey
        ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
   INSERT     (PGM_ID,
               FROM_POSN,
               TO_POSN,
               PARM_DESC,
               LITRL,
               MANDT_FLAG,
               VALID_CODE,
               VALID_SYS_CODE_REC_TYPE,
               VALID_SYS_CODE_TYPE,
               VALID_FROM,
               VALID_TO,
               VALID_VALUES,
               EDIT_STYLE,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               TRAN_PARM_ID,
               WM_VERSION_ID,
               PARM_ID,
               PARM_KEY)
       VALUES ('Mobile_Pack_Pick_Cart',
               64,
               64,
               'Suppress End of oLPN/Tote Message',
               'SuppressEndofoLPNMessage',
               'N',
               'F',
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               'CKBX',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               tran_parm_id_seq.NEXTVAL,
               1,
               NULL,
               'Suppress End of oLPN/Tote Message');

MERGE INTO TRAN_MASTER_PARM TMP
     USING (SELECT TRAN_ID, 64 FROM_POSN
              FROM TRAN_MASTER
             WHERE PGM_ID = 'Mobile_Pack_Pick_Cart') TM
        ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
   INSERT     (TRAN_ID,
               FROM_POSN,
               MENU_PARM,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               WHSE_MASTER_ID,
               CD_MASTER_ID,
               TRAN_MASTER_PARM_ID)
       VALUES (TM.TRAN_ID,
               64,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               1,
               NULL,
               NULL,
               TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);
			   
COMMIT;


-- DBTicket DB-3771
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Summary' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Summary',
               'Summary',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'CartNumber' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'CartNumber',
               'Cart Number',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'selectaoption' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'selectaoption',
               'Select an option to continue',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'selectatask' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'selectatask',
               'Select a task to view',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'TaskSummary' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'TaskSummary',
               'Task Summary',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'PickSummary' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'PickSummary',
               'Pick Summary',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'LPNSummary' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPNSummary',
               'LPN Summary',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'LPNItemSummary' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPNItemSummary',
               'Item summary for LPN',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Shorts' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Shorts',
               'Shorts',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'NoItemFound' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'NoItemFound',
               'No items found in cart',
               'dm_mobile_label');

commit;

-- DBTicket DB-3831
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ScanMajorSerialnumber' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Scan Major Serial number',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ScanMinorSerialnumber' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Scan Minor Serial number',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'UnitsRemaining' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Unit(s) remaining',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ScanItemBarcode' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Scan Item Barcode',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Minor' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.key,
               'Minor',
               B.BUNDLE_NAME);
COMMIT;

-- DBTicket DB-3847
MERGE INTO TRAN_PARM tpTrg
     USING (SELECT 'Mobile_Pack_Pick_Cart' pgm_id, 80 from_posn FROM DUAL) tpKey
        ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
   INSERT     (PGM_ID,
               FROM_POSN,
               TO_POSN,
               PARM_DESC,
               LITRL,
               MANDT_FLAG,
               VALID_CODE,
               VALID_SYS_CODE_REC_TYPE,
               VALID_SYS_CODE_TYPE,
               VALID_FROM,
               VALID_TO,
               VALID_VALUES,
               EDIT_STYLE,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               TRAN_PARM_ID,
               WM_VERSION_ID,
               PARM_ID,
               PARM_KEY)
       VALUES ('Mobile_Pack_Pick_Cart',
               80,
               80,
               'Display item image and attributes',
               'Displayitemimageandattr',
               'N',
               'F',
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               'CKBX',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               tran_parm_id_seq.NEXTVAL,
               1,
               NULL,
               'Display item image and attributes');

MERGE INTO TRAN_MASTER_PARM TMP
     USING (SELECT TRAN_ID, 80 FROM_POSN
              FROM TRAN_MASTER
             WHERE PGM_ID = 'Mobile_Pack_Pick_Cart') TM
        ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
   INSERT     (TRAN_ID,
               FROM_POSN,
               MENU_PARM,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               WHSE_MASTER_ID,
               CD_MASTER_ID,
               TRAN_MASTER_PARM_ID)
       VALUES (TM.TRAN_ID,
               80,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               1,
               NULL,
               NULL,
               TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);
			   
COMMIT;

-- DBTicket DB-3890
update tran_parm
   set parm_desc = 'Display item image and components', litrl = 'Displayitemimageandcomp', parm_key = 'Display item image and components'
 where pgm_id = 'Mobile_Pack_Pick_Cart' and from_posn = '80';
commit;

-- DBTicket DB-3886

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'EstimatedDuration' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Estimated Duration',
               B.BUNDLE_NAME);
               
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'CurrentDuration' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Current Duration',
               B.BUNDLE_NAME);
                   
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ElapsedDuration' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Elapsed Duration',
               B.BUNDLE_NAME);

commit;

-- DBTicket DB-3862

----TRAN_MASTER

Insert into TRAN_MASTER (TRAN_ID,TRAN_NAME,TRAN_TYPE,ICON_FILE_NAME,PGM_ID,PGM_TYPE,CUST_ID,OPTN_ACCESS,TASK_NAME,TRK_PROD,PROMPT_FOR_PRTR_DEST,ELS_ACTVTY_CODE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,DISABLED_RF_CTRL_KEYS,URL,WORKINFO_INTERFACE,WORKINFO_LOG,TRAN_GRP,FORM_CUSTOMIZE_LEVEL,DISABLE_ROW_COUNT,FACTORY_NAME,WM_VERSION_ID,TRAN_MASTER_ID,GET_EMP_PERF,FO_TYPE,MHE_ENABLED,REASON_CODE_ENABLED,APP_ID,CREATED_SOURCE_TYPE,CREATED_SOURCE,CREATED_DTTM,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_SOURCE,LAST_UPDATED_DTTM) 
values (209,'Mobile Anchor oLPN','M',null,'Mobile_Anchor_oLPN','M','PROD','E',null,'Y','N',null,null,CURRENT_TIMESTAMP,'PRODDB',null,'Mobile_Anchor_oLPN','0','N',null,1,0,null,1,TRAN_MASTER_ID_SEQ.NEXTVAL,null,null,'N','0',null,1,null,CURRENT_TIMESTAMP,1,'SEED',CURRENT_TIMESTAMP);

--TRAN_PARM && TRAN_MASTER_PARM
---1-1
MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
1 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(
    PGM_ID,
    FROM_POSN,
    TO_POSN,
    PARM_DESC,
    LITRL,
    MANDT_FLAG,
    VALID_CODE,
    VALID_SYS_CODE_REC_TYPE,
    VALID_SYS_CODE_TYPE,
    VALID_FROM,
    VALID_TO,
    VALID_VALUES,
    EDIT_STYLE,
    CREATE_DATE_TIME,
    MOD_DATE_TIME,
    USER_ID,
    TRAN_PARM_ID,
    WM_VERSION_ID,
    PARM_ID,
    PARM_KEY,
    CREATED_DTTM,
    LAST_UPDATED_DTTM
  )
  VALUES
  (
    'Mobile_Anchor_oLPN',
    1,1,
    'Location Verification Mode',
    'Location Verfication',
    'Y',
    'S',
    'S',
    '780',
    NULL,
    NULL,
    NULL,
    'DDDW',
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP,
    'SEED',
    TRAN_PARM_ID_SEQ.NEXTVAL,
    1,
    NULL,
    'Location_Verification_Mode',
    CURRENT_TIMESTAMP,
    NULL
  );
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 1 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
1,
'3',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);



--------------------2-2------------------------------------

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
2 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
 (
    PGM_ID,
    FROM_POSN,
    TO_POSN,
    PARM_DESC,
    LITRL,
    MANDT_FLAG,
    VALID_CODE,
    VALID_SYS_CODE_REC_TYPE,
    VALID_SYS_CODE_TYPE,
    VALID_FROM,
    VALID_TO,
    VALID_VALUES,
    EDIT_STYLE,
    CREATE_DATE_TIME,
    MOD_DATE_TIME,
    USER_ID,
    TRAN_PARM_ID,
    WM_VERSION_ID,
    PARM_ID,
    PARM_KEY,
    CREATED_DTTM,
    LAST_UPDATED_DTTM
  )
  VALUES
  (
    'Mobile_Anchor_oLPN',
    2,2,
    'Anchor oLPN - Consolidation Level Completion',
    'Consolidation Lvl Complet',
    'Y',
    'S',
    'S',
    '781',
    NULL,
    NULL,
    NULL,
    'DDDW',
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP,
    'SEED',
    TRAN_PARM_ID_SEQ.NEXTVAL,
    1,
    NULL,
    'Anchor_Carton_Consolidation_Level_Completion',
    CURRENT_TIMESTAMP,
    NULL
  );
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 2 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
2,
'1',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

-----------------------3-3----------------------

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
3 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
 (PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',3,3,'Anchor oLPN - Consolidation Level','Consolidation Level','Y','S','S','782',null,null,null,'DDDW',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Anchor_Carton_Consolidation_Level',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 3 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
3,
'1',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

-----4-4-------0
MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
4 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',4,4,'oLPN Label Print Type','Crtn Lbl Print','Y','S','S','235',null,null,null,'DDDW',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Carton_Label_Print_Type',CURRENT_TIMESTAMP,null);

MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 4 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
4,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

---------------5-5-----N
MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
5 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',5,5,'Prompt For Printer Destination','Printer Prompt','Y','F',null,null,null,null,null,'CKBX',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Prompt_For_Printer_Destination',CURRENT_TIMESTAMP,null);

MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 5 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
5,
'N',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);
----6-6  0

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
6 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
 (PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',6,6,'Pallet Label Print Type','Plt Lbl Print','Y','S','S','390',null,null,null,'DDDW',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Pallet_Label_Print_Type',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 6 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
6,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

-----7-7  0

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
7 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',7,7,'Loading Tasks Level','LoadTaskLevel','N','S','S','676',null,null,null,'DDDW',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Loading_Tasks_Level',CURRENT_TIMESTAMP,null);

MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 7 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
7,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

---8-8   0
MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
8 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',8,8,'Bypass Anchor and Load Container','BypassAnchAndLoadCtn','N','N',null,null,'1','0',null,'CKBX',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Bypass_Anchor_and_Load_OBContainer',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 8 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
8,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);


----209	     9			-
MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
9 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
 (PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',9,18,'Default Loading Task Creation Criteria','TaskParm','N','A',null,null,null,null,null,'EDIT',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Default_Loading_Task_Creation_Criteria',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 9 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
9,
NULL,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);


-----209	19			0

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
19 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',19,19,'Assign Loading Task to current user?','AssignLoadingTask','N','N',null,null,'1','0',null,'CKBX',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Assign_Loading_Task_to_current_user?',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 19 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
19,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);


-----209	20			N

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
20 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
 (PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',20,20,'Write LM message for printer visit',null,'N','F',null,null,null,null,null,'CKBX',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,null,CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 20 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
20,
'N',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);


-----209	21			0

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
21 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
 (PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',21,21,'Allow Multiple [oLPN]s','AllowMultipleOLPNs','N','N',null,null,'1','0',null,'CKBX',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Allow_Multiple_OLPNs',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 21 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
21,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

-----209	22	

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
22 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',22,23,'[oLPN] scan limit','OLPNScanLimit','N','N',null,null,null,null,null,'EDIT',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'OLPN_Scan_Limit',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 22 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
22,
NULL,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);
		
-----209	24			0

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
24 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
 (PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',24,24,'Consolidation Mode','ConsolidationMode','N','S','S','586',null,null,null,'DDDW',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Consolidation_Mode',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 24 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
24,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);


-----209	25			0

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
25 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',25,25,'AssignmentMode','AssignmentMode','N','S','S','587',null,null,null,'DDDW',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Assignment_Mode',CURRENT_TIMESTAMP,null);

MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 25 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
25,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
null,
null,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

-----209	26			0

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
26 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',26,26,'Load/Anchor all [oLPN]s on single scan','LoadAllOLPNOnSnglScan','N','N',null,null,'1','0',null,'CKBX',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Load_All_OLPNS_On_Single_Scan',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 26 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
26,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);


-----209	27	

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
27 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
(PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',27,27,'Processing order for multiple LPN scans','ProcessingOrder','N','S','S','588',null,null,null,'DDDW',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'Processing_Order',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 27 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
27,
NULL,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);
		
-----209	28	

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
28 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
 (PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',28,42,'Load container Transaction link name','RFLoadTranTaskName','N','A',null,null,null,null,null,'EDIT',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'RFLoad_Tran_Task_Name',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 28 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
28,
NULL,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

		
-----209	44			0
MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Anchor_oLPN' pgm_id,
44 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT 
 (PGM_ID,FROM_POSN,TO_POSN,PARM_DESC,LITRL,MANDT_FLAG,VALID_CODE,VALID_SYS_CODE_REC_TYPE,VALID_SYS_CODE_TYPE,VALID_FROM,VALID_TO,VALID_VALUES,EDIT_STYLE,CREATE_DATE_TIME,MOD_DATE_TIME,USER_ID,TRAN_PARM_ID,WM_VERSION_ID,PARM_ID,PARM_KEY,CREATED_DTTM,LAST_UPDATED_DTTM) values ('Mobile_Anchor_oLPN',44,44,'Use dock door anchor location for Staging','DockDoorLocnAsStaging','N','N',null,null,'1','0',null,'CKBX',CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'SEED',TRAN_PARM_ID_SEQ.NEXTVAL,1,null,'DockDoor_Locn_As_Staging',CURRENT_TIMESTAMP,null);
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 44 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Anchor_oLPN') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
44,
'0',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

--WORKFLOW_SCRIPT 
-- insert into workflow_script (workflow_script_id, script_location, script_name, script_description, is_base_script) values (4, 'outbound/AnchorOBLpn.dsl', 'Anchor OB Lpn', 'Base Anchor OB Lpn Workflow', 1);

update tran_master set workflow_script_name='Anchor OB Lpn'  where tran_id = 209;

Insert into RESOURCES (RESOURCE_ID,URI,MODULE,URI_TYPE_ID) values (SEQ_RESOURCE_ID.NEXTVAL,'Mobilepicking.screen.anchorolpn.AnchorOlpnScreen','WMMOBILE',2);
Insert into RESOURCE_PERMISSION(PERMISSION_CODE, RESOURCE_ID) values('WMAMOBANCOLPN', (select RESOURCE_ID from RESOURCES where URI='Mobilepicking.screen.anchorolpn.AnchorOlpnScreen' and MODULE='WMMOBILE' and URI_TYPE_ID=2));

commit;

-- DBTicket DB-3856

MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Pack_Pick_Cart' pgm_id,
65 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT (PGM_ID,
FROM_POSN,
TO_POSN,
PARM_DESC,
LITRL,
MANDT_FLAG,
VALID_CODE,
VALID_SYS_CODE_REC_TYPE,
VALID_SYS_CODE_TYPE,
VALID_FROM,
VALID_TO,
VALID_VALUES,
EDIT_STYLE,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
TRAN_PARM_ID,
WM_VERSION_ID,
PARM_ID,
PARM_KEY)
VALUES ('Mobile_Pack_Pick_Cart',
65,
79,
'RF Anchor Outbound Lpn transaction link name',
'RF Anchor trn lnk name',
'N',
NULL,
NULL,
NULL,
NULL,
NULL,
NULL,
'EDIT',
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
tran_parm_id_seq.NEXTVAL,
1,
NULL,
'RF Anchor Outbound Lpn transaction link name');

MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 65 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Pack_Pick_Cart') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
65,
NULL,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

commit;

-- DBTicket DB-3830

--CC related inputs
--ccLocnEmpty -- 'Location empty (Y/N)'
MERGE INTO LABEL L
USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ccLocnEmpty' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN MATCHED 
THEN
UPDATE SET L.VALUE = 'Location empty (Y/N)'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'ccLocnEmpty',
'Location empty (Y/N)?',
'dm_mobile_label');


--ccCreateTask  -- 'Create CC task?'
MERGE INTO LABEL L
USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ccCreateTask' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN MATCHED 
THEN
UPDATE SET L.VALUE = 'Create CC task(Y/N)?'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'ccCreateTask',
'Create CC task(Y/N)?',
'dm_mobile_label');


--ccRemainingQty -- 'Remaining Qty'

MERGE INTO LABEL L
USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ccRemainingQty' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN MATCHED 
THEN
UPDATE SET L.VALUE = 'Enter Remaining Qty'
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'ccRemainingQty',
'Enter Remaining Qty',
'dm_mobile_label');

commit;

-- DBTicket DB-3902

--Label Entry for Locate Tote Cart
MERGE INTO LABEL L
USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'scannedResvLocnToLocateCart' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'scannedResvLocnToLocateCart',
'Scan Resv Location',
'dm_mobile_label');


MERGE INTO LABEL L
USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'locatetotecart' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'locatetotecart',
'Locate LPN',
'dm_mobile_label');



MERGE INTO LABEL L
USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'lpn' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'lpn',
'LPN',
'dm_mobile_label');

commit;

-- DBTicket DB-3928

MERGE INTO TRAN_PARM tpTrg
     USING (SELECT 'Mobile_Pack_Pick_Cart' pgm_id,
                   81 from_posn
              FROM DUAL) tpKey
        ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
   INSERT     (PGM_ID,
               FROM_POSN,
               TO_POSN,
               PARM_DESC,
               LITRL,
               MANDT_FLAG,
               VALID_CODE,
               VALID_SYS_CODE_REC_TYPE,
               VALID_SYS_CODE_TYPE,
               VALID_FROM,
               VALID_TO,
               VALID_VALUES,
               EDIT_STYLE,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               TRAN_PARM_ID,
               WM_VERSION_ID,
               PARM_ID,
               PARM_KEY)
       VALUES ('Mobile_Pack_Pick_Cart',
               81,
               81,
               'Enable additional task type support',
               'EnableTaskTypeSupport',
               'N',
               'F',
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               'CKBX',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               tran_parm_id_seq.NEXTVAL,
               1,
               NULL,
               'Enable_Additional_Task_Type_Support');

MERGE INTO TRAN_MASTER_PARM TMP
     USING (SELECT TRAN_ID, 81 FROM_POSN
              FROM TRAN_MASTER
             WHERE PGM_ID = 'Mobile_Pack_Pick_Cart') TM
        ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
   INSERT     (TRAN_ID,
               FROM_POSN,
               MENU_PARM,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               WM_VERSION_ID,
               WHSE_MASTER_ID,
               CD_MASTER_ID,
               TRAN_MASTER_PARM_ID)
       VALUES (TM.TRAN_ID,
               81,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               1,
               NULL,
               NULL,
               TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

---82-96
MERGE INTO TRAN_PARM tpTrg
USING (SELECT 'Mobile_Pack_Pick_Cart' pgm_id,
82 from_posn
FROM DUAL) tpKey
ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
INSERT
  (
    PGM_ID,
    FROM_POSN,
    TO_POSN,
    PARM_DESC,
    LITRL,
    MANDT_FLAG,
    VALID_CODE,
    VALID_SYS_CODE_REC_TYPE,
    VALID_SYS_CODE_TYPE,
    VALID_FROM,
    VALID_TO,
    VALID_VALUES,
    EDIT_STYLE,
    CREATE_DATE_TIME,
    MOD_DATE_TIME,
    USER_ID,
    TRAN_PARM_ID,
    WM_VERSION_ID,
    PARM_ID,
    PARM_KEY,
    CREATED_DTTM,
    LAST_UPDATED_DTTM
  )
  VALUES
  (
    'Mobile_Pack_Pick_Cart',
    82,96,
    'Additional task types for Task List entry (Cubed)',
    'AdditionalTaskTypesCubed',
    'N',
    'A',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    'EDIT',
    CURRENT_TIMESTAMP,
    CURRENT_TIMESTAMP,
    'SEED',
    TRAN_PARM_ID_SEQ.NEXTVAL,
    1,
    NULL,
    'Additional_Task_Types_Cubed',
    CURRENT_TIMESTAMP,
    NULL
  );
  
MERGE INTO TRAN_MASTER_PARM TMP
USING (SELECT TRAN_ID, 82 FROM_POSN
FROM TRAN_MASTER
WHERE PGM_ID = 'Mobile_Pack_Pick_Cart') TM
ON (TMP.TRAN_ID = TM.TRAN_ID AND TMP.FROM_POSN = TM.FROM_POSN)
WHEN NOT MATCHED
THEN
INSERT (TRAN_ID,
FROM_POSN,
MENU_PARM,
CREATE_DATE_TIME,
MOD_DATE_TIME,
USER_ID,
WM_VERSION_ID,
WHSE_MASTER_ID,
CD_MASTER_ID,
TRAN_MASTER_PARM_ID)
VALUES (TM.TRAN_ID,
82,
NULL,
CURRENT_TIMESTAMP,
CURRENT_TIMESTAMP,
'SEED',
1,
NULL,
NULL,
TRAN_MASTER_PARM_ID_SEQ.NEXTVAL);

commit;

-- DBTicket DB-3885
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'PickedOrRequired' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'PickedOrRequired','Picked/Required','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'WorkGroupOrArea' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'WorkGroupOrArea','Work Group/Area','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'PickingSummary' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'PickingSummary','Picking Summary','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'PickQuantities' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'PickQuantities','Pick Quantities','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'LPNOrToteSummary' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'LPNOrToteSummary','oLPN or Tote Summary','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'LPNSizeTypeSummary' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'LPNSizeTypeSummary','LPN Size/Type Summary','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'LPNStatusSummary' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'LPNStatusSummary','LPN Status Summary','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'LPNSummary' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'LPNSummary','LPN Summary','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'oLPNSummary' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'oLPNSummary','oLPN Summary','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ToteSummary' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'ToteSummary','Tote Summary','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'PickedQuantity' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'PickedQuantity','Picked Quantity','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'LPNdetails' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'LPNdetails','LPN details','dm_mobile_label');

MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'for' KEY FROM DUAL) B 
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID,L.KEY,L.VALUE,L.BUNDLE_NAME) 
VALUES (SEQ_LABEL_ID.NEXTVAL, 'for','for','dm_mobile_label');

commit;

-- DBTicket DB-3942
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'EstimatedTimeToCompleteTask' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Estimated time to complete task',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'TimeElapsedSinceEndOfPreviousTask' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Time elapsed since end of previous task',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'TimeElapsedSinceStartOfCurrentTask' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Time elapsed since start of current task',
               B.BUNDLE_NAME);
commit;			   

-- DBTicket DB-3938
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'anchorolpn' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'anchorolpn',
               'Anchor OB LPN',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'newScannedLocn' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'newScannedLocn',
               'Scan Location',
               'dm_mobile_label');
commit;			   

-- DBTicket DB-3946

--Label Entries for MultiLPn and MultiTote in Aggregate Mode
MERGE INTO LABEL L
USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'MultiLpn' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'MultiLpn',
'*Multi LPN*',
'dm_mobile_label');


MERGE INTO LABEL L
USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'MultiTote' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
INSERT (L.LABEL_ID,
L.KEY,
L.VALUE,
L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL,
'MultiTote',
'*Multi Tote*',
'dm_mobile_label');

COMMIT;

-- DBTicket DB-3954
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'UserPerformance' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UserPerformance',
               'User Performance',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'utilization' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'utilization',
               'Utilization',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'performanceGoal' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'performanceGoal',
               'Performance Goal',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'department' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'department',
               'Department',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'shift' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'shift',
               'Shift',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'uph' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'uph',
               'UPH',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ep' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ep',
               'EP',
               'dm_mobile_label');
COMMIT;

-- DBTicket DB-3965
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Warning' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Warning',
               'Warning',
               'lm_mobile_label');
COMMIT;			   
			   
-- DBTicket DB-4019

UPDATE MESSAGE_MASTER SET MSG_TYPE='INFORM', MSG_CLASS='SYSTEM' WHERE MSG_ID='8112' AND MSG_MODULE='OUTBGEN';

commit;

-- DBTicket DB-3970

MERGE INTO TRAN_PARM tpTrg
     USING (SELECT 'Mobile_Make_oLPN_Cart' pgm_id,
                   37 from_posn
              FROM DUAL) tpKey
        ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN MATCHED 
THEN
UPDATE SET tpTrg.parm_desc = 'Suppress Cart Prompt',
tpTrg.LITRL='SuppressCartPrompt', PARM_KEY='Suppress_Cart_Prompt'
WHEN NOT MATCHED
THEN
   INSERT     (PGM_ID,
               FROM_POSN,
               TO_POSN,
               PARM_DESC,
               LITRL,
               MANDT_FLAG,
               VALID_CODE,
               VALID_SYS_CODE_REC_TYPE,
               VALID_SYS_CODE_TYPE,
               VALID_FROM,
               VALID_TO,
               VALID_VALUES,
               EDIT_STYLE,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               TRAN_PARM_ID,
               WM_VERSION_ID,
               PARM_ID,
               PARM_KEY)
       VALUES ('Mobile_Make_oLPN_Cart',
               37,
               37,
               'Suppress Cart Prompt',
               'SuppressCartPrompt',
               'N',
               'F',
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               'CKBX',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               tran_parm_id_seq.NEXTVAL,
               1,
               NULL,
               'Suppress_Cart_Prompt');
               
MERGE INTO TRAN_PARM tpTrg
     USING (SELECT 'Mobile_Make_Tote_Cart' pgm_id,
                   37 from_posn
              FROM DUAL) tpKey
        ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN MATCHED 
THEN
UPDATE SET tpTrg.parm_desc = 'Suppress Cart Prompt',
tpTrg.LITRL='SuppressCartPrompt', PARM_KEY='Suppress_Cart_Prompt'
WHEN NOT MATCHED
THEN
   INSERT     (PGM_ID,
               FROM_POSN,
               TO_POSN,
               PARM_DESC,
               LITRL,
               MANDT_FLAG,
               VALID_CODE,
               VALID_SYS_CODE_REC_TYPE,
               VALID_SYS_CODE_TYPE,
               VALID_FROM,
               VALID_TO,
               VALID_VALUES,
               EDIT_STYLE,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               TRAN_PARM_ID,
               WM_VERSION_ID,
               PARM_ID,
               PARM_KEY)
       VALUES ('Mobile_Make_Tote_Cart',
               37,
               37,
               'Suppress Cart Prompt',
               'SuppressCartPrompt',
               'N',
               'F',
               NULL,
               NULL,
               NULL,
               NULL,
               NULL,
               'CKBX',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'SEED',
               tran_parm_id_seq.NEXTVAL,
               1,
               NULL,
               'Suppress_Cart_Prompt');
			   
commit;

-- DBTicket DB-4025
MERGE INTO LABEL L
     USING (SELECT 'wm_label' BUNDLE_NAME, 'FindParentLpn' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'FindParentLpn',
               'Find Parent LPN',
               'wm_label');

COMMIT;

-- DBTicket DB-4010
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'TimeForCurrentTask' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Time for Current Task',
               B.BUNDLE_NAME);
COMMIT;

-- DBTicket DB-4082
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'nodatatodisplay' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'No data to display',
               B.BUNDLE_NAME);

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'taskId' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Scan [Task]',
               B.BUNDLE_NAME);
COMMIT;			   
			   
			   
-- DBTicket DB-4153

exec sequpdt(t_name => 'LABEL',c_name => 'LABEL_ID', s_name => 'SEQ_LABEL_ID');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Area' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Area',
               'Area',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Zone' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Zone',
               'Zone',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Aisle' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Aisle',
               'Aisle',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Bay' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Bay',
               'Bay',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Level' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Level',
               'Level',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Position' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Position',
               'Position',
               'dm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Disabled Reserve' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Disabled Reserve',
               'Dd Reserve',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Disabled Active' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Disabled Active',
               'Dd Active',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Disabled Case Pick' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Disabled Case Pick',
               'Dd Case Pk',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Active' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Active',
               'Active',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Case Pick' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Case Pick',
               'Case Pick',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Store Pack' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Store Pack',
               'Store Pack',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Miscellaneous' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Miscellaneous',
               'Misc',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Order Consolidation' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Order Consolidation',
               'Order Cons',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Pack and Hold' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Pack and Hold',
               'Pack Hold',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Dock Door' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Dock Door',
               'Dock Door',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Reserve' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Reserve',
               'Reserve',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Transitional Invn' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Transitional Invn',
               'Tx Invn',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Yard Slot' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Yard Slot',
               'Yard Slot',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Outbound Staging' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Outbound Staging',
               'Ob Staging',
               'dm_mobile_label');


MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'SupervisorOverride' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SupervisorOverride',
               'Supervisor Override',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'UserName' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'UserName',
               'User Name',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Password' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Password',
               'Password Override',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'SignIn' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SignIn',
               'Sign In',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'NoOverridePermission' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'NoOverridePermission',
               'User do not have permission to override the error.',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'InvalidUsernameOrPassword' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InvalidUsernameOrPassword',
               'Invalid User Name or Password.',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'wm_label' BUNDLE_NAME,
                   'DisplayWarehouseBUselectorduringRFOrMobileLogin' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'DisplayWarehouseBUselectorduringRFOrMobileLogin',
               'Display Warehouse/BU selector during RF/Mobile Login',
               'wm_label');

commit;

-- DBTicket DB-4040
MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'edit' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'edit',
               'Edit',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'add' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'add',
               'Add',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'delete' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'delete',
               'Delete',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Hierarchy Configuration' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Hierarchy Configuration',
               'Hierarchy Configuration',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'PageTitle' BUNDLE_NAME, 'Hierarchy Configuration' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (LABEL_ID,
               KEY,
               VALUE,
               BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Hierarchy Configuration',
               'Hierarchy Configuration',
               'PageTitle');
commit;

-- DBTicket DB-4214
UPDATE xmenu_item
   SET navigation_key = 'lm.screen.ObservationManagement_16'
 WHERE name = 'Employee Interactions';
 
COMMIT;

-- DBTicket DB-4209
MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Employee Interactions' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Employee Interactions',
               'Employee Interactions',
               'Navigation');

MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Employee Management' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Employee Management',
               'Employee Management',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Event Authorization' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Event Authorization',
               'Event Authorization',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Performance Management' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Performance Management',
               'Performance Management',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Reflective Assignments' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Reflective Assignments',
               'Reflective Assignments',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Team Assignments' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Team Assignments',
               'Team Assignments',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Work Standards' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Work Standards',
               'Work Standards',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Departments' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Departments',
               'Departments',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Location Search' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Location Search',
               'Location Search',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'LPN Search' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPN Search',
               'LPN Search',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Item Search' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Item Search',
               'Item Search',
               'Navigation');


MERGE INTO LABEL L
     USING (SELECT 'Navigation' BUNDLE_NAME, 'Work Monitoring' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Work Monitoring',
               'Work Monitoring',
               'Navigation');
			   
COMMIT;

-- DBTicket DB-4261
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Displaying' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Displaying',
               'Displaying',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'out of' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'out of',
               'out of',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'records' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'records',
               'records',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Updated at' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Updated at',
               'Updated at',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Results' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Results',
               'Results',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'exceeded' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'exceeded',
               'exceeded',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'limit' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'limit',
               'limit',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'Modify filter to reduce results' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Modify filter to reduce results',
               'Modify filter to reduce results',
               'lm_mobile_label');
COMMIT;			   

-- DBTicket DB-4266
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'cyclecount' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'cyclecount',
               'Cycle Count',
               'dm_mobile_label');
COMMIT;

-- DBTicket DB-4259
DELETE FROM LABEL
      WHERE KEY = 'iLPN' AND BUNDLE_NAME = 'dm_mobile_label';

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'iLPN' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'iLPN',
               'iLPN',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'area' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'area',
               'Area',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'zone' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'zone',
               'Zone',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'aisle' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'aisle',
               'Aisle',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'bay' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'bay',
               'Bay',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'level' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'level',
               'Level',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'position' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'position',
               'Position',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'AddTasksToTote' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Add Task(s) To Tote',
               B.BUNDLE_NAME);	
COMMIT;			   


-- DBTicket DB-4263

UPDATE tran_parm
   SET parm_desc = 'Display oLPN Size/Type and summary'
 WHERE pgm_id = 'Mobile_Pack_Pick_Cart' AND from_posn = 17 AND to_posn = 17;

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ScanLPN' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ScanLPN',
               'Scan oLPN/Tote',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'LPNNumber' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'LPNNumber',
               'LPN Number',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'InvalidCartNumber' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InvalidCartNumber',
               'Invalid Cart Number',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'InvalidTaskId' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InvalidTaskId',
               'Invalid Task Id',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'InvalidLPN' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'InvalidLPN',
               'Invalid oLPN Number/Tote Id',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Status' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Status',
               'Status',
               'dm_mobile_label');
			   
COMMIT;

-- DBTicket DB-3767

--Toast message after location is recorded
MERGE INTO MESSAGE_MASTER M USING
(SELECT '110904000' KEY, 'wm' ILS_MODULE, 'OUTBGEN' MSG_MODULE, 'ErrorMessage' BUNDLE_NAME FROM DUAL) D ON (M.KEY = D.KEY AND M.ILS_MODULE = D.ILS_MODULE AND M.MSG_MODULE=D.MSG_MODULE AND M.BUNDLE_NAME = D.BUNDLE_NAME)
WHEN NOT MATCHED THEN INSERT (MESSAGE_MASTER_ID, MSG_ID, KEY, ILS_MODULE, MSG_MODULE, MSG, BUNDLE_NAME, MSG_CLASS, MSG_TYPE, CREATED_DTTM,LAST_UPDATED_DTTM )
VALUES ( SEQ_MESSAGE_MASTER_ID.nextval,  '110904000', '110904000', 'wm', 'OUTBGEN', 'Location Recorded', 'ErrorMessage', 'USER', 'INFORM', sysdate, sysdate);

--Information popup text informing user that activity is in-progress
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'IndirectLaborInProgress' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME, CREATED_DTTM,LAST_UPDATED_DTTM)
VALUES (SEQ_LABEL_ID.NEXTVAL, 'IndirectLaborInProgress','Approved Indirect Labor in progress','dm_mobile_label', sysdate, sysdate);

--Button text for IndirectActivity popup
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'tapWhenReadyToContinue' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME, CREATED_DTTM,LAST_UPDATED_DTTM)
VALUES (SEQ_LABEL_ID.NEXTVAL, 'tapWhenReadyToContinue','Tap when you are ready to continue','dm_mobile_label', sysdate, sysdate);

--Barcode textfield label for RecordLocation
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'recordLocn' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME, CREATED_DTTM,LAST_UPDATED_DTTM)
VALUES (SEQ_LABEL_ID.NEXTVAL, 'recordLocn','Scan [Location]','dm_mobile_label', sysdate, sysdate);

--Barcode textfield label for non-standard activity
MERGE INTO LABEL L USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'nonStdActivity' KEY FROM DUAL) B
ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME) WHEN NOT MATCHED THEN INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME, CREATED_DTTM,LAST_UPDATED_DTTM)
VALUES (SEQ_LABEL_ID.NEXTVAL, 'nonStdActivity','Scan [Activity]','dm_mobile_label', sysdate, sysdate);

commit;

-- DBTicket DB-4356
MERGE INTO LABEL L
     USING (SELECT 'SysCode' BUNDLE_NAME, 'oLPN/Tote' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'oLPN/Tote',
               'oLPN/Tote',
               'SysCode');

COMMIT;
---Label Entry--

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'olpnOrToteNbrForEntry' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'olpnOrToteNbrForEntry',
               'Scan oLPN/Tote',
               'dm_mobile_label');

COMMIT;


-- DBTicket DB-2566
INSERT INTO XBASE_MENU_SECTION(XBASE_MENU_SECTION_ID,NAME,SEQUENCE,APP_ID)
values(SEQ_XBASE_MENU_SECT_ID.NEXTVAL, 'MobilePicking',SEQ_XBASE_MENU_SECT_ID.NEXTVAL,(SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'));

INSERT INTO XBASE_MENU_PART(XBASE_MENU_PART_ID,XBASE_MENU_SECTION_ID,NAME,SEQUENCE)
values(SEQ_XBASE_MENU_PART_ID.NEXTVAL, (select XBASE_MENU_SECTION_ID from  XBASE_MENU_SECTION where name='MobilePicking')  , 'MobilePicking' ,SEQ_XBASE_MENU_PART_ID.NEXTVAL );

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) 
VALUES(SEQ_XMENU_ITEM_ID.NEXTVAL,'Make Pick Cart - oLPN', 'Make Pick Cart - oLPN','Mobilepicking.screen.makepickcart.MakePickCartScreen','','default-icon','#002E5F',0,4,'1',0);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID, XMENU_ITEM_ID, XBASE_MENU_PART_ID, SEQUENCE) 
VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL, SEQ_XMENU_ITEM_ID.CURRVAL, (select XBASE_MENU_PART_ID from XBASE_MENU_PART where name='MobilePicking'), 1);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES (SEQ_XMENU_ITEM_ID.CURRVAL, (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'), NULL, 1, SYSDATE, NULL, 1, SYSDATE); 

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES (SEQ_XMENU_ITEM_ID.CURRVAL, 'WMARFMAKPICRT', NULL, 1, SYSDATE, NULL, 1, SYSDATE);

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) 
VALUES(SEQ_XMENU_ITEM_ID.NEXTVAL,'Make Pick Cart - Tote', 'Make Pick Cart - Tote','Mobilepicking.screen.makepickcart.MakePickCartScreen','','default-icon','#002E5F',0,4,'1',0);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID, XMENU_ITEM_ID, XBASE_MENU_PART_ID, SEQUENCE) 
VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL, SEQ_XMENU_ITEM_ID.CURRVAL, (select XBASE_MENU_PART_ID from XBASE_MENU_PART where name='MobilePicking'), 1);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES (SEQ_XMENU_ITEM_ID.CURRVAL, (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'), NULL, 1, SYSDATE, NULL, 1, SYSDATE); 

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES (SEQ_XMENU_ITEM_ID.CURRVAL, 'WMARFMAKPICRT', NULL, 1, SYSDATE, NULL, 1, SYSDATE);


MERGE INTO LABEL L USING
(SELECT 'TOUCHTOP' BUNDLE_NAME, 'makepickcart' KEY FROM DUAL) 
 B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL, 'makepickcart', 'Make Pick Cart', 'TOUCHTOP'
);

MERGE INTO LABEL L USING
(SELECT 'wm_label' BUNDLE_NAME, 'makepickcart' KEY FROM DUAL) 
 B ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED THEN
INSERT (L.LABEL_ID, L.KEY, L.VALUE, L.BUNDLE_NAME)
VALUES (SEQ_LABEL_ID.NEXTVAL, 'makepickcart', 'Make Pick Cart', 'wm_label'
);

commit;


-- DBTicket DB-4340
-- Add the Permission 'WMAMOBTASKSUM' to WMS

merge into app_mod_perm amp
     using (select app.app_id,
                   app.app_name,
                   module.module_id,
                   module.module_code,
                   permission.permission_id,
                   permission.permission_code
              from app, module, permission
             where     app.app_name = 'Warehouse Management'
                   and module.module_code = 'WMMOBILE'
                   and permission.permission_code = 'WMAMOBTASKSUM') app1
        on (    amp.app_id = app1.app_id
            and amp.module_id = app1.module_id
            and amp.permission_id = app1.permission_id)
when not matched
then
   insert     (app_id,
               module_id,
               permission_id,
               row_uid,
               created_dttm,
               last_updated_dttm)
       values ( (select app_id
                   from app
                  where app_name = 'Warehouse Management'),
               (select module_id
                  from module
                 where module_code = 'WMMOBILE'),
               (select permission_id
                  from permission
                 where permission_code = 'WMAMOBTASKSUM'),
               (select max (row_uid) + 4
                  from app_mod_perm),
               current_timestamp,
               current_timestamp);

-- Delete Existing 'Task Summary' MENU (if exist).

DELETE FROM XMENU_ITEM_PERMISSION
      WHERE XMENU_ITEM_ID = (SELECT XMENU_ITEM_ID
                               FROM XMENU_ITEM
                              WHERE NAME = 'Task Summary');

DELETE FROM XMENU_ITEM_APP
      WHERE XMENU_ITEM_ID = (SELECT XMENU_ITEM_ID
                               FROM XMENU_ITEM
                              WHERE NAME = 'Task Summary');

DELETE FROM XBASE_MENU_ITEM
      WHERE XMENU_ITEM_ID = (SELECT XMENU_ITEM_ID
                               FROM XMENU_ITEM
                              WHERE NAME = 'Task Summary');

DELETE FROM XMENU_ITEM
      WHERE NAME = 'Task Summary';

-- Add New Menu for 'Task Summary'...

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,
                        NAME,
                        SHORT_NAME,
                        NAVIGATION_KEY,
                        TILE_KEY,
                        ICON,
                        BGCOLOR,
                        SCREEN_MODE,
                        SCREEN_VERSION,
                        BASE_SECTION_NAME,
                        BASE_PART_NAME,
                        CREATED_SOURCE,
                        CREATED_SOURCE_TYPE,
                        CREATED_DTTM,
                        LAST_UPDATED_SOURCE,
                        LAST_UPDATED_SOURCE_TYPE,
                        LAST_UPDATED_DTTM,
                        IS_DEFAULT)
     VALUES (SEQ_XMENU_ITEM_ID.NEXTVAL,
             'Task Summary',
             'Task Summary',
             'Mobilepicking.screen.summary.TaskSummary',
             NULL,
             'task',
             '#002E5F',
             0,
             4,
             '1',
             'GENERAL',
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             0);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID,
                             XBASE_MENU_PART_ID,
                             XMENU_ITEM_ID,
                             SEQUENCE,
                             CREATED_SOURCE,
                             CREATED_SOURCE_TYPE,
                             CREATED_DTTM,
                             LAST_UPDATED_SOURCE,
                             LAST_UPDATED_SOURCE_TYPE,
                             LAST_UPDATED_DTTM)
     VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL,
             (SELECT XBASE_MENU_PART_ID
                FROM XBASE_MENU_PART
               WHERE NAME = 'MobilePicking'),
             SEQ_XMENU_ITEM_ID.CURRVAL,
             8,
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID,
                            APP_ID,
                            CREATED_SOURCE,
                            CREATED_SOURCE_TYPE,
                            CREATED_DTTM,
                            LAST_UPDATED_SOURCE,
                            LAST_UPDATED_SOURCE_TYPE,
                            LAST_UPDATED_DTTM)
     VALUES (SEQ_XMENU_ITEM_ID.CURRVAL,
             (SELECT APP_ID
                FROM APP
               WHERE APP_SHORT_NAME = 'WMS'),
             NULL,
             1,
             CURRENT_TIMESTAMP,
             NULL,
             1,
             CURRENT_TIMESTAMP);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID,
                                   PERMISSION_CODE,
                                   CREATED_SOURCE,
                                   CREATED_SOURCE_TYPE,
                                   CREATED_DTTM,
                                   LAST_UPDATED_SOURCE,
                                   LAST_UPDATED_SOURCE_TYPE,
                                   LAST_UPDATED_DTTM)
     VALUES (SEQ_XMENU_ITEM_ID.CURRVAL,
             'WMAMOBTASKSUM',
             NULL,
             1,
             SYSDATE,
             NULL,
             1,
             SYSDATE);

-- Add new Labels for Mobile Picking..

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'EndCartConformation' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'EndCartConformation',
               'Are you sure you want to end the cart?',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'BackButtonConformation' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'BackButtonConformation',
               'Are you sure you want to go to back to previous step?',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'EstDurationNotAvailable' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'EstDurationNotAvailable',
               'Estimated Duration is not available',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'ActiveTaskNotAvailable' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ActiveTaskNotAvailable',
               'Active Task is not available',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Menu' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Menu',
               'Menu',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Home' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Home',
               'Home',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Performance' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Performance',
               'Performance',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Time' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Time',
               'Time',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'NoTaskFound' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'NoTaskFound',
               'No Tasks found',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'SelectTasksToAssign' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectTasksToAssign',
               'Select Tasks to Assign',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'AddSelectedTasks' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AddSelectedTasks',
               'Add Selected Tasks',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Need' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Need',
               'Need',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'SelectItem' KEY FROM DUAL)
           B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'SelectItem',
               'Select a item from the list',
               'dm_mobile_label');

commit;

-- DBTicket DB-4412

UPDATE xmenu_item
   SET icon = 'trolley'
 WHERE     navigation_key = 'Mobilepicking.screen.makepickcart.MakePickCartScreen'
       AND name = 'Make Pick Cart - oLPN';

UPDATE xmenu_item
   SET icon = 'tote'
 WHERE     navigation_key = 'Mobilepicking.screen.makepickcart.MakePickCartScreen'
       AND name = 'Make Pick Cart - Tote';

UPDATE xmenu_item
   SET icon = 'lpn'
 WHERE     navigation_key = 'Mobilepicking.screen.packpickcart.PackPickCartScreen'
       AND name = 'Pack Pick Cart';

UPDATE xmenu_item
   SET icon = 'params'
 WHERE     navigation_key = 'Mobilepicking.screen.parameter.Parameter'
       AND name = 'User Profile';

UPDATE xmenu_item
   SET icon = 'task'
 WHERE     navigation_key = 'Mobilepicking.screen.summary.TaskSummary'
       AND name = 'Task Summary';

-- Remove un-wanted Menu/Tran Parm for [Make Pick Cart - oLPN + Make Pick Cart - Tote] Tran Ids.

DELETE FROM tran_parm
      WHERE     PGM_ID IN ('Mobile_Make_Tote_Cart', 'Mobile_Make_oLPN_Cart')
            AND from_posn = '36'
            AND to_posn = '36'
            AND parm_desc = 'Show Task Summary';

DELETE FROM TRAN_MASTER_PARM
      WHERE     TRAN_ID IN
                   (SELECT TRAN_ID
                      FROM TRAN_MASTER
                     WHERE PGM_ID IN
                              ('Mobile_Make_Tote_Cart',
                               'Mobile_Make_oLPN_Cart'))
            AND FROM_POSN = '36';
			
commit;

-- DBTicket DB-4394

MERGE INTO XMENU_ITEM_PERMISSION XIP
USING (SELECT XMENU_ITEM_ID FROM XMENU_ITEM 
WHERE NAVIGATION_KEY = 'Mobilepicking.screen.makepickcart.MakePickCartScreen' AND NAME = 'Make Pick Cart - oLPN')  XI
ON (XIP.XMENU_ITEM_ID = XI.XMENU_ITEM_ID )
WHEN MATCHED THEN
UPDATE  SET XIP.PERMISSION_CODE = 'WMAMOBMAKPICRT'
WHEN NOT MATCHED THEN 
INSERT (XMENU_ITEM_ID, PERMISSION_CODE, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES 
((SELECT XMENU_ITEM_ID FROM XMENU_ITEM 
WHERE NAME = 'Make Pick Cart - oLPN' AND NAVIGATION_KEY = 'Mobilepicking.screen.makepickcart.MakePickCartScreen'), 'WMAMOBMAKPICRT', NULL, 1, SYSDATE, NULL, 1, SYSDATE);


MERGE INTO XMENU_ITEM_PERMISSION XIP
USING (SELECT XMENU_ITEM_ID FROM XMENU_ITEM 
WHERE NAVIGATION_KEY = 'Mobilepicking.screen.makepickcart.MakePickCartScreen' AND NAME ='Make Pick Cart - Tote')  XI
ON (XIP.XMENU_ITEM_ID = XI.XMENU_ITEM_ID )
WHEN MATCHED THEN
UPDATE  SET XIP.PERMISSION_CODE = 'WMAMOBMAKPICRT'
WHEN NOT MATCHED THEN 
INSERT (XMENU_ITEM_ID, PERMISSION_CODE, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES 
((SELECT XMENU_ITEM_ID FROM XMENU_ITEM 
WHERE NAME = 'Make Pick Cart - Tote' AND NAVIGATION_KEY = 'Mobilepicking.screen.makepickcart.MakePickCartScreen'), 'WMAMOBMAKPICRT', NULL, 1, SYSDATE, NULL, 1, SYSDATE);


MERGE INTO XMENU_ITEM_PERMISSION XIP
USING (SELECT XMENU_ITEM_ID FROM XMENU_ITEM 
WHERE NAVIGATION_KEY = 'Mobilepicking.screen.packpickcart.PackPickCartScreen' AND NAME = 'Pack Pick Cart')  XI
ON (XIP.XMENU_ITEM_ID = XI.XMENU_ITEM_ID )
WHEN MATCHED THEN
UPDATE  SET XIP.PERMISSION_CODE = 'WMAMOBPKPIKCRT'
WHEN NOT MATCHED THEN 
INSERT (XMENU_ITEM_ID, PERMISSION_CODE, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES ((SELECT XMENU_ITEM_ID FROM XMENU_ITEM 
WHERE NAME = 'Pack Pick Cart' AND NAVIGATION_KEY = 'Mobilepicking.screen.packpickcart.PackPickCartScreen'), 'WMAMOBMAKPICRT', NULL, 1, SYSDATE, NULL, 1, SYSDATE);

commit;

-- DBTicket DB-4469
MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'view' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'view',
               'View',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'deleteHierarchyWarning' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'deleteHierarchyWarning',
                 'Levels exist for selected hierarchy. All Levels and Nodes will be deleted.',
                 'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'deleteLevelWarning' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'deleteLevelWarning',
                 'Nodes exist for selected level. Delete all nodes first, to delete this level.',
                 'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'deleteNodeWarning' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'deleteNodeWarning',
                 'Node Exists as parent for lower Level Node(s) can not delete.',
                 'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME,
                   'multiDeleteHierarchyWarning' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'multiDeleteHierarchyWarning',
               'Select single hierarchy for deletion.',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'maxLevelDeleteWarning' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'maxLevelDeleteWarning',
               'Levels with higher level numbers exist, can not delete',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'sameNameHierarchyError' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'sameNameHierarchyError',
               'Already Hierarchy exists with given name.',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME,
                   'sameTypeHierarchyActiveError' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'sameTypeHierarchyActiveError',
                 'Already an hierarchy with given type is active. There can be only one active hierarchy for a type.',
                 'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'sameNameLevelError' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'sameNameLevelError',
               'Already level exists with given name',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME,
                   'sameLevelContentAndContentError' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'sameLevelContentAndContentError',
               'Already Level with given content type and content exist.',
               'WmsHierarchy');


MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'levelContentEmptyError' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'levelContentEmptyError',
               'Content cannot be empty for level.',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'sameLevelContentError' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'sameLevelContentError',
               'Already Level with given content type exist.',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'sameNodeContentError' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'sameNodeContentError',
               'Already Node with given content exist.',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'discardChange' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'discardChange',
               'Discard Changes?',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'discardChangeMessage' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'discardChangeMessage',
               'Are you sure you want to discard your changes?',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'recordCreateSuccess' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'recordCreateSuccess',
               'Record created successfully.',
               'WmsHierarchy');

MERGE INTO LABEL L
     USING (SELECT 'WmsHierarchy' BUNDLE_NAME, 'changeSaveSuccess' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'changeSaveSuccess',
               'Changes saved successfully.',
               'WmsHierarchy');
COMMIT;

-- DBTicket DB-2634

INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) 
VALUES(SEQ_XMENU_ITEM_ID.NEXTVAL,'Set Parameter', 'Set Parameter','Mobilepicking.screen.parameter.Parameter','','default-icon','#002E5F',0,4,'1',0);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID, XMENU_ITEM_ID, XBASE_MENU_PART_ID, SEQUENCE) 
VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL, SEQ_XMENU_ITEM_ID.CURRVAL, (select XBASE_MENU_PART_ID from XBASE_MENU_PART where name='MobilePicking'), 1);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES (SEQ_XMENU_ITEM_ID.CURRVAL, (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'), NULL, 1, SYSDATE, NULL, 1, SYSDATE);

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES (SEQ_XMENU_ITEM_ID.CURRVAL, 'WMARFMAKPICRT', NULL, 1, SYSDATE, NULL, 1, SYSDATE);

commit;

-- DBTicket DB-2675
-- New Menu Param for 'Make Pick Cart - oLPN and Tote' flow
MERGE INTO XMENU_ITEM_PARAMETER X USING
(SELECT (select XMENU_ITEM_ID from XMENU_ITEM where NAME = 'Make Pick Cart - oLPN') XMENU_ITEM_ID, 'tranId' KEY, '205' VALUE FROM DUAL) B ON 
(X.KEY = B.KEY AND X.VALUE = B.VALUE AND X.XMENU_ITEM_ID = B.XMENU_ITEM_ID)
WHEN NOT MATCHED THEN
INSERT (X.XMENU_ITEM_PARAMETER_ID, X.XMENU_ITEM_ID, X.KEY, X.VALUE, X.CREATED_SOURCE, X.CREATED_SOURCE_TYPE, X.CREATED_DTTM, X.LAST_UPDATED_SOURCE, X.LAST_UPDATED_SOURCE_TYPE, X.LAST_UPDATED_DTTM) 
VALUES (SEQ_XMENU_ITEM_PARAMETER_ID.NEXTVAL,(select XMENU_ITEM_ID from XMENU_ITEM where NAME = 'Make Pick Cart - oLPN'),'tranId','205',null,1,SYSDATE,null,1,SYSDATE);

MERGE INTO XMENU_ITEM_PARAMETER X USING
(SELECT (select XMENU_ITEM_ID from XMENU_ITEM where NAME = 'Make Pick Cart - Tote') XMENU_ITEM_ID, 'tranId' KEY, '206' VALUE FROM DUAL) B ON 
(X.KEY = B.KEY AND X.VALUE = B.VALUE AND X.XMENU_ITEM_ID = B.XMENU_ITEM_ID)
WHEN NOT MATCHED THEN
INSERT (X.XMENU_ITEM_PARAMETER_ID, X.XMENU_ITEM_ID, X.KEY, X.VALUE, X.CREATED_SOURCE, X.CREATED_SOURCE_TYPE, X.CREATED_DTTM, X.LAST_UPDATED_SOURCE, X.LAST_UPDATED_SOURCE_TYPE, X.LAST_UPDATED_DTTM) 
VALUES (SEQ_XMENU_ITEM_PARAMETER_ID.NEXTVAL,(select XMENU_ITEM_ID from XMENU_ITEM where NAME = 'Make Pick Cart - Tote'),'tranId','206',null,1,SYSDATE,null,1,SYSDATE);

-- New Menu for 'Pack Pick Cart'
INSERT INTO XMENU_ITEM (XMENU_ITEM_ID,NAME,SHORT_NAME,NAVIGATION_KEY,TILE_KEY,ICON,BGCOLOR,SCREEN_MODE,SCREEN_VERSION,BASE_SECTION_NAME,IS_DEFAULT) 
VALUES(SEQ_XMENU_ITEM_ID.NEXTVAL,'Pack Pick Cart', 'Pack Pick Cart','Mobilepicking.screen.packpickcart.PackPickCartScreen','','default-icon','#002E5F',0,4,'1',0);

INSERT INTO XBASE_MENU_ITEM (XBASE_MENU_ITEM_ID, XMENU_ITEM_ID, XBASE_MENU_PART_ID, SEQUENCE) 
VALUES (SEQ_XBASE_MENU_ITEM_ID.NEXTVAL, SEQ_XMENU_ITEM_ID.CURRVAL, (select XBASE_MENU_PART_ID from XBASE_MENU_PART where name='MobilePicking'), 1);

INSERT INTO XMENU_ITEM_APP (XMENU_ITEM_ID, APP_ID, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES (SEQ_XMENU_ITEM_ID.CURRVAL, (SELECT APP_ID FROM APP WHERE APP_SHORT_NAME = 'WMS'), NULL, 1, SYSDATE, NULL, 1, SYSDATE); 

INSERT INTO XMENU_ITEM_PERMISSION (XMENU_ITEM_ID, PERMISSION_CODE, CREATED_SOURCE, CREATED_SOURCE_TYPE, CREATED_DTTM, LAST_UPDATED_SOURCE, LAST_UPDATED_SOURCE_TYPE, LAST_UPDATED_DTTM)
VALUES (SEQ_XMENU_ITEM_ID.CURRVAL, 'WMARFPKPIKCRT', NULL, 1, SYSDATE, NULL, 1, SYSDATE);

INSERT INTO XMENU_ITEM_PARAMETER (XMENU_ITEM_PARAMETER_ID,XMENU_ITEM_ID,KEY,VALUE,CREATED_SOURCE,CREATED_SOURCE_TYPE,CREATED_DTTM,LAST_UPDATED_SOURCE,LAST_UPDATED_SOURCE_TYPE,LAST_UPDATED_DTTM) 
VALUES (SEQ_XMENU_ITEM_PARAMETER_ID.NEXTVAL,(select XMENU_ITEM_ID from XMENU_ITEM where NAME = 'Pack Pick Cart'),'tranId','207',null,1,SYSDATE,null,1,SYSDATE);

commit;

-- DBTicket DB-3508

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'activity',
               'activity',
               'INPUT',
               'activity',
               '11410000001',
               NULL,
               CURRENT_TIMESTAMP , 
               NULL,
               CURRENT_TIMESTAMP , 
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'cartonNbr',
               'cartonNbr',
               'INPUT',
               'cartonNbr',
               '11410000011',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.20.959830000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 10.44.35.944458000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'location',
               'location',
               'INPUT',
               'location',
               '11410000002',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.21.232757000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 10.44.35.965428000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'pickCartNbr',
               'pickCartNbr',
               'INPUT',
               'pickCartNbr',
               '11410000003',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.21.501521000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 10.44.35.985437000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'pickerId',
               'pickerId',
               'INPUT',
               'pickerId',
               '11410000004',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.21.775612000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 10.44.36.005952000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'slotNbr',
               'slotNbr',
               'INPUT',
               'slotNbr',
               '11410000005',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.22.045985000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 10.44.36.027907000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'AcceptOLPN',
               'AcceptOLPN',
               'ACTION',
               'AcceptOLPN',
               'AcceptOLPN',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.22.314109000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'AcceptPickCart',
               'AcceptPickCart',
               'ACTION',
               'AcceptPickCart',
               'AcceptPickCart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.22.585205000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'AcceptPicker',
               'AcceptPicker',
               'ACTION',
               'AcceptPicker',
               'AcceptPicker',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.22.855946000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'AcceptSlot',
               'AcceptSlot',
               'ACTION',
               'AcceptSlot',
               'AcceptSlot',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.23.124686000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'CTRLF',
               'CTRLF',
               'ACTION',
               'CTRLF',
               'CTRLF',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.23.394795000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'CTRLQ',
               'CTRLQ',
               'ACTION',
               'CTRLQ',
               'CTRLQ',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.23.664151000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'EndPickCart',
               'EndPickCart',
               'ACTION',
               'EndPickCart',
               'EndPickCart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.23.933379000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'IndirectActivity',
               'IndirectActivity',
               'ACTION',
               'IndirectActivity',
               'IndirectActivity',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.24.200231000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'RecordLocation',
               'RecordLocation',
               'ACTION',
               'RecordLocation',
               'RecordLocation',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.24.468392000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'RestartMakeCart',
               'RestartMakeCart',
               'ACTION',
               'RestartMakeCart',
               'RestartMakeCart',
               'Touch to continue making cart',
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.24.741973000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make oLPN Cart',
               'StartPacking',
               'StartPacking',
               'ACTION',
               'StartPacking',
               'StartPacking',
               'Touch to start packing',
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.25.010912000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'Activity',
               'Activity',
               'INPUT',
               'Activity',
               '11410000001',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.25.277798000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.53.49.269749000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'Location',
               'Location',
               'INPUT',
               'Location',
               '11410000002',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.25.548704000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.53.49.289480000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'pickCartNbr',
               'pickCartNbr',
               'INPUT',
               'pickCartNbr',
               '11410000003',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.25.846510000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.53.49.307543000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'pickerId',
               'pickerId',
               'INPUT',
               'pickerId',
               '11410000004',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.26.113334000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.53.49.325542000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'slotNbr',
               'slotNbr',
               'INPUT',
               'slotNbr',
               '11410000005',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.26.388379000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.53.49.342231000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'taskIds',
               'taskIds',
               'INPUT',
               'taskIds',
               '11410000008',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.26.656101000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.53.49.382667000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'toteNbr',
               'toteNbr',
               'INPUT',
               'toteNbr',
               '11410000007',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.26.923975000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.53.49.408989000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'AcceptPickCart',
               'AcceptPickCart',
               'ACTION',
               'AcceptPickCart',
               'AcceptPickCart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.27.191835000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'AcceptPicker',
               'AcceptPicker',
               'ACTION',
               'AcceptPicker',
               'AcceptPicker',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.27.468435000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'AcceptSlot',
               'AcceptSlot',
               'ACTION',
               'AcceptSlot',
               'AcceptSlot',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.27.736534000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'AcceptTask',
               'AcceptTask',
               'ACTION',
               'AcceptTask',
               'AcceptTask',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.28.005361000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'AcceptTote',
               'AcceptTote',
               'ACTION',
               'AcceptTote',
               'AcceptTote',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.28.274252000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'CTRLF',
               'CTRLF',
               'ACTION',
               'CTRLF',
               'CTRLF',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.28.540864000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'CTRLQ',
               'CTRLQ',
               'ACTION',
               'CTRLQ',
               'CTRLQ',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.28.807060000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'EndPickCart',
               'EndPickCart',
               'ACTION',
               'EndPickCart',
               'EndPickCart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.29.073515000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'EndTote',
               'EndTote',
               'ACTION',
               'EndTote',
               'EndTote',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.29.340756000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 08.04.11.103235000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'IndirectAct',
               'IndirectAct',
               'ACTION',
               'IndirectAct',
               'IndirectAct',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.29.611699000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'ListTask',
               'ListTask',
               'ACTION',
               'ListTask',
               'ListTask',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.29.881604000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'RecordLocn',
               'RecordLocn',
               'ACTION',
               'RecordLocn',
               'RecordLocn',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.30.157790000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'RestartMakeCart',
               'RestartMakeCart',
               'ACTION',
               'RestartMakeCart',
               'RestartMakeCart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.30.430960000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Make Tote Cart',
               'StartPacking',
               'StartPacking',
               'ACTION',
               'StartPacking',
               'StartPacking',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.30.716953000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'itemAttributes',
               'itemAttributes',
               'INPUT',
               'itemAttributes',
               '11410000012',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.30.982056000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.541985000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'newCartonNbr',
               'newCartonNbr',
               'INPUT',
               'newCartonNbr',
               '11410000019',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.31.247954000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.560642000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'newToteNbr',
               'newToteNbr',
               'INPUT',
               'newToteNbr',
               '11410000020',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.31.521424000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.578505000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'pickCartNbr',
               'pickCartNbr',
               'INPUT',
               'pickCartNbr',
               '11410000003',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.31.787597000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.595991000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'pickerId',
               'pickerId',
               'INPUT',
               'pickerId',
               '11410000004',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.32.056637000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.617059000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'reasonCode',
               'reasonCode',
               'INPUT',
               'reasonCode',
               '11410000013',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.32.323385000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.636841000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'referenceCode',
               'referenceCode',
               'INPUT',
               'referenceCode',
               '11410000014',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.32.590872000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.654788000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scanCase',
               'scanCase',
               'INPUT',
               'scanCase',
               '11410000015',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.32.857130000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.673209000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scanQty',
               'scanQty',
               'INPUT',
               'scanQty',
               '11410000017',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.33.123169000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.692275000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scannedCarton',
               'scannedCarton',
               'INPUT',
               'scannedCarton',
               '1141000016',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.33.390896000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.711357000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scannedCheckDigit',
               'scannedCheckDigit',
               'INPUT',
               'scannedCheckDigit',
               'scannedCheckDigit',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.33.656746000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scannedItem',
               'scannedItem',
               'INPUT',
               'scannedItem',
               '11410000021',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.33.921622000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.731256000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scannedLocn',
               'scannedLocn',
               'INPUT',
               'scannedLocn',
               '11410000002',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.34.186589000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.749297000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scannedPrintRequestor',
               'scannedPrintRequestor',
               'INPUT',
               'scannedPrintRequestor',
               'scannedPrintRequestor',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.34.460351000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scannedSlot',
               'scannedSlot',
               'INPUT',
               'scannedSlot',
               '11410000005',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.34.728153000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.766803000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scannedTote',
               'scannedTote',
               'INPUT',
               'scannedTote',
               '11410000007',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.34.993180000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.785468000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'scannedTrackCase',
               'scannedTrackCase',
               'INPUT',
               'scannedTrackCase',
               '11410000018',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.35.259035000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('03-FEB-16 11.04.42.803125000 PM',
                             -- HH.MI.SSXFF AM'),
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptILPN',
               'AcceptILPN',
               'ACTION',
               'AcceptILPN',
               'AcceptILPN',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.35.524932000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptItemAttributes',
               'AcceptItemAttributes',
               'ACTION',
               'AcceptItemAttributes',
               'AcceptItemAttributes',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.35.791385000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptLocation',
               'AcceptLocation',
               'ACTION',
               'AcceptLocation',
               'AcceptLocation',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.36.057631000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptLocationQty',
               'AcceptLocationQty',
               'ACTION',
               'AcceptLocationQty',
               'AcceptLocationQty',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.36.325148000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptLocationSkuBrcd',
               'AcceptLocationSkuBrcd',
               'ACTION',
               'AcceptLocationSkuBrcd',
               'AcceptLocationSkuBrcd',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.36.590375000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptLocationSkuQty',
               'AcceptLocationSkuQty',
               'ACTION',
               'AcceptLocationSkuQty',
               'AcceptLocationSkuQty',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.36.857227000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptNewCartonOrTote',
               'AcceptNewCartonOrTote',
               'ACTION',
               'AcceptNewCartonOrTote',
               'AcceptNewCartonOrTote',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.37.124115000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptOLPNOrTote',
               'AcceptOLPNOrTote',
               'ACTION',
               'AcceptOLPNOrTote',
               'AcceptOLPNOrTote',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.37.390269000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptOLPNOrToteForAltLocnAction',
               'AcceptOLPNOrToteForAltLocnAction',
               'ACTION',
               'AcceptOLPNOrToteForAltLocnAction',
               'AcceptOLPNOrToteForAltLocnAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.37.654812000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptOLPNOrToteForEndAction',
               'AcceptOLPNOrToteForEndAction',
               'ACTION',
               'AcceptOLPNOrToteForEndAction',
               'AcceptOLPNOrToteForEndAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.37.919648000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptOLPNOrToteForEndCartAction',
               'AcceptOLPNOrToteForEndCartAction',
               'ACTION',
               'AcceptOLPNOrToteForEndCartAction',
               'AcceptOLPNOrToteForEndCartAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.38.185970000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptOLPNOrToteForShortAction',
               'AcceptOLPNOrToteForShortAction',
               'ACTION',
               'AcceptOLPNOrToteForShortAction',
               'AcceptOLPNOrToteForShortAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.38.451606000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptOLPNOrToteForSkipAction',
               'AcceptOLPNOrToteForSkipAction',
               'ACTION',
               'AcceptOLPNOrToteForSkipAction',
               'AcceptOLPNOrToteForSkipAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.38.720392000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptOLPNOrToteForSkipReplAction',
               'AcceptOLPNOrToteForSkipReplAction',
               'ACTION',
               'AcceptOLPNOrToteForSkipReplAction',
               'AcceptOLPNOrToteForSkipReplAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.38.986329000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptOUTQ',
               'AcceptOUTQ',
               'ACTION',
               'AcceptOUTQ',
               'AcceptOUTQ',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.39.252204000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptPickCart',
               'AcceptPickCart',
               'ACTION',
               'AcceptPickCart',
               'AcceptPickCart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.39.528012000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptPicker',
               'AcceptPicker',
               'ACTION',
               'AcceptPicker',
               'AcceptPicker',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.39.793837000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptQuantity',
               'AcceptQuantity',
               'ACTION',
               'AcceptQuantity',
               'AcceptQuantity',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.40.059769000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptReasonCode',
               'AcceptReasonCode',
               'ACTION',
               'AcceptReasonCode',
               'AcceptReasonCode',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.40.324574000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptReferenceCode',
               'AcceptReferenceCode',
               'ACTION',
               'AcceptReferenceCode',
               'AcceptReferenceCode',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.40.591462000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSkuBrcd',
               'AcceptSkuBrcd',
               'ACTION',
               'AcceptSkuBrcd',
               'AcceptSkuBrcd',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.40.857494000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSkuQty',
               'AcceptSkuQty',
               'ACTION',
               'AcceptSkuQty',
               'AcceptSkuQty',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.41.125300000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSlot',
               'AcceptSlot',
               'ACTION',
               'AcceptSlot',
               'AcceptSlot',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.41.392437000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSlotForAltLocnAction',
               'AcceptSlotForAltLocnAction',
               'ACTION',
               'AcceptSlotForAltLocnAction',
               'AcceptSlotForAltLocnAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.41.659053000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSlotForEndAction',
               'AcceptSlotForEndAction',
               'ACTION',
               'AcceptSlotForEndAction',
               'AcceptSlotForEndAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.41.934880000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSlotForEndCartAction',
               'AcceptSlotForEndCartAction',
               'ACTION',
               'AcceptSlotForEndCartAction',
               'AcceptSlotForEndCartAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.42.202767000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSlotForShortAction',
               'AcceptSlotForShortAction',
               'ACTION',
               'AcceptSlotForShortAction',
               'AcceptSlotForShortAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.42.469644000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSlotForSkipAction',
               'AcceptSlotForSkipAction',
               'ACTION',
               'AcceptSlotForSkipAction',
               'AcceptSlotForSkipAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.42.736567000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSlotForSkipReplAction',
               'AcceptSlotForSkipReplAction',
               'ACTION',
               'AcceptSlotForSkipReplAction',
               'AcceptSlotForSkipReplAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.43.003356000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptTrackCase',
               'AcceptTrackCase',
               'ACTION',
               'AcceptTrackCase',
               'AcceptTrackCase',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.43.270567000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'ActiveVerificationModeScreen',
               'ActiveVerificationModeScreen',
               'ACTION',
               'ActiveVerificationModeScreen',
               'ActiveVerificationModeScreen',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.43.546416000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AlternateLocationAction',
               'AlternateLocationAction',
               'ACTION',
               'AlternateLocationAction',
               'AlternateLocationAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.43.813106000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'BatchPrintLabels',
               'BatchPrintLabels',
               'ACTION',
               'BatchPrintLabels',
               'BatchPrintLabels',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.44.085829000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'BatchPrintLabelsAtEnd',
               'BatchPrintLabelsAtEnd',
               'ACTION',
               'BatchPrintLabelsAtEnd',
               'BatchPrintLabelsAtEnd',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.44.363832000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'BatchPrintLabelsAtStart',
               'BatchPrintLabelsAtStart',
               'ACTION',
               'BatchPrintLabelsAtStart',
               'BatchPrintLabelsAtStart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.44.630787000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'End',
               'End',
               'ACTION',
               'End',
               'End',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.44.897501000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'EndCartAction',
               'EndCartAction',
               'ACTION',
               'EndCartAction',
               'EndCartAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.45.174537000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'EndOlpnOrToteAction',
               'EndOlpnOrToteAction',
               'ACTION',
               'EndOlpnOrToteAction',
               'EndOlpnOrToteAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.45.440319000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'EntryScreenForPPC',
               'EntryScreenForPPC',
               'ACTION',
               'EntryScreenForPPC',
               'EntryScreenForPPC',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.45.707095000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'NextScreen',
               'NextScreen',
               'ACTION',
               'NextScreen',
               'NextScreen',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.45.973974000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'PartialPickAction',
               'PartialPickAction',
               'ACTION',
               'PartialPickAction',
               'PartialPickAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.46.240881000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'PromptForOUTQOrDefault',
               'PromptForOUTQOrDefault',
               'ACTION',
               'PromptForOUTQOrDefault',
               'PromptForOUTQOrDefault',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.46.509036000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'PromptLpnOrSlotScreen',
               'PromptLpnOrSlotScreen',
               'ACTION',
               'PromptLpnOrSlotScreen',
               'PromptLpnOrSlotScreen',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.46.778667000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'QtyUpdateForSkuBrcdMode',
               'QtyUpdateForSkuBrcdMode',
               'ACTION',
               'QtyUpdateForSkuBrcdMode',
               'QtyUpdateForSkuBrcdMode',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.47.048520000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'RemoveOlpnOrToteAction',
               'RemoveOlpnOrToteAction',
               'ACTION',
               'RemoveOlpnOrToteAction',
               'RemoveOlpnOrToteAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.47.315479000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'RestartPackCart',
               'RestartPackCart',
               'ACTION',
               'RestartPackCart',
               'RestartPackCart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.47.581215000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'ReturnToMakeCart',
               'ReturnToMakeCart',
               'ACTION',
               'ReturnToMakeCart',
               'ReturnToMakeCart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.47.851161000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'ShortAction',
               'ShortAction',
               'ACTION',
               'ShortAction',
               'ShortAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.48.118108000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'ShortActionAfterReasonCodePrompt',
               'ShortActionAfterReasonCodePrompt',
               'ACTION',
               'ShortActionAfterReasonCodePrompt',
               'ShortActionAfterReasonCodePrompt',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.48.385009000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'SkipAction',
               'SkipAction',
               'ACTION',
               'SkipAction',
               'SkipAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.48.651851000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'SkipReplAction',
               'SkipReplAction',
               'ACTION',
               'SkipReplAction',
               'SkipReplAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.48.919644000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'VerificationModeScreen',
               'VerificationModeScreen',
               'ACTION',
               'VerificationModeScreen',
               'VerificationModeScreen',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.49.185483000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'CheckForAutoManifestAndLabelPrint',
               'CheckForAutoManifestAndLabelPrint',
               'ACTION',
               'CheckForAutoManifestAndLabelPrint',
               'CheckForAutoManifestAndLabelPrint',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.49.452641000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'PrintLabelsForOlpn',
               'PrintLabelsForOlpn',
               'ACTION',
               'PrintLabelsForOlpn',
               'PrintLabelsForOlpn',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.49.720292000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'PrintOlpnLabelsForEndOlpnAction',
               'PrintOlpnLabelsForEndOlpnAction',
               'ACTION',
               'PrintOlpnLabelsForEndOlpnAction',
               'PrintOlpnLabelsForEndOlpnAction',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.49.989291000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'BatchPrintLabelsForPickCart',
               'BatchPrintLabelsForPickCart',
               'ACTION',
               'BatchPrintLabelsForPickCart',
               'BatchPrintLabelsForPickCart',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.50.254932000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'PrintOlpnLabels',
               'PrintOlpnLabels',
               'ACTION',
               'PrintOlpnLabels',
               'PrintOlpnLabels',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.50.520892000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'NextStepOnAllDetailsComplete',
               'NextStepOnAllDetailsComplete',
               'ACTION',
               'NextStepOnAllDetailsComplete',
               'NextStepOnAllDetailsComplete',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.50.815965000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'ListReasonCodes',
               'ListReasonCodes',
               'ACTION',
               'ListReasonCodes',
               'ListReasonCodes',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.51.083546000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'CompleteDetailSkip',
               'CompleteDetailSkip',
               'ACTION',
               'CompleteDetailSkip',
               'CompleteDetailSkip',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.51.350637000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'CompleteDetailEnd',
               'CompleteDetailEnd',
               'ACTION',
               'CompleteDetailEnd',
               'CompleteDetailEnd',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.51.617392000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'BeforeCompleteDetailSkip',
               'BeforeCompleteDetailSkip',
               'ACTION',
               'BeforeCompleteDetailSkip',
               'BeforeCompleteDetailSkip',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.51.886528000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'BeforeCompleteDetailEnd',
               'BeforeCompleteDetailEnd',
               'ACTION',
               'BeforeCompleteDetailEnd',
               'BeforeCompleteDetailEnd',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.52.158063000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSerialNumberCompleteSkip',
               'AcceptSerialNumberCompleteSkip',
               'ACTION',
               'AcceptSerialNumberCompleteSkip',
               'AcceptSerialNumberCompleteSkip',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.52.424515000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptSerialNumberCompleteEnd',
               'AcceptSerialNumberCompleteEnd',
               'ACTION',
               'AcceptSerialNumberCompleteEnd',
               'AcceptSerialNumberCompleteEnd',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.52.691207000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptCatchWtCompleteSkip',
               'AcceptCatchWtCompleteSkip',
               'ACTION',
               'AcceptCatchWtCompleteSkip',
               'AcceptCatchWtCompleteSkip',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.52.958722000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptCatchWtCompleteEnd',
               'AcceptCatchWtCompleteEnd',
               'ACTION',
               'AcceptCatchWtCompleteEnd',
               'AcceptCatchWtCompleteEnd',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.53.229571000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'serialNumbers',
               'serialNumbers',
               'INPUT',
               'serialNumbers',
               'serialNumbers',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.53.497622000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'catchWts',
               'catchWts',
               'INPUT',
               'catchWts',
               'catchWts',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.53.763524000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'ActiveVerificationModeInAggrPickScreen',
               'ActiveVerificationModeInAggrPickScreen',
               'ACTION',
               'ActiveVerificationModeInAggrPickScreen',
               'ActiveVerificationModeInAggrPickScreen',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.54.032282000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptLocationInAggrPickMode',
               'AcceptLocationInAggrPickMode',
               'ACTION',
               'AcceptLocationInAggrPickMode',
               'AcceptLocationInAggrPickMode',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.54.299786000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptAggregatedSkuQty',
               'AcceptAggregatedSkuQty',
               'ACTION',
               'AcceptAggregatedSkuQty',
               'AcceptAggregatedSkuQty',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.55.303676000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'LoopOlpnOrToteInAggrMode',
               'LoopOlpnOrToteInAggrMode',
               'ACTION',
               'LoopOlpnOrToteInAggrMode',
               'LoopOlpnOrToteInAggrMode',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.55.575783000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptToteInAggrMode',
               'AcceptToteInAggrMode',
               'ACTION',
               'AcceptToteInAggrMode',
               'AcceptToteInAggrMode',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.55.842347000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'AcceptOLPNInAggrMode',
               'AcceptOLPNInAggrMode',
               'ACTION',
               'AcceptOLPNInAggrMode',
               'AcceptOLPNInAggrMode',
               NULL,
               CURRENT_TIMESTAMP , -- TO_TIMESTAMP ('14-JAN-16 03.17.56.108375000 AM',
                             -- HH.MI.SSXFF AM'),
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'SkipActionInAggrMode',
               'SkipActionInAggrMode',
               'ACTION',
               'SkipActionInAggrMode',
               'SkipActionInAggrMode',
               NULL,
               CURRENT_TIMESTAMP , 
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'ShortActionInAggrMode',
               'ShortActionInAggrMode',
               'ACTION',
               'ShortActionInAggrMode',
               'ShortActionInAggrMode',
               NULL,
               CURRENT_TIMESTAMP , 
               NULL,
               NULL,
               NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY,
                                CREATED_DTTM,
                                CREATED_SOURCE,
                                LAST_UPDATED_DTTM,
                                LAST_UPDATED_SOURCE)
     VALUES (
               WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
               'Pack Pick Cart',
               'SkipReplActionInAggrMode',
               'SkipReplActionInAggrMode',
               'ACTION',
               'SkipReplActionInAggrMode',
               'SkipReplActionInAggrMode',
               NULL,
               CURRENT_TIMESTAMP , 
               NULL,
               NULL,
               NULL);
               
COMMIT;

-- DBTicket DB-4549

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000001',
               '11410000001',
               'wm',
               'MOBILEPICKING',
               'Select an [Activity]',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000002',
               '11410000002',
               'wm',
               'MOBILEPICKING',
               'Scan your current [Location]',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000003',
               '11410000003',
               'wm',
               'MOBILEPICKING',
               'Scan the [Cart] number',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000004',
               '11410000004',
               'wm',
               'MOBILEPICKING',
               'Enter the [Picker] ID',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000005',
               '11410000005',
               'wm',
               'MOBILEPICKING',
               'Enter the [Slot] number',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000006',
               '11410000006',
               'wm',
               'MOBILEPICKING',
               'Select a [Task]',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000007',
               '11410000007',
               'wm',
               'MOBILEPICKING',
               'Scan a [Tote] number',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000008',
               '11410000008',
               'wm',
               'MOBILEPICKING',
               'Scan or Enter [Task] ID',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000009',
               '11410000009',
               'wm',
               'MOBILEPICKING',
               'The [Tote] was ended',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '00016',
               '1141000016',
               'wm',
               'MOBILEPICKING',
               'Scan or Enter the [oLPN] number',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000011',
               '11410000011',
               'wm',
               'MOBILEPICKING',
               'Scan the [oLPN] number',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000012',
               '11410000012',
               'wm',
               'MOBILEPICKING',
               'Enter or Confirm the Item Attributes',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000014',
               '11410000014',
               'wm',
               'MOBILEPICKING',
               'Enter a reference value for reason code you selected',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000013',
               '11410000013',
               'wm',
               'MOBILEPICKING',
               'Enter or Select a reason code',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000015',
               '11410000015',
               'wm',
               'MOBILEPICKING',
               'Scan or Enter the [Tote] number',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000017',
               '11410000017',
               'wm',
               'MOBILEPICKING',
               'Confirm the need by entering quanity',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000018',
               '11410000018',
               'wm',
               'MOBILEPICKING',
               'Scan the prompted  [iLPN] from the [iLPN] tracked location',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000019',
               '11410000019',
               'wm',
               'MOBILEPICKING',
               'Scan or enter a new [oLPN] number to replace the [oLPN] you just ended',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000021',
               '11410000021',
               'wm',
               'MOBILEPICKING',
               'Scan or enter the Item ',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG,
                            CREATED_DTTM,
                            LAST_UPDATED_DTTM)
     VALUES (
               SEQ_MESSAGE_MASTER_ID.NEXTVAL,
               '000020',
               '11410000020',
               'wm',
               'MOBILEPICKING',
               'Scan or enter a new [Tote] number to replace the [Tote] you just ended',
               'ErrorMessage',
               'USER',
               'INFO',
               NULL,
               'N',
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP);
			   
COMMIT;

-- DBTicket DB-4544
UPDATE TRAN_MASTER
   SET TASK_NAME = 'MOBPckPickCart'
 WHERE TRAN_ID = 207 AND PGM_ID = 'Mobile_Pack_Pick_Cart';

UPDATE TRAN_MASTER
   SET TASK_NAME = 'MOBAnchorOBLpn'
 WHERE TRAN_ID = 209 AND PGM_ID = 'Mobile_Anchor_oLPN';

UPDATE TRAN_MASTER_PARM
   SET MENU_PARM = 'MOBPckPickCart'
 WHERE TRAN_ID IN (205, 206) AND FROM_POSN = 21;

UPDATE TRAN_MASTER_PARM
   SET MENU_PARM = 'MOBAnchorOBLpn'
 WHERE TRAN_ID IN (207) AND FROM_POSN = 65;
 
 COMMIT;
 
-- DBTicket DB-4573
MERGE INTO BRCD_STRUCT BS
     USING (SELECT 12 BRCD_LEN, 'Pick Cart' BRCD_TYPE FROM DUAL) B
        ON (BS.BRCD_LEN = B.BRCD_LEN AND BS.BRCD_TYPE = B.BRCD_TYPE)
WHEN NOT MATCHED
THEN
   INSERT     (BRCD_TYPE,
               BRCD_LEN,
               SEQ_NBR,
               BRCD_DESC,
               BRCD_PFX,
               BRCD_PFX_LEN,
               STRIP_BARCODE_PFX,
               CHK_DIGIT_START_POSN,
               CHK_DIGIT_LEN,
               CHK_DIGIT_TYPE,
               STRIP_CHK_DIGIT_FLAG,
               SEQ_NBR_START_POSN,
               SEQ_LEN,
               VALID_UNIQUE_SEQ_FLAG,
               STRIP_SEQ_NBR_FLAG,
               SUBFLD_1_START_POSN,
               SUBFLD_1_LEN,
               SUBFLD_2_START_POSN,
               SUBFLD_2_LEN,
               SUBFLD_3_START_POSN,
               SUBFLD_3_LEN,
               BRCD_SYMB,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               AI,
               STRIP_AI_FLAG,
               BRCD_STRUCT_ID,
               WM_VERSION_ID,
               AI_MASTER_ID,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
       VALUES ('Pick Cart',
               12,
               0,
               NULL,
               NULL,
               0,
               NULL,
               0,
               0,
               NULL,
               NULL,
               0,
               0,
               NULL,
               NULL,
               0,
               0,
               0,
               0,
               0,
               0,
               NULL,
               CURRENT_TIMESTAMP,
               CURRENT_TIMESTAMP,
               'WMADMIN',
               NULL,
               'N',
               BRCD_STRUCT_ID_SEQ.NEXTVAL,
               1,
               0,
               CURRENT_TIMESTAMP,
               NULL);
COMMIT;

-- DBTicket DB-4628
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'ServerRequestFailedRedo' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ServerRequestFailedRedo',
               'Server request failed, please redo the operation.',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'ServerRequestFailedAppLog' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ServerRequestFailedAppLog',
                 'An error occurred while processing this request on the server. Please contact an administrator.',
                 'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'AppServerUnreachable' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AppServerUnreachable',
               'Appserver is not reachable. Check Internet Connection.',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'ClientRequestFailedAppLog' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ClientRequestFailedAppLog',
                 'The mobile client has made an invalid request to the server. Please contact an administrator.',
                 'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'ErrorRetrievingLoginServerHandle' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (
                 SEQ_LABEL_ID.NEXTVAL,
                 'ErrorRetrievingLoginServerHandle',
                 'Error in retrieving the login server handle. Please contact an administrator.',
                 'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Picked' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Picked',
               'Picked',
               'dm_mobile_label');
			   
COMMIT;

-- DBTicket DB-4634
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'outof' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'outof',
               'out of',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Updatedat' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Updatedat',
               'Updated at',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Selected' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Selected',
               'Selected',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Resultsexceeded' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Resultsexceeded',
               'Results exceeded',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'limit' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'limit',
               'limit',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'Modifyfiltertoreduceresults' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Modifyfiltertoreduceresults',
               'Modify filter to reduce results',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Entertaskid' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Entertaskid',
               'Enter task id',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'OperationSuccessful' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'OperationSuccessful',
               'Operation Successful',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'AssignUser' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'AssignUser',
               'Assign User',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'Prioritize' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Prioritize',
               'Prioritize',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'E' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'E',
               'E',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'All' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'All',
               'All',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'enterNote' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'enterNote',
               'enter note:',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'NoMoreRecordsToLoad' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'NoMoreRecordsToLoad',
               'No more records to load',
               'lm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME, 'NSLASHA' KEY FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'NSLASHA',
               'N/A',
               'lm_mobile_label');
COMMIT;
-- DBTIcket DB-4701
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Tote(s)remaining' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Tote(s)remaining',
               'Tote(s) remaining',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'oLPN(s)remaining' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'oLPN(s)remaining',
               'oLPN(s) remaining',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME,
                   'ActiveTaskisnotavailable' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ActiveTaskisnotavailable',
               'Active Task is not available',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'minor' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'minor',
               'minor',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'T' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'T',
               'T',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Go' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Go',
               'Go',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'EndEntry' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'EndEntry',
               'End Entry',
               'dm_mobile_label');
			   
COMMIT;

-- DBTicket DB-4718
merge into label l
     using (select 'lm_mobile_label' bundle_name, 'Select' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (l.label_id,
               l.key,
               l.value,
               l.bundle_name)
       values (seq_label_id.nextval,
               'Select',
               'Select',
               'lm_mobile_label');

merge into label l
     using (select 'lm_mobile_label' bundle_name, 'Priority' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (l.label_id,
               l.key,
               l.value,
               l.bundle_name)
       values (seq_label_id.nextval,
               'Priority',
               'Priority',
               'lm_mobile_label');

merge into label l
     using (select 'lm_mobile_label' bundle_name, 'ReleaseTaskMsg' key
              from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (l.label_id,
               l.key,
               l.value,
               l.bundle_name)
       values (seq_label_id.nextval,
               'ReleaseTaskMsg',
               'Do you want to release the selected task(s)?',
               'lm_mobile_label');

merge into label l
     using (select 'lm_mobile_label' bundle_name, 'HoldTaskMsg' key from dual)
           b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (l.label_id,
               l.key,
               l.value,
               l.bundle_name)
       values (seq_label_id.nextval,
               'HoldTaskMsg',
               'Do you want to hold the selected task(s)?',
               'lm_mobile_label');

merge into label l
     using (select 'lm_mobile_label' bundle_name, 'SUCCESS' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (l.label_id,
               l.key,
               l.value,
               l.bundle_name)
       values (seq_label_id.nextval,
               'SUCCESS',
               'SUCCESS',
               'lm_mobile_label');

merge into label l
     using (select 'lm_mobile_label' bundle_name, 'FAILURE' key from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (l.label_id,
               l.key,
               l.value,
               l.bundle_name)
       values (seq_label_id.nextval,
               'FAILURE',
               'FAILURE',
               'lm_mobile_label');

merge into label l
     using (select 'lm_mobile_label' bundle_name,
                   'AddInteractionDisclaimer' key
              from dual) b
        on (l.key = b.key and l.bundle_name = b.bundle_name)
when not matched
then
   insert     (l.label_id,
               l.key,
               l.value,
               l.bundle_name)
       values (
                 seq_label_id.nextval,
                 'AddInteractionDisclaimer',
                 'Fill in the required *fields to activate the Save and Observe.',
                 'lm_mobile_label');

commit;

-- DBTicket DB-4758

MERGE INTO APPLICATION_CONFIGURATION AC
     USING (SELECT 'headless.service.auth.timeout' KEY FROM DUAL) B
        ON (AC.KEY = B.KEY)
WHEN NOT MATCHED
THEN
   INSERT     (AC.KEY,
               AC.VALUE,
               AC.MODULE,
               AC.APPLY_TO_SERVER,
               AC.COMMENTS,
               AC.TAGS)
       VALUES ('headless.service.auth.timeout',
               '36000000',
               'global',
               'ALL',
               'Timeout in milliseconds For Mobile App',
               'Session Timeout');
			   
COMMIT;

-- DBTicket DB-4821
UPDATE xmenu_item
 SET name = 'User Profile', short_name = 'User Profile', icon = 'params'
 WHERE navigation_key = 'Mobilepicking.screen.parameter.Parameter';

COMMIT;

-- DBTicket DB-4864
CREATE TABLE WORKFLOW_COMPONENT_PERM
(
   WORKFLOW_COMPONENT_PERM_ID   NUMBER (9) NOT NULL,
   SCRIPT_NAME                  VARCHAR2 (200) NOT NULL,
   COMPONENT_NAME               VARCHAR2 (100) NOT NULL,
   PERMISSION_CODE              VARCHAR2 (16) NOT NULL,
   CREATED_DTTM                 TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
   CREATED_SOURCE               VARCHAR2 (50),
   LAST_UPDATED_DTTM            TIMESTAMP,
   LAST_UPDATED_SOURCE          VARCHAR2 (50)
)
TABLESPACE LLMDATA;

ALTER TABLE WORKFLOW_COMPONENT_PERM
   ADD CONSTRAINT WORKFLOW_COMPONENT_PERM_PK PRIMARY KEY
          (WORKFLOW_COMPONENT_PERM_ID);

ALTER TABLE WORKFLOW_COMPONENT_PERM
   ADD CONSTRAINT WORKFLOW_COMPONENT_PERM_UK1 UNIQUE
          (SCRIPT_NAME, COMPONENT_NAME, PERMISSION_CODE);

ALTER TABLE WORKFLOW_COMPONENT_PERM
   ADD CONSTRAINT WORKFLOW_COMPONENT_PERM_FK1 FOREIGN KEY
          (SCRIPT_NAME, COMPONENT_NAME)
           REFERENCES WORKFLOW_COMPONENT (SCRIPT_NAME, COMPONENT_NAME);


COMMENT ON TABLE WORKFLOW_COMPONENT_PERM IS
   'Stores the cross references from workflow components to permissions';
COMMENT ON COLUMN WORKFLOW_COMPONENT_PERM.WORKFLOW_COMPONENT_PERM_ID IS
   'Identifier for cross references, primary key to this table';
COMMENT ON COLUMN WORKFLOW_COMPONENT_PERM.SCRIPT_NAME IS
   'Name of the DSL script';
COMMENT ON COLUMN WORKFLOW_COMPONENT_PERM.COMPONENT_NAME IS
   'name of the component for which the DSL script to be located';
COMMENT ON COLUMN WORKFLOW_COMPONENT_PERM.PERMISSION_CODE IS
   'Permission code';
COMMENT ON COLUMN WORKFLOW_COMPONENT_PERM.CREATED_DTTM IS 'Created date time';
COMMENT ON COLUMN WORKFLOW_COMPONENT_PERM.CREATED_SOURCE IS 'Created source';
COMMENT ON COLUMN WORKFLOW_COMPONENT_PERM.LAST_UPDATED_DTTM IS
   'last updated date time';
COMMENT ON COLUMN WORKFLOW_COMPONENT_PERM.LAST_UPDATED_SOURCE IS
   'last updated source';

CREATE SEQUENCE WORKFLOW_COMP_PERM_ID_SEQ;

-- DBTicket DB-4903
MERGE INTO LABEL L
     USING (SELECT 'wm_label' BUNDLE_NAME,
                   'DisplayContextSensitiveHelpMsgDuringMobilePicking' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               B.KEY,
               'Display Context Sensitive Help Msg during Mobile Picking',
               B.BUNDLE_NAME);
COMMIT;

-- DBTicket DB-4906

Update LABEL set value = 'Operation failed. Please try again and If the problem persists, please re-login.' where key = 'ServerRequestFailedAppLog' and bundle_name = 'dm_mobile_label';

MERGE INTO MESSAGE_MASTER m USING (SELECT '1141000022' KEY, 'ErrorMessage' BUNDLE_NAME FROM DUAL) d ON (m.KEY = d.KEY AND m.BUNDLE_NAME = d.BUNDLE_NAME)
WHEN NOT MATCHED THEN
INSERT (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM)
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'000022','1141000022','wm','MOBILEPICKING','Next [Location] <b>{0}</b> ','ErrorMessage','USER','INFO',null,'N',current_timestamp,current_timestamp);

MERGE INTO MESSAGE_MASTER m USING (SELECT '1141000023' KEY, 'ErrorMessage' BUNDLE_NAME FROM DUAL) d ON (m.KEY = d.KEY AND m.BUNDLE_NAME = d.BUNDLE_NAME)
WHEN NOT MATCHED THEN
INSERT (MESSAGE_MASTER_ID,MSG_ID,KEY,ILS_MODULE,MSG_MODULE,MSG,BUNDLE_NAME,MSG_CLASS,MSG_TYPE,OVRIDE_ROLE,LOG_FLAG,CREATED_DTTM,LAST_UPDATED_DTTM)
VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,'000023','1141000023','wm','MOBILEPICKING','Time Remaining <b>{0}</b> ','ErrorMessage','USER','INFO',null,'N',current_timestamp,current_timestamp);

commit;

-- DBTicket DB-4924

update hierarchy_types set name='INVENTORY_NEED' where name = 'Inventory Need Type Hierarchy';

commit;

-- DBTicket DB-4969

UPDATE TRAN_MASTER
   SET TRAN_NAME = 'Make Pick Cart - oLPNs'
 WHERE TRAN_ID = '205';

UPDATE tran_master
   SET tran_name = 'Make Pick Cart - Tote'
 WHERE tran_id = '206';

UPDATE TRAN_MASTER
   SET TRAN_NAME = 'Pack Pick Cart'
 WHERE TRAN_ID = '207';

UPDATE tran_master
   SET tran_name = 'Anchor oLPN'
 WHERE tran_id = '209';

COMMIT;



-- DBTicket DB-5098

UPDATE UI_MENU_ITEM
   SET link = 'return checkMessageTypeBeforeSend()', link_type = 'JS'
 WHERE     UI_MENU_SCREEN_ID IN (SELECT UI_MENU_SCREEN_ID
                                   FROM UI_MENU_SCREEN
                                  WHERE SCREEN_ID = '1800042')
       AND display_text = 'Send';

MERGE INTO TRAN_PARM tpTrg
     USING (SELECT 'Mobile_Pack_Pick_Cart' pgm_id, 97 from_posn FROM DUAL)
           tpKey
        ON (tpTrg.pgm_id = tpKey.pgm_id AND tpTrg.from_posn = tpKey.from_posn)
WHEN NOT MATCHED
THEN
   INSERT     (PGM_ID,
               FROM_POSN,
               TO_POSN,
               PARM_DESC,
               LITRL,
               MANDT_FLAG,
               VALID_CODE,
               VALID_SYS_CODE_REC_TYPE,
               VALID_SYS_CODE_TYPE,
               VALID_FROM,
               VALID_TO,
               VALID_VALUES,
               EDIT_STYLE,
               CREATE_DATE_TIME,
               MOD_DATE_TIME,
               USER_ID,
               TRAN_PARM_ID,
               WM_VERSION_ID,
               PARM_ID,
               PARM_KEY,
               CREATED_DTTM,
               LAST_UPDATED_DTTM)
     VALUES ('Mobile_Pack_Pick_Cart',
             97,
             97,
             'Display next location in advance during picking',
             'Display next locn',
             'N',
             'F',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             'CKBX',
             CURRENT_TIMESTAMP,
             CURRENT_TIMESTAMP,
             'SEED',
             TRAN_PARM_ID_SEQ.NEXTVAL,
             1,
             NULL,
             'Display_next_location',
             CURRENT_TIMESTAMP,
             CURRENT_TIMESTAMP);

commit;

-- DBTicket DB-5162
MERGE INTO LABEL L
     USING (SELECT 'lm_mobile_label' BUNDLE_NAME,
                   'EnterSearchCriteriaToViewResults' KEY
              FROM DUAL) B
        ON (L.key = B.key AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.key,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'EnterSearchCriteriaToViewResults',
               'Enter Search Criteria to View Results',
               'lm_mobile_label');
               
COMMIT;

-- DBTicket DB-5329
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Alerts' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Alerts',
               'Alerts',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'Notifications' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'Notifications',
               'Notifications',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ReceivedAt' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ReceivedAt',
               'Received at',
               'dm_mobile_label');

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'NoNotificationMsg' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'NoNotificationMsg',
               'No notification message available.',
               'dm_mobile_label');
COMMIT;

-- DBTicket DB-5323

MERGE INTO RESOURCES R
     USING (SELECT '/services/rest/wm/*' URI FROM DUAL) B
        ON (R.URI = B.URI)
WHEN NOT MATCHED
THEN 
INSERT (RESOURCE_ID,URI,MODULE,URI_TYPE_ID)
     VALUES (SEQ_RESOURCE_ID.NEXTVAL, '/services/rest/wm/*', 'WMMOBILE', '2');
     
     
MERGE INTO RESOURCE_PERMISSION R
     USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI= '/services/rest/wm/*') B
        ON (R.RESOURCE_ID = B.RESOURCE_ID AND  R.PERMISSION_CODE = 'UCLU')
WHEN NOT MATCHED
THEN 
INSERT (RESOURCE_ID,PERMISSION_CODE)
     VALUES ((select RESOURCE_ID from RESOURCES where URI = '/services/rest/wm/*' and MODULE = 'WMMOBILE' and URI_TYPE_ID = '2') , 'UCLU');     

MERGE INTO RESOURCES R
     USING (SELECT '/services/rest/dm/*' URI FROM DUAL) B
        ON (R.URI = B.URI)
WHEN NOT MATCHED
THEN 
INSERT (RESOURCE_ID,URI,MODULE,URI_TYPE_ID)
     VALUES ((SELECT MAX(RESOURCE_ID)+1 FROM RESOURCES), '/services/rest/dm/*', 'WMMOBILE', '2');
     
     
MERGE INTO RESOURCE_PERMISSION REP
     USING (SELECT RESOURCE_ID FROM RESOURCES WHERE URI= '/services/rest/dm/*' ) B
        ON (REP.RESOURCE_ID = B.RESOURCE_ID AND REP.PERMISSION_CODE = 'UCLU')
WHEN NOT MATCHED
THEN 
INSERT (RESOURCE_ID,PERMISSION_CODE)
     VALUES ((select RESOURCE_ID from RESOURCES where URI = '/services/rest/dm/*') , 'UCLU');

commit;

-- DBTicket DB-5436
DELETE FROM WORKFLOW_COMPONENT_PERM
      WHERE SCRIPT_NAME IN
               ('Make Tote Cart', 'Make oLPN Cart', 'Pack Pick Cart');

DELETE FROM WORKFLOW_COMPONENT
      WHERE SCRIPT_NAME IN
               ('Make Tote Cart', 'Make oLPN Cart', 'Pack Pick Cart');



INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000022',
             '11410000022',
             'wm',
             'MOBILEPICKING',
             'Touch to continue make pick cart',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000023',
             '11410000023',
             'wm',
             'MOBILEPICKING',
             'Touch to start picking.',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000024',
             '11410000024',
             'wm',
             'MOBILEPICKING',
             'Select an option for cycle count',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000025',
             '11410000025',
             'wm',
             'MOBILEPICKING',
             'Scan or enter cycle count location.',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000026',
             '11410000026',
             'wm',
             'MOBILEPICKING',
             'Count and enter quantity in location.',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000027',
             '11410000027',
             'wm',
             'MOBILEPICKING',
             'Scan or enter [oLPN] or [Tote] number',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000028',
             '11410000028',
             'wm',
             'MOBILEPICKING',
             'Enter Weight.',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000029',
             '11410000029',
             'wm',
             'MOBILEPICKING',
             'Enter the check digit.',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000030',
             '11410000030',
             'wm',
             'MOBILEPICKING',
             'Enter the print requestor.',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000031',
             '11410000031',
             'wm',
             'MOBILEPICKING',
             'Scan or Enter the [Location].',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000032',
             '11410000032',
             'wm',
             'MOBILEPICKING',
             'Enter serial number.',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');

INSERT INTO MESSAGE_MASTER (MESSAGE_MASTER_ID,
                            MSG_ID,
                            KEY,
                            ILS_MODULE,
                            MSG_MODULE,
                            MSG,
                            BUNDLE_NAME,
                            MSG_CLASS,
                            MSG_TYPE,
                            OVRIDE_ROLE,
                            LOG_FLAG)
     VALUES (SEQ_MESSAGE_MASTER_ID.NEXTVAL,
             '000033',
             '11410000033',
             'wm',
             'MOBILEPICKING',
             'Enter Task ID.',
             'ErrorMessage',
             'USER',
             'INFO',
             NULL,
             'N');


INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'EndPickCart',
                  'This button ends the tote cart thereby creating a verify pick cart task.',
                  'ACTION',
                  'EndPickCart',
                  'EndPickCart',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Make Tote Cart',
             'RecordLocation',
             'This button records current location of the user.',
             'ACTION',
             'RecordLocation',
             'RecordLocation',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'IndirectActivity',
                  'This button facilitates entering of approved indirect or non-standard activity',
                  'ACTION',
                  'IndirectActivity',
                  'IndirectActivity',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'EndTote',
                  'This button ends the Tote after tasks have been assigned to it. Original tasks get cancelled.',
                  'ACTION',
                  'EndTote',
                  'EndTote',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Make Tote Cart',
             'ListTask',
             'This button is used to list task for assigning to totes.',
             'ACTION',
             'ListTask',
             'ListTask',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'nonStdActivity',
                  'This user input field accepts approved indirect or non-standard activity.',
                  'INPUT',
                  'nonStdActivity',
                  'nonStdActivity',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Make Tote Cart',
             'pickCartNbr',
             'This user input field accepts cart number to be built.',
             'INPUT',
             'pickCartNbr',
             'pickCartNbr',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'pickerId',
                  'This user input field accepts picker number corresponding to a zone.',
                  'INPUT',
                  'pickerId',
                  'pickerId',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'recordLocn',
                  'This user input field accepts location to be recorded using Record Location action.',
                  'INPUT',
                  'recordLocn',
                  'recordLocn',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'slotNbr',
                  'This user input field accepts slot number for putting the tote on cart.',
                  'INPUT',
                  'slotNbr',
                  'slotNbr',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'taskIds',
                  'This user input field accepts tasks selected on Task List (Multi Select).',
                  'INPUT',
                  'taskIds',
                  'taskIds',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Make Tote Cart',
             'toteNbr',
             'This user input field accepts tote number for cart',
             'INPUT',
             'toteNbr',
             'toteNbr',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'RestartMakeCart',
                  'This intrinsic action restarts the cart building process for a new cart.',
                  'ACTION',
                  'RestartMakeCart',
                  'RestartMakeCart',
                  '11410000022');

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make Tote Cart',
                  'StartPacking',
                  'This intrinsic action starts the picking process once cart is created. Applicable only when verify cart menu parameter is set.',
                  'ACTION',
                  'StartPacking',
                  'StartPacking',
                  '11410000023');



INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make oLPN Cart',
                  'EndPickCart',
                  'This intinsic action ends OLPN cart thereby creating a verify pick cart task. ',
                  'ACTION',
                  'EndPickCart',
                  'EndPickCart',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Make oLPN Cart',
             'RecordLocation',
             'This button records current location of the user.',
             'ACTION',
             'RecordLocation',
             'RecordLocation',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make oLPN Cart',
                  'IndirectActivity',
                  'This button facilitates entering of approved indirect or non-standard activity',
                  'ACTION',
                  'IndirectActivity',
                  'IndirectActivity',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Make oLPN Cart',
             'cartonNbr',
             'This user input field accept olpn number for cart',
             'INPUT',
             'cartonNbr',
             'cartonNbr',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make oLPN Cart',
                  'nonStdActivity',
                  'This user input field accepts approved indirect or non-standard activity.',
                  'INPUT',
                  'nonStdActivity',
                  'nonStdActivity',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Make oLPN Cart',
             'pickCartNbr',
             'This user input field accepts cart number to be built.',
             'INPUT',
             'pickCartNbr',
             'pickCartNbr',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make oLPN Cart',
                  'pickerId',
                  'This user input field accepts picker number corresponding to a zone.',
                  'INPUT',
                  'pickerId',
                  'pickerId',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make oLPN Cart',
                  'recordLocn',
                  'This user input field accepts location to be recorded using Record Location action.',
                  'INPUT',
                  'recordLocn',
                  'recordLocn',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make oLPN Cart',
                  'slotNbr',
                  'This user input field accepts slot number for putting the tote on cart.',
                  'INPUT',
                  'slotNbr',
                  'slotNbr',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make oLPN Cart',
                  'RestartMakeCart',
                  'This intrinsic action restarts the cart building process for a new cart',
                  'ACTION',
                  'RestartMakeCart',
                  'RestartMakeCart',
                  '11410000022');

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Make oLPN Cart',
                  'StartPacking',
                  'This intrinsic action starts the picking process once cart is created. Applicable only when verify cart menu parameter is set.',
                  'ACTION',
                  'StartPacking',
                  'StartPacking',
                  '11410000023');



INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'AlternateLocationAction',
                  'This button searches for an alternate location from where allocation for the current need can be satisfied.',
                  'ACTION',
                  'AlternateLocationAction',
                  'AlternateLocationAction',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'EndCartAction',
                  'This button shorts/removes the remaining details on the cart. Details either get shorted or disassocaited from the cart based on menu parameter 27 i.e. Short remaining picks for Cart/Zone.',
                  'ACTION',
                  'EndCartAction',
                  'EndCartAction',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'EndLpnActionAtPutConfirmInAggrMode',
                  'This button ends the current OLPN/Tote and splits the remaining into a new OLPN/Tote. It is available only in aggregate Mode at OLPN/Tote put confirm screen after aggregate Qty has been picked.',
                  'ACTION',
                  'EndLpnActionAtPutConfirmInAggrMode',
                  'EndLpnActionAtPutConfirmInAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'EndLpnActionInPartialPickAggrMode',
                  'This button ends the current OLPN/Tote and splits the remaining into a new OLPN/Tote. It is available only in partial pick aggregate Mode at OLPN/Tote confirm screen after aggregate Qty has been picked and partial pick action taken.',
                  'ACTION',
                  'EndLpnActionInPartialPickAggrMode',
                  'EndLpnActionInPartialPickAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'EndOlpnOrToteAction',
                  'This button ends the current OLPN/Tote and splits the remaining into a new OLPN/Tote.',
                  'ACTION',
                  'EndOlpnOrToteAction',
                  'EndOlpnOrToteAction',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'IndirectActivity',
                  'This button facilitates entering of approved indirect or non-standard activity',
                  'ACTION',
                  'IndirectActivity',
                  'IndirectActivity',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'PartialPickAction',
                  'This button facilitates partial put of qty in aggregate mode. It is available at OLPN/Tote put confirm screen after aggregated qty has been picked.',
                  'ACTION',
                  'PartialPickAction',
                  'PartialPickAction',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'RecordLocation',
             'This button records current location of the user.',
             'ACTION',
             'RecordLocation',
             'RecordLocation',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'RemoveOlpnOrToteAction',
                  'This buttom removes the OLPN/Tote from the cart. It results in creation of fresh allocations in zero status. Removed OLPN/Tote can be reused via make pick cart process.',
                  'ACTION',
                  'RemoveOlpnOrToteAction',
                  'RemoveOlpnOrToteAction',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'ShortAction',
                  'This button shorts the detail being worked upon. Based on the reason code associated OLPN is kept open/completed/cancelled.',
                  'ACTION',
                  'ShortAction',
                  'ShortAction',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'ShortActionAtPutConfirmInAggrMode',
                  'This button shorts the current detail. Based on reason code associated OLPN is kept open/completed/cancelled. It is available only in aggregate Mode at OLPN/Tote put confirm screen after aggregate Qty has been picked.',
                  'ACTION',
                  'ShortActionAtPutConfirmInAggrMode',
                  'ShortActionAtPutConfirmInAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'ShortActionInAggrMode',
                  'This button shorts all the details which are associated with aggregated qty shown for Item location combination. It is available only in aggregate mode and shortage span across single/multiple OLPNs.',
                  'ACTION',
                  'ShortActionInAggrMode',
                  'ShortActionInAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'ShortActionInPartialPickAggrMode',
                  'This button shorts the current detail. It is available on partial put screen in aggregate mode.',
                  'ACTION',
                  'ShortActionInPartialPickAggrMode',
                  'ShortActionInPartialPickAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'SkipAction',
                  'This button skips the current detail and moves to next detail. In case of partial pick and skip OLPN goes to inpacking status.',
                  'ACTION',
                  'SkipAction',
                  'SkipAction',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'SkipActionAtPutConfirmInAggrMode',
                  'This button skips the current detail and moves to next detail. It is available on OLPN/Tote put confirm screem in aggregate mode. The skip loops between the aggregated details for a location/item combination.',
                  'ACTION',
                  'SkipActionAtPutConfirmInAggrMode',
                  'SkipActionAtPutConfirmInAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'SkipActionInAggrMode',
                  'This button skips all the aggregated details for item/location combination and and moves to next Item/Location picks. It is available only in aggregate mode.',
                  'ACTION',
                  'SkipActionInAggrMode',
                  'SkipActionInAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'SkipActionInPartialPickAggrMode',
                  'This button skips the current detail and moves to next detail of the aggregated pick. It is available on partial put qty screem in aggregate mode. The skip takes the user out of partial put and back to next put confirm within the aggregated details for a location/item combination.',
                  'ACTION',
                  'SkipActionInPartialPickAggrMode',
                  'SkipActionInPartialPickAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'SkipReplAction',
                  'This button skips the current detail and tries creating/bumping a replenishment task and moves to next detail. In case of partial pick and skip OLPN goes to inpacking status.',
                  'ACTION',
                  'SkipReplAction',
                  'SkipReplAction',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'SkipReplActionAtPutConfirmInAggrMode',
                  'This button skips the current detail and tries creating/bumping a replenishment task and moves to next detail. It is available on OLPN/Tote put confirm screem in aggregate mode. The skip loops between the aggregated details for a location/item combination.',
                  'ACTION',
                  'SkipReplActionAtPutConfirmInAggrMode',
                  'SkipReplActionAtPutConfirmInAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'SkipReplActionInAggrMode',
                  'This button skips all the aggregated details for item/location combination and tries creating/bumping a replenishment task and moves to next Item/Location picks. It is available only in aggregate mode.',
                  'ACTION',
                  'SkipReplActionInAggrMode',
                  'SkipReplActionInAggrMode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'ccCreateTask',
                  'This user input field accepts user response (Y/N) to create Cycle count task.',
                  'INPUT',
                  'ccCreateTask',
                  'ccCreateTask',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'ccLocnEmpty',
                  'This user input field accepts user response (Y/N) to confirm if the location is empty to determine if a cycle count task is needed.',
                  'INPUT',
                  'ccLocnEmpty',
                  '11410000025',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'ccRemainingQty',
                  'This user input field accepts remiaining quantity for a location to detemine if a cycle count task is needed.',
                  'INPUT',
                  'ccRemainingQty',
                  '11410000026',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'itemAttributes',
                  'This intrinsic input field facilitate transition to sku attribute screen.',
                  'INPUT',
                  'itemAttributes',
                  'itemAttributes',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'newCartonNbr',
                  'This user input field accepts new carton in split (End LPN) scenarios. ',
                  'INPUT',
                  'newCartonNbr',
                  'newCartonNbr',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'newSlotNbr',
                  'This user input field accepts new slot to put the newly created LPN on the cart.',
                  'INPUT',
                  'newSlotNbr',
                  'newSlotNbr',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'newToteNbr',
                  'This user input field accepts new tote in split (End LPN) scenarios.',
                  'INPUT',
                  'newToteNbr',
                  'newToteNbr',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'nonStdActivity',
                  'This user input field accepts approved indirect or non-standard activity.',
                  'INPUT',
                  'nonStdActivity',
                  'nonStdActivity',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'olpnOrToteNbrForEntry',
                  'This user entry field accepts OLPN/Tote nbr to identify the pick cart task to launch into. Available only when Entry mode menu parameter is set to OLPN/Tote.',
                  'INPUT',
                  'olpnOrToteNbrForEntry',
                  'olpnOrToteNbrForEntry',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'pickCartNbr',
                  'This user input field accepts cart number to identify and load the associated task details to be picked/packed.',
                  'INPUT',
                  'pickCartNbr',
                  'pickCartNbr',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'pickerId',
                  'This user input field accepts the picker id corresponding to a zone. Task details for that zone alone are loaded and available for picking/packing.',
                  'INPUT',
                  'pickerId',
                  'pickerId',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'reasonCode',
                  'This user input field accepts the reason code while shorting. Based on reason code shortages are performed at ORDER/OLPN level.',
                  'INPUT',
                  'reasonCode',
                  'reasonCode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'recordLocn',
                  'This user input field accepts location to be recorded using Record Location action.',
                  'INPUT',
                  'recordLocn',
                  'recordLocn',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'referenceCode',
                  'This user input field accepts reference code during shorting. It is an optional field and user can choose not to enter.',
                  'INPUT',
                  'referenceCode',
                  'referenceCode',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scanCase',
                  'This user input field accepts ILPN number for reserve location picks.',
                  'INPUT',
                  'scanCase',
                  'scanCase',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scanQty',
                  'This user input field accepts qty to be picked/put in respective UOM.',
                  'INPUT',
                  'scanQty',
                  'scanQty',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scanWt',
                  'This user input field accepts catch weight for catch weight enabled items.',
                  'INPUT',
                  'scanWt',
                  'scanWt',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scannedCarton',
                  'This user input field accepts the OLPN number after pick to complete packing.',
                  'INPUT',
                  'scannedCarton',
                  'scannedCarton',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'scannedCheckDigit',
             'This user input field accepts check digit for a location.',
             'INPUT',
             'scannedCheckDigit',
             'scannedCheckDigit',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'scannedItem',
             'This user input field accepts item for the current pick.',
             'INPUT',
             'scannedItem',
             'scannedItem',
             NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scannedLocn',
                  'This user input field accepts Location barcode for the current detail.',
                  'INPUT',
                  'scannedLocn',
                  'recordLocn',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scannedPrintRequestor',
                  'This user input field accepts Print Requestor for printing labels. ',
                  'INPUT',
                  'scannedPrintRequestor',
                  'scannedPrintRequestor',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scannedResvLocnToLocateCart',
                  'This user input field accepts reserve location to loacte the tote cart after pick cart task has been completed.',
                  'INPUT',
                  'scannedResvLocnToLocateCart',
                  'scannedResvLocnToLocateCart',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scannedSlot',
                  'This user input field accepts slot number for the current pick.',
                  'INPUT',
                  'scannedSlot',
                  'scannedSlot',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scannedTote',
                  'This user input field accepts tote number to complete current pick.',
                  'INPUT',
                  'scannedTote',
                  'scannedTote',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'scannedTrackCase',
                  'This user input field accepts ILPN number for picks from active/casepick locations which are case tracked.',
                  'INPUT',
                  'scannedTrackCase',
                  'scannedTrackCase',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'serialNumbers',
                  'This intrinsic input field facilitates entry of serial numbers for outbound/complete serial tracked items.',
                  'INPUT',
                  'serialNumbers',
                  'serialNumbers',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'taskId',
                  'This user input field accepts task Id to identify and launch into picks. ',
                  'INPUT',
                  'taskId',
                  'taskId',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'RestartPackCart',
                  'This intrinsic action restarts pack pick cart process once current pick cart task is completed.',
                  'ACTION',
                  'RestartPackCart',
                  'RestartPackCart',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
        VALUES (
                  WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
                  'Pack Pick Cart',
                  'ReturnToMakeCart',
                  'This intrinsic action returns to make pick cart process once current pick cart task is completed. It is only applicable when there is an auto transition from make pick cart to pack pick cart based on verify pick cart menu parameter.',
                  'ACTION',
                  'ReturnToMakeCart',
                  'ReturnToMakeCart',
                  NULL);

INSERT INTO WORKFLOW_COMPONENT (WORKFLOW_COMPONENT_ID,
                                SCRIPT_NAME,
                                COMPONENT_NAME,
                                COMPONENT_DESCRIPTION,
                                COMPONENT_TYPE,
                                COMPONENT_LABEL_KEY,
                                PRE_MESSAGE_KEY,
                                POST_MESSAGE_KEY)
     VALUES (WORKFLOW_COMPONENT_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'CompleteDetailEnd',
             'This intrinsic action completes the current pick.',
             'ACTION',
             'CompleteDetailEnd',
             'CompleteDetailEnd',
             NULL);


INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Make oLPN Cart',
             'EndPickCart',
             'WMAMMPCENDCART');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Make oLPN Cart',
             'IndirectActivity',
             'WMAMMPCAILTASK');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Make oLPN Cart',
             'RecordLocation',
             'WMAMMPCRECLOCN');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Make Tote Cart',
             'EndPickCart',
             'WMAMMPCENDCART');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Make Tote Cart',
             'EndTote',
             'WMAMMPCENDTOTE');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Make Tote Cart',
             'IndirectActivity',
             'WMAMMPCAILTASK');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Make Tote Cart',
             'RecordLocation',
             'WMAMMPCRECLOCN');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Make Tote Cart',
             'ListTask',
             'WMAMMPCTSKSELN');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'EndCartAction',
             'WMAMPPCENDCART');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'EndOlpnOrToteAction',
             'WMAMPPCENDLPN');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'EndLpnActionAtPutConfirmInAggrMode',
             'WMAMPPCENDLPN');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'EndLpnActionInPartialPickAggrMode',
             'WMAMPPCENDLPN');


INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'IndirectActivity',
             'WMAMPPCAILTASK');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'RecordLocation',
             'WMAMPPCRECLOCN');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'ShortAction',
             'WMAMPPCSHORT');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'ShortActionInAggrMode',
             'WMAMPPCSHORT');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'ShortActionAtPutConfirmInAggrMode',
             'WMAMPPCSHORT');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'ShortActionInPartialPickAggrMode',
             'WMAMPPCSHORT');


INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'SkipAction',
             'WMAMPPCSKIP');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'SkipActionInAggrMode',
             'WMAMPPCSKIP');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'SkipActionAtPutConfirmInAggrMode',
             'WMAMPPCSKIP');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'SkipActionInPartialPickAggrMode',
             'WMAMPPCSKIP');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'SkipReplAction',
             'WMAMPPCSKPREPL');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'SkipReplActionInAggrMode',
             'WMAMPPCSKPREPL');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'SkipReplActionAtPutConfirmInAggrMode',
             'WMAMPPCSKPREPL');


INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'RemoveOlpnOrToteAction',
             'WMAMPPCREMVLPN');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'AlternateLocationAction',
             'WMAMPPCALTERNT');

INSERT INTO WORKFLOW_COMPONENT_PERM (WORKFLOW_COMPONENT_PERM_ID,
                                     SCRIPT_NAME,
                                     COMPONENT_NAME,
                                     PERMISSION_CODE)
     VALUES (WORKFLOW_COMP_PERM_ID_SEQ.NEXTVAL,
             'Pack Pick Cart',
             'PartialPickAction',
             'WMAMPPCPUTPART');

COMMIT;

-- DBTicket DB-5447

MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ACTIONS' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ACTIONS',
               'ACTIONS',
               'dm_mobile_label');

COMMIT;

-- DBTicket DB-5605
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'exitWarning' KEY FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'exitWarning',
               'Are you sure you want to exit?',
               'dm_mobile_label');
COMMIT;			   

-- DBTicket DB-5612
UPDATE FILTER_LAYOUT
   SET HIBERNATE_FIELD_NAME = 'rfMessage.sendToUserRole'
 WHERE     FIELD_NAME = 'RF_MESSAGE.USER_GROUP'
       AND OBJECT_TYPE = 'WM_SEND_MSG_TO_RF_USER';

UPDATE FILTER_LAYOUT
   SET FIELD_NAME = 'RF_MESSAGE.WORK_GRP_WORK_AREA'
 WHERE     FIELD_LABEL = 'WorkGroup/WorkArea'
       AND OBJECT_TYPE = 'WM_SEND_MSG_TO_RF_USER';
       
commit;

-- DBTicket DB-5763
MERGE INTO RESOURCES R
     USING (SELECT '/services/rest/slotting/*' URI FROM DUAL) B
        ON (R.URI = B.URI)
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID,
               URI,
               MODULE,
               URI_TYPE_ID)
       VALUES (SEQ_RESOURCE_ID.NEXTVAL,
               '/services/rest/slotting/*',
               'DMMOB',
               '2');

MERGE INTO RESOURCE_PERMISSION REP
     USING (SELECT RESOURCE_ID
              FROM RESOURCES
             WHERE URI = '/services/rest/slotting/*') B
        ON (REP.RESOURCE_ID = B.RESOURCE_ID AND REP.PERMISSION_CODE = 'UCLU')
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/services/rest/slotting/*'),
               'UCLU');

MERGE INTO RESOURCE_PERMISSION REP
     USING (SELECT RESOURCE_ID
              FROM RESOURCES
             WHERE URI = '/services/rest/slotting/*') B
        ON (    REP.RESOURCE_ID = B.RESOURCE_ID
            AND REP.PERMISSION_CODE = 'LMMEXTRNLWMV')
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/services/rest/slotting/*'),
               'LMMEXTRNLWMV');

MERGE INTO RESOURCE_PERMISSION REP
     USING (SELECT RESOURCE_ID
              FROM RESOURCES
             WHERE URI = '/services/rest/slotting/*') B
        ON (    REP.RESOURCE_ID = B.RESOURCE_ID
            AND REP.PERMISSION_CODE = 'LMMWRKMNTR')
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/services/rest/slotting/*'),
               'LMMWRKMNTR');

MERGE INTO RESOURCE_PERMISSION REP
     USING (SELECT RESOURCE_ID
              FROM RESOURCES
             WHERE URI = '/services/rest/slotting/*') B
        ON (    REP.RESOURCE_ID = B.RESOURCE_ID
            AND REP.PERMISSION_CODE = 'WMVTSKLST')
WHEN NOT MATCHED
THEN
   INSERT     (RESOURCE_ID, PERMISSION_CODE)
       VALUES ( (SELECT RESOURCE_ID
                   FROM RESOURCES
                  WHERE URI = '/services/rest/slotting/*'),
               'WMVTSKLST');
               
COMMIT;

-- DBTicket DB-5847
MERGE INTO LABEL L
     USING (SELECT 'dm_mobile_label' BUNDLE_NAME, 'ExptedSrlNbr' KEY
              FROM DUAL) B
        ON (L.KEY = B.KEY AND L.BUNDLE_NAME = B.BUNDLE_NAME)
WHEN NOT MATCHED
THEN
   INSERT     (L.LABEL_ID,
               L.KEY,
               L.VALUE,
               L.BUNDLE_NAME)
       VALUES (SEQ_LABEL_ID.NEXTVAL,
               'ExptedSrlNbr',
               'Expected Serial Number',
               'dm_mobile_label');

COMMIT;

-- DBTicket DB-5991
MERGE INTO XMENU_ITEM_PERMISSION XIP
     USING (SELECT XMENU_ITEM_ID
              FROM XMENU_ITEM
             WHERE     NAVIGATION_KEY =
                          'Mobilepicking.screen.makepickcart.MakePickCartScreen'
                   AND NAME = 'Make Pick Cart - oLPN') XI
        ON (XIP.XMENU_ITEM_ID = XI.XMENU_ITEM_ID)
WHEN MATCHED
THEN
   UPDATE SET XIP.PERMISSION_CODE = 'WMAMOBMAKPICRT'
WHEN NOT MATCHED
THEN
   INSERT     (XMENU_ITEM_ID,
               PERMISSION_CODE,
               CREATED_SOURCE,
               CREATED_SOURCE_TYPE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_SOURCE_TYPE,
               LAST_UPDATED_DTTM)
       VALUES (
                 (SELECT XMENU_ITEM_ID
                    FROM XMENU_ITEM
                   WHERE     NAME = 'Make Pick Cart - oLPN'
                         AND NAVIGATION_KEY =
                                'Mobilepicking.screen.makepickcart.MakePickCartScreen'),
                 'WMAMOBMAKPICRT',
                 NULL,
                 1,
                 CURRENT_TIMESTAMP,
                 NULL,
                 1,
                 CURRENT_TIMESTAMP);


MERGE INTO XMENU_ITEM_PERMISSION XIP
     USING (SELECT XMENU_ITEM_ID
              FROM XMENU_ITEM
             WHERE     NAVIGATION_KEY =
                          'Mobilepicking.screen.makepickcart.MakePickCartScreen'
                   AND NAME = 'Make Pick Cart - Tote') XI
        ON (XIP.XMENU_ITEM_ID = XI.XMENU_ITEM_ID)
WHEN MATCHED
THEN
   UPDATE SET XIP.PERMISSION_CODE = 'WMAMOBMAKPICRT'
WHEN NOT MATCHED
THEN
   INSERT     (XMENU_ITEM_ID,
               PERMISSION_CODE,
               CREATED_SOURCE,
               CREATED_SOURCE_TYPE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_SOURCE_TYPE,
               LAST_UPDATED_DTTM)
       VALUES (
                 (SELECT XMENU_ITEM_ID
                    FROM XMENU_ITEM
                   WHERE     NAME = 'Make Pick Cart - Tote'
                         AND NAVIGATION_KEY =
                                'Mobilepicking.screen.makepickcart.MakePickCartScreen'),
                 'WMAMOBMAKPICRT',
                 NULL,
                 1,
                 CURRENT_TIMESTAMP,
                 NULL,
                 1,
                 CURRENT_TIMESTAMP);



MERGE INTO XMENU_ITEM_PERMISSION XIP
     USING (SELECT XMENU_ITEM_ID
              FROM XMENU_ITEM
             WHERE     NAVIGATION_KEY =
                          'Mobilepicking.screen.packpickcart.PackPickCartScreen'
                   AND NAME = 'Pack Pick Cart') XI
        ON (XIP.XMENU_ITEM_ID = XI.XMENU_ITEM_ID)
WHEN MATCHED
THEN
   UPDATE SET XIP.PERMISSION_CODE = 'WMAMOBPKPIKCRT'
WHEN NOT MATCHED
THEN
   INSERT     (XMENU_ITEM_ID,
               PERMISSION_CODE,
               CREATED_SOURCE,
               CREATED_SOURCE_TYPE,
               CREATED_DTTM,
               LAST_UPDATED_SOURCE,
               LAST_UPDATED_SOURCE_TYPE,
               LAST_UPDATED_DTTM)
       VALUES (
                 (SELECT XMENU_ITEM_ID
                    FROM XMENU_ITEM
                   WHERE     NAME = 'Pack Pick Cart'
                         AND NAVIGATION_KEY =
                                'Mobilepicking.screen.packpickcart.PackPickCartScreen'),
                 'WMAMOBPKPIKCRT',
                 NULL,
                 1,
                 CURRENT_TIMESTAMP,
                 NULL,
                 1,
                 CURRENT_TIMESTAMP);

COMMIT;

-- DBTicket DB-8036
alter sequence SEQ_HIERARCHY_TYPES_TYPE_ID maxvalue 99999999;
alter sequence LABOR_MSG_MONITOR_ID_SEQ maxvalue 999999999;
alter sequence WORKFLOW_COMP_PERM_ID_SEQ maxvalue 999999999;
